

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tpmtools
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim jpid, wonum, typ, stat, icost, pmid, ro, ts, pmtskid As String
    Dim jpm As New Utilities
    Dim ap As New AppUtils
    Protected WithEvents lbldir As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgparts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblview As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetDGLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            ts = Request.QueryString("ts").ToString
            lbldir.Value = ts
            If ts = "0" Then
                wonum = Request.QueryString("wonum").ToString
                pmid = Request.QueryString("pmid").ToString
                lblwo.Value = wonum
                lblpmid.Value = pmid
            Else
                pmtskid = Request.QueryString("pmtskid").ToString
                lblpmtskid.Value = pmtskid
            End If

            jpm.Open()
            GetParts()
            jpm.Dispose()
        End If
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            If CatID = "PASS" Then
                iL = 0
            ElseIf CatID = "FAIL" Then
                iL = 1
            ElseIf CatID = "YES" Then
                iL = 0
            ElseIf CatID = "NO" Then
                iL = 1
            End If
        Else
            iL = -1
        End If
        Return iL
    End Function
    Private Sub GetParts()
        wonum = lblwo.Value
        pmid = lblpmid.Value
        typ = "t" 'lblview.Value
        ts = lbldir.Value
        pmtskid = lblpmtskid.Value
        If typ = "p" Then
            sql = "select m.*, j.* from wotpmparts m left join pmtracktpm j on j.pmtskid = m.pmtskid " _
            + " where m.pmid = '" & pmid & "' and m.wonum = '" & wonum & "' order by j.routing, j.funcid, j.tasknum"
        ElseIf typ = "t" Then
            If ts = "0" Then
                sql = "select m.*, m.toolnum as itemnum, m.wotsktoolid as wotskpartid, m.rate as cost, j.* from wotpmtools m " _
                + "left join pmtracktpm j on j.pmtskid = m.pmtskid " _
                + " where m.pmid = '" & pmid & "' And m.wonum = '" & wonum & "' order by j.routing, j.funcid, j.tasknum"
            Else
                sql = "select t.tasknum, m.itemid, m.toolnum as itemnum, m.qty, '' as used, '' as qtyused, '' as wotskpartid " _
             + "from pmtaskstpm t left join pmtasktoolstpm m on m.pmtskid = t.pmtskid where t.pmtskid = '" & pmtskid & "'"
            End If

        ElseIf typ = "l" Then
            sql = "select m.*, m.lubenum as itemnum, m.wotsklubeid as wotskpartid, j.* from wotpmlubes m left join pmtracktpm j on j.pmtskid = m.pmtskid " _
            + " where m.pmid = '" & pmid & "' And m.wonum = '" & wonum & "' order by j.routing, j.funcid, j.tasknum"
        End If

        dr = jpm.GetRdrData(sql)
        dgparts.DataSource = dr
        dgparts.DataBind()

    End Sub

    Private Sub dgparts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand
        dgparts.EditItemIndex = e.Item.ItemIndex
        jpm.Open()
        GetParts()
        jpm.Dispose()
    End Sub

    Private Sub dgparts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        dgparts.EditItemIndex = -1
        jpm.Open()
        GetParts()
        jpm.Dispose()
    End Sub

    Private Sub dgparts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparts.ItemDataBound
        typ = "t" 'lblview.Value
        If e.Item.ItemType = ListItemType.EditItem Then
            If typ = "l" Then
                Dim qbox As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
                qbox.Enabled = False
            End If
        End If
    End Sub

    Private Sub dgparts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        ts = lbldir.Value
        If ts = "0" Then
            'ts = should be "0" for production mode
            Dim qtystr As String = CType(e.Item.FindControl("txtqty"), TextBox).Text
            Dim coststr As String = CType(e.Item.FindControl("txtcost"), Label).Text
            Dim pid As String = CType(e.Item.FindControl("lblpid"), Label).Text
            Dim itemid As String = CType(e.Item.FindControl("lblitemid"), Label).Text
            Dim ddu As DropDownList = CType(e.Item.FindControl("ddus"), DropDownList)
            Dim us As String = ddu.SelectedValue
            Dim txtchk As Long
            typ = "t" 'lblview.Value
            If typ <> "l" Then
                Try
                    txtchk = System.Convert.ToDecimal(qtystr)
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr1730", "tpmtools.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
            Try
                txtchk = System.Convert.ToDecimal(coststr)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1731", "tpmtools.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            jpm.Open()
            wonum = lblwo.Value
            If typ = "p" Then
                sql = "update wotpmparts set qtyused = '" & qtystr & "', cost = '" & coststr & "', " _
                + "used = '" & us & "' where wotskpartid = '" & pid & "'; update wotpmparts set usedtotal = (qtyused * cost) " _
                + "where wotskpartid = '" & pid & "'"
                If us = "Y" Then
                    sql += "update workorder set actmatcost = (select sum(usedtotal) from wotpmparts where wonum = '" & wonum & "' and " _
                    + "jpid = '" & jpid & "' and used = 'Y') " _
                    + "where wonum = '" & wonum & "'; " _
                    + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                    + "where itemid = '" & itemid & "'"
                End If

            ElseIf typ = "t" Then
                sql = "update wotpmtools set qtyused = '" & qtystr & "', rate = '" & coststr & "', " _
                + "used = '" & us & "' where wotsktoolid = '" & pid & "'; update wotpmtools set usedtotal = (qtyused * rate) " _
                + "where wotsktoolid = '" & pid & "'"
                If us = "Y" Then
                    sql += "update workorder set acttoolcost = (select sum(usedtotal) from wotpmtools where wonum = '" & wonum & "' and " _
                    + "jpid = '" & jpid & "' and used = 'Y') " _
                    + "where wonum = '" & wonum & "'; " _
                    + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                    + "where itemid = '" & itemid & "'"
                End If

            ElseIf typ = "l" Then
                sql = "update wotpmlubes set cost = '" & coststr & "', " _
                + "used = '" & us & "' where wotsklubeid = '" & pid & "'; "
                If us = "Y" Then
                    sql += "update workorder set actlubecost = (select sum(cost) from wotpmlubes where wonum = '" & wonum & "' and " _
                    + "jpid = '" & jpid & "' and used = 'Y') where wonum = '" & wonum & "'; " _
                    + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                    + "where itemid = '" & itemid & "'"
                End If

            End If

            jpm.Update(sql)
            dgparts.EditItemIndex = -1
            GetParts()
            jpm.Dispose()
        End If

    End Sub
    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgparts.Columns(0).HeaderText = dlabs.GetDGPage("tpmtools.aspx", "dgparts", "0")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(2).HeaderText = dlabs.GetDGPage("tpmtools.aspx", "dgparts", "2")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(4).HeaderText = dlabs.GetDGPage("tpmtools.aspx", "dgparts", "4")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(5).HeaderText = dlabs.GetDGPage("tpmtools.aspx", "dgparts", "5")
        Catch ex As Exception
        End Try

    End Sub

End Class
