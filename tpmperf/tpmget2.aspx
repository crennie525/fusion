﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tpmget2.aspx.vb" Inherits="lucy_r12.tpmget2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
	<script language="javascript" type="text/javascript">
		function getminsrch() {
			var sid = document.getElementById("lblsid").value;
			var wo = "";
			var typ = "lup";
			var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
				clearall();
				var ret = eReturn.split("~");
				var eqid = ret[4];
				document.getElementById("lbleqid").value = ret[4];
				document.getElementById("tdeq").innerHTML = ret[5];
				document.getElementById("lbldept").value = ret[0];
				document.getElementById("lblclid").value = ret[2];
				gettasks();
			}
		}
		function gettasks() {
			tl = "5"
			cid = "0"
			sid = document.getElementById("lblsid").value;
			did = document.getElementById("lbldept").value;
			clid = document.getElementById("lblclid").value;
			eqid = document.getElementById("lbleqid").value;
			pmstr = ""
			doctyp = ""
			chk = document.getElementById("lblchk").value;
			coid = document.getElementById("lblcoid").value;
			fuid = document.getElementById("lblfuid").value;
			tcnt = document.getElementById("lbltaskcnt").value;
			pmtyp = ""
			typ = document.getElementById("lbltyp").value;
			lid = document.getElementById("lbllid").value;
			var ts = document.getElementById("lblts").value;
			window.parent.handletasks("tpmgrid.aspx?comid=" + coid + "&start=yes&tl=5&ts=" + ts + "&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid, "ok")
		}
		function getlocs1() {
			var sid = document.getElementById("lblsid").value;
			var wo = "";
			var typ = "lup"
			var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lup&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
			if (eReturn) {
				clearall();
				var ret = eReturn.split("~");
				var eqid = ret[3];
				document.getElementById("lbleqid").value = ret[3];
				document.getElementById("tdeq").innerHTML = ret[2];
				document.getElementById("lbllid").value = ret[0];
				gettasks();
			}
		}
		function clearall() {
			document.getElementById("lbleqid").value = "";
			document.getElementById("tdeq").innerHTML = "";
			document.getElementById("tddesc").innerHTML = "";
			//document.getElementById("lbleq").value = "";
		}
		function checkpg() {
			var log = document.getElementById("lbllog").value;
			if (log == "no") window.parent.setref();
			var app = document.getElementById("appchk").value;
			if (app == "switch") window.parent.handleapp(app);
			chk = document.getElementById("lblpar").value;
			var chk = document.getElementById("lblpar").value;
			////alert(chk)
			if (chk == "site") {
				valu = document.getElementById("lblsid").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk == "dept") {
				valu = document.getElementById("lbldept").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk == "cell") {
				valu = document.getElementById("lblclid").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk == "eq") {
				valu = document.getElementById("lbleqid").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk == "func") {
				valu = document.getElementById("lblfuid").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk == "level") {
				valu = document.getElementById("lbltl").value;
				window.parent.handleeq(chk, valu);
			}
			else if (chk == "nopm") {
				document.getElementById("lblpar").value = "";
				AddNewPM();
			}
			var clean = document.getElementById("lblcleantasks").value;
			if (clean == "1") {
				document.getElementById("lblcleantasks").value = "2";
				window.parent.handletasks("tpmgrid.aspx?start=no")
			}
			var gototasks = document.getElementById("lblgototasks").value;
			if (gototasks == "1") {
				document.getElementById("lblgototasks").value = "0";
				document.getElementById("lblcleantasks").value = "0";
				tl = "5"//document.getElementById("lbltl").value;
				cid = "0"//document.getElementById("lblcid").value;
				sid = document.getElementById("lblsid").value;
				did = document.getElementById("lbldept").value;
				clid = document.getElementById("lblclid").value;
				eqid = document.getElementById("lbleqid").value;
				pmstr = ""//document.getElementById("lblpmnum").value;
				doctyp = ""//document.getElementById("lbldoctype").value;
				chk = document.getElementById("lblchk").value;
				coid = document.getElementById("lblcoid").value;
				//alert(coid)
				fuid = document.getElementById("lblfuid").value;
				tcnt = document.getElementById("lbltaskcnt").value;
				pmtyp = ""//document.getElementById("lblpmtyp").value;
				typ = document.getElementById("lbltyp").value;
				lid = document.getElementById("lbllid").value;
				var ts = document.getElementById("lblts").value;
				//alert(tcnt)
				if (tcnt == "1") {
					//alert("tpmgrid.aspx?comid=" + coid + "&start=yes&tl=5&ts=" + ts + "&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid)
					window.parent.handletasks("tpmgrid.aspx?comid=" + coid + "&start=yes&tl=5&ts=" + ts + "&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid, "ok")
				}
				else if (tcnt == "0") {
					//alert("PMTaskDivFunc.aspx?start=yes&tl=5&pmtyp=")
					//window.parent.handletasks("tpmtasks.aspx?start=yes&tl=5&pmtyp=" + pmtyp + "&chk=" + chk + "&cid=" + cid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&eqid=" + eqid + "&typ=" + typ + "&lid=" + lid, "ok")
				}
				else if (tcnt == "2") {
					//window.parent.handletasks("tpmtaskgrid.aspx?start=no", "dept")
				}
				else if (tcnt == "3") {
					//window.parent.handletasks("tpmtaskgrid.aspx?start=no", "cell")
				}
				else if (tcnt == "4") {
					//window.parent.handletasks("tpmtaskgrid.aspx?start=no", "eq")
				}
				else if (tcnt == "5") {
					//window.parent.handletasks("tpmtaskgrid.aspx?start=no", "fu")
				}
			}
			var arch = document.getElementById("lblgetarch").value;
			if (arch == "yes") {
				chk = document.getElementById("lblchk").value;
				did = document.getElementById("lbldept").value;
				clid = document.getElementById("lblclid").value;
				window.parent.getarch(chk, did, clid);
			}

			window.setTimeout("checkit2();", 100);
		}
		function checkit2() {
			var chk2 = document.getElementById("lblpar2").value;
			if (chk2 == "chk") {
				valu = document.getElementById("lblchk").value;
				//alert(valu)
				window.parent.handleeq2(chk2, valu);
			}
			else if (chk2 == "dchk") {
				valu = document.getElementById("lbldchk").value;
				window.parent.handleeq2(chk2, valu);
			}
		}
		function handleapp() {
			var app = document.getElementById("appchk").value;
			if (app == "switch") window.parent.handleapp(app);
		}
	</script>
</head>
<body onload="checkpg();">
	<form id="form1" runat="server">
	<div style="POSITION: absolute; TOP: 0px; LEFT: 4px">
				<table cellspacing="0" cellpadding="0" width="740">
					<tr>
						<td width="92"></td>
						<td width="180"></td>
						<td width="378"></td>
					</tr>
					<tr>
						<td colspan="3">
							<table>
								<tr>
									<td id="tddepts" class="bluelabel" runat="server">Use Departments</td>
									<td><IMG onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
									<td id="tdlocs1" class="bluelabel" runat="server">Use Locations</td>
									<td><IMG onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr id="eqdiv" height="20" runat="server">
						<td class="label"><asp:Label id="lang638" runat="server">Equipment#</asp:Label></td>
						<td class="plainlabel" id="tdeq" runat="server">
						</td>
						<td class="label" id="tddesc" runat="server"></td>
					</tr>
				</table>
			</div>
			<input id="lblsid" type="hidden" runat="server"> <input id="lbllog" type="hidden" runat="server">
			<input id="lbldept" type="hidden" runat="server"> <input id="lblpar" type="hidden" runat="server">
			<input id="lblchk" type="hidden" runat="server"> <input id="lblpar2" type="hidden" runat="server">
			<input id="lbltyp" type="hidden" runat="server"> <input id="lblclid" type="hidden" runat="server">
			<input id="lblpchk" type="hidden" runat="server"><input id="lblcleantasks" type="hidden" runat="server">
			<input id="lbltaskcnt" type="hidden" runat="server"> <input id="lblgetarch" type="hidden" runat="server">
			<input id="lblgototasks" type="hidden" runat="server"> <input id="lbllid" type="hidden" runat="server">
			<input id="lbleqid" type="hidden" runat="server"><input id="appchk" type="hidden" runat="server">
			<input type="hidden" id="lblts" runat="server"> <input type="hidden" id="lblfuid" runat="server">
			<input type="hidden" id="lblcoid" runat="server"> <input type="hidden" id="lbltl" runat="server">
			<input type="hidden" id="lblislabor" runat="server"> <input type="hidden" id="lbluserid" runat="server">
	</form>
</body>
</html>
