

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class tpmtask
    Inherits System.Web.UI.Page
    Dim mu As New mmenu_utils_a

    Protected WithEvents ovid297 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang3632 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3631 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3630 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3629 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3628 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3627 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3626 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3625 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3624 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3623 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3622 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3621 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3620 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3619 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3618 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3617 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3616 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3615 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3614 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3613 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3612 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3611 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, funcid, tasknum, pmid, eqid, freq, rd, typ, ts, wonum, pmhid, fcust, pmtskid, psite, sid, coi As String
    Dim username, userid As String
    Dim news As New Utilities
    Dim dr As SqlDataReader
    Dim ap As New AppUtils
    Protected WithEvents tdfail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmeas As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfm1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm4 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm5 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm1i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm2i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm3i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm4i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm5i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblts As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trtools As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents iftools As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents trparts As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ifparts As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents trpartsmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trtoolsmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trlubesmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trlubes As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents iflubes As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents trmeasmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trmeas As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ifmeas As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents imeas As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents itool As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilube As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ipart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblmalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trshiftdays As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trshift1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents cb1o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents trshift2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents cb2o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents trshift3 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents cb3o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblcalday As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdstat As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtocomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblacost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trfail As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdfaili As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents iffail As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents trfaili As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdqs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trqs As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trpm As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trtimemsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdtimemsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trtime As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents iftime As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents divtask As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tdtask2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgfail As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgpart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgtool As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imglube As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgmag As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgmeas As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfcust As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgcomptask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcomptpm As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldays As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnextdate As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim PageNumber As Integer
    Protected WithEvents tduser As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsite As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsite1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tduser1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblshift As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblusetdt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusetotal As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblttime As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldtime As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblacttime As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblactdtime As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldalert As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lbltdmeas As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltdpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltdlube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltdtool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim login As String
    Dim pdt, ttt As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents lblbpage As System.Web.UI.WebControls.Label
    Protected WithEvents tdtpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdpline1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bfirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bnext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents blast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsub As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdtools As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlubes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdparts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblititles As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmaxtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            userid = HttpContext.Current.Session("userid").ToString()
            username = HttpContext.Current.Session("username").ToString()
            psite = HttpContext.Current.Session("psite").ToString()
            lbluserid.Value = userid
            lblusername.Value = username
            tduser.InnerHtml = username
            tdsite.InnerHtml = psite
            tduser1.InnerHtml = username
            tdsite1.InnerHtml = psite
        Catch ex As Exception
            lblsubmit.Value = "return"
        End Try
        If Not IsPostBack Then
            Try

            Catch ex As Exception
                lblsubmit.Value = "return"
            End Try
            coi = mu.COMPI
            lblcoi.Value = coi
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            ts = Request.QueryString("ts").ToString
            lblts.Value = ts
            tasknum = "1" 'Request.QueryString("tasknum").ToString
            lbltasknum.Value = tasknum
            news.Open()
            If ts = "0" Then
                pmid = Request.QueryString("pmid").ToString
                lblpmid.Value = pmid
                pmhid = Request.QueryString("pmhid").ToString
                lblpmhid.Value = pmhid
                wonum = Request.QueryString("wonum").ToString
                lblwo.Value = wonum
                GetSTasks(pmid, tasknum)
                trqs.Attributes.Add("class", "details")
                trpm.Attributes.Add("class", "view")
            Else
                eqid = Request.QueryString("eqid").ToString '"275" '
                lbleqid.Value = eqid
                freq = Request.QueryString("freq").ToString '"7" '
                lblfreq.Value = freq
                rd = Request.QueryString("rd").ToString '"Running" '
                lblrd.Value = rd
                typ = Request.QueryString("typ").ToString '"2" '
                lbltyp.Value = typ
                GetOTasks(eqid, freq, rd, typ, tasknum)
                trqs.Attributes.Add("class", "view")
                trpm.Attributes.Add("class", "details")
            End If
            news.Dispose()
            pdt = ap.PDTTEntry
            If pdt = "lvdttt" Then
                lblusetdt.Value = "no"

            Else
                lblusetdt.Value = "yes"

            End If
            ttt = ap.TTTTEntry
            If ttt = "lvtptt" Then
                lblusetotal.Value = "no"

            Else
                lblusetotal.Value = "yes"

            End If
        Else
            tasknum = lbltasknum.Value
            funcid = lblfuncid.Value
            If Request.Form("lblsubmit") = "next" Then
                news.Open()
                GetNext()
                news.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                news.Open()
                PageNumber = lblmaxtask.Value
                lbltasknum.Value = PageNumber
                ts = lblts.Value
                If ts = "0" Then
                    pmid = lblpmid.Value
                    GetSTasks(pmid, PageNumber)
                Else
                    eqid = lbleqid.Value
                    freq = lblfreq.Value
                    rd = lblrd.Value
                    typ = lbltyp.Value
                    GetOTasks(eqid, freq, rd, typ, PageNumber)
                End If
                news.Dispose()
                lblsubmit.Value = ""
                Dim maxtask As String = lblmaxtask.Value
                lblbpage.Text = "Task " & PageNumber & " of " & maxtask
            ElseIf Request.Form("lblsubmit") = "prev" Then
                news.Open()
                GetPrev()
                news.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                news.Open()
                PageNumber = 1
                lbltasknum.Value = PageNumber
                ts = lblts.Value
                If ts = "0" Then
                    pmid = lblpmid.Value
                    GetSTasks(pmid, PageNumber)
                Else
                    eqid = lbleqid.Value
                    freq = lblfreq.Value
                    rd = lblrd.Value
                    typ = lbltyp.Value
                    GetOTasks(eqid, freq, rd, typ, PageNumber)
                End If
                news.Dispose()
                lblsubmit.Value = ""
                Dim maxtask As String = lblmaxtask.Value
                lblbpage.Text = "Task " & PageNumber & " of " & maxtask
            ElseIf Request.Form("lblsubmit") = "checktask" Then
                lblsubmit.Value = ""
                news.Open()
                PageNumber = lbltasknum.Value
                lbltasknum.Value = PageNumber
                ts = lblts.Value
                If ts = "0" Then
                    CheckTaskComp()
                    pmid = lblpmid.Value
                    GetSTasks(pmid, PageNumber)
                Else
                    eqid = lbleqid.Value
                    freq = lblfreq.Value
                    rd = lblrd.Value
                    typ = lbltyp.Value
                    GetOTasks(eqid, freq, rd, typ, PageNumber)
                End If
                news.Dispose()
            ElseIf Request.Form("lblsubmit") = "comptask" Then

                lblsubmit.Value = ""
                news.Open()
                PageNumber = lbltasknum.Value
                lbltasknum.Value = PageNumber
                ts = lblts.Value
                If ts = "0" Then
                    CompTask()
                    pmid = lblpmid.Value
                    GetSTasks(pmid, PageNumber)
                Else
                    eqid = lbleqid.Value
                    freq = lblfreq.Value
                    rd = lblrd.Value
                    typ = lbltyp.Value
                    GetOTasks(eqid, freq, rd, typ, PageNumber)
                End If
                news.Dispose()
            ElseIf Request.Form("lblsubmit") = "checktpm" Then
                lblsubmit.Value = ""
                news.Open()
                CheckActuals()
                news.Dispose()
            End If
        End If
    End Sub
    Private Sub CheckActuals(Optional ByVal bypass As Integer = 1)
        Dim icost As String = ap.InvEntry
        pmid = lblpmid.Value
        Dim won As String = lblwo.Value
        userid = lbluserid.Value
        username = lblusername.Value
        sql = "usp_uptpmcosts '" & won & "','" & pmid & "','" & userid & "','" & username & "','" & icost & "','" & bypass & "'"
        'usp_uptpmcosts(@wonum int, @pmid int, @userid int, @username varchar(50), @icost varchar(50))
        news.Update(sql)
        CompWO()

    End Sub
    Private Sub CompWO()
        wonum = lblwo.Value
        Dim cndate As Date = news.CNOW
        Dim pdt, ttt As String
        pdt = lblusetdt.Value
        ttt = lblusetotal.Value
        coi = lblcoi.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        fcust = lblfcust.Value
        Dim usr As String = HttpContext.Current.Session("username").ToString()
        Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
        Dim nd As String = "" 'lblretday.Value
        Dim cd As Integer
        Dim nnd As String
        Dim days As String = lbldays.Value
        Dim cndt As String = lblnextdate.Value
        Dim cndy As String
        Dim i, n, nn, nnn, tn As Integer
        If days <> "no" Then
            If nd = "" Then
                Try
                    Dim tndi As Integer = System.Convert.ToDateTime(cndt).DayOfWeek
                    Select Case tndi
                        Case "1"
                            nd = "M"
                        Case "2"
                            nd = "Tu"
                        Case "3"
                            nd = "W"
                        Case "4"
                            nd = "Th"
                        Case "5"
                            nd = "F"
                        Case "6"
                            nd = "Sa"
                        Case "7"
                            nd = "Su"
                        Case Else
                            Dim strMessage As String = tmod.getmsg("cdstr1718", "tpmtask.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            Exit Sub
                    End Select
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr1719", "tpmtask.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try

            End If
            Select Case nd
                Case "M"
                    cd = 1
                Case "Tu"
                    cd = 2
                Case "T"
                    cd = 2
                Case "W"
                    cd = 3
                Case "Th"
                    cd = 4
                Case "F"
                    cd = 5
                Case "Sa"
                    cd = 6
                Case "Su"
                    cd = 7
                Case Else
                    cd = -1
            End Select

            If cd <> -1 Then
                Dim daysarr() As String = days.Split(",")
                For i = 0 To daysarr.Length - 1
                    If daysarr(i) = nd Then
                        If i < daysarr.Length - 1 Then
                            n = i + 1
                        Else
                            n = 0
                        End If
                        cndy = daysarr(n)
                        Select Case cndy
                            Case "M"
                                nn = 1
                            Case "Tu"
                                nn = 2
                            Case "T"
                                nn = 2
                            Case "W"
                                nn = 3
                            Case "Th"
                                nn = 4
                            Case "F"
                                nn = 5
                            Case "Sa"
                                nn = 6
                            Case "Su"
                                nn = 7
                            Case Else
                                nn = -1
                        End Select
                        If nn <> -1 Then
                            If nn > cd Then
                                nnn = nn - cd 'nnn = days to add
                            ElseIf nn < cd Then
                                tn = 7 - nn
                                nnn = cd + nn
                                nnn = (7 - cd) + nn
                            ElseIf nn = cd Then
                                nnn = 7
                            End If
                            Try
                                nnd = DateAdd(DateInterval.Day, nnn, System.Convert.ToDateTime(cndt))
                            Catch ex As Exception
                                Dim strMessage As String = tmod.getmsg("cdstr1720", "tpmtask.aspx.vb")

                                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                                Exit Sub
                            End Try

                        Else
                            'error msg
                            Dim strMessage As String = tmod.getmsg("cdstr1721", "tpmtask.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            Exit Sub
                        End If
                        Exit For
                    End If
                Next
            Else
                'error msg
                Dim strMessage As String = tmod.getmsg("cdstr1722", "tpmtask.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

        End If

        sql = "usp_comppm1tpm1 '" & pmid & "', '" & pmhid & "', '" & fcust & "', '" & nd & "', '" & cndy & "', '" & nnd & "', '" & ustr & "'"
        sql = "usp_comppm1tpm3 '" & pmid & "', '" & pmhid & "', '" & fcust & "', '" & nd & "', '" & cndy & "', '" & nnd & "', '" & wonum & "','" & cndate & "','" & ttt & "', " _
        + "'" & pdt & "','" & ustr & "','" & coi & "'"
        'pmi.Update(sql)
        Dim newdate As String
        'newdate = Now
        newdate = news.strScalar(sql)
        newdate = CType(newdate, DateTime)

        Dim won As String = lblwo.Value
        sql = "usp_upreserved '" & won & "'"
        news.Update(sql)
        lblsubmit.Value = "return"
    End Sub
    Private Sub CheckTaskComp()
        pmid = lblpmid.Value
        pmtskid = lblpmtskid.Value
        Dim won As String = lblwo.Value
        Dim wom, womc, wojpm, wojpmc As Integer
        Dim esthrs, acthrs As Decimal
        Dim chkalert As Integer = 0
        Dim mchk As String = lblmalert.Value
        lblmalert.Value = ""

        'Check Measurements
        If mchk <> "ok" Then
            sql = "select womc = (select count(*) from pmTaskMeasDetMantpm where pmid = '" & pmid & "' " _
                + "and measurement is null and pmtskid = '" & pmtskid & "'), " _
                + "wom = (select count(*) from pmTaskMeasDetMantpm where pmid = '" & pmid & "' and pmtskid = '" & pmtskid & "')"
            dr = news.GetRdrData(sql)
            While dr.Read
                wom = dr.Item("wom").ToString
                womc = dr.Item("womc").ToString
            End While
            dr.Close()
            If (womc <> 0 And wom <> 0) Then
                chkalert += 1
                lblmalert.Value = "m"
            End If
        End If

        'Check Total Time
        Dim esthrsstr, acthrsstr
        sql = "select isnull(ttime, 0) as ttime, isnull(acttime, 0) as acttime from pmtracktpm where pmid = '" & pmid & "' and pmtskid = '" & pmtskid & "'"
        dr = news.GetRdrData(sql)
        While dr.Read
            esthrs = dr.Item("ttime").ToString
            acthrs = dr.Item("acttime").ToString
        End While
        dr.Close()
        If esthrs <> 0 Then
            If acthrs = 0 Then
                If mchk <> "ok" Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "h"
                    Else
                        lblmalert.Value += ",h"
                    End If

                Else
                    SaveTTime(esthrs, esthrs)
                End If

            End If
        End If

        'Check Total Down Time
        Dim estrd, actrd As Decimal
        sql = "select isnull(rd, 0) as ttime, isnull(actrd, 0) as acttime from pmtracktpm where pmid = '" & pmid & "' and pmtskid = '" & pmtskid & "'"
        dr = news.GetRdrData(sql)
        While dr.Read
            estrd = dr.Item("ttime").ToString
            actrd = dr.Item("acttime").ToString
        End While
        dr.Close()
        If estrd <> 0 Then
            If actrd = 0 Then
                If mchk <> "ok" Then
                    chkalert += 1
                    If lblmalert.Value = "" Then
                        lblmalert.Value = "d"
                    Else
                        lblmalert.Value += ",d"
                    End If

                Else
                    SaveDTime(estrd)
                End If

            End If
        End If

        If chkalert = 0 Then
            'CheckActuals(won)
            'CompWO()
            CompTask()
        Else
            Exit Sub
        End If


    End Sub
    Private Sub SaveTTime(ByVal atime As String, ByVal etime As String)
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim pmtid As String = lblpmtid.Value
        Dim wonum As String = lblwo.Value
        Dim usetotal As String = lblusetotal.Value
        If usetotal = "yes" Then
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToDecimal(atime)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1723", "tpmtask.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            sql = "update pmtracktpm set acttime = '" & atime & "' " _
            + "where pmtid = '" & pmtid & "'"
            news.Update(sql)
        Else
            Exit Sub
        End If
    End Sub
    Private Sub SaveDTime(ByVal ard As String)
        Dim usetdt As String = lblusetdt.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim pmtid As String = lblpmtid.Value
        Dim eqid As String = lbleqid.Value
        Dim wonum As String = lblwo.Value
        If usetdt = "no" Then
            Dim ardchk As Long
            Try
                ardchk = System.Convert.ToDecimal(ard)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1724", "tpmtask.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        sql = "update pmtracktpm set actrd = '" & ard & "' " _
         + "where pmtid = '" & pmtid & "'"
        news.Update(sql)

    End Sub
    Private Sub CompTask()
        pmid = lblpmid.Value
        pmtskid = lblpmtskid.Value
        sql = "update pmtracktpm set taskcomp = 1 where pmid = '" & pmid & "' and pmtskid = '" & pmtskid & "'"
        news.Update(sql)
    End Sub
    Private Sub GetNext()
        tasknum = lbltasknum.Value
        funcid = lblfuncid.Value
        Try
            Dim pg As Integer = lbltasknum.Value
            PageNumber = pg + 1
            lbltasknum.Value = PageNumber
            ts = lblts.Value
            If ts = "0" Then
                pmid = lblpmid.Value
                GetSTasks(pmid, PageNumber)
            Else
                eqid = lbleqid.Value
                freq = lblfreq.Value
                rd = lblrd.Value
                typ = lbltyp.Value
                GetOTasks(eqid, freq, rd, typ, PageNumber)
            End If
            Dim maxtask As String = lblmaxtask.Value
            lblbpage.Text = "Task " & PageNumber & " of " & maxtask
        Catch ex As Exception
            news.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1725", "tpmtask.aspx.vb")

            news.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        tasknum = lbltasknum.Value
        funcid = lblfuncid.Value
        Try
            Dim pg As Integer = lbltasknum.Value
            PageNumber = pg - 1
            lbltasknum.Value = PageNumber
            ts = lblts.Value
            If ts = "0" Then
                pmid = lblpmid.Value
                GetSTasks(pmid, PageNumber)
            Else
                eqid = lbleqid.Value
                freq = lblfreq.Value
                rd = lblrd.Value
                typ = lbltyp.Value
                GetOTasks(eqid, freq, rd, typ, PageNumber)
            End If
            Dim maxtask As String = lblmaxtask.Value
            lblbpage.Text = "Task " & PageNumber & " of " & maxtask
        Catch ex As Exception
            news.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1726", "tpmtask.aspx.vb")

            news.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetOTasks(ByVal eqid As String, ByVal freq As String, ByVal rd As String, ByVal typ As String, ByVal tasknum As String)
        Dim mtask As Integer
        sql = "usp_getWITotalTPMperfcnt '" & eqid & "','" & freq & "','" & rd & "','" & typ & "'"
        mtask = news.Scalar(sql)
        lblmaxtask.Value = mtask

        'temp
        tdtocomp.InnerHtml = mtask
        '

        sql = "usp_getWITotalTPMperf '" & eqid & "','" & freq & "','" & rd & "','" & typ & "','" & tasknum & "','1'"
        Dim skill, task, func, comp, lubes, parts, tools, meas, pmtskid, itask, shift, days, calday, subcnt As String
        Dim shift1, shift2, shift3, first, second, third, ttid, eqnum As String
        Dim fm1, fm2, fm3, fm4, fm5 As String
        Dim fm1i, fm2i, fm3i, fm4i, fm5i As String
        Dim fm1o, fm2o, fm3o, fm4o, fm5o As String
        Dim m1, m2, m3, m4, m5 As String
        Dim ms1, ms2, ms3, ms4, ms5 As String
        lblbpage.Text = "Task " & tasknum & " of " & mtask
        dr = news.GetRdrData(sql)
        While dr.Read
            pmtskid = dr.Item("pmtskid").ToString
            ttid = dr.Item("ttid").ToString
            eqnum = dr.Item("eqnum").ToString
            funcid = dr.Item("funcid").ToString
            lblfuncid.Value = funcid
            skill = dr.Item("freq").ToString
            task = dr.Item("task").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            lubes = dr.Item("lubes").ToString
            meas = dr.Item("meas").ToString
            itask = dr.Item("tasknum").ToString
            lbltasknum1.Value = itask
            fm1 = dr.Item("fm1s").ToString

            fm2 = dr.Item("fm2s").ToString
            fm3 = dr.Item("fm3s").ToString
            fm4 = dr.Item("fm4s").ToString
            fm5 = dr.Item("fm5s").ToString

            fm1i = dr.Item("fm1id").ToString
            fm2i = dr.Item("fm2id").ToString
            fm3i = dr.Item("fm3id").ToString
            fm4i = dr.Item("fm4id").ToString
            fm5i = dr.Item("fm5id").ToString

            m1 = dr.Item("tmdid1").ToString
            m2 = dr.Item("tmdid2").ToString
            m3 = dr.Item("tmdid3").ToString
            m4 = dr.Item("tmdid4").ToString
            m5 = dr.Item("tmdid5").ToString

            ms1 = dr.Item("mstr1").ToString
            ms2 = dr.Item("mstr2").ToString
            ms3 = dr.Item("mstr3").ToString
            ms4 = dr.Item("mstr4").ToString
            ms5 = dr.Item("mstr5").ToString


            subcnt = dr.Item("subcnt").ToString

            shift1 = dr.Item("shift1").ToString
            shift2 = dr.Item("shift2").ToString
            shift3 = dr.Item("shift3").ToString

            first = dr.Item("first").ToString
            second = dr.Item("second").ToString
            third = dr.Item("third").ToString

        End While
        dr.Close()

        tdeq.InnerHtml = eqnum

        Dim sbimg As String
        If ttid = "2" Then
            sbimg = "../images/appbuttons/minibuttons/tpmclean.gif"
        ElseIf ttid = "3" Then
            sbimg = "../images/appbuttons/minibuttons/tpmlube.gif"
        ElseIf ttid = "5" Then
            sbimg = "../images/appbuttons/minibuttons/tpminsp.gif"
        Else
            sbimg = "no"
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""350"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""310""></td></tr>")

        If Len(task) > 0 Then
            If sbimg <> "no" Then
                sb.Append("<tr><td colspan=""3"" class=""plainlabelblue14""><img src=""" & sbimg & """ align=""left"" hspace=""5"" vspace=""4""><font class=""plainlabel14""><b>Task</b></font> :" & task & "</td></tr>")
            Else
                sb.Append("<tr><td colspan=""3"" class=""plainlabelblue14""><font class=""plainlabel14""><b>Task</b></font> :" & task & "</td></tr>")
            End If

        Else
            sb.Append("<tr><td colspan=""3"" class=""plainlabelblue14""><b>Task:</b> No Task Description Provided</td></tr>")
        End If

        sb.Append("</table>")

        Dim fmout As String
        If Len(fm1) <> 0 Then
            fmout = "<a href=""#"" class=""A1"" onclick=""openfmo('" & fm1i & "','" & pmtskid & "');"">" & fm1 & "</a>"
        End If
        If Len(fm2) <> 0 Then
            fmout += "; " & "<a href=""#"" class=""A1"" onclick=""openfmo('" & fm2i & "','" & pmtskid & "');"">" & fm2 & "</a>"
        End If
        If Len(fm3) <> 0 Then
            fmout += "; " & "<a href=""#"" class=""A1"" onclick=""openfmo('" & fm3i & "','" & pmtskid & "');"">" & fm3 & "</a>"
        End If
        If Len(fm4) <> 0 Then
            fmout += "; " & "<a href=""#"" class=""A1"" onclick=""openfmo('" & fm4i & "','" & pmtskid & "');"">" & fm4 & "</a>"
        End If
        If Len(fm5) <> 0 Then
            fmout += "; " & "<a href=""#"" class=""A1"" onclick=""openfmo('" & fm5i & "','" & pmtskid & "');"">" & fm5 & "</a>"
        End If

        Dim mout As String
        If Len(m1) <> 0 Then
            mout = "<a href=""#"" class=""A1"" onclick=""openm1('" & m1 & "','" & pmtskid & "');"">" & ms1 & "</a>"
        End If
        If Len(m2) <> 0 Then
            mout += "; " & "<a href=""#"" class=""A1"" onclick=""openm1('" & m2 & "','" & pmtskid & "');"">" & ms2 & "</a>"
        End If
        If Len(m3) <> 0 Then
            mout += "; " & "<a href=""#"" class=""A1"" onclick=""openm1('" & m3 & "','" & pmtskid & "');"">" & ms3 & "</a>"
        End If
        If Len(m4) <> 0 Then
            mout += "; " & "<a href=""#"" class=""A1"" onclick=""openm1('" & m4 & "','" & pmtskid & "');"">" & ms4 & "</a>"
        End If
        If Len(m5) <> 0 Then
            mout += "; " & "<a href=""#"" class=""A1"" onclick=""openm1('" & m5 & "','" & pmtskid & "');"">" & ms5 & "</a>"
        End If


        divtask.InnerHtml = sb.ToString

        Dim sb1 As New System.Text.StringBuilder
        sb1.Append("<table>")
        If Len(fmout) <> 0 Then
            sb1.Append("<tr><td colspan=""1"" class=""plainlabel14""><b>Failure Modes:</b></td><td class=""plainlabelblue14"">" & fmout & "</td></tr>")
            imgfail.Attributes.Add("src", "../images/appbuttons/minibuttons/tpmfail.gif")
            imgfail.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov335", "tpmtask.aspx.vb") & "')")
            imgfail.Attributes.Add("onmouseout", "return nd()")
        Else
            imgfail.Attributes.Add("src", "../images/appbuttons/minibuttons/tpmfaildis.gif")
            imgfail.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov336", "tpmtask.aspx.vb") & "')")
            imgfail.Attributes.Add("onmouseout", "return nd()")
        End If
        If lubes <> "none" Then
            sb1.Append("<tr>")
            sb1.Append("<td colspan=""1"" class=""plainlabel14""><b>Lubes:</b></td><td class=""plainlabelblue14"">" & lubes & "</td></tr>")
            imglube.Attributes.Add("src", "../images/appbuttons/minibuttons/lubetrans.gif")
            imglube.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov337", "tpmtask.aspx.vb") & "')")
            imglube.Attributes.Add("onmouseout", "return nd()")
        Else
            imglube.Attributes.Add("src", "../images/appbuttons/minibuttons/lubetransdis.gif")
            imglube.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov338", "tpmtask.aspx.vb") & "')")
            imglube.Attributes.Add("onmouseout", "return nd()")
        End If
        If tools <> "none" Then
            sb1.Append("<tr>")
            sb1.Append("<td colspan=""1"" class=""plainlabel14""><b>Tools:</b></td><td class=""plainlabelblue14"">" & tools & "</td></tr>")
            imgtool.Attributes.Add("src", "../images/appbuttons/minibuttons/tooltrans.gif")
            imgtool.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov339", "tpmtask.aspx.vb") & "')")
            imgtool.Attributes.Add("onmouseout", "return nd()")
        Else
            imgtool.Attributes.Add("src", "../images/appbuttons/minibuttons/tooltransdis.gif")
            imgtool.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov340", "tpmtask.aspx.vb") & "')")
            imgtool.Attributes.Add("onmouseout", "return nd()")
        End If
        If parts <> "none" Then
            sb1.Append("<tr>")
            sb1.Append("<td colspan=""1"" class=""plainlabel14""><b>Parts:</b></td><td class=""plainlabelblue14"">" & parts & "</td></tr>")
            imgpart.Attributes.Add("src", "../images/appbuttons/minibuttons/parttrans.gif")
            imgpart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov341", "tpmtask.aspx.vb") & "')")
            imgpart.Attributes.Add("onmouseout", "return nd()")
        Else
            imgpart.Attributes.Add("src", "../images/appbuttons/minibuttons/parttransdis.gif")
            imgpart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov342", "tpmtask.aspx.vb") & "')")
            imgpart.Attributes.Add("onmouseout", "return nd()")
        End If
        If meas <> "none" Then
            sb1.Append("<tr>")
            sb1.Append("<td colspan=""1"" class=""plainlabel14""><b>Measurements:</b></td><td class=""plainlabelblue14"">" & mout & "</td></tr>")
            imgmeas.Attributes.Add("src", "../images/appbuttons/minibuttons/measure.gif")
            imgmeas.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov343", "tpmtask.aspx.vb") & "')")
            imgmeas.Attributes.Add("onmouseout", "return nd()")
        Else
            imgmeas.Attributes.Add("src", "../images/appbuttons/minibuttons/measureg.gif")
            imgmeas.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov344", "tpmtask.aspx.vb") & "')")
            imgmeas.Attributes.Add("onmouseout", "return nd()")
        End If
        sb1.Append("</table>")

        tdtask2.InnerHtml = sb1.ToString

        If subcnt = "0" Then
            imgsub.Attributes.Add("src", "../images/appbuttons/minibuttons/sgrid1dis.gif")
            imgsub.Attributes.Add("onclick", "")
            imgsub.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov345", "tpmtask.aspx.vb") & "')")
            imgsub.Attributes.Add("onmouseout", "return nd()")
        Else
            imgsub.Attributes.Add("src", "../images/appbuttons/minibuttons/sgrid1.gif")
            imgsub.Attributes.Add("onclick", "getsubdiv();")
            imgsub.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov346", "tpmtask.aspx.vb") & "')")
            imgsub.Attributes.Add("onmouseout", "return nd()")
        End If

        sql = "select failid, failuremode from pmtaskfailmodestpm where taskid = '" & pmtskid & "' order by failuremode"
        Dim chkdp, chkdf As String

        If fm1 <> "" Then
            'trfaili.Attributes.Add("class", "view")
            iffail.Attributes.Add("src", "tpmfm.aspx?dir=o&pmtskid=" & pmtskid)
        Else
            trfail.Attributes.Add("class", "details")
            tdfail.InnerHtml = "No Failure Modes"
        End If



        tdtpm.InnerHtml = skill
        tdfunc.InnerHtml = func
        tdcomp.InnerHtml = comp
        If tools = "" Then
            tools = "None"
        End If
        If lubes = "" Then
            lubes = "None"
        End If
        If parts = "" Then
            parts = "None"
        End If
        'tdtools.InnerHtml = tools
        'tdlubes.InnerHtml = lubes
        'tdparts.InnerHtml = parts
        'txtdesc.Text = task
        lblpmtskid.Value = pmtskid

        If tools = "" Or tools = "None" Then
            tools = "No Tools"
            tdtools.InnerHtml = tools
            trtools.Attributes.Add("class", "details")
            trtoolsmsg.Attributes.Add("class", "details") 'view plainlabelblue
            trtoolsmsg.Attributes.Add("height", "20")
            itool.Attributes.Add("onclick", "")
        Else
            iftools.Attributes.Add("src", "tpmtools.aspx?ts=1&pmtskid=" & pmtskid)
            trtools.Attributes.Add("class", "details")
            trtoolsmsg.Attributes.Add("class", "details")
            trtoolsmsg.Attributes.Add("height", "0")
            itool.Attributes.Add("onclick", "showtool();")
        End If
        If lubes = "" Or lubes = "None" Then
            lubes = "No Lubricants"
            tdlubes.InnerHtml = lubes
            trlubes.Attributes.Add("class", "details")
            trlubesmsg.Attributes.Add("class", "details") 'view plainlabelblue
            trlubesmsg.Attributes.Add("height", "20")
            ilube.Attributes.Add("onclick", "")
        Else
            iflubes.Attributes.Add("src", "tpmlubes.aspx?ts=1&pmtskid=" & pmtskid)
            trlubes.Attributes.Add("class", "details")
            trlubesmsg.Attributes.Add("class", "details")
            trlubesmsg.Attributes.Add("height", "0")
            ilube.Attributes.Add("onclick", "showlube();")
        End If
        If parts = "" Or parts = "None" Then
            parts = "No Parts"
            tdparts.InnerHtml = parts
            trparts.Attributes.Add("class", "details")
            trpartsmsg.Attributes.Add("class", "details") 'view plainlabelblue
            trpartsmsg.Attributes.Add("height", "20")
            ipart.Attributes.Add("onclick", "")
        Else
            ifparts.Attributes.Add("src", "tpmparts.aspx?ts=1&pmtskid=" & pmtskid)
            trparts.Attributes.Add("class", "details")
            trpartsmsg.Attributes.Add("class", "details")
            trpartsmsg.Attributes.Add("height", "0")
            ipart.Attributes.Add("onclick", "showpart();")
        End If


        Dim sbm As New StringBuilder
        'sbm.Append("<table>")
        Dim i As Integer
        Dim mcnt As Integer = 0
        Dim mid As String
        If meas <> "none" Then
            ifmeas.Attributes.Add("src", "tpmmeas.aspx?ts=1&pmtskid=" & pmtskid)
            trmeas.Attributes.Add("class", "details")
            trmeasmsg.Attributes.Add("class", "details")
            trmeasmsg.Attributes.Add("height", "0")
            imeas.Attributes.Add("onclick", "showmeas();")
            'sql = "select m.* " _
            '+ "from pmTaskMeasDetailstpm m " _
            '+ "where m.pmtskid = '" & pmtskid & "'"
            'dr = news.GetRdrData(sql)
            'While dr.Read
            'mcnt += 1
            'Select Case mcnt
            'Case "1"
            'lblm1i.Value = dr.Item("tmdid").ToString
            'mid = "lblm1"
            '    Case "2"
            'lblm2i.Value = dr.Item("tmdid").ToString
            'mid = "lblm2"
            '    Case "3"
            'lblm3i.Value = dr.Item("tmdid").ToString
            'mid = "lblm3"
            '    Case "4"
            'lblm4i.Value = dr.Item("tmdid").ToString
            'mid = "lblm4"
            '    Case "5"
            'lblm5i.Value = dr.Item("tmdid").ToString
            'mid = "lblm5"
            'End Select
            'sbm.Append("<tr><td><input type=""text"" onkeyup=""upmeas('" & mid & "', this.value);""></td>")
            'sbm.Append("<td class=""plainlabelblue"">Hi:&nbsp;" & dr.Item("hi").ToString & "</td>")
            'sbm.Append("<td class=""plainlabelblue"">Lo:&nbsp;" & dr.Item("lo").ToString & "</td>")
            'sbm.Append("<td class=""plainlabelblue"">Spec:&nbsp;" & dr.Item("spec").ToString & "</td></tr>")
            'End While
            'dr.Close()
        Else
            tdmeas.InnerHtml = "None"
            trmeas.Attributes.Add("class", "details")
            trmeasmsg.Attributes.Add("class", "details") 'view plainlabelblue
            trmeasmsg.Attributes.Add("height", "20")
            imeas.Attributes.Add("onclick", "")
            'sbm.Append("<tr><td class=""plainlabelblue"">none</td></tr>")
        End If
        'sbm.Append("</table>")
        'tdmeas.InnerHtml = sbm.ToString
        If first <> "" Then
            days = first
        ElseIf second <> "" Then
            days = second
        ElseIf third <> "" Then
            days = third
        End If
        Try
            'Dim freq As Integer = lblfreq.Value
            If freq = "1" Or freq = "7" Then
                'activate days
                'GetODays(pmtskid)
                If shift1 = "1st shift" Then
                    trshift2.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                ElseIf shift2 = "2nd shift" Then
                    trshift1.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                ElseIf shift3 = "3rd shift" Then
                    trshift1.Attributes.Add("class", "details")
                    trshift2.Attributes.Add("class", "details")
                Else
                    trshiftdays.Attributes.Add("class", "details")
                    trshift1.Attributes.Add("class", "details")
                    trshift2.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                End If
                Select Case days
                    Case "M"
                        calday = 1
                    Case "Tu"
                        calday = 2
                    Case "T"
                        calday = 2
                    Case "W"
                        calday = 3
                    Case "Th"
                        calday = 4
                    Case "F"
                        calday = 5
                    Case "Sa"
                        calday = 6
                    Case "Su"
                        calday = 0
                    Case Else
                        calday = -1
                End Select
                lblcalday.Value = calday
            Else
                'disable days
                DisableDays()
            End If
        Catch ex As Exception

        End Try

        GetPics(funcid, itask)
    End Sub
    Private Sub GetODays(ByVal pmtskid As String)
        sql = "select top 1 * from pmtasksdays where pmtskid = '" & pmtskid & "'"
        dr = news.GetRdrData(sql)
        While dr.Read
            If dr.Item("cb1o").ToString = "1" Then
                cb1o.Checked = True
            End If
            If dr.Item("cb1mon").ToString = "1" Then
                cb1mon.Checked = True
            Else
                cb1mon.Checked = False
            End If
            If dr.Item("cb1tue").ToString = "1" Then
                cb1tue.Checked = True
            Else
                cb1tue.Checked = False
            End If
            If dr.Item("cb1wed").ToString = "1" Then
                cb1wed.Checked = True
            Else
                cb1wed.Checked = False
            End If
            If dr.Item("cb1thu").ToString = "1" Then
                cb1thu.Checked = True
            Else
                cb1thu.Checked = False
            End If
            If dr.Item("cb1fri").ToString = "1" Then
                cb1fri.Checked = True
            Else
                cb1fri.Checked = False
            End If
            If dr.Item("cb1sat").ToString = "1" Then
                cb1sat.Checked = True
            Else
                cb1sat.Checked = False
            End If
            If dr.Item("cb1sun").ToString = "1" Then
                cb1sun.Checked = True
            Else
                cb1sun.Checked = False
            End If

            If dr.Item("cb2o").ToString = "1" Then
                cb2o.Checked = True
            End If
            If dr.Item("cb2mon").ToString = "1" Then
                cb2mon.Checked = True
            Else
                cb2mon.Checked = False
            End If
            If dr.Item("cb2tue").ToString = "1" Then
                cb2tue.Checked = True
            Else
                cb2tue.Checked = False
            End If
            If dr.Item("cb2wed").ToString = "1" Then
                cb2wed.Checked = True
            Else
                cb2wed.Checked = False
            End If
            If dr.Item("cb2thu").ToString = "1" Then
                cb2thu.Checked = True
            Else
                cb2thu.Checked = False
            End If
            If dr.Item("cb2fri").ToString = "1" Then
                cb2fri.Checked = True
            Else
                cb2fri.Checked = False
            End If
            If dr.Item("cb2sat").ToString = "1" Then
                cb2sat.Checked = True
            Else
                cb2sat.Checked = False
            End If
            If dr.Item("cb2sun").ToString = "1" Then
                cb2sun.Checked = True
            Else
                cb2sun.Checked = False
            End If

            If dr.Item("cb3o").ToString = "1" Then
                cb3o.Checked = True
            End If
            If dr.Item("cb3mon").ToString = "1" Then
                cb3mon.Checked = True
            Else
                cb3mon.Checked = False
            End If
            If dr.Item("cb3tue").ToString = "1" Then
                cb3tue.Checked = True
            Else
                cb3tue.Checked = False
            End If
            If dr.Item("cb3wed").ToString = "1" Then
                cb3wed.Checked = True
            Else
                cb3wed.Checked = False
            End If
            If dr.Item("cb3thu").ToString = "1" Then
                cb3thu.Checked = True
            Else
                cb3thu.Checked = False
            End If
            If dr.Item("cb3fri").ToString = "1" Then
                cb3fri.Checked = True
            Else
                cb3fri.Checked = False
            End If
            If dr.Item("cb3sat").ToString = "1" Then
                cb3sat.Checked = True
            Else
                cb3sat.Checked = False
            End If
            If dr.Item("cb3sun").ToString = "1" Then
                cb3sun.Checked = True
            Else
                cb3sun.Checked = False
            End If
        End While
        dr.Close()
    End Sub
    Private Sub GetSTasks(ByVal pmid As String, ByVal tasknum As String)
        Dim mtask As Integer
        sql = "select count(*) from pmtracktpm where pmid = '" & pmid & "' and subtask = 0"
        mtask = news.Scalar(sql)
        lblmaxtask.Value = mtask

        'temp
        'tdtocomp.InnerHtml = mtask
        '
        'sql = "usp_getpmmantpmperf '" & pmid & "','" & tasknum & "','1'"
        sql = "usp_getpmmantpmperf2 '" & pmid & "','" & tasknum & "','1'"
        Dim skill, task, func, comp, lubes, parts, tools, meas, pmtskid, itask, shift, days, calday, subcnt, pmtid, taskcomp, comid As String
        Dim rdt, actt, rd, tt, hrs, mins, aps, hre, mine, ape, hrsdt, minsdt, apsdt, hredt, minedt, apedt, ttid, eqnum, tocomp As String
        Dim nextdate As String
        Dim fm1, fm2, fm3, fm4, fm5 As String
        Dim fm1i, fm2i, fm3i, fm4i, fm5i As String
        Dim fm1o, fm2o, fm3o, fm4o, fm5o As String
        Dim m1, m2, m3, m4, m5 As String
        Dim ms1, ms2, ms3, ms4, ms5 As String
        lblbpage.Text = "Task " & tasknum & " of " & mtask
        dr = news.GetRdrData(sql)
        While dr.Read
            tocomp = dr.Item("tocomp").ToString
            tdtocomp.InnerHtml = tocomp
            taskcomp = dr.Item("taskcomp").ToString
            fcust = dr.Item("fcust").ToString
            lblfcust.Value = fcust
            nextdate = dr.Item("nextdate").ToString
            lblnextdate.Value = nextdate
            lblcomid.Value = dr.Item("comid").ToString
            rd = dr.Item("rd").ToString
            rdt = dr.Item("actrd").ToString
            tt = dr.Item("ttime").ToString
            actt = dr.Item("acttime").ToString

            subcnt = dr.Item("subcnt").ToString


            hrs = dr.Item("hrs").ToString
            mins = dr.Item("mins").ToString
            aps = dr.Item("aps").ToString
            hre = dr.Item("hre").ToString
            mine = dr.Item("mine").ToString
            ape = dr.Item("ape").ToString

            hrsdt = dr.Item("hrsdt").ToString
            minsdt = dr.Item("minsdt").ToString
            apsdt = dr.Item("apsdt").ToString
            hredt = dr.Item("hredt").ToString
            minedt = dr.Item("minedt").ToString
            apedt = dr.Item("apedt").ToString

            ttid = dr.Item("ttid").ToString
            eqnum = dr.Item("eqnum").ToString
            pmtskid = dr.Item("pmtskid").ToString
            pmtid = dr.Item("pmtid").ToString
            eqid = dr.Item("eqid").ToString
            lbleqid.Value = eqid
            funcid = dr.Item("funcid").ToString
            lblfuncid.Value = funcid
            skill = dr.Item("pm").ToString
            task = dr.Item("task").ToString
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            lubes = dr.Item("lubes").ToString
            meas = dr.Item("meas").ToString
            itask = dr.Item("tasknum").ToString
            lbltasknum1.Value = itask
            freq = dr.Item("freq").ToString
            lblfreq.Value = freq
            days = dr.Item("days").ToString
            If days = "" Then
                days = "no"
            End If
            lbldays.Value = days

            shift = dr.Item("shift").ToString

            If shift = "" Then
                shift = "No Shift"
            End If
            lblshift.Value = shift

            fm1 = dr.Item("fm1s").ToString
            fm2 = dr.Item("fm2s").ToString
            fm3 = dr.Item("fm3s").ToString
            fm4 = dr.Item("fm4s").ToString
            fm5 = dr.Item("fm5s").ToString

            fm1i = dr.Item("fm1id").ToString
            fm2i = dr.Item("fm2id").ToString
            fm3i = dr.Item("fm3id").ToString
            fm4i = dr.Item("fm4id").ToString
            fm5i = dr.Item("fm5id").ToString

            fm1o = dr.Item("fm1").ToString
            fm2o = dr.Item("fm2").ToString
            fm3o = dr.Item("fm3").ToString
            fm4o = dr.Item("fm4").ToString
            fm5o = dr.Item("fm5").ToString

            m1 = dr.Item("tmdid1").ToString
            m2 = dr.Item("tmdid2").ToString
            m3 = dr.Item("tmdid3").ToString
            m4 = dr.Item("tmdid4").ToString
            m5 = dr.Item("tmdid5").ToString

            ms1 = dr.Item("mstr1").ToString
            ms2 = dr.Item("mstr2").ToString
            ms3 = dr.Item("mstr3").ToString
            ms4 = dr.Item("mstr4").ToString
            ms5 = dr.Item("mstr5").ToString



        End While
        dr.Close()

        'need flag stuff here
        If taskcomp = "0" Then
            tdstat.InnerHtml = "Incomplete"
            imgcomptask.Attributes.Add("src", "../images/appbuttons/minibuttons/compgrn.gif")
            imgcomptask.Attributes.Add("onclick", "comptask();")
        Else
            tdstat.InnerHtml = "Complete"
            imgcomptask.Attributes.Add("src", "../images/appbuttons/minibuttons/compred.gif")
            imgcomptask.Attributes.Add("onclick", "")
        End If
        If tocomp = "0" Then
            imgcomptpm.Attributes.Add("src", "../images/appbuttons/minibuttons/comp.gif")
            imgcomptpm.Attributes.Add("onclick", "comptpm();")
        Else
            imgcomptpm.Attributes.Add("src", "../images/appbuttons/minibuttons/compdis.gif")
            imgcomptpm.Attributes.Add("onclick", "")
        End If

        tdeq.InnerHtml = eqnum

        Dim sbimg As String
        If ttid = "2" Then
            sbimg = "../images/appbuttons/minibuttons/tpmclean.gif"
        ElseIf ttid = "3" Then
            sbimg = "../images/appbuttons/minibuttons/tpmlube.gif"
        ElseIf ttid = "5" Then
            sbimg = "../images/appbuttons/minibuttons/tpminsp.gif"
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""350"">")
        sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""310""></td></tr>")

        If Len(task) > 0 Then
            If sbimg <> "" Then
                sb.Append("<tr><td colspan=""3"" class=""plainlabelblue14""><img src=""" & sbimg & """ align=""left"" hspace=""5"" vspace=""4""><font class=""plainlabel14""><b>Task</b></font> :" & task & "</td></tr>")
            Else
                sb.Append("<tr><td colspan=""3"" class=""plainlabelblue14""><font class=""plainlabel14""><b>Task</b></font> :" & task & "</td></tr>")
            End If

        Else
            sb.Append("<tr><td colspan=""3"" class=""plainlabelblue14""><b>Task:</b> No Task Description Provided</td></tr>")
        End If

        sb.Append("</table>")

        Dim fmout As String
        If Len(fm1) <> 0 Then
            fmout = "<a href=""#"" class=""A1"" onclick=""openfm('" & fm1i & "','" & pmtid & "','" & pmid & "','" & itask & "');"">" & fm1 & "</a>"
        End If
        If Len(fm2) <> 0 Then
            fmout += "; " & "<a href=""#"" class=""A1"" onclick=""openfm('" & fm2i & "','" & pmtid & "','" & pmid & "','" & itask & "');"">" & fm2 & "</a>"
        End If
        If Len(fm3) <> 0 Then
            fmout += "; " & "<a href=""#"" class=""A1"" onclick=""openfm('" & fm3i & "','" & pmtid & "','" & pmid & "','" & itask & "');"">" & fm3 & "</a>"
        End If
        If Len(fm4) <> 0 Then
            fmout += "; " & "<a href=""#"" class=""A1"" onclick=""openfm('" & fm4i & "','" & pmtid & "','" & pmid & "','" & itask & "');"">" & fm4 & "</a>"
        End If
        If Len(fm5) <> 0 Then
            fmout += "; " & "<a href=""#"" class=""A1"" onclick=""openfm('" & fm5i & "','" & pmtid & "','" & pmid & "','" & itask & "');"">" & fm5 & "</a>"
        End If

        Dim mout As String
        If Len(m1) <> 0 Then
            mout = "<a href=""#"" class=""A1"" onclick=""openm('" & m1 & "','" & pmtskid & "','" & pmid & "','" & shift & "');"">" & ms1 & "</a>"
        End If
        If Len(m2) <> 0 Then
            mout += "; " & "<a href=""#"" class=""A1"" onclick=""openm('" & m2 & "','" & pmtskid & "','" & pmid & "','" & shift & "');"">" & ms2 & "</a>"
        End If
        If Len(m3) <> 0 Then
            mout += "; " & "<a href=""#"" class=""A1"" onclick=""openm('" & m3 & "','" & pmtskid & "','" & pmid & "','" & shift & "');"">" & ms3 & "</a>"
        End If
        If Len(m4) <> 0 Then
            mout += "; " & "<a href=""#"" class=""A1"" onclick=""openm('" & m4 & "','" & pmtskid & "','" & pmid & "','" & shift & "');"">" & ms4 & "</a>"
        End If
        If Len(m5) <> 0 Then
            mout += "; " & "<a href=""#"" class=""A1"" onclick=""openm('" & m5 & "','" & pmtskid & "','" & pmid & "','" & shift & "');"">" & ms5 & "</a>"
        End If


        divtask.InnerHtml = sb.ToString

        Dim sb1 As New System.Text.StringBuilder
        sb1.Append("<table>")
        If Len(fmout) <> 0 Then
            sb1.Append("<tr><td colspan=""1"" class=""plainlabel14""><b>Failure Modes:</b></td><td class=""plainlabelblue14"">" & fmout & "</td></tr>")
            imgfail.Attributes.Add("src", "../images/appbuttons/minibuttons/tpmfail.gif")
            imgfail.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov347", "tpmtask.aspx.vb") & "')")
            imgfail.Attributes.Add("onmouseout", "return nd()")
        Else
            imgfail.Attributes.Add("src", "../images/appbuttons/minibuttons/tpmfaildis.gif")
            imgfail.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov348", "tpmtask.aspx.vb") & "')")
            imgfail.Attributes.Add("onmouseout", "return nd()")
        End If
        If lubes <> "none" Then
            sb1.Append("<tr>")
            sb1.Append("<td colspan=""1"" class=""plainlabel14""><b>Lubes:</b></td><td class=""plainlabelblue14"">" & lubes & "</td></tr>")
            imglube.Attributes.Add("src", "../images/appbuttons/minibuttons/lubetrans.gif")
            imglube.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov349", "tpmtask.aspx.vb") & "')")
            imglube.Attributes.Add("onmouseout", "return nd()")
        Else
            imglube.Attributes.Add("src", "../images/appbuttons/minibuttons/lubetransdis.gif")
            imglube.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov350", "tpmtask.aspx.vb") & "')")
            imglube.Attributes.Add("onmouseout", "return nd()")
        End If
        If tools <> "none" Then
            sb1.Append("<tr>")
            sb1.Append("<td colspan=""1"" class=""plainlabel14""><b>Tools:</b></td><td class=""plainlabelblue14"">" & tools & "</td></tr>")
            imgtool.Attributes.Add("src", "../images/appbuttons/minibuttons/tooltrans.gif")
            imgtool.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov351", "tpmtask.aspx.vb") & "')")
            imgtool.Attributes.Add("onmouseout", "return nd()")
        Else
            imgtool.Attributes.Add("src", "../images/appbuttons/minibuttons/tooltransdis.gif")
            imgtool.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov352", "tpmtask.aspx.vb") & "')")
            imgtool.Attributes.Add("onmouseout", "return nd()")
        End If
        If parts <> "none" Then
            sb1.Append("<tr>")
            sb1.Append("<td colspan=""1"" class=""plainlabel14""><b>Parts:</b></td><td class=""plainlabelblue14"">" & parts & "</td></tr>")
            imgpart.Attributes.Add("src", "../images/appbuttons/minibuttons/parttrans.gif")
            imgpart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov353", "tpmtask.aspx.vb") & "')")
            imgpart.Attributes.Add("onmouseout", "return nd()")
        Else
            imgpart.Attributes.Add("src", "../images/appbuttons/minibuttons/parttransdis.gif")
            imgpart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov354", "tpmtask.aspx.vb") & "')")
            imgpart.Attributes.Add("onmouseout", "return nd()")
        End If
        If meas <> "none" Then
            sb1.Append("<tr>")
            sb1.Append("<td colspan=""1"" class=""plainlabel14""><b>Measurements:</b></td><td class=""plainlabelblue14"">" & mout & "</td></tr>")
            imgmeas.Attributes.Add("src", "../images/appbuttons/minibuttons/measure.gif")
            imgmeas.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov355", "tpmtask.aspx.vb") & "')")
            imgmeas.Attributes.Add("onmouseout", "return nd()")
        Else
            imgmeas.Attributes.Add("src", "../images/appbuttons/minibuttons/measureg.gif")
            imgmeas.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov356", "tpmtask.aspx.vb") & "')")
            imgmeas.Attributes.Add("onmouseout", "return nd()")
        End If
        sb1.Append("</table>")

        tdtask2.InnerHtml = sb1.ToString

        If subcnt = "0" Then
            imgsub.Attributes.Add("src", "../images/appbuttons/minibuttons/sgrid1dis.gif")
            imgsub.Attributes.Add("onclick", "")
            imgsub.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov357", "tpmtask.aspx.vb") & "')")
            imgsub.Attributes.Add("onmouseout", "return nd()")
        Else
            imgsub.Attributes.Add("src", "../images/appbuttons/minibuttons/sgrid1.gif")
            imgsub.Attributes.Add("onclick", "getsubdiv();")
            imgsub.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov358", "tpmtask.aspx.vb") & "')")
            imgsub.Attributes.Add("onmouseout", "return nd()")
        End If

        Dim chkdp, chkdf As String
        If fm1i <> 0 Then
            'trfaili.Attributes.Add("class", "view")
            iffail.Attributes.Add("src", "tpmfm.aspx?dir=s&pmtid=" & pmtid & "&pmid=" & pmid & "&tasknum=" & itask)
        Else
            trfail.Attributes.Add("class", "details")
            tdfail.InnerHtml = "No Failure Modes"
        End If

        tdtpm.InnerHtml = skill
        tdfunc.InnerHtml = func
        tdcomp.InnerHtml = comp

        iftime.Attributes.Add("src", "tpmtime.aspx?ts=0&pmid=" & pmid & "&tasknum=" & pmtskid)
        'trtime.Attributes.Add("class", "view")


        If tools = "" Or tools = "none" Then
            tools = "No Tools"
            lbltdtool.Value = "yes"
            tdtools.InnerHtml = tools
            trtools.Attributes.Add("class", "details")
            trtoolsmsg.Attributes.Add("class", "details") 'view plainlabelblue
            trtoolsmsg.Attributes.Add("height", "20")
            itool.Attributes.Add("onclick", "")
        Else
            lbltdtool.Value = "no"
            wonum = lblwo.Value
            iftools.Attributes.Add("src", "tpmtools.aspx?ts=0&pmid=" & pmid & "&wonum=" & wonum)
            tdtools.InnerHtml = Nothing
            trtools.Attributes.Add("class", "details")
            trtoolsmsg.Attributes.Add("class", "details")
            trtoolsmsg.Attributes.Add("height", "0")
            itool.Attributes.Add("onclick", "showtool();")
        End If
        If lubes = "" Or lubes = "none" Then
            lubes = "No Lubricants"
            lbltdlube.Value = "yes"
            tdlubes.InnerHtml = lubes
            trlubes.Attributes.Add("class", "details")
            trlubesmsg.Attributes.Add("class", "details") 'view plainlabelblue
            trlubesmsg.Attributes.Add("height", "20")
            ilube.Attributes.Add("onclick", "")
        Else
            lbltdlube.Value = "no"
            wonum = lblwo.Value
            iflubes.Attributes.Add("src", "tpmlubes.aspx?ts=0&pmid=" & pmid & "&wonum=" & wonum)
            trlubes.Attributes.Add("class", "details")
            trlubesmsg.Attributes.Add("class", "details")
            trlubesmsg.Attributes.Add("height", "0")
            ilube.Attributes.Add("onclick", "showlube();")
        End If
        If parts = "" Or parts = "none" Then
            parts = "No Parts"
            lbltdpart.Value = "yes"
            tdparts.InnerHtml = parts
            trparts.Attributes.Add("class", "details")
            trpartsmsg.Attributes.Add("class", "details") 'view plainlabelblue
            trpartsmsg.Attributes.Add("height", "20")
            ipart.Attributes.Add("onclick", "")
        Else
            lbltdpart.Value = "no"
            wonum = lblwo.Value
            ifparts.Attributes.Add("src", "tpmparts.aspx?ts=0&pmid=" & pmid & "&wonum=" & wonum)
            trparts.Attributes.Add("class", "details")
            trpartsmsg.Attributes.Add("class", "details")
            trpartsmsg.Attributes.Add("height", "0")
            ipart.Attributes.Add("onclick", "showpart();")
        End If



        'txtdesc.Text = task
        lblpmtskid.Value = pmtskid
        lblpmtid.Value = pmtid

        Dim sbm As New StringBuilder
        'sbm.Append("<table>")
        Dim i As Integer
        Dim mcnt As Integer = 0
        Dim mid As String
        If meas <> "none" Then
            lbltdmeas.Value = "no"
            ifmeas.Attributes.Add("src", "tpmmeas.aspx?ts=0&pmid=" & pmid & "&pmtskid=" & pmtskid & "&shift=" & shift)
            trmeas.Attributes.Add("class", "details")
            trmeasmsg.Attributes.Add("class", "details")
            trmeasmsg.Attributes.Add("height", "0")
            imeas.Attributes.Add("onclick", "showmeas();")
            'sql = "select m.* " _
            '+ "from pmTaskMeasDetMantpm m " _
            '+ "where m.pmid = '" & pmid & "' and m.pmtskid = '" & pmtskid & "'"
            'dr = news.GetRdrData(sql)
            'While dr.Read
            'mcnt += 1
            'Select Case mcnt
            'Case "1"
            'lblm1i.Value = dr.Item("tmdid").ToString
            'mid = "lblm1"
            'Case "2"
            'lblm2i.Value = dr.Item("tmdid").ToString
            'mid = "lblm2"
            'Case "3"
            'lblm3i.Value = dr.Item("tmdid").ToString
            'mid = "lblm3"
            'Case "4"
            'lblm4i.Value = dr.Item("tmdid").ToString
            'mid = "lblm4"
            'Case "5"
            'lblm5i.Value = dr.Item("tmdid").ToString
            'mid = "lblm5"
            'End Select
            'sbm.Append("<tr><td><input type=""text"" onkeyup=""upmeas('" & mid & "', this.value);"" value=""" & dr.Item("measurement").ToString & """></td>")
            'sbm.Append("<td class=""plainlabelblue"">Hi:&nbsp;" & dr.Item("hi").ToString & "</td>")
            'sbm.Append("<td class=""plainlabelblue"">Lo:&nbsp;" & dr.Item("lo").ToString & "</td>")
            'sbm.Append("<td class=""plainlabelblue"">Spec:&nbsp;" & dr.Item("spec").ToString & "</td></tr>")
            'End While
            'dr.Close()
        Else
            lbltdmeas.Value = "yes"
            'sbm.Append("<tr><td class=""plainlabelblue"">none</td></tr>")
            tdmeas.InnerHtml = "No Measurements"
            trmeas.Attributes.Add("class", "details")
            trmeasmsg.Attributes.Add("class", "details") 'view plainlabelblue
            trmeasmsg.Attributes.Add("height", "20")
            imeas.Attributes.Add("onclick", "")
        End If

        Try
            Dim freq As Integer = lblfreq.Value
            If freq = 1 Or freq = 7 Then
                'activate days
                'GetDays(pmid)
                If shift = "1st shift" Then
                    trshift2.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                ElseIf shift = "2nd shift" Then
                    trshift1.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                ElseIf shift = "3rd shift" Then
                    trshift1.Attributes.Add("class", "details")
                    trshift2.Attributes.Add("class", "details")
                Else
                    trshiftdays.Attributes.Add("class", "details")
                    trshift1.Attributes.Add("class", "details")
                    trshift2.Attributes.Add("class", "details")
                    trshift3.Attributes.Add("class", "details")
                End If
                Select Case days
                    Case "M"
                        calday = 1
                    Case "Tu"
                        calday = 2
                    Case "T"
                        calday = 2
                    Case "W"
                        calday = 3
                    Case "Th"
                        calday = 4
                    Case "F"
                        calday = 5
                    Case "Sa"
                        calday = 6
                    Case "Su"
                        calday = 0
                    Case Else
                        calday = -1
                End Select
                lblcalday.Value = calday
            Else
                'disable days
                DisableDays()
            End If
        Catch ex As Exception

        End Try
        'sbm.Append("</table>")
        'tdmeas.InnerHtml = sbm.ToString
        GetSPics(funcid, itask)
    End Sub
    Private Sub GetDays(ByVal pmid As String)
        sql = "select top 1 * from pmtrackdaystpm where pmid = '" & pmid & "'"
        dr = news.GetRdrData(sql)
        While dr.Read
            If dr.Item("cb1o").ToString = "1" Then
                cb1o.Checked = True
            End If
            If dr.Item("cb1mon").ToString = "1" Then
                cb1mon.Checked = True
            Else
                cb1mon.Checked = False
            End If
            If dr.Item("cb1tue").ToString = "1" Then
                cb1tue.Checked = True
            Else
                cb1tue.Checked = False
            End If
            If dr.Item("cb1wed").ToString = "1" Then
                cb1wed.Checked = True
            Else
                cb1wed.Checked = False
            End If
            If dr.Item("cb1thu").ToString = "1" Then
                cb1thu.Checked = True
            Else
                cb1thu.Checked = False
            End If
            If dr.Item("cb1fri").ToString = "1" Then
                cb1fri.Checked = True
            Else
                cb1fri.Checked = False
            End If
            If dr.Item("cb1sat").ToString = "1" Then
                cb1sat.Checked = True
            Else
                cb1sat.Checked = False
            End If
            If dr.Item("cb1sun").ToString = "1" Then
                cb1sun.Checked = True
            Else
                cb1sun.Checked = False
            End If

            If dr.Item("cb2o").ToString = "1" Then
                cb2o.Checked = True
            End If
            If dr.Item("cb2mon").ToString = "1" Then
                cb2mon.Checked = True
            Else
                cb2mon.Checked = False
            End If
            If dr.Item("cb2tue").ToString = "1" Then
                cb2tue.Checked = True
            Else
                cb2tue.Checked = False
            End If
            If dr.Item("cb2wed").ToString = "1" Then
                cb2wed.Checked = True
            Else
                cb2wed.Checked = False
            End If
            If dr.Item("cb2thu").ToString = "1" Then
                cb2thu.Checked = True
            Else
                cb2thu.Checked = False
            End If
            If dr.Item("cb2fri").ToString = "1" Then
                cb2fri.Checked = True
            Else
                cb2fri.Checked = False
            End If
            If dr.Item("cb2sat").ToString = "1" Then
                cb2sat.Checked = True
            Else
                cb2sat.Checked = False
            End If
            If dr.Item("cb2sun").ToString = "1" Then
                cb2sun.Checked = True
            Else
                cb2sun.Checked = False
            End If

            If dr.Item("cb3o").ToString = "1" Then
                cb3o.Checked = True
            End If
            If dr.Item("cb3mon").ToString = "1" Then
                cb3mon.Checked = True
            Else
                cb3mon.Checked = False
            End If
            If dr.Item("cb3tue").ToString = "1" Then
                cb3tue.Checked = True
            Else
                cb3tue.Checked = False
            End If
            If dr.Item("cb3wed").ToString = "1" Then
                cb3wed.Checked = True
            Else
                cb3wed.Checked = False
            End If
            If dr.Item("cb3thu").ToString = "1" Then
                cb3thu.Checked = True
            Else
                cb3thu.Checked = False
            End If
            If dr.Item("cb3fri").ToString = "1" Then
                cb3fri.Checked = True
            Else
                cb3fri.Checked = False
            End If
            If dr.Item("cb3sat").ToString = "1" Then
                cb3sat.Checked = True
            Else
                cb3sat.Checked = False
            End If
            If dr.Item("cb3sun").ToString = "1" Then
                cb3sun.Checked = True
            Else
                cb3sun.Checked = False
            End If
        End While
        dr.Close()
    End Sub
    Private Sub DisableDays()
        cb1o.Disabled = True
        'cb1.Disabled = True
        cb1mon.Disabled = True
        cb1tue.Disabled = True
        cb1wed.Disabled = True
        cb1thu.Disabled = True
        cb1fri.Disabled = True
        cb1sat.Disabled = True
        cb1sun.Disabled = True

        cb2o.Disabled = True
        'cb2.Disabled = True
        cb2mon.Disabled = True
        cb2tue.Disabled = True
        cb2wed.Disabled = True
        cb2thu.Disabled = True
        cb2fri.Disabled = True
        cb2sat.Disabled = True
        cb2sun.Disabled = True

        cb3o.Disabled = True
        'cb3.Disabled = True
        cb3mon.Disabled = True
        cb3tue.Disabled = True
        cb3wed.Disabled = True
        cb3thu.Disabled = True
        cb3fri.Disabled = True
        cb3sat.Disabled = True
        cb3sun.Disabled = True
    End Sub
    Private Sub GetPics(ByVal funcid As String, ByVal tasknum As String)
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ns1i As Integer = nsimage.IndexOf("//")
        Dim nsstr As String = Mid(nsimage, ns1i + 3)
        Dim ns2i As Integer = nsstr.IndexOf("/")
        nsstr = Mid(nsstr, 1, ns2i)
        Dim hostflag As Integer = 0
        If nsstr <> ThisPage1 Then
            nsimage = nsimage.Replace(nsstr, ThisPage1)
            hostflag = 1
            'lblhostflag.Value = "1"
            'lblhost.Value = nsstr

        Else
            'lblhostflag.Value = "0"
        End If
        imgeq.Attributes.Add("src", "../images/appimages/tpmimg1.gif")
        Dim pcnt As Integer
        sql = "select count(*) from tpmimages where funcid = '" & funcid & "' and tasknum = '" & tasknum & "'"
        pcnt = news.Scalar(sql)
        lblpcnt.Value = pcnt
        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.tpm_image, p.tpm_image_thumb, p.tpm_image_med, p.tpm_image_order, " _
            + "p.tpm_image_title, f.func " _
            + "from tpmimages p " _
            + "left join functions f on f.func_id = p.funcid where p.funcid = '" & funcid & "' and p.tasknum = '" & tasknum & "' order by p.tpm_image_order"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, img, picid, imgs As String
            dr = news.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("tpm_image").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("tpm_image").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)
                'check host here
                If hostflag = 1 Then
                    img = img.Replace(nsstr, ThisPage1)
                    bimg = bimg.Replace(nsstr, ThisPage1)
                    'timg = timg.Replace(nsstr, ThisPage1)
                End If
                picid = dr.Item("pic_id").ToString
                order = dr.Item("tpm_image_order").ToString
                ititle = dr.Item("tpm_image_title").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If
                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If

                If ititles = "" Then
                    ititles = ititle
                Else
                    ititles += "," & ititle
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    imgeq.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid
                    tdpline1.InnerHtml = ititle
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblititles.Value = ititles
            lbliorders.Value = iorders
        End If
    End Sub
    Private Sub GetSPics(ByVal funcid As String, ByVal tasknum As String)
        'imgeq.Attributes.Add("src", "../images/appimages/pmimg1.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/tpmimg1.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/tpmimg1..gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/tpmimg1..gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/tpmimg1..gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/tpmimg1..gif")
        End If
        Dim pcnt As Integer
        sql = "select count(*) from pmtracktpmimg where funcid = '" & funcid & "' and tasknum = '" & tasknum & "'"
        pcnt = news.Scalar(sql)
        lblpcnt.Value = pcnt
        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.tpm_image, p.tpm_image_thumb, p.tpm_image_med, p.tpm_image_order, " _
            + "p.tpm_image_title, f.func " _
            + "from pmtracktpmimg p " _
            + "left join functions f on f.func_id = p.funcid where p.funcid = '" & funcid & "' and p.tasknum = '" & tasknum & "' order by p.tpm_image_order"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, img, picid, imgs As String
            dr = news.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("tpm_image").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("tpm_image").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)
                picid = dr.Item("pic_id").ToString
                order = dr.Item("tpm_image_order").ToString
                ititle = dr.Item("tpm_image_title").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If
                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If

                If ititles = "" Then
                    ititles = ititle
                Else
                    ititles += "," & ititle
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    imgeq.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid
                    tdpline1.InnerHtml = ititle
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblititles.Value = ititles
            lbliorders.Value = iorders
        End If
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3611.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3611")
        Catch ex As Exception
        End Try
        Try
            lang3612.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3612")
        Catch ex As Exception
        End Try
        Try
            lang3613.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3613")
        Catch ex As Exception
        End Try
        Try
            lang3614.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3614")
        Catch ex As Exception
        End Try
        Try
            lang3615.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3615")
        Catch ex As Exception
        End Try
        Try
            lang3616.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3616")
        Catch ex As Exception
        End Try
        Try
            lang3617.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3617")
        Catch ex As Exception
        End Try
        Try
            lang3618.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3618")
        Catch ex As Exception
        End Try
        Try
            lang3619.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3619")
        Catch ex As Exception
        End Try
        Try
            lang3620.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3620")
        Catch ex As Exception
        End Try
        Try
            lang3621.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3621")
        Catch ex As Exception
        End Try
        Try
            lang3622.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3622")
        Catch ex As Exception
        End Try
        Try
            lang3623.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3623")
        Catch ex As Exception
        End Try
        Try
            lang3624.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3624")
        Catch ex As Exception
        End Try
        Try
            lang3625.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3625")
        Catch ex As Exception
        End Try
        Try
            lang3626.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3626")
        Catch ex As Exception
        End Try
        Try
            lang3627.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3627")
        Catch ex As Exception
        End Try
        Try
            lang3628.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3628")
        Catch ex As Exception
        End Try
        Try
            lang3629.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3629")
        Catch ex As Exception
        End Try
        Try
            lang3630.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3630")
        Catch ex As Exception
        End Try
        Try
            lang3631.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3631")
        Catch ex As Exception
        End Try
        Try
            lang3632.Text = axlabs.GetASPXPage("tpmtask.aspx", "lang3632")
        Catch ex As Exception
        End Try
        Try
            lblbpage.Text = axlabs.GetASPXPage("tpmtask.aspx", "lblbpage")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("tpmtask.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                Img1.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                Img1.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                Img1.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                Img1.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                Img1.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgcomptask.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imgcomptask") & "')")
            imgcomptask.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgcomptpm.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imgcomptpm") & "')")
            imgcomptpm.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgfail.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imgfail") & "')")
            imgfail.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imglube.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imglube") & "')")
            imglube.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgmag.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imgmag") & "')")
            imgmag.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgmeas.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imgmeas") & "')")
            imgmeas.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgpart.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imgpart") & "')")
            imgpart.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsub.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imgsub") & "')")
            imgsub.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgtool.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "imgtool") & "')")
            imgtool.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid297.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmtask.aspx", "ovid297") & "')")
            ovid297.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
