<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmwr.aspx.vb" Inherits="lucy_r12.tpmwr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmwr</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/dragitem.js"></script>
		<script language="JavaScript" src="../scripts1/tpmwraspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function getsuper(typ) {
             var skill //= document.getElementById("lblskillid").value;
             var ro = document.getElementById("lblro").value;
             var sid = document.getElementById("lblsid").value;
             var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
             if (eReturn) {
                 if (eReturn != "") {
                     var retarr = eReturn.split(",")
                     if (typ == "sup") {
                         document.getElementById("lblsupid").value = retarr[0];
                         document.getElementById("txtsup").value = retarr[1];
                         document.getElementById("lblsup").value = retarr[1];
                     }
                     else {
                         document.getElementById("lblleadid").value = retarr[0];
                         document.getElementById("txtlead").value = retarr[1];
                         document.getElementById("lblsupid").value = retarr[2];
                         document.getElementById("txtsup").value = retarr[3];
                         document.getElementById("lbllead").value = retarr[1];
                         document.getElementById("lblsup").value = retarr[3];
                     }
                 }
             }
         }
         function getcal(fld) {
             var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
             if (eReturn) {
                 var fldret = fld; //"txt" +
                 document.getElementById(fldret).value = eReturn;
                 document.getElementById("lblstart").value = eReturn;
             }
         }
     </script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td width="100"></td>
					<td width="20"></td>
					<td width="80"></td>
					<td width="20"></td>
					<td width="130"></td>
					<td width="5"></td>
					<td width="50"></td>
					<td width="50"></td>
					<td width="250"></td>
				</tr>
				<tr>
					<td colspan="9" align="center" class="plainlabelred" id="tdmsg" runat="server"></td>
				</tr>
				<tr>
					<td><asp:label id="Label1" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Task#</asp:label></td>
					<td></td>
					<td><asp:label id="lbltsk" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
					<td></td>
					<td align="right" colSpan="5"><asp:imagebutton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
				</tr>
				<tr class="details">
					<td><asp:label id="Label2" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Failure Mode:</asp:label></td>
					<td><IMG id="Img2" onmouseover="return overlib('View Failure Mode History', ABOVE, LEFT)"
							onclick="getfail();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/fmhist.gif"
							border="0" runat="server"></td>
					<td colSpan="6"><asp:label id="lblfail" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="Label3" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True">Work Order:</asp:label></td>
					<td><IMG id="iwoadd" onmouseover="return overlib('Create a Corrective or Emergency Maintenance Work Order')"
							onclick="getrt();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addwhite.gif"
							runat="server"></td>
					<td><asp:label id="lblwonum" runat="server" Font-Names="Arial" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
					<td><IMG id="imgi2" onmouseover="return overlib('Print This Work Order', ABOVE, LEFT)" onclick="printwo();"
							onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif" runat="server"></td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel" colSpan="5"><asp:Label id="lang3633" runat="server">Problem Encountered</asp:Label></td>
					<td>&nbsp;</td>
					<td class="btmmenu plainlabel" colSpan="3"><asp:Label id="lang3634" runat="server">Corrective Action</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="5"><asp:textbox id="txtprob" runat="server" CssClass="plainlabel" TextMode="MultiLine" Height="120px"
							Width="360px"></asp:textbox></td>
					<td></td>
					<td colSpan="3"><asp:textbox id="txtcorr" runat="server" CssClass="plainlabel" TextMode="MultiLine" Height="120px"
							Width="352px"></asp:textbox></td>
				</tr>
				<tr class="details">
					<td colSpan="7">
						<table>
							<tr>
								<td class="bluelabel" colSpan="2"><asp:Label id="lang3635" runat="server">Generate Corrective Action Work Order?</asp:Label></td>
								<td><INPUT id="cbcwo" onclick="getrt();" type="checkbox" runat="server" NAME="cbcwo"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<div class="details" id="rtdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 370px; BORDER-BOTTOM: black 1px solid; HEIGHT: 180px">
				<table cellSpacing="0" cellPadding="0" width="370" bgColor="white">
					<tr bgColor="blue" height="20">
						<td class="whitelabel"><asp:Label id="lang3636" runat="server">Work Order Details</asp:Label></td>
						<td align="right"><IMG onclick="closert();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr class="details" id="trrt" height="180">
						<td class="bluelabelb" vAlign="middle" align="center" colSpan="2"><asp:Label id="lang3637" runat="server">Moving Window...</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="2">
							<table>
								<tr height="20">
									<td></td>
									<td></td>
									<td class="bluelabel"><asp:Label id="lang3638" runat="server">Skill Required</asp:Label></td>
									<td colSpan="2"><asp:dropdownlist id="ddskill" runat="server" Width="160px" cssclass="plainlabel"></asp:dropdownlist></td>
								</tr>
								<tr height="20">
									<td><INPUT id="cbsupe" type="checkbox" name="cbsupe" runat="server"></td>
									<td><IMG onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
											src="../images/appbuttons/minibuttons/emailcb.gif"></td>
									<td class="bluelabel"><asp:Label id="lang3639" runat="server">Supervisor</asp:Label></td>
									<td><asp:textbox id="txtsup" runat="server" CssClass="plainlabel" Width="130px" ReadOnly="True"></asp:textbox></td>
									<td><IMG onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
											border="0"></td>
								</tr>
								<tr height="20">
									<td><INPUT id="cbleade" type="checkbox" name="cbleade" runat="server"></td>
									<td><IMG onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
											src="../images/appbuttons/minibuttons/emailcb.gif"></td>
									<td class="bluelabel"><asp:Label id="lang3640" runat="server">Lead Craft</asp:Label></td>
									<td><asp:textbox id="txtlead" runat="server" CssClass="plainlabel" Width="130px" ReadOnly="True"></asp:textbox></td>
									<td><IMG onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
											border="0"></td>
								</tr>
								<tr height="20">
									<td></td>
									<td></td>
									<td class="bluelabel"><asp:Label id="lang3641" runat="server">Work Type</asp:Label></td>
									<td colSpan="2"><asp:dropdownlist id="ddwt" runat="server" CssClass="plainlabel">
											<asp:ListItem Value="CM" Selected="True">Corrective Maintenance</asp:ListItem>
											<asp:ListItem Value="EM">Emergency Maintenance</asp:ListItem>
										</asp:dropdownlist></td>
								</tr>
								<tr height="20">
									<td></td>
									<td></td>
									<td class="bluelabel"><asp:Label id="lang3642" runat="server">Target Start</asp:Label></td>
									<td><asp:textbox id="txtstart" runat="server" CssClass="plainlabel" Width="130px" ReadOnly="True"></asp:textbox></td>
									<td><IMG onclick="getcal('txtstart');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
											width="19"></td>
								</tr>
								<tr height="30">
									<td align="right" colSpan="5"><asp:imagebutton id="btnwo" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
								</tr>
								<tr>
									<td colSpan="5">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<input id="lblpmid" type="hidden" runat="server" NAME="lblpmid"> <input id="lblpmhid" type="hidden" runat="server" NAME="lblpmhid">
			<input id="lblpmtid" type="hidden" runat="server" NAME="lblpmtid"> <input id="lblpmfid" type="hidden" runat="server" NAME="lblpmfid">
			<input id="lblfailid" type="hidden" runat="server" NAME="lblfailid"> <input id="lbltask" type="hidden" runat="server" NAME="lbltask">
			<input id="lblfm" type="hidden" runat="server" NAME="lblfm"> <input id="lblfmid" type="hidden" runat="server" NAME="lblfmid">
			<input id="lblwo" type="hidden" runat="server" NAME="lblwo"><input id="lblsubmit" type="hidden" runat="server" NAME="lblsubmit">
			<input id="lblcomid" type="hidden" runat="server" NAME="lblcomid"><input id="lblcid" type="hidden" runat="server" NAME="lblcid">
			<input id="leade" type="hidden" runat="server" NAME="leade"> <input id="supe" type="hidden" runat="server" NAME="supe">
			<input id="lbllead" type="hidden" runat="server" NAME="lbllead"> <input id="lblsup" type="hidden" runat="server" NAME="lblsup">
			<input type="hidden" id="lblro" runat="server" NAME="lblro"> <input type="hidden" id="lbldir" runat="server" NAME="lbldir">
			<input id="lbldragid" type="hidden" name="lbldragid" runat="server"><input id="lblrowid" type="hidden" name="lblrowid" runat="server">
            <input type="hidden" id="lblsid" runat="server" />
            <input type="hidden" id="lblleadid" runat="server" />
            <input type="hidden" id="lblsupid" runat="server" />
		<input type="hidden" id="lblstart" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="Hidden1" runat="server" />

</form>
	</body>
</HTML>
