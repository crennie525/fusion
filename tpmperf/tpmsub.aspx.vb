

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text

Public Class tpmsub
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid, funcid, tasknum, pmid As String
    Protected WithEvents tdsub As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            pmid = Request.QueryString("pmid").ToString
            eqid = Request.QueryString("eqid").ToString
            funcid = Request.QueryString("funcid").ToString
            tasknum = Request.QueryString("tasknum").ToString
            pms.Open()
            GetTasks(eqid, funcid, tasknum, pmid)
            pms.Dispose()
        End If
    End Sub
    Private Sub GetTasks(ByVal eqid As String, ByVal funcid As String, ByVal tasknum As String, ByVal pmid As String)
        If pmid <> "" Then
            sql = "usp_getpmmantpmperfst '" & pmid & "','" & eqid & "','" & funcid & "','" & tasknum & "'"
        Else
            sql = "usp_getWITotalTPMST '" & eqid & "','" & funcid & "','" & tasknum & "'"
        End If

        Dim sb As New StringBuilder
        sb.Append("<table width=""590"">")
        sb.Append("<tr><td class=""thdrsing label"" colSpan=""7"">" & tmod.getlbl("cdlbl1257", "tpmsub.aspx.vb") & "</td></tr>")
        sb.Append("<tr><td class=""plainlabel"" colSpan=""7"">&nbsp;</td></tr>")
        Dim subtask, task As String

        dr = pms.GetRdrData(sql)
        While dr.Read
            subtask = dr.Item("subtask").ToString
            task = dr.Item("task").ToString
            If subtask = "0" Then
                sb.Append("<tr><td class=""plainlabelblue14"" colSpan=""7"">" & task & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"" colSpan=""7"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""thdrsing label"" colSpan=""7"">" & tmod.getlbl("cdlbl1258", "tpmsub.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"" colSpan=""7"">&nbsp;</td></tr>")
            Else
                sb.Append("<tr><td class=""plainlabel14"" colSpan=""1"">[" & subtask & "]</td></tr>")
                sb.Append("<tr><td class=""plainlabelblue14"" colSpan=""7"">" & task & "</td></tr>")
                sb.Append("<tr><td class=""plainlabel"" colSpan=""7"">&nbsp;</td></tr>")
                If dr.Item("lube").ToString <> "none" Then
                    sb.Append("<tr><td class=""plainlabel"" colSpan=""1"">&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""plainlabel14"" colSpan=""7"">Lubricants:&nbsp;" & dr.Item("lubes").ToString & "</td></tr>")
                End If
                If dr.Item("part").ToString <> "none" Then
                    sb.Append("<tr><td class=""plainlabel"" colSpan=""1"">&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""plainlabel14"" colSpan=""7"">Parts:&nbsp;" & dr.Item("parts").ToString & "</td></tr>")
                End If
                If dr.Item("tool").ToString <> "none" Then
                    sb.Append("<tr><td class=""plainlabel"" colSpan=""1"">&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""plainlabel14"" colSpan=""7"">Tools:&nbsp;" & dr.Item("tools").ToString & "</td></tr>")
                End If
                sb.Append("<tr><td class=""plainlabel"" colSpan=""7"">&nbsp;</td></tr>")
            End If
        End While
        dr.Close()

        sb.Append("</table>")
        tdsub.InnerHtml = sb.ToString
    End Sub

End Class
