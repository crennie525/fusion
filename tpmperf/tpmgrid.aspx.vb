

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tpmgrid
    Inherits System.Web.UI.Page
    Protected WithEvents ovid294 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid293 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid292 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid291 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents iwi As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents iwa As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang3589 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3588 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3587 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3586 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3585 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3584 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3583 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3582 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3581 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3580 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3579 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tasks As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim ds As DataSet
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim start, eqid, ts As String
    Dim tl, cid, sid, did, clid, chk, fuid, typ, lid, coid, userid, islabor As String
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rptrtasks2 As System.Web.UI.WebControls.Repeater
    Protected WithEvents lbltyp1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                userid = HttpContext.Current.Session("userid").ToString()
                islabor = HttpContext.Current.Session("islabor").ToString()
                lbluserid.Value = userid
                lblislabor.Value = islabor
            Catch ex As Exception

            End Try
            start = Request.QueryString("start").ToString
            taskcnt.Value = "0"
            If start = "yes" Then

                'tl = Request.QueryString("tl").ToString
                'lbltasklev.Value = tl
                'cid = Request.QueryString("cid").ToString
                'lblcid.Value = cid
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                'did = Request.QueryString("did").ToString
                'lbldid.Value = did
                'clid = Request.QueryString("clid").ToString
                'lblclid.Value = clid
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                'chk = Request.QueryString("chk").ToString
                'lblchk.Value = chk
                fuid = Request.QueryString("fuid").ToString

                'typ = Request.QueryString("typ").ToString
                'lbltyp.Value = typ
                'lid = Request.QueryString("lid").ToString
                'lbllid.Value = lid
                Try
                    coid = Request.QueryString("comid").ToString
                Catch ex As Exception
                    coid = ""
                End Try
                If fuid = "0" Then
                    fuid = ""
                End If
                If coid = "0" Then
                    coid = ""
                End If

                lblcoid.Value = coid
                lblfuid.Value = fuid

                'eqid = Request.QueryString("eqid").ToString
                'lbleqid.Value = eqid
                tasks.Open()
                Try
                    ts = Request.QueryString("ts").ToString
                Catch ex As Exception
                    ts = "0"
                End Try

                LoadPage(eqid, userid, islabor, ts)
                tasks.Dispose()
            Else
                eqid = "na"
                tasks.Open()
                Try
                    ts = Request.QueryString("ts").ToString
                Catch ex As Exception
                    ts = "0"
                End Try
                userid = lbluserid.Value
                islabor = lblislabor.Value
                LoadPage(eqid, userid, islabor, ts)
                tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub LoadPage(ByVal eqid As String, ByVal userid As String, ByVal islabor As String, Optional ByVal ts As String = "0", Optional ByVal typ As String = "no")
        Dim cnt As Integer
        Dim sqlcnt As String
        If eqid = "na" Then
            cnt = 0
            'ts = "0"
            If islabor = "1" Then
                sql = "select distinct pm.pmid, pm.wonum, pmhid = '', pm.fcust, " _
                + "pmd = (" _
                + "case t.ptid " _
                + "when 0 then 'None' " _
                + "else t.pretech " _
                + "End " _
                          + "), " _
                          + "pm = ('Operator/' + t.freq + ' days/' + t.rd), " _
                          + "pm.lastdate, pm.nextdate, '0' as 'flag', d.*, pm.shift " _
                          + "from tpm pm join pmtaskstpm t on t.pmid = pm.pmid left join pmtasksdays d on t.pmtskid = d.pmtskid " _
                          + "where pm.eqid = 0 order by nextdate"
            Else
                sql = "select distinct pm.pmid, pm.wonum, pmhid = '', pm.fcust, " _
               + "pmd = (" _
               + "case t.ptid " _
               + "when 0 then 'None' " _
               + "else t.pretech " _
               + "End " _
                         + "), " _
                         + "pm = ('Operator/' + t.freq + ' days/' + t.rd), " _
                         + "pm.lastdate, pm.nextdate, '0' as 'flag', d.*, pm.shift " _
                         + "from tpm pm join pmtaskstpm t on t.pmid = pm.pmid left join pmtasksdays d on t.pmtskid = d.pmtskid " _
                         + "where pm.eqid = 0 order by nextdate"
            End If
            lbleqid.Value = "na"
        Else
            cnt = 1
            'ts = "0" And 
            If islabor = "1" Then
                sql = "select distinct pm.pmid, pm.wonum, pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
                + "pmhid = (select max(p1.pmhid) from pmhisttpm p1 where p1.pmid = pm.pmid), " _
                 + "pm = ( " _
                + "case isnull(len(pm.shift),0) " _
                + "when 0 then 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd  " _
                + "else " _
                + "case pm.freq " _
                + "when 0 then 'Operator/STATIC/' + pm.rd + ' - ' + isnull(pm.shift,'open') + " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days end end  " _
                + "else 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd + ' - ' + isnull(pm.shift,'open') +  " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days  " _
                + "end end end end), " _
                + "Convert(char(10), pm.lastdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate', '0' as 'flag', " _
                + "pm.shift, " _
                + "d.cust, d.cb1o, d.cb1, d.cb1mon, d.cb1tue, d.cb1wed, d.cb1thu, d.cb1fri, d.cb1sat, d.cb1sun, d.cb2o, d.cb2, d.cb2mon, d.cb2tue, d.cb2wed, d.cb2thu, " _
                + "d.cb2fri, d.cb2sat, d.cb2sun, d.cb3o, d.cb3, d.cb3mon, d.cb3tue, d.cb3wed, d.cb3thu, d.cb3fri, d.cb3sat, d.cb3sun " _
                + "from tpm pm left join pmtrackdaystpm d on d.pmid = pm.pmid where pm.eqid = '" & eqid & "' and pm.nextdate is not null"

                sqlcnt = "select count(*) from tpm where eqid = '" & eqid & "' " _
                + "and (eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                + "eqid in (select eqid from tpm where leadid = '" & userid & "'))"
                sql += " and (pm.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                    + "pm.eqid in (select eqid from tpm where leadid = '" & userid & "'))"

                fuid = "" 'lblfuid.Value
                coid = "" 'lblcoid.Value

                If fuid <> "" And coid = "" Then
                    sql += " and pm.eqid in (select eqid from functions where func_id = '" & fuid & "')"
                    sqlcnt += " and eqid in (select eqid from functions where func_id = '" & fuid & "')"

                ElseIf fuid <> "" And coid <> "" Then
                    sql += " and pm.eqid in (select eqid from functions where func_id in (select func_id from components where comid =  '" & coid & "'))"
                    sqlcnt += " and eqid in (select eqid from functions where func_id in (select func_id from components where comid =  '" & coid & "'))"

                End If
            Else
                'fuid = lblfuid.Value
                'coid = lblcoid.Value

                'If fuid = "" And coid = "" Then
                'sql = "usp_getWITotalTPMEL '" & eqid & "'"

                'ElseIf fuid <> "" And coid = "" Then
                'sql = "usp_getWITotalTPMFL '" & eqid & "','" & fuid & "'"

                'ElseIf fuid <> "" And coid <> "" Then
                'sql = "usp_getWITotalTPMCL '" & eqid & "','" & fuid & "','" & coid & "'"

                'End If

                sql = "select distinct pm.pmid, pm.wonum, pmd = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
                + "pmhid = (select max(p1.pmhid) from pmhisttpm p1 where p1.pmid = pm.pmid), " _
                 + "pm = ( " _
                + "case isnull(len(pm.shift),0) " _
                + "when 0 then 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd  " _
                + "else " _
                + "case pm.freq " _
                + "when 0 then 'Operator/STATIC/' + pm.rd + ' - ' + isnull(pm.shift,'open') + " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days end end  " _
                + "else 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd + ' - ' + isnull(pm.shift,'open') +  " _
                + "case len(pm.days) " _
                + "when 0 then '' " _
                + "else " _
                + "case pm.freq " _
                + "when 1 then '' " _
                + "else ' - ' " _
                + "+ pm.days  " _
                + "end end end end), " _
                + "Convert(char(10), pm.lastdate,101) as 'lastdate', Convert(char(10), pm.nextdate,101) as 'nextdate', '0' as 'flag', " _
                + "pm.shift, " _
                + "d.cust, d.cb1o, d.cb1, d.cb1mon, d.cb1tue, d.cb1wed, d.cb1thu, d.cb1fri, d.cb1sat, d.cb1sun, d.cb2o, d.cb2, d.cb2mon, d.cb2tue, d.cb2wed, d.cb2thu, " _
                + "d.cb2fri, d.cb2sat, d.cb2sun, d.cb3o, d.cb3, d.cb3mon, d.cb3tue, d.cb3wed, d.cb3thu, d.cb3fri, d.cb3sat, d.cb3sun " _
                + "from tpm pm left join pmtrackdaystpm d on d.pmid = pm.pmid where pm.eqid = '" & eqid & "' and pm.nextdate is not null"

                sqlcnt = "select count(*) from tpm where eqid = '" & eqid & "' "
                'sql += " and (pm.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                '   + "pm.eqid in (select eqid from tpm where leadid = '" & userid & "'))"
            End If

        End If
        If cnt <> 0 Then
            If ts = "0" Then
                cnt = tasks.Scalar(sqlcnt)
                taskcnt.Value = cnt
            Else

            End If
        Else
            taskcnt.Value = "0"
        End If

        ds = tasks.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        If ts = "0" Then
            rptrtasks.DataSource = dt
            Dim i, eint As Integer
            eint = 15
            For i = cnt To eint
                dt.Rows.InsertAt(dt.NewRow(), i)
            Next
            rptrtasks.DataBind()
            rptrtasks2.Visible = False
            rptrtasks.Visible = True
        Else
            rptrtasks2.DataSource = dt
            cnt = dt.Rows.Count
            taskcnt.Value = cnt
            Dim i, eint As Integer
            eint = 15
            For i = cnt To eint
                dt.Rows.InsertAt(dt.NewRow(), i)
            Next
            rptrtasks2.DataBind()
            rptrtasks.Visible = False
            rptrtasks2.Visible = True
        End If



    End Sub

    Private Sub rptrtasks_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtasks.ItemCommand
        If e.CommandName = "Select" Then
            Dim pmid, pmstr, pdm, dlast, dnext, wonum, pmhid As String
            Try
                pmid = CType(e.Item.FindControl("lblpmiditem"), Label).Text
                pmhid = CType(e.Item.FindControl("lblpmhiditem"), Label).Text
                wonum = CType(e.Item.FindControl("lblpmwoi"), Label).Text
                'pmstr = CType(e.Item.FindControl("lbltn"), LinkButton).Text
                'pdm = CType(e.Item.FindControl("lblpdm"), Label).Text
                'dlast = CType(e.Item.FindControl("lbllast"), Label).Text
                'dnext = CType(e.Item.FindControl("lblnext"), Label).Text
            Catch ex As Exception
                pmid = CType(e.Item.FindControl("lblpmidalt"), Label).Text
                pmhid = CType(e.Item.FindControl("lblpmhidalt"), Label).Text
                wonum = CType(e.Item.FindControl("lblpmwoa"), Label).Text
                'pmstr = CType(e.Item.FindControl("lbltnalt"), LinkButton).Text
                'pdm = CType(e.Item.FindControl("lblpdmalt"), Label).Text
                'dlast = CType(e.Item.FindControl("lbllastalt"), Label).Text
                'dnext = CType(e.Item.FindControl("lblnextalt"), Label).Text
            End Try
            eqid = lbleqid.Value
            lblpmid.Value = pmid
            lblpmhid.Value = pmhid
            lblwo.Value = wonum
            lblret.Value = "s"
            'Response.Redirect("PMDivMantpm.aspx?pmid=" & pmid & "&eqid=" & eqid & "&pmstr=" & pmstr & "&pdm=" & pdm & "&last=" & dlast & "&next=" & dnext)

        End If
    End Sub
    Private Sub rptrtasks2_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrtasks2.ItemCommand

        If e.CommandName = "Select" Then
            Dim typ, rd, freqi As String
            Try
                typ = CType(e.Item.FindControl("lblpmiditem2"), Label).Text
                rd = CType(e.Item.FindControl("lblrd2"), Label).Text
                freqi = CType(e.Item.FindControl("lblfreqi2"), Label).Text

            Catch ex As Exception
                typ = CType(e.Item.FindControl("lblpmidalt2"), Label).Text
                rd = CType(e.Item.FindControl("lblrdalt2"), Label).Text
                freqi = CType(e.Item.FindControl("lblfreqialt2"), Label).Text

            End Try
            eqid = lbleqid.Value
            lbltyp1.Value = typ
            lblrd.Value = rd
            lblfreq.Value = freqi
            lblret.Value = "o"
            'Response.Redirect("PMDivMantpm.aspx?pmid=" & pmid & "&eqid=" & eqid & "&pmstr=" & pmstr & "&pdm=" & pdm & "&last=" & dlast & "&next=" & dnext)

        End If
    End Sub
    Private Sub rptrtasks2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks2.ItemDataBound
        Dim cb, cbmon, cbtue, cbwed, cbthu, cbfri, cbsat, cbsun As HtmlInputCheckBox
        Dim cbs, cbmons, cbtues, cbweds, cbthus, cbfris, cbsats, cbsuns As String
        Dim shift As String

        If e.Item.ItemType = ListItemType.Item Then

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then

        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang3579 As Label
                lang3579 = CType(e.Item.FindControl("lang3579"), Label)
                lang3579.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3579")
            Catch ex As Exception
            End Try
            Try
                Dim lang3580 As Label
                lang3580 = CType(e.Item.FindControl("lang3580"), Label)
                lang3580.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3580")
            Catch ex As Exception
            End Try
            Try
                Dim lang3581 As Label
                lang3581 = CType(e.Item.FindControl("lang3581"), Label)
                lang3581.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3581")
            Catch ex As Exception
            End Try
            Try
                Dim lang3582 As Label
                lang3582 = CType(e.Item.FindControl("lang3582"), Label)
                lang3582.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3582")
            Catch ex As Exception
            End Try
            Try
                Dim lang3583 As Label
                lang3583 = CType(e.Item.FindControl("lang3583"), Label)
                lang3583.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3583")
            Catch ex As Exception
            End Try
            Try
                Dim lang3584 As Label
                lang3584 = CType(e.Item.FindControl("lang3584"), Label)
                lang3584.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3584")
            Catch ex As Exception
            End Try
            Try
                Dim lang3585 As Label
                lang3585 = CType(e.Item.FindControl("lang3585"), Label)
                lang3585.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3585")
            Catch ex As Exception
            End Try
            Try
                Dim lang3586 As Label
                lang3586 = CType(e.Item.FindControl("lang3586"), Label)
                lang3586.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3586")
            Catch ex As Exception
            End Try
            Try
                Dim lang3587 As Label
                lang3587 = CType(e.Item.FindControl("lang3587"), Label)
                lang3587.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3587")
            Catch ex As Exception
            End Try
            Try
                Dim lang3588 As Label
                lang3588 = CType(e.Item.FindControl("lang3588"), Label)
                lang3588.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3588")
            Catch ex As Exception
            End Try
            Try
                Dim lang3589 As Label
                lang3589 = CType(e.Item.FindControl("lang3589"), Label)
                lang3589.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3589")
            Catch ex As Exception
            End Try

        End If


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang3579 As Label
                lang3579 = CType(e.Item.FindControl("lang3579"), Label)
                lang3579.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3579")
            Catch ex As Exception
            End Try
            Try
                Dim lang3580 As Label
                lang3580 = CType(e.Item.FindControl("lang3580"), Label)
                lang3580.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3580")
            Catch ex As Exception
            End Try
            Try
                Dim lang3581 As Label
                lang3581 = CType(e.Item.FindControl("lang3581"), Label)
                lang3581.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3581")
            Catch ex As Exception
            End Try
            Try
                Dim lang3582 As Label
                lang3582 = CType(e.Item.FindControl("lang3582"), Label)
                lang3582.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3582")
            Catch ex As Exception
            End Try
            Try
                Dim lang3583 As Label
                lang3583 = CType(e.Item.FindControl("lang3583"), Label)
                lang3583.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3583")
            Catch ex As Exception
            End Try
            Try
                Dim lang3584 As Label
                lang3584 = CType(e.Item.FindControl("lang3584"), Label)
                lang3584.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3584")
            Catch ex As Exception
            End Try
            Try
                Dim lang3585 As Label
                lang3585 = CType(e.Item.FindControl("lang3585"), Label)
                lang3585.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3585")
            Catch ex As Exception
            End Try
            Try
                Dim lang3586 As Label
                lang3586 = CType(e.Item.FindControl("lang3586"), Label)
                lang3586.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3586")
            Catch ex As Exception
            End Try
            Try
                Dim lang3587 As Label
                lang3587 = CType(e.Item.FindControl("lang3587"), Label)
                lang3587.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3587")
            Catch ex As Exception
            End Try
            Try
                Dim lang3588 As Label
                lang3588 = CType(e.Item.FindControl("lang3588"), Label)
                lang3588.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3588")
            Catch ex As Exception
            End Try
            Try
                Dim lang3589 As Label
                lang3589 = CType(e.Item.FindControl("lang3589"), Label)
                lang3589.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3589")
            Catch ex As Exception
            End Try

        End If

    End Sub
    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        Dim cb, cbmon, cbtue, cbwed, cbthu, cbfri, cbsat, cbsun As HtmlInputCheckBox
        Dim cbs, cbmons, cbtues, cbweds, cbthus, cbfris, cbsats, cbsuns As String
        Dim shift, pm As String

        If e.Item.ItemType = ListItemType.Item Then
            shift = DataBinder.Eval(e.Item.DataItem, "shift").ToString
            pm = DataBinder.Eval(e.Item.DataItem, "pm").ToString
            Dim tbli As HtmlTable = CType(e.Item.FindControl("tbli"), HtmlTable)
            cb = CType(e.Item.FindControl("cb1o"), HtmlInputCheckBox)
            cbmon = CType(e.Item.FindControl("cb1mon"), HtmlInputCheckBox)
            cbtue = CType(e.Item.FindControl("cb1tue"), HtmlInputCheckBox)
            cbwed = CType(e.Item.FindControl("cb1wed"), HtmlInputCheckBox)
            cbthu = CType(e.Item.FindControl("cb1thu"), HtmlInputCheckBox)
            cbfri = CType(e.Item.FindControl("cb1fri"), HtmlInputCheckBox)
            cbsat = CType(e.Item.FindControl("cb1sat"), HtmlInputCheckBox)
            cbsun = CType(e.Item.FindControl("cb1sun"), HtmlInputCheckBox)
            Dim si As HtmlTableCell = CType(e.Item.FindControl("lblsi"), HtmlTableCell)

            If shift = "1st shift" Then
                cbs = DataBinder.Eval(e.Item.DataItem, "cb1o").ToString
                cbmons = DataBinder.Eval(e.Item.DataItem, "cb1mon").ToString
                cbtues = DataBinder.Eval(e.Item.DataItem, "cb1tue").ToString
                cbweds = DataBinder.Eval(e.Item.DataItem, "cb1wed").ToString
                cbthus = DataBinder.Eval(e.Item.DataItem, "cb1thu").ToString
                cbfris = DataBinder.Eval(e.Item.DataItem, "cb1fri").ToString
                cbsats = DataBinder.Eval(e.Item.DataItem, "cb1sat").ToString
                cbsuns = DataBinder.Eval(e.Item.DataItem, "cb1sun").ToString
                tbli.Attributes.Add("class", "view")
                si.InnerHtml = "1st"
            ElseIf shift = "2nd shift" Then
                cbs = DataBinder.Eval(e.Item.DataItem, "cb2o").ToString
                cbmons = DataBinder.Eval(e.Item.DataItem, "cb2mon").ToString
                cbtues = DataBinder.Eval(e.Item.DataItem, "cb2tue").ToString
                cbweds = DataBinder.Eval(e.Item.DataItem, "cb2wed").ToString
                cbthus = DataBinder.Eval(e.Item.DataItem, "cb2thu").ToString
                cbfris = DataBinder.Eval(e.Item.DataItem, "cb2fri").ToString
                cbsats = DataBinder.Eval(e.Item.DataItem, "cb2sat").ToString
                cbsuns = DataBinder.Eval(e.Item.DataItem, "cb2sun").ToString
                tbli.Attributes.Add("class", "view")
                si.InnerHtml = "2nd"
            ElseIf shift = "3rd shift" Then
                cbs = DataBinder.Eval(e.Item.DataItem, "cb3o").ToString
                cbmons = DataBinder.Eval(e.Item.DataItem, "cb3mon").ToString
                cbtues = DataBinder.Eval(e.Item.DataItem, "cb3tue").ToString
                cbweds = DataBinder.Eval(e.Item.DataItem, "cb3wed").ToString
                cbthus = DataBinder.Eval(e.Item.DataItem, "cb3thu").ToString
                cbfris = DataBinder.Eval(e.Item.DataItem, "cb3fri").ToString
                cbsats = DataBinder.Eval(e.Item.DataItem, "cb3sat").ToString
                cbsuns = DataBinder.Eval(e.Item.DataItem, "cb3sun").ToString
                tbli.Attributes.Add("class", "view")
                si.InnerHtml = "3rd"
            ElseIf lbleqid.Value <> "na" And pm <> "" Then
                tbli.Attributes.Add("class", "view")
                si.InnerHtml = "N\A"
            End If

            If cbs = "1" Then
                cb.Checked = True
            End If
            If cbmons = "1" Then
                cbmon.Checked = True
            Else
                cbmon.Checked = False
            End If
            If cbtues = "1" Then
                cbtue.Checked = True
            Else
                cbtue.Checked = False
            End If
            If cbweds = "1" Then
                cbwed.Checked = True
            Else
                cbwed.Checked = False
            End If
            If cbthus = "1" Then
                cbthu.Checked = True
            Else
                cbthu.Checked = False
            End If
            If cbfris = "1" Then
                cbfri.Checked = True
            Else
                cbfri.Checked = False
            End If
            If cbsats = "1" Then
                cbsat.Checked = True
            Else
                cbsat.Checked = False
            End If
            If cbsuns = "1" Then
                cbsun.Checked = True
            Else
                cbsun.Checked = False
            End If
            Dim link As LinkButton = CType(e.Item.FindControl("lbltn"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "flag").ToString
            If id <> "0" Then
                link.Attributes.Add("class", "redlink")
                'link.Attributes("CssClass") = "redlink"
                'link.Attributes("ForeColor") = "Red"
            End If
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwi"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            If newdate <> "" Then
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov331", "tpmgrid.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov332", "tpmgrid.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Else
                simg.Attributes.Add("src", "../images/appbuttons/minibuttons/2PX.gif")
            End If

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            shift = DataBinder.Eval(e.Item.DataItem, "shift").ToString
            pm = DataBinder.Eval(e.Item.DataItem, "pm").ToString
            Dim tbla As HtmlTable = CType(e.Item.FindControl("tbla"), HtmlTable)
            cb = CType(e.Item.FindControl("cb1oa"), HtmlInputCheckBox)
            cbmon = CType(e.Item.FindControl("cb1mona"), HtmlInputCheckBox)
            cbtue = CType(e.Item.FindControl("cb1tuea"), HtmlInputCheckBox)
            cbwed = CType(e.Item.FindControl("cb1weda"), HtmlInputCheckBox)
            cbthu = CType(e.Item.FindControl("cb1thua"), HtmlInputCheckBox)
            cbfri = CType(e.Item.FindControl("cb1fria"), HtmlInputCheckBox)
            cbsat = CType(e.Item.FindControl("cb1sata"), HtmlInputCheckBox)
            cbsun = CType(e.Item.FindControl("cb1suna"), HtmlInputCheckBox)
            Dim sa As HtmlTableCell = CType(e.Item.FindControl("lblsa"), HtmlTableCell)

            If shift = "1st shift" Then
                cbs = DataBinder.Eval(e.Item.DataItem, "cb1o").ToString
                cbmons = DataBinder.Eval(e.Item.DataItem, "cb1mon").ToString
                cbtues = DataBinder.Eval(e.Item.DataItem, "cb1tue").ToString
                cbweds = DataBinder.Eval(e.Item.DataItem, "cb1wed").ToString
                cbthus = DataBinder.Eval(e.Item.DataItem, "cb1thu").ToString
                cbfris = DataBinder.Eval(e.Item.DataItem, "cb1fri").ToString
                cbsats = DataBinder.Eval(e.Item.DataItem, "cb1sat").ToString
                cbsuns = DataBinder.Eval(e.Item.DataItem, "cb1sun").ToString
                tbla.Attributes.Add("class", "view")
                sa.InnerHtml = "1st"
            ElseIf shift = "2nd shift" Then
                cbs = DataBinder.Eval(e.Item.DataItem, "cb2o").ToString
                cbmons = DataBinder.Eval(e.Item.DataItem, "cb2mon").ToString
                cbtues = DataBinder.Eval(e.Item.DataItem, "cb2tue").ToString
                cbweds = DataBinder.Eval(e.Item.DataItem, "cb2wed").ToString
                cbthus = DataBinder.Eval(e.Item.DataItem, "cb2thu").ToString
                cbfris = DataBinder.Eval(e.Item.DataItem, "cb2fri").ToString
                cbsats = DataBinder.Eval(e.Item.DataItem, "cb2sat").ToString
                cbsuns = DataBinder.Eval(e.Item.DataItem, "cb2sun").ToString
                tbla.Attributes.Add("class", "view")
                sa.InnerHtml = "2nd"
            ElseIf shift = "3rd shift" Then
                cbs = DataBinder.Eval(e.Item.DataItem, "cb3o").ToString
                cbmons = DataBinder.Eval(e.Item.DataItem, "cb3mon").ToString
                cbtues = DataBinder.Eval(e.Item.DataItem, "cb3tue").ToString
                cbweds = DataBinder.Eval(e.Item.DataItem, "cb3wed").ToString
                cbthus = DataBinder.Eval(e.Item.DataItem, "cb3thu").ToString
                cbfris = DataBinder.Eval(e.Item.DataItem, "cb3fri").ToString
                cbsats = DataBinder.Eval(e.Item.DataItem, "cb3sat").ToString
                cbsuns = DataBinder.Eval(e.Item.DataItem, "cb3sun").ToString
                tbla.Attributes.Add("class", "view")
                sa.InnerHtml = "3rd"
            ElseIf lbleqid.Value <> "na" And pm <> "" Then
                tbla.Attributes.Add("class", "view")
                sa.InnerHtml = "N\A"
            End If

            If cbs = "1" Then
                cb.Checked = True
            End If
            If cbmons = "1" Then
                cbmon.Checked = True
            Else
                cbmon.Checked = False
            End If
            If cbtues = "1" Then
                cbtue.Checked = True
            Else
                cbtue.Checked = False
            End If
            If cbweds = "1" Then
                cbwed.Checked = True
            Else
                cbwed.Checked = False
            End If
            If cbthus = "1" Then
                cbthu.Checked = True
            Else
                cbthu.Checked = False
            End If
            If cbfris = "1" Then
                cbfri.Checked = True
            Else
                cbfri.Checked = False
            End If
            If cbsats = "1" Then
                cbsat.Checked = True
            Else
                cbsat.Checked = False
            End If
            If cbsuns = "1" Then
                cbsun.Checked = True
            Else
                cbsun.Checked = False
            End If


            Dim link As LinkButton = CType(e.Item.FindControl("lbltnalt"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "flag").ToString
            If id <> "0" Then
                link.Attributes.Add("class", "redlink")
                'link.Attributes("CssClass") = "linklabelred"
            End If
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwa"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            If newdate <> "" Then
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov333", "tpmgrid.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov334", "tpmgrid.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Else
                simg.Attributes.Add("src", "../images/appbuttons/minibuttons/2PX.gif")
            End If
        End If
    End Sub














    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3579.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3579")
        Catch ex As Exception
        End Try
        Try
            lang3580.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3580")
        Catch ex As Exception
        End Try
        Try
            lang3581.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3581")
        Catch ex As Exception
        End Try
        Try
            lang3582.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3582")
        Catch ex As Exception
        End Try
        Try
            lang3583.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3583")
        Catch ex As Exception
        End Try
        Try
            lang3584.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3584")
        Catch ex As Exception
        End Try
        Try
            lang3585.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3585")
        Catch ex As Exception
        End Try
        Try
            lang3586.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3586")
        Catch ex As Exception
        End Try
        Try
            lang3587.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3587")
        Catch ex As Exception
        End Try
        Try
            lang3588.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3588")
        Catch ex As Exception
        End Try
        Try
            lang3589.Text = axlabs.GetASPXPage("tpmgrid.aspx", "lang3589")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmgrid.aspx", "Img1") & "', ABOVE, LEFT)")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmgrid.aspx", "Img2") & "', ABOVE, LEFT)")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwa.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmgrid.aspx", "iwa") & "', ABOVE, LEFT)")
            iwa.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwi.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmgrid.aspx", "iwi") & "', ABOVE, LEFT)")
            iwi.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid291.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmgrid.aspx", "ovid291") & "')")
            ovid291.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid292.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmgrid.aspx", "ovid292") & "')")
            ovid292.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid293.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmgrid.aspx", "ovid293") & "')")
            ovid293.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid294.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmgrid.aspx", "ovid294") & "')")
            ovid294.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
