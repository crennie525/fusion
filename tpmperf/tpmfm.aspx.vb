

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class tpmfm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, eqid, freq, rd, typ, tasknum, pmid, dir, pmtskid, pmtid, fid As String
    Dim news As New Utilities
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm4 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm5 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm1i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm2i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm3i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm4i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm5i As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldir As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdfail As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            dir = Request.QueryString("dir").ToString
            lbldir.Value = dir
            If dir = "o" Then
                Try
                    fid = Request.QueryString("fid").ToString
                Catch ex As Exception
                    fid = "0"
                End Try
                'eqid = "275" 'Request.QueryString("eqid").ToString
                'freq = "7" 'Request.QueryString("freq").ToString
                'rd = "Running" 'Request.QueryString("rd").ToString
                'typ = "0" 'Request.QueryString("typ").ToString
                'tasknum = "1" 'Request.QueryString("tasknum").ToString
                pmtskid = Request.QueryString("pmtskid").ToString '"24" '

                news.Open()
                LoadFail(pmtskid)
                news.Dispose()
            Else
                Try
                    fid = Request.QueryString("fid").ToString
                Catch ex As Exception
                    fid = "0"
                End Try
                pmid = Request.QueryString("pmid").ToString
                lblpmid.Value = pmid
                tasknum = Request.QueryString("tasknum").ToString
                lbltasknum.Value = tasknum
                pmtid = Request.QueryString("pmtid").ToString
                lblpmtid.Value = pmtid
                news.Open()
                LoadSFail(pmid, tasknum, fid)
                news.Dispose()
            End If

        Else
            If Request.Form("lblsubmit") = "savefm" Then
                lblsubmit.Value = ""
                pmid = lblpmid.Value
                pmtid = lblpmtid.Value
                tasknum = lbltasknum.Value
                news.Open()
                SaveFM()
                LoadSFail(pmid, tasknum)
                news.Dispose()
            End If


        End If
    End Sub
    Private Sub SaveFM()
        Dim fid As String = lblfmid.Value
        Dim val As String = lblval.Value
        pmtid = lblpmtid.Value
        Select Case fid
            Case "fm1i"
                sql = "update pmtracktpm set fm1 = '" & val & "', taskcomp = 0 where pmtid = '" & pmtid & "'"
            Case "fm2i"
                sql = "update pmtracktpm set fm2 = '" & val & "', taskcomp = 0 where pmtid = '" & pmtid & "'"
            Case "fm3i"
                sql = "update pmtracktpm set fm3 = '" & val & "', taskcomp = 0 where pmtid = '" & pmtid & "'"
            Case "fm4i"
                sql = "update pmtracktpm set fm4 = '" & val & "', taskcomp = 0 where pmtid = '" & pmtid & "'"
            Case "fm5i"
                sql = "update pmtracktpm set fm5 = '" & val & "', taskcomp = 0 where pmtid = '" & pmtid & "'"
        End Select
        news.Update(sql)
        'temp for demo mode
        If val = "2" Then
            lblalert.Value = "yes"
        End If

    End Sub
    Private Sub LoadSFail(ByVal pmid As String, ByVal tasknum As String, Optional ByVal fid As String = "0")
        sql = "usp_getpmmantpmperf '" & pmid & "','1','5','" & tasknum & "'"
        Dim fm1, fm2, fm3, fm4, fm5 As String
        Dim fm1i, fm2i, fm3i, fm4i, fm5i As String
        Dim fm1o, fm2o, fm3o, fm4o, fm5o As String
        Dim chkdp, chkdf As String
        Dim comid As String

        dr = news.GetRdrData(sql)
        While dr.Read

            fm1 = dr.Item("fm1s").ToString
            fm2 = dr.Item("fm2s").ToString
            fm3 = dr.Item("fm3s").ToString
            fm4 = dr.Item("fm4s").ToString
            fm5 = dr.Item("fm5s").ToString

            fm1i = dr.Item("fm1id").ToString
            fm2i = dr.Item("fm2id").ToString
            fm3i = dr.Item("fm3id").ToString
            fm4i = dr.Item("fm4id").ToString
            fm5i = dr.Item("fm5id").ToString

            fm1o = dr.Item("fm1").ToString
            fm2o = dr.Item("fm2").ToString
            fm3o = dr.Item("fm3").ToString
            fm4o = dr.Item("fm4").ToString
            fm5o = dr.Item("fm5").ToString

            comid = dr.Item("comid").ToString
            lblcomid.Value = comid
        End While
        dr.Close()
        Dim sb As New StringBuilder
        sb.Append("<table>")
        If fm1i <> 0 Then
            lblfm1i.Value = fm1i
            If fm1o = 0 Or fm1o = 1 Then
                chkdp = "checked"
                lblfm1.Value = "1"
            Else
                chkdf = "checked"
                lblfm1.Value = "2"
            End If
            If fm1i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm1 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm1 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb1p"" name=""rb1"" onclick=""uprb('lblfm1','1','fm1i','" & fm1 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb1f"" name=""rb1"" onclick=""uprb('lblfm1','2','fm1i','" & fm1 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If
        chkdp = ""
        chkdf = ""
        If fm2i <> 0 Then
            lblfm2i.Value = fm2i
            If fm2o = 0 Or fm2o = 1 Then
                chkdp = "checked"
                lblfm2.Value = "1"
            Else
                chkdf = "checked"
                lblfm2.Value = "2"
            End If
            If fm2i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm2 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm2 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb2p"" name=""rb2"" onclick=""uprb('lblfm2','1','fm2i','" & fm2 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb2f"" name=""rb2"" onclick=""uprb('lblfm2','2','fm2i','" & fm2 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If
        chkdp = ""
        chkdf = ""
        If fm3i <> 0 Then
            lblfm3i.Value = fm3i
            If fm3o = 0 Or fm3o = 1 Then
                chkdp = "checked"
                lblfm3.Value = "1"
            Else
                chkdf = "checked"
                lblfm3.Value = "2"
            End If
            If fm3i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm3 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm3 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb3p"" name=""rb3"" onclick=""uprb('lblfm3','1','fm3i','" & fm3 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb3f"" name=""rb3"" onclick=""uprb('lblfm3','2','fm3i','" & fm3 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If
        chkdp = ""
        chkdf = ""
        If fm4i <> 0 Then
            lblfm4i.Value = fm4i
            If fm4o = 0 Or fm4o = 1 Then
                chkdp = "checked"
                lblfm4.Value = "1"
            Else
                chkdf = "checked"
                lblfm4.Value = "2"
            End If
            If fm4i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm4 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm4 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb4p"" name=""rb4"" onclick=""uprb('lblfm4','1','fm4i','" & fm4 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb4f"" name=""rb4"" onclick=""uprb('lblfm4','2','fm4i','" & fm4 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If
        chkdp = ""
        chkdf = ""
        If fm5i <> 0 Then
            lblfm5i.Value = fm5i
            If fm5o = 0 Or fm5o = 1 Then
                chkdp = "checked"
                lblfm5.Value = "1"
            Else
                chkdf = "checked"
                lblfm5.Value = "2"
            End If
            If fm5i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm5 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm5 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb5p"" name=""rb5"" onclick=""uprb('lblfm5','1','fm5i','" & fm5 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb5f"" name=""rb5"" onclick=""uprb('lblfm5','2','fm5i','" & fm5 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If
        sb.Append("</table>")
        tdfail.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadFail(ByVal pmtskid As String, Optional ByVal fid As String = "0")
        'sql = "usp_getWITotalTPMperf '" & eqid & "','" & freq & "','" & rd & "','" & typ & "','" & tasknum & "','1'"
        sql = "select parentfailid as failid, failuremode from pmtaskfailmodestpm where taskid = '" & pmtskid & "' order by failuremode"
        Dim fm1, fm2, fm3, fm4, fm5 As String
        Dim fm1i, fm2i, fm3i, fm4i, fm5i As String
        Dim fm1o, fm2o, fm3o, fm4o, fm5o As String
        Dim chkdp, chkdf As String
        Dim fcnt As Integer = 1
        Dim sb As New StringBuilder
        sb.Append("<table>")
        'If fm1 <> "" Then
        dr = news.GetRdrData(sql)
        While dr.Read
            Select Case fcnt
                Case 1
                    fm1i = dr.Item("failid").ToString
                    fm1 = dr.Item("failuremode").ToString
                Case 2
                    fm2i = dr.Item("failid").ToString
                    fm2 = dr.Item("failuremode").ToString
                Case 3
                    fm3i = dr.Item("failid").ToString
                    fm3 = dr.Item("failuremode").ToString
                Case 4
                    fm4i = dr.Item("failid").ToString
                    fm4 = dr.Item("failuremode").ToString
                Case 5
                    fm5i = dr.Item("failid").ToString
                    fm5 = dr.Item("failuremode").ToString
            End Select
            fcnt += 1
        End While
        dr.Close()
        chkdp = "checked"
        chkdf = ""
        If fm1i <> 0 Then
            lblfm1i.Value = fm1i
            If fm1i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm1 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm1 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb1p"" name=""rb1"" onclick=""uprb('lblfm1','1','fm1i','" & fm1 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb1f"" name=""rb1"" onclick=""uprb('lblfm1','2','fm1i','" & fm1 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If

        If fm2i <> 0 Then
            lblfm2i.Value = fm2i
            If fm2i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm2 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm2 & "</td>")
            End If
            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb2p"" name=""rb2"" onclick=""uprb('lblfm2','1','fm2i','" & fm2 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb2f"" name=""rb2"" onclick=""uprb('lblfm2','2','fm2i','" & fm2 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If

        If fm3i <> 0 Then
            lblfm3i.Value = fm3i
            If fm3i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm3 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm3 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb3p"" name=""rb3"" onclick=""uprb('lblfm3','1','fm3i','" & fm3 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb3f"" name=""rb3"" onclick=""uprb('lblfm3','2','fm3i','" & fm3 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If

        If fm4i <> 0 Then
            lblfm4i.Value = fm4i
            If fm4i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm4 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm4 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb4p"" name=""rb4"" onclick=""uprb('lblfm4','1','fm4i','" & fm4 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb4f"" name=""rb4"" onclick=""uprb('lblfm4','2','fm4i','" & fm4 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If

        If fm5i <> 0 Then
            lblfm5i.Value = fm5i
            If fm5i = fid Then
                sb.Append("<tr><td class=""plainlabelred"">" & fm5 & "</td>")
            Else
                sb.Append("<tr><td class=""plainlabel"">" & fm5 & "</td>")
            End If

            sb.Append("<td class=""plainlabelblue""><input type=""radio"" id=""rb5p"" name=""rb5"" onclick=""uprb('lblfm5','1','fm5i','" & fm5 & "');"" " & chkdp & ">&nbsp;Pass&nbsp;<input type=""radio"" id=""rb5f"" name=""rb5"" onclick=""uprb('lblfm5','2','fm5i','" & fm5 & "');"" " & chkdf & ">&nbsp;Fail</td></tr>")
        End If
        'Else
        'sb.Append("<tr><td class=""plainlabelblue"">none</td></tr>")
        'End If

        sb.Append("</table>")
        tdfail.InnerHtml = sb.ToString

    End Sub
End Class
