<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmtime.aspx.vb" Inherits="lucy_r12.tpmtime" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmtime</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" style="POSITION: absolute; TOP: 0px; LEFT: 0px" cellSpacing="0"
				cellPadding="1">
				<tr id="trtime">
					<td id="tdtime" colSpan="2" runat="server"><asp:datagrid id="dgparts" runat="server" BackColor="Transparent" AutoGenerateColumns="False">
							<FooterStyle BackColor="Transparent"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle CssClass="prowtransblue  plainlabel"></AlternatingItemStyle>
							<ItemStyle CssClass="prowtrans  plainlabel"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Task Time">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ttime") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Start Time">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.starttime") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<table>
											<tr>
												<td>
													<asp:DropDownList id="ddhrs" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("hrs")) %>'>
														<asp:ListItem Value="NA">NA</asp:ListItem>
														<asp:ListItem Value="01">01</asp:ListItem>
														<asp:ListItem Value="02">02</asp:ListItem>
														<asp:ListItem Value="03">03</asp:ListItem>
														<asp:ListItem Value="04">04</asp:ListItem>
														<asp:ListItem Value="05">05</asp:ListItem>
														<asp:ListItem Value="06">06</asp:ListItem>
														<asp:ListItem Value="07">07</asp:ListItem>
														<asp:ListItem Value="08">08</asp:ListItem>
														<asp:ListItem Value="09">09</asp:ListItem>
														<asp:ListItem Value="10">10</asp:ListItem>
														<asp:ListItem Value="11">11</asp:ListItem>
														<asp:ListItem Value="12">12</asp:ListItem>
													</asp:DropDownList></td>
												<td class="bluelabel">
													:
												</td>
												<td>
													<asp:DropDownList id="ddmins" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexM(Container.DataItem("mins")) %>'>
														<asp:ListItem Value="00">00</asp:ListItem>
														<asp:ListItem Value="01">01</asp:ListItem>
														<asp:ListItem Value="02">02</asp:ListItem>
														<asp:ListItem Value="03">03</asp:ListItem>
														<asp:ListItem Value="04">04</asp:ListItem>
														<asp:ListItem Value="05">05</asp:ListItem>
														<asp:ListItem Value="06">06</asp:ListItem>
														<asp:ListItem Value="07">07</asp:ListItem>
														<asp:ListItem Value="08">08</asp:ListItem>
														<asp:ListItem Value="09">09</asp:ListItem>
														<asp:ListItem Value="10">10</asp:ListItem>
														<asp:ListItem Value="11">11</asp:ListItem>
														<asp:ListItem Value="12">12</asp:ListItem>
														<asp:ListItem Value="13">13</asp:ListItem>
														<asp:ListItem Value="14">14</asp:ListItem>
														<asp:ListItem Value="15">15</asp:ListItem>
														<asp:ListItem Value="16">16</asp:ListItem>
														<asp:ListItem Value="17">17</asp:ListItem>
														<asp:ListItem Value="18">18</asp:ListItem>
														<asp:ListItem Value="19">19</asp:ListItem>
														<asp:ListItem Value="20">20</asp:ListItem>
														<asp:ListItem Value="21">21</asp:ListItem>
														<asp:ListItem Value="22">22</asp:ListItem>
														<asp:ListItem Value="23">23</asp:ListItem>
														<asp:ListItem Value="24">24</asp:ListItem>
														<asp:ListItem Value="25">25</asp:ListItem>
														<asp:ListItem Value="26">26</asp:ListItem>
														<asp:ListItem Value="27">27</asp:ListItem>
														<asp:ListItem Value="28">28</asp:ListItem>
														<asp:ListItem Value="29">29</asp:ListItem>
														<asp:ListItem Value="30">30</asp:ListItem>
														<asp:ListItem Value="31">31</asp:ListItem>
														<asp:ListItem Value="32">32</asp:ListItem>
														<asp:ListItem Value="33">33</asp:ListItem>
														<asp:ListItem Value="34">34</asp:ListItem>
														<asp:ListItem Value="35">35</asp:ListItem>
														<asp:ListItem Value="36">36</asp:ListItem>
														<asp:ListItem Value="37">37</asp:ListItem>
														<asp:ListItem Value="38">38</asp:ListItem>
														<asp:ListItem Value="39">39</asp:ListItem>
														<asp:ListItem Value="40">40</asp:ListItem>
														<asp:ListItem Value="41">41</asp:ListItem>
														<asp:ListItem Value="42">42</asp:ListItem>
														<asp:ListItem Value="43">43</asp:ListItem>
														<asp:ListItem Value="44">44</asp:ListItem>
														<asp:ListItem Value="45">45</asp:ListItem>
														<asp:ListItem Value="46">46</asp:ListItem>
														<asp:ListItem Value="47">47</asp:ListItem>
														<asp:ListItem Value="48">48</asp:ListItem>
														<asp:ListItem Value="49">49</asp:ListItem>
														<asp:ListItem Value="50">50</asp:ListItem>
														<asp:ListItem Value="51">51</asp:ListItem>
														<asp:ListItem Value="52">52</asp:ListItem>
														<asp:ListItem Value="53">53</asp:ListItem>
														<asp:ListItem Value="54">54</asp:ListItem>
														<asp:ListItem Value="55">55</asp:ListItem>
														<asp:ListItem Value="56">56</asp:ListItem>
														<asp:ListItem Value="57">57</asp:ListItem>
														<asp:ListItem Value="58">58</asp:ListItem>
														<asp:ListItem Value="59">59</asp:ListItem>
													</asp:DropDownList></td>
												<td>
													<asp:DropDownList id="ddaps" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexAP(Container.DataItem("aps")) %>'>
														<asp:ListItem Value="AM">AM</asp:ListItem>
														<asp:ListItem Value="PM">PM</asp:ListItem>
													</asp:DropDownList></td>
											</tr>
										</table>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Stop Time">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label7" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.stoptime") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<table>
											<tr>
												<td>
													<asp:DropDownList id="ddhre" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("hre")) %>'>
														<asp:ListItem Value="NA">NA</asp:ListItem>
														<asp:ListItem Value="01">01</asp:ListItem>
														<asp:ListItem Value="02">02</asp:ListItem>
														<asp:ListItem Value="03">03</asp:ListItem>
														<asp:ListItem Value="04">04</asp:ListItem>
														<asp:ListItem Value="05">05</asp:ListItem>
														<asp:ListItem Value="06">06</asp:ListItem>
														<asp:ListItem Value="07">07</asp:ListItem>
														<asp:ListItem Value="08">08</asp:ListItem>
														<asp:ListItem Value="09">09</asp:ListItem>
														<asp:ListItem Value="10">10</asp:ListItem>
														<asp:ListItem Value="11">11</asp:ListItem>
														<asp:ListItem Value="12">12</asp:ListItem>
													</asp:DropDownList></td>
												<td class="bluelabel">
													:
												</td>
												<td>
													<asp:DropDownList id="ddmine" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexM(Container.DataItem("mine")) %>'>
														<asp:ListItem Value="00">00</asp:ListItem>
														<asp:ListItem Value="01">01</asp:ListItem>
														<asp:ListItem Value="02">02</asp:ListItem>
														<asp:ListItem Value="03">03</asp:ListItem>
														<asp:ListItem Value="04">04</asp:ListItem>
														<asp:ListItem Value="05">05</asp:ListItem>
														<asp:ListItem Value="06">06</asp:ListItem>
														<asp:ListItem Value="07">07</asp:ListItem>
														<asp:ListItem Value="08">08</asp:ListItem>
														<asp:ListItem Value="09">09</asp:ListItem>
														<asp:ListItem Value="10">10</asp:ListItem>
														<asp:ListItem Value="11">11</asp:ListItem>
														<asp:ListItem Value="12">12</asp:ListItem>
														<asp:ListItem Value="13">13</asp:ListItem>
														<asp:ListItem Value="14">14</asp:ListItem>
														<asp:ListItem Value="15">15</asp:ListItem>
														<asp:ListItem Value="16">16</asp:ListItem>
														<asp:ListItem Value="17">17</asp:ListItem>
														<asp:ListItem Value="18">18</asp:ListItem>
														<asp:ListItem Value="19">19</asp:ListItem>
														<asp:ListItem Value="20">20</asp:ListItem>
														<asp:ListItem Value="21">21</asp:ListItem>
														<asp:ListItem Value="22">22</asp:ListItem>
														<asp:ListItem Value="23">23</asp:ListItem>
														<asp:ListItem Value="24">24</asp:ListItem>
														<asp:ListItem Value="25">25</asp:ListItem>
														<asp:ListItem Value="26">26</asp:ListItem>
														<asp:ListItem Value="27">27</asp:ListItem>
														<asp:ListItem Value="28">28</asp:ListItem>
														<asp:ListItem Value="29">29</asp:ListItem>
														<asp:ListItem Value="30">30</asp:ListItem>
														<asp:ListItem Value="31">31</asp:ListItem>
														<asp:ListItem Value="32">32</asp:ListItem>
														<asp:ListItem Value="33">33</asp:ListItem>
														<asp:ListItem Value="34">34</asp:ListItem>
														<asp:ListItem Value="35">35</asp:ListItem>
														<asp:ListItem Value="36">36</asp:ListItem>
														<asp:ListItem Value="37">37</asp:ListItem>
														<asp:ListItem Value="38">38</asp:ListItem>
														<asp:ListItem Value="39">39</asp:ListItem>
														<asp:ListItem Value="40">40</asp:ListItem>
														<asp:ListItem Value="41">41</asp:ListItem>
														<asp:ListItem Value="42">42</asp:ListItem>
														<asp:ListItem Value="43">43</asp:ListItem>
														<asp:ListItem Value="44">44</asp:ListItem>
														<asp:ListItem Value="45">45</asp:ListItem>
														<asp:ListItem Value="46">46</asp:ListItem>
														<asp:ListItem Value="47">47</asp:ListItem>
														<asp:ListItem Value="48">48</asp:ListItem>
														<asp:ListItem Value="49">49</asp:ListItem>
														<asp:ListItem Value="50">50</asp:ListItem>
														<asp:ListItem Value="51">51</asp:ListItem>
														<asp:ListItem Value="52">52</asp:ListItem>
														<asp:ListItem Value="53">53</asp:ListItem>
														<asp:ListItem Value="54">54</asp:ListItem>
														<asp:ListItem Value="55">55</asp:ListItem>
														<asp:ListItem Value="56">56</asp:ListItem>
														<asp:ListItem Value="57">57</asp:ListItem>
														<asp:ListItem Value="58">58</asp:ListItem>
														<asp:ListItem Value="59">59</asp:ListItem>
													</asp:DropDownList></td>
												<td>
													<asp:DropDownList id="ddape" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexAP(Container.DataItem("ape")) %>'>
														<asp:ListItem Value="AM">AM</asp:ListItem>
														<asp:ListItem Value="PM">PM</asp:ListItem>
													</asp:DropDownList></td>
											</tr>
										</table>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Minutes">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.acttime") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtcostm" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.acttime") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="pmtid" Visible="False">
									<HeaderStyle Width="60px" CssClass="details"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label11" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblpmtidi" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr id="trtimed">
					<td id="tdtimed" colSpan="2" runat="server"><asp:datagrid id="dgparts2" runat="server" BackColor="Transparent" AutoGenerateColumns="False">
							<FooterStyle BackColor="Transparent"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle CssClass="prowtransblue  plainlabel"></AlternatingItemStyle>
							<ItemStyle CssClass="prowtrans  plainlabel"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Down Time">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblrecrd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rd") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Start Time">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label8" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.starttimedt") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<table>
											<tr>
												<td>
													<asp:DropDownList id="ddhrsd" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("hrsdt")) %>'>
														<asp:ListItem Value="NA">NA</asp:ListItem>
														<asp:ListItem Value="01">01</asp:ListItem>
														<asp:ListItem Value="02">02</asp:ListItem>
														<asp:ListItem Value="03">03</asp:ListItem>
														<asp:ListItem Value="04">04</asp:ListItem>
														<asp:ListItem Value="05">05</asp:ListItem>
														<asp:ListItem Value="06">06</asp:ListItem>
														<asp:ListItem Value="07">07</asp:ListItem>
														<asp:ListItem Value="08">08</asp:ListItem>
														<asp:ListItem Value="09">09</asp:ListItem>
														<asp:ListItem Value="10">10</asp:ListItem>
														<asp:ListItem Value="11">11</asp:ListItem>
														<asp:ListItem Value="12">12</asp:ListItem>
													</asp:DropDownList></td>
												<td class="bluelabel">
													:
												</td>
												<td>
													<asp:DropDownList id="ddminsd" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexM(Container.DataItem("minsdt")) %>'>
														<asp:ListItem Value="00">00</asp:ListItem>
														<asp:ListItem Value="01">01</asp:ListItem>
														<asp:ListItem Value="02">02</asp:ListItem>
														<asp:ListItem Value="03">03</asp:ListItem>
														<asp:ListItem Value="04">04</asp:ListItem>
														<asp:ListItem Value="05">05</asp:ListItem>
														<asp:ListItem Value="06">06</asp:ListItem>
														<asp:ListItem Value="07">07</asp:ListItem>
														<asp:ListItem Value="08">08</asp:ListItem>
														<asp:ListItem Value="09">09</asp:ListItem>
														<asp:ListItem Value="10">10</asp:ListItem>
														<asp:ListItem Value="11">11</asp:ListItem>
														<asp:ListItem Value="12">12</asp:ListItem>
														<asp:ListItem Value="13">13</asp:ListItem>
														<asp:ListItem Value="14">14</asp:ListItem>
														<asp:ListItem Value="15">15</asp:ListItem>
														<asp:ListItem Value="16">16</asp:ListItem>
														<asp:ListItem Value="17">17</asp:ListItem>
														<asp:ListItem Value="18">18</asp:ListItem>
														<asp:ListItem Value="19">19</asp:ListItem>
														<asp:ListItem Value="20">20</asp:ListItem>
														<asp:ListItem Value="21">21</asp:ListItem>
														<asp:ListItem Value="22">22</asp:ListItem>
														<asp:ListItem Value="23">23</asp:ListItem>
														<asp:ListItem Value="24">24</asp:ListItem>
														<asp:ListItem Value="25">25</asp:ListItem>
														<asp:ListItem Value="26">26</asp:ListItem>
														<asp:ListItem Value="27">27</asp:ListItem>
														<asp:ListItem Value="28">28</asp:ListItem>
														<asp:ListItem Value="29">29</asp:ListItem>
														<asp:ListItem Value="30">30</asp:ListItem>
														<asp:ListItem Value="31">31</asp:ListItem>
														<asp:ListItem Value="32">32</asp:ListItem>
														<asp:ListItem Value="33">33</asp:ListItem>
														<asp:ListItem Value="34">34</asp:ListItem>
														<asp:ListItem Value="35">35</asp:ListItem>
														<asp:ListItem Value="36">36</asp:ListItem>
														<asp:ListItem Value="37">37</asp:ListItem>
														<asp:ListItem Value="38">38</asp:ListItem>
														<asp:ListItem Value="39">39</asp:ListItem>
														<asp:ListItem Value="40">40</asp:ListItem>
														<asp:ListItem Value="41">41</asp:ListItem>
														<asp:ListItem Value="42">42</asp:ListItem>
														<asp:ListItem Value="43">43</asp:ListItem>
														<asp:ListItem Value="44">44</asp:ListItem>
														<asp:ListItem Value="45">45</asp:ListItem>
														<asp:ListItem Value="46">46</asp:ListItem>
														<asp:ListItem Value="47">47</asp:ListItem>
														<asp:ListItem Value="48">48</asp:ListItem>
														<asp:ListItem Value="49">49</asp:ListItem>
														<asp:ListItem Value="50">50</asp:ListItem>
														<asp:ListItem Value="51">51</asp:ListItem>
														<asp:ListItem Value="52">52</asp:ListItem>
														<asp:ListItem Value="53">53</asp:ListItem>
														<asp:ListItem Value="54">54</asp:ListItem>
														<asp:ListItem Value="55">55</asp:ListItem>
														<asp:ListItem Value="56">56</asp:ListItem>
														<asp:ListItem Value="57">57</asp:ListItem>
														<asp:ListItem Value="58">58</asp:ListItem>
														<asp:ListItem Value="59">59</asp:ListItem>
													</asp:DropDownList></td>
												<td>
													<asp:DropDownList id="ddapsd" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexAP(Container.DataItem("apsdt")) %>'>
														<asp:ListItem Value="AM">AM</asp:ListItem>
														<asp:ListItem Value="PM">PM</asp:ListItem>
													</asp:DropDownList></td>
											</tr>
										</table>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Stop Time">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label9" runat="server" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.stoptimedt") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<table>
											<tr>
												<td>
													<asp:DropDownList id="ddhred" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("hredt")) %>'>
														<asp:ListItem Value="NA">NA</asp:ListItem>
														<asp:ListItem Value="01">01</asp:ListItem>
														<asp:ListItem Value="02">02</asp:ListItem>
														<asp:ListItem Value="03">03</asp:ListItem>
														<asp:ListItem Value="04">04</asp:ListItem>
														<asp:ListItem Value="05">05</asp:ListItem>
														<asp:ListItem Value="06">06</asp:ListItem>
														<asp:ListItem Value="07">07</asp:ListItem>
														<asp:ListItem Value="08">08</asp:ListItem>
														<asp:ListItem Value="09">09</asp:ListItem>
														<asp:ListItem Value="10">10</asp:ListItem>
														<asp:ListItem Value="11">11</asp:ListItem>
														<asp:ListItem Value="12">12</asp:ListItem>
													</asp:DropDownList></td>
												<td class="bluelabel">
													:
												</td>
												<td>
													<asp:DropDownList id="ddmined" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexM(Container.DataItem("minedt")) %>'>
														<asp:ListItem Value="00">00</asp:ListItem>
														<asp:ListItem Value="01">01</asp:ListItem>
														<asp:ListItem Value="02">02</asp:ListItem>
														<asp:ListItem Value="03">03</asp:ListItem>
														<asp:ListItem Value="04">04</asp:ListItem>
														<asp:ListItem Value="05">05</asp:ListItem>
														<asp:ListItem Value="06">06</asp:ListItem>
														<asp:ListItem Value="07">07</asp:ListItem>
														<asp:ListItem Value="08">08</asp:ListItem>
														<asp:ListItem Value="09">09</asp:ListItem>
														<asp:ListItem Value="10">10</asp:ListItem>
														<asp:ListItem Value="11">11</asp:ListItem>
														<asp:ListItem Value="12">12</asp:ListItem>
														<asp:ListItem Value="13">13</asp:ListItem>
														<asp:ListItem Value="14">14</asp:ListItem>
														<asp:ListItem Value="15">15</asp:ListItem>
														<asp:ListItem Value="16">16</asp:ListItem>
														<asp:ListItem Value="17">17</asp:ListItem>
														<asp:ListItem Value="18">18</asp:ListItem>
														<asp:ListItem Value="19">19</asp:ListItem>
														<asp:ListItem Value="20">20</asp:ListItem>
														<asp:ListItem Value="21">21</asp:ListItem>
														<asp:ListItem Value="22">22</asp:ListItem>
														<asp:ListItem Value="23">23</asp:ListItem>
														<asp:ListItem Value="24">24</asp:ListItem>
														<asp:ListItem Value="25">25</asp:ListItem>
														<asp:ListItem Value="26">26</asp:ListItem>
														<asp:ListItem Value="27">27</asp:ListItem>
														<asp:ListItem Value="28">28</asp:ListItem>
														<asp:ListItem Value="29">29</asp:ListItem>
														<asp:ListItem Value="30">30</asp:ListItem>
														<asp:ListItem Value="31">31</asp:ListItem>
														<asp:ListItem Value="32">32</asp:ListItem>
														<asp:ListItem Value="33">33</asp:ListItem>
														<asp:ListItem Value="34">34</asp:ListItem>
														<asp:ListItem Value="35">35</asp:ListItem>
														<asp:ListItem Value="36">36</asp:ListItem>
														<asp:ListItem Value="37">37</asp:ListItem>
														<asp:ListItem Value="38">38</asp:ListItem>
														<asp:ListItem Value="39">39</asp:ListItem>
														<asp:ListItem Value="40">40</asp:ListItem>
														<asp:ListItem Value="41">41</asp:ListItem>
														<asp:ListItem Value="42">42</asp:ListItem>
														<asp:ListItem Value="43">43</asp:ListItem>
														<asp:ListItem Value="44">44</asp:ListItem>
														<asp:ListItem Value="45">45</asp:ListItem>
														<asp:ListItem Value="46">46</asp:ListItem>
														<asp:ListItem Value="47">47</asp:ListItem>
														<asp:ListItem Value="48">48</asp:ListItem>
														<asp:ListItem Value="49">49</asp:ListItem>
														<asp:ListItem Value="50">50</asp:ListItem>
														<asp:ListItem Value="51">51</asp:ListItem>
														<asp:ListItem Value="52">52</asp:ListItem>
														<asp:ListItem Value="53">53</asp:ListItem>
														<asp:ListItem Value="54">54</asp:ListItem>
														<asp:ListItem Value="55">55</asp:ListItem>
														<asp:ListItem Value="56">56</asp:ListItem>
														<asp:ListItem Value="57">57</asp:ListItem>
														<asp:ListItem Value="58">58</asp:ListItem>
														<asp:ListItem Value="59">59</asp:ListItem>
													</asp:DropDownList></td>
												<td>
													<asp:DropDownList id="ddaped" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndexAP(Container.DataItem("apedt")) %>'>
														<asp:ListItem Value="AM">AM</asp:ListItem>
														<asp:ListItem Value="PM">PM</asp:ListItem>
													</asp:DropDownList></td>
											</tr>
										</table>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Minutes">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label10" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.actrd") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtcostmd" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.actrd") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="pmtid" Visible="False">
									<HeaderStyle Width="60px" CssClass="details"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label12" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblpmtida" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.pmtid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lblwo" type="hidden" name="lblwo" runat="server"> <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
			<input id="lblstat" type="hidden" name="lblstat" runat="server"> <input id="lblpstr" type="hidden" name="lblpstr" runat="server">
			<input id="lbltstr" type="hidden" name="lbltstr" runat="server"> <input id="lbllstr" type="hidden" name="lbllstr" runat="server">
			<input id="lblupsav" type="hidden" name="lblupsav" runat="server"> <input id="xCoord" type="hidden" name="xCoord" runat="server">
			<input id="yCoord" type="hidden" name="yCoord" runat="server"> <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
			<input id="lblpmhid" type="hidden" name="lblpmhid" runat="server"><input id="lblro" type="hidden" name="lblro" runat="server">
			<input type="hidden" id="lbltasknum" runat="server"> <input type="hidden" id="lblts" runat="server">
			<input type="hidden" id="lbleqid" runat="server"> <input type="hidden" id="lblrd" runat="server">
			<input type="hidden" id="lbltyp" runat="server"> <input type="hidden" id="lblfreq" runat="server">
			<input type="hidden" id="lblacost" runat="server"> <input type="hidden" id="lbltentry" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
