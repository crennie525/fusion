

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tpmget
    Inherits System.Web.UI.Page
    Protected WithEvents ovid290 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang3578 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3577 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3576 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3575 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sql As String
    Dim sid, filt, dt, df, val, login, Filter As String
    Dim did, clid, lid, eq, tl, cid, eqid, ts, jump, tli As String
    Dim typ, fuid, coid, userid, islabor As String
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcleantasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbleqdesc As System.Web.UI.WebControls.Label
    Protected WithEvents eqdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgototasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblts As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbldeptdesc As System.Web.UI.WebControls.Label
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblcelldesc As System.Web.UI.WebControls.Label
    Protected WithEvents lblloc As System.Web.UI.WebControls.Label
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgsw As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents celldiv As System.Web.UI.HtmlControls.HtmlTableRow

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            userid = HttpContext.Current.Session("userid").ToString()
            islabor = HttpContext.Current.Session("islabor").ToString()
            lbluserid.Value = userid
            lblislabor.Value = islabor
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then

            lblgototasks.Value = "0"
            lblcleantasks.Value = "2"
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid

            tli = "5" 'Request.QueryString("tli").ToString
            lbltl.Value = tli

            tasks.Open()
            Try
                ts = Request.QueryString("ts").ToString
            Catch ex As Exception
                ts = "0"
            End Try
            lblts.Value = ts
            PopDepts(sid)
            jump = Request.QueryString("jump").ToString
            Dim deptcheck As Integer = 0
            Dim fuidcheck As Integer = 0
            If jump = "yes" Then
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                Dim eqcheck As Integer = 0
                If typ = "loc" Or typ = "dloc" Then
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    did = Request.QueryString("did").ToString
                    If did = "" Then
                        imgsw.Visible = True
                        dddepts.Enabled = False
                    Else
                        Try
                            dddepts.SelectedValue = did ' check
                            lbldept.Value = did
                            Filter = did
                            If ts = "1" Then
                                lblgetarch.Value = "yes"
                            End If

                        Catch ex As Exception
                            Try
                                dddepts.SelectedIndex = 0
                            Catch ex1 As Exception

                            End Try

                            deptcheck = 1
                        End Try
                        If deptcheck <> 1 Then
                            Dim cellchk As Integer = 0
                            Dim chk As String = CellCheck(Filter)
                            If chk = "no" Then
                                lblchk.Value = "no"
                            Else
                                lblchk.Value = "yes"
                                PopCells(did)
                                clid = Request.QueryString("clid").ToString
                                Try
                                    ddcells.SelectedValue = clid ' check
                                    lblclid.Value = clid
                                    lblpar.Value = "cell"
                                Catch ex As Exception
                                    Try
                                        ddcells.SelectedIndex = 0
                                    Catch ex1 As Exception

                                    End Try

                                    cellchk = 1
                                End Try
                            End If
                        End If
                    End If
                    PopEq(lid, "l")
                    eqid = Request.QueryString("eqid").ToString
                    Try
                        lbleqid.Value = eqid
                        ddeq.SelectedValue = eqid
                    Catch ex As Exception
                        Try
                            ddeq.SelectedIndex = 0
                        Catch ex1 As Exception

                        End Try

                        eqcheck = 1
                    End Try
                    If eqcheck <> 1 Then
                        Try
                            fuid = Request.QueryString("funid").ToString
                            lblfuid.Value = fuid
                        Catch ex As Exception
                            fuidcheck = 1
                            GoToTasks("yes")
                        End Try

                        If fuidcheck <> 1 Then
                            CheckTasks(fuid)
                            Try
                                coid = Request.QueryString("comid").ToString
                                lblcoid.Value = coid
                            Catch ex As Exception

                            End Try
                            If lblcoid.Value <> "" AndAlso lblcoid.Value <> "0" Then
                                lbltaskcnt.Value = "1"
                                'GoToTask(coid)
                                GoToTasks("yes")
                            Else
                                GoToTasks("yes")
                            End If
                        Else
                            lblgototasks.Value = "0"
                        End If

                    End If


                Else
                    imgsw.Visible = False
                    dddepts.Enabled = True
                    did = Request.QueryString("did").ToString
                    Try
                        dddepts.SelectedValue = did ' check
                        lbldept.Value = did
                        Filter = did
                        If ts = "1" Then
                            lblgetarch.Value = "yes"
                        End If

                    Catch ex As Exception
                        Try
                            dddepts.SelectedIndex = 0
                        Catch ex1 As Exception

                        End Try

                        deptcheck = 1
                    End Try
                    If deptcheck <> 1 Then
                        Dim cellchk As Integer = 0
                        Dim chk As String = CellCheck(Filter)
                        If chk = "no" Then
                            lblchk.Value = "no"
                        Else
                            lblchk.Value = "yes"
                            PopCells(did)
                            clid = Request.QueryString("clid").ToString
                            Try
                                ddcells.SelectedValue = clid ' check
                                lblclid.Value = clid
                                lblpar.Value = "cell"
                            Catch ex As Exception
                                Try
                                    ddcells.SelectedIndex = 0
                                Catch ex1 As Exception

                                End Try

                                cellchk = 1
                            End Try
                        End If
                        If cellchk <> 1 Then
                            'Dim eqcheck As Integer = 0
                            PopEq(did, "d")
                            eqid = Request.QueryString("eqid").ToString
                            Try
                                lbleqid.Value = eqid
                                ddeq.SelectedValue = eqid
                            Catch ex As Exception
                                Try
                                    ddeq.SelectedIndex = 0
                                Catch ex1 As Exception

                                End Try

                                eqcheck = 1
                            End Try
                            If tli <> "4" Then
                                If eqcheck <> 1 Then

                                    fuid = Request.QueryString("funid").ToString
                                    Try
                                        lblfuid.Value = fuid
                                    Catch ex As Exception
                                        fuidcheck = 1
                                        GoToTasks("yes")
                                    End Try

                                    If fuidcheck <> 1 Then
                                        CheckTasks(fuid)
                                        Dim coid As String
                                        Try
                                            coid = Request.QueryString("comid").ToString
                                            lblcoid.Value = coid
                                        Catch ex As Exception

                                        End Try
                                        If lblcoid.Value <> "" AndAlso lblcoid.Value <> "0" Then
                                            lbltaskcnt.Value = "1"
                                            'GoToTask(coid)
                                            GoToTasks("yes")
                                        Else
                                            GoToTasks("yes")
                                        End If
                                    Else
                                        lblgototasks.Value = "0"
                                    End If
                                Else
                                    GoToTasks()
                                End If
                            Else
                                lbltaskcnt.Value = "4"
                                GoToTasks()
                            End If
                        End If
                    End If
                End If

            End If
            tasks.Dispose()
        Else
            If Request.Form("lblpchk") = "loc" Then
                lblpchk.Value = ""
                tasks.Open()
                lid = lbllid.Value
                PopEq(lid, "l")
                tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub GoToTask(ByVal coid As String)
        Dim tn As Integer
        Dim tasknum, chk As String
        lblcoid.Value = coid
        tl = "5" 'lbltasklev.Value
        cid = "0" 'lblcid.Value
        sid = lblsid.Value
        did = lbldept.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        'sql = "select top(1) tasknum from pmtasks where funcid = '" & fuid & "' and comid = '" & coid & "'"
        'tn = tasks.Scalar(sql)
        'tasknum = tn
        lbltaskcnt.Value = "1"
        lblgototasks.Value = "1"
        lblcleantasks.Value = "0"
        'Response.Redirect("PMTaskDivFuncGrid.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
        'cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        '"&eqid=" & eqid & "&task=" & tasknum & "&comid=" & coid)
    End Sub
    Private Sub CheckTasks(ByVal fu As String)
        sql = "select count(*) from pmtaskstpm where funcid = '" & fu & "'"
        Dim cnt As Integer
        cnt = tasks.Scalar(sql)
        If cnt = 0 Then
            lbltaskcnt.Value = "0"
        Else
            lbltaskcnt.Value = "1"
        End If
    End Sub
    Private Sub PopDepts(ByVal site As String)
        userid = lbluserid.Value
        islabor = lblislabor.Value
        Dim lim As String = "1"

        filt = " where siteid = '" & site & "'"
        sql = "select count(*) from Dept where siteid = '" & sid & "'"
        ts = lblts.Value
        If ts = "1" Then
            sql += " and dept_id in (select distinct deptid from pmtaskstpm)"
            filt += " and dept_id in (select distinct deptid from pmtaskstpm)"
        Else
            If lim = "1" And islabor = "1" Then
                sql += " and dept_id in (select distinct dept_id from equipment where eqid in (select distinct eqid from tpm " _
                + "where eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "')))"
                filt += " and dept_id in (select distinct dept_id from equipment where eqid in (select distinct eqid from tpm " _
                + "where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "')))"
            Else
                sql += " and dept_id in (select distinct dept_id from equipment where eqid in (select distinct eqid from tpm where nextdate is not null))"
                filt += " and dept_id in (select distinct dept_id from equipment where eqid in (select distinct eqid from tpm where nextdate is not null))"
            End If

        End If
        Dim dcnt As Integer = tasks.Scalar(sql)
        If dcnt > 0 Then
            dt = "Dept"
            val = "dept_id , dept_line "
            dr = tasks.GetList(dt, val, filt)
            dddepts.DataSource = dr
            dddepts.DataTextField = "dept_line"
            dddepts.DataValueField = "dept_id"
            dddepts.DataBind()
            dr.Close()
            dddepts.Enabled = True
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1709", "tpmget.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            dddepts.Enabled = False
        End If
        dddepts.Items.Insert(0, "Select Department")
    End Sub
    Private Sub PopCells(ByVal dept As String)
        userid = lbluserid.Value
        islabor = lblislabor.Value
        Dim lim As String = "1"

        filt = " where dept_id = " & dept
        ts = lblts.Value
        If ts = "1" Then
            sql += " and cellid in (select distinct cellid from pmtaskstpm)"
            filt += " and cellid in (select distinct cellid from pmtaskstpm)"
        Else
            If lim = "1" And islabor = "1" Then
                sql += " and cellid in (select distinct cellid from equipment where eqid in (select distinct eqid from tpm " _
                + "where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "')))"
                filt += " and cellid in (select distinct cellid from equipment where eqid in (select distinct eqid from tpm " _
                + "where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "')))"
            Else
                sql += " and cellid in (select distinct cellid from equipment where eqid in (select distinct eqid from tpm where nextdate is not null))"
                filt += " and cellid in (select distinct cellid from equipment where eqid in (select distinct eqid from tpm where nextdate is not null))"
            End If

        End If
        dt = "Cells"
        val = "cellid , cell_name "
        dr = tasks.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
        celldiv.Style.Add("display", "block")
        celldiv.Style.Add("visibility", "visible")
    End Sub
    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        If dddepts.SelectedIndex <> 0 And dddepts.SelectedIndex <> -1 Then
            did = dddepts.SelectedValue.ToString
            Dim dept As String = dddepts.SelectedValue.ToString
            tasks.Open()
            If dddepts.SelectedIndex <> 0 And dddepts.SelectedIndex <> -1 Then
                celldiv.Style.Add("display", "none")
                celldiv.Style.Add("visibility", "hidden")
                eqdiv.Style.Add("display", "none")
                eqdiv.Style.Add("visibility", "hidden")
                CheckTyp(dept)
                lbldeptdesc.Text = ""
                Dim deptname As String = dddepts.SelectedItem.ToString
                lbldept.Value = dept
                lblpar.Value = "dept"
                dt = "dept"
                df = "dept_desc"
                filt = " where dept_id = " & dept
                Try
                    dr = tasks.GetDesc(dt, df, filt)
                    If dr.Read Then
                        lbldeptdesc.Text = dr.Item("dept_desc").ToString
                    End If
                    dr.Close()
                Catch ex As Exception
                    lbldeptdesc.Text = "1 select " & df & " from " & dt & filt
                End Try
                Filter = dept
                Dim chk As String = CellCheck(Filter)
                If chk = "no" Then
                    lblclid.Value = "0"
                    lblgetarch.Value = "yes"
                    lblchk.Value = "no"
                    eqdiv.Style.Add("display", "none")
                    eqdiv.Style.Add("visibility", "hidden")
                    PopEq(dept, "d")
                Else
                    lblchk.Value = "yes"
                    lblpar2.Value = "chk"
                    PopCells(dept)
                End If
            Else
                celldiv.Style.Add("display", "none")
                celldiv.Style.Add("visibility", "hidden")
                lbldeptdesc.Text = ""
            End If
            tasks.Dispose()
        End If
    End Sub
    Private Sub CheckTPMs(ByVal typ As String)
        Dim tchk As Integer
        If typ = "dept" Then
            did = lbldept.Value
            sql = "select count(*) from tpm where eqid in (select eqid from from equipment where dept_id = '" & did & "'"
        ElseIf typ = "cell" Then

        ElseIf typ = "loc" Then

        End If
    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = tasks.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = tasks.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
    Private Sub CheckTyp(ByVal dept As String, Optional ByVal loc As String = "N")
        Dim typ As String
        Dim cnt As Integer
        If loc = "N" Then
            sql = "select count(locid) from equipment where dept_id = '" & dept & "' and locid <> 0 and locid is not null"
            cnt = tasks.Scalar(sql)
            If cnt = 0 Then
                lbltyp.Value = "reg"
            Else
                lbltyp.Value = "dloc"
            End If
        Else
            If dept = "" Then
                lbltyp.Value = "loc"
            Else
                sql = "select count(dept_id) from equipment where locid = '" & loc & "' and dept_id <> 0 and dept_id is not null"
                cnt = tasks.Scalar(sql)
                If cnt = 0 Then
                    lbltyp.Value = "loc"
                Else
                    lbltyp.Value = "dloc"
                End If
            End If
        End If

    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        Dim cell As String = ddcells.SelectedValue.ToString
        If ddcells.SelectedIndex <> 0 Then
            Try
                tasks.Open()
                lblclid.Value = cell
                lblpar.Value = "cell"
                dt = "cells"
                df = "cell_desc"
                filt = " where cellid = '" & cell & "'"
                dr = tasks.GetDesc(dt, df, filt)
                If dr.Read Then
                    lblcelldesc.Text = dr.Item("cell_desc").ToString
                End If
                dr.Close()
                lblgetarch.Value = "yes"
                PopEq(cell, "c")
                tasks.Dispose()
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1710", "tpmget.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
            lblcleantasks.Value = "0"
            lbltaskcnt.Value = "3"
            CleanTasks()
        Else
            eqdiv.Style.Add("display", "none")
            eqdiv.Style.Add("visibility", "hidden")
            lblcleantasks.Value = "0"
            lbltaskcnt.Value = "2"
            CleanTasks()
        End If
    End Sub
    Private Sub CleanTasks()
        Dim tskchk As String = lblcleantasks.Value
        If tskchk = "0" Then
            lblcleantasks.Value = "1"
            lblgototasks.Value = "1"
        End If
    End Sub
    Private Sub PopEq(ByVal var As String, ByVal typ As String)
        userid = lbluserid.Value
        islabor = lblislabor.Value
        Dim lim As String = "1"

        'eqid in (select distinct eqid from tpm " _
        '+ "where eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))
        lid = lbllid.Value
        did = lbldept.Value
        clid = lblclid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                CheckTyp(did)
                sql = "select count(*) from equipment where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
                ts = lblts.Value
                If ts = "1" Then
                    sql += " and eqid in (select distinct eqid from pmtaskstpm)"
                    filt += " and eqid in (select distinct eqid from pmtaskstpm)"
                Else
                    If lim = "1" And islabor = "1" Then
                        sql += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                        filt += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                    Else
                        sql += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                        filt += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                    End If

                End If
            Else
                CheckTyp(did, lid)
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
                ts = lblts.Value
                ts = lblts.Value
                If ts = "1" Then
                    sql += " and eqid in (select distinct eqid from pmtaskstpm)"
                    filt += " and eqid in (select distinct eqid from pmtaskstpm)"
                Else
                    If lim = "1" And islabor = "1" Then
                        sql += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                        filt += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                    Else
                        sql += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                        filt += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                    End If
                End If
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                CheckTyp(did)
                sql = "select count(*) from equipment where cellid = '" & clid & "'"
                filt = " where cellid = '" & clid & "'"
                ts = lblts.Value
                ts = lblts.Value
                If ts = "1" Then
                    sql += " and eqid in (select distinct eqid from pmtaskstpm)"
                    filt += " and eqid in (select distinct eqid from pmtaskstpm)"
                Else
                    If lim = "1" And islabor = "1" Then
                        sql += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                        filt += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                    Else
                        sql += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                        filt += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                    End If
                End If
            Else
                CheckTyp(did, lid)
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
                ts = lblts.Value
                ts = lblts.Value
                If ts = "1" Then
                    sql += " and eqid in (select distinct eqid from pmtaskstpm)"
                    filt += " and eqid in (select distinct eqid from pmtaskstpm)"
                Else
                    If lim = "1" And islabor = "1" Then
                        sql += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                        filt += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                    Else
                        sql += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                        filt += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                    End If
                End If
            End If

        ElseIf typ = "l" Then
            CheckTyp(did, lid)
            If clid <> "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from equipment where locid = '" & lid & "'"
                filt = " where locid = '" & lid & "'"
                celldiv.Style.Add("display", "none")
                celldiv.Style.Add("visibility", "hidden")
                Try
                    dddepts.SelectedIndex = 0
                Catch ex As Exception

                End Try

                lbldeptdesc.Text = ""
            End If
            ts = lblts.Value
            ts = lblts.Value
            If ts = "1" Then
                sql += " and eqid in (select distinct eqid from pmtaskstpm)"
                filt += " and eqid in (select distinct eqid from pmtaskstpm)"
            Else
                If lim = "1" And islabor = "1" Then
                    sql += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                    filt += " and eqid in (select distinct eqid from tpm where nextdate is not null and eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "'))"
                Else
                    sql += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                    filt += " and eqid in (select distinct eqid from tpm where nextdate is not null)"
                End If
            End If
            '
            'ddcells.SelectedIndex = 0
        End If
        eqcnt = tasks.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "

            dr = tasks.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            eqdiv.Style.Add("display", "block")
            eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = True
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1711", "tpmget.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            eqdiv.Style.Add("display", "block")
            eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = False
        End If
        GetLoc()
        lbltaskcnt.Value = "2"
        lblcleantasks.Value = "0"
        CleanTasks()
    End Sub
    Private Sub GetLoc()
        lid = lbllid.Value
        If lid <> "" And lid <> "0" Then
            sql = "select location, description from pmlocations where locid = '" & lid & "'"
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lblloc.Text = dr.Item("location").ToString
            End While
            dr.Close()
        End If

    End Sub

    Private Sub ddeq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddeq.SelectedIndexChanged
        eq = ddeq.SelectedValue.ToString
        lbleqid.Value = eq
        lblpar.Value = "eq"

        If ddeq.SelectedIndex <> 0 Then
            Try
                tasks.Open()
                GoToTasks()
                tasks.Dispose()
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1712", "tpmget.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        End If
        lblcleantasks.Value = "0"
        lbltaskcnt.Value = "1"
        CleanTasks()
    End Sub
    Private Sub GoToTasks(Optional ByVal jump As String = "no")
        tl = "5" 'lbltl.Value
        cid = "0" 'lblcid.Value
        sid = lblsid.Value
        did = lbldept.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        Dim chk As String
        chk = lblchk.Value
        lblgototasks.Value = "1"
        lbltaskcnt.Value = "1"
        lblcleantasks.Value = "0"
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3575.Text = axlabs.GetASPXPage("tpmget.aspx", "lang3575")
        Catch ex As Exception
        End Try
        Try
            lang3576.Text = axlabs.GetASPXPage("tpmget.aspx", "lang3576")
        Catch ex As Exception
        End Try
        Try
            lang3577.Text = axlabs.GetASPXPage("tpmget.aspx", "lang3577")
        Catch ex As Exception
        End Try
        Try
            lang3578.Text = axlabs.GetASPXPage("tpmget.aspx", "lang3578")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgsw.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmget.aspx", "imgsw") & "')")
            imgsw.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid290.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmget.aspx", "ovid290") & "')")
            ovid290.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
