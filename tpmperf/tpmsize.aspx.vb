

'********************************************************
'*
'********************************************************



Public Class tpmsize
    Inherits System.Web.UI.Page
	Protected WithEvents lang3610 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3609 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3608 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3607 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3606 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3605 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents lblbpage As System.Web.UI.WebControls.Label
    Protected WithEvents tdtpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdpline1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bfirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bnext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents blast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgsub As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3605.Text = axlabs.GetASPXPage("tpmsize.aspx","lang3605")
		Catch ex As Exception
		End Try
		Try
			lang3606.Text = axlabs.GetASPXPage("tpmsize.aspx","lang3606")
		Catch ex As Exception
		End Try
		Try
			lang3607.Text = axlabs.GetASPXPage("tpmsize.aspx","lang3607")
		Catch ex As Exception
		End Try
		Try
			lang3608.Text = axlabs.GetASPXPage("tpmsize.aspx","lang3608")
		Catch ex As Exception
		End Try
		Try
			lang3609.Text = axlabs.GetASPXPage("tpmsize.aspx","lang3609")
		Catch ex As Exception
		End Try
		Try
			lang3610.Text = axlabs.GetASPXPage("tpmsize.aspx","lang3610")
		Catch ex As Exception
		End Try
		Try
			lblbpage.Text = axlabs.GetASPXPage("tpmsize.aspx","lblbpage")
		Catch ex As Exception
		End Try
		Try
			lblpg.Text = axlabs.GetASPXPage("tpmsize.aspx","lblpg")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			imgsub.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("tpmsize.aspx","imgsub") & "')")
			imgsub.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
