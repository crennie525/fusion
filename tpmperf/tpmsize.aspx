<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmsize.aspx.vb" Inherits="lucy_r12.tpmsize" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmsize</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/tpmimgtasknav.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/tpmsizeaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" cellpadding="0" width="1026">
				<tr>
					<td class="thdrsinglft" align="left" width="20"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
					<td class="thdrsingrt label" width="380"><asp:Label id="lang3605" runat="server">Task Activity Details</asp:Label></td>
					<td width="5">&nbsp;</td>
					<td class="thdrsinglft" align="left" width="20"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
					<td class="thdrsingrt label" width="600"><asp:Label id="lang3606" runat="server">Task Images</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<table cellSpacing="0" cellpadding="0">
							<tr height="20">
								<td class="label"><asp:Label id="lang3607" runat="server">Current TPM</asp:Label></td>
								<td class="plainlabelblue" id="tdtpm" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang3608" runat="server">Current Function</asp:Label></td>
								<td class="plainlabelblue" id="tdfunc" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang3609" runat="server">Current Component</asp:Label></td>
								<td class="plainlabelblue" id="tdcomp" runat="server"></td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" colSpan="2" rowSpan="15">
						<table width="571" cellpadding="0" cellspacing="0">
							<tr height="20">
								<td align="center" colSpan="3">
									<div style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 600px; PADDING-TOP: 10px; BORDER-BOTTOM: blue 1px solid; HEIGHT: 502px"><A href="#"><IMG id="imgeq" src="../images/appimages/tpmimg1.gif" border="0" runat="server"></A></div>
								</td>
							</tr>
							<tr>
								<td class="label" id="tdpline1" align="center" colSpan="3" runat="server"></td>
							</tr>
							<tr>
								<td align="center" colSpan="3">
									<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="134"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr vAlign="middle" height="32">
					<td align="center" colSpan="2">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bfirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblbpage" runat="server" CssClass="bluelabel">Task 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bnext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="blast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="20">
					<td class="btmmenu label" vAlign="top" colSpan="2"><asp:Label id="lang3610" runat="server">Task Description</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<table cellSpacing="0" cellpadding="0">
							<tr>
								<td><asp:textbox id="txtdesc" ReadOnly="True" Height="100px" cssclass="plainlabel" Runat="server"
										Rows="3" MaxLength="1000" TextMode="MultiLine" Width="360px"></asp:textbox></td>
								<td vAlign="top"><IMG id="imgsub" onmouseover="return overlib('View Sub-Tasks for this Task')" onclick="getsubdiv();"
										onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif" runat="server">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
