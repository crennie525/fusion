<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmmeas.aspx.vb" Inherits="lucy_r12.tpmmeas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmmeas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts1/tpmmeasaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" style="LEFT: 0px; POSITION: absolute; TOP: 0px">
				<tr>
					<td><asp:datagrid id="dgmeas" runat="server" ShowFooter="True" CellPadding="0" GridLines="None" AllowPaging="True"
							AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1" BackColor="transparent">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle cssclass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle cssclass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" Height="18px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Type">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblmtype runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label3" runat="server" Width="90px" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hi">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lblhi" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label6" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.hi") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Lo">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lbllo" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label8" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.lo") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Spec">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lblspec" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label10" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.spec") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Measurement">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.measurement2") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtmeas" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.measurement2") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id=lblpmtskid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblpmtskida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id="lbltmdid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lbltmdida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tmdid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
			</table>
			<div class="details" id="rtdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 370px; BORDER-BOTTOM: black 1px solid; HEIGHT: 180px">
				<table cellSpacing="0" cellPadding="0" width="370" bgColor="white">
					<tr bgColor="blue" height="20">
						<td class="whitelabel"><asp:Label id="lang3593" runat="server">Work Order Details</asp:Label></td>
						<td align="right"><IMG onclick="closert();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr class="details" id="trrt" height="180">
						<td class="bluelabelb" vAlign="middle" align="center" colSpan="2"><asp:Label id="lang3594" runat="server">Moving Window...</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="2">
							<table>
								<tr height="20">
									<td width="20"></td>
									<td width="20"></td>
									<td width="120" class="bluelabel"><asp:Label id="lang3595" runat="server">Skill Required</asp:Label></td>
									<td colSpan="2"><asp:dropdownlist id="ddskill" runat="server" Width="160px" cssclass="plainlabel"></asp:dropdownlist></td>
									<td width="50"></td>
								</tr>
								<tr height="20">
									<td><INPUT id="cbsupe" type="checkbox" runat="server" NAME="cbsupe"></td>
									<td><IMG onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
											src="../images/appbuttons/minibuttons/emailcb.gif"></td>
									<td class="bluelabel"><asp:Label id="lang3596" runat="server">Supervisor</asp:Label></td>
									<td width="140"><asp:textbox id="txtsup" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:textbox></td>
									<td width="20"><IMG onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
											border="0"></td>
									<td></td>
								</tr>
								<tr height="20">
									<td><INPUT id="cbleade" type="checkbox" runat="server" NAME="cbleade"></td>
									<td><IMG onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
											src="../images/appbuttons/minibuttons/emailcb.gif"></td>
									<td class="bluelabel"><asp:Label id="lang3597" runat="server">Lead Craft</asp:Label></td>
									<td><asp:textbox id="txtlead" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:textbox></td>
									<td><IMG onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
											border="0"></td>
									<td></td>
								</tr>
								<tr height="20">
									<td></td>
									<td></td>
									<td class="bluelabel"><asp:Label id="lang3598" runat="server">Work Type</asp:Label></td>
									<td colSpan="2"><asp:dropdownlist id="ddwt" runat="server" CssClass="plainlabel">
											<asp:ListItem Value="CM" Selected="True">Corrective Maintenance</asp:ListItem>
											<asp:ListItem Value="EM">Emergency Maintenance</asp:ListItem>
										</asp:dropdownlist></td>
									<td></td>
								</tr>
								<tr height="20">
									<td></td>
									<td></td>
									<td class="bluelabel"><asp:Label id="lang3599" runat="server">Target Start</asp:Label></td>
									<td><asp:textbox id="txtstart" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:textbox></td>
									<td><IMG onclick="getcal('tstart');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
											width="19"></td>
									<td></td>
								</tr>
								<tr height="20">
									<td colspan="6" class="bluelabel"><asp:Label id="lang3600" runat="server">Problem Description</asp:Label></td>
								</tr>
								<tr>
									<td colspan="6">
										<asp:TextBox id="txtprob" runat="server" Width="380px" TextMode="MultiLine" Height="50px" CssClass="plainlabel"></asp:TextBox></td>
								</tr>
								<tr height="20">
									<td colspan="6" class="bluelabel"><asp:Label id="lang3601" runat="server">Corrective Action</asp:Label></td>
								</tr>
								<tr>
									<td colspan="6">
										<asp:TextBox id="txtcorr" runat="server" Width="380px" TextMode="MultiLine" Height="50px" CssClass="plainlabel"></asp:TextBox></td>
								</tr>
								<tr height="30">
									<td align="right" colSpan="6"><asp:imagebutton id="btnwo" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
								</tr>
								<tr>
									<td colSpan="6">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<input id="xCoord" type="hidden" name="xCoord" runat="server"> <input id="yCoord" type="hidden" name="yCoord" runat="server">
			<input id="lbltid" type="hidden" runat="server" NAME="lbltid"> <input type="hidden" id="lblmcnt" runat="server" NAME="lblmcnt">
			<input type="hidden" id="lblmup" runat="server" NAME="lblmup"> <input type="hidden" id="lblpmid" runat="server" NAME="lblpmid">
			<input type="hidden" id="lblpmtid" runat="server" NAME="lblpmtid"> <input type="hidden" id="lbltmid" runat="server" NAME="lbltmid"><input type="hidden" id="lblcid" runat="server" NAME="lblcid">
			<input type="hidden" id="lblsup" runat="server" NAME="lblsup"> <input type="hidden" id="lbllead" runat="server" NAME="lbllead">
			<input type="hidden" id="lblwo" runat="server" NAME="lblwo"><input type="hidden" id="lblcomid" runat="server" NAME="lblcomid">
			<input type="hidden" id="lblro" runat="server" NAME="lblro"><input type="hidden" id="lblpmtskidin" runat="server">
			<input type="hidden" id="lbldir" runat="server"><input type="hidden" id="lblmid" runat="server">
			<input type="hidden" id="lblshift" runat="server"> <input id="lbldragid" type="hidden" name="lbldragid" runat="server"><input id="lblrowid" type="hidden" name="lblrowid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
