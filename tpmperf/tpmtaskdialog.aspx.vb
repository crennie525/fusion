

'********************************************************
'*
'********************************************************



Public Class tpmtaskdialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pmid, pmhid, wonum, ts, sid As String
    Dim eqid, freq, rd, typ, tasknum As String
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iftask As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            ts = Request.QueryString("ts").ToString
            If ts = "0" Then
                pmid = Request.QueryString("pmid").ToString
                pmhid = Request.QueryString("pmhid").ToString
                wonum = Request.QueryString("wonum").ToString
                sid = Request.QueryString("sid").ToString
                iftask.Attributes.Add("src", "tpmtask.aspx?ts=0&pmid=" + pmid + "&wonum=" + wonum + "&pmhid=" + pmhid + "&sid=" + sid + "&date=" + Now)

            Else
                eqid = Request.QueryString("eqid").ToString '"275" '
                freq = Request.QueryString("freq").ToString '"7" '
                rd = Request.QueryString("rd").ToString '"Running" '
                typ = Request.QueryString("typ").ToString '"2" '
                sid = Request.QueryString("sid").ToString
                iftask.Attributes.Add("src", "tpmtask.aspx?ts=1&eqid=" + eqid + "&typ=" + typ + "&rd=" + rd + "&freq=" + freq + "&sid=" + sid + "&date=" + Now)

            End If
        End If
    End Sub

End Class
