<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmtools.aspx.vb" Inherits="lucy_r12.tpmtools" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmtools</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts1/tpmtoolsaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" style="LEFT: 0px; POSITION: absolute; TOP: 0px">
				<tr>
					<td><asp:datagrid id="dgparts" runat="server" BackColor="transparent" AutoGenerateColumns="False">
							<FooterStyle BackColor="Transparent"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle CssClass="ptransrow"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="imgeditfm" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Task#" Visible="False">
									<HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label3" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Item#">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label2" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Qty">
									<HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label7" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:label id="lblqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
										</asp:label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Used">
									<HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label11" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.used") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:dropdownlist id="ddus" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("used")) %>'>
											<asp:ListItem Value="Y">Yes</asp:ListItem>
											<asp:ListItem Value="N">No</asp:ListItem>
										</asp:dropdownlist>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Qty Used">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label14" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qtyused") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id="txtqty" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.qtyused") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Task#">
									<HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label13" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.wotskpartid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblpid" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.wotskpartid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False" HeaderText="Task#">
									<HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label12" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblitemid" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
                                 <asp:TemplateColumn Visible="False" HeaderText="Task#">
									<HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lblcost" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="txtcost" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input type="hidden" id="lblpmid" runat="server" NAME="lblpmid"> <input type="hidden" id="lblwo" runat="server" NAME="lblwo">
			<input id="xCoord" type="hidden" name="xCoord" runat="server"> <input id="yCoord" type="hidden" name="yCoord" runat="server">
			<input id="lblview" type="hidden" runat="server" NAME="lblview"><input id="lblstat" type="hidden" runat="server" NAME="lblstat">
			<input type="hidden" id="lblro" runat="server" NAME="lblro"> <input type="hidden" id="lbldir" runat="server">
			<input type="hidden" id="lblpmtskid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
