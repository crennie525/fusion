

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tpmmeas
    Inherits System.Web.UI.Page
	Protected WithEvents ovid296 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid295 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3601 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3600 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3599 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3598 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3597 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3596 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3595 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3594 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3593 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim meas As New Utilities
    Dim pmtskid, pmid, cid, pmtid, ro, ts, mid, shift As String
    Protected WithEvents lbldir As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshift As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldragid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrowid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskidin As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgmeas As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddwt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtprob As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcorr As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnwo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cbsupe As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbleade As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()

        'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = "0"
            ts = Request.QueryString("ts").ToString
            lbldir.Value = ts

            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try

            lblro.Value = ro
            If ro = "1" Then
                dgmeas.Columns(0).Visible = False
                btnwo.ImageUrl = "../images/appbuttons/bgbuttons/submitdis.gif"
                btnwo.Enabled = False
            End If
            Try
                mid = Request.QueryString("mid").ToString
            Catch ex As Exception
                mid = 0
            End Try
            lblmid.Value = mid
            If ts = "0" Then
                pmid = Request.QueryString("pmid").ToString '"262" '
                lblpmid.Value = pmid
            End If

            pmtskid = Request.QueryString("pmtskid").ToString
            lblpmtskidin.Value = pmtskid
            Try
                shift = Request.QueryString("shift").ToString
                lblshift.Value = shift
            Catch ex As Exception

            End Try

            meas.Open()
            'GetLists()
            GetMeas(pmid, mid)
            meas.Dispose()

        End If
    End Sub
    Private Sub GetMeas(ByVal pmid As String, Optional ByVal mid As String = "0")
        pmtskid = lblpmtskidin.Value
        shift = lblshift.Value
        Dim mcnt As Integer
        ts = lbldir.Value
        If ts = "0" Then
            sql = "select count(*) from pmTaskMeasDetMantpm where pmid = '" & pmid & "' and pmtskid = '" & pmtskid & "' and shift = '" & shift & "'"
            mcnt = meas.Scalar(sql)
            lblmcnt.Value = mcnt

            sql = "select distinct m.*, t.tasknum, t.taskdesc, " _
            + "cnt = (select count(*) from pmTaskMeasDetManHisttpm where pmid = '" & pmid & "' and pmtskid = '" & pmtskid & "') " _
            + "from pmTaskMeasDetMantpm m " _
            + "left join pmtracktpm t on t.pmtskid = m.pmtskid " _
            + "where m.pmid = '" & pmid & "' and m.pmtskid = '" & pmtskid & "' and m.shift = '" & shift & "'"
        Else
            sql = "select m.tmdid, m.hi, m.lo, m.spec, m.type, '' as measurement, t.tasknum, t.pmtskid from pmtaskmeasdetailstpm m left join pmtaskstpm t on t.pmtskid = m.pmtskid " _
            + "where m.pmtskid = '" & pmtskid & "'"
        End If


        dr = meas.GetRdrData(sql)
        dgmeas.DataSource = dr
        dgmeas.DataBind()


    End Sub

    Private Sub dgmeas_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.CancelCommand
        dgmeas.EditItemIndex = -1
        pmid = lblpmid.Value
        meas.Open()
        GetMeas(pmid)
        meas.Dispose()
    End Sub

    Private Sub dgmeas_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.EditCommand
        dgmeas.EditItemIndex = e.Item.ItemIndex
        pmid = lblpmid.Value
        meas.Open()
        GetMeas(pmid)
        meas.Dispose()
    End Sub

    Private Sub dgmeas_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeas.UpdateCommand
        ts = lbldir.Value
        If ts = "0" Then
            'ts = should be "0" for production mode
            Dim mup As Integer
            Dim hi, lo As String
            Dim mstr As String = CType(e.Item.FindControl("txtmeas"), TextBox).Text
            Dim tmdid As String = CType(e.Item.FindControl("lbltmdida"), Label).Text '
            pmtskid = CType(e.Item.FindControl("lblpmtskida"), Label).Text
            Dim txtchk As Long
            Try
                txtchk = System.Convert.ToDecimal(mstr)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1715" , "tpmmeas.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If mstr <> "" Then
                shift = lblshift.Value
                'pmtskid = lbltid.Value
                'sql = "update pmTaskMeasDetMan set measurement = '" & mstr & "' where pmtskid = '" & pmtskid & "'"
                sql = "usp_upmeastpm '" & tmdid & "', '" & pmtskid & "', '" & mstr & "','" & shift & "'"
                meas.Open()
                'meas.Update(sql)
                'sql = "select count(*) from pmTaskMeasDetManHist where pmtskid = '" & pmtskid & "' and measurement is not null"
                'mup = meas.Scalar(sql)
                dr = meas.GetRdrData(sql)
                While dr.Read
                    mup = dr.Item("mup").ToString
                    hi = dr.Item("hi").ToString
                    mup = dr.Item("lo").ToString
                End While
                dr.Close()
                lblmup.Value = mup
                dgmeas.EditItemIndex = -1
                pmid = lblpmid.Value
                pmtid = lblpmtid.Value
                GetMeas(pmid) 'pmtskid
                meas.Dispose()
                'typ, pmid, pmtskid, pmtid, tmdid
                Dim mail As New pmmail
                If hi = "1" Or lo = "1" Then
                    'typ, pmid, pmtskid, pmtid, tmdid
                    mail.CheckIt("tpmmeas", pmid, pmtskid, pmtid, tmdid)
                End If
            End If

        End If

    End Sub

    Private Sub dgmeas_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgmeas.ItemDataBound
        Dim mid As String = lblmid.Value
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim tmdid As String = DataBinder.Eval(e.Item.DataItem, "tmdid").ToString
            If tmdid = mid Then
                Dim ml As Label = CType(e.Item.FindControl("lblmtype"), Label)
                ml.CssClass = "plainlabelred"
                Dim lo As Label = CType(e.Item.FindControl("lbllo"), Label)
                lo.CssClass = "plainlabelred"
                Dim hi As Label = CType(e.Item.FindControl("lblhi"), Label)
                hi.CssClass = "plainlabelred"
                Dim sp As Label = CType(e.Item.FindControl("lblspec"), Label)
                sp.CssClass = "plainlabelred"

            End If
        End If
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgmeas.Columns(0).HeaderText = dlabs.GetDGPage("tpmmeas.aspx","dgmeas","0")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(1).HeaderText = dlabs.GetDGPage("tpmmeas.aspx","dgmeas","1")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(2).HeaderText = dlabs.GetDGPage("tpmmeas.aspx","dgmeas","2")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(3).HeaderText = dlabs.GetDGPage("tpmmeas.aspx","dgmeas","3")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(4).HeaderText = dlabs.GetDGPage("tpmmeas.aspx","dgmeas","4")
		Catch ex As Exception
		End Try
		Try
			dgmeas.Columns(5).HeaderText = dlabs.GetDGPage("tpmmeas.aspx","dgmeas","5")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3593.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3593")
		Catch ex As Exception
		End Try
		Try
			lang3594.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3594")
		Catch ex As Exception
		End Try
		Try
			lang3595.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3595")
		Catch ex As Exception
		End Try
		Try
			lang3596.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3596")
		Catch ex As Exception
		End Try
		Try
			lang3597.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3597")
		Catch ex As Exception
		End Try
		Try
			lang3598.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3598")
		Catch ex As Exception
		End Try
		Try
			lang3599.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3599")
		Catch ex As Exception
		End Try
		Try
			lang3600.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3600")
		Catch ex As Exception
		End Try
		Try
			lang3601.Text = axlabs.GetASPXPage("tpmmeas.aspx","lang3601")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			btnwo.Attributes.Add("src" , "../images2/eng/bgbuttons/submit.gif")
			ElseIf lang = "fre" Then
			btnwo.Attributes.Add("src" , "../images2/fre/bgbuttons/submit.gif")
			ElseIf lang = "ger" Then
			btnwo.Attributes.Add("src" , "../images2/ger/bgbuttons/submit.gif")
			ElseIf lang = "ita" Then
			btnwo.Attributes.Add("src" , "../images2/ita/bgbuttons/submit.gif")
			ElseIf lang = "spa" Then
			btnwo.Attributes.Add("src" , "../images2/spa/bgbuttons/submit.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			ovid295.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("tpmmeas.aspx","ovid295") & "')")
			ovid295.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid296.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("tpmmeas.aspx","ovid296") & "')")
			ovid296.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
