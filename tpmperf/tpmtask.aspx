<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmtask.aspx.vb" Inherits="lucy_r12.tpmtask" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>TPM Performer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/tpmimgtasknav.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/tpmtaskaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         var recordflag = "0";
         function showrecords() {
             var tdmeas = document.getElementById("lbltdmeas").value;
             var tdpart = document.getElementById("lbltdpart").value;
             var tdtool = document.getElementById("lbltdtool").value;
             var tdlube = document.getElementById("lbltdlube").value;
             //alert(tdmeas + "," + tdpart + "," + tdtool + "," + tdlube + "," + recordflag)
             if (recordflag == "0") {
                 recordflag = "1";
                 document.getElementById("trdet").className = "details";
                 document.getElementById("trtask1").className = "details";
                 document.getElementById("trtask2").className = "details";
                 document.getElementById("itask").src = "../images/appbuttons/minibuttons/pdown.gif";

                 document.getElementById("trtimehd").className = "view";
                 document.getElementById("trthd").className = "view";
                 document.getElementById("trlhd").className = "view";
                 document.getElementById("trphd").className = "view";
                 document.getElementById("trmhd").className = "view";
                 document.getElementById("trfhd").className = "view";

                 document.getElementById("trtimemsg").className = "view plainlabelblue";
                 document.getElementById("trfail").className = "view plainlabelblue";
                 if (tdmeas == "yes") {
                     document.getElementById("trmeasmsg").className = "view plainlabelblue";
                 }
                 else {
                     document.getElementById("trmeasmsg").className = "details";
                 }
                 if (tdpart == "yes") {
                     document.getElementById("trpartsmsg").className = "view plainlabelblue";
                 }
                 else {
                     document.getElementById("trpartsmsg").className = "details";
                 }
                 
                 if (tdlube == "yes") {
                     document.getElementById("trlubesmsg").className = "view plainlabelblue";
                 }
                 else {
                     document.getElementById("trlubesmsg").className = "details";
                 }
                 
                 if (tdtool == "yes") {
                     document.getElementById("trtoolsmsg").className = "view plainlabelblue";
                 }
                 else {
                     document.getElementById("trtoolsmsg").className = "details";
                 }
                 

                 timeflag = 1;
                 failflag = 1;
                 measflag = 1;
                 toolflag = 1;
                 partflag = 1;
                 lubeflag = 1;
                 taskflag = 0;
             }
             else {
                 recordflag = "0";
                 document.getElementById("trdet").className = "view";
                 document.getElementById("trtask1").className = "view";
                 document.getElementById("trtask2").className = "view";
                 document.getElementById("itask").src = "../images/appbuttons/minibuttons/pup.gif";

                 document.getElementById("trtimehd").className = "details";
                 document.getElementById("trthd").className = "details";
                 document.getElementById("trlhd").className = "details";
                 document.getElementById("trphd").className = "details";
                 document.getElementById("trmhd").className = "details";
                 document.getElementById("trfhd").className = "details";

                 document.getElementById("trtimemsg").className = "details";
                 document.getElementById("trfail").className = "details";
                 document.getElementById("trmeasmsg").className = "details";
                 document.getElementById("trpartsmsg").className = "details";
                 document.getElementById("trlubesmsg").className = "details";
                 document.getElementById("trtoolsmsg").className = "details";

                 closeall();
             }

         }
         function genwr() {
         //alert()
             //dir, pmid, pmhid, pmtid, pmfid, fm, fmid, task, comid, ro
             var dir = "s"; //document.getElementById("lbldir").value;
             var pmid = document.getElementById("lblpmid").value;
             var pmhid = document.getElementById("lblpmhid").value;
             var pmtid = document.getElementById("lblpmtid").value;
             var sid = document.getElementById("lblsid").value;
             var pmfid = ""; //document.getElementById("lblpmfid").value;
             var fm = "";
             var fmid = "";
             var task = document.getElementById("lbltasknum").value;
             var comid = document.getElementById("lblcomid").value; ;
             var ro = "1";
             var eReturn = window.showModalDialog("tpmwrdialog.aspx?pmid=" + pmid + "&pmhid=" + pmhid + "&pmtid=" + pmtid + "&pmfid=" + pmfid + "&fm=" + fm + "&fmid=" + fmid + "&task=" + task + "&comid=" + comid + "&sid=" + sid + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:320px; dialogWidth:790px; resizable=yes");
             if (eReturn) {

             }
         }
         function compalert(malert) {
             //alert(malert)
             var marr = malert.split(",");
             var mmsg;
             var hasflg = 0;
             mmsg = "This PM has ";
             for (i = 0; i < marr.length; i++) {
                 if (marr[i] == "m") {
                     mmsg += "Measurements";
                 }
                 else if (marr[i] == "h") {
                     mmsg += "Task Times";
                 }
                 else if (marr[i] == "d") {
                     mmsg += "Down Times";
                 }
                 //if(i!=marr.length) {
                 //mmsg += ", ";
                 //}
                 //if(marr.length==1&&marr[i]!="m") {
                 //hasflg = 1;
                 //}
             }
             mmsg += " that have not been recorded\nDo you wish to Continue?";
             var conf = confirm(mmsg)
             if (conf == true) {
                 window.parent.setref();
                 document.getElementById("lblmalert").value = "ok";
                 document.getElementById("lblsubmit").value = "checktask"
                 document.getElementById("form1").submit();
             }
             else {
                 document.getElementById("lblmalert").value = "";
                 alert("Action Cancelled")
             }
         }
         function compalert2() {
             document.getElementById("lblmalert").value = "";
             var conf = confirm("This Task has Estimated Minutes with no Actual Minutes recorded.\nAre you sure you want to Continue?")
             if (conf == true) {
                 document.getElementById("lblsubmit").value = "comptask"
                 document.getElementById("form1").submit();
             }
             else {
                 alert("Action Cancelled")
             }
         }
     </script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 2px; LEFT: 2px" cellSpacing="0" cellPadding="0"
				width="1026">
				<tr class="details" id="trqs" height="20" runat="server">
					<td colSpan="5">
						<table cellSpacing="0" cellPadding="2">
							<tr>
							<tr>
								<td width="235"><IMG src="../menu/mimages/fusionsm.gif"></td>
								<td vAlign="top" width="420">
									<table>
										<tr>
											<td colSpan="2"><IMG src="../images/appheaders/tpmperf.gif">
											</td>
										</tr>
										<tr>
											<td class="graylabel" width="120"><asp:Label id="lang3611" runat="server">Current Plant Site:</asp:Label></td>
											<td class="plainlabel" id="tdsite1" width="300" runat="server"></td>
										</tr>
										<tr>
											<td class="graylabel" width="120"><asp:Label id="lang3612" runat="server">Current User:</asp:Label></td>
											<td class="plainlabel" id="tduser1" width="300" runat="server"></td>
										</tr>
									</table>
								</td>
								<td width="455">
									<table>
										<tr>
											<td id="Td2" vAlign="middle" align="right" width="455" runat="server"><IMG id="Img1" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
													runat="server"></td>
										</tr>
										<tr>
											<td class="plainlabelred" id="tdqs" align="center" width="455" runat="server"><asp:Label id="lang3613" runat="server">REVIEW MODE - CANNOT SAVE CHANGES OR PERFORM COMPLETE OPERATIONS. OTHER FUNCTIONALITY MAY BE LIMITED.</asp:Label></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="details" id="trpm" height="20" runat="server">
					<td colSpan="5">
						<table cellSpacing="0" cellPadding="2">
							<tr>
								<td width="235"><IMG src="../menu/mimages/fusionsm.gif"></td>
								<td vAlign="top" width="420">
									<table>
										<tr>
											<td colSpan="2"><IMG src="../images/appheaders/tpmperf.gif">
											</td>
										</tr>
										<tr>
											<td class="graylabel" width="120"><asp:Label id="lang3614" runat="server">Current Plant Site:</asp:Label></td>
											<td class="plainlabel" id="tdsite" width="300" runat="server"></td>
										</tr>
										<tr>
											<td class="graylabel" width="120"><asp:Label id="lang3615" runat="server">Current User:</asp:Label></td>
											<td class="plainlabel" id="tduser" width="300" runat="server"></td>
										</tr>
									</table>
								</td>
								<td id="Td1" vAlign="middle" align="right" width="455" runat="server"><IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><IMG src="../images/appbuttons/minibuttons/4PX.gif"></td>
				</tr>
				<tr>
					<td class="thdrsinglft" align="left" width="20"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
					<td class="thdrsingrt label" width="400"><asp:Label id="lang3616" runat="server">Task Activity Details</asp:Label></td>
					<td width="5">&nbsp;</td>
					<td class="thdrsinglft" align="left" width="20"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
					<td class="thdrsingrt label" width="580"><asp:Label id="lang3617" runat="server">Task Images</asp:Label></td>
				</tr>
				<tr>
					<td vAlign="top" colSpan="2">
						<table cellSpacing="0" cellPadding="3">
							<tr height="20">
								<td class="label"><asp:Label id="lang3618" runat="server">Current TPM</asp:Label></td>
								<td class="plainlabelblue" id="tdtpm" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang3619" runat="server">Current Equipment</asp:Label></td>
								<td class="plainlabelblue" id="tdeq" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang3620" runat="server">Current Function</asp:Label></td>
								<td class="plainlabelblue" id="tdfunc" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang3621" runat="server">Current Component</asp:Label></td>
								<td class="plainlabelblue" id="tdcomp" runat="server"></td>
							</tr>
							<TR>
								<td class="bluelabel"><asp:Label id="lang3622" runat="server">Current Task Status:</asp:Label></td>
								<td class="plainlabelred" id="tdstat" runat="server"><asp:Label id="lang3623" runat="server">Incomplete</asp:Label></td>
							</TR>
							<tr height="20">
								<td class="bluelabel"><asp:Label id="lang3624" runat="server">Tasks Not Completed:</asp:Label></td>
								<td class="plainlabelred" id="tdtocomp" runat="server">1</td>
							</tr>
						</table>
						<table cellSpacing="0" cellPadding="2" width="400">
							<tr>
								<td width="20"></td>
								<td width="380"></td>
							</tr>
							<tr vAlign="middle" height="30">
								<td align="center" colSpan="2">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bfirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblbpage" runat="server" CssClass="bluelabel">Task 1 of 1</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bnext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="blast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr id="trdet" height="20">
								<td class="btmmenu label" align="center"><IMG id="itask" onclick="showtask();" alt="" src="../images/appbuttons/minibuttons/pup.gif"></td>
								<td class="btmmenu label" vAlign="middle"><asp:Label id="lang3625" runat="server">Task Details</asp:Label></td>
							</tr>
							<tr id="trtask1">
								<td colSpan="2">
									<table cellSpacing="0" cellPadding="0">
										<tr>
											<td rowSpan="8">
												<div id="divtask" style="BORDER-BOTTOM: gray 1px solid; BORDER-LEFT: gray 1px solid; WIDTH: 376px; HEIGHT: 180px; OVERFLOW: auto; BORDER-TOP: gray 1px solid; BORDER-RIGHT: gray 1px solid"
													runat="server"></div>
											</td>
											<td vAlign="middle" align="center"><IMG id="imgmag" onmouseover="return overlib('Full View')" onclick="getsubdiv();" onmouseout="return nd()"
													alt="" src="../images/appbuttons/minibuttons/magnifier.gif" runat="server">
											</td>
										</tr>
										<tr>
											<td vAlign="middle" align="center"><IMG id="imgsub" onmouseover="return overlib('View Sub-Tasks for this Task')" onclick="getsubdiv();"
													onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif" runat="server">
											</td>
										</tr>
										<tr>
											<td vAlign="middle" align="center"><IMG id="imgfail" onmouseover="return overlib('This Task Has Failure Modes To Be Addressed')"
													onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/tpmfail.gif" runat="server">
											</td>
										</tr>
										<tr>
											<td vAlign="middle" align="center"><IMG id="imgpart" onmouseover="return overlib('This Task Requires Parts')" onmouseout="return nd()"
													alt="" src="../images/appbuttons/minibuttons/parttrans.gif" runat="server">
											</td>
										</tr>
										<tr>
											<td vAlign="middle" align="center"><IMG id="imgtool" onmouseover="return overlib('This Task Requires Tools')" onmouseout="return nd()"
													alt="" src="../images/appbuttons/minibuttons/tooltrans.gif" runat="server">
											</td>
										</tr>
										<tr>
											<td vAlign="middle" align="center"><IMG id="imglube" onmouseover="return overlib('This Task Requires Lubricants')" onmouseout="return nd()"
													alt="" src="../images/appbuttons/minibuttons/lubetrans.gif" runat="server">
											</td>
										</tr>
										<tr>
											<td vAlign="middle" align="center"><IMG id="imgmeas" onmouseover="return overlib('This Task Requires Measurements')" onmouseout="return nd()"
													alt="" src="../images/appbuttons/minibuttons/measure.gif" runat="server">
											</td>
										</tr>
										<tr height="20">
											<td>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr id="trtask2" height="160">
								<td id="tdtask2" vAlign="top" colSpan="2" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="btmmenu label" align="center"><IMG id="irecords" onclick="showrecords();" alt="" src="../images/appbuttons/minibuttons/pdown.gif"></td>
								<td class="btmmenu label" vAlign="middle"><asp:Label id="lang3626" runat="server">Task Records\Actions</asp:Label></td>
							</tr>
							<tr height="20">
								<td align="right" colSpan="2"><IMG onmouseover="return overlib('Generate Work Request')" onclick="genwr();" onmouseout="return nd()"
										src="../images/appbuttons/minibuttons/woreq.gif"> <IMG id="imgcomptask" onmouseover="return overlib('Complete This Task')" onclick="comptask();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/compgrn.gif" runat="server"><IMG id="imgcomptpm" onmouseover="return overlib('Complete This TPM')" onclick="comptpm();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/compdis.gif" runat="server"></td>
							</tr>
							<tr class="details" id="trtimehd" height="20">
								<td class="btmmenu label" align="center"><IMG id="itime" onclick="showtime();" alt="" src="../images/appbuttons/minibuttons/pdown.gif"></td>
								<td class="btmmenu label" vAlign="middle"><asp:Label id="lang3627" runat="server">Task Time \ Down Time</asp:Label></td>
							</tr>
							<tr class="details" id="trtimemsg" runat="server">
								<td class="plainlabelblue" id="tdtimemsg" colSpan="2" runat="server"></td>
							</tr>
							<tr class="details" id="trtime" runat="server">
								<td id="tdtime" colSpan="2"><iframe id="iftime" style="BORDER-BOTTOM-STYLE: none; PADDING-BOTTOM: 0px; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none; PADDING-TOP: 0px"
										src="" frameBorder="no" width="380" scrolling="yes" height="90" allowTransparency runat="server">
									</iframe>
								</td>
							</tr>
							<tr class="details" id="trfhd" height="20">
								<td class="btmmenu label" align="center"><IMG id="ifail" onclick="showfail();" alt="" src="../images/appbuttons/minibuttons/pdown.gif"></td>
								<td class="btmmenu label" vAlign="middle"><asp:Label id="lang3628" runat="server">Task Failure Modes</asp:Label></td>
							</tr>
							<tr class="details" id="trfail" runat="server">
								<td class="plainlabelblue" id="tdfail" colSpan="2" runat="server"></td>
							</tr>
							<tr class="details" id="trfaili" runat="server">
								<td id="tdfaili" colSpan="2"><iframe id="iffail" style="BORDER-BOTTOM-STYLE: none; PADDING-BOTTOM: 0px; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none; PADDING-TOP: 0px"
										src="" frameBorder="no" width="380" scrolling="yes" height="90" allowTransparency runat="server">
									</iframe>
								</td>
							</tr>
							<tr class="details" id="trmhd" height="20">
								<td class="btmmenu label" align="center"><IMG id="imeas" alt="" src="../images/appbuttons/minibuttons/pdown.gif" runat="server"></td>
								<td class="btmmenu label" vAlign="middle"><asp:Label id="lang3629" runat="server">Task Measurements</asp:Label></td>
							</tr>
							<tr id="trmeasmsg" runat="server" class="details">
								<td class="plainlabelblue" id="tdmeas" colSpan="2" runat="server"></td>
							</tr>
							<tr class="details" id="trmeas" height="20" runat="server">
								<td colSpan="2"><iframe id="ifmeas" style="BORDER-BOTTOM-STYLE: none; PADDING-BOTTOM: 0px; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none; PADDING-TOP: 0px"
										src="" frameBorder="no" width="380" scrolling="yes" height="90" allowTransparency runat="server"></iframe>
								</td>
							</tr>
							<tr class="details" id="trthd" height="20">
								<td class="btmmenu label" align="center"><IMG id="itool" alt="" src="../images/appbuttons/minibuttons/pdown.gif" runat="server"></td>
								<td class="btmmenu label" vAlign="middle"><asp:Label id="lang3630" runat="server">Task Tools</asp:Label></td>
							</tr>
							<tr id="trtoolsmsg" height="20" runat="server" class="details">
								<td class="plainlabelblue" id="tdtools" colSpan="2" runat="server"></td>
							</tr>
							<tr class="details" id="trtools" height="20" runat="server">
								<td colSpan="2"><iframe id="iftools" style="BORDER-BOTTOM-STYLE: none; PADDING-BOTTOM: 0px; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none; PADDING-TOP: 0px"
										src="" frameBorder="no" width="380" scrolling="yes" height="90" allowTransparency runat="server"></iframe>
								</td>
							</tr>
							<tr class="details" id="trlhd" height="20">
								<td class="btmmenu label" align="center"><IMG id="ilube" alt="" src="../images/appbuttons/minibuttons/pdown.gif" runat="server"></td>
								<td class=" btmmenu label" vAlign="middle"><asp:Label id="lang3631" runat="server">Task Lubricants</asp:Label></td>
							</tr>
							<tr id="trlubesmsg" height="20" runat="server" class="details">
								<td class="plainlabelblue" id="tdlubes" colSpan="2" runat="server"></td>
							</tr>
							<tr class="details" id="trlubes" height="20" runat="server">
								<td colSpan="2"><iframe id="iflubes" style="BORDER-BOTTOM-STYLE: none; PADDING-BOTTOM: 0px; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none; PADDING-TOP: 0px"
										src="" frameBorder="no" width="380" scrolling="yes" height="90" allowTransparency runat="server"></iframe>
								</td>
							</tr>
							<tr class="details" id="trphd" height="20">
								<td class="btmmenu label" align="center"><IMG id="ipart" alt="" src="../images/appbuttons/minibuttons/pdown.gif" runat="server"></td>
								<td class="btmmenu label" vAlign="middle"><asp:Label id="lang3632" runat="server">Task Parts</asp:Label></td>
							</tr>
							<tr id="trpartsmsg" height="20" runat="server" class="details">
								<td class="plainlabelblue" id="tdparts" colSpan="2" runat="server"></td>
							</tr>
							<tr class="details" id="trparts" height="20" runat="server">
								<td vAlign="top" colSpan="2"><iframe id="ifparts" style="BORDER-BOTTOM-STYLE: none; PADDING-BOTTOM: 0px; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none; PADDING-TOP: 0px"
										src="" frameBorder="no" width="380" scrolling="yes" height="90" allowTransparency runat="server"></iframe>
								</td>
							</tr>
							<tr height="150">
								<td colSpan="2">&nbsp;</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td vAlign="top" colSpan="2">
						<table cellSpacing="1" cellPadding="2" width="580">
							<tr height="20">
								<td align="center" colSpan="3">
									<div style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 600px; HEIGHT: 502px; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid; PADDING-TOP: 10px">
										<table cellSpacing="0" cellPadding="0" width="600">
											<tr height="500">
												<td vAlign="middle" align="center" width="600"><A href="#"><IMG id="imgeq" src="../images/appimages/tpmimg1.gif" border="0" runat="server"></A></td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td class="label" id="tdpline1" align="center" colSpan="3" runat="server"></td>
							</tr>
							<tr>
								<td align="center" colSpan="3">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="134"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbltasknum" type="hidden" name="lbltasknum" runat="server"> <input id="lblfuncid" type="hidden" name="lblfuncid" runat="server">
			<input id="lblcurrp" type="hidden" name="lblcurrp" runat="server"> <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
			<input id="lblimgid" type="hidden" name="lblimgid" runat="server"> <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
			<input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server"><input id="lblpcnt" type="hidden" name="lblpcnt" runat="server">
			<input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server"> <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
			<input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"> <input id="lblititles" type="hidden" name="lblititles" runat="server">
			<input id="lbliorders" type="hidden" name="lbliorders" runat="server"> <input id="lblmaxtask" type="hidden" name="lblmaxtask" runat="server">
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"> <input id="lblpmtskid" type="hidden" runat="server">
			<input id="lblts" type="hidden" runat="server"> <input id="lbleqid" type="hidden" runat="server">
			<input id="lblfreq" type="hidden" runat="server"> <input id="lblrd" type="hidden" runat="server">
			<input id="lbltyp" type="hidden" runat="server"> <input id="lblpmid" type="hidden" runat="server">
			<input id="lbltasknum1" type="hidden" runat="server"> <input id="lblwo" type="hidden" runat="server">
			<input id="lblmalert" type="hidden" runat="server"><input id="lblpmhid" type="hidden" runat="server">
			<input id="lblcalday" type="hidden" runat="server"><input id="lblacost" type="hidden" runat="server">
			<input id="lblpmtid" type="hidden" runat="server"> <input id="lblfcust" type="hidden" runat="server">
			<input id="lbluserid" type="hidden" runat="server"> <input id="lblusername" type="hidden" runat="server">
			<input id="lbldays" type="hidden" runat="server"> <input id="lblnextdate" type="hidden" runat="server">
			<input type="hidden" id="lblshift" runat="server"> <input id="lblusetdt" type="hidden" runat="server" NAME="lblusetdt">
			<input id="lblusetotal" type="hidden" runat="server" NAME="lblusetotal"> <input id="lblttime" type="hidden" runat="server" NAME="lblttime">
			<input id="lbldtime" type="hidden" runat="server" NAME="lbldtime"> <input id="lblacttime" type="hidden" runat="server" NAME="lblacttime">
			<input id="lblactdtime" type="hidden" runat="server" NAME="lblactdtime"><input type="hidden" id="lblhalert" runat="server" NAME="lblhalert">
			<input type="hidden" id="lbldalert" runat="server" NAME="lbldalert">
            <input type="hidden" id="lblsid" runat="server" />
		<input type="hidden" id="lbltdmeas" runat="server" />
        <input type="hidden" id="lbltdpart" runat="server" />
        <input type="hidden" id="lbltdlube" runat="server" />
        <input type="hidden" id="lbltdtool" runat="server" />
        <input type="hidden" id="lblcomid" runat="server" />
        <input type="hidden" id="lblcoi" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
