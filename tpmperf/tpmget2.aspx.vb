﻿Imports System.Data.SqlClient
Public Class tpmget2
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sid, tli, ts, jump, typ, did, lid, Filter, clid, eqid, fuid As String
    Dim tl, cid, coid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblgototasks.Value = "0"
            lblcleantasks.Value = "2"
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            tli = "5" 'Request.QueryString("tli").ToString
            lbltl.Value = tli
            tasks.Open()
            Try
                ts = Request.QueryString("ts").ToString
            Catch ex As Exception
                ts = "0"
            End Try
            lblts.Value = ts
            jump = Request.QueryString("jump").ToString
            Dim deptcheck As Integer = 0
            Dim fuidcheck As Integer = 0
            If jump = "yes" Then
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                Dim eqcheck As Integer = 0
                If typ = "loc" Or typ = "dloc" Then
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    did = Request.QueryString("did").ToString
                    If did <> "" Then
                        lbldept.Value = did
                        Filter = did
                        If ts = "1" Then
                            lblgetarch.Value = "yes"
                        End If
                    Else
                        deptcheck = 1
                    End If
                    If deptcheck <> 1 Then
                        Dim cellchk As Integer = 0
                        Dim chk As String = CellCheck(Filter)
                        If chk = "no" Then
                            lblchk.Value = "no"
                            cellchk = 1
                        Else
                            lblchk.Value = "yes"
                            clid = Request.QueryString("clid").ToString
                            lblclid.Value = clid
                            lblpar.Value = "cell"
                        End If
                    End If
                End If
                eqid = Request.QueryString("eqid").ToString
                If eqid <> "" Then
                    lbleqid.Value = eqid
                    GetEq(eqid)
                Else
                    eqcheck = "1"
                End If
                If eqcheck <> 1 Then
                    Try
                        fuid = Request.QueryString("funid").ToString
                        lblfuid.Value = fuid
                    Catch ex As Exception
                        fuidcheck = 1
                        GoToTasks("yes")
                    End Try
                    If fuidcheck <> 1 Then
                        CheckTasks(fuid)
                        Try
                            coid = Request.QueryString("comid").ToString
                            lblcoid.Value = coid
                        Catch ex As Exception

                        End Try
                        If lblcoid.Value <> "" AndAlso lblcoid.Value <> "0" Then
                            lbltaskcnt.Value = "1"
                            'GoToTask(coid)
                            GoToTasks("yes")
                        Else
                            GoToTasks("yes")
                        End If
                    Else
                        lblgototasks.Value = "0"
                    End If
                End If
            End If
            tasks.Dispose()
        Else

        End If
    End Sub
    Private Sub GetEq(ByVal eqid As String)
        Dim eqnum, eqdesc As String
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eqid & "'"
        dr = tasks.GetRdrData(sql)
        While dr.Read
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
        End While
        dr.Close()
        tdeq.InnerHtml = eqnum
        tddesc.InnerHtml = eqdesc
    End Sub
    Private Sub CheckTasks(ByVal fu As String)
        sql = "select count(*) from pmtaskstpm where funcid = '" & fu & "'"
        Dim cnt As Integer
        cnt = tasks.Scalar(sql)
        If cnt = 0 Then
            lbltaskcnt.Value = "0"
        Else
            lbltaskcnt.Value = "1"
        End If
    End Sub
    Private Sub GoToTask(ByVal coid As String)
        Dim tn As Integer
        Dim tasknum, chk As String
        lblcoid.Value = coid
        tl = "5" 'lbltasklev.Value
        cid = "0" 'lblcid.Value
        sid = lblsid.Value
        did = lbldept.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        'sql = "select top(1) tasknum from pmtasks where funcid = '" & fuid & "' and comid = '" & coid & "'"
        'tn = tasks.Scalar(sql)
        'tasknum = tn
        lbltaskcnt.Value = "1"
        lblgototasks.Value = "1"
        lblcleantasks.Value = "0"
        'Response.Redirect("PMTaskDivFuncGrid.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
        'cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        '"&eqid=" & eqid & "&task=" & tasknum & "&comid=" & coid)
    End Sub
    Private Sub GoToTasks(Optional ByVal jump As String = "no")
        tl = "5" 'lbltl.Value
        cid = "0" 'lblcid.Value
        sid = lblsid.Value
        did = lbldept.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        Dim chk As String
        chk = lblchk.Value
        lblgototasks.Value = "1"
        lbltaskcnt.Value = "1"
        lblcleantasks.Value = "0"
    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = tasks.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = tasks.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
End Class