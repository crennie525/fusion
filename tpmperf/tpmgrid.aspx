<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmgrid.aspx.vb" Inherits="lucy_r12.tpmgrid" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmgrid</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/tpmgridaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function checktask() {
             checkit();
             var dir = document.getElementById("lblret").value;
             document.getElementById("lblret").value = "";
             if (dir == "s") {
                 var eqid = document.getElementById("lbleqid").value;
                 var fuid = document.getElementById("lblfuid").value;
                 var coid = document.getElementById("lblcoid").value;
                 var pmid = document.getElementById("lblpmid").value;
                 var pmhid = document.getElementById("lblpmhid").value;
                 var wonum = document.getElementById("lblwo").value;
                 var sid = document.getElementById("lblsid").value;
                 //window.open("tpmtask.aspx?ts=0&pmid=" + pmid + "&wonum=" + wonum + "&pmhid=" + pmhid + "&date=" + Date());
                 var eReturn = window.showModalDialog("tpmtaskdialog.aspx?ts=0&pmid=" + pmid + "&wonum=" + wonum + "&pmhid=" + pmhid + "&sid=" + sid + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px; resizable=yes");
                 if (eReturn) {
                     if (eReturn == "0" || eReturn == "1") {
                         var ts = eReturn;
                         window.location = "tpmgrid.aspx?comid=" + coid + "&start=yes&tl=5&ts=" + ts + "&fuid=" + fuid + "&eqid=" + eqid + "&sid=" + sid;
                     }
                 }
             }
             else if (dir == "o") {
                 var eqid = document.getElementById("lbleqid").value;
                 var fuid = document.getElementById("lblfuid").value;
                 var coid = document.getElementById("lblcoid").value;
                 var typ = document.getElementById("lbltyp1").value;
                 var rd = document.getElementById("lblrd").value;
                 var freq = document.getElementById("lblfreq").value;
                 var sid = document.getElementById("lblsid").value;
                 //window.open("tpmtask.aspx?ts=1&eqid=" + eqid + "&typ=" + typ + "&rd=" + rd + "&freq=" + freq + "&date=" + Date());
                 var eReturn = window.showModalDialog("tpmtaskdialog.aspx?ts=1&eqid=" + eqid + "&typ=" + typ + "&rd=" + rd + "&freq=" + freq + "&sid=" + sid + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px; resizable=yes");
                 if (eReturn) {
                     if (eReturn == "0" || eReturn == "1") {
                         var ts = eReturn;
                         window.location = "tpmgrid.aspx?comid=" + coid + "&start=yes&tl=5&ts=" + ts + "&fuid=" + fuid + "&eqid=" + eqid + "&sid=" + sid;
                     }
                 }
             }
         }
     </script>
	</HEAD>
	<body onload="checktask();"  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px" cellSpacing="0" width="720">
				<TBODY>
					<tr height="24">
						<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/pmgridhdr.gif" border="0"></td>
						<td class="thdrsingrt label" align="left" width="694"><asp:Label id="lang3579" runat="server">TPM Records</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="2">
							<div style="OVERFLOW: auto; WIDTH: 710px; HEIGHT: 362px"><asp:repeater id="rptrtasks" runat="server">
									<HeaderTemplate>
										<table width="690" cellspacing="2">
											<tr>
												<td width="270"></td>
												<td width="200"></td>
												<td width="90"></td>
												<td width="90"></td>
												<td width="30"></td>
											</tr>
											<tr class="tbg" height="26">
												<td class="thdrsingg plainlabel" colspan="2"><asp:Label id="lang3580" runat="server">TPM</asp:Label></td>
												<td class="thdrsingg plainlabel"><asp:Label id="lang3581" runat="server">Last Date</asp:Label></td>
												<td class="thdrsingg plainlabel"><asp:Label id="lang3582" runat="server">Next Date</asp:Label></td>
												<td class="thdrsingg plainlabel"><img src="../images/appbuttons/minibuttons/gwarningnbg.gif"></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg" height="20">
											<td class="plainlabel">
												<asp:LinkButton ID="lbltn"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>' Runat = server>
												</asp:LinkButton>
											</td>
											<td class="plainlabel">
												<table cellSpacing="0" cellPadding="0" id="tbli" runat="server" class="details">
													<tr class="tbg" id="trshiftdays" runat="server">
														<td class="bluelabel" onmouseover="return overlib('Used to indicate Shift designation')"
															onmouseout="return nd()" align="center"><asp:Label id="lang3583" runat="server">Shift</asp:Label></td>
														<td class="bluelabel"></td>
														<td class="bluelabel" align="center" width="22">M</td>
														<td class="bluelabel" align="center" width="22">Tu</td>
														<td class="bluelabel" align="center" width="22">W</td>
														<td class="bluelabel" align="center" width="22">Th</td>
														<td class="bluelabel" align="center" width="22">F</td>
														<td class="bluelabel" align="center" width="22">Sa</td>
														<td class="bluelabel" align="center" width="22">Su</td>
													</tr>
													<tr class="tbg" id="trshift1" runat="server">
														<td><INPUT id="cb1o" type="checkbox" name="cb1o" runat="server" disabled></td>
														<td class="label" id="lblsi" runat="server">1st</td>
														<td align="center"><INPUT id="cb1mon" type="checkbox" name="Checkbox1" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1tue" type="checkbox" name="Checkbox2" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1wed" type="checkbox" name="Checkbox3" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1thu" type="checkbox" name="Checkbox4" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1fri" type="checkbox" name="Checkbox5" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1sat" type="checkbox" name="Checkbox6" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1sun" type="checkbox" name="Checkbox7" runat="server" disabled></td>
													</tr>
												</table>
											</td>
											<td class="plainlabel">
												<asp:Label ID="lbllast" Text='<%# DataBinder.Eval(Container.DataItem,"lastdate")%>' Runat = server>
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label ID="lblnext" Text='<%# DataBinder.Eval(Container.DataItem,"nextdate")%>' Runat = server>
												</asp:Label></td>
											<td align="center"><IMG id="iwi" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
													src="../images/appbuttons/minibuttons/gwarning.gif" runat="server"></td>
											<td class="details">
												<asp:Label ID="lblpmiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lblpmhiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmhid")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lblpmwoi" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<AlternatingItemTemplate>
										<tr height="20">
											<td class="plainlabel transrowblue">
												<asp:LinkButton ID="lbltnalt"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>' Runat = server>
												</asp:LinkButton>
											</td>
											<td class="plainlabel transrowblue">
												<table cellSpacing="0" cellPadding="0" id="tbla" runat="server" class="details">
													<tr class="tbg" id="Tr1" runat="server">
														<td class="bluelabel" onmouseover="return overlib('Used to indicate Shift designation')"
															onmouseout="return nd()" align="center"><asp:Label id="lang3584" runat="server">Shift</asp:Label></td>
														<td class="bluelabel"></td>
														<td class="bluelabel" align="center" width="22">M</td>
														<td class="bluelabel" align="center" width="22">Tu</td>
														<td class="bluelabel" align="center" width="22">W</td>
														<td class="bluelabel" align="center" width="22">Th</td>
														<td class="bluelabel" align="center" width="22">F</td>
														<td class="bluelabel" align="center" width="22">Sa</td>
														<td class="bluelabel" align="center" width="22">Su</td>
													</tr>
													<tr class="tbg" id="Tr2" runat="server">
														<td><INPUT id="cb1oa" type="checkbox" name="cb1oa" runat="server" disabled></td>
														<td class="label" id="lblsa" runat="server">1st</td>
														<td align="center"><INPUT id="cb1mona" type="checkbox" name="Checkbox1" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1tuea" type="checkbox" name="Checkbox2" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1weda" type="checkbox" name="Checkbox3" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1thua" type="checkbox" name="Checkbox4" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1fria" type="checkbox" name="Checkbox5" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1sata" type="checkbox" name="Checkbox6" runat="server" disabled></td>
														<td align="center"><INPUT id="cb1suna" type="checkbox" name="Checkbox7" runat="server" disabled></td>
													</tr>
												</table>
											</td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="lbllastalt" Text='<%# DataBinder.Eval(Container.DataItem,"lastdate")%>' Runat = server>
												</asp:Label></td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="lblnextalt" Text='<%# DataBinder.Eval(Container.DataItem,"nextdate")%>' Runat = server>
												</asp:Label></td>
											<td align="center" class="transrowblue"><IMG id="iwa" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
													src="../images/appbuttons/minibuttons/gwarning.gif" runat="server"></td>
											<td class="details">
												<asp:Label ID="lblpmidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lblpmhidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmhid")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lblpmwoa" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</AlternatingItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater><asp:repeater id="rptrtasks2" runat="server" visible="False">
				<HeaderTemplate>
					<table width="690" cellspacing="2">
						<tr>
							<td width="270"></td>
							<td width="200"></td>
							<td width="90"></td>
							<td width="90"></td>
							<td width="30"></td>
						</tr>
						<tr class="tbg" height="26">
							<td class="thdrsingg plainlabel" colspan="2"><asp:Label id="lang3585" runat="server">TPM</asp:Label></td>
							<td class="thdrsingg plainlabel"><asp:Label id="lang3586" runat="server">Last Date</asp:Label></td>
							<td class="thdrsingg plainlabel"><asp:Label id="lang3587" runat="server">Next Date</asp:Label></td>
							<td class="thdrsingg plainlabel"><img src="../images/appbuttons/minibuttons/gwarningnbg.gif"></td>
						</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr class="tbg" height="20">
						<td class="plainlabel">
							<asp:LinkButton ID="lbltn2" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"freq")%>' Runat = server>
							</asp:LinkButton>
						</td>
						<td class="plainlabel">
							<table cellSpacing="0" cellPadding="0" id="Table1" runat="server" class="details">
								<tr class="tbg" id="Tr3" runat="server">
									<td class="bluelabel" onmouseover="return overlib('Used to indicate Shift designation')"
										onmouseout="return nd()" align="center"><asp:Label id="lang3588" runat="server">Shift</asp:Label></td>
									<td class="bluelabel"></td>
									<td class="bluelabel" align="center" width="22">M</td>
									<td class="bluelabel" align="center" width="22">Tu</td>
									<td class="bluelabel" align="center" width="22">W</td>
									<td class="bluelabel" align="center" width="22">Th</td>
									<td class="bluelabel" align="center" width="22">F</td>
									<td class="bluelabel" align="center" width="22">Sa</td>
									<td class="bluelabel" align="center" width="22">Su</td>
								</tr>
								<tr class="tbg" id="Tr4" runat="server">
									<td><INPUT id="cb1o1" type="checkbox" name="cb1oa" runat="server"></td>
									<td class="label" id="lblsi1" runat="server">1st</td>
									<td align="center"><INPUT id="cb1mon1" type="checkbox" name="Checkbox1" runat="server"></td>
									<td align="center"><INPUT id="cb1tue1" type="checkbox" name="Checkbox2" runat="server"></td>
									<td align="center"><INPUT id="cb1wed1" type="checkbox" name="Checkbox3" runat="server"></td>
									<td align="center"><INPUT id="cb1thu1" type="checkbox" name="Checkbox4" runat="server"></td>
									<td align="center"><INPUT id="cb1fri1" type="checkbox" name="Checkbox5" runat="server"></td>
									<td align="center"><INPUT id="cb1sat1" type="checkbox" name="Checkbox6" runat="server"></td>
									<td align="center"><INPUT id="cb1sun1" type="checkbox" name="Checkbox7" runat="server"></td>
								</tr>
							</table>
						</td>
						<td class="plainlabel">
							<asp:Label ID="lbllast2" Runat="server"></asp:Label></td>
						<td class="plainlabel">
							<asp:Label ID="lblnext2" Runat="server"></asp:Label></td>
						<td align="center"><IMG class="details" id="Img1" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)"
								onmouseout="return nd()" src="../images/appbuttons/minibuttons/gwarning.gif" runat="server"></td>
						<td class="details">
							<asp:Label ID="lblpmiditem2" Text='<%# DataBinder.Eval(Container.DataItem,"typ")%>' Runat = server>
							</asp:Label></td>
						<td class="details">
							<asp:Label ID="lblrd2" Text='<%# DataBinder.Eval(Container.DataItem,"rd")%>' Runat = server>
							</asp:Label></td>
						<td class="details">
							<asp:Label ID="lblfreqi2" Text='<%# DataBinder.Eval(Container.DataItem,"freqi")%>' Runat = server>
							</asp:Label></td>
					</tr>
				</ItemTemplate>
				<AlternatingItemTemplate>
					<tr height="20">
						<td class="plainlabel transrowblue">
							<asp:LinkButton ID="lbltnalt2" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"freq")%>' Runat = server>
							</asp:LinkButton>
						</td>
						<td class="plainlabel transrowblue">
							<table cellSpacing="0" cellPadding="0" id="Table2" runat="server" class="details">
								<tr class="tbg" id="Tr5" runat="server">
									<td class="bluelabel" onmouseover="return overlib('Used to indicate Shift designation')"
										onmouseout="return nd()" align="center"><asp:Label id="lang3589" runat="server">Shift</asp:Label></td>
									<td class="bluelabel"></td>
									<td class="bluelabel" align="center" width="22">M</td>
									<td class="bluelabel" align="center" width="22">Tu</td>
									<td class="bluelabel" align="center" width="22">W</td>
									<td class="bluelabel" align="center" width="22">Th</td>
									<td class="bluelabel" align="center" width="22">F</td>
									<td class="bluelabel" align="center" width="22">Sa</td>
									<td class="bluelabel" align="center" width="22">Su</td>
								</tr>
								<tr class="tbg" id="Tr6" runat="server">
									<td><INPUT id="cb1oa1" type="checkbox" name="cb1oa" runat="server"></td>
									<td class="label" id="lblsa1" runat="server">1st</td>
									<td align="center"><INPUT id="cb1mona1" type="checkbox" name="Checkbox1" runat="server"></td>
									<td align="center"><INPUT id="cb1tuea1" type="checkbox" name="Checkbox2" runat="server"></td>
									<td align="center"><INPUT id="cb1weda1" type="checkbox" name="Checkbox3" runat="server"></td>
									<td align="center"><INPUT id="cb1thua1" type="checkbox" name="Checkbox4" runat="server"></td>
									<td align="center"><INPUT id="cb1fria1" type="checkbox" name="Checkbox5" runat="server"></td>
									<td align="center"><INPUT id="cb1sata1" type="checkbox" name="Checkbox6" runat="server"></td>
									<td align="center"><INPUT id="cb1suna1" type="checkbox" name="Checkbox7" runat="server"></td>
								</tr>
							</table>
						</td>
						<td class="plainlabel transrowblue">
							<asp:Label ID="lbllastalt2" Runat="server"></asp:Label></td>
						<td class="plainlabel transrowblue">
							<asp:Label ID="lblnextalt2" Runat="server"></asp:Label></td>
						<td align="center" class="transrowblue"><IMG class="details" id="Img2" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)"
								onmouseout="return nd()" src="../images/appbuttons/minibuttons/gwarning.gif" runat="server"></td>
						<td class="details">
							<asp:Label ID="lblpmidalt2" Text='<%# DataBinder.Eval(Container.DataItem,"typ")%>' Runat = server>
							</asp:Label></td>
						<td class="details">
							<asp:Label ID="lblrdalt2" Text='<%# DataBinder.Eval(Container.DataItem,"rd")%>' Runat = server>
							</asp:Label></td>
						<td class="details">
							<asp:Label ID="lblfreqialt2" Text='<%# DataBinder.Eval(Container.DataItem,"freqi")%>' Runat = server>
							</asp:Label></td>
					</tr>
				</AlternatingItemTemplate>
				<FooterTemplate>
					</table>
				</FooterTemplate>
			</asp:repeater></DIV></TD></TR></TBODY></TABLE><input id="taskcnt" type="hidden" name="taskcnt" runat="server">
			<input id="lbleqid" type="hidden" runat="server"> <input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblfuid" type="hidden" name="lblfuid" runat="server"> <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"> <input id="lbltasknum" type="hidden" name="lbltasknum" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"><input id="lbltyp" type="hidden" name="lbltyp" runat="server">
			<input id="lbllid" type="hidden" name="lbllid" runat="server"> <input id="lblfreq" type="hidden" name="lblfreq" runat="server">
			<input id="lblrd" type="hidden" name="lblrd" runat="server"> <input id="lbltyp1" type="hidden" name="Hidden4" runat="server">
			<input id="lblpmid" type="hidden" name="lblpmid" runat="server"> <input id="lblret" type="hidden" runat="server">
			<input id="lblwo" type="hidden" runat="server"> <input id="lblpmhid" type="hidden" runat="server">
            <input type="hidden" id="lbluserid" runat="server" />
            <input type="hidden" id="lblislabor" runat="server" />
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
