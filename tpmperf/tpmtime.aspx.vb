

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class tpmtime
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, funcid, tasknum, pmid, eqid, freq, rd, typ, ts, wonum, pmhid As String
    Dim news As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblts As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim ap As New AppUtils
    Protected WithEvents lblacost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltentry As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgparts2 As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdtimed As System.Web.UI.HtmlControls.HtmlTableCell
    Dim ds As DataSet
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdtime As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgparts As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            ts = Request.QueryString("ts").ToString '"0" '
            lblts.Value = ts
            'tasknum is pmtskid
            tasknum = Request.QueryString("tasknum").ToString '"24" '
            lbltasknum.Value = tasknum

            Dim acost As String = ap.ActCost()
            lblacost.Value = acost
            Dim tentry As String = ap.TimeEntry()
            lbltentry.Value = tentry

            If acost <> "lcaext" Then
                If tentry = "lvst" Then
                    dgparts.Columns(2).Visible = True
                    dgparts.Columns(3).Visible = True
                    'dgparts.Columns(4).Visible = False

                    dgparts2.Columns(2).Visible = True
                    dgparts2.Columns(3).Visible = True
                    'dgparts2.Columns(4).Visible = False
                    'dgparts.Columns(5).Visible = False
                    'tdstat.InnerHtml = "Default is Start Time \ Stop Time Entry"
                Else
                    dgparts.Columns(2).Visible = False
                    dgparts.Columns(3).Visible = False
                    'dgparts.Columns(4).Visible = False

                    dgparts2.Columns(2).Visible = False
                    dgparts2.Columns(3).Visible = False
                    'dgparts2.Columns(4).Visible = False
                    'dgparts.Columns(5).Visible = True
                    'tdstat.InnerHtml = "Default is Time Entry Only"
                End If
            Else
                'dgparts.Columns(5).Visible = True
                dgparts.Columns(2).Visible = False
                dgparts.Columns(3).Visible = False
                dgparts.Columns(4).Visible = True

                dgparts2.Columns(2).Visible = False
                dgparts2.Columns(3).Visible = False
                dgparts2.Columns(4).Visible = True
                'tdstat.InnerHtml = "Default is External Entry"
            End If

            news.Open()
            If ts = "0" Then
                pmid = Request.QueryString("pmid").ToString '"134" '
                lblpmid.Value = pmid
                'wonum = Request.QueryString("wonum").ToString
                'lblwo.Value = wonum
                GetSTasks(pmid, tasknum)
                GetSTasks2(pmid, tasknum)
                'trqs.Attributes.Add("class", "details")
                'trpm.Attributes.Add("class", "view")
            Else
                eqid = Request.QueryString("eqid").ToString '"275" '
                lbleqid.Value = eqid
                'GetOTasks(eqid, freq, rd, typ, tasknum)
                'trqs.Attributes.Add("class", "view")
                'trpm.Attributes.Add("class", "details")
            End If
            news.Dispose()
        End If
    End Sub
    Private Sub GetOTasks(ByVal pmtskid As String)
        sql = "select ttime, '' as starttime, '' as stoptime, '' as hrs, '' as mins, '' as aps, " _
        + "'' as starttimedt, '' as stoptimedt, '' as hrsdt, '' as minsdt, '' as apsdt " _
        + "from pmtaskstpm where pmtskid = '" & pmtskid & "'"
        ds = news.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dgparts.DataSource = dv
        dgparts.DataBind()
    End Sub
    Private Sub GetOTasks2(ByVal pmtskid As String)
        sql = "select ttime, '' as starttime, '' as stoptime, '' as hrs, '' as mins, '' as aps, " _
        + "'' as starttimedt, '' as stoptimedt, '' as hrsdt, '' as minsdt, '' as apsdt " _
        + "from pmtaskstpm where pmtskid = '" & pmtskid & "'"
        ds = news.GetDSData(sql)
        Dim dv1 As DataView
        dv1 = ds.Tables(0).DefaultView
        'Try
        dgparts2.DataSource = dv1
        dgparts2.DataBind()
    End Sub
    Private Sub GetSTasks(ByVal pmid As String, ByVal tasknum As String)
        sql = "usp_gettpmtmins2 '" & pmid & "','" & tasknum & "'"
        ds = news.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dgparts.DataSource = dv
        dgparts.DataBind()


    End Sub
    Private Sub GetSTasks2(ByVal pmid As String, ByVal tasknum As String)
        sql = "usp_gettpmdtmins '" & pmid & "','" & tasknum & "'"
        ds = news.GetDSData(sql)
        Dim dv1 As DataView
        dv1 = ds.Tables(0).DefaultView
        'Try
        dgparts2.DataSource = dv1
        dgparts2.DataBind()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        Dim cati As Integer
        If Len(CatID) <> 0 Then
            cati = CType(CatID, Integer)
            iL = CatID '- 1
        Else
            iL = 0
        End If
        Return iL
    End Function
    Function GetSelIndexM(ByVal CatID As String) As Integer
        Dim iL As Integer
        Dim cati As Integer
        If Len(CatID) <> 0 Then
            cati = CType(CatID, Integer)
            If CatID < 15 Then
                iL = 0
            ElseIf CatID < 30 Then
                iL = 1
            ElseIf CatID < 45 Then
                iL = 2
            Else
                iL = 3
            End If
        Else
            iL = 0
        End If
        Return iL
    End Function
    Function GetSelIndexAP(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Len(CatID) <> 0 Then
            If CatID = "AM" Then
                iL = 0
            Else
                iL = 1
            End If
        Else
            iL = 0
        End If
        Return iL
    End Function

    Private Sub dgparts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand
        news.Open()
        pmid = lblpmid.Value
        tasknum = lbltasknum.Value
        dgparts.EditItemIndex = e.Item.ItemIndex
        GetSTasks(pmid, tasknum)
        news.Dispose()
    End Sub

    Private Sub dgparts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        dgparts.EditItemIndex = -1
        news.Open()
        pmid = lblpmid.Value
        tasknum = lbltasknum.Value
        GetSTasks(pmid, tasknum)
        news.Dispose()
    End Sub

    Private Sub dgparts2_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts2.EditCommand
        news.Open()
        pmid = lblpmid.Value
        tasknum = lbltasknum.Value
        dgparts2.EditItemIndex = e.Item.ItemIndex
        GetSTasks2(pmid, tasknum)
        news.Dispose()
    End Sub

    Private Sub dgparts2_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts2.CancelCommand
        dgparts2.EditItemIndex = -1
        news.Open()
        pmid = lblpmid.Value
        tasknum = lbltasknum.Value
        GetSTasks2(pmid, tasknum)
        news.Dispose()
    End Sub

    Private Sub dgparts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        'temp for demo
        dgparts.EditItemIndex = -1
        news.Open()
        pmid = lblpmid.Value
        Dim dhrs, dmins, daps, dhre, dmine, dape As DropDownList
        wonum = lblwo.Value
        Dim tentry As String = lbltentry.Value
        Dim acost As String = lblacost.Value


        Dim pmtid As String = CType(e.Item.FindControl("lblpmtidi"), Label).Text

        dhrs = CType(e.Item.FindControl("ddhrs"), DropDownList)
        dmins = CType(e.Item.FindControl("ddmins"), DropDownList)
        daps = CType(e.Item.FindControl("ddaps"), DropDownList)
        dhre = CType(e.Item.FindControl("ddhre"), DropDownList)
        dmine = CType(e.Item.FindControl("ddmine"), DropDownList)
        dape = CType(e.Item.FindControl("ddape"), DropDownList)

        Dim shrs, smins, saps, shre, smine, sape
        shrs = dhrs.SelectedValue
        smins = dmins.SelectedValue
        saps = daps.SelectedValue
        shre = dhre.SelectedValue
        smine = dmine.SelectedValue
        sape = dape.SelectedValue

        Dim sdate, sedate, mins As String
        Dim ndate, nedate As Date

        Dim wd, wd1, wd2, wd3 As String
        wd1 = DatePart(DateInterval.Month, Now)
        wd2 = DatePart(DateInterval.Day, Now)
        wd3 = DatePart(DateInterval.Year, Now)
        wd = wd1 & "/" & wd2 & "/" & wd3

        If tentry = "lvst" Then
            If shrs <> "NA" Then
                sdate = wd & " " & shrs & ":" & smins & ":00 " & saps
                ndate = CType(sdate, Date)
            End If

            If shre <> "NA" Then
                sedate = wd & " " & shre & ":" & smine & ":00 " & sape
                nedate = CType(sedate, Date)
            End If

            Dim tmins As Integer = DateDiff(DateInterval.Minute, ndate, nedate)

            If shrs <> "NA" And shre <> "NA" Then
                sql = "update pmtracktpm set starttime = '" & ndate & "', stoptime = '" & nedate & "' " _
                + "acttime = '" & tmins & "' where pmtid = '" & pmtid & "' "
            ElseIf shrs <> "NA" And shre = "NA" Then
                sql = "update pmtracktpm set starttime = '" & ndate & "' where pmtid = '" & pmtid & "'"
            ElseIf shrs = "NA" & shre <> "NA" Then
                Dim strMessage As String =  tmod.getmsg("cdstr1727" , "tpmtime.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        Else
            mins = CType(e.Item.FindControl("txtcostm"), TextBox).Text
            If mins = "" Then
                mins = "0"
            End If
            Dim minchk As Long
            Try
                minchk = System.Convert.ToDecimal(mins)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1728" , "tpmtime.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            sql = "update pmtracktpm set acttime = '" & mins & "' " _
            + "where pmtid = '" & pmtid & "'" 
        End If

        news.Update(sql)

        tasknum = lbltasknum.Value
        GetSTasks(pmid, tasknum)
        news.Dispose()
    End Sub

    Private Sub dgparts2_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts2.UpdateCommand
        'temp for demo
        dgparts2.EditItemIndex = -1
        news.Open()
        pmid = lblpmid.Value
        Dim pmtid, mins, rd As String
        rd = CType(e.Item.FindControl("lblrecrd"), Label).Text
        If rd <> "0" Then
            pmtid = CType(e.Item.FindControl("lblpmtida"), Label).Text
            mins = CType(e.Item.FindControl("txtcostmd"), TextBox).Text
            If mins = "" Then
                mins = "0"
            End If
            Dim minchk As Long
            Try
                minchk = System.Convert.ToDecimal(mins)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1729" , "tpmtime.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            sql = "update pmtracktpm set actrd = '" & mins & "' " _
            + "where pmtid = '" & pmtid & "'"
            news.Update(sql)
        End If

        tasknum = lbltasknum.Value
        GetSTasks2(pmid, tasknum)
        news.Dispose()
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgparts.Columns(0).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts","0")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(1).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts","1")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(4).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts","4")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(6).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts","6")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(7).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts","7")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(10).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts","10")
		Catch ex As Exception
		End Try
		Try
			dgparts2.Columns(0).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts2","0")
		Catch ex As Exception
		End Try
		Try
			dgparts2.Columns(1).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts2","1")
		Catch ex As Exception
		End Try
		Try
			dgparts2.Columns(4).HeaderText = dlabs.GetDGPage("tpmtime.aspx","dgparts2","4")
		Catch ex As Exception
		End Try

	End Sub

End Class
