<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmget.aspx.vb" Inherits="lucy_r12.tpmget" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmget</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/taskgridtpm.js"></script>
		<script language="JavaScript" src="../scripts1/tpmgetaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkpg();" >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 4px; POSITION: absolute; TOP: 0px" cellSpacing="0" cellPadding="1"
				width="740">
				<tr>
					<td width="92"></td>
					<td width="180"></td>
					<td width="22"></td>
					<td width="24"></td>
					<td width="422"></td>
				</tr>
				<tr id="deptdiv" runat="server">
					<td class="label"><asp:Label id="lang3575" runat="server">Department</asp:Label></td>
					<td class="label"><asp:dropdownlist id="dddepts" runat="server" Width="170px" AutoPostBack="True" CssClass="plainlabel"></asp:dropdownlist></td>
					<td class="label"><IMG id="imgsw" onmouseover="return overlib('Clear Screen and Start Over', LEFT)" onclick="undoloc();"
							onmouseout="return nd()" src="../images/appbuttons/minibuttons/switch.gif" runat="server"></td>
					<td class="label"></td>
					<td class="label"><asp:label id="lbldeptdesc" runat="server" CssClass="label" Font-Size="X-Small" Font-Names="Arial"
							Font-Bold="True"></asp:label></td>
				</tr>
				<tr class="details" id="celldiv" runat="server">
					<td class="label"><asp:Label id="lang3576" runat="server">Station/Cell</asp:Label></td>
					<td class="label"><asp:dropdownlist id="ddcells" runat="server" Width="170px" AutoPostBack="True" CssClass="plainlabel"></asp:dropdownlist></td>
					<td class="label"></td>
					<td class="label"></td>
					<td class="label"><asp:label id="lblcelldesc" runat="server" CssClass="label" Font-Size="X-Small" Font-Names="Arial"
							Font-Bold="True"></asp:label></td>
				</tr>
				<tr class="details" id="eqdiv" runat="server">
					<td class="label" style="HEIGHT: 1px"><asp:Label id="lang3577" runat="server">Equipment#</asp:Label></td>
					<td class="label" style="HEIGHT: 1px"><asp:dropdownlist id="ddeq" runat="server" Width="170px" AutoPostBack="True" CssClass="plainlabel"></asp:dropdownlist></td>
					<td class="label" style="HEIGHT: 1px"></td>
					<td class="label" style="HEIGHT: 1px"></td>
					<td class="label" style="HEIGHT: 1px"><asp:label id="lbleqdesc" runat="server" CssClass="label" Font-Size="X-Small" Font-Names="Arial"
							Font-Bold="True"></asp:label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3578" runat="server">Location</asp:Label></td>
					<td class="label"><asp:label id="lblloc" runat="server" Width="170px" CssClass="plainlabel" Height="20px" BorderColor="#8080FF"
							BorderWidth="1px" BorderStyle="Solid"></asp:label></td>
					<td class="label"><IMG onmouseover="return overlib('Use Locations', LEFT)" onclick="srchloc();" onmouseout="return nd()"
							src="../images/appbuttons/minibuttons/useloc.gif"></td>
					<td class="label"></td>
				</tr>
			</table>
			<input id="lblsid" type="hidden" runat="server"> <input id="lbllog" type="hidden" runat="server">
			<input id="lbldept" type="hidden" runat="server"> <input id="lblpar" type="hidden" runat="server">
			<input id="lblchk" type="hidden" runat="server"> <input id="lblpar2" type="hidden" runat="server">
			<input id="lbltyp" type="hidden" runat="server"> <input id="lblclid" type="hidden" runat="server">
			<input id="lblpchk" type="hidden" runat="server"><input id="lblcleantasks" type="hidden" runat="server">
			<input id="lbltaskcnt" type="hidden" runat="server"> <input id="lblgetarch" type="hidden" runat="server">
			<input id="lblgototasks" type="hidden" runat="server"> <input id="lbllid" type="hidden" runat="server">
			<input id="lbleqid" type="hidden" runat="server"><input id="appchk" type="hidden" runat="server">
			<input type="hidden" id="lblts" runat="server"> <input type="hidden" id="lblfuid" runat="server">
			<input type="hidden" id="lblcoid" runat="server"> <input type="hidden" id="lbltl" runat="server">
			<input type="hidden" id="lblislabor" runat="server"> <input type="hidden" id="lbluserid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
