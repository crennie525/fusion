Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Izenda.AdHoc

Partial Class Dashboards
	Inherits System.Web.UI.Page

	Public Class ReportCachedInfo
		Public Category As String
		Public Name As String
		Public FullName As String
		Public Dashboard As Boolean
	End Class

	Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
		Session("sFrom") = Nothing
		Session("sTo") = Nothing
	End Sub

	Public Sub UpdateReferenceGenerated(ByVal href As String)
		updateRef.Attributes.Add("onclick", href)
	End Sub

	Public Function GetReportName(ByVal cat As String, ByVal repN As String) As String
		Dim result As String
		result = repN
		If ((Not String.IsNullOrEmpty(cat)) AndAlso cat <> "Uncategorized") Then
			result = cat & AdHocSettings.CategoryCharacter & result
		End If
		Return result
	End Function

	Public Function CropSlashes(ByVal s As String) As String
		Dim res As String
		res = s
		While res.Contains("\\")
			res = res.Replace("\\", "\")
		End While
		Return res
	End Function

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		Dim cachedInfos As ReportInfo()
		cachedInfos = AdHocSettings.AdHocConfig.FilteredListReports()
		Dim rn As String
		rn = Request.Params("rn")
		If (AdHocContext.CurrentReportSet.IsDashBoard) Then
			Try
				If (AdHocContext.CurrentReportSet.ReportName = "Dashboard Preview") Then
					rn = AdHocContext.CurrentReportSet.ReportName
				End If
			Catch ex As Exception
			End Try
		ElseIf (String.IsNullOrEmpty(Request.Params("rn"))) Then
			Dim demoUrl As String
			Dim normalUrl As String
			demoUrl = ""
			normalUrl = ""
			For Each ReportInfo As ReportInfo In cachedInfos
				If (ReportInfo.Dashboard) Then
					If (String.IsNullOrEmpty(normalUrl)) Then
						normalUrl = "Dashboards.aspx?rn=" & HttpUtility.UrlEncode(ReportInfo.FullName)
					End If
					If (String.IsNullOrEmpty(demoUrl) AndAlso ReportInfo.Category.ToLower() = "department reports" AndAlso ReportInfo.Name.ToLower() = "dashboard with map") Then
						demoUrl = "Dashboards.aspx?rn=" & HttpUtility.UrlEncode(ReportInfo.FullName)
					End If
				End If
			Next
			If (Not String.IsNullOrEmpty(demoUrl)) Then
				Response.Redirect(demoUrl)
			Else
				If (String.IsNullOrEmpty(normalUrl)) Then
					normalUrl = "ReportDesigner.aspx?clear=1"
				End If
				Response.Redirect(normalUrl)
			End If
			Response.End()
			Return
		End If
		crv.NoToolbar = True
		crv.UpdateReferenceGenerated = AddressOf UpdateReferenceGenerated
		Dim currentCat As String
		currentCat = Request.Params("catSel")
		If (String.IsNullOrEmpty(currentCat) And (Not (String.IsNullOrEmpty(rn)))) Then
			Dim nodes As String()
			nodes = CropSlashes(rn).Split("\")
			currentCat = "Uncategorized"
			If (nodes.Length > 1) Then
				currentCat = nodes(0)
			End If
		End If
		catSel.Items.Clear()
		Dim cats As Dictionary(Of String, String)
		cats = New Dictionary(Of String, String)()
		Dim frs As Dictionary(Of String, List(Of String))
		frs = New Dictionary(Of String, List(Of String))()
		For Each info As ReportInfo In cachedInfos
			If (Not info.Dashboard) Then
				Continue For
			End If
			Dim cat As String
			cat = info.Category
			If (String.IsNullOrEmpty(cat)) Then
				cat = "Uncategorized"
			End If
			If (Not cats.ContainsKey(cat)) Then
				If (String.IsNullOrEmpty(currentCat)) Then
					currentCat = cat
				End If
				cats.Add(cat, "")
			End If
			If (Not frs.ContainsKey(cat)) Then
				frs.Add(cat, New List(Of String)())
			End If
			frs(cat).Add(info.Name)
		Next
		For Each cat As String In cats.Keys
			Dim li As ListItem
			li = New ListItem(cat)
			If (cat = currentCat) Then
				li.Selected = True
			End If
			Dim sortedRs As String()
			sortedRs = frs(cat).ToArray()
			Array.Sort(sortedRs)
			li.Attributes.Add("catName", cat)
			li.Attributes.Add("tabInd", "0")
			li.Attributes.Add("repName", GetReportName(cat, sortedRs(0)).Replace("\", "\\"))
			catSel.Items.Add(li)
		Next
		Dim currentTab As Integer
		currentTab = -1
		Dim tabsL As List(Of String)
		tabsL = New List(Of String)()
		For Each info As ReportInfo In cachedInfos
			Dim iCat As String
			iCat = info.Category
			If (String.IsNullOrEmpty(info.Category)) Then
				iCat = "Uncategorized"
			End If
			If ((Not info.Dashboard) OrElse iCat <> currentCat) Then
				Continue For
			End If
			tabsL.Add(info.Name)
		Next
		Dim tabs As String()
		tabs = tabsL.ToArray()
		Array.Sort(tabs)
		If (currentTab >= tabs.Length) Then
			currentTab = tabs.Length - 1
		End If
		Dim tabsStr As String
		tabsStr = ""
		Dim tabsOtherStr As String
		tabsOtherStr = ""
		Dim index As Integer
		For index = tabs.Length - 1 To 0
			Dim reportName As String
			reportName = tabs(index)
			Dim curRn As String
			curRn = GetReportName(currentCat, reportName)
			If ((Not (String.IsNullOrEmpty(curRn))) AndAlso (Not (String.IsNullOrEmpty(rn)))) Then
				If (currentTab < 0 AndAlso CropSlashes(curRn.Replace("\", "\\")) = CropSlashes(rn)) Then
					currentTab = index
				End If
			End If
			Dim clickScript As String
			clickScript = ""
			If (currentTab <> index) Then
				clickScript = " onclick=""GoToCatTab('" & GetReportName(currentCat, reportName).Replace("\", "\\") & "');"""
			Else
				'dbTitle.InnerHtml = reportName;
				'viewButton.HRef = "ReportViewer.aspx?rn=" & HttpUtility.UrlEncode(GetReportName(currentCat, tabs[index]));
				'designButton.HRef = "DashboardDesigner.aspx?rn=" & HttpUtility.UrlEncode(GetReportName(currentCat, tabs[index]));
			End If
			tabsStr = tabsStr & "<li><a href=""#tabs-" & index & """" & clickScript & ">" & tabs(index) & "</a></li>"
			tabsOtherStr = tabsOtherStr & "<div id=""tabs-" & index & """ class=""dashboard-tab""></div>"
		Next
		If (AdHocSettings.ShowDesignLinks) Then
			'//tabsStr += "<li><a href=\"#tabs-1000\" onclick=\"window.location = 'DashboardDesigner.aspx?clear=1';\">+</a></li>";
			tabsStr += "<li><a id=""addDashboardLink"" href=""#tabs-1000"">+</a></li>"
			tabsOtherStr += "<div id=""tabs-1000"" class=""dashboard-tab""></div>"
		End If
		dbList.Controls.Add(New LiteralControl(tabsStr))
		otherTabs.Controls.Add(New LiteralControl(tabsOtherStr))
		If (currentTab >= 0) Then
			Dim currentTabInv As Integer
			currentTabInv = tabs.Length - currentTab - 1
			Session("StartUpScriptDBS") = "<script language='javascript'>function SwitchTab() {$(""#cdTabs"").tabs({ active: " & currentTabInv & "});}</script>"
		End If
		If (String.IsNullOrEmpty(rn) AndAlso Not (String.IsNullOrEmpty(currentCat))) Then
			Dim sortedRs As String()
			sortedRs = frs(currentCat).ToArray()
			Array.Sort(sortedRs)
			Session("ReloadScriptDBS") = "<script language='javascript'>GoToCatTab('" & GetReportName(currentCat, sortedRs(0)).Replace("\", "\\") & "');</script>"
		Else
			Session("ReloadScriptDBS") = ""
		End If
	End Sub

	Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
		Dim rs As Object
		rs = Session("ReloadScriptDBS")
		If (rs = Nothing) Then
			Return
		End If
		Dim r As String
		r = rs.ToString()
		Dim o As Object
		o = New Object()
		If (Not (String.IsNullOrEmpty(r))) Then
			Page.ClientScript.RegisterStartupScript(o.GetType(), "reloadF", r)
		End If
		o = Session("StartUpScriptDBS")
		If (o Is Nothing) Then
			Return
		End If
		Dim s As String
		s = o.ToString()
		Dim o2 As Object
		o2 = New Object()
		If (Not (String.IsNullOrEmpty(s))) Then
			Page.ClientScript.RegisterStartupScript(o2.GetType(), "switchTabF", s)
		End If
	End Sub
End Class

