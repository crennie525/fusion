﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dashboarddisplay.aspx.vb" Inherits="lucy_r12.dashboarddisplay" %>

<%@ Register src="../menu/mmenu1.ascx" tagname="mmenu1" tagprefix="uc1" %>
<%@ Register TagPrefix="cc1" Namespace="Izenda.Web.UI" Assembly="Izenda.AdHoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
    <form id="form1" runat="server">
    <cc1:ReportViewer ID="HtmlOutputReportResults" runat="server"></cc1:ReportViewer>
    
    </form>
    <uc1:mmenu1 ID="mmenu11" runat="server" />
</body>
</html>
