<%@ Control AutoEventWireup="true" %>


    <iframe style="display:none" name="reportFrame" id='reportFrame' width='0' height='0'></iframe>
    <div id="loadingrv2" style="z-index:500;top:0px;left:0px;width:100%;background-color:#FFFFFF;position:fixed;display:none;text-align:center;vertical-align:middle;">
        Loading...<br />
        <img alt="" src="rs.aspx?image=loading.gif" />
    </div>
    
    <div id="saveAsDialog" style="z-index:515;top:0px;left:0px;width:100%;position:absolute;display:none;text-align:center;vertical-align:middle;">
    <div style="padding:20px; border-style:solid; border-width:1px;  background-color:#FFFFFF;  display: table;   margin: 0 auto;">
    <div>Input report name</div>
    <div><input type="text" id="newReportName" style="width:200px;margin:0px; border-style:solid; border-width:1px;" value="" /></div>
    <div style="margin-top:5px;">Category</div>
    <div><select onchange="CheckNewCatName();" id="newCategoryName" style="width:206px; border-style:solid; border-width:1px;"></select></div>
    <div style="margin-top:5px;">
            <div class="f-button" style="margin-bottom: 4px;">
            	<a class="blue" onclick="javascript:SaveReportAs();" href="javascript:void(0);" style="width:50px;"><span class="text">OK</span></a>
            </div>
                <div class="f-button">
                	<a class="gray" onclick="javascript:CancelSave();" href="javascript:void(0);" style="width:120px;"><span class="text">Cancel</span></a>
                </div>
    </div>
    </div>
    </div>
    
    <div id="newCatDialog" style="z-index:515;top:0px;left:0px;width:100%;position:absolute;display:none;text-align:center;vertical-align:middle;">
    <div style="padding:20px; border-style:solid; border-width:1px; background-color:#FFFFFF;   display: table;   margin: 0 auto;">
    <div>New category name</div>
    <div><input type="text" id="addedCatName" style="width:200px;margin:0px;border-style:solid; border-width:1px;" value="" /></div>
    <div style="margin-top:5px;">
            <div class="f-button" style="margin-bottom: 4px;">
            	<a class="blue" onclick="javascript:AddNewCategory();" href="javascript:void(0);" style="width:120px;"><span class="text">Create</span></a>
            </div>
                <div class="f-button">
                	<a class="gray" onclick="javascript:CancelAddCategory();" href="javascript:void(0);" style="width:120px;"><span class="text">Cancel</span></a>
                </div>
    </div>
    </div>
    </div>
    
    <div id="popupEsDialog" style="z-index:515;top:0px;left:0px;width:100%;position:absolute;display:none;text-align:center;vertical-align:middle;">
    <div style="padding:20px; background-color:#FFFFFF; border-style:solid; border-width:1px; display: table; margin: 0 auto; width:500px;">
    <div id="epdContent"></div>
    <div style="margin-top:5px;">
            <div class="f-button" style="margin-bottom: 4px;">
            	<a class="blue" onclick="javascript:HidePopupDialog(true);" href="javascript:void(0);" style="width:50px;"><span class="text">OK</span></a>
            </div>
                <div class="f-button">
                	<a class="gray" onclick="javascript:HidePopupDialog(false);" href="javascript:void(0);" style="width:120px;"><span class="text">Cancel</span></a>
                </div>
    </div>
    </div>
    </div>
    
	<div class="btn-toolbar" style="margin: 4px 38px; z-index: 6; position: relative; width: 50%;">
	<div class="btn-group">
		<a class="btn" id="rlhref" href="ReportList.aspx" title="Report list">
			<img class="icon" src="rs.aspx?image=ModernImages.report-list.png" alt="Report list" />
			<span class="hide">Report list</span>
		</a>
	</div>	
	<div class="btn-group cool designer-only hide-locked hide-viewonly" id="saveControls">
		<button type="button" class="btn" title="Save" id="btnSaveDirect" onclick="javascript:event.preventDefault();SaveReportSet();">
			<img class="icon" src="rs.aspx?image=ModernImages.floppy.png" alt="Save" />
			<span class="hide">Save</span>
		</button>
		<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li class="hide-readonly"><a href="javascript:void(0)" style="min-width: 18em;"
				onclick="javascript:SaveReportSet();">
				<img class="icon" src="rs.aspx?image=ModernImages.save-32.png" alt="Save changes" />
				<b>Save</b><br>
				Save changes to the report for everyone it is shared with
			</a></li>
			<li><a href="javascript:void(0)" 
				onclick="javascript:ShowSaveAsDialog();">
				<img class="icon" src="rs.aspx?image=ModernImages.save-as-32.png" alt="Save a copy" />
				<b>Save As</b><br>
				Save a copy with a new name, keeping the original intact
			</a></li>
		</ul>
	</div>
	<div class="btn-group cool">
		<button type="button" class="btn" title="Print" 
			onclick="responseServer.OpenUrl('rs.aspx?p=htmlreport&print=1', 'aspnetForm', '');">
			<img class="icon" src="rs.aspx?image=ModernImages.print.png" alt="Printer" />
			<span class="hide">Print</span>
		</button>
		<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="javascript:void(0)" title="" style="min-width: 18em;"
				onclick="responseServer.OpenUrl('rs.aspx?p=htmlreport&print=1', 'aspnetForm', '');">
				<img class="icon" src="rs.aspx?image=ModernImages.print-32.png" alt="" />
				<b>Print HTML</b><br>
				Print directly from your browser, the fastest way for modern browsers
			</a></li>
			<li><a href="javascript:void(0)" title="" onclick="responseServer.OpenUrlWithModalDialogNewCustomRsUrl('rs.aspx?output=PDF', 'aspnetForm', 'reportFrame', nrvConfig.ResponseServerUrl);">
				<img class="icon" src="rs.aspx?image=ModernImages.html-to-pdf-32.png" alt="" />
				<b>HTML-powered PDF</b><br>
				One-file compilation of all the report's pages
				</a></li>
<!--			<li><a href="javascript:void(0)" title="" 
				onclick="responseServer.OpenUrlWithModalDialogNew('rs.aspx?output=PDF', 'aspnetForm', 'reportFrame');">
				<img class="icon" src="rs.aspx?image=ModernImages.pdf-32.png" alt="" />
				<b>Standard PDF</b><br>
				Non-HTML PDF generation
			</a></li>  -->
		</ul>
	</div>	
	<div class="btn-group cool">
		<button type="button" class="btn" title="Excel" 
			onclick="responseServer.OpenUrlWithModalDialogNewCustomRsUrl('rs.aspx?output=XLS(MIME)', 'aspnetForm', 'reportFrame', nrvConfig.ResponseServerUrl);">
			<img class="icon" src="rs.aspx?image=ModernImages.excel.png" alt="Get Excel file" />
			<span class="hide">Export to Excel</span>
		</button>
		<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="javascript:void(0)" title="" style="min-width: 18em;"
				onclick="responseServer.OpenUrlWithModalDialogNewCustomRsUrl('rs.aspx?output=XLS(MIME)', 'aspnetForm', 'reportFrame', nrvConfig.ResponseServerUrl);">
				<img class="icon" src="rs.aspx?image=ModernImages.xls-32.png" alt="" />
				<b>Export to Excel</b><br>
				File for Microsoft's spreadsheet application
			</a></li>
			<li><a href="javascript:void(0)" title="" 
				onclick="responseServer.OpenUrlWithModalDialogNewCustomRsUrl('rs.aspx?output=DOC', 'aspnetForm', 'reportFrame', nrvConfig.ResponseServerUrl);">
				<img class="icon" src="rs.aspx?image=ModernImages.word-32.png" alt="" />
				<b>Word document</b><br>
				File for Microsoft's word processor, most widely-used office application
			</a></li>
			<li><a href="javascript:void(0)" title="" 
				onclick="responseServer.OpenUrlWithModalDialogNewCustomRsUrl('rs.aspx?output=CSV', 'aspnetForm', 'reportFrame', nrvConfig.ResponseServerUrl);">
				<img class="icon" src="rs.aspx?image=ModernImages.csv-32.png" alt="" />
				<b>CSV</b><br>
				Stores tabular data in text file, that can be used in Google Docs
			</a></li>
			<li><a href="javascript:void(0)" title="" 
				onclick="responseServer.OpenUrlWithModalDialogNewCustomRsUrl('rs.aspx?output=XML', 'aspnetForm', 'reportFrame', nrvConfig.ResponseServerUrl);">
				<img class="icon" src="rs.aspx?image=ModernImages.xml-32.png" alt="" />
				<b>XML</b><br>
				Both human-readable and machine-readable text file
			</a></li>
			<li><a href="javascript:void(0)" title="" 
				onclick="responseServer.OpenUrlWithModalDialogNewCustomRsUrl('rs.aspx?output=RTF', 'aspnetForm', 'reportFrame', nrvConfig.ResponseServerUrl);">
				<img class="icon" src="rs.aspx?image=ModernImages.rtf-32.png" alt="" />
				<b>RTF</b><br>
				File format for cross-platform document interchange
			</a></li>
			<!--<li><a href="javascript:void(0)" title="" 
				onclick="responseServer.OpenUrlWithModalDialogNewCustomRsUrl('rs.aspx?output=ODT', 'aspnetForm', 'reportFrame', nrvConfig.ResponseServerUrl);">
				<img class="icon" src="rs.aspx?image=ModernImages.open-office-32.png" alt="" />
				<b>Open Office</b><br>
				File for open-source office software suite 
			</a></li>-->
		</ul>
	</div>
	<div class="btn-group">
		<button type="button" class="btn" title="Send report" 
			onclick="InitiateEmail();">
			<img class="icon" src="rs.aspx?image=ModernImages.mail.png" alt="Send report" />
			<span class="hide">Send report</span>
		</button>
	</div>	
	<div class="btn-group cool" data-toggle="buttons-radio">
		<button type="button" class="btn" title="Results per page" onclick="">
			<img class="icon" id="resNumImg" src="rs.aspx?image=ModernImages.rows-100.png" alt="Results per page" />
			<span class="hide">Results per page</span>
		</button>
		<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li onclick="ChangeTopRecords(1, true);" id="resNumLi0"><a href="javascript:void(0)" title="" style="min-width: 12em;">
				<img class="icon" src="rs.aspx?image=ModernImages.result-1-32.png" alt="" />
				<b>1 Result</b><br />
				Ideal for large forms
			</a></li>
			<li onclick="ChangeTopRecords(10, true);" id="resNumLi1"><a href="javascript:void(0)" title="">
				<img class="icon" src="rs.aspx?image=ModernImages.results-10-32.png" alt="" />
				<b>10 Results</b><br />
				Good for single parameter reports
			</a></li>
			<li onclick="ChangeTopRecords(100, true);" id="resNumLi2"><a href="javascript:void(0)" title="">
				<img class="icon" src="rs.aspx?image=ModernImages.results-100-32.png" alt="" />
				<b>100 Results</b><br />
				Default and recommended value
			</a></li>
			<li onclick="ChangeTopRecords(1000, true);" id="resNumLi3"><a href="javascript:void(0)" title="">
				<img class="icon" src="rs.aspx?image=ModernImages.results-1000-32.png" alt="" />
				<b>1000 Results</b><br />
				Good for larger reports
			</a></li>
			<li class="divider"></li>
			<li onclick="ChangeTopRecords(-1, true);" id="resNumLi4"><a href="javascript:void(0)" title="">
				<img class="icon" src="rs.aspx?image=ModernImages.results-all-32.png" alt="" />
				<b>Show all results</b><br>
				Use carefully as this may overload the browser
			</a></li>
		</ul>
	</div>	
	<div class="btn-group">
		<button type="button" class="btn designer-only hide-locked hide-viewonly" title="Open in designer" id="designerBtn">
			<img class="icon" src="rs.aspx?image=ModernImages.design.png" alt="Open in designer" />
			<span class="hide">Open in designer</span>
		</button>
	</div>	
	</div>


<div class="tabbable" id="navdiv">
	<div style="position: relative; width: 100%; height: 0px; overflow: visible;">
	<div style="position: absolute; top: -1.8em; right: 0; width: 100%; z-index: 5;">
	<ul class="nav nav-tabs" style="margin:0px;">
		<li style="padding-left: 8px; display: none;">
			<select id="RowCount" name="RowCount" class="yaselect-select" title="Rows per page">
				<option value="1">1</option>
				<option value="10">10</option>
				<option value="100" selected="selected">100</option>
				<option value="1000">1000</option>
				<option value="-1">All</option>
			</select>
		</li>
    <li class="visibility-pivots designer-only hide-locked"><a href="#tab3" data-toggle="tab"><img src="rs.aspx?image=ModernImages.pivots.png" alt="" class="icon" />Pivots</a></li>
		<li class="designer-only hide-locked"><a href="#tab2" data-toggle="tab"><img src="rs.aspx?image=ModernImages.fields.png" alt="" class="icon" />Fields</a></li>
		<li id="tab1li"><a href="#tab1" data-toggle="tab" id="tab1a"><img src="rs.aspx?image=ModernImages.filter.png" alt="" class="icon" />Filters</a></li>
	</ul>
	</div>
	</div>
	<div id="repHeader"></div>
            <div id="updateBtnPC" class="f-button" style="margin-bottom: 4px; margin-left:40px;">
            	<a id="btnUpdateResultsC" class="blue" onclick="GetRenderedReportSet(true);" href="javascript:void(0);"><img src="rs.aspx?image=ModernImages.refresh-white.png" alt="Refresh" /><span class="text">Update results</span></a>
            </div>

	<div class="tab-content" id="tabsContentsDiv">
		<div class="tab-pane" id="tab1">
            <style></style>
	        <div id="htmlFilters">
	        </div>
		</div>
		<div class="tab-pane" id="tab2">
		  <div id="data-source-field" title="Field name">
          <div id="propertiesDiv">
              <table style="width:100%;">
                  <tr>
                      <td>
                          <div id="titleDiv" style="width:392px; text-align:left;text-transform:capitalize;color:#000000;background-color:#CCEEFF;padding:6px;"></div>
                            <table cellpadding="0" cellspacing="0">
                              <tr><td style="vertical-align:top">
                                  <table cellpadding="0" cellspacing="0">
                                      <tr><td style="padding-top:10px;">Description</td></tr>
                                      <tr><td><input id="propDescription" type="text" value="" style="width:400px;margin:0px;" /></td></tr>
                                      <tr><td style="padding-top:10px;">Format</td></tr>
                                      <tr><td>
                                          <select id="propFormats" style="margin:0px;width:406px;" >
                                          </select>
                                      </td></tr>
                                      <tr><td style="padding-top:10px;">Filter Operator</td></tr>
                                      <tr><td>
                                          <select id="propFilterOperators" style="margin:0px;width:406px;" >
                                          </select>
                                      </td></tr>                                      
                                  </table>
                              </td><td style="vertical-align:top; padding:20px;">
                              <table>
                                  <tr><td><input id="propTotal" type="checkbox" /><label style="cursor:pointer;" for="propTotal">Total</label></td></tr>
                                  <tr><td><input id="propVG" type="checkbox" /><label style="cursor:pointer;" for="propVG">Visual Group</label></td></tr>
                                  <tr><td><input type="checkbox" onchange="javascript:this.checked = false;UpdateMSV('labelJ');" /><label msv="L" msvs="L,M,R" id="labelJ" onclick="javascript:UpdateMSV('labelJ');" style="font-family:Courier New, monospace;font-size:11px;cursor:pointer;position:relative;left:-14px;top:-3px;" for="labelJ">L</label><label onclick="javscript:UpdateMSV('labelJ');" style="cursor:pointer;position:relative;left:-6px;" for="labelJ">Label Justification</label></td></tr>
                                  <tr><td><input type="checkbox" onchange="javascript:this.checked = false;UpdateMSV('valueJ');" /><label msv="&nbsp;" msvs="&nbsp;,L,M,R" id="valueJ" onclick="javascript:UpdateMSV('valueJ');" style="font-family:Courier New, monospace;font-size:11px;cursor:pointer;position:relative;left:-14px;top:-3px;" for="labelV">&nbsp;</label><label onclick="javascript:UpdateMSV('valueJ');" style="cursor:pointer;position:relative;left:-6px;" for="valueJ">Value Justification</label></td></tr>
                              </table>
                            </td></tr>
                          </table>
                      </td>
                  </tr>
              </table>
          </div>
        </div>
        <table id="fieldsCtlTable">
        <tr><td colspan="3">
		    <select id="dsUlList" onchange="DetectCurrentDs(this.value); wereChecked.length = 0; RefreshFieldsList();">
		    </select>
        </td></tr>
        <tr>
            <td>
            	<div id="remainingFieldsSel" class="field-selector-container"></div>
            </td>
            <td>
                <div style="float:left; width:60px; height:200px; text-align:center;">
                    <div class="f-button middle">
                    	<a class="gray" onclick="javascript:AddRemainingFields();" href="javascript:void(0);"><img src="rs.aspx?image=ModernImages.right-add-white.png" alt="Right" /><span class="text">Add</span></a>
                    </div>
                    <br />
                    <div class="f-button middle">
                    	<a class="gray" onclick="javascript:RemoveUsedFields();" href="javascript:void(0);"><img src="rs.aspx?image=ModernImages.left-remove-white.png" alt="Refresh" /><span class="text">Remove</span></a>
                    </div>
                </div>        
            </td>
            <td>
            	<div id="usedFieldsSel" class="field-selector-container" style=""></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            	<div class="f-button">
            		<a class="blue" onclick="javascript:updateFields();" href="javascript:void(0);"><img src="rs.aspx?image=ModernImages.refresh-white.png" alt="Refresh" /><span class="text">Update results</span></a>
            	</div>
            </td>
            <td>
                <div class="f-button right">
                	<a class="gray" id="fpButton" onclick="javascript:ShowFieldProperties();" href="javascript:void(0);"><img src="rs.aspx?image=ModernImages.properties-white.png" alt="Cancel" /><span class="text">Field properties</span></a>
                </div>                
                <div class="f-button right">
                	<a class="gray" onclick="javascript:MoveDown();" href="javascript:void(0);"><img src="rs.aspx?image=ModernImages.down-white.png" alt="Down" /><span class="text">Down</span></a>
                </div>                
                <div class="f-button right">
                	<a class="gray" onclick="javascript:MoveUp();" href="javascript:void(0);"><img src="rs.aspx?image=ModernImages.up-white.png" alt="Up" /><span class="text">Up</span></a>
                </div>
            </td>
        </tr>
        </table>
        <style type="text/css">
        </style>
		</div>
        <div class="tab-pane" id="tab3">
            <div class="pivot-selector" id="pivot-selector">
             </div>
             <div class="f-button">
             <a class="blue" name="update-button-pivots" href="javascript:void(0);" onclick="GetRenderedReportSet(true);"><img src="rs.aspx?image=ModernImages.refresh-white.png" alt="Refresh" /><span class="text">Update results</span></a>
             </div>
		</div>
	</div>

</div>
<script type="text/javascript">
	var urlSettings;
	var responseServer;
	var responseServerWithDelimeter;
	var switchTabAfterRefreshCycle = false;
	
	$(document).ready(function () {
		InitializeViewer();
	});
</script>
    
<div class="page">
	<div id="renderedReportDiv"></div>
</div>
