<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<%@ Page Title="Report list" Language="VB" MasterPageFile="~/Default.master" AutoEventWireup="true" Inherits="lucy_r12.ReportList" Codebehind="ReportList.aspx.vb" %>

<%@ Register Src="~/Resources/HTML/ReportList-Head.ascx" TagName="ccn1" TagPrefix="ccp1" %>
<%@ Register Src="~/Resources/HTML/ReportList-Body.ascx" TagName="ccn2" TagPrefix="ccp2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
<ccp1:ccn1 runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolder" runat="server">
<ccp2:ccn2 runat="server" />
</asp:Content>


