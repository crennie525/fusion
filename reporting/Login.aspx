<%@ Page Title="Login Page" Language="VB" MasterPageFile="Default.master" AutoEventWireup="true" Inherits="lucy_r12.Login" Codebehind="Login.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolder" Runat="Server">
<div class="page">


	<form id="form1" runat="server">
		<table style="width: 100%; height: 100%;">
			<tr>
				<td align=center>
					<table style="text-align: center">
						<tr>
							<td>
								User name</td>
							<td align="left">
								<asp:TextBox ID="userNameTextbox" runat="server" Width="100px"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="height: 26px">
								Password</td>
							<td style="height: 26px" align="left">
								<input id="userPassword" runat="server" type="password" style="width: 100px" /></td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<asp:Button ID="Button1" runat="server" Text="Login" Width="110px" OnClick="Button1_Click" />
							</td>
						</tr>
					</table>
			<asp:CustomValidator ID="loginValidator" runat="server" ErrorMessage="Login failed. Please check your user name and password and try again."></asp:CustomValidator></td>
			</tr>
		</table>

		<script runat="server">
			Public Function AuthenticateUser(ByVal userName As String, ByVal password As String, ByRef isAdmin As Boolean) As Boolean
				isAdmin = False
				If (userName.ToLower() = "admin" Or userName.ToLower() = "administrator") Then
					isAdmin = True
				End If
				Return True
			End Function

			Public Sub Button1_Click(ByVal sender As Object, ByVal args As EventArgs)
				loginValidator.IsValid = True
				Dim isAdmin As Boolean
				If (AuthenticateUser(userNameTextbox.Text, userPassword.Value, isAdmin)) Then
					HttpContext.Current.Session("UserName") = userNameTextbox.Text
					If (isAdmin) Then
						HttpContext.Current.Session("Role") = "Administrator"
					Else
						HttpContext.Current.Session("Role") = "RegularUser"
					End If
					FormsAuthentication.RedirectFromLoginPage(userNameTextbox.Text, False)
					Return
				End If
				loginValidator.IsValid = False
			End Sub
		</script>

		<div style="text-align: center;">
			&nbsp;</div>
	</form>


</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TrackerPlaceHolder" Runat="Server">
</asp:Content>

