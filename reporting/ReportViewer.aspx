<%@ Page Title="" Language="VB" MasterPageFile="Default.master" AutoEventWireup="true" Inherits="lucy_r12.ReportViewer" Codebehind="ReportViewer.aspx.vb" %>
<%@ Register Src="~/Resources/HTML/ReportViewer-Head.ascx" TagName="ccn1" TagPrefix="ccp1" %>
<%@ Register Src="~/Resources/HTML/ReportViewer-Body.ascx" TagName="ccn2" TagPrefix="ccp2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
<ccp1:ccn1 runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolder" Runat="Server">
<ccp2:ccn2 runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TrackerPlaceHolder" Runat="Server">
</asp:Content>
