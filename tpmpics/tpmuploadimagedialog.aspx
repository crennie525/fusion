<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmuploadimagedialog.aspx.vb" Inherits="lucy_r12.tpmuploadimagedialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Upload Image Dialog</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script language="JavaScript" src="../scripts/sessrefdialog.js"></script>
		<script language="JavaScript" src="../scripts1/tpmuploadimagedialogaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function handleexit() {
             window.returnValue = "ok";
             window.close();
         }
         function pageScroll() {
             window.scrollTo(0, top);
             //scrolldelay = setTimeout('pageScroll()', 200); 
         } 
         //-->
     </script>
	</HEAD>
	<body  onunload="handleexit();" onload="resetsess();pageScroll();">
		<form id="form1" method="post" runat="server">
			<iframe id="ifimg" runat="server" width="100%" height="100%" frameBorder="no"></iframe>
			<iframe id="ifsession" class="details" src="" frameBorder="no" runat="server" style="Z-INDEX: 0">
			</iframe><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
