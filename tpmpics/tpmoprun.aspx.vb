

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class tpmoprun
    Inherits System.Web.UI.Page
	Protected WithEvents lang3653 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3652 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3651 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3650 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3649 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3648 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3647 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3646 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3645 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, funcid, tasknum As String
    Dim news As New Utilities
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblbpage As System.Web.UI.WebControls.Label
    Protected WithEvents bfirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bnext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents blast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgmeas As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdtpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtools As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlubes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdparts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgsub As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblititles As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tdpline1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Dim dr As SqlDataReader
    Protected WithEvents lblmaxtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim PageNumber As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            funcid = "1073" 'Request.QueryString("funcid").ToString
            tasknum = "1" 'Request.QueryString("tasknum").ToString
            lbltasknum.Value = tasknum
            lblfuncid.Value = funcid
            news.Open()
            GetTask(funcid, tasknum)
            GetPics(funcid, tasknum)
            news.Dispose()
        Else
            tasknum = lbltasknum.Value
            funcid = lblfuncid.Value
            If Request.Form("lblsubmit") = "next" Then
                news.Open()
                GetNext()
                news.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                news.Open()
                PageNumber = lblmaxtask.Value
                lbltasknum.Value = PageNumber
                GetTask(funcid, PageNumber)
                GetPics(funcid, PageNumber)
                news.Dispose()
                lblsubmit.Value = ""
                Dim maxtask As String = lblmaxtask.Value
                lblbpage.Text = "Task " & PageNumber & " of " & maxtask
            ElseIf Request.Form("lblsubmit") = "prev" Then
                news.Open()
                GetPrev()
                news.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                news.Open()
                PageNumber = 1
                lbltasknum.Value = PageNumber
                GetTask(funcid, PageNumber)
                GetPics(funcid, PageNumber)
                news.Dispose()
                lblsubmit.Value = ""
                Dim maxtask As String = lblmaxtask.Value
                lblbpage.Text = "Task " & PageNumber & " of " & maxtask
            End If
        End If
    End Sub
    Private Sub GetNext()
        tasknum = lbltasknum.Value
        funcid = lblfuncid.Value
        Try
            Dim pg As Integer = lbltasknum.Value
            PageNumber = pg + 1
            lbltasknum.Value = PageNumber
            GetTask(funcid, PageNumber)
            GetPics(funcid, PageNumber)
            Dim maxtask As String = lblmaxtask.Value
            lblbpage.Text = "Task " & PageNumber & " of " & maxtask
        Catch ex As Exception
            news.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1735" , "tpmoprun.aspx.vb")
 
            news.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        tasknum = lbltasknum.Value
        funcid = lblfuncid.Value
        Try
            Dim pg As Integer = lbltasknum.Value
            PageNumber = pg - 1
            lbltasknum.Value = PageNumber
            GetTask(funcid, PageNumber)
            GetPics(funcid, PageNumber)
            Dim maxtask As String = lblmaxtask.Value
            lblbpage.Text = "Task " & PageNumber & " of " & maxtask
        Catch ex As Exception
            news.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1736" , "tpmoprun.aspx.vb")
 
            news.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetTask(ByVal funcid As String, ByVal tasknum As String)
        'sql = "select taskdesc from pmtaskstpm where funcid = '" & funcid & "' and tasknum = '" & tasknum & "'"
        Dim mtask As Integer
        sql = "select count(*) from pmtaskstpm where funcid = '" & funcid & "' and subtask = 0"
        mtask = news.Scalar(sql)
        lblmaxtask.Value = mtask
        sql = "usp_gettpmfuncoutput '" & funcid & "','" & tasknum & "'"
        Dim skill, task, func, comp, lubes, parts, tools As String
        lblbpage.Text = "Task " & tasknum & " of " & mtask
        dr = news.GetRdrData(sql)
        While dr.Read
            skill = "Operator / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            If dr.Item("subtask").ToString = "0" Then
                task = dr.Item("task").ToString
            End If
            func = dr.Item("func").ToString
            comp = dr.Item("compnum").ToString
            parts = dr.Item("parts").ToString
            tools = dr.Item("tools").ToString
            lubes = dr.Item("lubes").ToString
        End While
        dr.Close()
        tdtpm.InnerHtml = skill
        tdfunc.InnerHtml = func
        tdcomp.InnerHtml = comp
        If tools = "" Then
            tools = "None"
        End If
        If lubes = "" Then
            lubes = "None"
        End If
        If parts = "" Then
            parts = "None"
        End If
        tdtools.InnerHtml = tools
        tdlubes.InnerHtml = lubes
        tdparts.InnerHtml = parts
        txtdesc.Text = task
    End Sub
    Private Sub GetPics(ByVal funcid As String, ByVal tasknum As String)
        'imgeq.Attributes.Add("src", "../images/appimages/pmimg1.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/archlabel.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/archlabel.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/archlabel.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/archlabel.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/archlabel.gif")
        End If
        Dim pcnt As Integer
        sql = "select count(*) from tpmimages where funcid = '" & funcid & "' and tasknum = '" & tasknum & "'"
        pcnt = news.Scalar(sql)
        lblpcnt.Value = pcnt
        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.tpm_image, p.tpm_image_thumb, p.tpm_image_med, p.tpm_image_order, " _
            + "p.tpm_image_title, f.func " _
            + "from tpmimages p " _
            + "left join functions f on f.func_id = p.funcid where p.funcid = '" & funcid & "' and p.tasknum = '" & tasknum & "' order by p.tpm_image_order"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, img, picid, imgs As String
            dr = news.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("tpm_image").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("tpm_image").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)
                picid = dr.Item("pic_id").ToString
                order = dr.Item("tpm_image_order").ToString
                ititle = dr.Item("tpm_image_title").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If
                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If

                If ititles = "" Then
                    ititles = ititle
                Else
                    ititles += "," & ititle
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    imgeq.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid
                    tdpline1.InnerHtml = ititle
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblititles.Value = ititles
            lbliorders.Value = iorders
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3645.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3645")
        Catch ex As Exception
        End Try
        Try
            lang3646.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3646")
        Catch ex As Exception
        End Try
        Try
            lang3647.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3647")
        Catch ex As Exception
        End Try
        Try
            lang3648.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3648")
        Catch ex As Exception
        End Try
        Try
            lang3649.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3649")
        Catch ex As Exception
        End Try
        Try
            lang3650.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3650")
        Catch ex As Exception
        End Try
        Try
            lang3651.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3651")
        Catch ex As Exception
        End Try
        Try
            lang3652.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3652")
        Catch ex As Exception
        End Try
        Try
            lang3653.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lang3653")
        Catch ex As Exception
        End Try
        Try
            lblbpage.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lblbpage")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("tpmoprun.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmoprun.aspx", "Img1") & "')")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgmeas.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmoprun.aspx", "imgmeas") & "')")
            imgmeas.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsub.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmoprun.aspx", "imgsub") & "')")
            imgsub.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
