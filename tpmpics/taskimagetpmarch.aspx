<%@ Page Language="vb" AutoEventWireup="false" Codebehind="taskimagetpmarch.aspx.vb" Inherits="lucy_r12.taskimagetpmarch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>taskimagetpmarch</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/taskimagetpmarchaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 5px; POSITION: absolute; TOP: 0px" cellPadding="0">
				<tr>
					<td align="center" colSpan="3"><A onclick="getbig();" href="#"><IMG id="imgeq" height="216" src="../images/appimages/tpmimg1.gif" width="216" border="0"
								runat="server"></A></td>
				</tr>
				<tr>
					<td colSpan="3">
						<table width="260">
							<tr>
								<td class="bluelabel" width="80"><asp:Label id="lang3644" runat="server">Order</asp:Label></td>
								<td width="60"><asp:textbox id="txtiorder" runat="server" CssClass="plainlabel" Width="40px" ReadOnly="True"></asp:textbox></td>
								<td width="20"></td>
								<td width="20"></td>
								<td width="20"></td>
								<td width="20">
								</td>
								<td width="20"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
								<td width="20"><IMG id="Img1" onmouseover="return overlib('Open TPM Operator View Task Review', ABOVE, LEFT)"
										onclick="getops();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="134"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblfuncid" type="hidden" runat="server" NAME="lblfuncid"> <input id="lblfunc" type="hidden" runat="server" NAME="lblfunc">
			<input id="lbltasknum" type="hidden" runat="server" NAME="lbltasknum"> <input id="lblro" type="hidden" runat="server" NAME="lblro">
			<input id="lblcurrp" type="hidden" name="lblcurrp" runat="server"> <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
			<input id="lblimgid" type="hidden" name="lblimgid" runat="server"> <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
			<input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server"><input id="lblpcnt" type="hidden" name="lblpcnt" runat="server">
			<input id="lblstrfrom" type="hidden" runat="server" NAME="lblstrfrom"> <input id="lblcurrimg" type="hidden" runat="server" NAME="lblcurrimg">
			<input id="lblcurrbimg" type="hidden" runat="server" NAME="lblcurrbimg"> <input id="lblnsimage" type="hidden" runat="server" NAME="lblnsimage">
			<input id="lblbimgs" type="hidden" runat="server" NAME="lblbimgs"> <input id="lblititles" type="hidden" name="lblititles" runat="server">
			<input id="lbliorders" type="hidden" runat="server" NAME="lbliorders"><input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="lbloldorder" runat="server" NAME="lbloldorder"> <input type="hidden" id="lblretmed" runat="server" NAME="lblretmed">
			<input type="hidden" id="lblretbig" runat="server" NAME="lblretbig"><input type="hidden" id="lblret" runat="server" NAME="lblret">
			<input type="hidden" id="lbledit" runat="server" NAME="lbledit"> <input type="hidden" id="lblrev" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
