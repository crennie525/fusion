<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmuploadimage.aspx.vb" Inherits="lucy_r12.tpmuploadimage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmuploadimage</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/tpmimgtasknav.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/tpmuploadimageaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="GetsScroll();" >
		<form id="form1" method="post" runat="server">
			<table width="520">
				<TBODY>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang3654" runat="server">Image Upload Dialog</asp:Label></td>
					</tr>
					<tr>
						<td class="bluelabel" width="150"><asp:Label id="lang3655" runat="server">Choose Image to Upload</asp:Label></td>
						<td class="plainlabel" width="240"><INPUT id="MyFile" style="WIDTH: 230px" type="file" size="5" name="MyFile" RunAt="Server"></td>
						<td width="130"><input class="plainlabel" id="btnupload" type="button" value="Upload" name="btnupload"
								runat="server"></td>
					</tr>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang3656" runat="server">Image Details</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="3">
							<table>
								<tr>
									<td class="bluelabel" id="tdcurr" width="480" runat="server"><asp:Label id="lang3657" runat="server">Current Image</asp:Label></td>
									<td align="right" width="20"><IMG id="imgsavdet" onmouseover="return overlib('Save Image Details', ABOVE, LEFT)" onclick="savdets();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif" runat="server">
									</td>
									<td width="20"><IMG id="imgref" onmouseover="return overlib('Refresh Details for New Image Entry', ABOVE, LEFT)"
											onclick="refreshit();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/refreshit.gif"
											runat="server">
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="3">
							<div id="Div1" style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 520px; HEIGHT: 40px; OVERFLOW: auto; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								runat="server">
								<TABLE cellSpacing="0" cellPadding="1">
									<tr>
										<td align="center" colSpan="3">
											<TABLE cellSpacing="0" cellPadding="1">
												<TBODY>
													<TR>
														<TD class="bluelabel"><asp:Label id="lang3658" runat="server">Image Title</asp:Label></TD>
														<td><asp:textbox id="txtpictitle" runat="server" Width="160px" CssClass="plainlabel"></asp:textbox></td>
														<td class="bluelabel"><asp:Label id="lang3659" runat="server">Task Number</asp:Label></td>
														<td>
															<asp:DropDownList id="ddtasks" runat="server"></asp:DropDownList></td>
													</TR>
												</TBODY>
											</TABLE>
										</td>
									</tr>
								</TABLE>
							</div>
						</td>
					</tr>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang3660" runat="server">Current Images</asp:Label></td>
					</tr>
					<tr>
						<td class="plainlabelblue" align="center" colSpan="3"><asp:Label id="lang3661" runat="server">Click Image Once to View Details Above</asp:Label><br><asp:Label id="lang3662" runat="server">Double Click Image to View Full Scale</asp:Label></td>
					</tr>
					<tr>
						<td id="tblpics" align="center" colSpan="3" runat="server">
							<div id="picdiv" style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 520px; HEIGHT: 260px; OVERFLOW: auto; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								onscroll="SetsDivPosition();" runat="server"></div>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="3">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bfirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblbpage" runat="server" CssClass="bluelabel">Task 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bnext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td width="20"><IMG id="blast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
			<input id="lblfuncid" type="hidden" runat="server" NAME="lblblockid"> <input type="hidden" id="lbltasknum" runat="server">
			<input type="hidden" id="lblfunc" runat="server"><input id="lblimgid" type="hidden" runat="server" NAME="lblimgid">
			<input id="lblro" type="hidden" runat="server" NAME="lblro"><input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="lblpicorder" type="hidden" runat="server" NAME="lblpicorder"> <input id="lblsubmit" type="hidden" runat="server" NAME="lblsubmit">
			<input id="lblneworder" type="hidden" runat="server" NAME="lblneworder"> <input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder">
			<input id="lblmaxorder" type="hidden" runat="server" NAME="lblmaxorder"> <input id="lblititles" type="hidden" runat="server" NAME="lblititles">
			<input id="lblref" type="hidden" runat="server" NAME="lblref"><input type="hidden" id="lbloldtask" runat="server">
			<input type="hidden" id="lblmaxtask" runat="server"><input type="hidden" id="lbledit" runat="server">
			<input type="hidden" id="lblimg" runat="server" NAME="lblimg"> <input type="hidden" id="lblbimg" runat="server" NAME="lblbimg">
			<input type="hidden" id="lblnimg" runat="server" NAME="lblnimg">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
