<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmoprun.aspx.vb" Inherits="lucy_r12.tpmoprun" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>TPM Operator View Task Review</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/tpmimgtasknav.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/tpmoprunaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" width="1023">
				<tr>
					<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
					<td class="thdrsingrt label" width="374"><asp:Label id="lang3645" runat="server">Task Activity Details</asp:Label></td>
					<td width="5">&nbsp;</td>
					<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/3gearsh.gif" border="0"></td>
					<td class="thdrsingrt label" width="594"><asp:Label id="lang3646" runat="server">Task Image</asp:Label></td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr height="20">
								<td class="label"><asp:Label id="lang3647" runat="server">Current TPM</asp:Label></td>
								<td class="plainlabelblue" id="tdtpm" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang3648" runat="server">Current Function</asp:Label></td>
								<td class="plainlabelblue" id="tdfunc" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang3649" runat="server">Current Component</asp:Label></td>
								<td class="plainlabelblue" id="tdcomp" runat="server"></td>
							</tr>
						</table>
					</td>
					<td></td>
					<td rowspan="11" colspan="2" valign="top">
						<table width="571">
							<tr height="20">
								<td align="center" colSpan="3"><div style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 620px; PADDING-TOP: 10px; BORDER-BOTTOM: blue 1px solid; HEIGHT: 502px">
										<A href="#"><IMG id="imgeq" src="../images/appimages/tpmimg1.gif" border="0" runat="server"></A></div>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" id="tdpline1" runat="server" class="label"></td>
							</tr>
							<tr>
								<td align="center" colSpan="3">
									<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="134"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="32" valign="middle">
					<td align="center" colSpan="2">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bfirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblbpage" runat="server" CssClass="bluelabel">Task 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="bnext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="blast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="20">
					<td class="btmmenu label" vAlign="top" colspan="2"><asp:Label id="lang3650" runat="server">Task Description</asp:Label></td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td><asp:textbox id="txtdesc" Width="380px" TextMode="MultiLine" MaxLength="1000" Rows="3" Runat="server"
										cssclass="plainlabel" Height="120px" ReadOnly="True"></asp:textbox></td>
								<td valign="top">
									<IMG onmouseover="return overlib('View Sub-Tasks for this Task')" onclick="getSubDiv();"
										onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif" id="imgsub"
										runat="server"> <IMG onmouseover="return overlib('Check Failure Modes for this Task')" onclick="getFailDiv();"
										onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/checked.gif" id="Img1" runat="server">
									<IMG onmouseover="return overlib('Record Measurements for this Task')" onclick="getMeasDiv();"
										onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/measure.gif" id="imgmeas"
										runat="server">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="20">
					<td class="btmmenu label" vAlign="top" colspan="2"><asp:Label id="lang3651" runat="server">Task Tools</asp:Label></td>
				</tr>
				<tr height="20">
					<td class="plainlabelblue" id="tdtools" runat="server" colspan="2"></td>
				</tr>
				<tr height="20">
					<td class=" btmmenu label" vAlign="top" colspan="2"><asp:Label id="lang3652" runat="server">Task Lubricants</asp:Label></td>
				</tr>
				<tr height="20">
					<td class="plainlabelblue" id="tdlubes" runat="server" colspan="2"></td>
				</tr>
				<tr height="20">
					<td class="btmmenu label" vAlign="top" colspan="2"><asp:Label id="lang3653" runat="server">Task Parts</asp:Label></td>
				</tr>
				<tr height="20">
					<td class="plainlabelblue" id="tdparts" runat="server" colspan="2"></td>
				</tr>
				<tr height="150">
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
			<input type="hidden" id="lbltasknum" runat="server"> <input type="hidden" id="lblfuncid" runat="server">
			<input id="lblcurrp" type="hidden" name="lblcurrp" runat="server"> <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
			<input id="lblimgid" type="hidden" name="lblimgid" runat="server"> <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
			<input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server"><input id="lblpcnt" type="hidden" name="lblpcnt" runat="server">
			<input id="lblcurrimg" type="hidden" runat="server" NAME="lblcurrimg"> <input id="lblcurrbimg" type="hidden" runat="server" NAME="lblcurrbimg">
			<input id="lblbimgs" type="hidden" runat="server" NAME="lblbimgs"> <input id="lblititles" type="hidden" name="lblititles" runat="server">
			<input id="lbliorders" type="hidden" runat="server" NAME="lbliorders"> <input type="hidden" id="lblmaxtask" runat="server">
			<input type="hidden" id="lblsubmit" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
