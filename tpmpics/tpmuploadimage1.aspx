<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmuploadimage1.aspx.vb" Inherits="lucy_r12.tpmuploadimage1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tpmuploadimage1</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/tpmimgtasknav.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" type="text/javascript" src="../scripts1/tpmuploadimage1aspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     
     <!--
         function checkit() {
         
             var chk = document.getElementById("lblsubmit").value;
             if (chk == "return") {
                 window.parent.handleexit();
             }
         }
         //-->
     </script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table width="520">
				<TBODY>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang3663" runat="server">Image Upload Dialog</asp:Label></td>
					</tr>
					<tr>
						<td class="bluelabel" width="150"><asp:Label id="lang3664" runat="server">Choose Image to Upload</asp:Label></td>
						<td class="plainlabel" width="240"><INPUT id="MyFile" style="WIDTH: 230px" type="file" size="5" name="MyFile" RunAt="Server"></td>
						<td width="130"><asp:Button ID="btnimgup" runat="server" Text="Upload" /></td>
					</tr>
				</TBODY>
			</table>
			<input id="lblfuncid" type="hidden" runat="server" NAME="lblblockid"> <input type="hidden" id="lbltasknum" runat="server" NAME="lbltasknum">
			<input type="hidden" id="lblfunc" runat="server" NAME="lblfunc"><input id="lblimgid" type="hidden" runat="server" NAME="lblimgid">
			<input id="lblro" type="hidden" runat="server" NAME="lblro"><input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="lblpicorder" type="hidden" runat="server" NAME="lblpicorder"> <input id="lblsubmit" type="hidden" runat="server" NAME="lblsubmit">
			<input id="lblneworder" type="hidden" runat="server" NAME="lblneworder"> <input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder">
			<input id="lblmaxorder" type="hidden" runat="server" NAME="lblmaxorder"> <input id="lblititles" type="hidden" runat="server" NAME="lblititles">
			<input id="lblref" type="hidden" runat="server" NAME="lblref"><input type="hidden" id="lbloldtask" runat="server" NAME="lbloldtask">
			<input type="hidden" id="lblmaxtask" runat="server" NAME="lblmaxtask"><input type="hidden" id="lbledit" runat="server" NAME="lbledit">
			<input type="hidden" id="lblimg" runat="server" NAME="lblimg"> <input type="hidden" id="lblbimg" runat="server" NAME="lblbimg">
			<input type="hidden" id="lblnimg" runat="server" NAME="lblnimg">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
