

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class taskimagetpmarch
    Inherits System.Web.UI.Page
	Protected WithEvents lang3644 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim funcid, func, ro, tasknum, rev
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim news As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtiorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstrfrom As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnsimage As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblititles As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretmed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretbig As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbledit As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                funcid = Request.QueryString("funcid").ToString '"0" ' 
                'func = "Test Fuction" 'Request.QueryString("func").ToString '"0" ' 
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                rev = Request.QueryString("rev").ToString
                tasknum = Request.QueryString("tasknum").ToString '"0" ' 
                lblfuncid.Value = funcid
                lblfunc.Value = func
                lblro.Value = ro
                lbltasknum.Value = tasknum
                lblrev.Value = rev
                If ro = "1" Then
                    'imgdel.Disabled = True
                    'imgsavdet.Disabled = True
                    'imgsavdet.Attributes.Add("src", "../images/appbuttons/minibuttons/saveDisk1dis.gif")
                    'imgdel.Attributes.Add("src", "../images/appbuttons/minibuttons/deldis.gif")
                    lbledit.Value = "no"
                End If
                news.Open()
                LoadPics()
                news.Dispose()
            Catch ex As Exception

            End Try

        Else
                If Request.Form("lblsubmit") = "delimg" Then
                    lblsubmit.Value = ""
                    news.Open()
                    'DelImg()
                    LoadPics()
                    news.Dispose()
                    'lblchksav.Value = "yes"
                ElseIf Request.Form("lblsubmit") = "checkpic" Then
                    lblsubmit.Value = ""
                    news.Open()
                    LoadPics()
                    news.Dispose()
                ElseIf Request.Form("lblsubmit") = "changepic" Then
                    lblsubmit.Value = ""
                    news.Open()
                    'SavePics()
                    LoadPics()
                    news.Dispose()
                ElseIf Request.Form("lblsubmit") = "savedets" Then
                    lblsubmit.Value = ""
                    news.Open()
                    'SaveDets()
                    LoadPics()
                    news.Dispose()
                End If
        End If
    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        lblnsimage.Value = nsimage
        lblstrfrom.Value = strfrom
        'imgeq.Attributes.Add("src", "../images/appimages/pmimg1.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/archlabel.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/archlabel.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/archlabel.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/archlabel.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/archlabel.gif")
        End If

        Dim img, imgs, picid As String

        Dim funcid As String = lblfuncid.Value
        tasknum = lbltasknum.Value
        Dim pcnt As Integer
        sql = "select count(*) from tpmimagesarch where funcid = '" & funcid & "' and tasknum = '" & tasknum & "' and rev = '" & rev & "'"
        pcnt = news.Scalar(sql)
        lblpcnt.Value = pcnt
        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.tpm_image, p.tpm_image_thumb, p.tpm_image_med, p.tpm_image_order, " _
            + "p.tpm_image_title, f.func " _
            + "from tpmimagesarch p " _
            + "left join functionstpmarch f on f.func_id = p.funcid and f.rev = p.rev where p.funcid = '" & funcid & "' and p.tasknum = '" & tasknum & "' and p.rev = '" & rev & "' order by p.tpm_image_order"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs
            dr = news.GetRdrData(sql)
            While dr.Read
                iflag += 1
                lblfunc.Value = dr.Item("func").ToString
                img = dr.Item("tpm_image_med").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("tpm_image").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)
                picid = dr.Item("pic_id").ToString
                order = dr.Item("tpm_image_order").ToString
                ititle = dr.Item("tpm_image_title").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If
                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If

                If ititles = "" Then
                    ititles = ititle
                Else
                    ititles += "," & ititle
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    imgeq.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    'txtpictitle.Text = ititle
                    'tdpline2.InnerHtml = ititle


                    txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblititles.Value = ititles
            lbliorders.Value = iorders
        End If
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3644.Text = axlabs.GetASPXPage("taskimagetpmarch.aspx","lang3644")
		Catch ex As Exception
		End Try
		Try
			lblpg.Text = axlabs.GetASPXPage("taskimagetpmarch.aspx","lblpg")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			Img1.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("taskimagetpmarch.aspx","Img1") & "', ABOVE, LEFT)")
			Img1.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
