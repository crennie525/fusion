<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ForumMain.aspx.vb" Inherits="lucy_r12.ForumMain" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ForumMain</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" src="../scripts/msg.js"></script>
		<script language="JavaScript" src="../scripts1/ForumMainaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkit();"  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; LEFT: -2px; POSITION: absolute; TOP: 2px; background-color: transparent;" cellSpacing="0" cellPadding="0"
				width="980">
				<tr>
					<td vAlign="top" width="300">
						<table width="300">
							<tr height="20">
								<td class="thdrsingrt label" width="270" bgColor="#1d11ef" colSpan="4"><asp:Label id="lang2557" runat="server">Forum Options</asp:Label></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang2558" runat="server">Search Forums</asp:Label></td>
								<td class="plainlabel" colSpan="2"><input id="txtsrch" type="text" size="20" name="txtsrch" runat="server"></td>
								<td><IMG id="ibtnsrch" onclick="getsrch();" src="../images/appbuttons/minibuttons/srchsm.gif"
										runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label"><asp:Label id="lang2559" runat="server">Add a Forum</asp:Label></td>
								<td class="plainlabel" colSpan="2"><input id="txtnew" type="text" size="20" name="Text1" runat="server" maxLength="100"></td>
								<td><asp:imagebutton id="ibtnaddfor" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnew.gif"></asp:imagebutton></td>
							</tr>
							<tr>
								<td></td>
								<td colSpan="3"><asp:radiobuttonlist id="rbforopts" runat="server" CssClass="label" RepeatLayout="Flow" RepeatDirection="Horizontal"
										Width="170px">
										<asp:ListItem Value="Open" Selected="True">Open</asp:ListItem>
										<asp:ListItem Value="Secured">Project Secured</asp:ListItem>
									</asp:radiobuttonlist></td>
							</tr>
							<tr class="details">
								<td colSpan="4"><IMG height="4" src="../images/4PX.gif" width="4"></td>
							</tr>
							<tr class="details" height="20">
								<td class="thdrsingrt label" width="270" bgColor="#1d11ef" colSpan="4"><asp:Label id="lang2560" runat="server">Secure Forums Login</asp:Label></td>
							</tr>
							<tr class="details">
								<td class="label"><asp:Label id="lang2561" runat="server">Forum Login ID</asp:Label></td>
								<td class="plainlabel" colSpan="2"><input id="txtid" type="text" size="20" runat="server" maxLength="20"></td>
							</tr>
							<tr class="details">
								<td class="label"><asp:Label id="lang2562" runat="server">Forum Password</asp:Label></td>
								<td class="plainlabel" colSpan="2"><input id="txtps" type="password" size="20" runat="server" maxLength="20"></td>
							</tr>
							<tr class="details">
								<td class="bluelabel" colSpan="2"><asp:Label id="lang2563" runat="server">(Secure Forums Only)</asp:Label></td>
								<td align="right">
									<p class="lilmenu" onmouseover="this.className='lilmenuhov'" onmouseout="this.className='lilmenu'"
										align="left"><A class="plainlabel" onmouseover="this.className='labelu'" onclick="login();" onmouseout="this.className='plainlabel'"
											href="#"><asp:Label id="lang2564" runat="server">Login</asp:Label></A>
									</p>
								</td>
								<td></td>
							</tr>
							<tr>
								<td colSpan="4"><IMG height="4" src="../images/4PX.gif" width="4"></td>
							</tr>
							<tr>
								<td width="130"></td>
								<td width="50"></td>
								<td width="90"></td>
								<td width="20"></td>
							</tr>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr height="20">
								<td class="thdrsingrt label" width="270" bgColor="#1d11ef" colSpan="4"><asp:Label id="lang2565" runat="server">Open Forums</asp:Label></td>
							</tr>
							<tr>
								<td id="tdlist" colSpan="4" runat="server"></td>
							</tr>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr height="20">
								<td class="thdrsingrt label" width="270" bgColor="#1d11ef" colSpan="4"><asp:Label id="lang2566" runat="server">Secured Project Forums</asp:Label></td>
							</tr>
							<tr>
								<td id="tdlist1" colSpan="4" runat="server"></td>
							</tr>
						</table>
					</td>
					<td width="10"></td>
					<td vAlign="top" width="670">
						<table id="tdforum" width="670" border="0">
							<tr height="20">
								<td class="thdrsingrt label" bgColor="#1d11ef" colSpan="5"><asp:Label id="lang2567" runat="server">Forum Subjects</asp:Label></td>
							</tr>
							<tr>
								<td width="90"></td>
								<td width="190"></td>
								<td width="130"></td>
								<td width="30"></td>
								<td width="140"></td>
							</tr>
							<tr height="20">
								<td class="bluelabel" id="tdcurr" colSpan="2" runat="server"><asp:Label id="lang2568" runat="server">Current Forum:</asp:Label></td>
								<td align="right" colSpan="3"><A class="linklabelbld" onclick="addit();" href="#"><asp:Label id="lang2569" runat="server">Post New Subject</asp:Label></A>&nbsp;&nbsp;<A class="linklabelbld" id="A1" onclick="refrsh();" href="#" runat="server"><asp:Label id="lang2570" runat="server">Check for New Subjects</asp:Label></A>&nbsp;&nbsp;<A class="details" id="A3" onclick="addusers();" href="#" runat="server"><asp:Label id="lang2571" runat="server">Add Users</asp:Label></A></td>
							</tr>
							<tr height="20">
								<td class="btmmenu plainlabel" id="tdpgnavtop" runat="server"></td>
								<td class="btmmenu plainlabel" id="navtop" colSpan="4" runat="server"></td>
							</tr>
							<tr>
								<td><IMG height="4" src="../images/4PX.gif" width="4"></td>
							</tr>
							<tr>
								<td id="tdsubj" colSpan="5" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="btmmenu plainlabel" id="tdpgnavbot" runat="server"></td>
								<td class="btmmenu plainlabel" id="navbot" colSpan="4" runat="server"></td>
							</tr>
						</table>
						<table class="details" id="tdadd" width="670">
							<tr height="20">
								<td class="thdrsingrt label" bgColor="#1d11ef" colSpan="6"><asp:Label id="lang2572" runat="server">Add a Subject</asp:Label></td>
							</tr>
							<tr height="20">
								<td width="5" rowSpan="5">&nbsp;</td>
								<td class="label" style="HEIGHT: 26px" width="80"><asp:Label id="lang2573" runat="server">Forum</asp:Label></td>
								<td class="plainlabel" id="tdcurrent" style="HEIGHT: 26px" width="320" runat="server"></td>
							</tr>
							<tr height="20">
								<td class="label" width="80"><asp:Label id="lang2574" runat="server">Author</asp:Label></td>
								<td class="plainlabel" id="tdauth" width="320" runat="server"></td>
							</tr>
							<tr>
								<td class="label" width="80"><asp:Label id="lang2575" runat="server">Subject</asp:Label></td>
								<td class="plainlabel" width="320"><input id="txtsubj" type="text" maxLength="50" size="50" name="txtsubj" runat="server"></td>
							</tr>
							<tr>
								<td class="plainlabel" colSpan="2"><asp:textbox id="txtmsg" runat="server" Width="646px" TextMode="MultiLine" Height="394px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr height="26">
								<td align="right" colSpan="2">
									<table>
										<tr>
											<td><INPUT class="lilmenu plainlabel" onmouseover="this.className='lilmenu labelu'" style="WIDTH: 55px; HEIGHT: 26px"
													onclick="addsubj();" onmouseout="this.className='lilmenu plainlabel'" type="button"
													value="Submit">
											</td>
											<td><INPUT class="lilmenu plainlabel" onmouseover="this.className='lilmenu labelu'" style="WIDTH: 55px; HEIGHT: 24px"
													onclick="canit();" onmouseout="this.className='lilmenu plainlabel'" type="button" value="Cancel">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table class="details" id="tblreply">
							<tr height="20">
								<td class="thdrsingrt label" bgColor="#1d11ef" colSpan="5"><asp:Label id="lang2576" runat="server">Add a Reply</asp:Label></td>
							</tr>
							<tr height="24">
								<td width="5" rowSpan="5">&nbsp;</td>
								<td class="label" width="80"><asp:Label id="lang2577" runat="server">Forum</asp:Label></td>
								<td class="plainlabel" id="tdforumr" width="320" runat="server"></td>
							</tr>
							<tr height="24">
								<td class="label" width="80"><asp:Label id="lang2578" runat="server">Author</asp:Label></td>
								<td class="plainlabel" id="tdauthr" width="320" runat="server"></td>
							</tr>
							<tr height="24">
								<td class="label" width="80"><asp:Label id="lang2579" runat="server">Subject</asp:Label></td>
								<td class="plainlabel" id="tdsubjr" width="320" runat="server"></td>
							</tr>
							<tr>
								<td class="label" width="80"><asp:Label id="lang2580" runat="server">Reply Title</asp:Label></td>
								<td class="plainlabel" width="320"><input id="txtreply1" type="text" maxLength="50" size="50" name="txtsubj" runat="server"></td>
							</tr>
							<tr>
								<td class="plainlabel" colSpan="2"><asp:textbox id="txtmsgr" runat="server" Width="646px" TextMode="MultiLine" Height="374px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td></td>
								<td align="right" colSpan="2">
									<table>
										<tr>
											<td><INPUT class="lilmenu plainlabel" onmouseover="this.className='lilmenu labelu'" style="WIDTH: 55px; HEIGHT: 21px"
													onclick="addreply();" onmouseout="this.className='lilmenu plainlabel'" type="button"
													value="Submit">
											</td>
											<td><INPUT class="lilmenu plainlabel" onmouseover="this.className='lilmenu labelu'" style="WIDTH: 55px; HEIGHT: 20px"
													onclick="canit1();" onmouseout="this.className='lilmenu plainlabel'" type="button"
													value="Cancel">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table class="details" id="tblreplies" width="680" border="0">
							<TBODY>
								<tr height="20">
									<td class="thdrsingrt label" bgColor="#1d11ef" colSpan="6"><asp:Label id="lang2581" runat="server">Forum Replies</asp:Label></td>
								</tr>
								<tr>
									<td width="70"></td>
									<td width="80"></td>
									<td width="250"></td>
									<td width="100"></td>
									<td width="20"></td>
									<td width="160"></td>
								</tr>
								<tr height="26">
									<td class="bluelabel" id="tdcurr1" colSpan="3" runat="server"><asp:Label id="lang2582" runat="server">Current Forum:</asp:Label></td>
									<td align="right" colSpan="3"><A class="linklabelbld" onclick="ret();" href="#"><asp:Label id="lang2583" runat="server">Return to Subject List</asp:Label></A></td>
								</tr>
								<tr>
									<td class="btmmenu plainlabel" colSpan="6" height="20"><asp:Label id="lang2584" runat="server">Current Subject</asp:Label></td>
								</tr>
								<tr height="20">
									<td id="currpic" rowSpan="3" runat="server"></td>
									<td class="label"><asp:Label id="lang2585" runat="server">Original Post:</asp:Label></td>
									<td class="plainlabel" id="lblpostdate" colSpan="2" runat="server"></td>
									<td align="right" colSpan="2"><A class="linklabelbld" onclick="check();" href="#"><asp:Label id="lang2586" runat="server">Check for New Replies</asp:Label></A></td>
								</tr>
								<tr height="20">
									<td class="label"><asp:Label id="lang2587" runat="server">Member:</asp:Label></td>
									<td class="plainlabel" id="lblmem" runat="server"></td>
									<td align="right" colSpan="3"><A class="linklabelbld" onclick="viewprofile();" href="#"><asp:Label id="lang2588" runat="server">View Member Profile</asp:Label></A></td>
								</tr>
								<tr height="20">
									<td class="label"><asp:Label id="lang2589" runat="server">Subject:</asp:Label></td>
									<td class="plainlabel" id="lblsubj" colSpan="3" runat="server"></td>
									<td class="label" id="lblcnt" align="right" colSpan="1" runat="server"><asp:Label id="lang2590" runat="server">Replies:</asp:Label></td>
								</tr>
								<tr>
									<td class="plainlabel rdivns" id="tdmsg" colSpan="6" runat="server"></td>
								</tr>
								<TR height="26">
									<td colSpan="6"><A class="linklabelbld" onclick="reply('0');" href="#"><asp:Label id="lang2591" runat="server">Subject Reply</asp:Label></A>&nbsp;&nbsp;
										<A class="linklabelbld" onclick="post();" href="#"><asp:Label id="lang2592" runat="server">Post New Subject</asp:Label></A></td>
								</TR>
								<tr>
									<td class="btmmenu plainlabel" align="left" colSpan="6" height="22"><asp:Label id="lang2593" runat="server">Current Replies</asp:Label></td>
								</tr>
								<tr height="26">
									<TD id="tdreplies" colSpan="6" runat="server"></TD>
								</tr>
							</TBODY>
						</table>
						<table class="details" id="tdsecadd" width="670">
							<tr height="20">
								<td class="thdrsingrt label" bgColor="#1d11ef" colSpan="6"><asp:Label id="lang2594" runat="server">Add Secured Project Forum Members</asp:Label></td>
							</tr>
							<tr>
								<td align="center"><iframe id="ifsec" src="" frameBorder="no" width="670" height="500" runat="server"></iframe>
								</td>
							</tr>
						</table>
						<table class="details" id="tdsrch" width="670">
							<tr height="20">
								<td class="thdrsingrt label" bgColor="#1d11ef"><asp:Label id="lang2595" runat="server">Search Results</asp:Label></td>
							</tr>
							<tr>
								<td id="tdresults" align="center" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<div class="details" id="prdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 760px; BORDER-BOTTOM: black 1px solid; HEIGHT: 320px">
				<table cellSpacing="0" cellPadding="0" width="760" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="whitelabel12" id="tdname"></td>
						<td align="right"><IMG onclick="closepr();" height="18" alt="" src="../images/close.gif" width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifpr" style="WIDTH: 760px; HEIGHT: 320px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<input id="lblfilt" type="hidden" runat="server"> <input id="lblfid" type="hidden" runat="server">
			<input id="lblforum" type="hidden" runat="server"> <input id="lblret" type="hidden" runat="server">
			<input id="lblinput" type="hidden" runat="server"> <input id="lblpid" type="hidden" runat="server">
			<input id="lblpuid" type="hidden" runat="server"> <input id="lblmode" type="hidden" runat="server">
			<input id="lblrid" type="hidden" runat="server"> <input id="lblname" type="hidden" runat="server"><input id="lbluid" type="hidden" runat="server">
			<input id="lblowner" type="hidden" runat="server"><input id="lbldb" type="hidden" runat="server">
			<input type="hidden" id="lbltyp" runat="server"><input type="hidden" id="lbladmin" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
