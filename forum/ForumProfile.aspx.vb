

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class ForumProfile
    Inherits System.Web.UI.Page
	Protected WithEvents lang2605 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2604 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2603 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2602 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2601 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2600 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2599 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2598 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2597 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2596 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim pr As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents tdphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdemail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdname As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdaltphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdaltemail As System.Web.UI.HtmlControls.HtmlTableCell
    Dim uid, admin As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdtitle As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgpr As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents dvov As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            lbldb.Value = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            uid = Request.QueryString("uid").ToString
            admin = HttpContext.Current.Session("pmadmin").ToString()
            pr.Open()
            LoadProfile(uid, admin)
            pr.Dispose()
        End If
    End Sub
    Private Sub LoadProfile(ByVal uid As String, ByVal admin As String)
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        If admin = "no" Then
            sql = "select username, phonenum, altphonenum, email, altemail from pmsysusers where uid = '" & uid & "'"
        Else
            Dim amdb As String
            amdb = "laiadmin"
            sql = "select username, phonenum, altphonenum, email, altemail from [" & srvr & "].[" & amdb & "].[dbo].[pmsysusers] where uid = '" & uid & "'"
        End If

        dr = pr.GetRdrData(sql)
        Dim ph, aph, em, aem As String
        While dr.Read
            tdname.InnerHtml = dr.Item("username").ToString
            ph = dr.Item("phonenum").ToString
            aph = dr.Item("altphonenum").ToString
            em = "<a href='mailto:" & dr.Item("email").ToString & "'>" & dr.Item("email").ToString & "</a>"
            aem = "<a href='mailto:" & dr.Item("altemail").ToString & "'>" & dr.Item("altemail").ToString & "</a>"
        End While
        dr.Close()
        tdphone.InnerHtml = ph
        tdaltphone.InnerHtml = aph
        tdemail.InnerHtml = em
        tdaltemail.InnerHtml = aem
        Dim pic As String
        sql = "select title, intro, pic from [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] where uid = '" & uid & "'"
        dr = pr.GetRdrData(sql)
        'If dr.HasRows Then
        While dr.Read
            tdtitle.InnerHtml = dr.Item("title").ToString
            dvov.InnerHtml = dr.Item("intro").ToString
            pic = dr.Item("pic").ToString
        End While
        'End If
        If Len(tdtitle.InnerHtml) = 0 Then
            tdtitle.InnerHtml = "No Job Title Provided"
        End If
        If Len(dvov.InnerHtml) = 0 Then
            dvov.InnerHtml = "No Profile Overview Provided"
        End If
        dr.Close()
        Dim src As String = "../userimages/"
        If Len(pic) <> 0 Then
            imgpr.Src = src & pic
        End If

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2596.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2596")
		Catch ex As Exception
		End Try
		Try
			lang2597.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2597")
		Catch ex As Exception
		End Try
		Try
			lang2598.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2598")
		Catch ex As Exception
		End Try
		Try
			lang2599.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2599")
		Catch ex As Exception
		End Try
		Try
			lang2600.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2600")
		Catch ex As Exception
		End Try
		Try
			lang2601.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2601")
		Catch ex As Exception
		End Try
		Try
			lang2602.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2602")
		Catch ex As Exception
		End Try
		Try
			lang2603.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2603")
		Catch ex As Exception
		End Try
		Try
			lang2604.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2604")
		Catch ex As Exception
		End Try
		Try
			lang2605.Text = axlabs.GetASPXPage("ForumProfile.aspx","lang2605")
		Catch ex As Exception
		End Try

	End Sub

End Class
