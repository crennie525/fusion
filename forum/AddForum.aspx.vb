

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class AddForum
    Inherits System.Web.UI.Page
	Protected WithEvents lang2556 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2555 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2554 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim lab As New Utilities
    Dim sql, filter, lc, fid, forum, typ, admin As String
    Protected WithEvents lbmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntolist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromlist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbselect As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbllc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdforum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblforum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladmin As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsrch As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            admin = HttpContext.Current.Session("pmadmin").ToString()
            lbladmin.Value = admin
            lbldb.Value = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            lc = "maximo"
            lbllc.Value = lc
            fid = Request.QueryString("fid").ToString '"6"
            lblfid.Value = fid
            forum = Request.QueryString("forum").ToString
            tdforum.InnerHtml = "Current Forum: " & forum
            lblforum.Value = forum
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            lab.Open()
            GetLabor()
            GetSelect()
            lab.Dispose()

        End If

    End Sub
    Private Sub GetLabor()
        admin = lbladmin.Value
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim srch As String = txtsrch.Text
        txtsrch.Text = ""
        fid = lblfid.Value
        If srch <> "" Then
            filter = " where (uid like '%" & srch & "%' or username like '%" & srch & "%')"
                filter += " and (uid not in (select laborcode from [" & srvr & "].[" & mdb & "].[dbo].[web_forumusers] where sec_fid = '" & fid & "'))"

        Else
            filter = "where uid not in (select laborcode from [" & srvr & "].[" & mdb & "].[dbo].[web_forumusers] where sec_fid = '" & fid & "')"
        End If
        Dim amdb As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        If admin = "no" Then
            sql = "select * from pmsysusers " & filter
        Else
            'sql = "select * from [" & srvr & "].[" & amdb & "].[dbo].[pmsysusers] " & filter
            If srch <> "" Then
                sql = "[" & srvr & "].[" & amdb & "].[dbo].[usp_getUsers] '" & fid & "', '" & srch & "'"
            Else
                sql = "[" & srvr & "].[" & amdb & "].[dbo].[usp_getUsers] '" & fid & "'"
            End If

        End If

        dr = lab.GetRdrData(sql)
        lbmaster.DataSource = dr
        lbmaster.DataValueField = "uid"
        lbmaster.DataTextField = "username"
        lbmaster.DataBind()
        dr.Close()

    End Sub

    Private Sub ibtnsrch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsrch.Click
        'If Len(txtsrch.Text) > 0 Then
        lab.Open()
        GetLabor()
        lab.Dispose()
        'End If
    End Sub

    Private Sub btntolist_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntolist.Click
        Dim Item As ListItem
        Dim f, fi As String
        lab.Open()
        For Each Item In lbmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(fi, f)
            End If
        Next

        GetLabor()
        GetSelect()
        lab.Dispose()
    End Sub
    Private Sub GetSelect()
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        fid = lblfid.Value
        sql = "select laborcode, name from [" & srvr & "].[" & mdb & "].[dbo].[web_forumusers] where " _
        + "sec_fid = '" & fid & "'"
        dr = lab.GetRdrData(sql)
        lbselect.DataSource = dr
        lbselect.DataValueField = "laborcode"
        lbselect.DataTextField = "name"
        lbselect.DataBind()
        dr.Close()
    End Sub
    Private Sub GetItems(ByVal id As String, ByVal str As String)
        Dim mdb As String = lbldb.Value
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        fid = lblfid.Value
        sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[web_forumusers] where " _
        + "laborcode = '" & id & "' and sec_fid = '" & fid & "'"
        Dim labcnt As Integer
        labcnt = lab.Scalar(sql)
        If labcnt = 0 Then
            sql = "insert into [" & srvr & "].[" & mdb & "].[dbo].[web_forumusers] (sec_fid, laborcode, name) values " _
            + "('" & fid & "', '" & id & "', '" & str & "')"
            lab.Update(sql)
        End If

    End Sub

    Private Sub btnfromlist_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromlist.Click
        Dim Item As ListItem
        Dim f, fi As String
        lab.Open()
        For Each Item In lbselect.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        GetLabor()
        GetSelect()
        lab.Dispose()
    End Sub
    Private Sub RemItems(ByVal id As String, ByVal str As String)
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        fid = lblfid.Value
        sql = "delete from [" & srvr & "].[" & mdb & "].[dbo].[web_forumusers] where laborcode = '" & id & "' and " _
        + "sec_fid = '" & fid & "'"
        lab.Update(sql)

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2554.Text = axlabs.GetASPXPage("AddForum.aspx", "lang2554")
        Catch ex As Exception
        End Try
        Try
            lang2555.Text = axlabs.GetASPXPage("AddForum.aspx", "lang2555")
        Catch ex As Exception
        End Try
        Try
            lang2556.Text = axlabs.GetASPXPage("AddForum.aspx", "lang2556")
        Catch ex As Exception
        End Try

    End Sub

End Class
