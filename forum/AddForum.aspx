<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddForum.aspx.vb" Inherits="lucy_r12.AddForum" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AddForum</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/AddForumaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table align="center">
				<tr height="30">
					<td class="bluelabel" id="tdforum" align="left" colSpan="4" runat="server"></td>
				</tr>
				<tr>
					<td class="label" width="130"><asp:Label id="lang2554" runat="server">Search Labor/Logins</asp:Label></td>
					<td width="150"><asp:textbox id="txtsrch" runat="server"></asp:textbox></td>
					<td width="20"><asp:imagebutton id="ibtnsrch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td width="80"></td>
				</tr>
				<tr>
					<td colSpan="4">
						<table width="400">
							<tr>
								<td class="bluelabel" width="185"><asp:Label id="lang2555" runat="server">Available PM Logins</asp:Label></td>
								<td vAlign="middle" align="center" width="20"></td>
								<td class="bluelabel" width="185"><asp:Label id="lang2556" runat="server">Selected Forum Members</asp:Label></td>
							</tr>
							<tr>
								<td><asp:listbox id="lbmaster" runat="server" SelectionMode="Multiple" Height="250px" Width="176px"></asp:listbox></td>
								<td><asp:imagebutton id="btntolist" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><br>
									<asp:imagebutton id="btnfromlist" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
								<td><asp:listbox id="lbselect" runat="server" SelectionMode="Multiple" Height="250px" Width="175px"></asp:listbox></td>
							</tr>
							<tr>
								<td align="right" colSpan="4"><INPUT class="lilmenu plainlabel" onmouseover="this.className='lilmenuhov plainlabel'"
										style="WIDTH: 55px; HEIGHT: 24px" onclick="gotoforum();" onmouseout="this.className='lilmenu plainlabel'" type="button"
										value="Submit"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbllc" type="hidden" runat="server"><input id="lblfid" type="hidden" runat="server">
			<input id="lblforum" type="hidden" runat="server"><input id="lbltyp" type="hidden" runat="server">
			<input id="lbldb" type="hidden" runat="server"><input id="lbladmin" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
