<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ForumProfile.aspx.vb" Inherits="lucy_r12.ForumProfile" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ForumProfile</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT:5px;POSITION:absolute;TOP:5px" width="745">
				<tr>
					<td width="130"></td>
					<td width="140"></td>
					<td width="50"></td>
					<td width="280"></td>
					<td width="5" rowspan="8">&nbsp;</td>
					<td width="220"></td>
				</tr>
				<tr>
					<td class="thdrsingrt label" colSpan="4"><asp:Label id="lang2596" runat="server">General Information</asp:Label></td>
					<td class="thdrsingrt label" colSpan="4"><asp:Label id="lang2597" runat="server">Forum Pic or Logo</asp:Label></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2598" runat="server">Screen Name:</asp:Label></td>
					<td id="tdname" runat="server" class="bluelabel" colspan="3"></td>
					<td rowspan="8" align="center" valign="middle"><IMG id="imgpr" height="216" src="../images/nophoto.gif" width="216" border="0" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2599" runat="server">Phone Number:</asp:Label></td>
					<td id="tdphone" runat="server" class="bluelabel" colspan="3"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2600" runat="server">Alt Phone Number:</asp:Label></td>
					<td id="tdaltphone" runat="server" class="bluelabel" colspan="3"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2601" runat="server">Email Address:</asp:Label></td>
					<td id="tdemail" runat="server" class="bluelabel" colspan="3"></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2602" runat="server">Alt Email Address:</asp:Label></td>
					<td id="tdaltemail" runat="server" class="bluelabel" colspan="3"></td>
				</tr>
				<tr>
					<td class="thdrsingrt label" colSpan="4"><asp:Label id="lang2603" runat="server">Profile Information</asp:Label></td>
				</tr>
				<tr height="20">
					<td class="label"><asp:Label id="lang2604" runat="server">Job Title:</asp:Label></td>
					<td id="tdtitle" runat="server" class="bluelabel" colspan="3"></td>
				</tr>
				<tr>
					<td class="label" valign="top"><asp:Label id="lang2605" runat="server">Profile Overview:</asp:Label></td>
					<td colspan="3" valign="top"><div class="plainlabel" id="dvov" runat="server" style="BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 3px; BORDER-TOP: black 1px solid; PADDING-LEFT: 3px; PADDING-BOTTOM: 3px; OVERFLOW: auto; BORDER-LEFT: black 1px solid; PADDING-TOP: 3px; BORDER-BOTTOM: black 1px solid; HEIGHT: 100px"></div>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbldb" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
