

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class ForumMain
    Inherits System.Web.UI.Page
	Protected WithEvents lang2595 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2594 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2593 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2592 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2591 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2590 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2589 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2588 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2587 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2586 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2585 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2584 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2583 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2582 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2581 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2580 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2579 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2578 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2577 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2576 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2575 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2574 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2573 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2572 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2571 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2570 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2569 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2568 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2567 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2566 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2565 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2564 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2563 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2562 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2561 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2560 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2559 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2558 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2557 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim fo As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = "web_forum_subjects"
    Dim PK As String = "subject_id"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    '"*, cnt = (select count(*) from functions f where f.eqid = eqid)"
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Sort As String = "subject_id DESC"
    Dim pgcnt As Integer
    Dim pgnav As Integer
    Dim fid, forum, pid, login, usrname, uid, mode, admin As String
    Protected WithEvents lblpuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtmsgr As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdforumr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdauthr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsubjr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtreply1 As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents lblrid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbforopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblname As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsec As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblowner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents A3 As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tdcurr1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpostdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblmem As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsubj As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdreplies As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdresults As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifpr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnaddfor As System.Web.UI.WebControls.ImageButton
    Protected WithEvents currpic As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladmin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpid As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdlist1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtnew As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents A1 As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tdpgnavtop As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents navtop As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpgnavbot As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents navbot As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcurrent As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdauth As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtsubj As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblinput As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtmsg As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblforum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents tdsubj As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtid As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents txtps As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents tdlist As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            admin = HttpContext.Current.Session("pmadmin").ToString()
            lbladmin.Value = admin
        Catch ex As Exception
            Dim redir As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
            redir = redir & "?logout=yes"
            Response.Redirect(redir)
            Exit Sub
        End Try
        If Not IsPostBack Then
            lbldb.Value = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            usrname = HttpContext.Current.Session("username").ToString()
            lblname.Value = usrname
            uid = HttpContext.Current.Session("uid").ToString()
            lbluid.Value = uid
            If Request.QueryString("forum") = "" Then
                forum = "General Questions and Concerns"
                tdcurr.InnerHtml = "Current Forum: " & forum
                tdcurrent.InnerHtml = forum
                lblforum.Value = "General Questions and Concerns"
                Filter = "nsforum_id = 1 and active = 0"
                lblfilt.Value = Filter
                lblfid.Value = "1"
                tdauth.InnerHtml = usrname
                mode = "open"
                lblmode.Value = mode
            Else
                forum = Request.QueryString("forum")
                tdcurr.InnerHtml = "Current Forum: " & forum
                lblforum.Value = forum
                tdcurrent.InnerHtml = forum
                fid = Request.QueryString("fid")
                Filter = "nsforum_id = " & fid & " and active = 0"
                lblfilt.Value = Filter
                lblfid.Value = fid
                tdauth.InnerHtml = usrname
                Try
                    mode = Request.QueryString("mode")
                    lblmode.Value = mode
                Catch ex As Exception
                    mode = "na"
                    lblmode.Value = mode
                End Try
            End If
            fo.Open()
            LoadLists()
            If mode = "secured" Then
                Dim chk As Integer
                chk = CheckLogin()
                If chk <> 0 Then
                    PageNumber = 1
                    GetForumList(PageNumber, chk)
                End If
            Else
                GetForumList(PageNumber)
                fo.Dispose()
            End If
        Else

            If Request.Form("lblret") = "yes" Then
                lblret.Value = "no"
                AddSubj()
            ElseIf Request.Form("lblret") = "ret" Then
                lblret.Value = "no"
                fo.Open()
                LoadLists()
                fid = lblfid.Value
                Filter = "nsforum_id = " & fid & " and active = 0"
                lblfilt.Value = Filter
                GetForumList(PageNumber)
                fo.Dispose()
            ElseIf Request.Form("lblret") = "open" Then
                lblret.Value = "no"
                PageNumber = 1
                fo.Open()
                fid = lblfid.Value
                Filter = "nsforum_id = " & fid & " and active = 0"
                lblfilt.Value = Filter
                GetForumList(PageNumber)
                forum = lblforum.Value
                tdcurrent.InnerHtml = forum
                tdcurr.InnerHtml = "Current Forum: " & forum
                fo.Dispose()
            ElseIf Request.Form("lblret") = "reply" Then
                lblret.Value = "no"
                lblmode.Value = "reply"
                fo.Open()
                loadForum()
                forum = lblforum.Value
                tdcurrent.InnerHtml = forum
                tdforumr.InnerHtml = forum
                tdcurr1.InnerHtml = "Current Forum: " & forum
                tdauthr.InnerHtml = lblname.Value
                fo.Dispose()
            ElseIf Request.Form("lblret") = "oreply" Then
                lblret.Value = "no"
                lblmode.Value = "reply"
                fo.Open()
                forum = lblforum.Value
                PageNumber = 1
                fid = lblfid.Value
                Filter = "nsforum_id = " & fid & " and active = 0"
                GetForumList(PageNumber)
                loadForum()
                tdcurrent.InnerHtml = forum
                tdcurr.InnerHtml = "Current Forum: " & forum
                tdcurrent.InnerHtml = forum
                tdforumr.InnerHtml = forum
                tdcurr1.InnerHtml = "Current Forum: " & forum
                tdauthr.InnerHtml = lblname.Value
                fo.Dispose()
            ElseIf Request.Form("lblret") = "replyyes" Then
                lblret.Value = "no"
                lblmode.Value = "reply"
                fo.Open()
                AddReply()
                loadForum()
                forum = lblforum.Value
                tdcurrent.InnerHtml = forum
                tdforumr.InnerHtml = forum
                tdcurr1.InnerHtml = "Current Forum: " & forum
                tdauthr.InnerHtml = lblname.Value
                fo.Dispose()
            ElseIf Request.Form("lblret") = "srch" Then
                lblret.Value = "no"
                lblmode.Value = "srch"
                Dim srch As String = txtsrch.Value
                uid = lbluid.Value
                fo.Open()
                GetSearchList(srch, uid)
                fo.Dispose()
            ElseIf Request.Form("lblret") = "sreply" Then
                Dim chk As Integer
                fo.Open()
                chk = CheckLogin()
                If chk <> 0 Then
                    lblret.Value = "no"
                    lblmode.Value = "reply"
                    forum = lblforum.Value
                    PageNumber = 1
                    lbltyp.Value = "2"
                    fid = lblfid.Value
                    Filter = "nsforum_id = " & fid & " and active = 0"
                    GetForumList(PageNumber, chk)
                    loadForum()
                    tdcurrent.InnerHtml = forum
                    tdcurr.InnerHtml = "Current Forum: " & forum
                    tdcurrent.InnerHtml = forum
                    tdforumr.InnerHtml = forum
                    tdcurr1.InnerHtml = "Current Forum: " & forum
                    tdauthr.InnerHtml = lblname.Value
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1007" , "ForumMain.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lblret.Value = "no"
                    lblmode.Value = "srch"
                    uid = lbluid.Value
                    Dim srch As String = txtsrch.Value
                    GetSearchList(srch, uid)
                End If
                fo.Dispose()
            ElseIf Request.Form("lblret") = "login" Then
                lblret.Value = "no"
                Dim chk As Integer
                fo.Open()
                chk = CheckLogin()
                If chk <> 0 Then
                    PageNumber = 1
                    fid = lblfid.Value
                    lbltyp.Value = "2"
                    Filter = "nsforum_id = " & fid & " and active = 0"
                    lblfilt.Value = Filter
                    GetForumList(PageNumber, chk)
                    forum = lblforum.Value
                    tdcurrent.InnerHtml = forum
                    tdcurr.InnerHtml = "Current Forum: " & forum
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1008" , "ForumMain.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                fo.Dispose()

            End If
        End If

    End Sub
   
    Private Function CheckLogin() As Integer
        Dim ret As Integer
        uid = lbluid.Value
        fid = lblfid.Value
        Dim cnt, ocnt As Integer
        Dim mdb As String = lbldb.Value
        Dim amdb As String = "laiadmin"
        admin = lbladmin.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")

        sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[web_forumusers] where sec_fid = '" & fid & "' and laborcode = '" & uid & "'"
        'sql = "select count(*) from web_forumusers where sec_fid = '" & fid & "' and laborcode = '" & uid & "'"
            cnt = fo.Scalar(sql)
        If cnt <> 0 Then
            sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[web_forums] where id = '" & fid & "' and laborcode = '" & uid & "'"
            'sql = "select count(*) from web_forums where id = '" & fid & "' and laborcode = '" & uid & "'"
            ocnt = fo.Scalar(sql)
            If ocnt <> 0 Then
                ret = 2
                lblowner.Value = 2
            Else
                ret = 1
            End If
        Else
            ret = 0
        End If
        Return ret
    End Function
    Private Sub AddReply()
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim subj As String
        Dim msg As String
        Dim reply, uid, usr, rid As String
        fid = lblfid.Value
        pid = lblpid.Value
        uid = lbluid.Value
        usr = tdauthr.InnerHtml()
        rid = lblrid.Value
        usr = tdauth.InnerHtml()
        forum = tdforumr.InnerHtml
        subj = tdsubjr.InnerHtml
        reply = txtreply1.Value
        msg = lblinput.Value
        sql = "[" & srvr & "].[" & mdb & "].[dbo].[usp_newreply1] '" & uid & "', '" & usr & "', '" & fid & "', '" & pid & "', '" & reply & "', '" & msg & "', '" & rid & "'"
        'Try
        'add.Open()
        fo.Update(sql)
        'add.Dispose()
        'Response.Redirect("nsforum_view.aspx?fid=" & fid & "&pid=" & pid & "&uid=" & uid & "&usr=" & usr & "&forum=" & forum & "&subj=" & subj & "&date=" + Now())
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr1009" , "ForumMain.aspx.vb")
 
        'utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End Try
    End Sub
    Private Sub loadForum()
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        fid = lblfid.Value
        pid = lblpid.Value
        Dim cnt As String
        sql = "select s.*, pic = (select p.pic from [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] p " _
        + "where p.uid = profileid) from [" & srvr & "].[" & mdb & "].[dbo].[web_forum_subjects] s where " _
        + "s.subject_id = '" & pid & "'"
        dr = fo.GetRdrData(sql)
        Dim msg, pic As String
        While dr.Read
            pic = dr.Item("pic").ToString()
            msg = dr.Item("msg").ToString()
            'lblrid.Value = dr.Item("nsforum_id").ToString()
            lblpuid.Value = dr.Item("profileid").ToString()
            lblmem.InnerHtml = dr.Item("nsf_login_id").ToString()
            lblpostdate.InnerHtml = dr.Item("created").ToString()
            lblsubj.InnerHtml = dr.Item("subject").ToString()
            tdsubjr.InnerHtml = dr.Item("subject").ToString()
            cnt = dr.Item("reply_cnt").ToString()
            lblcnt.InnerHtml = "Replies:&nbsp;" & cnt
        End While
        msg = msg.Replace("~flb~", "<br>")
        msg = msg.Replace("~c~", ",")
        msg = msg.Replace("~lt~", "<")
        msg = msg.Replace("~gt~", ">")
        msg = msg.Replace("~dq~", """")
        msg = msg.Replace("~sq~", "'")

        msg = Server.HtmlEncode(msg)
        msg = msg.Replace("&lt;br&gt;", "<br>")
        tdmsg.InnerHtml = msg
        dr.Close()
        pic = Replace(pic, "atm", "atn")
        If pic <> "" Then
            pic = "../userimages/" & pic
        Else
            pic = "../userimages/nophoto60.gif"
        End If
        currpic.InnerHtml = "<img src='" & pic & "'>"
        loadReplies()
    End Sub
    Private Sub loadReplies()
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        fid = lblfid.Value
        pid = lblpid.Value
        sql = "select r.*, pic = (select p.pic from [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] p " _
        + "where p.uid = profileid) from [" & srvr & "].[" & mdb & "].[dbo].[web_forum_replies] r where " _
        + "subject_id = '" & pid & "' and active = 0 order by parent_id, child_id, created asc"
        'sql = "select l1.*, isnull(l2.reply_L1_id, 0) as rid, l2.created as rcreate, " _
        '+ "l2.profileid as rprofile, l2.nsf_login_id as rlogin, l2.title as rtitle, l2.msg as rmsg " _
        '+ "from nsforum_replies l1 " _
        '+ "left join nsforum_replies_L1 l2 on l2.reply_id = l1.reply_id " _
        '+ "where l1.subject_id = '" & pid & "' and l1.active = 0 order by l1.created asc"

        dr = fo.GetRdrData(sql)
        Dim row As Integer = 1
        Dim start As Integer = 1
        Dim rflag As Integer = 0
        Dim pcnt As Integer
        Dim bgcolor, bgmsg, lbl, lnk, lbltxt, txt, msg, puid, rep, irep, imsg, tdrep, rtdrep, rtdmsg, lrep, lmsg, fdb, pic, pname As String
        rep = "0"
        txt = "plainlabel"
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<table  width=""680"" border=""0"" cellspacing=""0"" cellpadding=""0"">") '
        sb.Append("<tr><td width=""15""></td>")
        sb.Append("<td width=""70""></td>")
        sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""290""></td>")
        sb.Append("<td width=""225""></td></tr>")
        While dr.Read
            'If row = 0 Then row = 1 Else row = 0
            'lblrid.Value = dr.Item("parent_id").ToString()
            puid = dr.Item("profileid").ToString
            pname = dr.Item("nsf_login_id").ToString
            pic = dr.Item("pic").ToString
            pic = Replace(pic, "atm", "atn")
            If pic <> "" Then
                pic = "../userimages/" & pic
            Else
                pic = "../userimages/nophoto60.gif"
            End If
            If rep <> dr.Item("parent_id").ToString Then
                rep = dr.Item("parent_id").ToString
                pcnt = dr.Item("reply_cnt").ToString
                rflag = 0
            End If
            fdb = dr.Item("child_id").ToString
            irep = "img" + rep
            tdrep = "td" + rep
            lrep = "lnk" + rep
            rtdrep = "rtd" + rep
            imsg = "msgimg" + rep + fdb
            rtdmsg = "msg" + rep + fdb
            lmsg = "msglnk" + rep + fdb
            Dim chid As String = dr.Item("child_id").ToString
            If chid = "0" Then
                If row = 1 Then
                    row = 0
                    bgcolor = "#e7f1fd"
                    bgmsg = "white"
                    lbl = "label"
                    lnk = "linklabelbld"
                    lbltxt = "plainlabel"
                Else
                    row = 1
                    bgcolor = "white"
                    bgmsg = "#e7f1fd"
                    lbl = "label"
                    lnk = "linklabelbld"
                    lbltxt = "plainlabel"
                End If
                sb.Append("<tr bgcolor=""" & bgcolor & """><td></td>")
                sb.Append("<td rowspan=""3""><img src='" & pic & "'></td>")
                sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl484" , "ForumMain.aspx.vb") & "</td>")
                sb.Append("<td class=""" & lbltxt & """>" & dr.Item("created").ToString & "</td>")
                sb.Append("<td align=""right""><a id=""" & lrep & """ class=""" & lnk & """ href=""#""  onclick=""getreply('" & irep & "', '" & tdrep & "', '" & lrep & "', '" & rtdrep & "');"">View This Reply</a></td>")
                sb.Append("</tr>")
                sb.Append("<tr bgcolor=""" & bgcolor & """><td></td>")
                sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl485" , "ForumMain.aspx.vb") & "</td>")
                sb.Append("<td class=""" & lbltxt & """>" & dr.Item("nsf_login_id").ToString)
                sb.Append("<input type=""hidden"" id=""lblpuid"" value=""" & puid & """></td>")
                sb.Append("<td  align=""right""><a class=""" & lnk & """ href=""#"" onclick=""viewprofile('" & puid & "', '" & pname & "');"">View Member Profile</a></td>")
                sb.Append("</tr>")
                sb.Append("<tr bgcolor=""" & bgcolor & """><td><img src='../images/appbuttons/bgbuttons/plus.gif'")
                sb.Append("id=""" & irep & """ onclick=""getreply('" & irep & "', '" & tdrep & "', '" & lrep & "', '" & rtdrep & "');"" ></td>")
                sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl486" , "ForumMain.aspx.vb") & "</td>")
                sb.Append("<td class=""" & lbltxt & """>" & dr.Item("title").ToString & "</td>")
                sb.Append("<td class=""" & lbl & """  align=""right"">Replies:&nbsp;" & dr.Item("reply_cnt").ToString & "</td>")
                sb.Append("</tr>")
                msg = dr.Item("msg").ToString()
                msg = msg.Replace("~flb~", "<br>")
                msg = msg.Replace("~c~", ",")
                msg = msg.Replace("~lt~", "<")
                msg = msg.Replace("~gt~", ">")
                msg = msg.Replace("~dq~", """")
                msg = msg.Replace("~sq~", "'")
                msg = Server.HtmlEncode(msg)
                msg = msg.Replace("&lt;br&gt;", "<br>")
                sb.Append("<tr><td></td><td colspan=""4"" valign=""top"" align=""left"">")
                sb.Append("<table id=""" & tdrep & """ width=""660""  class=""details"" border=""0""><tr>") 'cellpadding=""0"" cellspacing=""0""
                sb.Append("<td  colspan=""5"" bgcolor=""" & bgmsg & """ class=""plainlabel fdivns"">" & msg & "</td></tr>")
                sb.Append("<tr height=""20""><td colspan=""5"" class=""plainlabelblue""><a class=""" & lnk & """ href=""#""  onclick=""reply('" & rep & "');"">Feedback Reply</a>") 'dr.Item("parent_id").ToString

                sb.Append("&nbsp;&nbsp;(Use to reply directly to this comment. Use the Subject Reply Link above to add to Subject Thread.)</td></tr>")
                sb.Append("</table></td></tr>")
            Else
                'If bgcolor = "#e7f1fd" Then bgcolor = "white" Else bgcolor = "#e7f1fd"
                If rflag = 0 Then
                    rflag = 1
                    sb.Append("<tr id=""" & rtdrep & """ class=""details""><td></td><td colspan=""5"" valign=""top"" align=""left""><table width=""665"" border=""0"" cellspacing=""0"" cellpadding=""0"">")
                    sb.Append("<tr><td width=""15""></td>")
                    sb.Append("<td width=""70""></td>")
                    sb.Append("<td width=""80""></td>")
                    sb.Append("<td width=""270""></td>")
                    sb.Append("<td width=""260""></td></tr>")
                    sb.Append("<td class=""btmmenu plainlabel"" colspan=""5"" height=""20"">" & tmod.getlbl("cdlbl487" , "ForumMain.aspx.vb") & "</td></tr>")
                    sb.Append("<tr><td><img src=""../images/4PX.gif""></td></tr>")
                End If
                'start
                sb.Append("<tr bgcolor=""" & bgcolor & """ height=""20""><td></td>")
                sb.Append("<td rowspan=""3""><img src='" & pic & "'></td>")
                sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl488" , "ForumMain.aspx.vb") & "</td>")
                sb.Append("<td class=""" & lbltxt & """>" & dr.Item("created").ToString & "</td>")
                sb.Append("<td align=""right""><a id=""" & lmsg & """ class=""" & lnk & """ href=""#""  onclick=""getfeedback('" & imsg & "', '" & rtdmsg & "', '" & lmsg & "');"">View This Reply</a></td>")
                sb.Append("</tr>")
                sb.Append("<tr bgcolor=""" & bgcolor & """><td></td>")
                sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl489" , "ForumMain.aspx.vb") & "</td>")
                sb.Append("<td class=""" & lbltxt & """>" & dr.Item("nsf_login_id").ToString)
                sb.Append("<input type=""hidden"" id=""lblpuid"" value=""" & puid & """></td>")
                sb.Append("<td  align=""right""><a class=""" & lnk & """ href=""#"" onclick=""viewprofile('" & puid & "', '" & pname & "');"">View Member Profile</a></td>")
                sb.Append("</tr>")
                sb.Append("<tr bgcolor=""" & bgcolor & """><td><img src='../images/appbuttons/bgbuttons/plus.gif'")
                sb.Append("id=""" & imsg & """ onclick=""getfeedback('" & imsg & "', '" & rtdmsg & "', '" & lmsg & "');"" ></td>")
                sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl490" , "ForumMain.aspx.vb") & "</td>")
                sb.Append("<td class=""" & lbltxt & """ colspan=""2"">" & dr.Item("title").ToString & "</td>")
                'sb.Append("<td class=""" & lbl & """  align=""right"">Replies:&nbsp;" & dr.Item("reply_cnt").ToString & "</td>")
                sb.Append("</tr>")
                msg = dr.Item("msg").ToString()
                msg = msg.Replace("~flb~", "<br>")
                msg = msg.Replace("~c~", ",")
                msg = msg.Replace("~lt~", "<")
                msg = msg.Replace("~gt~", ">")
                msg = msg.Replace("~dq~", """")
                msg = msg.Replace("~sq~", "'")
                msg = Server.HtmlEncode(msg)
                msg = msg.Replace("&lt;br&gt;", "<br>")
                sb.Append("<tr id=""" & rtdmsg & """ class=""details""><td colspan=""5"" valign=""top"" align=""left"">")
                sb.Append("<table width=""665"" ><tr>")
                sb.Append("<td width=""10"">&nbsp;</td>")
                sb.Append("<td width=""655"" class=""plainlabel rlsdivns"" bgcolor=""" & bgmsg & """ >" & msg & "</td></tr>")
                'sb.Append("<tr><td><a class=""" & lnk & """ href=""#""  onclick=""reply('" & dr.Item("parent_id").ToString & "');"">Reply</a></td></tr>")
                sb.Append("</table></td></tr>")
                'end

                If fdb = pcnt Then
                    sb.Append("<tr><td>&nbsp;</td></tr></table></td></tr>")
                End If
                'sb.Append("<tr><td><img src=""../nsimages/4PX.gif""></td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</table>")
        tdreplies.InnerHtml = sb.ToString
    End Sub
    Private Sub AddSubj()
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim subj As String
        Dim msg As String
        Dim uid, usr As String
        fid = lblfid.Value
        uid = lbluid.Value
        usr = tdauth.InnerHtml()
        forum = tdsubj.InnerHtml
        subj = txtsubj.Value
        msg = lblinput.Value
        sql = "[" & srvr & "].[" & mdb & "].[dbo].[usp_newpost] '" & uid & "', '" & usr & "', '" & fid & "', '" & subj & "', '" & msg & "'"
        Try
            fo.Open()
            fo.Update(sql)
            lblret.Value = "go"
            txtmsg.Text = ""
            lblinput.Value = ""
            fid = lblfid.Value
            Filter = "nsforum_id = " & fid & " and active = 0"
            lblfilt.Value = Filter
            GetForumList(PageNumber)
            fo.Dispose()
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1010" , "ForumMain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetForumList(ByVal PageNumber As Integer, Optional ByVal owner As Integer = 0)
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        lblfilt.Value = Filter
        fid = lblfid.Value
        'If Filter = "" Then
        'Filter = "nsforum_id = " & fid & " and active = 0"
        'End If
        Fields = "*, pic = (select p.pic from [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] p where p.uid = profileid)"
        sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[" & Tables & "] where " & Filter
        pgcnt = fo.PageCount(sql, PageSize)
        Dim row As Integer = 1
        Dim bgcolor, lbl, lnk, lbltxt, txt, msg, puid, pic, pname As String
        txt = "plainlabel"
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<table cellspacing=""0"" cellpadding=""1"" width=""650"" border=""0"">")
        sb.Append("<tr><td width=""80""><td width=""100""></td>")
        sb.Append("<td width=""420""></td>") 'was 420
        sb.Append("<td width=""150""></td></tr>")

        If owner = 2 Then
            A3.Attributes.Add("class", "linklabelbld")
        Else
            A3.Attributes.Add("class", "details")
        End If
        sql = "[" & srvr & "].[" & mdb & "].[dbo].[Paging_Cursor] '" & Tables & "', '" & PK & "', '" & Sort & "', '" & PageNumber & "', '" & PageSize & "', '" & Fields & "', '" & Filter & "', '" & Group & "'"
        dr = fo.GetRdrData(sql)
        While dr.Read
            If row = 1 Then row = 0 Else row = 1
            If row = 1 Then
                bgcolor = "#e7f1fd"
                lbl = "label"
                lnk = "linklabelbld"
                lbltxt = "plainlabel"
            Else
                bgcolor = "white"
                lbl = "label"
                lnk = "linklabelbld"
                lbltxt = "plainlabel"
            End If
            puid = dr.Item("profileid").ToString
            pname = dr.Item("nsf_login_id").ToString
            pic = dr.Item("pic").ToString
            pic = Replace(pic, "atm", "atn")
            If pic <> "" Then
                pic = "../userimages/" & pic
            Else
                pic = "../userimages/nophoto60.gif"
            End If
            sb.Append("<tr bgcolor=""" & bgcolor & """>")
            sb.Append("<td rowspan=""3""><img src='" & pic & "'></td>")
            sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl491" , "ForumMain.aspx.vb") & "</td>")
            sb.Append("<td class=""" & lbltxt & """>" & dr.Item("created").ToString & "</td>")
            sb.Append("<td  align=""right""><a class=""" & lnk & """ href=""#"" onclick=""getsubj('" & dr.Item("subject_id").ToString & "', '" & dr.Item("subject").ToString & "');"">View This Thread</a></td>")
            sb.Append("</tr>")
            sb.Append("<tr bgcolor=""" & bgcolor & """>")
            sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl492" , "ForumMain.aspx.vb") & "</td>")
            sb.Append("<td class=""" & lbltxt & """>" & dr.Item("nsf_login_id").ToString)
            sb.Append("<input type=""hidden"" id=""lblpuid"" value=""" & puid & """></td>")
            sb.Append("<td  align=""right""><a class=""" & lnk & """ href=""#"" onclick=""viewprofile('" & puid & "', '" & pname & "');"">View Member Profile</a></td>")
            sb.Append("</tr>")
            sb.Append("<tr bgcolor=""" & bgcolor & """>")
            sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl493" , "ForumMain.aspx.vb") & "</td>")
            sb.Append("<td class=""" & lbltxt & """>" & dr.Item("subject").ToString & "</td>")
            sb.Append("<td class=""" & lbl & """ align=""right"">Replies: " & dr.Item("reply_cnt").ToString & "</td>")
            sb.Append("</tr>")
            msg = dr.Item("msg").ToString()
            msg = msg.Replace("~flb~", "<br>")
            msg = msg.Replace("~c~", ",")
            msg = msg.Replace("~lt~", "<")
            msg = msg.Replace("~gt~", ">")
            msg = msg.Replace("~dq~", """")
            msg = msg.Replace("~sq~", "'")
            msg = Server.HtmlEncode(msg)
            msg = msg.Replace("&lt;br&gt;", "<br>")
            sb.Append("<tr bgcolor=""" & bgcolor & """>")
            sb.Append("<td colspan=""4""><div class=""plainlabel fdivns"">" & msg & "</div></td></tr>")
            sb.Append("<tr><td><img src=""../images/4PX.gif""></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        tdsubj.InnerHtml = sb.ToString
        updatepos(pgcnt, PageNumber)
    End Sub
    Private Sub updatepos(ByVal pgcnt As Integer, ByVal PageNumber As Integer)
        Dim i As Integer
        Dim sb As StringBuilder = New StringBuilder
        Dim lnk As String
        If pgcnt > 1 Then
            For i = 1 To pgcnt
                If i = PageNumber Then
                    lnk = "linklabel"
                Else
                    lnk = "linklabelbld"
                End If
                sb.Append("<a class=""" & lnk & """ onclick=""getpg(" & i & ")"" href=""javascript:void(0);"" > " & i & " </a>")
            Next
            navtop.InnerHtml = sb.ToString
            navbot.InnerHtml = sb.ToString
        Else
            navtop.InnerHtml = "&nbsp;"
            navbot.InnerHtml = "&nbsp;"
        End If
        If pgcnt = 0 Then
            tdpgnavtop.InnerHtml = "Page 0 of 0"
        Else
            tdpgnavtop.InnerHtml = "Page " & PageNumber & " of " & pgcnt
        End If
        If pgcnt = 0 Then
            tdpgnavbot.InnerHtml = "Page 0 of 0"
        Else
            tdpgnavbot.InnerHtml = "Page " & PageNumber & " of " & pgcnt
        End If

       
    End Sub
    Private Sub LoadLists()
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        sql = "select id, forum_name, type from [" & srvr & "].[" & mdb & "].[dbo].[web_Forums] order by type"
        Dim os As New StringBuilder
        Dim ps As New StringBuilder
        os.Append("<table>")
        ps.Append("<table>")
        dr = fo.GetRdrData(sql)
        Dim typ, name, id As String
        While dr.Read
            typ = dr.Item("type").ToString
            name = dr.Item("forum_name").ToString
            id = dr.Item("id").ToString
            If typ = "1" Then
                os.Append("<tr><td><a class=""linklabelbld"" onclick=""getfor('" & id & "' , '" & name & "', 'open');"" href=""#"" > " & name & " </a></td></tr>")
            Else
                ps.Append("<tr><td><a class=""linklabelbld"" onclick=""getfor('" & id & "' , '" & name & "', 'secured');"" href=""#"" > " & name & " </a></td></tr>")
            End If

        End While
        dr.Close()
        os.Append("</table>")
        ps.Append("</table>")
        tdlist.InnerHtml = os.ToString
        tdlist1.InnerHtml = ps.ToString
    End Sub
    Private Sub ibtnaddfor_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnaddfor.Click
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim forum As String = txtnew.Value
        Dim type = rbforopts.SelectedValue.ToString
        usrname = lblname.Value
        uid = lbluid.Value
        If Len(forum) <> 0 Then
            If type = "Open" Then
                type = "1"
            Else
                type = "2"
            End If
            sql = "insert into [" & srvr & "].[" & mdb & "].[dbo].[web_forums] (forum_name, active, created, createdby, laborcode, type) values " _
            + "('" & forum & "', 0, getDate(), '" & usrname & "', '" & uid & "', '" & type & "') select @@identity as 'newid'"

            fo.Open()
            fid = fo.Scalar(sql)

            If type = "1" Then
                Response.Redirect("ForumMain.aspx?forum=" & forum & "&fid=" & fid & "&mode=new")
            Else
                sql = "insert into [" & srvr & "].[" & mdb & "].[dbo].[web_forumusers] (sec_fid, laborcode, name) values " _
                + "('" & fid & "', '" & uid & "', '" & usrname & "')"
                fo.Update(sql)
                Response.Redirect("ForumMain.aspx?forum=" & forum & "&fid=" & fid & "&mode=secnew&typ=new")
            End If
            fo.Dispose()
        End If
        
    End Sub

   

    Private Sub GetSearchList(ByVal srch As String, ByVal uid As String)
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim row As Integer = 1
        Dim bgcolor, lbl, lnk, lbltxt, txt, msg, puid, pname As String
        txt = "plainlabel"
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<table cellspacing=""0"" cellpadding=""3"" width=""650"">")
        sb.Append("<tr><td width=""80""></td>")
        sb.Append("<td width=""400""></td>")
        sb.Append("<td width=""150""></td>")
        sb.Append("<td width=""20""></td></tr>")

        sql = "[" & srvr & "].[" & mdb & "].[dbo].[usp_srchForum] '%" & srch & "%', '" & uid & "'"
        dr = fo.GetRdrData(sql)
        Dim typ, mem, cnt, fn, lock As String
        While dr.Read
            If row = 1 Then row = 0 Else row = 1
            If row = 1 Then
                bgcolor = "#e7f1fd"
                lbl = "label"
                lnk = "linklabelbld"
                lbltxt = "plainlabel"
                lock = "../images/lockblue.gif"
            Else
                bgcolor = "white"
                lbl = "label"
                lnk = "linklabelbld"
                lbltxt = "plainlabel"
                lock = "..images/lock.gif"
            End If
            puid = dr.Item("prof").ToString
            pname = dr.Item("usr").ToString 'nsf_login_id
            typ = dr.Item("type").ToString
            mem = dr.Item("mem").ToString
            cnt = dr.Item("cnt").ToString
            fn = dr.Item("fn").ToString
            sb.Append("<tr bgcolor=""" & bgcolor & """>")
            sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl494" , "ForumMain.aspx.vb") & "</td>")
            sb.Append("<td class=""" & lbltxt & """>" & dr.Item("dat").ToString & "</td>")

            If typ = "2" AndAlso mem = "0" Then
                sb.Append("<td  align=""right""><a class=""" & lnk & """ href='mailto:" & dr.Item("email").ToString & "' >Email Owner</a></td>")
                sb.Append("<td><img src='" & lock & "'></td>")
            Else
                sb.Append("<td  align=""right""><a class=""" & lnk & """ href=""#"" onclick=""getsubj2('" & dr.Item("subj").ToString & "', '" & dr.Item("fid").ToString & "', '" & typ & "', '" & fn & "');"">View This Thread</a></td>")
                sb.Append("<td>&nbsp;</td>")
            End If
            sb.Append("</tr>")
            sb.Append("<tr bgcolor=""" & bgcolor & """>")
            sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl495" , "ForumMain.aspx.vb") & "</td>")
            sb.Append("<td class=""" & lbltxt & """>" & dr.Item("usr").ToString)
            sb.Append("<input type=""hidden"" id=""lblpuid"" value=""" & puid & """></td>")
            sb.Append("<td  align=""right""><a class=""" & lnk & """ href=""#"" onclick=""viewprofile('" & puid & "', '" & pname & "');"">View Member Profile</a></td>")
            sb.Append("<td>&nbsp;</td>")
            sb.Append("</tr>")
            sb.Append("<tr bgcolor=""" & bgcolor & """>")
            sb.Append("<td class=""" & lbl & """>" & tmod.getlbl("cdlbl496" , "ForumMain.aspx.vb") & "</td>")
            sb.Append("<td class=""" & lbltxt & """>" & dr.Item("title").ToString & "</td>")
            sb.Append("<td class=""" & lbl & """ align=""right"">Replies: " & cnt & "</td>")
            sb.Append("<td>&nbsp;</td>")
            sb.Append("</tr>")
            msg = dr.Item("msg").ToString()
            msg = msg.Replace("~flb~", "<br>")
            msg = msg.Replace("~c~", ",")
            msg = msg.Replace("~lt~", "<")
            msg = msg.Replace("~gt~", ">")
            msg = msg.Replace("~dq~", """")
            msg = msg.Replace("~sq~", "'")
            msg = Server.HtmlEncode(msg)
            msg = msg.Replace("&lt;br&gt;", "<br>")
            sb.Append("<tr bgcolor=""" & bgcolor & """>")
            sb.Append("<td colspan=""4""><div class=""plainlabel fdivns"">" & msg & "</div></td></tr>")
            sb.Append("<tr><td><img src=""../images/4PX.gif""></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        tdresults.InnerHtml = sb.ToString
    End Sub


	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2557.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2557")
		Catch ex As Exception
		End Try
		Try
			lang2558.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2558")
		Catch ex As Exception
		End Try
		Try
			lang2559.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2559")
		Catch ex As Exception
		End Try
		Try
			lang2560.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2560")
		Catch ex As Exception
		End Try
		Try
			lang2561.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2561")
		Catch ex As Exception
		End Try
		Try
			lang2562.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2562")
		Catch ex As Exception
		End Try
		Try
			lang2563.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2563")
		Catch ex As Exception
		End Try
		Try
			lang2564.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2564")
		Catch ex As Exception
		End Try
		Try
			lang2565.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2565")
		Catch ex As Exception
		End Try
		Try
			lang2566.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2566")
		Catch ex As Exception
		End Try
		Try
			lang2567.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2567")
		Catch ex As Exception
		End Try
		Try
			lang2568.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2568")
		Catch ex As Exception
		End Try
		Try
			lang2569.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2569")
		Catch ex As Exception
		End Try
		Try
			lang2570.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2570")
		Catch ex As Exception
		End Try
		Try
			lang2571.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2571")
		Catch ex As Exception
		End Try
		Try
			lang2572.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2572")
		Catch ex As Exception
		End Try
		Try
			lang2573.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2573")
		Catch ex As Exception
		End Try
		Try
			lang2574.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2574")
		Catch ex As Exception
		End Try
		Try
			lang2575.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2575")
		Catch ex As Exception
		End Try
		Try
			lang2576.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2576")
		Catch ex As Exception
		End Try
		Try
			lang2577.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2577")
		Catch ex As Exception
		End Try
		Try
			lang2578.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2578")
		Catch ex As Exception
		End Try
		Try
			lang2579.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2579")
		Catch ex As Exception
		End Try
		Try
			lang2580.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2580")
		Catch ex As Exception
		End Try
		Try
			lang2581.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2581")
		Catch ex As Exception
		End Try
		Try
			lang2582.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2582")
		Catch ex As Exception
		End Try
		Try
			lang2583.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2583")
		Catch ex As Exception
		End Try
		Try
			lang2584.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2584")
		Catch ex As Exception
		End Try
		Try
			lang2585.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2585")
		Catch ex As Exception
		End Try
		Try
			lang2586.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2586")
		Catch ex As Exception
		End Try
		Try
			lang2587.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2587")
		Catch ex As Exception
		End Try
		Try
			lang2588.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2588")
		Catch ex As Exception
		End Try
		Try
			lang2589.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2589")
		Catch ex As Exception
		End Try
		Try
			lang2590.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2590")
		Catch ex As Exception
		End Try
		Try
			lang2591.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2591")
		Catch ex As Exception
		End Try
		Try
			lang2592.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2592")
		Catch ex As Exception
		End Try
		Try
			lang2593.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2593")
		Catch ex As Exception
		End Try
		Try
			lang2594.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2594")
		Catch ex As Exception
		End Try
		Try
			lang2595.Text = axlabs.GetASPXPage("ForumMain.aspx","lang2595")
		Catch ex As Exception
		End Try

	End Sub

End Class
