history.forward();
		var levflg, ddl, srchval, filt, tbl, val, txt, tid, ret, tli;
		var cell, sid, did, clid, eq, funid, comid, Filter, Pg, Cnt, Tnum, Vals;
		function getsites() {
		cleanpage();
		cleantasks();
		levflg = document.getElementById("ddtasklev").value;
		var cid = document.getElementById("lblcid").innerHTML;
		Tasks.PopSites(cid, callback_getsites);
		}
		function callback_getsites(res) {
		if(res.error != null) alert(res.error);
		if(res != null && res.value != null && res.value.Tables != 0 && res.value.Tables.length == 1) {
		if(res.value.Tables[0].Rows.length!=0) {
		document.getElementById("sitediv").style.display = "block";
		document.getElementById("sitediv").style.visibility = "visible";
		document.getElementById("ddsites").innerHTML = "";
		document.getElementById("ddsites").options[0] = new Option('Select Plant Site', '0');
		for(var i=0; i<res.value.Tables[0].Rows.length; i++) {
		val = res.value.Tables[0].Rows[i].siteid;
		text = res.value.Tables[0].Rows[i].siteName;
		listItem = new Option(text, val,  false, false);
		document.getElementById("ddsites").options[document.getElementById("ddsites").length] = listItem;
		}
		var sid = document.getElementById("lblsid").innerHTML;
		if (sid!="0") {
		document.getElementById("ddsites").value = sid;
		getlist('ddsites', 'dddepts', 'Dept', 'Dept_ID', 'Dept_Line', 'siteid', 'siteDesc', 'Sites');
		}
		}}}
		//end sites
		function getlist(id, newd, tb, v, t, f, df, dt) {
		//alert("test")
		cleanchk(id);
		cleantasks();
		srchval = document.getElementById(id).value;
		filt = " where " + f + " = " + "'" + srchval + "'";
		tbl = tb;
		val = v;
		txt = t;
		ddl = newd;
		
		if (id=="ddsites"&&levflg=="1") {
		Tasks.GetDesc(dt, df, filt, callback_getdesc);
		gettasks();
		}
		else if (id=="dddepts"&&levflg=="2") {
		//Tasks.CellCheck(srchval, callback_cellcheck);
		Tasks.GetDesc(dt, df, filt, callback_getdesc);
		gettasks();
		}
		else if (id=="ddcells"&&levflg=="3") {
		Tasks.GetDesc(dt, df, filt, callback_getdesc);
		gettasks();
		}
		else if (id=="ddeq"&&levflg=="4") {
		Tasks.GetDesc(dt, df, filt, callback_getdesc);
		gettasks();
		}
		else if (id=="ddfunc"&&levflg=="5") {
		Tasks.GetDesc(dt, df, filt, callback_getdesc);
		gettasks();
		}
		else {
			if (ddl=="ddcells") {
			Tasks.CellCheck(srchval, callback_cellcheck);
			}
			 if (ddl=="tasks") {
			Tasks.GetDesc(dt, df, filt, callback_getdesc);
			gettasks();
			}
			else {
			Tasks.GetDesc(dt, df, filt, callback_getdesc);
			Tasks.GetList(tbl, val, txt, filt, callback_getlist);
			}
		}
		//Tasks.GetDesc(dt, df, filt, callback_getdesc);
		}
		function callback_cellcheck(res) {
		if(res.error != null) alert(res.error);
		var f = document.getElementById("dddepts").value;
		filt = " where Dept_ID = '" + f + "'";
		var df = "Dept_Desc";
		var dt = "Dept";
		document.getElementById("cell").innerHTML = res.value;
		if(res != null && res.value != null) {
			if (res.value=="yes") {
			//alert(cellchk)
			cellchk = "yes";
			var tbl = "Cells";
			var val = "cellid";
			var txt = "Cell_Name";
			Tasks.GetDesc(dt, df, filt, callback_getdesc);
			Tasks.GetList(tbl, val, txt, filt, callback_getlist);
			}
			else {
			
			ddl = "ddeq";
			//cellchk = "no";
			alert(cellchk)
			var tbl = "Equipment";
			var val = "eqid";
			var txt = "eqnum";
			Tasks.GetDesc(dt, df, filt, callback_getdesc);
			Tasks.GetList(tbl, val, txt, filt, callback_getlist);
			}
		}
		}
		function callback_getdesc(res) {
		if(res.error != null) alert(res.error);
		if(res != null && res.value != null )
		//&& res.value.Tables != 0 && res.value.Tables.length == 1
		{
			if (ddl=="dddepts") {
			document.getElementById("lblplantdesc").innerHTML = res.value;
			//.Tables[0].Rows[0].siteDesc;
			}
			else if (ddl=="ddcells") {
			document.getElementById("lbldeptdesc").innerHTML = res.value;
			//.Tables[0].Rows[0].Dept_Desc;
			}
			else if (ddl=="ddeq") {

				if (cellchk=="no") {
				
				document.getElementById("lbldeptdesc").innerHTML = res.value;
				//.Tables[0].Rows[0].Dept_Desc;
				}
				else {
				
				document.getElementById("lblcelldesc").innerHTML = res.value;
				//.Tables[0].Rows[0].Cell_Desc;
				}
			}
			else if (ddl=="ddfunc") {
			document.getElementById("lbleqdesc").innerHTML = res.value;
			//.Tables[0].Rows[0].eqdesc; 
			}
			else if (ddl=="ddcomp") {
			document.getElementById("lblfuncdesc").innerHTML = res.value;
			//.Tables[0].Rows[0].Func_Desc; 
			}
			else if (ddl=="tasks") {
			document.getElementById("lblcompdesc").innerHTML = res.value;
			//.Tables[0].Rows[0].compdesc; 
			}
		}
		// need no result actions
		
		else
		{
			if (ddl=="dddepts") {
			document.getElementById("lblplantdesc").innerHTML = "No Plant Description Found";
			}
			else if (ddl=="ddcells") {
			document.getElementById("lbldeptdesc").innerHTML = "No Department Description Found";
			}
			else if (ddl=="ddeq") {
				if (cellchk=="no") {
				document.getElementById("lbldeptdesc").innerHTML = "No Department Description Found";
				}
				else {
				document.getElementById("lblcelldesc").innerHTML = "No Cell Description Found";
				}
			}
			else if (ddl=="ddfunc") {
			document.getElementById("lbleqdesc").innerHTML = "No Equipment Description Found";
			}
			else if (ddl=="ddcomp") {
			document.getElementById("lblfuncdesc").innerHTML = "No Function Description Found";
			} 
			else if (ddl=="tasks") {
			document.getElementById("lblcompdesc").innerHTML = "No Component Description Found";
			} 
		}
		}
		function callback_getlist(res)
		
		{
		if(res.error != null) alert(res.error);
		if(res != null && res.value != null && res.value.Tables != 0 && res.value.Tables.length == 1)
		{
		if(res.value.Tables[0].Rows.length!=0)
		{
		var dl;
		if(ddl=="dddepts") {
			document.getElementById("deptdiv").style.display = "block";
			document.getElementById("deptdiv").style.visibility = "visible";
			document.getElementById("dddepts").innerHTML = "";
			document.getElementById("dddepts").options[0] = new Option('Select Department', '0');
		}
		else if(ddl=="ddcells") {
			document.getElementById("celldiv").style.display = "block";
			document.getElementById("celldiv").style.visibility = "visible";
			document.getElementById("ddcells").innerHTML = "";
			document.getElementById("ddcells").options[0] = new Option('Select Station/Cell', '0');
		}
		else if(ddl=="ddeq") {
			document.getElementById("eqdiv").style.display = "block";
			document.getElementById("eqdiv").style.visibility = "visible";
			document.getElementById("ddeq").innerHTML = "";
			document.getElementById("ddeq").options[0] = new Option('Select Equipment', '0');
		}
		else if(ddl=="ddfunc") {
			document.getElementById("funcdiv").style.display = "block";
			document.getElementById("funcdiv").style.visibility = "visible";
			document.getElementById("ddfunc").innerHTML = "";
			document.getElementById("ddfunc").options[0] = new Option('Select Function', '0');
		}
		else if(ddl=="ddcomp") {
			document.getElementById("compdiv").style.display = "block";
			document.getElementById("compdiv").style.visibility = "visible";
			document.getElementById("ddcomp").innerHTML = "";
			document.getElementById("ddcomp").options[0] = new Option('Select Component', '0');
		}
		for(var i=0; i<res.value.Tables[0].Rows.length; i++)
		{
			//function getlist(id, newd, tb, v, t, f) = (self, destination dd id, table, dd value, dd text, filter field, self desc, self table)
			if(ddl=="dddepts") {
				val = res.value.Tables[0].Rows[i].Dept_ID;
				text = res.value.Tables[0].Rows[i].Dept_Line;
				listItem = new Option(text, val,  false, false);
				document.getElementById("dddepts").options[document.getElementById("dddepts").length] = listItem;
			}
			else if(ddl=="ddcells") {
				val = res.value.Tables[0].Rows[i].cellid;
				text = res.value.Tables[0].Rows[i].Cell_Name;
				listItem = new Option(text, val,  false, false);
				document.getElementById("ddcells").options[document.getElementById("ddcells").length] = listItem;
			}
			else if(ddl=="ddeq") {
				val = res.value.Tables[0].Rows[i].eqid;
				text = res.value.Tables[0].Rows[i].eqnum;
				listItem = new Option(text, val,  false, false);
				document.getElementById("ddeq").options[document.getElementById("ddeq").length] = listItem;
			}
			else if(ddl=="ddfunc") {
				val = res.value.Tables[0].Rows[i].Func_ID;
				text = res.value.Tables[0].Rows[i].Func;
				listItem = new Option(text, val,  false, false);
				document.getElementById("ddfunc").options[document.getElementById("ddfunc").length] = listItem;
			}
			else if(ddl=="ddcomp") {
				val = res.value.Tables[0].Rows[i].comid;
				text = res.value.Tables[0].Rows[i].compnum;
				listItem = new Option(text, val,  false, false);
				document.getElementById("ddcomp").options[document.getElementById("ddcomp").length] = listItem;
			}
		}//for
		if(ddl=="dddepts") {
		var did = document.getElementById("lbldid").innerHTML;
		if (did!="0") {
		document.getElementById("dddepts").value = did;
		setTimeout("getlist('dddepts', 'ddcells', 'Cells', 'cellid', 'Cell_Name', 'Dept_ID', 'Dept_Desc', 'Dept');", 100);
		}}
		else if(ddl=="ddcells") {
		var clid = document.getElementById("lblclid").innerHTML;
		if (clid!="0") {
		document.getElementById("ddcells").value = clid;
		setTimeout("getlist('ddcells', 'ddeq', 'Equipment', 'eqid', 'eqnum', 'cellid', 'Cell_Desc', 'Cells');", 300);
		}}
		else if(ddl=="ddeq") {
		var eqid = document.getElementById("lbleqid").innerHTML;
		if (eqid!="0") {
		document.getElementById("ddeq").value = eqid;
		setTimeout("getlist('ddeq', 'ddfunc', 'Functions', 'Func_ID', 'Func', 'eqid', 'eqdesc', 'Equipment');", 500);
		}}
		else if(ddl=="ddfunc") {
		var fuid = document.getElementById("lblfuid").innerHTML;
		if (fuid!="0") {
		document.getElementById("ddfunc").value = fuid;
		setTimeout("getlist('ddfunc', 'ddcomp', 'Components', 'comid', 'compnum', 'Func_ID', 'Func_Desc', 'Functions');", 700);
		}}
		else if(ddl=="ddcomp") {
		var coid = document.getElementById("lblcoid").innerHTML;
		if (coid!="0") {
		document.getElementById("ddcomp").value = coid;
		setTimeout("getlist('ddcomp', 'tasks', 'Components', 'comid', 'compnum', 'comid', 'compdesc', 'Components');", 900);
		}}
		}//inner if
		else
		{
			if(ddl=="dddepts") {
			alert("No Department Records Found")	
			}
			else if(ddl=="ddcells") {
			alert("No Cell Records Found")
			}
			else if(ddl=="ddeq") {
			alert("No Equipment Records Found")
			}
			else if(ddl=="ddfunc") {
			alert("No Function Records Found")
			}
			else if(ddl=="ddcomp") {
			alert("No Components Records Found")
			}
		}//inner else
		}//outer if
		}

		function getnext() {		
		savetask();
		Pg = document.getElementById("lblpg").innerHTML;
		Cnt = document.getElementById("lblcnt").innerHTML;
		if (Pg<Cnt) {
		var x = Number(Pg);
		document.getElementById("lblpg").innerHTML = x+1;
		gettasks();
		}
		}
		function getprev() {
		savetask();
		Pg = document.getElementById("lblpg").innerHTML;
		Cnt = document.getElementById("lblcnt").innerHTML;
		if (Pg>1) {
		var x = Number(Pg);
		document.getElementById("lblpg").innerHTML = x-1;
		gettasks();
		}
		}
		function gettasks() {
		Pg = document.getElementById("lblpg").innerHTML;
		if (Pg=="0") {
		Pg="1";
		document.getElementById("lblpg").innerHTML = "1";
		}
		cid = document.getElementById("lblcid").innerHTML;
		GetFilter(cid);
		Tasks.GetCnt(Filter, callback_loadcnt);
		ret = document.getElementById("lblret").innerHTML;
		if (ret=="yes") {
		setTimeout("Tasks.GetTasks(Filter, Pg, callback_loadtasks)", 100);
		}
		else {
		Tasks.GetTasks(Filter, Pg, callback_loadtasks);
		}
		}
		function callback_loadcnt(res) {
		if(res.error != null) alert(res.error);
		if(res != null && res.value != null) {
		document.getElementById("lblcnt").innerHTML = res.value;
		}
		}
		function GetFilter(cid) {
		if (levflg=="1") {
		sid = document.getElementById("ddsites").value;
		Filter = "compid = '" + cid + "' and siteid = '" + sid + "' and subtask = '0' ";
		}
		else if (levflg=="2") {
		did = document.getElementById("dddepts").value;
		Filter = "compid = '" + cid + "' and deptid = '" + did + "' and subtask = '0' ";
		}
		else if (levflg=="3") {
		clid = document.getElementById("ddcells").value;
		Filter = "compid = '" + cid + "' and cellid = '" + clid + "' and subtask = '0' ";
		}
		else if (levflg=="4") {
		eqid = document.getElementById("ddeq").value;
		Filter = "compid = '" + cid + "' and eqid = '" + eqid + "' and subtask = '0' ";
		}
		else if (levflg=="5") {
		funid = document.getElementById("ddfunc").value;
		Filter = "compid = '" + cid + "' and funcid = '" + funid + "' and subtask = '0' ";
		}
		else if (levflg=="6") {
		comid = document.getElementById("ddcomp").value;
		Filter = "compid = '" + cid + "' and comid = '" + comid + "' and subtask = '0' ";
		}
		}
		function AddTask() {
		document.getElementById("msglbl").innerHTML = "";
		cid = document.getElementById("lblcid").innerHTML;
		Cnt = document.getElementById("lblcnt").innerHTML;
		Tnum = Number(Cnt) + 1;
		//alert(cid + " , " + Cnt + " , " + Tnum)
		GetVals(cid, Tnum); 
		//alert(Vals)
		Tasks.AddTask(Vals, callback_chkadd);
		}
		function callback_chkadd(res) {
		if(res.error != null) alert(res.error);
		//alert(res.value)
		if(res != null && res.value != null) {
			var chk = res.value;
			if (chk=="ok") {
			document.getElementById("lblpg").innerHTML = Tnum;
			gettasks();
			}
			else {
			alert(res.value)
			}
		}
		}
		function GetVals(cid, Tnum) {
		//alert(cid + " , " + Tnum)
		if (levflg=="1") {
		sid = document.getElementById("ddsites").value;
		Vals = "(compid, siteid, tasknum, created) values ('" + cid + "', '" + sid + "', '" + Tnum + "', getDate()) ";
		}
		else if (levflg=="2") {
		did = document.getElementById("dddepts").value;
		Vals = "(compid, deptid, tasknum, created) values ('" + cid + "', '" + did + "', '" + Tnum + "', getDate()) ";
		}
		else if (levflg=="3") {
		clid = document.getElementById("ddcells").value;
		Vals = "(compid, cellid, tasknum, created) values ('" + cid + "', '" + clid + "', '" + Tnum + "', getDate()) ";
		}
		else if (levflg=="4") {
		eqid = document.getElementById("ddeq").value;
		Vals = "(compid, eqid, tasknum, created) values ('" + cid + "', '" + eqid + "', '" + Tnum + "', getDate()) ";
		}
		else if (levflg=="5") {
		funid = document.getElementById("ddfunc").value;
		Vals = "(compid, funcid, tasknum, created) values ('" + cid + "', '" + funid + "', '" + Tnum + "', getDate()) ";
		}
		else if (levflg=="6") {
		comid = document.getElementById("ddcomp").value;
		Vals = "(compid, comid, tasknum, created) values ('" + cid + "', '" + comid + "', '" + Tnum + "',  getDate()) ";
		}
		}

		function callback_loadtasks(res) {
		ret = document.getElementById("lblret").innerHTML;
		if (ret=="yes") {
		document.getElementById("lblret").innerHTML = "no";
		getnext();
		}
		else {
		if(res.error != null) alert(res.error + ", load tasks");
		}
		if(res != null && res.value != null && res.value.Tables != 0 && res.value.Tables.length == 1) {
		//alert("ok")
			if(res.value.Tables[0].Rows.length!=0) {
			document.getElementById("lblt").innerHTML = res.value.Tables[0].Rows[0].tasknum;
			document.getElementById("lblst").innerHTML = res.value.Tables[0].Rows[0].subtaskcnt;
			document.getElementById("ddtype").value = res.value.Tables[0].Rows[0].ttid;
			document.getElementById("txtfreq").value = res.value.Tables[0].Rows[0].freqid;
			document.getElementById("txtdesc").value = res.value.Tables[0].Rows[0].taskdesc;
			if (document.getElementById("txtdesc").value=="null") {
			document.getElementById("txtdesc").value="";
			}
			document.getElementById("ddrmt").value = res.value.Tables[0].Rows[0].rmtid;
			document.getElementById("ddpt").value = res.value.Tables[0].Rows[0].ptid;
			document.getElementById("ddskill").value = res.value.Tables[0].Rows[0].skillid;
			document.getElementById("txtqty").value = res.value.Tables[0].Rows[0].qty;
			if (document.getElementById("txtqty").value=="null") {
			document.getElementById("txtqty").value="";
			}
			document.getElementById("txttr").value = res.value.Tables[0].Rows[0].tTime;
			document.getElementById("ddeqstat").value = res.value.Tables[0].Rows[0].rdid;
			document.getElementById("ddloto").value = res.value.Tables[0].Rows[0].lotoid;
			document.getElementById("ddcs").value = res.value.Tables[0].Rows[0].conid;
			document.getElementById("lblpmtid").innerHTML = res.value.Tables[0].Rows[0].pmtskid;
			}
			else {
			document.getElementById("msglbl").innerHTML="No Task Records Created Yet";
			document.getElementById("lblpg").innerHTML = "0";
			}
		}
		}
		function savetask(){
		document.getElementById("msglbl").innerHTML = "";
	var t = document.getElementById("lblt").innerHTML;
	var st = document.getElementById("lblst").innerHTML;
	var typ = document.getElementById("ddtype").value;
	var typn = document.getElementById("ddtype").options[document.getElementById("ddtype").selectedIndex].text;
	var fre = document.getElementById("txtfreq").value;
	var des = document.getElementById("txtdesc").value;
	
	var rmt = document.getElementById("ddrmt").value;
	var rmtn = document.getElementById("ddrmt").options[document.getElementById("ddrmt").selectedIndex].text;
	
	var pt = document.getElementById("ddpt").value;
	var ptn = document.getElementById("ddpt").options[document.getElementById("ddpt").selectedIndex].text;
	
	var ski = document.getElementById("ddskill").value;
	var skin = document.getElementById("ddskill").options[document.getElementById("ddskill").selectedIndex].text;
	
	var qty = document.getElementById("txtqty").value;
	
	var tr = document.getElementById("txttr").value;
	
	var eqs = document.getElementById("ddeqstat").value;
	var eqsn = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").selectedIndex].text;
	//alert("varchk")
	var lot = document.getElementById("ddloto").value;
	var lotn = document.getElementById("ddloto").options[document.getElementById("ddloto").selectedIndex].text;
	var cs = document.getElementById("ddcs").value;
	var csn = document.getElementById("ddcs").options[document.getElementById("ddcs").selectedIndex].text;
	
		cid = document.getElementById("lblcid").innerHTML;
		GetFilter(cid);
		
		Tasks.SaveTasks(t, st, typ, typn, fre, fren, des, rmt, rmtn, pt, ptn, ski, skin, qty, tr, eqs, eqsn, lot, lotn, cs, csn, Filter, callback_chksave);
		}
		function callback_chksave(res) {
		if(res.error != null) alert(res.error);
		if(res != null && res.value != null) {
		var chk = res.value;
		if (chk=="ok") {
		gettasks();
		}
		else {
		alert(res.value)
		}
		}
		}
		
		//start clean
		function cleanchk(id) {
		if (id=="ddsites") {
		cleansitepage();
		}
		if (id=="dddepts") {
		cleandeptpage();
		}
		if (id=="ddcells") {
		cleancellpage();
		}
		if (id=="ddeq") {
		cleaneqpage();
		}
		if (id=="ddfunc") {
		cleanfuncpage();
		}
		if (id=="ddcomp") {
		cleantasks();
		}
		}
		function cleantasks() {
		document.getElementById("lblt").innerHTML = "";
		document.getElementById("lblst").innerHTML = "";
		document.getElementById("ddtype").value = "0";
		document.getElementById("txtfreq").value = "0";
		document.getElementById("txtdesc").value = "";
		document.getElementById("ddrmt").value = "0";
		document.getElementById("ddpt").value = "0";
		document.getElementById("ddskill").value = "0";
		document.getElementById("txtqty").value = "";
		document.getElementById("txttr").value = "";
		document.getElementById("ddeqstat").value = "0";
		document.getElementById("ddloto").value = "0";
		document.getElementById("ddcs").value = "0";
		document.getElementById("msglbl").value = "";
		document.getElementById("lblpg").value = "1";
		document.getElementById("lblcnt").value = "0";
		}
		function cleanpage() {
		document.getElementById("sitediv").style.display = "none";
		document.getElementById("sitediv").style.visibility = "hidden";
		document.getElementById("lblplantdesc").innerHTML = "";
		document.getElementById("deptdiv").style.display = "none";
		document.getElementById("deptdiv").style.visibility = "hidden";
		document.getElementById("lbldeptdesc").innerHTML = "";
		document.getElementById("celldiv").style.display = "none";
		document.getElementById("celldiv").style.visibility = "hidden";
		document.getElementById("lblcelldesc").innerHTML = "";
		document.getElementById("eqdiv").style.display = "none";
		document.getElementById("eqdiv").style.visibility = "hidden";
		document.getElementById("lbleqdesc").innerHTML = "";
		document.getElementById("funcdiv").style.display = "none";
		document.getElementById("funcdiv").style.visibility = "hidden";
		document.getElementById("lblfuncdesc").innerHTML = "";
		document.getElementById("compdiv").style.display = "none";
		document.getElementById("compdiv").style.visibility = "hidden";
		document.getElementById("lblcompdesc").innerHTML = "";
		document.getElementById("txtdesc").innerHTML = "";
		hideiframe();
		
		ret = document.getElementById("lblret").innerHTML;
		if (ret=="yes") {
		tli = document.getElementById("lbltli").innerHTML;
		document.getElementById("ddtasklev").value = tli.charAt(0);
		levflg = tli.charAt(0);
		var cid = document.getElementById("lblcid").innerHTML;
		Tasks.PopSites(cid, callback_getsites);
		}
		
		}
		function checktl() {
		var tlc = document.getElementById("lbltlchk").innerHTML;
		if (tlc=="1") {
		getsites();
		}
		}
		function cleansitepage() {
		document.getElementById("deptdiv").style.display = 'none';
		document.getElementById("deptdiv").style.visibility="hidden";
		document.getElementById("lbldeptdesc").innerHTML = "";
		document.getElementById("celldiv").style.display = 'none';
		document.getElementById("celldiv").style.visibility="hidden";
		document.getElementById("lblcelldesc").innerHTML = "";
		document.getElementById("eqdiv").style.display = 'none';
		document.getElementById("eqdiv").style.visibility="hidden";
		document.getElementById("lbleqdesc").innerHTML = "";
		document.getElementById("funcdiv").style.display = 'none';
		document.getElementById("funcdiv").style.visibility="hidden";
		document.getElementById("lblfuncdesc").innerHTML = "";
		document.getElementById("compdiv").style.display = 'none';
		document.getElementById("compdiv").style.visibility="hidden";
		document.getElementById("lblcompdesc").innerHTML = "";
		}
		function cleandeptpage() {
		document.getElementById("celldiv").style.display = 'none';
		document.getElementById("celldiv").style.visibility="hidden";
		document.getElementById("eqdiv").style.display = 'none';
		document.getElementById("eqdiv").style.visibility="hidden";
		document.getElementById("funcdiv").style.display = 'none';
		document.getElementById("funcdiv").style.visibility="hidden";
		document.getElementById("compdiv").style.display = 'none';
		document.getElementById("compdiv").style.visibility="hidden";
		}
		function cleancellpage() {
		document.getElementById("eqdiv").style.display = 'none';
		document.getElementById("eqdiv").style.visibility="hidden";
		document.getElementById("funcdiv").style.display = 'none';
		document.getElementById("funcdiv").style.visibility="hidden";
		document.getElementById("compdiv").style.display = 'none';
		document.getElementById("compdiv").style.visibility="hidden";
		}
		function cleaneqpage() {
		document.getElementById("funcdiv").style.display = 'none';
		document.getElementById("funcdiv").style.visibility="hidden";
		document.getElementById("compdiv").style.display = 'none';
		document.getElementById("compdiv").style.visibility="hidden";
		}
		function cleanfuncpage() {
		document.getElementById("compdiv").style.display = 'none';
		document.getElementById("compdiv").style.visibility="hidden";
		}
		function hideiframe() {
		document.getElementById("if1").src="";
		document.getElementById("if1").style.visibility="hidden";
		document.getElementById("subdiv").style.display="none";
		document.getElementById("subdiv").style.visibility="hidden";
		document.getElementById("if2").src="";
		document.getElementById("if2").style.visibility="hidden";
		document.getElementById("subdiv2").style.display="none";
		document.getElementById("subdiv2").style.visibility="hidden";
		document.getElementById("if3").src="";
		document.getElementById("if3").style.visibility="hidden";
		document.getElementById("subdiv3").style.visibility="hidden";
		document.getElementById("subdiv3").style.display="none";
		document.getElementById("if4").src="";
		document.getElementById("if4").style.visibility="hidden";
		document.getElementById("subdiv4").style.visibility="hidden";
		document.getElementById("subdiv4").style.display="none";
		document.getElementById("r1").style.display="none";
		document.getElementById("r1").style.visibility="hidden";
		}
		//parts/tools/lubes and notes
		function GetPartDiv() {
		cid = document.getElementById("lblcid").innerHTML;
		ptid = document.getElementById("lblpmtid").innerHTML;
		if (ptid!="") {
		document.getElementById("if2").src="TaskParts.aspx?ptid=" + ptid + "&cid=" + cid;
		document.getElementById("subdiv2").style.top="70px";
		document.getElementById("subdiv2").style.display="block";
		document.getElementById("subdiv2").style.visibility="visible";
		document.getElementById("if2").style.visibility="visible";
		}
		}
		function GetToolDiv() {
		cid = document.getElementById("lblcid").innerHTML;
		ptid = document.getElementById("lblpmtid").innerHTML;
		if (ptid!="") {
		document.getElementById("if1").src="TaskTools.aspx?ptid=" + ptid + "&cid=" + cid;
		document.getElementById("subdiv").style.top="70px";
		document.getElementById("subdiv").style.display="block";
		document.getElementById("subdiv").style.visibility="visible";
		document.getElementById("if1").style.visibility="visible";
		}
		}
		function GetLubeDiv() {
		cid = document.getElementById("lblcid").innerHTML;
		ptid = document.getElementById("lblpmtid").innerHTML;
		if (ptid!="") {
		document.getElementById("if3").src="TaskLubes.aspx?ptid=" + ptid + "&cid=" + cid;
		document.getElementById("subdiv3").style.top="70px";
		document.getElementById("subdiv3").style.display="block";
		document.getElementById("subdiv3").style.visibility="visible";
		document.getElementById("if3").style.visibility="visible";
		}
		}
		function GetNoteDiv() {
		ptid = document.getElementById("lblpmtid").innerHTML;
		if (ptid!="") {
		document.getElementById("if4").src="TaskNotes.aspx?ptid=" + ptid;
		document.getElementById("subdiv4").style.top="90px";
		document.getElementById("subdiv4").style.left="440px";
		document.getElementById("subdiv4").style.display="block";
		document.getElementById("subdiv4").style.visibility="visible";
		document.getElementById("if4").style.visibility="visible";
		}
		}
		//grid view
		function CheckTask(type) {
		tid = document.getElementById("lblt").innerHTML;
		if (tid!="") {
		GetGrid(type);
		}
		else {
		alert("No Tasks Selected")
		}
		}
		function Get1s(type) {
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		sid = document.getElementById("ddsites").value;
		if (type=="subt") {
			//alert("Going to Sub-Task Table View")
			window.location="TaskGrid.aspx?tli=1s&sid=" + sid + "&cid=" + cid + "&tid=" + tid;
			}
		else {
			window.location="GTasks.aspx?tli=1s&sid=" + sid + "&cid=" + cid + "&tid=" + tid;
			}
		}
		function Get1a(type) {
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		sid = document.getElementById("ddsites").value;
			if (type=="allt") {
				//alert("Going to All Tasks Table View")
				window.location="TaskGrid.aspx?tli=1a&sid=" + sid + "&cid=" + cid;
				}
			else {
				window.location="Gtasks.aspx?tli=1a&sid=" + sid + "&cid=" + cid;
				}
		}
		function Get2s(type) {
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		did = document.getElementById("dddepts").value;
		if (type=="subt") {
			//alert("Going to Sub-Task Table View")
			window.location="TaskGrid.aspx?tli=2s&did=" + did + "&cid=" + cid + "&tid=" + tid;
			}
		else {
			window.location="GTasks.aspx?tli=2s&did=" + did + "&cid=" + cid + "&tid=" + tid;
			}
		}
		function Get2a(type) {
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		did = document.getElementById("dddepts").value;
			if (type=="allt") {
				window.location="TaskGrid.aspx?tli=2a&did=" + did + "&cid=" + cid;
				}
			else {
				window.location="GTasks.aspx?tli=2a&did=" + did + "&cid=" + cid;
			}
		}
		function Get3s(type) {
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		clid = document.getElementById("ddcells").value;
		if (type=="subt") {
			//alert("Going to Sub-Task Table View")
			window.location="TaskGrid.aspx?tli=3s&clid=" + clid + "&cid=" + cid + "&tid=" + tid;
			}
		else {
			window.location="GTasks.aspx?tli=3s&clid=" + clid + "&cid=" + cid + "&tid=" + tid;
			}
		}
		function Get3a(type) {
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		clid = document.getElementById("ddcells").value;
			if (type=="allt") {
				window.location="TaskGrid.aspx?tli=3a&clid=" + clid + "&cid=" + cid;
				}
			else {
				window.location="GTasks.aspx?tli=3a&clid=" + clid + "&cid=" + cid;
				}
		}
		function Get4s(type) {
		cell = document.getElementById("cell").innerHTML;
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		eqid = document.getElementById("ddeq").value;
		if (type=="subt") {
			//alert("Going to Sub-Task Table View")
			window.location="TaskGrid.aspx?tli=4s&eqid=" + eqid + "&cid=" + cid + "&tid=" + tid + "&cell=" + cell;
			}
		else {
			window.location="GTasks.aspx?tli=4s&eqid=" + eqid + "&cid=" + cid + "&tid=" + tid + "&cell=" + cell;
			}
		}
		function Get4a(type) {
		cell = document.getElementById("cell").innerHTML;
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		eqid = document.getElementById("ddeq").value;
			if (type=="allt") {
				window.location="TaskGrid.aspx?tli=4a&eqid=" + eqid + "&cid=" + cid + "&cell=" + cell;
				}
			else {
				window.location="Gtasks.aspx?tli=4a&eqid=" + eqid + "&cid=" + cid + "&cell=" + cell;
				}
		}
		function Get5s(type) {
		cell = document.getElementById("cell").innerHTML;
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		funid = document.getElementById("ddfunc").value;
		if (type=="subt") {
			//alert("Going to Sub-Task Table View")
			window.location="TaskGrid.aspx?tli=5s&funid=" + funid + "&cid=" + cid + "&tid=" + tid + "&cell=" + cell;
			}
		else {
			//alert("Going to Sub-Task Grid View")
			window.location="GTasks.aspx?tli=5s&funid=" + funid + "&cid=" + cid + "&tid=" + tid + "&cell=" + cell;
			}
		}
		function Get5a(type) {
		cell = document.getElementById("cell").innerHTML;
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		funid = document.getElementById("ddfunc").value;
			if (type=="allt") {
				//alert("Going to All Tasks Table View")
				window.location="TaskGrid.aspx?tli=5a&funid=" + funid + "&cid=" + cid + "&cell=" + cell;
				}
			else {
				//alert("Going to All Tasks Grid View")
				window.location="GTasks.aspx?tli=5a&funid=" + funid + "&cid=" + cid + "&cell=" + cell;
				}
		}
		function Get6s(type) {
		cell = document.getElementById("cell").innerHTML;
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		comid = document.getElementById("ddcomp").value;
			if (type=="subt") {
			//alert("Going to Sub-Task Table View")
			window.location="TaskGrid.aspx?tli=6s&comid=" + comid + "&cid=" + cid + "&tid=" + tid + "&cell=" + cell;
			}
			else {
			//alert("Going to Sub-Task Grid View")
			window.location="GTasks.aspx?tli=6s&comid=" + comid + "&cid=" + cid + "&tid=" + tid + "&cell=" + cell;
			}
		}
		function Get6a(type) {
		cell = document.getElementById("cell").innerHTML;
		cid = document.getElementById("lblcid").innerHTML;
		tid = document.getElementById("lblt").innerHTML;
		comid = document.getElementById("ddcomp").value;
			if (type=="allt") {
				//alert("Going to All Tasks Table View")
				window.location="TaskGrid.aspx?tli=6a&comid=" + comid + "&cid=" + cid + "&cell=" + cell;
				}
			else {
				//alert("Going to All Tasks Grid View")
				window.location="GTasks.aspx?tli=6a&comid=" + comid + "&cid=" + cid + "&cell=" + cell;
				}
		}
		function GetGrid(type) {
		if (levflg=="1") {
			if (type=="subt"||type=="subg") {
			Get1s(type);
			}
			else {
			Get1a(type);
			}
		}
		else if (levflg=="2") {
			if (type=="subt"||type=="subg") {
			Get2s(type);
			}
			else {
			Get2a(type);
			}
		}
		else if (levflg=="3") {
			if (type=="subt"||type=="subg") {
			Get3s(type);
			}
			else {
			Get3a(type);
			}
		}
		else if (levflg=="4") {
			if (type=="subt"||type=="subg") {
			Get4s(type);
			}
			else {
			Get4a(type);
			}
		}
		else if (levflg=="5") {
			if (type=="subt"||type=="subg") {
			Get5s(type);
			}
			else {
			Get5a(type);
			}
		}
		else if (levflg=="6") {
			if (type=="subt"||type=="subg") {
			Get6s(type);	
			}
			else {
			Get6a(type);
			}
		}
		}
		//auto complete
		function SendQuery(val) {
		filt = val;
		if (filt.length!=0) {
		Tasks.GetSuggest(filt, callback_getsugg);
		}
		else {
		clearsugg()
		}
		}
		function check(val) {
		document.getElementById("txtdesc").value = val;
		clearsugg();
		}
		
		function callback_getsugg(res) {
		if(res.error != null) alert(res.error);
		if(res != null && res.value != null && res.value.Tables != 0 && res.value.Tables.length == 1)
		{
		if (res.value.Tables[0].Rows.length!=0) {
		//alert("has rows")
		document.getElementById("r1").style.display="block";
		document.getElementById("r1").style.visibility="visible";
		var html = [];
		var chk = new Array()
		for(var i=0; i<res.value.Tables[0].Rows.length; i++) {
		chk[i] = res.value.Tables[0].Rows[i].taskdesc;
		var chki = chk[i];
		//alert(chki)
		var t = "s" + i;
		html[html.length] = "<a id=\"" + t + "\" href='javascript:void(0)' onclick=\"check(\'" + chki + "\');\" " +
		"style=\"text-decoration: none\"  ONKEYDOWN=\"return checkNext(this, event, \'" + i + "\')\" onFocus=\"changeclass(\'s" + i + "\');\" onBlur=\"loseclass(\'s" + i + "\');\"><FONT COLOR=\'#000000\' onMouseOver=\" " +
		"this.style.color = \'#cc0000\'\" onMouseOut=\"this.style.color = \'#000000\'\">" + chki + 
		"</font></a><br>";
		document.getElementById("suggest").innerHTML = html.join("");
		}	
		}
		else {
		//alert("has no rows")
		clearsugg()
		}
		}
		}
		function checkArrows (field, evt) {
		var keyCode = 
		document.layers ? evt.which :
		document.all ? event.keyCode :
		document.getElementById ? evt.keyCode : 0;
		var r = '';
		if (keyCode == 40) {
		r += 'arrow down'; 
		document.getElementById("s0").focus(); }
		return true;
		}
		function checkNext (field, evt, id) {
		var keyCode = 
		document.layers ? evt.which :
		document.all ? event.keyCode :
		document.getElementById ? evt.keyCode : 0;
		if (keyCode == 40) {
		var t = parseInt(id) + 1;
		var n = "s" + t
		document.getElementById(n).focus(); 
		}
		else if (keyCode == 38) {
		var t = parseInt(id) - 1;
		var n = "s" + t
		document.getElementById(n).focus(); 
		}
		else if (keyCode == 9) {
		//alert("hello")
		document.getElementById("TextBox1").focus();
		clearsugg();
		return false;
		}
		return true;
		}

		function handleEnter (field, event) {
		var keyCode = event.keyCode ? event.keyCode : 
                event.which ? event.which : event.charCode;
		if (keyCode == 13) {
		 var i;
		for (i = 0; i < field.form.elements.length; i++)
		if (field == field.form.elements[i])
        break;
		i = (i + 1) % field.form.elements.length;
		field.form.elements[i].focus();
		clearsugg();
		return false;
		}
		else
		return true;
		}
		function changeclass(id) {
		document.getElementById(id).className="sug";
		}
		function loseclass(id) {
		document.getElementById(id).className="";
		}
		function clearsugg() {
		document.getElementById("r1").style.display="none";
		document.getElementById("r1").style.visibility="hidden";
		}
		function startpg() {
		cleanpage();
		checktl();
		}
		window.onscroll = clearsugg;
		window.onload = cleanpage;