﻿$(document).ready(function () {

    if (!window.showModalDialog) {

        window.showModalDialog = function (url, callback, props) {

            if (!window.parent.$("#fusionModal").length) {

                window.parent.$("form").append("<div id='fusionModal'></div>");
                window.parent.$("form").append("<input type='hidden' id='fusionModalReturn' />");
            }

            var modalWidth = 700;
            var modalHeight = 500;
            var closable = true;
            
            var arg3Array = props.split(";");
            for (var i = 0; i < arg3Array.length; i++) {
                if (arg3Array[i].indexOf("dialogWidth") > -1) {
                    var widthArray = arg3Array[i].split(":");
                    modalWidth = widthArray[1];
                    modalWidth = modalWidth.replace("px", "");
                }
                if (arg3Array[i].indexOf("dialogHeight") > -1) {
                    var heightArray = arg3Array[i].split(":");
                    modalHeight = heightArray[1];
                    modalHeight = modalHeight.replace("px", "");
                }
                if (arg3Array[i].indexOf("showclose") > -1) {
                    var showCloseArray = arg3Array[i].split(":");
                    var showClose = showCloseArray[1];
                    if (showClose == "no") {
                        closable = false;
                    }
                }
            }

            modalHeight = modalHeight - 50;

            window.parent.$("#fusionModal").html("");
            window.parent.$("#fusionModalReturn").val("");

            window.parent.$("#fusionModal").append("<iframe id='fusioniFrame' src=" + url + " width='" + (modalWidth - 30) + "' height='" + modalHeight + "' scrolling='no'></iframe>").dialog({
                title: " ",
                width: modalWidth,
                height: modalHeight,
                modal: true,
                resizable: true,
                closable: closable,
                onClose: function () {

                    if (callback != "" && window.parent.$("#fusionModalReturn").val() != "") {
                        callback(window.parent.$("#fusionModalReturn").val());
                    }
                    else if (callback != "" && window.parent.$("#fusionModalReturn").val() == "") {
                        callback("ok");
                    }
                }
            });
        }
    }

});

function closeModal(ret) {
    $("#fusionModalReturn").val(ret);
    $("#fusionModal").dialog("close");
}