document.onmousemove = mouseMove;
document.onmouseup   = mouseUp;
window.onmousemove = mouseMove;
window.onmouseup   = mouseUp;

var dragObject  = null;
var mouseOffset = null;

function getMouseOffset(target, ev){
	ev = ev || window.event;

	var docPos    = getPosition(target);
	var mousePos  = mouseCoords(ev);
	return {x:mousePos.x - docPos.x, y:mousePos.y - docPos.y};
}

function getPosition(e){
	var left = 0;
	var top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top  += e.offsetTop;
		e     = e.offsetParent;
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {x:left, y:top};
}

function mouseCoords(ev){
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,
		y:ev.clientY + document.body.scrollTop  - document.body.clientTop
	};
}


function mouseMove(ev){
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);

	if(dragObject){
		dragObject.style.position = 'absolute';
		dragObject.style.top      = mousePos.y - mouseOffset.y;
		dragObject.style.left     = mousePos.x - mouseOffset.x;

		return false;
	}
}
/*function mouseUp(){
	dragObject = null;
}*/

function makeDraggable(item, id, tr){
	if(!item) return;
	item.onmousedown = function(ev){
		dragObject  = this;
		mouseOffset = getMouseOffset(this, ev);
		document.getElementById("lbldragid").value=id;
		document.getElementById("lblrowid").value=tr;
		document.getElementById(id).className="details";//style.visibility="hidden";
		document.getElementById(tr).className="view";
		return false;
	}
}

var dropTargets = [];

function addDropTarget(dropTarget){
	dropTargets.push(dropTarget);
}

function mouseUp(ev){
try {
	ev           = ev || window.event;
	var mousePos = mouseCoords(ev);
	var id = document.getElementById("lbldragid").value;
	var tr = document.getElementById("lblrowid").value;
	//alert(mousePos.y)
	if(mousePos.y < 80) {
	try {
	dragObject.style.top = 90;
	}
	catch(err) {
	document.getElementById(id).style.top = 90;
	}
	}
	for(var i=0; i<dropTargets.length; i++){
		var curTarget  = dropTargets[i];
		var targPos    = getPosition(curTarget);
		var targWidth  = parseInt(curTarget.offsetWidth);
		var targHeight = parseInt(curTarget.offsetHeight);

		if(
			(mousePos.x > targPos.x)                &&
			(mousePos.x < (targPos.x + targWidth))  &&
			(mousePos.y > targPos.y)                &&
			(mousePos.y < (targPos.y + targHeight))){
				// dragObject was dropped onto curTarget!
		}
	}

	dragObject   = null;
	restore(id, tr);
	}
	catch(err) {

	}
}
function restore(id, tr) {
try {
var id1 = document.getElementById("lbldragid").value;
var tr1 = document.getElementById("lblrowid").value;
document.getElementById(id1).className="view";//style.visibility="visible";
document.getElementById(tr1).className="details";
}
catch(err) {

}
}