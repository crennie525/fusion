countries = new Array(

new Array(
new Array("Abkhazia", "Abkhazia"),
new Array("Afghanistan", "Afghanistan"),
new Array("Akrotiri", "Akrotiri"),
new Array("Aland", "Aland"),
new Array("Albania", "Albania"),
new Array("Algeria", "Algeria"),
new Array("American Samoa", "American Samoa"),
new Array("Andorra", "Andorra"),
new Array("Angola", "Angola"),
new Array("Anguilla", "Anguilla"),
new Array("Antigua", "Antigua"),
new Array("Argentina", "Argentina"),
new Array("Armenia", "Armenia"),
new Array("Aruba", "Aruba"),
new Array("Ascension Island", "Ascension Island"),
new Array("Australia", "Australia"),
new Array("Austria", "Austria"),
new Array("Azerbaijan", "Azerbaijan"),
new Array("Bahamas", "Bahamas"),
new Array("Bahrain", "Bahrain"),
new Array("Bangladesh", "Bangladesh"),
new Array("Barbados", "Barbados"),
new Array("Belarus", "Belarus"),
new Array("Belgium", "Belgium"),
new Array("Belize", "Belize"),
new Array("Benin", "Benin"),
new Array("Bermuda", "Bermuda"),
new Array("Bhutan", "Bhutan"),
new Array("Bolivia", "Bolivia"),
new Array("Bosnia", "Bosnia"),
new Array("Botswana", "Botswana"),
new Array("Brazil", "Brazil"),
new Array("British Virgin Islands", "British Virgin Islands"),
new Array("Brunei", "Brunei"),
new Array("Bulgaria", "Bulgaria"),
new Array("Burkina Faso", "Burkina Faso"),
new Array("Burundi", "Burundi"),
new Array("Cambodia", "Cambodia"),
new Array("Cameroon", "Cameroon"),
new Array("Canada", "Canada"),
new Array("Cape Verde", "Cape Verde"),
new Array("Cayman_Islands", "Cayman_Islands"),
new Array("Central African Republic", "Central African Republic"),
new Array("Chad", "Chad"),
new Array("Chile", "Chile"),
new Array("China", "China"),
new Array("Christmas Island", "Christmas Island"),
new Array("Cocos (Keeling) Islands", "Cocos (Keeling) Islands"),
new Array("Colombia", "Colombia"),
new Array("Comoros", "Comoros"),
new Array("Congo (Kinshasa)", "Congo (Kinshasa)"),
new Array("Congo (Brazzaville)", "Congo (Brazzaville)"),
new Array("Cook Islands", "Cook Islands"),
new Array("Costa Rica", "Costa Rica"),
new Array("C�te d'Ivoire", "C�te d'Ivoire"),
new Array("Croatia", "Croatia"),
new Array("Cuba", "Cuba"),
new Array("Cyprus", "Cyprus"),
new Array("Czech Republic", "Czech Republic"),
new Array("Denmark", "Denmark"),
new Array("Dhekelia", "Dhekelia"),
new Array("Djibouti", "Djibouti"),
new Array("Dominica", "Dominica"),
new Array("Dominican Republic", "Dominican Republic"),
new Array("East Timor", "East Timor"),
new Array("Ecuador", "Ecuador"),
new Array("Egypt", "Egypt"),
new Array("El Salvador", "El Salvador"),
new Array("Equatorial Guinea", "Equatorial Guinea"),
new Array("Eritrea", "Eritrea"),
new Array("Estonia", "Estonia"),
new Array("Ethiopia", "Ethiopia"),
new Array("Falkland Islands", "Falkland Islands"),
new Array("Faroe Islands", "Faroe Islands"),
new Array("Fiji", "Fiji"),
new Array("Finland", "Finland"),
new Array("France", "France"),
new Array("French Polynesia", "French Polynesia"),
new Array("Gabon", "Gabon"),
new Array("Gambia", "Gambia"),
new Array("Georgia", "Georgia"),
new Array("Germany", "Germany"),
new Array("Ghana", "Ghana"),
new Array("Gibraltar", "Gibraltar"),
new Array("Greece", "Greece"),
new Array("Greenland", "Greenland"),
new Array("Grenada", "Grenada"),
new Array("Guam", "Guam"),
new Array("Guatemala", "Guatemala"),
new Array("Guernsey", "Guernsey"),
new Array("Guinea", "Guinea"),
new Array("Guinea-Bissau", "Guinea-Bissau"),
new Array("Guyana", "Guyana"),
new Array("Haiti", "Haiti"),
new Array("Honduras", "Honduras"),
new Array("Hong Kong", "Hong Kong"),
new Array("Hungary", "Hungary"),
new Array("Iceland", "Iceland"),
new Array("India", "India"),
new Array("Indonesia", "Indonesia"),
new Array("Iran", "Iran"),
new Array("Iraq", "Iraq"),
new Array("Ireland", "Ireland"),
new Array("Israel", "Israel"),
new Array("Italy", "Italy"),
new Array("Jamaica", "Jamaica"),
new Array("Japan", "Japan"),
new Array("Jersey", "Jersey"),
new Array("Jordan", "Jordan"),
new Array("Kazakhstan", "Kazakhstan"),
new Array("Kenya", "Kenya"),
new Array("Kiribati", "Kiribati"),
new Array("Korea (North)", "Korea (North)"),
new Array("Korea (South)", "Korea (South)"),
new Array("Kosovo", "Kosovo"),
new Array("Kuwait", "Kuwait"),
new Array("Kyrgyzstan", "Kyrgyzstan"),
new Array("Laos", "Laos"),
new Array("Latvia", "Latvia"),
new Array("Lebanon", "Lebanon"),
new Array("Lesotho", "Lesotho"),
new Array("Liberia", "Liberia"),
new Array("Libya", "Libya"),
new Array("Liechtenstein", "Liechtenstein"),
new Array("Lithuania", "Lithuania"),
new Array("Luxembourg", "Luxembourg"),
new Array("Macau", "Macau"),
new Array("Macedonia", "Macedonia"),
new Array("Madagascar", "Madagascar"),
new Array("Malawi", "Malawi"),
new Array("Malaysia", "Malaysia"),
new Array("Maldives", "Maldives"),
new Array("Mali", "Mali"),
new Array("Malta", "Malta"),
new Array("Isle of Man", "Isle of Man"),
new Array("Marshall Islands", "Marshall Islands"),
new Array("Mauritania", "Mauritania"),
new Array("Mauritius", "Mauritius"),
new Array("Mayotte", "Mayotte"),
new Array("Mexico", "Mexico"),
new Array("Micronesia", "Micronesia"),
new Array("Moldova", "Moldova"),
new Array("Monaco", "Monaco"),
new Array("Mongolia", "Mongolia"),
new Array("Montserrat", "Montserrat"),
new Array("Morocco", "Morocco"),
new Array("Mozambique", "Mozambique"),
new Array("Myanmar", "Myanmar"),
new Array("Nagorno-Karabakh", "Nagorno-Karabakh"),
new Array("Namibia", "Namibia"),
new Array("Nauru", "Nauru"),
new Array("Nepal", "Nepal"),
new Array("Netherlands", "Netherlands"),
new Array("Netherlands Antilles", "Netherlands Antilles"),
new Array("New Caledonia", "New Caledonia"),
new Array("New Zealand", "New Zealand"),
new Array("Nicaragua", "Nicaragua"),
new Array("Niger", "Niger"),
new Array("Nigeria", "Nigeria"),
new Array("Niue", "Niue"),
new Array("Norfolk Island", "Norfolk Island"),
new Array("Northern Cyprus", "Northern Cyprus"),
new Array("Northern Mariana Islands", "Northern Mariana Islands"),
new Array("Norway", "Norway"),
new Array("Oman", "Oman"),
new Array("Pakistan", "Pakistan"),
new Array("Palau", "Palau"),
new Array("Palestine", "Palestine"),
new Array("Panama", "Panama"),
new Array("Papua New Guinea", "Papua New Guinea"),
new Array("Paraguay", "Paraguay"),
new Array("Peru", "Peru"),
new Array("Philippines", "Philippines"),
new Array("Pitcairn Islands", "Pitcairn Islands"),
new Array("Poland", "Poland"),
new Array("Portugal", "Portugal"),
new Array("Puerto Rico", "Puerto Rico"),
new Array("Romania", "Romania"),
new Array("Russia", "Russia"),
new Array("Rwanda", "Rwanda"),
new Array("Saint Helena", "Saint Helena"),
new Array("Saint Kitts and Nevis", "Saint Kitts and Nevis"),
new Array("Saint Lucia", "Saint Lucia"),
new Array("Saint Pierre and Miquelon", "Saint Pierre and Miquelon"),
new Array("Saint Vincent and the Grenadines", "Saint Vincent and the Grenadines"),
new Array("Samoa", "Samoa"),
new Array("San Marino", "San Marino"),
new Array("S�o Tom� and Pr�ncipe", "S�o Tom� and Pr�ncipe"),
new Array("Saudi Arabia", "Saudi Arabia"),
new Array("Senegal", "Senegal"),
new Array("Serbia and Montenegro", "Serbia and Montenegro"),
new Array("Seychelles", "Seychelles"),
new Array("Sierra Leone", "Sierra Leone"),
new Array("Singapore", "Singapore"),
new Array("Slovakia", "Slovakia"),
new Array("Slovenia", "Slovenia"),
new Array("Solomon Islands", "Solomon Islands"),
new Array("Somalia", "Somalia"),
new Array("Somaliland", "Somaliland"),
new Array("South Africa", "South Africa"),
new Array("South Ossetia", "South Ossetia"),
new Array("Spain", "Spain"),
new Array("Sri Lanka", "Sri Lanka"),
new Array("Sudan", "Sudan"),
new Array("Suriname", "Suriname"),
new Array("Svalbard", "Svalbard"),
new Array("Swaziland", "Swaziland"),
new Array("Sweden", "Sweden"),
new Array("Switzerland", "Switzerland"),
new Array("Syria", "Syria"),
new Array("Taiwan", "Taiwan"),
new Array("Tajikistan", "Tajikistan"),
new Array("Tanzania", "Tanzania"),
new Array("Thailand", "Thailand"),
new Array("Togo", "Togo"),
new Array("Tokelau", "Tokelau"),
new Array("Tonga", "Tonga"),
new Array("Transnistria", "Transnistria"),
new Array("Trinidad and Tobago", "Trinidad and Tobago"),
new Array("Tristan da Cunha", "Tristan da Cunha"),
new Array("Tunisia", "Tunisia"),
new Array("Turkey", "Turkey"),
new Array("Turkmenistan", "Turkmenistan"),
new Array("Turks and Caicos Islands", "Turks and Caicos Islands"),
new Array("Tuvalu", "Tuvalu"),
new Array("Uganda", "Uganda"),
new Array("Ukraine", "Ukraine"),
new Array("United Arab Emirates", "United Arab Emirates"),
new Array("United Kingdom", "United Kingdom"),
new Array("United States", "United States"),
new Array("Uruguay", "Uruguay"),
new Array("Uzbekistan", "Uzbekistan"),
new Array("Vanuatu", "Vanuatu"),
new Array("Vatican City", "Vatican City"),
new Array("Venezuela", "Venezuela"),
new Array("Vietnam", "Vietnam"),
new Array("Virgin Islands", "Virgin Islands"),
new Array("Wallis and Futuna", "Wallis and Futuna"),
new Array("Western Sahara", "Western Sahara"),
new Array("Yemen", "Yemen"),
new Array("Zambia", "Zambia"),
new Array("Zimbabwe", "Zimbabwe")
),
new Array(
new Array("Alabama", "AL"),                                           
new Array("Alaska", " AK"),                                           
new Array("Arizona", "AZ"),                                           
new Array("Arkansas", "AR"),                     
new Array("California", "CA"),               
new Array("Colorado", "CO"),             
new Array("Connecticut", "CT"),                 
new Array("Delaware", "DE"),                  
new Array("Dist. of Columbia", "DC"),                     
new Array("Florida", "FL"),                                          
new Array("Georgia", "GA"),                     
new Array("Guam", "GU"),                                          
new Array("Hawaii", "HI"),                                            
new Array("Idaho", "ID"),                                            
new Array("Illinois", "IL"),                                          
new Array("Indiana", "IN"),                                            
new Array("Iowa", "IA"),                                             
new Array("Kansas", "KS"),                                            
new Array("Kentucky", "KY"),                                             
new Array("Louisiana", "LA"),                                            
new Array("Maine", "ME"),                                             
new Array("Maryland", "MD"),                                             
new Array("Massachusetts", "MA"),                                            
new Array("Michigan", "MI"),                                            
new Array("Minnesota", "MN"),                                            
new Array("Mississippi", "MS"),                                             
new Array("Missouri", "MO"),                                             
new Array("Montana", "MT"),                                             
new Array("Nebraska", "NE"),                                           
new Array("Nevada", "NV"),                                             
new Array("New Hampshire", "NH"),                                             
new Array("New Jersey", "NJ"),                                           
new Array("New Mexico", "NM"), 
new Array("New York", "NY"),                                             
new Array("North Carolina", "NC"),                                             
new Array("North Dakota", "ND"),                       
new Array("Ohio", "OH"),                                            
new Array("Oklahoma", "OK"),                                             
new Array("Oregon", "OR"),                                             
new Array("Pennsylvania", "PA"),                                            
new Array("Puerto Rico", "PR"),                                           
new Array("Rhode Island", "RI"),                                           
new Array("South Carolina", "SC"),                                             
new Array("South Dakota", "SD"),                                           
new Array("Tennessee", "TN"),                                           
new Array("Texas", "TX"),                                             
new Array("Utah", "UT"),                                             
new Array("Vermon", "VT"),                                             
new Array("Virginia", "VA"),                                             
new Array("Virgin Islands", "VI"),                                             
new Array("Washington WA"),                                            
new Array("West Virginia", "WV"),                                           
new Array("Wisconsin", "WI"),                                            
new Array("Wyoming", "WY"),                                                                
new Array("New South Wales", "NSW"),                       
new Array("Northern Territory", "NT"),                                             
new Array("Queensland", "QLD"),                                             
new Array("South Australia", "SA"),                      
new Array("Tasmani", "TAS"),                      
new Array("Victoria", "VIC"),                      
new Array("Western Australia", "WA")
)
);

function Get(n) {
//alert("ok")
var Ctrl = document.getElementById("ddcountry");
itemArray = countries[n];
var i, j;
for (i = Ctrl.options.length; i >= 0; i--) {
	Ctrl.options[i] = null; 
	}
j = 1;
if (itemArray != null) {
for (i = 0; i < itemArray.length; i++) {
Ctrl.options[j] = new Option(itemArray[i][0]);
if (itemArray[i][1] != null) {
Ctrl.options[j].value = itemArray[i][1];}
j++;
}
}
var s = document.getElementById("lblcindex").value;
document.getElementById("form1").ddcountry.selectedIndex = s;
}
function GetS(n) {
//alert("ok")
var Ctrl = document.getElementById("ddstate");
itemArray = countries[n];
var i, j;
for (i = Ctrl.options.length; i >= 0; i--) {
	Ctrl.options[i] = null; 
	}
j = 1;
if (itemArray != null) {
for (i = 0; i < itemArray.length; i++) {
Ctrl.options[j] = new Option(itemArray[i][0]);
if (itemArray[i][1] != null) {
Ctrl.options[j].value = itemArray[i][1];}
j++;
}
}
var s = document.getElementById("lblsindex").value;
document.getElementById("form1").ddstate.selectedIndex = s;
}

function GetC() {
var c = document.getElementById("form1").ddcountry.selectedIndex;
var t = document.getElementById("form1").ddcountry.options[c].value;
document.getElementById("form1").lblcindex.value = s;
document.getElementById("form1").lblcountry.value = t;
}
function GetSt() {
var s = document.getElementById("form1").ddstate.selectedIndex;
var t = document.getElementById("form1").ddstate.options[s].value;
document.getElementById("form1").lblsindex.value = s;
document.getElementById("form1").lblstate.value = t;
}

function GetC1() {
var c = document.getElementById("form1").countries.selectedIndex;
var t = document.getElementById("form1").countries.options[c].value;

document.getElementById("form1").txtcountry.value = t;
}
function GetSt1() {
var s = document.getElementById("form1").states.selectedIndex;
var t = document.getElementById("form1").states.options[s].value;

document.getElementById("form1").txtstate.value = t;
}
