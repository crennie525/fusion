

'********************************************************
'*
'********************************************************



Public Class PMSetAdjManDialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pmid, pmhid, typ, fuid, tid, Login, ro, rev As String
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iffmadj As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString

            If typ = "pm" Then
                pmid = Request.QueryString("pmid").ToString
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                iffmadj.Attributes.Add("src", "PMSetAdjMan.aspx?pmid=" + pmid + "&ro=" + ro)
            ElseIf typ = "pre" Then
                pmid = Request.QueryString("fuid").ToString
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                iffmadj.Attributes.Add("src", "PMSetAdjManPRE.aspx?pmid=" + pmid + "&ro=" + ro + "&tid=0&rev=0&typ=0")
            ElseIf typ = "rev" Or typ = "trev" Then
                pmid = Request.QueryString("fuid").ToString
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                rev = Request.QueryString("rev").ToString
                iffmadj.Attributes.Add("src", "PMSetAdjManPRE.aspx?pmid=" + pmid + "&ro=" + ro + "&tid=0&rev=" & rev & "&typ=" & typ)
            Else
                pmid = Request.QueryString("fuid").ToString
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                tid = Request.QueryString("tid").ToString
                iffmadj.Attributes.Add("src", "PMSetAdjManPRE.aspx?pmid=" + pmid + "&tid=" + tid + "&ro=" + ro + "&rev=0&typ=0")
            End If

        End If
    End Sub

End Class
