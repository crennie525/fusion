

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMMainMan
    Inherits System.Web.UI.Page
    Dim mu As New mmenu_utils_a
    Dim coi As String
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents ovid83 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid82 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid81 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid80 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid79 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid78 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents iwi As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents iwa As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imgi3 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imgi2 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imgi As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imga3 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imga2 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents imga As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang662 As System.Web.UI.WebControls.Label

    Protected WithEvents lang661 As System.Web.UI.WebControls.Label

    Protected WithEvents lang660 As System.Web.UI.WebControls.Label

    Protected WithEvents lang659 As System.Web.UI.WebControls.Label

    Protected WithEvents lang658 As System.Web.UI.WebControls.Label

    Protected WithEvents lang657 As System.Web.UI.WebControls.Label

    Protected WithEvents lang656 As System.Web.UI.WebControls.Label

    Protected WithEvents lang655 As System.Web.UI.WebControls.Label

    Protected WithEvents lang654 As System.Web.UI.WebControls.Label

    Protected WithEvents lang653 As System.Web.UI.WebControls.Label

    Protected WithEvents lang652 As System.Web.UI.WebControls.Label

    Protected WithEvents lang651 As System.Web.UI.WebControls.Label

    Protected WithEvents lang650 As System.Web.UI.WebControls.Label

    Protected WithEvents lang649 As System.Web.UI.WebControls.Label

    Protected WithEvents lang648 As System.Web.UI.WebControls.Label

    Protected WithEvents lang647 As System.Web.UI.WebControls.Label

    Protected WithEvents lang646 As System.Web.UI.WebControls.Label

    Protected WithEvents lang645 As System.Web.UI.WebControls.Label

    Protected WithEvents lang644 As System.Web.UI.WebControls.Label

    Protected WithEvents lang643 As System.Web.UI.WebControls.Label

    Protected WithEvents lang642 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim man As New Utilities
    Dim sid, cid, srch, jump, eqid, fuid, coid, typ, ro, Login, tab, islabor, issched, userid, isplanner, cadm As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = "50"
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents TextBox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txte As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddpdmlist As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblprint As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddfunc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcomp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsrchwo As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddstatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cbbylab As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifrt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtrte As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblrtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgrteprint As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgrtewprint As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblprintwolist As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgwopm As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfiltwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgregpm As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divywr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhide As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwrs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tbwrs As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lbllocret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trdepts As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trlocs3 As System.Web.UI.HtmlControls.HtmlTableRow

    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdloc3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco3 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents lblsupstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblleadstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbls As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lble As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissched As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblref As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim cnt, ref As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                userid = HttpContext.Current.Session("userid").ToString()
                islabor = HttpContext.Current.Session("islabor").ToString()
                isplanner = HttpContext.Current.Session("isplanner").ToString()
                cadm = HttpContext.Current.Session("cadm").ToString()
                lbluserid.Value = userid
                lblislabor.Value = islabor
                lblisplanner.Value = isplanner
                lblcadm.Value = cadm
            Catch ex As Exception

            End Try
            Try
                tab = Request.QueryString("tab").ToString
                lbltab.Value = tab
                tdwrs.Width = 674
                tbwrs.Width = 720
            Catch ex As Exception

            End Try
            lblhide.Value = "no"
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            lblro.Value = ro
            man.Open()
            coi = mu.COMPI
            lblcoi.Value = coi
            If coi = "NISS_SYM" Or coi = "NISS" Then
                sql = "exec usp_smy_pmwoclean"
                man.Update(sql)
            End If
            txtpg.Value = PageNumber
            'LoadSuper()
            LoadSkill()
            GetPdM()
            'GetDept()
            GetLists()
            'PopEq()

            Try
                jump = Request.QueryString("jump").ToString
                If jump = "yes" Then
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    If typ = "eq" Then
                        eqid = Request.QueryString("eqid").ToString
                        lbleqid.Value = eqid
                    ElseIf typ = "fu" Then
                        fuid = Request.QueryString("fuid").ToString
                        lblfuid.Value = fuid
                    ElseIf typ = "co" Then
                        coid = Request.QueryString("coid").ToString
                        lblcoid.Value = coid
                    Else
                        lbltyp.Value = "no"
                    End If
                Else
                    lbltyp.Value = "no"
                End If
            Catch ex As Exception
                lbltyp.Value = "no"
            End Try
            GetPg(PageNumber)
            man.Dispose()
            'ddcells.Enabled = False
            lblprint.Value = "no"
        Else
            If Request.Form("lblprint") = "go" Then
                man.Open()
                GetPg(PageNumber, "yes")
                man.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                man.Open()
                GetNext()
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                man.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetPg(PageNumber)
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                man.Open()
                GetPrev()
                man.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                man.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetPg(PageNumber)
                man.Dispose()
                lblret.Value = ""
            End If
        End If
        'ibfilt.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov71" , "PMMainMan.aspx.vb") & "', BELOW, LEFT)")
        'ibfilt.Attributes.Add("onmouseout", "return nd()")

    End Sub
    Private Sub GetLists()
        'get default
        Dim def As String
        sql = "select wostatus from wostatus where compid = '" & cid & "' and isdefault = 1"
        dr = man.GetRdrData(sql)
        While dr.Read
            def = dr.Item("wostatus").ToString
        End While
        dr.Close()
        sql = "select wostatus from wostatus where compid = '" & cid & "' and active = 1"
        dr = man.GetRdrData(sql)
        ddstatus.DataSource = dr
        ddstatus.DataTextField = "wostatus"
        ddstatus.DataValueField = "wostatus"
        ddstatus.DataBind()
        dr.Close()
        'If def <> "" Then
        'ddstatus.SelectedValue = def
        'Else
        ddstatus.Items.Insert(0, New ListItem("Select"))
        ddstatus.Items(0).Value = 0
        'End If

    End Sub
    Private Sub GetCell()
        sid = lblsid.Value 'HttpContext.Current.Session("dfltps").ToString
        Dim dept As String = dddepts.SelectedValue.ToString
        sql = "select c.cellid, c.cell_name from cells c where c.dept_id = '" & dept & "'"
        dr = man.GetRdrData(sql)
        ddcells.DataSource = dr
        ddcells.DataValueField = "cellid"
        ddcells.DataTextField = "cell_name"
        ddcells.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddcells.Items.Insert(0, "All Cells")
        Try
            ddcells.SelectedIndex = 0
        Catch ex As Exception

        End Try

        ddcells.Enabled = True
    End Sub
    Private Sub GetDept()
        sid = lblsid.Value 'HttpContext.Current.Session("dfltps").ToString
        sql = "select d.dept_id, d.dept_line from dept d where d.siteid = '" & sid & "'"
        dr = man.GetRdrData(sql)
        dddepts.DataSource = dr
        dddepts.DataValueField = "dept_id"
        dddepts.DataTextField = "dept_line"
        dddepts.DataBind()
        dr.Close()
        dddepts.Items.Insert(0, "All Departments")
        Try
            dddepts.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopEq()
        sid = lblsid.Value 'HttpContext.Current.Session("dfltps").ToString
        Dim dt, val As String
        Dim eqcnt As Integer
        Dim filt As String
        sql = "select count(*) from equipment where siteid = '" & sid & "'"
        filt = " where siteid = '" & sid & "'"
        eqcnt = man.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "
            dr = man.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr338" , "PMMainMan.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = False
        End If

    End Sub
    Private Sub GetPdM()
        sid = lblsid.Value 'HttpContext.Current.Session("dfltps").ToString
        sql = "select distinct pm.ptid, pm.pretech from equipment e " _
        + "right join pm pm on pm.eqid = e.eqid " _
        + "where e.siteid = '" + sid + "' and ptid <> 0"
        dr = man.GetRdrData(sql)
        ddpdmlist.DataSource = dr
        ddpdmlist.DataValueField = "ptid"
        ddpdmlist.DataTextField = "pretech"
        ddpdmlist.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddpdmlist.Items.Insert(0, "All Skills")
        Try
            ddpdmlist.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadSkill()
        sql = "select distinct p.skillid, p.skill from pm p left join equipment e on e.eqid = p.eqid"
        dr = man.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataValueField = "skillid"
        ddskill.DataTextField = "skill"
        ddskill.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddskill.Items.Insert(0, "All Skills")
        Try
            ddskill.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Function fixsrch(ByVal srch As String)
        srch = man.ModString2(srch)
        Return srch
    End Function
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        lblsrch.Value = "yes"
        PageNumber = 1
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub
    Private Sub GetPg(ByVal PageNumber As Integer, Optional ByVal pall As String = "no")
        Dim filt, s, e, Filter, FilterCNT, srch As String

        sid = lblsid.Value
        cid = "0"

        issched = lblissched.Value
        userid = lbluserid.Value
        isplanner = lblisplanner.Value
        islabor = lblislabor.Value
        cadm = lblcadm.Value

        Dim sup, nex, las As String
        Dim piccnt As Integer

        Dim sflag As Integer = 0
        typ = lbltyp.Value
        'If typ = "no" Then
        If lblsrch.Value = "yes" Then
            Filter = "e.siteid = ''" & sid & "'' and pm.nextdate is not null "
            FilterCNT = "e.siteid = '" & sid & "' and pm.nextdate is not null "
        Else
            Filter = "e.siteid = ''" & sid & "'' and pm.nextdate is not null "
            FilterCNT = "e.siteid = '" & sid & "' and pm.nextdate is not null "
        End If

        If txtsrchwo.Text <> "" Then
            srch = txtsrchwo.Text
            srch = fixsrch(srch)
            Dim woi As Long
            Try
                woi = System.Convert.ToInt32(srch)
            Catch ex As Exception
                Dim strMessage As String = "Work Order Number Must Be A Numeric Value"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            Filter += "and pm.wonum = ''" & srch & "'' "
            FilterCNT += "and pm.wonum = '" & srch & "' "
        End If

        If ddstatus.SelectedIndex <> 0 And ddstatus.SelectedIndex <> -1 Then
            Filter += "and w.status = ''" & ddstatus.SelectedValue.ToString & "'' "
            FilterCNT += "and w.status = '" & ddstatus.SelectedValue.ToString & "' "
        Else
            Filter += "and w.status not in (''COMP'',''CLOSE'') "
            FilterCNT += "and w.status not in ('COMP','CLOSE') "
        End If

        If lblsup.Value <> "" Then
            srch = lblsup.Value 'txtsup.Text
            srch = fixsrch(srch)
            Filter += "and pm.superid = ''" & srch & "'' "
            FilterCNT += "and pm.superid = '" & srch & "' "
        End If
        If lbllead.Value <> "" Then
            srch = lbllead.Value 'txtlead.Text
            srch = fixsrch(srch)
            Filter += "and pm.leadid = ''" & srch & "'' "
            FilterCNT += "and pm.leadid = '" & srch & "' "
        End If

        If lblrtid.Value <> "" Then
            srch = lblrtid.Value
            srch = fixsrch(srch)
            Filter += "and pm.rid = ''" & lblrtid.Value & "'' "
            FilterCNT += "and pm.rid = '" & lblrtid.Value & "' "
        End If


        'And dddepts.SelectedIndex <> 0
        If lbldept.Value <> "" Then
            Filter += "and e.dept_id = ''" & lbldid.Value & "'' "
            FilterCNT += "and e.dept_id = '" & lbldid.Value & "' "
        End If
        If lblclid.Value <> "" Then
            Filter += "and e.cellid = ''" & lblclid.Value & "'' "
            FilterCNT += "and e.cellid = '" & lblclid.Value & "' "
        End If

        FilterCNT += " and w.worktype = 'PM'"
        Filter += " and w.worktype = ''PM''"

        Dim parchk, eqchk As Integer
        Dim curlocid As String
        curlocid = lbllid.Value
        Dim curloc As String
        Dim par As String = lbllocstr.Value
        lblloc.Value = lbllocstr.Value
        If curlocid <> "" Then
            sql = "Select location from pmlocations where locid = '" & curlocid & "'"
            curloc = man.strScalar(sql)
            sql = "select count(*) from pmlocheir where location = '" & curloc & "' and parent is null"
            parchk = man.Scalar(sql)
            sql = "select isnull(eqid, 0) from pmlocations where location = '" & curloc & "'"
            eqchk = man.Scalar(sql)

            If eqchk <> 0 Then
                'reg ?
                Filter += "and e.eqnum in (select h.location from pmlocheir h where h.parent = ''" & par & "'')"
                FilterCNT += "and e.eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "')"
            Else
                If parchk = 0 Then
                    'using top level
                    par = curloc
                End If
                Filter += "and (e.eqnum in (select distinct h.location from pmlocheir h where h.parent = ''" & par & "'') " _
                + "or e.eqnum in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = ''" & par & "'')) " _
+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = ''" & par & "''))) " _
+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where h.parent = ''" & par & "'')))) " _
                + "or e.eqnum in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where h.parent = ''" & par & "''))))) " _
                + "or e.eqnum in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where " _
                + "h.parent in (select distinct h.location from pmlocheir h where h.parent = ''" & par & "'')))))) )" _
                '+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = ''" & par & "'')))))) " _
                ' "or e.eqnum in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = ''" & par & "'')))))))) ) "

                FilterCNT += "and (e.eqnum in (select distinct h.location from pmlocheir h where h.parent = '" & par & "') " _
                + "or e.eqnum in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = '" & par & "')) " _
+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = '" & par & "'))) " _
+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = '" & par & "')))) " _
+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = '" & par & "'))))) " _
+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where " _
+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = '" & par & "')))))) )" _
'+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = '" & par & "'))))))) " _
                '+ "or e.eqnum in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where " _
                '+ "h.parent in (select distinct h.location from pmlocheir h where h.parent = '" & par & "')))))))) ) "
            End If
        End If



        'If lbllid.Value <> "" And par <> "" Then
        ''e.eqnum in (select h.location from pmlocheir h where h.parent = 'CRTST')
        ''Filter += "and e.locid = ''" & lbllid.Value & "'' "
        ''FilterCNT += "and e.locid = '" & lbllid.Value & "' "
        'Filter += "and e.eqnum in (select h.location from pmlocheir h where h.parent = ''" & par & "'')"
        'FilterCNT += "and e.eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "')"
        'lblloc.Value = lbllocstr.Value
        'End If

        If lbleqid.Value <> "" Then
            Filter += "and pm.eqid = ''" & lbleqid.Value & "'' "
            FilterCNT += "and pm.eqid = '" & lbleqid.Value & "' "
        End If
        If lblfuid.Value <> "" Then
            Filter += "and pt.funcid = ''" & lblfuid.Value & "'' "
            FilterCNT += "and pt.funcid = '" & lblfuid.Value & "' "
        End If
        If lblcomid.Value <> "" Then
            Filter += "and pt.comid = ''" & lblcomid.Value & "'' "
            FilterCNT += "and pt.comid = '" & lblcomid.Value & "' "
        End If
        If ddskill.SelectedIndex <> 0 And ddskill.SelectedIndex <> -1 Then
            Filter += "and pm.skillid = ''" & ddskill.SelectedValue.ToString & "'' "
            FilterCNT += "and pm.skillid = '" & ddskill.SelectedValue.ToString & "' "
        End If

        If ddpdmlist.SelectedIndex <> 0 And ddpdmlist.SelectedIndex <> -1 Then
            FilterCNT += "and pm.ptid = '" & ddpdmlist.SelectedValue.ToString & "'"
            Filter += "and pm.ptid = ''" & ddpdmlist.SelectedValue.ToString & "''"
        End If

        Dim rtid As String
        'was txtrte.Text
        If lblrtid.Value <> "" Then
            rtid = lblrtid.Value
            FilterCNT += "and pm.rid = '" & rtid & "'"
            Filter += "and pm.rid = ''" & rtid & "''"
            imgrteprint.Attributes.Add("class", "")
            imgrtewprint.Attributes.Add("class", "")

            imgregpm.Attributes.Add("class", "details")
            imgwopm.Attributes.Add("class", "details")
        Else
            imgrteprint.Attributes.Add("class", "details")
            imgrtewprint.Attributes.Add("class", "details")
            If lblsrch.Value = "yes" Then
                imgregpm.Attributes.Add("class", "")
                imgwopm.Attributes.Add("class", "")
            Else
                imgregpm.Attributes.Add("class", "details")
                imgwopm.Attributes.Add("class", "details")
            End If
        End If
        'Else
        If typ = "eq" Then
            eqid = lbleqid.Value
            FilterCNT += "and pm.eqid = '" & eqid & "'"
            Filter += "and pm.eqid = ''" & eqid & "''"
        ElseIf typ = "fu" Then
            fuid = lblfuid.Value
            FilterCNT += "and pt.funcid = '" & fuid & "'"
            Filter += "and pt.funcid = ''" & fuid & "''"
        ElseIf typ = "co" Then
            coid = lblcoid.Value
            FilterCNT += "and pt.comid = '" & coid & "'"
            Filter += "and pt.comid = ''" & coid & "''"
        End If
        'End If
        If cadm <> "1" And cbbylab.Checked = False Then
            If islabor = "1" And isplanner <> "1" Then
                If Filter <> "" Then
                    Filter += " and (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & userid & "'') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = ''" & userid & "''))"
                    FilterCNT += " and (w.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = '" & userid & "'))"
                Else
                    Filter += " (w.eqid in (select eqid from pmlaborlocs where laborid = ''" & userid & "'') or " _
                       + "w.eqid in (select eqid from workorder where leadcraftid = ''" & userid & "''))"
                    FilterCNT += " (w.eqid in (select eqid from pmlaborlocs where laborid = '" & userid & "') or " _
                        + "w.eqid in (select eqid from workorder where leadcraftid = '" & userid & "'))"
                End If

            End If
        End If

        s = lbls.Value 'txts.Text 'fixsrch(txts.Text)
        e = lble.Value 'txte.Text 'fixsrch(txte.Text)
        If Len(s) <> 0 Then
            If Len(e) <> 0 Then
                FilterCNT += " and pm.nextdate between '" & s & "' and '" & e & "'"
                Filter += " and pm.nextdate between ''" & s & "'' and ''" & e & "''"
            Else
                FilterCNT += " and pm.nextdate >= '" & s & "'"
                Filter += " and pm.nextdate >= ''" & s & "''"
            End If
        End If

        sql = "select count(distinct pm.pmid) from pm pm " _
        + "left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.wonum = pm.wonum " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
        + "left join pmtrack pt on pt.pmid = pm.pmid " _
            + " where " + FilterCNT

        Dim intPgCnt, intPgNav As Integer
        intPgCnt = man.Scalar(sql)
        'intPgCnt = 1

        If intPgCnt > 0 Then
            txtpg.Value = PageNumber
            intPgNav = man.PageCountRev(intPgCnt, PageSize)
            sql = "usp_getAllPMPg2 '" & sid & "', '" & PageNumber & "', '" & PageSize & "', '" & Filter & "'"
            Dim ds As New DataSet
            ds = man.GetDSData(sql)
            Dim dt As New DataTable
            dt = ds.Tables(0)
            rptrtasks.DataSource = dt
            rptrtasks.DataBind()
            Dim i, eint As Integer
            eint = 15
            lblfiltwo.Value = " where " & FilterCNT
            lblfilt.Value = " where " & FilterCNT
            filt = " where " & FilterCNT
            'GetDocs(filt)
            Dim row As DataRow
            Dim pmidstr, pmid As String
            For Each row In dt.Rows
                pmid = row("pmid").ToString
                If pmidstr = "" Then
                    pmidstr = pmid
                Else
                    pmidstr += "," & pmid
                End If
            Next
            Dim pmidarr() As String = pmidstr.Split(",")
            Dim sbc As Integer
            Dim doc, docs, fn, ty As String
            For i = 0 To pmidarr.Length - 1
                pmid = pmidarr(i).ToString
                'sql = "select count(*) from pmManAttach where pmid = '" & pmid & "'"
                'sbc = man.Scalar(sql)
                'If sbc > 0 Then ' should be using has rows instead of count
                sql = "select * from pmManAttach where pmid = '" & pmid & "'"
                ds = man.GetDSData(sql)
                dt = ds.Tables(0)
                'dr = man.GetRdrData(sql)
                'While dr.Read
                For Each row In dt.Rows
                    fn = row("filename").ToString 'dr.Item("filename").ToString
                    ty = row("doctype").ToString 'dr.Item("doctype").ToString
                    doc = ty & "~" & fn
                    If docs = "" Then
                        docs = doc
                    Else
                        docs += ";" & doc
                    End If
                Next

                'End While
                'dr.Close()
                lbldocs.Value = docs
                'End If
            Next
            If pall = "yes" Then
                lblprint.Value = "yes"
            End If
        Else
            lblfiltwo.Value = ""
            lblfilt.Value = ""
            lblprint.Value = "no"
            imgregpm.Attributes.Add("class", "details")
            imgwopm.Attributes.Add("class", "details")
            imgrteprint.Attributes.Add("class", "details")
            imgrtewprint.Attributes.Add("class", "details")
            Dim strMessage As String = tmod.getmsg("cdstr339", "PMMainMan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        Dim who As String = lblwho.Value
        If who = "depts" Then
            trdepts.Attributes.Add("class", "view")
            'tddept.InnerHtml = lbldept.Value
            'tdcell.InnerHtml = lblcell.Value
            'tdeq.InnerHtml = lbleq.Value
            'tdfu.InnerHtml = lblfu.Value
            'tdco.InnerHtml = lblcomp.Value
        Else
            trdepts.Attributes.Add("class", "details")
        End If
        If who = "locs" Then
            trlocs3.Attributes.Add("class", "view")
            'tdloc3.InnerHtml = lblloc.Value
            'tdeq3.InnerHtml = lbleq.Value
            'tdfu3.InnerHtml = lblfu.Value
            'tdco3.InnerHtml = lblcomp.Value
        Else
            trlocs3.Attributes.Add("class", "details")
        End If

        Try
            If lblsrch.Value = "yes" Then
                sql = "declare @ref int insert into wofiltref (filt) values (' where " & Filter & "') set @ref = @@identity select @ref"
                ref = man.Scalar(sql)
                lblref.Value = ref
            End If
        Catch ex As Exception

        End Try
        
        
    End Sub
    Private Sub GetDocs(ByVal filt As String)
        sql = "select distinct pm.pmid from pm pm left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.pmid = pm.pmid " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
        + "left join pmtrack pt on pt.pmid = pm.pmid " & filt
        Dim pmidstr, pmid As String
        Dim sbc As Integer
        Dim doc, docs, fn, ty As String
        dr = man.GetRdrData(sql)
        While dr.Read
            If pmidstr = "" Then
                pmidstr = dr.Item("pmid").ToString
            Else
                pmidstr += "," & dr.Item("pmid").ToString
            End If
        End While
        dr.Close()
        Dim pmidarr() As String = pmidstr.Split(",")
        Dim i As Integer
        For i = 0 To pmidarr.Length - 1
            pmid = pmidarr(i).ToString
            sql = "select count(*) from pmManAttach where pmid = '" & pmid & "'"
            sbc = man.Scalar(sql)
            If sbc > 0 Then ' should be using has rows instead of count
                sql = "select * from pmManAttach where pmid = '" & pmid & "'"
                dr = man.GetRdrData(sql)
                While dr.Read
                    fn = dr.Item("filename").ToString
                    ty = dr.Item("doctype").ToString
                    doc = ty & "~" & fn
                    If docs = "" Then
                        docs = doc
                    Else
                        docs += ";" & doc
                    End If
                End While
                dr.Close()
                lbldocs.Value = docs
            End If
        Next
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetPg(PageNumber)
        Catch ex As Exception
            man.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr340", "PMMainMan.aspx.vb")

            man.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetPg(PageNumber)
        Catch ex As Exception
            man.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr341", "PMMainMan.aspx.vb")

            man.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub


    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub

    Private Sub rptrtasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            Dim tdwo As HtmlTableCell = CType(e.Item.FindControl("tdwo"), HtmlTableCell)
            'Dim tblywr As HtmlTable = CType(e.Item.FindControl("tblywr"), HtmlTable)
            tab = lbltab.Value
            If tab = "yes" Then
                tdwo.Attributes.Add("class", "details")
                'tblywr.Attributes.Add("width", "670")
            End If
        End If


        If e.Item.ItemType = ListItemType.Item Then

            Dim tdwoi As HtmlTableCell = CType(e.Item.FindControl("tdwoi"), HtmlTableCell)
            tab = lbltab.Value
            If tab = "yes" Then
                tdwoi.Attributes.Add("class", "details")
            End If


            Dim img As HtmlImage = CType(e.Item.FindControl("imgi"), HtmlImage)
            Dim imgi As HtmlImage = CType(e.Item.FindControl("imgi2"), HtmlImage)
            Dim imgi3 As HtmlImage = CType(e.Item.FindControl("imgi3"), HtmlImage)
            Dim link As LinkButton = CType(e.Item.FindControl("lbltn"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmid").ToString
            Dim eid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim wo As String = DataBinder.Eval(e.Item.DataItem, "wonum").ToString
            Dim fccnt As String = DataBinder.Eval(e.Item.DataItem, "piccnt").ToString
            Dim rtid As String = DataBinder.Eval(e.Item.DataItem, "rtid").ToString
            img.Attributes("onclick") = "print('" & id & "');"
            'img.Attributes("onclick") = "print('" & id & "');"
            imgi.Attributes("onclick") = "printwo('" & id & "','" & wo & "','" & rtid & "');"
            If fccnt <> "0" And fccnt <> "" Then
                imgi3.Attributes("onclick") = "printwopics('" & id & "','" & wo & "');"
            Else
                imgi3.Attributes.Add("class", "details")
            End If
            sid = lblsid.Value
            link.Attributes("onclick") = "jump('" & id & "', '" & eid & "','" & fccnt & "','" & sid & "');"
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwi"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            Try
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov72", "PMMainMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov73", "PMMainMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Catch ex As Exception

            End Try

        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim tdwoe As HtmlTableCell = CType(e.Item.FindControl("tdwoe"), HtmlTableCell)
            tab = lbltab.Value
            If tab = "yes" Then
                tdwoe.Attributes.Add("class", "details")
            End If

            Dim img As HtmlImage = CType(e.Item.FindControl("imga"), HtmlImage)
            Dim imga As HtmlImage = CType(e.Item.FindControl("imga2"), HtmlImage)
            Dim imga3 As HtmlImage = CType(e.Item.FindControl("imga3"), HtmlImage)
            Dim link As LinkButton = CType(e.Item.FindControl("lbltnalt"), LinkButton)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmid").ToString
            Dim eid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim wo As String = DataBinder.Eval(e.Item.DataItem, "wonum").ToString
            Dim fccnt1 As String = DataBinder.Eval(e.Item.DataItem, "piccnt").ToString
            Dim rtid As String = DataBinder.Eval(e.Item.DataItem, "rtid").ToString
            img.Attributes("onclick") = "print('" & id & "');"
            'img.Attributes("onclick") = "print('" & id & "');"
            imga.Attributes("onclick") = "printwo('" & id & "','" & wo & "','" & rtid & "');"
            sid = lblsid.Value
            link.Attributes("onclick") = "jump('" & id & "', '" & eid & "','" & fccnt1 & "','" & sid & "');"
            If fccnt1 <> "0" And fccnt1 <> "" Then
                imga3.Attributes("onclick") = "printwopics('" & id & "','" & wo & "');"
            Else
                imga3.Attributes.Add("class", "details")
            End If
            Dim simg As HtmlImage = CType(e.Item.FindControl("iwa"), HtmlImage)
            Dim newdate As String = DataBinder.Eval(e.Item.DataItem, "nextdate").ToString
            Try
                newdate = CType(newdate, DateTime)
                If newdate < Now Then
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/rwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov74", "PMMainMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                Else
                    simg.Attributes.Add("src", "../images/appbuttons/minibuttons/gwarningnbg.gif")
                    simg.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov75", "PMMainMan.aspx.vb") & "', ABOVE, LEFT)")
                    simg.Attributes.Add("onmouseout", "return nd()")
                End If
            Catch ex As Exception

            End Try

        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang642 As Label
                lang642 = CType(e.Item.FindControl("lang642"), Label)
                lang642.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang642")
            Catch ex As Exception
            End Try
            Try
                Dim lang643 As Label
                lang643 = CType(e.Item.FindControl("lang643"), Label)
                lang643.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang643")
            Catch ex As Exception
            End Try
            Try
                Dim lang644 As Label
                lang644 = CType(e.Item.FindControl("lang644"), Label)
                lang644.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang644")
            Catch ex As Exception
            End Try
            Try
                Dim lang645 As Label
                lang645 = CType(e.Item.FindControl("lang645"), Label)
                lang645.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang645")
            Catch ex As Exception
            End Try
            Try
                Dim lang646 As Label
                lang646 = CType(e.Item.FindControl("lang646"), Label)
                lang646.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang646")
            Catch ex As Exception
            End Try
            Try
                Dim lang647 As Label
                lang647 = CType(e.Item.FindControl("lang647"), Label)
                lang647.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang647")
            Catch ex As Exception
            End Try
            Try
                Dim lang648 As Label
                lang648 = CType(e.Item.FindControl("lang648"), Label)
                lang648.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang648")
            Catch ex As Exception
            End Try
            Try
                Dim lang649 As Label
                lang649 = CType(e.Item.FindControl("lang649"), Label)
                lang649.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang649")
            Catch ex As Exception
            End Try
            Try
                Dim lang650 As Label
                lang650 = CType(e.Item.FindControl("lang650"), Label)
                lang650.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang650")
            Catch ex As Exception
            End Try
            Try
                Dim lang651 As Label
                lang651 = CType(e.Item.FindControl("lang651"), Label)
                lang651.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang651")
            Catch ex As Exception
            End Try
            Try
                Dim lang652 As Label
                lang652 = CType(e.Item.FindControl("lang652"), Label)
                lang652.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang652")
            Catch ex As Exception
            End Try
            Try
                Dim lang653 As Label
                lang653 = CType(e.Item.FindControl("lang653"), Label)
                lang653.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang653")
            Catch ex As Exception
            End Try
            Try
                Dim lang654 As Label
                lang654 = CType(e.Item.FindControl("lang654"), Label)
                lang654.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang654")
            Catch ex As Exception
            End Try
            Try
                Dim lang655 As Label
                lang655 = CType(e.Item.FindControl("lang655"), Label)
                lang655.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang655")
            Catch ex As Exception
            End Try
            Try
                Dim lang656 As Label
                lang656 = CType(e.Item.FindControl("lang656"), Label)
                lang656.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang656")
            Catch ex As Exception
            End Try
            Try
                Dim lang657 As Label
                lang657 = CType(e.Item.FindControl("lang657"), Label)
                lang657.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang657")
            Catch ex As Exception
            End Try
            Try
                Dim lang658 As Label
                lang658 = CType(e.Item.FindControl("lang658"), Label)
                lang658.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang658")
            Catch ex As Exception
            End Try
            Try
                Dim lang659 As Label
                lang659 = CType(e.Item.FindControl("lang659"), Label)
                lang659.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang659")
            Catch ex As Exception
            End Try
            Try
                Dim lang660 As Label
                lang660 = CType(e.Item.FindControl("lang660"), Label)
                lang660.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang660")
            Catch ex As Exception
            End Try
            Try
                Dim lang661 As Label
                lang661 = CType(e.Item.FindControl("lang661"), Label)
                lang661.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang661")
            Catch ex As Exception
            End Try
            Try
                Dim lang662 As Label
                lang662 = CType(e.Item.FindControl("lang662"), Label)
                lang662.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang662")
            Catch ex As Exception
            End Try

        End If

    End Sub

    Private Sub ibfilt_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        man.Open()
        GetPg(PageNumber)
        man.Dispose()
    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = man.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = man.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
    Private Sub dddepts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub PopEq(ByVal var As String, ByVal typ As String)
        Dim filt, dt, val, did, lid, clid As String
        lid = lbllid.Value
        did = lbldept.Value
        clid = lblclid.Value
        Dim eqcnt As Integer
        If typ = "d" Then
            If lid = "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "'"
                filt = " where dept_id = '" & did & "'"
            Else
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            End If

        ElseIf typ = "c" Then
            If lid = "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "'"
                filt = " where cellid = '" & clid & "'"
            Else
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & clid & "'"
            End If

        ElseIf typ = "l" Then
            If clid <> "" Then
                sql = "select count(*) from equipment where cellid = '" & clid & "' and locid = '" & lid & "'"
                filt = " where cellid = '" & clid & "' and locid = '" & lid & "'"
            ElseIf did <> "" Then
                sql = "select count(*) from equipment where dept_id = '" & did & "' and locid = '" & lid & "'"
                filt = " where dept_id = '" & did & "' and locid = '" & lid & "'"
            Else
                sql = "select count(*) from equipment where locid = '" & lid & "'"
                filt = " where locid = '" & lid & "'"
            End If

        End If
        eqcnt = man.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "

            dr = man.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = True
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr342" , "PMMainMan.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = False
        End If
        GetLoc()
    End Sub
    Private Sub GetLoc(Optional ByVal lid As String = "0")
        If lid <> 0 Then
            lid = lbllid.Value
        End If

        If lid <> "" And lid <> "0" Then
            sql = "select location, description from pmlocations where locid = '" & lid & "'"
            dr = man.GetRdrData(sql)
            While dr.Read
                lblloc.Value = dr.Item("location").ToString
            End While
            dr.Close()
        End If

    End Sub
    Private Sub PopCells(ByVal dept As String)
        Dim filt, dt, val As String
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = man.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
        ddcells.Enabled = True
    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        Dim cell As String = ddcells.SelectedValue.ToString
        If ddcells.SelectedIndex <> 0 Then
            Try
                man.Open()
                lblclid.Value = cell
                PopEq(cell, "c")
                'PopNC(cell, "c")
                ddeq.Enabled = True
                man.Dispose()
            Catch ex As Exception
                man.Dispose()
                'Dim strMessage As String =  tmod.getmsg("cdstr343" , "PMMainMan.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
            lblclid.Value = ""
        End If
    End Sub

    Private Sub ddeq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddeq.SelectedIndexChanged
        Dim eq As String = ddeq.SelectedValue.ToString
        Dim did, clid, lid, Filter As String
        lbleqid.Value = eq
        Dim loctype As String
        If ddeq.SelectedIndex <> 0 Then
            man.Open()
            sql = "select * from equipment where eqid = '" & eq & "'"
            dr = man.GetRdrData(sql)
            While dr.Read
                did = dr.Item("dept_id").ToString
                clid = dr.Item("cellid").ToString
                lid = dr.Item("locid").ToString
                loctype = dr.Item("loctype").ToString
            End While
            dr.Close()
            lbldept.Value = did
            lblclid.Value = clid
            lbllid.Value = lid
            If loctype = "reg" Or loctype = "dloc" Then
                If dddepts.SelectedIndex = 0 Then
                    dddepts.SelectedValue = did
                    Dim chk As String = CellCheck(Filter)
                    If chk = "no" Then
                        lblchk.Value = "no"
                    Else
                        lblchk.Value = "yes"
                        PopCells(did)
                        If clid <> "" Then
                            ddcells.SelectedValue = clid
                        End If
                    End If
                End If
            End If
            If loctype = "loc" Or loctype = "dloc" Then
                GetLoc(lid)
            End If
            PopFunc(eq)
            man.Dispose()
        Else
            lbleqid.Value = ""
        End If
    End Sub
    Private Sub PopComp()
        Dim fuid As String = lblfuid.Value
        sql = "select comid, compnum + ' - ' + isnull(compdesc, '') as compnum " _
          + "from components where func_id = '" & fuid & "' order by compnum desc"
        dr = man.GetRdrData(sql)
        ddcomp.DataSource = dr
        ddcomp.DataTextField = "compnum"
        ddcomp.DataValueField = "comid"
        ddcomp.DataBind()
        dr.Close()
        ddcomp.Items.Insert(0, New ListItem("Select"))
        ddcomp.Items(0).Value = 0
    End Sub
    Private Sub PopFunc(ByVal eq As String)
        Dim dt, val, filt As String
        Dim fucnt As Integer
        sql = "select count(*) from Functions where eqid = '" & eq & "'"
        fucnt = man.Scalar(sql)
        If fucnt = 0 Then
            ddfunc.Items.Insert(0, "Select Function")
            ddfunc.Enabled = "False"
            Dim strMessage As String = tmod.getmsg("cdstr344", "PMMainMan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            dt = "Functions"
            val = "func_id, func"
            filt = " where eqid = '" & eq & "'"
            dr = man.GetList(dt, val, filt)
            ddfunc.DataSource = dr
            ddfunc.DataTextField = "func"
            ddfunc.DataValueField = "func_id"
            ddfunc.DataBind()
            dr.Close()
            ddfunc.Items.Insert(0, "Select Function")
            ddfunc.Enabled = "True"
        End If
    End Sub

    Private Sub ddfunc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddfunc.SelectedIndexChanged
        Dim fuid As String = ddfunc.SelectedValue.ToString
        lblfuid.Value = fuid
        If ddfunc.SelectedIndex <> 0 Then
            man.Open()
            PopComp()
            man.Dispose()
        Else
            lblfuid.Value = ""
        End If
    End Sub



    Private Sub dddepts_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        Dim dept As String = dddepts.SelectedValue.ToString
        lbldept.Value = dept
        Dim Filter, lid As String
        man.Open()
        If dddepts.SelectedIndex <> 0 Then

            Dim deptname As String = dddepts.SelectedItem.ToString
            Filter = dept
            Dim chk As String = CellCheck(Filter)
            If chk = "no" Then
                lblchk.Value = "no"
                lid = lbllid.Value
                If lid <> "" And lid <> "0" Then
                    PopEq(lid, "l")
                    'PopNC(lid, "l")
                Else
                    PopEq(dept, "d")
                    'PopNC(dept, "d")
                End If

            Else
                lblchk.Value = "yes"
                ddeq.Enabled = False
                PopCells(dept)

            End If
        Else
            lbldept.Value = ""
        End If
        man.Dispose()
    End Sub

    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang642.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang642")
        Catch ex As Exception
        End Try
        Try
            lang643.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang643")
        Catch ex As Exception
        End Try
        Try
            lang644.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang644")
        Catch ex As Exception
        End Try
        Try
            lang645.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang645")
        Catch ex As Exception
        End Try
        Try
            lang646.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang646")
        Catch ex As Exception
        End Try
        Try
            lang647.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang647")
        Catch ex As Exception
        End Try
        Try
            lang648.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang648")
        Catch ex As Exception
        End Try
        Try
            lang649.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang649")
        Catch ex As Exception
        End Try
        Try
            lang650.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang650")
        Catch ex As Exception
        End Try
        Try
            lang651.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang651")
        Catch ex As Exception
        End Try
        Try
            lang652.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang652")
        Catch ex As Exception
        End Try
        Try
            lang653.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang653")
        Catch ex As Exception
        End Try
        Try
            lang654.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang654")
        Catch ex As Exception
        End Try
        Try
            lang655.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang655")
        Catch ex As Exception
        End Try
        Try
            lang656.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang656")
        Catch ex As Exception
        End Try
        Try
            lang657.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang657")
        Catch ex As Exception
        End Try
        Try
            lang658.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang658")
        Catch ex As Exception
        End Try
        Try
            lang659.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang659")
        Catch ex As Exception
        End Try
        Try
            lang660.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang660")
        Catch ex As Exception
        End Try
        Try
            lang661.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang661")
        Catch ex As Exception
        End Try
        Try
            lang662.Text = axlabs.GetASPXPage("PMMainMan.aspx", "lang662")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imga.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imga") & "', ABOVE, LEFT)")
            imga.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imga2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imga2") & "', ABOVE, LEFT)")
            imga2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imga3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imga3") & "', ABOVE, LEFT)")
            imga3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imgi") & "', ABOVE, LEFT)")
            imgi.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imgi2") & "', ABOVE, LEFT)")
            imgi2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imgi3") & "', ABOVE, LEFT)")
            imgi3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgregpm.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imgregpm") & "', ABOVE, LEFT)")
            imgregpm.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgrteprint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imgrteprint") & "', ABOVE, LEFT)")
            imgrteprint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgrtewprint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imgrtewprint") & "', ABOVE, LEFT)")
            imgrtewprint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgwopm.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "imgwopm") & "', ABOVE, LEFT)")
            imgwopm.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwa.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "iwa") & "', ABOVE, LEFT)")
            iwa.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwi.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "iwi") & "', ABOVE, LEFT)")
            iwi.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid78.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "ovid78") & "')")
            ovid78.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid79.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "ovid79") & "')")
            ovid79.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid80.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "ovid80") & "')")
            ovid80.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid81.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "ovid81") & "')")
            ovid81.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid82.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "ovid82") & "')")
            ovid82.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid83.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "ovid83") & "')")
            ovid83.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            tdwrs.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMMainMan.aspx", "tdwrs") & "', ABOVE, LEFT)")
            tdwrs.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub


End Class
