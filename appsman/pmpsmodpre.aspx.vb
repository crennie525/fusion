﻿Imports System.Data.SqlClient
Public Class pmpsmodpre
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Tables As String = "pmtrack"
    Dim PK As String = "pmtid"
    Dim Sort As String
    Dim dr As SqlDataReader
    Dim pmid As String
    Dim sql As String
    Dim adj As New Utilities
    Dim taskhold As String = "0"
    Dim headhold As String = "0"
    Dim func, funchold, task, pmhid, pmtskid, tid, ro, typ, rev, eqid As String
    Dim funce, funcholde As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'GetFSLangs()

        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then

            End If
            tid = "2" 'Request.QueryString("tid").ToString
            pmid = "0" 'Request.QueryString("pmid").ToString
            eqid = Request.QueryString("eqid").ToString '"393" '
            lbltid.Value = tid
            lblpmid.Value = pmid
            lbleqid.Value = eqid
            rev = "" 'Request.QueryString("rev").ToString
            typ = "0" 'Request.QueryString("typ").ToString
            lblrev.Value = rev
            lbltyp.Value = typ
            adj.Open()
            LoadAdj(tid)
            adj.Dispose()
        End If
    End Sub
    Private Sub LoadAdj(ByVal tid As String)
        pmid = lblpmid.Value
        rev = lblrev.Value
        typ = lbltyp.Value
        eqid = lbleqid.Value
        If typ = "rev" Then
            sql = "usp_getpmadjPREarch '" & pmid & "', '" & tid & "','" & rev & "'"
        ElseIf typ = "trev" Then
            sql = "usp_getpmadjPREarchtpm '" & pmid & "', '" & tid & "','" & rev & "'"
        Else
            sql = "usp_getpmadjPREmod '" & pmid & "', '" & tid & "', '" & eqid & "'"
        End If

        Dim ds As New DataSet
        ds = adj.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dgadj.DataSource = dv
        dgadj.DataBind()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            iL = CatID
        Else
            iL = -1
        End If
        Return iL
    End Function
    Private Sub dgadj_CancelCommand(source As Object, e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.CancelCommand
        dgadj.EditItemIndex = -1
        tid = lbltid.Value
        adj.Open()
        pmid = lblpmid.Value
        LoadAdj(tid)
        adj.Dispose()
    End Sub

    Private Sub dgadj_EditCommand(source As Object, e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.EditCommand
        lblrow.Value = CType(e.Item.FindControl("A1"), Label).Text '
        lbloadjc.Value = CType(e.Item.FindControl("lblokadjcurr"), Label).Text
        lblfadjc.Value = CType(e.Item.FindControl("lblfadjcurr"), Label).Text
        tid = lbltid.Value
        adj.Open()
        pmid = lblpmid.Value
        dgadj.EditItemIndex = e.Item.ItemIndex
        LoadAdj(tid)
        adj.Dispose()
    End Sub

    Private Sub dgadj_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dgadj.ItemDataBound
        Dim trhd, trd As HtmlTableRow
        Dim fcnt As Integer = 0


        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If funchold = "" Then
                funchold = "0"
            End If
            ro = lblro.Value
            If ro = "1" Then '
                Dim ibn As ImageButton = CType(e.Item.FindControl("Imagebutton1"), ImageButton)
                ibn.ImageUrl = "../images/appbuttons/minibuttons/lilpendis.gif"
                ibn.Enabled = False
            End If
            func = CType(e.Item.FindControl("tdfunci"), HtmlTableCell).InnerHtml.ToString
            If funchold = "0" Then
                funchold = func
                'Else
                'If func <> funchold Then
                'funchold = func
                'End If
            End If
            If headhold = "0" Then
                headhold = "1"
                trhd = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                trhd.Attributes.Add("class", "view")
                trd = CType(e.Item.FindControl("trdiv"), HtmlTableRow)
                trd.Attributes.Add("class", "view")
            Else
                If func <> funchold And funchold <> "0" Then
                    trhd = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                    trhd.Attributes.Add("class", "view")
                    trd = CType(e.Item.FindControl("trdiv"), HtmlTableRow)
                    trd.Attributes.Add("class", "view")
                    funchold = func
                Else
                    trhd = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                    trhd.Attributes.Add("class", "details")
                    trd = CType(e.Item.FindControl("trdiv"), HtmlTableRow)
                    trd.Attributes.Add("class", "details")
                End If

            End If
        ElseIf e.Item.ItemType = ListItemType.EditItem Then
            If headhold = "0" Then
                headhold = "1"
                trhd = CType(e.Item.FindControl("trfunce"), HtmlTableRow)
                trhd.Attributes.Add("class", "view")
                trd = CType(e.Item.FindControl("trdive"), HtmlTableRow)
                trd.Attributes.Add("class", "view")
            Else
                trhd = CType(e.Item.FindControl("trfunce"), HtmlTableRow)
                trhd.Attributes.Add("class", "details")
                trd = CType(e.Item.FindControl("trdive"), HtmlTableRow)
                trd.Attributes.Add("class", "details")
            End If
        End If
    End Sub

    Private Sub dgadj_UpdateCommand(source As Object, e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.UpdateCommand
        Dim pmtid As Integer
        pmtid = lblrow.Value
        Dim okadj, fadj, okuse, fuse As String
        Dim po As String = lbloadjc.Value
        Dim fo As String = lblfadjc.Value
        okuse = CType(e.Item.FindControl("ddokuse"), DropDownList).SelectedValue.ToString
        If okuse = 0 Then okuse = "2"
        fuse = CType(e.Item.FindControl("ddfuse"), DropDownList).SelectedValue.ToString
        If fuse = 0 Then fuse = "2"
        okadj = CType(e.Item.FindControl("lblokadj"), TextBox).Text.ToString
        Dim okchk As Long
        Try
            okchk = System.Convert.ToInt64(okadj)
            po = System.Convert.ToInt64(po)
            If po <> okchk Then
                po = 1
            Else
                po = 0
            End If
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr349", "PMSetAdjManPRE.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        fadj = CType(e.Item.FindControl("lblfadj"), TextBox).Text.ToString
        Dim fchk As Long
        Try
            fchk = System.Convert.ToInt64(okadj)
            fo = System.Convert.ToInt64(fo)
            If fo <> fchk Then
                fo = 1
            Else
                po = 0
            End If
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr350", "PMSetAdjManPRE.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "update pmtasks set okadj = '" & okadj & "', okalt = '" & po & "', useadj = '" & okuse & "', " _
        + "fadj = '" & fadj & "', falt = '" & fo & "', usefadj = '" & fuse & "' " _
        + "where pmtskid = '" & pmtid & "'"
        dgadj.EditItemIndex = -1
        adj.Open()
        adj.Update(sql)
        adj.UpModTask(pmtid)
        pmid = lblpmid.Value
        tid = lbltid.Value
        LoadAdj(tid)
        adj.Dispose()
    End Sub

End Class