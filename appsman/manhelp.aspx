<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="manhelp.aspx.vb" Inherits="lucy_r12.manhelp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>manhelp</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="z-index: 1; left: 7px; position: absolute; top: 7px">
        <tr>
            <td class="plainlabelred" height="32">
                <asp:Label ID="lang531" runat="server">The following information applies to both PM and TPM Record Sets</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang532" runat="server">To create or review a PM Record Set;</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="plainlabelblue">
                <br>
                <asp:Label ID="lang533" runat="server">Select an Equipment Record</asp:Label><br>
                <br>
                <asp:Label ID="lang534" runat="server">If a PM Record Set Exists for your selected Equipment Record these records will be displayed in the Grid View.</asp:Label><br>
                <br>
                <asp:Label ID="lang535" runat="server">If no PM Record Set Exists for your selected Equipment Record it will then be reviewed to make sure there are a sufficient number of records to create a new PM Record Set.</asp:Label><br>
                <br>
                <asp:Label ID="lang536" runat="server">If a sufficient number of records are found you will be presented with an Alert Message stating that No PM Records Created for this Equipment Record and to Click the Add Button to Create a PM Record Set for this Asset.</asp:Label><br>
                <br>
                <asp:Label ID="lang537" runat="server">If a sufficient number of records are not found you will be presented with an Alert Message stating that No PM Record Set has been created for this Equipment Record and that you should review this record in the PM Optimizer or PM Developer to ensure that all Skills, Frequencies, and Running/Down Times have been established.</asp:Label><br>
                <br>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang538" runat="server">To update a PM Record Set;</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="plainlabelblue">
                <br>
                <asp:Label ID="lang539" runat="server">If your selected Equipment Record has been updated in the PM Optimizer or PM Developer a red Alert Icon will appear next to your selected Equipment Record. If your selected Equipment Record has not been updated this Alert Icon will always be green.</asp:Label><br>
                <br>
                <asp:Label ID="lang540" runat="server">Clicking this icon in either color state will allow you to update your current PM Record Set.</asp:Label><br>
                <br>
                <asp:Label ID="lang541" runat="server">If any of your PM Records are currently scheduled you will be presented with an Alert Message stating that you have PM Records with pending due dates, that _ performing this update could change task requirements, and if you wish to continue.</asp:Label><br>
                <asp:Label ID="lang542" runat="server">If any PM in this PM Record Set is currently in progress or an email regarding any PM in this PM Record Set has already has already been sent you will need to send a copy of the current revision of any updated PM to any persons responsible for performing this PM immediately.</asp:Label>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
