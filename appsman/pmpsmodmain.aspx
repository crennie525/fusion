﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmpsmodmain.aspx.vb" Inherits="lucy_r12.pmpsmodmain" %>
<%@ Register Src="../menu/mmenu1.ascx" TagName="mmenu1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript">
    <!--
        function getminsrch() {
            var did = document.getElementById("lbldid").value;
            //alert(did)
            if (did != "") {
                retminsrch();
            }
            else {
                var sid = document.getElementById("lblsid").value;
                var wo = "";
                var typ = "lu";
                var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    clearall();
                    //alert(eReturn)
                    var ret = eReturn.split("~");
                    var did = ret[0];
                    document.getElementById("lbldchk").value = "yes";
                    document.getElementById("lbldid").value = ret[0];
                    document.getElementById("lbldept").value = ret[1];
                    document.getElementById("tddept").innerHTML = ret[1];
                    var clid = ret[2];
                    //alert(clid)
                    document.getElementById("lblclid").value = ret[2];
                    if (ret[2] != "") {
                        document.getElementById("lblchk").value = "yes";
                    }
                    else {
                        document.getElementById("lblchk").value = "no";
                    }
                    document.getElementById("lblcell").value = ret[3];
                    document.getElementById("tdcell").innerHTML = ret[3];
                    var eqid = ret[4];
                    document.getElementById("lbleqid").value = ret[4];
                    document.getElementById("tdeqd").innerHTML = ret[5];
                    var fuid = ret[6];
                    document.getElementById("lblfuid").value = ret[6];
                    var coid = ret[8];
                    document.getElementById("lblcoid").value = ret[8];
                    document.getElementById("lblncid").value = ret[10];
                    document.getElementById("lbllid").value = ret[12];
                    document.getElementById("trdepts").className = "view";
                    document.getElementById("lblrettyp").value = "depts";
                    if (coid != "") {
                        document.getElementById("lbllvl").value = "co";
                        document.getElementById("lblgototasks").value = "1";
                    }
                    else if (fuid != "") {
                        document.getElementById("lbllvl").value = "fu";
                        document.getElementById("lblgototasks").value = "1";
                    }
                    else if (eqid != "") {
                        document.getElementById("lbllvl").value = "eq";
                        //alert(document.getElementById("lbllvl").value)
                        document.getElementById("lblgototasks").value = "1";
                    }
                    else {
                        document.getElementById("lbllvl").value = "";
                        document.getElementById("lblgototasks").value = "1";
                    }
                    if (did != "") {
                        document.getElementById("lbldchk").value = "yes";
                    }
                    if (clid != "") {
                        document.getElementById("lblchk").value = "yes";
                    }
                    else {
                        document.getElementById("lblchk").value = "no";
                    }
                    checkret();
                }
            }
        }
        function retminsrch() {
            //alert()
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "ret";
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = ""; // document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            var fu = ""; // document.getElementById("lblfu").value;
            var coid = document.getElementById("lblcoid").value;
            var comp = ""; // document.getElementById("lblcomp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = ""; //  document.getElementById("lblloc").value;
            //if (cell == "" && who != "checkfu" && who != "checkeq") {
            who = "deptret";
            //}
            // alert("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who)
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                //var ret = eReturn.split("~");
                //alert(ret)
                var did = ret[0];
                document.getElementById("lbldchk").value = "yes";
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                var clid = ret[2];
                document.getElementById("lblclid").value = ret[2];
                if (ret[2] != "") {
                    document.getElementById("lblchk").value = "yes";
                }
                else {
                    document.getElementById("lblchk").value = "yes";
                }
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                var eqid = ret[4];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                
                document.getElementById("tdeqd").innerHTML = ret[5];
                var fuid = ret[6];
                document.getElementById("lblfuid").value = ret[6];
                var coid = ret[8];
                document.getElementById("lblcoid").value = ret[8];
                document.getElementById("lblncid").value = ret[10];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";
                if (coid != "") {
                    document.getElementById("lbllvl").value = "co";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (fuid != "") {
                    document.getElementById("lbllvl").value = "fu";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (eqid != "") {
                    document.getElementById("lbllvl").value = "eq";
                    //alert(document.getElementById("lbllvl").value)
                    document.getElementById("lblgototasks").value = "1";
                }
                else {
                    document.getElementById("lblgototasks").value = "1";
                }
                if (did != "") {
                    document.getElementById("lbldchk").value = "yes";
                }
                if (clid != "") {
                    document.getElementById("lblchk").value = "yes";
                }
                else {
                    document.getElementById("lblchk").value = "no";
                }
                checkret();
            }
        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lu"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                var lid = ret[0];
                document.getElementById("lbldchk").value = "no";
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc2").innerHTML = ret[2];
                var eqid = ret[3];
                document.getElementById("lbleqid").value = ret[3];
                document.getElementById("tdeql").innerHTML = ret[4];
                var fuid = ret[5];
                document.getElementById("lblfuid").value = ret[5];
                var coid = ret[7];
                document.getElementById("lblcoid").value = ret[7];
                document.getElementById("trlocs").className = "view";
                document.getElementById("lblrettyp").value = "locs";
                if (coid != "") {
                    document.getElementById("lbllvl").value = "co";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (fuid != "") {
                    document.getElementById("lbllvl").value = "fu";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (eqid != "") {
                    document.getElementById("lbllvl").value = "eq";
                    document.getElementById("lblgototasks").value = "1";
                }

                else {
                    document.getElementById("lbllvl").value = "";
                    document.getElementById("lblgototasks").value = "1";
                }
                if (lid != "") {
                    document.getElementById("lbldchk").value = "no";
                }
                checkret();

            }
        }
        function checkret() {
            var lid = document.getElementById("lbllid").value;
            var did = document.getElementById("lbldid").value;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc"
                }
                else {
                    typ = "reg"
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc"
                }
            }
            document.getElementById("lbltyp").value = typ;
            //handlearch();
            document.getElementById("lblpar").value = "0"
            //document.getElementById("lblgototasks").value= "1";
            //document.getElementById("lbllvl").value = "";
            //checkeq();
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                gettab("tdeq");
            }
        }
        function clearall() {
            document.getElementById("deq").className = 'view';
            document.getElementById("dfu").className = 'details';
            document.getElementById("dco").className = 'details';
            document.getElementById("dot").className = 'details';
            //handlegeteq("EQBotGrid.aspx?start=no&who=&typ=na&sid=na&cid=na&lid=na&did=na&clid=na");
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblcoid").value = "";
            document.getElementById("lblncid").value = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            document.getElementById("lblrettyp").value = "";
        }
        function gettab(id) {
            if (id == "tdeq") {
                //alert("eq")
                var eqid = document.getElementById("lbleqid").value;
                if (eqid != "") {
                    var cid = document.getElementById("lblcid").value;
                    var sid = document.getElementById("lblsid").value;
                    var did = document.getElementById("lbldid").value;
                    var clid = document.getElementById("lblclid").value;

                    document.getElementById("lbltab").value = "eq"
                    document.getElementById("lbllvl").value = "eq"
                    closeall();
                    document.getElementById("tdeq").className = "thdrhov plainlabel";
                    document.getElementById("deq").style.display = 'block';
                    document.getElementById("deq").style.visibility = 'visible';
                    //document.getElementById("lblapp").value = "pmopt";
                    document.getElementById("ifeq").src = "pmpsmodpre.aspx?&eqid=" + eqid;
                }
            }
            else if (id == "tdot") {
                var eqid = document.getElementById("lbleqid").value;
                if (eqid != "") {
                /*
                    document.getElementById("lbltab").value = "ot"
                    closeall();
                    document.getElementById("tdot").className = "thdrhov plainlabel";
                    document.getElementById("dot").style.display = 'block';
                    document.getElementById("dot").style.visibility = 'visible';
                    document.getElementById("lblapp").value = "pmopt";
                    var chk = document.getElementById("lblchk").value;
                    var dchk = document.getElementById("lbldchk").value;
                    var cid = document.getElementById("lblcid").value;
                    var sid = document.getElementById("lblsid").value;
                    var did = document.getElementById("lbldid").value;
                    var clid = document.getElementById("lblclid").value;
                    var lid = document.getElementById("lbllid").value;
                    document.getElementById("ifot").src = "NCGrid.aspx?&dchk=" + dchk + "&chk=" + chk + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid;
                */
                    alert("Under Contruction");
                }
            }
            else if (id == "tdfu") {
                var fuid = document.getElementById("lblfuid").value;
                var eqid = document.getElementById("lbleqid").value;
                //alert(eqid)
                if (eqid != "") {
                /*
                    var cid = document.getElementById("lblcid").value;
                    var sid = document.getElementById("lblsid").value;
                    var did = document.getElementById("lbldid").value;
                    var clid = document.getElementById("lblclid").value;
                    document.getElementById("lblapp").value = "pmopt";
                    document.getElementById("lbltab").value = "fu";
                    document.getElementById("lbllvl").value = "fu"
                    closeall();
                    var cd = document.getElementById("lblcid").value;
                    document.getElementById("iffu").src = "FuncDivGrid.aspx?eqid=" + eqid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid;
                    document.getElementById("tdfu").className = "thdrhov plainlabel";
                    document.getElementById("dfu").style.display = 'block';
                    document.getElementById("dfu").style.visibility = 'visible';
                    */
                    alert("Under Contruction");
                }
                //else {
                    //alert("No Equipment Record Selected!");
                //}
                //}
            }
            else if (id == "tdco") {
                var eqid = document.getElementById("lbleqid").value;
                var fuid = document.getElementById("lblfuid").value;
                if (eqid != "") {
                /*
                    var cid = document.getElementById("lblcid").value;
                    var sid = document.getElementById("lblsid").value;
                    var did = document.getElementById("lbldid").value;
                    var clid = document.getElementById("lblclid").value;
                    var eqid = document.getElementById("lbleqid").value;
                    document.getElementById("lblapp").value = "pmopt";
                    document.getElementById("lbltab").value = "co";
                    document.getElementById("lbllvl").value = "co"
                    closeall();
                    //alert(sid)
                    var cd = document.getElementById("lblcid").value;
                    document.getElementById("ifco").src = "CompDivGrid.aspx?eqid=" + eqid + "&fuid=" + fuid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid;
                    document.getElementById("tdco").className = "thdrhov plainlabel";
                    document.getElementById("dco").style.display = 'block';
                    document.getElementById("dco").style.visibility = 'visible';
                    */
                    alert("Under Contruction");
                }
                //else alert("No Function Record Selected!");
            }
        }
        function closeall() {
            document.getElementById("tdeq").className = "thdr plainlabel";
            document.getElementById("tdfu").className = "thdr plainlabel";
            document.getElementById("tdco").className = "thdr plainlabel";
            document.getElementById("tdot").className = "thdr plainlabel";
            document.getElementById("deq").style.display = 'none';
            document.getElementById("deq").style.visibility = 'hidden';
            document.getElementById("dfu").style.display = 'none';
            document.getElementById("dfu").style.visibility = 'hidden';
            document.getElementById("dco").style.display = 'none';
            document.getElementById("dco").style.visibility = 'hidden';
            document.getElementById("dot").style.display = 'none';
            document.getElementById("dot").style.visibility = 'hidden';
        }


        


    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table style="z-index: 1; position: absolute; top: 76px; left: 2px" cellspacing="0"
        cellpadding="2" width="890">
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td id="tddepts" class="bluelabel" runat="server">
                            Use Departments
                        </td>
                        <td>
                            <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                        </td>
                        <td id="tdlocs1" class="bluelabel" runat="server">
                            Use Locations
                        </td>
                        <td>
                            <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trdepts" class="details" runat="server">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="label" width="110">
                            Department
                        </td>
                        <td id="tddept" class="plainlabel" width="170" runat="server">
                        </td>
                        <td width="50">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Station\Cell
                        </td>
                        <td id="tdcell" class="plainlabel" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Equipment
                        </td>
                        <td id="tdeqd" class="plainlabel" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trlocs" class="details" runat="server">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="label">
                            Location
                        </td>
                        <td id="tdloc2" class="plainlabel" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Equipment
                        </td>
                        <td id="tdeql" class="plainlabel" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="4" height="20">
                <table cellpadding="0" width="770" border="0">
                    <tr height="20">
                        <td class="thdrhov plainlabel" id="tdeq" onclick="gettab('tdeq');" width="140">
                            <asp:Label ID="lang2293" runat="server">PMO Pass/Fail</asp:Label>
                        </td>
                        <td class="thdr plainlabel" id="tdfu" onclick="gettab('tdfu');" width="140">
                            <asp:Label ID="lang2294" runat="server">TPMO Pass/Fail</asp:Label>
                        </td>
                        <td class="thdr plainlabel" id="tdco" onclick="gettab('tdco');" width="140">
                            <asp:Label ID="lang2295" runat="server">PM-CMMS Pass/Fail</asp:Label>
                        </td>
                        <td class="thdr plainlabel" id="tdot" onclick="gettab('tdot');" width="140">
                            <asp:Label ID="lang2296" runat="server">TPM-CMMS Pass/Fail</asp:Label>
                        </td>
                        <td width="182">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="deq" style="z-index: 1; border-bottom: 2px groove; border-left: 1px groove;
                    border-top: 1px groove; border-right: 2px groove">
                    <iframe id="ifeq" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                        border-top-style: none; border-left-style: none" src="../genhold.htm" frameborder="no"
                        width="980" height="420" runat="server"></iframe>
                </div>
                <div class="details" id="dfu" style="z-index: 1; border-bottom: 2px groove; border-left: 1px groove;
                    border-top: 1px groove; border-right: 2px groove">
                    <iframe id="iffu" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                        border-top-style: none; border-left-style: none" src="#" frameborder="no" width="980"
                        height="420" runat="server"></iframe>
                </div>
                <div class="details" id="dco" style="z-index: 1; border-bottom: 2px groove; border-left: 1px groove;
                    border-top: 1px groove; border-right: 2px groove">
                    <iframe id="ifco" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                        border-top-style: none; border-left-style: none" src="#" frameborder="no" width="980"
                        height="420" runat="server"></iframe>
                </div>
                <div class="details" id="dot" style="z-index: 1; border-bottom: 2px groove; border-left: 1px groove;
                    border-top: 1px groove; border-right: 2px groove">
                    <iframe id="ifot" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                        border-top-style: none; border-left-style: none" src="#" frameborder="no" width="980"
                        height="420" runat="server"></iframe>
                </div>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" value="0" name="lblcid" runat="server" />
    <input id="lbltab" type="hidden" name="lbltab" runat="server" />
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server" /><input id="lblsid"
        type="hidden" name="lblsid" runat="server" />
    <input id="lbldid" type="hidden" name="lbldid" runat="server" /><input id="lblclid"
        type="hidden" name="lblclid" runat="server" />
    <input id="lblret" type="hidden" name="lblret" runat="server" />
    <input id="lblchk" type="hidden" name="lblchk" runat="server" />
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server" /><input id="lblfuid"
        type="hidden" name="lblfuid" runat="server" />
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server" />
    <input id="lblapp" type="hidden" runat="server" name="lblapp" />
    <input id="lblpar2" type="hidden" value="0" name="lblpar2" runat="server" /><input
        id="lblpar" type="hidden" value="0" name="lblpar" runat="server" />
    <input id="lblgototasks" type="hidden" name="lblgototasks" runat="server" /><input
        id="appchk" type="hidden" name="appchk" runat="server" />
    <input id="lbllvl" type="hidden" runat="server" name="lbllvl" />
    <input id="lbllid" type="hidden" runat="server" name="lbllid" />
    <input id="lblsubmit" type="hidden" runat="server" name="lblsubmit" /><input id="lbltyp"
        type="hidden" runat="server" name="lbltyp">
    <input id="lblstart" type="hidden" runat="server" name="lblstart" /><input id="lbllochold"
        type="hidden" runat="server" name="lbllochold" />
    <input type="hidden" id="lbllog" runat="server" name="lbllog" />
    <input type="hidden" id="lblfslang" runat="server" name="lblfslang" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblcell" runat="server" /><input id="lblrettyp" type="hidden"
        runat="server" name="lblrettyp" />
    <input type="hidden" id="lblncid" runat="server" /><input type="hidden" id="lblloc"
        runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="yCoord" runat="server" />
    <input type="hidden" id="xCoord" runat="server">
    <uc1:mmenu1 ID="mmenu11" runat="server" />
    </form>
</body>
</html>
