﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmmeters.aspx.vb" Inherits="lucy_r12.pmmeters" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
    <!--
        function getcal(fld) {
         var mid = document.getElementById("lblmeterid").value;
         if (mid != "") {
             var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
             if (eReturn) {
                 document.getElementById(fld).innerHTML = eReturn;
                 document.getElementById("lblnewdate").value = eReturn;
             }
         }
        }
        function addread() {
            var mid = document.getElementById("lblmeterid").value;
            if(mid != "") {
            var nread = document.getElementById("txtnewreading").value;
            var rdate = document.getElementById("lblnewdate").value;
            if (rdate == "") {
                alert("Reading Date Required")
            }
            else if (nread == "") {
                alert("New Reading Required")
            }
            else if (isNaN(nread)) {
                alert("New Reading Must Be a Numeric Value")
            }
            else {
                document.getElementById("lblsubmit").value = "addread";
                document.getElementById("form1").submit();
            }
            }
    }
    function getnext() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg < cnt) {
            document.getElementById("lblret").value = "next"
            document.getElementById("form1").submit();
        }
    }
    function getlast() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg < cnt) {
            document.getElementById("lblret").value = "last"
            document.getElementById("form1").submit();
        }
    }
    function getprev() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg > 1) {
            document.getElementById("lblret").value = "prev"
            document.getElementById("form1").submit();
        }
    }
    function getfirst() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg > 1) {
            document.getElementById("lblret").value = "first"
            document.getElementById("form1").submit();
        }
    }
    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="details" align="center" id="tdmsg" runat="server">No Meter Reading Required</td>
    </tr>
    <tr>
                <td>
                    <table>
                    <tr>
                            <td class="bluelabel" width="100">
                                Work Order#
                            </td>
                            <td id="tdwonum" class="plainlabel" runat="server" width="160">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel" width="100">
                                Equipment#
                            </td>
                            <td id="tdeq" class="plainlabel" runat="server" width="160">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                Meter
                            </td>
                            <td id="tdmeter" runat="server" class="plainlabel">
                            </td>
                            <td>
                                <img alt="" class="details" id="imgmtr" runat="server" onclick="getmeter();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                Units
                            </td>
                            <td id="tdunits" runat="server" class="plainlabel">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr id="trnew" runat="server" width="100">
                            <td class="label">
                                Meter Reading
                            </td>
                            <td>
                                <asp:TextBox ID="txtnewreading" runat="server" Width="100"></asp:TextBox>
                            </td>
                            <td class="label" width="100">
                                Reading Date
                            </td>
                            <td id="tddate" runat="server" class="plainlabel" width="100">
                            </td>
                            <td>
                                <img onclick="getcal('tddate');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                    width="19" height="19">
                            </td>
                            <td>
                                <img id="imgreading" onmouseover="return overlib('Add New Reading', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="addread();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trseeall" runat="server">
            <td class="plainlabel" align="center">
            <a href="#" onclick="getall();">View Full Meter History</a>
            </td>
            </tr>
             <tr id="trgrid" runat="server">
            <td align="center">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                        <AlternatingRowStyle CssClass="ptransrowblue"></AlternatingRowStyle>
                        <RowStyle CssClass="ptransrow"></RowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Edit" Visible="false">
                                <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel" ></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton7" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Meter Reading">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblread" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterreading") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtreading" runat="server" Width="80px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.meterreading") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reading Date">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbldate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.readingdate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblreaddate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.readingdate") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Entered By">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblby" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.enterby") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblbye" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.enterby") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField Visible="false">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmroute" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mroute") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmroute" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mroute") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false">
                                <HeaderStyle Width="400px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblpm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblpme" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmhist" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mhistid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmhiste" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mhistid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
            </td>
            </tr>
            <tr id="trnav" runat="server">
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
    </table>
    </div>
     <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblmeter" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblunit" runat="server" />
    <input type="hidden" id="lblenterby" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblnewdate" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblwonum" runat="server" />
    <input type="hidden" id="lblpmid" runat="server" />
    <input type="hidden" id="lblolddate" runat="server" />
    <input type="hidden" id="lbloldread" runat="server" />
    </form>
</body>
</html>
