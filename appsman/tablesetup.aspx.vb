

'********************************************************
'*
'********************************************************



Public Class tablesetup
    Inherits System.Web.UI.Page
	Protected WithEvents ovid89 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid88 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang749 As System.Web.UI.WebControls.Label

	Protected WithEvents lang748 As System.Web.UI.WebControls.Label

	Protected WithEvents lang747 As System.Web.UI.WebControls.Label

	Protected WithEvents lang746 As System.Web.UI.WebControls.Label

	Protected WithEvents lang745 As System.Web.UI.WebControls.Label

	Protected WithEvents lang744 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlead As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddwt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtstart As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnwo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cbsupe As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbleade As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents txtcorr As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang744.Text = axlabs.GetASPXPage("tablesetup.aspx","lang744")
		Catch ex As Exception
		End Try
		Try
			lang745.Text = axlabs.GetASPXPage("tablesetup.aspx","lang745")
		Catch ex As Exception
		End Try
		Try
			lang746.Text = axlabs.GetASPXPage("tablesetup.aspx","lang746")
		Catch ex As Exception
		End Try
		Try
			lang747.Text = axlabs.GetASPXPage("tablesetup.aspx","lang747")
		Catch ex As Exception
		End Try
		Try
			lang748.Text = axlabs.GetASPXPage("tablesetup.aspx","lang748")
		Catch ex As Exception
		End Try
		Try
			lang749.Text = axlabs.GetASPXPage("tablesetup.aspx","lang749")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			btnwo.Attributes.Add("src" , "../images2/eng/bgbuttons/submit.gif")
			ElseIf lang = "fre" Then
			btnwo.Attributes.Add("src" , "../images2/fre/bgbuttons/submit.gif")
			ElseIf lang = "ger" Then
			btnwo.Attributes.Add("src" , "../images2/ger/bgbuttons/submit.gif")
			ElseIf lang = "ita" Then
			btnwo.Attributes.Add("src" , "../images2/ita/bgbuttons/submit.gif")
			ElseIf lang = "spa" Then
			btnwo.Attributes.Add("src" , "../images2/spa/bgbuttons/submit.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			ovid88.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("tablesetup.aspx","ovid88") & "')")
			ovid88.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid89.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("tablesetup.aspx","ovid89") & "')")
			ovid89.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
