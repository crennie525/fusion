

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmactm
    Inherits System.Web.UI.Page
	Protected WithEvents lang569 As System.Web.UI.WebControls.Label

	Protected WithEvents lang568 As System.Web.UI.WebControls.Label

	Protected WithEvents lang567 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim jpid, wonum, typ, stat, icost, pmid, ro, Login As String
    Dim jpm As New Utilities
    Dim ap As New AppUtils
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdnotes As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgparts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents trp As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trt As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trl As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblview As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgparts.Columns(0).Visible = False
            End If
            wonum = Request.QueryString("wo").ToString
            pmid = Request.QueryString("pmid").ToString
            typ = Request.QueryString("typ").ToString
            lblwo.Value = wonum
            lblpmid.Value = pmid
            lblview.Value = typ
            'lblstat.Value = stat
            Dim icost As String = ap.InvEntry
            If icost = "ext" Then
                tdnotes.InnerHtml = "Material Costs\Usage are Entered Externally"
                dgparts.Columns(0).Visible = False
            ElseIf icost = "inv" Then
                tdnotes.InnerHtml = "Material Costs\Usage are Entered via Inventory"
                dgparts.Columns(0).Visible = False
            Else
                tdnotes.InnerHtml = "Material Costs\Usage are Entered Manually"
            End If
            If typ = "p" Then
                trp.Attributes.Add("class", "view")
            ElseIf typ = "t" Then
                trt.Attributes.Add("class", "view")
            ElseIf typ = "l" Then
                trl.Attributes.Add("class", "view")
                dgparts.Columns(7).Visible = False
            End If
            jpm.Open()
            GetParts()
            jpm.Dispose()
            If wonum = "" Then
                dgparts.Columns(0).Visible = False
            End If
        End If
    End Sub
    Private Sub GetParts()
        wonum = lblwo.Value
        pmid = lblpmid.Value
        typ = lblview.Value
        If typ = "p" Then
            sql = "select m.*, j.* from wopmparts m left join pmtrack j on j.pmtskid = m.pmtskid " _
            + " where m.pmid = '" & pmid & "' and m.wonum = '" & wonum & "' order by j.routing, j.funcid, j.tasknum"
        ElseIf typ = "t" Then
            sql = "select m.*, m.toolnum as itemnum, m.wotsktoolid as wotskpartid, m.rate as cost, j.* from wopmtools m " _
            + "left join pmtrack j on j.pmtskid = m.pmtskid " _
            + " where m.pmid = '" & pmid & "' And m.wonum = '" & wonum & "' order by j.routing, j.funcid, j.tasknum"
        ElseIf typ = "l" Then
            sql = "select m.*, m.lubenum as itemnum, m.wotsklubeid as wotskpartid, j.* from wopmlubes m left join pmtrack j on j.pmtskid = m.pmtskid " _
            + " where m.pmid = '" & pmid & "' And m.wonum = '" & wonum & "' order by j.routing, j.funcid, j.tasknum"
        End If

        dr = jpm.GetRdrData(sql)
        dgparts.DataSource = dr
        dgparts.DataBind()

    End Sub

    Private Sub dgparts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand

            dgparts.EditItemIndex = e.Item.ItemIndex
            jpm.Open()
            GetParts()
            jpm.Dispose()

    End Sub

    Private Sub dgparts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        dgparts.EditItemIndex = -1
        jpm.Open()
        GetParts()
        jpm.Dispose()
    End Sub

    Private Sub dgparts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparts.ItemDataBound
        typ = lblview.Value
        Dim ibfm As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim img As HtmlImage = CType(e.Item.FindControl("imgti"), HtmlImage)
            'Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            If typ = "l" Then
                Dim qbox As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
                qbox.Enabled = False
            End If
            Dim img As HtmlImage = CType(e.Item.FindControl("imgte"), HtmlImage)
            'Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "taskdesc").ToString
            'tdtsk.InnerHtml = tsk
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")
        End If

    End Sub

    Private Sub dgparts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        Dim qtystr As String = CType(e.Item.FindControl("txtqty"), TextBox).Text
        Dim coststr As String = CType(e.Item.FindControl("txtcost"), Label).Text
        Dim pid As String = CType(e.Item.FindControl("lblpid"), Label).Text
        Dim itemid As String = CType(e.Item.FindControl("lblitemid"), Label).Text
        Dim ddu As DropDownList = CType(e.Item.FindControl("ddus"), DropDownList)
        Dim us As String = ddu.SelectedValue
        Dim txtchk As Long
        typ = lblview.Value
        If typ <> "l" Then
            Try
                txtchk = System.Convert.ToDecimal(qtystr)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr324" , "pmactm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        Try
            txtchk = System.Convert.ToDecimal(coststr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr325" , "pmactm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        jpm.Open()
        wonum = lblwo.Value
        If typ = "p" Then
            sql = "update wopmparts set qtyused = '" & qtystr & "', cost = '" & coststr & "', " _
            + "used = '" & us & "' where wotskpartid = '" & pid & "'; update wopmparts set usedtotal = (qtyused * cost) " _
            + "where wotskpartid = '" & pid & "'"
            If us = "Y" Then
                sql += "update workorder set actmatcost = (select sum(usedtotal) from wojpparts where wonum = '" & wonum & "' and " _
                + "jpid = '" & jpid & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; " _
                + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                + "where itemid = '" & itemid & "'"
            End If

        ElseIf typ = "t" Then
            sql = "update wopmtools set qtyused = '" & qtystr & "', rate = '" & coststr & "', " _
            + "used = '" & us & "' where wotsktoolid = '" & pid & "'; update wopmtools set usedtotal = (qtyused * rate) " _
            + "where wotsktoolid = '" & pid & "'"
            If us = "Y" Then
                sql += "update workorder set acttoolcost = (select sum(usedtotal) from wojptools where wonum = '" & wonum & "' and " _
                + "jpid = '" & jpid & "' and used = 'Y') " _
                + "where wonum = '" & wonum & "'; " _
                + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                + "where itemid = '" & itemid & "'"
            End If

        ElseIf typ = "l" Then
            sql = "update wopmlubes set cost = '" & coststr & "', " _
            + "used = '" & us & "' where wotsklubeid = '" & pid & "'; "
            If us = "Y" Then
                sql += "update workorder set actlubecost = (select sum(cost) from wojplubes where wonum = '" & wonum & "' and " _
                + "jpid = '" & jpid & "' and used = 'Y') where wonum = '" & wonum & "'; " _
                + "update invbalances set curbal = curbal - '" & qtystr & "', reserved = reserved - '" & qtystr & "' " _
                + "where itemid = '" & itemid & "'"
            End If

        End If

        jpm.Update(sql)
        dgparts.EditItemIndex = -1
        GetParts()
        jpm.Dispose()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            If CatID = "PASS" Then
                iL = 0
            ElseIf CatID = "FAIL" Then
                iL = 1
            ElseIf CatID = "YES" Then
                iL = 0
            ElseIf CatID = "NO" Then
                iL = 1
            End If
        Else
            iL = -1
        End If
        Return iL
    End Function
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgparts.Columns(0).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "0")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(1).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "1")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(2).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "2")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(3).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "3")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(4).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "4")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(5).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "5")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(6).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "6")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(8).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "8")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(9).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "9")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(10).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "10")
        Catch ex As Exception
        End Try
        Try
            dgparts.Columns(11).HeaderText = dlabs.GetDGPage("pmactm.aspx", "dgparts", "11")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("pmactm.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang567.Text = axlabs.GetASPXPage("pmactm.aspx", "lang567")
        Catch ex As Exception
        End Try
        Try
            lang568.Text = axlabs.GetASPXPage("pmactm.aspx", "lang568")
        Catch ex As Exception
        End Try
        Try
            lang569.Text = axlabs.GetASPXPage("pmactm.aspx", "lang569")
        Catch ex As Exception
        End Try

    End Sub

End Class
