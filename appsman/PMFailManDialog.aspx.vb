

'********************************************************
'*
'********************************************************



Public Class PMFailManDialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pmid, pmtid, pmhid, pmfid, fm, fmid, task, comid, ro, sid As String
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents iffmh As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            pmid = Request.QueryString("pmid").ToString
            pmhid = Request.QueryString("pmhid").ToString
            pmtid = Request.QueryString("pmtid").ToString
            fm = Request.QueryString("fm").ToString
            fmid = Request.QueryString("fmid").ToString
            task = Request.QueryString("task").ToString
            pmfid = Request.QueryString("pmfid").ToString
            comid = Request.QueryString("comid").ToString
            sid = Request.QueryString("sid").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            iffmh.Attributes.Add("src", "PMFailMan.aspx?pmid=" + pmid + "&pmhid=" + pmhid + "&pmtid=" + pmtid + "&pmfid=" + pmfid + "&fm=" + fm + "&fmid=" + fmid + "&task=" + task + "&comid=" + comid + "&ro=" + ro + "&sid=" + sid)
        End If
    End Sub

End Class
