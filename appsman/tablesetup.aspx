<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tablesetup.aspx.vb" Inherits="lucy_r12.tablesetup" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>tablesetup</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table>
        <tr height="20">
            <td width="20">
            </td>
            <td width="20">
            </td>
            <td width="120" class="bluelabel">
                <asp:Label ID="lang744" runat="server">Skill Required</asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="ddskill" runat="server" Width="160px" CssClass="plainlabel">
                </asp:DropDownList>
            </td>
            <td width="50">
            </td>
        </tr>
        <tr height="20">
            <td>
                <input id="cbsupe" type="checkbox" runat="server" name="cbsupe">
            </td>
            <td>
                <img onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
                    src="../images/appbuttons/minibuttons/emailcb.gif">
            </td>
            <td class="bluelabel">
                <asp:Label ID="lang745" runat="server">Supervisor</asp:Label>
            </td>
            <td width="140">
                <asp:TextBox ID="txtsup" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
            </td>
            <td width="20">
                <img onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0">
            </td>
            <td>
            </td>
        </tr>
        <tr height="20">
            <td>
                <input id="cbleade" type="checkbox" runat="server" name="cbleade">
            </td>
            <td>
                <img onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
                    src="../images/appbuttons/minibuttons/emailcb.gif">
            </td>
            <td class="bluelabel">
                <asp:Label ID="lang746" runat="server">Lead Craft</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtlead" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
            </td>
            <td>
                <img onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0">
            </td>
            <td>
            </td>
        </tr>
        <tr height="20">
            <td>
            </td>
            <td>
            </td>
            <td class="bluelabel">
                <asp:Label ID="lang747" runat="server">Work Type</asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="ddwt" runat="server" CssClass="plainlabel">
                    <asp:ListItem Value="CM" Selected="True">Corrective Maintenance</asp:ListItem>
                    <asp:ListItem Value="EM">Emergency Maintenance</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
        <tr height="20">
            <td>
            </td>
            <td>
            </td>
            <td class="bluelabel">
                <asp:Label ID="lang748" runat="server">Target Start</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtstart" runat="server" Width="130px" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcal('tstart');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                    width="19">
            </td>
            <td>
            </td>
        </tr>
        <tr height="20">
            <td colspan="6" class="bluelabel">
                <asp:Label ID="lang749" runat="server">Corrective Action</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:TextBox ID="txtcorr" runat="server" Width="380px" TextMode="MultiLine" Height="50px"></asp:TextBox>
            </td>
        </tr>
        <tr height="30">
            <td align="right" colspan="6">
                <asp:ImageButton ID="btnwo" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                &nbsp;
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
