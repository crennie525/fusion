<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMMeasDialog.aspx.vb"
    Inherits="lucy_r12.PMMeasDialog" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMMeasDialog</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMMeasDialogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onunload="handleret();" onload="resetsess();pageScroll();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifmeas" runat="server" src="" width="100%" height="100%" frameborder="no">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblmcnt" runat="server" name="lblmcnt">
    <input type="hidden" id="lblmup" runat="server" name="lblmup">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
