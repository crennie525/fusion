<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmactmain.aspx.vb" Inherits="lucy_r12.pmactmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmactmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmactmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 2px; left: 2px" cellspacing="0" cellpadding="0"
        width="790">
        <tr>
            <td>
                <table width="770">
                    <tr height="22">
                        <td class="label" width="100">
                            <asp:Label ID="lang570" runat="server">Work Order#</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdwo" width="140" runat="server">
                        </td>
                        <td class="plainlabel" id="tdwod" width="430" runat="server">
                        </td>
                        <td align="right" width="50">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="770">
                    <tr height="22">
                        <td class="thdrhov label" id="tdj" onclick="gettab('jp');" width="170" runat="server">
                            <asp:Label ID="lang571" runat="server">PM Task Details (Labor)</asp:Label>
                        </td>
                        <td class="thdr label" id="tdm" onclick="gettab('jpm');" width="180" runat="server">
                            <asp:Label ID="lang572" runat="server">PM Task Details (Materials)</asp:Label>
                        </td>
                        <td class="thdr label" id="tdl" onclick="gettab('lab');" width="170" runat="server">
                            <asp:Label ID="lang573" runat="server">PM Labor Reporting</asp:Label>
                        </td>
                        <td width="250">
                        </td>
                    </tr>
                    <tr>
                        <td class="details" id="wo" valign="top" colspan="4" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifwo" style="background-color: transparent" src="pmhold.htm" frameborder="no"
                                            width="1020" height="520" allowtransparency runat="server"></iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tdborder" id="jp" valign="top" colspan="4" runat="server">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <iframe id="ifjp" style="background-color: transparent" src="pmhold.htm" frameborder="no"
                                            width="1020" height="520" allowtransparency runat="server"></iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblwo" type="hidden" name="lblwo" runat="server">
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lblupsav" type="hidden" name="lblupsav" runat="server">
    <input id="lblstat" type="hidden" name="lblstat" runat="server">
    <input id="lblpmhid" type="hidden" runat="server"><input id="lblro" type="hidden"
        runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
