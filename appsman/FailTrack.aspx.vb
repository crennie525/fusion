

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class FailTrack
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
    Dim pmid, reptype, pmtskid, pmtid, failid, ro As String
    Protected WithEvents lblcharturl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Dim charturl As String = System.Configuration.ConfigurationManager.AppSettings("chartURL")
            lblcharturl.Value = charturl
            pmid = Request.QueryString("pmid").ToString '"197" '
            Try
                reptype = Request.QueryString("typ").ToString
            Catch ex As Exception
                'use for testing
                reptype = "meas"
            End Try
            pms.Open()
            If reptype = "pm" Then
                PopReport(pmid)
            ElseIf reptype = "task" Then
                pmtskid = Request.QueryString("pmtskid").ToString '"3378" '
                PopTaskReport(pmid, pmtskid)
            ElseIf reptype = "fail" Then
                pmtid = Request.QueryString("pmtid").ToString '"1042" '
                failid = Request.QueryString("failid").ToString '"5" '
                PopFailReport(pmid, pmtid, failid)
            ElseIf reptype = "meas" Then
                PopMeas(pmid)
            End If
            pms.Dispose()
        End If
    End Sub
    Private Sub PopThresh()

    End Sub
    Private Sub PopMeas(ByVal pmid As String)
        Dim flag As Integer = 0
        Dim fmcnt As Integer
        Dim func, funcchk, task, tasknum, pm, pmtskid, tmdid As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""40""></td><td width=""640""></td></tr>")
        sql = "usp_getmeas '" & pmid & "'"

        dr = pms.GetRdrData(sql)
        Dim headhold As String = "0"
        Dim taskhold As String = "0"
        Dim meashold As String = "0"
        While dr.Read
            If headhold = "0" Then
                headhold = "1"
                pm = dr.Item("pm").ToString

                sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""2"" align=""center"">" & tmod.getlbl("cdlbl20" , "FailTrack.aspx.vb") & "</td></tr>")
                sb.Append("<tr height=""30"" valign=""top""><td class=""plainlabel"" colspan=""2"" align=""center"">" & Now & "</td></tr>")
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""2"">" & pm & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">Equipment#: " & dr.Item("eqnum").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">Last Date: " & dr.Item("lastdate").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">Next Date: " & dr.Item("nextdate").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2"">Total # of Runs: " & dr.Item("pmcnt") & "</td></tr>")
                sb.Append("<tr><td class=""bigbold"" colspan=""2""><hr></td></tr>")
            End If
            tmdid = dr.Item("tmdid").ToString
            func = dr.Item("func").ToString
            If funcchk <> func Then
                If flag = 0 Then
                    flag = 1
                End If

                funcchk = func
                sb.Append("<tr height=""20""><td class=""label"" colspan=""2""><u>Function: " & func & "</u></td></tr>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl21" , "FailTrack.aspx.vb") & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl22" , "FailTrack.aspx.vb") & "</td></tr>")

            End If
            task = dr.Item("task").ToString
            tasknum = dr.Item("tasknum").ToString
            pmtskid = dr.Item("pmtskid").ToString
            pmtid = dr.Item("pmtid").ToString
            
            If taskhold <> task Then
                taskhold = task
                sb.Append("<tr><td class=""plainlabel"" valign=""top"">[ " & tasknum & " ]</td>")
                If Len(dr.Item("task").ToString) > 0 Then
                    sb.Append("<td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & task & "</td>")
                Else
                    sb.Append("<td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & tmod.getlbl("cdlbl23" , "FailTrack.aspx.vb") & "</td>")
                End If
                sb.Append("<tr><td colspan=""2""><table><tr>")
                sb.Append("<td class=""label"" width=""100""><u>Measure Type</u></td>")
                sb.Append("<td class=""label"" width=""150""><u>Measure</u></td>")
                sb.Append("<td class=""label"" width=""50""><u>Graph</u></td>")
                sb.Append("<td class=""label"" width=""50""><u>Email</u></td>")
                sb.Append("<td class=""label"" width=""70""><u>Hi</u></td>")
                sb.Append("<td class=""label"" width=""70""><u>Lo</u></td>")
                sb.Append("<td class=""label"" width=""79""><u>Spec</u></td>")
                sb.Append("<td class=""label"" width=""100""><u>Measure Cnt</u></td>")
                sb.Append("</tr></table></td></tr>")
            End If

            sb.Append("<tr><td colspan=""2""><table><tr>")
            sb.Append("<td class=""plainlabel"" width=""100"">" & dr.Item("type").ToString & "</td>")
            Dim rec As String = dr.Item("record").ToString
            Dim mscnt As Integer
            Try
                mscnt = System.Convert.ToInt32(dr.Item("meascnt").ToString)
                If mscnt > 1 And rec = "Yes" Then
                    sb.Append("<td class=""plainlabel"" width=""150""><a href=""#"" onclick=""print('" & dr.Item("pmtskid").ToString & "','" & tmdid & "')"">" & dr.Item("measure").ToString & "</a></td>")
                    If mscnt > 3 Then
                        sb.Append("<td width=""50""><img src=""../images/appbuttons/minibuttons/lchart.gif"" onclick=""getchart('" & dr.Item("pmtskid").ToString & "','" & tmdid & "','" & dr.Item("hi").ToString & "','" & dr.Item("spec").ToString & "')""> </td>")
                    Else
                        sb.Append("<td width=""50"">&nbsp;</td>")
                    End If
                Else
                    sb.Append("<td class=""plainlabel"" width=""140"">" & dr.Item("measure").ToString & "</td><td width=""50"">&nbsp;</td>")
                End If
            Catch ex As Exception
                If dr.Item("meascnt").ToString > 1 And dr.Item("record").ToString = "Yes" Then
                    sb.Append("<td class=""plainlabel"" width=""150""><a href=""#"" onclick=""print('" & dr.Item("pmtskid").ToString & "','" & tmdid & "')"">" & dr.Item("measure").ToString & "</a></td>")
                    If dr.Item("meascnt").ToString > 3 Then
                        sb.Append("<td width=""50""><img src=""../images/appbuttons/minibuttons/lchart.gif"" onclick=""getchart('" & dr.Item("pmtskid").ToString & "','" & tmdid & "','" & dr.Item("hi").ToString & "')""> </td>")
                    Else
                        sb.Append("<td width=""50"">&nbsp;</td>")
                    End If
                Else
                    sb.Append("<td class=""plainlabel"" width=""140"">" & dr.Item("measure").ToString & "</td><td width=""50"">&nbsp;</td>")
                End If
            End Try
            
            sb.Append("<td width=""50""><img src=""../images/appbuttons/minibuttons/emailcb.gif"" onclick=""getemail('" & dr.Item("pmtskid").ToString & "','" & tmdid & "','" & dr.Item("hi").ToString & "')""> </td>")
            sb.Append("<td class=""plainlabel"" width=""70"">" & dr.Item("hi").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" width=""70"">" & dr.Item("lo").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" width=""70"">" & dr.Item("spec").ToString & "</td>")
            sb.Append("<td class=""plainlabel"" width=""100"">" & dr.Item("meascnt").ToString & "</td>")
            sb.Append("</tr></table></td></tr>")

            sb.Append("<tr><td>&nbsp;</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString
    End Sub
    Private Sub PopReport(ByVal pmid As String)
        Dim flag As Integer = 0
        Dim fmcnt As Integer
        Dim func, funcchk, task, tasknum, pm, pmtskid As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""40""></td><td width=""420""></td><td width=""10""></td><td width=""120""></td><td width=""90""></td></tr>")
        sql = "usp_getfailtrack '" & pmid & "'"

        dr = pms.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            If headhold = "0" Then
                headhold = "1"
                pm = dr.Item("pm").ToString
                sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & tmod.getlbl("cdlbl24" , "FailTrack.aspx.vb") & "</td></tr>")
                sb.Append("<tr height=""30"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"">" & pm & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Equipment#: " & dr.Item("eqnum").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Last Date: " & dr.Item("lastdate").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Next Date: " & dr.Item("nextdate").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Total # of Runs: " & dr.Item("pmcnt") & "</td></tr>")
                'sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Total Down Time: " & dr.Item("dtime") & " & Minutes</td></tr>")
                'sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Scheduled Start Date: " & dr.Item("nextdate") & "</td></tr>")
                sb.Append("<tr><td class=""bigbold"" colspan=""5""><hr></td></tr>")
            End If
            fmcnt = dr.Item("fmcnt").ToString
            If fmcnt = 0 Then
                fmcnt = 1
            End If
            func = dr.Item("func").ToString
            If funcchk <> func Then
                If flag = 0 Then
                    flag = 1
                End If

                funcchk = func
                sb.Append("<tr height=""20""><td class=""label"" colspan=""4""><u>Function: " & func & "</u></td></tr>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl25" , "FailTrack.aspx.vb") & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl26" , "FailTrack.aspx.vb") & "</td>")
                sb.Append("<td class=""label"">&nbsp;</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl27" , "FailTrack.aspx.vb") & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl28" , "FailTrack.aspx.vb") & "</td></tr>")
            End If
            task = dr.Item("task").ToString
            tasknum = dr.Item("tasknum").ToString
            pmtskid = dr.Item("pmtskid").ToString
            pmtid = dr.Item("pmtid").ToString
            If dr.Item("pmcnt") <> 0 Then
                sb.Append("<tr><td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """><a href=""#"" onclick=""gettask('" & pmid & "','" & pmtskid & "');"">[ " & tasknum & " ]</a></td>")
            Else
                sb.Append("<tr><td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>[ " & tasknum & " ]</td>")
            End If

            If Len(dr.Item("task").ToString) > 0 Then
                sb.Append("<td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & task & "</td>")
            Else
                sb.Append("<td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & tmod.getlbl("cdlbl29" , "FailTrack.aspx.vb") & "</td>")
            End If
            failid = dr.Item("fm1id").ToString
            If dr.Item("fm1dd").ToString <> "0" Then
                sb.Append("<td rowspan=""" & fmcnt & """></td>")
                sb.Append("<td class=""plainlabel""><a href=""#"" ")
                sb.Append(" onclick=""getfail('" & pmid & "','" & pmtid & "','" & failid & "');"">" & dr.Item("fm1s").ToString & "</a></td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fm1dd").ToString & "</td></tr>")
            Else
                sb.Append("<td rowspan=""" & fmcnt & """></td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fm1s").ToString & "</a></td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fm1dd").ToString & "</td></tr>")
            End If


            If fmcnt > 1 Then
                failid = dr.Item("fm2id").ToString
                If dr.Item("fm2dd").ToString <> "0" Then
                    sb.Append("<tr><td class=""plainlabel""><a href=""#"" ")
                    sb.Append(" onclick=""getfail('" & pmid & "','" & pmtid & "','" & failid & "');"">" & dr.Item("fm2s").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm2dd").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"">" & dr.Item("fm2s").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm2dd").ToString & "</td></tr>")
                End If
            End If

            If fmcnt > 2 Then
                failid = dr.Item("fm3id").ToString
                If dr.Item("fm3dd").ToString <> "0" Then
                    sb.Append("<tr><td class=""plainlabel""><a href=""#"" ")
                    sb.Append(" onclick=""getfail('" & pmid & "','" & pmtid & "','" & failid & "');"">" & dr.Item("fm3s").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm3dd").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"">" & dr.Item("fm3s").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm3dd").ToString & "</td></tr>")
                End If
            End If

            If fmcnt > 3 Then
                failid = dr.Item("fm4id").ToString
                If dr.Item("fm4dd").ToString <> "0" Then
                    sb.Append("<tr><td class=""plainlabel""><a href=""#"" ")
                    sb.Append(" onclick=""getfail('" & pmid & "','" & pmtid & "','" & failid & "');"">" & dr.Item("fm4s").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm4dd").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"">" & dr.Item("fm4s").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm4dd").ToString & "</td></tr>")
                End If
            End If

            If fmcnt > 4 Then
                failid = dr.Item("fm5id").ToString
                If dr.Item("fm5dd").ToString <> "0" Then
                    sb.Append("<tr><td class=""plainlabel""><a href=""#"" ")
                    sb.Append(" onclick=""getfail('" & pmid & "','" & pmtid & "','" & failid & "');"">" & dr.Item("fm5s").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm5dd").ToString & "</td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabel"">" & dr.Item("fm5s").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm5dd").ToString & "</td></tr>")
                End If

            End If

            sb.Append("<tr><td>&nbsp;</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString

    End Sub
    Private Sub PopFailReport(ByVal pmid As String, ByVal pmtid As String, ByVal failid As String)
        Dim flag As Integer = 0
        Dim fmcnt As Integer
        Dim func, funcchk, task, tasknum, pm As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""40""></td><td width=""420""></td><td width=""10""></td><td width=""120""></td><td width=""90""></td></tr>")
        sql = "select count(*) from pmfailhist where pmid = '" & pmid & "' and failid = '" & failid & "' and pmtid = '" & pmtid & "'"
        Dim cnt As Integer = pms.Scalar(sql)
        If cnt = 0 Then
            sb.Append("<tr height=""100"" valign=""center""><td class=""bigbold1"" colspan=""5"" align=""center"">" & tmod.getlbl("cdlbl30" , "FailTrack.aspx.vb") & "<br>for this Failure Mode</td></tr>")
            sb.Append("</table>")
            tdwi.InnerHtml = sb.ToString
        Else

            sql = "usp_getfailhist '" & pmid & "','" & failid & "','" & pmtid & "'"

            dr = pms.GetRdrData(sql)
            Dim headhold As String = "0"
            While dr.Read
                If headhold = "0" Then
                    headhold = "1"
                    pm = dr.Item("pm").ToString
                    sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & tmod.getlbl("cdlbl31" , "FailTrack.aspx.vb") & "</td></tr>")
                    sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold"" colspan=""5"" align=""center"">Failure Mode:&nbsp;&nbsp;" & dr.Item("failure").ToString & "</td></tr>")
                    sb.Append("<tr height=""30"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
                    sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"">" & pm & "</td></tr>")
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Equipment#: " & dr.Item("eqnum").ToString & "</td></tr>")
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Last Date: " & dr.Item("lastdate").ToString & "</td></tr>")
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Next Date: " & dr.Item("nextdate").ToString & "</td></tr>")
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Total # of Runs: " & dr.Item("pmcnt") & "</td></tr>")
                    'sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Total Down Time: " & dr.Item("dtime") & " & Minutes</td></tr>")
                    'sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Scheduled Start Date: " & dr.Item("nextdate") & "</td></tr>")
                    sb.Append("<tr><td class=""bigbold"" colspan=""5""><hr></td></tr>")
                End If
                fmcnt = 1
                func = dr.Item("func").ToString
                If funcchk <> func Then
                    If flag = 0 Then
                        flag = 1
                    End If

                    funcchk = func
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""4""><u>Function: " & func & "</u></td></tr>")
                    sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl32" , "FailTrack.aspx.vb") & "</td>")
                    sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl33" , "FailTrack.aspx.vb") & "</td>")
                    sb.Append("<td class=""label"">&nbsp;</td>")
                    sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl34" , "FailTrack.aspx.vb") & "</td>")
                    sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl35" , "FailTrack.aspx.vb") & "</td></tr>")

                    task = dr.Item("task").ToString
                    tasknum = dr.Item("tasknum").ToString
                    'pmtskid = dr.Item("pmtskid").ToString

                    sb.Append("<tr><td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & tasknum & "</td>")
                    If Len(dr.Item("task").ToString) > 0 Then
                        sb.Append("<td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & task & "</td>")
                    Else
                        sb.Append("<td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & tmod.getlbl("cdlbl36" , "FailTrack.aspx.vb") & "</td>")
                    End If

                    sb.Append("<td rowspan=""" & fmcnt & """></td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("failure").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("fm1dd").ToString & "</td></tr>")

                    sb.Append("<tr><td>&nbsp;</td></tr>")
                End If
                sb.Append("<tr><td colspan=""5""><table><tr>")
                sb.Append("<td colspan=""3"" class=""plainlabel""><b>Failure Date:</b>&nbsp;&nbsp;" & dr.Item("faildate").ToString & "</td></tr>")
                sb.Append("<tr><td class=""label"" width=""335""><u>Problem Encountered</u></td>")
                sb.Append("<td width=""10"">&nbsp;</td>")
                sb.Append("<td class=""label"" width=""335""><u>Corrective Action</u></td></tr>")

                sb.Append("<tr><td class=""plainlabel"">" & dr.Item("problem").ToString & "</td>")
                sb.Append("<td>&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("corraction").ToString & "</td></tr>")

                sb.Append("<tr><td colspan=""3"" class=""plainlabel""><b>Work Order#</b>&nbsp;&nbsp;" & dr.Item("wonum").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""3"" class=""plainlabel""><b>Description:</b>&nbsp;&nbsp;" & dr.Item("description").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""3"" class=""plainlabel""><b>Work Order Status:</b>&nbsp;&nbsp;" & dr.Item("status").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""3"" class=""plainlabel""><b>Status Date:</b>&nbsp;&nbsp;" & dr.Item("statusdate").ToString & "</td></tr>")

                sb.Append("</table></td></tr>")
                sb.Append("<tr><td>&nbsp;</td></tr>")

            End While
            dr.Close()
            sb.Append("</table>")
            tdwi.InnerHtml = sb.ToString

        End If

    End Sub
    Private Sub PopTaskReport(ByVal pmid As String, ByVal failid As String)
        Dim flag As Integer = 0
        Dim fmcnt As Integer
        Dim func, funcchk, task, tasknum, pm As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""140""></td><td width=""320""></td><td width=""10""></td><td width=""120""></td><td width=""90""></td></tr>")
        sql = "usp_getfailtasks '" & pmid & "','" & pmtskid & "'"

        dr = pms.GetRdrData(sql)
        Dim headhold As String = "0"
        While dr.Read
            If headhold = "0" Then
                headhold = "1"
                pm = dr.Item("pm").ToString
                sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & tmod.getlbl("cdlbl37" , "FailTrack.aspx.vb") & "</td></tr>")
                sb.Append("<tr height=""30"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"">" & pm & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Equipment#: " & dr.Item("eqnum").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Last Date: " & dr.Item("lastdate").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Next Date: " & dr.Item("nextdate").ToString & "</td></tr>")
                sb.Append("<tr height=""20""><td class=""label"" colspan=""5"">Total # of Runs: " & dr.Item("pmcnt") & "</td></tr>")
                'sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Total Down Time: " & dr.Item("dtime") & " & Minutes</td></tr>")
                'sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">Scheduled Start Date: " & dr.Item("nextdate") & "</td></tr>")
                sb.Append("<tr><td class=""bigbold"" colspan=""5""><hr></td></tr>")
            End If
            fmcnt = dr.Item("fmcnt").ToString
            If fmcnt = 0 Then
                fmcnt = 1
            End If
            func = dr.Item("func").ToString
            If funcchk <> func Then
                If flag = 0 Then
                    flag = 1
                End If

                funcchk = func
                sb.Append("<tr height=""20""><td class=""label"" colspan=""4""><u>Function: " & func & "</u></td></tr>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl38" , "FailTrack.aspx.vb") & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl39" , "FailTrack.aspx.vb") & "</td>")
                sb.Append("<td class=""label"">&nbsp;</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl40" , "FailTrack.aspx.vb") & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl41" , "FailTrack.aspx.vb") & "</td></tr>")
            End If
            task = dr.Item("task").ToString
            tasknum = dr.Item("tasknum").ToString
            'pmtskid = dr.Item("pmtskid").ToString

            sb.Append("<tr><td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & dr.Item("compdate") & "</td>")
            If Len(dr.Item("task").ToString) > 0 Then
                sb.Append("<td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & task & "</td>")
            Else
                sb.Append("<td class=""plainlabel"" valign=""top"" rowspan=""" & fmcnt & """>" & tmod.getlbl("cdlbl42" , "FailTrack.aspx.vb") & "</td>")
            End If

            sb.Append("<td rowspan=""" & fmcnt & """></td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("fm1s").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("fm1dd").ToString & "</td></tr>")

            If fmcnt > 1 Then
                sb.Append("<tr><td class=""plainlabel"">" & dr.Item("fm2s").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fm2dd").ToString & "</td></tr>")
            End If

            If fmcnt > 2 Then
                sb.Append("<tr><td class=""plainlabel"">" & dr.Item("fm3s").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fm3dd").ToString & "</td></tr>")
            End If

            If fmcnt > 3 Then
                sb.Append("<tr><td class=""plainlabel"">" & dr.Item("fm4s").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fm4dd").ToString & "</td></tr>")
            End If

            If fmcnt > 4 Then
                sb.Append("<tr><td class=""plainlabel"">" & dr.Item("fm5s").ToString & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("fm5dd").ToString & "</td></tr>")
            End If

            sb.Append("<tr><td>&nbsp;</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString

    End Sub
End Class
