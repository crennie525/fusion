

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class PFAdj
    Inherits System.Web.UI.Page
	Protected WithEvents lang556 As System.Web.UI.WebControls.Label

	Protected WithEvents lang555 As System.Web.UI.WebControls.Label

	Protected WithEvents lang554 As System.Web.UI.WebControls.Label

	Protected WithEvents lang553 As System.Web.UI.WebControls.Label

	Protected WithEvents lang552 As System.Web.UI.WebControls.Label

	Protected WithEvents lang551 As System.Web.UI.WebControls.Label

	Protected WithEvents lang550 As System.Web.UI.WebControls.Label

	Protected WithEvents lang549 As System.Web.UI.WebControls.Label

	Protected WithEvents lang548 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim thres As New Utilities
    Dim sql, sid, ro As String
    Protected WithEvents tdpass As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rbp As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbf As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbpmp As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbpmf As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblpass As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Dim pa As Integer = 0
    Protected WithEvents txtdays As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim fa As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtpass As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtfail As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdf As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                ImageButton1.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                ImageButton1.Enabled = False
            End If
            sid = HttpContext.Current.Session("dfltps").ToString()
            lblsid.Value = sid
            thres.Open()
            GetVals()
            thres.Dispose()
        End If
        'ImageButton1.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ImageButton1.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")

    End Sub
    Private Sub GetVals()
        Dim cid As String = "0" 
        sid = lblsid.Value
        sql = "select passthres, failthres from pmsysvars where compid = '" & cid & "'"
        dr = thres.GetRdrData(sql)
        While dr.Read
            tdpass.InnerHtml = dr.Item("passthres").ToString
            lblpass.Value = (dr.Item("passthres").ToString)
            tdfail.InnerHtml = dr.Item("failthres").ToString
            lblfail.Value = dr.Item("failthres").ToString
        End While
        dr.Close()
        sql = "select evalue from evars where etype = 'THRESH' and siteid = '" & sid & "'"
        dr = thres.GetRdrData(sql)
        While dr.Read
            txtdays.Text = dr.Item("evalue").ToString
        End While
        dr.Close()
       
    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim cid As String = "0" 
        Dim p, f, po, fo, pmp, pmf As String
        p = txtpass.Text
        If p = "" Then
            p = lblpass.Value
            pa = 1
        End If
        f = txtfail.Text
        If f = "" Then
            f = lblfail.Value
            fa = 1
        End If
        po = rbp.SelectedValue
        fo = rbf.SelectedValue
        pmp = rbpmp.SelectedValue
        pmf = rbpmf.SelectedValue
        Try
            p = Convert.ToInt32(p)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr318" , "PFAdj.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Try
            f = Convert.ToInt32(f)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr319" , "PFAdj.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        thres.Open()
        sql = "usp_thresadmin '" & cid & "','" & p & "','" & f & "','" & po & "','" & fo & "','" & pmp & "','" & pmf & "', '" & pa & "','" & fa & "'"
        thres.Update(sql)
        txtpass.Text = ""
        txtfail.Text = ""
        sid = lblsid.Value
        Dim edays As String = txtdays.Text
        sql = "update evars set evalue = '" & edays & "' where etype = 'THRESH' and siteid = '" & sid & "'"
        thres.Update(sql)
        GetVals()
        thres.Dispose()

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang548.Text = axlabs.GetASPXPage("PFAdj.aspx","lang548")
		Catch ex As Exception
		End Try
		Try
			lang549.Text = axlabs.GetASPXPage("PFAdj.aspx","lang549")
		Catch ex As Exception
		End Try
		Try
			lang550.Text = axlabs.GetASPXPage("PFAdj.aspx","lang550")
		Catch ex As Exception
		End Try
		Try
			lang551.Text = axlabs.GetASPXPage("PFAdj.aspx","lang551")
		Catch ex As Exception
		End Try
		Try
			lang552.Text = axlabs.GetASPXPage("PFAdj.aspx","lang552")
		Catch ex As Exception
		End Try
		Try
			lang553.Text = axlabs.GetASPXPage("PFAdj.aspx","lang553")
		Catch ex As Exception
		End Try
		Try
			lang554.Text = axlabs.GetASPXPage("PFAdj.aspx","lang554")
		Catch ex As Exception
		End Try
		Try
			lang555.Text = axlabs.GetASPXPage("PFAdj.aspx","lang555")
		Catch ex As Exception
		End Try
		Try
			lang556.Text = axlabs.GetASPXPage("PFAdj.aspx","lang556")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			ImageButton1.Attributes.Add("src" , "../images2/eng/bgbuttons/save.gif")
			ElseIf lang = "fre" Then
			ImageButton1.Attributes.Add("src" , "../images2/fre/bgbuttons/save.gif")
			ElseIf lang = "ger" Then
			ImageButton1.Attributes.Add("src" , "../images2/ger/bgbuttons/save.gif")
			ElseIf lang = "ita" Then
			ImageButton1.Attributes.Add("src" , "../images2/ita/bgbuttons/save.gif")
			ElseIf lang = "spa" Then
			ImageButton1.Attributes.Add("src" , "../images2/spa/bgbuttons/save.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class
