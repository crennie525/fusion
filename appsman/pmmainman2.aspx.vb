Imports System.Data.SqlClient
Imports System.Text
Public Class pmmainman2
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim mm As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim rowcnt As Integer
    Dim usid, nme, typ, eqid, fuid, coid, sid, cid, lid, did, clid, ro, rostr, Login, tab, issuper As String
    Dim islabor, wonum, isplanner, waid, wpaid, fdate, tdate, wtret, statret As String
    Protected WithEvents rptrtasks As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblprint As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblprintwolist As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents divywr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhide As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            'Try
            'tab = Request.QueryString("tab").ToString
            'lbltab.Value = tab
            'tdwrs.Width = 674
            'tbwrs.Width = 700
            'divywr.Attributes.Add("class", "wolistbig1")
            'Catch ex As Exception
            'divywr.Attributes.Add("class", "wolistbig")
            'End Try
            'lblhide.Value = "no"
            'trhide1.Attributes.Add("class", "details")
            'trhide2.Attributes.Add("class", "details")

            Try
                ro = "1" 'HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro <> "1" Then
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = mm.CheckROS(rostr, "wo")
                    lblro.Value = ro
                End If
            End If
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            mm.Open()
            BuildMyWR(PageNumber)
            mm.Dispose()
        Else
            If Request.Form("lblret") = "filt" Then
                lblret.Value = ""
                mm.Open()
                BuildMyWR(PageNumber)
                mm.Dispose()
            End If
        End If
    End Sub
    Private Sub BuildMyWR(ByVal PageNumber As Integer)
        Dim sb As New StringBuilder
        Dim field, srch As String
        Dim srchi As Integer
        usid = "124" 'HttpContext.Current.Session("userid").ToString()
        Dim uid1 As String = usid
        uid1 = Replace(uid1, "'", "''")
        nme = "Lab Sched1" 'HttpContext.Current.Session("username").ToString()
        Dim nme1 As String = nme
        nme1 = Replace(nme, "'", "''")
        Dim sid1 As String = lblsid.Value
        typ = lbltyp.Value
        tab = lbltab.Value
        islabor = "0" 'HttpContext.Current.Session("islabor").ToString()
        isplanner = "1" 'HttpContext.Current.Session("islabor").ToString()
        If islabor = "1" Then
            Filter = " a.userid = ''" & usid & "''"
            FilterCNT = " a.userid = '" & usid & "'"
        ElseIf isplanner = "1" Then


        End If
        'waid + "~" + wpaid + "~" + fdate + "~" + tdate + "~" + wt + "~" + stat;
        Dim filtret As String '= lblfiltret.Value
        If filtret <> "" Then
            Dim filtarr() As String = filtret.Split("~")
            waid = filtarr(0).ToString
            wpaid = filtarr(1).ToString
            fdate = filtarr(2).ToString
            tdate = filtarr(3).ToString
            wtret = filtarr(4).ToString
            statret = filtarr(5).ToString
            If waid <> "" Then
                If Filter <> "" Then
                    Filter += " and w.waid = ''" & waid & "''"
                    FilterCNT += " and w.waid = '" & waid & "'"
                Else
                    Filter = " w.waid = ''" & waid & "''"
                    FilterCNT = " w.waid = '" & waid & "'"
                End If

            End If
            If wpaid <> "" Then
                If Filter <> "" Then
                    Filter += " and w.wpaid = ''" & wpaid & "''"
                    FilterCNT += " and w.wpaid = '" & wpaid & "'"
                Else
                    Filter = " w.wpaid = ''" & wpaid & "''"
                    FilterCNT = " w.wpaid = '" & wpaid & "'"
                End If

            End If

            If wtret <> "" Then
                If Filter <> "" Then
                    Filter += " and w.worktype = ''" & wtret & "''"
                    FilterCNT += " and w.worktype = '" & wtret & "'"
                Else
                    Filter = " w.worktype = ''" & wtret & "''"
                    FilterCNT = " w.worktype = '" & wtret & "'"
                End If

            End If

        End If
        If Filter = "" Then
            Filter = " w.status not in (''COMP'',''CLOSE'') and w.worktype <> ''PM''"
            FilterCNT = " w.status not in ('COMP','CLOSE') and w.worktype <> 'PM'"
        End If

        Dim intPgNav, intPgCnt As Integer
        sql = "SELECT Count(*) FROM workorder w left join woassign a on a.wonum = w.wonum where " & FilterCNT

        intPgCnt = mm.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")

            mm.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        PageSize = "50"
        intPgNav = mm.PageCount1(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "w.wonum asc"
        'SELECT w.wonum, w.status, w.worktype, w.description, w.schedstart, w.schedfinish
        sb.Append("<table cellspacing=""2"" width=""440"">" & vbCrLf)
        sb.Append("<tr>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""80px"" height=""20px"">" & tmod.getlbl("cdlbl209", "wolist.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""50px"">Status</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""70px"">Work Type</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""140px"">Equipment</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""100px"">Scheduled Start</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""30px"">" & tmod.getlbl("cdlbl214", "wolist.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("</tr>" & vbCrLf)
        Dim wt, wo, stat, bg, isdown, desc, ss, sc, eq As String
        Dim rowflag As Integer = 0
        If islabor = "1" Then
            sql = "usp_getwolist_labor '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        ElseIf isplanner = "1" Then
            sql = "usp_getwolist_plnr '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        End If
        sql = "usp_getAllPMPg2 '" & sid & "', '" & PageNumber & "', '" & PageSize & "', '" & Filter & "'"
        Dim jp, pm As String
        dr = mm.GetRdrData(sql)
        While dr.Read
            If rowflag = 0 Then
                bg = "ptransrow"
                rowflag = 1
            Else
                bg = "ptransrowblue"
                rowflag = "0"
            End If
            isdown = "0" 'dr.Item("isdown").ToString
            Dim cr, crl As String
            If isdown = "1" Then
                cr = "plainlabelred"
                crl = "A1R"
            Else
                cr = "plainlabel"
                crl = "A1U"
            End If
            wt = dr.Item("worktype").ToString
            wo = dr.Item("wonum").ToString
            eq = dr.Item("eqnum").ToString
            stat = dr.Item("status").ToString
            desc = dr.Item("description").ToString
            ss = dr.Item("schedstart").ToString
            sc = dr.Item("schedfinish").ToString
            sb.Append("<tr>" & vbCrLf)
            sb.Append("<td class=""" & bg & """><a class=""" & crl & """ href=""#"" onclick=""gotoreq('" & wo & "')"">" & wo & "</a></td>" & vbCrLf)
            'sb.Append("<td class=""" & bg & " " & cr & """>" & wo & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & stat & "</td>" & vbCrLf)
            If wt <> "0" Then
                sb.Append("<td class=""" & bg & " " & cr & """>" & wt & "</td>" & vbCrLf)
            Else
                sb.Append("<td class=""" & bg & " " & cr & """></td>" & vbCrLf)
            End If
            sb.Append("<td class=""" & bg & " " & cr & """>" & eq & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """>" & ss & "</td>" & vbCrLf)
            sb.Append("<td class=""" & bg & " " & cr & """ align=""center""><a href=""#"" onclick=""printwo('" & wo & "','" & typ & "','" & jp & "','" & pm & "')"" onmouseover=""return overlib('" & tmod.getov("cov174", "wolist.aspx.vb") & "', ABOVE, LEFT)"" onmouseout=""return nd()""><img src=""../images/appbuttons/minibuttons/printx.gif"" border=""0""></a></td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
        End While
        dr.Close()
        sb.Append("</table>")
        divywr.InnerHtml = sb.ToString
    End Sub
End Class
