<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMDivMan.aspx.vb" Inherits="lucy_r12.PMDivMan" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMDivMan</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <style type="text/css">
        .FreezePaneOn6
        {
            position: absolute;
            top: 52px;
            left: 0px;
            visibility: visible;
            display: block;
            width: 760px;
            height: 480px;
            background-color: #eeeded;
            z-index: 999;
            filter: alpha(opacity=35);
            -moz-opacity: 0.35;
            padding-top: 3%;
        }
        .FreezePaneOn7
        {
            position: absolute;
            top: 0px;
            left: 0px;
            visibility: visible;
            display: block;
            width: 760px;
            height: 480px;
            background-color: #eeeded;
            z-index: 999;
            filter: alpha(opacity=35);
            -moz-opacity: 0.35;
            padding-top: 3%;
        }
        .view2
        {
            visibility:visible;
        }
        .viewcell 
{ 
DISPLAY: table-cell;
VISIBILITY: visible; 
}
}
        .detailscell 
{ 
DISPLAY: table-cell;
VISIBILITY: hidden;
}
    </style>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMDivManaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function printwo() {
            var pm = document.getElementById("lblpmid").value;
            var wo = document.getElementById("lblwo").value;
            if (wo == "") {
                alert("No Work Order Assigned for this PM")
            }
            else {
                window.parent.setref();
                getdocs();
                getroute();
                window.open("../appswo/woprint.aspx?typ=PM&pm=" + pm + "&wo=" + wo, "repWin", popwin);
            }
        }
        function getroute() {
            var popwin = "directories=0,height=520,width=820,location=1,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            var rtid = document.getElementById("lblrtid").value;
            //alert(rtid)
            var typ = "RBASM3";
            var pm = document.getElementById("lblpmid").value;
            if (rtid != "") {
                window.open("../reports/pmrhtml7_new.aspx?rid=" + rtid + "&typ=" + typ + "&pmid=" + pm, "repWin2", popwin);
            }
        }
        function getdocs() {
            var cbs = document.getElementById("lbldocs").value;
            var cbsarr = cbs.split(";")
            for (var i = 0; i < cbsarr.length; i++) {
                var cbstr = cbsarr[i];
                //alert(cbstr)
                if (cbstr != "") {
                    var cbstrarr = cbstr.split("~")
                    var fnstr = cbstrarr[1]
                    var docstr = cbstrarr[0]
                    OpenFile(fnstr, docstr);
                }
            }
        }
        function getattachrt() {
            var sid = document.getElementById("lblsid").value;
            var pm = document.getElementById("lblpmid").value;
            var typ = "RBASWO";
            if (sid != "") {
                window.open("../appsrt/pmrrtelkup.aspx?sid=" + sid + "&typ=" + typ + "&pmid=" + pm + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
            }
        }

        function checkdeltask() {
            var wo = document.getElementById("txtwonum").value;
            var deltask = confirm("Are you sure you want to Delete this PM Record?");
            if (deltask == true) {
                if (wo == "") {
                    document.getElementById("lbldelwo").value = "0";
                    document.getElementById("lblsubmit").value = "delpm";
                    document.getElementById("form1").submit();
                }
                else {
                    var delwo = confirm("Would you like to CANCEL the Current Work Order?");
                    if (delwo == true) {
                    //alert("hello")
                        document.getElementById("lbldelwo").value = "1";
                        document.getElementById("lblsubmit").value = "delpm";
                        //alert(document.getElementById("lblsubmit").value)
                        document.getElementById("form1").submit();
                    }
                    else {
                        document.getElementById("lbldelwo").value = "0";
                        document.getElementById("lblsubmit").value = "delpm";
                        //alert(document.getElementById("lblsubmit").value)
                        document.getElementById("form1").submit();
                    }
                    
                }
                
            }
            else if (deltask == false) {
                return false;
                alert("Action Cancelled")
            }
        }
        function checkit() {
            var gret = document.getElementById("lblsubmit").value;
            if (gret == "go") {
                gridret();
            }
            var nodown;
            var pmid = document.getElementById("lblpmid").value;
            var pmhid = document.getElementById("lblpmhid").value;
            var eqid = document.getElementById("lbleqid").value;
            var wo = document.getElementById("txtwonum").value;
            if (pmid != "") {
                window.parent.updatepmid(pmid, pmhid, eqid, wo);
            }

            var malert = document.getElementById("lblmalert").value;
            if (malert != "") {
                compalert(malert);
            }

            var alertstr = document.getElementById("lblalert").value;
            document.getElementById("lblalert").value = "";
            if (alertstr == "1" || alertstr == "2") {
                adjalert(alert);
            }
            else if (alertstr == "3") {
                document.getElementById("lblsubmit").value = "comp";
                FreezeScreenComp('Your Data Is Being Processed...');
                document.getElementById("form1").submit();
            }
            else if (alertstr == "4") {
                alert("Your New PM Next Date is in the past\n Please Adjust if you believe this is incorrect")
                document.getElementById("lblsubmit").value = "comp";
                document.getElementById("form1").submit();
            }
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
        }
        function compalert(malert) {
            var marr = malert.split(",");
            var mmsg;
            var hasflg = 0;
            mmsg = "This PM has ";
            for (i = 0; i < marr.length; i++) {
                if (marr[i] == "m") {
                    if (mmsg == "This PM has ") {
                        mmsg = mmsg + "Measurements";
                    }
                    else {
                        mmsg = mmsg + ", Measurements";
                    }
                }
                else if (marr[i] == "h") {
                    if (mmsg == "This PM has ") {
                        mmsg = mmsg + "Task Times";
                    }
                    else {
                        mmsg = mmsg + ", Task Times";
                    }
                }
                else if (marr[i] == "l") {
                    if (mmsg == "This PM has ") {
                        mmsg = mmsg + "Labor Times";
                    }
                    else {
                        mmsg = mmsg + ", Labor Times";
                    }
                }
                else if (marr[i] == "d") {
                    if (mmsg == "This PM has ") {
                        mmsg = mmsg + "Down Times";
                    }
                    else {
                        mmsg = mmsg + ", Down Times";
                    }
                    nodown = "yes"
                }
            }
            mmsg += " that have not been recorded\nDo you wish to Continue?";
            var conf = confirm(mmsg)
            if (conf == true) {
                window.parent.setref();
                //if (nodown == "yes") {
                //    document.getElementById("lblnodown").value = "yes";
                //}
                document.getElementById("lblmalert").value = "ok";
                document.getElementById("lblsubmit").value = "checkcomp"
                FreezeScreenComp('Your Data Is Being Processed...');
                document.getElementById("form1").submit();
            }
            else {
                document.getElementById("lblmalert").value = "";
                alert("Action Cancelled")
            }
        }
        function FreezeScreenComp(msg) {
            scroll(0, 0);
            var outerPane = document.getElementById('FreezePane');
            var innerPane = document.getElementById('InnerFreezePane');
            if (outerPane) outerPane.className = 'FreezePaneOn7';
            if (innerPane) innerPane.innerHTML = msg;

            var outerPane1 = document.getElementById('FreezePane1');
            var innerPane1 = document.getElementById('InnerFreezePane1');
            //if (outerPane1) outerPane1.className = 'FreezePaneOn5';
            //if (innerPane1) innerPane1.innerHTML = msg;
        }
        
        function checkit() {
            var gret = document.getElementById("lblsubmit").value;
            if (gret == "go") {
                gridret();
            }
            var pmid = document.getElementById("lblpmid").value;
            var pmhid = document.getElementById("lblpmhid").value;
            var eqid = document.getElementById("lbleqid").value;
            var wo = document.getElementById("txtwonum").value;
            if (pmid != "") {
                window.parent.updatepmid(pmid, pmhid, eqid, wo);
            }

            var malert = document.getElementById("lblmalert").value;
            if (malert != "") {
                compalert(malert);
            }

            var alertstr = document.getElementById("lblalert").value;
            document.getElementById("lblalert").value = "";
            if (alertstr == "1" || alertstr == "2") {
                adjalert(alert);
            }
            else if (alertstr == "3") {
                document.getElementById("lblsubmit").value = "comp";
                document.getElementById("form1").submit();
            }
            else if (alertstr == "4") {
                alert("Your New PM Next Date is in the past\n Please Adjust if you believe this is incorrect")
                document.getElementById("lblsubmit").value = "comp";
                document.getElementById("form1").submit();
            }
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
        }
        function getleadsched() {
            var wo = document.getElementById("txtwonum").value;
            var sid = document.getElementById("lblsid").value;
            var pmid = document.getElementById("lblpmid").value;
            var typ;
            if (wo != "") {
                typ = "wo";
            }
            else {
                typ = "pm";
            }
            var issched = document.getElementById("lblissched").value;
            var eReturn = window.showModalDialog("../appswo/womlabordialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&issched=" + issched + "&pmid=" + pmid, "", "dialogHeight:520px; dialogWidth:820px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                document.getElementById("lblsubmit").value = "refresh";
                document.getElementById("form1").submit();
            }
        }
        function getsuper(typ) {
            var sched = document.getElementById("lblusesched").value;
            var issched = document.getElementById("lblissched").value;
            var skill = document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            //alert(typ + "," + sched + "," + issched)
            //&& sched == "yes" && issched == "1"
            if (typ == "lead") {
                getleadsched();
            }
            else {
                var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "log") {
                        if (eReturn != "") {
                            var retarr = eReturn.split(",")
                            if (typ == "sup") {
                                document.getElementById("lblsupid").value = retarr[0];
                                document.getElementById("lblsup").value = retarr[1];
                                document.getElementById("txtsup").value = retarr[1];

                            }
                            else {
                                document.getElementById("lblleadid").value = retarr[0];
                                document.getElementById("lbllead").value = retarr[1];
                                document.getElementById("txtlead").value = retarr[1];

                            }
                        }
                    }
                    else {
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        function getcal(fld) {
            var chk = document.getElementById("lblacnt").value;
            if (chk != "0") {
                var conf = confirm("Dates for this PM have been Adjusted in the PM Scheduler\nAre you sure you want to continue?")
                if (conf == true) {
                    window.parent.setref();
                    var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:300px; dialogWidth:300px; resizable=yes");
                    if (eReturn) {
                        var fldret = "txt" + fld;
                        document.getElementById(fldret).value = eReturn;
                        document.getElementById("lblschk").value = "1";
                        if (fld == "n") {
                            document.getElementById("lblnext").value = eReturn;
                            if (fld == "s") {
                                document.getElementById("lblstart").value = eReturn;
                                checknext();
                            }
                        }
                    }
                }
                else {
                    alert("Action Cancelled")
                }
            }
            else {
                window.parent.setref();
                var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:300px; dialogWidth:300px; resizable=yes");
                if (eReturn) {
                    //alert(eReturn)
                    var fldret = "txt" + fld;
                    document.getElementById(fldret).value = eReturn;
                    document.getElementById("lblschk").value = "1";
                    if (fld == "n") {
                        document.getElementById("lblnext").value = eReturn;

                    }
                    if (fld == "s") {
                        document.getElementById("lblstart").value = eReturn;
                        checknext();
                    }
                }
            }
        }
        function savetask() {
            document.getElementById("lblsubmit").value = "save";
            document.getElementById("form1").submit();
        }
        function checknext() {
            var cbs = document.getElementById("cbs");
            if (cbs.checked == true) {
                var nxt = document.getElementById("lblnext").value;
                if (nxt == "") {
                    var nnxt = document.getElementById("lblstart").value;
                    document.getElementById("txtn").value = nnxt;
                    document.getElementById("lblnext").value = nnxt;
                }
            }
        }
        function checkoff() {
            var cbs = document.getElementById("cboff");
            var nxt = document.getElementById("lblnext").value;
            if (cbs.checked == true) {
                document.getElementById("txtn").value = "";
                document.getElementById("lblnext").value = "";
                document.getElementById("lblsubmit").value = "save";
                document.getElementById("form1").submit();
            }
        }
        function checkcomp() {
            var wo = document.getElementById("lblwo").value;
            var next = document.getElementById("lblnext").value;
            var cbs = document.getElementById("cboff");
            if (next != "") {
                if (wo != "") {
                    document.getElementById("lblsubmit").value = "checkcomp";
                    FreezeScreenComp('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
                else {
                    alert("No PM Work Order Created")
                }
            }
            else {
                if (cbs.checked == true) {
                    if (wo != "") {
                        document.getElementById("lblsubmit").value = "checkcomp";
                        FreezeScreenComp('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                    else {
                        alert("No PM Work Order Created")
                    }
                }
                else {
                    alert("No Next Date Selected for this PM")
                }
                
            }
        }
        function checkdel(who) {
            var conf
            if (who == "lead") {
                conf = confirm("Are you sure you want to Delete this Lead Craft?")
            }
            else if (who == "sup") {
                conf = confirm("Are you sure you want to Delete this Supervisor?")
            }
            if (conf == true) {
                if (who == "lead") {
                    document.getElementById("lblleadid").value = ""
                    document.getElementById("lbllead").value = ""
                    document.getElementById("txtlead").value = ""
                    document.getElementById("lblsubmit").value = "save"
                    document.getElementById("form1").submit();
                }
                else if (who == "sup") {
                    document.getElementById("lblsupid").value = ""
                    document.getElementById("lblsup").value = ""
                    document.getElementById("txtsup").value = ""
                    document.getElementById("lblsubmit").value = "save"
                    document.getElementById("form1").submit();
                }
            }
            else {
                alert("Action Cancelled")
            }
        }
            //-->
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div id="FreezePane1" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane1" class="InnerFreezePane">
        </div>
    </div>
    <table style="position: absolute; top: 0px; left: 0px" cellspacing="0" width="740"
        border="1">
        <tr height="24">
            <td class="thdrsinglft" width="26">
                <img border="0" src="../images/appbuttons/minibuttons/pmgridhdr.gif">
            </td>
            <td class="thdrsingrt label" width="740" align="left">
                <asp:Label ID="lang591" runat="server">PM Details</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="2" width="740">
                    <tr class="tbg" height="20">
                        <td class="btmmenulft plainlabel" width="180">
                            PdM
                        </td>
                        <td class="btmmenulft plainlabel" width="350">
                            PM
                        </td>
                        <td class="btmmenulft plainlabel" width="120">
                            <asp:Label ID="lang592" runat="server">Last Date</asp:Label>
                        </td>
                    </tr>
                    <tr height="20">
                        <td id="pmpdm" class="label" runat="server">
                        </td>
                        <td id="pmtitle" class="label" runat="server">
                        </td>
                        <td id="pmlast" class="label" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="0" width="740" border="0">
                    <tr>
                        <td width="110">
                        </td>
                        <td width="110">
                        </td>
                        <td width="20">
                        </td>
                        <td width="50">
                        </td>
                        <td width="140">
                        </td>
                        <td width="5">
                        </td>
                        <td width="10">
                        </td>
                        <td width="20">
                        </td>
                        <td width="20">
                        </td>
                        <td width="80">
                        </td>
                        <td width="60">
                        </td>
                        <td width="35">
                        </td>
                    </tr>
                    <tr height="20">
                        <td class="thdrsingg plainlabel" colspan="5">
                            <asp:Label ID="lang594" runat="server">Scheduling\Charge Information</asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="thdrsingg plainlabel" colspan="6">
                            <asp:Label ID="lang595" runat="server">Responsibility</asp:Label>
                        </td>
                    </tr>
                    <tr height="20">
                        <td class="bluelabel">
                            <asp:Label ID="lang596" runat="server">Start Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txts" runat="server" CssClass="plainlabel" ReadOnly="True" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('s');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                        <td class="plainlabelblue" colspan="2">
                            <asp:Label ID="lang597" runat="server">Use Start?</asp:Label><input id="cbs" onclick="checknext();"
                                type="checkbox" runat="server">
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <input id="cbsupe" type="checkbox" name="cbsupe" runat="server">
                        </td>
                        <td>
                            <img onmouseover="return overlib('Check to send email alert to Supervisor')" onmouseout="return nd()"
                                src="../images/appbuttons/minibuttons/emailcb.gif">
                        </td>
                        <td class="bluelabel">
                            <asp:Label ID="lang598" runat="server">Supervisor</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtsup" runat="server" CssClass="plainlabel" ReadOnly="True" Width="160px"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getsuper('sup');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                height="12px" width="12px" />
                            <img onclick="checkdel('sup');" alt="" src="../images/appbuttons/minibuttons/del.gif"
                                height="12px" width="12px" />
                        </td>
                    </tr>
                    <tr height="20">
                        <td class="bluelabel">
                            <asp:Label ID="lang599" runat="server">Next Date</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtn" runat="server" CssClass="plainlabel" ReadOnly="True" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcal('n');" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19" height="19">
                        </td>
                        <td class="plainlabelblue">
                            <input type="checkbox" id="cboff" runat="server" onclick="checkoff();" />Off
                        </td>
                        <td id="tdcnt" class="plainlabelblue" runat="server" colspan="1">
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <input id="cbleade" type="checkbox" name="cbleade" runat="server">
                        </td>
                        <td>
                            <img onmouseover="return overlib('Check to send email alert to Lead Craft')" onmouseout="return nd()"
                                src="../images/appbuttons/minibuttons/emailcb.gif">
                        </td>
                        <td class="bluelabel">
                            <asp:Label ID="lang600" runat="server">Lead Craft</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtlead" runat="server" CssClass="plainlabel" ReadOnly="True" Width="160px"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getsuper('lead');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                height="12px" width="12px" />
                            <img onclick="checkdel('lead');" alt="" src="../images/appbuttons/minibuttons/del.gif"
                                height="12px" width="12px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel" height="20">
                            <asp:Label ID="lang601" runat="server">Work Order#</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtwonum" runat="server" CssClass="plainlabel" ReadOnly="True" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            <img id="imgwoshed" onmouseover="return overlib('Add or Edit Schedule Days for this Work Order', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="getwosched('wo');" alt="" src="../images/appbuttons/minibuttons/wosched.gif"
                                runat="server">
                        </td>
                        <td class="plainlabelblue">
                            #Runs
                        </td>
                        <td>
                            <asp:TextBox ID="txtruns" runat="server" Width="50px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td rowspan="2">
                        </td>
                        <td class="bluelabel" valign="top" colspan="2" rowspan="2">
                            <asp:Label ID="Label2" runat="server">Labor Group</asp:Label>
                        </td>
                        <td colspan="2" rowspan="2">
                            <div style="border-bottom: 2px groove; border-left: 2px groove; width: 160px; height: 45px;
                                overflow: auto; border-top: 2px groove; border-right: 2px groove" id="divlg"
                                runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="bluelabel">
                            <asp:Label ID="lang602" runat="server">Charge#</asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtcharge" runat="server" CssClass="plainlabel" ReadOnly="True"
                                Width="150px"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="getcharge('wo');" border="0" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <img id="imgsav" onmouseover="return overlib('Save Changes Above', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="savetask();" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                width="20" height="20" runat="server">
                            <img onmouseover="return overlib('Return to Navigation Grid', ABOVE, LEFT)" onmouseout="return nd()"
                                onclick="gridret();" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
                                width="20" height="20">
                            <img id="Img1" onmouseover="return overlib('View PM History', ABOVE, LEFT)" onmouseout="return nd()"
                                onclick="gethist();" border="0" alt="" src="../images/appbuttons/minibuttons/medclock.gif"
                                runat="server">
                            <img id="Img2" onmouseover="return overlib('View Failure Mode History', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="getfmhist();" border="0" alt="" src="../images/appbuttons/minibuttons/fmhist.gif"
                                runat="server"><img id="imgmeas" onmouseover="return overlib('View Measurements for this PM')"
                                    onmouseout="return nd()" onclick="getMeasDiv();" alt="" src="../images/appbuttons/minibuttons/measure.gif"
                                    width="27" height="20" runat="server">
                            <img id="btnedittask" onmouseover="return overlib('View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="editadj();" border="0" alt="" src="../images/appbuttons/minibuttons/screwd.gif"
                                runat="server">
                            <img id="Img3" class="details" onmouseover="return overlib('Adjust Email Lead Time', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="editlead();" border="0" alt="" src="../images/appbuttons/minibuttons/emailadmin.gif"
                                runat="server"><img id="imgat" runat="server" onmouseover="return overlib('Add Attachments', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="getattach();" src="../images/appbuttons/minibuttons/attach.gif">
                                    <img id="imgrt" runat="server" onmouseover="return overlib('Add Route Report', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="getattachrt();" src="../images/appbuttons/minibuttons/attach.gif">
                            <img onmouseover="return overlib('Print Current PM', ABOVE, LEFT)" onmouseout="return nd()"
                                onclick="printpm();" src="../images/appbuttons/minibuttons/printex.gif">&nbsp;<img
                                    id="imgi2" onmouseover="return overlib('Print This PM Work Order', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="printwo();" src="../images/appbuttons/minibuttons/woprint.gif"
                                    runat="server">
                            <img id="Img4" onmouseover="return overlib('Print This PM Work Order w\images', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="printwopic();" src="../images/appbuttons/minibuttons/printpic.gif"
                                runat="server">
                            <img id="imgcomp" onmouseover="return overlib('Complete this PM', ABOVE, LEFT)" onmouseout="return nd()"
                                onclick="checkcomp();" src="../images/appbuttons/minibuttons/comp.gif" runat="server">
                                &nbsp;&nbsp;&nbsp;
                                <img id="imgdeltask" onmouseover="return overlib('Delete This PM Record', ABOVE, LEFT)"
                            onmouseout="return nd()" onclick="checkdeltask();" border="0" alt="" src="../images/appbuttons/minibuttons/del.gif"
                            runat="server" visible="false">
                            
                        </td>
                        <td class="bluelabel" colspan="3">
                            <asp:Label ID="lang603" runat="server">Email Lead Time (Days)</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtleadtime" runat="server" CssClass="plainlabel" Width="45px"></asp:TextBox>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
        <tr height="20">
            <td style="height: 24px" class="thdrsinglft" width="26">
                <img border="0" src="../images/appbuttons/minibuttons/pmgridhdr.gif">
            </td>
            <td style="height: 24px" class="thdrsingrt label" width="714" align="left">
                <asp:Label ID="lang604" runat="server">PM Tasks</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="width: 740px; height: 198px; overflow: auto; border-right: 1px solid black"
                    id="tdtasks" runat="server">
                </div>
            </td>
        </tr>
    </table>
    <input id="lblpmid" type="hidden" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lblpmhid" type="hidden" runat="server"><input id="lbleqid" type="hidden"
        runat="server">
    <input id="lblnext" type="hidden" runat="server"><input id="lblschk" type="hidden"
        runat="server">
    <input id="lblsup" type="hidden" runat="server">
    <input id="lbllead" type="hidden" runat="server">
    <input id="lblskillid" type="hidden" runat="server">
    <input id="lblacnt" type="hidden" runat="server">
    <input id="txtsearch" type="hidden" runat="server">
    <input id="lblwo" type="hidden" runat="server">
    <input id="lbllog" type="hidden" runat="server">
    <input id="lblmalert" type="hidden" runat="server">
    <input id="lblalert" type="hidden" runat="server">
    <input id="lbldocs" type="hidden" runat="server">
    <input id="lblondate" type="hidden" runat="server"><input id="lblro" type="hidden"
        runat="server">
    <input id="lblpiccnt" type="hidden" runat="server">
    <input id="lblusetdt" type="hidden" runat="server">
    <input id="lblusetotal" type="hidden" runat="server">
    <input id="lblttime" type="hidden" runat="server">
    <input id="lbldtime" type="hidden" runat="server">
    <input id="lblacttime" type="hidden" runat="server">
    <input id="lblactdtime" type="hidden" runat="server"><input type="hidden" id="lblhalert"
        runat="server">
    <input type="hidden" id="lbldalert" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblworuns" runat="server">
    <input type="hidden" id="lblruns" runat="server">
    <input type="hidden" id="lblstart" runat="server" />
    <input type="hidden" id="lblleadid" runat="server" />
    <input type="hidden" id="lblsupid" runat="server" />
    <input type="hidden" id="lblissched" runat="server" />
    <input id="lblusesched" type="hidden" runat="server" />
    <input id="lblhidejts" type="hidden" runat="server" />
    <input type="hidden" id="lblisactive" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    <input type="hidden" id="lblnodown" runat="server" />
    <input type="hidden" id="lblerrcnt" runat="server" />
    <input type="hidden" id="lbllcnt" runat="server" />
    <input type="hidden" id="lblMWNO" runat="server" />
    <input type="hidden" id="lbldelwo" runat="server" />
    <input type="hidden" id="lblrtid" runat="server" />
    </form>
</body>
</html>
