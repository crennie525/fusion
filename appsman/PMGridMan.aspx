<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMGridMan.aspx.vb" Inherits="lucy_r12.PMGridMan" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMGridMan</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMGridManaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <style type="text/css">
        .blklink
{
font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:12px;
	text-decoration:none;
color: Black;	
text-decoration: underline;
}
    </style>
</head>
<body class="tbg" onload="checkcnt();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 0px" width="660" cellspacing="0">
        <tbody>
            <tr height="22">
                <td class="thdrsinglft" width="26">
                    <img src="../images/appbuttons/minibuttons/pmgridhdr.gif" border="0">
                </td>
                <td class="thdrsingrt label" align="left" width="634">
                    <asp:Label ID="lang639" runat="server">Current Records</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="../images/appbuttons/minibuttons/2PX.gif">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="width: 680px; height: 392px; overflow: auto">
                        <asp:Repeater ID="rptrtasks" runat="server">
                            <HeaderTemplate>
                                <table width="660" cellspacing="2">
                                    <tr class="tbg" height="26">
                                        <td class="thdrsingg plainlabel" width="150">
                                            PdM
                                        </td>
                                        <td class="thdrsingg plainlabel" width="320">
                                            PM
                                        </td>
                                        <td class="details" width="100">
                                            <asp:Label ID="lang640" runat="server">Last Date</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="90">
                                            <asp:Label ID="lang641" runat="server">Next Date</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="30">
                                            <img src="../images/appbuttons/minibuttons/gwarningnbg.gif">
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg" height="20">
                                    <td class="plainlabel">
                                        &nbsp;
                                        <asp:Label ID="lblpdm" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:LinkButton ID="lbltn" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbllast" Text='<%# DataBinder.Eval(Container.DataItem,"lastdate")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lblnext" Text='<%# DataBinder.Eval(Container.DataItem,"nextdate")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td align="center">
                                        <img id="iwi" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                            src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblpmiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="transrowblue" height="20">
                                    <td class="plainlabel transrowblue">
                                        &nbsp;
                                        <asp:Label ID="lblpdmalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:LinkButton ID="lbltnalt" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbllastalt" Text='<%# DataBinder.Eval(Container.DataItem,"lastdate")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="lblnextalt" Text='<%# DataBinder.Eval(Container.DataItem,"nextdate")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td align="center" class="transrowblue">
                                        <img id="iwa" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                            src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblpmidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid"
        type="hidden" name="lbltaskid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="taskcnt" type="hidden" runat="server" name="taskcnt"><input id="appchk"
        type="hidden" name="appchk" runat="server">
    <input id="lblcoid" type="hidden" runat="server" name="lblcoid"><input type="hidden"
        id="lblpmid" runat="server">
    <input type="hidden" id="lbltyp" runat="server">
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="lblpiccnt" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
