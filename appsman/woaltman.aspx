<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="woaltman.aspx.vb" Inherits="lucy_r12.woaltman" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>woaltman</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/woaltmanaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="exittask();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td colspan="2" id="tdplain" runat="server" class="details">
                <table>
                    <tr height="30">
                        <td class="bluelabel" id="tdtop" colspan="2" runat="server">
                        </td>
                    </tr>
                    <tr height="30">
                        <td class="bluelabel" id="tdend" colspan="2" runat="server">
                        </td>
                    </tr>
                    <tr height="24">
                        <td class="label" colspan="2">
                            <asp:Label ID="lang750" runat="server">New Scheduled Start</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtnewdate" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Get a New Start Date', ABOVE, LEFT)" onclick="getcal('newdate');"
                                onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19">
                        </td>
                    </tr>
                    <tr height="24">
                        <td class="label" colspan="2">
                            <asp:Label ID="lang751" runat="server">New Scheduled Complete</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtnewend" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <img onmouseover="return overlib('Get a New Complete Date', ABOVE, LEFT)" onclick="getcal('newend');"
                                onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                width="19">
                        </td>
                    </tr>
                    <tr>
                        <td class="plainlabel" colspan="2">
                            <asp:CheckBox ID="cbcalc" runat="server" Checked="True" Text="Calculate Complete">
                            </asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <img onmouseover="return overlib('Return Without Saving Changes', ABOVE, LEFT)" onclick="exit();"
                                onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/return.gif"
                                width="20">
                            <img id="imgsav" runat="server" onmouseover="return overlib('Save Date Revision Changes', ABOVE, LEFT)"
                                onclick="savetask();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                width="20">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="tdboth" runat="server" colspan="2" class="details">
                <table>
                    <tr>
                        <td id="tdwos" runat="server">
                            <a href="#" onclick="getwosched('wo');" class="A1">
                                <asp:Label ID="lang752" runat="server">Edit Work Order Scheduling</asp:Label></a>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdjos" runat="server">
                            <a href="#" onclick="getwosched('jp');" class="A1">
                                <asp:Label ID="lang753" runat="server">Edit Job Plan Scheduling</asp:Label></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblpmid" type="hidden" runat="server" name="lblpmid">
    <input id="lblorig" type="hidden" runat="server" name="lblorig">
    <input id="lblpost" type="hidden" runat="server" name="lblpost"><input id="rbval"
        type="hidden" runat="server" name="rbval">
    <input type="hidden" id="lbllast" runat="server" name="lbllast"><input type="hidden"
        id="lblfreq" runat="server" name="lblfreq">
    <input type="hidden" id="lbljpid" runat="server">
    <input type="hidden" id="lblstat" runat="server">
    <input type="hidden" id="lblskill" runat="server">
    <input type="hidden" id="lblskillid" runat="server">
    <input type="hidden" id="lblsdays" runat="server"><input type="hidden" id="lblfslang"
        runat="server" name="lblfslang">
    </form>
</body>
</html>
