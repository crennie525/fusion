﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmpsmodpre.aspx.vb" Inherits="lucy_r12.pmpsmodpre" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMSetAdjManPREaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head> 
<body onload="sstchur_SmartScroller_Scroll();checkit();">
    <form id="form1" method="post" runat="server">
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <asp:DataList ID="dgadj" runat="server">
                        <HeaderTemplate>
                            <table width="880">
                                <tr>
                                    <td width="60">
                                    </td>
                                    <td width="60">
                                    </td>
                                    <td width="510">
                                    </td>
                                    <td width="100">
                                    </td>
                                    <td width="100">
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="trfunc" runat="server">
                                <td class="label">
                                    <asp:Label ID="lang721" runat="server">Function:</asp:Label>
                                </td>
                                <td class="plainlabel" colspan="5" id="tdfunci" runat="server">
                                    <%# DataBinder.Eval(Container, "DataItem.func") %>
                                </td>
                            </tr>
                            <tr id="trdiv" runat="server">
                                <td colspan="5">
                                    <hr>
                                </td>
                            </tr>
                            <tr id="trhead" runat="server">
                                <td colspan="4" class="plainlabel">
                                    <b>
                                        <asp:Label ID="lang722" runat="server">PM Designation:</asp:Label></b>&nbsp;&nbsp;<%# DataBinder.Eval(Container, "DataItem.pm") %>
                                </td>
                                <td>
                                    <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtskid") %>'
                                        CssClass="details">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr id="trhdr" runat="server">
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang723" runat="server">Edit</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang724" runat="server">Task#</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang725" runat="server">Task Description</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang726" runat="server">Current Pass Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang727" runat="server">Use Pass Adj?</asp:Label>
                                </td>
                            </tr>
                            <tr id="trtask" runat="server">
                                <td rowspan="3">
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                        ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="lbltasknume" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.task") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:Label ID="lblokadjcurr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.okadj") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.useadj") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="btmmenu plainlabel" colspan="2">
                                    <asp:Label ID="lang728" runat="server">Failure Modes</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang729" runat="server">Current Fail Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang730" runat="server">Use Fail Adj?</asp:Label>
                                </td>
                            </tr>
                            <tr id="trfm" runat="server">
                                <td class="plainlabel" colspan="2">
                                    <%# DataBinder.Eval(Container, "DataItem.fm1") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:Label ID="lblfadjcurr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fadj") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.usefadj") %>
                                </td>
                            </tr>
                            <tr id="Tr4" runat="server">
                                <td colspan="5">
                                    <hr>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <tr id="trfunce" runat="server">
                                <td class="label">
                                    <asp:Label ID="lang731" runat="server">Function:</asp:Label>
                                </td>
                                <td class="plainlabel" colspan="5">
                                    <%# DataBinder.Eval(Container, "DataItem.func") %>
                                </td>
                            </tr>
                            <tr id="trdive" runat="server">
                                <td colspan="5">
                                    <hr>
                                </td>
                            </tr>
                            <tr id="Tr1" runat="server">
                                <td colspan="5" class="plainlabel">
                                    <b>
                                        <asp:Label ID="lang732" runat="server">PM Designation:</asp:Label></b>&nbsp;&nbsp;<%# DataBinder.Eval(Container, "DataItem.pm") %>
                                </td>
                            </tr>
                            <tr id="Tr2" runat="server">
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang733" runat="server">Edit</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang734" runat="server">Task#</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang735" runat="server">Task Description</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang736" runat="server">Current Pass Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang737" runat="server">Use Pass Adj?</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="3">
                                    <asp:ImageButton ID="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update"
                                        ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                        ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.tasknum") %>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.task") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:TextBox ID="lblokadj" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.okadj") %>'
                                        Width="40px">
                                    </asp:TextBox>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:DropDownList ID="ddokuse" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("oi")) %>'>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="btmmenu plainlabel" colspan="2">
                                    <asp:Label ID="lang738" runat="server">Failure Modes</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang739" runat="server">Current Fail Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang740" runat="server">Use Fail Adj?</asp:Label>
                                </td>
                            </tr>
                            <tr id="Tr3" runat="server">
                                <td class="plainlabel" colspan="2">
                                    <%# DataBinder.Eval(Container, "DataItem.fm1") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:TextBox ID="lblfadj" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fadj") %>'
                                        Width="40px">
                                    </asp:TextBox>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:DropDownList ID="ddfuse" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fi")) %>'>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="Tr5" runat="server">
                                <td colspan="5">
                                    <hr>
                                </td>
                            </tr>
                        </EditItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server">
    <input id="lbltaskcnt" type="hidden" name="lbltaskcnt" runat="server"><input id="lbltaskcurrcnt"
        type="hidden" name="lbltaskcurrcnt" runat="server">
    <input id="lblrow" type="hidden" name="lblrow" runat="server"><input id="lbltid"
        type="hidden" runat="server">
    <input id="lbloadjc" type="hidden" runat="server">
    <input id="lblfadjc" type="hidden" runat="server">
    <input id="lblro" type="hidden" runat="server">
    <input type="hidden" id="lbltyp" runat="server"><input type="hidden" id="lblrev"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="yCoord" runat="server" />
    <input type="hidden" id="xCoord" runat="server">
    </form>
</body>
</html>

