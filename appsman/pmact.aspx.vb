

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmact
    Inherits System.Web.UI.Page
    Protected WithEvents lang566 As System.Web.UI.WebControls.Label

    Protected WithEvents lang565 As System.Web.UI.WebControls.Label

    Protected WithEvents lang564 As System.Web.UI.WebControls.Label

    Protected WithEvents lang563 As System.Web.UI.WebControls.Label

    Protected WithEvents lang562 As System.Web.UI.WebControls.Label

    Protected WithEvents lang561 As System.Web.UI.WebControls.Label

    Protected WithEvents lang560 As System.Web.UI.WebControls.Label

    Protected WithEvents lang559 As System.Web.UI.WebControls.Label

    Protected WithEvents lang558 As System.Web.UI.WebControls.Label

    Protected WithEvents lang557 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim ap As New AppUtils
    Dim ds As DataSet
    Dim wonum, jpid, stat, pmid, pmhid, ro, Login, pdt, rd, actrd, isdown, ttt, ttime, acttime As String
    Dim dgw As Integer = 0
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisdown As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txttdt As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblusetdt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblactrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdestdt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblusetotal As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trtdt As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tddtt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtdt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsavedt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtttt As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdttt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsavettt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblest As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblruns As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblworuns As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbls As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbletasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tddttp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtdtp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsavedtp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txttdtp As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblexd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblexdp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblactdt As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txts As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtf As System.Web.UI.WebControls.TextBox
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdactstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdactfinish As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdest As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdact As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dghours As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dghours.Columns(0).Visible = False
                dgw += 60
            End If
            wonum = Request.QueryString("wo").ToString '"1352" '
            lblwo.Value = wonum
            pmid = Request.QueryString("pmid").ToString '"139" '
            pmhid = Request.QueryString("pmhid").ToString '"501" '
            lblpmid.Value = pmid
            lblpmhid.Value = pmhid
            comp.Open()
            If pmhid = "" And pmid <> "" Then
                Try
                    sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                    pmhid = comp.strScalar(sql)
                    lblpmhid.Value = pmhid
                Catch ex As Exception
                    sql = "insert into pmhist (pmid) values ('" & pmid & "')"
                    comp.Update(sql)
                    Try
                        sql = "select max(pmhid) from pmhist where pmid = '" & pmid & "'"
                        pmhid = comp.strScalar(sql)
                        lblpmhid.Value = pmhid
                    Catch ex1 As Exception

                    End Try
                End Try
                
            End If
            GetWoHead(wonum)
            pdt = ap.PDTEntry
            If pdt = "lvdtt" Then
                lblusetdt.Value = "no"
                tddtt.Attributes.Add("class", "plainlabel view")
                tdtdt.Attributes.Add("class", "details")
                tdsavedt.Attributes.Add("class", "details")

                tddttp.Attributes.Add("class", "plainlabel view")
                tdtdtp.Attributes.Add("class", "details")
                tdsavedtp.Attributes.Add("class", "details")
            Else
                lblusetdt.Value = "yes"
                tddtt.Attributes.Add("class", "details")
                tdsavedt.Attributes.Add("class", "view")
                tdtdt.Attributes.Add("class", "plainlabel view")

                tddttp.Attributes.Add("class", "details")
                tdsavedtp.Attributes.Add("class", "view")
                tdtdtp.Attributes.Add("class", "plainlabel view")

                dghours.Columns(10).Visible = False
                dgw += 120
            End If
            ttt = ap.TTTEntry
            If ttt = "lvtpt" Then
                lblusetotal.Value = "no"
                tdact.Attributes.Add("class", "plainlabel view")
                tdttt.Attributes.Add("class", "details")
                tdsavettt.Attributes.Add("class", "details")
            Else
                lblusetotal.Value = "yes"
                tdact.Attributes.Add("class", "details")
                tdsavettt.Attributes.Add("class", "view")
                tdttt.Attributes.Add("class", "plainlabel view")
                dghours.Columns(8).Visible = False
                dgw += 120
            End If
            If pdt = "lvtdt" And ttt = "lvttt" Then
                If dghours.Columns(0).Visible = True Then
                    dghours.Columns(0).Visible = False
                    dgw += 60

                End If
            End If
            checkdown()
            CheckDT(pmid)
            PopDL(pmid)
            comp.Dispose()
            If wonum = "" Then
                If dghours.Columns(0).Visible = True Then
                    dghours.Columns(0).Visible = False
                    dgw += 60

                End If
            End If
            dghours.Width = 1000 - dgw
        Else
            If Request.Form("lblsubmit") = "s" Then
                lblsubmit.Value = ""
                comp.Open()
                CompWo("s")
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "f" Then
                lblsubmit.Value = ""
                comp.Open()
                CompWo("f")
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "savettime" Then
                lblsubmit.Value = ""
                comp.Open()
                Dim etime As String = lblest.Value
                Dim atime As String = txtttt.Text
                SaveTTime(atime, etime)
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "savedtime" Then
                lblsubmit.Value = ""
                comp.Open()
                Dim ard As String = txttdt.Text
                SaveDTime(ard)
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "savedtimep" Then
                lblsubmit.Value = ""
                comp.Open()
                Dim ard As String = txttdtp.Text
                SaveDTimep(ard)
                comp.Dispose()
            End If
        End If
    End Sub
    Private Sub checkdown()
        Dim startdown, totaldown, startdownp, totaldownp As String
        sql = "select startdown, totaldown, startdownp, totaldownp from eqhist where wonum = '" & wonum & "' and pmid = '" & pmid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            startdown = dr.Item("startdown").ToString
            totaldown = dr.Item("totaldown").ToString
            startdownp = dr.Item("startdownp").ToString
            totaldownp = dr.Item("totaldownp").ToString
        End While
        dr.Close()
        If startdown <> "" Then
            lblexd.Value = "Y"
            'lblactdt
            lang565.Attributes.Add("class", "disablelabel")
            txttdt.Enabled = False
        End If
        If startdownp <> "" Then
            lblexdp.Value = "Y"
            lblactdt.Attributes.Add("class", "disablelabel")
            txttdtp.Enabled = False
        End If
        'If totaldown <> "" Then
        'txttdt.Text = totaldown
        'End If
        'If totaldownp <> "" Then
        'txttdt.Text = totaldownp
        'End If
    End Sub
    Private Sub PopDL(ByVal pmid As String)
        sql = "usp_getpmlab '" & pmid & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dghours.DataSource = dv
        dghours.DataBind()
    End Sub
    Private Sub CheckDT(ByVal pmid As String)
        Dim totalttime, totaldtime, totaldtimep As String
        Dim usedown As String = lblusetdt.Value
        Dim usetotal As String = lblusetotal.Value

        sql = "select sum(isnull(t.rd,0)) as 'rd', sum(isnull(t.actrd,0)) as 'actrd', p.rd as 'isdown', " _
        + "sum(isnull(t.ttime,0)) as 'ttime', sum(isnull(t.acttime,0)) as 'acttime', " _
        + "isnull(p.acttime,0) as 'totalttime', isnull(p.actdtime,0) as 'totaldtime', isnull(p.actdptime,0) as 'totaldtimep' " _
        + "from pmtrack t left join pm p on p.pmid = t.pmid where t.pmid = '" & pmid & "' " _
        + "group by p.rd, p.acttime, p.actdtime, p.actdptime"
        dr = comp.GetRdrData(sql)
        While dr.Read
            rd = dr.Item("rd").ToString
            actrd = dr.Item("actrd").ToString
            isdown = dr.Item("isdown").ToString
            ttime = dr.Item("ttime").ToString
            acttime = dr.Item("acttime").ToString

            totalttime = dr.Item("totalttime").ToString
            totaldtime = dr.Item("totaldtime").ToString
            totaldtimep = dr.Item("totaldtimep").ToString
        End While
        dr.Close()
        lblrd.Value = rd
        tdestdt.InnerHtml = rd
        lblactrd.Value = actrd
        txttdt.Text = actrd
        tdest.InnerHtml = ttime
        lblest.Value = ttime

        If usedown = "no" Then
            tddtt.InnerHtml = actrd
        Else
            txttdt.Text = totaldtime
            txttdtp.Text = totaldtimep
        End If

        If usetotal = "no" Then
            tdact.InnerHtml = acttime
        Else
            txtttt.Text = totalttime
        End If
        If isdown = "Down" Then
            lblisdown.Value = "yes"
        Else
            lblisdown.Value = "no"
        End If

    End Sub
    Private Sub GetWoHead(ByVal wonum As String)
        Dim lc, skid, eqid, runs, cruns As String
        wonum = lblwo.Value
        sql = "select eqid, wonum, description, " _
             + "Convert(char(10),targstartdate,101) as 'tstart', " _
        + "Convert(char(10),targcompdate,101) as 'tcomp', " _
        + "Convert(char(10),schedstart,101) as 'sstart', " _
        + "Convert(char(10),schedfinish,101) as 'scomp1', " _
        + "Convert(char(10),actstart,101) as 'astart', " _
        + "Convert(char(10),actfinish,101) as 'acomp', " _
            + "targstartdate, actstart, actfinish, estlabhrs, " _
        + "actlabhrs, jpid, leadcraft, skillid, multidates, multicnt from workorder where wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdstart.InnerHtml = dr.Item("tstart").ToString
            txts.Text = dr.Item("astart").ToString
            txtf.Text = dr.Item("acomp").ToString
            lbls.Value = dr.Item("astart").ToString
            lblf.Value = dr.Item("acomp").ToString
            'tdest.InnerHtml = dr.Item("estlabhrs").ToString
            'tdact.InnerHtml = dr.Item("actlabhrs").ToString
            'txtlead.Text = dr.Item("leadcraft").ToString
            skid = dr.Item("skillid").ToString
            eqid = dr.Item("eqid").ToString
            runs = dr.Item("multidates").ToString
            cruns = dr.Item("multicnt").ToString
        End While
        dr.Close()
        lbleqid.Value = eqid
        lblruns.Value = runs
        lblworuns.Value = cruns
        Try
            'ddskill.SelectedValue = skid
        Catch ex As Exception

        End Try
        'GetWO()
        'ifmain.Attributes.Add("src", "woactm.aspx?typ=p&wo=" + wonum + "&stat=" + stat + "&sav=no")
    End Sub
    Private Sub CompWo(ByVal fld As String)
        wonum = lblwo.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim dat As String
        Dim csel As Date
        Dim ssel As Date
        Dim sel As Date
        Dim odat As String = ""
        If fld = "s" Then
            dat = txts.Text
            dat = lbls.Value
            ssel = CDate(dat)
            odat = lblf.Value

            If odat <> "" Then
                sel = lblf.Value
                csel = CDate(sel)
            End If
            If ssel > csel Or odat = "" Then
                sql = "update workorder set actstart = '" & dat & "', actfinish = '" & dat & "' where wonum = '" & wonum & "'; " _
            + "update pmhist set actstart = '" & dat & "', actfinish = '" & dat & "' where pmid = '" & pmid & "' and pmhid = '" & pmhid & "'"
                comp.Update(sql)
                txtf.Text = dat
            Else
                sql = "update workorder set actstart = '" & dat & "' where wonum = '" & wonum & "'; " _
                            + "update pmhist set actstart = '" & dat & "' where pmid = '" & pmid & "' and pmhid = '" & pmhid & "'"
                comp.Update(sql)
            End If
            
            txts.Text = dat
            lbls.Value = dat
        ElseIf fld = "f" Then
            dat = txtf.Text
            dat = lblf.Value
            csel = CDate(dat)
            odat = lbls.Value
            If odat <> "" Then
                sel = lbls.Value
                ssel = CDate(sel)
            End If
            If csel < ssel Or odat = "" Then
                sql = "update workorder set actfinish = '" & dat & "', actstart = '" & dat & "' where wonum = '" & wonum & "'; " _
           + "update pmhist set actfinish = '" & dat & "', actstart = '" & dat & "' where pmid = '" & pmid & "' and pmhid = '" & pmhid & "'"
                txts.Text = dat
            Else
                sql = "update workorder set actfinish = '" & dat & "' where wonum = '" & wonum & "'; " _
           + "update pmhist set actfinish = '" & dat & "' where pmid = '" & pmid & "' and pmhid = '" & pmhid & "'"
            End If
           
            comp.Update(sql)
            txtf.Text = dat
            lblf.Value = dat
        End If
    End Sub

    Private Sub dghours_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.CancelCommand
        dghours.EditItemIndex = -1
        comp.Open()
        wonum = lblwo.Value
        pmid = lblpmid.Value
        PopDL(pmid)
        comp.Dispose()
    End Sub

    Private Sub dghours_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.EditCommand
        comp.Open()
        pmid = lblpmid.Value
        wonum = lblwo.Value
        dghours.EditItemIndex = e.Item.ItemIndex
        PopDL(pmid)
        comp.Dispose()
    End Sub

    Private Sub dghours_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dghours.ItemDataBound
        Dim ibfm As ImageButton
        stat = lblstat.Value
        Dim icnt As Integer = 0
        Dim ecnt As Integer = 0
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'Dim img As HtmlImage = CType(e.Item.FindControl("imgti"), HtmlImage)
            'Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tsk = comp.ModString2(tsk)
            'tsk = Mid(tsk, 1, 500)
            'tsk = RTrim(tsk)
            'tsk = LTrim(tsk)
            'tdtsk.InnerHtml = tsk
            'img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

            'Dim img1 As HtmlImage = CType(e.Item.FindControl("imglci"), HtmlImage)
            'Dim wojtid As String = DataBinder.Eval(e.Item.DataItem, "wojtid").ToString
            'Dim indx As String = "N"
            'img1.Attributes.Add("onclick", "getsuper('" & indx & "','" & wojtid & "');")

            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "compl").ToString
            Dim tsklbl As Label = CType(e.Item.FindControl("lblcompl"), Label)
            If tsk = "0" Or tsk = "" Then
                tsklbl.Text = "No"
            Else
                tsklbl.Text = "Yes"
            End If
        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "compl").ToString
            Dim tskbx As System.Web.UI.HtmlControls.HtmlInputCheckBox = CType(e.Item.FindControl("cbcompl"), System.Web.UI.HtmlControls.HtmlInputCheckBox)
            If tsk = "1" Then
                tskbx.Checked = True
            Else
                tskbx.Checked = False
            End If
            'Dim img As HtmlImage = CType(e.Item.FindControl("imgte"), HtmlImage)
            'Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tsk = comp.ModString2(tsk)
            ''tdtsk.InnerHtml = tsk
            'img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

            'Dim img1 As HtmlImage = CType(e.Item.FindControl("imglce"), HtmlImage)
            'Dim wojtid As String = DataBinder.Eval(e.Item.DataItem, "wojtid").ToString
            'Dim indx As String = e.Item.ItemIndex
            'img1.Attributes.Add("onclick", "getsuper('" & indx & "','" & wojtid & "');")

        End If
    End Sub

    Private Sub dghours_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.UpdateCommand
        Dim ret, typ, fail, pass, task, atime, lc, ard, eqid As String
        Dim tsk, cnt, adj, compl As String
        wonum = lblwo.Value
        eqid = lbleqid.Value
        pmid = lblpmid.Value
        Dim pmtskid As String = CType(e.Item.FindControl("lblpmtskid"), Label).Text
        Dim tskbx As System.Web.UI.HtmlControls.HtmlInputCheckBox = CType(e.Item.FindControl("cbcompl"), System.Web.UI.HtmlControls.HtmlInputCheckBox)
        If tskbx.Checked = True Then
            compl = "1"
        Else
            compl = "0"
        End If

        Dim usetotal As String = lblusetotal.Value
        Dim usetdt As String = lblusetdt.Value

        atime = CType(e.Item.FindControl("txtatime"), TextBox).Text
        lc = "" 'CType(e.Item.FindControl("txtalead"), TextBox).Text

        If usetotal = "no" Then
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToDecimal(atime)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr320", "pmact.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        Else
            atime = "0"
        End If


        ard = CType(e.Item.FindControl("txtactrd"), TextBox).Text
        If usetdt = "no" Then
            Dim ardchk As Long
            Try
                ardchk = System.Convert.ToDecimal(ard)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr321", "pmact.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        Else
            ard = "0"
        End If
        pmhid = lblpmhid.Value

        'sql = "usp_uppmlab '" & lc & "','" & atime & "', '" & pmtskid & "', '" & pmid & "', '" & wonum & "','" & ard & "'"
        sql = "usp_uppmlab2 '" & lc & "','" & atime & "', '" & pmtskid & "', '" & pmid & "', " _
        + "'" & wonum & "','" & ard & "','" & eqid & "','" & usetotal & "','" & usetdt & "','" & pmhid & "','" & compl & "'"

        comp.Open()
        comp.Update(sql)
        wonum = lblwo.Value
        pmid = lblpmid.Value
        dghours.EditItemIndex = -1
        PopDL(pmid)
        CheckDT(pmid)
        GetWoHead(wonum)
        comp.Dispose()
    End Sub
    Private Sub SaveTTime(ByVal atime As String, ByVal etime As String)
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim wonum As String = lblwo.Value

        Dim runs, cruns As String
        runs = lblruns.Value
        cruns = lblworuns.Value
        Dim runsi As Integer
        Try
            runsi = CType(runs, Integer)
        Catch ex As Exception
            runs = "1"
            runsi = 1
        End Try
        Dim crunsi As Integer
        Try
            crunsi = CType(cruns, Integer)
        Catch ex As Exception
            cruns = "0"
            crunsi = 0
        End Try

        Dim usetotal As String = lblusetotal.Value
        If usetotal = "yes" Then
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToDecimal(atime)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr332", "PMDivMan.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If runs = "1" Then
                sql = "update pm set acttime = '" & atime & "' where pmid = '" & pmid & "'; " _
                + "update pmhist set acttime = '" & atime & "' where pmhid = '" & pmhid & "'; " _
                + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @emins decimal(10,2), @ehrs decimal(10,2); " _
                + "set @dmins = '" & etime & "'; " _
                + "set @dhrs = @dmins / 60; " _
                + "set @emins = '" & etime & "'; " _
                + "set @ehrs = @emins / 60; " _
                + "update workorder set actlabhrspmt = @dhrs, estlabhrs = @ehrs where wonum = '" & wonum & "'; "
                comp.Update(sql)
            Else
                crunsi = crunsi + 1
                sql = "update pm set acttime = '" & atime & "' where pmid = '" & pmid & "'; " _
                + "update pmhist set acttime = '" & atime & "' where pmhid = '" & pmhid & "'; " _
                + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @emins decimal(10,2), @ehrs decimal(10,2); " _
                + "set @dmins = '" & etime & "'; " _
                + "set @dhrs = @dmins / 60; " _
                + "set @emins = '" & etime & "'; " _
                + "set @ehrs = @emins / 60; " _
                + "update workorder set actlabhrspmt = isnull(actlabhrs, 0) + @dhrs, estlabhrs = isnull(estlabhrs, 0) + @ehrs where wonum = '" & wonum & "'; " _
                + "update womultidates set actlabhrspmt = @dhrs, estlabhrs = @ehrs where wonum = '" & wonum & "' and wocnt = '" & crunsi & "'; "
                comp.Update(sql)
            End If

        Else
            Exit Sub
        End If
    End Sub
    Private Sub SaveDTime(ByVal ard As String)
        Dim usetdt As String = lblusetdt.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim eqid As String = lbleqid.Value
        wonum = lblwo.Value
        If usetdt = "no" Then
            Dim ardchk As Long
            Try
                ardchk = System.Convert.ToDecimal(ard)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr323", "pmact.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        sql = "update pm set actdtime = '" & ard & "' where pmid = '" & pmid & "'; " _
        + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @eqhid int; " _
        + "set @dmins = '" & ard & "'; " _
        + "set @dhrs = @dmins / 60; " _
        + "select @eqhid = eqhid from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "if @eqhid is null " _
        + "begin " _
        + "insert into eqhist (eqid, wonum, ispm, pmid, totaldown)" _
        + "values ('" & eqid & "', '" & wonum & "', 1, '" & pmid & "', @dhrs)" _
        + "end " _
        + "else " _
        + "begin " _
        + "update eqhist set totaldown = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "' " _
        + "end; " _
        + "update pmhist set actdtime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update equipment set totaldown = (select sum(totaldown) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
        comp.Update(sql)
        CheckDT(pmid)
    End Sub


    Private Sub SaveDTimep(ByVal ard As String)
        Dim usetdt As String = lblusetdt.Value
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        Dim eqid As String = lbleqid.Value
        wonum = lblwo.Value
        If usetdt = "no" Then
            Dim ardchk As Long
            Try
                ardchk = System.Convert.ToDecimal(ard)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr323", "pmact.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        sql = "update pm set actdptime = '" & ard & "' where pmid = '" & pmid & "'; " _
        + "declare @dmins decimal(10,2), @dhrs decimal(10,2), @eqhid int; " _
        + "set @dmins = '" & ard & "'; " _
        + "set @dhrs = @dmins / 60; " _
        + "select @eqhid = eqhid from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "'; " _
        + "if @eqhid is null " _
        + "begin " _
        + "insert into eqhist (eqid, wonum, ispm, pmid, totaldownp)" _
        + "values ('" & eqid & "', '" & wonum & "', 1, '" & pmid & "', @dhrs)" _
        + "end " _
        + "else " _
        + "begin " _
        + "update eqhist set totaldownp = @dhrs where eqid = '" & eqid & "' and wonum = '" & wonum & "' " _
        + "end; " _
        + "update pmhist set actdptime = @dmins where pmhid = '" & pmhid & "'; " _
        + "update equipment set totaldownp = (select sum(totaldownp) from eqhist where eqid = '" & eqid & "' and wonum = '" & wonum & "') " _
        + "where eqid = '" & eqid & "'"
        comp.Update(sql)
        CheckDT(pmid)
    End Sub

    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dghours.Columns(0).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "0")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(1).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "1")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(2).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "2")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(3).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "3")
        Catch ex As Exception
        End Try
        Try
            'dghours.Columns(4).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "4")
        Catch ex As Exception
        End Try
        'Try
        'dghours.Columns(5).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "5")
        'Catch ex As Exception
        'End Try
        Try
            dghours.Columns(7).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "7")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(8).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "8")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(10).HeaderText = dlabs.GetDGPage("pmact.aspx", "dghours", "10")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("pmact.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang557.Text = axlabs.GetASPXPage("pmact.aspx", "lang557")
        Catch ex As Exception
        End Try
        Try
            lang558.Text = axlabs.GetASPXPage("pmact.aspx", "lang558")
        Catch ex As Exception
        End Try
        Try
            lang559.Text = axlabs.GetASPXPage("pmact.aspx", "lang559")
        Catch ex As Exception
        End Try
        Try
            lang560.Text = axlabs.GetASPXPage("pmact.aspx", "lang560")
        Catch ex As Exception
        End Try
        Try
            lang561.Text = axlabs.GetASPXPage("pmact.aspx", "lang561")
        Catch ex As Exception
        End Try
        Try
            lang562.Text = axlabs.GetASPXPage("pmact.aspx", "lang562")
        Catch ex As Exception
        End Try
        Try
            lang563.Text = axlabs.GetASPXPage("pmact.aspx", "lang563")
        Catch ex As Exception
        End Try
        Try
            lang564.Text = axlabs.GetASPXPage("pmact.aspx", "lang564")
        Catch ex As Exception
        End Try
        Try
            lang565.Text = axlabs.GetASPXPage("pmact.aspx", "lang565")
        Catch ex As Exception
        End Try
        Try
            lang566.Text = axlabs.GetASPXPage("pmact.aspx", "lang566")
        Catch ex As Exception
        End Try

    End Sub

End Class
