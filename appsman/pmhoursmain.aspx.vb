

'********************************************************
'*
'********************************************************



Public Class pmhoursmain
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim typ, nam, dat, wk, qtr, sid, pdm As String
    Protected WithEvents trgraph As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnam As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpdm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnsw As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblqtr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Button1 As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents Button2 As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents ifg As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                typ = Request.QueryString("typ").ToString
            Catch ex As Exception
                'use for testing
                'typ = "Minutes" 'Request.QueryString("typ").ToString
            End Try

            lbltyp.Value = typ
            nam = Request.QueryString("nam").ToString '"2" '
            lblnam.Value = nam
            If nam = "0" Then
                nam = "2"
            End If
            dat = Request.QueryString("dat").ToString '"39545" '
            lbldat.Value = dat
            wk = Request.QueryString("wk").ToString '"15" '
            lblwk.Value = wk
            qtr = Request.QueryString("qtr").ToString '"2" '
            lblqtr.Value = qtr
            sid = Request.QueryString("sid").ToString '"12" '
            lblsid.Value = sid
            pdm = Request.QueryString("pdm").ToString '"0" '
            lblpdm.Value = pdm
            If typ = "Minutes" Then
                btnsw.Attributes.Add("Value", "Switch To Hours")
            ElseIf typ = "Hours" Then
                btnsw.Attributes.Add("Value", "Switch To Minutes")
            End If
            ifg.Attributes.Add("src", "pmhours.aspx?typ=" & typ & "&nam=" & nam & "&dat=" & dat & "&wk=" & wk & "&qtr=" & qtr & "&sid=" & sid & "&pdm=" & pdm)
        End If

    End Sub

End Class
