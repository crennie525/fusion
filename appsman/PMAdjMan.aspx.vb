

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMAdjMan
    Inherits System.Web.UI.Page
	Protected WithEvents ovid65 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents btnpfint As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang582 As System.Web.UI.WebControls.Label

	Protected WithEvents lang581 As System.Web.UI.WebControls.Label

	Protected WithEvents lang580 As System.Web.UI.WebControls.Label

	Protected WithEvents lang579 As System.Web.UI.WebControls.Label

	Protected WithEvents lang578 As System.Web.UI.WebControls.Label

	Protected WithEvents lang577 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, pmtid As String
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpfint As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpfint As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim freq As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            pmtid = Request.QueryString("pmtid").ToString
            lblpmtid.Value = pmtid
            freq.Open()
            GetData(pmtid)
            GetFreq()
            freq.Dispose()

        End If
    End Sub
    Private Sub GetData(ByVal pmtid As String)
        sql = "usp_pmadjdata '" & pmtid & "'"
        Dim eqid, pmtskid, pfreq, tfreq, pfint
        dr = freq.GetRdrData(sql)
        While dr.Read
            eqid = dr.Item("eqid").ToString
            pmtskid = dr.Item("pmtskid").ToString
            pfreq = dr.Item("pfreq").ToString
            tfreq = dr.Item("tfreq").ToString
            pfint = dr.Item("pfint").ToString
        End While
        dr.Close()
        lbleqid.Value = eqid
        lblpmtskid.Value = pmtskid
        lblpfreq.Value = pfreq
        lbltfreq.Value = tfreq
        lblpfint.value = pfint
        txtpfint.Text = pfint
        tdcurr.InnerHtml = pfreq
    End Sub
    Private Sub GetFreq()
        

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang577.Text = axlabs.GetASPXPage("PMAdjMan.aspx", "lang577")
        Catch ex As Exception
        End Try
        Try
            lang578.Text = axlabs.GetASPXPage("PMAdjMan.aspx", "lang578")
        Catch ex As Exception
        End Try
        Try
            lang579.Text = axlabs.GetASPXPage("PMAdjMan.aspx", "lang579")
        Catch ex As Exception
        End Try
        Try
            lang580.Text = axlabs.GetASPXPage("PMAdjMan.aspx", "lang580")
        Catch ex As Exception
        End Try
        Try
            lang581.Text = axlabs.GetASPXPage("PMAdjMan.aspx", "lang581")
        Catch ex As Exception
        End Try
        Try
            lang582.Text = axlabs.GetASPXPage("PMAdjMan.aspx", "lang582")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnpfint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMAdjMan.aspx", "btnpfint") & "')")
            btnpfint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid65.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMAdjMan.aspx", "ovid65") & "', ABOVE, LEFT)")
            ovid65.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
