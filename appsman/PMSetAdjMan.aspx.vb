

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMSetAdjMan
    Inherits System.Web.UI.Page
	Protected WithEvents lang720 As System.Web.UI.WebControls.Label

	Protected WithEvents lang719 As System.Web.UI.WebControls.Label

	Protected WithEvents lang718 As System.Web.UI.WebControls.Label

	Protected WithEvents lang717 As System.Web.UI.WebControls.Label

	Protected WithEvents lang716 As System.Web.UI.WebControls.Label

	Protected WithEvents lang715 As System.Web.UI.WebControls.Label

	Protected WithEvents lang714 As System.Web.UI.WebControls.Label

	Protected WithEvents lang713 As System.Web.UI.WebControls.Label

	Protected WithEvents lang712 As System.Web.UI.WebControls.Label

	Protected WithEvents lang711 As System.Web.UI.WebControls.Label

	Protected WithEvents lang710 As System.Web.UI.WebControls.Label

	Protected WithEvents lang709 As System.Web.UI.WebControls.Label

	Protected WithEvents lang708 As System.Web.UI.WebControls.Label

	Protected WithEvents lang707 As System.Web.UI.WebControls.Label

	Protected WithEvents lang706 As System.Web.UI.WebControls.Label

	Protected WithEvents lang705 As System.Web.UI.WebControls.Label

	Protected WithEvents lang704 As System.Web.UI.WebControls.Label

	Protected WithEvents lang703 As System.Web.UI.WebControls.Label

	Protected WithEvents lang702 As System.Web.UI.WebControls.Label

	Protected WithEvents lang701 As System.Web.UI.WebControls.Label

	Protected WithEvents lang700 As System.Web.UI.WebControls.Label

	Protected WithEvents lang699 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Tables As String = "pmtrack"
    Dim PK As String = "pmtid"
    Dim Sort As String
    Dim dr As SqlDataReader
    Protected WithEvents dgadj As System.Web.UI.WebControls.DataList
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim pmid As String
    Dim sql As String
    Dim adj As New Utilities
    Dim taskhold As String = "0"
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim headhold As String = "0"
    Dim func, funchold, task, pmhid, ro As String
    Protected WithEvents lbloadjc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfadjc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbleq As System.Web.UI.WebControls.Label
    Protected WithEvents dgeqstat As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgeq As System.Web.UI.WebControls.Label
    Protected WithEvents eqstatPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents eqstatNext As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            pmid = Request.QueryString("pmid").ToString
            lblpmid.Value = pmid
            adj.Open()
            LoadAdj()
            adj.Dispose()
        End If
    End Sub
    Private Sub LoadAdj()
        pmid = lblpmid.Value
        sql = "usp_getpmadj '" & pmid & "'"
        Dim ds As New DataSet
        ds = adj.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dgadj.DataSource = dv
        dgadj.DataBind()
    End Sub

    Private Sub dgadj_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dgadj.ItemDataBound
        'funchold = "0"
        Dim trs, trf, trt, trfm, trhd As HtmlTableRow
        Dim fcnt As Integer = 0
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            ro = lblro.Value
            If ro = "1" Then '
                Dim ibn As ImageButton = CType(e.Item.FindControl("Imagebutton1"), ImageButton)
                ibn.ImageUrl = "../images/appbuttons/minibuttons/lilpendis.gif"
                ibn.Enabled = False
            End If
            lbltaskcnt.Value = DataBinder.Eval(e.Item.DataItem, "tcnt").ToString 'e.Item.DataItem("tcnt").ToString
            lbltaskcurrcnt.Value = DataBinder.Eval(e.Item.DataItem, "tccnt").ToString 'e.Item.DataItem("tccnt").ToString
            func = DataBinder.Eval(e.Item.DataItem, "func").ToString 'e.Item.DataItem("func").ToString
            If headhold = "0" Then
                headhold = "1"
                trhd = CType(e.Item.FindControl("trhead"), HtmlTableRow)
                trhd.Attributes.Add("class", "view")
            Else
                trhd = CType(e.Item.FindControl("trhead"), HtmlTableRow)
                trhd.Attributes.Add("class", "details")
            End If
            If func <> funchold Then
                funchold = func
                If taskhold = "0" Then
                    taskhold = "1"
                    trs = CType(e.Item.FindControl("trsep"), HtmlTableRow)
                    trs.Attributes.Add("class", "view")

                Else
                    trs = CType(e.Item.FindControl("trsep"), HtmlTableRow)
                    trs.Attributes.Add("class", "view")

                End If

                trf = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                trf.Attributes.Add("class", "view")
            Else
                trs = CType(e.Item.FindControl("trsep"), HtmlTableRow)
                trs.Attributes.Add("class", "details")
                trf = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                trf.Attributes.Add("class", "details")

            End If
        End If

    End Sub

    Private Sub dgadj_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.EditCommand
        lblrow.Value = CType(e.Item.FindControl("A1"), Label).Text
        lbloadjc.Value = CType(e.Item.FindControl("lblokadjcurr"), Label).Text
        lblfadjc.Value = CType(e.Item.FindControl("lblfadjcurr"), Label).Text
        adj.Open()
        pmid = lblpmid.Value
        dgadj.EditItemIndex = e.Item.ItemIndex
        LoadAdj()
        adj.Dispose()
    End Sub

    Private Sub dgadj_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.CancelCommand
        dgadj.EditItemIndex = -1
        adj.Open()
        pmid = lblpmid.Value
        LoadAdj()
        adj.Dispose()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            iL = CatID
        Else
            iL = -1
        End If
        Return iL
    End Function

    Private Sub dgadj_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.UpdateCommand
        Dim pmtid As Integer
        pmtid = lblrow.Value
        Dim okadj, fadj, okuse, fuse As String
        Dim po As String = lbloadjc.Value
        Dim fo As String = lblfadjc.Value
        okuse = CType(e.Item.FindControl("ddokuse"), DropDownList).SelectedValue.ToString
        If okuse = 0 Then okuse = "2"
        fuse = CType(e.Item.FindControl("ddfuse"), DropDownList).SelectedValue.ToString
        If fuse = 0 Then fuse = "2"
        okadj = CType(e.Item.FindControl("lblokadj"), TextBox).Text.ToString
        Dim okchk As Long
        Try
            okchk = System.Convert.ToInt64(okadj)
            po = System.Convert.ToInt64(po)
            If po <> okchk Then
                po = 1
            Else
                po = 0
            End If
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr347" , "PMSetAdjMan.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        fadj = CType(e.Item.FindControl("lblfadj"), TextBox).Text.ToString
        Dim fchk As Long
        Try
            fchk = System.Convert.ToInt64(okadj)
            fo = System.Convert.ToInt64(fo)
            If fo <> fchk Then
                fo = 1
            Else
                po = 0
            End If
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr348" , "PMSetAdjMan.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "update pmtrack set okadj = '" & okadj & "', okalt = '" & po & "', useadj = '" & okuse & "', " _
        + "fadj = '" & fadj & "', falt = '" & fo & "', usefadj = '" & fuse & "' " _
        + "where pmtid = '" & pmtid & "'"
        dgadj.EditItemIndex = -1
        adj.Open()
        adj.Update(sql)
        LoadAdj()
        adj.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang699.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang699")
        Catch ex As Exception
        End Try
        Try
            lang700.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang700")
        Catch ex As Exception
        End Try
        Try
            lang701.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang701")
        Catch ex As Exception
        End Try
        Try
            lang702.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang702")
        Catch ex As Exception
        End Try
        Try
            lang703.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang703")
        Catch ex As Exception
        End Try
        Try
            lang704.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang704")
        Catch ex As Exception
        End Try
        Try
            lang705.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang705")
        Catch ex As Exception
        End Try
        Try
            lang706.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang706")
        Catch ex As Exception
        End Try
        Try
            lang707.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang707")
        Catch ex As Exception
        End Try
        Try
            lang708.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang708")
        Catch ex As Exception
        End Try
        Try
            lang709.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang709")
        Catch ex As Exception
        End Try
        Try
            lang710.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang710")
        Catch ex As Exception
        End Try
        Try
            lang711.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang711")
        Catch ex As Exception
        End Try
        Try
            lang712.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang712")
        Catch ex As Exception
        End Try
        Try
            lang713.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang713")
        Catch ex As Exception
        End Try
        Try
            lang714.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang714")
        Catch ex As Exception
        End Try
        Try
            lang715.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang715")
        Catch ex As Exception
        End Try
        Try
            lang716.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang716")
        Catch ex As Exception
        End Try
        Try
            lang717.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang717")
        Catch ex As Exception
        End Try
        Try
            lang718.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang718")
        Catch ex As Exception
        End Try
        Try
            lang719.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang719")
        Catch ex As Exception
        End Try
        Try
            lang720.Text = axlabs.GetASPXPage("PMSetAdjMan.aspx", "lang720")
        Catch ex As Exception
        End Try

    End Sub

End Class
