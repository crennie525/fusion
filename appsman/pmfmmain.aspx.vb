

'********************************************************
'*
'********************************************************



Public Class pmfmmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang637 As System.Web.UI.WebControls.Label

	Protected WithEvents lang636 As System.Web.UI.WebControls.Label

	Protected WithEvents lang635 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pmid, ro, wo As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdj As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifwo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents jp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifjp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            pmid = Request.QueryString("pmid").ToString
            wo = Request.QueryString("wo").ToString
            lblpmid.Value = pmid
            lblwo.Value = wo
            ifjp.Attributes.Add("src", "pmfm.aspx?pmid=" & pmid & "&wo=" & wo)
        End If
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang635.Text = axlabs.GetASPXPage("pmfmmain.aspx", "lang635")
        Catch ex As Exception
        End Try
        Try
            lang636.Text = axlabs.GetASPXPage("pmfmmain.aspx", "lang636")
        Catch ex As Exception
        End Try
        Try
            lang637.Text = axlabs.GetASPXPage("pmfmmain.aspx", "lang637")
        Catch ex As Exception
        End Try

    End Sub

End Class
