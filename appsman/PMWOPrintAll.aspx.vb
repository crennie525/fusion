

'********************************************************
'*
'********************************************************


Imports System.Data.SqlClient
Public Class PMWOPrintAll
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Dim sql As String
    Dim man As New Utilities
    Dim pgall As New PMWOALL
    Dim filter, longstring, ref As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

'Put user code to initialize the page here
        If Not IsPostBack Then
            filter = Request.QueryString("filt").ToString
            man.Open()
            sql = "select filt from wofiltref where ref = '" & filter & "'"
            ref = man.strScalar(sql)
            longstring = pgall.GetWOPMReport(ref)
            Response.Write(longstring)
            sql = "delete from wofiltref where ref = '" & filter & "'"
            man.Update(sql)
            man.Dispose()
        End If
    End Sub

End Class
