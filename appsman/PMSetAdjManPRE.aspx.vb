

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMSetAdjManPRE
    Inherits System.Web.UI.Page
	Protected WithEvents lang740 As System.Web.UI.WebControls.Label

	Protected WithEvents lang739 As System.Web.UI.WebControls.Label

	Protected WithEvents lang738 As System.Web.UI.WebControls.Label

	Protected WithEvents lang737 As System.Web.UI.WebControls.Label

	Protected WithEvents lang736 As System.Web.UI.WebControls.Label

	Protected WithEvents lang735 As System.Web.UI.WebControls.Label

	Protected WithEvents lang734 As System.Web.UI.WebControls.Label

	Protected WithEvents lang733 As System.Web.UI.WebControls.Label

	Protected WithEvents lang732 As System.Web.UI.WebControls.Label

	Protected WithEvents lang731 As System.Web.UI.WebControls.Label

	Protected WithEvents lang730 As System.Web.UI.WebControls.Label

	Protected WithEvents lang729 As System.Web.UI.WebControls.Label

	Protected WithEvents lang728 As System.Web.UI.WebControls.Label

	Protected WithEvents lang727 As System.Web.UI.WebControls.Label

	Protected WithEvents lang726 As System.Web.UI.WebControls.Label

	Protected WithEvents lang725 As System.Web.UI.WebControls.Label

	Protected WithEvents lang724 As System.Web.UI.WebControls.Label

	Protected WithEvents lang723 As System.Web.UI.WebControls.Label

	Protected WithEvents lang722 As System.Web.UI.WebControls.Label

	Protected WithEvents lang721 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Tables As String = "pmtrack"
    Dim PK As String = "pmtid"
    Dim Sort As String
    Dim dr As SqlDataReader   
    Dim pmid As String
    Dim sql As String
    Dim adj As New Utilities
    Dim taskhold As String = "0"
    Dim headhold As String = "0"
    Dim func, funchold, task, pmhid, pmtskid, tid, ro, typ, rev As String
    Protected WithEvents lbloadjc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfadjc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgadj As System.Web.UI.WebControls.DataList
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then

            End If
            tid = Request.QueryString("tid").ToString
            pmid = Request.QueryString("pmid").ToString
            lbltid.Value = tid
            lblpmid.Value = pmid
            rev = Request.QueryString("rev").ToString
            typ = Request.QueryString("typ").ToString
            lblrev.Value = rev
            lbltyp.Value = typ
            adj.Open()
            LoadAdj(tid)
            adj.Dispose()
        End If
    End Sub
    Private Sub LoadAdj(ByVal tid As String)
        pmid = lblpmid.Value
        rev = lblrev.Value
        typ = lbltyp.Value
        If typ = "rev" Then
            sql = "usp_getpmadjPREarch '" & pmid & "', '" & tid & "','" & rev & "'"
        ElseIf typ = "trev" Then
            sql = "usp_getpmadjPREarchtpm '" & pmid & "', '" & tid & "','" & rev & "'"
        Else
            sql = "usp_getpmadjPRE '" & pmid & "', '" & tid & "'"
        End If

        Dim ds As New DataSet
        ds = adj.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dgadj.DataSource = dv
        dgadj.DataBind()
    End Sub

    Private Sub dgadj_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.EditCommand
        lblrow.Value = CType(e.Item.FindControl("A1"), Label).Text '
        lbloadjc.Value = CType(e.Item.FindControl("lblokadjcurr"), Label).Text
        lblfadjc.Value = CType(e.Item.FindControl("lblfadjcurr"), Label).Text
        tid = lbltid.Value
        adj.Open()
        pmid = lblpmid.Value
        dgadj.EditItemIndex = e.Item.ItemIndex
        LoadAdj(tid)
        adj.Dispose()
    End Sub

    Private Sub dgadj_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.CancelCommand
        dgadj.EditItemIndex = -1
        tid = lbltid.Value
        adj.Open()
        pmid = lblpmid.Value
        LoadAdj(tid)
        adj.Dispose()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            iL = CatID
        Else
            iL = -1
        End If
        Return iL
    End Function

    Private Sub dgadj_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dgadj.UpdateCommand
        Dim pmtid As Integer
        pmtid = lblrow.Value
        Dim okadj, fadj, okuse, fuse As String
        Dim po As String = lbloadjc.Value
        Dim fo As String = lblfadjc.Value
        okuse = CType(e.Item.FindControl("ddokuse"), DropDownList).SelectedValue.ToString
        If okuse = 0 Then okuse = "2"
        fuse = CType(e.Item.FindControl("ddfuse"), DropDownList).SelectedValue.ToString
        If fuse = 0 Then fuse = "2"
        okadj = CType(e.Item.FindControl("lblokadj"), TextBox).Text.ToString
        Dim okchk As Long
        Try
            okchk = System.Convert.ToInt64(okadj)
            po = System.Convert.ToInt64(po)
            If po <> okchk Then
                po = 1
            Else
                po = 0
            End If
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr349" , "PMSetAdjManPRE.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        fadj = CType(e.Item.FindControl("lblfadj"), TextBox).Text.ToString
        Dim fchk As Long
        Try
            fchk = System.Convert.ToInt64(okadj)
            fo = System.Convert.ToInt64(fo)
            If fo <> fchk Then
                fo = 1
            Else
                po = 0
            End If
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr350" , "PMSetAdjManPRE.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "update pmtasks set okadj = '" & okadj & "', okalt = '" & po & "', useadj = '" & okuse & "', " _
        + "fadj = '" & fadj & "', falt = '" & fo & "', usefadj = '" & fuse & "' " _
        + "where pmtskid = '" & pmtid & "'"
        dgadj.EditItemIndex = -1
        adj.Open()
        adj.Update(sql)
        adj.UpModTask(pmtid)
        pmid = lblpmid.Value
        tid = lbltid.Value
        LoadAdj(tid)
        adj.Dispose()
    End Sub

    Private Sub dgadj_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dgadj.ItemDataBound

        Dim trhd, trd As HtmlTableRow
        Dim fcnt As Integer = 0
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            ro = lblro.Value
            If ro = "1" Then '
                Dim ibn As ImageButton = CType(e.Item.FindControl("Imagebutton1"), ImageButton)
                ibn.ImageUrl = "../images/appbuttons/minibuttons/lilpendis.gif"
                ibn.Enabled = False
            End If
            If headhold = "0" Then
                headhold = "1"
                trhd = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                trhd.Attributes.Add("class", "view")
                trd = CType(e.Item.FindControl("trdiv"), HtmlTableRow)
                trd.Attributes.Add("class", "view")
            Else
                trhd = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                trhd.Attributes.Add("class", "details")
                trd = CType(e.Item.FindControl("trdiv"), HtmlTableRow)
                trd.Attributes.Add("class", "details")
            End If
        ElseIf e.Item.ItemType = ListItemType.EditItem Then
            If headhold = "0" Then
                headhold = "1"
                trhd = CType(e.Item.FindControl("trfunce"), HtmlTableRow)
                trhd.Attributes.Add("class", "view")
                trd = CType(e.Item.FindControl("trdive"), HtmlTableRow)
                trd.Attributes.Add("class", "view")
            Else
                trhd = CType(e.Item.FindControl("trfunce"), HtmlTableRow)
                trhd.Attributes.Add("class", "details")
                trd = CType(e.Item.FindControl("trdive"), HtmlTableRow)
                trd.Attributes.Add("class", "details")
            End If
        End If

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang721.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang721")
        Catch ex As Exception
        End Try
        Try
            lang722.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang722")
        Catch ex As Exception
        End Try
        Try
            lang723.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang723")
        Catch ex As Exception
        End Try
        Try
            lang724.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang724")
        Catch ex As Exception
        End Try
        Try
            lang725.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang725")
        Catch ex As Exception
        End Try
        Try
            lang726.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang726")
        Catch ex As Exception
        End Try
        Try
            lang727.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang727")
        Catch ex As Exception
        End Try
        Try
            lang728.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang728")
        Catch ex As Exception
        End Try
        Try
            lang729.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang729")
        Catch ex As Exception
        End Try
        Try
            lang730.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang730")
        Catch ex As Exception
        End Try
        Try
            lang731.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang731")
        Catch ex As Exception
        End Try
        Try
            lang732.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang732")
        Catch ex As Exception
        End Try
        Try
            lang733.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang733")
        Catch ex As Exception
        End Try
        Try
            lang734.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang734")
        Catch ex As Exception
        End Try
        Try
            lang735.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang735")
        Catch ex As Exception
        End Try
        Try
            lang736.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang736")
        Catch ex As Exception
        End Try
        Try
            lang737.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang737")
        Catch ex As Exception
        End Try
        Try
            lang738.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang738")
        Catch ex As Exception
        End Try
        Try
            lang739.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang739")
        Catch ex As Exception
        End Try
        Try
            lang740.Text = axlabs.GetASPXPage("PMSetAdjManPRE.aspx", "lang740")
        Catch ex As Exception
        End Try

    End Sub

End Class
