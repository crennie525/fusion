

'********************************************************
'*
'********************************************************



Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Data.SqlClient
Public Class pmhours
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim aMonths As ArrayList = New ArrayList, aTime As ArrayList = New ArrayList
    Dim aMin As ArrayList = New ArrayList, aDate As ArrayList = New ArrayList
    Protected WithEvents tdgr As System.Web.UI.HtmlControls.HtmlTableCell
    Dim gra As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim nam, dat, wk, qtr, sid, pdm, typ As String
    Protected WithEvents lblhdec As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmdec As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            nam = Request.QueryString("nam").ToString
            dat = Request.QueryString("dat").ToString
            wk = Request.QueryString("wk").ToString
            qtr = Request.QueryString("qtr").ToString
            sid = Request.QueryString("sid").ToString
            pdm = Request.QueryString("pdm").ToString
            FillaMonth()
            gra.Open()
            FillaTime(nam, dat, wk, qtr, sid)
            nam = GetNam(nam, pdm)
            gra.Dispose()
            If typ = "Hours" Then
                DrawBarGraph(nam & " Hours", aMonths, aTime, aDate)
            Else
                DrawBarGraph(nam & " Minutes", aMonths, aMin, aDate)
            End If

        End If


    End Sub
    Private Function GetNam(ByVal nam As String, ByVal pdm As String) As String
        If pdm = "0" Then
            sql = "select skill from pmskills where skillid = '" & nam & "'"
        Else
            sql = "select pretech as 'skill' from pmpretech where ptid = '" & nam & "'"
        End If

        dr = gra.GetRdrData(sql)
        While dr.Read
            nam = dr.Item("skill").ToString
        End While
        dr.Close()
        Return nam
    End Function
    Private Sub FillaTime(ByVal nam As String, ByVal dat As String, ByVal wk As String, ByVal qtr As String, ByVal sid As String)
        sql = "usp_gethours4 '0', '" & qtr & "', '" & sid & "', '" & nam & "', '%', '0', '0', '0', '0', '" & wk & "', '" & dat & "'"
        Dim tim As String
        Dim fflg As Integer = 0
        Dim hdec, mdec As Decimal
        dr = gra.GetRdrData(sql)
        While dr.Read
            fflg = 1
            tim = dr.Item("ttimeh").ToString
            Try
                hdec += CType(tim, Decimal)
            Catch ex As Exception

            End Try

            aTime.Add(dr.Item("ttimeh").ToString)
            tim = dr.Item("ttime").ToString
            Try
                mdec += CType(tim, Decimal)
            Catch ex As Exception

            End Try

            aMin.Add(dr.Item("ttime").ToString)
            tim = dr.Item("tdate").ToString
            aDate.Add(dr.Item("tdate").ToString)
        End While
        dr.Close()
        lblhdec.Value = hdec
        lblmdec.Value = mdec
        If fflg = 0 Then
            aTime.Add(0)
            aTime.Add(0)
            aTime.Add(0)
            aTime.Add(0)
            aTime.Add(0)
            aMin.Add(0)
            aMin.Add(0)
            aMin.Add(0)
            aMin.Add(0)
            aMin.Add(0)
        End If

        'aTime.Add(8)
        'aTime.Add(4)
    End Sub
    Private Sub FillaMonth()
        aMonths.Add("Mon")
        aMonths.Add("Tue")
        aMonths.Add("Wed")
        aMonths.Add("Thu")
        aMonths.Add("Fri")
        aMonths.Add("Sat")
        aMonths.Add("Sun")
    End Sub

    Sub DrawBarGraph(ByVal strTitle As String, ByVal aX As ArrayList, ByVal aY As ArrayList, ByVal aD As ArrayList)
        Const iColWidth As Integer = 30, iColSpace As Integer = 16, _
        iMaxHeight As Integer = 140, iHeightSpace As Integer = 40, _
         iXLegendSpace As Integer = 20, iTitleSpace As Integer = 30
        Dim iMaxWidth As Integer = (iColWidth + iColSpace) * aX.Count + iColSpace, iMaxColHeight As Integer = 0, iTotalHeight As Integer = iMaxHeight + iXLegendSpace + iTitleSpace

        Dim objBitmap As Bitmap = New Bitmap(iMaxWidth, iTotalHeight)
        Dim objGraphics As Graphics = Graphics.FromImage(objBitmap)

        objGraphics.FillRectangle(New SolidBrush(System.Drawing.Color.White), 0, 0, iMaxWidth, iTotalHeight)
        objGraphics.FillRectangle(New SolidBrush(System.Drawing.Color.Ivory), 0, 0, iMaxWidth, iMaxHeight)

        Dim wk As String = aD(0).ToString
        strTitle = strTitle & " for Week of " & wk
        Dim strLegend As String
        strLegend = "Total Minutes:  " & lblmdec.Value & "  Total Hours:  " & lblhdec.Value
        'find the maximum value
        Dim iValue As Integer
        Try
            For Each iValue In aY
                'iValue = iValue * 10
                Try
                    If iValue > iMaxColHeight Then iMaxColHeight = iValue
                Catch ex As Exception
                    iMaxColHeight = 0
                End Try


            Next
        Catch ex As Exception

        End Try
       
        If iMaxColHeight > iMaxHeight Then
            iMaxColHeight = iMaxHeight
        End If
        Dim iBarX As Integer = iColSpace, iCurrentHeight As Integer
        Dim objBrush As SolidBrush = New SolidBrush(System.Drawing.Color.FromArgb(26, 3, 253))
        Dim objTitleBrush As SolidBrush = New SolidBrush(System.Drawing.Color.FromArgb(0, 0, 0))
        Dim fontLegend As Font = New Font("Arial", 8, FontStyle.Regular), fontValues As Font = New Font("Arial", 8), fontTitle As Font = New Font("Arial", 10, FontStyle.Bold)

        'dim fontw as Font = new Font(

        ' loop through and draw each bar
        Dim iLoop As Integer
        Dim dLoop As Decimal
        For iLoop = 0 To aX.Count - 1
            Try
                iCurrentHeight = ((Convert.ToDouble(aY(iLoop)) / Convert.ToDouble(iMaxColHeight)) * Convert.ToDouble(iMaxHeight - iHeightSpace))
            Catch ex As Exception
                iCurrentHeight = 1
            End Try

            objGraphics.FillRectangle(objBrush, iBarX, _
            iMaxHeight - iCurrentHeight, iColWidth, iCurrentHeight)
            objGraphics.DrawString(aX(iLoop), fontLegend, objBrush, iBarX, iMaxHeight)
            Try
                dLoop = aY(iLoop) 'aY(iLoop)
            Catch ex As Exception
                dLoop = 0
            End Try


            objGraphics.DrawString(Format(dLoop, "#,###.##"), fontValues, objTitleBrush, iBarX, iMaxHeight - iCurrentHeight - 15)
            iBarX += (iColSpace + iColWidth)
        Next
        objGraphics.DrawString(strLegend, fontLegend, objTitleBrush, (iMaxWidth / 2) - strLegend.Length * 2.5, iMaxHeight + iXLegendSpace)
        objGraphics.DrawString(strTitle, fontTitle, objTitleBrush, (iMaxWidth / 2) - strTitle.Length * 3.5, 0)
        'objBitmap.Save("C:\inetpub\wwwroot\graph.gif", ImageFormat.GIF)
        objBitmap.Save(Response.OutputStream, ImageFormat.Gif)
        objGraphics.Dispose()
        objBitmap.Dispose()
    End Sub

End Class
