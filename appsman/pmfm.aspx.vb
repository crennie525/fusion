

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmfm
    Inherits System.Web.UI.Page
	Protected WithEvents lang634 As System.Web.UI.WebControls.Label

	Protected WithEvents lang633 As System.Web.UI.WebControls.Label

	Protected WithEvents lang632 As System.Web.UI.WebControls.Label

	Protected WithEvents lang631 As System.Web.UI.WebControls.Label

	Protected WithEvents lang630 As System.Web.UI.WebControls.Label

	Protected WithEvents lang629 As System.Web.UI.WebControls.Label

	Protected WithEvents lang628 As System.Web.UI.WebControls.Label

	Protected WithEvents lang627 As System.Web.UI.WebControls.Label

	Protected WithEvents lang626 As System.Web.UI.WebControls.Label

	Protected WithEvents lang625 As System.Web.UI.WebControls.Label

	Protected WithEvents lang624 As System.Web.UI.WebControls.Label

	Protected WithEvents lang623 As System.Web.UI.WebControls.Label

	Protected WithEvents lang622 As System.Web.UI.WebControls.Label

	Protected WithEvents lang621 As System.Web.UI.WebControls.Label

	Protected WithEvents lang620 As System.Web.UI.WebControls.Label

	Protected WithEvents lang619 As System.Web.UI.WebControls.Label

	Protected WithEvents lang618 As System.Web.UI.WebControls.Label

	Protected WithEvents lang617 As System.Web.UI.WebControls.Label

	Protected WithEvents lang616 As System.Web.UI.WebControls.Label

	Protected WithEvents lang615 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid As String
    Dim ds As DataSet
    Dim func, funchold, task, pmhid, co, cohold, ro, Login, wonum As String
    Dim taskhold As String = "0"
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblokcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblokadj As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluseadj As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfadj As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusefadj As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblalert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ealert As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwod As System.Web.UI.HtmlControls.HtmlTableCell
    Dim headhold As String = "0"

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dltasks As System.Web.UI.WebControls.DataList
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbltaskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfmcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            pmid = Request.QueryString("pmid").ToString '"139" '
            wonum = Request.QueryString("wo").ToString '"139" '
            'pmhid = "501" 'Request.QueryString("pmhid").ToString
            lblpmid.Value = pmid
            lblpmhid.Value = pmhid
            lblwo.Value = wonum
            lblmflg.Value = "no"
            lblmcomp.Value = "incomp"
            comp.Open()
            GetWoHead(wonum)
            PopDL(pmid)
            comp.Dispose()
        Else
            If Request.Form("lblsubmit") = "comp" Then
                lblsubmit.Value = ""
                comp.Open()
                'CompPM()
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "txts" Then
                lblsubmit.Value = ""
                comp.Open()
                'UpStart()
                'PopDates(pmhid)
                'PopDL(pmid)
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "txtf" Then
                lblsubmit.Value = ""
                comp.Open()
                'UpFin()
                'PopDates(pmhid)
                'PopDL(pmid)
                comp.Dispose()
            End If
        End If
    End Sub
    Private Sub GetWoHead(ByVal wonum As String)
        wonum = lblwo.Value
        sql = "select w.wonum, w.description, w.jpid, w.status, j.jpnum, j.description as jdesc, w.siteid " _
        + "from workorder w left join wojobplans j on j.jpid = w.jpid  where w.wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdwo.InnerHtml = dr.Item("wonum").ToString
            tdwod.InnerHtml = dr.Item("description").ToString
            lblsid.Value = dr.Item("siteid").ToString
            'lbljpid.Value = dr.Item("jpid").ToString
            'jpid = dr.Item("jpid").ToString
            'stat = dr.Item("status").ToString
            'tdjp.InnerHtml = dr.Item("jpnum").ToString
            'tdjpd.InnerHtml = dr.Item("jdesc").ToString
        End While
        dr.Close()
        'lbljpid.Value = jpid
        'lblstat.Value = stat
    End Sub
    Private Sub PopDL(ByVal pmid As String)
        
        sql = "usp_getpmtrack2 '" & pmid & "'"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dltasks.DataSource = dv
        dltasks.DataBind()
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If CatID <> "Select" And CatID <> "N/A" Then 'Not IsDBNull(CatID) OrElse  
            If CatID = "PASS" Then
                iL = 0
            ElseIf CatID = "FAIL" Then
                iL = 1
            ElseIf CatID = "YES" Then
                iL = 0
            ElseIf CatID = "NO" Then
                iL = 1
            End If
        Else
            iL = -1
        End If
        Return iL
    End Function

    Private Sub dltasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.EditCommand
        lblrow.Value = CType(e.Item.FindControl("A1"), Label).Text 'e.Item.ItemIndex
        'lblfmcnt.Value = CType(e.Item.FindControl("A2"), Label).Text 'e.Item.ItemIndex
        lbltasknum.Value = CType(e.Item.FindControl("lbltasknume"), Label).Text 'e.Item.ItemIndex
        comp.Open()
        pmid = lblpmid.Value
        dltasks.EditItemIndex = e.Item.ItemIndex

        PopDL(pmid)
        comp.Dispose()
    End Sub

    Private Sub dltasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dltasks.ItemDataBound
        Dim trs, trf, trt, trfm, trhd As HtmlTableRow

        Dim fm1, fm2, fm3, fm4, fm5, pm, wo As String
        Dim rb1, rb2, rb3, rb4, rb5 As DropDownList
        Dim ibfm As ImageButton
        Dim rb1tc As RadioButtonList
        Dim rb1tci As String
        Dim rb1i, rb2i, rb3i, rb4i, rb5i As String
        Dim pmtid, pmfid, fm, fmid, comid As String
        Dim fcnt As Integer = 0
        pmid = lblpmid.Value
        pmhid = lblpmhid.Value
        wo = lblwo.Value
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            ro = lblro.Value
            If ro = "1" Or wo = "" Then '
                Dim ibn As ImageButton = CType(e.Item.FindControl("Imagebutton1"), ImageButton)
                ibn.ImageUrl = "../images/appbuttons/minibuttons/lilpendis.gif"
                ibn.Enabled = False
            End If
            pm = DataBinder.Eval(e.Item.DataItem, "pm").ToString 'e.Item.DataItem("pm").ToString
            'If headhold = "0" Then
            'headhold = "1"
            'tdjpn.InnerHtml = pm
            'End If
            comid = DataBinder.Eval(e.Item.DataItem, "comid").ToString 'e.Item.DataItem("comid").ToString
            func = DataBinder.Eval(e.Item.DataItem, "func").ToString 'e.Item.DataItem("func").ToString
            co = DataBinder.Eval(e.Item.DataItem, "compnum").ToString 'e.Item.DataItem("compnum").ToString
            If func <> funchold Then
                funchold = func
                If taskhold = "0" Then
                    taskhold = "1"
                    trs = CType(e.Item.FindControl("trsep"), HtmlTableRow)
                    trs.Attributes.Add("class", "view")

                Else
                    trs = CType(e.Item.FindControl("trsep"), HtmlTableRow)
                    trs.Attributes.Add("class", "view")

                End If

                trf = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                trf.Attributes.Add("class", "view")
            Else
                trs = CType(e.Item.FindControl("trsep"), HtmlTableRow)
                trs.Attributes.Add("class", "details")
                trf = CType(e.Item.FindControl("trfunc"), HtmlTableRow)
                trf.Attributes.Add("class", "details")

            End If
            If co <> cohold Then
                cohold = co
                trf = CType(e.Item.FindControl("trco"), HtmlTableRow)
                trf.Attributes.Add("class", "view")
            Else
                trf = CType(e.Item.FindControl("trco"), HtmlTableRow)
                trf.Attributes.Add("class", "details")

            End If
            Dim img As HtmlImage = CType(e.Item.FindControl("imgti"), HtmlImage)
            Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tdtsk.InnerHtml = tsk
            'tsk = tsk.Replace("'", "")
            'tsk = comp.ModString1(tsk)
            'tsk = tsk.Replace("(", "-")
            'tsk = tsk.Replace(")", " ")
            task = DataBinder.Eval(e.Item.DataItem, "tasknum").ToString
            img.Attributes.Add("onclick", "gettsk('" & pmid & "','" & pmtskid & "');")
        End If

        If e.Item.ItemType = ListItemType.EditItem Then
            task = DataBinder.Eval(e.Item.DataItem, "tasknum").ToString
            pmtid = DataBinder.Eval(e.Item.DataItem, "pmtid").ToString
            comid = DataBinder.Eval(e.Item.DataItem, "comid").ToString
            func = DataBinder.Eval(e.Item.DataItem, "func").ToString 'e.Item.DataItem("func").ToString
            co = DataBinder.Eval(e.Item.DataItem, "compnum").ToString 'e.Item.DataItem("compnum").ToString
            If func <> funchold Then
                funchold = func
                If taskhold = "0" Then
                    taskhold = "1"
                    trs = CType(e.Item.FindControl("trsepe"), HtmlTableRow)
                    trs.Attributes.Add("class", "view")

                Else
                    trs = CType(e.Item.FindControl("trsepe"), HtmlTableRow)
                    trs.Attributes.Add("class", "view")

                End If

                trf = CType(e.Item.FindControl("trfunce"), HtmlTableRow)
                trf.Attributes.Add("class", "view")
            Else
                trs = CType(e.Item.FindControl("trsepe"), HtmlTableRow)
                trs.Attributes.Add("class", "details")
                trf = CType(e.Item.FindControl("trfunce"), HtmlTableRow)
                trf.Attributes.Add("class", "details")

            End If
            If co <> cohold Then
                cohold = co
                trf = CType(e.Item.FindControl("trcoe"), HtmlTableRow)
                trf.Attributes.Add("class", "view")
            Else
                trf = CType(e.Item.FindControl("trcoe"), HtmlTableRow)
                trf.Attributes.Add("class", "details")

            End If
            Dim img As HtmlImage = CType(e.Item.FindControl("imgte"), HtmlImage)
            'Dim pmtskid As String = DataBinder.Eval(e.Item.DataItem, "pmtskid").ToString
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            'tdtsk.InnerHtml = tsk
            tsk = tsk.Replace("'", "")
            tsk = comp.ModString1(tsk)
            tsk = tsk.Replace("(", "-")
            tsk = tsk.Replace(")", " ")

            img.Attributes.Add("onclick", "gettsk('" & pmid & "','" & task & "');")

            fcnt = 0
            Dim tmp As String = pmtid
            fm1 = DataBinder.Eval(e.Item.DataItem, "fm1id").ToString 'e.Item.DataItem("fm1id").ToString
            fm = DataBinder.Eval(e.Item.DataItem, "fm1s").ToString 'e.Item.DataItem("fm1s").ToString
            fm = fm.Replace("'", "")
            fm = comp.ModString1(fm)
            If fm1 = "" And fm <> "" Then
                fm1 = fixfm1s(fm, pmtid, "one")
            End If
            Try
                If fm1 = "0" Or fm1 = "" Then
                    rb1 = CType(e.Item.FindControl("ddfm1"), DropDownList)
                    rb1.Enabled = False
                Else
                    If DataBinder.Eval(e.Item.DataItem, "fm1dd").ToString <> "Select" Then
                        fcnt = fcnt + 1
                    End If
                    rb1 = CType(e.Item.FindControl("ddfm1"), DropDownList)
                    rb1i = rb1.ClientID.ToString
                    
                    fmid = fm1 'DataBinder.Eval(e.Item.DataItem, "fm1id").ToString 'e.Item.DataItem("fm1id").ToString
                    'Val(, pmid, pmhid, pmtid, pmfid, fm, task)
                    rb1.Attributes.Add("onchange", "checkfail('" & rb1i & "', '" & pmid & "', '" & pmhid & "', '" & pmtid & "', '" & pmfid & "', '" & fm & "', '" & fmid & "', '" & task & "','" & comid & "');")
                    fm = ""
                End If
            Catch ex As Exception
                fm = ""
                rb1 = CType(e.Item.FindControl("ddfm1"), DropDownList)
                rb1.Enabled = False
            End Try
            
            fm2 = DataBinder.Eval(e.Item.DataItem, "fm2id").ToString 'DataBinder.Eval(e.Item.DataItem("fm2id")).ToString
            fm = DataBinder.Eval(e.Item.DataItem, "fm2s").ToString 'e.Item.DataItem("fm2s").ToString
            fm = fm.Replace("'", "")
            fm = comp.ModString1(fm)
            If fm2 = "" And fm <> "" Then
                fm2 = fixfm1s(fm, pmtid, "two")
            End If
            Try
                If fm2 = "0" Or fm2 = "" Then
                    rb2 = CType(e.Item.FindControl("ddfm2"), DropDownList)
                    rb2.Enabled = False
                Else
                    If DataBinder.Eval(e.Item.DataItem, "fm2dd").ToString <> "Select" Then
                        fcnt = fcnt + 1
                    End If
                    rb2 = CType(e.Item.FindControl("ddfm2"), DropDownList)
                    rb2i = rb2.ClientID.ToString
                    
                    fmid = fm2 'DataBinder.Eval(e.Item.DataItem, "fm2id").ToString 'e.Item.DataItem("fm2id").ToString
                    'Val(, pmid, pmhid, pmtid, pmfid, fm, task)
                    rb2.Attributes.Add("onchange", "checkfail('" & rb2i & "', '" & pmid & "', '" & pmhid & "', '" & pmtid & "', '" & pmfid & "', '" & fm & "', '" & fmid & "', '" & task & "','" & comid & "');")
                    fm = ""
                End If
            Catch ex As Exception
                fm = ""
                rb2 = CType(e.Item.FindControl("ddfm2"), DropDownList)
                rb2.Enabled = False
            End Try
            
            fm3 = DataBinder.Eval(e.Item.DataItem, "fm3id").ToString 'e.Item.DataItem("fm3id").ToString
            fm = DataBinder.Eval(e.Item.DataItem, "fm3s").ToString 'e.Item.DataItem("fm3s").ToString
            fm = fm.Replace("'", "")
            fm = comp.ModString1(fm)
            If fm3 = "" And fm <> "" Then
                fm3 = fixfm1s(fm, pmtid, "three")
            End If
            Try
                If fm3 = "0" Or fm3 = "" Then
                    rb3 = CType(e.Item.FindControl("ddfm3"), DropDownList)
                    rb3.Enabled = False
                Else
                    If DataBinder.Eval(e.Item.DataItem, "fm3dd").ToString <> "Select" Then
                        fcnt = fcnt + 1
                    End If
                    rb3 = CType(e.Item.FindControl("ddfm3"), DropDownList)
                    rb3i = rb3.ClientID.ToString
                    
                    fmid = fm3 'DataBinder.Eval(e.Item.DataItem, "fm3id").ToString '.Item.DataItem("fm3id").ToString
                    'Val(, pmid, pmhid, pmtid, pmfid, fm, task)
                    rb3.Attributes.Add("onchange", "checkfail('" & rb3i & "', '" & pmid & "', '" & pmhid & "', '" & pmtid & "', '" & pmfid & "', '" & fm & "', '" & fmid & "', '" & task & "','" & comid & "');")
                    fm = ""
                End If
            Catch ex As Exception
                fm = ""
                rb3 = CType(e.Item.FindControl("ddfm3"), DropDownList)
                rb3.Enabled = False
            End Try
            
            fm4 = DataBinder.Eval(e.Item.DataItem, "fm4id").ToString 'e.Item.DataItem("fm4id").ToString
            fm = DataBinder.Eval(e.Item.DataItem, "fm4s").ToString 'e.Item.DataItem("fm4s").ToString
            fm = fm.Replace("'", "")
            fm = comp.ModString1(fm)
            If fm4 = "" And fm <> "" Then
                fm4 = fixfm1s(fm, pmtid, "four")
            End If
            Try
                If fm4 = "0" Or fm4 = "" Then
                    rb4 = CType(e.Item.FindControl("ddfm4"), DropDownList)
                    rb4.Enabled = False
                Else
                    If DataBinder.Eval(e.Item.DataItem, "fm4dd").ToString <> "Select" Then
                        fcnt = fcnt + 1
                    End If
                    rb4 = CType(e.Item.FindControl("ddfm4"), DropDownList)
                    rb4i = rb4.ClientID.ToString
                   
                    fmid = fm4 'DataBinder.Eval(e.Item.DataItem, "fm4id").ToString 'e.Item.DataItem("fm4id").ToString
                    'Val(, pmid, pmhid, pmtid, pmfid, fm, task)
                    rb4.Attributes.Add("onchange", "checkfail('" & rb4i & "', '" & pmid & "', '" & pmhid & "', '" & pmtid & "', '" & pmfid & "', '" & fm & "', '" & fmid & "', '" & task & "','" & comid & "');")
                    fm = ""
                End If
            Catch ex As Exception
                fm = ""
                rb4 = CType(e.Item.FindControl("ddfm4"), DropDownList)
                rb4.Enabled = False
            End Try
            
            fm5 = DataBinder.Eval(e.Item.DataItem, "fm5id").ToString 'e.Item.DataItem("fm5id").ToString
            fm = DataBinder.Eval(e.Item.DataItem, "fm5s").ToString 'e.Item.DataItem("fm5s").ToString
            fm = fm.Replace("'", "")
            fm = comp.ModString1(fm)
            If fm5 = "" And fm <> "" Then
                fm5 = fixfm1s(fm, pmtid, "five")
            End If
            Try
                If fm5 = "0" Or fm5 = "" Then
                    rb5 = CType(e.Item.FindControl("ddfm5"), DropDownList)
                    rb5.Enabled = False
                Else
                    If DataBinder.Eval(e.Item.DataItem, "fm5dd").ToString <> "Select" Then
                        fcnt = fcnt + 1
                    End If
                    rb5 = CType(e.Item.FindControl("ddfm5"), DropDownList)
                    rb5i = rb5.ClientID.ToString
                    
                    fmid = fm5 'DataBinder.Eval(e.Item.DataItem, "fm5id").ToString 'e.Item.DataItem("fm5id").ToString
                    'Val(, pmid, pmhid, pmtid, pmfid, fm, task)
                    rb5.Attributes.Add("onchange", "checkfail('" & rb5i & "', '" & pmid & "', '" & pmhid & "', '" & pmtid & "', '" & pmfid & "', '" & fm & "', '" & fmid & "', '" & task & "','" & comid & "');")
                    fm = ""
                End If
            Catch ex As Exception
                fm = ""
                rb5 = CType(e.Item.FindControl("ddfm5"), DropDownList)
                rb5.Enabled = False
            End Try
            

        End If
    End Sub
    Private Function fixfm1s(ByVal fm1s As String, ByVal pmtid As String, ByVal who As String) As String
        Dim ret As String
        sql = "select top 1 failid from FailureModes where failuremode = '" & fm1s & "'"
        Try
            ret = comp.strScalar(sql)
            If ret <> "" Then
                Select Case who
                    Case "one"
                        sql = "update pmtrack set fm1id = '" & ret & "' where pmtid = '" & pmtid & "'"
                    Case "two"
                        sql = "update pmtrack set fm2id = '" & ret & "' where pmtid = '" & pmtid & "'"
                    Case "three"
                        sql = "update pmtrack set fm3id = '" & ret & "' where pmtid = '" & pmtid & "'"
                    Case "four"
                        sql = "update pmtrack set fm4id = '" & ret & "' where pmtid = '" & pmtid & "'"
                    Case "five"
                        sql = "update pmtrack set fm5id = '" & ret & "' where pmtid = '" & pmtid & "'"
                End Select
                comp.Update(sql)
            End If
        Catch ex As Exception
            ret = ""
        End Try
        Return ret
    End Function

    Private Sub dltasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.UpdateCommand
        Dim ret, typ, fail, pass, task As String
        Dim tsk, cnt, adj As String
        Dim fcnt As String = lblfmcnt.Value
        Dim fccnt As String = lblcurrcnt.Value
        Dim tasknum As String = lbltasknum.Value
        Dim pmtid As String = lblrow.Value
        Dim rb1, rb2, rb3, rb4, rb5, rbc As DropDownList
        Dim sel1, sel2, sel3, sel4, sel5, selc As String
        rb1 = CType(e.Item.FindControl("ddfm1"), DropDownList)
        sel1 = rb1.SelectedValue.ToString
        If sel1 = "" Then sel1 = "0"
        rb2 = CType(e.Item.FindControl("ddfm2"), DropDownList)
        sel2 = rb2.SelectedValue.ToString
        If sel2 = "" Then sel2 = "0"
        rb3 = CType(e.Item.FindControl("ddfm3"), DropDownList)
        sel3 = rb3.SelectedValue.ToString
        If sel3 = "" Then sel3 = "0"
        rb4 = CType(e.Item.FindControl("ddfm4"), DropDownList)
        sel4 = rb4.SelectedValue.ToString
        If sel4 = "" Then sel4 = "0"
        rb5 = CType(e.Item.FindControl("ddfm5"), DropDownList)
        sel5 = rb5.SelectedValue.ToString
        selc = "1"

        sql = "usp_uppmtsk '" & sel1 & "', '" & sel2 & "', '" & sel3 & "', '" & sel4 & "', '" & sel5 & "', '" & selc & "', '" & pmtid & "', '" & fcnt & "', '" & fccnt & "', '" & tasknum & "'"

        comp.Open()
        dr = comp.GetRdrData(sql)
        While dr.Read
            ret = dr.Item("ret").ToString
            If ret <> 0 Then
                tsk = dr.Item("task").ToString
                lbltasknum.Value = tsk
                cnt = dr.Item("cnt").ToString
                adj = dr.Item("adj").ToString
            End If
        End While
        dr.Close()

        If ret = "1" Then
            lblrow.Value = pmtid
            lblalert.Value = "1"
            lblokcnt.Value = cnt
            lblokadj.Value = adj
        ElseIf ret = "2" Then
            lblrow.Value = pmtid
            lblalert.Value = "2"
            lblfcnt.Value = cnt
            lblfadj.Value = adj
        Else
            lblalert.Value = "0"
        End If
        dltasks.EditItemIndex = -1
        pmid = lblpmid.Value
        PopDL(pmid)
        comp.Dispose()
    End Sub

    Private Sub dltasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dltasks.CancelCommand
        dltasks.EditItemIndex = -1
        comp.Open()
        pmid = lblpmid.Value
        PopDL(pmid)
        comp.Dispose()
    End Sub
    









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("pmfm.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang615.Text = axlabs.GetASPXPage("pmfm.aspx", "lang615")
        Catch ex As Exception
        End Try
        Try
            lang616.Text = axlabs.GetASPXPage("pmfm.aspx", "lang616")
        Catch ex As Exception
        End Try
        Try
            lang617.Text = axlabs.GetASPXPage("pmfm.aspx", "lang617")
        Catch ex As Exception
        End Try
        Try
            lang618.Text = axlabs.GetASPXPage("pmfm.aspx", "lang618")
        Catch ex As Exception
        End Try
        Try
            lang619.Text = axlabs.GetASPXPage("pmfm.aspx", "lang619")
        Catch ex As Exception
        End Try
        Try
            lang620.Text = axlabs.GetASPXPage("pmfm.aspx", "lang620")
        Catch ex As Exception
        End Try
        Try
            lang621.Text = axlabs.GetASPXPage("pmfm.aspx", "lang621")
        Catch ex As Exception
        End Try
        Try
            lang622.Text = axlabs.GetASPXPage("pmfm.aspx", "lang622")
        Catch ex As Exception
        End Try
        Try
            lang623.Text = axlabs.GetASPXPage("pmfm.aspx", "lang623")
        Catch ex As Exception
        End Try
        Try
            lang624.Text = axlabs.GetASPXPage("pmfm.aspx", "lang624")
        Catch ex As Exception
        End Try
        Try
            lang625.Text = axlabs.GetASPXPage("pmfm.aspx", "lang625")
        Catch ex As Exception
        End Try
        Try
            lang626.Text = axlabs.GetASPXPage("pmfm.aspx", "lang626")
        Catch ex As Exception
        End Try
        Try
            lang627.Text = axlabs.GetASPXPage("pmfm.aspx", "lang627")
        Catch ex As Exception
        End Try
        Try
            lang628.Text = axlabs.GetASPXPage("pmfm.aspx", "lang628")
        Catch ex As Exception
        End Try
        Try
            lang629.Text = axlabs.GetASPXPage("pmfm.aspx", "lang629")
        Catch ex As Exception
        End Try
        Try
            lang630.Text = axlabs.GetASPXPage("pmfm.aspx", "lang630")
        Catch ex As Exception
        End Try
        Try
            lang631.Text = axlabs.GetASPXPage("pmfm.aspx", "lang631")
        Catch ex As Exception
        End Try
        Try
            lang632.Text = axlabs.GetASPXPage("pmfm.aspx", "lang632")
        Catch ex As Exception
        End Try
        Try
            lang633.Text = axlabs.GetASPXPage("pmfm.aspx", "lang633")
        Catch ex As Exception
        End Try
        Try
            lang634.Text = axlabs.GetASPXPage("pmfm.aspx", "lang634")
        Catch ex As Exception
        End Try

    End Sub

End Class
