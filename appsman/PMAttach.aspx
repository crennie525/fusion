<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMAttach.aspx.vb" Inherits="lucy_r12.PMAttach" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMAttach</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table width="480">
        <tr>
            <td class="thdrsing label">
                <asp:Label ID="lang586" runat="server">Attachment Upload Dialog</asp:Label>
            </td>
        </tr>
        <tr height="26">
            <td style="border-bottom: #7ba4e0 thin solid" colspan="4" class="label" id="tdhdr"
                runat="server">
            </td>
        </tr>
    </table>
    <table width="480">
        <tr>
            <td class="bluelabel">
                <asp:Label ID="lang587" runat="server">Choose Documents to Upload</asp:Label>
            </td>
            <td>
                <input class="3DButton" id="btnupload" type="button" value="Upload" name="btnupload"
                    runat="server">
            </td>
            <td class="bluelabel">
                <asp:Label ID="lang588" runat="server">Select Files to Delete</asp:Label>
            </td>
            <td>
                <asp:ImageButton ID="ibdelproc" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td class="label" colspan="2">
                <input class="3DButton" id="File1" type="file" size="32" name="MyFile" runat="Server">
            </td>
            <td class="labellt" id="tbfiles" valign="top" colspan="2" rowspan="10" runat="server">
                <asp:CheckBoxList ID="cblproc" CssClass="labellt" runat="server">
                </asp:CheckBoxList>
            </td>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File2" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File3" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File4" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File5" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2" style="height: 1px">
                    <input class="3DButton" id="File6" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File7" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File8" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File9" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="label" colspan="2">
                    <input class="3DButton" id="File10" type="file" size="32" name="MyFile" runat="Server">
                </td>
            </tr>
            <tr>
                <td class="plainlabel" id="tddoc" runat="server">
                </td>
            </tr>
    </table>
    <input id="lbldoctype" type="hidden" runat="server" name="lbldoctype"><input id="lblpmid"
        type="hidden" runat="server" name="lblpmid">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
