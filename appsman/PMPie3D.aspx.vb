

'********************************************************
'*
'********************************************************



Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Public Class PMPie3D
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim mu As New mmenu_utils_a
    Dim coi As String
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Private myBrushes(12) As Brush
    Private mySkills(100) As String
    Private myHours(100) As String
    Private myHoursDec(100) As Decimal
    Dim sk, hr, title As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        coi = mu.COMPI
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        sk = Request.QueryString("skills").ToString
        hr = Request.QueryString("hrs").ToString
        title = Request.QueryString("title").ToString
        createSkills(sk)
        createHours(hr)
        Response.ContentType = "image/jpeg"
        Const width As Integer = 260
        Const height As Integer = 300
        Dim objBitmap As Bitmap = New Bitmap(width, height)
        Dim objGraphics As Graphics = Graphics.FromImage(objBitmap)
        objGraphics.FillRectangle(New SolidBrush(System.Drawing.Color.White), 0, 0, width, height)
        createBrushes()
        Draw3DPieChart(objGraphics, title, coi)
        objBitmap.Save(Response.OutputStream, ImageFormat.Jpeg)
        objGraphics.Dispose()
        objBitmap.Dispose()
        'FillPieFloat()
    End Sub
    Private Sub createHours(ByVal hr As String)
        'Try
        myHours = Split(hr, ",")
        'Catch ex As Exception
        'Throw ex
        'End Try
    End Sub
    Private Sub createSkills(ByVal sk As String)
        'Try
        mySkills = Split(sk, ",")
        'Catch ex As Exception
        'Throw ex
        'End Try
    End Sub
    Public Sub FillPieFloat()
        Response.ContentType = "image/jpeg"
        Const width As Integer = 300
        Const height As Integer = 300
        Dim objBitmap As Bitmap = New Bitmap(width, height)
        Dim objGraphics As Graphics = Graphics.FromImage(objBitmap)
        objGraphics.FillRectangle(New SolidBrush(System.Drawing.Color.White), 0, 0, width, height)
        ' Create solid brush.
        Dim redBrush As New SolidBrush(System.Drawing.Color.Red)
        ' Create location and size of ellipse.
        Dim x As Single = 0.0F
        Dim y As Single = 0.0F
        Dim width1 As Single = 200.0F
        Dim height1 As Single = 100.0F
        ' Create start and sweep angles.
        Dim startAngle As Single = 0.0F
        Dim sweepAngle As Single = 45.0F
        ' Fill pie to screen.
        objGraphics.FillPie(redBrush, x, y, width1, height1, startAngle, sweepAngle)
        'g.FillPie(myBrushes(i), 5, 5, 160, 160, CInt(angleSoFar), CInt(percentage))
        objBitmap.Save(Response.OutputStream, ImageFormat.Jpeg)
        objGraphics.Dispose()
        objBitmap.Dispose()
    End Sub
    Private Sub createBrushes()
        'Try
        myBrushes(0) = New SolidBrush(System.Drawing.Color.Red)
        myBrushes(1) = New SolidBrush(System.Drawing.Color.Green)
        myBrushes(2) = New SolidBrush(System.Drawing.Color.Blue)
        myBrushes(3) = New SolidBrush(System.Drawing.Color.Orange)
        myBrushes(4) = New SolidBrush(System.Drawing.Color.Purple)
        myBrushes(5) = New SolidBrush(System.Drawing.Color.Aqua)
        myBrushes(6) = New SolidBrush(System.Drawing.Color.Brown)
        myBrushes(7) = New SolidBrush(System.Drawing.Color.Yellow)
        myBrushes(8) = New SolidBrush(System.Drawing.Color.Navy)
        myBrushes(9) = New SolidBrush(System.Drawing.Color.Lime)
        myBrushes(10) = New SolidBrush(System.Drawing.Color.Fuchsia)
        myBrushes(11) = New SolidBrush(System.Drawing.Color.Silver)
        myBrushes(12) = New SolidBrush(System.Drawing.Color.Teal)
        'Catch ex As Exception
        'Throw ex
        'End Try
    End Sub
    'Draws a 3D pie chart where ever slice is 45 degrees in value
    Private Sub Draw3DPieChart(ByVal objGraphics As Graphics, ByVal title As String, ByVal coi As String)
        Dim iLoop, iLoop2 As Integer
        Dim x As Integer = 0
        Dim y As Integer = 22
        Dim width As Integer = 140
        Dim height As Integer = 70
        Dim startAngle As Integer = 0
        Dim sweepAngle As Integer = 45
        Dim objBrush As SolidBrush = New SolidBrush(System.Drawing.Color.Coral)
        Dim rand As Random = New Random
        objGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias
        If coi = "BRI" Then
            If title = "Original" Then
                objGraphics.DrawString("Utilisation des ressources originale", New Font("Arial", 9, FontStyle.Bold), Brushes.Blue, 30, 0)
            Else
                objGraphics.DrawString("Utilisation des ressources optimis�", New Font("Arial", 9, FontStyle.Bold), Brushes.Blue, 30, 0)
            End If

        Else
            objGraphics.DrawString("Resource Usage " & title, New Font("Arial", 9, FontStyle.Bold), Brushes.Blue, 30, 0)
        End If

        'Loop through 180 back around to 135 degress so it gets drawn correctly.
        Dim p() As Integer = {50, 5, 95}
        Dim i As Integer
        Dim total As Decimal
        Dim percentage As Double
        Dim angleSoFar As Double = 0.0
        '   Caculates the total
        'myHours.Sort(myHours)
        'myHours.Reverse(myHours)
        'mySkills.Sort(mySkills)
        'mySkills.Reverse(mySkills)
        Dim t As String = myHours(0)
        For i = 0 To myHours.Length - 2
            If myHours(i) <> "0" Or myHours(i) <> "" Then
                total += Convert.ToDecimal(myHours(i))
            End If

        Next

        sweepAngle = 360
        For iLoop2 = 0 To 9
            objGraphics.FillPie(New HatchBrush(HatchStyle.Percent50, objBrush.Color), x, y + iLoop2, width, height, startAngle, sweepAngle)
        Next
        For i = 0 To myHours.Length - 2
            If myHours(i) <> "0" And total <> 0 Then
                percentage = Convert.ToDecimal(myHours(i)) / total * 360
                sweepAngle = percentage 'Mod 360 ''(i + 180)

                Try
                    objGraphics.FillPie(myBrushes(i), x, y, width, height, startAngle, sweepAngle)
                    objGraphics.FillRectangle(myBrushes(i), 155, 22 + (i * 19), 12, 12)
                Catch ex As Exception
                    objBrush.Color = System.Drawing.Color.FromArgb(rand.Next(255), rand.Next(255), rand.Next(255))
                    objGraphics.FillPie(objBrush, x, y, width, height, startAngle, sweepAngle)
                    objGraphics.FillRectangle(objBrush, 155, 22 + (i * 19), 12, 12)
                End Try
                startAngle += percentage
                objGraphics.DrawString(mySkills(i), New Font("Arial", 8, FontStyle.Regular), Brushes.Black, 170, 22 + (i * 19))
            End If
        Next
    End Sub


End Class
