<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="measalert.aspx.vb" Inherits="lucy_r12.measalert" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>measalert</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/measalertaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="label" align="center" colspan="4">
                <input id="rblead" onclick="swlist('lead');" type="radio" name="rdchoose" runat="server"><asp:Label
                    ID="lang543" runat="server">Lead Craft</asp:Label><input id="rbsup" onclick="swlist('sup');"
                        type="radio" name="rdchoose" runat="server"><asp:Label ID="lang544" runat="server">Supervisor</asp:Label><input
                            id="rblog" onclick="swlist('log');" type="radio" checked name="rdchoose" runat="server"><asp:Label
                                ID="lang545" runat="server">PM Logins</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table width="400">
                    <tr>
                        <td class="bluelabel" width="185">
                            <asp:Label ID="lang546" runat="server">Available Labor/Logins</asp:Label>
                        </td>
                        <td valign="middle" align="center" width="20">
                        </td>
                        <td class="bluelabel" width="185">
                            <asp:Label ID="lang547" runat="server">Selected Labor/Logins</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ListBox ID="lbmaster" runat="server" Width="176px" Height="250px" SelectionMode="Multiple">
                            </asp:ListBox>
                        </td>
                        <td>
                            <img class="details" id="todis" runat="server" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                width="20" height="20">
                            <img class="details" id="fromdis" runat="server" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                width="20" height="20"><asp:ImageButton ID="btntolist" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                                </asp:ImageButton><br>
                            <asp:ImageButton ID="btnfromlist" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                            </asp:ImageButton>
                        </td>
                        <td>
                            <asp:ListBox ID="lbselect" runat="server" Width="175px" Height="250px" SelectionMode="Multiple">
                            </asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">
                            <input class="lilmenu plainlabel" onmouseover="this.className='lilmenuhov plainlabel'"
                                style="width: 55px; height: 24px" onclick="gotoform();" onmouseout="this.className='lilmenu plainlabel'"
                                type="button" value="Submit">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lbltyp" type="hidden" runat="server"><input id="lblmeas" type="hidden"
        runat="server">
    <input id="lblsubmit" type="hidden" runat="server"><input type="hidden" id="lblatyp"
        runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
