

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class pmactmain
    Inherits System.Web.UI.Page
    Protected WithEvents lang573 As System.Web.UI.WebControls.Label

    Protected WithEvents lang572 As System.Web.UI.WebControls.Label

    Protected WithEvents lang571 As System.Web.UI.WebControls.Label

    Protected WithEvents lang570 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pmid, pmhid, won, ro As String
    Dim dr As SqlDataReader
    Dim sql As String
    Dim comp As New Utilities
    Dim sid As String
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmhid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdj As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents wo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifwo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents jp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifjp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblupsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpmd As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            pmid = Request.QueryString("pmid").ToString '"139" '
            pmhid = Request.QueryString("pmhid").ToString
            won = Request.QueryString("wo").ToString
            sid = Request.QueryString("sid").ToString
            lblpmid.Value = pmid
            lblpmhid.Value = pmhid
            lblwo.Value = won
            lblsid.Value = sid
            comp.Open()
            GetWoHead(won)
            comp.Dispose()
            'document.getElementById("ifjp").src="pmact.aspx?wo=" + wo + "&pmid=" + jp + "&pmhid=" + pmhid + "&date=" + Date();
            ifjp.Attributes.Add("src", "pmact.aspx?wo=" & won & "&pmid=" & pmid & "&pmhid=" & pmhid & "&ro=" & ro)
        End If

    End Sub
    Private Sub GetWoHead(ByVal wonum As String)
        wonum = lblwo.Value
        sql = "select w.wonum, w.description, w.jpid, w.status, j.jpnum, j.description as jdesc " _
        + "from workorder w left join wojobplans j on j.jpid = w.jpid  where w.wonum = '" & wonum & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdwo.InnerHtml = dr.Item("wonum").ToString
            tdwod.InnerHtml = dr.Item("description").ToString
            'lbljpid.Value = dr.Item("jpid").ToString
            'jpid = dr.Item("jpid").ToString
            'stat = dr.Item("status").ToString
            'tdjp.InnerHtml = dr.Item("jpnum").ToString
            'tdjpd.InnerHtml = dr.Item("jdesc").ToString
        End While
        dr.Close()
        'lbljpid.Value = jpid
        'lblstat.Value = stat
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang570.Text = axlabs.GetASPXPage("pmactmain.aspx", "lang570")
        Catch ex As Exception
        End Try
        Try
            lang571.Text = axlabs.GetASPXPage("pmactmain.aspx", "lang571")
        Catch ex As Exception
        End Try
        Try
            lang572.Text = axlabs.GetASPXPage("pmactmain.aspx", "lang572")
        Catch ex As Exception
        End Try
        Try
            lang573.Text = axlabs.GetASPXPage("pmactmain.aspx", "lang573")
        Catch ex As Exception
        End Try

    End Sub

End Class
