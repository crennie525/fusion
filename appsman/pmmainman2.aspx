<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmmainman2.aspx.vb" Inherits="lucy_r12.pmmainman2" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmmainman2</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts/dragitem.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMMainManaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 0px">
        <tbody>
            <tr>
                <td colspan="3">
                    <div id="divywr" class="wolistsmall" runat="server">
                        <asp:Repeater ID="rptrtasks" runat="server">
                            <HeaderTemplate>
                                <table cellspacing="2" cellpadding="0">
                                    <tr class="tbg" height="26">
                                        <td class="thdrsingg plainlabel" width="150">
                                            <asp:Label ID="lang642" runat="server">Equipment#</asp:Label>
                                        </td>
                                        <td class="details" width="70" id="tdwo" runat="server">
                                            <asp:Label ID="lang643" runat="server">Work Order#</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="150">
                                            PdM
                                        </td>
                                        <td class="thdrsingg plainlabel" width="210">
                                            PM
                                        </td>
                                        <td class="thdrsingg plainlabel" width="70">
                                            <asp:Label ID="lang644" runat="server">Next Date</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="20">
                                            <img src="../images/appbuttons/minibuttons/gwarningnbg.gif">
                                        </td>
                                        <td class="thdrsingg plainlabel" width="70">
                                            <asp:Label ID="lang645" runat="server">Print</asp:Label>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg" height="20">
                                    <td class="plainlabel">
                                        &nbsp;
                                        <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details" id="tdwoi" runat="server">
                                        &nbsp;
                                        <asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        &nbsp;
                                        <asp:Label ID="lblpdm" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:LinkButton ID="lbltn" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lbllast" Text='<%# DataBinder.Eval(Container.DataItem,"nextdates")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td align="center">
                                        <img id="iwi" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                            src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                    </td>
                                    <td class="plainlabel" align="center">
                                        <img id="imgi" runat="server" onmouseover="return overlib('Print This PM', ABOVE, LEFT)"
                                            onclick="printpm();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif">
                                        <img id="imgi2" runat="server" onmouseover="return overlib('Print This PM Work Order', ABOVE, LEFT)"
                                            onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif">
                                        <img id="imgi3" onmouseover="return overlib('Print This PM Work Order w\images', ABOVE, LEFT)"
                                            onmouseout="return nd()" src="../images/appbuttons/minibuttons/printpic.gif"
                                            runat="server">
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblpmiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="transrowblue" height="20">
                                    <td class="plainlabel transrowblue">
                                        &nbsp;
                                        <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details" id="tdwoe" runat="server">
                                        &nbsp;
                                        <asp:Label ID="Label4" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        &nbsp;
                                        <asp:Label ID="lblpdmalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:LinkButton ID="lbltnalt" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="lbllastalt" Text='<%# DataBinder.Eval(Container.DataItem,"nextdates")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td align="center">
                                        <img id="iwa" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                            src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                    </td>
                                    <td class="plainlabel transrowblue" align="center">
                                        <img id="imga" runat="server" onmouseover="return overlib('Print This PM', ABOVE, LEFT)"
                                            onclick="printpm();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif">
                                        <img id="imga2" runat="server" onmouseover="return overlib('Print This PM Work Order', ABOVE, LEFT)"
                                            onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif">
                                        <img id="imga3" onmouseover="return overlib('Print This PM Work Order w\images', ABOVE, LEFT)"
                                            onmouseout="return nd()" src="../images/appbuttons/minibuttons/printpic.gif"
                                            runat="server">
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblpmidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="right">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <input id="txtpg" type="hidden" runat="server" name="txtpg">
    <input id="txtpgcnt" type="hidden" runat="server" name="txtpgcnt"><input id="lblfilt"
        type="hidden" runat="server" name="lblfilt">
    <input id="lblprint" type="hidden" runat="server" name="lblprint"><input id="lblpmid"
        type="hidden" runat="server" name="lblpmid">
    <input id="lblret" type="hidden" runat="server" name="lblret">
    <input id="lblsrch" type="hidden" runat="server" name="lblsrch">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server">
    <input id="lblncid" type="hidden" name="lblncid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lbldept" type="hidden" name="lbldept" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lbllid" type="hidden" name="lbllid" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lblskillid" type="hidden" name="lblskillid" runat="server">
    <input id="lblsup" type="hidden" name="Hidden1" runat="server">
    <input id="lbllead" type="hidden" name="Hidden1" runat="server">
    <input id="lblpchk" type="hidden" name="lblpchk" runat="server"><input id="lbldragid"
        type="hidden"><input id="lblrowid" type="hidden">
    <input id="lblrtid" type="hidden" name="lblrtid" runat="server">
    <input id="lblprintwolist" type="hidden" runat="server" name="lblprintwolist">
    <input id="lblfiltwo" type="hidden" runat="server" name="lblfiltwo">
    <input id="lbldocs" type="hidden" runat="server" name="lbldocs">
    <input id="lblro" type="hidden" runat="server" name="lblro"><input id="lbllocstr"
        type="hidden" runat="server" name="lbllocstr">
    <input id="Hidden1" type="hidden" name="lbldragid" runat="server"><input id="lbllog"
        type="hidden" runat="server" name="lbllog">
    <input id="lbltab" type="hidden" runat="server" name="lbltab"><input id="lblhide"
        type="hidden" runat="server" name="lblhide">
    <input type="hidden" id="lblfslang" runat="server" name="lblfslang">
    </form>
</body>
</html>
