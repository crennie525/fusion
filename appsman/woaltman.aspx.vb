

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class woaltman
    Inherits System.Web.UI.Page
	Protected WithEvents ovid92 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid91 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid90 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang753 As System.Web.UI.WebControls.Label

	Protected WithEvents lang752 As System.Web.UI.WebControls.Label

	Protected WithEvents lang751 As System.Web.UI.WebControls.Label

	Protected WithEvents lang750 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim orig, pmid, rev, revend, use, last, freq, jpid, ro As String
    Dim sql As String
    Protected WithEvents cbcalc As System.Web.UI.WebControls.CheckBox
    Protected WithEvents tdplain As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim alt As New Utilities
    Protected WithEvents tdboth As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdwos As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjos As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsdays As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgsav As System.Web.UI.HtmlControls.HtmlImage
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtnewdate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnewend As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdtop As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdend As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblorig As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllast As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()


	GetFSLangs()
'Put user code to initialize the page here
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                imgsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsav.Attributes.Add("onclick", "")
            End If
            pmid = Request.QueryString("pmid").ToString
            orig = Request.QueryString("orig").ToString
            last = Request.QueryString("last").ToString
            freq = Request.QueryString("freq").ToString
            jpid = Request.QueryString("jpid").ToString
            lblorig.Value = orig
            lblpmid.Value = pmid
            lbllast.Value = last
            lblfreq.Value = freq
            lbljpid.Value = jpid
            alt.Open()
            CheckSStats(pmid, jpid)
            alt.Dispose()
        ElseIf Request.Form("lblpost") = "go" Then
            alt.Open()
            ChangeDate()
            alt.Dispose()
        End If

    End Sub
    Private Sub CheckSStats(ByVal wonum As String, ByVal jpid As String)
        Dim wscnt, jscnt As Integer
        sql = "select count(*) as wscnt from wosched where wonum = '" & wonum & "'"
        dr = alt.GetRdrData(sql)
        While dr.Read
            wscnt = dr.Item("wscnt").ToString
        End While
        dr.Close()
        sql = "select count(*) as jscnt from wojobtasks where wonum = '" & wonum & "' and jpid = '" & jpid & "'"
        dr = alt.GetRdrData(sql)
        While dr.Read
            jscnt = dr.Item("jscnt").ToString
        End While
        dr.Close()

        sql = "select status, skill, skillid, isnull(datediff(day, schedstart, schedfinish),0) as sdays from workorder where wonum = '" & wonum & "'"
        dr = alt.GetRdrData(sql)
        While dr.Read
            lblstat.Value = dr.Item("status").ToString
            lblskill.Value = dr.Item("skill").ToString
            lblskillid.Value = dr.Item("skillid").ToString
            lblsdays.Value = dr.Item("sdays").ToString
        End While
        dr.Close()

        If wscnt = 0 And jscnt = 0 Then
            tdplain.Attributes.Add("class", "view")
            tdtop.InnerHtml = "Original Start:  " & orig
            tdend.InnerHtml = "Original Complete:  " & last
            cbcalc.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov76" , "woaltman.aspx.vb") & "', ABOVE, LEFT)")
            cbcalc.Attributes.Add("onmouseout", "return nd()")
        Else
            If wscnt <> 0 And jscnt <> 0 Then
                tdboth.Attributes.Add("class", "view")
                tdwos.Attributes.Add("class", "view")
                tdjos.Attributes.Add("class", "view")
            ElseIf wscnt <> 0 Then
                tdboth.Attributes.Add("class", "view")
                tdwos.Attributes.Add("class", "view")
            ElseIf jscnt <> 0 Then
                tdboth.Attributes.Add("class", "view")
                tdjos.Attributes.Add("class", "view")
            End If

        End If

    End Sub
    Private Sub ChangeDate()
        pmid = lblpmid.Value
        orig = lblorig.Value
        rev = txtnewdate.Text
        last = lbllast.Value
        revend = txtnewend.Text
        If cbcalc.Checked = True Or txtnewend.Text = "" Then
            use = "calc"
        Else
            use = "yes"
        End If
        sql = "usp_upwoalt '" & pmid & "', '" & orig & "', '" & last & "', '" & rev & "', '" & revend & "', '" & use & "'"
        alt.Update(sql)
        lblpost.Value = "go"
    End Sub


	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang750.Text = axlabs.GetASPXPage("woaltman.aspx","lang750")
		Catch ex As Exception
		End Try
		Try
			lang751.Text = axlabs.GetASPXPage("woaltman.aspx","lang751")
		Catch ex As Exception
		End Try
		Try
			lang752.Text = axlabs.GetASPXPage("woaltman.aspx","lang752")
		Catch ex As Exception
		End Try
		Try
			lang753.Text = axlabs.GetASPXPage("woaltman.aspx","lang753")
		Catch ex As Exception
		End Try

	End Sub

	

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			imgsav.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("woaltman.aspx","imgsav") & "', ABOVE, LEFT)")
			imgsav.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid90.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("woaltman.aspx","ovid90") & "', ABOVE, LEFT)")
			ovid90.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid91.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("woaltman.aspx","ovid91") & "', ABOVE, LEFT)")
			ovid91.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid92.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("woaltman.aspx","ovid92") & "', ABOVE, LEFT)")
			ovid92.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
