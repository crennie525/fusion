﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChartSample.aspx.vb" Inherits="lucy_r12.ChartSample" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:Chart ID="Chart1" Width="820px"  Height="500px" runat="server" Palette="Pastel" BackColor="WhiteSmoke"
        BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom"
        BorderWidth="2" BorderColor="26, 59, 105" ImageType="Png" ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)">
        <Titles>
            <asp:Title Text="Measurement History" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold"
                ShadowOffset="2" Alignment="TopCenter" ForeColor="26, 59, 105">
            </asp:Title>
        </Titles>
        <Legends>
            <asp:Legend LegendStyle="Row" IsTextAutoFit="False" DockedToChartArea="MainChartArea"
                Docking="Bottom" IsDockedInsideChartArea="False" Name="Default" BackColor="Transparent"
                Font="Trebuchet MS, 8.25pt, style=Bold" Alignment="Center">
            </asp:Legend>
        </Legends>
        <Series>
            <asp:Series Name="Measurements" ChartType="Line" ChartArea="MainChartArea" BorderWidth="2"
                Color="Blue">
            </asp:Series>
            <asp:Series Name="Spec" ChartType="Line" ChartArea="MainChartArea" BorderWidth="2"
                Color="Green">
            </asp:Series>
            <asp:Series Name="High" ChartType="Line" ChartArea="MainChartArea" BorderWidth="2"
                Color="Red">
            </asp:Series>
            <asp:Series Name="Low" ChartType="Line" ChartArea="MainChartArea" BorderWidth="2"
                Color="Purple">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="MainChartArea" BorderColor="64, 64, 64, 64" BackSecondaryColor="White"
                BackColor="WhiteSmoke" ShadowColor="Transparent">
                <AxisY LineColor="64, 64, 64, 64" IsLabelAutoFit="False">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisY>
                <AxisX LineColor="64, 64, 64, 64" IsLabelAutoFit="False">
                    <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                    <MajorGrid LineColor="64, 64, 64, 64" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>

    </div>
    </form>
</body>
</html>
