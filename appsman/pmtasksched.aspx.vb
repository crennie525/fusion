

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmtasksched
    Inherits System.Web.UI.Page
	Protected WithEvents lang743 As System.Web.UI.WebControls.Label

	Protected WithEvents lang742 As System.Web.UI.WebControls.Label

	Protected WithEvents lang741 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim pmid As String
    Dim ds As DataSet
    Dim wonum, jpid, stat, eqid, ro As String
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltool As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllube As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dghours As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents txttsk As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdjpn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdjpd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtlead As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwojtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblindx As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsdateret As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            pmid = Request.QueryString("pmid").ToString
            eqid = Request.QueryString("eqid").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblpmid.Value = pmid
            lbleqid.Value = eqid
            lblro.Value = ro
            comp.Open()
            GetJPHead(pmid)
            PopDL(pmid)
            comp.Dispose()
        Else
            If Request.Form("lblsubmit") = "getlead" Then
                lblsubmit.Value = ""
                comp.Open()
                'GetLead()
                comp.Dispose()
            ElseIf Request.Form("lblsubmit") = "upsdate" Then
                lblsubmit.Value = ""
                comp.Open()
                UpSDate()
                comp.Dispose()
            End If
        End If
    End Sub
    Private Sub UpSDate()
        Dim wojtid, sdate As String
        wojtid = lblwojtid.Value
        sdate = lblsdateret.Value
        sql = "update pmtrack set sdate = '" & sdate & "' where pmtid = '" & wojtid & "'"
        comp.Update(sql)
        wonum = lblwo.Value
        pmid = lblpmid.Value
        sql = "usp_checkpmsdates '" & wonum & "','" & pmid & "'"
        comp.Update(sql)
        PopDL(pmid)
    End Sub


    Private Sub GetJPHead(ByVal pmid As String)
        sql = "select p.skill, p.qty, e.eqnum, p.wonum, w.status, pm = (p.skill + '/' + cast(p.freq as varchar(10)) + ' days/' + p.rd) from pm p " _
        + "left join workorder w on w.wonum = p.wonum left join equipment e on e.eqid = p.eqid where p.pmid = '" & pmid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdjpn.InnerHtml = dr.Item("pm").ToString
            tdeq.InnerHtml = dr.Item("eqnum").ToString
            wonum = dr.Item("wonum").ToString
            stat = dr.Item("status").ToString
        End While
        dr.Close()
        lblwo.Value = wonum
        lblstat.Value = stat
        If (stat <> "COMP" And stat <> "CAN" And stat <> "WAPPR") Then
            dghours.Columns(7).Visible = True
        Else
            dghours.Columns(7).Visible = False
        End If
    End Sub
    Private Sub PopDL(ByVal pmid As String)
        sql = "select distinct t.pmtid, t.pmtskid, t.tasknum, p.skill, p.qty, t.ttime, t.compnum, t.func," _
        + "task = t.taskdesc, Convert(char(10),t.sdate,101) as sdate " _
        + "from pmtrack t left join pm p on p.pmid = t.pmid where t.pmid = '" & pmid & "' " _
        + "and t.subtask = 0 order by t.func, t.compnum, t.tasknum"
        ds = comp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        'Try
        dghours.DataSource = dv
        dghours.DataBind()
    End Sub

    Private Sub dghours_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dghours.ItemDataBound

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim img As HtmlImage = CType(e.Item.FindControl("imgti"), HtmlImage)
            Dim tsk As String = DataBinder.Eval(e.Item.DataItem, "task").ToString
            img.Attributes.Add("onclick", "gettsk('" & tsk & "');")

            Dim wojtid As String = DataBinder.Eval(e.Item.DataItem, "pmtid").ToString

            Dim lid As String = CType(e.Item.FindControl("lblsdate"), Label).ClientID
            Dim imgs As HtmlImage = CType(e.Item.FindControl("imgsdate"), HtmlImage)
            imgs.Attributes.Add("onclick", "getcal('" & lid & "','" & wojtid & "');")
        End If
       
    End Sub

    Private Sub dghours_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dghours.ItemCommand
        If e.CommandName = "Tool" Then
            lbltool.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If

        End If
        If e.CommandName = "Part" Then
            lblpart.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If
        If e.CommandName = "Lube" Then
            lbllube.Value = "yes"
            If e.Item.ItemType = ListItemType.EditItem Then
                lblpmtid.Value = CType(e.Item.FindControl("lblttid"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblt"), TextBox).Text 'e.Item.Cells(24).Text
            Else
                lblpmtid.Value = CType(e.Item.FindControl("lbltida"), Label).Text 'e.Item.Cells(24).Text
                lbltasknum.Value = CType(e.Item.FindControl("lblta"), Label).Text 'e.Item.Cells(24).Text
            End If
        End If

    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dghours.Columns(1).HeaderText = dlabs.GetDGPage("pmtasksched.aspx", "dghours", "1")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(3).HeaderText = dlabs.GetDGPage("pmtasksched.aspx", "dghours", "3")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(4).HeaderText = dlabs.GetDGPage("pmtasksched.aspx", "dghours", "4")
        Catch ex As Exception
        End Try
        Try
            dghours.Columns(5).HeaderText = dlabs.GetDGPage("pmtasksched.aspx", "dghours", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("pmtasksched.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang741.Text = axlabs.GetASPXPage("pmtasksched.aspx", "lang741")
        Catch ex As Exception
        End Try
        Try
            lang742.Text = axlabs.GetASPXPage("pmtasksched.aspx", "lang742")
        Catch ex As Exception
        End Try
        Try
            lang743.Text = axlabs.GetASPXPage("pmtasksched.aspx", "lang743")
        Catch ex As Exception
        End Try

    End Sub

End Class
