<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMMainMan.aspx.vb" Inherits="lucy_r12.PMMainMan" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMMainMan</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts/dragitem.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMMainManaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <style type="text/css">
        .blklink
{
font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:12px;
	text-decoration:none;
color: Black;	
text-decoration: underline;
}
    </style>
</head>
    <script language="javascript" type="text/javascript">
    <!--
        function printwo(id, wo, rtid) {
        alert(rtid)
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            if (rtid != "" & rtid != 0) {
                getroute(rtid, id);
            }
            if (wo == "") {
                alert("No Work Order Assigned for this PM")
            }
            else {
                window.open("../appswo/woprint.aspx?typ=PM&pm=" + id + "&wo=" + wo, "repWin", popwin);
                //window.showModalDialog("../appswo/woprint.aspx?typ=PM&pm=" + id + "&wo=" + wo, "repWin", popwin);
            }
        }
        function getroute(rtid, pm) {
            var popwin = "directories=0,height=520,width=820,location=1,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            //var rtid = document.getElementById("lblrtid").value;
            //alert(rtid)
            var typ = "RBASM3";
            //var pm = document.getElementById("lblpmid").value;
            if (rtid != "") {
                window.open("../reports/pmrhtml7_new.aspx?rid=" + rtid + "&typ=" + typ + "&pmid=" + pm, "repWin2", popwin);
            }
        }
        function printpm(typ) {
            /*document.getElementById("lblprintwolist").value = typ;
            var chk = document.getElementById("lblprint").value;
            document.getElementById("lblprint").value = "go";
            document.getElementById("form1").submit();*/
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            var filt = document.getElementById("lblfilt").value;
            var filtwo = document.getElementById("lblfiltwo").value;
            var ref = document.getElementById("lblref").value;
            if (typ == "pm") {
                if (filt != "") {
                    //document.getElementById("lblfilt").value = "";
                    getdocs();
                    window.open("PMPrintAll.aspx?filt=" + filt, "repWin", popwin);
                    //document.getElementById("imgregpm").className="details";
                    //document.getElementById("imgrteprint").className="details";
                }
                else {
                    //alert("Print Limited to Once Per Search")
                }
            }
            else {
                if (filtwo != "") {
                    //document.getElementById("lblfiltwo").value = "";
                    getdocs();
                    window.open("PMWOPrintAll.aspx?filt=" + ref, "repWin", popwin);
                    //document.getElementById("imgrtewprint").className="details";
                    //document.getElementById("imgwopm").className="details";
                }
                else {
                    //alert("Print Limited to Once Per Search")
                }
            }
        }
        function getdocs() {
            var cbs = document.getElementById("lbldocs").value;
            var cbsarr = cbs.split(";")
            for (var i = 0; i < cbsarr.length; i++) {
                //alert(i)
                var cbstr = cbsarr[i];
                //alert(cbstr)
                if (cbstr != "") {
                    var cbstrarr = cbstr.split("~")
                    var fnstr = cbstrarr[1]
                    var docstr = cbstrarr[0]
                    OpenFile(fnstr, docstr);
                }
            }
        }
        function jump(id, eid, piccnt, sid) {
            //alert(sid)
            window.parent.handlejump(id, eid, piccnt, sid);
        }
        function resetsrch() {
            var url = window.location.href;
            var nohttp = url.split('//')[1];
            var hostPort = nohttp.split('/')[1]
            //alert (hostPort)
            //window.location = "/" + hostPort + "/appsman/PMMainMan.aspx?jump=no";
            var tab = document.getElementById("lbltab").value;
            sid = document.getElementById("lblsid").value;
            if (tab == "") {
                window.location = "PMMainMan.aspx?jump=no&sid=" + sid;
            }
            else {
                window.location = "PMMainMan.aspx?jump=no&tab=" + tab + "&sid=" + sid;
            }

        }
        function handlert(id, rt) {
            document.getElementById("lblrtid").value = id;
            document.getElementById("tdrte").innerHTML = rt;
            document.getElementById("lblrt").value = rt;
            closert()
        }
        function getcal(fld) {
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                var fldret = "td" + fld;
                document.getElementById(fldret).innerHTML = eReturn;
                //document.getElementById("lblschk").value = "1";
                if (fld == "n") {
                    document.getElementById("lblnext").value = eReturn;
                }
                if (fld == "s") {
                    document.getElementById("lbls").value = eReturn;
                }
                else if (fld == "e") {
                    document.getElementById("lble").value = eReturn;
                }
            }
        }

        function getsuper(typ) {
            var skill = document.getElementById("lblskillid").value;
            var ro = document.getElementById("lblro").value;
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var retarr = eReturn.split(",")
                    if (typ == "sup") {
                        document.getElementById("lblsup").value = retarr[0];
                        document.getElementById("tdsup").innerHTML = retarr[1];
                        document.getElementById("lblsupstr").value = retarr[1];
                    }
                    else {
                        document.getElementById("lbllead").value = retarr[0];
                        document.getElementById("tdlead").innerHTML = retarr[1];
                        document.getElementById("lblleadstr").value = retarr[1];
                    }
                }
            }
        }
        function checkother() {
            document.getElementById("tdrte").innerHTML = document.getElementById("lblrt").value;
            document.getElementById("tds").innerHTML = document.getElementById("lbls").value;
            document.getElementById("tde").innerHTML = document.getElementById("lble").value;
            document.getElementById("tdsup").innerHTML = document.getElementById("lblsupstr").value;
            document.getElementById("tdlead").innerHTML = document.getElementById("lblleadstr").value;
        }
        function checkprint() {
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
            var chk = document.getElementById("lblprint").value;
            if (chk == "yes") {
                document.getElementById("lblprint").value = "no";
                var filt = document.getElementById("lblfilt").value;
                var ref = document.getElementById("lblref").value;
                //alert(ref)
                document.getElementById("lblfilt").value = "";
                var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
                var typ = document.getElementById("lblprintwolist").value;
                document.getElementById("lblprintwolist").value = "";
                if (typ == "pm") {
                    window.open("PMPrintAll.aspx?filt=" + filt, "repWin", popwin);
                }
                else {
                    window.open("PMWOPrintAll.aspx?filt=" + ref, "repWin", popwin);
                }
            }
            var who = document.getElementById("lblwho").value;
            if (who != "") {
                checkwho(who);
            }
            checkother();
        }

        function checkwho(who) {
            if (who == "depts") {
                document.getElementById("tddept").innerHTML = document.getElementById("lbldept").value;
                document.getElementById("tdcell").innerHTML = document.getElementById("lblcell").value;
                document.getElementById("tdeq").innerHTML = document.getElementById("lbleq").value;
                document.getElementById("tdfu").innerHTML = document.getElementById("lblfu").value;
                document.getElementById("tdco").innerHTML = document.getElementById("lblcomp").value;
            }
            else if (who == "locs") {
                document.getElementById("tdloc3").innerHTML = document.getElementById("lbllocstr").value;
                document.getElementById("tdeq3").innerHTML = document.getElementById("lbleq").value;
                document.getElementById("tdfu3").innerHTML = document.getElementById("lblfu").value;
                document.getElementById("tdco3").innerHTML = document.getElementById("lblcomp").value;
            }
        }
        function retminsrch(who) {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "ret";
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            var fu = document.getElementById("lblfu").value;
            var coid = document.getElementById("lblcoid").value;
            var comp = document.getElementById("lblcomp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            //alert(clid)
            //if (cell == "") {
            who = "deptret";
            //}
            //alert("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who)
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~");

                document.getElementById("lbllid").value = "";
                document.getElementById("lblloc").value = "";
                document.getElementById("tdloc3").innerHTML = "";
                document.getElementById("lbllocstr").value = "";

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq3").innerHTML = "";
                document.getElementById("lbleqid").value = "";

                document.getElementById("lblfuid").value = "";
                document.getElementById("lblfu").value = "";
                document.getElementById("tdfu3").innerHTML = "";

                document.getElementById("lblcomid").value = "";
                document.getElementById("lblcomp").value = "";
                document.getElementById("tdco3").innerHTML = "";

                document.getElementById("trlocs3").className = "details";

                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblfuid").value = ret[6];
                document.getElementById("lblfu").value = ret[7];
                document.getElementById("tdfu").innerHTML = ret[7];
                document.getElementById("lblcomid").value = ret[8];
                document.getElementById("lblcomp").value = ret[9];
                document.getElementById("tdco").innerHTML = ret[9];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblwho").value = "depts";
            }
        }
        function getminsrch() {
            var did = document.getElementById("lbldid").value;
            //alert(did)
            if (did != "") {
                retminsrch();
            }
            else {
                var sid = document.getElementById("lblsid").value;
                var typ = "lul";
                var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn.split("~");

                    document.getElementById("lbllid").value = "";
                    document.getElementById("lblloc").value = "";
                    document.getElementById("tdloc3").innerHTML = "";
                    document.getElementById("lbllocstr").value = "";

                    document.getElementById("lbleq").value = ret[4];
                    document.getElementById("tdeq3").innerHTML = "";
                    document.getElementById("lbleqid").value = "";

                    document.getElementById("lblfuid").value = "";
                    document.getElementById("lblfu").value = "";
                    document.getElementById("tdfu3").innerHTML = "";

                    document.getElementById("lblcomid").value = "";
                    document.getElementById("lblcomp").value = "";
                    document.getElementById("tdco3").innerHTML = "";

                    document.getElementById("trlocs3").className = "details";

                    document.getElementById("lbldid").value = ret[0];
                    document.getElementById("lbldept").value = ret[1];
                    document.getElementById("tddept").innerHTML = ret[1];
                    document.getElementById("lblclid").value = ret[2];
                    document.getElementById("lblcell").value = ret[3];
                    document.getElementById("tdcell").innerHTML = ret[3];
                    document.getElementById("lbleqid").value = ret[4];
                    document.getElementById("lbleq").value = ret[5];
                    document.getElementById("tdeq").innerHTML = ret[5];
                    document.getElementById("lblfuid").value = ret[6];
                    document.getElementById("lblfu").value = ret[7];
                    document.getElementById("tdfu").innerHTML = ret[7];
                    document.getElementById("lblcomid").value = ret[8];
                    document.getElementById("lblcomp").value = ret[9];
                    document.getElementById("tdco").innerHTML = ret[9];
                    document.getElementById("lbllid").value = ret[12];
                    document.getElementById("lblloc").value = ret[13];
                    document.getElementById("trdepts").className = "view";
                    document.getElementById("lblwho").value = "depts";
                }

            }

        }

        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lul"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                document.getElementById("lbldid").value = "";
                document.getElementById("lbldept").value = "";
                document.getElementById("tddept").innerHTML = "";
                document.getElementById("lblclid").value = "";
                document.getElementById("lblcell").value = "";
                document.getElementById("tdcell").innerHTML = "";
                document.getElementById("lbleqid").value = "";
                document.getElementById("lbleq").value = "";
                document.getElementById("tdeq").innerHTML = "";
                document.getElementById("lblfuid").value = "";
                document.getElementById("lblfu").value = "";
                document.getElementById("tdfu").innerHTML = "";
                document.getElementById("lblcomid").value = "";
                document.getElementById("lblcomp").value = "";
                document.getElementById("tdco").innerHTML = "";
                document.getElementById("lbllid").value = "";
                document.getElementById("lblloc").value = "";
                document.getElementById("trdepts").className = "details";
                //alert(eReturn)
                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];
                document.getElementById("lbllocstr").value = ret[1];

                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq3").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];

                document.getElementById("lblfuid").value = ret[5];
                document.getElementById("lblfu").value = ret[6];
                document.getElementById("tdfu3").innerHTML = ret[6];

                document.getElementById("lblcomid").value = ret[7];
                document.getElementById("lblcomp").value = ret[8];
                document.getElementById("tdco3").innerHTML = ret[8];

                document.getElementById("trlocs3").className = "view";
                document.getElementById("lblwho").value = "locs";



            }

        }
        function srchloc1() {
            /*
            var eReturn = window.showModalDialog("LocDialog.aspx?typ=loc", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
            var ret = eReturn.split(",");
            document.getElementById("lbllhid").value = ret[0];
            document.getElementById("lbltyp").value = ret[1];
            document.getElementById("lblsubmit").value = "srch";
            document.getElementById("form1").submit();
				
            }
            */
            //alert("Temporarily Disabled")
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lu"
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lul&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
            //alert(eReturn)
                var ret = eReturn.split("~");
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").innerHTML = ret[1];
                document.getElementById("lbllocstr").value = ret[1];
                //document.getElementById("lblsubmit").value = "srch";
                //document.getElementById("form1").submit();
            }
        }
        //-->
    </script>
</head>
<body class="tbg" onload="checkprint();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 0px">
        <tbody>
            <tr>
                <td colspan="3">
                    <div id="divywr" class="wolistsmall" runat="server">
                        <asp:Repeater ID="rptrtasks" runat="server">
                            <HeaderTemplate>
                                <table cellspacing="2" cellpadding="0">
                                    <tr class="tbg">
                                        <td class="thdrsingg plainlabel" width="150" height="26">
                                            <asp:Label ID="lang642" runat="server">Equipment#</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="90" id="tdwo" runat="server">
                                            <asp:Label ID="lang643" runat="server">Work Order#</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="150">
                                            PdMa
                                        </td>
                                        <td class="thdrsingg plainlabel" width="210">
                                            PM
                                        </td>
                                        <td class="thdrsingg plainlabel" width="70">
                                            <asp:Label ID="lang644" runat="server">Next Date</asp:Label>
                                        </td>
                                        <td class="thdrsingg plainlabel" width="20">
                                            <img src="../images/appbuttons/minibuttons/gwarningnbg.gif">
                                        </td>
                                        <td class="thdrsingg plainlabel" width="70">
                                            <asp:Label ID="lang645" runat="server">Print</asp:Label>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg">
                                    <td class="plainlabel" height="20">
                                        &nbsp;
                                        <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel" id="tdwoi" runat="server">
                                        &nbsp;
                                        <asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        &nbsp;
                                        <asp:Label ID="lblpdm" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:LinkButton ID="lbltn" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lbllast" Text='<%# DataBinder.Eval(Container.DataItem,"nextdates")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td align="center">
                                        <img id="iwi" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                            src="../images/appbuttons/minibuttons/gwarning.gif" runat="server" />
                                    </td>
                                    <td class="plainlabel" align="center">
                                        <img id="imgi" runat="server" onmouseover="return overlib('Print This PM', ABOVE, LEFT)"
                                            onclick="printpm();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif" />
                                        <img id="imgi2" runat="server" onmouseover="return overlib('Print This PM Work Order', ABOVE, LEFT)"
                                            onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif" />
                                        <img id="imgi3" onmouseover="return overlib('Print This PM Work Order w\images', ABOVE, LEFT)"
                                            onmouseout="return nd()" src="../images/appbuttons/minibuttons/printpic.gif"
                                            runat="server" />
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblpmiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="transrowblue">
                                    <td class="plainlabel transrowblue" height="20">
                                        &nbsp;
                                        <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue" id="tdwoe" runat="server">
                                        &nbsp;
                                        <asp:Label ID="Label4" Text='<%# DataBinder.Eval(Container.DataItem,"wonum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        &nbsp;
                                        <asp:Label ID="lblpdmalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmd")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:LinkButton ID="lbltnalt" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"pm")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="lbllastalt" Text='<%# DataBinder.Eval(Container.DataItem,"nextdates")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td align="center">
                                        <img id="iwa" onmouseover="return overlib('Status Check Icon', ABOVE, LEFT)" onmouseout="return nd()"
                                            src="../images/appbuttons/minibuttons/gwarning.gif" runat="server">
                                    </td>
                                    <td class="plainlabel transrowblue" align="center">
                                        <img id="imga" runat="server" onmouseover="return overlib('Print This PM', ABOVE, LEFT)"
                                            onclick="printpm();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif">
                                        <img id="imga2" runat="server" onmouseover="return overlib('Print This PM Work Order', ABOVE, LEFT)"
                                            onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/woprint.gif">
                                        <img id="imga3" onmouseover="return overlib('Print This PM Work Order w\images', ABOVE, LEFT)"
                                            onmouseout="return nd()" src="../images/appbuttons/minibuttons/printpic.gif"
                                            runat="server">
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblpmidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="right">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="2" width="750" id="tbwrs" runat="server">
                        <tr height="22">
                            <td class="thdrsinglft" width="26">
                                <img border="0" src="../images/appbuttons/minibuttons/pmgridhdr.gif">
                            </td>
                            <td class="thdrsingrt label hand" onmouseover="return overlib('Click to Show or Hide Search Options', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="getsrch();" colspan="4" align="left" width="734"
                                id="tdwrs" runat="server">
                                <asp:Label ID="lang646" runat="server">PM Record Search</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table border="0" width="750" cellpadding="2" cellspacing="0" id="trhide1">
                        <tr>
                            <td colspan="6">
                                <table cellspacing="0" cellpadding="2" width="750">
                                    <tr>
                                        <td width="110">
                                        </td>
                                        <td width="135">
                                        </td>
                                        <td width="25">
                                        </td>
                                        <td width="120">
                                        </td>
                                        <td width="135">
                                        </td>
                                        <td width="25">
                                        </td>
                                        <td width="70">
                                        </td>
                                        <td width="160">
                                        </td>
                                        <td width="20">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9">
                                            <table cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td id="tddepts" class="bluelabel" runat="server">
                                                        Use Departments
                                                    </td>
                                                    <td>
                                                        <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                                    </td>
                                                    <td id="tdlocs1" class="bluelabel" runat="server">
                                                        Use Locations
                                                    </td>
                                                    <td>
                                                        <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trdepts" class="details" runat="server">
                                        <td colspan="9">
                                            <table cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td class="label" width="100">
                                                        Department
                                                    </td>
                                                    <td id="tddept" class="plainlabel" width="170" runat="server">
                                                    </td>
                                                    <td width="30">
                                                    </td>
                                                    <td class="label" width="100">
                                                        Equipment
                                                    </td>
                                                    <td id="tdeq" class="plainlabel" width="170" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        Station\Cell
                                                    </td>
                                                    <td id="tdcell" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="label">
                                                        Function
                                                    </td>
                                                    <td id="tdfu" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        <asp:Label ID="Label3" runat="server"></asp:Label>
                                                    </td>
                                                    <td id="tdcharge1" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="label">
                                                        Component
                                                    </td>
                                                    <td id="tdco" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trlocs3" class="details" runat="server">
                                        <td colspan="9">
                                            <table cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td class="label" width="100">
                                                        Location
                                                    </td>
                                                    <td id="tdloc3" class="plainlabel" runat="server" width="170">
                                                    </td>
                                                    <td width="30">
                                                    </td>
                                                    <td class="label" width="100">
                                                        Function
                                                    </td>
                                                    <td id="tdfu3" class="plainlabel" runat="server" width="170">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="label">
                                                        Equipment
                                                    </td>
                                                    <td id="tdeq3" class="plainlabel" runat="server">
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="label">
                                                        Component
                                                    </td>
                                                    <td id="tdco3" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang653" runat="server">Work Order#</asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtsrchwo" runat="server" CssClass="plainlabel" Font-Size="9pt"></asp:TextBox>
                                        </td>
                                        <td class="label" colspan="3">
                                            <asp:Label ID="lang654" runat="server" CssClass="details">Work Status</asp:Label>
                                            <input type="checkbox" id="cbbylab" runat="server" />Bypass Labor Restriction
                                             <asp:DropDownList ID="ddstatus" runat="server" CssClass="details" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                        
                                        <td class="label">
                                            <asp:Label ID="lang655" runat="server">Skill</asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="ddskill" runat="server" CssClass="plainlabel" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang874" runat="server">Supervisor</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tdsup">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Select Supervisor', LEFT)" onclick="getsuper('sup');"
                                                onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                border="0">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang875" runat="server">Lead Craft</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tdlead">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Select Lead Craft', LEFT)" onclick="getsuper('lead');"
                                                onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                border="0">
                                        </td>
                                        <td class="label">
                                            PdM
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="ddpdmlist" runat="server" CssClass="plainlabel" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang876" runat="server">From Date</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tds">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Get From Date', LEFT)" onclick="getcal('s');" onmouseout="return nd()"
                                                height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg" width="19">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang877" runat="server">To Date</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tde">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Get To Date', LEFT)" onclick="getcal('e');" onmouseout="return nd()"
                                                height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg" width="19">
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang878" runat="server">Route</asp:Label>
                                        </td>
                                        <td class="plainlabel" id="tdrte">
                                        </td>
                                        <td>
                                            <img onmouseover="return overlib('Get Route', LEFT)" onclick="getrt('pmrt');" onmouseout="return nd()"
                                                alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" align="right">
                                            <img id="imgregpm" class="details" onmouseover="return overlib('Print This PM Collection', ABOVE, LEFT)"
                                                onmouseout="return nd()" onclick="printpm('pm');" src="../images/appbuttons/minibuttons/printx.gif"
                                                runat="server" />&nbsp;
                                            <img id="imgwopm" class="details" onmouseover="return overlib('Print This PM Work Order Collection', ABOVE, LEFT)"
                                                onmouseout="return nd()" onclick="printpm('wo');" src="../images/appbuttons/minibuttons/woprint.gif"
                                                runat="server" />&nbsp;
                                            <img id="imgrteprint" class="details" onmouseover="return overlib('Print This PM Route Collection', ABOVE, LEFT)"
                                                onmouseout="return nd()" onclick="printpm('pm');" src="../images/appbuttons/minibuttons/routeall.gif"
                                                runat="server" />&nbsp;
                                            <img id="imgrtewprint" class="details" onmouseover="return overlib('Print This PM Route Work Order Collection', ABOVE, LEFT)"
                                                onmouseout="return nd()" onclick="printpm('wo');" src="../images/appbuttons/minibuttons/routeallwo.gif"
                                                runat="server" />&nbsp;
                                            <img onclick="resetsrch();" alt="" src="../images/appbuttons/minibuttons/switch.gif" />
                                            <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                                            </asp:ImageButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <div style="z-index: 999; border-bottom: black 1px solid; border-left: black 1px solid;
        width: 570px; height: 450px; border-top: black 1px solid; border-right: black 1px solid"
        id="rtdiv" onmouseup="mouseUp();restore('ifrt', 'trrt');" class="details" onmousedown="makeDraggable(this, 'ifrt', 'trrt');">
        <table class="tbg" cellspacing="0" cellpadding="0" width="570">
            <tr  bgcolor="blue">
                <td class="whitelabel" height="20">
                    <asp:Label ID="lang661" runat="server">Record Lookup</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closert();" alt="" src="../images/close.gif" width="18" height="18" /><br />
                </td>
            </tr>
            <tr id="trrt" class="details">
                <td class="bluelabelb" valign="middle" colspan="2" align="center"  height="450">
                    <asp:Label ID="lang662" runat="server">Moving Window...</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe style="width: 570px; height: 450px; background-color: White;" id="ifrt" src=""  frameborder="0" runat="server">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    <input id="txtpg" type="hidden" runat="server" />
    <input id="txtpgcnt" type="hidden" runat="server" />
    <input id="lblfilt" type="hidden" runat="server" />
    <input id="lblprint" type="hidden" runat="server" />
    <input id="lblpmid" type="hidden" runat="server" />
    <input id="lblret" type="hidden" runat="server" />
    <input id="lblsrch" type="hidden" runat="server" />
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server" />
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server" />
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server" />
    <input id="lbltyp" type="hidden" name="lbltyp" runat="server" />
    <input id="lblncid" type="hidden" name="lblncid" runat="server" />
    <input id="lblsid" type="hidden" name="lblsid" runat="server" />
    <input id="lblcid" type="hidden" name="lblcid" runat="server" />
    <input id="lbldept" type="hidden" name="lbldept" runat="server" />
    <input id="lblchk" type="hidden" name="lblchk" runat="server" />
    <input id="lbllid" type="hidden" name="lbllid" runat="server" />
    <input id="lblclid" type="hidden" name="lblclid" runat="server" />
    <input id="lblskillid" type="hidden" name="lblskillid" runat="server" />
    <input id="lblsup" type="hidden" name="Hidden1" runat="server" />
    <input id="lbllead" type="hidden" name="Hidden1" runat="server" />
    <input id="lblpchk" type="hidden" name="lblpchk" runat="server" />
    <input id="lbldragid" type="hidden" />
    <input id="lblrowid" type="hidden" />
    <input id="lblrtid" type="hidden" name="lblrtid" runat="server" />
    <input id="lblprintwolist" type="hidden" runat="server" />
    <input id="lblfiltwo" type="hidden" runat="server" />
    <input id="lbldocs" type="hidden" runat="server" />
    <input id="lblro" type="hidden" runat="server" />
    <input id="lbllocstr" type="hidden" runat="server" />
    <input id="Hidden1" type="hidden" name="lbldragid" runat="server" />
    <input id="lbllog" type="hidden" runat="server" />
    <input id="lbltab" type="hidden" runat="server" name="lbltab" />
    <input id="lblhide" type="hidden" runat="server" name="lblhide" />
    <input type="hidden" id="lbllocret" runat="server" />
    <input type="hidden" id="lblcomid" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lblfu" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblsupstr" runat="server" />
    <input type="hidden" id="lblleadstr" runat="server" />
    <input type="hidden" id="lbls" runat="server" />
    <input type="hidden" id="lble" runat="server" />
    <input type="hidden" id="lblrt" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbluserid" runat="server" />
    <input type="hidden" id="lblislabor" runat="server" />
    <input type="hidden" id="lblissched" runat="server" />
    <input type="hidden" id="lblisplanner" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    <input type="hidden" id="lblref" runat="server" />
    </form>
</body>
</html>
