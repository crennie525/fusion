

'********************************************************
'*
'********************************************************



Imports System.Drawing
Imports System.Drawing.Imaging

Public Class PMPie
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Private myImage As Bitmap
    Private g As Graphics
    Private p() As Integer = {1, 55, 0}
    Private towns() As String = {"Mechanic", "Technician", "Operator"}
    Private myBrushes(4) As Brush
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnBarchart As System.Web.UI.WebControls.Button
    Protected WithEvents btnPiechart As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        initialiseGraphics()
        'Try
        ' Set the background color and rendering quality.
        g.Clear(System.Drawing.Color.White)

        '   draws the barchart
        drawPieChart(g)

        ' Render the image to the HTML output stream.
        myImage.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg)
        'Catch ex As Exception
        'Throw ex
        'End Try
    End Sub
    '   Initialises the bitmap, graphics context and pens for drawing
    Private Sub initialiseGraphics()
        'Try
        ' Create an in-memory bitmap where you will draw the image. 
        ' The Bitmap is 300 pixels wide and 200 pixels high.
        myImage = New Bitmap(400, 200, PixelFormat.Format32bppRgb)

        ' Get the graphics context for the bitmap.
        g = Graphics.FromImage(myImage)

        '   Create the brushes for drawing
        createBrushes()
        'Catch ex As Exception
        'Throw ex
        'End Try
    End Sub
    Private Sub createBrushes()
        'Try
        myBrushes(0) = New SolidBrush(System.Drawing.Color.Red)
        myBrushes(1) = New SolidBrush(System.Drawing.Color.Blue)
        myBrushes(2) = New SolidBrush(System.Drawing.Color.Yellow)
        'myBrushes(3) = New SolidBrush(System.Drawing.Color.Green)
        'Catch ex As Exception
        'Throw ex
        'End Try
    End Sub
    Private Sub drawPieChart(ByVal g As Graphics)
        'Try
        '   Variables declaration
        Dim i As Integer
        Dim total As Integer
        Dim percentage As Double
        Dim angleSoFar As Double = 0.0

        '   Caculates the total
        For i = 0 To p.Length - 1
            total += p(i)
        Next
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias
        '   Draws the pie chart
        For i = 0 To p.Length - 1
            percentage = p(i) / total * 360

            g.FillPie(myBrushes(i), 5, 5, 160, 160, CInt(angleSoFar), CInt(percentage))

            angleSoFar += percentage

            '   Draws the lengend
            'g.FillRectangle(myBrushes(i), 180, 25 + (i * 25), 12, 12)
            'g.FillRectangle(myBrushes(i), 350, 25 + (i * 50), 25, 25) Orig
            g.FillRectangle(myBrushes(i), 180, 20 + (i * 25), 15, 15)
            '   Label the towns
            'g.DrawString("Town " & towns(i), New Font("Verdana", 8, FontStyle.Bold), Brushes.Brown, 390, 25 + (i * 50) + 10)
            g.DrawString(towns(i), New Font("Arial", 8, FontStyle.Regular), Brushes.Black, 200, 20 + (i * 25) + 2)
        Next
        'Catch ex As Exception
        'Throw ex
        'End Try
    End Sub
    Private Sub btnPiechart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPiechart.Click
      
    End Sub
End Class
