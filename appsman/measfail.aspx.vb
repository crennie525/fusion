﻿Imports System.Data.SqlClient
Public Class measfail
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim pmid, pmtid, pmhid, pmfid, fm, fmid, task, comid As String
    Dim pmf As New Utilities
    Dim mu As New mmenu_utils_a
    Dim coi As String
    Dim dr As SqlDataReader
    Dim sql, cid, ro As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        coi = mu.COMPI
        lblcoi.Value = coi
        GetFSOVLIBS()
        GetFSLangs()
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        If Not IsPostBack Then
            cid = "0"
            lblcid.Value = cid
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try

            lblro.Value = ro
            pmid = Request.QueryString("pmid").ToString
            pmhid = Request.QueryString("pmhid").ToString
            pmtid = Request.QueryString("pmtid").ToString
            fm = Request.QueryString("fm").ToString
            fmid = Request.QueryString("fmid").ToString
            task = Request.QueryString("task").ToString
            pmfid = Request.QueryString("pmfid").ToString
            comid = Request.QueryString("comid").ToString
            lblpmid.Value = pmid
            lblpmhid.Value = pmhid

            lblpmfid.Value = pmfid
            lblfm.Value = fm
            lblpmtid.Value = pmtid
            lblfmid.Value = fmid
            lblfail.Text = fm
            lbltask.Value = task
            lbltsk.Text = task
            lblcomid.Value = comid
            pmf.Open()
            GetLists()
            'If pmfid <> "" Then
            GetFail(pmtid, fmid)
            'End If
            pmf.Dispose()
        Else
            If Request.Form("lblsubmit") = "gencorr" Then
                lblsubmit.Value = ""
                pmf.Open()
                GenCorr()
                pmf.Dispose()
            ElseIf Request.Form("lblsubmit") = "can" Then
                lblsubmit.Value = ""
                pmf.Open()
                CanCorr()
                pmf.Dispose()
            End If
        End If
        ImageButton1.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov64", "PMFailMan.aspx.vb") & "', ABOVE, LEFT)")
        ImageButton1.Attributes.Add("onmouseout", "return nd()")

    End Sub
    Private Sub GetLists()
        cid = lblcid.Value
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = pmf.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0
    End Sub
    Private Sub CanCorr()
        Dim wonum As String = lblwo.Value
        Dim stat As String
        sql = "select status from workorder where wonum = '" & wonum & "'"
        stat = pmf.Scalar(sql)
        If stat = "WAPPR" Then
            sql = "update workorder set status = 'CAN' where wonum = '" & wonum & "'"
            pmf.Update(sql)
            Dim strMessage As String = tmod.getmsg("cdstr334", "PMFailMan.aspx.vb") & " " & wonum & " is Cancelled"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Dim strMessage As String = tmod.getmsg("cdstr335", "PMFailMan.aspx.vb") & " " & wonum & " was not Cancelled\As it has already been Approved\or has been changed to some other Status"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub btnwo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnwo.Click
        pmf.Open()
        GenCorr()
        pmf.Dispose()
    End Sub
    Private Sub GenCorr()
        SaveChanges()
        pmfid = lblpmfid.Value
        pmid = lblpmid.Value
        Dim eqid, eqnum, sid, deptid, cellid, locid, location, desc, funcid, comid, chrg As String
        sql = "select p.eqid, e.* from pm p left join equipment e on e.eqid = p.eqid where pmid = '" & pmid & "'"
        dr = pmf.GetRdrData(sql)
        While dr.Read
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            sid = dr.Item("siteid").ToString
            deptid = dr.Item("dept_id").ToString
            cellid = dr.Item("cellid").ToString
            locid = dr.Item("locid").ToString
            chrg = dr.Item("chargenum").ToString
        End While
        dr.Close()
        Dim wonum As Integer
        Dim superid, super, leadid, lead, se, le, start, skillid, skill As String
        superid = lblsup.Value
        super = txtsup.Text
        leadid = lbllead.Value
        lead = txtlead.Text
        If cbsupe.Checked = True Then
            se = "1"
        Else
            se = "0"
        End If
        If cbleade.Checked = True Then
            le = "1"
        Else
            le = "0"
        End If
        start = txtstart.Text
        If start = "" Then
            start = "NULL"
        Else
            start = "'" & start & "'"
        End If
        Dim wt As String
        Dim wtc As Integer = 0
        coi = lblcoi.Value
        If coi = "NISS" Or coi = "NISS_SYM" Then
            sql = "select count(*) from wotype where wotype = 'PM4'"
            Try
                wtc = pmf.Scalar(sql)
            Catch ex As Exception
                wtc = 0
            End Try
            If wtc > 0 Then
                wt = "PM4"
            Else
                wt = "CM"
            End If
        Else
            wt = "CM"
        End If
        skillid = ddskill.SelectedValue.ToString
        skill = ddskill.SelectedItem.ToString
        Dim usr As String = HttpContext.Current.Session("username").ToString
        comid = lblcomid.Value
        sql = "select func_id from components where comid = '" & comid & "'"
        funcid = pmf.Scalar(sql)
        sql = "insert into workorder (status, statusdate, changeby, changedate, reportedby, reportdate, worktype, pmfid, eqid, " _
        + "eqnum, siteid, deptid, cellid, locid, funcid, comid, chargenum, targstartdate, leadcraftid, leadcraft, leadealert, " _
        + "superid, supervisor, supealert, skillid, skill) " _
      + "values ('WAPPR', getDate(),'" & usr & "', getDate(),'" & usr & "', getDate(), '" & wt & "' ,'" & pmfid & "','" & eqid & "', " _
      + "'" & eqnum & "','" & sid & "','" & deptid & "','" & cellid & "','" & locid & "','" & funcid & "','" & comid & "','" & chrg & "', " _
      + "" & start & ",'" & leadid & "','" & lead & "','" & le & "','" & superid & "','" & super & "','" & se & "', " _
      + "'" & skillid & "','" & skill & "') " _
      + "select @@identity"
        wonum = pmf.Scalar(sql)
        lblwonum.Text = wonum
        lblwo.Value = wonum
        sql = "update pmfail set wonum = '" & wonum & "' where pmfid = '" & pmfid & "'"
        pmf.Update(sql)
        'Insert Failure Mode to wofail - use sp
        fmid = lblfmid.Value
        fm = lblfm.Value
        sql = "usp_addWoFailureMode " & wonum & ", " & fmid & ", '" & fm & "', '" & comid & "','hascomp'"
        pmf.Update(sql)
        sql = "insert into wocorr (wctype, wonum, problem, corraction) values ('fail','" & wonum & "','" & txtprob.Text & "','" & txtcorr.Text & "')"
        pmf.Update(sql)
        desc = "Corrective Action for Equipment# " & eqnum
        SaveDesc(desc)
        Dim mail As New pmmail
        If cbsupe.Checked = True Then
            'SendIt("sup", superid, wonum, desc, start)
            Dim mail1 As New pmmail
            mail1.CheckIt("sup", superid, wonum)
        End If
        If cbleade.Checked = True Then
            'SendIt("lead", leadid, wonum, desc, start)
            Dim mail2 As New pmmail
            mail2.CheckIt("lead", leadid, wonum)
        End If
    End Sub
    Private Sub SendIt(ByVal typ As String, ByVal uid As String, ByVal wonum As String, ByVal desc As String, ByVal start As String)
        Dim email As New System.Web.Mail.MailMessage

        Dim lid, pas, ema As String
        lid = "test"
        pas = "test"

        email.To = "chuck.rennie@adelphia.net"
        email.From = "system_admin@laisoftware.com"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "mail.laisoftware.com"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = "system_admin"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "sysadm1"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
        email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = 25

        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If


        email.Body = AlertBody(wonum, desc, start)
        email.Subject = "New Work Order#  " + wonum
        email.BodyFormat = Web.Mail.MailFormat.Html
        System.Web.Mail.SmtpMail.SmtpServer.Insert(0, "mail.laisoftware.com")
        System.Web.Mail.SmtpMail.Send(email)
    End Sub
    Private Function AlertBody(ByVal wo As String, ByVal desc As String, ByVal start As String) As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrle")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim body As String
        body = "<table width='800px' style='font-size:8pt; font-family:Verdana;'>"

        body &= "<tr><td>" & tmod.getlbl("cdlbl60", "PMFailMan.aspx.vb") & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Work Order #  " & wo & "<br><br></td></tr>" & vbCrLf
        body &= "<tr><td>Description:  " & desc & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Target Start: " & start & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getlbl("cdlbl61", "PMFailMan.aspx.vb") & "<a href='" + urlname + appname + "/appswo/woprint.aspx?typ=wo&wo=" + wo + "'>Work Order# " + wo + "</td></tr>" & vbCrLf & vbCrLf
        body &= "</table>"

        Return body
    End Function
    Private Sub SaveDesc(ByVal lg As String)
        Dim wonum As String = lblwo.Value
        Dim sh As String
        Dim lgcnt As Integer
        Dim test As String = lg
        If Len(lg) > 79 Then
            sh = Mid(lg, 1, 79)
            sql = "update workorder set description = '" & sh & "' where wonum = '" & wonum & "'"
            pmf.Update(sql)
            lg = Mid(lg, 80)
            sql = "select count(*) from wolongdesc where wonum = '" & wonum & "'"
            lgcnt = pmf.Scalar(sql)
            If lgcnt <> 0 Then
                sql = "update wolongdesc set longdesc = '" & lg & "' where wonum = '" & wonum & "'"
                pmf.Update(sql)
            Else
                sql = "insert into wolongdesc (wonum, longdesc) values ('" & wonum & "','" & lg & "')"
                pmf.Update(sql)
            End If
        Else
            sql = "update workorder set description = '" & lg & "' where wonum = '" & wonum & "'" _
            + "delete from wolongdesc where wonum = '" & wonum & "'"
            pmf.Update(sql)
        End If

    End Sub
    Private Sub GetFail(ByVal pmtid As String, ByVal fmid As String)
        Dim wochk, lchk As String
        sql = "select * from pmfail where pmtid = '" & pmtid & "' and failid = '" & fmid & "'"
        dr = pmf.GetRdrData(sql)
        While dr.Read
            txtprob.Text = dr.Item("problem").ToString
            txtcorr.Text = dr.Item("corraction").ToString
            lblwo.Value = dr.Item("wonum").ToString
            lblwonum.Text = dr.Item("wonum").ToString
            lblcomid.Value = dr.Item("comid").ToString
            'lchk = dr.Item("saveforgen").ToString
        End While
        dr.Close()
        If lblwo.Value <> "" Then
            cbcwo.Checked = True
            iwoadd.Attributes.Add("class", "details")
            imgi2.Attributes.Add("class", "view")
            GetWo(lblwo.Value)
        Else
            iwoadd.Attributes.Add("class", "view")
            imgi2.Attributes.Add("class", "details")
        End If
        'If lchk <> "0" Then
        'cbeqr.Checked = True
        'End If

    End Sub
    Private Sub GetWo(ByVal wo As String)
        sql = "select skillid, targstartdate, superid, supervisor, supealert, leadcraftid, leadcraft, leadealert from workorder where wonum = '" & wo & "'"
        Dim skillid, se, le As String
        dr = pmf.GetRdrData(sql)
        While dr.Read
            skillid = dr.Item("skillid").ToString
            If skillid <> "" Then
                Try
                    ddskill.SelectedValue = skillid
                Catch ex As Exception

                End Try
                txtstart.Text = dr.Item("targstartdate").ToString
                txtsup.Text = dr.Item("supervisor").ToString
                txtlead.Text = dr.Item("leadcraft").ToString
                lblsup.Value = dr.Item("superid").ToString
                lbllead.Value = dr.Item("leadcraftid").ToString
                le = dr.Item("leadealert").ToString
                se = dr.Item("supealert").ToString
                If le = "1" Then
                    cbleade.Checked = True
                End If
                If se = "1" Then
                    cbsupe.Checked = True
                End If
            End If
        End While
        dr.Close()
    End Sub
    Private Sub SaveChanges()
        pmfid = lblpmfid.Value
        Dim prob, corr, cmid, cmfid, compnum As String
        prob = txtprob.Text
        prob = pmf.ModString2(prob)
        corr = txtcorr.Text
        corr = pmf.ModString2(corr)

        If pmfid <> "" Then
            sql = "update pmfail set problem = '" & prob & "', corraction = '" & corr & "', faildate = getDate() where pmfid= '" & pmfid & "'"
            pmf.Update(sql)
        Else
            Dim ret As Integer
            pmid = lblpmid.Value
            pmhid = lblpmhid.Value
            pmtid = lblpmtid.Value
            Dim fm, fmid As String
            fm = lblfm.Value
            fmid = lblfmid.Value
            cmid = lblcomid.Value
            sql = "select compfailid from componentfailmodes where comid = '" & comid & "' and failid = '" & fmid & "'"
            dr = pmf.GetRdrData(sql)
            While dr.Read
                cmfid = dr.Item("compfailid").ToString
            End While
            dr.Close()
            sql = "select compnum from components where comid = '" & comid & "'"
            dr = pmf.GetRdrData(sql)
            While dr.Read
                compnum = dr.Item("compnum").ToString
            End While
            dr.Close()
            sql = "insert into pmfail (pmid, pmtid, pmhid, problem, corraction, faildate, failid, failuremode, comid, compfailid, compnum) values " _
            + "('" & pmid & "', '" & pmtid & "', '" & pmhid & "', '" & prob & "', '" & corr & "', getDate(), " _
            + "'" & fmid & "','" & fm & "','" & cmid & "','" & cmfid & "','" & compnum & "') select @@identity as 'identity'"
            ret = pmf.Scalar(sql)
            lblpmfid.Value = ret

        End If


    End Sub
    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        pmf.Open()
        SaveChanges()
        pmtid = lblpmtid.Value
        fmid = lblfmid.Value
        GetFail(pmtid, fmid)
        pmf.Dispose()
        lblsave.Value = "yes"
    End Sub












    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label1.Text = axlabs.GetASPXPage("PMFailMan.aspx", "Label1")
        Catch ex As Exception
        End Try
        Try
            Label2.Text = axlabs.GetASPXPage("PMFailMan.aspx", "Label2")
        Catch ex As Exception
        End Try
        Try
            Label3.Text = axlabs.GetASPXPage("PMFailMan.aspx", "Label3")
        Catch ex As Exception
        End Try
        Try
            lang605.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang605")
        Catch ex As Exception
        End Try
        Try
            lang606.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang606")
        Catch ex As Exception
        End Try
        Try
            lang607.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang607")
        Catch ex As Exception
        End Try
        Try
            lang608.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang608")
        Catch ex As Exception
        End Try
        Try
            lang609.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang609")
        Catch ex As Exception
        End Try
        Try
            lang610.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang610")
        Catch ex As Exception
        End Try
        Try
            lang611.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang611")
        Catch ex As Exception
        End Try
        Try
            lang612.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang612")
        Catch ex As Exception
        End Try
        Try
            lang613.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang613")
        Catch ex As Exception
        End Try
        Try
            lang614.Text = axlabs.GetASPXPage("PMFailMan.aspx", "lang614")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnwo.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnwo.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnwo.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnwo.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnwo.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMFailMan.aspx", "Img2") & "', ABOVE, LEFT)")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgi2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMFailMan.aspx", "imgi2") & "', ABOVE, LEFT)")
            imgi2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            iwoadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMFailMan.aspx", "iwoadd") & "')")
            iwoadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
       

    End Sub
End Class