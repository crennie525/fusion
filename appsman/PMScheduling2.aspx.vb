

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMScheduling2
    Inherits System.Web.UI.Page
    Protected WithEvents lang698 As System.Web.UI.WebControls.Label

    Protected WithEvents lang697 As System.Web.UI.WebControls.Label

    Protected WithEvents lang696 As System.Web.UI.WebControls.Label

    Protected WithEvents lang695 As System.Web.UI.WebControls.Label

    Protected WithEvents lang694 As System.Web.UI.WebControls.Label

    Protected WithEvents lang693 As System.Web.UI.WebControls.Label

    Protected WithEvents lang692 As System.Web.UI.WebControls.Label

    Protected WithEvents lang691 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim ga As New Utilities
    Dim proj, qrtr, qrtrh, sid, dt, val, filt, eq, rd, rds, pdm, pdms, jump, opt, typ, href, pmid, ro As String
    Dim wo, uid, username, islabor, isplanner, issuper, Logged_In, ms, appstr, curlang, psite As String
    Dim yr As Integer
    Protected WithEvents lblhref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqidret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents sret As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents ddtyp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents A1 As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents tropts As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblsopts As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ECC1 As ECCTPM35.Scheduler.ECCTPM35
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmcurr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtot As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgnav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddqtr As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddopts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddpdm As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddpdmlist As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblqrtr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblyear As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpdm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpdms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim Login As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            lblpb.Value = "0"
            txtpg.Value = "1"
            Logged_In = Request.QueryString("Logged_In").ToString
            lblLogged_In.Value = Logged_In
            ro = Request.QueryString("ro").ToString
            lblro.Value = ro
            ms = Request.QueryString("ms").ToString
            lblms.Value = ms
            appstr = Request.QueryString("appstr").ToString
            lblappstr.Value = appstr
            If ro = "1" Then
                'pgtitle.InnerHtml = "PM Scheduler (Read Only)"
            Else
                'pgtitle.InnerHtml = "PM Scheduler"
            End If
            sid = Request.QueryString("sid").ToString '"12"
            lblsid.Value = sid
            Try
                uid = Request.QueryString("uid").ToString
            Catch ex As Exception
                uid = Request.QueryString("userid").ToString
            End Try
            lbluid.Value = uid
            Try
                username = Request.QueryString("usrname").ToString
            Catch ex As Exception
                username = Request.QueryString("username").ToString
            End Try
            lblusername.Value = username
            islabor = Request.QueryString("islabor").ToString
            lblislabor.Value = islabor
            issuper = Request.QueryString("issuper").ToString
            lblissuper.Value = issuper
            isplanner = Request.QueryString("isplanner").ToString
            lblisplanner.Value = isplanner
            Try
                wo = Request.QueryString("wo").ToString
            Catch ex As Exception
                wo = ""
            End Try

            lblwonum.Value = wo
            jump = Request.QueryString("jump").ToString
            typ = Request.QueryString("typ").ToString
            lblsopts.Value = "0"
            lbltyp.Value = typ
            If typ = "pm" Then
                eq = Request.QueryString("eqid").ToString
                lbleqidret.Value = eq
                pmid = Request.QueryString("pmid").ToString
                lblpmid.Value = pmid
                sret.InnerHtml = "Return to PM Manager"
                If eq = "0" Then
                    sret.Attributes.Add("href", "PMManager.aspx?jump=no" + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr)
                Else
                    sret.Attributes.Add("href", "PMManager.aspx?jump=yes&typ=no&eqid=" + eq + "&pmid=" + pmid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr)
                End If
            ElseIf typ = "tpm" Then
                eq = Request.QueryString("eqid").ToString
                lbleqidret.Value = eq
                pmid = Request.QueryString("pmid").ToString
                lblpmid.Value = pmid
                sret.InnerHtml = "Return to TPM Manager"
                If eq = "0" Then
                    sret.Attributes.Add("href", "../appsmantpm/TPMManager.aspx?jump=no" + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr)
                Else
                    sret.Attributes.Add("href", "../appsmantpm/TPMManager.aspx?jump=yes&typ=no&eqid=" + eq + "&pmid=" + pmid + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr)
                End If
            ElseIf typ = "wo" Then
                href = Request.QueryString("href").ToString
                lblhref.Value = href
                'href = Replace(href, "&type=", "&typ=")
                href = Replace(href, "~", "&")
                sret.InnerHtml = "Return to Work Manager"
                href = "../appswo/" & href
                sret.Attributes.Add("href", href)
            ElseIf typ = "main" Then
                sret.InnerHtml = "Return to PM (Main Menu)"

                sret.Attributes.Add("href", "../mainmenu/NewMainMenu2.aspx?sid=" + sid + "&userid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&Logged_In=" + Logged_In + "&curlang=" + curlang + "&Logged_In=" + Logged_In + "&ms=" + ms + "&psite=" + psite)
            End If
            ga.Open()
            Dim mnth As Integer
            Try
                mnth = Now.Month
            Catch ex As Exception
                Try
                    mnth = Month(Now)
                Catch ex1 As Exception
                    mnth = 0
                End Try
            End Try

            If mnth > 0 And mnth < 13 Then
                qrtr = ga.Qtr(mnth)
            Else
                sql = "declare @qtr int " _
                + "set @qtr = 0 " _
                + "set @qtr = (select qtr =  " _
                + "case when month(getdate()) in (10, 11, 12) then 4 " _
                + "when month(getdate()) in (7, 8, 9) then 3 " _
                + "when month(getdate()) in (4, 5, 6) then 2 " _
                + "when month(getdate()) in (1, 2, 3) then 1 " _
                + "end) " _
                + "select @qtr"
                qrtr = ga.strScalar(sql)
            End If
            If qrtr <> "0" And qrtr <> "" Then
                lblqrtr.Value = qrtr
            Else
                Dim strMessage1 As String = "Problem getting Date from server"
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
            Try
                yr = Year(Now)
            Catch ex As Exception
                Try
                    yr = Now.Year
                Catch ex1 As Exception
                    yr = 0
                End Try
            End Try

            If yr = 0 Then
                sql = "declare @yr int " _
                + "set @yr = (select datepart(yy, getdate())) " _
                + "select @yr"
                Try
                    yr = ga.Scalar(sql)
                Catch ex As Exception
                    yr = 0
                End Try
            End If
            If yr <> 0 Then
                lblyear.Value = yr
            Else
                Dim strMessage1 As String = "Problem getting Date from server"
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If

            If jump = "yes" Then
                'If typ = "pm" Then
                eq = Request.QueryString("eqid").ToString
                'Else
                'eq = Request.QueryString("eq").ToString
                'End If
                lbleqid.Value = eq
            Else
                eq = "0"
                lbleqid.Value = eq
            End If
            'ga.Open()
            GetQtrs()
            PopEq(sid)
            ddqtr.SelectedValue = qrtr
            If eq = "0" Then
                Try
                    ddeq.SelectedIndex = 0
                Catch ex As Exception

                End Try

            Else
                Try
                    ddeq.SelectedValue = eq
                Catch ex As Exception
                    Try
                        ddeq.SelectedIndex = 0
                    Catch ex1 As Exception

                    End Try

                End Try

            End If
            GetSkills()
            GetPdM()
            GetDept()
            Try
                ddopts.SelectedIndex = 0
            Catch ex As Exception

            End Try

            lblopt.Value = ddopts.SelectedValue
            Try
                ddskill.SelectedIndex = 0
            Catch ex As Exception

            End Try

            lblskill.Value = ddskill.SelectedValue
            Try
                ddpdm.SelectedIndex = 0
            Catch ex As Exception

            End Try

            lblpdm.Value = "0"
            Try
                dddepts.SelectedIndex = 0
            Catch ex As Exception

            End Try

            lbldept.Value = "0"
            lblcell.Value = "0"
            ddcells.Items.Insert(0, "All Cells")
            Try
                ddcells.SelectedIndex = 0
            Catch ex As Exception

            End Try
            txtpg.Value = PageNumber
            ddcells.Enabled = False
            GetGantt(qrtr, yr, eq, sid, PageNumber)
            ga.Dispose()
        Else
            lblpb.Value = "1"
            Try
                If Request.Form("lblpost") = "ret" Then
                    lblpost.Value = ""
                    qrtr = lblqrtr.Value
                    yr = lblyear.Value
                    eq = lbleqid.Value
                    sid = lblsid.Value
                    opt = lblopt.Value
                    ga.Open()
                    GetQtrs()
                    PopEq(sid)
                    GetSkills()
                    GetPdM()
                    If lblskill.Value <> "" Then
                        ddskill.SelectedValue = lblskill.Value
                    End If
                    ddopts.SelectedValue = lblopt.Value
                    ddpdm.SelectedValue = lblpdm.Value
                    ddqtr.SelectedValue = qrtr
                    If eq = "0" Then
                        Try
                            ddeq.SelectedIndex = 0
                        Catch ex As Exception

                        End Try

                    Else
                        Try
                            ddeq.SelectedValue = eq
                        Catch ex As Exception

                        End Try


                    End If
                    PageNumber = txtpg.Value
                    GetGantt(qrtr, yr, eq, sid, PageNumber)
                    ga.Dispose()
                ElseIf Request.Form("lblpost") = "next" Then
                    ga.Open()
                    GetNext()
                    ga.Dispose()
                    lblpost.Value = ""
                ElseIf Request.Form("lblpost") = "last" Then
                    ga.Open()
                    PageNumber = txtpgcnt.Value
                    txtpg.Value = PageNumber
                    qrtr = lblqrtr.Value
                    yr = lblyear.Value
                    eq = lbleqid.Value
                    sid = lblsid.Value
                    GetGantt(qrtr, yr, eq, sid, PageNumber)
                    ga.Dispose()
                    lblpost.Value = ""
                ElseIf Request.Form("lblpost") = "prev" Then
                    ga.Open()
                    GetPrev()
                    ga.Dispose()
                    lblpost.Value = ""
                ElseIf Request.Form("lblpost") = "first" Then
                    ga.Open()
                    PageNumber = 1
                    txtpg.Value = PageNumber
                    qrtr = lblqrtr.Value
                    yr = lblyear.Value
                    eq = lbleqid.Value
                    sid = lblsid.Value
                    GetGantt(qrtr, yr, eq, sid, PageNumber)
                    ga.Dispose()
                    lblpost.Value = ""
                End If
            Catch ex As Exception

            End Try

        End If

    End Sub
    Private Sub GetQtrs()
        sql = "usp_getqtrs"
        dr = ga.GetRdrData(sql)
        ddqtr.DataSource = dr
        ddqtr.DataValueField = "qtrval"
        ddqtr.DataTextField = "qtrstr"
        ddqtr.DataBind()
        dr.Close()
        ddqtr.Items.Insert(0, "Select QTR")

    End Sub
    Private Sub GetDept()
        sid = lblsid.Value
        eq = lbleqid.Value
        sql = "select d.dept_id, d.dept_line from dept d where d.siteid = '" & sid & "'"
        dr = ga.GetRdrData(sql)
        dddepts.DataSource = dr
        dddepts.DataValueField = "dept_id"
        dddepts.DataTextField = "dept_line"
        dddepts.DataBind()
        dr.Close()
        dddepts.Items.Insert(0, "All Departments")
        Try
            dddepts.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetCell()
        sid = lblsid.Value
        Dim dept As String = lbldept.Value
        sql = "select c.cellid, c.cell_name from cells c where c.dept_id = '" & dept & "'"
        dr = ga.GetRdrData(sql)
        ddcells.DataSource = dr
        ddcells.DataValueField = "cellid"
        ddcells.DataTextField = "cell_name"
        ddcells.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddcells.Items.Insert(0, "All Cells")
        Try
            ddcells.SelectedIndex = 0
        Catch ex As Exception

        End Try

        ddcells.Enabled = True
    End Sub
    Private Sub GetPdM()
        sid = lblsid.Value
        eq = lbleqid.Value
        If eq <> "0" Then
            sql = "select distinct pm.ptid, pm.pretech from equipment e " _
            + "right join pm pm on pm.eqid = e.eqid " _
            + "where e.siteid = '" + sid + "' and ptid <> 0 and e.eqid = '" & eq & "'"
        Else
            sql = "select distinct pm.ptid, pm.pretech from equipment e " _
            + "right join pm pm on pm.eqid = e.eqid " _
            + "where e.siteid = '" + sid + "' and ptid <> 0"
        End If
        dr = ga.GetRdrData(sql)
        ddpdmlist.DataSource = dr
        ddpdmlist.DataValueField = "ptid"
        ddpdmlist.DataTextField = "pretech"
        ddpdmlist.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddpdmlist.Items.Insert(0, "All Skills")
        Try
            ddpdmlist.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetSkills()
        sql = "select distinct p.skillid, p.skill from pm p left join equipment e on e.eqid = p.eqid"
        dr = ga.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataValueField = "skillid"
        ddskill.DataTextField = "skill"
        ddskill.DataBind()
        dr.Close()
        'ddskill.Items.Insert(0, "Select Skill")
        ddskill.Items.Insert(0, "All Skills")
        Try
            ddskill.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub PopEq(ByVal sid As String, Optional ByVal dept As String = "0", Optional ByVal cell As String = "0")
        Dim eqcnt As Integer
        If dept = "0" Then
            dept = "%"
        End If
        If cell = "0" Then
            cell = "%"
        End If
        sql = "select count(*) from equipment where siteid = '" & sid & "' and eqid in (select eqid from pm where siteid = '" & sid & "' " _
        + "and isnull(dept_id, '') like '" & dept & "' and isnull(cellid, '') like '" & cell & "')"
        filt = " where siteid = '" & sid & "' and eqid in (select eqid from pm where siteid = '" & sid & "' and isnull(dept_id, '') like '" & dept & "')"
        eqcnt = ga.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "
            dr = ga.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "All Site Assets")
            ddeq.Enabled = True
            If dept <> "%" Then
                ddeq.Items.Insert(1, "All Department Assets")
                eq = lbleqid.Value
                If eq = "0" Then
                    ddeq.SelectedIndex = 1
                Else
                    ddeq.SelectedValue = eq
                End If
            End If
        Else
            Dim strMessage As String = " No PM Records Activated for the Selected Filter Values"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            ddeq.Enabled = False
        End If
    End Sub
    Private Sub GetNext()
        qrtr = lblqrtr.Value
        yr = lblyear.Value
        eq = lbleqid.Value
        sid = lblsid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetGantt(qrtr, yr, eq, sid, PageNumber)
        Catch ex As Exception
            ga.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr340", "PMMainMan.aspx.vb")

            ga.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        qrtr = lblqrtr.Value
        yr = lblyear.Value
        eq = lbleqid.Value
        sid = lblsid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetGantt(qrtr, yr, eq, sid, PageNumber)
        Catch ex As Exception
            ga.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr341", "PMMainMan.aspx.vb")

            ga.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetGantt(ByVal qrtr As String, ByVal year As String, ByVal eq As String, ByVal sid As Integer, ByVal PageNumber As Integer)
        'running only
        'down only
        'PdM only
        'no PdM
        Dim pdmsk, pdmski As String
        Try
            Dim xldata As String
            Dim con As SqlConnection
            Dim DA As SqlDataAdapter
            con = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("strConn"))
            Dim eqgrp As String
            If eq = "0" Then
                eqgrp = "%"
            Else
                eqgrp = eq
            End If

            Dim dept, deptgrp As String
            If dddepts.SelectedIndex = 0 Then
                dept = "0"
                deptgrp = "%"
            Else
                dept = dddepts.SelectedValue.ToString
                deptgrp = dddepts.SelectedValue.ToString
            End If

            Dim cell, cellgrp As String
            If ddcells.SelectedIndex = 0 Or ddcells.SelectedIndex = -1 Then
                cell = "0"
                cellgrp = "%"
            Else
                cell = ddcells.SelectedValue.ToString
                cellgrp = ddcells.SelectedValue.ToString
            End If

            Dim skill, skillgrp As String
            If ddskill.SelectedIndex = 0 Then
                skill = ""
                skillgrp = "%"
            Else
                skill = ddskill.SelectedValue.ToString
                skillgrp = ddskill.SelectedValue.ToString
            End If
            rds = ddopts.SelectedValue.ToString
            Dim typ As String
            'typ = "all"
            typ = ddtyp.SelectedValue.ToString

            Dim DS As New DataSet
            Dim mm As Integer = ga.MaxMnth(lblqrtr.Value)
            Dim mn As Integer = ga.MinMnth(lblqrtr.Value)
            If ddpdm.SelectedIndex = 0 Then
                'If eq = "0" Then
                'DA = New SqlDataAdapter("select distinct skillid as groupid, skill as 'name', 'red' as blockcolor from pm where eqid like '%' and nextdate is not null and skill like '" & skillgrp & "' " _
                '+ "and rd like '" & rds & "' and ptid = 0", con)
                'Else
                sql = "usp_getschedgrps9a '" & mm & "','" & mn & "','" & eqgrp & "','" & deptgrp & "','" & cellgrp & "','" & skillgrp & "','" & rds & "','0','" & sid & "','" & typ & "'"
                Dim pickup1 As String = sql
                DA = New SqlDataAdapter("usp_getschedgrps9a '" & mm & "','" & mn & "','" & eqgrp & "','" & deptgrp & "','" & cellgrp & "','" & skillgrp & "','" & rds & "','0','" & sid & "','" & typ & "'", con)


                pdms = "0"
                pdmsk = "0"
            Else
                If ddpdmlist.SelectedIndex = 0 Then
                    pdmsk = "%"
                    pdmski = "0"
                Else
                    pdmsk = ddpdmlist.SelectedValue.ToString
                    pdmski = ddpdmlist.SelectedValue.ToString
                End If
                'If eq = "0" Then
                'DA = New SqlDataAdapter("select distinct ptid as groupid, pretech + ' - ' + skill as 'name', 'red' as blockcolor from pm where eqid like '%' and nextdate is not null and skill like '" & skillgrp & "' " _
                '+ "and rd like '" & rds & "' and ptid <> 0", con)
                'Else
                sql = "usp_getschedgrpsa '" & mm & "','" & mn & "','" & eqgrp & "','" & deptgrp & "','" & cellgrp & "','" & skillgrp & "','" & rds & "','" & pdmsk & "','" & sid & "'"
                Dim pickup2 As String = sql
                DA = New SqlDataAdapter("usp_getschedgrps9 '" & mm & "','" & mn & "','" & eqgrp & "','" & deptgrp & "','" & cellgrp & "','" & skillgrp & "','" & rds & "','" & pdmsk & "','" & sid & "','" & typ & "'", con)

                'sql = "select distinct cast((cast(pm.ptid as varchar(10)) + cast(pm.skillid as varchar(10))) as int) as groupid, pm.pretech + ' - ' + skill as 'name', 'red' as blockcolor from equipment e " _
                '+ "right join pm pm on pm.eqid = e.eqid where e.eqid like '" & eqgrp & "' and e.dept_id like '" & deptgrp & "' " _
                '+ "and e.cellid like '" & cellgrp & "' and pm.nextdate is not null and pm.skill like '" & skillgrp & "' " _
                '+ "and pm.rd like '" & rds & "' and (pm.ptid <> 0 and pm.ptid like '" & pdmsk & "')"
                'DA = New SqlDataAdapter("select distinct cast((cast(pm.ptid as varchar(10)) + cast(pm.skillid as varchar(10))) as int) as groupid, pm.pretech + ' - ' + skill as 'name', 'red' as blockcolor from equipment e " _
                '+ "right join pm pm on pm.eqid = e.eqid where e.eqid like '" & eqgrp & "' and e.dept_id like '" & deptgrp & "' " _
                '+ "and e.cellid like '" & cellgrp & "' and pm.nextdate is not null and pm.skill like '" & skillgrp & "' " _
                '+ "and pm.rd like '" & rds & "' and (pm.ptid <> 0 and pm.ptid like '" & pdmsk & "')", con)

                'DA = New SqlDataAdapter("select distinct ptid as groupid, pretech + ' - ' + skill as 'name', 'red' as blockcolor from pm where eqid like '" & eqgrp & "' and nextdate is not null and skill like '" & skillgrp & "' " _
                '+ "and rd like '" & rds & "' and (ptid <> 0 and ptid like '" & pdmsk & "'", con)
                'End If
                pdms = "1"
            End If
            year = lblyear.Value
            Try
                DA.Fill(DS, "pmMaster")
            Catch ex As Exception
                Dim strMessage1 As String = " No Records Found"
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End Try

            Dim dt As New DataTable
            dt = New DataTable
            dt = DS.Tables(0)
            If dt.Rows.Count > 0 Then
                sql = "usp_getsched8c '" & eq & "', '" & qrtr & "', '" & sid & "', '" & skill & "', '" & rds & "', '" & pdms & "', '" & pdmski & "', '" & dept & "', '" & cell & "', '" & mm & "', '" & mn & "','" & year & "','" & typ & "','" & PageNumber & "','" & PageSize & "'"
                Dim pickup3 As String = sql
                DA = New SqlDataAdapter("usp_getsched8c '" & eq & "', '" & qrtr & "', '" & sid & "', '" & skill & "', '" & rds & "', '" & pdms & "', '" & pdmski & "', '" & dept & "', '" & cell & "', '" & mm & "', '" & mn & "','" & year & "','" & typ & "','" & PageNumber & "','" & PageSize & "'", con)
                DA.Fill(DS, "pmGantt")

                Dim pmMasterRel As DataRelation = DS.Relations.Add("pmMasterTasks", _
                                DS.Tables("pmMaster").Columns("groupid"), _
                                DS.Tables("pmGantt").Columns("groupid"))
                DS.Tables(0).TableName = "group"
                DS.Tables(1).TableName = "block"
                Dim intPgCnt, intPgNav, intPgNav1 As Integer

                'If PageNumber = 1 Then
                'intPgNav = 0
                Dim dtpg As New DataTable
                dtpg = DS.Tables(1)
                If dtpg.Rows.Count > 0 Then
                    Dim pmcurr, pmtot, ocurr, ccurr, chkint, pgs As Integer
                    Dim tmin As Integer = 0
                    Dim row As DataRow
                    For Each row In dtpg.Rows
                        pmcurr = row("pmcurr").ToString
                        pmtot = row("pmtot").ToString
                        pgs = row("pgcnt").ToString
                        Exit For
                    Next
                    lblpmtot.Value = pmtot

                    If PageNumber = 1 Then
                        'lblpmcurr.Value = pmcurr
                        intPgNav = pgs 'ga.PageCountRev(pmtot, pmcurr)
                        'lblpgnav.Value = pmcurr
                    Else
                        'ocurr = lblpmcurr.Value
                        'lblpgnav.Value = ocurr + pmcurr
                        'If pmcurr < ocurr Or pmcurr > ocurr Then
                        'intPgNav = ga.PageCountRev(pmtot, pmcurr)
                        'need to check for number of records left
                        intPgNav = txtpgcnt.Value
                        'Else

                        'intPgNav = ga.PageCountRev(pmtot, pmcurr)
                    End If
                    'intPgNav = ga.PageCountRev(pmtot, pmcurr)
                    'End If

                    txtpgcnt.Value = intPgNav
                    'Else

                    'End If
                    If intPgNav = 0 Then
                        lblpg.Text = "Page 0 of 0"
                    Else
                        lblpg.Text = "Page " & PageNumber & " of " & intPgNav
                    End If

                    pmMasterRel.Nested = True


                    Try
                        xldata = DS.GetXml()
                        ECC1.XMLData = xldata
                        ECC1.BlankGifPath = "trans.gif"
                        ECC1.Year = year
                        ECC1.Quarter = qrtr
                        ECC1.BlockColor = "green"
                        ECC1.ToggleColor = "#05C7FA" '05C7FA
                        ECC1.WToggleColor = "#37F903"
                        ECC1.WPToggleColor = "#FBD005"
                        ECC1.TPMToggleColor = "#E4F807"
                        ECC1.CellHeight = 15
                        ECC1.CellWidth = 20
                    Catch ex As Exception
                        Dim strMessage2 As String = " No Records Found"
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                    End Try
                Else
                    Try
                        xldata = DS.GetXml()
                        ECC1.XMLData = xldata
                        ECC1.BlankGifPath = "trans.gif"
                        ECC1.Year = year
                        ECC1.Quarter = qrtr
                        ECC1.BlockColor = "green"
                        ECC1.ToggleColor = "#05C7FA" '05C7FA
                        ECC1.WToggleColor = "#37F903"
                        ECC1.WPToggleColor = "#FBD005"
                        ECC1.TPMToggleColor = "#E4F807"
                        ECC1.CellHeight = 15
                        ECC1.CellWidth = 20
                    Catch ex As Exception
                        Dim strMessage2 As String = " No Records Found"
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                    End Try
                    Dim strMessage3 As String = " No Records Found"
                    Utilities.CreateMessageAlert(Me, strMessage3, "strKey1")
                End If


            Else
                Try
                    xldata = DS.GetXml()
                    ECC1.XMLData = xldata
                    ECC1.BlankGifPath = "trans.gif"
                    ECC1.Year = year
                    ECC1.Quarter = qrtr
                    ECC1.BlockColor = "green"
                    ECC1.ToggleColor = "#05C7FA" '05C7FA
                    ECC1.WToggleColor = "#37F903"
                    ECC1.WPToggleColor = "#FBD005"
                    ECC1.TPMToggleColor = "#E4F807"
                    ECC1.CellHeight = 15
                    ECC1.CellWidth = 20
                Catch ex As Exception
                    Dim strMessage2 As String = " No Records Found"
                    Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                End Try
                Dim strMessage As String = " No Records Found"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        Catch ex As Exception
            Dim strMessage As String = " No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try


    End Sub

    Private Sub ddqtr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddqtr.SelectedIndexChanged
        Dim cell, dept As String
        If ddeq.SelectedIndex = 0 Then
            eq = 0
            lbldept.Value = "0"
            lblcell.Value = "0"
        ElseIf dddepts.SelectedIndex <> 0 And ddeq.SelectedIndex = 1 Then
            eq = 0
        Else
            eq = ddeq.SelectedValue.ToString
        End If
        dept = lbldept.Value
        cell = lblcell.Value
        qrtr = ddqtr.SelectedValue
        sid = lblsid.Value
        yr = Mid(ddqtr.SelectedItem.Text, 1, 4) '"2008"
        lblyear.Value = yr
        Try
            ga.Open()
        Catch ex As Exception

        End Try
        GetQtrs()
        PopEq(sid, dept, cell)
        ddqtr.SelectedValue = qrtr
        lblqrtr.Value = qrtr
        If eq = "0" Then
            If dddepts.SelectedIndex = 0 Then
                Try
                    ddeq.SelectedIndex = 0
                Catch ex As Exception

                End Try

            Else
                Try
                    ddeq.SelectedIndex = 1
                Catch ex As Exception

                End Try

            End If
        Else
            ddeq.SelectedValue = eq
        End If
        If dept = "0" Then
            Try
                dddepts.SelectedIndex = 0
            Catch ex As Exception

            End Try
            Try
                ddcells.SelectedIndex = 0
            Catch ex As Exception

            End Try

            ddcells.Enabled = False
        Else
            dddepts.SelectedValue = dept
            If cell = "0" Then
                Try
                    ddcells.SelectedIndex = 0
                Catch ex As Exception

                End Try

            Else
                Try
                    ddcells.SelectedValue = cell
                Catch ex As Exception

                End Try

            End If
        End If
        PageNumber = txtpg.Value
        GetGantt(qrtr, yr, eq, sid, PageNumber)
        ga.Dispose()
    End Sub

    Private Sub ddeq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddeq.SelectedIndexChanged
        Dim cell, dept As String
        If ddeq.SelectedIndex = 0 Then
            eq = 0
            lbldept.Value = "0"
            lblcell.Value = "0"
        ElseIf dddepts.SelectedIndex <> 0 And ddeq.SelectedIndex = 1 Then
            eq = 0
        Else
            eq = ddeq.SelectedValue.ToString
        End If
        lbleqid.Value = eq
        qrtr = ddqtr.SelectedValue
        sid = lblsid.Value
        yr = Mid(ddqtr.SelectedItem.Text, 1, 4) '"2008"
        Try
            ga.Open()
        Catch ex As Exception

        End Try

        GetQtrs()
        dept = lbldept.Value
        cell = lblcell.Value
        PopEq(sid, dept, cell)
        Try
            ddqtr.SelectedValue = qrtr
        Catch ex As Exception

        End Try

        lblqrtr.Value = qrtr
        If eq = "0" Then
            If dddepts.SelectedIndex = 0 Then
                Try
                    ddeq.SelectedIndex = 0
                Catch ex As Exception

                End Try

            Else
                Try
                    ddeq.SelectedIndex = 1
                Catch ex As Exception

                End Try

            End If
        Else
            ddeq.SelectedValue = eq
        End If
        If dept = "0" Then
            Try
                dddepts.SelectedIndex = 0
            Catch ex As Exception

            End Try
            Try
                ddcells.SelectedIndex = 0
            Catch ex As Exception

            End Try

            ddcells.Enabled = False
        Else
            Try
                dddepts.SelectedValue = dept
            Catch ex As Exception

            End Try

            If cell = "0" Then
                Try
                    ddcells.SelectedIndex = 0
                Catch ex As Exception

                End Try

            Else
                Try
                    ddcells.SelectedValue = cell
                Catch ex As Exception

                End Try

            End If
        End If

        PageNumber = txtpg.Value
        GetGantt(qrtr, yr, eq, sid, PageNumber)
        ga.Dispose()
    End Sub
    Private Sub SelectedIndexChanged()
        Dim skill, rd, pdm, pdms, dept, cell As String
        Dim depti, celli As Integer
        Try
            If ddcells.SelectedIndex = 0 Then
                cell = "0"
            Else
                cell = ddcells.SelectedValue
            End If
            lblcell.Value = cell
            If dddepts.SelectedIndex = 0 Then
                dept = "0"
            Else
                dept = dddepts.SelectedValue
            End If
            lbldept.Value = dept
            pdms = ddpdmlist.SelectedValue
            lblpdms.Value = pdms
            pdm = ddpdm.SelectedValue
            lblpdm.Value = pdm
            skill = ddskill.SelectedValue
            lblskill.Value = skill
            rd = ddopts.SelectedValue
            lblopt.Value = rd
            qrtr = lblqrtr.Value
            yr = lblyear.Value
            eq = lbleqid.Value
            sid = lblsid.Value
            ga.Open()
            GetQtrs()
            PopEq(sid, dept, cell)
            GetSkills()
            If dddepts.SelectedIndex <> 0 Then
                GetCell()
            End If
            ddpdm.SelectedValue = pdm
            ddskill.SelectedValue = skill
            ddopts.SelectedValue = rd
            ddqtr.SelectedValue = qrtr
            If eq = "0" Then
                If dddepts.SelectedIndex = 0 Then
                    Try
                        ddeq.SelectedIndex = 0
                    Catch ex As Exception

                    End Try

                Else
                    Try
                        ddeq.SelectedIndex = 1
                    Catch ex As Exception

                    End Try

                End If
            Else
                ddeq.SelectedValue = eq
            End If
            If dept = "0" Then
                Try
                    dddepts.SelectedIndex = 0
                Catch ex As Exception

                End Try
                Try
                    ddcells.SelectedIndex = 0
                Catch ex As Exception

                End Try

                ddcells.Enabled = False
            Else
                dddepts.SelectedValue = dept
                If cell = "0" Then
                    Try
                        ddcells.SelectedIndex = 0
                    Catch ex As Exception

                    End Try

                Else
                    ddcells.SelectedValue = cell
                End If
            End If
            Try
                PageNumber = txtpg.Value
                GetGantt(qrtr, yr, eq, sid, PageNumber)
            Catch ex As Exception

            End Try

            ga.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ddskill_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddskill.SelectedIndexChanged
        SelectedIndexChanged()
    End Sub

    Private Sub ddopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddopts.SelectedIndexChanged
        SelectedIndexChanged()
    End Sub

    Private Sub ddpdm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpdm.SelectedIndexChanged
        SelectedIndexChanged()
    End Sub

    Private Sub ddpdmlist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpdmlist.SelectedIndexChanged
        SelectedIndexChanged()
    End Sub

    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        SelectedIndexChanged()
    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        SelectedIndexChanged()
    End Sub

    Private Sub ddpdm_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpdm.Unload

    End Sub

    Private Sub ddpdm_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpdm.PreRender

    End Sub

    Private Sub ddtyp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtyp.SelectedIndexChanged
        SelectedIndexChanged()
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang691.Text = axlabs.GetASPXPage("PMScheduling2.aspx", "lang691")
        Catch ex As Exception
        End Try
        Try
            lang692.Text = axlabs.GetASPXPage("PMScheduling2.aspx", "lang692")
        Catch ex As Exception
        End Try
        Try
            lang693.Text = axlabs.GetASPXPage("PMScheduling2.aspx", "lang693")
        Catch ex As Exception
        End Try
        Try
            lang694.Text = axlabs.GetASPXPage("PMScheduling2.aspx", "lang694")
        Catch ex As Exception
        End Try
        Try
            lang695.Text = axlabs.GetASPXPage("PMScheduling2.aspx", "lang695")
        Catch ex As Exception
        End Try
        Try
            lang696.Text = axlabs.GetASPXPage("PMScheduling2.aspx", "lang696")
        Catch ex As Exception
        End Try
        Try
            lang697.Text = axlabs.GetASPXPage("PMScheduling2.aspx", "lang697")
        Catch ex As Exception
        End Try
        Try
            lang698.Text = axlabs.GetASPXPage("PMScheduling2.aspx", "lang698")
        Catch ex As Exception
        End Try

    End Sub

End Class
