<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMScheduling2.aspx.vb"
    Inherits="lucy_r12.PMScheduling2" %>

<%@ Register Assembly="ECCTPM35" Namespace="ECCTPM35.Scheduler" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title id="pgtitle">PMScheduling2</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="gcss.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="JavaScript" src="../scripts/overlib_hideform.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMScheduling2aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function getinval(val) {
            //alert(val)
            var valarr = val.split(",")
            var eq = valarr[0];
            var pm = valarr[1];
            var st = valarr[2];
            var lt = valarr[3];
            var fr = valarr[4];
            var stop = valarr[5];
            //var jpid = valarr[7];
            var opt = document.getElementById("lblopt").value;
            var ro = document.getElementById("lblro").value;
            var eReturn
            //alert(stop)
            if (stop != "w" && stop != "p" && stop != "t") {
                eReturn = window.showModalDialog("PMAltManDialog.aspx?pmid=" + pm + "&eqid=" + eq + "&orig=" + st + "&last=" + lt + "&freq=" + fr + "&ro=" + ro, "", "dialogHeight:300px; dialogWidth:600px; resizable=yes");
            }
            else {
                var jpid = valarr[7];
                var pmid = valarr[7];
                if (stop == "w") {
                    eReturn = window.showModalDialog("woaltmandialog.aspx?pmid=" + pm + "&eqid=" + eq + "&orig=" + st + "&last=" + lt + "&freq=" + fr + "&jpid=" + jpid + "&ro=" + ro, "", "dialogHeight:250px; dialogWidth:250px; resizable=yes");
                }
                else if (stop == "t") {
                    eReturn = window.showModalDialog("../appsmantpm/PMAltManDialogtpm.aspx?pmid=" + pm + "&eqid=" + eq + "&orig=" + st + "&last=" + lt + "&freq=" + fr + "&ro=" + ro, "", "dialogHeight:300px; dialogWidth:600px; resizable=yes");
                }
                else {
                    eReturn = window.showModalDialog("pmtaskscheddialog.aspx?pmid=" + pmid + "&eqid=" + eq + "&ro=" + ro, "", "dialogHeight:550px; dialogWidth:850px; resizable=yes");
                }
            }
            if (eReturn) {
                //alert(eReturn)
                if (eReturn == "no" || eReturn == "") {
                    // do nothing
                }
                else {
                    document.getElementById("lblpost").value = "ret";
                    document.getElementById("form1").submit();
                }
            }
        }
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblpost").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblpost").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblpost").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblpost").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function getval(val) {
            var valarr = val.split(",")
            var eq = valarr[0];
            var pm = valarr[1];
            var st = valarr[2];
            var stop = valarr[5];
            //alert(eq + "," + pm + "," + st + "," + stop)
            if (stop == "w") {
                var won = pm; //document.getElementById("lblwo").value;
                var sid = document.getElementById("lblsid").value;
                var uid = document.getElementById("lbluid").value;
                var username = document.getElementById("lblusername").value;
                var islabor = document.getElementById("lblislabor").value;
                var issuper = document.getElementById("lblissuper").value;
                var isplanner = document.getElementById("lblisplanner").value;
                var Logged_In = document.getElementById("lblLogged_In").value;
                var ro = document.getElementById("lblro").value;
                var ms = document.getElementById("lblms").value;
                var appstr = document.getElementById("lblappstr").value;
                window.location = "../appswo/wolabmain.aspx?sid=" + sid + "&wo=" + won + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
            }
            else if (stop == "t") {
                window.location = "../appsmantpm/TPMManager.aspx?jump=yes&typ=no&sid=" + sid + "&eqid=" + eq + "&pmid=" + pm + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
            }
            else {
                window.location = "PMManager.aspx?jump=yes&&typ=no&eqid=" + eq + "&pmid=" + pm + "&uid=" + uid + "&usrname=" + username + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
            }
            //alert(eq + ", " + pm + ", " + st)
        }
        function getopts() {
            var schk = document.getElementById("lblsopts").value;


            if (schk == "0") {
                document.getElementById("lblsopts").value = "1";
                document.getElementById("tropts").className = "view";
                document.getElementById("A1").innerHTML = "Hide Detailed Options";
            }
            else {
                document.getElementById("lblsopts").value = "0";
                document.getElementById("tropts").className = "details";
                document.getElementById("A1").innerHTML = "View Detailed Options";
            }
        }
        function getpbopts() {
            var schk = document.getElementById("lblsopts").value;
            if (schk == "0") {
                document.getElementById("lblsopts").value = "0";
                document.getElementById("tropts").className = "details";
                document.getElementById("A1").innerHTML = "View Detailed Options";
            }
            else {
                document.getElementById("lblsopts").value = "1";
                document.getElementById("tropts").className = "view";
                document.getElementById("A1").innerHTML = "Hide Detailed Options";
            }
        }
        function checkit() {
            //window.setTimeout("setref();", 1205000);
            resetsess();
            var pbchk = document.getElementById("lblpb").value;
            if (pbchk == "1") {
                document.getElementById("lblpb").value = 0;
                getpbopts();
            }

        }
        //-->
    </script>
</head>
<body>
    <script type="text/javascript">

        window.setTimeout("checkit();", 1000);

    
    </script>
    <form id="form1" method="post" runat="server">
    <table height="100%" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="label" width="120">
                            <asp:Label ID="lang691" runat="server">Scheduler Options</asp:Label>
                        </td>
                        <td width="280">
                            <asp:DropDownList ID="ddtyp" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="all" Selected="True">All Records</asp:ListItem>
                                <asp:ListItem Value="opt">PM Optimistic (based on PM Start Date</asp:ListItem>
                                <asp:ListItem Value="pm">PM Work Orders (with scheduled dates)</asp:ListItem>
                                <asp:ListItem Value="wo">Regular Work Orders</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="150">
                            <a class="A1" id="A1" onclick="getopts();" href="#" runat="server">
                                <asp:Label ID="lang692" runat="server">View Detailed Options</asp:Label></a>
                        </td>
                        <td width="150">
                            <a class="A1" id="sret" href="#" runat="server">
                                <asp:Label ID="lang693" runat="server">Test Link</asp:Label></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="details" id="tropts" runat="server">
            <td>
                <table width="1080">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="label">
                                        QTR
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddqtr" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="label">
                                        <asp:Label ID="lang694" runat="server">Dept</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="dddepts" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="label">
                                        <asp:Label ID="lang695" runat="server">Cell</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddcells" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="label">
                                        <asp:Label ID="lang696" runat="server">Equipment</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddeq" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <asp:Label ID="lang697" runat="server">Skill</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddskill" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="label">
                                        R/D
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddopts" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="%">ALL</asp:ListItem>
                                            <asp:ListItem Value="Running">Running Only</asp:ListItem>
                                            <asp:ListItem Value="Down">Down Only</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="label">
                                        PdM
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddpdm" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">No PdM</asp:ListItem>
                                            <asp:ListItem Value="1">PdM Only</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="label">
                                        <asp:Label ID="lang698" runat="server">PdM Skill</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddpdmlist" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="95%">
            <td valign="top">
                <cc1:ECCTPM35 ID="ECC1" runat="server" Width="1080"></cc1:ECCTPM35>
            </td>
        </tr>
        <tr>
            <td colspan="80">
                <table width="1080">
                    <tr>
                        <td align="center">
                            <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="border-right: blue 1px solid" width="20">
                                        <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                            runat="server">
                                    </td>
                                    <td style="border-right: blue 1px solid" width="20">
                                        <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                            runat="server">
                                    </td>
                                    <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                        <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                                    </td>
                                    <td style="border-right: blue 1px solid" width="20">
                                        <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                            runat="server">
                                    </td>
                                    <td width="20">
                                        <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                            runat="server">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <iframe id="ifsession" class="details" src="" frameborder="no" runat="server" style="z-index: 0">
    </iframe>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblqrtr" type="hidden" name="lblqrtr" runat="server">
    <input id="lblskill" type="hidden" name="lblskill" runat="server">
    <input id="lblopt" type="hidden" name="lblopt" runat="server">
    <input id="lblpost" type="hidden" name="lblpost" runat="server">
    <input id="lblyear" type="hidden" name="lblyear" runat="server"><input id="lblrd"
        type="hidden" name="lblrd" runat="server">
    <input id="lblpdm" type="hidden" name="lblpdm" runat="server"><input id="lblpdms"
        type="hidden" name="lblpdms" runat="server">
    <input id="lbldept" type="hidden" name="lbldept" runat="server"><input id="lblcell"
        type="hidden" name="lblcell" runat="server">
    <input id="lblhref" type="hidden" runat="server"><input id="lbltyp" type="hidden"
        runat="server">
    <input id="lbleqidret" type="hidden" runat="server">
    <input id="lblpmid" type="hidden" runat="server">
    <input id="lblsopts" type="hidden" runat="server"><input id="lblro" type="hidden"
        runat="server">
    <input type="hidden" id="lblwonum" runat="server" name="lblwonum">
    <input type="hidden" id="lblusername" runat="server" name="lblusername">
    <input type="hidden" id="lbluid" runat="server" name="lbluid">
    <input type="hidden" id="lblislabor" runat="server" name="lblislabor">
    <input type="hidden" id="lblissuper" runat="server" name="lblissuper">
    <input type="hidden" id="lblisplanner" runat="server" name="lblisplanner">
    <input type="hidden" id="lblfslang" runat="server" /><input type="hidden" id="lblLogged_In"
        runat="server" name="lblLogged_In">
    <input type="hidden" id="Hidden1" runat="server" name="lblro">
    <input type="hidden" id="lblms" runat="server" name="lblms">
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr"><input id="txtpg"
        type="hidden" runat="server" />
    <input id="txtpgcnt" type="hidden" runat="server" />
    <input type="hidden" id="lblpgnav" runat="server" />
    <input type="hidden" id="lblpmcurr" runat="server" />
    <input type="hidden" id="lblpmtot" runat="server" />
    <input type="hidden" id="lblpb" runat="server" />
    </form>
</body>
</html>
