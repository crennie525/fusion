﻿Imports System.Data.SqlClient
Public Class pmmeters
    Inherits System.Web.UI.Page
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim eqid, eqnum, meterid, meter, munit, enterby, sid, pmid, wo As String
    Dim mread, mdate As String
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            enterby = Request.QueryString("uid").ToString
            lblenterby.Value = enterby
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            pmid = Request.QueryString("pmid").ToString
            lblpmid.Value = pmid
            wo = Request.QueryString("wo").ToString
            lblwonum.Value = wo
            If wo <> "" Then
                appset.Open()
                getdetails(wo)
                appset.Dispose()
            End If
        Else
            If Request.Form("lblsubmit") = "addread" Then
                lblsubmit.Value = ""
                appset.Open()
                addread()
                'getmeter(PageNumber)
                appset.Dispose()
                tddate.InnerHtml = lblnewdate.Value
            End If

            eqnum = lbleq.Value
            meter = lblmeter.Value
            munit = lblunit.Value
            tdeq.InnerHtml = eqnum
            tdmeter.InnerHtml = meter
            tdunits.InnerHtml = munit

            mdate = lblnewdate.Value
            tddate.InnerHtml = mdate
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getmeter(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getmeter(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getmeter(ByVal PageNumber As Integer)
        pmid = lblpmid.Value
        Dim intPgNav, intPgCnt As Integer
        eqid = lbleqid.Value
        meterid = lblmeterid.Value
        sql = "select count(*) from meterhist where meterid = '" & meterid & "' and pmid = '" & pmid & "'"
        intPgCnt = appset.Scalar(sql)
        If intPgCnt = 0 Then
            If meterid <> "0" Then
                Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")
                appset.CreateMessageAlert(Me, strMessage, "strKey1")
                'Exit Sub
            End If

        End If
        PageSize = "50"
        intPgNav = appset.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "h.readingdate desc"

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        Filter = "h.meterid = ''" & meterid & "' and pmid = '" & pmid & "'"
        'sql = "select * from meterhist where meterid = '" & meterid & "'"
        sql = "usp_getmeterlist_eq '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        dr = appset.GetRdrData(sql)
        GridView1.DataSource = dr
        GridView1.DataBind()
    End Sub
    Private Sub getdetails(ByVal wonum As String)
        sql = "select e.eqnum, p.meterid, m.meter, m.unit from pm p " _
            + "left join equipment e on e.eqid = p.eqid " _
            + "left join meters m on m.meterid = p.meterid " _
            + "where p.pmid = '" & pmid & "'"
        dr = appset.GetRdrData(sql)
        While dr.Read
            eqnum = dr.Item("eqnum").ToString
            meterid = dr.Item("meterid").ToString
            meter = dr.Item("meter").ToString
            munit = dr.Item("unit").ToString
        End While
        dr.Close()
        lbleq.Value = eqnum
        lblmeterid.Value = meterid
        lblmeter.Value = meter
        lblunit.Value = munit
        tdeq.InnerHtml = eqnum
        tdmeter.InnerHtml = meter
        tdunits.InnerHtml = munit
        wo = lblwonum.Value
        tdwonum.InnerHtml = wo
        sql = "select meterreading, readingdate from meterhist where meterid = '" & meterid & "' and pmid = '" & pmid & "' and wonum = '" & wonum & "'"
        dr = appset.GetRdrData(sql)
        While dr.Read
            mread = dr.Item("meterreading").ToString
            mdate = dr.Item("readingdate").ToString
        End While
        dr.Close()
        txtnewreading.Text = mread
        lblnewdate.Value = mdate
        tddate.InnerHtml = mdate
        If mdate = "" And meterid <> "" Then
            sql = "select Convert(char(10),getDate(),101) as 'mdate'"
            mdate = appset.strScalar(sql)
            lblnewdate.Value = mdate
            tddate.InnerHtml = mdate
        End If

        lbloldread.Value = mread
        lblolddate.Value = mdate

        If meterid = "" Then
            tdmsg.Attributes.Add("class", "plainlabelred view")
            trnav.Attributes.Add("class", "details")
            trgrid.Attributes.Add("class", "details")
            trseeall.Attributes.Add("class", "details")
        Else
            trnav.Attributes.Add("class", "view")
            trgrid.Attributes.Add("class", "view")
            trseeall.Attributes.Add("class", "view")
            PageNumber = 1
            getmeter(PageNumber)
        End If
    End Sub
    Private Sub addread()
        Dim mread As String = txtnewreading.Text
        Dim rdate As String = lblnewdate.Value
        wo = lblwonum.Value
        pmid = lblpmid.Value
        meterid = lblmeterid.Value
        enterby = lblenterby.Value
        Dim pcnt As Integer
        sql = "select count(*) from meterhist where meterid = '" & meterid & "' and pmid = '" & pmid & "' and wonum = '" & wo & "'"
        pcnt = appset.Scalar(sql)
        If pcnt = 0 Then
            sql = "insert into meterhist (meterid, meterreading, readingdate, enterby) values " _
            + "('" & meterid & "','" & mread & "','" & rdate & "','" & enterby & "'); " _
            + "update meters set lastreading = '" & mread & "', lastdate = '" & rdate & "' " _
            + "where meterid = '" & meterid & "'"
        Else
            sql = "update meterhist set meterreading, = '" & mread & "', readingdate = '" & rdate & "' " _
            + "where meterid = '" & meterid & "' and pmid = '" & pmid & "' and wonum = '" & wo & "'; " _
            + "update meters set lastreading = '" & mread & "', lastdate = '" & rdate & "' " _
            + "where meterid = '" & meterid & "'"
        End If

        appset.Update(sql)
        txtnewreading.Text = ""

    End Sub
End Class