<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMSetAdjMan.aspx.vb" Inherits="lucy_r12.PMSetAdjMan" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMSetAdjMan</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMSetAdjManaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <asp:DataList ID="dgadj" runat="server">
                        <HeaderTemplate>
                            <table width="880">
                                <tr>
                                    <td width="60">
                                    </td>
                                    <td width="60">
                                    </td>
                                    <td width="410">
                                    </td>
                                    <td width="100">
                                    </td>
                                    <td width="100">
                                    </td>
                                    <td width="100">
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="trhead" runat="server">
                                <td colspan="3" class="label">
                                    <%# DataBinder.Eval(Container, "DataItem.pm") %>
                                </td>
                                <td colspan="3" class="label">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="trsep" runat="server">
                                <td colspan="11">
                                    <hr>
                                </td>
                            </tr>
                            <tr id="trfunc" runat="server">
                                <td class="label">
                                    <asp:Label ID="lang699" runat="server">Function:</asp:Label>
                                </td>
                                <td class="plainlabel" colspan="6">
                                    <%# DataBinder.Eval(Container, "DataItem.func") %>
                                </td>
                                <td>
                                    <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtid") %>'
                                        CssClass="details">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr id="trhdr" runat="server">
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang700" runat="server">Edit</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang701" runat="server">Task#</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang702" runat="server">Task Description</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang703" runat="server">Current Pass Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang704" runat="server">Use Pass Adj?</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang705" runat="server">Current Pass Cnt</asp:Label>
                                </td>
                            </tr>
                            <tr id="trtask" runat="server">
                                <td rowspan="3">
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ToolTip="Edit Record" CommandName="Edit"
                                        ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"></asp:ImageButton>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="lbltasknume" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tasknum") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.task") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:Label ID="lblokadjcurr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.okadj") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.useadj") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.okcnt") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="btmmenu plainlabel" colspan="2">
                                    <asp:Label ID="lang706" runat="server">Failure Modes</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang707" runat="server">Current Fail Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang708" runat="server">Use Fail Adj?</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang709" runat="server">Current Fail Cnt</asp:Label>
                                </td>
                            </tr>
                            <tr id="trfm" runat="server">
                                <td class="plainlabel" colspan="2">
                                    <%# DataBinder.Eval(Container, "DataItem.fm1") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:Label ID="lblfadjcurr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fadj") %>'
                                        CssClass="plainlabel">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.usefadj") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.fcnt") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <tr id="trsepe" runat="server">
                                <td colspan="11">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr id="trfunce" runat="server">
                                <td class="label">
                                    <asp:Label ID="lang710" runat="server">Function:</asp:Label>
                                </td>
                                <td class="plainlabel" colspan="10">
                                    <%# DataBinder.Eval(Container, "DataItem.func") %>
                                </td>
                            </tr>
                            <tr id="Tr2" runat="server">
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang711" runat="server">Edit</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang712" runat="server">Task#</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang713" runat="server">Task Description</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang714" runat="server">Current Pass Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang715" runat="server">Use Pass Adj?</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang716" runat="server">Current Pass Cnt</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="3">
                                    <asp:ImageButton ID="Imagebutton2" runat="server" ToolTip="Save Changes" CommandName="Update"
                                        ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton3" runat="server" ToolTip="Cancel Changes" CommandName="Cancel"
                                        ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"></asp:ImageButton>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.tasknum") %>
                                </td>
                                <td class="plainlabel">
                                    <%# DataBinder.Eval(Container, "DataItem.task") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:TextBox ID="lblokadj" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.okadj") %>'
                                        Width="40px">
                                    </asp:TextBox>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:DropDownList ID="ddokuse" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("oi")) %>'>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.okcnt") %>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="btmmenu plainlabel" colspan="2">
                                    <asp:Label ID="lang717" runat="server">Failure Modes</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang718" runat="server">Current Fail Adj</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang719" runat="server">Use Fail Adj?</asp:Label>
                                </td>
                                <td class="btmmenu plainlabel">
                                    <asp:Label ID="lang720" runat="server">Current Fail Cnt</asp:Label>
                                </td>
                            </tr>
                            <tr id="Tr1" runat="server">
                                <td class="plainlabel" colspan="2">
                                    <%# DataBinder.Eval(Container, "DataItem.fm1") %>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:TextBox ID="lblfadj" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fadj") %>'
                                        Width="40px">
                                    </asp:TextBox>
                                </td>
                                <td class="plainlabel" align="center">
                                    <asp:DropDownList ID="ddfuse" runat="server" Width="60px" SelectedIndex='<%# GetSelIndex(Container.DataItem("fi")) %>'>
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="2">No</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="plainlabel" align="center">
                                    <%# DataBinder.Eval(Container, "DataItem.fcnt") %>
                                </td>
                            </tr>
                        </EditItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lblpmid" type="hidden" runat="server">
    <input id="lbltaskcnt" type="hidden" runat="server" name="lbltaskcnt"><input id="lbltaskcurrcnt"
        type="hidden" runat="server" name="lbltaskcurrcnt">
    <input type="hidden" id="lblrow" runat="server">
    <input type="hidden" id="lbloadjc" runat="server" name="lbloadjc">
    <input type="hidden" id="lblfadjc" runat="server" name="lblfadjc"><input type="hidden"
        id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
