<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMManager.aspx.vb" Inherits="lucy_r12.PMManager" %>

<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title runat="server" id="pgtitle">PM Manager</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/taskgrid.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMManageraspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function checkup(eqid) {
            window.parent.setref();
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("subdiv").className = "view";
            document.getElementById("subdiv").style.position = "absolute";
            document.getElementById("subdiv").style.top = "115px";
            document.getElementById("subdiv").style.left = "120px";
            document.getElementById("ifrev").src = "PMUpDate.aspx?eqid=" + eqid;
        }

        function gettab(name) {
            closeall(name);
            if (name == "pm") {
                document.getElementById("tdpm").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var sid = document.getElementById("lblsid").value;
                document.getElementById("ifmain").src = "PMMainMan.aspx?sid=" + sid + "&date=" + Date();
            }
            else if (name == "eq") {
                document.getElementById("tdeq").className = "thdrhov label";
                document.getElementById("eq").className = 'tdborder view';
                //var pmid = document.getElementById("lblpmid").value;
                //var eqid = document.getElementById("lbleqid").value;
                //geteq.Attributes.Add("src", "PMGetPMMan.aspx?jump=yes&typ=no&eqid=" & eqid & "&pmid=" & pmid)
                document.getElementById("lbljump").value = "eq";
                var id = document.getElementById("lblpmid").value;
                var eid = document.getElementById("lbleqid").value;
                var piccnt = "";
                //checkarch();
                var sid = document.getElementById("lblsid").value;
                document.getElementById("geteq").src = "PMGetPMMan.aspx?jump=yes&typ=no&eqid=" + eid + "&pmid=" + id + "&piccnt=" + piccnt + "&sid=" + sid;
            }
            else if (name == "fm") {
                document.getElementById("tdfm").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var wo = document.getElementById("lblwo").value;
                if (pmid == "") pmid = "0";
                document.getElementById("ifmain").src = "pmfmmain.aspx?pmid=" + pmid + "&wo=" + wo + "&date=" + Date();

            }
            else if (name == "act") {
                document.getElementById("tdact").className = "thdrhov label";
                //document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                var wo = document.getElementById("lblwo").value;
                var sid = document.getElementById("lblsid").value;
                if (pmid == "") pmid = "0";


                //document.getElementById("ifmain").src = "pmactmain.aspx?sid=" + sid + "&pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date();
                var eReturn = window.showModalDialog("pmactdialog.aspx?sid=" + sid + "&pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date(), "", "dialogHeight: 700px; dialogWidth:1040px;resizable=yes");

                if (eReturn) {

                }
            }
            else if (name == "cost") {
                document.getElementById("tdcost").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                var wo = document.getElementById("lblwo").value;
                if (pmid == "") pmid = "0";
                document.getElementById("ifmain").src = "pmcosts.aspx?pmid=" + pmid + "&pmhid=" + pmhid + "&wo=" + wo + "&date=" + Date();

            }
            else if (name == "mtr") {
                document.getElementById("tdmtr").className = "thdrhov label";
                document.getElementById("pm").className = 'tdborder view';
                var pmid = document.getElementById("lblpmid").value;
                var pmhid = document.getElementById("lblpmhid").value;
                var wo = document.getElementById("lblwo").value;
                var uid = document.getElementById("lbluid").value;
                var eqid = document.getElementById("lbleqid").value;
                //alert(wo)
                if (pmid == "") pmid = "0";
                document.getElementById("ifmain").src = "pmmeters.aspx?pmid=" + pmid + "&wo=" + wo + "&eqid=" + eqid + "&uid=" + uid + "&date=" + Date();

            }

        }
        function closeall(name) {

            if (name != "act") {
                document.getElementById("tdeq").className = "thdr label";
                document.getElementById("tdpm").className = "thdr label";
                document.getElementById("tdfm").className = "thdr label";
                document.getElementById("tdact").className = "thdr label";
                document.getElementById("tdcost").className = "thdr label";
                document.getElementById("tdmtr").className = "thdr label";

                document.getElementById("pm").className = 'details';
                document.getElementById("eq").className = 'details';
            }

        }
            //-->
    </script>
</head>
<body class="tbg" onload="checkarch();">
    <form id="form1" method="post" runat="server">
    <table style="z-index: 1; position: absolute; top: 83px; left: 7px" cellspacing="0"
        cellpadding="2" width="1043">
        <tr>
            <td width="760">
                <table cellspacing="1" cellpadding="1">
                    <tr>
                        <td class="thdrhov label" id="tdpm" onclick="gettab('pm');" width="170" runat="server"
                            height="20">
                            <asp:Label ID="lang663" runat="server">Current PM Records</asp:Label>
                        </td>
                        <td class="thdr label" id="tdeq" onclick="gettab('eq');" width="140" runat="server">
                            <asp:Label ID="lang664" runat="server">PM Manager</asp:Label>
                        </td>
                        <td class="thdr label" id="tdfm" onclick="gettab('fm');" width="140" runat="server">
                            <asp:Label ID="lang665" runat="server">Failure Reporting</asp:Label>
                        </td>
                        <td class="thdr label" id="tdact" onclick="gettab('act');" width="140" runat="server">
                            <asp:Label ID="lang666" runat="server">PM Actuals</asp:Label>
                        </td>
                        <td class="thdr label" id="tdmtr" onclick="gettab('mtr');" width="140" runat="server">
                            Meter Readings
                        </td>
                        <td class="thdr label" id="tdcost" onclick="gettab('cost');" width="140" runat="server">
                            <asp:Label ID="lang667" runat="server">PM Costs</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="3">
                &nbsp;
            </td>
            <td class="thdrsinglft" id="e1" width="26">
                <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
            </td>
            <td class="thdrsingrt label" id="e2" align="left" width="230">
                <asp:Label ID="lang668" runat="server">Asset Hierarchy</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="details" id="eq" runat="server" valign="top">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="geteq" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                                margin: 0px; border-top-style: none; border-left-style: none" src="pmhold.htm"
                                frameborder="no" width="790" scrolling="no" height="52" allowtransparency runat="server">
                            </iframe>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <iframe id="iftaskdet" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                border-top-style: none; border-left-style: none; padding-top: 0px" src="pmhold.htm"
                                frameborder="no" width="790" scrolling="no" height="452" allowtransparency runat="server">
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="tdborder" id="pm" valign="top" runat="server">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <iframe id="ifmain" style="background-color: transparent" src="pmhold.htm" frameborder="no"
                                width="780" scrolling="auto" height="550" allowtransparency runat="server"></iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
            <td valign="top" colspan="2" rowspan="2">
                <table cellspacing="0">
                    <tr>
                        <td colspan="2">
                            <iframe id="ifarch" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                border-top-style: none; border-left-style: none; padding-top: 0px" src="PMArchMan.aspx?start=no"
                                frameborder="no" width="260" scrolling="yes" height="200" allowtransparency runat="server">
                                ></iframe>
                        </td>
                    </tr>
                    <tr height="22">
                        <td class="thdrsinglft" width="26">
                            <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                        </td>
                        <td class="thdrsingrt label" width="204">
                            <asp:Label ID="lang669" runat="server">Asset Images</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <iframe id="ifimg" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                border-top-style: none; border-left-style: none; padding-top: 0px" src="../equip/eqimg.aspx?eqid=0&amp;fuid=0"
                                frameborder="no" width="250" scrolling="no" height="272" allowtransparency runat="server">
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="details" id="subdiv" style="z-index: 999; border-bottom: black 1px solid;
        border-left: black 1px solid; width: 360px; height: 96px; border-top: black 1px solid;
        border-right: black 1px solid;">
        <table cellspacing="0" cellpadding="0" width="360" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="labelwht">
                    <asp:Label ID="lang670" runat="server">Revision Alert</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closerev();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="ifrev" style="width: 360px; height: 100px" src="" frameborder="no" runat="server">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    <input id="lbltab" type="hidden" name="lbltab" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid"
        type="hidden" name="lblclid" runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblchk"
        type="hidden" name="lblchk" runat="server">
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server"><input id="lblfuid"
        type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="tasknum" type="hidden" name="tasknum" runat="server"><input id="taskcnt"
        type="hidden" name="taskcnt" runat="server">
    <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
    <input id="lblpmid" type="hidden" runat="server">
    <input id="lbljump" type="hidden" runat="server">
    <input id="lblwo" type="hidden" runat="server">
    <input id="lblpmhid" type="hidden" runat="server"><input id="lblro" type="hidden"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblusername" runat="server">
    <input type="hidden" id="lbluid" runat="server">
    <input type="hidden" id="lblislabor" runat="server">
    <input type="hidden" id="lblissuper" runat="server">
    <input type="hidden" id="lblisplanner" runat="server">
    <input type="hidden" id="lblLogged_In" runat="server" name="lblLogged_In">
    <input type="hidden" id="lblms" runat="server" name="lblms">
    <input type="hidden" id="lblappstr" runat="server" name="lblappstr">
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
