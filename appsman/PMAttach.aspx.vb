

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class PMAttach
    Inherits System.Web.UI.Page
    Protected WithEvents lang588 As System.Web.UI.WebControls.Label

    Protected WithEvents lang587 As System.Web.UI.WebControls.Label

    Protected WithEvents lang586 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tddoc As System.Web.UI.HtmlControls.HtmlTableCell
    Dim MyFile As HtmlInputFile
    Dim eqid, eqnum, doctype, filename, sql, pmid, ret, ro As String
    Dim dr As SqlDataReader
    Dim up As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ibdelproc As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cblproc As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents tdhdr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnupload As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents File1 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents tbfiles As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents File2 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents File3 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents File4 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents File5 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents File6 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents File7 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents File8 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents File9 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents File10 As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents lbldoctype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btnupload.Disabled = True
                ibdelproc.ImageUrl = "../images/appbuttons/minibuttons/deldis.gif"
                ibdelproc.Enabled = False
            End If
            pmid = Request.QueryString("pmid").ToString
            'eqnum = Request.QueryString("eqnum").ToString
            lblpmid.Value = pmid
            up.Open()
            ret = GetPM(pmid)
            tdhdr.InnerHtml = "Current PM:  " & ret
            PopProcedures(pmid)
            'GetProcedures(pmid)
            up.Dispose()
        End If
    End Sub
    Private Function GetPM(ByVal pmid As String) As String
        Dim ret, pdm, pm As String
        sql = "select pdm = (case pm.ptid when 0 then 'None' else pm.pretech End ), " _
             + "pm = (pm.skill + '/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd) " _
             + "from pm pm where pm.pmid = '" & pmid & "'"
        dr = up.GetRdrData(sql)
        While dr.Read
            pdm = dr.Item("pdm")
            pm = dr.Item("pm")
            If pdm <> "None" Then
                ret = pdm & " - " & pm
            Else
                ret = pm
            End If
        End While
        dr.Close()
        Return ret
    End Function
    Private Sub btnupload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.ServerClick
        'MyFile = File1
        up.Open()
        If Not (File1.PostedFile Is Nothing) Then
            Import(File1)
        End If
        If Not (File2.PostedFile Is Nothing) Then
            Import(File2)
        End If
        If Not (File3.PostedFile Is Nothing) Then
            Import(File3)
        End If
        If Not (File4.PostedFile Is Nothing) Then
            Import(File4)
        End If
        If Not (File5.PostedFile Is Nothing) Then
            Import(File5)
        End If
        If Not (File6.PostedFile Is Nothing) Then
            Import(File6)
        End If
        If Not (File7.PostedFile Is Nothing) Then
            Import(File7)
        End If
        If Not (File8.PostedFile Is Nothing) Then
            Import(File8)
        End If
        If Not (File9.PostedFile Is Nothing) Then
            Import(File9)
        End If
        If Not (File10.PostedFile Is Nothing) Then
            Import(File10)
        End If
        up.Dispose()
    End Sub
    Private Sub UpdateOptOld(ByVal doctype As String, ByVal pmid As String, ByVal file As String)
        sql = "insert into pmManAttach (pmid, doctype, filename) values ('" & pmid & "', '" & doctype & "', '" & file & "')"
        up.Update(sql)
    End Sub
    Private Sub PopProcedures(ByVal pmid As String)
        Dim dt, val, filt As String
        dt = "pmManAttach"
        val = "pmaid, filename"
        filt = " where pmid = '" & pmid & "'"
        dr = up.GetList(dt, val, filt)
        cblproc.DataSource = dr
        cblproc.DataTextField = "filename"
        cblproc.DataValueField = "pmaid"
        cblproc.DataBind()
        dr.Close()
    End Sub


    Private Sub GetProcedures(ByVal pmid As String)
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<table>")
        Dim sbc As Integer
        sql = "select count(*) from pmManAttach where pmid = '" & pmid & "'"
        sbc = up.Scalar(sql)
        If sbc > 0 Then ' should be using has rows instead of count
            sql = "select * from pmManAttach where pmid = '" & pmid & "'"
            dr = up.GetRdrData(sql)
            If dr.HasRows Then
                While dr.Read
                    sb.Append("<tr>")
                    sb.Append("<input type=""checkbox"" name=""cbdel"">")
                    sb.Append("<td class=""labellt"">")
                    sb.Append(dr.Item("filename"))
                    sb.Append("</td></tr>")
                End While
                sb.Append("</table>")
                tbfiles.InnerHtml = sb.ToString
            End If

        Else
            tbfiles.InnerHtml = "No Documents Uploaded"
        End If
        dr.Close()


    End Sub
    Private Sub Import(ByVal MyFile As HtmlInputFile)
        If Not (MyFile.PostedFile Is Nothing) Then
            'Check to make sure we actually have a file to upload
            Dim strLongFilePath As String = MyFile.PostedFile.FileName
            Dim intFileNameLength As Integer = InStr(1, StrReverse(strLongFilePath), "\")
            Dim strFileName As String = Mid(strLongFilePath, (Len(strLongFilePath) - intFileNameLength) + 2)
            If Len(strFileName) = 0 Then
                strFileName = strLongFilePath
            End If
            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            'appstr + 
            Dim strto As String = appstr + "/eqimages/"
            'Dim newstr As String = lblpmstr.Value
            Dim strScript As String
            'Dim newstr As String = lblpmstr.Value

            If Len(strFileName) <> 0 Then

                Select Case MyFile.PostedFile.ContentType

                    Case "Application/pdf", "application/pdf"
                        lbldoctype.Value = "pdf"
                        Try
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        Catch ex As Exception
                            strto = "/eqimages/"
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        End Try


                    Case "Application/msword", "application/msword"
                        lbldoctype.Value = "msword"
                        Try
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        Catch ex As Exception
                            strto = "/eqimages/"
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        End Try


                    Case "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                        lbldoctype.Value = "mswordx"
                        Try
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        Catch ex As Exception
                            strto = "/eqimages/"
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        End Try


                    Case "Application/x-msexcel", "application/x-msexcel", "application/vnd.ms-excel"
                        lbldoctype.Value = "msexcel"
                        Try
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        Catch ex As Exception
                            strto = "/eqimages/"
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        End Try


                    Case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        lbldoctype.Value = "msexcelx"
                        Try
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        Catch ex As Exception
                            strto = "/eqimages/"
                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                        End Try

                        'Case "text/plain"
                        'lbldoctype.Value = "text"
                        'MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)

                    Case "Application/octet-stream", "application/octet-stream"
                        'Try to determine what type of file we have
                        Dim start As Integer = strFileName.IndexOf(".") + 2
                        Dim qstr As String = Mid(strFileName, start)
                        Select Case qstr
                            Case "doc"
                                lbldoctype.Value = "msword"
                                tddoc.InnerHtml = "msword"
                                Try
                                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                                Catch ex As Exception
                                    strto = "/eqimages/"
                                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                                End Try

                            Case "xls"
                                lbldoctype.Value = "msexcel"
                                Try
                                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                                Catch ex As Exception
                                    strto = "/eqimages/"
                                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                                End Try

                            Case "pdf"
                                lbldoctype.Value = "pdf"
                                Try
                                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                                Catch ex As Exception
                                    strto = "/eqimages/"
                                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                                End Try

                                'Case "txt"
                                'lbldoctype.Value = "text"
                                'MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & strFileName)
                            Case Else
                                lbldoctype.Value = "na"
                                Dim strMessage As String = tmod.getmsg("cdstr330", "PMAttach.aspx.vb") & " " + "Your Document Type " & MyFile.PostedFile.ContentType & " needs to be converted to one of the above"
                                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End Select

                    Case Else
                        lbldoctype.Value = "na"
                        Dim strMessage As String = tmod.getmsg("cdstr331", "PMAttach.aspx.vb") & " " + "Your Document Type " & MyFile.PostedFile.ContentType & " needs to be converted to one of the above"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Select
                Dim doctype As String = lbldoctype.Value
                If doctype <> "na" Then
                    filename = strFileName
                    Dim start, tend, sq, dq As Integer
                    Dim errmsg As String
                    start = 0
                    tend = filename.Length
                    sq = filename.IndexOf("'", start, tend)
                    dq = filename.IndexOf("''", start, tend)
                    If dq <> -1 Then
                        errmsg = "Double Quotes Are Not Allowed in File Name"
                        Utilities.CreateMessageAlert(Me, errmsg, "strKey1")
                    ElseIf sq <> -1 Then
                        errmsg = "Single Quotes Are Not Allowed in File Name"
                        Utilities.CreateMessageAlert(Me, errmsg, "strKey1")
                    Else
                        doctype = lbldoctype.Value
                        pmid = lblpmid.Value

                        UpdateOptOld(doctype, pmid, filename)
                        PopProcedures(pmid)
                    End If
                    'file = Replace(file, "'", Chr(180), , , vbTextCompare)      
                End If
            End If
        End If
    End Sub
    Private Sub DeleteProcedures(ByVal pi As String, ByVal p As String)
        sql = "delete from pmManAttach where pmaid = '" & pi & "'"
        up.Update(sql)
    End Sub

    Private Sub ibdelproc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibdelproc.Click
        Dim Item As ListItem
        Dim p, pi As String
        pmid = lblpmid.Value
        up.Open()
        For Each Item In Me.cblproc.Items
            If Item.Selected Then
                p = Item.ToString
                pi = Item.Value.ToString
                DeleteProcedures(pi, p)
            End If
        Next
        PopProcedures(pmid)
        up.Dispose()
    End Sub












    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang586.Text = axlabs.GetASPXPage("PMAttach.aspx", "lang586")
        Catch ex As Exception
        End Try
        Try
            lang587.Text = axlabs.GetASPXPage("PMAttach.aspx", "lang587")
        Catch ex As Exception
        End Try
        Try
            lang588.Text = axlabs.GetASPXPage("PMAttach.aspx", "lang588")
        Catch ex As Exception
        End Try

    End Sub

End Class
