<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMAltMan.aspx.vb" Inherits="lucy_r12.PMAltMan" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMAltMan</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
        <script language="JavaScript" type="text/javascript" src="../scripts1/PMAltManaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function getcal(fld) {
            var dt = document.getElementById("lblorig").value;
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld + "&dt=" + dt, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                var fldret = "txt" + fld;
                document.getElementById(fldret).value = eReturn;
                //document.getElementById("lblschk").value = "1";
                if (fld == "n") {
                    document.getElementById("lblnext").value = eReturn;
                }
            }
        }
        function exittask() {
            var chk = document.getElementById("lblpost").value;
            if (chk == "go") {
                window.parent.handleexit("go");
            }
            else if (chk == "no") {
                window.parent.handleexit("no");
            }
            else if (chk == "wo") {
                var rev = document.getElementById("txtnewdate").value;
                window.parent.handleexit(rev);
            }
        }
        function checkskip() {
            var cd = document.getElementById("cbskip");
            if (cd.checked = true) {
                document.getElementById("lblskip").value = "yes";
            }
            else {
                document.getElementById("lblskip").value = "no";
            }
        }
    </script>
</head>
<body onload="exittask();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr >
            <td class="plainlabel" id="tdtop" colspan="3" runat="server" height="30">
            </td>
        </tr>
        <tr>
            <td width="100">
                <asp:TextBox ID="txtnewdate" runat="server"></asp:TextBox>
            </td>
            <td width="30">
                <img onmouseover="return overlib('Get a New Date', ABOVE, LEFT)" onclick="getcal('newdate');"
                    onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                    width="19">
            </td>
            <td width="250">&nbsp;</td>
        </tr>
        <tr>
            <td class="plainlabelblue" colspan="3">
                <input id="rb1" onclick="chkit(this.value);" type="radio" checked value="0" name="rb"><asp:Label
                    ID="lang583" runat="server">Use Original For Next</asp:Label>&nbsp;(changes work order scheduled start date only)<br>
                <input id="rb2" onclick="chkit(this.value);" type="radio" value="1" name="rb"><asp:Label
                    ID="lang584" runat="server">Use Revised For Next</asp:Label>&nbsp;(changes both work order scheduled date and pm next date)
            </td>
        </tr>
        <tr>
        <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
        <td colspan="3" class="plainlabel"><input type="checkbox" id="cbskip" runat="server" onclick="checkskip();" />&nbsp;Ignore past date conflicts?</td>
        
        </tr>
        <tr>
        <td colspan="3" class="plainlabelblue" align="right"><a href="#" onclick="exit();">Return without saving changes</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="savetask();">Save changes and return</a></td>
        </tr>
    </table>
    <input id="lblpmid" type="hidden" runat="server">
    <input id="lblorig" type="hidden" runat="server">
    <input id="lblpost" type="hidden" runat="server"><input id="rbval" type="hidden"
        runat="server">
    <input type="hidden" id="lbllast" runat="server"><input type="hidden" id="lblfreq"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id = "lblwonum" runat="server" />
    <input type="hidden" id= "lblwho" runat="server" />
    <input type="hidden" id="lblskip" runat="server" />
    </form>
</body>
</html>
