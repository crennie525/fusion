﻿Imports System.Data.SqlClient
Public Class PMWOALLMAX
    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pma As New Utilities
    Dim filt As String
    Dim pmidarr As ArrayList = New ArrayList
    Dim pmarr As ArrayList = New ArrayList
    Public Function GetWOPMReport(ByVal filt As String) As String
        Dim func, funcchk, task, tasknum, subtask, pm, pm1, psite, wo As String
        sql = "select distinct pm.wonum, pm.pmid from pmmax pm " _
        + "left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.wonum = pm.wonum " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
        + filt
        pma.Open()
        dr = pma.GetRdrData(sql)
        While dr.Read
            If dr.Item("wonum").ToString <> "" Then
                pmidarr.Add(dr.Item("wonum"))
                pmarr.Add(dr.Item("pmid"))
            End If
            'pm = dr.Item("pmid").ToString
        End While
        dr.Close()

        Dim sb As New System.Text.StringBuilder

        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")


        Dim i As Integer
        For i = 0 To pmidarr.Count - 1
            wo = pmidarr(i)
            pm = pmarr(i)
            Dim flag As Integer = 0
            Dim subcnt As Integer = 0
            Dim subcnth As Integer = 0
            sql = "select w.siteid, s.sitename from workorder w left join sites s on s.siteid = w.siteid where w.wonum = '" & wo & "'"
            dr = pma.GetRdrData(sql)
            While dr.Read
                psite = dr.Item("sitename").ToString
            End While
            dr.Close()


            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
            sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""130""></td><td width=""170""></td><td width=""90""></td></tr>")

            sql = "usp_getwoprintmax '" & wo & "'"

            dr = pma.GetRdrData(sql)
            Dim headhold As String = "0"
            While dr.Read

                Dim pms As String = dr.Item("pm").ToString
                sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & psite & " Preventive Maintenance Work Request</td></tr>")
                sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"" align=""center"">" & pms & "</td></tr>")
                sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center""><b>Work Order#</b>&nbsp;&nbsp;" & wo & "</td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl247", "woprint.aspx.vb") & "</td></tr>")
                Dim start As String = dr.Item("schedstart").ToString
                If start = "" Then
                    start = dr.Item("targstartdate").ToString
                    If start = "" Then
                        start = Now
                    End If
                End If
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl248", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & start & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl249", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl250", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("reportedby").ToString & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl251", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("reportdate").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl252", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl253", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td></tr>")

                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")

                sb.Append("<tr height=""30""><td class=""label"">" & tmod.getlbl("cdlbl254", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("supervisor").ToString & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl255", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("leadcraft").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl256", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""4"">" & dr.Item("description").ToString & "</td></tr>")

                sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl257", "woprint.aspx.vb") & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl258", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("dept_desc").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl259", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("cell_desc").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl260", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("ldesc").ToString & "</td></tr>")

                If dr.Item("eqnum").ToString <> "" Then
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl261", "woprint.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                End If
            End While
            dr.Close()
            'sb.Append("<tr><td>&nbsp;</td></tr>")

            Dim tnum, td, fm, sk, qt, st, lube, tool, part, stnum, jpstr, mea, jp As String
            Try
                sql = "select pmjp1 from pmmax where pmid = '" & pm & "'"
                jp = pma.strScalar(sql)
            Catch ex As Exception
                jp = "-1"
            End Try

            If jp <> "-1" Then
                sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
                sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""20""></td><td width=""580""></td></tr>")
                sql = "usp_getjpwo '" & jp & "','" & wo & "'"
                sql = "select distinct t.tasknum, t.subtask, t.taskdesc, t.failuremode, t.skill, t.qty, t.ttime, " _
               + "cast(l.output as varchar(3000)) as lout, cast(tl.output as varchar(3000)) as tout, cast(p.output as varchar(3000)) as pout, w.jpnum, w.description, m.measurement " _
               + "from wojobtasks t " _
               + "left join wojplubesout l on t.wojtid = l.wojtid " _
               + "left join wojptoolsout tl on t.wojtid = tl.wojtid " _
               + "left join wojppartsout p on t.wojtid = p.wojtid " _
               + "left join wojobplans w on w.jpid = t.jpid " _
               + "left join wojptaskmeasoutput m on m.wojtid = t.wojtid " _
               + "where t.jpid = '" & jp & "' and t.wonum = '" & wo & "' order by t.tasknum"
                Dim jpdesc As String
                dr = pma.GetRdrData(sql)
                While dr.Read
                    jpstr = dr.Item("jpnum").ToString
                    jpdesc = dr.Item("description").ToString
                    If headhold = "0" Then
                        headhold = "1"
                        'jp = dr.Item("pm").ToString
                        sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
                        sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">Job Plan Details</td></tr>")
                        sb.Append("<tr><td class=""label"" colspan=""3"">Job Plan</td><td class=""plainlabel"">" & jpstr & "</tr>")
                        sb.Append("<tr><td class=""label"" colspan=""3"">Description</td><td class=""plainlabel"">" & jpdesc & "</tr>")
                        sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")

                    End If

                    tnum = dr.Item("tasknum").ToString
                    stnum = dr.Item("subtask").ToString
                    td = dr.Item("taskdesc").ToString
                    sk = dr.Item("skill").ToString
                    qt = dr.Item("qty").ToString
                    st = dr.Item("ttime").ToString
                    fm = dr.Item("failuremode").ToString
                    lube = dr.Item("lout").ToString
                    tool = dr.Item("tout").ToString
                    part = dr.Item("pout").ToString
                    mea = dr.Item("measurement").ToString

                    If stnum = "0" Then
                        sb.Append("<tr><td class=""plainlabel"">[" & tnum & "]</td>")
                        If Len(td) > 0 Then
                            sb.Append("<td colspan=""3"" class=""plainlabel"">" & td & "</td></tr>")
                        Else
                            sb.Append("<td colspan=""3"" class=""plainlabel"">" & tmod.getlbl("cdlbl286", "woprint.aspx.vb") & "</td></tr>")
                        End If
                        If sk <> "" And sk <> "Select" Then
                            sb.Append("<tr><td></td>")
                            sb.Append("<td colspan=""3"" class=""plainlabel""><b>Skill:&nbsp;&nbsp;</b>" & sk)
                            sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                            sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                        End If
                        If fm <> "" Then
                            sb.Append("<tr><td></td>")
                            sb.Append("<td colspan=""3"" class=""plainlabel""><b>Failure Modes:&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                        End If
                        If mea <> "" Then
                            sb.Append("<tr><td></td>")
                            sb.Append("<td colspan=""3"" class=""plainlabel""><b>Measurements:&nbsp;&nbsp;</b>" & mea & "</td></tr>")
                        End If
                        If lube <> "" Then
                            sb.Append("<tr><td></td>")
                            sb.Append("<td colspan=""3"" class=""plainlabel""><b>Lubes:&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                        End If
                        If tool <> "" Then
                            sb.Append("<tr><td></td>")
                            sb.Append("<td colspan=""3"" class=""plainlabel""><b>Tools:&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                        End If
                        If part <> "" Then
                            sb.Append("<tr><td></td>")
                            sb.Append("<td colspan=""3"" class=""plainlabel""><b>Parts:&nbsp;&nbsp;</b>" & part & "</td></tr>")
                        End If
                        sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                    Else
                        sb.Append("<tr><td class=""plainlabel"">[" & stnum & "]</td>")
                        If Len(td) > 0 Then
                            sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & td & "</td></tr>")
                        Else
                            sb.Append("<td></td><td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl287", "woprint.aspx.vb") & "</td></tr>")
                        End If
                        If sk <> "" And sk <> "Select" Then
                            sb.Append("<tr><td></td><td></td>")
                            sb.Append("<td colspan=""2"" class=""plainlabel""><b>Skill:&nbsp;&nbsp;</b>" & sk)
                            sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                            sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                        End If
                        If fm <> "" Then
                            sb.Append("<tr><td></td><td></td>")
                            sb.Append("<td colspan=""2"" class=""plainlabel""><b>Failure Modes:&nbsp;&nbsp;</b>" & fm & "</td></tr>")
                        End If
                        If lube <> "" Then
                            sb.Append("<tr><td></td><td></td>")
                            sb.Append("<td colspan=""2"" class=""plainlabel""><b>Lubes:&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                        End If
                        If tool <> "" Then
                            sb.Append("<tr><td></td><td></td>")
                            sb.Append("<td colspan=""2"" class=""plainlabel""><b>Tools:&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                        End If
                        If part <> "" Then
                            sb.Append("<tr><td></td><td></td>")
                            sb.Append("<td colspan=""2"" class=""plainlabel""><b>Parts:&nbsp;&nbsp;</b>" & part & "</td></tr>")
                        End If
                        sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                    End If
                End While
                dr.Close()
            End If

            

            '*** Parts
            Dim pflg As String = "0"
            headhold = 0
            sql = "select itemnum, description, location, qty from woparts where wonum = '" & wo & "'"
            dr = pma.GetRdrData(sql)
            While dr.Read
                If headhold = 0 Then
                    headhold = "1"

                    sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                    sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                    'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl288", "woprint.aspx.vb") & "</td></tr>")
                    sb.Append("<tr><td class=""label""><u>Part#</u></td>")
                    sb.Append("<td class=""label""><u>Qty</u></td>")
                    sb.Append("<td class=""label""><u>Description</u></td>")
                    sb.Append("<td class=""label""><u>Location</u></td></tr>")
                End If
                If Len(dr.Item("itemnum").ToString) > 0 Then
                    pflg = "1"
                    sb.Append("<tr><td class=""label"">" & dr.Item("itemnum").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
                End If
            End While
            dr.Close()
            If pflg = "1" Then
                sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
                sb.Append("</Table></td></tr>")
            End If

            '*** Tools
            Dim tflg As String = "0"
            headhold = 0
            sql = "select toolnum, description, location, qty from wotools where wonum = '" & wo & "'"
            dr = pma.GetRdrData(sql)
            While dr.Read
                If headhold = 0 Then
                    headhold = "1"

                    sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                    sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                    'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl289", "woprint.aspx.vb") & "</td></tr>")
                    sb.Append("<tr><td class=""label""><u>Tool#</u></td>")
                    sb.Append("<td class=""label""><u>Qty</u></td>")
                    sb.Append("<td class=""label""><u>Description</u></td>")
                    sb.Append("<td class=""label""><u>Location</u></td></tr>")
                End If
                If Len(dr.Item("toolnum").ToString) > 0 Then
                    tflg = "1"
                    sb.Append("<tr><td class=""label"">" & dr.Item("toolnum").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
                End If
            End While
            dr.Close()
            If tflg = "1" Then
                sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
                sb.Append("</Table></td></tr>")
            End If

            '*** Lubes
            Dim lflg As String = "0"
            headhold = 0
            sql = "select lubenum, description, location, qty from wolubes where wonum = '" & wo & "'"
            dr = pma.GetRdrData(sql)
            While dr.Read
                If headhold = 0 Then
                    headhold = "1"
                    sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
                    sb.Append("<tr><td width=""150""></td><td width=""30""></td><td width=""200""></td><td width=""400""></td></tr>")
                    'sb.Append("<tr><td class=""label"">" & tmod.getlbl("cdlbl290" , "woprint.aspx.vb") & "</td></tr>")
                    'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""bigbold graybot"" colspan=""4"">" & tmod.getlbl("cdlbl291", "woprint.aspx.vb") & "</td></tr>")
                    sb.Append("<tr><td class=""label""><u>Lubricant#</u></td>")
                    sb.Append("<td class=""graylabel""><u>Qty</u></td>")
                    sb.Append("<td class=""label""><u>Description</u></td>")
                    sb.Append("<td class=""label""><u>Location</u></td></tr>")
                End If
                If Len(dr.Item("lubenum").ToString) > 0 Then
                    lflg = "1"
                    sb.Append("<tr><td class=""label"">" & dr.Item("lubenum").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">N/A</td>") '" & dr.Item("qty").ToString & "
                    sb.Append("<td class=""plainlabel"">" & dr.Item("description").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td></tr>")
                End If
            End While
            dr.Close()
            If lflg = "1" Then
                sb.Append("<tr><td class=""bigbold"" colspan=""4"">&nbsp;</td></tr>")
                sb.Append("</Table></td></tr>")
            End If

            sb.Append("<tr><td colspan=""5"" class=""plainlabel"">Work Performed by:  ________________________   Date: __________</td></tr>")
            sb.Append("<tr><td colspan=""5"">")
            sb.Append("<table border=""0"">")
            sb.Append("<tr><td class=""plainlabel"">Time&nbsp;(min)</td>")
            sb.Append("<td height=""24"" width=""100"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">REG</td>")
            sb.Append("<td width=""100"" align=""center"" class=""plainlabel"">OT</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Actual:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Other:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("</table>")
            sb.Append("</td></tr>")

            sb.Append("</table></td></tr>")
            sb.Append("</table>")
        Next
        sb.Append("</body>")
        pma.Dispose()
        Dim longstring As String = sb.ToString
        Return longstring
    End Function

    
End Class
