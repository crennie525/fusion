

'********************************************************
'*
'********************************************************



Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Collections
Imports System.Math
Imports System.Drawing.Graphics
Public Class Line

    Public b As Bitmap
    Public Title As String = "Default Title"
    Public SubTitle As String = "Default Sub Title"
    Public TaskTitle As String = "Default Task Title"
    Public SpecTitle As String = "Default Spec Title"
    Public chartValues As ArrayList = New ArrayList
    Public xValues As ArrayList = New ArrayList
    Public yValues As ArrayList = New ArrayList
    Public Xorigin As Single = 0
    Public Yorigin As Single = 0
    Public ScaleX As Single
    Public ScaleY As Single
    Public Xdivs As Single = 2
    Public Ydivs As Single = 2
    Private Width, Height As Integer
    Private g As Graphics
    Private p As System.Web.UI.Page
    Private _hi As Decimal
    Private _lo As Decimal
    Private _spec As Decimal

    Structure datapoint
        Public x As Decimal 'Single
        Public y As Decimal 'Single
        Public valid As Boolean
    End Structure
    Public Sub New(ByVal myWidth As Integer, ByVal myHeight As Integer, ByVal myPage As System.Web.UI.Page)
        Width = myWidth
        Height = myHeight
        ScaleX = myWidth
        ScaleY = myHeight
        b = New Bitmap(myWidth, myHeight)
        g = Graphics.FromImage(b)
        p = myPage
    End Sub
    Public Property Spec() As Decimal
        Get
            Return _spec
        End Get
        Set(ByVal Value As Decimal)
            _spec = Value
        End Set

    End Property
    Public Property HiLimit() As Decimal
        Get
            Return _hi
        End Get
        Set(ByVal Value As Decimal)
            _hi = Value
        End Set

    End Property
    Public Property LoLimit() As Decimal
        Get
            Return _lo
        End Get
        Set(ByVal Value As Decimal)
            _lo = Value
        End Set

    End Property
    Public Sub AddValue(ByVal x As Decimal, ByVal y As Decimal)
        Dim myPoint As datapoint
        myPoint.x = x
        myPoint.y = y
        myPoint.valid = True
        chartValues.Add(myPoint)
    End Sub
    Public Sub AddxLabel(ByVal x As String)
        xValues.Add(x)
    End Sub
    Public Sub AddyLabel(ByVal y As String)
        yValues.Add(y)
    End Sub
    Public Sub DrawAngledText(ByVal gr As Graphics, ByVal _
    the_font As Font, ByVal the_brush As Brush, ByVal txt _
    As String, ByVal x As Integer, ByVal y As Integer, _
    ByVal angle_degrees As Single, ByVal y_scale As Single)
        Try
            ' Translate the point to the origin.
            gr.TranslateTransform(-x, -y, _
                Drawing2D.MatrixOrder.Append)

            ' Rotate through the angle.
            gr.RotateTransform(angle_degrees, _
                Drawing2D.MatrixOrder.Append)

            ' Scale vertically by a factor of Tan(angle).
            Dim angle_radians As Double = angle_degrees * PI / 180
            gr.ScaleTransform(1, y_scale, _
                Drawing2D.MatrixOrder.Append)

            ' Find the inverse angle and rotate back.
            angle_radians = Math.Atan(y_scale * Tan(angle_radians))
            angle_degrees = CSng(angle_radians * 180 / PI)
            gr.RotateTransform(-angle_degrees, _
                Drawing2D.MatrixOrder.Append)

            ' Translate the origin back to the point.
            gr.TranslateTransform(x, y, _
                Drawing2D.MatrixOrder.Append)

            ' Draw the text.
            Dim gstring As Graphics
            gr.TextRenderingHint = gstring.TextRenderingHint.AntiAliasGridFit
            gr.DrawString(txt, the_font, the_brush, x, y)
        Catch ex As Exception

        End Try

    End Sub
    Public Sub Draw()
        Dim i As Integer
        Dim x, y, x0, y0 As Single
        Dim myLabel As String
        Dim redpen As Pen = New Pen(System.Drawing.Color.Red, 2)
        Dim bluepen As Pen = New Pen(System.Drawing.Color.Blue, 2)
        Dim graypen As Pen = New Pen(System.Drawing.Color.Gray, 1)
        Dim greenpen As Pen = New Pen(System.Drawing.Color.Green, 2)
        Dim blackpen As Pen = New Pen(System.Drawing.Color.Black, 1)
        Dim blackBrush As Brush = New SolidBrush(System.Drawing.Color.Black)
        Dim axesFont As Font = New Font("Arial", 8)
        'drawing area
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias
        p.Response.ContentType = "image/jpeg"
        g.FillRectangle(New SolidBrush(System.Drawing.Color.White), 0, 0, Width, Height)
        Dim ChartInset As Integer = 80
        Dim ChartTop As Integer = 80
        Dim ChartWidth As Integer = Width - (2 * ChartInset)
        Dim ChartHeight As Integer = Height - (2 * ChartInset)
        g.DrawRectangle(New Pen(System.Drawing.Color.Black, 1), ChartInset, ChartInset, ChartWidth, ChartHeight)
        g.FillRectangle(New SolidBrush(System.Drawing.Color.White), ChartInset + 1, ChartInset + 1, ChartWidth - 1, ChartHeight - 1)
        g.DrawString(Title, New Font("arial", 12, FontStyle.Bold), blackBrush, Width / 4, 0)
        g.DrawString(SubTitle, New Font("arial", 10, FontStyle.Bold), blackBrush, Width / 4, 20)
        g.DrawString(TaskTitle, New Font("arial", 10, FontStyle.Bold), blackBrush, Width / 4, 40)
        g.DrawString(SpecTitle, New Font("arial", 8, FontStyle.Bold), blackBrush, Width / 4, 60)
        'x axis labels
        xValues.Reverse()
        For i = 0 To Xdivs
            x = ChartInset + (i * ChartWidth) / Xdivs
            y = ChartHeight + ChartInset
            myLabel = xValues(i).ToString '"test" '(Xorigin + (ScaleX * i / Xdivs)).ToString()
            Dim drawFormat As New StringFormat
            drawFormat.FormatFlags = StringFormatFlags.DirectionVertical
            'DrawAngledText(g, axesFont, blackBrush, myLabel, x - 4, y + 10, 45, 10)
            g.DrawString(myLabel, axesFont, blackBrush, x - 30, y + 10)
            g.DrawLine(blackpen, x, y + 2, x, y - 2)

        Next
        'y axis labels
        For i = 0 To Ydivs
            x = ChartInset
            y = ChartHeight + ChartInset - (i * ChartHeight / Ydivs)

            'myLabel = yValues(i).ToString
            myLabel = (Yorigin + (ScaleY * i / Ydivs)).ToString()
            g.DrawString(myLabel, axesFont, blackBrush, 5, y - 6)
            Dim tst As String = y - 6
            g.DrawLine(blackpen, x + 2, y, x - 2, y)
            g.DrawLine(graypen, x + 2, y, ChartWidth + ChartInset, y)

        Next



        'transform drawing coords to lower-left (0,0)
        g.RotateTransform(180)
        g.TranslateTransform(0, -Height)
        g.TranslateTransform(-ChartInset, ChartInset)
        g.ScaleTransform(-1, 1)

        y = ChartHeight * (HiLimit - Yorigin) / ScaleY
        If y > 0 Then
            g.DrawLine(redpen, x - ChartInset, y, ChartWidth, y)
        End If
        y = ChartHeight * (LoLimit - Yorigin) / ScaleY
        If y > 0 Then
            g.DrawLine(redpen, x - ChartInset, y, ChartWidth, y)
        End If
        y = ChartHeight * (Spec - Yorigin) / ScaleY
        If y > 0 Then
            g.DrawLine(greenpen, x - ChartInset, y, ChartWidth, y)
        End If

        'draw chart data
        Dim prevPoint As datapoint = New datapoint
        prevPoint.valid = False
        'chartValues.Sort()
        chartValues.Reverse()
        For Each myPoint As datapoint In chartValues
            If prevPoint.valid = True Then
                x0 = ChartWidth * (prevPoint.x - Xorigin) / ScaleX
                y0 = ChartHeight * (prevPoint.y - Yorigin) / ScaleY
                x = ChartWidth * (myPoint.x - Xorigin) / ScaleX
                y = ChartHeight * (myPoint.y - Yorigin) / ScaleY
                g.DrawLine(bluepen, x0, y0, x, y)
                g.FillEllipse(blackBrush, x0 - 2, y0 - 2, 5, 5)
                g.FillEllipse(blackBrush, x - 2, y - 2, 5, 5)
            End If
            prevPoint = myPoint
        Next
        b.Save(p.Response.OutputStream, ImageFormat.Jpeg)

    End Sub

End Class

