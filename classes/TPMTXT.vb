

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.HttpServerUtility
Public Class TPMTXT

    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid As String
    Public Function WritePM(ByVal eqid As String, ByVal eqnum As String, Optional ByVal pdm As String = "") As String
        Dim app As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim newfile As String = HttpContext.Current.Server.MapPath("\") & app & "\eqimages\docs\" & eqid & "_" & eqnum & pdm & ".txt"
        'If File.Exists(newfile) Then
        'File.Delete1(newfile)
        'End If
        Dim sw As StreamWriter = New StreamWriter(HttpContext.Current.Server.MapPath("\") & app & "\eqimages\docs\" & eqid & "_" & eqnum & pdm & ".txt")
        If pdm <> "" Then
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        Else
            sql = "usp_getWITotalTPM '" & eqid & "'"
        End If

        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim task, lube, meas, revtask, revtask2, revtask3, revtask4 As String
        Dim tlem, splen, bklen, splen2, bklen2, splen3, bklen3, splen4, bklen4 As Integer
        Dim parts, tools, lubes, tasknum As String
        parts = ""
        tools = ""
        lubes = ""
        Dim lines As Integer = 1
        pms.Open()
        dr = pms.GetRdrData(sql)
        Dim pretech, pretechchk As String
        While dr.Read
            skill = "Operator / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            'pretech = dr.Item("ptid").ToString
            Dim skchk, skchk2 As String
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            If skchk <> skchk2 Then 'OrElse pretech <> pretechchk Then
                If skillchk <> "" Then

                    sw.WriteLine("                                                                                                          ")
                    sw.WriteLine(tmod.getxlbl("xlb152" , "TPMTXT.vb"))
                    'parts = dr.Item("parts").ToString
                    Dim partarr As String() = Split(parts, ";")
                    Dim p As Integer
                    For p = 0 To partarr.Length - 1
                        If partarr(p) = "none" Then ' Len(partarr(p)) = 0 Or 
                            sw.WriteLine(tmod.getxlbl("xlb153" , "TPMTXT.vb"))
                        Else
                            sw.WriteLine(partarr(p))
                        End If
                    Next

                    sw.WriteLine("                                                                                                          ")
                    sw.WriteLine(tmod.getxlbl("xlb154" , "TPMTXT.vb"))
                    'tools = dr.Item("tools").ToString
                    Dim toolarr As String() = Split(tools, ";")
                    Dim t As Integer
                    For t = 0 To toolarr.Length - 1
                        If toolarr(t) = "none" Then 'Len(toolarr(t)) = 0 Or 
                            sw.WriteLine(tmod.getxlbl("xlb155" , "TPMTXT.vb"))
                        Else
                            sw.WriteLine(toolarr(t))
                        End If
                    Next

                    sw.WriteLine("                                                                                                          ")
                    sw.WriteLine(tmod.getxlbl("xlb156" , "TPMTXT.vb"))
                    'lubes = dr.Item("lubes").ToString
                    Dim lubearr As String() = Split(lubes, ";")
                    Dim l As Integer
                    For l = 0 To lubearr.Length - 1
                        If lubearr(l) = "none" Then 'Len(lubearr(l)) = 0 Or 
                            sw.WriteLine(tmod.getxlbl("xlb157" , "TPMTXT.vb"))
                        Else
                            sw.WriteLine(lubearr(l))
                        End If

                    Next
                    parts = ""
                    tools = ""
                    lubes = ""
                    sw.WriteLine("                                                                                                          ")
                    sw.WriteLine("**********************************************************************************************************")
                    sw.WriteLine("**********************************************************************************************************")
                    sw.WriteLine("                                                                                                          ")

                End If
                skillchk = skill
                start = 0
                flag = 0
                If pdm <> "" Then
                    sw.WriteLine("PdM - " & dr.Item("pretech").ToString & " - " & dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString)
                Else
                    sw.WriteLine(tmod.getxlbl("xlb160" , "TPMTXT.vb") & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString)
                End If
                sw.WriteLine(tmod.getxlbl("xlb158" , "TPMTXT.vb") & "  " & dr.Item("down").ToString & " " & tmod.getxlbl("xlb159" , "TPMTXT.vb"))
                sw.WriteLine(dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString)
                sw.WriteLine("                                                                                                          ")
                sw.WriteLine(tmod.getxlbl("xlb161" , "TPMTXT.vb"))
                sw.WriteLine("                                                                                                          ")
            End If
            If start <> 0 Then
                sw.WriteLine("                                                                                                          ")
                sw.WriteLine("**********************************************************************************************************")
            End If
            func = dr.Item("func").ToString
            If flag = 0 Then
                flag = 1
                funcchk = func
                sw.WriteLine("                                                                                                          ")
                sw.WriteLine(dr.Item("func").ToString)
                sw.WriteLine("                                                                                                          ")
            Else
                If func <> funcchk Then

                    funcchk = func
                    sw.WriteLine("                                                                                                          ")
                    sw.WriteLine("**********************************************************************************************************")
                    sw.WriteLine("                                                                                                          ")
                    sw.WriteLine(dr.Item("func").ToString)
                    sw.WriteLine("                                                                                                          ")
                End If
            End If
            Dim subtask = dr.Item("subtask").ToString
            Dim subt = dr.Item("subt").ToString
            task = dr.Item("task").ToString
            If dr.Item("lubes").ToString <> "none" Or Len(dr.Item("lubes").ToString) = 0 Then
                lube = "; " & dr.Item("lubes").ToString
            Else
                lube = ""
            End If
            'If dr.Item("lube").ToString <> "none" Then
            'lube = "; " & dr.Item("lube").ToString
            'Else
            'lube = ""
            'End If
            If dr.Item("meas").ToString <> "none" Then
                meas = "; " & dr.Item("meas").ToString
            Else
                meas = ""
            End If

            If subtask = 0 Then
                task = task & lube & meas
                tlem = Len(task)
            Else
                task = subt
                tlem = Len(subt)
            End If

            If tlem <= 103 Then
                lines = 1
            End If
            If tlem > 103 Then
                lines = 2
                revtask = StrReverse(Mid(task, 1, 103))
                revtask = Trim(revtask)
                Dim rev As Integer = Len(revtask)
                splen = revtask.IndexOf(" ")
                bklen = rev - splen
            End If
            If tlem > 206 Then
                lines = 3
                revtask2 = StrReverse(Mid(task, bklen, 103))
                revtask2 = Trim(revtask2)
                Dim rev2 As Integer = Len(revtask2)
                splen2 = revtask2.IndexOf(" ")
                bklen2 = (rev2) - splen2 + 1
            End If
            If tlem > 309 Then
                lines = 4
                revtask3 = StrReverse(Mid(task, bklen2, 103))
                revtask3 = Trim(revtask3)
                Dim rev3 As Integer = Len(revtask3)
                splen3 = revtask3.IndexOf(" ")
                bklen3 = (rev3) - splen3 + 1
            End If
            If tlem > 412 Then
                lines = 5
                revtask4 = StrReverse(Mid(task, bklen3, 103))
                revtask4 = Trim(revtask4)
                Dim rev4 As Integer = Len(revtask4)
                splen4 = revtask4.IndexOf(" ")
                bklen4 = (rev4) - splen4 + 1
            End If
            Select Case lines
                Case 1
                    If subtask <> 0 Then
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & task)
                    Else
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & task)
                    End If
                Case 2
                    If subtask <> 0 Then
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sw.WriteLine("  " & Mid(task, bklen, tlem))
                Case 3
                    If subtask <> 0 Then
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sw.WriteLine("  " & Mid(task, bklen, bklen2))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2, tlem))
                Case 4
                    If subtask <> 0 Then
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sw.WriteLine("  " & Mid(task, bklen, bklen2))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2, bklen3))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2 + bklen3, tlem))
                Case 5
                    If subtask <> 0 Then
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sw.WriteLine("  " & Mid(task, bklen, bklen2))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2, bklen3))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2 + bklen3, bklen4))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem))
                Case Else
                    If subtask <> 0 Then
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sw.WriteLine("  " & Mid(task, bklen, bklen2))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2, bklen3))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2 + bklen3, bklen4))
                    sw.WriteLine("  " & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem))
            End Select
            tasknum = dr.Item("tasknum").ToString
            sw.WriteLine("   OK(___) " & dr.Item("fm1").ToString)
            If dr.Item("parts").ToString <> "none" Then 'Or Len(dr.Item("parts").ToString) = 0 Then
                If Len(parts) = 0 Then
                    parts += dr.Item("parts").ToString
                Else
                    parts += ";" & dr.Item("parts").ToString
                End If
            End If

            If dr.Item("tools").ToString <> "none" Then 'Or Len(dr.Item("tools").ToString) <> 0 Then
                If Len(tools) = 0 Then
                    tools += dr.Item("tools").ToString
                Else
                    tools += ";" & dr.Item("tools").ToString
                End If

            End If

            If dr.Item("lubes").ToString <> "none" Then 'Or Len(dr.Item("lubes").ToString) <> 0 Then
                If Len(lubes) = 0 Then
                    lubes += dr.Item("lubes").ToString
                Else
                    lubes += ";" & dr.Item("lubes").ToString
                End If

            End If


        End While
        dr.Close()

        sw.WriteLine("                                                                                                          ")
        sw.WriteLine(tmod.getxlbl("xlb162" , "TPMTXT.vb"))

        Dim partarr1 As String() = Split(parts, ";")
        Dim p1 As Integer
        For p1 = 0 To partarr1.Length - 1
            If Len(partarr1(p1)) = 0 Or partarr1(p1) = "none" Then
                sw.WriteLine(tmod.getxlbl("xlb163" , "TPMTXT.vb"))
            Else
                sw.WriteLine(partarr1(p1))
            End If
        Next

        sw.WriteLine("                                                                                                          ")
        sw.WriteLine(tmod.getxlbl("xlb164" , "TPMTXT.vb"))

        Dim toolarr1 As String() = Split(tools, ";")
        Dim t1 As Integer
        For t1 = 0 To toolarr1.Length - 1
            If Len(toolarr1(t1)) = 0 Or toolarr1(t1) = "none" Then
                sw.WriteLine(tmod.getxlbl("xlb165" , "TPMTXT.vb"))
            Else
                sw.WriteLine(toolarr1(t1))
            End If
        Next

        sw.WriteLine("                                                                                                          ")
        sw.WriteLine(tmod.getxlbl("xlb166" , "TPMTXT.vb"))

        Dim lubearr1 As String() = Split(lubes, ";")
        Dim l1 As Integer
        For l1 = 0 To lubearr1.Length - 1
            If Len(lubearr1(l1)) = 0 Or lubearr1(l1) = "none" Then
                sw.WriteLine(tmod.getxlbl("xlb167" , "TPMTXT.vb"))
            Else
                sw.WriteLine(lubearr1(l1))
            End If

        Next

        sw.WriteLine("                                                                                                          ")
        sw.WriteLine("**********************************************************************************************************")
        sw.WriteLine("**********************************************************************************************************")
        sw.WriteLine("                                                                                                          ")

        'Parts
        'sw.WriteLine("Parts")
        'Dim pflg As String = 0
        'If pdm <> "" Then
        'sql = "usp_getWIPartsTotalPdM '" & eqid & "', '1'"
        'Else
        'sql = "usp_getWIPartsTotalPdM '" & eqid & "', '0'"
        'End If

        'dr = pms.GetRdrData(sql)
        'While dr.Read
        'If Len(dr.Item("parts").ToString) > 0 Then
        'pflg = "1"
        'sw.WriteLine(dr.Item("func").ToString & " - [" & dr.Item("tasknum").ToString & "] " & dr.Item("parts").ToString)
        'End If
        'End While
        'dr.Close()
        'If pflg <> "1" Then
        'sw.WriteLine("No Parts Found")
        'End If
        'Tools'
        'sw.WriteLine("Tools")
        'Dim tflg As String = 0
        'If pdm <> "" Then
        'sql = "usp_getWIToolsTotalPdM '" & eqid & "', '1'"
        'Else
        'sql = "usp_getWIToolsTotalPdM '" & eqid & "', '0'"
        'End If
        'dr = pms.GetRdrData(sql)
        'While dr.Read
        'If Len(dr.Item("parts").ToString) > 0 Then
        'tflg = "1"
        'sw.WriteLine(dr.Item("func").ToString & " - [" & dr.Item("tasknum").ToString & "] " & dr.Item("parts").ToString)
        'End If
        'End While
        'dr.Close()
        'If tflg <> "1" Then
        'sw.WriteLine("No Tools Found")
        'End If
        'Lubes'
        'sw.WriteLine("Lubricants")
        'Dim lflg As String = 0
        'If pdm <> "" Then
        'sql = "usp_getWILubesTotalPdM '" & eqid & "', '1'"
        'Else
        'sql = "usp_getWILubesTotalPdM '" & eqid & "', '0'"
        'End If
        'dr = pms.GetRdrData(sql)
        'While dr.Read
        'If Len(dr.Item("parts").ToString) > 0 Then
        'lflg = "1"
        'sw.WriteLine(dr.Item("func").ToString & " - [" & dr.Item("tasknum").ToString & "] " & dr.Item("parts").ToString)
        'End If
        'End While
        'dr.Close()
        'If lflg <> "1" Then
        'sw.W'riteLine("No Lubricants Found")
        'End If
        pms.Dispose()
        sw.Close()
        Dim href As String = "../eqimages/docs/" & eqid & "_" & eqnum & pdm & ".txt"
        Return href
    End Function
End Class

