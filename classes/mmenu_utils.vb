Imports System.Data.SqlClient

Public Class mmenu_utils
    Dim tmod As New transmod
    Dim mu As New Utilities
    Private intro, link, access As String
    Private langtyp, dfltlang, appvers As String
    Private pscnt As Integer = 2
    Private dfirst As Integer = 1
    Private csched, sql As String
    Private isched As Integer = 1
    Private isintra As Integer = 1
    Private isuni As Integer = 0
    Private isecd As Integer = 1
    Private sawo As Integer = 1
    Private compid As String = "NOT"
    Public Property COMPI() As String
        Get
            Return compid
        End Get
        Set(ByVal Value As String)
            compid = Value
        End Set
    End Property
    Public Property SAW() As Integer
        '*** 0 for no, 1 for yes ***
        Get
            Return sawo
        End Get
        Set(ByVal Value As Integer)
            sawo = Value
        End Set
    End Property
    Public Property ECD() As Integer
        '*** 0 for no, 1 for yes ***
        Get
            Return isecd
        End Get
        Set(ByVal Value As Integer)
            isecd = Value
        End Set
    End Property
    Public Property UNI() As Integer
        '*** 0 for reg, 1 for uni ***
        Get
            Return isuni
        End Get
        Set(ByVal Value As Integer)
            isuni = Value
        End Set
    End Property
    Public Property INTRA() As Integer
        '*** 0 for reg, 1 for intra ***
        Get
            Return isintra
        End Get
        Set(ByVal Value As Integer)
            isintra = Value
        End Set
    End Property
    Public Property FDAY() As Integer
        Get
            Return dfirst
        End Get
        Set(ByVal Value As Integer)
            dfirst = Value
        End Set
    End Property
    Public Property GetPSCNT() As String
        Get
            Return pscnt
        End Get
        Set(ByVal Value As String)
            pscnt = Value
        End Set
    End Property
    Public Property Sched() As String
        Get
            Return GetSched()
        End Get
        Set(ByVal Value As String)
            csched = Value
        End Set
    End Property
    Public Property Is_Sched() As Integer
        Get
            Return isched
        End Get
        Set(ByVal Value As Integer)
            isched = Value
        End Set
    End Property
    Private Function GetSched() As String
        sql = "select [default] from wovars where [column] = 'sched'"
        mu.Open()
        csched = mu.strScalar(sql)
        mu.Dispose()
        csched = "yes"
        Return csched
    End Function

#Region "Private Values for Application Access"
    '**** 0 for Access - 1 for Denied ****
    Private asa As Integer = 0 'PM Assurance Admin
    Private asam As Integer = 0 'Assessment Admin
    Private asm As Integer = 0 'Assessment Master
    Private asu As Integer = 0 'PM Assurance
    Private clib As Integer = 0 'Component Library
    Private dev As Integer = 0 'PM Developer
    Private ecr As Integer = 0 'PM ECR Administration
    Private ep As Integer = 0 'Edit Profile
    Private eq As Integer = 0 'PM Equipment
    Private forum As Integer = 0 'PM Forums
    Private inc As Integer = 1 'PM Inventory Control
    Private inv As Integer = 0 'PM Inventory Administration
    Private job As Integer = 0 'Job Plans
    Private plib As Integer = 0 'PM Library
    Private loc As Integer = 0 'PM Locations
    Private man As Integer = 0 'PM Manager
    Private mea As Integer = 0 'Measurements
    Private opt As Integer = 0 'PM Optimizer
    Private prc As Integer = 0 'PM Archive
    Private rep As Integer = 0 'PM Reports
    Private res As Integer = 0 'PM Resources
    Private rte As Integer = 0 'PM Routes
    Private sch As Integer = 0 'PM Scheduler
    Private setup As Integer = 0 'Application Set-Up
    Private sqla As Integer = 0 'SQL Analyzer
    Private tan As Integer = 0 'TPM Manager
    Private tpd As Integer = 0 'TPM Developer
    Private tpo As Integer = 0 'TPM Optimizer
    Private tpp As Integer = 0 'TPM Performer
    Private trc As Integer = 0 'TPM Archive
    Private ua As Integer = 0 'PM User Administration
    Private wo As Integer = 0 'Work Manager
    Private wra As Integer = 1 'Work Request Admin
    Private wrr As Integer = 0 'Work Requests

    Private fwp As Integer = 0 'Four Week Planner
    Private sdp As Integer = 0 'Weekly Planner
    Private wob As Integer = 0 'Work Order Backlog
#End Region


#Region "New Login Email Entries"
    Public Property AppIntro() As String
        Get
            Return GetIntro()
        End Get
        Set(ByVal Value As String)
            intro = Value
        End Set
    End Property
    Public Property AppLink() As String
        Get
            Return GetLink()
        End Get
        Set(ByVal Value As String)
            link = Value
        End Set
    End Property
    Public Property AppAccess() As String
        Get
            Return GetAccess()
        End Get
        Set(ByVal Value As String)
            access = Value
        End Set
    End Property
    Public Function GetIntro() As String
        intro = tmod.getxlbl("xlb211", "mmenu_utils.vb")
        Return intro
    End Function
    Public Function GetLink() As String
        intro = tmod.getxlbl("xlb212", "mmenu_utils.vb")
        Return intro
    End Function
    Public Function GetAccess() As String
        intro = tmod.getxlbl("xlb213", "mmenu_utils.vb")
        Return intro
    End Function
#End Region
#Region "Application Version Property"
    '**** 0 for Standard - 1 for GRP ****
    Public Property AppVer() As String
        Get
            Return GetVer()
        End Get
        Set(ByVal Value As String)
            access = Value
        End Set
    End Property

    Public Function GetVer() As String
        appvers = 0
        Return appvers
    End Function
#End Region
#Region "Application Language Properties"
    Public Property AppLangTyp() As String
        Get
            Return GetLangTyp()
        End Get
        Set(ByVal Value As String)
            langtyp = Value
        End Set
    End Property
    Public Property AppDfltLang() As String
        Get
            Return GetDfltLang()
        End Get
        Set(ByVal Value As String)
            dfltlang = Value
        End Set
    End Property
    Public Function GetDfltLang() As String
        dfltlang = "eng"
        Return dfltlang
    End Function
    Public Function GetLangTyp() As String
        langtyp = "yes"
        Return langtyp
    End Function
#End Region
#Region "Application Access Properties - Use Private Values Above"
    Public Property GetMan() As String
        Get
            Return man
        End Get
        Set(ByVal Value As String)
            man = Value
        End Set
    End Property
    Public Property GetWO() As String
        Get
            Return wo
        End Get
        Set(ByVal Value As String)
            wo = Value
        End Set
    End Property
    Public Property GetWR() As String
        Get
            Return wrr
        End Get
        Set(ByVal Value As String)
            wrr = Value
        End Set
    End Property
    Public Property GetTan() As String
        Get
            Return tan
        End Get
        Set(ByVal Value As String)
            tan = Value
        End Set
    End Property
    Public Property GetOpt() As String
        Get
            Return opt
        End Get
        Set(ByVal Value As String)
            opt = Value
        End Set
    End Property
    Public Property GetDev() As String
        Get
            Return dev
        End Get
        Set(ByVal Value As String)
            dev = Value
        End Set
    End Property
    Public Property GetEQ() As String
        Get
            Return eq
        End Get
        Set(ByVal Value As String)
            eq = Value
        End Set
    End Property
    Public Property GetTPD() As String
        Get
            Return tpd
        End Get
        Set(ByVal Value As String)
            tpd = Value
        End Set
    End Property
    Public Property GetTPO() As String
        Get
            Return tpo
        End Get
        Set(ByVal Value As String)
            tpo = Value
        End Set
    End Property

#End Region
#Region "Main Menu Drop Down List - Use Private Values Above - Site Assignments are Required"
    Public Function MainMenuList()
        Dim dtmlist As DataTable = New DataTable("tblmlist")
        Dim dtcol As DataColumn
        Dim dtrow As DataRow

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "mid"
        dtmlist.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "micon"
        dtmlist.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "hovicon"
        dtmlist.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "mname"
        dtmlist.Columns.Add(dtcol)

        dtrow = dtmlist.NewRow()
        dtrow("mid") = "0"
        dtrow("micon") = "../images/appbuttons/minibuttons/start.gif"
        dtrow("hovicon") = "../images/appbuttons/minibuttons/start.gif"
        dtrow("mname") = tmod.getxlbl("xlb214", "mmenu_utils.vb")
        dtmlist.Rows.Add(dtrow)

        dtrow = dtmlist.NewRow()
        dtrow("mid") = "1"
        dtrow("micon") = "../images/appbuttons/minibuttons/admin.gif"
        dtrow("hovicon") = "../images/appbuttons/minibuttons/admin.gif"
        dtrow("mname") = tmod.getxlbl("xlb215", "mmenu_utils.vb")
        dtmlist.Rows.Add(dtrow)

        dtrow = dtmlist.NewRow()
        dtrow("mid") = "2"
        dtrow("micon") = "../images/appbuttons/minibuttons/addmod.gif"
        dtrow("hovicon") = "../images/appbuttons/minibuttons/addmod.gif"
        dtrow("mname") = tmod.getxlbl("xlb216", "mmenu_utils.vb")
        dtmlist.Rows.Add(dtrow)

        dtrow = dtmlist.NewRow()
        dtrow("mid") = "3"
        dtrow("micon") = "../images/appbuttons/minibuttons/globe.gif"
        dtrow("hovicon") = "../images/appbuttons/minibuttons/globegrn.gif"
        dtrow("mname") = tmod.getxlbl("xlb217", "mmenu_utils.vb")
        dtmlist.Rows.Add(dtrow)

        Dim ds As New DataSet
        ds.Tables.Add(dtmlist)
        Return ds

    End Function

    Public Function MainMenuApps(ByVal thispage As String, ByVal appname As String, ByVal appstr As String) As DataSet
        Dim dtmapps As DataTable = New DataTable("tblmapps")
        Dim dtcol As DataColumn
        Dim dtrow As DataRow

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "menuid"
        dtmapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "app"
        dtmapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "appicon"
        dtmapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "hovicon"
        dtmapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "appname"
        dtmapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "apploc"
        dtmapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "active"
        dtmapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "disicon"
        dtmapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "hovdisicon"
        dtmapps.Columns.Add(dtcol)

        'Add Apps Here
        Dim appflag As Integer = -1
        'Main
        If dev = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "dev"
            dtrow("appicon") = "../images/appbuttons/minibuttons/3gearstrans.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/3gearstrans_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/3gearstranshov.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/3gearstranshov_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb218", "mmenu_utils.vb")
            dtrow("apploc") = "../apps/PMTasks.aspx?jump=no"
            appflag = appstr.IndexOf("dev")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If opt = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "opt"
            dtrow("appicon") = "../images/appbuttons/minibuttons/compress.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/compress_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/compress.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/compress_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb219", "mmenu_utils.vb")
            dtrow("apploc") = "../appsopt/PM3OptMain.aspx?jump=no"
            appflag = appstr.IndexOf("opt")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If tpd = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "tpd"
            dtrow("appicon") = "../images/appbuttons/minibuttons/3gearstranstpm.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/3gearstranstpm_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/3gearstranstpmhov.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/3gearstranstpmhov_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb220", "mmenu_utils.vb")
            dtrow("apploc") = "../appstpm/tpmtasksmain.aspx?jump=no"
            appflag = appstr.IndexOf("tpd")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If tpo = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "tpo"
            dtrow("appicon") = "../images/appbuttons/minibuttons/compresstpm.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/compresstpm_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/compresstpm.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/compresstpm_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb221", "mmenu_utils.vb")
            dtrow("apploc") = "../appsopttpm/tpmopttasksmain.aspx?jump=no"
            appflag = appstr.IndexOf("tpo")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If man = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "man"
            dtrow("appicon") = "../images/appbuttons/minibuttons/cal1.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/cal1_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/cal1.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/cal1_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb222", "mmenu_utils.vb")
            dtrow("apploc") = "../appsman/PMManager.aspx?jump=no"
            appflag = appstr.IndexOf("man")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If tan = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "tan"
            dtrow("appicon") = "../images/appbuttons/minibuttons/cal1.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/cal1_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/cal1.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/cal1_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb223", "mmenu_utils.vb")
            dtrow("apploc") = "../appsmantpm/TPMManager.aspx?jump=no"
            appflag = appstr.IndexOf("tan")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If tpp = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "tpp"
            dtrow("appicon") = "../images/appbuttons/minibuttons/cal1.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/cal1_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/cal1.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/cal1_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb224", "mmenu_utils.vb")
            dtrow("apploc") = "../tpmperf/tpmmain.aspx?jump=no"
            appflag = appstr.IndexOf("tpp")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If wo = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "wo"
            dtrow("appicon") = "../images/appbuttons/minibuttons/wo.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/wo_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/wo.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/wo_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb225", "mmenu_utils.vb")
            dtrow("apploc") = "../appswo/wolabmain.aspx"
            appflag = appstr.IndexOf("wo")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If wrr = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "wrr"
            dtrow("appicon") = "../images/appbuttons/minibuttons/woreq.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/woreq_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/woreq.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/woreq_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb226", "mmenu_utils.vb")
            dtrow("apploc") = "../appswo/wrmain.aspx"
            appflag = appstr.IndexOf("wrr")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)

        End If

        If prc = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "prc"
            dtrow("appicon") = "../images/appbuttons/minibuttons/archtop1transpm.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/archtop1transpm_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/archtop1transpm.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/archtop1transpm_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb227", "mmenu_utils.vb")
            dtrow("apploc") = "../appsarch/pmarchmain.aspx?jump=no"
            appflag = appstr.IndexOf("prc")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If trc = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "trc"
            dtrow("appicon") = "../images/appbuttons/minibuttons/archtop1trans.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/archtop1trans_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/archtop1trans.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/archtop1trans_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb228", "mmenu_utils.vb")
            dtrow("apploc") = "../appsarchtpm/pmarchmaintpm.aspx?jump=no"
            appflag = appstr.IndexOf("trc")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If rep = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "0"
            dtrow("app") = "rep"
            dtrow("appicon") = "../images/appbuttons/minibuttons/lilreport1trans.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/lilreport1trans_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/lilreport1transhov.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/lilreport1transhov_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb229", "mmenu_utils.vb")
            dtrow("apploc") = ""
            appflag = appstr.IndexOf("rep")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If


        'Admin
        If setup = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "setup"
            dtrow("appicon") = "../images/appbuttons/minibuttons/projects.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/projects_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/projects.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/projects_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb230", "mmenu_utils.vb")
            dtrow("apploc") = "../admin/Appset.aspx"
            appflag = appstr.IndexOf("setup")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If eq = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "eq"
            dtrow("appicon") = "../images/appbuttons/minibuttons/eqtrans.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/eqtrans_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/eqtranshov.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/eqtranshov_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb231", "mmenu_utils.vb")
            dtrow("apploc") = "../equip/eqmain2.aspx?start=no"
            appflag = appstr.IndexOf("eq")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If ecr = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "ecr"
            dtrow("appicon") = "../images/appbuttons/minibuttons/calctrans.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/calctrans_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/calctranshov.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/calctranshov_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb232", "mmenu_utils.vb")
            dtrow("apploc") = "../equip/ECRAdm.aspx"
            appflag = appstr.IndexOf("ecr")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If inv = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "inv"
            dtrow("appicon") = "../images/appbuttons/minibuttons/invtrans.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/invtrans_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/invtranshov.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/invtranshov_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb233", "mmenu_utils.vb")
            dtrow("apploc") = "../inv/invmainmenu.aspx"
            appflag = appstr.IndexOf("inv")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)

        End If

        If inc = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "inc"
            dtrow("appicon") = "../images/appbuttons/minibuttons/invtrans.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/invtrans_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/invtranshov.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/invtranshov_dis.gif"
            dtrow("appname") = "PM Inventory Control"
            dtrow("apploc") = "../inv/invmain.aspx"
            appflag = appstr.IndexOf("inc")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If


        If ua = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "ua"
            dtrow("appicon") = "../images/appbuttons/minibuttons/magnifier.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/magnifier_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/magnifier.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/magnifier_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb234", "mmenu_utils.vb")
            dtrow("apploc") = "../security/useradminmain.aspx"
            appflag = appstr.IndexOf("ua")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)

        End If

        If ep = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "ep"
            dtrow("appicon") = "../images/appbuttons/minibuttons/pencil.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/pencil_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/pencil.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/pencil_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb235", "mmenu_utils.vb")
            dtrow("apploc") = "../security/editprofilemain.aspx"
            appflag = appstr.IndexOf("ep")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If asa = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "asa"
            dtrow("appicon") = "../images/appbuttons/minibuttons/write.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/write_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/write.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/write_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb236", "mmenu_utils.vb")
            dtrow("apploc") = "../admin/AppSetpmAppr.aspx"
            appflag = appstr.IndexOf("asa")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If wra = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "1"
            dtrow("app") = "wra"
            dtrow("appicon") = "../images/appbuttons/minibuttons/woreq.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/woreq_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/woreq.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/woreq_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb237", "mmenu_utils.vb")
            dtrow("apploc") = "../labor/wrassignmain.aspx"
            appflag = appstr.IndexOf("wra")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        'Add-Ons
        If plib = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "lib"
            dtrow("appicon") = "../images/appbuttons/minibuttons/lillib.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/lillib_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/lillib.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/lillib_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb238", "mmenu_utils.vb")
            dtrow("apploc") = "../equip/eqcopymain.aspx"
            appflag = appstr.IndexOf("lib")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If clib = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "clib"
            dtrow("appicon") = "../images/appbuttons/minibuttons/lillib.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/lillib_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/lillib.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/lillib_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb239", "mmenu_utils.vb")
            dtrow("apploc") = "../complib/complib.aspx"
            appflag = appstr.IndexOf("clib")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If sch = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "sch"
            dtrow("appicon") = "../images/appbuttons/minibuttons/sched.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/sched_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/sched.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/sched_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb240", "mmenu_utils.vb")
            dtrow("apploc") = ""
            appflag = appstr.IndexOf("sch")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If fwp = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "fwp"
            dtrow("appicon") = "../images/appbuttons/minibuttons/sched.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/sched_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/sched.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/sched_dis.gif"
            dtrow("appname") = "Four Week Planner"
            dtrow("apploc") = ""
            appflag = appstr.IndexOf("fwp")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If sdp = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "sdp"
            dtrow("appicon") = "../images/appbuttons/minibuttons/sched.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/sched_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/sched.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/sched_dis.gif"
            dtrow("appname") = "Weekly Planner"
            dtrow("apploc") = ""
            appflag = appstr.IndexOf("sdp")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If wob = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "wob"
            dtrow("appicon") = "../images/appbuttons/minibuttons/sched.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/sched_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/sched.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/sched_dis.gif"
            dtrow("appname") = "Work Order Backlog"
            dtrow("apploc") = ""
            appflag = appstr.IndexOf("wob")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If rte = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "rte"
            dtrow("appicon") = "../images/appbuttons/minibuttons/routepms.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/routepms_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/routepms.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/routepms_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb241", "mmenu_utils.vb")
            dtrow("apploc") = "../appsrt/PMRoutesMain.aspx?jump=no"
            appflag = appstr.IndexOf("rte")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If loc = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "loc"
            dtrow("appicon") = "../images/appbuttons/minibuttons/loctrans.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/loctrans_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/loctranshov.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/loctranshov_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb242", "mmenu_utils.vb")
            dtrow("apploc") = "../locs/pmlocmain.aspx"
            appflag = appstr.IndexOf("loc")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If asu = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "asu"
            dtrow("appicon") = "../images/appbuttons/minibuttons/write.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/write_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/write.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/write_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb243", "mmenu_utils.vb")
            dtrow("apploc") = "../utils/PMApprovalMain.aspx"
            appflag = appstr.IndexOf("asu")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If job = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "job"
            dtrow("appicon") = "../images/appbuttons/minibuttons/planb1.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/planb1_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/planb1.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/planb1_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb244", "mmenu_utils.vb")
            dtrow("apploc") = "../appsrt/JobPlanMain.aspx?jump=no"
            appflag = appstr.IndexOf("job")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If res = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "res"
            dtrow("appicon") = "../images/appbuttons/minibuttons/people2.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/people2_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/people2.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/people2_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb245", "mmenu_utils.vb")
            dtrow("apploc") = "../utils/resourcesmain.aspx"
            appflag = appstr.IndexOf("res")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If forum = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "forum"
            dtrow("appicon") = "../images/appbuttons/minibuttons/forum.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/forum_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/forum.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/forum_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb246", "mmenu_utils.vb")
            dtrow("apploc") = "../forum/forummain2.aspx"
            appflag = appstr.IndexOf("forum")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If sqla = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "sqla"
            dtrow("appicon") = "../images/appbuttons/minibuttons/db.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/dbf.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/db.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/dbf.gif"
            dtrow("appname") = tmod.getxlbl("xlb247", "mmenu_utils.vb")
            dtrow("apploc") = "../utils/SQLAdmin.aspx"
            appflag = appstr.IndexOf("sqla")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If asam = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "asam"
            dtrow("appicon") = "../images/appbuttons/minibuttons/magnifier.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/magnifier_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/magnifier_dis.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/magnifier_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb248", "mmenu_utils.vb")
            dtrow("apploc") = "../asmstr/Assessment_Main2.aspx?atyp=a"
            appflag = appstr.IndexOf("asam")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)

        End If

        If asm = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "asm"
            dtrow("appicon") = "../images/appbuttons/minibuttons/pencil.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/pencil_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/pencil.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/pencil_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb249", "mmenu_utils.vb")
            dtrow("apploc") = "../asmstr/Assessment_Main.aspx?atyp=u"
            appflag = appstr.IndexOf("asm")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        If mea = 0 Then
            dtrow = dtmapps.NewRow()
            dtrow("menuid") = "2"
            dtrow("app") = "mea"
            dtrow("appicon") = "../images/appbuttons/minibuttons/measmenu.gif"
            dtrow("disicon") = "../images/appbuttons/minibuttons/measmenu_dis.gif"
            dtrow("hovicon") = "../images/appbuttons/minibuttons/measmenuy.gif"
            dtrow("hovdisicon") = "../images/appbuttons/minibuttons/measmenuy_dis.gif"
            dtrow("appname") = tmod.getxlbl("xlb250", "mmenu_utils.vb")
            dtrow("apploc") = ""
            appflag = appstr.IndexOf("mea")
            If appflag <> -1 Or appstr = "all" Then
                dtrow("active") = "yes"
            Else
                dtrow("active") = "no"
            End If
            dtmapps.Rows.Add(dtrow)
        End If

        'Sites

        dtrow = dtmapps.NewRow()
        dtrow("menuid") = "3"
        dtrow("app") = "12"
        dtrow("appicon") = "../images/appbuttons/minibuttons/globetrans.gif"
        dtrow("hovicon") = "../images/appbuttons/minibuttons/globetranshov.gif"
        dtrow("appname") = "Practice Site"
        dtrow("apploc") = ""
        dtrow("active") = "yes"
        dtmapps.Rows.Add(dtrow)

        dtrow = dtmapps.NewRow()
        dtrow("menuid") = "3"
        dtrow("app") = "13"
        dtrow("appicon") = "../images/appbuttons/minibuttons/globetrans.gif"
        dtrow("hovicon") = "../images/appbuttons/minibuttons/globetranshov.gif"
        dtrow("appname") = "Canton"
        dtrow("apploc") = ""
        dtrow("active") = "yes"
        dtmapps.Rows.Add(dtrow)

        'End Sites

        Dim ds As New DataSet
        ds.Tables.Add(dtmapps)
        Return ds
    End Function

#End Region


End Class
