

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class PGEMSHTM

    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid As String
    Public Function WritePMHTM(ByVal eqid As String, Optional ByVal pdm As String = "") As String

        Dim sb As New System.Text.StringBuilder
        If pdm <> "" Then
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        Else
            sql = "usp_getWITotalPGEMS '" & eqid & "'"
        End If

        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim task, lube, meas, revtask, revtask2, revtask3, revtask4 As String
        Dim tlem, splen, bklen, splen2, bklen2, splen3, bklen3, splen4, bklen4 As Integer
        Dim eq, fu, rd As String
        Dim lines As Integer = 1
        pms.Open()
        dr = pms.GetRdrData(sql)
        While dr.Read
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            If skill <> skillchk Then
                If skillchk <> "" Then
                    sb.Append("<br>")
                    sb.Append("**********************************************************************************************************")
                    sb.Append("<br>")
                End If
                skillchk = skill
                start = 0
                flag = 0
                If pdm <> "" Then
                    sb.Append("PdM - " & dr.Item("pretech").ToString & " - " & dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString & "<br>")
                Else
                    sb.Append(dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString & "<br>")
                End If
                sb.Append(tmod.getxlbl("xlb14" , "PGEMSHTM.vb") & "  " & dr.Item("down").ToString & " " & tmod.getxlbl("xlb15" , "PGEMSHTM.vb") & "<br>")
                sb.Append(dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "<br><br>")
                sb.Append("<br>")
                sb.Append(tmod.getxlbl("xlb16" , "PGEMSHTM.vb"))
                sb.Append("<br>")
            End If
            If start <> 0 Then
                sb.Append("<br>")
                sb.Append("**********************************************************************************************************")
            End If
            func = dr.Item("func").ToString
            If flag = 0 Then
                flag = 1
                funcchk = func
                sb.Append("<br>")
                sb.Append(dr.Item("func").ToString)
                sb.Append("<br><br>")
            Else
                If func <> funcchk Then

                    funcchk = func
                    sb.Append("<br>")
                    sb.Append("**********************************************************************************************************")
                    sb.Append("<br>")
                    sb.Append(dr.Item("func").ToString)
                    sb.Append("<br><br>")
                End If
            End If
            task = dr.Item("task").ToString
            If dr.Item("lube").ToString <> "none" Then
                lube = "; " & dr.Item("lube").ToString
            Else
                lube = ""
            End If
            If dr.Item("meas").ToString <> "none" Then
                meas = "; " & dr.Item("meas").ToString
            Else
                meas = ""
            End If
            task = task & lube & meas
            tlem = Len(task)
            If tlem <= 103 Then
                lines = 1
            End If
            If tlem > 103 Then
                lines = 2
                revtask = StrReverse(Mid(task, 1, 103))
                revtask = Trim(revtask)
                Dim rev As Integer = Len(revtask)
                splen = revtask.IndexOf(" ")
                bklen = rev - splen
            End If
            If tlem > 206 Then
                lines = 3
                revtask2 = StrReverse(Mid(task, bklen, 103))
                revtask2 = Trim(revtask2)
                Dim rev2 As Integer = Len(revtask2)
                splen2 = revtask2.IndexOf(" ")
                bklen2 = (rev2) - splen2 + 1
            End If
            If tlem > 309 Then
                lines = 4
                revtask3 = StrReverse(Mid(task, bklen2, 103))
                revtask3 = Trim(revtask3)
                Dim rev3 As Integer = Len(revtask3)
                splen3 = revtask3.IndexOf(" ")
                bklen3 = (rev3) - splen3 + 1
            End If
            If tlem > 412 Then
                lines = 5
                revtask4 = StrReverse(Mid(task, bklen3, 103))
                revtask4 = Trim(revtask4)
                Dim rev4 As Integer = Len(revtask4)
                splen4 = revtask4.IndexOf(" ")
                bklen4 = (rev4) - splen4 + 1
            End If
            Select Case lines
                Case 1
                    Dim test As String = dr.Item("tasknum").ToString & "]" & task
                    sb.Append("[" & dr.Item("tasknum").ToString & "]" & task & "<br>")
                Case 2
                    sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    sb.Append("  " & Mid(task, bklen, tlem) & "<br>")
                Case 3
                    sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    sb.Append("  " & Mid(task, bklen, bklen2) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2, tlem) & "<br>")
                Case 4
                    sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    sb.Append("  " & Mid(task, bklen, bklen2) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2, bklen3) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3, tlem) & "<br>")
                Case 5
                    sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    sb.Append("  " & Mid(task, bklen, bklen2) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2, bklen3) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3, bklen4) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem) & "<br>")
                Case Else
                    sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    sb.Append("  " & Mid(task, bklen, bklen2) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2, bklen3) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3, bklen4) & "<br>")
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem) & "<br>")
            End Select

            'If tlem > 103 Then
            'sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
            'Dim test1 As String = Mid(task, bklen, bklen2)
            'sw.WriteLine("  " & Mid(task, bklen, bklen2))
            'Dim test2 As String = Mid(task, bklen + bklen2, bklen3)
            'sw.WriteLine("  " & Mid(task, bklen + bklen2, bklen3))
            'Dim test3 As String = Mid(task, bklen + bklen2 + bklen3, bklen4)
            'sw.WriteLine("  " & Mid(task, bklen + bklen2 + bklen3, bklen4))
            'Dim test4 As String = Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem)
            'sw.WriteLine("  " & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem))
            'Else
            'sw.WriteLine("[" & dr.Item("tasknum").ToString & "]" & task) ' 
            'End If
            sb.Append("   OK(___) " & dr.Item("fm1").ToString & "<br>")


        End While
        dr.Close()
        sb.Append("                                                                                                          ")
        sb.Append("**********************************************************************************************************")
        sb.Append("                                                                                                          ")

        'Parts
        sb.Append(tmod.getxlbl("xlb17" , "PGEMSHTM.vb") & "<br>")
        Dim pflg As String = 0
        If pdm <> "" Then
            sql = "usp_getWIPartsTotalPdM '" & eqid & "', '1'"
        Else
            sql = "usp_getWIPartsTotalPdM '" & eqid & "', '0'"
        End If

        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                pflg = "1"
                sb.Append(dr.Item("func").ToString & " - [" & dr.Item("tasknum").ToString & "] " & dr.Item("parts").ToString & "<br>")
            End If
        End While
        dr.Close()
        If pflg <> "1" Then
            sb.Append(tmod.getxlbl("xlb18" , "PGEMSHTM.vb") & "<br>")
        End If
        'Tools'
        sb.Append(tmod.getxlbl("xlb19" , "PGEMSHTM.vb") & "<br>")
        Dim tflg As String = 0
        If pdm <> "" Then
            sql = "usp_getWIToolsTotalPdM '" & eqid & "', '1'"
        Else
            sql = "usp_getWIToolsTotalPdM '" & eqid & "', '0'"
        End If
        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                tflg = "1"
                sb.Append(dr.Item("func").ToString & " - [" & dr.Item("tasknum").ToString & "] " & dr.Item("parts").ToString & "<br>")
            End If
        End While
        dr.Close()
        If tflg <> "1" Then
            sb.Append(tmod.getxlbl("xlb20" , "PGEMSHTM.vb") & "<br>")
        End If
        'Lubes'
        sb.Append(tmod.getxlbl("xlb21" , "PGEMSHTM.vb") & "<br>")
        Dim lflg As String = 0
        If pdm <> "" Then
            sql = "usp_getWILubesTotalPdM '" & eqid & "', '1'"
        Else
            sql = "usp_getWILubesTotalPdM '" & eqid & "', '0'"
        End If
        dr = pms.GetRdrData(sql)
        While dr.Read
            If Len(dr.Item("parts").ToString) > 0 Then
                lflg = "1"
                sb.Append(dr.Item("func").ToString & " - [" & dr.Item("tasknum").ToString & "] " & dr.Item("parts").ToString & "<br>")
            End If
        End While
        dr.Close()
        If lflg <> "1" Then
            sb.Append(tmod.getxlbl("xlb22" , "PGEMSHTM.vb") & "<br>")
        End If
        pms.Dispose()
        Dim longstring As String = sb.ToString
        Return longstring
    End Function
End Class
