

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMALLTPM

    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pma As New Utilities
    Dim filt As String
    Dim pmidarr As ArrayList = New ArrayList
    Public Function GetReport(ByVal filt As String) As String
        sql = "select distinct pm.pmid from tpm pm " _
             + "left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.wonum = pm.wonum " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
        + "left join pmtracktpm pt on pt.pmid = pm.pmid " & filt
        'left join equipment e on e.eqid = pm.eqid left join workorder w on w.wonum = pm.wonum " & filt
        pma.Open()
        dr = pma.GetRdrData(sql)
        While dr.Read
            pmidarr.Add(dr.Item("pmid"))
        End While
        dr.Close()

        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        Dim func, funcchk, task, tasknum, subtask, pm As String
        Dim sb As New System.Text.StringBuilder

        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")

        Dim i As Integer
        For i = 0 To pmidarr.Count - 1
            flag = 0
            subcnt = 0
            subcnth = 0



            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"" style=""page-break-after:always;"">")
            sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""610""></td></tr>")
            sql = "usp_getpmmantpm '" & pmidarr(i) & "'"
            dr = pma.GetRdrData(sql)
            Dim headhold As String = "0"
            While dr.Read
                If headhold = "0" Then
                    headhold = "1"
                    pm = dr.Item("pm").ToString
                    sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""3"">" & pm & "</td></tr>")
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">" & tmod.getxlbl("xlb69", "PMALLTPM.vb") & " " & dr.Item("ttime") & " " & tmod.getxlbl("xlb70", "PMALLTPM.vb") & "</td></tr>")
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">" & tmod.getxlbl("xlb71", "PMALLTPM.vb") & " " & dr.Item("dtime") & " " & tmod.getxlbl("xlb72", "PMALLTPM.vb") & "</td></tr>")
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""3"">" & tmod.getxlbl("xlb73", "PMALLTPM.vb") & " " & dr.Item("nextdate") & "</td></tr>")
                    sb.Append("<tr><td class=""bigbold"" colspan=""3""><hr></td></tr>")
                End If

                func = dr.Item("func").ToString
                If funcchk <> func Then 'flag = 0 Then
                    If flag = 0 Then
                        flag = 1
                    Else
                        'sb.Append("<tr><td>&nbsp;</td></tr>")
                    End If

                    funcchk = func
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""3""><u>" & tmod.getxlbl("xlb74", "PMALLTPM.vb") & " " & func & "</u></td>")
                    'sb.Append("<td class=""plainlabel"" colspan=""2"">" & func & "</td></tr>")
                    'sb.Append("<tr><td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl363" , "PMALLTPM.vb") & "</td>")
                    'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl364" , "PMALLTPM.vb") & "</td>")
                    'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl365" , "PMALLTPM.vb") & "</td></tr>")
                End If
                task = dr.Item("task").ToString
                tasknum = dr.Item("tasknum").ToString
                subtask = dr.Item("subtask").ToString
                subcnt = dr.Item("subcnt").ToString


                If subtask = "0" Then
                    'sb.Append("<tr height=""20""><td></td><td class=""label"">" & subcnt & "</td></tr>")
                    sb.Append("<tr><td class=""plainlabel"">[" & tasknum & "]</td>")
                    If Len(dr.Item("task").ToString) > 0 Then
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb75", "PMALLTPM.vb") & "</b> :" & task & "</td></tr>")
                    Else
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>" & tmod.getxlbl("xlb76", "PMALLTPM.vb") & "</b> " & tmod.getxlbl("xlb77", "PMALLTPM.vb") & "</td></tr>")
                    End If
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr height=""20""><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Check: OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
                    If dr.Item("lube") <> "none" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getxlbl("xlb78", "PMALLTPM.vb") & " " & dr.Item("lube").ToString & "</td></tr>")
                    End If
                    If dr.Item("tool") <> "none" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getxlbl("xlb79", "PMALLTPM.vb") & " " & dr.Item("tool").ToString & "</td></tr>")
                    End If
                    If dr.Item("part") <> "none" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getxlbl("xlb80", "PMALLTPM.vb") & " " & dr.Item("part").ToString & "</td></tr>")
                    End If
                    If dr.Item("meas") <> "none" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getxlbl("xlb81", "PMALLTPM.vb") & " " & dr.Item("meas").ToString & "</td></tr>")
                    End If
                    If subcnt = 0 Then
                        sb.Append("<tr><td>&nbsp;</td></tr>")
                    Else
                        If subcnt <> 0 Then

                            sb.Append("<tr height=""20""><td></td><td class=""label"" colspan=""2""><u>" & tmod.getxlbl("xlb82", "PMALLTPM.vb") & "</u></td></tr>")
                        End If

                    End If

                Else
                    'sb.Append("<tr><td>" & subcnt & "</td></tr>")
                    sb.Append("<tr height=""20""><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                    If Len(dr.Item("subt").ToString) > 0 Then
                        sb.Append("<td class=""plainlabel""><b>" & tmod.getxlbl("xlb83", "PMALLTPM.vb") & "</b>: " & dr.Item("subt").ToString & "</td></tr>")
                    Else
                        sb.Append("<td class=""plainlabel""><b>" & tmod.getxlbl("xlb84", "PMALLTPM.vb") & "</b>: " & tmod.getxlbl("xlb85", "PMALLTPM.vb") & "</td></tr>")
                    End If
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td class=""plainlabel"">Check: OK(___)</td></tr>")
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                End If


            End While
            dr.Close()
            sb.Append("</table>")

        Next
        sb.Append("</body></html>")
        pma.Dispose()
        Dim longstring As String = sb.ToString
        Return longstring
    End Function
End Class
