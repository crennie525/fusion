Public Class aspxovlibvals
Public Function getAddCompovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddasset"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Asset Class"
				Case "fre"
					ret = "Add/Edit Asset Class"
				Case "ger"
					ret = "Add/Edit Asset Class"
				Case "ita"
					ret = "Add/Edit Asset Class"
				Case "spa"
					ret = "Add/Edit Asset Class"
			End Select
		Case "btncget"
			Select Case lang
				Case "eng"
					ret = "Lookup Asset Class"
				Case "fre"
					ret = "Lookup Asset Class"
				Case "ger"
					ret = "Lookup Asset Class"
				Case "ita"
					ret = "Lookup Asset Class"
				Case "spa"
					ret = "Lookup Asset Class"
			End Select
		End Select
	Return ret
End Function
Public Function getaddcomp2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddasset"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Asset Class"
				Case "fre"
					ret = "Add/Edit Asset Class"
				Case "ger"
					ret = "Add/Edit Asset Class"
				Case "ita"
					ret = "Add/Edit Asset Class"
				Case "spa"
					ret = "Add/Edit Asset Class"
			End Select
		Case "btncget"
			Select Case lang
				Case "eng"
					ret = "Lookup Asset Class"
				Case "fre"
					ret = "Lookup Asset Class"
				Case "ger"
					ret = "Lookup Asset Class"
				Case "ita"
					ret = "Lookup Asset Class"
				Case "spa"
					ret = "Lookup Asset Class"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Task Steps for this Component"
				Case "fre"
					ret = "Ajouter/ modifier les �tapes de t�che pour ce composant"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schritte f�r dieses Bauteil"
				Case "ita"
					ret = "Aggiungi/modifica passaggi attivit� per questo componente"
				Case "spa"
					ret = "Agregar/Editar Pasos de Tarea para este Componente "
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid247"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		End Select
	Return ret
End Function
Public Function getAddFailovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddtosite"
			Select Case lang
				Case "eng"
					ret = "Choose Failure Modes for this Plant Site "
				Case "fre"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ger"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ita"
					ret = "Choose Failure Modes for this Plant Site "
				Case "spa"
					ret = "Seleccionar los Modos de Falla para este Sitio de la Planta"
			End Select
		End Select
	Return ret
End Function
Public Function getAppSetComTabovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddtosite"
			Select Case lang
				Case "eng"
					ret = "Choose Failure Modes for this Plant Site "
				Case "fre"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ger"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ita"
					ret = "Choose Failure Modes for this Plant Site "
				Case "spa"
					ret = "Seleccionar los Modos de Falla para este Sitio de la Planta"
			End Select
		End Select
	Return ret
End Function
Public Function getAppSetpmApprovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgadd"
			Select Case lang
				Case "eng"
					ret = "Add a New Revision from scratch"
				Case "fre"
					ret = "Ajouter une Nouvelle R�vision du fichier de travail"
				Case "ger"
					ret = "Hinzuf�gen einer neuen �nderung von vorne"
				Case "ita"
					ret = "Aggiungi Nuova Revisione da scratch"
				Case "spa"
					ret = "A�adir una nueva revisi�n desde cero"
			End Select
		Case "imgcheck"
			Select Case lang
				Case "eng"
					ret = "Approve or update the Approval Date for this Revision"
				Case "fre"
					ret = "Approuver ou Mettre � Jour la Date d`Approbation de cette R�vision"
				Case "ger"
					ret = "Zulassung oder Aktualisierung des Zulassungsdatums f�r diese �nderung"
				Case "ita"
					ret = "Approva o aggiorna la Data di Approvazione per questa Revisione "
				Case "spa"
					ret = "Aprobar o actualizar la fecha de aprobacion para esta revisi�n"
			End Select
		Case "imgcomp"
			Select Case lang
				Case "eng"
					ret = "Make this the Current Revision"
				Case "fre"
					ret = "Faire de cette R�vision la R�vision Actuelle"
				Case "ger"
					ret = "Dies zur aktuellen Revision machen"
				Case "ita"
					ret = "Effettuare questa Revisione Corrente"
				Case "spa"
					ret = "Hacer de �sta la revisi�n actual"
			End Select
		Case "imgcopy"
			Select Case lang
				Case "eng"
					ret = "Add a New Revision using this Revision as a Template"
				Case "fre"
					ret = "Ajouter une Nouvelle R�vision en utilisant cette R�vision comme Mod�le"
				Case "ger"
					ret = "Hinzuf�gen einer neuen �nderung, wobei diese �nderung als Vorlage benutzt wird"
				Case "ita"
					ret = "Aggiungi Nuova Revisione usando questa Revisione come Template"
				Case "spa"
					ret = "A�adir una nueva revisi�n usando esta revisi�n como plantilla"
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete this Revision"
				Case "fre"
					ret = "Effacer cette R�vision"
				Case "ger"
					ret = "L�schen dieser Revision"
				Case "ita"
					ret = "Cancella questa Revisione"
				Case "spa"
					ret = "Borrar esta revisi�n"
			End Select
		End Select
	Return ret
End Function
Public Function getAppSetTaskTabSTovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid1"
			Select Case lang
				Case "eng"
					ret = "Choose Skills for this Plant Site "
				Case "fre"
					ret = "Choisir les Comp�tences pour ce Site d`�tablissement"
				Case "ger"
					ret = "Kompetenzen f�r dieses Werksgrundst�ck ausw�hlen"
				Case "ita"
					ret = "Scegliere Skill per questa localit� Impianto "
				Case "spa"
					ret = "Elija habilidades para este sitio de planta"
			End Select
		End Select
	Return ret
End Function
Public Function getarchrationale2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid25"
			Select Case lang
				Case "eng"
					ret = "Print PMO Task Rationale Report"
				Case "fre"
					ret = "Imprimer le rapport de justification de t�che de PMO"
				Case "ger"
					ret = "Drucke PMO-Aufgaben-Begr�ndungs-Bericht"
				Case "ita"
					ret = "Stampa report rationale attivit� PMO"
				Case "spa"
					ret = "Imprimir Reporte de Razonamientos de Tareas PMO"
			End Select
		End Select
	Return ret
End Function
Public Function getarchrationaletpm2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid35"
			Select Case lang
				Case "eng"
					ret = "Print PMO Task Rationale Report"
				Case "fre"
					ret = "Imprimer le rapport de justification de t�che de PMO"
				Case "ger"
					ret = "Drucke PMO-Aufgaben-Begr�ndungs-Bericht"
				Case "ita"
					ret = "Stampa report rationale attivit� PMO"
				Case "spa"
					ret = "Imprimir Reporte de Razonamientos de Tareas PMO"
			End Select
		End Select
	Return ret
End Function
Public Function getcatalogovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Save selected details"
				Case "fre"
					ret = "Sauvegarder les d�tails s�lectionn�s"
				Case "ger"
					ret = "Gew�hlte Details speichern"
				Case "ita"
					ret = "Salva dettagli selezionati"
				Case "spa"
					ret = "Guardar los detalles seleccionados "
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Select Stock Type"
				Case "fre"
					ret = "S�lectionner le type de stock"
				Case "ger"
					ret = "Bestands-Typ w�hlen"
				Case "ita"
					ret = "Seleziona tipo scorta"
				Case "spa"
					ret = "Seleccionar Tipo de Inventario "
			End Select
		Case "Img7"
			Select Case lang
				Case "eng"
					ret = "Select an Order Unit for this Item"
				Case "fre"
					ret = "S�lectionner une unit� de commande pour cet article"
				Case "ger"
					ret = "F�r diesen Artikel eine Auftragseinheit w�hlen"
				Case "ita"
					ret = "Seleziona un`unit� ordine per questo articolo"
				Case "spa"
					ret = "Seleccionar una Unidad del Orden para este Art�culo  "
			End Select
		Case "imgadd"
			Select Case lang
				Case "eng"
					ret = "Add a New Item with the Item Code, Description, and Store Room values you selected"
				Case "fre"
					ret = "Ajouter un nouvel article avec les valeurs de code d`article, de description et de local de service que vous avez s�lectionn�es"
				Case "ger"
					ret = "Einen neuen Artikel mit der Artikel-Nr., der Beschreibung und den gew�hlten Lagerraumwerten hinzuf�gen"
				Case "ita"
					ret = "Aggiungi un nuovo articolo con il codice articolo, la descrizione e i valori del deposito selezionati"
				Case "spa"
					ret = "Agregar un Nuevo Art�culo  con el C�digo del Art�culo , Descripci�n, y Valores de Almac�n que usted Seleccion� "
			End Select
		Case "imghaz"
			Select Case lang
				Case "eng"
					ret = "Select a Hazard for this Item"
				Case "fre"
					ret = "S�lectionner un danger pour cet article"
				Case "ger"
					ret = "F�r diesen Artikel eine Gefahr w�hlen"
				Case "ita"
					ret = "Seleziona un pericolo per questo articolo"
				Case "spa"
					ret = "Seleccionar un Riesgo para este Art�culo  "
			End Select
		Case "imglot"
			Select Case lang
				Case "eng"
					ret = "Select a Lot Type for this Item"
				Case "fre"
					ret = "S�lectionner un type de lot pour cet article"
				Case "ger"
					ret = "F�r diesen Artikel einen Los-Typ w�hlen"
				Case "ita"
					ret = "Seleziona un tipo lotto per questo articolo"
				Case "spa"
					ret = "Seleccionar Tipo de Lote para este Art�culo  "
			End Select
		Case "imgsrch"
			Select Case lang
				Case "eng"
					ret = "Look up Inventory Items"
				Case "fre"
					ret = "Rechercher les articles d`inventaire"
				Case "ger"
					ret = "Inventarartikel-Suche"
				Case "ita"
					ret = "Ricerca articoli inventario"
				Case "spa"
					ret = "Buscar Art�culos en Inventario"
			End Select
		Case "ovid266"
			Select Case lang
				Case "eng"
					ret = "Clear Page"
				Case "fre"
					ret = "Effacer la page"
				Case "ger"
					ret = "Seite leeren"
				Case "ita"
					ret = "Eliminare pagina"
				Case "spa"
					ret = "Limpiar P�gina"
			End Select
		Case "ovid267"
			Select Case lang
				Case "eng"
					ret = "Select a Default Store Room for this Item"
				Case "fre"
					ret = "S�lectionner un local de service par d�faut pour cet article"
				Case "ger"
					ret = "F�r diesen Artikel einen Standard-Lagerraum w�hlen"
				Case "ita"
					ret = "Seleziona un deposito default per questo articolo"
				Case "spa"
					ret = "Seleccionar un Almac�n Predefinido para este Art�culo  "
			End Select
		End Select
	Return ret
End Function
Public Function getcommontasksovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ibaddnew"
			Select Case lang
				Case "eng"
					ret = "Add To Common Tasks"
				Case "fre"
					ret = "Ajouter aux t�ches communes"
				Case "ger"
					ret = "F�ge gew�hnlichen Aufgaben hinzu"
				Case "ita"
					ret = "Aggiungi ad Attivit� comuni"
				Case "spa"
					ret = "Agregar A Tareas Comunes"
			End Select
		Case "imgsavecommon"
			Select Case lang
				Case "eng"
					ret = "Save Changes to This Common Task"
				Case "fre"
					ret = "Sauvegarder les modifications pour cette t�che commune"
				Case "ger"
					ret = "�nderung an dieser gew�hnlichen Aufgabe speichern"
				Case "ita"
					ret = "Salva le modifiche a questa attivit� comune "
				Case "spa"
					ret = "Guardar los Cambios a Esta Tarea Com�n"
			End Select
		Case "ir1"
			Select Case lang
				Case "eng"
					ret = "Add Task and Return This Task to my PM Task Screen"
				Case "fre"
					ret = "Ajouter une t�che et renvoyer cette t�che � mon �cran de t�che de MP"
				Case "ger"
					ret = "F�ge Aufgabe hinzu und bringe diese Aufgabe auf meinen PM-Aufgaben-Bildschirm zur�ck"
				Case "ita"
					ret = "Aggiungi Attivit� e riporta questa attivit� nella finestra attivit� PM"
				Case "spa"
					ret = "Agregar Tarea y Regresar Esta Tarea a mi Pantalla de Tareas PM"
			End Select
		Case "ir2"
			Select Case lang
				Case "eng"
					ret = "Save Changes and Return This Task to my PM Task Screen"
				Case "fre"
					ret = "Sauvegarder les modifications et renvoyer cette t�che � mon �cran de t�che de MP"
				Case "ger"
					ret = "�nderungen speichern und diese Aufgabe auf meinen PM-Aufgaben-Bildschirm zur�ckbringen"
				Case "ita"
					ret = "Salva le modifiche e riportare questa attivit� alla finestra mie Attivit� di PM"
				Case "spa"
					ret = "Guardar los Cambios y Regresar Esta Tarea a mi Pantalla de Tareas PM"
			End Select
		Case "ir3"
			Select Case lang
				Case "eng"
					ret = "Return This Task to my PM Task Screen"
				Case "fre"
					ret = "Renvoyer cette t�che � mon �cran de t�che de MP"
				Case "ger"
					ret = "Bringe diese Aufgabe zu meinen PM-Aufgaben-Schirm zur�ck"
				Case "ita"
					ret = "Riportare questa attivit� alla finestra mie Attivit� di PM"
				Case "spa"
					ret = "Regresar Esta Tarea a mi Pantalla de Tareas PM"
			End Select
		Case "ovid2"
			Select Case lang
				Case "eng"
					ret = "Reset Screen"
				Case "fre"
					ret = "Reset Screen"
				Case "ger"
					ret = "Reset Screen"
				Case "ita"
					ret = "Reset Screen"
				Case "spa"
					ret = "Reset Screen"
			End Select
		Case "ovid3"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Name"
				Case "fre"
					ret = "Lookup Component Name"
				Case "ger"
					ret = "Lookup Component Name"
				Case "ita"
					ret = "Lookup Component Name"
				Case "spa"
					ret = "Lookup Component Name"
			End Select
		Case "ovid4"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Description"
				Case "fre"
					ret = "Rechercher la description de composant"
				Case "ger"
					ret = "Bauteilbeschreibungs-Suche"
				Case "ita"
					ret = "Ricerca descrizione componente"
				Case "spa"
					ret = "Buscar Descripci�n de Componente "
			End Select
		Case "ovid5"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Designation"
				Case "fre"
					ret = "Lookup Component Designation"
				Case "ger"
					ret = "Lookup Component Designation"
				Case "ita"
					ret = "Lookup Component Designation"
				Case "spa"
					ret = "Lookup Component Designation"
			End Select
		Case "ovid6"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Special Identifier"
				Case "fre"
					ret = "Rechercher l`identifiant sp�cial de composant"
				Case "ger"
					ret = "Suche nach Besonderem Bauteil-Bezeichner"
				Case "ita"
					ret = "Ricerca identificatore speciale componente"
				Case "spa"
					ret = "Buscar Identificador Especial de Componente"
			End Select
		Case "ovid7"
			Select Case lang
				Case "eng"
					ret = "Lookup Function Name"
				Case "fre"
					ret = "Rechercher le nom de fonction"
				Case "ger"
					ret = "Funktionsnamen-Suche"
				Case "ita"
					ret = "Ricerca nome funzione"
				Case "spa"
					ret = "Buscar Nombre de Funci�n"
			End Select
		Case "ovid8"
			Select Case lang
				Case "eng"
					ret = "Lookup Function Special Identifier"
				Case "fre"
					ret = "Rechercher l`identifiant sp�cial de fonction"
				Case "ger"
					ret = "Suche nach Besonderem Funktions-Bezeichner"
				Case "ita"
					ret = "Ricerca identificatore speciale funzione"
				Case "spa"
					ret = "Buscar Identificador Especial de Funci�n"
			End Select
		End Select
	Return ret
End Function
Public Function getcommontaskstpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ibaddnew"
			Select Case lang
				Case "eng"
					ret = "Add To Common Tasks"
				Case "fre"
					ret = "Ajouter aux t�ches communes"
				Case "ger"
					ret = "F�ge gew�hnlichen Aufgaben hinzu"
				Case "ita"
					ret = "Aggiungi ad Attivit� comuni"
				Case "spa"
					ret = "Agregar A Tareas Comunes"
			End Select
		Case "imgsavecommon"
			Select Case lang
				Case "eng"
					ret = "Save Changes to This Common Task"
				Case "fre"
					ret = "Sauvegarder les modifications pour cette t�che commune"
				Case "ger"
					ret = "�nderung an dieser gew�hnlichen Aufgabe speichern"
				Case "ita"
					ret = "Salva le modifiche a questa attivit� comune "
				Case "spa"
					ret = "Guardar los Cambios a Esta Tarea Com�n"
			End Select
		Case "ir1"
			Select Case lang
				Case "eng"
					ret = "Add Task and Return This Task to my TPM Task Screen"
				Case "fre"
					ret = "Ajouter une t�che et renvoyer cette t�che � mon �cran de t�che de MPT"
				Case "ger"
					ret = "F�ge Aufgabe hinzu und bringe diese Aufgabe auf meinen TPM-Aufgaben-Bildschirm zur�ck"
				Case "ita"
					ret = "Aggiungi attivit� e riporta questa attivit� alla finestra Mia attivit� TPM"
				Case "spa"
					ret = "Agregar Tarea y Regresar Esta Tarea a mi  Pantalla de Tareas TPM "
			End Select
		Case "ir2"
			Select Case lang
				Case "eng"
					ret = "Save Changes and Return This Task to my TPM Task Screen"
				Case "fre"
					ret = "Sauvegarder les modifications et renvoyer cette t�che � mon �cran de t�che de MPT"
				Case "ger"
					ret = "�nderungen speichern und diese Aufgabe auf meinen TPM-Aufgaben-Bildschirm zur�ckbringen"
				Case "ita"
					ret = "Salva modifiche e riporta questa attivit� alla finestra mia attivit� TPM"
				Case "spa"
					ret = "Guardar Cambios y Regresar Esta Tarea a mi Pantalla de Tareas TPM"
			End Select
		Case "ir3"
			Select Case lang
				Case "eng"
					ret = "Return This Task to my TPM Task Screen"
				Case "fre"
					ret = "Renvoyer cette t�che � mon �cran de t�che de MPT"
				Case "ger"
					ret = "Bringe diese Aufgabe auf meinen TPM-Aufgaben-Schirm zur�ck"
				Case "ita"
					ret = "Riporta questa attivit� alla finestra mia attivit� TPM"
				Case "spa"
					ret = "Regresar Esta Tarea a mi Pantalla de Tareas TPM"
			End Select
		Case "ovid173"
			Select Case lang
				Case "eng"
					ret = "Reset Screen"
				Case "fre"
					ret = "Reset Screen"
				Case "ger"
					ret = "Reset Screen"
				Case "ita"
					ret = "Reset Screen"
				Case "spa"
					ret = "Reset Screen"
			End Select
		Case "ovid174"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Name"
				Case "fre"
					ret = "Lookup Component Name"
				Case "ger"
					ret = "Lookup Component Name"
				Case "ita"
					ret = "Lookup Component Name"
				Case "spa"
					ret = "Lookup Component Name"
			End Select
		Case "ovid175"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Description"
				Case "fre"
					ret = "Rechercher la description de composant"
				Case "ger"
					ret = "Bauteilbeschreibungs-Suche"
				Case "ita"
					ret = "Ricerca descrizione componente"
				Case "spa"
					ret = "Buscar Descripci�n de Componente "
			End Select
		Case "ovid176"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Designation"
				Case "fre"
					ret = "Lookup Component Designation"
				Case "ger"
					ret = "Lookup Component Designation"
				Case "ita"
					ret = "Lookup Component Designation"
				Case "spa"
					ret = "Lookup Component Designation"
			End Select
		Case "ovid177"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Special Identifier"
				Case "fre"
					ret = "Rechercher l`identifiant sp�cial de composant"
				Case "ger"
					ret = "Suche nach Besonderem Bauteil-Bezeichner"
				Case "ita"
					ret = "Ricerca identificatore speciale componente"
				Case "spa"
					ret = "Buscar Identificador Especial de Componente"
			End Select
		Case "ovid178"
			Select Case lang
				Case "eng"
					ret = "Lookup Function Name"
				Case "fre"
					ret = "Rechercher le nom de fonction"
				Case "ger"
					ret = "Funktionsnamen-Suche"
				Case "ita"
					ret = "Ricerca nome funzione"
				Case "spa"
					ret = "Buscar Nombre de Funci�n"
			End Select
		Case "ovid179"
			Select Case lang
				Case "eng"
					ret = "Lookup Function Special Identifier"
				Case "fre"
					ret = "Rechercher l`identifiant sp�cial de fonction"
				Case "ger"
					ret = "Suche nach Besonderem Funktions-Bezeichner"
				Case "ita"
					ret = "Ricerca identificatore speciale funzione"
				Case "spa"
					ret = "Buscar Identificador Especial de Funci�n"
			End Select
		End Select
	Return ret
End Function
Public Function getCompDivovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddasset"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Asset Class"
				Case "fre"
					ret = "Add/Edit Asset Class"
				Case "ger"
					ret = "Add/Edit Asset Class"
				Case "ita"
					ret = "Add/Edit Asset Class"
				Case "spa"
					ret = "Add/Edit Asset Class"
			End Select
		Case "btnaddtosite"
			Select Case lang
				Case "eng"
					ret = "Choose Failure Modes for this Plant Site "
				Case "fre"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ger"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ita"
					ret = "Choose Failure Modes for this Plant Site "
				Case "spa"
					ret = "Seleccionar los Modos de Falla para este Sitio de la Planta"
			End Select
		Case "btncget"
			Select Case lang
				Case "eng"
					ret = "Lookup Asset Class"
				Case "fre"
					ret = "Lookup Asset Class"
				Case "ger"
					ret = "Lookup Asset Class"
				Case "ita"
					ret = "Lookup Asset Class"
				Case "spa"
					ret = "Lookup Asset Class"
			End Select
		Case "btncrem"
			Select Case lang
				Case "eng"
					ret = "Remove Asset Class from Component (Will Not Remove Failure Modes)"
				Case "fre"
					ret = "Remove Asset Class from Component (Will Not Remove Failure Modes)"
				Case "ger"
					ret = "Remove Asset Class from Component (Will Not Remove Failure Modes)"
				Case "ita"
					ret = "Remove Asset Class from Component (Will Not Remove Failure Modes)"
				Case "spa"
					ret = "Remove Asset Class from Component (Will Not Remove Failure Modes)"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Copy Components for Selected Function"
				Case "fre"
					ret = "Copier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "Kopiere Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Copia i componenti per la funzione scelta"
				Case "spa"
					ret = "Copiar Componentes para la Funci�n Seleccionada"
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid248"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		Case "ovid249"
			Select Case lang
				Case "eng"
					ret = "Go to the Centralized PM Library"
				Case "fre"
					ret = "Go to the Centralized PM Library"
				Case "ger"
					ret = "Go to the Centralized PM Library"
				Case "ita"
					ret = "Go to the Centralized PM Library"
				Case "spa"
					ret = "Ir a la Biblioteca de PM Centralizada"
			End Select
		End Select
	Return ret
End Function
Public Function getCompDivGridovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgcopy"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Records to Copy"
				Case "fre"
					ret = "Lookup Component Records to Copy"
				Case "ger"
					ret = "Lookup Component Records to Copy"
				Case "ita"
					ret = "Lookup Component Records to Copy"
				Case "spa"
					ret = "Localizar Registros de Componentes para Copiar"
			End Select
		End Select
	Return ret
End Function
Public Function getcomplibovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Add a Component to the Library"
				Case "fre"
					ret = "Add a Component to the Library"
				Case "ger"
					ret = "Add a Component to the Library"
				Case "ita"
					ret = "Add a Component to the Library"
				Case "spa"
					ret = "Add a Component to the Library"
			End Select
		End Select
	Return ret
End Function
Public Function getcomplibaddovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddasset"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Asset Class"
				Case "fre"
					ret = "Add/Edit Asset Class"
				Case "ger"
					ret = "Add/Edit Asset Class"
				Case "ita"
					ret = "Add/Edit Asset Class"
				Case "spa"
					ret = "Add/Edit Asset Class"
			End Select
		Case "btncget"
			Select Case lang
				Case "eng"
					ret = "Lookup Asset Class"
				Case "fre"
					ret = "Lookup Asset Class"
				Case "ger"
					ret = "Lookup Asset Class"
				Case "ita"
					ret = "Lookup Asset Class"
				Case "spa"
					ret = "Lookup Asset Class"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Task Steps for this Component"
				Case "fre"
					ret = "Ajouter/ modifier les �tapes de t�che pour ce composant"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schritte f�r dieses Bauteil"
				Case "ita"
					ret = "Aggiungi/modifica passaggi attivit� per questo componente"
				Case "spa"
					ret = "Agregar/Editar Pasos de Tarea para este Componente "
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid210"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		End Select
	Return ret
End Function
Public Function getcomplibeditovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddasset"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Asset Class"
				Case "fre"
					ret = "Add/Edit Asset Class"
				Case "ger"
					ret = "Add/Edit Asset Class"
				Case "ita"
					ret = "Add/Edit Asset Class"
				Case "spa"
					ret = "Add/Edit Asset Class"
			End Select
		Case "btncget"
			Select Case lang
				Case "eng"
					ret = "Lookup Asset Class"
				Case "fre"
					ret = "Lookup Asset Class"
				Case "ger"
					ret = "Lookup Asset Class"
				Case "ita"
					ret = "Lookup Asset Class"
				Case "spa"
					ret = "Lookup Asset Class"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Task Steps for this Component"
				Case "fre"
					ret = "Ajouter/ modifier les �tapes de t�che pour ce composant"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schritte f�r dieses Bauteil"
				Case "ita"
					ret = "Aggiungi/modifica passaggi attivit� per questo componente"
				Case "spa"
					ret = "Agregar/Editar Pasos de Tarea para este Componente "
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid211"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		End Select
	Return ret
End Function
Public Function getcomplibtaskeadittpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "img1"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "ovid212"
			Select Case lang
				Case "eng"
					ret = "Against what is the TPM intended to protect?"
				Case "fre"
					ret = "Against what is the TPM intended to protect?"
				Case "ger"
					ret = "Against what is the TPM intended to protect?"
				Case "ita"
					ret = "Against what is the TPM intended to protect?"
				Case "spa"
					ret = "Against what is the TPM intended to protect?"
			End Select
		Case "ovid213"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid214"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid215"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid216"
			Select Case lang
				Case "eng"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "fre"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "ger"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "ita"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "spa"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
			End Select
		Case "ovid217"
			Select Case lang
				Case "eng"
					ret = "Selects All Days for Selected Shift"
				Case "fre"
					ret = "S�lectionne tous les jours pour le poste s�lectionn�"
				Case "ger"
					ret = "W�hlt alle Tage f�r gew�hlte Schicht "
				Case "ita"
					ret = "Seleziona tutti i giorni per il turno selezionato"
				Case "spa"
					ret = "Selecciona Todos los D�as para el Turno Seleccionado "
			End Select
		Case "ovid218"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid219"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getcomplibtasks2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "img1"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "ovid220"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid221"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid222"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid223"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid224"
			Select Case lang
				Case "eng"
					ret = "Choose Skills for this Plant Site "
				Case "fre"
					ret = "Choisir les Comp�tences pour ce Site d`�tablissement"
				Case "ger"
					ret = "Kompetenzen f�r dieses Werksgrundst�ck ausw�hlen"
				Case "ita"
					ret = "Scegliere Skill per questa localit� Impianto "
				Case "spa"
					ret = "Elija habilidades para este sitio de planta"
			End Select
		Case "ovid225"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid226"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getcomplibtasks3ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "img1"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "ovid227"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid228"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid229"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid230"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid231"
			Select Case lang
				Case "eng"
					ret = "Choose Skills for this Plant Site "
				Case "fre"
					ret = "Choisir les Comp�tences pour ce Site d`�tablissement"
				Case "ger"
					ret = "Kompetenzen f�r dieses Werksgrundst�ck ausw�hlen"
				Case "ita"
					ret = "Scegliere Skill per questa localit� Impianto "
				Case "spa"
					ret = "Elija habilidades para este sitio de planta"
			End Select
		Case "ovid232"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid233"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getcomplibtaskseditovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "img1"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "ovid234"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid235"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid236"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid237"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid238"
			Select Case lang
				Case "eng"
					ret = "Choose Skills for this Plant Site "
				Case "fre"
					ret = "Choisir les Comp�tences pour ce Site d`�tablissement"
				Case "ger"
					ret = "Kompetenzen f�r dieses Werksgrundst�ck ausw�hlen"
				Case "ita"
					ret = "Scegliere Skill per questa localit� Impianto "
				Case "spa"
					ret = "Elija habilidades para este sitio de planta"
			End Select
		Case "ovid239"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid240"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getcomplibtasksedit2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "ovid241"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid242"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid243"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid244"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid245"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid246"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getdragndropnoovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Save Current Tasks to Optimize"
				Case "fre"
					ret = "Sauvegarder les t�ches actuelles � optimiser"
				Case "ger"
					ret = "Aktuelle Aufgaben zur Optimierung speichern"
				Case "ita"
					ret = "Salva le correnti attivit� per l`ottimizzazione"
				Case "spa"
					ret = "Guardar Tareas Actuales para Optimizar"
			End Select
		Case "imgbulk"
			Select Case lang
				Case "eng"
					ret = "Choose a Bulk Loaded Procedure to Optimize"
				Case "fre"
					ret = "S�lectionner une proc�dure de chargement en vrac � optimiser"
				Case "ger"
					ret = "W�hle ein 08/15-Verfahren zur Optimierung"
				Case "ita"
					ret = "Scegliere un procedimento di caricamento di massa per ottimizzare"
				Case "spa"
					ret = "***Escoja un Procedimiento Cargado A granel para Perfeccionar"
			End Select
		Case "imgbulkg"
			Select Case lang
				Case "eng"
					ret = "Choose a Bulk Loaded Procedure from the Document Library to Optimize"
				Case "fre"
					ret = "S�lectionner une proc�dure de chargement en vrac � partir de la biblioth�que de documents � optimiser"
				Case "ger"
					ret = "W�hle ein 08/15-Verfahren aus der Dokumenten-Bibliothek zur Optimierung"
				Case "ita"
					ret = "Scegliere un procedimento di caricamento di massa dalla galleria documenti per ottimizzare"
				Case "spa"
					ret = "***Escoja un Procedimiento Cargado en  de la Biblioteca del Documento para Optimizar"
			End Select
		Case "imgsavopt"
			Select Case lang
				Case "eng"
					ret = "Save Current Tasks to Optimize"
				Case "fre"
					ret = "Sauvegarder les t�ches actuelles � optimiser"
				Case "ger"
					ret = "Aktuelle Aufgaben zur Optimierung speichern"
				Case "ita"
					ret = "Salva le correnti attivit� per l`ottimizzazione"
				Case "spa"
					ret = "Guardar Tareas Actuales para Optimizar"
			End Select
		Case "ovid49"
			Select Case lang
				Case "eng"
					ret = "Select an Established Drag and Drop PM Cycle to Edit"
				Case "fre"
					ret = "Select an Established Drag and Drop PM Cycle to Edit"
				Case "ger"
					ret = "Select an Established Drag and Drop PM Cycle to Edit"
				Case "ita"
					ret = "Select an Established Drag and Drop PM Cycle to Edit"
				Case "spa"
					ret = "Select an Established Drag and Drop PM Cycle to Edit"
			End Select
		Case "ovid50"
			Select Case lang
				Case "eng"
					ret = "View\Edit Original Total Values"
				Case "fre"
					ret = "Afficher/ modifier les valeurs originales totales"
				Case "ger"
					ret = "Urspr�ngliche Gesamtwerte anzeigen/bearbeiten"
				Case "ita"
					ret = "Visualizza/modifica i valori totali originari"
				Case "spa"
					ret = "Ver\Editar los Valores Totales Originales"
			End Select
		Case "ovid51"
			Select Case lang
				Case "eng"
					ret = "Transfer PM Data to Drag-n-Drop Screen"
				Case "fre"
					ret = "Transf�rer les donn�es de MP vers l`�cran Glisser-d�poser"
				Case "ger"
					ret = "PM-Daten zum Drag-n-Drop-Bildschirm �bertragen"
				Case "ita"
					ret = "Trasferire i dati PM nella finestra Drag-n-Drop"
				Case "spa"
					ret = "Transferir los Datos de PM a la Pantalla de Arrastrar y Soltar"
			End Select
		Case "ovid52"
			Select Case lang
				Case "eng"
					ret = "Add Procedures to Optimize"
				Case "fre"
					ret = "Add Procedures to Optimize"
				Case "ger"
					ret = "Add Procedures to Optimize"
				Case "ita"
					ret = "Add Procedures to Optimize"
				Case "spa"
					ret = "Agregar Procedimientos para Optimizar"
			End Select
		End Select
	Return ret
End Function
Public Function getEQBotovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddasset"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Asset Class"
				Case "fre"
					ret = "Add/Edit Asset Class"
				Case "ger"
					ret = "Add/Edit Asset Class"
				Case "ita"
					ret = "Add/Edit Asset Class"
				Case "spa"
					ret = "Add/Edit Asset Class"
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid250"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this Asset"
				Case "fre"
					ret = "Add\Edit Images for this Asset"
				Case "ger"
					ret = "Add\Edit Images for this Asset"
				Case "ita"
					ret = "Add\Edit Images for this Asset"
				Case "spa"
					ret = "Add\Edit Images for this Asset"
			End Select
		Case "ovid251"
			Select Case lang
				Case "eng"
					ret = "Copy an Equipment Record"
				Case "fre"
					ret = "Copier un enregistrement d`�quipement"
				Case "ger"
					ret = "Kopiere eine Ausr�stungs-Eintragung"
				Case "ita"
					ret = "Copia una voce Apparecchiatura"
				Case "spa"
					ret = "Copiar un Registro de Equipo"
			End Select
		End Select
	Return ret
End Function
Public Function geteqcopy2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid252"
			Select Case lang
				Case "eng"
					ret = "Use Max Search (Bypasses Grid Selection)"
				Case "fre"
					ret = "Use Max Search (Bypasses Grid Selection)"
				Case "ger"
					ret = "Use Max Search (Bypasses Grid Selection)"
				Case "ita"
					ret = "Use Max Search (Bypasses Grid Selection)"
				Case "spa"
					ret = "Use Max Search (Bypasses Grid Selection)"
			End Select
		End Select
	Return ret
End Function
Public Function getEQCopyMiniovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddasset"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Asset Class"
				Case "fre"
					ret = "Add/Edit Asset Class"
				Case "ger"
					ret = "Add/Edit Asset Class"
				Case "ita"
					ret = "Add/Edit Asset Class"
				Case "spa"
					ret = "Add/Edit Asset Class"
			End Select
		End Select
	Return ret
End Function
Public Function getEQCopyMinitpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddasset"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Asset Class"
				Case "fre"
					ret = "Add/Edit Asset Class"
				Case "ger"
					ret = "Add/Edit Asset Class"
				Case "ita"
					ret = "Add/Edit Asset Class"
				Case "spa"
					ret = "Add/Edit Asset Class"
			End Select
		End Select
	Return ret
End Function
Public Function getEQMainovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Start Over"
				Case "fre"
					ret = "Recommencer"
				Case "ger"
					ret = "Noch einmal starten"
				Case "ita"
					ret = "Ricomincia"
				Case "spa"
					ret = "Start Over"
			End Select
		Case "ovid253"
			Select Case lang
				Case "eng"
					ret = "Use Max Search"
				Case "fre"
					ret = "Use Max Search"
				Case "ger"
					ret = "Use Max Search"
				Case "ita"
					ret = "Use Max Search"
				Case "spa"
					ret = "Use Max Search"
			End Select
		Case "ovid254"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
Public Function getEqSelectovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddtosite"
			Select Case lang
				Case "eng"
					ret = "Choose Failure Modes for this Plant Site "
				Case "fre"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ger"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ita"
					ret = "Choose Failure Modes for this Plant Site "
				Case "spa"
					ret = "Seleccionar los Modos de Falla para este Sitio de la Planta"
			End Select
		End Select
	Return ret
End Function
Public Function geteqtab2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid280"
			Select Case lang
				Case "eng"
					ret = "Use Max Search"
				Case "fre"
					ret = "Use Max Search"
				Case "ger"
					ret = "Use Max Search"
				Case "ita"
					ret = "Use Max Search"
				Case "spa"
					ret = "Use Max Search"
			End Select
		End Select
	Return ret
End Function
Public Function getFormatTestovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddcomp"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Components for Selected Function"
				Case "fre"
					ret = "Ajouter/ modifier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Aggiungi/modifica i componenti per la funzione selezionata"
				Case "spa"
					ret = "Agregar/Editar Componentes para la Funci�n Seleccionada"
			End Select
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "ggrid"
			Select Case lang
				Case "eng"
					ret = "Review Changes in Grid View"
				Case "fre"
					ret = "Review Changes in Grid View"
				Case "ger"
					ret = "Review Changes in Grid View"
				Case "ita"
					ret = "Review Changes in Grid View"
				Case "spa"
					ret = "Revisar Cambios en Vista de la Parrilla"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Copy Components for Selected Function"
				Case "fre"
					ret = "Copier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "Kopiere Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Copia i componenti per la funzione scelta"
				Case "spa"
					ret = "Copiar Componentes para la Funci�n Seleccionada"
			End Select
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - All Tasks"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - toutes les t�ches"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � alle Aufgaben"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Tutte le attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Todas las Tareas"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - This Task"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - cette t�che"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � diese Aufgabe"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Questa attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Esta Tarea"
			End Select
		Case "imgrat"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Rationale for Task Changes"
				Case "fre"
					ret = "Ajouter/�diter un Expos� Raisonn� pour les Changements de T�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Begr�ndung f�r Aufgaben�nderungen"
				Case "ita"
					ret = "Aggiungi/modifica rationale per modifiche attivit�"
				Case "spa"
					ret = "Agregar/Editar Razonamiento para Cambios en Tarea "
			End Select
		Case "ovid115"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid116"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid117"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid118"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid119"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid120"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "ovid121"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Notes for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les remarques pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Anmerkungen f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le note per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Notas para esta Tarea"
			End Select
		Case "ovid122"
			Select Case lang
				Case "eng"
					ret = "View Reports"
				Case "fre"
					ret = "View Reports"
				Case "ger"
					ret = "View Reports"
				Case "ita"
					ret = "View Reports"
				Case "spa"
					ret = "Ver Reportes"
			End Select
		Case "ovid123"
			Select Case lang
				Case "eng"
					ret = "Go to the Centralized PM Library"
				Case "fre"
					ret = "Go to the Centralized PM Library"
				Case "ger"
					ret = "Go to the Centralized PM Library"
				Case "ita"
					ret = "Go to the Centralized PM Library"
				Case "spa"
					ret = "Ir a la Biblioteca de PM Centralizada"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getFuncDivovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgcopy"
			Select Case lang
				Case "eng"
					ret = "Copy a Function Record"
				Case "fre"
					ret = "Copier un enregistrement de fonction"
				Case "ger"
					ret = "Kopiere eine Funktions-Eintragung"
				Case "ita"
					ret = "Copia una voce Funzione"
				Case "spa"
					ret = "Copiar un Registro de Funci�n"
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid255"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		Case "ovid256"
			Select Case lang
				Case "eng"
					ret = "Go to the Centralized PM Library"
				Case "fre"
					ret = "Go to the Centralized PM Library"
				Case "ger"
					ret = "Go to the Centralized PM Library"
				Case "ita"
					ret = "Go to the Centralized PM Library"
				Case "spa"
					ret = "Ir a la Biblioteca de PM Centralizada"
			End Select
		End Select
	Return ret
End Function
Public Function getFuncDivGridovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgcopy"
			Select Case lang
				Case "eng"
					ret = "Lookup Function Records to Copy"
				Case "fre"
					ret = "Lookup Function Records to Copy"
				Case "ger"
					ret = "Lookup Function Records to Copy"
				Case "ita"
					ret = "Lookup Function Records to Copy"
				Case "spa"
					ret = "Localizar Registros de Funciones para Copiar"
			End Select
		Case "ovid257"
			Select Case lang
				Case "eng"
					ret = "Add a New Function Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement de fonction"
				Case "ger"
					ret = "F�ge eine neue Funktions-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record  Nuova funzione"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Funci�n"
			End Select
		End Select
	Return ret
End Function
Public Function getgtaskfailovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "ovid9"
			Select Case lang
				Case "eng"
					ret = "Return"
				Case "fre"
					ret = "Retour"
				Case "ger"
					ret = "zur�ck"
				Case "ita"
					ret = "indietro"
				Case "spa"
					ret = "regresar"
			End Select
		End Select
	Return ret
End Function
Public Function getGTasksFunctpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddnewcomp"
			Select Case lang
				Case "eng"
					ret = "Add a New Component to the Selected Function"
				Case "fre"
					ret = "Add a New Component to the Selected Function"
				Case "ger"
					ret = "Add a New Component to the Selected Function"
				Case "ita"
					ret = "Add a New Component to the Selected Function"
				Case "spa"
					ret = "Agregar un Nuevo Componente a la Funci�n Seleccionada"
			End Select
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Selected Component"
				Case "fre"
					ret = "Add a New Failure Mode to the Selected Component"
				Case "ger"
					ret = "Add a New Failure Mode to the Selected Component"
				Case "ita"
					ret = "Add a New Failure Mode to the Selected Component"
				Case "spa"
					ret = "Agregar un Nuevo Modo de Falla al Componente Seleccionado"
			End Select
		End Select
	Return ret
End Function
Public Function getissueinvovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Add Item Qty to Issue List"
				Case "fre"
					ret = "Add Item Qty to Issue List"
				Case "ger"
					ret = "Add Item Qty to Issue List"
				Case "ita"
					ret = "Add Item Qty to Issue List"
				Case "spa"
					ret = "Add Item Qty to Issue List"
			End Select
		Case "imgbuild"
			Select Case lang
				Case "eng"
					ret = "Build Pick List from selected Work Order#"
				Case "fre"
					ret = "�tablir une liste de s�lection � partir du n� de bon de commande s�lectionn�"
				Case "ger"
					ret = "Erstelle Materialbereitstellliste aus gew�hltem Arbeitsauftrag Nr."
				Case "ita"
					ret = "Creare una picklist dall`ordine di lavoro# selezionato"
				Case "spa"
					ret = "Elaborar Lista de Partes Orden de Trabajo Seleccionada # "
			End Select
		Case "imgcomp"
			Select Case lang
				Case "eng"
					ret = "Complete this Issue or Transfer"
				Case "fre"
					ret = "Terminer ce probl�me ou transf�rer"
				Case "ger"
					ret = "Schlie�e diese Ausgabe oder diese �bertragung ab"
				Case "ita"
					ret = "Completa questa emissione o trasferimento"
				Case "spa"
					ret = "Completar esta Emisi�n o Traslado"
			End Select
		Case "imgprint"
			Select Case lang
				Case "eng"
					ret = "Print Current Pick List"
				Case "fre"
					ret = "Imprimer la liste de s�lection actuelle"
				Case "ger"
					ret = "Drucke aktuelle Entnahmeliste"
				Case "ita"
					ret = "Stampa picklist corrente"
				Case "spa"
					ret = "Imprimir Lista Actual de Partes "
			End Select
		End Select
	Return ret
End Function
Public Function getJobPlanovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "cball"
			Select Case lang
				Case "eng"
					ret = "Apply this Skill to All Tasks"
				Case "fre"
					ret = "Apply this Skill to All Tasks"
				Case "ger"
					ret = "Apply this Skill to All Tasks"
				Case "ita"
					ret = "Apply this Skill to All Tasks"
				Case "spa"
					ret = "Apply this Skill to All Tasks"
			End Select
		Case "imgadd"
			Select Case lang
				Case "eng"
					ret = "Create a Job Plan with the Name and Decription You Have Entered"
				Case "fre"
					ret = "Create a Job Plan with the Name and Decription You Have Entered"
				Case "ger"
					ret = "Create a Job Plan with the Name and Decription You Have Entered"
				Case "ita"
					ret = "Create a Job Plan with the Name and Decription You Have Entered"
				Case "spa"
					ret = "Create a Job Plan with the Name and Decription You Have Entered"
			End Select
		Case "imgloc"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		Case "imgsav"
			Select Case lang
				Case "eng"
					ret = "Save Changes to Job Plan # and Description"
				Case "fre"
					ret = "Save Changes to Job Plan # and Description"
				Case "ger"
					ret = "Save Changes to Job Plan # and Description"
				Case "ita"
					ret = "Save Changes to Job Plan # and Description"
				Case "spa"
					ret = "Save Changes to Job Plan # and Description"
			End Select
		Case "imgsav2"
			Select Case lang
				Case "eng"
					ret = "Save Changes to Job Plan Details"
				Case "fre"
					ret = "Save Changes to Job Plan Details"
				Case "ger"
					ret = "Save Changes to Job Plan Details"
				Case "ita"
					ret = "Save Changes to Job Plan Details"
				Case "spa"
					ret = "Save Changes to Job Plan Details"
			End Select
		Case "ovid157"
			Select Case lang
				Case "eng"
					ret = "View Job Plan Record List"
				Case "fre"
					ret = "View Job Plan Record List"
				Case "ger"
					ret = "View Job Plan Record List"
				Case "ita"
					ret = "View Job Plan Record List"
				Case "spa"
					ret = "View Job Plan Record List"
			End Select
		Case "ovid158"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Create a New Job Plan"
				Case "fre"
					ret = "Clear Screen and Create a New Job Plan"
				Case "ger"
					ret = "Clear Screen and Create a New Job Plan"
				Case "ita"
					ret = "Clear Screen and Create a New Job Plan"
				Case "spa"
					ret = "Clear Screen and Create a New Job Plan"
			End Select
		Case "ovid159"
			Select Case lang
				Case "eng"
					ret = "Use to select a Common Component without Location and Asset Details"
				Case "fre"
					ret = "Use to select a Common Component without Location and Asset Details"
				Case "ger"
					ret = "Use to select a Common Component without Location and Asset Details"
				Case "ita"
					ret = "Use to select a Common Component without Location and Asset Details"
				Case "spa"
					ret = "Use to select a Common Component without Location and Asset Details"
			End Select
		End Select
	Return ret
End Function
Public Function getJobPlanListovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgadd"
			Select Case lang
				Case "eng"
					ret = "Add a Job Plan"
				Case "fre"
					ret = "Ajouter un plan de travail"
				Case "ger"
					ret = "Einen Arbeitsplan hinzuf�gen"
				Case "ita"
					ret = "Aggiungi un job plan"
				Case "spa"
					ret = "Agregar un Plan del Trabajo "
			End Select
		Case "ovid160"
			Select Case lang
				Case "eng"
					ret = "Search Routes"
				Case "fre"
					ret = "Rechercher les chemins"
				Case "ger"
					ret = "Strecken suchen"
				Case "ita"
					ret = "Cerca instradamenti"
				Case "spa"
					ret = "Buscar Rutas "
			End Select
		Case "ovid161"
			Select Case lang
				Case "eng"
					ret = "Sort Ascending"
				Case "fre"
					ret = "Sort Ascending"
				Case "ger"
					ret = "Sort Ascending"
				Case "ita"
					ret = "Sort Ascending"
				Case "spa"
					ret = "Sort Ascending"
			End Select
		Case "ovid162"
			Select Case lang
				Case "eng"
					ret = "Sort Descending"
				Case "fre"
					ret = "Sort Descending"
				Case "ger"
					ret = "Sort Descending"
				Case "ita"
					ret = "Sort Descending"
				Case "spa"
					ret = "Sort Descending"
			End Select
		End Select
	Return ret
End Function
Public Function getJobPlanRefReqovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgadd"
			Select Case lang
				Case "eng"
					ret = "Add a Job Plan Request"
				Case "fre"
					ret = "Add a Job Plan Request"
				Case "ger"
					ret = "Add a Job Plan Request"
				Case "ita"
					ret = "Add a Job Plan Request"
				Case "spa"
					ret = "Add a Job Plan Request"
			End Select
		Case "ovid163"
			Select Case lang
				Case "eng"
					ret = "Search Requests"
				Case "fre"
					ret = "Search Requests"
				Case "ger"
					ret = "Search Requests"
				Case "ita"
					ret = "Search Requests"
				Case "spa"
					ret = "Search Requests"
			End Select
		Case "ovid164"
			Select Case lang
				Case "eng"
					ret = "Sort Ascending"
				Case "fre"
					ret = "Sort Ascending"
				Case "ger"
					ret = "Sort Ascending"
				Case "ita"
					ret = "Sort Ascending"
				Case "spa"
					ret = "Sort Ascending"
			End Select
		Case "ovid165"
			Select Case lang
				Case "eng"
					ret = "Sort Descending"
				Case "fre"
					ret = "Sort Descending"
				Case "ger"
					ret = "Sort Descending"
				Case "ita"
					ret = "Sort Descending"
				Case "spa"
					ret = "Sort Descending"
			End Select
		End Select
	Return ret
End Function
Public Function getjpfaillistovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddtosite"
			Select Case lang
				Case "eng"
					ret = "Choose Failure Modes for this Plant Site "
				Case "fre"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ger"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ita"
					ret = "Choose Failure Modes for this Plant Site "
				Case "spa"
					ret = "Seleccionar los Modos de Falla para este Sitio de la Planta"
			End Select
		End Select
	Return ret
End Function
Public Function getlaborassignovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Filter"
				Case "fre"
					ret = "Clear Filter"
				Case "ger"
					ret = "Clear Filter"
				Case "ita"
					ret = "Clear Filter"
				Case "spa"
					ret = "Clear Filter"
			End Select
		Case "ovid289"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
Public Function getlubegrideditovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid278"
			Select Case lang
				Case "eng"
					ret = "Add Lubricant for this Task"
				Case "fre"
					ret = "Add Lubricant for this Task"
				Case "ger"
					ret = "Add Lubricant for this Task"
				Case "ita"
					ret = "Add Lubricant for this Task"
				Case "spa"
					ret = "Add Lubricant for this Task"
			End Select
		End Select
	Return ret
End Function
Public Function getlubeoutovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid279"
			Select Case lang
				Case "eng"
					ret = "Add Lubricant for this Task"
				Case "fre"
					ret = "Add Lubricant for this Task"
				Case "ger"
					ret = "Add Lubricant for this Task"
				Case "ita"
					ret = "Add Lubricant for this Task"
				Case "spa"
					ret = "Add Lubricant for this Task"
			End Select
		End Select
	Return ret
End Function
Public Function getlubeouteditovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Add History"
				Case "fre"
					ret = "Add History"
				Case "ger"
					ret = "Add History"
				Case "ita"
					ret = "Add History"
				Case "spa"
					ret = "Add History"
			End Select
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "imgee"
			Select Case lang
				Case "eng"
					ret = "Edit History"
				Case "fre"
					ret = "Edit History"
				Case "ger"
					ret = "Edit History"
				Case "ita"
					ret = "Edit History"
				Case "spa"
					ret = "Edit History"
			End Select
		Case "imgei"
			Select Case lang
				Case "eng"
					ret = "Edit History"
				Case "fre"
					ret = "Edit History"
				Case "ger"
					ret = "Edit History"
				Case "ita"
					ret = "Edit History"
				Case "spa"
					ret = "Edit History"
			End Select
		Case "imgmag"
			Select Case lang
				Case "eng"
					ret = "View All Equipment Records"
				Case "fre"
					ret = "Afficher tous les enregistrements d`�quipement"
				Case "ger"
					ret = "Alle Ausr�stungs-Eintr�ge anzeigen"
				Case "ita"
					ret = "Visualizza tutti i record apparecchiatura"
				Case "spa"
					ret = "Ver Todos los Registros de Equipo  "
			End Select
		End Select
	Return ret
End Function
Public Function getlubeoutroutesovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print Route Report"
				Case "fre"
					ret = "Print Route Report"
				Case "ger"
					ret = "Print Route Report"
				Case "ita"
					ret = "Print Route Report"
				Case "spa"
					ret = "Print Route Report"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Add or Edit Measurements for this Route"
				Case "fre"
					ret = "Add or Edit Measurements for this Route"
				Case "ger"
					ret = "Add or Edit Measurements for this Route"
				Case "ita"
					ret = "Add or Edit Measurements for this Route"
				Case "spa"
					ret = "Add or Edit Measurements for this Route"
			End Select
		Case "imgei"
			Select Case lang
				Case "eng"
					ret = "Enter Route Measurement Values"
				Case "fre"
					ret = "Enter Route Measurement Values"
				Case "ger"
					ret = "Enter Route Measurement Values"
				Case "ita"
					ret = "Enter Route Measurement Values"
				Case "spa"
					ret = "Enter Route Measurement Values"
			End Select
		End Select
	Return ret
End Function
Public Function getmaxsearchovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid258"
			Select Case lang
				Case "eng"
					ret = "Save Selection and Return"
				Case "fre"
					ret = "Save Selection and Return"
				Case "ger"
					ret = "Save Selection and Return"
				Case "ita"
					ret = "Save Selection and Return"
				Case "spa"
					ret = "Save Selection and Return"
			End Select
		End Select
	Return ret
End Function
Public Function getmeasouteditovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ihg"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ihga"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Add History"
				Case "fre"
					ret = "Add History"
				Case "ger"
					ret = "Add History"
				Case "ita"
					ret = "Add History"
				Case "spa"
					ret = "Add History"
			End Select
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "imgee"
			Select Case lang
				Case "eng"
					ret = "Edit History"
				Case "fre"
					ret = "Edit History"
				Case "ger"
					ret = "Edit History"
				Case "ita"
					ret = "Edit History"
				Case "spa"
					ret = "Edit History"
			End Select
		Case "imgei"
			Select Case lang
				Case "eng"
					ret = "Edit History"
				Case "fre"
					ret = "Edit History"
				Case "ger"
					ret = "Edit History"
				Case "ita"
					ret = "Edit History"
				Case "spa"
					ret = "Edit History"
			End Select
		Case "imgmag"
			Select Case lang
				Case "eng"
					ret = "View All Equipment Records"
				Case "fre"
					ret = "Afficher tous les enregistrements d`�quipement"
				Case "ger"
					ret = "Alle Ausr�stungs-Eintr�ge anzeigen"
				Case "ita"
					ret = "Visualizza tutti i record apparecchiatura"
				Case "spa"
					ret = "Ver Todos los Registros de Equipo  "
			End Select
		End Select
	Return ret
End Function
Public Function getmeasoutflyovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ihg"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ihga"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Add History"
				Case "fre"
					ret = "Add History"
				Case "ger"
					ret = "Add History"
				Case "ita"
					ret = "Add History"
				Case "spa"
					ret = "Add History"
			End Select
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "imgee"
			Select Case lang
				Case "eng"
					ret = "Edit History"
				Case "fre"
					ret = "Edit History"
				Case "ger"
					ret = "Edit History"
				Case "ita"
					ret = "Edit History"
				Case "spa"
					ret = "Edit History"
			End Select
		Case "imgei"
			Select Case lang
				Case "eng"
					ret = "Edit History"
				Case "fre"
					ret = "Edit History"
				Case "ger"
					ret = "Edit History"
				Case "ita"
					ret = "Edit History"
				Case "spa"
					ret = "Edit History"
			End Select
		Case "imgmag"
			Select Case lang
				Case "eng"
					ret = "View All Equipment Records"
				Case "fre"
					ret = "Afficher tous les enregistrements d`�quipement"
				Case "ger"
					ret = "Alle Ausr�stungs-Eintr�ge anzeigen"
				Case "ita"
					ret = "Visualizza tutti i record apparecchiatura"
				Case "spa"
					ret = "Ver Todos los Registros de Equipo  "
			End Select
		Case "iwo"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "iwoa"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ovid283"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid284"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function getmeasoutroutesovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print Route Report"
				Case "fre"
					ret = "Print Route Report"
				Case "ger"
					ret = "Print Route Report"
				Case "ita"
					ret = "Print Route Report"
				Case "spa"
					ret = "Print Route Report"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Add or Edit Measurements for this Route"
				Case "fre"
					ret = "Add or Edit Measurements for this Route"
				Case "ger"
					ret = "Add or Edit Measurements for this Route"
				Case "ita"
					ret = "Add or Edit Measurements for this Route"
				Case "spa"
					ret = "Add or Edit Measurements for this Route"
			End Select
		Case "imgei"
			Select Case lang
				Case "eng"
					ret = "Enter Route Measurement Values"
				Case "fre"
					ret = "Enter Route Measurement Values"
				Case "ger"
					ret = "Enter Route Measurement Values"
				Case "ita"
					ret = "Enter Route Measurement Values"
				Case "spa"
					ret = "Enter Route Measurement Values"
			End Select
		End Select
	Return ret
End Function
Public Function getmeasoutroutesminiovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print Route Report"
				Case "fre"
					ret = "Print Route Report"
				Case "ger"
					ret = "Print Route Report"
				Case "ita"
					ret = "Print Route Report"
				Case "spa"
					ret = "Print Route Report"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Add or Edit Measurements for this Route"
				Case "fre"
					ret = "Add or Edit Measurements for this Route"
				Case "ger"
					ret = "Add or Edit Measurements for this Route"
				Case "ita"
					ret = "Add or Edit Measurements for this Route"
				Case "spa"
					ret = "Add or Edit Measurements for this Route"
			End Select
		Case "imgei"
			Select Case lang
				Case "eng"
					ret = "Enter Route Measurement Values"
				Case "fre"
					ret = "Enter Route Measurement Values"
				Case "ger"
					ret = "Enter Route Measurement Values"
				Case "ita"
					ret = "Enter Route Measurement Values"
				Case "spa"
					ret = "Enter Route Measurement Values"
			End Select
		End Select
	Return ret
End Function
Public Function getmeasoutwoovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid285"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid286"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function getmmenuoptsovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid287"
			Select Case lang
				Case "eng"
					ret = "Save Changes and Exit"
				Case "fre"
					ret = "Save Changes and Exit"
				Case "ger"
					ret = "Save Changes and Exit"
				Case "ita"
					ret = "Save Changes and Exit"
				Case "spa"
					ret = "Save Changes and Exit"
			End Select
		End Select
	Return ret
End Function
Public Function getNCBotovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid259"
			Select Case lang
				Case "eng"
					ret = "Copy this Asset Record"
				Case "fre"
					ret = "Copy this Asset Record"
				Case "ger"
					ret = "Copy this Asset Record"
				Case "ita"
					ret = "Copy this Asset Record"
				Case "spa"
					ret = "Copy this Asset Record"
			End Select
		End Select
	Return ret
End Function
Public Function getNewMainMenu2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgnav"
			Select Case lang
				Case "eng"
					ret = "Show/Hide Location Information"
				Case "fre"
					ret = "Show/Hide Location Information"
				Case "ger"
					ret = "Show/Hide Location Information"
				Case "ita"
					ret = "Show/Hide Location Information"
				Case "spa"
					ret = "Show/Hide Location Information"
			End Select
		End Select
	Return ret
End Function
Public Function getPFIntovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "c1"
			Select Case lang
				Case "eng"
					ret = "Calculate"
				Case "fre"
					ret = "Calculate"
				Case "ger"
					ret = "Calculate"
				Case "ita"
					ret = "Calculate"
				Case "spa"
					ret = "Calcular"
			End Select
		Case "c2"
			Select Case lang
				Case "eng"
					ret = "Calculate Interval"
				Case "fre"
					ret = "Calculate Interval"
				Case "ger"
					ret = "Calculate Interval"
				Case "ita"
					ret = "Calculate Interval"
				Case "spa"
					ret = "Calcular Intervalo"
			End Select
		End Select
	Return ret
End Function
Public Function getPFInt_freovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "c1"
			Select Case lang
				Case "eng"
					ret = "Calculez"
				Case "fre"
					ret = "Calculez"
				Case "ger"
					ret = "Calculez"
				Case "ita"
					ret = "Calculez"
				Case "spa"
					ret = "Calculez"
			End Select
		Case "c2"
			Select Case lang
				Case "eng"
					ret = "Calculez L`Intervalle"
				Case "fre"
					ret = "Calculez L`Intervalle"
				Case "ger"
					ret = "Calculez L`Intervalle"
				Case "ita"
					ret = "Calculez L`Intervalle"
				Case "spa"
					ret = "Calculez L`Intervalle"
			End Select
		End Select
	Return ret
End Function
Public Function getPFInt_gerovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "c1"
			Select Case lang
				Case "eng"
					ret = "Errechnen Sie"
				Case "fre"
					ret = "Errechnen Sie"
				Case "ger"
					ret = "Errechnen Sie"
				Case "ita"
					ret = "Errechnen Sie"
				Case "spa"
					ret = "Errechnen Sie"
			End Select
		Case "c2"
			Select Case lang
				Case "eng"
					ret = "Errechnen Sie Abstand"
				Case "fre"
					ret = "Errechnen Sie Abstand"
				Case "ger"
					ret = "Errechnen Sie Abstand"
				Case "ita"
					ret = "Errechnen Sie Abstand"
				Case "spa"
					ret = "Errechnen Sie Abstand"
			End Select
		End Select
	Return ret
End Function
Public Function getPFInt_itaovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "c1"
			Select Case lang
				Case "eng"
					ret = "Calcoli"
				Case "fre"
					ret = "Calcoli"
				Case "ger"
					ret = "Calcoli"
				Case "ita"
					ret = "Calcoli"
				Case "spa"
					ret = "Calcoli"
			End Select
		Case "c2"
			Select Case lang
				Case "eng"
					ret = "Calcoli L`Intervallo"
				Case "fre"
					ret = "Calcoli L`Intervallo"
				Case "ger"
					ret = "Calcoli L`Intervallo"
				Case "ita"
					ret = "Calcoli L`Intervallo"
				Case "spa"
					ret = "Calcoli L`Intervallo"
			End Select
		End Select
	Return ret
End Function
Public Function getPFInt_spaovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "c1"
			Select Case lang
				Case "eng"
					ret = "Calcular"
				Case "fre"
					ret = "Calcular"
				Case "ger"
					ret = "Calcular"
				Case "ita"
					ret = "Calcular"
				Case "spa"
					ret = "Calcular"
			End Select
		Case "c2"
			Select Case lang
				Case "eng"
					ret = "Calcular Intervalo"
				Case "fre"
					ret = "Calcular Intervalo"
				Case "ger"
					ret = "Calcular Intervalo"
				Case "ita"
					ret = "Calcular Intervalo"
				Case "spa"
					ret = "Calcular Intervalo"
			End Select
		End Select
	Return ret
End Function
Public Function getPM3OptMainovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imghide"
			Select Case lang
				Case "eng"
					ret = "Expand/Shrink Procedure to Optimize"
				Case "fre"
					ret = "D�velopper/ iconiser la proc�dure � optimiser"
				Case "ger"
					ret = "Vergr��ere/verkleinere Verfahren zur Optimierung"
				Case "ita"
					ret = "Espandi/riduci procedura per ottimizzare"
				Case "spa"
					ret = "Procedimiento para Optimizar Expandir/Reducir"
			End Select
		Case "imgnav"
			Select Case lang
				Case "eng"
					ret = "Show/Hide Asset Information"
				Case "fre"
					ret = "Afficher/ masquer les informations d`actif"
				Case "ger"
					ret = "Anlagen-Information anzeigen/ausblenden"
				Case "ita"
					ret = "Mostra/nascondi informazioni prodotto"
				Case "spa"
					ret = "Mostrar/Ocultar Informaci�n del Activo "
			End Select
		End Select
	Return ret
End Function
Public Function getPMAdjManovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "ovid65"
			Select Case lang
				Case "eng"
					ret = "Save Changes Above"
				Case "fre"
					ret = "Save Changes Above"
				Case "ger"
					ret = "Save Changes Above"
				Case "ita"
					ret = "Save Changes Above"
				Case "spa"
					ret = "Save Changes Above"
			End Select
		End Select
	Return ret
End Function
Public Function getPMAdjMantpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "ovid93"
			Select Case lang
				Case "eng"
					ret = "Save Changes Above"
				Case "fre"
					ret = "Save Changes Above"
				Case "ger"
					ret = "Save Changes Above"
				Case "ita"
					ret = "Save Changes Above"
				Case "spa"
					ret = "Save Changes Above"
			End Select
		End Select
	Return ret
End Function
Public Function getPMAltManovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsav"
			Select Case lang
				Case "eng"
					ret = "Save Date Revision Changes"
				Case "fre"
					ret = "Save Date Revision Changes"
				Case "ger"
					ret = "Save Date Revision Changes"
				Case "ita"
					ret = "Save Date Revision Changes"
				Case "spa"
					ret = "Save Date Revision Changes"
			End Select
		Case "ovid66"
			Select Case lang
				Case "eng"
					ret = "Get a New Date"
				Case "fre"
					ret = "Get a New Date"
				Case "ger"
					ret = "Get a New Date"
				Case "ita"
					ret = "Get a New Date"
				Case "spa"
					ret = "Get a New Date"
			End Select
		Case "ovid67"
			Select Case lang
				Case "eng"
					ret = "Return Without Saving Changes"
				Case "fre"
					ret = "Return Without Saving Changes"
				Case "ger"
					ret = "Return Without Saving Changes"
				Case "ita"
					ret = "Return Without Saving Changes"
				Case "spa"
					ret = "Return Without Saving Changes"
			End Select
		End Select
	Return ret
End Function
Public Function getPMAltMantpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsav"
			Select Case lang
				Case "eng"
					ret = "Save Date Revision Changes"
				Case "fre"
					ret = "Save Date Revision Changes"
				Case "ger"
					ret = "Save Date Revision Changes"
				Case "ita"
					ret = "Save Date Revision Changes"
				Case "spa"
					ret = "Save Date Revision Changes"
			End Select
		Case "ovid94"
			Select Case lang
				Case "eng"
					ret = "Get a New Date"
				Case "fre"
					ret = "Get a New Date"
				Case "ger"
					ret = "Get a New Date"
				Case "ita"
					ret = "Get a New Date"
				Case "spa"
					ret = "Get a New Date"
			End Select
		Case "ovid95"
			Select Case lang
				Case "eng"
					ret = "Return Without Saving Changes"
				Case "fre"
					ret = "Return Without Saving Changes"
				Case "ger"
					ret = "Return Without Saving Changes"
				Case "ita"
					ret = "Return Without Saving Changes"
				Case "spa"
					ret = "Return Without Saving Changes"
			End Select
		End Select
	Return ret
End Function
Public Function getpmarchgetovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Start Over"
				Case "fre"
					ret = "Effacer l`�cran et red�marrer"
				Case "ger"
					ret = "Bildschirminhalt l�schen und neu starten"
				Case "ita"
					ret = "Cancellare finestra e riavviare"
				Case "spa"
					ret = "Limpiar Pantalla y Volver a Empezar"
			End Select
		Case "ovid26"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
Public Function getpmarchgettpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Start Over"
				Case "fre"
					ret = "Effacer l`�cran et red�marrer"
				Case "ger"
					ret = "Bildschirminhalt l�schen und neu starten"
				Case "ita"
					ret = "Cancellare finestra e riavviare"
				Case "spa"
					ret = "Limpiar Pantalla y Volver a Empezar"
			End Select
		Case "ovid36"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
Public Function getpmarchtasksovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - All Tasks"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - toutes les t�ches"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � alle Aufgaben"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Tutte le attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Todas las Tareas"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - This Task"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - cette t�che"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � diese Aufgabe"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Questa attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Esta Tarea"
			End Select
		Case "imgrat"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Rationale for Task Changes"
				Case "fre"
					ret = "Ajouter/�diter un Expos� Raisonn� pour les Changements de T�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Begr�ndung f�r Aufgaben�nderungen"
				Case "ita"
					ret = "Aggiungi/modifica rationale per modifiche attivit�"
				Case "spa"
					ret = "Agregar/Editar Razonamiento para Cambios en Tarea "
			End Select
		Case "ovid27"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid28"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid29"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid30"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid31"
			Select Case lang
				Case "eng"
					ret = "Choose Equipment Spare Parts for this Task"
				Case "fre"
					ret = "S�lectionner les pi�ces de rechange de l`�quipement pour cette t�che "
				Case "ger"
					ret = "W�hle die Ausr�stungs-Ersatzteile f�r diese Aufgabe"
				Case "ita"
					ret = "Scegliere le parti di ricambio dell`apparecchiatura per questa attivit�"
				Case "spa"
					ret = "Escoja las Partes de Repuesto del Equipo para esta Tarea"
			End Select
		Case "ovid32"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid33"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "ovid34"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Notes for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les remarques pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Anmerkungen f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le note per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Notas para esta Tarea"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getpmarchtaskstpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - All Tasks"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - toutes les t�ches"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � alle Aufgaben"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Tutte le attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Todas las Tareas"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - This Task"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - cette t�che"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � diese Aufgabe"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Questa attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Esta Tarea"
			End Select
		Case "imgrat"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Rationale for Task Changes"
				Case "fre"
					ret = "Ajouter/�diter un Expos� Raisonn� pour les Changements de T�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Begr�ndung f�r Aufgaben�nderungen"
				Case "ita"
					ret = "Aggiungi/modifica rationale per modifiche attivit�"
				Case "spa"
					ret = "Agregar/Editar Razonamiento para Cambios en Tarea "
			End Select
		Case "ovid37"
			Select Case lang
				Case "eng"
					ret = "Against what is the TPM intended to protect?"
				Case "fre"
					ret = "Against what is the TPM intended to protect?"
				Case "ger"
					ret = "Against what is the TPM intended to protect?"
				Case "ita"
					ret = "Against what is the TPM intended to protect?"
				Case "spa"
					ret = "Against what is the TPM intended to protect?"
			End Select
		Case "ovid38"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid39"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid40"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid41"
			Select Case lang
				Case "eng"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "fre"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "ger"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "ita"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "spa"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
			End Select
		Case "ovid42"
			Select Case lang
				Case "eng"
					ret = "Selects All Days for Selected Shift"
				Case "fre"
					ret = "S�lectionne tous les jours pour le poste s�lectionn�"
				Case "ger"
					ret = "W�hlt alle Tage f�r gew�hlte Schicht "
				Case "ita"
					ret = "Seleziona tutti i giorni per il turno selezionato"
				Case "spa"
					ret = "Selecciona Todos los D�as para el Turno Seleccionado "
			End Select
		Case "ovid43"
			Select Case lang
				Case "eng"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "fre"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "ger"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "ita"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "spa"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
			End Select
		Case "ovid44"
			Select Case lang
				Case "eng"
					ret = "Selects All Days for Selected Shift"
				Case "fre"
					ret = "S�lectionne tous les jours pour le poste s�lectionn�"
				Case "ger"
					ret = "W�hlt alle Tage f�r gew�hlte Schicht "
				Case "ita"
					ret = "Seleziona tutti i giorni per il turno selezionato"
				Case "spa"
					ret = "Selecciona Todos los D�as para el Turno Seleccionado "
			End Select
		Case "ovid45"
			Select Case lang
				Case "eng"
					ret = "Choose Equipment Spare Parts for this Task"
				Case "fre"
					ret = "S�lectionner les pi�ces de rechange de l`�quipement pour cette t�che "
				Case "ger"
					ret = "W�hle die Ausr�stungs-Ersatzteile f�r diese Aufgabe"
				Case "ita"
					ret = "Scegliere le parti di ricambio dell`apparecchiatura per questa attivit�"
				Case "spa"
					ret = "Escoja las Partes de Repuesto del Equipo para esta Tarea"
			End Select
		Case "ovid46"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid47"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "ovid48"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Notes for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les remarques pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Anmerkungen f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le note per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Notas para esta Tarea"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getPMDivManovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnedittask"
			Select Case lang
				Case "eng"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
				Case "fre"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
				Case "ger"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
				Case "ita"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
				Case "spa"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "View PM History"
				Case "fre"
					ret = "View PM History"
				Case "ger"
					ret = "View PM History"
				Case "ita"
					ret = "View PM History"
				Case "spa"
					ret = "View PM History"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "View Failure Mode History"
				Case "fre"
					ret = "View Failure Mode History"
				Case "ger"
					ret = "View Failure Mode History"
				Case "ita"
					ret = "View Failure Mode History"
				Case "spa"
					ret = "View Failure Mode History"
			End Select
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Adjust Email Lead Time"
				Case "fre"
					ret = "Adjust Email Lead Time"
				Case "ger"
					ret = "Adjust Email Lead Time"
				Case "ita"
					ret = "Adjust Email Lead Time"
				Case "spa"
					ret = "Adjust Email Lead Time"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Print This PM Work Order w\images"
				Case "fre"
					ret = "Print This PM Work Order w\images"
				Case "ger"
					ret = "Print This PM Work Order w\images"
				Case "ita"
					ret = "Print This PM Work Order w\images"
				Case "spa"
					ret = "Print This PM Work Order w\images"
			End Select
		Case "imgcomp"
			Select Case lang
				Case "eng"
					ret = "Complete this PM"
				Case "fre"
					ret = "Complete this PM"
				Case "ger"
					ret = "Complete this PM"
				Case "ita"
					ret = "Complete this PM"
				Case "spa"
					ret = "Complete this PM"
			End Select
		Case "imgi2"
			Select Case lang
				Case "eng"
					ret = "Print This PM Work Order"
				Case "fre"
					ret = "Print This PM Work Order"
				Case "ger"
					ret = "Print This PM Work Order"
				Case "ita"
					ret = "Print This PM Work Order"
				Case "spa"
					ret = "Print This PM Work Order"
			End Select
		Case "imgmeas"
			Select Case lang
				Case "eng"
					ret = "View Measurements for this PM"
				Case "fre"
					ret = "View Measurements for this PM"
				Case "ger"
					ret = "View Measurements for this PM"
				Case "ita"
					ret = "View Measurements for this PM"
				Case "spa"
					ret = "View Measurements for this PM"
			End Select
		Case "imgsav"
			Select Case lang
				Case "eng"
					ret = "Save Changes Above"
				Case "fre"
					ret = "Save Changes Above"
				Case "ger"
					ret = "Save Changes Above"
				Case "ita"
					ret = "Save Changes Above"
				Case "spa"
					ret = "Save Changes Above"
			End Select
		Case "imgwoshed"
			Select Case lang
				Case "eng"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "fre"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "ger"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "ita"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "spa"
					ret = "Add or Edit Schedule Days for this Work Order"
			End Select
		Case "ovid68"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid69"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		Case "ovid70"
			Select Case lang
				Case "eng"
					ret = "Return to Navigation Grid"
				Case "fre"
					ret = "Return to Navigation Grid"
				Case "ger"
					ret = "Return to Navigation Grid"
				Case "ita"
					ret = "Return to Navigation Grid"
				Case "spa"
					ret = "Return to Navigation Grid"
			End Select
		Case "ovid71"
			Select Case lang
				Case "eng"
					ret = "Add Attachments"
				Case "fre"
					ret = "Add Attachments"
				Case "ger"
					ret = "Add Attachments"
				Case "ita"
					ret = "Add Attachments"
				Case "spa"
					ret = "Add Attachments"
			End Select
		Case "ovid72"
			Select Case lang
				Case "eng"
					ret = "Print Current PM"
				Case "fre"
					ret = "Print Current PM"
				Case "ger"
					ret = "Print Current PM"
				Case "ita"
					ret = "Print Current PM"
				Case "spa"
					ret = "Print Current PM"
			End Select
		End Select
	Return ret
End Function
Public Function getPMDivMantpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnedittask"
			Select Case lang
				Case "eng"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
				Case "fre"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
				Case "ger"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
				Case "ita"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
				Case "spa"
					ret = "View or Edit Pass/Fail Adjustment Levels - View Current Pass/Fail Counts"
			End Select
		Case "cbleade"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		Case "cbsupe"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "View TPM History"
				Case "fre"
					ret = "View TPM History"
				Case "ger"
					ret = "View TPM History"
				Case "ita"
					ret = "View TPM History"
				Case "spa"
					ret = "View TPM History"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "View Failure Mode History"
				Case "fre"
					ret = "View Failure Mode History"
				Case "ger"
					ret = "View Failure Mode History"
				Case "ita"
					ret = "View Failure Mode History"
				Case "spa"
					ret = "View Failure Mode History"
			End Select
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Adjust Email Lead Time"
				Case "fre"
					ret = "Adjust Email Lead Time"
				Case "ger"
					ret = "Adjust Email Lead Time"
				Case "ita"
					ret = "Adjust Email Lead Time"
				Case "spa"
					ret = "Adjust Email Lead Time"
			End Select
		Case "imgcomp"
			Select Case lang
				Case "eng"
					ret = "Complete this TPM"
				Case "fre"
					ret = "Terminer cette MPT "
				Case "ger"
					ret = "Schlie�e dieses PM ab"
				Case "ita"
					ret = "Completa questo TPM"
				Case "spa"
					ret = "Completar Este TPM "
			End Select
		Case "imgi2"
			Select Case lang
				Case "eng"
					ret = "Print This TPM Work Order"
				Case "fre"
					ret = "Print This TPM Work Order"
				Case "ger"
					ret = "Print This TPM Work Order"
				Case "ita"
					ret = "Print This TPM Work Order"
				Case "spa"
					ret = "Print This TPM Work Order"
			End Select
		Case "imgmeas"
			Select Case lang
				Case "eng"
					ret = "View Measurements for this TPM"
				Case "fre"
					ret = "View Measurements for this TPM"
				Case "ger"
					ret = "View Measurements for this TPM"
				Case "ita"
					ret = "View Measurements for this TPM"
				Case "spa"
					ret = "View Measurements for this TPM"
			End Select
		Case "imgsav"
			Select Case lang
				Case "eng"
					ret = "Save Changes Above"
				Case "fre"
					ret = "Save Changes Above"
				Case "ger"
					ret = "Save Changes Above"
				Case "ita"
					ret = "Save Changes Above"
				Case "spa"
					ret = "Save Changes Above"
			End Select
		Case "imgwoshed"
			Select Case lang
				Case "eng"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "fre"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "ger"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "ita"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "spa"
					ret = "Add or Edit Schedule Days for this Work Order"
			End Select
		Case "ovid96"
			Select Case lang
				Case "eng"
					ret = "Used to indicate Shift designation"
				Case "fre"
					ret = "Used to indicate Shift designation"
				Case "ger"
					ret = "Used to indicate Shift designation"
				Case "ita"
					ret = "Used to indicate Shift designation"
				Case "spa"
					ret = "Used to indicate Shift designation"
			End Select
		Case "ovid97"
			Select Case lang
				Case "eng"
					ret = "Return to Navigation Grid"
				Case "fre"
					ret = "Return to Navigation Grid"
				Case "ger"
					ret = "Return to Navigation Grid"
				Case "ita"
					ret = "Return to Navigation Grid"
				Case "spa"
					ret = "Return to Navigation Grid"
			End Select
		Case "ovid98"
			Select Case lang
				Case "eng"
					ret = "Add Attachments"
				Case "fre"
					ret = "Add Attachments"
				Case "ger"
					ret = "Add Attachments"
				Case "ita"
					ret = "Add Attachments"
				Case "spa"
					ret = "Add Attachments"
			End Select
		Case "ovid99"
			Select Case lang
				Case "eng"
					ret = "Print Current TPM"
				Case "fre"
					ret = "Print Current TPM"
				Case "ger"
					ret = "Print Current TPM"
				Case "ita"
					ret = "Print Current TPM"
				Case "spa"
					ret = "Print Current TPM"
			End Select
		End Select
	Return ret
End Function
Public Function getPMFailManovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "View Failure Mode History"
				Case "fre"
					ret = "View Failure Mode History"
				Case "ger"
					ret = "View Failure Mode History"
				Case "ita"
					ret = "View Failure Mode History"
				Case "spa"
					ret = "View Failure Mode History"
			End Select
		Case "imgi2"
			Select Case lang
				Case "eng"
					ret = "Print This Work Order"
				Case "fre"
					ret = "Imprimer cet ordre de travail"
				Case "ger"
					ret = "Drucken dieses Arbeitsauftrags"
				Case "ita"
					ret = "Stampa questo ordine di lavoro"
				Case "spa"
					ret = "Print This Work Order"
			End Select
		Case "iwoadd"
			Select Case lang
				Case "eng"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "fre"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "ger"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "ita"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "spa"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
			End Select
		Case "ovid73"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid74"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function getPMFailMantpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "View Failure Mode History"
				Case "fre"
					ret = "View Failure Mode History"
				Case "ger"
					ret = "View Failure Mode History"
				Case "ita"
					ret = "View Failure Mode History"
				Case "spa"
					ret = "View Failure Mode History"
			End Select
		Case "imgi2"
			Select Case lang
				Case "eng"
					ret = "Print This Work Order"
				Case "fre"
					ret = "Imprimer cet ordre de travail"
				Case "ger"
					ret = "Drucken dieses Arbeitsauftrags"
				Case "ita"
					ret = "Stampa questo ordine di lavoro"
				Case "spa"
					ret = "Print This Work Order"
			End Select
		Case "iwoadd"
			Select Case lang
				Case "eng"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "fre"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "ger"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "ita"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "spa"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
			End Select
		Case "ovid100"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid101"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function getPMGetPMManovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "iadd"
			Select Case lang
				Case "eng"
					ret = "Create PM Records For This Equipment Record"
				Case "fre"
					ret = "Create PM Records For This Equipment Record"
				Case "ger"
					ret = "Create PM Records For This Equipment Record"
				Case "ita"
					ret = "Create PM Records For This Equipment Record"
				Case "spa"
					ret = "Create PM Records For This Equipment Record"
			End Select
		Case "iw"
			Select Case lang
				Case "eng"
					ret = "Revision Check Icon"
				Case "fre"
					ret = "Revision Check Icon"
				Case "ger"
					ret = "Revision Check Icon"
				Case "ita"
					ret = "Revision Check Icon"
				Case "spa"
					ret = "Revision Check Icon"
			End Select
		Case "ovid75"
			Select Case lang
				Case "eng"
					ret = "Lookup Equipment"
				Case "fre"
					ret = "Lookup Equipment"
				Case "ger"
					ret = "Lookup Equipment"
				Case "ita"
					ret = "Lookup Equipment"
				Case "spa"
					ret = "Lookup Equipment"
			End Select
		Case "ovid76"
			Select Case lang
				Case "eng"
					ret = "Jump to PM Scheduling"
				Case "fre"
					ret = "Jump to PM Scheduling"
				Case "ger"
					ret = "Jump to PM Scheduling"
				Case "ita"
					ret = "Jump to PM Scheduling"
				Case "spa"
					ret = "Jump to PM Scheduling"
			End Select
		Case "ovid77"
			Select Case lang
				Case "eng"
					ret = "Help"
				Case "fre"
					ret = "Help"
				Case "ger"
					ret = "Help"
				Case "ita"
					ret = "Help"
				Case "spa"
					ret = "Help"
			End Select
		End Select
	Return ret
End Function
Public Function getPMGetPMMantpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "iadd"
			Select Case lang
				Case "eng"
					ret = "Create PM Records For This Equipment Record"
				Case "fre"
					ret = "Create PM Records For This Equipment Record"
				Case "ger"
					ret = "Create PM Records For This Equipment Record"
				Case "ita"
					ret = "Create PM Records For This Equipment Record"
				Case "spa"
					ret = "Create PM Records For This Equipment Record"
			End Select
		Case "iw"
			Select Case lang
				Case "eng"
					ret = "Revision Check Icon"
				Case "fre"
					ret = "Revision Check Icon"
				Case "ger"
					ret = "Revision Check Icon"
				Case "ita"
					ret = "Revision Check Icon"
				Case "spa"
					ret = "Revision Check Icon"
			End Select
		Case "ovid102"
			Select Case lang
				Case "eng"
					ret = "Lookup Equipment"
				Case "fre"
					ret = "Lookup Equipment"
				Case "ger"
					ret = "Lookup Equipment"
				Case "ita"
					ret = "Lookup Equipment"
				Case "spa"
					ret = "Lookup Equipment"
			End Select
		Case "ovid103"
			Select Case lang
				Case "eng"
					ret = "Jump to PM Scheduling"
				Case "fre"
					ret = "Jump to PM Scheduling"
				Case "ger"
					ret = "Jump to PM Scheduling"
				Case "ita"
					ret = "Jump to PM Scheduling"
				Case "spa"
					ret = "Jump to PM Scheduling"
			End Select
		Case "ovid104"
			Select Case lang
				Case "eng"
					ret = "Help"
				Case "fre"
					ret = "Help"
				Case "ger"
					ret = "Help"
				Case "ita"
					ret = "Help"
				Case "spa"
					ret = "Help"
			End Select
		End Select
	Return ret
End Function
Public Function getPMGetTasksFuncovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Jump to the TPM Optimizer"
				Case "fre"
					ret = "Sauter � optimiseur de MPT"
				Case "ger"
					ret = "Springe zum TPM-Optimierer"
				Case "ita"
					ret = "Passa all`Ottimizzatore TPM"
				Case "spa"
					ret = "Salte al Optimizador de TPM"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Jump to the TPM Developer"
				Case "fre"
					ret = "Sauter � d�veloppeur de MPT"
				Case "ger"
					ret = "Springe zum TPM-Entwickler"
				Case "ita"
					ret = "Passa allo Sviluppatore TPM"
				Case "spa"
					ret = "Salte al Desarrollador de TPM"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Archive Current PM Revision and Create a New Revision"
				Case "fre"
					ret = "Archiver une r�vision actuelle de MP et cr�er une nouvelle r�vision"
				Case "ger"
					ret = "Archiviere die aktuelle PM-�berarbeitung und erstelle eine neue �berarbeitung"
				Case "ita"
					ret = "Archivia l`attuale revisione PM e crea una nuova revisione"
				Case "spa"
					ret = "Archivar la Revisi�n de PM Actual y Crear una Nueva Revisi�n"
			End Select
		Case "Img6"
			Select Case lang
				Case "eng"
					ret = "Archive Current PM Revision and Convert for Optimization"
				Case "fre"
					ret = "Archiver une r�vision actuelle de MP et la convertir pour optimisation"
				Case "ger"
					ret = "Archiviere aktuelle PM-�berarbeitung und konvertiere f�r Optimierung"
				Case "ita"
					ret = "Archivia l`attuale revisione PM e converti per l`ottimizzazione"
				Case "spa"
					ret = "Archivar la Revisi�n de PM Actual y Convertir para Optimizaci�n"
			End Select
		Case "imgaddeq"
			Select Case lang
				Case "eng"
					ret = "Add a New Equipment Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement d`�quipement"
				Case "ger"
					ret = "F�ge eine neue Ausr�stungs-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record Nuova apparecchiatura"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Equipo"
			End Select
		Case "imgaddfu"
			Select Case lang
				Case "eng"
					ret = "Add a New Function Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement de fonction"
				Case "ger"
					ret = "F�ge eine neue Funktions-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record  Nuova funzione"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Funci�n"
			End Select
		Case "imgcopyeq"
			Select Case lang
				Case "eng"
					ret = "Copy an Equipment Record"
				Case "fre"
					ret = "Copier un enregistrement d`�quipement"
				Case "ger"
					ret = "Kopiere eine Ausr�stungs-Eintragung"
				Case "ita"
					ret = "Copia una voce Apparecchiatura"
				Case "spa"
					ret = "Copiar un Registro de Equipo"
			End Select
		Case "imgcopyfu"
			Select Case lang
				Case "eng"
					ret = "Copy a Function Record"
				Case "fre"
					ret = "Copier un enregistrement de fonction"
				Case "ger"
					ret = "Kopiere eine Funktions-Eintragung"
				Case "ita"
					ret = "Copia una voce Funzione"
				Case "spa"
					ret = "Copiar un Registro de Funci�n"
			End Select
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Start Over"
				Case "fre"
					ret = "Effacer l`�cran et red�marrer"
				Case "ger"
					ret = "Bildschirminhalt l�schen und neu starten"
				Case "ita"
					ret = "Cancellare finestra e riavviare"
				Case "spa"
					ret = "Limpiar Pantalla y Volver a Empezar"
			End Select
		Case "imgtpma"
			Select Case lang
				Case "eng"
					ret = "You have potential TPM Tasks - Click this icon to Review"
				Case "fre"
					ret = "Vous avez des t�ches de MPT potentielles - Cliquer sur cette ic�ne � revoir"
				Case "ger"
					ret = "Sie haben m�gliche TPM-Aufgaben � Auf dieses Icon f�r �berarbeitung klicken"
				Case "ita"
					ret = "Sono presenti probabili attivit� TPM - Cliccare su questa icona per la revisione"
				Case "spa"
					ret = "Usted tieneTareas Potencialaes de TPM - Pulse este �cono para Revisar"
			End Select
		Case "ovid10"
			Select Case lang
				Case "eng"
					ret = "Use Max Search"
				Case "fre"
					ret = "Use Max Search"
				Case "ger"
					ret = "Use Max Search"
				Case "ita"
					ret = "Use Max Search"
				Case "spa"
					ret = "Use Max Search"
			End Select
		Case "ovid11"
			Select Case lang
				Case "eng"
					ret = "Review\Edit PM Approval Process for this Asset"
				Case "fre"
					ret = "Revoir/ modifier le processus d`approbation de MP pour cet actif"
				Case "ger"
					ret = "�berarbeite/bearbeite das PM-Zustimmungsverfahren f�r dieses Wirtschaftsgut"
				Case "ita"
					ret = "Esaminare/modificare il processo di approvazione PM per questo prodotto"
				Case "spa"
					ret = "Revisar\Editar el Proceso de Aprobaci�n PM para este Activo"
			End Select
		Case "ovid12"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
Public Function getPMGridManovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "iwa"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "iwi"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		End Select
	Return ret
End Function
Public Function getPMGridMantpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "iwa"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "iwi"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		End Select
	Return ret
End Function
Public Function getpmlibnewcodetsovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddcomp"
			Select Case lang
				Case "eng"
					ret = "Add a New Component Record"
				Case "fre"
					ret = "Add a New Component Record"
				Case "ger"
					ret = "Add a New Component Record"
				Case "ita"
					ret = "Add a New Component Record"
				Case "spa"
					ret = "Agregar un Nuevo Registro del Componente"
			End Select
		Case "btncopycomp"
			Select Case lang
				Case "eng"
					ret = "Lookup Component Records to Copy"
				Case "fre"
					ret = "Lookup Component Records to Copy"
				Case "ger"
					ret = "Lookup Component Records to Copy"
				Case "ita"
					ret = "Lookup Component Records to Copy"
				Case "spa"
					ret = "Localizar Registros de Componentes para Copiar"
			End Select
		Case "ggrid"
			Select Case lang
				Case "eng"
					ret = "Edit in Grid View"
				Case "fre"
					ret = "Edit in Grid View"
				Case "ger"
					ret = "Edit in Grid View"
				Case "ita"
					ret = "Edit in Grid View"
				Case "spa"
					ret = "Edit in Grid View"
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid260"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		End Select
	Return ret
End Function
Public Function getpmlibneweqdetsovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid261"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		End Select
	Return ret
End Function
Public Function getpmlibnewfudetsovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ggrid"
			Select Case lang
				Case "eng"
					ret = "Edit in Grid View"
				Case "fre"
					ret = "Edit in Grid View"
				Case "ger"
					ret = "Edit in Grid View"
				Case "ita"
					ret = "Edit in Grid View"
				Case "spa"
					ret = "Edit in Grid View"
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Order"
				Case "fre"
					ret = "Sauvegarder l`ordre d`image"
				Case "ger"
					ret = "Abbildungs-Reihenfolge speichern"
				Case "ita"
					ret = "Salva ordine immagine"
				Case "spa"
					ret = "Guardar el Orden de Im�genes"
			End Select
		Case "ovid262"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		Case "ovid263"
			Select Case lang
				Case "eng"
					ret = "Add a New Function Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement de fonction"
				Case "ger"
					ret = "F�ge eine neue Funktions-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record  Nuova funzione"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Funci�n"
			End Select
		Case "ovid264"
			Select Case lang
				Case "eng"
					ret = "Lookup Function Records to Copy"
				Case "fre"
					ret = "Lookup Function Records to Copy"
				Case "ger"
					ret = "Lookup Function Records to Copy"
				Case "ita"
					ret = "Lookup Function Records to Copy"
				Case "spa"
					ret = "Localizar Registros de Funciones para Copiar"
			End Select
		End Select
	Return ret
End Function
Public Function getpmlibnewpmtaskdetsovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid265"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tasks for Selected Function"
				Case "fre"
					ret = "Add/Edit Tasks for Selected Function"
				Case "ger"
					ret = "Add/Edit Tasks for Selected Function"
				Case "ita"
					ret = "Add/Edit Tasks for Selected Function"
				Case "spa"
					ret = "Add/Edit Tasks for Selected Function"
			End Select
		End Select
	Return ret
End Function
Public Function getPMMainManovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print This PM"
				Case "fre"
					ret = "Print This PM"
				Case "ger"
					ret = "Print This PM"
				Case "ita"
					ret = "Print This PM"
				Case "spa"
					ret = "Print This PM"
			End Select
		Case "imga2"
			Select Case lang
				Case "eng"
					ret = "Print This PM Work Order"
				Case "fre"
					ret = "Print This PM Work Order"
				Case "ger"
					ret = "Print This PM Work Order"
				Case "ita"
					ret = "Print This PM Work Order"
				Case "spa"
					ret = "Print This PM Work Order"
			End Select
		Case "imga3"
			Select Case lang
				Case "eng"
					ret = "Print This PM Work Order w\images"
				Case "fre"
					ret = "Print This PM Work Order w\images"
				Case "ger"
					ret = "Print This PM Work Order w\images"
				Case "ita"
					ret = "Print This PM Work Order w\images"
				Case "spa"
					ret = "Print This PM Work Order w\images"
			End Select
		Case "imgi"
			Select Case lang
				Case "eng"
					ret = "Print This PM"
				Case "fre"
					ret = "Print This PM"
				Case "ger"
					ret = "Print This PM"
				Case "ita"
					ret = "Print This PM"
				Case "spa"
					ret = "Print This PM"
			End Select
		Case "imgi2"
			Select Case lang
				Case "eng"
					ret = "Print This PM Work Order"
				Case "fre"
					ret = "Print This PM Work Order"
				Case "ger"
					ret = "Print This PM Work Order"
				Case "ita"
					ret = "Print This PM Work Order"
				Case "spa"
					ret = "Print This PM Work Order"
			End Select
		Case "imgi3"
			Select Case lang
				Case "eng"
					ret = "Print This PM Work Order w\images"
				Case "fre"
					ret = "Print This PM Work Order w\images"
				Case "ger"
					ret = "Print This PM Work Order w\images"
				Case "ita"
					ret = "Print This PM Work Order w\images"
				Case "spa"
					ret = "Print This PM Work Order w\images"
			End Select
		Case "imgregpm"
			Select Case lang
				Case "eng"
					ret = "Print This PM Collection"
				Case "fre"
					ret = "Print This PM Collection"
				Case "ger"
					ret = "Print This PM Collection"
				Case "ita"
					ret = "Print This PM Collection"
				Case "spa"
					ret = "Print This PM Collection"
			End Select
		Case "imgrteprint"
			Select Case lang
				Case "eng"
					ret = "Print This PM Route Collection"
				Case "fre"
					ret = "Print This PM Route Collection"
				Case "ger"
					ret = "Print This PM Route Collection"
				Case "ita"
					ret = "Print This PM Route Collection"
				Case "spa"
					ret = "Print This PM Route Collection"
			End Select
		Case "imgrtewprint"
			Select Case lang
				Case "eng"
					ret = "Print This PM Route Work Order Collection"
				Case "fre"
					ret = "Print This PM Route Work Order Collection"
				Case "ger"
					ret = "Print This PM Route Work Order Collection"
				Case "ita"
					ret = "Print This PM Route Work Order Collection"
				Case "spa"
					ret = "Print This PM Route Work Order Collection"
			End Select
		Case "imgwopm"
			Select Case lang
				Case "eng"
					ret = "Print This PM Work Order Collection"
				Case "fre"
					ret = "Print This PM Work Order Collection"
				Case "ger"
					ret = "Print This PM Work Order Collection"
				Case "ita"
					ret = "Print This PM Work Order Collection"
				Case "spa"
					ret = "Print This PM Work Order Collection"
			End Select
		Case "iwa"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "iwi"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "ovid78"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		Case "ovid79"
			Select Case lang
				Case "eng"
					ret = "Select Supervisor"
				Case "fre"
					ret = "S�lectionner le superviseur"
				Case "ger"
					ret = "Aufsichtsf�hrenden w�hlen"
				Case "ita"
					ret = "Seleziona supervisore"
				Case "spa"
					ret = "Seleccionar Supervisor "
			End Select
		Case "ovid80"
			Select Case lang
				Case "eng"
					ret = "Select Lead Craft"
				Case "fre"
					ret = "Select Lead Craft"
				Case "ger"
					ret = "Select Lead Craft"
				Case "ita"
					ret = "Select Lead Craft"
				Case "spa"
					ret = "Select Lead Craft"
			End Select
		Case "ovid81"
			Select Case lang
				Case "eng"
					ret = "Get From Date"
				Case "fre"
					ret = "Get From Date"
				Case "ger"
					ret = "Get From Date"
				Case "ita"
					ret = "Get From Date"
				Case "spa"
					ret = "Get From Date"
			End Select
		Case "ovid82"
			Select Case lang
				Case "eng"
					ret = "Get To Date"
				Case "fre"
					ret = "Get To Date"
				Case "ger"
					ret = "Get To Date"
				Case "ita"
					ret = "Get To Date"
				Case "spa"
					ret = "Get To Date"
			End Select
		Case "ovid83"
			Select Case lang
				Case "eng"
					ret = "Get Route"
				Case "fre"
					ret = "Get Route"
				Case "ger"
					ret = "Get Route"
				Case "ita"
					ret = "Get Route"
				Case "spa"
					ret = "Get Route"
			End Select
		Case "tdwrs"
			Select Case lang
				Case "eng"
					ret = "Click to Show or Hide Search Options"
				Case "fre"
					ret = "Click to Show or Hide Search Options"
				Case "ger"
					ret = "Click to Show or Hide Search Options"
				Case "ita"
					ret = "Click to Show or Hide Search Options"
				Case "spa"
					ret = "Click to Show or Hide Search Options"
			End Select
		End Select
	Return ret
End Function
Public Function getPMMainMantpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print This TPM"
				Case "fre"
					ret = "Print This TPM"
				Case "ger"
					ret = "Print This TPM"
				Case "ita"
					ret = "Print This TPM"
				Case "spa"
					ret = "Print This TPM"
			End Select
		Case "imga2"
			Select Case lang
				Case "eng"
					ret = "Print This TPM Work Order"
				Case "fre"
					ret = "Print This TPM Work Order"
				Case "ger"
					ret = "Print This TPM Work Order"
				Case "ita"
					ret = "Print This TPM Work Order"
				Case "spa"
					ret = "Print This TPM Work Order"
			End Select
		Case "imgi"
			Select Case lang
				Case "eng"
					ret = "Print This TPM"
				Case "fre"
					ret = "Print This TPM"
				Case "ger"
					ret = "Print This TPM"
				Case "ita"
					ret = "Print This TPM"
				Case "spa"
					ret = "Print This TPM"
			End Select
		Case "imgi2"
			Select Case lang
				Case "eng"
					ret = "Print This TPM Work Order"
				Case "fre"
					ret = "Print This TPM Work Order"
				Case "ger"
					ret = "Print This TPM Work Order"
				Case "ita"
					ret = "Print This TPM Work Order"
				Case "spa"
					ret = "Print This TPM Work Order"
			End Select
		Case "imgregpm"
			Select Case lang
				Case "eng"
					ret = "Print This TPM Collection"
				Case "fre"
					ret = "Print This TPM Collection"
				Case "ger"
					ret = "Print This TPM Collection"
				Case "ita"
					ret = "Print This TPM Collection"
				Case "spa"
					ret = "Print This TPM Collection"
			End Select
		Case "imgrteprint"
			Select Case lang
				Case "eng"
					ret = "Print This TPM Route Collection"
				Case "fre"
					ret = "Print This TPM Route Collection"
				Case "ger"
					ret = "Print This TPM Route Collection"
				Case "ita"
					ret = "Print This TPM Route Collection"
				Case "spa"
					ret = "Print This TPM Route Collection"
			End Select
		Case "imgrtewprint"
			Select Case lang
				Case "eng"
					ret = "Print This TPM Route Work Order Collection"
				Case "fre"
					ret = "Print This TPM Route Work Order Collection"
				Case "ger"
					ret = "Print This TPM Route Work Order Collection"
				Case "ita"
					ret = "Print This TPM Route Work Order Collection"
				Case "spa"
					ret = "Print This TPM Route Work Order Collection"
			End Select
		Case "imgwopm"
			Select Case lang
				Case "eng"
					ret = "Print This TPM Work Order Collection"
				Case "fre"
					ret = "Print This TPM Work Order Collection"
				Case "ger"
					ret = "Print This TPM Work Order Collection"
				Case "ita"
					ret = "Print This TPM Work Order Collection"
				Case "spa"
					ret = "Print This TPM Work Order Collection"
			End Select
		Case "iwa"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "iwi"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "ovid105"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		Case "ovid106"
			Select Case lang
				Case "eng"
					ret = "Select Supervisor"
				Case "fre"
					ret = "S�lectionner le superviseur"
				Case "ger"
					ret = "Aufsichtsf�hrenden w�hlen"
				Case "ita"
					ret = "Seleziona supervisore"
				Case "spa"
					ret = "Seleccionar Supervisor "
			End Select
		Case "ovid107"
			Select Case lang
				Case "eng"
					ret = "Select Lead Craft"
				Case "fre"
					ret = "Select Lead Craft"
				Case "ger"
					ret = "Select Lead Craft"
				Case "ita"
					ret = "Select Lead Craft"
				Case "spa"
					ret = "Select Lead Craft"
			End Select
		Case "ovid108"
			Select Case lang
				Case "eng"
					ret = "Get From Date"
				Case "fre"
					ret = "Get From Date"
				Case "ger"
					ret = "Get From Date"
				Case "ita"
					ret = "Get From Date"
				Case "spa"
					ret = "Get From Date"
			End Select
		Case "ovid109"
			Select Case lang
				Case "eng"
					ret = "Get To Date"
				Case "fre"
					ret = "Get To Date"
				Case "ger"
					ret = "Get To Date"
				Case "ita"
					ret = "Get To Date"
				Case "spa"
					ret = "Get To Date"
			End Select
		Case "ovid110"
			Select Case lang
				Case "eng"
					ret = "Get Route"
				Case "fre"
					ret = "Get Route"
				Case "ger"
					ret = "Get Route"
				Case "ita"
					ret = "Get Route"
				Case "spa"
					ret = "Get Route"
			End Select
		End Select
	Return ret
End Function
Public Function getpmmeovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ihg"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ihga"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "iwo"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "iwoa"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ovid84"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid85"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function getPMMeasovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ihg"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ihga"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "iwo"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "iwoa"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ovid86"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid87"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function getPMMeastpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ihg"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ihga"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "iwo"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "iwoa"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ovid111"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid112"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function getpmmetpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ihg"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ihga"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "img"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print History"
				Case "fre"
					ret = "Print History"
				Case "ger"
					ret = "Print History"
				Case "ita"
					ret = "Print History"
				Case "spa"
					ret = "Print History"
			End Select
		Case "iwo"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "iwoa"
			Select Case lang
				Case "eng"
					ret = "View History Graph"
				Case "fre"
					ret = "View History Graph"
				Case "ger"
					ret = "View History Graph"
				Case "ita"
					ret = "View History Graph"
				Case "spa"
					ret = "View History Graph"
			End Select
		Case "ovid113"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid114"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function getPMOptMainovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Archive Current PM Revision and Create a New Revision"
				Case "fre"
					ret = "Archiver une r�vision actuelle de MP et cr�er une nouvelle r�vision"
				Case "ger"
					ret = "Archiviere die aktuelle PM-�berarbeitung und erstelle eine neue �berarbeitung"
				Case "ita"
					ret = "Archivia l`attuale revisione PM e crea una nuova revisione"
				Case "spa"
					ret = "Archivar la Revisi�n de PM Actual y Crear una Nueva Revisi�n"
			End Select
		Case "Img6"
			Select Case lang
				Case "eng"
					ret = "Archive Current PM Revision and Convert for Optimization"
				Case "fre"
					ret = "Archiver une r�vision actuelle de MP et la convertir pour optimisation"
				Case "ger"
					ret = "Archiviere aktuelle PM-�berarbeitung und konvertiere f�r Optimierung"
				Case "ita"
					ret = "Archivia l`attuale revisione PM e converti per l`ottimizzazione"
				Case "spa"
					ret = "Archivar la Revisi�n de PM Actual y Convertir para Optimizaci�n"
			End Select
		Case "imgaddeq"
			Select Case lang
				Case "eng"
					ret = "Add a New Equipment Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement d`�quipement"
				Case "ger"
					ret = "F�ge eine neue Ausr�stungs-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record Nuova apparecchiatura"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Equipo"
			End Select
		Case "imgaddfu"
			Select Case lang
				Case "eng"
					ret = "Add a New Function Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement de fonction"
				Case "ger"
					ret = "F�ge eine neue Funktions-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record  Nuova funzione"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Funci�n"
			End Select
		Case "imgbulk"
			Select Case lang
				Case "eng"
					ret = "Choose a Bulk Loaded Procedure to Optimize"
				Case "fre"
					ret = "S�lectionner une proc�dure de chargement en vrac � optimiser"
				Case "ger"
					ret = "W�hle ein 08/15-Verfahren zur Optimierung"
				Case "ita"
					ret = "Scegliere un procedimento di caricamento di massa per ottimizzare"
				Case "spa"
					ret = "***Escoja un Procedimiento Cargado A granel para Perfeccionar"
			End Select
		Case "imgbulkg"
			Select Case lang
				Case "eng"
					ret = "Choose a Bulk Loaded Procedure from the Document Library to Optimize"
				Case "fre"
					ret = "S�lectionner une proc�dure de chargement en vrac � partir de la biblioth�que de documents � optimiser"
				Case "ger"
					ret = "W�hle ein 08/15-Verfahren aus der Dokumenten-Bibliothek zur Optimierung"
				Case "ita"
					ret = "Scegliere un procedimento di caricamento di massa dalla galleria documenti per ottimizzare"
				Case "spa"
					ret = "***Escoja un Procedimiento Cargado en  de la Biblioteca del Documento para Optimizar"
			End Select
		Case "imgcopyeq"
			Select Case lang
				Case "eng"
					ret = "Copy an Equipment Record"
				Case "fre"
					ret = "Copier un enregistrement d`�quipement"
				Case "ger"
					ret = "Kopiere eine Ausr�stungs-Eintragung"
				Case "ita"
					ret = "Copia una voce Apparecchiatura"
				Case "spa"
					ret = "Copiar un Registro de Equipo"
			End Select
		Case "imgcopyfu"
			Select Case lang
				Case "eng"
					ret = "Copy a Function Record"
				Case "fre"
					ret = "Copier un enregistrement de fonction"
				Case "ger"
					ret = "Kopiere eine Funktions-Eintragung"
				Case "ita"
					ret = "Copia una voce Funzione"
				Case "spa"
					ret = "Copiar un Registro de Funci�n"
			End Select
		Case "imgdrag"
			Select Case lang
				Case "eng"
					ret = "Drag and Drop Tasks from Procedures"
				Case "fre"
					ret = "Glisser-d�poser les t�ches � partir des proc�dures"
				Case "ger"
					ret = "Drag and Drop Aufgaben von Verfahren"
				Case "ita"
					ret = "Trascina e rilascia Attivit� da Procedure"
				Case "spa"
					ret = "Arrastrar y soltar Tareas de los Procedimientos "
			End Select
		Case "imgproc"
			Select Case lang
				Case "eng"
					ret = "Add Procedures to Optimize"
				Case "fre"
					ret = "Add Procedures to Optimize"
				Case "ger"
					ret = "Add Procedures to Optimize"
				Case "ita"
					ret = "Add Procedures to Optimize"
				Case "spa"
					ret = "Agregar Procedimientos para Optimizar"
			End Select
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Start Over"
				Case "fre"
					ret = "Effacer l`�cran et red�marrer"
				Case "ger"
					ret = "Bildschirminhalt l�schen und neu starten"
				Case "ita"
					ret = "Cancellare finestra e riavviare"
				Case "spa"
					ret = "Limpiar Pantalla y Volver a Empezar"
			End Select
		Case "imgtpd"
			Select Case lang
				Case "eng"
					ret = "Jump to the TPM Developer"
				Case "fre"
					ret = "Sauter � d�veloppeur de MPT"
				Case "ger"
					ret = "Springe zum TPM-Entwickler"
				Case "ita"
					ret = "Passa allo Sviluppatore TPM"
				Case "spa"
					ret = "Salte al Desarrollador de TPM"
			End Select
		Case "imgtpma"
			Select Case lang
				Case "eng"
					ret = "You have potential TPM Tasks - Click this icon to Review"
				Case "fre"
					ret = "Vous avez des t�ches de MPT potentielles - Cliquer sur cette ic�ne � revoir"
				Case "ger"
					ret = "Sie haben m�gliche TPM-Aufgaben � Auf dieses Icon f�r �berarbeitung klicken"
				Case "ita"
					ret = "Sono presenti probabili attivit� TPM - Cliccare su questa icona per la revisione"
				Case "spa"
					ret = "Usted tieneTareas Potencialaes de TPM - Pulse este �cono para Revisar"
			End Select
		Case "imgtpo"
			Select Case lang
				Case "eng"
					ret = "Jump to the TPM Optimizer"
				Case "fre"
					ret = "Sauter � optimiseur de MPT"
				Case "ger"
					ret = "Springe zum TPM-Optimierer"
				Case "ita"
					ret = "Passa all`Ottimizzatore TPM"
				Case "spa"
					ret = "Salte al Optimizador de TPM"
			End Select
		Case "ovid124"
			Select Case lang
				Case "eng"
					ret = "Use Max Search"
				Case "fre"
					ret = "Use Max Search"
				Case "ger"
					ret = "Use Max Search"
				Case "ita"
					ret = "Use Max Search"
				Case "spa"
					ret = "Use Max Search"
			End Select
		Case "ovid125"
			Select Case lang
				Case "eng"
					ret = "Review\Edit PM Approval Process for this Asset"
				Case "fre"
					ret = "Revoir/ modifier le processus d`approbation de MP pour cet actif"
				Case "ger"
					ret = "�berarbeite/bearbeite das PM-Zustimmungsverfahren f�r dieses Wirtschaftsgut"
				Case "ita"
					ret = "Esaminare/modificare il processo di approvazione PM per questo prodotto"
				Case "spa"
					ret = "Revisar\Editar el Proceso de Aprobaci�n PM para este Activo"
			End Select
		Case "ovid126"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		Case "ovid127"
			Select Case lang
				Case "eng"
					ret = "View\Edit Original Total Values"
				Case "fre"
					ret = "Afficher/ modifier les valeurs originales totales"
				Case "ger"
					ret = "Urspr�ngliche Gesamtwerte anzeigen/bearbeiten"
				Case "ita"
					ret = "Visualizza/modifica i valori totali originari"
				Case "spa"
					ret = "Ver\Editar los Valores Totales Originales"
			End Select
		End Select
	Return ret
End Function
Public Function getPMOptRationale2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid128"
			Select Case lang
				Case "eng"
					ret = "Print PMO Task Rationale Report"
				Case "fre"
					ret = "Imprimer le rapport de justification de t�che de PMO"
				Case "ger"
					ret = "Drucke PMO-Aufgaben-Begr�ndungs-Bericht"
				Case "ita"
					ret = "Stampa report rationale attivit� PMO"
				Case "spa"
					ret = "Imprimir Reporte de Razonamientos de Tareas PMO"
			End Select
		End Select
	Return ret
End Function
Public Function getPMOptRationaleTPM2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid140"
			Select Case lang
				Case "eng"
					ret = "Print PMO Task Rationale Report"
				Case "fre"
					ret = "Imprimer le rapport de justification de t�che de PMO"
				Case "ger"
					ret = "Drucke PMO-Aufgaben-Begr�ndungs-Bericht"
				Case "ita"
					ret = "Stampa report rationale attivit� PMO"
				Case "spa"
					ret = "Imprimir Reporte de Razonamientos de Tareas PMO"
			End Select
		End Select
	Return ret
End Function
Public Function getPMOptTasksovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddcomp"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Components for Selected Function"
				Case "fre"
					ret = "Ajouter/ modifier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Aggiungi/modifica i componenti per la funzione selezionata"
				Case "spa"
					ret = "Agregar/Editar Componentes para la Funci�n Seleccionada"
			End Select
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "ggrid"
			Select Case lang
				Case "eng"
					ret = "Review Changes in Grid View"
				Case "fre"
					ret = "Review Changes in Grid View"
				Case "ger"
					ret = "Review Changes in Grid View"
				Case "ita"
					ret = "Review Changes in Grid View"
				Case "spa"
					ret = "Revisar Cambios en Vista de la Parrilla"
			End Select
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - All Tasks"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - toutes les t�ches"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � alle Aufgaben"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Tutte le attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Todas las Tareas"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - This Task"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - cette t�che"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � diese Aufgabe"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Questa attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Esta Tarea"
			End Select
		Case "imgcopycomp"
			Select Case lang
				Case "eng"
					ret = "Copy Components for Selected Function"
				Case "fre"
					ret = "Copier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "Kopiere Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Copia i componenti per la funzione scelta"
				Case "spa"
					ret = "Copiar Componentes para la Funci�n Seleccionada"
			End Select
		Case "imgdeltask"
			Select Case lang
				Case "eng"
					ret = "Delete This Task"
				Case "fre"
					ret = "Supprimer cette t�che"
				Case "ger"
					ret = "Diese Aufgabe l�schen"
				Case "ita"
					ret = "Elimina questa attivit�"
				Case "spa"
					ret = "Anular esta Tarea"
			End Select
		Case "imgdeltpm"
			Select Case lang
				Case "eng"
					ret = "Delete This Task From the TPM Module and Restore Editing"
				Case "fre"
					ret = "Supprimer cette t�che du module de MPT et restaurer la modification�?"
				Case "ger"
					ret = "L�sche diese Aufgabe vom TPM-Modul und stelle Bearbeitung wieder her"
				Case "ita"
					ret = "Elimina questa attivit� dal Modulo TPM e ripristinare le modifiche"
				Case "spa"
					ret = "Anular Esta Tarea Del M�dulo de TPM y Restaurar la Edici�n"
			End Select
		Case "imgrat"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Rationale for Task Changes"
				Case "fre"
					ret = "Ajouter/�diter un Expos� Raisonn� pour les Changements de T�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Begr�ndung f�r Aufgaben�nderungen"
				Case "ita"
					ret = "Aggiungi/modifica rationale per modifiche attivit�"
				Case "spa"
					ret = "Agregar/Editar Razonamiento para Cambios en Tarea "
			End Select
		Case "ovid129"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid130"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid131"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid132"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid133"
			Select Case lang
				Case "eng"
					ret = "Choose Equipment Spare Parts for this Task"
				Case "fre"
					ret = "S�lectionner les pi�ces de rechange de l`�quipement pour cette t�che "
				Case "ger"
					ret = "W�hle die Ausr�stungs-Ersatzteile f�r diese Aufgabe"
				Case "ita"
					ret = "Scegliere le parti di ricambio dell`apparecchiatura per questa attivit�"
				Case "spa"
					ret = "Escoja las Partes de Repuesto del Equipo para esta Tarea"
			End Select
		Case "ovid134"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid135"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "ovid136"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Notes for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les remarques pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Anmerkungen f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le note per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Notas para esta Tarea"
			End Select
		Case "ovid137"
			Select Case lang
				Case "eng"
					ret = "View or Edit Images for this Task"
				Case "fre"
					ret = "View or Edit Images for this Task"
				Case "ger"
					ret = "View or Edit Images for this Task"
				Case "ita"
					ret = "View or Edit Images for this Task"
				Case "spa"
					ret = "View or Edit Images for this Task"
			End Select
		Case "ovid138"
			Select Case lang
				Case "eng"
					ret = "View Reports"
				Case "fre"
					ret = "View Reports"
				Case "ger"
					ret = "View Reports"
				Case "ita"
					ret = "View Reports"
				Case "spa"
					ret = "Ver Reportes"
			End Select
		Case "ovid139"
			Select Case lang
				Case "eng"
					ret = "Go to the Centralized PM Library"
				Case "fre"
					ret = "Go to the Centralized PM Library"
				Case "ger"
					ret = "Go to the Centralized PM Library"
				Case "ita"
					ret = "Go to the Centralized PM Library"
				Case "spa"
					ret = "Ir a la Biblioteca de PM Centralizada"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getPMRouteListovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid166"
			Select Case lang
				Case "eng"
					ret = "Search Routes"
				Case "fre"
					ret = "Rechercher les chemins"
				Case "ger"
					ret = "Strecken suchen"
				Case "ita"
					ret = "Cerca instradamenti"
				Case "spa"
					ret = "Buscar Rutas "
			End Select
		Case "ovid167"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Routes"
				Case "fre"
					ret = "Ajouter/modifier les itin�raires"
				Case "ger"
					ret = "Hinzuf�gen / Bearbeiten von Routen"
				Case "ita"
					ret = "Aggiungi/Modifica percorsi"
				Case "spa"
					ret = "Add/Edit Routes"
			End Select
		Case "ovid168"
			Select Case lang
				Case "eng"
					ret = "Sort Ascending"
				Case "fre"
					ret = "Sort Ascending"
				Case "ger"
					ret = "Sort Ascending"
				Case "ita"
					ret = "Sort Ascending"
				Case "spa"
					ret = "Sort Ascending"
			End Select
		Case "ovid169"
			Select Case lang
				Case "eng"
					ret = "Sort Descending"
				Case "fre"
					ret = "Sort Descending"
				Case "ger"
					ret = "Sort Descending"
				Case "ita"
					ret = "Sort Descending"
				Case "spa"
					ret = "Sort Descending"
			End Select
		End Select
	Return ret
End Function
Public Function getPMRoutesovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgadd"
			Select Case lang
				Case "eng"
					ret = "Create a Route with the Name and Decription You Have Entered"
				Case "fre"
					ret = "Create a Route with the Name and Decription You Have Entered"
				Case "ger"
					ret = "Create a Route with the Name and Decription You Have Entered"
				Case "ita"
					ret = "Create a Route with the Name and Decription You Have Entered"
				Case "spa"
					ret = "Create a Route with the Name and Decription You Have Entered"
			End Select
		Case "ovid170"
			Select Case lang
				Case "eng"
					ret = "View Equipment Route Record List"
				Case "fre"
					ret = "View Equipment Route Record List"
				Case "ger"
					ret = "View Equipment Route Record List"
				Case "ita"
					ret = "View Equipment Route Record List"
				Case "spa"
					ret = "View Equipment Route Record List"
			End Select
		Case "ovid171"
			Select Case lang
				Case "eng"
					ret = "Save Changes to Route# and Description"
				Case "fre"
					ret = "Save Changes to Route# and Description"
				Case "ger"
					ret = "Save Changes to Route# and Description"
				Case "ita"
					ret = "Save Changes to Route# and Description"
				Case "spa"
					ret = "Save Changes to Route# and Description"
			End Select
		End Select
	Return ret
End Function
Public Function getPMRoutes2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgadd"
			Select Case lang
				Case "eng"
					ret = "Create a Route with the Name and Decription You Have Entered"
				Case "fre"
					ret = "Create a Route with the Name and Decription You Have Entered"
				Case "ger"
					ret = "Create a Route with the Name and Decription You Have Entered"
				Case "ita"
					ret = "Create a Route with the Name and Decription You Have Entered"
				Case "spa"
					ret = "Create a Route with the Name and Decription You Have Entered"
			End Select
		Case "imgsav"
			Select Case lang
				Case "eng"
					ret = "Save Changes to Route# and Description"
				Case "fre"
					ret = "Save Changes to Route# and Description"
				Case "ger"
					ret = "Save Changes to Route# and Description"
				Case "ita"
					ret = "Save Changes to Route# and Description"
				Case "spa"
					ret = "Save Changes to Route# and Description"
			End Select
		Case "ovid172"
			Select Case lang
				Case "eng"
					ret = "View Equipment Route Record List"
				Case "fre"
					ret = "View Equipment Route Record List"
				Case "ger"
					ret = "View Equipment Route Record List"
				Case "ita"
					ret = "View Equipment Route Record List"
				Case "spa"
					ret = "View Equipment Route Record List"
			End Select
		End Select
	Return ret
End Function
Public Function getpmselectovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid53"
			Select Case lang
				Case "eng"
					ret = "Add a New Equipment Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement d`�quipement"
				Case "ger"
					ret = "F�ge eine neue Ausr�stungs-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record Nuova apparecchiatura"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Equipo"
			End Select
		Case "ovid54"
			Select Case lang
				Case "eng"
					ret = "Copy an Equipment Record"
				Case "fre"
					ret = "Copier un enregistrement d`�quipement"
				Case "ger"
					ret = "Kopiere eine Ausr�stungs-Eintragung"
				Case "ita"
					ret = "Copia una voce Apparecchiatura"
				Case "spa"
					ret = "Copiar un Registro de Equipo"
			End Select
		Case "ovid55"
			Select Case lang
				Case "eng"
					ret = "Add Procedures to Optimize"
				Case "fre"
					ret = "Add Procedures to Optimize"
				Case "ger"
					ret = "Add Procedures to Optimize"
				Case "ita"
					ret = "Add Procedures to Optimize"
				Case "spa"
					ret = "Agregar Procedimientos para Optimizar"
			End Select
		Case "ovid56"
			Select Case lang
				Case "eng"
					ret = "Transfer PM Data to Drag-n-Drop Screen"
				Case "fre"
					ret = "Transf�rer les donn�es de MP vers l`�cran Glisser-d�poser"
				Case "ger"
					ret = "PM-Daten zum Drag-n-Drop-Bildschirm �bertragen"
				Case "ita"
					ret = "Trasferire i dati PM nella finestra Drag-n-Drop"
				Case "spa"
					ret = "Transferir los Datos de PM a la Pantalla de Arrastrar y Soltar"
			End Select
		End Select
	Return ret
End Function
Public Function getPMTaskDivFuncovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddcomp"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Components for Selected Function"
				Case "fre"
					ret = "Ajouter/ modifier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Aggiungi/modifica i componenti per la funzione selezionata"
				Case "spa"
					ret = "Agregar/Editar Componentes para la Funci�n Seleccionada"
			End Select
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "ggrid"
			Select Case lang
				Case "eng"
					ret = "Review Changes in Grid View"
				Case "fre"
					ret = "Review Changes in Grid View"
				Case "ger"
					ret = "Review Changes in Grid View"
				Case "ita"
					ret = "Review Changes in Grid View"
				Case "spa"
					ret = "Revisar Cambios en Vista de la Parrilla"
			End Select
		Case "img1"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - All Tasks"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - toutes les t�ches"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � alle Aufgaben"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Tutte le attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Todas las Tareas"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - This Task"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - cette t�che"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � diese Aufgabe"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Questa attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Esta Tarea"
			End Select
		Case "imgcopycomp"
			Select Case lang
				Case "eng"
					ret = "Copy Components for Selected Function"
				Case "fre"
					ret = "Copier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "Kopiere Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Copia i componenti per la funzione scelta"
				Case "spa"
					ret = "Copiar Componentes para la Funci�n Seleccionada"
			End Select
		Case "imgdeltpm"
			Select Case lang
				Case "eng"
					ret = "Delete This Task From the TPM Module and Restore Editing"
				Case "fre"
					ret = "Supprimer cette t�che du module de MPT et restaurer la modification�?"
				Case "ger"
					ret = "L�sche diese Aufgabe vom TPM-Modul und stelle Bearbeitung wieder her"
				Case "ita"
					ret = "Elimina questa attivit� dal Modulo TPM e ripristinare le modifiche"
				Case "spa"
					ret = "Anular Esta Tarea Del M�dulo de TPM y Restaurar la Edici�n"
			End Select
		Case "ovid13"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid14"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid15"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid16"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid17"
			Select Case lang
				Case "eng"
					ret = "Choose Skills for this Plant Site "
				Case "fre"
					ret = "Choisir les Comp�tences pour ce Site d`�tablissement"
				Case "ger"
					ret = "Kompetenzen f�r dieses Werksgrundst�ck ausw�hlen"
				Case "ita"
					ret = "Scegliere Skill per questa localit� Impianto "
				Case "spa"
					ret = "Elija habilidades para este sitio de planta"
			End Select
		Case "ovid18"
			Select Case lang
				Case "eng"
					ret = "Choose Equipment Spare Parts for this Task"
				Case "fre"
					ret = "S�lectionner les pi�ces de rechange de l`�quipement pour cette t�che "
				Case "ger"
					ret = "W�hle die Ausr�stungs-Ersatzteile f�r diese Aufgabe"
				Case "ita"
					ret = "Scegliere le parti di ricambio dell`apparecchiatura per questa attivit�"
				Case "spa"
					ret = "Escoja las Partes de Repuesto del Equipo para esta Tarea"
			End Select
		Case "ovid19"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid20"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "ovid21"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Notes for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les remarques pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Anmerkungen f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le note per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Notas para esta Tarea"
			End Select
		Case "ovid22"
			Select Case lang
				Case "eng"
					ret = "View or Edit Images for this Task"
				Case "fre"
					ret = "View or Edit Images for this Task"
				Case "ger"
					ret = "View or Edit Images for this Task"
				Case "ita"
					ret = "View or Edit Images for this Task"
				Case "spa"
					ret = "View or Edit Images for this Task"
			End Select
		Case "ovid23"
			Select Case lang
				Case "eng"
					ret = "View Reports"
				Case "fre"
					ret = "View Reports"
				Case "ger"
					ret = "View Reports"
				Case "ita"
					ret = "View Reports"
				Case "spa"
					ret = "Ver Reportes"
			End Select
		Case "ovid24"
			Select Case lang
				Case "eng"
					ret = "Go to the Centralized PM Library"
				Case "fre"
					ret = "Go to the Centralized PM Library"
				Case "ger"
					ret = "Go to the Centralized PM Library"
				Case "ita"
					ret = "Go to the Centralized PM Library"
				Case "spa"
					ret = "Ir a la Biblioteca de PM Centralizada"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function getpmuploadimageovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgref"
			Select Case lang
				Case "eng"
					ret = "Refresh Details for New Image Entry"
				Case "fre"
					ret = "Actualiser les d�tails pour la nouvelle entr�e d`image"
				Case "ger"
					ret = "Einzelheiten f�r neue Bildeingabe auffrischen"
				Case "ita"
					ret = "Aggiorna dettagli per la voce Nuova immagine"
				Case "spa"
					ret = "Refrescar Detalles para Entrada de Nueva Imagen "
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Details"
				Case "fre"
					ret = "Sauvegarder les d�tails de l`image"
				Case "ger"
					ret = "Bildeinzelheiten speichern"
				Case "ita"
					ret = "Salva dettagli immagine"
				Case "spa"
					ret = "Guardar los Detalles de la Imagen "
			End Select
		End Select
	Return ret
End Function
Public Function getpurchreqovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Look up Vendor"
				Case "fre"
					ret = "Look up Vendor"
				Case "ger"
					ret = "Look up Vendor"
				Case "ita"
					ret = "Look up Vendor"
				Case "spa"
					ret = "Look up Vendor"
			End Select
		Case "imgsrch"
			Select Case lang
				Case "eng"
					ret = "Look up Inventory Items"
				Case "fre"
					ret = "Rechercher les articles d`inventaire"
				Case "ger"
					ret = "Inventarartikel-Suche"
				Case "ita"
					ret = "Ricerca articoli inventario"
				Case "spa"
					ret = "Buscar Art�culos en Inventario"
			End Select
		End Select
	Return ret
End Function
Public Function getpurchreqeditovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Save Purchasing Updates"
				Case "fre"
					ret = "Save Purchasing Updates"
				Case "ger"
					ret = "Save Purchasing Updates"
				Case "ita"
					ret = "Save Purchasing Updates"
				Case "spa"
					ret = "Save Purchasing Updates"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Look up Vendor"
				Case "fre"
					ret = "Look up Vendor"
				Case "ger"
					ret = "Look up Vendor"
				Case "ita"
					ret = "Look up Vendor"
				Case "spa"
					ret = "Look up Vendor"
			End Select
		End Select
	Return ret
End Function
Public Function getqueryovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgadmqt"
			Select Case lang
				Case "eng"
					ret = "Administrator Access to Adjust Query Timeout"
				Case "fre"
					ret = "Acc�s d`administrateur pour r�gler le d�lai d`attente de requ�te "
				Case "ger"
					ret = "Administrator-Zugriff zur Einstellung des Suchanfragen-Timeouts"
				Case "ita"
					ret = "Accesso dell`amministratore per regolare il timeout delle query"
				Case "spa"
					ret = "Administrador: Acceso para ajustar tiempo l�mite de Pregunta"
			End Select
		Case "ovid301"
			Select Case lang
				Case "eng"
					ret = "Export Results to Excel"
				Case "fre"
					ret = "Exporter les r�sultats dans Excel"
				Case "ger"
					ret = "Exportiere Ergebnisse nach Excel"
				Case "ita"
					ret = "Esporta risultati su Excel"
				Case "spa"
					ret = "Resultados de exportaci�n a Excel "
			End Select
		Case "ovid302"
			Select Case lang
				Case "eng"
					ret = "Save Query for Future Use"
				Case "fre"
					ret = "Sauvegarder la requ�te pour une utilisation ult�rieure"
				Case "ger"
					ret = "Suchanfrage f�r zuk�nftige Verwendung speichern"
				Case "ita"
					ret = "Salva query per utilizzo futuro"
				Case "spa"
					ret = "Guardar la Pregunta para Uso Futuro "
			End Select
		Case "ovid303"
			Select Case lang
				Case "eng"
					ret = "Look-up Saved Queries"
				Case "fre"
					ret = "Rechercher les requ�tes sauvegard�es"
				Case "ger"
					ret = "Suche nach gespeicherten Suchanfragen"
				Case "ita"
					ret = "Ricerca query salvate"
				Case "spa"
					ret = "Buscar Preguntas Guardadas "
			End Select
		Case "ovid304"
			Select Case lang
				Case "eng"
					ret = "Clear Query Screen"
				Case "fre"
					ret = "Effacer l`�cran de requ�te"
				Case "ger"
					ret = "Suchanfrage-Schirm leeren"
				Case "ita"
					ret = "Eliminare finestra query"
				Case "spa"
					ret = "Limpiar Pantalla de Preguntas"
			End Select
		Case "ovid305"
			Select Case lang
				Case "eng"
					ret = "Submit Query"
				Case "fre"
					ret = "Envoyer une requ�te"
				Case "ger"
					ret = "Suchanfrage einreichen"
				Case "ita"
					ret = "Inserisci query"
				Case "spa"
					ret = "Someter Pregunta "
			End Select
		End Select
	Return ret
End Function
Public Function getrecvinvovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Save Receive Information"
				Case "fre"
					ret = "Save Receive Information"
				Case "ger"
					ret = "Save Receive Information"
				Case "ita"
					ret = "Save Receive Information"
				Case "spa"
					ret = "Save Receive Information"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Look up current Purchase Requisitions"
				Case "fre"
					ret = "Look up current Purchase Requisitions"
				Case "ger"
					ret = "Look up current Purchase Requisitions"
				Case "ita"
					ret = "Look up current Purchase Requisitions"
				Case "spa"
					ret = "Look up current Purchase Requisitions"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Look up Vendor"
				Case "fre"
					ret = "Look up Vendor"
				Case "ger"
					ret = "Look up Vendor"
				Case "ita"
					ret = "Look up Vendor"
				Case "spa"
					ret = "Look up Vendor"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Look up Manufacturer"
				Case "fre"
					ret = "Look up Manufacturer"
				Case "ger"
					ret = "Look up Manufacturer"
				Case "ita"
					ret = "Look up Manufacturer"
				Case "spa"
					ret = "Look up Manufacturer"
			End Select
		Case "imglot"
			Select Case lang
				Case "eng"
					ret = "Select a Lot Type"
				Case "fre"
					ret = "Select a Lot Type"
				Case "ger"
					ret = "Select a Lot Type"
				Case "ita"
					ret = "Select a Lot Type"
				Case "spa"
					ret = "Select a Lot Type"
			End Select
		Case "imgsrch"
			Select Case lang
				Case "eng"
					ret = "Look up Inventory Items"
				Case "fre"
					ret = "Rechercher les articles d`inventaire"
				Case "ger"
					ret = "Inventarartikel-Suche"
				Case "ita"
					ret = "Ricerca articoli inventario"
				Case "spa"
					ret = "Buscar Art�culos en Inventario"
			End Select
		Case "ovid268"
			Select Case lang
				Case "eng"
					ret = "Clear Page"
				Case "fre"
					ret = "Effacer la page"
				Case "ger"
					ret = "Seite leeren"
				Case "ita"
					ret = "Eliminare pagina"
				Case "spa"
					ret = "Limpiar P�gina"
			End Select
		Case "ovid269"
			Select Case lang
				Case "eng"
					ret = "Select Receive Date"
				Case "fre"
					ret = "Select Receive Date"
				Case "ger"
					ret = "Select Receive Date"
				Case "ita"
					ret = "Select Receive Date"
				Case "spa"
					ret = "Select Receive Date"
			End Select
		Case "ovid270"
			Select Case lang
				Case "eng"
					ret = "Select Use By Date"
				Case "fre"
					ret = "Select Use By Date"
				Case "ger"
					ret = "Select Use By Date"
				Case "ita"
					ret = "Select Use By Date"
				Case "spa"
					ret = "Select Use By Date"
			End Select
		Case "ovid271"
			Select Case lang
				Case "eng"
					ret = "Select Store Room where this item will be received"
				Case "fre"
					ret = "Select Store Room where this item will be received"
				Case "ger"
					ret = "Select Store Room where this item will be received"
				Case "ita"
					ret = "Select Store Room where this item will be received"
				Case "spa"
					ret = "Select Store Room where this item will be received"
			End Select
		End Select
	Return ret
End Function
Public Function getreorderdetailsovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Save selected reorder datails"
				Case "fre"
					ret = "Save selected reorder datails"
				Case "ger"
					ret = "Save selected reorder datails"
				Case "ita"
					ret = "Save selected reorder datails"
				Case "spa"
					ret = "Save selected reorder datails"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Select Order Unit"
				Case "fre"
					ret = "S�lectionner l`unit� de commande"
				Case "ger"
					ret = "Auftragseinheit w�hlen"
				Case "ita"
					ret = "Seleziona unit� ordine"
				Case "spa"
					ret = "Seleccionar Unidad de Orden"
			End Select
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Select Order Unit"
				Case "fre"
					ret = "S�lectionner l`unit� de commande"
				Case "ger"
					ret = "Auftragseinheit w�hlen"
				Case "ita"
					ret = "Seleziona unit� ordine"
				Case "spa"
					ret = "Seleccionar Unidad de Orden"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Look up Primary Vendor"
				Case "fre"
					ret = "Look up Primary Vendor"
				Case "ger"
					ret = "Look up Primary Vendor"
				Case "ita"
					ret = "Look up Primary Vendor"
				Case "spa"
					ret = "Look up Primary Vendor"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "look up Manufacturer"
				Case "fre"
					ret = "look up Manufacturer"
				Case "ger"
					ret = "look up Manufacturer"
				Case "ita"
					ret = "look up Manufacturer"
				Case "spa"
					ret = "look up Manufacturer"
			End Select
		Case "imgsrch"
			Select Case lang
				Case "eng"
					ret = "Look up Inventory Items"
				Case "fre"
					ret = "Rechercher les articles d`inventaire"
				Case "ger"
					ret = "Inventarartikel-Suche"
				Case "ita"
					ret = "Ricerca articoli inventario"
				Case "spa"
					ret = "Buscar Art�culos en Inventario"
			End Select
		Case "ovid272"
			Select Case lang
				Case "eng"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
				Case "fre"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
				Case "ger"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
				Case "ita"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
				Case "spa"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
			End Select
		End Select
	Return ret
End Function
Public Function getreports2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "pgemshtm1"
			Select Case lang
				Case "eng"
					ret = "Print version with formatting of CMMS Output"
				Case "fre"
					ret = "Print version with formatting of CMMS Output"
				Case "ger"
					ret = "Print version with formatting of CMMS Output"
				Case "ita"
					ret = "Print version with formatting of CMMS Output"
				Case "spa"
					ret = "Imprimir la versi�n con Formato de Salida de CMMS"
			End Select
		Case "pgemshtm12"
			Select Case lang
				Case "eng"
					ret = "Print version with formatting of CMMS PdM Output"
				Case "fre"
					ret = "Print version with formatting of CMMS PdM Output"
				Case "ger"
					ret = "Print version with formatting of CMMS PdM Output"
				Case "ita"
					ret = "Print version with formatting of CMMS PdM Output"
				Case "spa"
					ret = "Imprimir la versi�n con Formato de Salida PdM de CMMS"
			End Select
		Case "pgemshtm1tpm"
			Select Case lang
				Case "eng"
					ret = "Print version with formatting of CMMS Output"
				Case "fre"
					ret = "Print version with formatting of CMMS Output"
				Case "ger"
					ret = "Print version with formatting of CMMS Output"
				Case "ita"
					ret = "Print version with formatting of CMMS Output"
				Case "spa"
					ret = "Imprimir la versi�n con Formato de Salida de CMMS"
			End Select
		Case "pgemshtm1tpmpic"
			Select Case lang
				Case "eng"
					ret = "Print version with formatting of CMMS Output"
				Case "fre"
					ret = "Print version with formatting of CMMS Output"
				Case "ger"
					ret = "Print version with formatting of CMMS Output"
				Case "ita"
					ret = "Print version with formatting of CMMS Output"
				Case "spa"
					ret = "Imprimir la versi�n con Formato de Salida de CMMS"
			End Select
		Case "pgemstxt1"
			Select Case lang
				Case "eng"
					ret = "Output to Text File - Requires Lower Internet Security Settings"
				Case "fre"
					ret = "Output to Text File - Requires Lower Internet Security Settings"
				Case "ger"
					ret = "Output to Text File - Requires Lower Internet Security Settings"
				Case "ita"
					ret = "Output to Text File - Requires Lower Internet Security Settings"
				Case "spa"
					ret = "Salida de informaci�n en forma texto - Requiere Ajustes m�s bajos de seguridad Internet "
			End Select
		Case "pgemstxt1tpm"
			Select Case lang
				Case "eng"
					ret = "Output to Text File - Requires Lower Internet Security Settings"
				Case "fre"
					ret = "Output to Text File - Requires Lower Internet Security Settings"
				Case "ger"
					ret = "Output to Text File - Requires Lower Internet Security Settings"
				Case "ita"
					ret = "Output to Text File - Requires Lower Internet Security Settings"
				Case "spa"
					ret = "Salida de informaci�n en forma texto - Requiere Ajustes m�s bajos de seguridad Internet "
			End Select
		Case "pgemstxt2"
			Select Case lang
				Case "eng"
					ret = "PdM Output to Text File - Requires Lower Internet Security Settings"
				Case "fre"
					ret = "PdM Output to Text File - Requires Lower Internet Security Settings"
				Case "ger"
					ret = "PdM Output to Text File - Requires Lower Internet Security Settings"
				Case "ita"
					ret = "PdM Output to Text File - Requires Lower Internet Security Settings"
				Case "spa"
					ret = "Salida de informaci�n PdM en forma texto - Requiere Ajustes m�s bajos de seguridad Internet "
			End Select
		Case "pgemstxthtm"
			Select Case lang
				Case "eng"
					ret = "Outputs Text version to HTML format - Use if you cannot open Text Output Above"
				Case "fre"
					ret = "Outputs Text version to HTML format - Use if you cannot open Text Output Above"
				Case "ger"
					ret = "Outputs Text version to HTML format - Use if you cannot open Text Output Above"
				Case "ita"
					ret = "Outputs Text version to HTML format - Use if you cannot open Text Output Above"
				Case "spa"
					ret = "Salida de Versi�n Texto al formato HTML - �sela si no puede abrir el Texto de Salida de Arriba"
			End Select
		Case "pgemstxthtm1"
			Select Case lang
				Case "eng"
					ret = "Outputs PdM Text version to HTML format - Use if you cannot open Text Output Above"
				Case "fre"
					ret = "Outputs PdM Text version to HTML format - Use if you cannot open Text Output Above"
				Case "ger"
					ret = "Outputs PdM Text version to HTML format - Use if you cannot open Text Output Above"
				Case "ita"
					ret = "Outputs PdM Text version to HTML format - Use if you cannot open Text Output Above"
				Case "spa"
					ret = "Salida de Versi�n Texto PdM al formato HTML - �sela si no puede abrir el Texto de Salida de Arriba"
			End Select
		Case "pgemstxthtmtpm"
			Select Case lang
				Case "eng"
					ret = "Outputs Text version to HTML format - Use if you cannot open Text Output Above"
				Case "fre"
					ret = "Outputs Text version to HTML format - Use if you cannot open Text Output Above"
				Case "ger"
					ret = "Outputs Text version to HTML format - Use if you cannot open Text Output Above"
				Case "ita"
					ret = "Outputs Text version to HTML format - Use if you cannot open Text Output Above"
				Case "spa"
					ret = "Salida de Versi�n Texto al formato HTML - �sela si no puede abrir el Texto de Salida de Arriba"
			End Select
		Case "pgemstxtwrd"
			Select Case lang
				Case "eng"
					ret = "Outputs Text version to Word in your Browser"
				Case "fre"
					ret = "Outputs Text version to Word in your Browser"
				Case "ger"
					ret = "Outputs Text version to Word in your Browser"
				Case "ita"
					ret = "Outputs Text version to Word in your Browser"
				Case "spa"
					ret = "Outputs Text version to Word in your Browser"
			End Select
		Case "pgemstxtwrd1"
			Select Case lang
				Case "eng"
					ret = "Outputs Text version to MS Word on your Desktop"
				Case "fre"
					ret = "Outputs Text version to MS Word on your Desktop"
				Case "ger"
					ret = "Outputs Text version to MS Word on your Desktop"
				Case "ita"
					ret = "Outputs Text version to MS Word on your Desktop"
				Case "spa"
					ret = "Outputs Text version to MS Word on your Desktop"
			End Select
		Case "pgemstxtwrd3"
			Select Case lang
				Case "eng"
					ret = "Outputs Text version to Word in your Browser"
				Case "fre"
					ret = "Outputs Text version to Word in your Browser"
				Case "ger"
					ret = "Outputs Text version to Word in your Browser"
				Case "ita"
					ret = "Outputs Text version to Word in your Browser"
				Case "spa"
					ret = "Outputs Text version to Word in your Browser"
			End Select
		Case "pgemstxtwrd4"
			Select Case lang
				Case "eng"
					ret = "Outputs Text version to MS Word on your Desktop"
				Case "fre"
					ret = "Outputs Text version to MS Word on your Desktop"
				Case "ger"
					ret = "Outputs Text version to MS Word on your Desktop"
				Case "ita"
					ret = "Outputs Text version to MS Word on your Desktop"
				Case "spa"
					ret = "Outputs Text version to MS Word on your Desktop"
			End Select
		End Select
	Return ret
End Function
Public Function getResourcesovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid306"
			Select Case lang
				Case "eng"
					ret = "Refresh Page"
				Case "fre"
					ret = "Refresh Page"
				Case "ger"
					ret = "Refresh Page"
				Case "ita"
					ret = "Refresh Page"
				Case "spa"
					ret = "Refrescar la P�gina"
			End Select
		Case "ovid307"
			Select Case lang
				Case "eng"
					ret = "Add a New Subject Matter"
				Case "fre"
					ret = "Add a New Subject Matter"
				Case "ger"
					ret = "Add a New Subject Matter"
				Case "ita"
					ret = "Add a New Subject Matter"
				Case "spa"
					ret = "Agregar una Nueva Materia"
			End Select
		End Select
	Return ret
End Function
Public Function getScheduledTasksovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Print Job Plan Filter Selections"
				Case "fre"
					ret = "Print Job Plan Filter Selections"
				Case "ger"
					ret = "Print Job Plan Filter Selections"
				Case "ita"
					ret = "Print Job Plan Filter Selections"
				Case "spa"
					ret = "Print Job Plan Filter Selections"
			End Select
		Case "imga"
			Select Case lang
				Case "eng"
					ret = "Print This Job Plan"
				Case "fre"
					ret = "Print This Job Plan"
				Case "ger"
					ret = "Print This Job Plan"
				Case "ita"
					ret = "Print This Job Plan"
				Case "spa"
					ret = "Print This Job Plan"
			End Select
		Case "imgi"
			Select Case lang
				Case "eng"
					ret = "Print This Job Plan"
				Case "fre"
					ret = "Print This Job Plan"
				Case "ger"
					ret = "Print This Job Plan"
				Case "ita"
					ret = "Print This Job Plan"
				Case "spa"
					ret = "Print This Job Plan"
			End Select
		End Select
	Return ret
End Function
Public Function getsfailovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddtosite"
			Select Case lang
				Case "eng"
					ret = "Choose Failure Modes for this Plant Site "
				Case "fre"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ger"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ita"
					ret = "Choose Failure Modes for this Plant Site "
				Case "spa"
					ret = "Seleccionar los Modos de Falla para este Sitio de la Planta"
			End Select
		End Select
	Return ret
End Function
Public Function getsiteassets2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid281"
			Select Case lang
				Case "eng"
					ret = "Use Max Search"
				Case "fre"
					ret = "Use Max Search"
				Case "ger"
					ret = "Use Max Search"
				Case "ita"
					ret = "Use Max Search"
				Case "spa"
					ret = "Use Max Search"
			End Select
		End Select
	Return ret
End Function
Public Function getsiteassets3ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid282"
			Select Case lang
				Case "eng"
					ret = "Use Max Search"
				Case "fre"
					ret = "Use Max Search"
				Case "ger"
					ret = "Use Max Search"
				Case "ita"
					ret = "Use Max Search"
				Case "spa"
					ret = "Use Max Search"
			End Select
		End Select
	Return ret
End Function
Public Function getstoreroomovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Save Stock Category and Default Bin Changes"
				Case "fre"
					ret = "Save Stock Category and Default Bin Changes"
				Case "ger"
					ret = "Save Stock Category and Default Bin Changes"
				Case "ita"
					ret = "Save Stock Category and Default Bin Changes"
				Case "spa"
					ret = "Save Stock Category and Default Bin Changes"
			End Select
		Case "imgsrch"
			Select Case lang
				Case "eng"
					ret = "Look up Inventory Items"
				Case "fre"
					ret = "Rechercher les articles d`inventaire"
				Case "ger"
					ret = "Inventarartikel-Suche"
				Case "ita"
					ret = "Ricerca articoli inventario"
				Case "spa"
					ret = "Buscar Art�culos en Inventario"
			End Select
		Case "ovid273"
			Select Case lang
				Case "eng"
					ret = "Clear Page"
				Case "fre"
					ret = "Effacer la page"
				Case "ger"
					ret = "Seite leeren"
				Case "ita"
					ret = "Eliminare pagina"
				Case "spa"
					ret = "Limpiar P�gina"
			End Select
		Case "ovid274"
			Select Case lang
				Case "eng"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
				Case "fre"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
				Case "ger"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
				Case "ita"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
				Case "spa"
					ret = "Switch Store Room for this Item if it is stored in multiple locations"
			End Select
		Case "ovid275"
			Select Case lang
				Case "eng"
					ret = "Adjust Current Balance (Physical Count Difference, Removed Expired Items) - Does not apply to Issues, Transfers, or Receiving Transactions"
				Case "fre"
					ret = "Adjust Current Balance (Physical Count Difference, Removed Expired Items) - Does not apply to Issues, Transfers, or Receiving Transactions"
				Case "ger"
					ret = "Adjust Current Balance (Physical Count Difference, Removed Expired Items) - Does not apply to Issues, Transfers, or Receiving Transactions"
				Case "ita"
					ret = "Adjust Current Balance (Physical Count Difference, Removed Expired Items) - Does not apply to Issues, Transfers, or Receiving Transactions"
				Case "spa"
					ret = "Adjust Current Balance (Physical Count Difference, Removed Expired Items) - Does not apply to Issues, Transfers, or Receiving Transactions"
			End Select
		End Select
	Return ret
End Function
Public Function gettablesetupovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid88"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid89"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function gettaskedit2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - All Tasks"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - toutes les t�ches"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � alle Aufgaben"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Tutte le attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Todas las Tareas"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - This Task"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - cette t�che"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � diese Aufgabe"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Questa attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Esta Tarea"
			End Select
		Case "imgrat"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Rationale for Task Changes"
				Case "fre"
					ret = "Ajouter/�diter un Expos� Raisonn� pour les Changements de T�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Begr�ndung f�r Aufgaben�nderungen"
				Case "ita"
					ret = "Aggiungi/modifica rationale per modifiche attivit�"
				Case "spa"
					ret = "Agregar/Editar Razonamiento para Cambios en Tarea "
			End Select
		Case "ovid57"
			Select Case lang
				Case "eng"
					ret = "Against what is the PM intended to protect?"
				Case "fre"
					ret = "Against what is the PM intended to protect?"
				Case "ger"
					ret = "Against what is the PM intended to protect?"
				Case "ita"
					ret = "Against what is the PM intended to protect?"
				Case "spa"
					ret = "�Contra qu� intenta el PM proteger?"
			End Select
		Case "ovid58"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid59"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid60"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid61"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid62"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "ovid63"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Notes for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les remarques pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Anmerkungen f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le note per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Notas para esta Tarea"
			End Select
		Case "ovid64"
			Select Case lang
				Case "eng"
					ret = "View Reports"
				Case "fre"
					ret = "View Reports"
				Case "ger"
					ret = "View Reports"
				Case "ita"
					ret = "View Reports"
				Case "spa"
					ret = "Ver Reportes"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function gettaskimagepmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Open TPM Operator View Task Review"
				Case "fre"
					ret = "Ouvrir la revue des t�ches de l`affichage de l`op�rateur de MPT"
				Case "ger"
					ret = "�ffne PM-Bediener-Anzeige Aufgaben�bersicht "
				Case "ita"
					ret = "Apri revisione attivit� visualizzazione operatore TPM"
				Case "spa"
					ret = "Vista Abierta de Revisi�n de Tareas TPM del Operador"
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgov"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Overlay Images to Current Image"
				Case "fre"
					ret = "Ajouter\ modifier les images de recouvrement � l`image actuelle"
				Case "ger"
					ret = "F�ge hinzu/bearbeite �berlagerungsbilder f�r aktuelle Abbildung"
				Case "ita"
					ret = "Aggiungi/modifica immagini in overlay nell`immagine attuale"
				Case "spa"
					ret = "Agregar\Editar Sobreponer Im�genes a la Imagen Actual "
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Details"
				Case "fre"
					ret = "Sauvegarder les d�tails de l`image"
				Case "ger"
					ret = "Bildeinzelheiten speichern"
				Case "ita"
					ret = "Salva dettagli immagine"
				Case "spa"
					ret = "Guardar los Detalles de la Imagen "
			End Select
		Case "ovid288"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		End Select
	Return ret
End Function
Public Function gettaskimagetpmovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Open TPM Operator View Task Review"
				Case "fre"
					ret = "Ouvrir la revue des t�ches de l`affichage de l`op�rateur de MPT"
				Case "ger"
					ret = "�ffne PM-Bediener-Anzeige Aufgaben�bersicht "
				Case "ita"
					ret = "Apri revisione attivit� visualizzazione operatore TPM"
				Case "spa"
					ret = "Vista Abierta de Revisi�n de Tareas TPM del Operador"
			End Select
		Case "imgdel"
			Select Case lang
				Case "eng"
					ret = "Delete This Image"
				Case "fre"
					ret = "Supprimer cette image"
				Case "ger"
					ret = "Diese Abbildung l�schen"
				Case "ita"
					ret = "Elimina questa immagine"
				Case "spa"
					ret = "Anular Esta Imagen"
			End Select
		Case "imgov"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Overlay Images to Current Image"
				Case "fre"
					ret = "Ajouter\ modifier les images de recouvrement � l`image actuelle"
				Case "ger"
					ret = "F�ge hinzu/bearbeite �berlagerungsbilder f�r aktuelle Abbildung"
				Case "ita"
					ret = "Aggiungi/modifica immagini in overlay nell`immagine attuale"
				Case "spa"
					ret = "Agregar\Editar Sobreponer Im�genes a la Imagen Actual "
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Details"
				Case "fre"
					ret = "Sauvegarder les d�tails de l`image"
				Case "ger"
					ret = "Bildeinzelheiten speichern"
				Case "ita"
					ret = "Salva dettagli immagine"
				Case "spa"
					ret = "Guardar los Detalles de la Imagen "
			End Select
		Case "ovid300"
			Select Case lang
				Case "eng"
					ret = "Add\Edit Images for this block"
				Case "fre"
					ret = "Ajouter/ modifier les images pour ce bloc"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Abbildungen f�r diesen Block"
				Case "ita"
					ret = "Aggiungi/elimina Immagini per questo blocco"
				Case "spa"
					ret = "Agregar/Editar Im�genes para este bloque"
			End Select
		End Select
	Return ret
End Function
Public Function gettaskimagetpmarchovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Open TPM Operator View Task Review"
				Case "fre"
					ret = "Ouvrir la revue des t�ches de l`affichage de l`op�rateur de MPT"
				Case "ger"
					ret = "�ffne PM-Bediener-Anzeige Aufgaben�bersicht "
				Case "ita"
					ret = "Apri revisione attivit� visualizzazione operatore TPM"
				Case "spa"
					ret = "Vista Abierta de Revisi�n de Tareas TPM del Operador"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmgetovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Start Over"
				Case "fre"
					ret = "Effacer l`�cran et red�marrer"
				Case "ger"
					ret = "Bildschirminhalt l�schen und neu starten"
				Case "ita"
					ret = "Cancellare finestra e riavviare"
				Case "spa"
					ret = "Limpiar Pantalla y Volver a Empezar"
			End Select
		Case "ovid290"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmgettasksovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Archive Current PM Revision and Create a New Revision"
				Case "fre"
					ret = "Archiver une r�vision actuelle de MP et cr�er une nouvelle r�vision"
				Case "ger"
					ret = "Archiviere die aktuelle PM-�berarbeitung und erstelle eine neue �berarbeitung"
				Case "ita"
					ret = "Archivia l`attuale revisione PM e crea una nuova revisione"
				Case "spa"
					ret = "Archivar la Revisi�n de PM Actual y Crear una Nueva Revisi�n"
			End Select
		Case "Img6"
			Select Case lang
				Case "eng"
					ret = "Archive Current PM Revision and Convert for Optimization"
				Case "fre"
					ret = "Archiver une r�vision actuelle de MP et la convertir pour optimisation"
				Case "ger"
					ret = "Archiviere aktuelle PM-�berarbeitung und konvertiere f�r Optimierung"
				Case "ita"
					ret = "Archivia l`attuale revisione PM e converti per l`ottimizzazione"
				Case "spa"
					ret = "Archivar la Revisi�n de PM Actual y Convertir para Optimizaci�n"
			End Select
		Case "imgaddeq"
			Select Case lang
				Case "eng"
					ret = "Add a New Equipment Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement d`�quipement"
				Case "ger"
					ret = "F�ge eine neue Ausr�stungs-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record Nuova apparecchiatura"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Equipo"
			End Select
		Case "imgaddfu"
			Select Case lang
				Case "eng"
					ret = "Add a New Function Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement de fonction"
				Case "ger"
					ret = "F�ge eine neue Funktions-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record  Nuova funzione"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Funci�n"
			End Select
		Case "imgcopyeq"
			Select Case lang
				Case "eng"
					ret = "Copy an Equipment Record"
				Case "fre"
					ret = "Copier un enregistrement d`�quipement"
				Case "ger"
					ret = "Kopiere eine Ausr�stungs-Eintragung"
				Case "ita"
					ret = "Copia una voce Apparecchiatura"
				Case "spa"
					ret = "Copiar un Registro de Equipo"
			End Select
		Case "imgcopyfu"
			Select Case lang
				Case "eng"
					ret = "Copy a Function Record"
				Case "fre"
					ret = "Copier un enregistrement de fonction"
				Case "ger"
					ret = "Kopiere eine Funktions-Eintragung"
				Case "ita"
					ret = "Copia una voce Funzione"
				Case "spa"
					ret = "Copiar un Registro de Funci�n"
			End Select
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Start Over"
				Case "fre"
					ret = "Effacer l`�cran et red�marrer"
				Case "ger"
					ret = "Bildschirminhalt l�schen und neu starten"
				Case "ita"
					ret = "Cancellare finestra e riavviare"
				Case "spa"
					ret = "Limpiar Pantalla y Volver a Empezar"
			End Select
		Case "ovid180"
			Select Case lang
				Case "eng"
					ret = "Use Max Search"
				Case "fre"
					ret = "Use Max Search"
				Case "ger"
					ret = "Use Max Search"
				Case "ita"
					ret = "Use Max Search"
				Case "spa"
					ret = "Use Max Search"
			End Select
		Case "ovid181"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmgridovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "iwa"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "iwi"
			Select Case lang
				Case "eng"
					ret = "Status Check Icon"
				Case "fre"
					ret = "Status Check Icon"
				Case "ger"
					ret = "Status Check Icon"
				Case "ita"
					ret = "Status Check Icon"
				Case "spa"
					ret = "Status Check Icon"
			End Select
		Case "ovid291"
			Select Case lang
				Case "eng"
					ret = "Used to indicate Shift designation"
				Case "fre"
					ret = "Used to indicate Shift designation"
				Case "ger"
					ret = "Used to indicate Shift designation"
				Case "ita"
					ret = "Used to indicate Shift designation"
				Case "spa"
					ret = "Used to indicate Shift designation"
			End Select
		Case "ovid292"
			Select Case lang
				Case "eng"
					ret = "Used to indicate Shift designation"
				Case "fre"
					ret = "Used to indicate Shift designation"
				Case "ger"
					ret = "Used to indicate Shift designation"
				Case "ita"
					ret = "Used to indicate Shift designation"
				Case "spa"
					ret = "Used to indicate Shift designation"
			End Select
		Case "ovid293"
			Select Case lang
				Case "eng"
					ret = "Used to indicate Shift designation"
				Case "fre"
					ret = "Used to indicate Shift designation"
				Case "ger"
					ret = "Used to indicate Shift designation"
				Case "ita"
					ret = "Used to indicate Shift designation"
				Case "spa"
					ret = "Used to indicate Shift designation"
			End Select
		Case "ovid294"
			Select Case lang
				Case "eng"
					ret = "Used to indicate Shift designation"
				Case "fre"
					ret = "Used to indicate Shift designation"
				Case "ger"
					ret = "Used to indicate Shift designation"
				Case "ita"
					ret = "Used to indicate Shift designation"
				Case "spa"
					ret = "Used to indicate Shift designation"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmmeasovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid295"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid296"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmoprunovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img1"
			Select Case lang
				Case "eng"
					ret = "Check Failure Modes for this Task"
				Case "fre"
					ret = "Check Failure Modes for this Task"
				Case "ger"
					ret = "Check Failure Modes for this Task"
				Case "ita"
					ret = "Check Failure Modes for this Task"
				Case "spa"
					ret = "Check Failure Modes for this Task"
			End Select
		Case "imgmeas"
			Select Case lang
				Case "eng"
					ret = "Record Measurements for this Task"
				Case "fre"
					ret = "Record Measurements for this Task"
				Case "ger"
					ret = "Record Measurements for this Task"
				Case "ita"
					ret = "Record Measurements for this Task"
				Case "spa"
					ret = "Record Measurements for this Task"
			End Select
		Case "imgsub"
			Select Case lang
				Case "eng"
					ret = "View Sub-Tasks for this Task"
				Case "fre"
					ret = "View Sub-Tasks for this Task"
				Case "ger"
					ret = "View Sub-Tasks for this Task"
				Case "ita"
					ret = "View Sub-Tasks for this Task"
				Case "spa"
					ret = "View Sub-Tasks for this Task"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmoptgetmainovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Archive Current PM Revision and Create a New Revision"
				Case "fre"
					ret = "Archiver une r�vision actuelle de MP et cr�er une nouvelle r�vision"
				Case "ger"
					ret = "Archiviere die aktuelle PM-�berarbeitung und erstelle eine neue �berarbeitung"
				Case "ita"
					ret = "Archivia l`attuale revisione PM e crea una nuova revisione"
				Case "spa"
					ret = "Archivar la Revisi�n de PM Actual y Crear una Nueva Revisi�n"
			End Select
		Case "Img6"
			Select Case lang
				Case "eng"
					ret = "Archive Current PM Revision and Convert for Optimization"
				Case "fre"
					ret = "Archiver une r�vision actuelle de MP et la convertir pour optimisation"
				Case "ger"
					ret = "Archiviere aktuelle PM-�berarbeitung und konvertiere f�r Optimierung"
				Case "ita"
					ret = "Archivia l`attuale revisione PM e converti per l`ottimizzazione"
				Case "spa"
					ret = "Archivar la Revisi�n de PM Actual y Convertir para Optimizaci�n"
			End Select
		Case "imgaddeq"
			Select Case lang
				Case "eng"
					ret = "Add a New Equipment Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement d`�quipement"
				Case "ger"
					ret = "F�ge eine neue Ausr�stungs-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record Nuova apparecchiatura"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Equipo"
			End Select
		Case "imgaddfu"
			Select Case lang
				Case "eng"
					ret = "Add a New Function Record"
				Case "fre"
					ret = "Ajouter un nouvel enregistrement de fonction"
				Case "ger"
					ret = "F�ge eine neue Funktions-Eintragung hinzu"
				Case "ita"
					ret = "Aggiungi il record  Nuova funzione"
				Case "spa"
					ret = "Agregar un Nuevo Registro de Funci�n"
			End Select
		Case "imgbulk"
			Select Case lang
				Case "eng"
					ret = "Choose a Bulk Loaded Procedure to Optimize"
				Case "fre"
					ret = "S�lectionner une proc�dure de chargement en vrac � optimiser"
				Case "ger"
					ret = "W�hle ein 08/15-Verfahren zur Optimierung"
				Case "ita"
					ret = "Scegliere un procedimento di caricamento di massa per ottimizzare"
				Case "spa"
					ret = "***Escoja un Procedimiento Cargado A granel para Perfeccionar"
			End Select
		Case "imgbulkg"
			Select Case lang
				Case "eng"
					ret = "Choose a Bulk Loaded Procedure from the Document Library to Optimize"
				Case "fre"
					ret = "S�lectionner une proc�dure de chargement en vrac � partir de la biblioth�que de documents � optimiser"
				Case "ger"
					ret = "W�hle ein 08/15-Verfahren aus der Dokumenten-Bibliothek zur Optimierung"
				Case "ita"
					ret = "Scegliere un procedimento di caricamento di massa dalla galleria documenti per ottimizzare"
				Case "spa"
					ret = "***Escoja un Procedimiento Cargado en  de la Biblioteca del Documento para Optimizar"
			End Select
		Case "imgcopyeq"
			Select Case lang
				Case "eng"
					ret = "Copy an Equipment Record"
				Case "fre"
					ret = "Copier un enregistrement d`�quipement"
				Case "ger"
					ret = "Kopiere eine Ausr�stungs-Eintragung"
				Case "ita"
					ret = "Copia una voce Apparecchiatura"
				Case "spa"
					ret = "Copiar un Registro de Equipo"
			End Select
		Case "imgcopyfu"
			Select Case lang
				Case "eng"
					ret = "Copy a Function Record"
				Case "fre"
					ret = "Copier un enregistrement de fonction"
				Case "ger"
					ret = "Kopiere eine Funktions-Eintragung"
				Case "ita"
					ret = "Copia una voce Funzione"
				Case "spa"
					ret = "Copiar un Registro de Funci�n"
			End Select
		Case "imgdrag"
			Select Case lang
				Case "eng"
					ret = "Drag and Drop Tasks from Procedures"
				Case "fre"
					ret = "Glisser-d�poser les t�ches � partir des proc�dures"
				Case "ger"
					ret = "Drag and Drop Aufgaben von Verfahren"
				Case "ita"
					ret = "Trascina e rilascia Attivit� da Procedure"
				Case "spa"
					ret = "Arrastrar y soltar Tareas de los Procedimientos "
			End Select
		Case "imgproc"
			Select Case lang
				Case "eng"
					ret = "Add Procedures to Optimize"
				Case "fre"
					ret = "Add Procedures to Optimize"
				Case "ger"
					ret = "Add Procedures to Optimize"
				Case "ita"
					ret = "Add Procedures to Optimize"
				Case "spa"
					ret = "Agregar Procedimientos para Optimizar"
			End Select
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Start Over"
				Case "fre"
					ret = "Effacer l`�cran et red�marrer"
				Case "ger"
					ret = "Bildschirminhalt l�schen und neu starten"
				Case "ita"
					ret = "Cancellare finestra e riavviare"
				Case "spa"
					ret = "Limpiar Pantalla y Volver a Empezar"
			End Select
		Case "ovid141"
			Select Case lang
				Case "eng"
					ret = "Use Max Search"
				Case "fre"
					ret = "Use Max Search"
				Case "ger"
					ret = "Use Max Search"
				Case "ita"
					ret = "Use Max Search"
				Case "spa"
					ret = "Use Max Search"
			End Select
		Case "ovid142"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmopttasksovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddcomp"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Components for Selected Function"
				Case "fre"
					ret = "Ajouter/ modifier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Aggiungi/modifica i componenti per la funzione selezionata"
				Case "spa"
					ret = "Agregar/Editar Componentes para la Funci�n Seleccionada"
			End Select
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "ggrid"
			Select Case lang
				Case "eng"
					ret = "Review Changes in Grid View"
				Case "fre"
					ret = "Review Changes in Grid View"
				Case "ger"
					ret = "Review Changes in Grid View"
				Case "ita"
					ret = "Review Changes in Grid View"
				Case "spa"
					ret = "Revisar Cambios en Vista de la Parrilla"
			End Select
		Case "Img3"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - All Tasks"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - toutes les t�ches"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � alle Aufgaben"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Tutte le attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Todas las Tareas"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - This Task"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - cette t�che"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � diese Aufgabe"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Questa attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Esta Tarea"
			End Select
		Case "imgcopycomp"
			Select Case lang
				Case "eng"
					ret = "Copy Components for Selected Function"
				Case "fre"
					ret = "Copier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "Kopiere Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Copia i componenti per la funzione scelta"
				Case "spa"
					ret = "Copiar Componentes para la Funci�n Seleccionada"
			End Select
		Case "imgdeltask"
			Select Case lang
				Case "eng"
					ret = "Delete This Task"
				Case "fre"
					ret = "Supprimer cette t�che"
				Case "ger"
					ret = "Diese Aufgabe l�schen"
				Case "ita"
					ret = "Elimina questa attivit�"
				Case "spa"
					ret = "Anular esta Tarea"
			End Select
		Case "imgrat"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Rationale for Task Changes"
				Case "fre"
					ret = "Ajouter/�diter un Expos� Raisonn� pour les Changements de T�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Begr�ndung f�r Aufgaben�nderungen"
				Case "ita"
					ret = "Aggiungi/modifica rationale per modifiche attivit�"
				Case "spa"
					ret = "Agregar/Editar Razonamiento para Cambios en Tarea "
			End Select
		Case "ovid143"
			Select Case lang
				Case "eng"
					ret = "Against what is the TPM intended to protect?"
				Case "fre"
					ret = "Against what is the TPM intended to protect?"
				Case "ger"
					ret = "Against what is the TPM intended to protect?"
				Case "ita"
					ret = "Against what is the TPM intended to protect?"
				Case "spa"
					ret = "Against what is the TPM intended to protect?"
			End Select
		Case "ovid144"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid145"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid146"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid147"
			Select Case lang
				Case "eng"
					ret = "Use to indicate shift - Auto Filled for Day Enries"
				Case "fre"
					ret = "Utiliser pour indiquer le poste - Rempli automatiquement pour les entr�es de jour"
				Case "ger"
					ret = "Um Schicht anzuzeigen � Selbstvervollst�ndigung f�r Tag-Eingaben"
				Case "ita"
					ret = "Utilizza per indicare il turno - Riempimento automatico per le voci di giorni"
				Case "spa"
					ret = "Use para indicar el turno - Auto-Llenado para entradas del D�a"
			End Select
		Case "ovid148"
			Select Case lang
				Case "eng"
					ret = "Selects All Days for Selected Shift"
				Case "fre"
					ret = "S�lectionne tous les jours pour le poste s�lectionn�"
				Case "ger"
					ret = "W�hlt alle Tage f�r gew�hlte Schicht "
				Case "ita"
					ret = "Seleziona tutti i giorni per il turno selezionato"
				Case "spa"
					ret = "Selecciona Todos los D�as para el Turno Seleccionado "
			End Select
		Case "ovid149"
			Select Case lang
				Case "eng"
					ret = "Use to indicate shift - Auto Filled for Day Enries"
				Case "fre"
					ret = "Utiliser pour indiquer le poste - Rempli automatiquement pour les entr�es de jour"
				Case "ger"
					ret = "Um Schicht anzuzeigen � Selbstvervollst�ndigung f�r Tag-Eingaben"
				Case "ita"
					ret = "Utilizza per indicare il turno - Riempimento automatico per le voci di giorni"
				Case "spa"
					ret = "Use para indicar el turno - Auto-Llenado para entradas del D�a"
			End Select
		Case "ovid150"
			Select Case lang
				Case "eng"
					ret = "Selects All Days for Selected Shift"
				Case "fre"
					ret = "S�lectionne tous les jours pour le poste s�lectionn�"
				Case "ger"
					ret = "W�hlt alle Tage f�r gew�hlte Schicht "
				Case "ita"
					ret = "Seleziona tutti i giorni per il turno selezionato"
				Case "spa"
					ret = "Selecciona Todos los D�as para el Turno Seleccionado "
			End Select
		Case "ovid151"
			Select Case lang
				Case "eng"
					ret = "Choose Equipment Spare Parts for this Task"
				Case "fre"
					ret = "S�lectionner les pi�ces de rechange de l`�quipement pour cette t�che "
				Case "ger"
					ret = "W�hle die Ausr�stungs-Ersatzteile f�r diese Aufgabe"
				Case "ita"
					ret = "Scegliere le parti di ricambio dell`apparecchiatura per questa attivit�"
				Case "spa"
					ret = "Escoja las Partes de Repuesto del Equipo para esta Tarea"
			End Select
		Case "ovid152"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid153"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "ovid154"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Notes for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les remarques pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Anmerkungen f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le note per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Notas para esta Tarea"
			End Select
		Case "ovid155"
			Select Case lang
				Case "eng"
					ret = "View Reports"
				Case "fre"
					ret = "View Reports"
				Case "ger"
					ret = "View Reports"
				Case "ita"
					ret = "View Reports"
				Case "spa"
					ret = "Ver Reportes"
			End Select
		Case "ovid156"
			Select Case lang
				Case "eng"
					ret = "Go to the Centralized PM Library"
				Case "fre"
					ret = "Go to the Centralized PM Library"
				Case "ger"
					ret = "Go to the Centralized PM Library"
				Case "ita"
					ret = "Go to the Centralized PM Library"
				Case "spa"
					ret = "Ir a la Biblioteca de PM Centralizada"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmopttasksmainovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imghide"
			Select Case lang
				Case "eng"
					ret = "Expand/Shrink Procedure to Optimize"
				Case "fre"
					ret = "D�velopper/ iconiser la proc�dure � optimiser"
				Case "ger"
					ret = "Vergr��ere/verkleinere Verfahren zur Optimierung"
				Case "ita"
					ret = "Espandi/riduci procedura per ottimizzare"
				Case "spa"
					ret = "Procedimiento para Optimizar Expandir/Reducir"
			End Select
		Case "imgnav"
			Select Case lang
				Case "eng"
					ret = "Show/Hide Asset Information"
				Case "fre"
					ret = "Afficher/ masquer les informations d`actif"
				Case "ger"
					ret = "Anlagen-Information anzeigen/ausblenden"
				Case "ita"
					ret = "Mostra/nascondi informazioni prodotto"
				Case "spa"
					ret = "Mostrar/Ocultar Informaci�n del Activo "
			End Select
		End Select
	Return ret
End Function
Public Function gettpmsizeovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsub"
			Select Case lang
				Case "eng"
					ret = "View Sub-Tasks for this Task"
				Case "fre"
					ret = "View Sub-Tasks for this Task"
				Case "ger"
					ret = "View Sub-Tasks for this Task"
				Case "ita"
					ret = "View Sub-Tasks for this Task"
				Case "spa"
					ret = "View Sub-Tasks for this Task"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmtaskovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgcomptask"
			Select Case lang
				Case "eng"
					ret = "Complete This Task"
				Case "fre"
					ret = "Terminer cette t�che "
				Case "ger"
					ret = "Schlie�e diese Aufgabe ab"
				Case "ita"
					ret = "Completa questa attivit� "
				Case "spa"
					ret = "Completar Esta Tarea "
			End Select
		Case "imgcomptpm"
			Select Case lang
				Case "eng"
					ret = "Complete This TPM"
				Case "fre"
					ret = "Terminer cette MPT "
				Case "ger"
					ret = "Schlie�e dieses PM ab"
				Case "ita"
					ret = "Completa questo TPM"
				Case "spa"
					ret = "Completar Este TPM "
			End Select
		Case "imgfail"
			Select Case lang
				Case "eng"
					ret = "This Task Has Failure Modes To Be Addressed"
				Case "fre"
					ret = "Cette t�che a des modes de d�faillance � adresser"
				Case "ger"
					ret = "Diese Aufgabe besitzt zu adressierende Ausfallarten"
				Case "ita"
					ret = "In questa attivit� sono presenti modalit� di errore da indirizzare"
				Case "spa"
					ret = "Esta Tarea Tiene Modos de Falla por ser Atendidos"
			End Select
		Case "imglube"
			Select Case lang
				Case "eng"
					ret = "This Task Requires Lubricants"
				Case "fre"
					ret = "Cette t�che n�cessite des lubrifiants"
				Case "ger"
					ret = "Diese Aufgabe erfordert Schmiermittel"
				Case "ita"
					ret = "Questa attivit� richiede Lubrificanti"
				Case "spa"
					ret = "Esta Tarea Requiere Lubricantes "
			End Select
		Case "imgmag"
			Select Case lang
				Case "eng"
					ret = "Full View"
				Case "fre"
					ret = "Affichage complet"
				Case "ger"
					ret = "Vollanzeige"
				Case "ita"
					ret = "Visualizzazione completa"
				Case "spa"
					ret = "Vista Completa "
			End Select
		Case "imgmeas"
			Select Case lang
				Case "eng"
					ret = "This Task Requires Measurements"
				Case "fre"
					ret = "Cette t�che n�cessite des mesures"
				Case "ger"
					ret = "Diese Aufgabe erfordert Messungen"
				Case "ita"
					ret = "Questa attivit� richiede Misurazioni"
				Case "spa"
					ret = "Esta Tarea Requiere Mediciones "
			End Select
		Case "imgpart"
			Select Case lang
				Case "eng"
					ret = "This Task Requires Parts"
				Case "fre"
					ret = "Cette t�che n�cessite des pi�ces"
				Case "ger"
					ret = "Diese Aufgabe erfordert Teile"
				Case "ita"
					ret = "Questa attivit� richiede Parti"
				Case "spa"
					ret = "Esta Tarea Requiere Partes "
			End Select
		Case "imgsub"
			Select Case lang
				Case "eng"
					ret = "View Sub-Tasks for this Task"
				Case "fre"
					ret = "View Sub-Tasks for this Task"
				Case "ger"
					ret = "View Sub-Tasks for this Task"
				Case "ita"
					ret = "View Sub-Tasks for this Task"
				Case "spa"
					ret = "View Sub-Tasks for this Task"
			End Select
		Case "imgtool"
			Select Case lang
				Case "eng"
					ret = "This Task Requires Tools"
				Case "fre"
					ret = "Cette t�che n�cessite des outils"
				Case "ger"
					ret = "Diese Aufgabe erfordert Werkzeuge"
				Case "ita"
					ret = "Questa attivit� richiede Strumenti"
				Case "spa"
					ret = "Esta Tarea Requiere Herramientas "
			End Select
		Case "ovid297"
			Select Case lang
				Case "eng"
					ret = "Generate Work Request"
				Case "fre"
					ret = "G�n�rer une demande de travaux"
				Case "ger"
					ret = "Arbeitsanfrage erstellen"
				Case "ita"
					ret = "Creare richiesta lavoro"
				Case "spa"
					ret = "Generar Requisici�n de Trabajo "
			End Select
		End Select
	Return ret
End Function
Public Function gettpmtasksovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddcomp"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Components for Selected Function"
				Case "fre"
					ret = "Ajouter/ modifier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Aggiungi/modifica i componenti per la funzione selezionata"
				Case "spa"
					ret = "Agregar/Editar Componentes para la Funci�n Seleccionada"
			End Select
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "fre"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ger"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "ita"
					ret = "Add a New Failure Mode to the Component Selected Above"
				Case "spa"
					ret = "Add a New Failure Mode to the Component Selected Above"
			End Select
		Case "btnpfint"
			Select Case lang
				Case "eng"
					ret = "Estimate Frequency using the PF Interval"
				Case "fre"
					ret = "Estimer la fr�quence utilisant l`intervalle PF"
				Case "ger"
					ret = "Gesch�tzte Frequenz bei Verwendung von PF-Intervall"
				Case "ita"
					ret = "Valutare la frequenza utilizzando l`intervallo PF"
				Case "spa"
					ret = "Estime Frecuencia usando el Intervalo de PF"
			End Select
		Case "ggrid"
			Select Case lang
				Case "eng"
					ret = "Review Changes in Grid View"
				Case "fre"
					ret = "Review Changes in Grid View"
				Case "ger"
					ret = "Review Changes in Grid View"
				Case "ita"
					ret = "Review Changes in Grid View"
				Case "spa"
					ret = "Revisar Cambios en Vista de la Parrilla"
			End Select
		Case "img1"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "Img4"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - All Tasks"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - toutes les t�ches"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � alle Aufgaben"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Tutte le attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Todas las Tareas"
			End Select
		Case "Img5"
			Select Case lang
				Case "eng"
					ret = "Edit Pass/Fail Adjustment Levels - This Task"
				Case "fre"
					ret = "Modifier les niveaux de r�glage r�ussite/ �chec - cette t�che"
				Case "ger"
					ret = "Bearbeite Freigabe/Sperre Verstellungsh�hen � diese Aufgabe"
				Case "ita"
					ret = "Modifica i livelli di regolazione passa-non passa - Questa attivit�"
				Case "spa"
					ret = "Revise los Niveles de Ajuste de Pasa/Falla  - Esta Tarea"
			End Select
		Case "imgcopycomp"
			Select Case lang
				Case "eng"
					ret = "Copy Components for Selected Function"
				Case "fre"
					ret = "Copier les composants pour la fonction s�lectionn�e"
				Case "ger"
					ret = "Kopiere Bauteile f�r gew�hlte Funktion"
				Case "ita"
					ret = "Copia i componenti per la funzione scelta"
				Case "spa"
					ret = "Copiar Componentes para la Funci�n Seleccionada"
			End Select
		Case "ovid182"
			Select Case lang
				Case "eng"
					ret = "Against what is the TPM intended to protect?"
				Case "fre"
					ret = "Against what is the TPM intended to protect?"
				Case "ger"
					ret = "Against what is the TPM intended to protect?"
				Case "ita"
					ret = "Against what is the TPM intended to protect?"
				Case "spa"
					ret = "Against what is the TPM intended to protect?"
			End Select
		Case "ovid183"
			Select Case lang
				Case "eng"
					ret = "What are you going to do?"
				Case "fre"
					ret = "What are you going to do?"
				Case "ger"
					ret = "What are you going to do?"
				Case "ita"
					ret = "What are you going to do?"
				Case "spa"
					ret = "�Qu� va a hacer?"
			End Select
		Case "ovid184"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this Task"
				Case "fre"
					ret = "Add/Edit Measurements for this Task"
				Case "ger"
					ret = "Add/Edit Measurements for this Task"
				Case "ita"
					ret = "Add/Edit Measurements for this Task"
				Case "spa"
					ret = "Add/Edit Measurements for this Task"
			End Select
		Case "ovid185"
			Select Case lang
				Case "eng"
					ret = "How are you going to do it?"
				Case "fre"
					ret = "How are you going to do it?"
				Case "ger"
					ret = "How are you going to do it?"
				Case "ita"
					ret = "How are you going to do it?"
				Case "spa"
					ret = "�C�mo va usted a hacerlo?"
			End Select
		Case "ovid186"
			Select Case lang
				Case "eng"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "fre"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "ger"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "ita"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
				Case "spa"
					ret = "Use to indicate shift - Clears and Disables any Day Enries"
			End Select
		Case "ovid187"
			Select Case lang
				Case "eng"
					ret = "Selects All Days for Selected Shift"
				Case "fre"
					ret = "S�lectionne tous les jours pour le poste s�lectionn�"
				Case "ger"
					ret = "W�hlt alle Tage f�r gew�hlte Schicht "
				Case "ita"
					ret = "Seleziona tutti i giorni per il turno selezionato"
				Case "spa"
					ret = "Selecciona Todos los D�as para el Turno Seleccionado "
			End Select
		Case "ovid188"
			Select Case lang
				Case "eng"
					ret = "Choose Equipment Spare Parts for this Task"
				Case "fre"
					ret = "S�lectionner les pi�ces de rechange de l`�quipement pour cette t�che "
				Case "ger"
					ret = "W�hle die Ausr�stungs-Ersatzteile f�r diese Aufgabe"
				Case "ita"
					ret = "Scegliere le parti di ricambio dell`apparecchiatura per questa attivit�"
				Case "spa"
					ret = "Escoja las Partes de Repuesto del Equipo para esta Tarea"
			End Select
		Case "ovid189"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les outils pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Werkzeuge f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica gli strumenti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Herramientas para esta Tarea"
			End Select
		Case "ovid190"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les lubrifiants pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Schmiermittel f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica i lubrificanti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Lubricantes para esta Tarea"
			End Select
		Case "ovid191"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Notes for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les remarques pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Anmerkungen f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le note per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Notas para esta Tarea"
			End Select
		Case "ovid192"
			Select Case lang
				Case "eng"
					ret = "View Equipment, Function &amp; Component Images for this Task"
				Case "fre"
					ret = "View Equipment, Function &amp; Component Images for this Task"
				Case "ger"
					ret = "View Equipment, Function &amp; Component Images for this Task"
				Case "ita"
					ret = "View Equipment, Function &amp; Component Images for this Task"
				Case "spa"
					ret = "View Equipment, Function &amp; Component Images for this Task"
			End Select
		Case "ovid193"
			Select Case lang
				Case "eng"
					ret = "View Reports"
				Case "fre"
					ret = "View Reports"
				Case "ger"
					ret = "View Reports"
				Case "ita"
					ret = "View Reports"
				Case "spa"
					ret = "Ver Reportes"
			End Select
		Case "sgrid"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Sub Tasks"
				Case "fre"
					ret = "Ajouter/ modifier les sous-t�ches"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Unteraufgaben"
				Case "ita"
					ret = "Aggiungi/modifica le sottoattivit�"
				Case "spa"
					ret = "Agregar/Editar Sub-Tareas"
			End Select
		End Select
	Return ret
End Function
Public Function gettpmuploadimageovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgref"
			Select Case lang
				Case "eng"
					ret = "Refresh Details for New Image Entry"
				Case "fre"
					ret = "Actualiser les d�tails pour la nouvelle entr�e d`image"
				Case "ger"
					ret = "Einzelheiten f�r neue Bildeingabe auffrischen"
				Case "ita"
					ret = "Aggiorna dettagli per la voce Nuova immagine"
				Case "spa"
					ret = "Refrescar Detalles para Entrada de Nueva Imagen "
			End Select
		Case "imgsavdet"
			Select Case lang
				Case "eng"
					ret = "Save Image Details"
				Case "fre"
					ret = "Sauvegarder les d�tails de l`image"
				Case "ger"
					ret = "Bildeinzelheiten speichern"
				Case "ita"
					ret = "Salva dettagli immagine"
				Case "spa"
					ret = "Guardar los Detalles de la Imagen "
			End Select
		End Select
	Return ret
End Function
Public Function gettpmwrovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "Img2"
			Select Case lang
				Case "eng"
					ret = "View Failure Mode History"
				Case "fre"
					ret = "View Failure Mode History"
				Case "ger"
					ret = "View Failure Mode History"
				Case "ita"
					ret = "View Failure Mode History"
				Case "spa"
					ret = "View Failure Mode History"
			End Select
		Case "imgi2"
			Select Case lang
				Case "eng"
					ret = "Print This Work Order"
				Case "fre"
					ret = "Imprimer cet ordre de travail"
				Case "ger"
					ret = "Drucken dieses Arbeitsauftrags"
				Case "ita"
					ret = "Stampa questo ordine di lavoro"
				Case "spa"
					ret = "Print This Work Order"
			End Select
		Case "iwoadd"
			Select Case lang
				Case "eng"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "fre"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "ger"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "ita"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
				Case "spa"
					ret = "Create a Corrective or Emergency Maintenance Work Order"
			End Select
		Case "ovid298"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid299"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		End Select
	Return ret
End Function
Public Function gettransassignovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid277"
			Select Case lang
				Case "eng"
					ret = "Choose Work Request Skills"
				Case "fre"
					ret = "Choose Work Request Skills"
				Case "ger"
					ret = "Choose Work Request Skills"
				Case "ita"
					ret = "Choose Work Request Skills"
				Case "spa"
					ret = "Choose Work Request Skills"
			End Select
		End Select
	Return ret
End Function
Public Function getUserAdminovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add Muliple Site Access to Selected User"
				Case "fre"
					ret = "Add Muliple Site Access to Selected User"
				Case "ger"
					ret = "Add Muliple Site Access to Selected User"
				Case "ita"
					ret = "Add Muliple Site Access to Selected User"
				Case "spa"
					ret = "Agregar Acceso a Sitios M�ltiples al Usuario Seleccionado"
			End Select
		Case "btnaddnewsites"
			Select Case lang
				Case "eng"
					ret = "Add Muliple Site Access to Selected User"
				Case "fre"
					ret = "Add Muliple Site Access to Selected User"
				Case "ger"
					ret = "Add Muliple Site Access to Selected User"
				Case "ita"
					ret = "Add Muliple Site Access to Selected User"
				Case "spa"
					ret = "Agregar Acceso a Sitios M�ltiples al Usuario Seleccionado"
			End Select
		End Select
	Return ret
End Function
Public Function getuseradmin2ovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddnewfail"
			Select Case lang
				Case "eng"
					ret = "Add Muliple Site Access to Selected User"
				Case "fre"
					ret = "Add Muliple Site Access to Selected User"
				Case "ger"
					ret = "Add Muliple Site Access to Selected User"
				Case "ita"
					ret = "Add Muliple Site Access to Selected User"
				Case "spa"
					ret = "Agregar Acceso a Sitios M�ltiples al Usuario Seleccionado"
			End Select
		End Select
	Return ret
End Function
Public Function getwhereusedovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsrch"
			Select Case lang
				Case "eng"
					ret = "Look up Inventory Items"
				Case "fre"
					ret = "Rechercher les articles d`inventaire"
				Case "ger"
					ret = "Inventarartikel-Suche"
				Case "ita"
					ret = "Ricerca articoli inventario"
				Case "spa"
					ret = "Buscar Art�culos en Inventario"
			End Select
		Case "ovid276"
			Select Case lang
				Case "eng"
					ret = "Clear Page"
				Case "fre"
					ret = "Effacer la page"
				Case "ger"
					ret = "Seite leeren"
				Case "ita"
					ret = "Eliminare pagina"
				Case "spa"
					ret = "Limpiar P�gina"
			End Select
		End Select
	Return ret
End Function
Public Function getwoaddovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgcomp"
			Select Case lang
				Case "eng"
					ret = "Complete this Work Order"
				Case "fre"
					ret = "Complete this Work Order"
				Case "ger"
					ret = "Complete this Work Order"
				Case "ita"
					ret = "Complete this Work Order"
				Case "spa"
					ret = "Complete this Work Order"
			End Select
		Case "ovid194"
			Select Case lang
				Case "eng"
					ret = "Create New Work Order With Auto Number"
				Case "fre"
					ret = "Create New Work Order With Auto Number"
				Case "ger"
					ret = "Create New Work Order With Auto Number"
				Case "ita"
					ret = "Create New Work Order With Auto Number"
				Case "spa"
					ret = "Create New Work Order With Auto Number"
			End Select
		Case "ovid195"
			Select Case lang
				Case "eng"
					ret = "Duplicate This Work Order"
				Case "fre"
					ret = "Duplicate This Work Order"
				Case "ger"
					ret = "Duplicate This Work Order"
				Case "ita"
					ret = "Duplicate This Work Order"
				Case "spa"
					ret = "Duplicate This Work Order"
			End Select
		Case "ovid196"
			Select Case lang
				Case "eng"
					ret = "Refresh Screen to Create New Work Order"
				Case "fre"
					ret = "Refresh Screen to Create New Work Order"
				Case "ger"
					ret = "Refresh Screen to Create New Work Order"
				Case "ita"
					ret = "Refresh Screen to Create New Work Order"
				Case "spa"
					ret = "Refresh Screen to Create New Work Order"
			End Select
		End Select
	Return ret
End Function
Public Function getwoaltmanovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsav"
			Select Case lang
				Case "eng"
					ret = "Save Date Revision Changes"
				Case "fre"
					ret = "Save Date Revision Changes"
				Case "ger"
					ret = "Save Date Revision Changes"
				Case "ita"
					ret = "Save Date Revision Changes"
				Case "spa"
					ret = "Save Date Revision Changes"
			End Select
		Case "ovid90"
			Select Case lang
				Case "eng"
					ret = "Get a New Start Date"
				Case "fre"
					ret = "Get a New Start Date"
				Case "ger"
					ret = "Get a New Start Date"
				Case "ita"
					ret = "Get a New Start Date"
				Case "spa"
					ret = "Get a New Start Date"
			End Select
		Case "ovid91"
			Select Case lang
				Case "eng"
					ret = "Get a New Complete Date"
				Case "fre"
					ret = "Get a New Complete Date"
				Case "ger"
					ret = "Get a New Complete Date"
				Case "ita"
					ret = "Get a New Complete Date"
				Case "spa"
					ret = "Get a New Complete Date"
			End Select
		Case "ovid92"
			Select Case lang
				Case "eng"
					ret = "Return Without Saving Changes"
				Case "fre"
					ret = "Return Without Saving Changes"
				Case "ger"
					ret = "Return Without Saving Changes"
				Case "ita"
					ret = "Return Without Saving Changes"
				Case "spa"
					ret = "Return Without Saving Changes"
			End Select
		End Select
	Return ret
End Function
Public Function getwodetovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "img1"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Parts for this Task"
				Case "fre"
					ret = "Ajouter/ modifier les pi�ces pour cette t�che"
				Case "ger"
					ret = "F�ge hinzu/bearbeite Teile f�r diese Aufgabe"
				Case "ita"
					ret = "Aggiungi/modifica le parti per questa attivit�"
				Case "spa"
					ret = "Agregar/Editar Partes para esta Tarea"
			End Select
		Case "imgjpshed"
			Select Case lang
				Case "eng"
					ret = "Add or Edit Job Plan Schedule Days for this Work Order"
				Case "fre"
					ret = "Add or Edit Job Plan Schedule Days for this Work Order"
				Case "ger"
					ret = "Add or Edit Job Plan Schedule Days for this Work Order"
				Case "ita"
					ret = "Add or Edit Job Plan Schedule Days for this Work Order"
				Case "spa"
					ret = "Add or Edit Job Plan Schedule Days for this Work Order"
			End Select
		Case "imgwoshed"
			Select Case lang
				Case "eng"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "fre"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "ger"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "ita"
					ret = "Add or Edit Schedule Days for this Work Order"
				Case "spa"
					ret = "Add or Edit Schedule Days for this Work Order"
			End Select
		Case "ovid197"
			Select Case lang
				Case "eng"
					ret = "Jump to Scheduling (Scheduled Start, Scheduled Complete, and top level Work Order Skill Required to view this record in Scheduler)"
				Case "fre"
					ret = "Jump to Scheduling (Scheduled Start, Scheduled Complete, and top level Work Order Skill Required to view this record in Scheduler)"
				Case "ger"
					ret = "Jump to Scheduling (Scheduled Start, Scheduled Complete, and top level Work Order Skill Required to view this record in Scheduler)"
				Case "ita"
					ret = "Jump to Scheduling (Scheduled Start, Scheduled Complete, and top level Work Order Skill Required to view this record in Scheduler)"
				Case "spa"
					ret = "Jump to Scheduling (Scheduled Start, Scheduled Complete, and top level Work Order Skill Required to view this record in Scheduler)"
			End Select
		Case "ovid198"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Lead Craft"
				Case "fre"
					ret = "Check to send email alert to Lead Craft"
				Case "ger"
					ret = "Check to send email alert to Lead Craft"
				Case "ita"
					ret = "Check to send email alert to Lead Craft"
				Case "spa"
					ret = "Check to send email alert to Lead Craft"
			End Select
		Case "ovid199"
			Select Case lang
				Case "eng"
					ret = "Check to send email alert to Supervisor"
				Case "fre"
					ret = "Check to send email alert to Supervisor"
				Case "ger"
					ret = "Check to send email alert to Supervisor"
				Case "ita"
					ret = "Check to send email alert to Supervisor"
				Case "spa"
					ret = "Check to send email alert to Supervisor"
			End Select
		Case "ovid200"
			Select Case lang
				Case "eng"
					ret = "Choose Equipment Spare Parts for this Task"
				Case "fre"
					ret = "S�lectionner les pi�ces de rechange de l`�quipement pour cette t�che "
				Case "ger"
					ret = "W�hle die Ausr�stungs-Ersatzteile f�r diese Aufgabe"
				Case "ita"
					ret = "Scegliere le parti di ricambio dell`apparecchiatura per questa attivit�"
				Case "spa"
					ret = "Escoja las Partes de Repuesto del Equipo para esta Tarea"
			End Select
		Case "ovid201"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Tools for this WO"
				Case "fre"
					ret = "Add/Edit Tools for this WO"
				Case "ger"
					ret = "Add/Edit Tools for this WO"
				Case "ita"
					ret = "Add/Edit Tools for this WO"
				Case "spa"
					ret = "Add/Edit Tools for this WO"
			End Select
		Case "ovid202"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Lubricants for this WO"
				Case "fre"
					ret = "Add/Edit Lubricants for this WO"
				Case "ger"
					ret = "Add/Edit Lubricants for this WO"
				Case "ita"
					ret = "Add/Edit Lubricants for this WO"
				Case "spa"
					ret = "Add/Edit Lubricants for this WO"
			End Select
		Case "ovid203"
			Select Case lang
				Case "eng"
					ret = "Add/Edit Measurements for this WO"
				Case "fre"
					ret = "Add/Edit Measurements for this WO"
				Case "ger"
					ret = "Add/Edit Measurements for this WO"
				Case "ita"
					ret = "Add/Edit Measurements for this WO"
				Case "spa"
					ret = "Add/Edit Measurements for this WO"
			End Select
		Case "ovid204"
			Select Case lang
				Case "eng"
					ret = "Print Current Work Order"
				Case "fre"
					ret = "Print Current Work Order"
				Case "ger"
					ret = "Print Current Work Order"
				Case "ita"
					ret = "Print Current Work Order"
				Case "spa"
					ret = "Print Current Work Order"
			End Select
		End Select
	Return ret
End Function
Public Function getwofaillistovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddtosite"
			Select Case lang
				Case "eng"
					ret = "Choose Failure Modes for this Plant Site "
				Case "fre"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ger"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ita"
					ret = "Choose Failure Modes for this Plant Site "
				Case "spa"
					ret = "Seleccionar los Modos de Falla para este Sitio de la Planta"
			End Select
		End Select
	Return ret
End Function
Public Function getwojpfaillistovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "btnaddtosite"
			Select Case lang
				Case "eng"
					ret = "Choose Failure Modes for this Plant Site "
				Case "fre"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ger"
					ret = "Choose Failure Modes for this Plant Site "
				Case "ita"
					ret = "Choose Failure Modes for this Plant Site "
				Case "spa"
					ret = "Seleccionar los Modos de Falla para este Sitio de la Planta"
			End Select
		End Select
	Return ret
End Function
Public Function getwolistovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid205"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		Case "tdwrs"
			Select Case lang
				Case "eng"
					ret = "Click to Show or Hide Search Options"
				Case "fre"
					ret = "Click to Show or Hide Search Options"
				Case "ger"
					ret = "Click to Show or Hide Search Options"
				Case "ita"
					ret = "Click to Show or Hide Search Options"
				Case "spa"
					ret = "Click to Show or Hide Search Options"
			End Select
		End Select
	Return ret
End Function
Public Function getwomanovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgloc"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Clear Screen and Start Over"
				Case "fre"
					ret = "Effacer l`�cran et red�marrer"
				Case "ger"
					ret = "Bildschirminhalt l�schen und neu starten"
				Case "ita"
					ret = "Cancellare finestra e riavviare"
				Case "spa"
					ret = "Limpiar Pantalla y Volver a Empezar"
			End Select
		End Select
	Return ret
End Function
Public Function getwoplansovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgaddplan"
			Select Case lang
				Case "eng"
					ret = "Create a New Job Plan"
				Case "fre"
					ret = "Create a New Job Plan"
				Case "ger"
					ret = "Create a New Job Plan"
				Case "ita"
					ret = "Create a New Job Plan"
				Case "spa"
					ret = "Create a New Job Plan"
			End Select
		Case "imgeditplan"
			Select Case lang
				Case "eng"
					ret = "Edit Selected Job Plan (Applies to Work Order Only)"
				Case "fre"
					ret = "Edit Selected Job Plan (Applies to Work Order Only)"
				Case "ger"
					ret = "Edit Selected Job Plan (Applies to Work Order Only)"
				Case "ita"
					ret = "Edit Selected Job Plan (Applies to Work Order Only)"
				Case "spa"
					ret = "Edit Selected Job Plan (Applies to Work Order Only)"
			End Select
		Case "imgplans"
			Select Case lang
				Case "eng"
					ret = "View Existing Job Plans"
				Case "fre"
					ret = "View Existing Job Plans"
				Case "ger"
					ret = "View Existing Job Plans"
				Case "ita"
					ret = "View Existing Job Plans"
				Case "spa"
					ret = "View Existing Job Plans"
			End Select
		End Select
	Return ret
End Function
Public Function getwraddovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid206"
			Select Case lang
				Case "eng"
					ret = "Create New Work Order With Auto Number"
				Case "fre"
					ret = "Create New Work Order With Auto Number"
				Case "ger"
					ret = "Create New Work Order With Auto Number"
				Case "ita"
					ret = "Create New Work Order With Auto Number"
				Case "spa"
					ret = "Create New Work Order With Auto Number"
			End Select
		End Select
	Return ret
End Function
Public Function getwrdetovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "ovid207"
			Select Case lang
				Case "eng"
					ret = "Submit This Work Request and Exit"
				Case "fre"
					ret = "Submit This Work Request and Exit"
				Case "ger"
					ret = "Submit This Work Request and Exit"
				Case "ita"
					ret = "Submit This Work Request and Exit"
				Case "spa"
					ret = "Submit This Work Request and Exit"
			End Select
		Case "ovid208"
			Select Case lang
				Case "eng"
					ret = "Save Changes to This Work Request Without Submitting and Exit"
				Case "fre"
					ret = "Save Changes to This Work Request Without Submitting and Exit"
				Case "ger"
					ret = "Save Changes to This Work Request Without Submitting and Exit"
				Case "ita"
					ret = "Save Changes to This Work Request Without Submitting and Exit"
				Case "spa"
					ret = "Save Changes to This Work Request Without Submitting and Exit"
			End Select
		End Select
	Return ret
End Function
Public Function getwrlaborovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgadd"
			Select Case lang
				Case "eng"
					ret = "Add Labor to Current Supervisor"
				Case "fre"
					ret = "Add Labor to Current Supervisor"
				Case "ger"
					ret = "Add Labor to Current Supervisor"
				Case "ita"
					ret = "Add Labor to Current Supervisor"
				Case "spa"
					ret = "Add Labor to Current Supervisor"
			End Select
		End Select
	Return ret
End Function
Public Function getwrmanovval(byVal lang as String, byVal aspxlabel as String) as String
	Dim ret as String
	Select Case aspxlabel
		Case "imgsw"
			Select Case lang
				Case "eng"
					ret = "Reset values and Refresh Location Information"
				Case "fre"
					ret = "Reset values and Refresh Location Information"
				Case "ger"
					ret = "Reset values and Refresh Location Information"
				Case "ita"
					ret = "Reset values and Refresh Location Information"
				Case "spa"
					ret = "Reset values and Refresh Location Information"
			End Select
		Case "ovid209"
			Select Case lang
				Case "eng"
					ret = "Use Locations"
				Case "fre"
					ret = "Utiliser les localisations"
				Case "ger"
					ret = "Einsatzorte verwenden"
				Case "ita"
					ret = "Utilizza posizioni"
				Case "spa"
					ret = "Use las Ubicaciones"
			End Select
		End Select
	Return ret
End Function
End Class
