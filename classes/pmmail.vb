Imports System.Net.Mail

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmmail

    Dim tmod As New transmod
    Dim ap As New AppUtils
    Dim sql As String
    Dim mail As New Utilities
    Dim dr As SqlDataReader

    Private str14, str4, str5, port, spass, sauth, sfrom As String
    Public Sub CheckIt(ByVal typ As String, ByVal id As String, Optional ByVal id1 As String = "0", Optional ByVal id2 As String = "0", Optional ByVal id3 As String = "0")
        Select Case typ
            Case "sup"
                'typ, uid, wonum
                Try
                    mail.Open()
                Catch ex As Exception

                End Try
                SendIt(typ, id, id1)
                mail.Dispose()
            Case "lead"
                'typ, uid, wonum
                Try
                    mail.Open()
                Catch ex As Exception

                End Try
                SendIt(typ, id, id1)
                mail.Dispose()
            Case "reqr"
                'typ, uid, wonum
                Try
                    mail.Open()
                Catch ex As Exception

                End Try
                SendItReq(typ, id, id1)
                mail.Dispose()
            Case "meas"
                'typ, pmid, pmtskid, pmtid, tmdid
                Try
                    mail.Open()
                Catch ex As Exception

                End Try
                SendIt(typ, id, id1, id2, id3)
                mail.Dispose()
            Case "tpmmeas"
                'typ, pmid, pmtskid, pmtid, tmdid
                Try
                    mail.Open()
                Catch ex As Exception

                End Try
                SendItTPM(typ, id, id1, id2, id3)
                mail.Dispose()
            Case "comp"
                'typ, pmid, pmtskid, pmtid, tmdid
                Try
                    mail.Open()
                Catch ex As Exception

                End Try
                SendItComp(typ, id, id1)
                mail.Dispose()
            Case "pfadj"
                'typ, pmid, pmtskid, pmtid, tmdid
                Try
                    mail.Open()
                Catch ex As Exception

                End Try
                SendItComp(typ, id, id1)
                mail.Dispose()
        End Select
    End Sub
    Private Sub SendItPFADJ(ByVal typ As String, ByVal superid As String, ByVal pmid As String)
        Dim sup As String

    End Sub
    Private Sub SendItTPM(ByVal typ As String, ByVal pmid As String, ByVal pmtskid As String, ByVal pmtid As String, ByVal tmdid As String)

    End Sub
    Private Sub SendIt(ByVal typ As String, ByVal pmid As String, ByVal pmtskid As String, ByVal pmtid As String, ByVal tmdid As String)
        Dim email As New System.Web.Mail.MailMessage
        Dim ema, user, sup, lead As String
        Dim uf, sf, lf, ef As Integer
        uf = 0
        sf = 0
        lf = 0
        ef = 0
        'sql = "select userid, superid, laborid from ealert where alerttype = 'meas' and alertid = '" & tmdid & "'"
        sql = "select userid from ealert where alerttype = '" & typ & "' and alertid = '" & tmdid & "'"
        dr = mail.GetRdrData(sql)
        'should be userid only now
        While dr.Read
            If dr.Item("userid").ToString <> "" And uf = 0 Then
                uf = 1
                user = "'" & dr.Item("userid").ToString & "'"
            ElseIf dr.Item("userid").ToString <> "" And uf = 1 Then
                user += ",'" & dr.Item("userid").ToString & "'"
            End If
            If dr.Item("superid").ToString <> "" And sf = 0 Then
                sf = 1
                sup = "'" & dr.Item("superid").ToString & "'"
            ElseIf dr.Item("superid").ToString <> "" And sf = 1 Then
                sup += ",'" & dr.Item("superid").ToString & "'"
            End If
            If dr.Item("laborid").ToString <> "" And lf = 0 Then
                lf = 1
                lead = "'" & dr.Item("laborid").ToString & "'"
            ElseIf dr.Item("laborid").ToString <> "" And lf = 1 Then
                lead += ",'" & dr.Item("laborid").ToString & "'"
            End If
        End While
        dr.Close()
        If user <> "" Then
            sql = "select s.email from pmsysusers s where s.email is not null and s.userid in (" & user & ")"
            dr = mail.GetRdrData(sql)
            While dr.Read
                If dr.Item("email").ToString <> "" Then
                    ema += dr.Item("email").ToString & ";"
                End If
            End While
            dr.Close()
        End If
        If sup <> "" Then
            sql = "select s.email from pmsuper s where s.email is not null and s.superid in (" & sup & ")"
            dr = mail.GetRdrData(sql)
            While dr.Read
                If dr.Item("email").ToString <> "" Then
                    ema += dr.Item("email").ToString & ";"
                End If
            End While
            dr.Close()
        End If
        If lead <> "" Then
            sql = "select s.email from pmlabor s where s.email is not null and s.laborid in (" & lead & ")"
            dr = mail.GetRdrData(sql)
            While dr.Read
                If dr.Item("email").ToString <> "" Then
                    ema += dr.Item("email").ToString & ";"
                End If
            End While
            dr.Close()
        End If
        Me.str14 = System.Configuration.ConfigurationManager.AppSettings("custSMTP")
        Me.str4 = System.Configuration.ConfigurationManager.AppSettings("custSendUser")
        Me.str5 = System.Configuration.ConfigurationManager.AppSettings("custSendUsing")
        Me.port = System.Configuration.ConfigurationManager.AppSettings("custSMTPPort")
        Me.spass = System.Configuration.ConfigurationManager.AppSettings("custSendPass")
        Me.sauth = System.Configuration.ConfigurationManager.AppSettings("custAMTPAuth")
        Me.sfrom = System.Configuration.ConfigurationManager.AppSettings("custSendFrom")
        If ema <> "" Then
            email.To = ema
            email.From = Me.sfrom
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = Me.str5
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = Me.str14
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = Me.str4
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = Me.spass
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = Me.sauth
            email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = Me.port

            Dim intralog As New mmenu_utils_a
            Dim isintra As Integer = intralog.INTRA
            If isintra <> 1 Then
                email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
            End If

            email.Body = AlertBody(typ, pmid, pmtskid, pmtid, tmdid)
            email.Subject = "Out of Spec Alert"
            email.BodyFormat = Web.Mail.MailFormat.Html
            System.Web.Mail.SmtpMail.SmtpServer.Insert(0, Me.str14)
            'System.Web.Mail.SmtpMail.Send(email)
            Try
                System.Web.Mail.SmtpMail.Send(email)
            Catch ex As Exception
                Try
                    Dim pmail As New System.Net.Mail.MailMessage
                    pmail.From = New System.Net.Mail.MailAddress(sfrom)
                    pmail.To.Add(ema)
                    pmail.BodyEncoding = Encoding.Unicode
                    pmail.Subject = "Out of Spec Alert"
                    pmail.Body = AlertBody(typ, pmid, pmtskid, pmtid, tmdid)
                    pmail.IsBodyHtml = True
                    Dim smtp As SmtpClient = New SmtpClient(str14)
                    smtp.Send(pmail)
                Catch ex0 As Exception
                    'Dim strMessage As String = "Problem Sending Email"
                    'mail.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try

            End Try
        End If

    End Sub
    Private Function AlertBody(ByVal typ As String, ByVal pmid As String, ByVal pmtskid As String, ByVal pmtid As String, ByVal tmdid As String) As String

        Dim eqid, eqnum, sid, deptid, cellid, locid, location, desc, funcid, comid, chrg, pm As String
        If typ = "meas" Then
            sql = "select p.eqid, e.eqnum, s.sitename, d.dept_line, c.cell_name, l.location, " _
            + "pm = (p.skill + '/' + cast(p.freq as varchar(10)) + ' days/' + p.rd) " _
            + "from pm p " _
            + "left join equipment e on e.eqid = p.eqid " _
            + "left join sites s on e.siteid = s.siteid " _
            + "left join dept d on d.dept_id = e.dept_id " _
            + "left join cells c on c.dept_id = d.dept_id " _
            + "left join pmlocations l on l.locid = e.locid " _
            + "where pmid = '" & pmid & "'"
        Else
            sql = "select pm.eqid, e.eqnum, s.sitename, d.dept_line, c.cell_name, l.location, " _
            + "pm = ( " _
            + "case isnull(len(pm.shift),0) " _
            + "when 0 then 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd  " _
            + "else " _
            + "case pm.freq " _
            + "when 0 then 'Operator/STATIC/' + pm.rd + ' - ' + isnull(pm.shift,'open') + " _
            + "case len(pm.days) " _
            + "when 0 then '' " _
            + "else " _
            + "case pm.freq " _
            + "when 1 then '' " _
            + "else ' - ' " _
            + "+ pm.days end end  " _
            + "else 'Operator/' + cast(pm.freq as varchar(10)) + ' days/' + pm.rd + ' - ' + isnull(pm.shift,'open') +  " _
            + "case len(pm.days) " _
            + "when 0 then '' " _
            + "else " _
            + "case pm.freq " _
            + "when 1 then '' " _
            + "else ' - ' " _
            + "+ pm.days  " _
            + "end end end end) " _
            + "from tpm p " _
            + "left join equipment e on e.eqid = pm.eqid " _
            + "left join sites s on e.siteid = s.siteid " _
            + "left join dept d on d.dept_id = e.dept_id " _
            + "left join cells c on c.dept_id = d.dept_id " _
            + "left join pmlocations l on l.locid = e.locid " _
            + "where pm.pmid = '" & pmid & "'"
        End If

        dr = mail.GetRdrData(sql)
        While dr.Read
            eqnum = dr.Item("eqnum").ToString
            sid = dr.Item("sitename").ToString
            deptid = dr.Item("dept_line").ToString
            cellid = dr.Item("cell_name").ToString
            locid = dr.Item("location").ToString
            pm = dr.Item("pm").ToString
        End While
        dr.Close()
        If typ = "meas" Then
            sql = "select p.funcid, f.func, p.comid, c.compnum from pmtrack p " _
            + "left join functions f on f.func_id = p.funcid " _
            + "left join components c on c.comid = p.comid " _
            + "where p.pmtid = '" & pmtid & "'"
        Else
            sql = "select p.funcid, f.func, p.comid, c.compnum from pmtracktpm p " _
            + "left join functions f on f.func_id = p.funcid " _
            + "left join components c on c.comid = p.comid " _
            + "where p.pmtid = '" & pmtid & "'"
        End If

        dr = mail.GetRdrData(sql)
        While dr.Read
            funcid = dr.Item("funcid").ToString
            comid = dr.Item("comid").ToString
        End While
        dr.Close()
        Dim meas, hi, hio, lo, lou, spec, read As String
        If typ = "meas" Then
            sql = "select measure, hi, mover, lo, munder, spec, measurement from pmtaskmeasdetman where tmdid = '" & tmdid & "'"
        Else
            sql = "select measure, hi, mover, lo, munder, spec, measurement from pmtaskmeasdetmantpm where tmdid = '" & tmdid & "'"
        End If

        dr = mail.GetRdrData(sql)
        While dr.Read
            meas = dr.Item("measure").ToString
            hi = dr.Item("hi").ToString
            hio = dr.Item("mover").ToString
            lo = dr.Item("lo").ToString
            lou = dr.Item("munder").ToString
            spec = dr.Item("spec").ToString
            read = dr.Item("measurement").ToString
        End While
        dr.Close()

        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrle")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim body As String
        body = "<table width='800px' style='font-size:8pt; font-family:Verdana;'>"

        body &= "<tr><td><b>" & tmod.getxlbl("xlb86", "pmmail.vb") & " </b><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b>" & tmod.getxlbl("xlb87", "pmmail.vb") & " :" & pm & "</b><br><br></td></tr>" & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb88", "pmmail.vb") & "     " & meas & "<br><br></td></tr>" & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb89", "pmmail.vb") & "          " & hi & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb90", "pmmail.vb") & "          " & lo & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb91", "pmmail.vb") & "        " & lo & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb92", "pmmail.vb") & "     " & read & "<br><br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b><u>" & tmod.getxlbl("xlb93", "pmmail.vb") & "</u></b><br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb94", "pmmail.vb") & "  " & eqnum & "<br><br></td></tr>" & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb95") & "    " & funcid & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb96", "pmmail.vb") & "   " & comid & "<br><br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b><u>" & tmod.getxlbl("xlb97", "pmmail.vb") & "</u></b><br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb98", "pmmail.vb") & "  " & sid & "<br><br></td></tr>" & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb99", "pmmail.vb") & "  " & deptid & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb100", "pmmail.vb") & cellid & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb101", "pmmail.vb") & "    " & locid & "<br><br><br></td></tr>" & vbCrLf & vbCrLf

        body &= "</table>"

        Return body
    End Function
    Private Sub SendItReq(ByVal typ As String, ByVal uid As String, ByVal wonum As String)
        Dim email As New System.Web.Mail.MailMessage

        Dim ema, desc, astart, sstart, tstart, wtyp, comp As String

        sql = "select isnull(email, '') from pmsysusers where username = '" & uid & "'"
        ema = mail.strScalar(sql)

        If ema = "" Then
            Exit Sub
        End If
        sql = "select actstart, schedstart, targstartdate, actfinish, worktype, description from workorder where wonum = '" & wonum & "'"
        dr = mail.GetRdrData(sql)
        While dr.Read
            desc = ""
            tstart = dr.Item("targstartdate").ToString
            sstart = dr.Item("schedstart").ToString
            astart = dr.Item("actstart").ToString
            wtyp = dr.Item("worktype").ToString
            comp = dr.Item("actfinish").ToString
        End While
        dr.Close()

        Me.str14 = System.Configuration.ConfigurationManager.AppSettings("custSMTP")
        Me.str4 = System.Configuration.ConfigurationManager.AppSettings("custSendUser")
        Me.str5 = System.Configuration.ConfigurationManager.AppSettings("custSendUsing")
        Me.port = System.Configuration.ConfigurationManager.AppSettings("custSMTPPort")
        Me.spass = System.Configuration.ConfigurationManager.AppSettings("custSendPass")
        Me.sauth = System.Configuration.ConfigurationManager.AppSettings("custAMTPAuth")
        Me.sfrom = System.Configuration.ConfigurationManager.AppSettings("custSendFrom")
        email.To = ema
        email.From = Me.sfrom
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = Me.str5
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = Me.str14
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = Me.str4
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = Me.spass
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = Me.sauth
        email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = Me.port

        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If

        email.Body = AlertBodyComp(wonum, desc, astart, sstart, tstart, comp, wtyp)
        email.Subject = "Work Order#  " & wonum & " has been Completed"
        email.BodyFormat = Web.Mail.MailFormat.Html
        System.Web.Mail.SmtpMail.SmtpServer.Insert(0, Me.str14)
        'System.Web.Mail.SmtpMail.Send(email)
        Try
            System.Web.Mail.SmtpMail.Send(email)
        Catch ex As Exception
            Try
                Dim pmail As New System.Net.Mail.MailMessage
                pmail.From = New System.Net.Mail.MailAddress(sfrom)
                pmail.To.Add(ema)
                pmail.BodyEncoding = Encoding.Unicode
                pmail.Subject = "Work Order#  " & wonum & " has been Completed"
                pmail.Body = AlertBodyComp(wonum, desc, astart, sstart, tstart, comp, wtyp)
                pmail.IsBodyHtml = True
                Dim smtp As SmtpClient = New SmtpClient(str14)
                smtp.Send(pmail)
            Catch ex0 As Exception
                'Dim strMessage As String = "Problem Sending Email"

            End Try

        End Try

    End Sub
    


    Private Sub SendIt(ByVal typ As String, ByVal uid As String, ByVal wonum As String)
        Dim email As New System.Web.Mail.MailMessage

        Dim start, desc, wtyp, ema As String
        If typ = "sup" Then
            sql = "select isnull(email, '') from pmsuper where superid = '" & uid & "'"
            ema = mail.strScalar(sql)
            If ema = "" Then
                sql = "select isnull(email, '') from pmsysusers where userid = '" & uid & "'"
                ema = mail.strScalar(sql)
            End If
        ElseIf typ = "lead" Then
            sql = "select isnull(email, '') from pmlabor where laborid = '" & uid & "'"
            ema = mail.strScalar(sql)
            If ema = "" Then
                sql = "select isnull(email, '') from pmsysusers where userid = '" & uid & "'"
                ema = mail.strScalar(sql)
            End If

        End If
        If ema <> "" Then
            sql = "select targstartdate, worktype, description from workorder where wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                desc = dr.Item("description").ToString
                start = dr.Item("targstartdate").ToString
                wtyp = dr.Item("worktype").ToString
            End While
            dr.Close()
            Me.str14 = System.Configuration.ConfigurationManager.AppSettings("custSMTP")
            Me.str4 = System.Configuration.ConfigurationManager.AppSettings("custSendUser")
            Me.str5 = System.Configuration.ConfigurationManager.AppSettings("custSendUsing")
            Me.port = System.Configuration.ConfigurationManager.AppSettings("custSMTPPort")
            Me.spass = System.Configuration.ConfigurationManager.AppSettings("custSendPass")
            Me.sauth = System.Configuration.ConfigurationManager.AppSettings("custAMTPAuth")
            Me.sfrom = System.Configuration.ConfigurationManager.AppSettings("custSendFrom")
            email.To = ema
            email.From = Me.sfrom
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = Me.str5
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = Me.str14
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = Me.str4
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = Me.spass
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = Me.sauth
            email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = Me.port

            Dim intralog As New mmenu_utils_a
            Dim isintra As Integer = intralog.INTRA
            If isintra <> 1 Then
                email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
            End If

            email.Body = AlertBody(wonum, desc, start, wtyp)
            email.Subject = "New Work Order#  " + wonum
            email.BodyFormat = Web.Mail.MailFormat.Html
            System.Web.Mail.SmtpMail.SmtpServer.Insert(0, Me.str14)
            'System.Web.Mail.SmtpMail.Send(email)
            Try
                System.Web.Mail.SmtpMail.Send(email)
            Catch ex As Exception
                Try
                    Dim mail As New System.Net.Mail.MailMessage
                    mail.From = New System.Net.Mail.MailAddress(sfrom)
                    mail.To.Add(ema)
                    mail.BodyEncoding = Encoding.Unicode
                    mail.Subject = "New Work Order#  " + wonum
                    mail.Body = AlertBody(wonum, desc, start, wtyp)
                    mail.IsBodyHtml = True
                    Dim smtp As SmtpClient = New SmtpClient(str14)
                    smtp.Send(mail)
                Catch ex0 As Exception
                    'Dim strMessage As String = "Problem Sending Email"
                    'adm.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try

            End Try
        End If

    End Sub

    Private Function AlertBody(ByVal wo As String, ByVal desc As String, ByVal start As String, ByVal wtyp As String) As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrle")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")

        sql = "select w.description, l.longdesc, w.qty, w.estlabhrs, w.estlabcost, w.estmatcost, w.esttoolcost, w.estlubecost, isnull(w.actlabhrs,0) as actlabhrs, " _
       + "isnull(w.actlabcost,0) as actlabcost, " _
       + "isnull(w.actmatcost,0) as actmatcost, isnull(w.acttoolcost,0) as acttoolcost, isnull(w.actlubecost,0) as actlubecost, " _
        + "isnull(w.leadcraft, 'Not Provided') as 'leadcraft', isnull(w.supervisor, 'Not Provided') as 'supervisor', " _
        + "w.eqnum, d.dept_line, c.cell_name, l1.location, e.fpc " _
       + "from workorder w " _
       + "left join wolongdesc l on l.wonum = w.wonum " _
       + "left join dept d on d.dept_id = w.deptid " _
       + "left join cells c on c.cellid = d.dept_id " _
       + "left join equipment e on e.eqid = w.eqid " _
       + "left join pmlocations l1 on l1.locid = w.locid where w.wonum = '" & wo & "'"
        dr = mail.GetRdrData(sql)
        Dim eq, dp, cl, lo, col, sd, ld As String
        While dr.Read
            eq = dr.Item("eqnum").ToString
            dp = dr.Item("dept_line").ToString
            cl = dr.Item("cell_name").ToString
            lo = dr.Item("location").ToString
            col = dr.Item("fpc").ToString

            sd = dr.Item("description").ToString
            ld = dr.Item("longdesc").ToString
        End While
        dr.Close()
        desc = sd & ld
        Dim body As String
        body = "<table width='800px' style='font-size:10pt; font-family:Verdana;'>"
        body &= "<tr><td colspan=""2""><b><u>Work Order Details</u></b></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td width=""120px"">" & tmod.getxlbl("xlb103", "pmmail.vb") & "</td><td width=""680px"">" & wo & "</td></tr>" & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb104", "pmmail.vb") & "</td><td>" & wtyp & "</td></tr>" & vbCrLf
        body &= "<tr><td>Equipment#</td><td>" & eq & "</td></tr>" & vbCrLf
        If lo <> "" Then
            body &= "<tr><td>Location</td><td>" & lo & "</td></tr>" & vbCrLf
        ElseIf dp <> "" Then
            If cl <> "" Then
                body &= "<tr><td>Department</td><td>" & dp & "</td></tr>" & vbCrLf
                body &= "<tr><td>Cell</td><td>" & cl & "</td></tr>" & vbCrLf
            Else
                body &= "<tr><td>Department</td><td>" & dp & "</td></tr>" & vbCrLf
            End If
        End If
        If col <> "" Then
            body &= "<tr><td>Column</td><td>" & col & "</td></tr>" & vbCrLf
        End If

        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td colspan=""2""><u>" & tmod.getxlbl("xlb105", "pmmail.vb") & "</u></td><tr><td colspan=""2"">" & desc & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td>Target Start</td><td>" & start & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td colspan=""2"">" & tmod.getlbl("cdlbl366", "pmmail.vb") & "<a href='" + urlname + appname + "/appswo/woprint.aspx?typ=wo&wo=" + wo + "'>" & tmod.getxlbl("xlb107", "pmmail.vb") & " " + wo + "</td></tr>" & vbCrLf & vbCrLf
        body &= "</table>"

        Return body
    End Function
    Private Sub SendItStart(ByVal typ As String, ByVal uid As String, ByVal wonum As String)

    End Sub
    Private Sub SendItComp(ByVal typ As String, ByVal uid As String, ByVal wonum As String)
        Dim email As New System.Web.Mail.MailMessage
        Dim ema, desc, astart, sstart, tstart, wtyp, comp As String
        sql = "select isnull(email, '') from pmsuper where superid = '" & uid & "'"
        ema = mail.strScalar(sql)
        If ema = "" Then
            sql = "select isnull(email, '') from pmsysusers where userid = '" & uid & "'"
            Try
                ema = mail.strScalar(sql)
            Catch ex As Exception
                Exit Sub
            End Try
        End If
        If ema = "" Then
            Exit Sub
        End If
        sql = "select actstart, schedstart, targstartdate, actfinish, worktype, description from workorder where wonum = '" & wonum & "'"
        dr = mail.GetRdrData(sql)
        While dr.Read
            desc = ""
            tstart = dr.Item("targstartdate").ToString
            sstart = dr.Item("schedstart").ToString
            astart = dr.Item("actstart").ToString
            wtyp = dr.Item("worktype").ToString
            comp = dr.Item("actfinish").ToString
        End While
        dr.Close()

        Me.str14 = System.Configuration.ConfigurationManager.AppSettings("custSMTP")
        Me.str4 = System.Configuration.ConfigurationManager.AppSettings("custSendUser")
        Me.str5 = System.Configuration.ConfigurationManager.AppSettings("custSendUsing")
        Me.port = System.Configuration.ConfigurationManager.AppSettings("custSMTPPort")
        Me.spass = System.Configuration.ConfigurationManager.AppSettings("custSendPass")
        Me.sauth = System.Configuration.ConfigurationManager.AppSettings("custAMTPAuth")
        Me.sfrom = System.Configuration.ConfigurationManager.AppSettings("custSendFrom")
        email.To = ema
        email.From = Me.sfrom
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = Me.str5
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = Me.str14
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = Me.str4
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = Me.spass
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = Me.sauth
        email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = Me.port

        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If

        email.Body = AlertBodyComp(wonum, desc, astart, sstart, tstart, comp, wtyp)
        email.Subject = "Work Order#  " & wonum & " has been Completed"
        email.BodyFormat = Web.Mail.MailFormat.Html
        System.Web.Mail.SmtpMail.SmtpServer.Insert(0, Me.str14)
        'System.Web.Mail.SmtpMail.Send(email)
        Try
            System.Web.Mail.SmtpMail.Send(email)
        Catch ex As Exception
            Try
                Dim pmail As New System.Net.Mail.MailMessage
                pmail.From = New System.Net.Mail.MailAddress(sfrom)
                pmail.To.Add(ema)
                pmail.BodyEncoding = Encoding.Unicode
                pmail.Subject = "Work Order#  " & wonum & " has been Completed"
                pmail.Body = AlertBodyComp(wonum, desc, astart, sstart, tstart, comp, wtyp)
                pmail.IsBodyHtml = True
                Dim smtp As SmtpClient = New SmtpClient(str14)
                smtp.Send(pmail)
            Catch ex0 As Exception
                'Dim strMessage As String = "Problem Sending Email"
                'mail.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try

        End Try
    End Sub
    Private Function AlertBodyComp(ByVal wonum As String, ByVal desc As String, ByVal astart As String, ByVal sstart As String, _
                                   ByVal tstart As String, ByVal comp As String, ByVal wtyp As String) As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrle")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim sd, ld, lc, su, eq, dp, cl, lo, col, dt As String
        Dim sqty As Integer
        Dim esthr, estjphr, acthr, actjphr As Decimal
        Dim estcost, estjpcost, actcost, actjpcost As Decimal
        Dim estmatcost, estjpmatcost, actmatcost, actjpmatcost As Decimal
        Dim esttoolcost, estjptoolcost, acttoolcost, actjptoolcost As Decimal
        Dim estlubecost, estjplubecost, actlubecost, actjplubecost As Decimal
        Dim icost As String = ap.InvEntry()
        sql = "select w.description, l.longdesc, w.qty, w.estlabhrs, w.estlabcost, w.estmatcost, w.esttoolcost, w.estlubecost, isnull(w.actlabhrs,0) as actlabhrs, " _
        + "isnull(w.actlabcost,0) as actlabcost, " _
        + "isnull(w.actmatcost,0) as actmatcost, isnull(w.acttoolcost,0) as acttoolcost, isnull(w.actlubecost,0) as actlubecost, " _
         + "isnull(w.leadcraft, 'Not Provided') as 'leadcraft', isnull(w.supervisor, 'Not Provided') as 'supervisor', " _
         + "w.eqnum, d.dept_line, c.cell_name, l1.location, e.fpc, isnull(h.totaldown, '0') as totaldown " _
        + "from workorder w " _
        + "left join wolongdesc l on l.wonum = w.wonum " _
        + "left join dept d on d.dept_id = w.deptid " _
        + "left join cells c on c.cellid = d.dept_id " _
        + "left join equipment e on e.eqid = w.eqid " _
        + "left join eqhist h on h.wonum = w.wonum " _
        + "left join pmlocations l1 on l1.locid = w.locid where w.wonum = '" & wonum & "'"
        dr = mail.GetRdrData(sql)
        While dr.Read
            eq = dr.Item("eqnum").ToString
            dp = dr.Item("dept_line").ToString
            cl = dr.Item("cell_name").ToString
            lo = dr.Item("location").ToString
            col = dr.Item("fpc").ToString
            dt = dr.Item("totaldown").ToString

            sd = dr.Item("description").ToString
            ld = dr.Item("longdesc").ToString
            su = dr.Item("supervisor").ToString
            lc = dr.Item("leadcraft").ToString
            esthr = dr.Item("estlabhrs").ToString
            acthr = dr.Item("actlabhrs").ToString
            estcost = dr.Item("estlabcost").ToString
            actcost = dr.Item("actlabcost").ToString
            estmatcost = dr.Item("estmatcost").ToString
            actmatcost = dr.Item("actmatcost").ToString
            esttoolcost = dr.Item("esttoolcost").ToString
            acttoolcost = dr.Item("acttoolcost").ToString
            estlubecost = dr.Item("estlubecost").ToString
            actlubecost = dr.Item("actlubecost").ToString
            Try
                sqty = dr.Item("qty").ToString
            Catch ex As Exception
                sqty = 1
            End Try
        End While
        dr.Close()
        desc = sd & ld
        Dim body As String
        body = "<table width='800px' style='font-size:10pt; font-family:Verdana;'>"
        body &= "<tr><td colspan=""2""><b><u>Work Order Details</u></b></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td width=""120px"">" & tmod.getxlbl("xlb103", "pmmail.vb") & "</td><td width=""680px"">" & wonum & "</td></tr>" & vbCrLf
        body &= "<tr><td>" & tmod.getxlbl("xlb104", "pmmail.vb") & "</td><td>" & wtyp & "</td></tr>" & vbCrLf
        body &= "<tr><td>Equipment#</td><td>" & eq & "</td></tr>" & vbCrLf
        If lo <> "" Then
            body &= "<tr><td>Location</td><td>" & lo & "</td></tr>" & vbCrLf
        ElseIf dp <> "" Then
            If cl <> "" Then
                body &= "<tr><td>Department</td><td>" & dp & "</td></tr>" & vbCrLf
                body &= "<tr><td>Cell</td><td>" & cl & "</td></tr>" & vbCrLf
            Else
                body &= "<tr><td>Department</td><td>" & dp & "</td></tr>" & vbCrLf
            End If
        End If
        If col <> "" Then
            body &= "<tr><td>Column</td><td>" & col & "</td></tr>" & vbCrLf
        End If

        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2""><u>" & tmod.getxlbl("xlb105", "pmmail.vb") & "</u></td><tr><td colspan=""2"">" & desc & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Target Start</td><td>" & tstart & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Scheduled Start</td><td>" & tstart & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Actual Start</td><td>" & tstart & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Actual Complete</td><td>" & comp & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        body &= "<tr><td>Supervisor</td><td>" & su & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Lead Craft</td><td>" & lc & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf



        Dim comcnt, rootcnt, failcnt As Integer
        sql = "select count(*) from wonotes where wonum = '" & wonum & "'"
        Try
            comcnt = mail.Scalar(sql)
        Catch ex As Exception
            comcnt = 0
        End Try
        sql = "select count(*) from worootcause where wonum = '" & wonum & "'"
        Try
            rootcnt = mail.Scalar(sql)
        Catch ex As Exception
            rootcnt = 0
        End Try
        'Downtime
        body &= "<tr><td>Down Time</td><td>" & dt & "</td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        Dim fcnt As Integer = 0
        Dim mcnt As Integer = 0
        Dim fu, co, fm, pr, cr, cw As String
        Dim ty, ms, hi, low, sp, m2, munder, mover, mw As String
        If wtyp = "PM" Then
            sql = "select f.pmhid, f1.func, c.compnum, f.failuremode, isnull(f.problem, 'Not Provided') as 'problem', " _
                + "isnull(f.corraction, 'Not Provided') as 'corraction', f.wonum " _
                + "from pmfailhist f " _
                + "left join pmhist h on h.pmhid = f.pmhid " _
                + "left join components c on c.comid = f.comid " _
                + "left join functions f1 on f1.func_id = c.func_id " _
                + "where h.wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                fcnt += 1
                fu = dr.Item("func").ToString
                co = dr.Item("compnum").ToString
                fm = dr.Item("failuremode").ToString
                pr = dr.Item("problem").ToString
                cr = dr.Item("corraction").ToString
                cw = dr.Item("wonum").ToString
                If fcnt = 1 Then
                    body &= "<tr><td colspan=""2""><b><u>Failures</u></b></td></tr>" & vbCrLf & vbCrLf

                    body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

                    body &= "<tr><td width=""100px""><u>Function</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""120px""><u>Component</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Failure Mode</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""150px""><u>Problem</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""150px""><u>Corrective Action</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Work Order#</u></td></tr>" & vbCrLf & vbCrLf
                End If
                body &= "<tr><td width=""100px"" valign=""top"">" & fu & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""120px"" valign=""top"">" & co & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""100px"" valign=""top"">" & fm & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""150px"" valign=""top"">" & pr & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""150px"" valign=""top"">" & cr & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""100px"" valign=""top"">" & cw & "</td></tr>" & vbCrLf & vbCrLf
            End While
            dr.Close()
            If fcnt = 0 Then
                body &= "<tr><td colspan=""2"">No Failures Reported</td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            Else
                body &= "</table></td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            End If

            sql = "select f.pmhid, f1.func, c.compnum, f.[type], f.measure, " _
                + "f.hii, f.loi, f.speci, f.measurement2, isnull(f.mover, '0') as 'mover', isnull(f.munder, '0') as 'munder'," _
                + "isnull(f.problem, 'Not Provided') as 'problem', isnull(f.corraction, 'Not Provided') as 'corraction',  " _
                + "f.wonum " _
                + "from pmTaskMeasDetManHIST f " _
                + "left join PMTrackArch a on a.pmhid = f.pmhid " _
                + "left join pmhist h on h.pmhid = f.pmhid " _
                + "left join components c on c.comid = a.comid  " _
                + "left join functions f1 on f1.func_id = c.func_id " _
                + "where h.wonum = '" & wonum & "' and ((isnull(mover, 0) <> 0 or isnull(munder, 0) <> 0) or f.wonum is not null)"
            dr = mail.GetRdrData(sql)
            While dr.Read
                mcnt += 1
                fu = dr.Item("func").ToString
                co = dr.Item("compnum").ToString
                ty = dr.Item("type").ToString
                ms = dr.Item("measure").ToString
                hi = dr.Item("hii").ToString
                low = dr.Item("loi").ToString
                sp = dr.Item("speci").ToString
                m2 = dr.Item("measurement2").ToString
                munder = dr.Item("munder").ToString
                mover = dr.Item("mover").ToString
                mw = dr.Item("wonum").ToString
                If mcnt = 1 Then
                    body &= "<tr><td colspan=""2""><b><u>Measurement Failures</u></b></td></tr>" & vbCrLf & vbCrLf

                    body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

                    body &= "<tr><td width=""100px""><u>Function</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""120px""><u>Component</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Type</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Measure</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Hi</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Lo</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Spec</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Measurement</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Work Order#</u></td></tr>" & vbCrLf & vbCrLf
                End If
                body &= "<tr><td valign=""top"">" & fu & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & co & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & ty & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & ms & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & hi & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & low & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & sp & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & m2 & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & munder & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & mover & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & mw & "</td></tr>" & vbCrLf & vbCrLf
            End While
            dr.Close()
            If fcnt = 0 Then
                body &= "<tr><td colspan=""2"">No Measurement Failures Reported</td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            Else
                body &= "</table></td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            End If
        ElseIf wtyp = "TPM" Then
            fcnt = 0
            mcnt = 0
            sql = "select f.pmhid, f1.func, c.compnum, f.failuremode, isnull(f.problem, 'Not Provided') as 'problem', " _
                + "isnull(f.corraction, 'Not Provided') as 'corraction', f.wonum " _
                + "from pmfailhisttpm f " _
                + "left join pmhisttpm h on h.pmhid = f.pmhid " _
                + "left join components c on c.comid = f.comid " _
                + "left join functions f1 on f1.func_id = c.func_id " _
                + "where h.wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                fcnt += 1
                fu = dr.Item("func").ToString
                co = dr.Item("compnum").ToString
                fm = dr.Item("failuremode").ToString
                pr = dr.Item("problem").ToString
                cr = dr.Item("corraction").ToString
                cw = dr.Item("wonum").ToString
                If fcnt = 1 Then
                    body &= "<tr><td colspan=""2""><b><u>Failures</u></b></td></tr>" & vbCrLf & vbCrLf

                    body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

                    body &= "<tr><td width=""100px""><u>Function</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""120px""><u>Component</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Failure Mode</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""150px""><u>Problem</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""150px""><u>Corrective Action</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Work Order#</u></td></tr>" & vbCrLf & vbCrLf
                End If
                body &= "<tr><td width=""100px"" valign=""top"">" & fu & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""120px"" valign=""top"">" & co & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""100px"" valign=""top"">" & fm & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""150px"" valign=""top"">" & pr & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""150px"" valign=""top"">" & cr & "</td>" & vbCrLf & vbCrLf
                body &= "<td width=""100px"" valign=""top"">" & cw & "</td></tr>" & vbCrLf & vbCrLf
            End While
            dr.Close()
            If fcnt = 0 Then
                body &= "<tr><td colspan=""2"">No Failures Reported</td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            Else
                body &= "</table></td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            End If

            sql = "select f.pmhid, f1.func, c.compnum, f.[type], f.measure, " _
                + "f.hii, f.loi, f.speci, f.measurement2, isnull(f.mover, '0') as 'mover', isnull(f.munder, '0') as 'munder'," _
                + "isnull(f.problem, 'Not Provided') as 'problem', isnull(f.corraction, 'Not Provided') as 'corraction',  " _
                + "f.wonum " _
                + "from pmTaskMeasDetManHISTtpm f " _
                + "left join PMTrackArchtpm a on a.pmhid = f.pmhid " _
                + "left join pmhisttpm h on h.pmhid = f.pmhid " _
                + "left join components c on c.comid = a.comid  " _
                + "left join functions f1 on f1.func_id = c.func_id " _
                + "where h.wonum = '" & wonum & "' and ((isnull(mover, 0) <> 0 or isnull(munder, 0) <> 0) or f.wonum is not null)"
            dr = mail.GetRdrData(sql)
            While dr.Read
                mcnt += 1
                fu = dr.Item("func").ToString
                co = dr.Item("compnum").ToString
                ty = dr.Item("type").ToString
                ms = dr.Item("measure").ToString
                hi = dr.Item("hii").ToString
                low = dr.Item("loi").ToString
                sp = dr.Item("speci").ToString
                m2 = dr.Item("measurement2").ToString
                munder = dr.Item("munder").ToString
                mover = dr.Item("mover").ToString
                mw = dr.Item("wonum").ToString
                If mcnt = 1 Then
                    body &= "<tr><td colspan=""2""><b><u>Measurement Failures</u></b></td></tr>" & vbCrLf & vbCrLf

                    body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

                    body &= "<tr><td width=""100px""><u>Function</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""120px""><u>Component</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Type</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Measure</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Hi</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Lo</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""40px""><u>Spec</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""80px""><u>Measurement</u></td>" & vbCrLf & vbCrLf
                    body &= "<td width=""100px""><u>Work Order#</u></td></tr>" & vbCrLf & vbCrLf
                End If
                body &= "<tr><td valign=""top"">" & fu & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & co & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & ty & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & ms & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & hi & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & low & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & sp & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & m2 & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & munder & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & mover & "</td>" & vbCrLf & vbCrLf
                body &= "<td valign=""top"">" & mw & "</td></tr>" & vbCrLf & vbCrLf
            End While
            dr.Close()
            If fcnt = 0 Then
                body &= "<tr><td colspan=""2"">No Measurement Failures Reported</td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            Else
                body &= "</table></td></tr>" & vbCrLf & vbCrLf
                body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
            End If
        Else

        End If

        Dim rootid, comments, enterdate, user, moddate, modby As String
        If comcnt > 0 Then
            sql = "select * from wonotes where wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                rootid = dr.Item("noteid").ToString
                comments = dr.Item("note").ToString
                enterdate = dr.Item("notedate").ToString
                user = dr.Item("noteby").ToString
                moddate = dr.Item("modifieddate").ToString
                modby = dr.Item("modifiedby").ToString
            End While
            dr.Close()
            body &= "<tr><td colspan=""2""><b><u>Work Order Notes</u></b></td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td>Entered By</td><td>" & user & "</td></tr>" & vbCrLf
            body &= "<tr><td>Enter Date</td><td>" & enterdate & "</td></tr>" & vbCrLf
            body &= "<tr><td>Modified By</td><td>" & modby & "</td></tr>" & vbCrLf
            body &= "<tr><td>Modified Date</td><td>" & moddate & "</td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2"">" & comments & "</td><td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        Else
            body &= "<tr><td colspan=""2"">No Work Order Notes Provided</td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        End If
        Dim rootcause, corraction As String
        If rootcnt > 0 Then
            sql = "select * from worootcause where wonum = '" & wonum & "'"
            dr = mail.GetRdrData(sql)
            While dr.Read
                rootid = dr.Item("rootid").ToString
                rootcause = dr.Item("rootcause").ToString
                corraction = dr.Item("corraction").ToString
                comments = dr.Item("comments").ToString
                enterdate = dr.Item("enterdate").ToString
                user = dr.Item("enterby").ToString
                moddate = dr.Item("modifieddate").ToString
                modby = dr.Item("modifiedby").ToString
            End While
            dr.Close()
            body &= "<tr><td colspan=""2""><b><u>Root Cause Details</u></b></td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td>Entered By</td><td>" & user & "</td></tr>" & vbCrLf
            body &= "<tr><td>Enter Date</td><td>" & enterdate & "</td></tr>" & vbCrLf
            body &= "<tr><td>Modified By</td><td>" & modby & "</td></tr>" & vbCrLf
            body &= "<tr><td>Modified Date</td><td>" & moddate & "</td></tr>" & vbCrLf
            body &= "<tr><td>Root Cause</td><td>" & rootcause & "</td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2""><u>Corrective Action</u></td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td colspan=""2"">" & corraction & "</td><td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2""><u>Comments</u></td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td colspan=""2"">" & comments & "</td><td></tr>" & vbCrLf
            body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        Else
            body &= "<tr><td colspan=""2"">No Root Cause Provided</td></tr>" & vbCrLf & vbCrLf
            body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        End If

        'Costs
        body &= "<tr><td colspan=""2""><u>Work Order Costs</u></td></tr>" & vbCrLf & vbCrLf
        'body &= "<tr><td colspan=""2"">&nbsp;</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td colspan=""2""><table>" & vbCrLf & vbCrLf

        body &= "<tr><td width=""120px""><u>Item</u></td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px""><u>Estimated</u></td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px""><u>Actual</u></td></tr>" & vbCrLf & vbCrLf

        esthr = esthr * sqty
        acthr = acthr * sqty
        body &= "<tr><td width=""120px"">Labor Hours</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(esthr, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(acthr, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf

        estcost = estcost * sqty
        actcost = actcost * sqty
        body &= "<tr><td width=""120px"">Labor Costs</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(estcost, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(actcost, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf


        body &= "<tr><td width=""120px"">Parts</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(estmatcost, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(actmatcost, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td width=""120px"">Tools</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(esttoolcost, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(acttoolcost, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td width=""120px"">Lubricants</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(estlubecost, "##############0.00") & "</td>" & vbCrLf & vbCrLf
        body &= "<td width=""100px"">" & Format(actlubecost, "##############0.00") & "</td></tr>" & vbCrLf & vbCrLf


        body &= "</table></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2"">&nbsp;</td><td></tr>" & vbCrLf
        'End Costs

        body &= "<tr><td colspan=""2"">" & tmod.getlbl("cdlbl366", "pmmail.vb") & "<a href='" + urlname + appname + "/appswo/woprint.aspx?typ=wo&wo=" + wonum + "'>" & tmod.getxlbl("xlb107", "pmmail.vb") & " " + wonum + "</td></tr>" & vbCrLf & vbCrLf
        body &= "</table>"
        Return body
    End Function
End Class
