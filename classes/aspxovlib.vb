Public Class aspxovlib
    Dim dlang As New mmenu_utils
    Dim dlab As New aspxovlibvals
    Public Function GetASPXOVLIB(ByVal aspxpage As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Dim lang As String
        Try
            lang = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            lang = dlang.AppDfltLang
        End Try
        Select Case aspxpage
            Case "AddComp.aspx"
                ret = dlab.getAddCompovval(lang, aspxlabel)
            Case "addcomp2.aspx"
                ret = dlab.getaddcomp2ovval(lang, aspxlabel)
            Case "AddFail.aspx"
                ret = dlab.getAddFailovval(lang, aspxlabel)
            Case "AppSetComTab.aspx"
                ret = dlab.getAppSetComTabovval(lang, aspxlabel)
            Case "AppSetpmAppr.aspx"
                ret = dlab.getAppSetpmApprovval(lang, aspxlabel)
            Case "AppSetTaskTabST.aspx"
                ret = dlab.getAppSetTaskTabSTovval(lang, aspxlabel)
            Case "archrationale2.aspx"
                ret = dlab.getarchrationale2ovval(lang, aspxlabel)
            Case "archrationaletpm2.aspx"
                ret = dlab.getarchrationaletpm2ovval(lang, aspxlabel)
            Case "catalog.aspx"
                ret = dlab.getcatalogovval(lang, aspxlabel)
            Case "commontasks.aspx"
                ret = dlab.getcommontasksovval(lang, aspxlabel)
            Case "commontaskstpm.aspx"
                ret = dlab.getcommontaskstpmovval(lang, aspxlabel)
            Case "CompDiv.aspx"
                ret = dlab.getCompDivovval(lang, aspxlabel)
            Case "CompDivGrid.aspx"
                ret = dlab.getCompDivGridovval(lang, aspxlabel)
            Case "complib.aspx"
                ret = dlab.getcomplibovval(lang, aspxlabel)
            Case "complibadd.aspx"
                ret = dlab.getcomplibaddovval(lang, aspxlabel)
            Case "complibedit.aspx"
                ret = dlab.getcomplibeditovval(lang, aspxlabel)
            Case "complibtaskeadittpm.aspx"
                ret = dlab.getcomplibtaskeadittpmovval(lang, aspxlabel)
            Case "complibtasks2.aspx"
                ret = dlab.getcomplibtasks2ovval(lang, aspxlabel)
            Case "complibtasks3.aspx"
                ret = dlab.getcomplibtasks3ovval(lang, aspxlabel)
            Case "complibtasksedit.aspx"
                ret = dlab.getcomplibtaskseditovval(lang, aspxlabel)
            Case "complibtasksedit2.aspx"
                ret = dlab.getcomplibtasksedit2ovval(lang, aspxlabel)
            Case "dragndropno.aspx"
                ret = dlab.getdragndropnoovval(lang, aspxlabel)
            Case "EQBot.aspx"
                ret = dlab.getEQBotovval(lang, aspxlabel)
            Case "eqcopy2.aspx"
                ret = dlab.geteqcopy2ovval(lang, aspxlabel)
            Case "EQCopyMini.aspx"
                ret = dlab.getEQCopyMiniovval(lang, aspxlabel)
            Case "EQCopyMinitpm.aspx"
                ret = dlab.getEQCopyMinitpmovval(lang, aspxlabel)
            Case "EQMain.aspx"
                ret = dlab.getEQMainovval(lang, aspxlabel)
            Case "EqSelect.aspx"
                ret = dlab.getEqSelectovval(lang, aspxlabel)
            Case "eqtab2.aspx"
                ret = dlab.geteqtab2ovval(lang, aspxlabel)
            Case "FormatTest.aspx"
                ret = dlab.getFormatTestovval(lang, aspxlabel)
            Case "FuncDiv.aspx"
                ret = dlab.getFuncDivovval(lang, aspxlabel)
            Case "FuncDivGrid.aspx"
                ret = dlab.getFuncDivGridovval(lang, aspxlabel)
            Case "gtaskfail.aspx"
                ret = dlab.getgtaskfailovval(lang, aspxlabel)
            Case "GTasksFunctpm.aspx"
                ret = dlab.getGTasksFunctpmovval(lang, aspxlabel)
            Case "issueinv.aspx"
                ret = dlab.getissueinvovval(lang, aspxlabel)
            Case "JobPlan.aspx"
                ret = dlab.getJobPlanovval(lang, aspxlabel)
            Case "JobPlanList.aspx"
                ret = dlab.getJobPlanListovval(lang, aspxlabel)
            Case "JobPlanRefReq.aspx"
                ret = dlab.getJobPlanRefReqovval(lang, aspxlabel)
            Case "jpfaillist.aspx"
                ret = dlab.getjpfaillistovval(lang, aspxlabel)
            Case "laborassign.aspx"
                ret = dlab.getlaborassignovval(lang, aspxlabel)
            Case "lubegridedit.aspx"
                ret = dlab.getlubegrideditovval(lang, aspxlabel)
            Case "lubeout.aspx"
                ret = dlab.getlubeoutovval(lang, aspxlabel)
            Case "lubeoutedit.aspx"
                ret = dlab.getlubeouteditovval(lang, aspxlabel)
            Case "lubeoutroutes.aspx"
                ret = dlab.getlubeoutroutesovval(lang, aspxlabel)
            Case "maxsearch.aspx"
                ret = dlab.getmaxsearchovval(lang, aspxlabel)
            Case "measoutedit.aspx"
                ret = dlab.getmeasouteditovval(lang, aspxlabel)
            Case "measoutfly.aspx"
                ret = dlab.getmeasoutflyovval(lang, aspxlabel)
            Case "measoutroutes.aspx"
                ret = dlab.getmeasoutroutesovval(lang, aspxlabel)
            Case "measoutroutesmini.aspx"
                ret = dlab.getmeasoutroutesminiovval(lang, aspxlabel)
            Case "measoutwo.aspx"
                ret = dlab.getmeasoutwoovval(lang, aspxlabel)
            Case "mmenuopts.aspx"
                ret = dlab.getmmenuoptsovval(lang, aspxlabel)
            Case "NCBot.aspx"
                ret = dlab.getNCBotovval(lang, aspxlabel)
            Case "NewMainMenu2.aspx"
                ret = dlab.getNewMainMenu2ovval(lang, aspxlabel)
            Case "PFInt.aspx"
                ret = dlab.getPFIntovval(lang, aspxlabel)
            Case "PFInt_fre.aspx"
                ret = dlab.getPFInt_freovval(lang, aspxlabel)
            Case "PFInt_ger.aspx"
                ret = dlab.getPFInt_gerovval(lang, aspxlabel)
            Case "PFInt_ita.aspx"
                ret = dlab.getPFInt_itaovval(lang, aspxlabel)
            Case "PFInt_spa.aspx"
                ret = dlab.getPFInt_spaovval(lang, aspxlabel)
            Case "PM3OptMain.aspx"
                ret = dlab.getPM3OptMainovval(lang, aspxlabel)
            Case "PMAdjMan.aspx"
                ret = dlab.getPMAdjManovval(lang, aspxlabel)
            Case "PMAdjMantpm.aspx"
                ret = dlab.getPMAdjMantpmovval(lang, aspxlabel)
            Case "PMAltMan.aspx"
                ret = dlab.getPMAltManovval(lang, aspxlabel)
            Case "PMAltMantpm.aspx"
                ret = dlab.getPMAltMantpmovval(lang, aspxlabel)
            Case "pmarchget.aspx"
                ret = dlab.getpmarchgetovval(lang, aspxlabel)
            Case "pmarchgettpm.aspx"
                ret = dlab.getpmarchgettpmovval(lang, aspxlabel)
            Case "pmarchtasks.aspx"
                ret = dlab.getpmarchtasksovval(lang, aspxlabel)
            Case "pmarchtaskstpm.aspx"
                ret = dlab.getpmarchtaskstpmovval(lang, aspxlabel)
            Case "PMDivMan.aspx"
                ret = dlab.getPMDivManovval(lang, aspxlabel)
            Case "PMDivMantpm.aspx"
                ret = dlab.getPMDivMantpmovval(lang, aspxlabel)
            Case "PMFailMan.aspx"
                ret = dlab.getPMFailManovval(lang, aspxlabel)
            Case "PMFailMantpm.aspx"
                ret = dlab.getPMFailMantpmovval(lang, aspxlabel)
            Case "PMGetPMMan.aspx"
                ret = dlab.getPMGetPMManovval(lang, aspxlabel)
            Case "PMGetPMMantpm.aspx"
                ret = dlab.getPMGetPMMantpmovval(lang, aspxlabel)
            Case "PMGetTasksFunc.aspx"
                ret = dlab.getPMGetTasksFuncovval(lang, aspxlabel)
            Case "PMGridMan.aspx"
                ret = dlab.getPMGridManovval(lang, aspxlabel)
            Case "PMGridMantpm.aspx"
                ret = dlab.getPMGridMantpmovval(lang, aspxlabel)
            Case "pmlibnewcodets.aspx"
                ret = dlab.getpmlibnewcodetsovval(lang, aspxlabel)
            Case "pmlibneweqdets.aspx"
                ret = dlab.getpmlibneweqdetsovval(lang, aspxlabel)
            Case "pmlibnewfudets.aspx"
                ret = dlab.getpmlibnewfudetsovval(lang, aspxlabel)
            Case "pmlibnewpmtaskdets.aspx"
                ret = dlab.getpmlibnewpmtaskdetsovval(lang, aspxlabel)
            Case "PMMainMan.aspx"
                ret = dlab.getPMMainManovval(lang, aspxlabel)
            Case "PMMainMantpm.aspx"
                ret = dlab.getPMMainMantpmovval(lang, aspxlabel)
            Case "pmme.aspx"
                ret = dlab.getpmmeovval(lang, aspxlabel)
            Case "PMMeas.aspx"
                ret = dlab.getPMMeasovval(lang, aspxlabel)
            Case "PMMeastpm.aspx"
                ret = dlab.getPMMeastpmovval(lang, aspxlabel)
            Case "pmmetpm.aspx"
                ret = dlab.getpmmetpmovval(lang, aspxlabel)
            Case "PMOptMain.aspx"
                ret = dlab.getPMOptMainovval(lang, aspxlabel)
            Case "PMOptRationale2.aspx"
                ret = dlab.getPMOptRationale2ovval(lang, aspxlabel)
            Case "PMOptRationaleTPM2.aspx"
                ret = dlab.getPMOptRationaleTPM2ovval(lang, aspxlabel)
            Case "PMOptTasks.aspx"
                ret = dlab.getPMOptTasksovval(lang, aspxlabel)
            Case "PMRouteList.aspx"
                ret = dlab.getPMRouteListovval(lang, aspxlabel)
            Case "PMRoutes.aspx"
                ret = dlab.getPMRoutesovval(lang, aspxlabel)
            Case "PMRoutes2.aspx"
                ret = dlab.getPMRoutes2ovval(lang, aspxlabel)
            Case "pmselect.aspx"
                ret = dlab.getpmselectovval(lang, aspxlabel)
            Case "PMTaskDivFunc.aspx"
                ret = dlab.getPMTaskDivFuncovval(lang, aspxlabel)
            Case "pmuploadimage.aspx"
                ret = dlab.getpmuploadimageovval(lang, aspxlabel)
            Case "purchreq.aspx"
                ret = dlab.getpurchreqovval(lang, aspxlabel)
            Case "purchreqedit.aspx"
                ret = dlab.getpurchreqeditovval(lang, aspxlabel)
            Case "query.aspx"
                ret = dlab.getqueryovval(lang, aspxlabel)
            Case "recvinv.aspx"
                ret = dlab.getrecvinvovval(lang, aspxlabel)
            Case "reorderdetails.aspx"
                ret = dlab.getreorderdetailsovval(lang, aspxlabel)
            Case "reports2.aspx"
                ret = dlab.getreports2ovval(lang, aspxlabel)
            Case "Resources.aspx"
                ret = dlab.getResourcesovval(lang, aspxlabel)
            Case "ScheduledTasks.aspx"
                ret = dlab.getScheduledTasksovval(lang, aspxlabel)
            Case "sfail.aspx"
                ret = dlab.getsfailovval(lang, aspxlabel)
            Case "siteassets2.aspx"
                ret = dlab.getsiteassets2ovval(lang, aspxlabel)
            Case "siteassets3.aspx"
                ret = dlab.getsiteassets3ovval(lang, aspxlabel)
            Case "storeroom.aspx"
                ret = dlab.getstoreroomovval(lang, aspxlabel)
            Case "tablesetup.aspx"
                ret = dlab.gettablesetupovval(lang, aspxlabel)
            Case "taskedit2.aspx"
                ret = dlab.gettaskedit2ovval(lang, aspxlabel)
            Case "taskimagepm.aspx"
                ret = dlab.gettaskimagepmovval(lang, aspxlabel)
            Case "taskimagetpm.aspx"
                ret = dlab.gettaskimagetpmovval(lang, aspxlabel)
            Case "taskimagetpmarch.aspx"
                ret = dlab.gettaskimagetpmarchovval(lang, aspxlabel)
            Case "tpmget.aspx"
                ret = dlab.gettpmgetovval(lang, aspxlabel)
            Case "tpmgettasks.aspx"
                ret = dlab.gettpmgettasksovval(lang, aspxlabel)
            Case "tpmgrid.aspx"
                ret = dlab.gettpmgridovval(lang, aspxlabel)
            Case "tpmmeas.aspx"
                ret = dlab.gettpmmeasovval(lang, aspxlabel)
            Case "tpmoprun.aspx"
                ret = dlab.gettpmoprunovval(lang, aspxlabel)
            Case "tpmoptgetmain.aspx"
                ret = dlab.gettpmoptgetmainovval(lang, aspxlabel)
            Case "tpmopttasks.aspx"
                ret = dlab.gettpmopttasksovval(lang, aspxlabel)
            Case "tpmopttasksmain.aspx"
                ret = dlab.gettpmopttasksmainovval(lang, aspxlabel)
            Case "tpmsize.aspx"
                ret = dlab.gettpmsizeovval(lang, aspxlabel)
            Case "tpmtask.aspx"
                ret = dlab.gettpmtaskovval(lang, aspxlabel)
            Case "tpmtasks.aspx"
                ret = dlab.gettpmtasksovval(lang, aspxlabel)
            Case "tpmuploadimage.aspx"
                ret = dlab.gettpmuploadimageovval(lang, aspxlabel)
            Case "tpmwr.aspx"
                ret = dlab.gettpmwrovval(lang, aspxlabel)
            Case "transassign.aspx"
                ret = dlab.gettransassignovval(lang, aspxlabel)
            Case "UserAdmin.aspx"
                ret = dlab.getUserAdminovval(lang, aspxlabel)
            Case "useradmin2.aspx"
                ret = dlab.getuseradmin2ovval(lang, aspxlabel)
            Case "whereused.aspx"
                ret = dlab.getwhereusedovval(lang, aspxlabel)
            Case "woadd.aspx"
                ret = dlab.getwoaddovval(lang, aspxlabel)
            Case "woaltman.aspx"
                ret = dlab.getwoaltmanovval(lang, aspxlabel)
            Case "wodet.aspx"
                ret = dlab.getwodetovval(lang, aspxlabel)
            Case "wofaillist.aspx"
                ret = dlab.getwofaillistovval(lang, aspxlabel)
            Case "wojpfaillist.aspx"
                ret = dlab.getwojpfaillistovval(lang, aspxlabel)
            Case "wolist.aspx"
                ret = dlab.getwolistovval(lang, aspxlabel)
            Case "woman.aspx"
                ret = dlab.getwomanovval(lang, aspxlabel)
            Case "woplans.aspx"
                ret = dlab.getwoplansovval(lang, aspxlabel)
            Case "wradd.aspx"
                ret = dlab.getwraddovval(lang, aspxlabel)
            Case "wrdet.aspx"
                ret = dlab.getwrdetovval(lang, aspxlabel)
            Case "wrlabor.aspx"
                ret = dlab.getwrlaborovval(lang, aspxlabel)
            Case "wrman.aspx"
                ret = dlab.getwrmanovval(lang, aspxlabel)
        End Select
        Return ret
    End Function
End Class
