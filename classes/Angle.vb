

'********************************************************
'*Angle.vb
'********************************************************



Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Collections
Imports System.Math
Imports System.Drawing.Graphics

Public Class Angle
    Private g As Graphics
    Public b As Bitmap
    Private p As System.Web.UI.Page

    Public Sub DrawAngledText(ByVal gr As Graphics, ByVal _
       the_font As Font, ByVal the_brush As Brush, ByVal txt _
       As String, ByVal x As Integer, ByVal y As Integer, _
       ByVal angle_degrees As Single, ByVal y_scale As Single)
        'Try
        ' Translate the point to the origin.
        gr.TranslateTransform(-x, -y, _
            Drawing2D.MatrixOrder.Append)

        ' Rotate through the angle.
        gr.RotateTransform(angle_degrees, _
            Drawing2D.MatrixOrder.Append)

        ' Scale vertically by a factor of Tan(angle).
        Dim angle_radians As Double = angle_degrees * PI / 180
        gr.ScaleTransform(1, y_scale, _
            Drawing2D.MatrixOrder.Append)

        ' Find the inverse angle and rotate back.
        angle_radians = Math.Atan(y_scale * Tan(angle_radians))
        angle_degrees = CSng(angle_radians * 180 / PI)
        gr.RotateTransform(-angle_degrees, _
            Drawing2D.MatrixOrder.Append)

        ' Translate the origin back to the point.
        gr.TranslateTransform(x, y, _
            Drawing2D.MatrixOrder.Append)

        ' Draw the text.
        Dim gstring As Graphics
        gr.TextRenderingHint = gstring.TextRenderingHint.AntiAliasGridFit
        gr.DrawString(txt, the_font, the_brush, x, y)
        'Catch ex As Exception

        'End Try
        p.Response.ContentType = "image/jpeg"
        b.Save(p.Response.OutputStream, ImageFormat.Jpeg)
    End Sub
End Class
