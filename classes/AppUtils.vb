

'********************************************************
'*
'********************************************************



Public Class AppUtils
    Private sessid As String
    Private url As String
    Private tentry As String
    Private ecost As String
    Private acost As String
    Private iuse As String
    Private idte As String
    Private ittt As String
    Private idtet As String
    Private itttt As String
    Public Property TTTEntry() As String
        Get
            Return ttt()
        End Get
        Set(ByVal Value As String)
            ittt = Value
        End Set
    End Property
    Public Property PDTEntry() As String
        Get
            Return dte()
        End Get
        Set(ByVal Value As String)
            idte = Value
        End Set
    End Property
    Public Property TTTTEntry() As String
        Get
            Return tttt()
        End Get
        Set(ByVal Value As String)
            itttt = Value
        End Set
    End Property
    Public Property PDTTEntry() As String
        Get
            Return dtet()
        End Get
        Set(ByVal Value As String)
            idtet = Value
        End Set
    End Property
    Public Property InvEntry() As String
        Get
            Return iuuse()
        End Get
        Set(ByVal Value As String)
            iuse = Value
        End Set
    End Property
    Private Function iuuse() As String
        Dim app As New Utilities
        Dim sql As String
        sql = "select [default] from wovars where [column] = 'invuse'"
        app.Open()
        iuse = app.strScalar(sql)
        app.Dispose()
        Return iuse
    End Function
    Private Function dte() As String
        Dim app As New Utilities
        Dim sql As String
        sql = "select [default] from wovars where [column] = 'pdt'"
        app.Open()
        idte = app.strScalar(sql)
        app.Dispose()
        Return idte
    End Function
    Private Function ttt() As String
        Dim app As New Utilities
        Dim sql As String
        sql = "select [default] from wovars where [column] = 'pttt'"
        app.Open()
        ittt = app.strScalar(sql)
        app.Dispose()
        Return ittt
    End Function
    Private Function dtet() As String
        Dim app As New Utilities
        Dim sql As String
        sql = "select [default] from wovars where [column] = 'pdtt'"
        app.Open()
        idtet = app.strScalar(sql)
        app.Dispose()
        Return idtet
    End Function
    Private Function tttt() As String
        Dim app As New Utilities
        Dim sql As String
        sql = "select [default] from wovars where [column] = 'ptttt'"
        app.Open()
        itttt = app.strScalar(sql)
        app.Dispose()
        Return itttt
    End Function
    Public Property TimeEntry() As String
        Get
            Return tent()
        End Get
        Set(ByVal Value As String)
            tentry = Value
        End Set
    End Property
    Private Function tent() As String
        Dim app As New Utilities
        Dim sql As String
        sql = "select [default] from wovars where [column] = 'timeentry'"
        app.Open()
        tentry = app.strScalar(sql)
        app.Dispose()
        Return tentry
    End Function
    Public Property ActCost() As String
        Get
            Return accost()
        End Get
        Set(ByVal Value As String)
            acost = Value
        End Set
    End Property
    Private Function accost() As String
        Dim app As New Utilities
        Dim sql As String
        sql = "select [default] from wovars where [column] = 'actlabcost'"
        app.Open()
        acost = app.strScalar(sql)
        app.Dispose()
        Return acost
    End Function
    Public Property EstCost() As String
        Get
            Return eccost()
        End Get
        Set(ByVal Value As String)
            ecost = Value
        End Set
    End Property
    Private Function eccost() As String
        Dim app As New Utilities
        Dim sql As String
        sql = "select [default] from wovars where [column] = 'estlabcost'"
        app.Open()
        ecost = app.strScalar(sql)
        app.Dispose()
        Return ecost
    End Function
    Public Property Switch() As String
        Get
            Return url
        End Get
        Set(ByVal Value As String)
            url = Value
        End Set
    End Property
    Public Sub New()
        If System.Configuration.ConfigurationManager.AppSettings.Item("custAppVer").ToUpper = "PM3SW" Then
            url = System.Configuration.ConfigurationManager.AppSettings.Item("custAppUrl").ToString
        Else
            url = "ok"
        End If
    End Sub
    Public Shared Sub LogOut(ByVal sess As String)
        Dim app As New Utilities
        Dim sql As String
        sql = "update logtrack set logouttime = getDate() where sessionid = '" & sess & "'"
        app.Open()
        app.Update(sql)
        app.Dispose()
    End Sub
    Public Sub LogOut()
        sessid = HttpContext.Current.Session.SessionID
        Dim app As New Utilities
        Dim sql As String
        app.Open()
        Try
            Dim username As String = HttpContext.Current.Session("username").ToString()
            sql = "declare @sess int " _
            + "set @sess = (select max(sessid) from logtrack where userid = '" & username & "' and sessionid = '" & sessid & "') " _
            + "update logtrack set logouttime = getDate() where sessid = @sess" ' and userid = '" & username & "'"
            app.Update(sql)
        Catch ex As Exception

        End Try
        app.Dispose()
    End Sub
    Public Property Log(ByVal pg As String) As String
        Get
            Return LogIn(pg)
        End Get
        Set(ByVal Value As String)
            sessid = Value
        End Set
    End Property
    Public Function LogIn(ByVal pg As String)
        sessid = HttpContext.Current.Session.SessionID
        Dim app As New Utilities
        Dim sql As String
        app.Open()
        Try
            Dim username As String = HttpContext.Current.Session("username").ToString()
            sql = "declare @sess int, @dat datetime; set @dat = getDate(); " _
            + "set @sess = (select max(sessid) from logtrack where userid = '" & username & "' and sessionid = '" & sessid & "'); " _
            + "update logtrack set currentpage = '" & pg & "', pageenter = @dat, " _
            + "logouttime = NULL where sessid = @sess; insert into logtrackhist (sessid, apppage, pageenter) values(@sess, '" & pg & "', @dat)"
            app.Update(sql)
        Catch ex As Exception

        End Try
        app.Dispose()
        Return sessid
    End Function
End Class
