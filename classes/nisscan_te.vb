﻿Imports System.Data.SqlClient
Public Class nisscan_te
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sc As New Utilities
    Dim sConfigPosition, sEquipment, sStructureType, sService, sResponsible, sDescription, sPlanningGroup, sPriority As String
    Dim sApprovedBy, sReasonCode, sStartDate, sFinishDate, sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType As String
    Dim iRetroFlag, sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, iSetupTime, iRuntime As String
    Dim iPlannedNumWkrs, sTextBlock, iOperation, sTransactionID, pmid, tpmid, wonum, sup, superid, wa, waid, lc, lcid As String
    Dim MWNO, OPNO, RPRE, UMAT, PCTP, REND As String
    Dim RPDT As String
    Dim UMAS, DOWT, DLY1, DLY2 As String
    Dim EMNO, FCLA, FCL2, FCL3 As String
    Dim retval As String
    Dim CONO As String = "1"
    Dim errcnt As Integer = 0
    Dim isact As String
    '***
    Dim s1flag, s2flag As String

    Dim s3flag, s4flag As String


    '***
    Public Function SendCant(ByVal wonum As String) As String
        sc.Open()
        '***
        s1flag = "0"
        s2flag = "0"
        s3flag = "0"
        s4flag = "0"
        '***
        Try
            isact = System.Configuration.ConfigurationManager.AppSettings("servicemode")

        Catch ex As Exception
            isact = "test"

        End Try
        Dim nodate As Date = sc.CNOW
        sql = "select seed from pmseed where pmtable = 'service'; update pmseed set seed = seed + 1 where pmtable = 'service'"
        sTransactionID = sc.strScalar(sql)
        'need xtra col in wotype for Operation value
        sql = "select e.eqnum, w.targstartdate, w.targcompdate, cast(isnull(w.actlabhrs, 0) as decimal(10,2)) as actlabhrs, pnw = (select count(*) from woassign where wonum = '" & wonum & "'), " _
            + "x.POSEQIP, x.RESPONSIBLE, w.actstart, w.actfinish, w.description, w.worktype, w.wopriority, w.reportedby, wa.workarea, w.wopriority, w.superid, w.leadcraftid, " _
            + "w.targstartdate, w.targcompdate, cast(isnull(w.estlabhrs, 0) as decimal(10,2)) as estlabhrs, cast(isnull(w.estjplabhrs, 0) as decimal(10,2)) as estjplabhrs, w.qty, t.xcol, t.xcol2, w.pmid, w.tpmid " _
            + "from workorder w " _
            + "left join equipment e on e.eqid = w.eqid " _
            + "left join equipment_xtra x on x.eqid = e.eqid " _
            + "left join workareas wa on wa.waid = w.waid " _
            + "left join wotype t on t.wotype = w.worktype " _
            + "where w.wonum = '" & wonum & "'"
        dr = sc.GetRdrData(sql)
        While dr.Read
            superid = dr.Item("superid").ToString
            lcid = dr.Item("leadcraftid").ToString
            sConfigPosition = dr.Item("POSEQIP").ToString
            sEquipment = dr.Item("eqnum").ToString
            '****
            'If sEquipment = "" Then
            'sEquipment = "0"
            'End If
            '****
            'sService = dr.Item("worktype").ToString
            sService = dr.Item("xcol2").ToString
            sResponsible = dr.Item("reportedby").ToString
            sDescription = dr.Item("description").ToString
            '***
            'If sDescription = "" Then
            'sDescription = "NONE"
            'End If
            '***
            sDescription = wonum & " - " & sDescription
            sDescription = Mid(sDescription, 1, 39)
            sPlanningGroup = dr.Item("workarea").ToString
            '***
            'If sPlanningGroup = "" Then
            'sPlanningGroup = "0"
            'End If
            '***
            sPriority = dr.Item("wopriority").ToString
            '***
            'If sPriority = "" Then
            'sPriority = "0"
            'End If
            '***
            sStartDate = dr.Item("actstart").ToString
            If sStartDate <> "" Then
                Try
                    Dim sdate As Date = sStartDate
                    sStartDate = sdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch ex As Exception

                End Try
            End If

            sFinishDate = dr.Item("actfinish").ToString
            RPDT = dr.Item("actfinish").ToString
            If sFinishDate <> "" Then
                Try
                    Dim fdate As Date = sFinishDate
                    Dim rdate As Date = sFinishDate
                    sFinishDate = fdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                    RPDT = rdate.ToString("yyyyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch ex As Exception

                End Try
            End If

            '****ADDED FOR TEST****

            'If sStartDate = "" Then
            'If sFinishDate <> "" Then
            'sStartDate = sFinishDate
            's3flag = "1"
            'Else
            'sStartDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            'sFinishDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            's3flag = "1"
            's4flag = "1"
            'End If
            'End If
            'If sFinishDate = "" Then
            'sFinishDate = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            's4flag = "1"
            'End If
            '****END ADDED FOR TEST****

            'errorCode1 = dr.Item("worktype").ToString
            'errorCode2 = dr.Item("worktype").ToString
            'errorCode3 = dr.Item("worktype").ToString
            iRetroFlag = "1"
            sItemNumber = dr.Item("eqnum").ToString
            '***
            'If sItemNumber = "" Then
            'sItemNumber = "0"
            'End If
            '***
            sLotNumber = "1"
            sFacility = "CAN"
            sRequestedStartDte = dr.Item("targstartdate").ToString
            If sRequestedStartDte <> "" Then
                Try
                    Dim rsfdate As Date = sRequestedStartDte
                    sRequestedStartDte = rsfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch ex As Exception

                End Try
            End If
            sRequestedFinishDte = dr.Item("targcompdate").ToString
            If sRequestedFinishDte <> "" Then
                Try
                    Dim rfdate As Date = sRequestedFinishDte
                    sRequestedFinishDte = rfdate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
                Catch ex As Exception

                End Try
            End If
            '****ADDED FOR TEST****

            'If sRequestedStartDte = "" Then
            'If sRequestedFinishDte <> "" Then
            'sRequestedStartDte = sRequestedFinishDte
            's1flag = "1"
            'Else
            'sRequestedStartDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            'sRequestedFinishDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            's1flag = "1"
            's2flag = "1"
            'End If
            'End If
            'If sRequestedFinishDte = "" Then
            'sRequestedFinishDte = nodate.ToString("yyMMdd", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
            's2flag = "1"
            'End If
            '****END ADDED FOR TEST****
            iSetupTime = dr.Item("estjplabhrs").ToString
            UMAS = dr.Item("estjplabhrs").ToString
            '***
            'If UMAS = "" Then
            'UMAS = "0"
            'End If
            '***
            iRuntime = dr.Item("estlabhrs").ToString
            UMAT = dr.Item("estlabhrs").ToString
            iPlannedNumWkrs = dr.Item("qty").ToString
            sTextBlock = dr.Item("description").ToString
            '***
            'If sTextBlock = "" Then
            'sTextBlock = "NONE"
            'End If
            '***
            iOperation = dr.Item("xcol").ToString
            OPNO = dr.Item("xcol").ToString
            pmid = dr.Item("pmid").ToString
            tpmid = dr.Item("tpmid").ToString
        End While
        dr.Close()
        iPlannedQuantity = "1"
        If iSetupTime = "" Then
            iSetupTime = "0"
            UMAS = "0"
        End If
        If iRuntime = "" Then
            iRuntime = "0"
            UMAT = "0"
        End If
        If iPlannedNumWkrs = "" Then
            iPlannedNumWkrs = "0"
        End If
        Dim ecd1id, ecd2id, ecd3id As String
        If pmid <> "" Then
            sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodesman1 where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodesman1.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodesman1.ecd2id) " _
                + "and pmid = '" & pmid & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                ecd1id = dr.Item("ecd1id").ToString
                ecd2id = dr.Item("ecd2id").ToString
                ecd3id = dr.Item("ecd3id").ToString

            End While
            dr.Close()

        ElseIf tpmid <> "" Then
            sql = "select top 1 ecd1id, ecd2id, ecd3id from pmtaskfailmodestpmman where ecd1id in (select ecd1id from ecd1) " _
                + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = pmtaskfailmodestpmman.ecd1id) " _
                + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = pmtaskfailmodestpmman.ecd2id) " _
                + "and pmid = '" & pmid & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                ecd1id = dr.Item("ecd1id").ToString
                ecd2id = dr.Item("ecd2id").ToString
                ecd3id = dr.Item("ecd3id").ToString

            End While
            dr.Close()

        Else
            sql = "select top 1 ecd1id, ecd2id, ecd3id from wofailmodes1 where ecd1id in (select ecd1id from ecd1) " _
                    + "and ecd2id in (select e2.ecd2id from ecd2 e2 where e2.ecd1id = wofailmodes1.ecd1id) " _
                    + "and ecd3id in (select e3.ecd3id from ecd3 e3 where e3.ecd2id = wofailmodes1.ecd2id) " _
                    + "and wonum = '" & wonum & "'"
            dr = sc.GetRdrData(sql)
            While dr.Read
                ecd1id = dr.Item("ecd1id").ToString
                ecd2id = dr.Item("ecd2id").ToString
                ecd3id = dr.Item("ecd3id").ToString

            End While
            dr.Close()

        End If

        sql = "select top 1 e1.ecd1, e2.ecd2, e3.ecd3 " _
                + "from ecd1 e1 " _
                + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
                + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
                + "where e1.ecd1id = '" & ecd1id & "' and e2.ecd2id = '" & ecd2id & "' and e3.ecd3id = '" & ecd3id & "'"
        dr = sc.GetRdrData(sql)
        While dr.Read
            sErrorCode1 = dr.Item("ecd1").ToString
            sErrorCode2 = dr.Item("ecd2").ToString
            sErrorCode3 = dr.Item("ecd3").ToString
            FCLA = dr.Item("ecd1").ToString
            FCL2 = dr.Item("ecd2").ToString
            FCL3 = dr.Item("ecd3").ToString
        End While
        dr.Close()

        '***ADDED FOR TEST***
        'If sErrorCode1 = "" Then
        'sErrorCode1 = "0"
        'End If
        'If sErrorCode2 = "" Then
        'sErrorCode2 = "0"
        'End If
        'If sErrorCode3 = "" Then
        'sErrorCode3 = "0"
        'End If

        'If sComplaintType = "" Then
        'sComplaintType = "0"
        'End If
        '***END***

        'sReasonCode = "0"
        'sTxt1 = "0"
        'sTxt2 = "0"
        'sApprovedBy = sResponsible
        'sStructureType = "0"

        If sResponsible = "" Then
            If superid <> "" Then
                sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & superid & "'"
                dr = sc.GetRdrData(sql)
                While dr.Read
                    sup = dr.Item("empno").ToString
                    waid = dr.Item("waid").ToString
                    wa = dr.Item("workarea").ToString
                End While
                dr.Close()
                If sup <> "" Then
                    sResponsible = sup
                    If wa <> "" Then
                        If sPlanningGroup = "" Then
                            sPlanningGroup = wa
                        End If

                    Else
                        If waid <> "" And sPlanningGroup = "" Then
                            sql = "select workarea from workareas where waid = '" & waid & "'"
                            Try
                                wa = sc.strScalar(sql)
                            Catch ex As Exception
                                wa = ""
                            End Try
                            If wa <> "" And sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            Else
                                'start with lcid, end with no lcid option at bottom
                                If lcid <> "" Then
                                    sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & lcid & "'"
                                    dr = sc.GetRdrData(sql)
                                    While dr.Read
                                        lc = dr.Item("empno").ToString
                                        waid = dr.Item("waid").ToString
                                        wa = dr.Item("workarea").ToString
                                    End While
                                    dr.Close()
                                    If lc <> "" Then
                                        sResponsible = lc
                                        If wa <> "" And sPlanningGroup = "" Then
                                            If sPlanningGroup = "" Then
                                                sPlanningGroup = wa
                                            End If
                                        Else
                                            If waid <> "" And sPlanningGroup = "" Then
                                                sql = "select workarea from workareas where waid = '" & waid & "'"
                                                Try
                                                    wa = sc.strScalar(sql)
                                                Catch ex As Exception
                                                    wa = ""
                                                End Try
                                                If wa <> "" And sPlanningGroup = "" Then
                                                    If sPlanningGroup = "" Then
                                                        sPlanningGroup = wa
                                                    End If
                                                Else
                                                    'not sure if there any option left here
                                                End If
                                            End If
                                        End If
                                    End If
                                Else
                                    'bottom option here
                                    sql = "select top 1 u.empno, u.waid, u.workarea from wolabtrans w " _
                                    + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                                    dr = sc.GetRdrData(sql)
                                    While dr.Read
                                        lc = dr.Item("empno").ToString
                                        waid = dr.Item("waid").ToString
                                        wa = dr.Item("workarea").ToString
                                    End While
                                    dr.Close()
                                    If lc <> "" Then
                                        sResponsible = lc
                                        If wa <> "" And sPlanningGroup = "" Then
                                            If sPlanningGroup = "" Then
                                                sPlanningGroup = wa
                                            End If
                                        Else
                                            If waid <> "" And sPlanningGroup = "" Then
                                                sql = "select workarea from workareas where waid = '" & waid & "'"
                                                Try
                                                    wa = sc.strScalar(sql)
                                                Catch ex As Exception
                                                    wa = ""
                                                End Try
                                                If wa <> "" And sPlanningGroup = "" Then
                                                    If sPlanningGroup = "" Then
                                                        sPlanningGroup = wa
                                                    End If
                                                Else
                                                    'no more options at this point?
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    If lcid <> "" Then
                        sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & lcid & "'"
                        dr = sc.GetRdrData(sql)
                        While dr.Read
                            lc = dr.Item("empno").ToString
                            waid = dr.Item("waid").ToString
                            wa = dr.Item("workarea").ToString
                        End While
                        dr.Close()
                        If lc <> "" Then
                            sResponsible = lc
                            If wa <> "" And sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            Else
                                If waid <> "" And sPlanningGroup = "" Then
                                    sql = "select workarea from workareas where waid = '" & waid & "'"
                                    Try
                                        wa = sc.strScalar(sql)
                                    Catch ex As Exception
                                        wa = ""
                                    End Try
                                    If wa <> "" And sPlanningGroup = "" Then
                                        If sPlanningGroup = "" Then
                                            sPlanningGroup = wa
                                        End If
                                    Else

                                    End If
                                End If
                            End If
                        End If
                    Else
                        'bottom option here
                        sql = "select top 1 u.empno, u.waid, u.workarea from wolabtrans w " _
                    + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                        dr = sc.GetRdrData(sql)
                        While dr.Read
                            lc = dr.Item("empno").ToString
                            waid = dr.Item("waid").ToString
                            wa = dr.Item("workarea").ToString
                        End While
                        dr.Close()
                        If lc <> "" Then
                            sResponsible = lc
                            If wa <> "" And sPlanningGroup = "" Then
                                If sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            Else
                                If waid <> "" And sPlanningGroup = "" Then
                                    sql = "select workarea from workareas where waid = '" & waid & "'"
                                    Try
                                        wa = sc.strScalar(sql)
                                    Catch ex As Exception
                                        wa = ""
                                    End Try
                                    If wa <> "" And sPlanningGroup = "" Then
                                        sPlanningGroup = wa
                                    Else
                                        'no more options at this point?
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            ElseIf lcid <> "" Then
                sql = "select superid from pmsysusers where userid = '" & lcid & "'"
                Try
                    superid = sc.strScalar(sql)
                Catch ex As Exception
                    superid = ""
                End Try
                If superid <> "" Then
                    sql = "select top 1 empno, waid, workarea from pmsysusers where userid = '" & superid & "'"
                    dr = sc.GetRdrData(sql)
                    While dr.Read
                        sup = dr.Item("empno").ToString
                        waid = dr.Item("waid").ToString
                        wa = dr.Item("workarea").ToString
                    End While
                    dr.Close()
                    If sup <> "" Then
                        sResponsible = sup
                        If wa <> "" And sPlanningGroup = "" Then
                            If sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            End If
                        Else
                            If waid <> "" And sPlanningGroup = "" Then
                                sql = "select workarea from workareas where waid = '" & waid & "'"
                                Try
                                    wa = sc.strScalar(sql)
                                Catch ex As Exception
                                    wa = ""
                                End Try
                                If wa <> "" And sPlanningGroup = "" Then
                                    If sPlanningGroup = "" Then
                                        sPlanningGroup = wa
                                    End If
                                Else

                                End If
                            End If
                        End If
                    Else
                        'use bottom option here
                        sql = "select top 1 u.superid from wolabtrans w " _
                + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' and u.superid is not null"
                        Try
                            superid = sc.strScalar(sql)
                        Catch ex As Exception
                            superid = ""
                        End Try
                        If sup <> "" Then
                            sResponsible = sup
                            If wa <> "" And sPlanningGroup = "" Then
                                If sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            Else
                                If waid <> "" And sPlanningGroup = "" Then
                                    sql = "select workarea from workareas where waid = '" & waid & "'"
                                    Try
                                        wa = sc.strScalar(sql)
                                    Catch ex As Exception
                                        wa = ""
                                    End Try
                                    If wa <> "" And sPlanningGroup = "" Then
                                        If sPlanningGroup = "" Then
                                            sPlanningGroup = wa
                                        End If
                                    Else

                                    End If
                                End If
                            End If
                        Else
                            sql = "select top 1 u.empno, u.waid, u.workarea from wolabtrans w " _
                            + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                            dr = sc.GetRdrData(sql)
                            While dr.Read
                                lc = dr.Item("empno").ToString
                                waid = dr.Item("waid").ToString
                                wa = dr.Item("workarea").ToString
                            End While
                            dr.Close()
                            If lc <> "" Then
                                sResponsible = lc
                                If wa <> "" And sPlanningGroup = "" Then
                                    If sPlanningGroup = "" Then
                                        sPlanningGroup = wa
                                    End If
                                Else
                                    If waid <> "" And sPlanningGroup = "" Then
                                        sql = "select workarea from workareas where waid = '" & waid & "'"
                                        Try
                                            wa = sc.strScalar(sql)
                                        Catch ex As Exception
                                            wa = ""
                                        End Try
                                        If wa <> "" And sPlanningGroup = "" Then
                                            If sPlanningGroup = "" Then
                                                sPlanningGroup = wa
                                            End If
                                        Else
                                            'no more options at this point?
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    'use bottom option here
                    sql = "select top 1 u.superid from wolabtrans w " _
                + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' and u.superid is not null"
                    Try
                        superid = sc.strScalar(sql)
                    Catch ex As Exception
                        superid = ""
                    End Try
                    If sup <> "" Then
                        sResponsible = sup
                        If wa <> "" And sPlanningGroup = "" Then
                            If sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            End If
                        Else
                            If waid <> "" And sPlanningGroup = "" Then
                                sql = "select workarea from workareas where waid = '" & waid & "'"
                                Try
                                    wa = sc.strScalar(sql)
                                Catch ex As Exception
                                    wa = ""
                                End Try
                                If wa <> "" And sPlanningGroup = "" Then
                                    If sPlanningGroup = "" Then
                                        sPlanningGroup = wa
                                    End If
                                Else

                                End If
                            End If
                        End If
                    Else
                        sql = "select top 1 u.empno, u.waid, u.workarea from wolabtrans w " _
                        + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                        dr = sc.GetRdrData(sql)
                        While dr.Read
                            lc = dr.Item("empno").ToString
                            waid = dr.Item("waid").ToString
                            wa = dr.Item("workarea").ToString
                        End While
                        dr.Close()
                        If lc <> "" Then
                            sResponsible = lc
                            If wa <> "" And sPlanningGroup = "" Then
                                If sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            Else
                                If waid <> "" And sPlanningGroup = "" Then
                                    sql = "select workarea from workareas where waid = '" & waid & "'"
                                    Try
                                        wa = sc.strScalar(sql)
                                    Catch ex As Exception
                                        wa = ""
                                    End Try
                                    If wa <> "" And sPlanningGroup = "" Then
                                        If sPlanningGroup = "" Then
                                            sPlanningGroup = wa
                                        End If
                                    Else
                                        'no more options at this point?
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                sql = "select top 1 u.superid from wolabtrans w " _
                + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' and u.superid is not null"
                Try
                    superid = sc.strScalar(sql)
                Catch ex As Exception
                    superid = ""
                End Try
                If sup <> "" Then
                    sResponsible = sup
                    If wa <> "" And sPlanningGroup = "" Then
                        If sPlanningGroup = "" Then
                            sPlanningGroup = wa
                        End If
                    Else
                        If waid <> "" And sPlanningGroup = "" Then
                            sql = "select workarea from workareas where waid = '" & waid & "'"
                            Try
                                wa = sc.strScalar(sql)
                            Catch ex As Exception
                                wa = ""
                            End Try
                            If wa <> "" And sPlanningGroup = "" Then
                                If sPlanningGroup = "" Then
                                    sPlanningGroup = wa
                                End If
                            Else

                            End If
                        End If
                    End If
                Else
                    sql = "select top 1 u.empno, u.waid, u.workarea from wolabtrans w " _
                    + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                    dr = sc.GetRdrData(sql)
                    While dr.Read
                        lc = dr.Item("empno").ToString
                        waid = dr.Item("waid").ToString
                        wa = dr.Item("workarea").ToString
                    End While
                    dr.Close()
                    If lc <> "" Then
                        sResponsible = lc
                        If wa <> "" And sPlanningGroup = "" Then
                            If sPlanningGroup = "" Then
                                sPlanningGroup = wa
                            End If
                        Else
                            If waid <> "" And sPlanningGroup = "" Then
                                sql = "select workarea from workareas where waid = '" & waid & "'"
                                Try
                                    wa = sc.strScalar(sql)
                                Catch ex As Exception
                                    wa = ""
                                End Try
                                If wa <> "" And sPlanningGroup = "" Then
                                    If sPlanningGroup = "" Then
                                        sPlanningGroup = wa
                                    End If
                                Else
                                    'no more options at this point?
                                End If
                            End If
                        End If
                    End If
                End If

            End If
        End If

        Dim cansend As New Service1
        Try
            cansend.Url = System.Configuration.ConfigurationManager.AppSettings("service1")

        Catch ex As Exception
            cansend.Url = "http://ncsmiwta/Services/CMMS/OpenWorkOrder/Service.asmx"

        End Try

        Dim retstring As String
        'sTransactionID,sConfigPosition,sEquipment,sStructureType,sService,sResponsible,iOperation,sDescription,
        'sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, sTxt1, sTxt2, sErrorCode1,
        'sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, sItemNumber, sLotNumber, iPlannedQuantity, sFacility,
        'sRequestedStartDte, sRequestedFinishDte, iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock
        retstring = cansend.OpenWorkOrder(sTransactionID, sConfigPosition, sEquipment, sStructureType, sService, sResponsible, iOperation, sDescription, _
                                          sPlanningGroup, sPriority, sApprovedBy, sReasonCode, sStartDate, sFinishDate, _
                                          sTxt1, sTxt2, sErrorCode1, sErrorCode2, sErrorCode3, sComplaintType, iRetroFlag, _
                                          sItemNumber, sLotNumber, iPlannedQuantity, sFacility, sRequestedStartDte, sRequestedFinishDte, _
                                          iSetupTime, iRuntime, iPlannedNumWkrs, sTextBlock)
        'tdgo.InnerHtml = retstring
        Dim retarr() As String = retstring.Split(",")
        Dim retwonum, retval, errmsg As String
        Try
            Dim woarr() As String = retarr(1).Split(":")
            retwonum = woarr(1)
            Dim valarr() As String = retarr(2).Split(":")
            retval = valarr(1)
            Dim msgarr() As String = retarr(3).Split(":")
            errmsg = msgarr(1)
            MWNO = RTrim(retwonum)
            MWNO = LTrim(MWNO)
            retwonum = MWNO
            If retval = 1 Then
                sql = "insert into woout (fuswonum, sdate, retwonum, retval, errmsg) " _
                + "values ('" & wonum & "',getdate(),'" & retwonum & "','" & retval & "','" & errmsg & "')"
                sc.Update(sql)
                Try
                    sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
                Catch ex As Exception
                    errcnt = 1
                End Try

            Else
                If isact = "active" Then
                    sql = "update workorder set retwonum = '" & retwonum & "' where wonum = '" & wonum & "'"
                    sc.Update(sql)
                End If
                'sql = "update workorder set retwonum = '" & retwonum & "' where wonum = '" & wonum & "'"
                'sc.Update(sql)
                Try
                    sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
                Catch ex As Exception
                    errcnt = 1
                End Try

            End If
            If errcnt = 0 Then
                Return "0"
            Else
                Return "1"
            End If

            'lblMWNO.Value = retwonum
        Catch ex As Exception
            'Try
            'sql = "update workorder set retwonum = '" & retwonum & "' where wonum = '" & wonum & "'"
            'sc.Update(sql)
            'Catch ex0 As Exception

            'End Try
            Try
                sql = "insert into woout (fuswonum, sdate, errovr) " _
                    + "values ('" & wonum & "',getdate(),'" & retstring & "')"
                sc.Update(sql)
            Catch ex1 As Exception

            End Try
            Return "1"
            'Dim strMessage As String = "Problem with OpenWorkOrder Return Value - Please Stop and Contact LAI"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        'sendtime(wonum, MWNO, OPNO, RPDT, UMAS, FCLA, FCL2, FCL3)
        'PageNumber = txtpg.Value
        'getwr(PageNumber)
        '[POSEQIP],[ITEMGRP],[STATUS], [SUBPROCESS], [RESPONSIBLE], [FIXEDASSET], [PRIORITY], [CRITICALITYCLS],[PLANNINGPOSITION], [EQID], [structureType]
        'configPosition [POSEQIP]
        'Equipment [eqnum] 
        'structureType ??? ***added to equipment_xtra  [structureType]
        'Service ***is this work type??? [worktype]
        'Responsible [RESPONSIBLE]
        'Description ***first 40 characters of work description??? [description] 
        'planningGroup ***[PLANNINGPOSITION] or planning area??? 
        'Priority [wopriority]
        'approvedBy ***not in Fusion - can we use a default??? 
        'reasonCode ??? 
        'startDate [actstart] 
        'finishDate [actfinish] 
        'txt1 ??? 
        'txt2 ??? 
        '***Nissan has many PM records with multiple error code values
        '***Will use top 1 for testing
        'errorCode1 [ecd1] 
        'errorCode2 [ecd2] 
        'errorCode3 [ecd3] 
        'complaintType ???
        'retroFlag ***always 1??? 
        'itemNumber ??? 
        'lotNumber ??? 
        'plannedQuantity ??? 
        'Facility ***always CAN 
        'requestedStartDte [targstartdate] 
        'requestedFinishDte [targcompdate]
        'setupTime ??? 
        'Runtime ***total time??? [actlabhrs]
        'plannedNumWkrs [pnw] ***calculated field 
        'textBlock ??? 
        'Operation ???
        sc.Dispose()
    End Function
    Private Sub sendtime(ByVal wonum As String, ByVal MWNO As String, ByVal OPNO As String, ByVal RPDT As String, ByVal UMAS As String, _
                         ByVal FCLA As String, ByVal FCL2 As String, ByVal FCL3 As String)
        'need RPRE, EMNO, DOWT (responsible for reporting, emp no, down time 
        '? PCTP (reg or over), REND (manual completion flag)
        '? DLY1 = 0.00, DLY2 = 0.00 (should be numeric, no more info)
        'default PCTP as 3 for now
        'CONO set at top
        Dim DOWTP As String
        sql = "select cast(isnull((totaldown * 60), '0.00') as decimal(10,2)) as 'dowt', cast(isnull((totaldownp * 60), '0.00') as decimal(10,2)) as 'dowtp' from eqhist where wonum = '" & wonum & "'"
        'DOWT = sc.strScalar(sql)
        dr = sc.GetRdrData(sql)
        While dr.Read
            DOWT = dr.Item("dowt").ToString
            DOWTP = dr.Item("dowtp").ToString
        End While
        dr.Close()
        DLY1 = DOWTP

        If DOWT = "" Then
            DOWT = "0"
        End If
        If DOWTP = "" Then
            DOWTP = "0"
            DLY1 = "0"
        End If

        sql = "select cast(sum(isnull((w.minutes_man / 60),0)) as decimal(10,2)) as minutes_man, w.laborid, u.username, u.empno, u.uid, u.islabor from wolabtrans w " _
            + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "' group by w.laborid, u.username, u.empno, u.uid, u.islabor "
        Dim empcnt As Integer = 0
        Dim ierrcnt As Integer = 0
        Dim laborid, username, popstring, uid, islabor As String
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = sc.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim row As DataRow
        For Each row In dt.Rows
            empcnt += 1
            uid = row("uid").ToString
            islabor = row("islabor").ToString
            username = row("username").ToString
            EMNO = row("empno").ToString
            If islabor = "0" Then
                EMNO = uid
            End If
            laborid = row("laborid").ToString
            UMAT = row("minutes_man").ToString
            If UMAT = "" Then
                UMAT = "0"
            End If
            'Dim UMAT_n As Long
            'Try
            'UMAT_n = System.Convert.ToDecimal(UMAT)
            'UMAT_n = Math.Round(UMAT_n, 2)
            'UMAT = UMAT_n

            'Catch ex As Exception

            'End Try
            If EMNO = "" Then
                ierrcnt += 1
                'need to collect data for pop-up here
                If popstring = "" Then
                    popstring = empcnt + "~" + username + "~" + laborid + "~" + wonum + "~" + DOWT + "~" + MWNO + "~" + OPNO + "~" + RPDT + "~" + UMAT + "~" + UMAS + "~" + FCLA + "~" + FCL2 + "~" + FCL3 + "~" _
                        + EMNO
                Else
                    popstring += "," + empcnt + "~" + username + "~" + laborid + "~" + wonum + "~" + DOWT + "~" + "~" + MWNO + "~" + OPNO + "~" + RPDT + "~" + UMAT + "~" + UMAS + "~" + FCLA + "~" + FCL2 + "~" + FCL3 + "~" _
                        + EMNO
                End If
            Else
                PCTP = "3"
                '*** set REND to 0 for test
                REND = "0"
                'DLY1 = "0.00"
                DLY2 = "0.00"
                '***set RPRE to 0 for test
                sql = "select top 1 u.empno from wolabtrans w " _
            + "left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                Try
                    RPRE = "" 'sc.strScalar(sql)
                Catch ex As Exception
                    RPRE = ""
                    'Dim strMessage As String = "No Labor Entry Found to Use for sResponsible"
                    'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    'Exit Sub
                End Try
                If RPRE = "" Then
                    'Dim strMessage As String = "No Labor Entry Found to Use for sResponsible"
                    'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    'Exit Sub
                End If
                'sWorkOrdNum,sOpNum,sWorkDate,sUserId,sEmpNumbr,sLabrHrs,sCostTyp,sCloseOperat,sLabrSetupTim,
                'sFailurCls, sErrCde2, sErrCde3, sDownTim, sDelyTim1, sDelyTim2
                timeentry(wonum, laborid, MWNO, OPNO, RPDT, RPRE, EMNO, UMAT, PCTP, REND, UMAS, FCLA, FCL2, FCL3, DOWT, DLY1, DLY2)

            End If
            'check data and send - use else in above or separate proc?
            'error checking on return?

        Next
        If ierrcnt <> 0 Then
            'lblerrcnt.Value = errcnt
            errcnt = 1
        End If
    End Sub
    Private Sub timeentry(ByVal wonum As String, ByVal laborid As String, _
        ByVal MWNO As String, _
        ByVal OPNO As String, _
        ByVal RPDT As String, _
        ByVal RPRE As String, _
        ByVal EMNO As String, _
        ByVal UMAT As String, _
        ByVal PCTP As String, _
        ByVal REND As String, _
        ByVal UMAS As String, _
        ByVal FCLA As String, _
        ByVal FCL2 As String, _
        ByVal FCL3 As String, _
        ByVal DOWT As String, _
        ByVal DLY1 As String, _
        ByVal DLY2 As String)
        '***TEST***
        FCLA = ""
        FCL2 = ""
        FCL3 = ""
        '*********
        Try
            isact = System.Configuration.ConfigurationManager.AppSettings("servicemode")

        Catch ex As Exception
            isact = "test"

        End Try
        PCTP = "3"
        REND = "1"
        'DLY1 = "0.00"
        DLY2 = "0.00"
        Dim cansend As New Service2
        Try
            cansend.Url = System.Configuration.ConfigurationManager.AppSettings("service2")

        Catch ex As Exception
            cansend.Url = "http://ncsmiwta/Services/CMMS/TimeEntry/TimeEntry/Service.asmx"

        End Try

        Dim retstring As String
        Dim ds As New DataSet

        Dim te(0) As TimeEntryInputVar
        te(0).sOpNum = OPNO
        te(0).sWorkDate = RPDT
        te(0).sUserId = RPRE
        te(0).sEmpNumbr = EMNO
        te(0).sLabrHrs = UMAT
        te(0).sCostTyp = PCTP
        te(0).sCloseOperat = REND
        te(0).sLabrSetupTim = UMAS
        te(0).sFailurCls = FCLA
        te(0).sErrCde2 = FCL2
        te(0).sErrCde3 = FCL3
        te(0).sDownTim = DOWT
        te(0).sDelyTim1 = DLY1
        te(0).sDelyTim2 = DLY2

        Dim sWorkOrdNum As String = MWNO
        sWorkOrdNum = RTrim(sWorkOrdNum)
        sWorkOrdNum = LTrim(sWorkOrdNum)

        ds = cansend.TimeEntry(sWorkOrdNum, te)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        Dim drM As DataRow
        Dim errval, errval1, errval2 As String
        For Each drM In dt.Select()
            errval = drM(0).ToString
            errval1 = drM(1).ToString
            Try
                errval2 = drM(2).ToString
            Catch ex As Exception
                errval2 = ""
            End Try

            If retstring = "" Then
                If errval2 <> "" Then
                    retstring = errval + " - " + errval1 + " - " + errval2
                Else
                    retstring = errval + " - " + errval1
                End If

            Else
                If errval2 <> "" Then
                    retstring += ", " + errval + " - " + errval1 + " - " + errval2
                Else
                    retstring += ", " + errval + " - " + errval1
                End If

            End If
        Next
        'tdgo.InnerHtml += "<br />" & EMNO & " - " & retstring
        Dim retval, errmsg As String
        Try
            'Dim retarr() As String = retstring.Split(",")
            'Dim valarr() As String = retarr(0).Split(":")
            'retval = valarr(1)
            'Dim msgarr() As String = retarr(1).Split(":")
            'errmsg = msgarr(1)
            If retstring <> "OK" Then
                sql = "insert into woout_te (fuswonum, sdate, retwonum, errmsg) " _
                + "values ('" & wonum & "',getdate(),'" & MWNO & "','" & retstring & "')"
                sc.Update(sql)
            Else
                If isact = "active" Then
                    'sql = "select sum(isnull(w.minutes_man,0)) as minutes_man, w.laborid, u.username, u.empno from wolabtrans w left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                    sql = "update wolabtrans set retwonum = '" & MWNO & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
                    sc.Update(sql)
                End If
                ''sql = "select sum(isnull(w.minutes_man,0)) as minutes_man, w.laborid, u.username, u.empno from wolabtrans w left join pmsysusers u on u.userid = w.laborid where w.wonum = '" & wonum & "'"
                'sql = "update wolabtrans set retwonum = '" & MWNO & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
                'sc.Update(sql)
            End If
        Catch ex As Exception
            'Try
            'sql = "update wolabtrans set retwonum = '" & MWNO & "' where wonum = '" & wonum & "' and laborid = '" & laborid & "'"
            'sc.Update(sql)
            'Catch ex0 As Exception

            'End Try
            Try
                sql = "insert into woout_te (fuswonum, sdate, errovr) " _
                    + "values ('" & wonum & "',getdate(),'" & retstring & "')"
                sc.Update(sql)
            Catch ex1 As Exception

            End Try
            errcnt = 1
            'Dim strMessage As String = "Problem with TimeEntry Return Value - Please Stop and Contact LAI"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
End Class
