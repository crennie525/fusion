﻿Imports System.IO
Public Class officedown
    Private docret As String
    Public Property DXTS(ByVal fname As String) As String
        Get
            Return docx2doc(fname)
        End Get
        Set(ByVal Value As String)
            docret = Value
        End Set
    End Property
    Public Property DTW(ByVal fname As String, ByVal ret As String) As String
        Get
            Return doc2web(fname, ret)
        End Get
        Set(ByVal Value As String)
            docret = Value
        End Set
    End Property
    Public Property EXTS(ByVal fname As String) As String
        Get
            Return xlx2xls(fname)
        End Get
        Set(ByVal Value As String)
            docret = Value
        End Set
    End Property
    Public Property ETW(ByVal fname As String, ByVal ret As String) As String
        Get
            Return xs2web(fname, ret)
        End Get
        Set(ByVal Value As String)
            docret = Value
        End Set
    End Property
    Private Function docx2doc(ByVal fname As String) As String
        Dim objApp As Object
        Dim strDocName As String
        Dim strDocSaveName As String
        objApp = CreateObject("Word.Application")
        strDocName = fname
        Dim indx As Integer = fname.IndexOf(".docx")
        Dim dupflg As Integer = 0
        Dim dupchk As Integer = 1
        Dim probi As Integer = 0
        Dim ret As String = ""
        If indx <> -1 Then
            Dim FilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
            strDocName = FilePath
            If File.Exists(FilePath) Then
                fname = Mid(fname, 1, indx) & ".doc"
                If File.Exists(FilePath) Then
                    dupflg = 1
                End If
                If dupflg = 0 Then
                    Dim NewFilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    strDocSaveName = NewFilePath
                    Try
                        objApp.Documents.Open(strDocName)
                        objApp.Activedocument.SaveAs(FileName:=strDocSaveName, FileFormat:=0)
                        objApp.Activedocument.Close()
                        ret = "eok~" & fname
                    Catch ex As Exception
                        ret = "eapp"
                    End Try 
                Else
                    fname = Mid(FilePath, 1, indx) & "(1).doc"
                    Dim DupFilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    While dupflg = 1
                        If File.Exists(DupFilePath) Then
                            dupchk += 1
                            fname = Mid(FilePath, 1, indx) & "(" & dupchk & ").doc"
                            DupFilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                        Else
                            dupflg = 0
                        End If
                        If dupchk = 5 Then
                            probi = 1
                            Exit While
                        End If
                    End While
                    If probi = 0 Then
                        Try
                            DupFilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                            strDocSaveName = DupFilePath
                            objApp.Documents.Open(strDocName)
                            objApp.Activedocument.SaveAs(FileName:=strDocSaveName, FileFormat:=0)
                            objApp.Activedocument.Close()
                            ret = "eok~" & fname
                        Catch ex As Exception
                            ret = "eapp"
                        End Try
                    Else
                        ret = "edup"
                    End If
                End If
            Else
                ret = "enex"
            End If
        Else
            ret = "exst"
        End If
        Dim retar() As String = ret.Split("~")
        If retar(0) = "eok" Then
            'if ok then try to create html version
            ret = doc2web(fname, ret)
        End If
        Return ret
    End Function
    Private Function doc2web(ByVal fname As String, ByVal ret As String) As String
        Dim objApp As Object
        Dim FilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
        Dim strDocName As String = FilePath
        Dim strDocSaveName As String
        objApp = CreateObject("Word.Application")
        Dim indx As Integer = fname.IndexOf(".doc")
        fname = Mid(fname, 1, indx) & ".htm"
        FilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
        Dim dupflg As Integer = 0
        Dim dupchk As Integer = 1
        Dim probi As Integer = 0
        If File.Exists(FilePath) Then
            dupflg = 1
            fname = Mid(FilePath, 1, indx) & "(1).htm"
            Dim DupFilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
            While dupflg = 1
                If File.Exists(DupFilePath) Then
                    dupflg = 0
                Else
                    dupchk += 1
                    fname = Mid(FilePath, 1, indx) & "(" & dupchk & ").htm"
                End If
                If dupchk = 5 Then
                    probi = 1
                    Exit While
                End If
            End While
            If probi = 0 Then
                Try
                    DupFilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    strDocSaveName = DupFilePath
                    objApp.Documents.Open(strDocName)
                    objApp.Activedocument.SaveAs(FileName:=strDocSaveName, FileFormat:=0)
                    objApp.Activedocument.Close()
                    ret = "~e2ok~" & fname
                Catch ex As Exception
                    ret = ret & "~e2app"
                End Try
            Else
                ret = ret & "~edup"
            End If
        Else
            Try
                strDocSaveName = FilePath
                objApp.Documents.Open(strDocName)
                objApp.Activedocument.SaveAs(FileName:=strDocSaveName, FileFormat:=8)
                objApp.Activedocument.Close()
                ret = ret & "~e2ok~" & fname
            Catch ex As Exception
                ret = ret & "~e2app"
            End Try
        End If
        Return ret
    End Function
    Private Function xlx2xls(ByVal fname As String) As String
        Dim objApp As Object
        Dim strDocName As String
        Dim strDocSaveName As String
        objApp = CreateObject("Excel.Application")
        strDocName = fname
        Dim indx As Integer = fname.IndexOf(".xlsx")
        Dim dupflg As Integer = 0
        Dim dupchk As Integer = 1
        Dim probi As Integer = 0
        Dim ret As String = ""
        If indx <> -1 Then
            Dim FilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
            strDocName = FilePath
            If File.Exists(FilePath) Then
                fname = Mid(fname, 1, indx) & ".xls"
                If File.Exists(FilePath) Then
                    dupflg = 1
                End If
                If dupflg = 0 Then
                    Dim NewFilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    strDocSaveName = NewFilePath
                    Try
                        objApp.workbooks.Open(strDocName)
                        objApp.activeworkbook.SaveAs(FileName:=strDocSaveName, FileFormat:=0)
                        objApp.activeworkbook.Close()
                        ret = "eok~" & fname
                    Catch ex As Exception
                        ret = "eapp"
                    End Try
                Else
                    fname = Mid(FilePath, 1, indx) & "(1).xls"
                    Dim DupFilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    While dupflg = 1
                        If File.Exists(DupFilePath) Then
                            dupchk += 1
                            fname = Mid(FilePath, 1, indx) & "(" & dupchk & ").xls"
                            DupFilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                        Else
                            dupflg = 0
                        End If
                        If dupchk = 5 Then
                            probi = 1
                            Exit While
                        End If
                    End While
                    If probi = 0 Then
                        Try
                            DupFilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                            strDocSaveName = DupFilePath
                            objApp.workbooks.Open(strDocName)
                            objApp.activeworkbook.SaveAs(FileName:=strDocSaveName, FileFormat:=0)
                            objApp.activeworkbook.Close()
                            ret = "eok~" & fname
                        Catch ex As Exception
                            ret = "eapp"
                        End Try
                    Else
                        ret = "edup"
                    End If
                End If
            Else
                ret = "enex"
            End If
        Else
            ret = "exst"
        End If
        Dim retar() As String = ret.Split("~")
        If retar(0) = "eok" Then
            'if ok then try to create html version
            ret = xs2web(fname, ret)
        End If
        Return ret
    End Function
    Private Sub xlsx2xls()
        Dim objApp As Object
        Dim strDocName As String
        Dim strDocSaveName As String
        objApp = CreateObject("Excel.Application")
        strDocName = "C:\Temp\temp.xlsx"
        strDocSaveName = "C:\Temp\temp.htm"
        objApp.workbooks.Open(strDocName)
        objApp.activeworkbook.SaveAs(FileName:=strDocSaveName, FileFormat:=43)
        objApp.activeworkbook.Close()
    End Sub
    Private Function xs2web(ByVal fname As String, ByVal ret As String) As String
        Dim objApp As Object
        Dim FilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
        Dim strDocName As String = FilePath
        Dim strDocSaveName As String
        objApp = CreateObject("Excel.Application")
        Dim indx As Integer = fname.IndexOf(".xls")
        fname = Mid(fname, 1, indx) & ".htm"
        FilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
        Dim dupflg As Integer = 0
        Dim dupchk As Integer = 1
        Dim probi As Integer = 0
        If File.Exists(FilePath) Then
            dupflg = 1
            fname = Mid(FilePath, 1, indx) & "(1).htm"
            Dim DupFilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
            While dupflg = 1
                If File.Exists(DupFilePath) Then
                    dupflg = 0
                Else
                    dupchk += 1
                    fname = Mid(FilePath, 1, indx) & "(" & dupchk & ").htm"
                End If
                If dupchk = 5 Then
                    probi = 1
                    Exit While
                End If
            End While
            If probi = 0 Then
                Try
                    DupFilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    strDocSaveName = DupFilePath
                    objApp.workbooks.Open(strDocName)
                    objApp.activeworkbook.SaveAs(FileName:=strDocSaveName, FileFormat:=0)
                    objApp.activeworkbook.Close()
                    ret = "~e2ok~" & fname
                Catch ex As Exception
                    ret = ret & "~e2app"
                End Try
            Else
                ret = ret & "~edup"
            End If
        Else
            Try
                strDocSaveName = FilePath
                objApp.workbooks.Open(strDocName)
                objApp.activeworkbook.SaveAs(FileName:=strDocSaveName, FileFormat:=8)
                objApp.activeworkbook.Close()
                ret = ret & "~e2ok~" & fname
            Catch ex As Exception
                ret = ret & "~e2app"
            End Try
        End If
        Return ret
    End Function
    Private Sub xls2web()
        Dim objApp As Object
        Dim strDocName As String
        Dim strDocSaveName As String
        objApp = CreateObject("Excel.Application")
        strDocName = "C:\Temp\temp.xlsx"
        strDocSaveName = "C:\Temp\temp.htm"
        objApp.workbooks.Open(strDocName)
        objApp.activeworkbook.SaveAs(FileName:=strDocSaveName, FileFormat:=44)
        objApp.activeworkbook.Close()
    End Sub
End Class
