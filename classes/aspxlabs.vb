Public Class aspxlabs
    Dim dlang As New mmenu_utils
    Dim dlab As New aspxlabvals
    Public Function GetASPXPage(ByVal aspxpage As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Dim lang As String
        Try
            lang = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            lang = dlang.AppDfltLang
        End Try
        Select Case aspxpage
            Case "PFAdjtpm.aspx"
                ret = dlab.getPFAdjtpmval(lang, aspxlabel)
            Case "PMLeadMan.aspx"
                ret = dlab.getPMLeadManval(lang, aspxlabel)
            Case "tasksched.aspx"
                ret = dlab.gettaskschedval(lang, aspxlabel)
            Case "PMMainMantpm.aspx"
                ret = dlab.getPMMainMantpmval(lang, aspxlabel)
            Case "complibedit.aspx"
                ret = dlab.getcomplibeditval(lang, aspxlabel)
            Case "contractmain.aspx"
                ret = dlab.getcontractmainval(lang, aspxlabel)
            Case "wodet.aspx"
                ret = dlab.getwodetval(lang, aspxlabel)
            Case "AppSetWoCharge.aspx"
                ret = dlab.getAppSetWoChargeval(lang, aspxlabel)
            Case "complibcodets.aspx"
                ret = dlab.getcomplibcodetsval(lang, aspxlabel)
            Case "dragndropno.aspx"
                ret = dlab.getdragndropnoval(lang, aspxlabel)
            Case "PMFailMan.aspx"
                ret = dlab.getPMFailManval(lang, aspxlabel)
            Case "woarch.aspx"
                ret = dlab.getwoarchval(lang, aspxlabel)
            Case "inprog.aspx"
                ret = dlab.getinprogval(lang, aspxlabel)
            Case "optTaskToolListtpm.aspx"
                ret = dlab.getoptTaskToolListtpmval(lang, aspxlabel)
            Case "gsubarch.aspx"
                ret = dlab.getgsubarchval(lang, aspxlabel)
            Case "measroutesaddbot.aspx"
                ret = dlab.getmeasroutesaddbotval(lang, aspxlabel)
            Case "gsubarchtpm.aspx"
                ret = dlab.getgsubarchtpmval(lang, aspxlabel)
            Case "wojpact.aspx"
                ret = dlab.getwojpactval(lang, aspxlabel)
            Case "pmlibnewcodets.aspx"
                ret = dlab.getpmlibnewcodetsval(lang, aspxlabel)
            Case "wrman.aspx"
                ret = dlab.getwrmanval(lang, aspxlabel)
            Case "coimg.aspx"
                ret = dlab.getcoimgval(lang, aspxlabel)
            Case "pmlibcodets.aspx"
                ret = dlab.getpmlibcodetsval(lang, aspxlabel)
            Case "archrationaletpm.aspx"
                ret = dlab.getarchrationaletpmval(lang, aspxlabel)
            Case "AddEditFuncPop.aspx"
                ret = dlab.getAddEditFuncPopval(lang, aspxlabel)
            Case "AppSetInvLists.aspx"
                ret = dlab.getAppSetInvListsval(lang, aspxlabel)
            Case "lubeoutedit.aspx"
                ret = dlab.getlubeouteditval(lang, aspxlabel)
            Case "devTaskPartListtpm.aspx"
                ret = dlab.getdevTaskPartListtpmval(lang, aspxlabel)
            Case "AppSetWoChargeType.aspx"
                ret = dlab.getAppSetWoChargeTypeval(lang, aspxlabel)
            Case "PFAdj.aspx"
                ret = dlab.getPFAdjval(lang, aspxlabel)
            Case "PMGridMantpm.aspx"
                ret = dlab.getPMGridMantpmval(lang, aspxlabel)
            Case "AppSetEmail.aspx"
                ret = dlab.getAppSetEmailval(lang, aspxlabel)
            Case "tpmgettasks.aspx"
                ret = dlab.gettpmgettasksval(lang, aspxlabel)
            Case "adjinv.aspx"
                ret = dlab.getadjinvval(lang, aspxlabel)
            Case "wrassign.aspx"
                ret = dlab.getwrassignval(lang, aspxlabel)
            Case "GTasksFunctpm.aspx"
                ret = dlab.getGTasksFunctpmval(lang, aspxlabel)
            Case "draghelp2.aspx"
                ret = dlab.getdraghelp2val(lang, aspxlabel)
            Case "wojpfaillist.aspx"
                ret = dlab.getwojpfaillistval(lang, aspxlabel)
            Case "pmactmmaintpm.aspx"
                ret = dlab.getpmactmmaintpmval(lang, aspxlabel)
            Case "PMRouteList.aspx"
                ret = dlab.getPMRouteListval(lang, aspxlabel)
            Case "JobPlan.aspx"
                ret = dlab.getJobPlanval(lang, aspxlabel)
            Case "wrskills.aspx"
                ret = dlab.getwrskillsval(lang, aspxlabel)
            Case "maxsearch.aspx"
                ret = dlab.getmaxsearchval(lang, aspxlabel)
            Case "fudets.aspx"
                ret = dlab.getfudetsval(lang, aspxlabel)
            Case "upload.aspx"
                ret = dlab.getuploadval(lang, aspxlabel)
            Case "AppSetApprPlan.aspx"
                ret = dlab.getAppSetApprPlanval(lang, aspxlabel)
            Case "pmMeasure.aspx"
                ret = dlab.getpmMeasureval(lang, aspxlabel)
            Case "UserApps.aspx"
                ret = dlab.getUserAppsval(lang, aspxlabel)
            Case "assetclass.aspx"
                ret = dlab.getassetclassval(lang, aspxlabel)
            Case "addcomp2.aspx"
                ret = dlab.getaddcomp2val(lang, aspxlabel)
            Case "pmSiteSkills.aspx"
                ret = dlab.getpmSiteSkillsval(lang, aspxlabel)
            Case "PM3OptMain.aspx"
                ret = dlab.getPM3OptMainval(lang, aspxlabel)
            Case "archtasktoollist.aspx"
                ret = dlab.getarchtasktoollistval(lang, aspxlabel)
            Case "measoutroutes.aspx"
                ret = dlab.getmeasoutroutesval(lang, aspxlabel)
            Case "PGEMShtml.aspx"
                ret = dlab.getPGEMShtmlval(lang, aspxlabel)
            Case "JobLoc.aspx"
                ret = dlab.getJobLocval(lang, aspxlabel)
            Case "measouthistedit.aspx"
                ret = dlab.getmeasouthisteditval(lang, aspxlabel)
            Case "wofmme2.aspx"
                ret = dlab.getwofmme2val(lang, aspxlabel)
            Case "devTaskToolList.aspx"
                ret = dlab.getdevTaskToolListval(lang, aspxlabel)
            Case "pmlocmain.aspx"
                ret = dlab.getpmlocmainval(lang, aspxlabel)
            Case "ECRAdm.aspx"
                ret = dlab.getECRAdmval(lang, aspxlabel)
            Case "pmarchgettpm.aspx"
                ret = dlab.getpmarchgettpmval(lang, aspxlabel)
            Case "EditProfile.aspx"
                ret = dlab.getEditProfileval(lang, aspxlabel)
            Case "PMRoutesMain.aspx"
                ret = dlab.getPMRoutesMainval(lang, aspxlabel)
            Case "wocosts.aspx"
                ret = dlab.getwocostsval(lang, aspxlabel)
            Case "cclasslookup.aspx"
                ret = dlab.getcclasslookupval(lang, aspxlabel)
            Case "proclib.aspx"
                ret = dlab.getproclibval(lang, aspxlabel)
            Case "weights.aspx"
                ret = dlab.getweightsval(lang, aspxlabel)
            Case "Confirm.aspx"
                ret = dlab.getConfirmval(lang, aspxlabel)
            Case "TaskNotes.aspx"
                ret = dlab.getTaskNotesval(lang, aspxlabel)
            Case "complibtasksedit.aspx"
                ret = dlab.getcomplibtaskseditval(lang, aspxlabel)
            Case "pmlibtpmtaskdets.aspx"
                ret = dlab.getpmlibtpmtaskdetsval(lang, aspxlabel)
            Case "pmarchmain.aspx"
                ret = dlab.getpmarchmainval(lang, aspxlabel)
            Case "comp.aspx"
                ret = dlab.getcompval(lang, aspxlabel)
            Case "tpmmain.aspx"
                ret = dlab.gettpmmainval(lang, aspxlabel)
            Case "AppSetApprPrev.aspx"
                ret = dlab.getAppSetApprPrevval(lang, aspxlabel)
            Case "comptasks.aspx"
                ret = dlab.getcomptasksval(lang, aspxlabel)
            Case "purchreq.aspx"
                ret = dlab.getpurchreqval(lang, aspxlabel)
            Case "sparepartlist.aspx"
                ret = dlab.getsparepartlistval(lang, aspxlabel)
            Case "lubeoutroutes.aspx"
                ret = dlab.getlubeoutroutesval(lang, aspxlabel)
            Case "measureoutedit.aspx"
                ret = dlab.getmeasureouteditval(lang, aspxlabel)
            Case "AppSetEqTabMain.aspx"
                ret = dlab.getAppSetEqTabMainval(lang, aspxlabel)
            Case "PMFailMantpm.aspx"
                ret = dlab.getPMFailMantpmval(lang, aspxlabel)
            Case "taskimagepm.aspx"
                ret = dlab.gettaskimagepmval(lang, aspxlabel)
            Case "PMOptMain.aspx"
                ret = dlab.getPMOptMainval(lang, aspxlabel)
            Case "wrselect.aspx"
                ret = dlab.getwrselectval(lang, aspxlabel)
            Case "PMSetAdjMantpm.aspx"
                ret = dlab.getPMSetAdjMantpmval(lang, aspxlabel)
            Case "wojpactmmain.aspx"
                ret = dlab.getwojpactmmainval(lang, aspxlabel)
            Case "PMSetAdjManPREtpm.aspx"
                ret = dlab.getPMSetAdjManPREtpmval(lang, aspxlabel)
            Case "JobPlanMain.aspx"
                ret = dlab.getJobPlanMainval(lang, aspxlabel)
            Case "purchreqedit.aspx"
                ret = dlab.getpurchreqeditval(lang, aspxlabel)
            Case "archtasklubelisttpm.aspx"
                ret = dlab.getarchtasklubelisttpmval(lang, aspxlabel)
            Case "pmuploadimage.aspx"
                ret = dlab.getpmuploadimageval(lang, aspxlabel)
            Case "chngpass.aspx"
                ret = dlab.getchngpassval(lang, aspxlabel)
            Case "EditProfile_v2.aspx"
                ret = dlab.getEditProfile_v2val(lang, aspxlabel)
            Case "AppSetTaskTabPT.aspx"
                ret = dlab.getAppSetTaskTabPTval(lang, aspxlabel)
            Case "tpmopttasksmain.aspx"
                ret = dlab.gettpmopttasksmainval(lang, aspxlabel)
            Case "FuncDivGrid.aspx"
                ret = dlab.getFuncDivGridval(lang, aspxlabel)
            Case "measureout.aspx"
                ret = dlab.getmeasureoutval(lang, aspxlabel)
            Case "transassign.aspx"
                ret = dlab.gettransassignval(lang, aspxlabel)
            Case "AppSetSYS.aspx"
                ret = dlab.getAppSetSYSval(lang, aspxlabel)
            Case "funcedit.aspx"
                ret = dlab.getfunceditval(lang, aspxlabel)
            Case "appsetwovars.aspx"
                ret = dlab.getappsetwovarsval(lang, aspxlabel)
            Case "PMSuperMan.aspx"
                ret = dlab.getPMSuperManval(lang, aspxlabel)
            Case "pmtasksched.aspx"
                ret = dlab.getpmtaskschedval(lang, aspxlabel)
            Case "compclass.aspx"
                ret = dlab.getcompclassval(lang, aspxlabel)
            Case "userlic.aspx"
                ret = dlab.getuserlicval(lang, aspxlabel)
            Case "GTasksFunc2.aspx"
                ret = dlab.getGTasksFunc2val(lang, aspxlabel)
            Case "pmfm.aspx"
                ret = dlab.getpmfmval(lang, aspxlabel)
            Case "WoChargeEntry.aspx"
                ret = dlab.getWoChargeEntryval(lang, aspxlabel)
            Case "wojpedit.aspx"
                ret = dlab.getwojpeditval(lang, aspxlabel)
            Case "transassn.aspx"
                ret = dlab.gettransassnval(lang, aspxlabel)
            Case "PMManager.aspx"
                ret = dlab.getPMManagerval(lang, aspxlabel)
            Case "AppSetWoType.aspx"
                ret = dlab.getAppSetWoTypeval(lang, aspxlabel)
            Case "measoutedit.aspx"
                ret = dlab.getmeasouteditval(lang, aspxlabel)
            Case "AppSetWo.aspx"
                ret = dlab.getAppSetWoval(lang, aspxlabel)
            Case "EqSelect.aspx"
                ret = dlab.getEqSelectval(lang, aspxlabel)
            Case "Resources.aspx"
                ret = dlab.getResourcesval(lang, aspxlabel)
            Case "archtasklubelist.aspx"
                ret = dlab.getarchtasklubelistval(lang, aspxlabel)
            Case "wofaillist.aspx"
                ret = dlab.getwofaillistval(lang, aspxlabel)
            Case "lubegridedit.aspx"
                ret = dlab.getlubegrideditval(lang, aspxlabel)
            Case "lubeout.aspx"
                ret = dlab.getlubeoutval(lang, aspxlabel)
            Case "gtaskfail.aspx"
                ret = dlab.getgtaskfailval(lang, aspxlabel)
            Case "EQCopyMini.aspx"
                ret = dlab.getEQCopyMinival(lang, aspxlabel)
            Case "pmarchtasks.aspx"
                ret = dlab.getpmarchtasksval(lang, aspxlabel)
            Case "LocLook.aspx"
                ret = dlab.getLocLookval(lang, aspxlabel)
            Case "pmTaskMeasurements.aspx"
                ret = dlab.getpmTaskMeasurementsval(lang, aspxlabel)
            Case "pmlibfudets.aspx"
                ret = dlab.getpmlibfudetsval(lang, aspxlabel)
            Case "lotlook.aspx"
                ret = dlab.getlotlookval(lang, aspxlabel)
            Case "PMGetPMMantpm.aspx"
                ret = dlab.getPMGetPMMantpmval(lang, aspxlabel)
            Case "tpmuploadimage.aspx"
                ret = dlab.gettpmuploadimageval(lang, aspxlabel)
            Case "tpmsize.aspx"
                ret = dlab.gettpmsizeval(lang, aspxlabel)
            Case "pmSiteFM.aspx"
                ret = dlab.getpmSiteFMval(lang, aspxlabel)
            Case "JobPlanList.aspx"
                ret = dlab.getJobPlanListval(lang, aspxlabel)
            Case "EqGrid.aspx"
                ret = dlab.getEqGridval(lang, aspxlabel)
            Case "complibsrchkey.aspx"
                ret = dlab.getcomplibsrchkeyval(lang, aspxlabel)
            Case "AppSetDeptTab.aspx"
                ret = dlab.getAppSetDeptTabval(lang, aspxlabel)
            Case "bulkproc.aspx"
                ret = dlab.getbulkprocval(lang, aspxlabel)
            Case "manhelp.aspx"
                ret = dlab.getmanhelpval(lang, aspxlabel)
            Case "woactmain.aspx"
                ret = dlab.getwoactmainval(lang, aspxlabel)
            Case "saadmin.aspx"
                ret = dlab.getsaadminval(lang, aspxlabel)
            Case "optTaskToolList.aspx"
                ret = dlab.getoptTaskToolListval(lang, aspxlabel)
            Case "ForumMain.aspx"
                ret = dlab.getForumMainval(lang, aspxlabel)
            Case "wojpfm2.aspx"
                ret = dlab.getwojpfm2val(lang, aspxlabel)
            Case "PMGetTasksFunc.aspx"
                ret = dlab.getPMGetTasksFuncval(lang, aspxlabel)
            Case "UserAdmin.aspx"
                ret = dlab.getUserAdminval(lang, aspxlabel)
            Case "pmarchtaskstpm.aspx"
                ret = dlab.getpmarchtaskstpmval(lang, aspxlabel)
            Case "NewMainMenu2.aspx"
                ret = dlab.getNewMainMenu2val(lang, aspxlabel)
            Case "getPMReport.aspx"
                ret = dlab.getgetPMReportval(lang, aspxlabel)
            Case "devTaskPartList.aspx"
                ret = dlab.getdevTaskPartListval(lang, aspxlabel)
            Case "compnotasks.aspx"
                ret = dlab.getcompnotasksval(lang, aspxlabel)
            Case "optTaskLubeListtpm.aspx"
                ret = dlab.getoptTaskLubeListtpmval(lang, aspxlabel)
            Case "PMMeas.aspx"
                ret = dlab.getPMMeasval(lang, aspxlabel)
            Case "wotemp.aspx"
                ret = dlab.getwotempval(lang, aspxlabel)
            Case "funclist2.aspx"
                ret = dlab.getfunclist2val(lang, aspxlabel)
            Case "AppSetCellTab.aspx"
                ret = dlab.getAppSetCellTabval(lang, aspxlabel)
            Case "AppSetLabor.aspx"
                ret = dlab.getAppSetLaborval(lang, aspxlabel)
            Case "AppSetpmAppr.aspx"
                ret = dlab.getAppSetpmApprval(lang, aspxlabel)
            Case "CompDiv.aspx"
                ret = dlab.getCompDivval(lang, aspxlabel)
            Case "LocArch.aspx"
                ret = dlab.getLocArchval(lang, aspxlabel)
            Case "origrevtpm.aspx"
                ret = dlab.getorigrevtpmval(lang, aspxlabel)
            Case "complibtasksedit2.aspx"
                ret = dlab.getcomplibtasksedit2val(lang, aspxlabel)
            Case "GSub.aspx"
                ret = dlab.getGSubval(lang, aspxlabel)
            Case "pmlibeqdets.aspx"
                ret = dlab.getpmlibeqdetsval(lang, aspxlabel)
            Case "PMDivMantpm.aspx"
                ret = dlab.getPMDivMantpmval(lang, aspxlabel)
            Case "PMGridMan.aspx"
                ret = dlab.getPMGridManval(lang, aspxlabel)
            Case "eqlookup.aspx"
                ret = dlab.geteqlookupval(lang, aspxlabel)
            Case "funclistdet.aspx"
                ret = dlab.getfunclistdetval(lang, aspxlabel)
            Case "NCBot.aspx"
                ret = dlab.getNCBotval(lang, aspxlabel)
            Case "levels.aspx"
                ret = dlab.getlevelsval(lang, aspxlabel)
            Case "LAIOrigRev.aspx"
                ret = dlab.getLAIOrigRevval(lang, aspxlabel)
            Case "tpmgrid.aspx"
                ret = dlab.gettpmgridval(lang, aspxlabel)
            Case "AppSetTaskTabTT.aspx"
                ret = dlab.getAppSetTaskTabTTval(lang, aspxlabel)
            Case "woactm.aspx"
                ret = dlab.getwoactmval(lang, aspxlabel)
            Case "uploadtpm.aspx"
                ret = dlab.getuploadtpmval(lang, aspxlabel)
            Case "archtaskpartlist.aspx"
                ret = dlab.getarchtaskpartlistval(lang, aspxlabel)
            Case "partsmain.aspx"
                ret = dlab.getpartsmainval(lang, aspxlabel)
            Case "PMOptRationale2.aspx"
                ret = dlab.getPMOptRationale2val(lang, aspxlabel)
            Case "complookup.aspx"
                ret = dlab.getcomplookupval(lang, aspxlabel)
            Case "CompDivGrid.aspx"
                ret = dlab.getCompDivGridval(lang, aspxlabel)
            Case "tpmopttasks.aspx"
                ret = dlab.gettpmopttasksval(lang, aspxlabel)
            Case "wrdet.aspx"
                ret = dlab.getwrdetval(lang, aspxlabel)
            Case "ECMError.aspx"
                ret = dlab.getECMErrorval(lang, aspxlabel)
            Case "Prime_Selection.aspx"
                ret = dlab.getPrime_Selectionval(lang, aspxlabel)
            Case "AddComp.aspx"
                ret = dlab.getAddCompval(lang, aspxlabel)
            Case "tpmoptgetmain.aspx"
                ret = dlab.gettpmoptgetmainval(lang, aspxlabel)
            Case "PMRouteLU.aspx"
                ret = dlab.getPMRouteLUval(lang, aspxlabel)
            Case "whereused.aspx"
                ret = dlab.getwhereusedval(lang, aspxlabel)
            Case "PMOptTasks.aspx"
                ret = dlab.getPMOptTasksval(lang, aspxlabel)
            Case "tpmwr.aspx"
                ret = dlab.gettpmwrval(lang, aspxlabel)
            Case "complibtaskeadittpm.aspx"
                ret = dlab.getcomplibtaskeadittpmval(lang, aspxlabel)
            Case "reorderdetails.aspx"
                ret = dlab.getreorderdetailsval(lang, aspxlabel)
            Case "devTaskToolListtpm.aspx"
                ret = dlab.getdevTaskToolListtpmval(lang, aspxlabel)
            Case "wrlabor.aspx"
                ret = dlab.getwrlaborval(lang, aspxlabel)
            Case "pmactmain.aspx"
                ret = dlab.getpmactmainval(lang, aspxlabel)
            Case "wrmain.aspx"
                ret = dlab.getwrmainval(lang, aspxlabel)
            Case "wojpfm.aspx"
                ret = dlab.getwojpfmval(lang, aspxlabel)
            Case "optTaskPartListtpm.aspx"
                ret = dlab.getoptTaskPartListtpmval(lang, aspxlabel)
            Case "woman.aspx"
                ret = dlab.getwomanval(lang, aspxlabel)
            Case "wojpact2.aspx"
                ret = dlab.getwojpact2val(lang, aspxlabel)
            Case "woadd.aspx"
                ret = dlab.getwoaddval(lang, aspxlabel)
            Case "measflyadd.aspx"
                ret = dlab.getmeasflyaddval(lang, aspxlabel)
            Case "LocDets.aspx"
                ret = dlab.getLocDetsval(lang, aspxlabel)
            Case "FuncCopyMini.aspx"
                ret = dlab.getFuncCopyMinival(lang, aspxlabel)
            Case "PMRoutes.aspx"
                ret = dlab.getPMRoutesval(lang, aspxlabel)
            Case "AppSetLocTab.aspx"
                ret = dlab.getAppSetLocTabval(lang, aspxlabel)
            Case "pmlibpmtaskdets.aspx"
                ret = dlab.getpmlibpmtaskdetsval(lang, aspxlabel)
            Case "pmactmaintpm.aspx"
                ret = dlab.getpmactmaintpmval(lang, aspxlabel)
            Case "PMApproval.aspx"
                ret = dlab.getPMApprovalval(lang, aspxlabel)
            Case "pmme.aspx"
                ret = dlab.getpmmeval(lang, aspxlabel)
            Case "matlookup.aspx"
                ret = dlab.getmatlookupval(lang, aspxlabel)
            Case "measoutadd.aspx"
                ret = dlab.getmeasoutaddval(lang, aspxlabel)
            Case "PMSetAdjManPRE.aspx"
                ret = dlab.getPMSetAdjManPREval(lang, aspxlabel)
            Case "PMDivMan.aspx"
                ret = dlab.getPMDivManval(lang, aspxlabel)
            Case "measalerttpm.aspx"
                ret = dlab.getmeasalerttpmval(lang, aspxlabel)
            Case "pmuploadimage1.aspx"
                ret = dlab.getpmuploadimage1val(lang, aspxlabel)
            Case "pmmetpm.aspx"
                ret = dlab.getpmmetpmval(lang, aspxlabel)
            Case "CompGrid.aspx"
                ret = dlab.getCompGridval(lang, aspxlabel)
            Case "tpmoprun.aspx"
                ret = dlab.gettpmoprunval(lang, aspxlabel)
            Case "lubesmain.aspx"
                ret = dlab.getlubesmainval(lang, aspxlabel)
            Case "archtaskpartlisttpm.aspx"
                ret = dlab.getarchtaskpartlisttpmval(lang, aspxlabel)
            Case "csub.aspx"
                ret = dlab.getcsubval(lang, aspxlabel)
            Case "pmactmmain.aspx"
                ret = dlab.getpmactmmainval(lang, aspxlabel)
            Case "compclassmain.aspx"
                ret = dlab.getcompclassmainval(lang, aspxlabel)
            Case "GTasksFunctpm2.aspx"
                ret = dlab.getGTasksFunctpm2val(lang, aspxlabel)
            Case "eqimg.aspx"
                ret = dlab.geteqimgval(lang, aspxlabel)
            Case "failnotasks.aspx"
                ret = dlab.getfailnotasksval(lang, aspxlabel)
            Case "measuremain.aspx"
                ret = dlab.getmeasuremainval(lang, aspxlabel)
            Case "PMAdjMantpm.aspx"
                ret = dlab.getPMAdjMantpmval(lang, aspxlabel)
            Case "wrexpd.aspx"
                ret = dlab.getwrexpdval(lang, aspxlabel)
            Case "SQLAdmin.aspx"
                ret = dlab.getSQLAdminval(lang, aspxlabel)
            Case "eqcopy2.aspx"
                ret = dlab.geteqcopy2val(lang, aspxlabel)
            Case "CustomCMMShtmlTPM.aspx"
                ret = dlab.getCustomCMMShtmlTPMval(lang, aspxlabel)
            Case "compush.aspx"
                ret = dlab.getcompushval(lang, aspxlabel)
            Case "sparepartpickarch.aspx"
                ret = dlab.getsparepartpickarchval(lang, aspxlabel)
            Case "PMTaskDivFunc.aspx"
                ret = dlab.getPMTaskDivFuncval(lang, aspxlabel)
            Case "tablesetup.aspx"
                ret = dlab.gettablesetupval(lang, aspxlabel)
            Case "PFInt.aspx"
                ret = dlab.getPFIntval(lang, aspxlabel)
            Case "pmarchget.aspx"
                ret = dlab.getpmarchgetval(lang, aspxlabel)
            Case "PMArchRT.aspx"
                ret = dlab.getPMArchRTval(lang, aspxlabel)
            Case "PopQty.aspx"
                ret = dlab.getPopQtyval(lang, aspxlabel)
            Case "PMMainMan.aspx"
                ret = dlab.getPMMainManval(lang, aspxlabel)
            Case "TPMManager.aspx"
                ret = dlab.getTPMManagerval(lang, aspxlabel)
            Case "TaskNotestpm.aspx"
                ret = dlab.getTaskNotestpmval(lang, aspxlabel)
            Case "AppSetApprLLTasks.aspx"
                ret = dlab.getAppSetApprLLTasksval(lang, aspxlabel)
            Case "eqtab2.aspx"
                ret = dlab.geteqtab2val(lang, aspxlabel)
            Case "archrationaletpm2.aspx"
                ret = dlab.getarchrationaletpm2val(lang, aspxlabel)
            Case "jpfaillist.aspx"
                ret = dlab.getjpfaillistval(lang, aspxlabel)
            Case "invmain.aspx"
                ret = dlab.getinvmainval(lang, aspxlabel)
            Case "commontasks.aspx"
                ret = dlab.getcommontasksval(lang, aspxlabel)
            Case "proclibtpm.aspx"
                ret = dlab.getproclibtpmval(lang, aspxlabel)
            Case "funclist3.aspx"
                ret = dlab.getfunclist3val(lang, aspxlabel)
            Case "pmlibnewtpmtaskdets.aspx"
                ret = dlab.getpmlibnewtpmtaskdetsval(lang, aspxlabel)
            Case "reclookup.aspx"
                ret = dlab.getreclookupval(lang, aspxlabel)
            Case "pmarchgrid.aspx"
                ret = dlab.getpmarchgridval(lang, aspxlabel)
            Case "laborassign.aspx"
                ret = dlab.getlaborassignval(lang, aspxlabel)
            Case "eqdets.aspx"
                ret = dlab.geteqdetsval(lang, aspxlabel)
            Case "PMTaskDivFuncGrid.aspx"
                ret = dlab.getPMTaskDivFuncGridval(lang, aspxlabel)
            Case "PMAttachtpm.aspx"
                ret = dlab.getPMAttachtpmval(lang, aspxlabel)
            Case "AppSet.aspx"
                ret = dlab.getAppSetval(lang, aspxlabel)
            Case "PMArchMan.aspx"
                ret = dlab.getPMArchManval(lang, aspxlabel)
            Case "reports2.aspx"
                ret = dlab.getreports2val(lang, aspxlabel)
            Case "PMSetAdjMan.aspx"
                ret = dlab.getPMSetAdjManval(lang, aspxlabel)
            Case "superassign.aspx"
                ret = dlab.getsuperassignval(lang, aspxlabel)
            Case "wojpactm.aspx"
                ret = dlab.getwojpactmval(lang, aspxlabel)
            Case "LocLook1.aspx"
                ret = dlab.getLocLook1val(lang, aspxlabel)
            Case "complibuploadimage.aspx"
                ret = dlab.getcomplibuploadimageval(lang, aspxlabel)
            Case "fuimg.aspx"
                ret = dlab.getfuimgval(lang, aspxlabel)
            Case "AppSetTaskTabST.aspx"
                ret = dlab.getAppSetTaskTabSTval(lang, aspxlabel)
            Case "FormatTest.aspx"
                ret = dlab.getFormatTestval(lang, aspxlabel)
            Case "womain.aspx"
                ret = dlab.getwomainval(lang, aspxlabel)
            Case "tpmmeas.aspx"
                ret = dlab.gettpmmeasval(lang, aspxlabel)
            Case "invmainmenu.aspx"
                ret = dlab.getinvmainmenuval(lang, aspxlabel)
            Case "NCGrid.aspx"
                ret = dlab.getNCGridval(lang, aspxlabel)
            Case "LocEq.aspx"
                ret = dlab.getLocEqval(lang, aspxlabel)
            Case "devTaskLubeListtpm.aspx"
                ret = dlab.getdevTaskLubeListtpmval(lang, aspxlabel)
            Case "tpmtask.aspx"
                ret = dlab.gettpmtaskval(lang, aspxlabel)
            Case "cget.aspx"
                ret = dlab.getcgetval(lang, aspxlabel)
            Case "pmselect.aspx"
                ret = dlab.getpmselectval(lang, aspxlabel)
            Case "pmMeasureTypes.aspx"
                ret = dlab.getpmMeasureTypesval(lang, aspxlabel)
            Case "origrev.aspx"
                ret = dlab.getorigrevval(lang, aspxlabel)
            Case "compmain.aspx"
                ret = dlab.getcompmainval(lang, aspxlabel)
            Case "CustomCMMShtmlTPMpics.aspx"
                ret = dlab.getCustomCMMShtmlTPMpicsval(lang, aspxlabel)
            Case "archnotes.aspx"
                ret = dlab.getarchnotesval(lang, aspxlabel)
            Case "taskimagetpmarch.aspx"
                ret = dlab.gettaskimagetpmarchval(lang, aspxlabel)
            Case "CustomCMMShtml.aspx"
                ret = dlab.getCustomCMMShtmlval(lang, aspxlabel)
            Case "AppSetComTab.aspx"
                ret = dlab.getAppSetComTabval(lang, aspxlabel)
            Case "FuncCopyMinitpm.aspx"
                ret = dlab.getFuncCopyMinitpmval(lang, aspxlabel)
            Case "AppSetLT.aspx"
                ret = dlab.getAppSetLTval(lang, aspxlabel)
            Case "ChangePass.aspx"
                ret = dlab.getChangePassval(lang, aspxlabel)
            Case "devTaskLubeList.aspx"
                ret = dlab.getdevTaskLubeListval(lang, aspxlabel)
            Case "mmenuopts.aspx"
                ret = dlab.getmmenuoptsval(lang, aspxlabel)
            Case "InventoryLubes.aspx"
                ret = dlab.getInventoryLubesval(lang, aspxlabel)
            Case "labshedmain.aspx"
                ret = dlab.getlabshedmainval(lang, aspxlabel)
            Case "AppSetAssetClass.aspx"
                ret = dlab.getAppSetAssetClassval(lang, aspxlabel)
            Case "wolistmini.aspx"
                ret = dlab.getwolistminival(lang, aspxlabel)
            Case "complibtasks2.aspx"
                ret = dlab.getcomplibtasks2val(lang, aspxlabel)
            Case "archnotestpm.aspx"
                ret = dlab.getarchnotestpmval(lang, aspxlabel)
            Case "OptHoldertpm.aspx"
                ret = dlab.getOptHoldertpmval(lang, aspxlabel)
            Case "PopAddPart.aspx"
                ret = dlab.getPopAddPartval(lang, aspxlabel)
            Case "useradmin2.aspx"
                ret = dlab.getuseradmin2val(lang, aspxlabel)
            Case "measoutfly.aspx"
                ret = dlab.getmeasoutflyval(lang, aspxlabel)
            Case "eqtab.aspx"
                ret = dlab.geteqtabval(lang, aspxlabel)
            Case "SiteAssets.aspx"
                ret = dlab.getSiteAssetsval(lang, aspxlabel)
            Case "PMAltMan.aspx"
                ret = dlab.getPMAltManval(lang, aspxlabel)
            Case "pmact.aspx"
                ret = dlab.getpmactval(lang, aspxlabel)
            Case "taskimagetpm.aspx"
                ret = dlab.gettaskimagetpmval(lang, aspxlabel)
            Case "logtrack.aspx"
                ret = dlab.getlogtrackval(lang, aspxlabel)
            Case "tpmuploadimage1.aspx"
                ret = dlab.gettpmuploadimage1val(lang, aspxlabel)
            Case "wofail.aspx"
                ret = dlab.getwofailval(lang, aspxlabel)
            Case "ScheduledTasks.aspx"
                ret = dlab.getScheduledTasksval(lang, aspxlabel)
            Case "tpmopttaskgrid.aspx"
                ret = dlab.gettpmopttaskgridval(lang, aspxlabel)
            Case "wofm2.aspx"
                ret = dlab.getwofm2val(lang, aspxlabel)
            Case "pmarchgridtpm.aspx"
                ret = dlab.getpmarchgridtpmval(lang, aspxlabel)
            Case "woaltman.aspx"
                ret = dlab.getwoaltmanval(lang, aspxlabel)
            Case "pmfmmaintpm.aspx"
                ret = dlab.getpmfmmaintpmval(lang, aspxlabel)
            Case "ChargeSelect.aspx"
                ret = dlab.getChargeSelectval(lang, aspxlabel)
            Case "functasks.aspx"
                ret = dlab.getfunctasksval(lang, aspxlabel)
            Case "pmactmtpm.aspx"
                ret = dlab.getpmactmtpmval(lang, aspxlabel)
            Case "tables.aspx"
                ret = dlab.gettablesval(lang, aspxlabel)
            Case "sfail.aspx"
                ret = dlab.getsfailval(lang, aspxlabel)
            Case "PMApprovalMain.aspx"
                ret = dlab.getPMApprovalMainval(lang, aspxlabel)
            Case "eqimages.aspx"
                ret = dlab.geteqimagesval(lang, aspxlabel)
            Case "JSub.aspx"
                ret = dlab.getJSubval(lang, aspxlabel)
            Case "EqBotGrid.aspx"
                ret = dlab.getEqBotGridval(lang, aspxlabel)
            Case "Assessment_Admin.aspx"
                ret = dlab.getAssessment_Adminval(lang, aspxlabel)
            Case "wrassign2.aspx"
                ret = dlab.getwrassign2val(lang, aspxlabel)
            Case "lubelistmini.aspx"
                ret = dlab.getlubelistminival(lang, aspxlabel)
            Case "pmacttpm.aspx"
                ret = dlab.getpmacttpmval(lang, aspxlabel)
            Case "OptHolder.aspx"
                ret = dlab.getOptHolderval(lang, aspxlabel)
            Case "pmfmmain.aspx"
                ret = dlab.getpmfmmainval(lang, aspxlabel)
            Case "measoutaddrt.aspx"
                ret = dlab.getmeasoutaddrtval(lang, aspxlabel)
            Case "tpmopsmain.aspx"
                ret = dlab.gettpmopsmainval(lang, aspxlabel)
            Case "NewLogin.aspx"
                ret = dlab.getNewLoginval(lang, aspxlabel)
            Case "pmactm.aspx"
                ret = dlab.getpmactmval(lang, aspxlabel)
            Case "newssamp.aspx"
                ret = dlab.getnewssampval(lang, aspxlabel)
            Case "wofmmain.aspx"
                ret = dlab.getwofmmainval(lang, aspxlabel)
            Case "complib.aspx"
                ret = dlab.getcomplibval(lang, aspxlabel)
            Case "storeroom.aspx"
                ret = dlab.getstoreroomval(lang, aspxlabel)
            Case "wolabtrans.aspx"
                ret = dlab.getwolabtransval(lang, aspxlabel)
            Case "AppSetpmApprGrps.aspx"
                ret = dlab.getAppSetpmApprGrpsval(lang, aspxlabel)
            Case "tpmvars.aspx"
                ret = dlab.gettpmvarsval(lang, aspxlabel)
            Case "wjsub.aspx"
                ret = dlab.getwjsubval(lang, aspxlabel)
            Case "sclasslookup.aspx"
                ret = dlab.getsclasslookupval(lang, aspxlabel)
            Case "siteassets2.aspx"
                ret = dlab.getsiteassets2val(lang, aspxlabel)
            Case "FuncGrid.aspx"
                ret = dlab.getFuncGridval(lang, aspxlabel)
            Case "optTaskPartList.aspx"
                ret = dlab.getoptTaskPartListval(lang, aspxlabel)
            Case "ctadmin.aspx"
                ret = dlab.getctadminval(lang, aspxlabel)
            Case "ttno.aspx"
                ret = dlab.getttnoval(lang, aspxlabel)
            Case "Assessment_Master.aspx"
                ret = dlab.getAssessment_Masterval(lang, aspxlabel)
            Case "AppSetApprHLTasks.aspx"
                ret = dlab.getAppSetApprHLTasksval(lang, aspxlabel)
            Case "archtasktoollisttpm.aspx"
                ret = dlab.getarchtasktoollisttpmval(lang, aspxlabel)
            Case "equploadimage.aspx"
                ret = dlab.getequploadimageval(lang, aspxlabel)
            Case "tpmtasks.aspx"
                ret = dlab.gettpmtasksval(lang, aspxlabel)
            Case "pmlibneweqdets.aspx"
                ret = dlab.getpmlibneweqdetsval(lang, aspxlabel)
            Case "ECRCalc.aspx"
                ret = dlab.getECRCalcval(lang, aspxlabel)
            Case "qtime.aspx"
                ret = dlab.getqtimeval(lang, aspxlabel)
            Case "draghelp1.aspx"
                ret = dlab.getdraghelp1val(lang, aspxlabel)
            Case "AddForum.aspx"
                ret = dlab.getAddForumval(lang, aspxlabel)
            Case "wolist.aspx"
                ret = dlab.getwolistval(lang, aspxlabel)
            Case "AppSetApprComp.aspx"
                ret = dlab.getAppSetApprCompval(lang, aspxlabel)
            Case "measoutroutesadd.aspx"
                ret = dlab.getmeasoutroutesaddval(lang, aspxlabel)
            Case "PMArchMantpm.aspx"
                ret = dlab.getPMArchMantpmval(lang, aspxlabel)
            Case "tpmtasksmain.aspx"
                ret = dlab.gettpmtasksmainval(lang, aspxlabel)
            Case "Login.aspx"
                ret = dlab.getLoginval(lang, aspxlabel)
            Case "PMScheduling2.aspx"
                ret = dlab.getPMScheduling2val(lang, aspxlabel)
            Case "PMTasks.aspx"
                ret = dlab.getPMTasksval(lang, aspxlabel)
            Case "PMAttach.aspx"
                ret = dlab.getPMAttachval(lang, aspxlabel)
            Case "AppSetTaskTabET.aspx"
                ret = dlab.getAppSetTaskTabETval(lang, aspxlabel)
            Case "EQMain.aspx"
                ret = dlab.getEQMainval(lang, aspxlabel)
            Case "Assessment_Details.aspx"
                ret = dlab.getAssessment_Detailsval(lang, aspxlabel)
            Case "EQBot.aspx"
                ret = dlab.getEQBotval(lang, aspxlabel)
            Case "ForumProfile.aspx"
                ret = dlab.getForumProfileval(lang, aspxlabel)
            Case "pmtaskschedtpm.aspx"
                ret = dlab.getpmtaskschedtpmval(lang, aspxlabel)
            Case "GSubtpm.aspx"
                ret = dlab.getGSubtpmval(lang, aspxlabel)
            Case "altitem.aspx"
                ret = dlab.getaltitemval(lang, aspxlabel)
            Case "JobPlanRefReq.aspx"
                ret = dlab.getJobPlanRefReqval(lang, aspxlabel)
            Case "PMAdjMan.aspx"
                ret = dlab.getPMAdjManval(lang, aspxlabel)
            Case "Benchmark_Findings.aspx"
                ret = dlab.getBenchmark_Findingsval(lang, aspxlabel)
            Case "TranEq.aspx"
                ret = dlab.getTranEqval(lang, aspxlabel)
            Case "PMGetPMMan.aspx"
                ret = dlab.getPMGetPMManval(lang, aspxlabel)
            Case "pmcoststpm.aspx"
                ret = dlab.getpmcoststpmval(lang, aspxlabel)
            Case "tpmtaskgrid.aspx"
                ret = dlab.gettpmtaskgridval(lang, aspxlabel)
            Case "binlook.aspx"
                ret = dlab.getbinlookval(lang, aspxlabel)
            Case "wosched.aspx"
                ret = dlab.getwoschedval(lang, aspxlabel)
            Case "pmfmtpm.aspx"
                ret = dlab.getpmfmtpmval(lang, aspxlabel)
            Case "complibadd.aspx"
                ret = dlab.getcomplibaddval(lang, aspxlabel)
            Case "measoutwo.aspx"
                ret = dlab.getmeasoutwoval(lang, aspxlabel)
            Case "commontaskstpm.aspx"
                ret = dlab.getcommontaskstpmval(lang, aspxlabel)
            Case "sparepartpicktpm.aspx"
                ret = dlab.getsparepartpicktpmval(lang, aspxlabel)
            Case "squery.aspx"
                ret = dlab.getsqueryval(lang, aspxlabel)
            Case "wojpme2.aspx"
                ret = dlab.getwojpme2val(lang, aspxlabel)
            Case "AddFail.aspx"
                ret = dlab.getAddFailval(lang, aspxlabel)
            Case "catalog.aspx"
                ret = dlab.getcatalogval(lang, aspxlabel)
            Case "Inventory.aspx"
                ret = dlab.getInventoryval(lang, aspxlabel)
            Case "woplans.aspx"
                ret = dlab.getwoplansval(lang, aspxlabel)
            Case "codets.aspx"
                ret = dlab.getcodetsval(lang, aspxlabel)
            Case "CompCopyMinitpm.aspx"
                ret = dlab.getCompCopyMinitpmval(lang, aspxlabel)
            Case "PMRoutes2.aspx"
                ret = dlab.getPMRoutes2val(lang, aspxlabel)
            Case "wradd.aspx"
                ret = dlab.getwraddval(lang, aspxlabel)
            Case "servicecontract.aspx"
                ret = dlab.getservicecontractval(lang, aspxlabel)
            Case "ECMExport.aspx"
                ret = dlab.getECMExportval(lang, aspxlabel)
            Case "measoutroutesmini.aspx"
                ret = dlab.getmeasoutroutesminival(lang, aspxlabel)
            Case "woact.aspx"
                ret = dlab.getwoactval(lang, aspxlabel)
            Case "wojpsched.aspx"
                ret = dlab.getwojpschedval(lang, aspxlabel)
            Case "query.aspx"
                ret = dlab.getqueryval(lang, aspxlabel)
            Case "complibtasks3.aspx"
                ret = dlab.getcomplibtasks3val(lang, aspxlabel)
            Case "FuncDiv.aspx"
                ret = dlab.getFuncDivval(lang, aspxlabel)
            Case "PMAltMantpm.aspx"
                ret = dlab.getPMAltMantpmval(lang, aspxlabel)
            Case "AppSetDeptTrans.aspx"
                ret = dlab.getAppSetDeptTransval(lang, aspxlabel)
            Case "optTaskLubeList.aspx"
                ret = dlab.getoptTaskLubeListval(lang, aspxlabel)
            Case "UserSites.aspx"
                ret = dlab.getUserSitesval(lang, aspxlabel)
            Case "InventoryTools.aspx"
                ret = dlab.getInventoryToolsval(lang, aspxlabel)
            Case "archrationale2.aspx"
                ret = dlab.getarchrationale2val(lang, aspxlabel)
            Case "Prime_Measures.aspx"
                ret = dlab.getPrime_Measuresval(lang, aspxlabel)
            Case "MissingData.aspx"
                ret = dlab.getMissingDataval(lang, aspxlabel)
            Case "PopAddEq.aspx"
                ret = dlab.getPopAddEqval(lang, aspxlabel)
            Case "wocomp.aspx"
                ret = dlab.getwocompval(lang, aspxlabel)
            Case "PMApprovalUsers.aspx"
                ret = dlab.getPMApprovalUsersval(lang, aspxlabel)
            Case "PMMeastpm.aspx"
                ret = dlab.getPMMeastpmval(lang, aspxlabel)
            Case "CompCopyMini.aspx"
                ret = dlab.getCompCopyMinival(lang, aspxlabel)
            Case "PMOptTasksGrid.aspx"
                ret = dlab.getPMOptTasksGridval(lang, aspxlabel)
            Case "tpmget.aspx"
                ret = dlab.gettpmgetval(lang, aspxlabel)
            Case "pmcosts.aspx"
                ret = dlab.getpmcostsval(lang, aspxlabel)
            Case "AppSetEqTab.aspx"
                ret = dlab.getAppSetEqTabval(lang, aspxlabel)
            Case "PMOptRationaleTPM2.aspx"
                ret = dlab.getPMOptRationaleTPM2val(lang, aspxlabel)
            Case "wojpcompman.aspx"
                ret = dlab.getwojpcompmanval(lang, aspxlabel)
            Case "PMSuperSelect.aspx"
                ret = dlab.getPMSuperSelectval(lang, aspxlabel)
            Case "pmlibnewfudets.aspx"
                ret = dlab.getpmlibnewfudetsval(lang, aspxlabel)
            Case "pmarchmaintpm.aspx"
                ret = dlab.getpmarchmaintpmval(lang, aspxlabel)
            Case "sparepartpick.aspx"
                ret = dlab.getsparepartpickval(lang, aspxlabel)
            Case "archrationale.aspx"
                ret = dlab.getarchrationaleval(lang, aspxlabel)
            Case "measalert.aspx"
                ret = dlab.getmeasalertval(lang, aspxlabel)
            Case "AppSetWoStatus.aspx"
                ret = dlab.getAppSetWoStatusval(lang, aspxlabel)
            Case "wofm.aspx"
                ret = dlab.getwofmval(lang, aspxlabel)
            Case "AppSetTaskTab.aspx"
                ret = dlab.getAppSetTaskTabval(lang, aspxlabel)
            Case "sparepartpicharchtpm.aspx"
                ret = dlab.getsparepartpicharchtpmval(lang, aspxlabel)
            Case "recvinv.aspx"
                ret = dlab.getrecvinvval(lang, aspxlabel)
            Case "woexpd.aspx"
                ret = dlab.getwoexpdval(lang, aspxlabel)
            Case "EQReport.aspx"
                ret = dlab.getEQReportval(lang, aspxlabel)
            Case "EQCopyMinitpm.aspx"
                ret = dlab.getEQCopyMinitpmval(lang, aspxlabel)
            Case "issueinv.aspx"
                ret = dlab.getissueinvval(lang, aspxlabel)
            Case "taskedit2.aspx"
                ret = dlab.gettaskedit2val(lang, aspxlabel)
            Case "siteassets3.aspx"
                ret = dlab.getsiteassets3val(lang, aspxlabel)
            Case "pmlibnewpmtaskdets.aspx"
                ret = dlab.getpmlibnewpmtaskdetsval(lang, aspxlabel)
        End Select
        Return ret
    End Function
End Class
