

'********************************************************
'*
'********************************************************



Imports System.Text
Imports System.Data.SqlClient
Imports System.Security.Cryptography

Public Class Utilities
    Dim myconn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("strConn"))
    Dim dr As SqlDataReader
    Dim sql As String
    Private intCmdTSecs As Integer = 500
    Private userlic As Integer '= 100
    Private qrtr As Integer
    Private mmnth As Integer
    Private mnnth As Integer
    Private hpm As Integer
    Private sessid As String
    Private logsess As String
    Private bdlog As String = "la19i76"
    Private cnowdate As DateTime

    Public Property CNOW() As DateTime
        Get
            Return GetCNOW()
        End Get
        Set(ByVal Value As DateTime)
            cnowdate = Value
        End Set
    End Property
    Public Property BDLOGIN() As String
        Get
            Return bdlog
        End Get
        Set(ByVal Value As String)
            bdlog = Value
        End Set
    End Property
    Public Property MaxMnth(ByVal qtr As Integer) As Integer
        Get
            Return GetMaxMnth(qtr)
        End Get
        Set(ByVal Value As Integer)
            mmnth = Value
        End Set
    End Property
    Public Property MinMnth(ByVal qtr As Integer) As Integer
        Get
            Return GetMinMnth(qtr)
        End Get
        Set(ByVal Value As Integer)
            mnnth = Value
        End Set
    End Property
    Public Property Qtr(ByVal mnth As Integer) As Integer
        Get
            Return GetQtr(mnth)
        End Get
        Set(ByVal Value As Integer)
            qrtr = Value
        End Set
    End Property
    Public Property Users(ByVal cid As String) As Integer
        Get
            Return CompLics(cid) 'userlic
        End Get
        Set(ByVal Value As Integer)
            userlic = Value
        End Set
    End Property
    Public Property Rev() As Integer
        Get
            Return ApprRev()
        End Get
        Set(ByVal Value As Integer)
            Rev = Value
        End Set
    End Property
    Public Property CmdTimeout() As Integer
        Get
            Return intCmdTSecs
        End Get
        Set(ByVal Value As Integer)
            intCmdTSecs = Value
        End Set
    End Property
    Public Property HasPM(ByVal eqid As String) As Integer
        Get
            Return CheckPM(eqid) 'userlic
        End Get
        Set(ByVal Value As Integer)
            hpm = Value
        End Set
    End Property
    Public Function GetCNOW() As DateTime
        Dim adjustval As Integer
        Try
            adjustval = HttpContext.Current.Session("timeadjust").ToString()
            If adjustval = 0 Then
                cnowdate = Now
            Else
                cnowdate = DateAdd(DateInterval.Hour, adjustval, Now)
            End If
        Catch ex As Exception
            cnowdate = Now
        End Try
        Return cnowdate

    End Function
    Public Function CompLics(ByVal cid As String) As Integer
        Dim lic, ucnt As Integer
        sql = "select lics from Company where compid = '" & cid & "'"
        'Open()
        lic = Scalar(sql)
        sql = "select count(*) from pmsysusers"
        ucnt = Scalar(sql)
        Try
            lic = lic - ucnt
        Catch ex As Exception
            lic = 100
        End Try
        'Dispose()
        lic = 2000
        Return lic
    End Function
    Public Property TS() As Integer
        Get
            Return tsource()
        End Get
        Set(ByVal Value As Integer)
            TS = Value
        End Set
    End Property
    Public Function CheckRO(ByVal uid As String, ByVal app As String) As String
        Dim roret As String
        sql = "select readonly from pmuserapps where uid = '" & uid & "' and app = '" & app & "'"
        roret = strScalar(sql)
        Return roret
    End Function
    Public Function CheckROS(ByVal rostr As String, ByVal app As String) As String
        Dim roret As String = "0"
        Dim roarr() As String = rostr.Split(",")
        Dim i As Integer
        For i = 0 To roarr.Length - 1
            If roarr(i) = app Then
                roret = "1"
                Exit For
            End If
        Next
        Return roret
    End Function
    Public Function tsource()
        Dim ts As Integer
        sql = "select tpmsource from pmsysvars"
        ts = Scalar(sql)
        Return ts
    End Function
    Public Function ApprRev()
        Dim rev As Integer
        sql = "select rev from pmApprRev where iscurrent = 1"
        rev = Scalar(sql)
        Return rev
    End Function
    Public Function Encrypt(ByVal str As String) As String
        Dim md5Hasher As New MD5CryptoServiceProvider
        Dim hashedBytes As Byte()
        Dim encoder As New UTF8Encoding
        hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(str))
        Return Convert.ToBase64String(hashedBytes)
    End Function
    Public Function Update(ByVal sql As String) As String
        Dim cmd As New SqlCommand
        'cmd.CommandTimeout = 500
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        cmd.ExecuteNonQuery()
        cmd.Dispose()
    End Function
    Public Function UpdateHack(ByVal cmd As SqlCommand) As String
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        cmd.ExecuteNonQuery()
        cmd.Dispose()
    End Function
    Public Function Scalar(ByVal sql As String) As Integer
        Dim cmd As New SqlCommand
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        Dim ret As Integer = cmd.ExecuteScalar()
        Return ret
        cmd.Dispose()
    End Function
    Public Function ScalarHack(ByVal cmd As SqlCommand) As Integer
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        Dim ret As Integer = cmd.ExecuteScalar()
        Return ret
        cmd.Dispose()
    End Function
    Public Function strScalar(ByVal sql As String) As String
        Dim cmd As New SqlCommand
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        Dim ret As String = cmd.ExecuteScalar()
        Return ret
        cmd.Dispose()
    End Function
    Public Function GetRdrData(ByVal sql As String) As SqlClient.SqlDataReader
        Dim cmd As New SqlCommand
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        dr = cmd.ExecuteReader()
        Return dr
    End Function
    Public Function GetRdrDataHack(ByVal cmd As SqlCommand) As SqlClient.SqlDataReader
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        dr = cmd.ExecuteReader()
        Return dr
    End Function
    Public Function GetDSDataHack(ByVal cmd As SqlCommand) As DataSet
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        Dim daData As SqlDataAdapter = New SqlDataAdapter
        daData.SelectCommand = cmd
        Dim dsData As DataSet = New DataSet
        daData.Fill(dsData)
        cmd.Dispose()
        Return dsData
    End Function
    Public Function GetDSData(ByVal sql As String) As DataSet

        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        Dim daData As SqlDataAdapter = New SqlDataAdapter
        daData.SelectCommand = cmd
        Dim dsData As DataSet = New DataSet
        daData.Fill(dsData)
        cmd.Dispose()
        Return dsData

    End Function
    Public Function GetPage(ByVal Tables As String, ByVal PK As String, _
    ByVal Sort As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, _
    ByVal Fields As String, ByVal Filter As String, ByVal Group As String) As SqlClient.SqlDataReader
        'Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group
        Dim spName As String = "Paging_Cursor"
        Dim cmd As New SqlCommand
        'If PageSize = 1 Then PageNumber = 1
        sql = "Paging_Cursor '" & Tables & "', '" & PK & "', '" & Sort & "', '" & PageNumber & "', '" & PageSize & "', '" & Fields & "', '" & Filter & "', '" & Group & "'"
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        dr = cmd.ExecuteReader()
        Return dr
    End Function
    Public Function GetPage2(ByVal Tables As String, ByVal JTables As String, ByVal PK As String, _
    ByVal JPK As String, ByVal Sort As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, _
    ByVal Fields As String, ByVal Filter As String, ByVal Group As String) As SqlClient.SqlDataReader
        'Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group
        Dim spName As String = "Paging_Cursor2"
        Dim cmd As New SqlCommand
        'If PageSize = 1 Then PageNumber = 1
        sql = "Paging_Cursor2 '" & Tables & "', '" & JTables & "', '" & PK & "', '" & JPK & "', '" & Sort & "', '" & PageNumber & "', '" & PageSize & "', '" & Fields & "', '" & Filter & "', '" & Group & "'"
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        dr = cmd.ExecuteReader()
        Return dr
    End Function
    Public Function GetPage3(ByVal Tables As String, ByVal JTables As String, ByVal J2Tables As String, ByVal PK As String, _
    ByVal JPK As String, ByVal J2PK As String, ByVal Sort As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, _
    ByVal Fields As String, ByVal Filter As String, ByVal Group As String) As SqlClient.SqlDataReader
        'Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group
        Dim spName As String = "Paging_Cursor2"
        Dim cmd As New SqlCommand
        'If PageSize = 1 Then PageNumber = 1
        sql = "Paging_Cursor3 '" & Tables & "', '" & JTables & "', '" & J2Tables & "', '" & PK & "', '" & JPK & "', '" & J2PK & "', '" & Sort & "', '" & PageNumber & "', '" & PageSize & "', '" & Fields & "', '" & Filter & "', '" & Group & "'"
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        dr = cmd.ExecuteReader()
        Return dr
    End Function
    Public Function GetPageHack(ByVal Tables As String, ByVal PK As String, _
    ByVal Sort As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, _
    ByVal Fields As String, ByVal Filter As String, ByVal Group As String) As SqlClient.SqlDataReader
        'Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group
        Dim cmd As New SqlCommand("exec Paging_Cursor @tables, @pk, @sort, @pagenumber, @pagesize, @fields, @filter, @group")
        Dim param = New SqlParameter("@tables", SqlDbType.VarChar)
        param.Value = Tables
        cmd.Parameters.Add(param)
        Dim param1 = New SqlParameter("@pk", SqlDbType.VarChar)
        param1.Value = PK
        cmd.Parameters.Add(param1)
        Dim param2 = New SqlParameter("@sort", SqlDbType.VarChar)
        param2.Value = Sort
        cmd.Parameters.Add(param2)
        Dim param3 = New SqlParameter("@pagenumber", SqlDbType.Int)
        param3.Value = PageNumber
        cmd.Parameters.Add(param3)
        Dim param4 = New SqlParameter("@pagesize", SqlDbType.Int)
        param4.Value = PageSize
        cmd.Parameters.Add(param4)
        Dim param5 = New SqlParameter("@fields", SqlDbType.VarChar)
        param5.Value = Fields
        cmd.Parameters.Add(param5)
        Dim param6 = New SqlParameter("@filter", SqlDbType.VarChar)
        param6.Value = Filter
        cmd.Parameters.Add(param6)
        Dim param7 = New SqlParameter("@group", SqlDbType.VarChar)
        param7.Value = Group
        cmd.Parameters.Add(param7)
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        dr = cmd.ExecuteReader()
        Return dr
    End Function
    Public Function GetPageMOD(ByVal Tables As String, ByVal PK As String, _
    ByVal Sort As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, _
    ByVal Fields As String, ByVal Filter As String, ByVal Group As String, _
    ByVal fuid As String) As SqlClient.SqlDataReader
        'Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group
        Dim spName As String = "Paging_Cursor"
        Dim cmd As New SqlCommand
        sql = "Paging_CursorMOD '" & Tables & "', '" & PK & "', '" & Sort & "', '" & PageNumber & "', '" & PageSize & "', '" & Fields & "', '" & Filter & "', '" & Group & "', '" & fuid & "'"
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        dr = cmd.ExecuteReader()
        Return dr
    End Function
    Public Function GetDSPage(ByVal Tables As String, ByVal PK As String, _
    ByVal Sort As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, _
    ByVal Fields As String, ByVal Filter As String, ByVal Group As String) As DataSet
        'Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group

        Dim spName As String = "Paging_Cursor"
        sql = "Paging_Cursor '" & Tables & "', '" & PK & "', '" & Sort & "', '" & PageNumber & "', '" & PageSize & "', '" & Fields & "', '" & Filter & "', '" & Group & "'"
        Dim cmd As New SqlCommand
        cmd.CommandText = sql
        cmd.Connection = myconn
        cmd.CommandTimeout = intCmdTSecs
        Dim daData As SqlDataAdapter = New SqlDataAdapter
        daData.SelectCommand = cmd
        Dim pgData As DataSet = New DataSet
        daData.Fill(pgData)
        cmd.Dispose()
        Return pgData

    End Function
    Public Function Open()
        myconn.Open()
    End Function
    Public Function Close()
        dr.Close()
    End Function

    Public Function Dispose()
        myconn.Close()
        myconn.Dispose()
    End Function
    Public Function PageCount(ByVal sql As String, ByVal PageRows As Integer) As Integer
        Dim cmd As New SqlCommand
        cmd.CommandText = sql
        cmd.Connection = myconn
        Dim TotalRows As Integer = cmd.ExecuteScalar()
        cmd.Dispose()
        Dim PageCnt As Integer
        Dim Remainder As Integer
        Remainder = (TotalRows Mod PageRows)
        PageCnt = (TotalRows - Remainder) / PageRows
        If Remainder > 0 Then
            PageCnt += 1
        End If
        Return PageCnt
    End Function
    Public Function PageCountRev(ByVal TotalRows As Integer, ByVal PageRows As Integer) As Integer
        Dim PageCnt As Integer
        Dim Remainder As Integer
        Remainder = (TotalRows Mod PageRows)
        PageCnt = (TotalRows - Remainder) / PageRows
        If Remainder > 0 Then
            PageCnt += 1
        End If
        Return PageCnt
    End Function
    Public Function PageCount(ByVal TotalRows As Integer, ByVal PageRows As Integer) As Integer
        Dim PageCnt As Integer
        Dim Remainder As Integer
        Remainder = (TotalRows Mod PageRows)
        PageCnt = (TotalRows - Remainder) / 5
        If Remainder > 0 Then
            PageCnt += 1
        End If
        Return PageCnt
    End Function
    Public Function PageCount1(ByVal TotalRows As Integer, ByVal PageRows As Integer) As Integer
        Dim PageCnt As Integer
        Dim Remainder As Integer
        Remainder = (TotalRows Mod PageRows)
        PageCnt = (TotalRows - Remainder) / 10
        If Remainder > 0 Then
            PageCnt += 1
        End If
        Return PageCnt
    End Function
    Public Function PageCount100(ByVal TotalRows As Integer, ByVal PageRows As Integer) As Integer
        Dim PageCnt As Integer
        Dim Remainder As Integer
        Remainder = (TotalRows Mod PageRows)
        PageCnt = (TotalRows - Remainder) / 100
        If Remainder > 0 Then
            PageCnt += 1
        End If
        Return PageCnt
    End Function
    Public Function GetList(ByVal dt As String, ByVal val As String, ByVal filt As String) As SqlDataReader
        Dim sortarr() As String = val.Split(",")
        If filt = "" Then
            Dim sort As String = sortarr(1)
            sort = RTrim(sort)
            sort = LTrim(sort)
            sql = "select distinct " & val & " from " & dt & filt & " order by " & sort & " asc"
            dr = GetRdrData(sql)
        Else
            Dim sort As String = sortarr(1)
            sort = RTrim(sort)
            sort = LTrim(sort)
            Try
                sql = "select distinct " & val & " from " & dt & filt & " order by " & sort & " asc"
                dr = GetRdrData(sql)
            Catch ex As Exception
                sql = "select distinct " & val & " from " & dt & filt
                dr = GetRdrData(sql)
            End Try

        End If


        Return dr
    End Function
    Public Function GetDesc(ByVal dt As String, ByVal df As String, ByVal filt As String) As SqlDataReader
        sql = "select " & df & " from " & dt & filt
        dr = GetRdrData(sql)
        Return dr
    End Function

    Public Function UpMod(ByVal eqid As String)
        '**** Added for PM Manager
        sql = "update equipment set modifieddate = getDate(), hasmod = 1 " _
    + "where eqid = '" & eqid & "' and haspm = 1"
        Update(sql)

    End Function
    Public Function UpModTask(ByVal taskid As String)
        '**** Added for PM Manager
        Dim eqid As Integer
        sql = "select eqid from pmtaskstpm where pmtskid = '" & taskid & "'"
        eqid = Scalar(sql)
        sql = "update equipment set modifieddate = getDate(), hasmod = 1 " _
    + "where eqid = '" & eqid & "' and haspm = 1"
        Update(sql)

    End Function
    Public Function CheckPM(ByVal eqid As String) As Integer
        Dim haspm As Integer
        sql = "select haspm from equipment where eqid = '" & eqid & "'"
        dr = GetRdrData(sql)
        While dr.Read
            haspm = dr.Item("haspm").ToString
        End While
        dr.Close()
        Return haspm
    End Function
    Public Shared Sub CreateJS(ByRef aspxPage As System.Web.UI.Page, ByVal strMessage As String, ByVal strKey As String)
        Dim strScript As String = "<script language=JavaScript>" & strMessage & "</script>"

        If (Not aspxPage.IsStartupScriptRegistered(strKey)) Then
            aspxPage.RegisterStartupScript(strKey, strScript)
        End If
    End Sub
    Public Shared Sub CreateMessageAlert(ByRef aspxPage As System.Web.UI.Page, ByVal strMessage As String, ByVal strKey As String)
        Dim strScript As String = "<script language=JavaScript>alert('" & strMessage & "')</script>"

        If (Not aspxPage.IsStartupScriptRegistered(strKey)) Then
            aspxPage.RegisterStartupScript(strKey, strScript)
        End If
    End Sub
    Public Shared Sub FileUpload(ByRef aspxPage As System.Web.UI.Page, ByVal strScript As String, ByVal strKey As String)

        If (Not aspxPage.IsStartupScriptRegistered(strKey)) Then
            aspxPage.RegisterStartupScript(strKey, strScript)
        End If
    End Sub
    Public Function Confirm(ByRef aspxPage As System.Web.UI.Page, ByVal msg As String, ByVal strKey As String)

    End Function
    Private Function GetQtr(ByVal mnth As Integer) As Integer
        Dim qtr As Integer
        Select Case mnth
            Case 1, 2, 3
                qtr = 1
            Case 4, 5, 6
                qtr = 2
            Case 7, 8, 9
                qtr = 3
            Case 10, 11, 12
                qtr = 4
        End Select
        Return qtr
    End Function
    Private Function GetMaxMnth(ByVal qtr As Integer) As Integer
        Dim mnth As Integer
        Select Case qtr
            Case 1
                mnth = 4
            Case 2
                mnth = 7
            Case 3
                mnth = 10
            Case 4
                mnth = 13
        End Select
        Return mnth
    End Function
    Private Function GetMinMnth(ByVal qtr As Integer) As Integer
        Dim mnth As Integer
        Select Case qtr
            Case 1
                mnth = 0
            Case 2
                mnth = 3
            Case 3
                mnth = 6
            Case 4
                mnth = 9
        End Select
        Return mnth
    End Function
    Public Function ModString1(ByVal str As String) As String
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, """", Chr(180) & Chr(180), , , vbTextCompare)
        str = Replace(str, "--", " ", , , vbTextCompare)
        str = Replace(str, "-", " ", , , vbTextCompare)
        str = Replace(str, "+", " ", , , vbTextCompare)
        str = Replace(str, "=", " ", , , vbTextCompare)
        str = Replace(str, ";", " ", , , vbTextCompare)
        str = Replace(str, ":", " ", , , vbTextCompare)
        str = Replace(str, "/", " ", , , vbTextCompare)
        str = Replace(str, "\", " ", , , vbTextCompare)
        str = Replace(str, "&", " ", , , vbTextCompare)
        str = Replace(str, "#", " ", , , vbTextCompare)
        str = Replace(str, "$", " ", , , vbTextCompare)
        str = Replace(str, "%", " ", , , vbTextCompare)
        str = Replace(str, "!", " ", , , vbTextCompare)
        str = Replace(str, "~", " ", , , vbTextCompare)
        str = Replace(str, "^", " ", , , vbTextCompare)
        str = Replace(str, "*", " ", , , vbTextCompare)
        'str = Replace(str, ".", " ", , , vbTextCompare)
        str = Replace(str, ">", " ", , , vbTextCompare)
        str = Replace(str, "<", " ", , , vbTextCompare)
        '
        Return str
    End Function
    Public Function ModString2(ByVal str As String) As String
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, """", Chr(180) & Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        str = Replace(str, "/", "\", , , vbTextCompare)

        Return str
    End Function
    Public Function ModString3(ByVal str As String) As String
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, """", Chr(180) & Chr(180), , , vbTextCompare)
        str = Replace(str, "+", " ", , , vbTextCompare)
        str = Replace(str, "=", " ", , , vbTextCompare)
        str = Replace(str, ";", " ", , , vbTextCompare)
        str = Replace(str, ":", " ", , , vbTextCompare)
        str = Replace(str, "/", "\", , , vbTextCompare)
        'str = Replace(str, "\", " ", , , vbTextCompare)
        str = Replace(str, "&", " ", , , vbTextCompare)
        str = Replace(str, "#", " ", , , vbTextCompare)
        str = Replace(str, "$", " ", , , vbTextCompare)
        str = Replace(str, "%", " ", , , vbTextCompare)
        str = Replace(str, "!", " ", , , vbTextCompare)
        str = Replace(str, "~", " ", , , vbTextCompare)
        str = Replace(str, "^", " ", , , vbTextCompare)
        str = Replace(str, "*", " ", , , vbTextCompare)
        str = Replace(str, ">", " ", , , vbTextCompare)
        str = Replace(str, "<", " ", , , vbTextCompare)

        str = Replace(str, "--", "-", , , vbTextCompare)

        Return str
    End Function
    Public Sub ResetSession()

    End Sub
    Public Sub DelBadEq()
        sql = "delete from pmtasks where eqid is null; " _
        + "delete from pmtasks where eqid = 0 "

        Update(sql)
    End Sub
    Public Sub DelBadEqAll()
        sql = "delete from pmtasks where eqid is null; " _
        + "delete from pmtasks where eqid = 0 " _
        + "declare @eqveri int, @ecnt int " _
        + "declare eq_cursor cursor for " _
        + "select distinct eqid from pmtasks " _
        + "open eq_cursor " _
        + "fetch next from eq_cursor into @eqveri " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "set @ecnt = (select count(*) from equipment where eqid = @eqveri) " _
        + "if @ecnt = 0 " _
        + "begin " _
        + "delete from pmtasks where eqid = @eqveri " _
        + "end " _
        + "fetch next from eq_cursor into @eqveri " _
        + "end " _
        + "close eq_cursor " _
        + "deallocate eq_cursor " _
        + "declare @comid int, @pmtskid int, @fuid int " _
        + "declare @tblf table ( " _
        + "funcid int " _
        + ") " _
        + "insert into @tblf " _
        + "select func_id from functions where eqid not in " _
        + "(select eqid from equipment) " _
        + "declare f_cursor cursor for " _
        + "select funcid from @tblf " _
        + "open f_cursor " _
        + "fetch next from f_cursor into @fuid " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "delete from functions where func_id = @fuid " _
        + "declare task_cursor cursor for " _
        + "select pmtskid from pmtasks where funcid = @fuid " _
        + "open task_cursor " _
        + "fetch next from task_cursor into @pmtskid " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "delete from pmtaskfailmodes where taskid = @pmtskid " _
        + "delete from pmotaskfailmodes where taskid = @pmtskid " _
        + "delete from pmoptrationale where taskid = @pmtskid " _
        + "delete from pmotaskparts where pmtskid = @pmtskid " _
        + "delete from pmotasktools where pmtskid = @pmtskid " _
        + "delete from pmotasklubes where pmtskid = @pmtskid " _
        + "delete from pmtaskparts where pmtskid = @pmtskid " _
        + "delete from pmtasktools where pmtskid = @pmtskid " _
        + "delete from pmtasklubes where pmtskid = @pmtskid " _
        + "delete from pmtaskpartsout where pmtskid = @pmtskid " _
        + "delete from pmtasktoolsout where pmtskid = @pmtskid " _
        + "delete from pmtasklubesout where pmtskid = @pmtskid " _
        + "delete from pmtaskmeasdetails where pmtskid = @pmtskid " _
        + "delete from pmtaskmeasoutput where pmtskid = @pmtskid " _
        + "delete from pmtasks where pmtskid = @pmtskid " _
        + "fetch next from task_cursor into @pmtskid " _
        + "end " _
        + "close task_cursor " _
        + "deallocate task_cursor " _
        + "fetch next from f_cursor into @fuid " _
        + "end " _
        + "close f_cursor " _
        + "deallocate f_cursor " _
        + "declare @tblc table ( " _
        + "comid int " _
        + ") " _
        + "insert into @tblc " _
        + "select comid from components where func_id not in " _
        + "(select func_id from functions) " _
        + "declare c_cursor cursor for " _
        + "select comid from @tblc " _
        + "open c_cursor " _
        + "fetch next from c_cursor into @comid " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "delete from componentfailmodes where comid = @comid " _
        + "delete from components where comid = @comid " _
        + "declare task_cursor cursor for " _
        + "select pmtskid from pmtasks where comid = @comid " _
        + "open task_cursor " _
        + "fetch next from task_cursor into @pmtskid " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "delete from pmtaskfailmodes where taskid = @pmtskid " _
        + "delete from pmotaskfailmodes where taskid = @pmtskid " _
        + "delete from pmoptrationale where taskid = @pmtskid " _
        + "delete from pmotaskparts where pmtskid = @pmtskid " _
        + "delete from pmotasktools where pmtskid = @pmtskid " _
        + "delete from pmotasklubes where pmtskid = @pmtskid " _
        + "delete from pmtaskparts where pmtskid = @pmtskid " _
        + "delete from pmtasktools where pmtskid = @pmtskid " _
        + "delete from pmtasklubes where pmtskid = @pmtskid " _
        + "delete from pmtaskpartsout where pmtskid = @pmtskid " _
        + "delete from pmtasktoolsout where pmtskid = @pmtskid " _
        + "delete from pmtasklubesout where pmtskid = @pmtskid " _
        + "delete from pmtaskmeasdetails where pmtskid = @pmtskid " _
        + "delete from pmtaskmeasoutput where pmtskid = @pmtskid " _
        + "delete from pmtasks where pmtskid = @pmtskid " _
        + "fetch next from task_cursor into @pmtskid " _
        + "end " _
        + "close task_cursor " _
        + "deallocate task_cursor " _
        + "fetch next from c_cursor into @comid " _
        + "end " _
        + "close c_cursor " _
        + "deallocate c_cursor " _
        + "delete from pmtasks where tasknum > 10000"
        Try
            Update(sql)
        Catch ex As Exception

        End Try

    End Sub
    Public Sub RemTaskDups(ByVal fuid As String)
        sql = "declare @revised datetime, @pmtskid int, @eqid int, @funcid int, @tasknum int " _
        + "declare @pmtskidkp int, @subtask int, @revchk datetime, @scnt int " _
        + "declare @tbl table ( " _
        + "eqid int, " _
        + "funcid int, " _
        + "tasknum int, " _
        + "cnt int " _
        + ") " _
        + "declare @tbl2 table ( " _
        + "pmtskid int, " _
        + "revised datetime " _
        + ") " _
        + "declare @tbl3 table ( " _
        + "pmtskid int, " _
        + "subtask int, " _
        + "revised datetime " _
        + ") " _
        + "delete from pmtasks where tasknum > 10000 " _
        + "insert into @tbl (eqid, funcid, tasknum, cnt) " _
        + "select t.eqid, t.funcid, t.tasknum, count(t.tasknum) " _
        + "from pmtasks t " _
        + "left join equipment e on e.eqid = t.eqid " _
        + "left join functions f on f.func_id = t.funcid " _
        + "where t.subtask = 0 and t.funcid = '" & fuid & "'" _
        + "group by t.eqid, t.funcid, t.tasknum " _
        + "having count(t.tasknum) > 1 " _
        + "order by t.eqid, t.funcid, t.tasknum " _
        + "declare task_cursor cursor for " _
        + "select eqid, funcid, tasknum from @tbl " _
        + "order by eqid, funcid, tasknum " _
        + "open task_cursor " _
        + "fetch next from task_cursor into @eqid, @funcid, @tasknum " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "insert into @tbl2 (pmtskid, revised) " _
        + "select pmtskid, revised from pmtasks " _
        + "where eqid = @eqid and funcid = @funcid and tasknum = @tasknum " _
        + "and subtask = 0 " _
        + "set @revchk = (select max(revised) from @tbl2) " _
        + "set @pmtskidkp = (select pmtskid from @tbl2 where revised = @revchk) " _
        + "delete from @tbl2 where pmtskid = @pmtskidkp " _
        + "delete from pmtasks where pmtskid in (select pmtskid from @tbl2) " _
        + "delete from @tbl2 " _
        + "insert into @tbl3 (pmtskid, subtask, revised) " _
        + "select pmtskid, subtask, revised from pmtasks " _
        + "where eqid = @eqid and funcid = @funcid and tasknum = @tasknum " _
        + "and subtask <> 0 " _
        + "set @scnt = (select count(*) from @tbl3) " _
        + "if @scnt <> 0 " _
        + "begin " _
        + "declare subtask_cursor cursor for " _
        + "select distinct subtask from @tbl3 " _
        + "order by subtask " _
        + "open subtask_cursor " _
        + "fetch next from subtask_cursor into @subtask " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "set @revchk = (select max(revised) from @tbl3 where subtask = @subtask) " _
        + "set @pmtskidkp = (select pmtskid from @tbl3 where revised = @revchk and subtask = @subtask) " _
        + "delete from @tbl3 where pmtskid = @pmtskidkp " _
        + "delete from pmtasks where pmtskid in (select pmtskid from @tbl3 where subtask = @subtask) " _
        + "fetch next from subtask_cursor into @subtask " _
        + "end " _
        + "close subtask_cursor " _
        + "deallocate subtask_cursor " _
        + "end " _
        + "delete from @tbl3 " _
        + "fetch next from task_cursor into @eqid, @funcid, @tasknum " _
        + "end " _
        + "close task_cursor " _
        + "deallocate task_cursor"

        Update(sql)
    End Sub
    Public Sub RemTaskDupsAll()
        sql = "declare @revised datetime, @pmtskid int, @eqid int, @funcid int, @tasknum int " _
        + "declare @pmtskidkp int, @subtask int, @revchk datetime, @scnt int " _
        + "declare @tbl table ( " _
        + "eqid int, " _
        + "funcid int, " _
        + "tasknum int, " _
        + "cnt int " _
        + ") " _
        + "declare @tbl2 table ( " _
        + "pmtskid int, " _
        + "revised datetime " _
        + ") " _
        + "declare @tbl3 table ( " _
        + "pmtskid int, " _
        + "subtask int, " _
        + "revised datetime " _
        + ") " _
        + "insert into @tbl (eqid, funcid, tasknum, cnt) " _
        + "select t.eqid, t.funcid, t.tasknum, count(t.tasknum) " _
        + "from pmtasks t " _
        + "left join equipment e on e.eqid = t.eqid " _
        + "left join functions f on f.func_id = t.funcid " _
        + "where t.subtask = 0 " _
        + "group by t.eqid, t.funcid, t.tasknum " _
        + "having count(t.tasknum) > 1 " _
        + "order by t.eqid, t.funcid, t.tasknum " _
        + "declare task_cursor cursor for " _
        + "select eqid, funcid, tasknum from @tbl " _
        + "order by eqid, funcid, tasknum " _
        + "open task_cursor " _
        + "fetch next from task_cursor into @eqid, @funcid, @tasknum " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "insert into @tbl2 (pmtskid, revised) " _
        + "select pmtskid, revised from pmtasks " _
        + "where eqid = @eqid and funcid = @funcid and tasknum = @tasknum " _
        + "and subtask = 0 " _
        + "set @revchk = (select max(revised) from @tbl2) " _
        + "set @pmtskidkp = (select pmtskid from @tbl2 where revised = @revchk) " _
        + "delete from @tbl2 where pmtskid = @pmtskidkp " _
        + "delete from pmtasks where pmtskid in (select pmtskid from @tbl2) " _
        + "delete from @tbl2 " _
        + "insert into @tbl3 (pmtskid, subtask, revised) " _
        + "select pmtskid, subtask, revised from pmtasks " _
        + "where eqid = @eqid and funcid = @funcid and tasknum = @tasknum " _
        + "and subtask <> 0 " _
        + "set @scnt = (select count(*) from @tbl3) " _
        + "if @scnt <> 0 " _
        + "begin " _
        + "declare subtask_cursor cursor for " _
        + "select distinct subtask from @tbl3 " _
        + "order by subtask " _
        + "open subtask_cursor " _
        + "fetch next from subtask_cursor into @subtask " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "set @revchk = (select max(revised) from @tbl3 where subtask = @subtask) " _
        + "set @pmtskidkp = (select pmtskid from @tbl3 where revised = @revchk and subtask = @subtask) " _
        + "delete from @tbl3 where pmtskid = @pmtskidkp " _
        + "delete from pmtasks where pmtskid in (select pmtskid from @tbl3 where subtask = @subtask) " _
        + "fetch next from subtask_cursor into @subtask " _
        + "end " _
        + "close subtask_cursor " _
        + "deallocate subtask_cursor " _
        + "end " _
        + "delete from @tbl3 " _
        + "fetch next from task_cursor into @eqid, @funcid, @tasknum " _
        + "end " _
        + "close task_cursor " _
        + "deallocate task_cursor"
        Try
            Update(sql)
        Catch ex As Exception

        End Try

    End Sub
    Public Sub PopMissFuncAll()
        sql = "declare @pareqid int, @parfuid int, @parfunc varchar(200), @fcnt int " _
        + "declare @neweqid int, @user varchar(50), @cid int, @newfuid int " _
        + "declare @compflg int, @db varchar(50), @orig varchar(50), @srvr varchar(200) " _
        + "declare @sid int, @did int, @clid int, @pm varchar(1), @tpm varchar(1) " _
        + "declare @app varchar(50), @oloc varchar(50), @roa varchar(1), @rat varchar(1) " _
        + "set @db = '1' " _
        + "set @orig = '1' " _
        + "set @srvr = '1' " _
        + "set @pm = 'Y' " _
        + "set @tpm = 'N' " _
        + "set @roa = 'A' " _
        + "set @rat = 'Y' " _
        + "set @oloc = NULL " _
        + "set @compflg = 1 " _
        + "set @cid = 0 " _
        + "declare e_cursor cursor for " _
        + "select eqid, func_id, func from functions " _
        + "open e_cursor " _
        + "fetch next from e_cursor into @pareqid, @parfuid, @parfunc " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "set @fcnt = (select count(funcid) from pmtasks where funcid = @parfuid and eqid <> @pareqid) " _
        + "if @fcnt > 1 " _
        + "begin " _
        + "declare f_cursor cursor for " _
        + "select distinct eqid, createdby, siteid, deptid, cellid from pmtasks where funcid = @parfuid and eqid <> @pareqid " _
        + "open f_cursor " _
        + "fetch next from f_cursor into @neweqid, @user, @sid, @did, @clid " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "delete from pmtasks where eqid = @neweqid " _
        + "exec usp_copyAllMDB2 @cid, @pareqid, @neweqid, @compflg, @db, @orig, @srvr " _
        + "exec usp_copyEqTasks2MDB @cid, @sid, @did, @clid, @neweqid, @user, @db, @orig, @pm, @tpm, @app, @oloc, @srvr, @roa, @rat " _
        + "exec iusp_updateCompFailId @neweqid " _
        + "fetch next from f_cursor into @neweqid, @user, @sid, @did, @clid " _
        + "end " _
        + "close f_cursor " _
        + "deallocate f_cursor " _
        + "end " _
        + "fetch next from e_cursor into @pareqid, @parfuid, @parfunc " _
        + "end " _
        + "close e_cursor " _
        + "deallocate e_cursor"

        Update(sql)
    End Sub
    Public Sub PopMissFuncAll2()
        sql = "declare @pareqid int, @parfuid int, @parfunc varchar(200), @fcnt int " _
        + "declare @neweqid int, @user varchar(50), @cid int, @newfuid int " _
        + "declare @compflg int, @db varchar(50), @orig varchar(50), @srvr varchar(200) " _
        + "declare @sid int, @did int, @clid int, @pm varchar(1), @tpm varchar(1) " _
        + "declare @app varchar(50), @oloc varchar(50), @roa varchar(1), @rat varchar(1) " _
        + "set @db = '1' " _
        + "set @orig = '1' " _
        + "set @srvr = '1' " _
        + "set @pm = 'Y' " _
        + "set @tpm = 'N' " _
        + "set @roa = 'A' " _
        + "set @rat = 'Y' " _
        + "set @oloc = NULL " _
        + "set @compflg = 1 " _
        + "set @cid = 0 " _
        + "declare e_cursor cursor for " _
        + "select f.eqid, f.func_id, f.func from functions f where f.func_id in " _
        + "(select t.funcid from pmtasks t where t.funcid = f.func_id and t.eqid <> f.eqid) " _
        + "open e_cursor " _
        + "fetch next from e_cursor into @pareqid, @parfuid, @parfunc " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "declare f_cursor cursor for " _
        + "select distinct eqid, createdby, siteid, deptid, cellid from pmtasks where funcid = @parfuid and eqid <> @pareqid " _
        + "open f_cursor " _
        + "fetch next from f_cursor into @neweqid, @user, @sid, @did, @clid " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "delete from pmtasks where eqid = @neweqid " _
        + "exec usp_copyAllMDB2 @cid, @pareqid, @neweqid, @compflg, @db, @orig, @srvr " _
        + "exec usp_copyEqTasks2MDB @cid, @sid, @did, @clid, @neweqid, @user, @db, @orig, @pm, @tpm, @app, @oloc, @srvr, @roa, @rat " _
        + "exec iusp_updateCompFailId @neweqid " _
        + "fetch next from f_cursor into @neweqid, @user, @sid, @did, @clid " _
        + "end " _
        + "close f_cursor " _
        + "deallocate f_cursor " _
        + "fetch next from e_cursor into @pareqid, @parfuid, @parfunc " _
        + "end " _
        + "close e_cursor " _
        + "deallocate e_cursor"

        Update(sql)
    End Sub
End Class
