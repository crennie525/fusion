

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class intra_utils
    Dim tmod As New transmod
    Public Function CustApps() As DataSet
        Dim dtapps As DataTable = New DataTable("tblapps")
        Dim dtcol As DataColumn
        Dim dtrow As DataRow

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "app"
        dtapps.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "appname"
        dtapps.Columns.Add(dtcol)

        'Edit Client Applications Here
        'Admin
        dtrow = dtapps.NewRow()
        dtrow("app") = "setup"
        dtrow("appname") = tmod.getxlbl("xlb184" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "eq"
        dtrow("appname") = tmod.getxlbl("xlb185" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "ecr"
        dtrow("appname") = tmod.getxlbl("xlb186" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "inv"
        dtrow("appname") = tmod.getxlbl("xlb187" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        'dtrow = dtapps.NewRow()
        'dtrow("app") = "inc"
        'dtrow("appname") = "PM Inventory Control"
        'dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "ua"
        dtrow("appname") = tmod.getxlbl("xlb188" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "ep"
        dtrow("appname") = tmod.getxlbl("xlb189" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "asa"
        dtrow("appname") = tmod.getxlbl("xlb190" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "wra"
        dtrow("appname") = tmod.getxlbl("xlb191" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)


        'Main
        dtrow = dtapps.NewRow()
        dtrow("app") = "dev"
        dtrow("appname") = tmod.getxlbl("xlb192" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "opt"
        dtrow("appname") = tmod.getxlbl("xlb193" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "tpd"
        dtrow("appname") = tmod.getxlbl("xlb194" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "tpo"
        dtrow("appname") = tmod.getxlbl("xlb195" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "man"
        dtrow("appname") = tmod.getxlbl("xlb196" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "tan"
        dtrow("appname") = tmod.getxlbl("xlb197" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "tpp"
        dtrow("appname") = tmod.getxlbl("xlb198" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "wo"
        dtrow("appname") = tmod.getxlbl("xlb199" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "wrr"
        dtrow("appname") = tmod.getxlbl("xlb200" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "prc"
        dtrow("appname") = tmod.getxlbl("xlb201" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "trc"
        dtrow("appname") = tmod.getxlbl("xlb202" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)


        'Add-Ons
        dtrow = dtapps.NewRow()
        dtrow("app") = "lib"
        dtrow("appname") = tmod.getxlbl("xlb203" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "sch"
        dtrow("appname") = tmod.getxlbl("xlb204" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "rte"
        dtrow("appname") = tmod.getxlbl("xlb205" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "loc"
        dtrow("appname") = tmod.getxlbl("xlb206" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)


        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "asu"
        dtrow("appname") = tmod.getxlbl("xlb207" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '
        dtrow = dtapps.NewRow()
        dtrow("app") = "job"
        dtrow("appname") = tmod.getxlbl("xlb208" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)
        '

        dtrow = dtapps.NewRow()
        dtrow("app") = "rep"
        dtrow("appname") = tmod.getxlbl("xlb209" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)

        dtrow = dtapps.NewRow()
        dtrow("app") = "forum"
        dtrow("appname") = tmod.getxlbl("xlb210" , "intra_utils.vb")
        dtapps.Rows.Add(dtrow)


        Dim ds As New DataSet
        ds.Tables.Add(dtapps)
        Return ds

    End Function
    Public Function CustSites() As DataSet
        Dim dtsites As DataTable = New DataTable("tblsites")
        Dim dtcol As DataColumn
        Dim dtrow As DataRow

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "siteid"
        dtsites.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "site"
        dtsites.Columns.Add(dtcol)

        dtrow = dtsites.NewRow()
        dtrow("siteid") = "12"
        dtrow("site") = "Practice Site"
        dtsites.Rows.Add(dtrow)

        'Add Additional Sites Here

        Dim ds As New DataSet
        ds.Tables.Add(dtsites)
        Return ds

    End Function
End Class
