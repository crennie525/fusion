﻿Imports System.IO
Public Class excel2csv
    Private docret As String
    Public Property XTCS(ByVal fname As String) As String
        Get
            Return x2csv(fname)
        End Get
        Set(ByVal Value As String)
            docret = Value
        End Set
    End Property
    Private Function x2csv(ByVal fname As String) As String
        Dim ret As String = ""
        Dim dupflg As Integer = 0
        Dim dupchk As Integer = 1
        Dim probi As Integer = 0
        Dim objApp As Object
        Dim strDocName As String
        Dim strDocSaveName As String = ""
        objApp = CreateObject("Excel.Application")
        strDocName = fname
        Dim indx As Integer = fname.IndexOf(".xls")
        If indx <> -1 Then
            Dim FilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
            Dim OldFilePath As String = FilePath
            strDocName = OldFilePath
            If File.Exists(FilePath) Then
                fname = Mid(fname, 1, indx) & ".csv"
                FilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                If File.Exists(FilePath) Then
                    dupflg = 1
                End If
                If dupflg = 0 Then
                    Dim NewFilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    strDocSaveName = NewFilePath

                    Try
                        objApp.workbooks.Open(strDocName)
                        objApp.activeworkbook.SaveAs(filename:=strDocSaveName, FileFormat:=6)
                        objApp.activeworkbook.Close()
                        ret = "eok~" & fname
                    Catch ex As Exception
                        ret = "eapp"
                    End Try
                Else
                    fname = Mid(fname, 1, indx) & "(1).csv"
                    Dim DupFilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    While dupflg = 1
                        If File.Exists(DupFilePath) Then
                            dupchk += 1
                            fname = Mid(fname, 1, indx) & "(" & dupchk & ").csv"
                            DupFilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)

                        Else
                            dupflg = 0
                        End If
                        If dupchk = 5 Then
                            probi = 1
                            Exit While
                        End If
                    End While
                    If probi = 0 Then
                        Try
                            DupFilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                            strDocSaveName = DupFilePath
                            objApp.workbooks.Open(strDocName)
                            objApp.activeworkbook.SaveAs(FileName:=strDocSaveName, FileFormat:=6)
                            objApp.activeworkbook.Close()
                            ret = "eok~" & fname
                        Catch ex As Exception
                            ret = "eapp"
                        End Try
                    Else
                        ret = "edup"
                    End If
                End If
            Else
                ret = "enex"
            End If
        Else
            ret = "exst"
        End If

        Return ret
    End Function
End Class
