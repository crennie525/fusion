

'********************************************************
'*
'********************************************************



Public Class cmpToWord
    Public Shared Sub TableToWord(ByVal tblexport As HtmlTable, ByVal response As HttpResponse)
        response.Clear()
        response.Charset = ""
        response.ContentType = "application/msword"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        Dim tbl As New HtmlTable
        tbl = tblexport
        tbl.RenderControl(htmlWrite)
        response.Write(stringWrite.ToString)
        response.End()
    End Sub
End Class
