

'********************************************************
'*
'********************************************************



Public Class transmod
    Public Function getmsg(ByVal id As String, ByVal pg As String) As String
        Dim ret As String
        Dim cmsg As New codemsgs2
        ret = cmsg.GetCodePage(pg, id) '"test"
        Return ret
    End Function
    Public Function getlbl(ByVal id As String, ByVal pg As String) As String
        Dim ret As String
        Dim clab As New codelabs2
        ret = clab.GetCodePage(pg, id) '"test"
        Return ret
    End Function
    Public Function getov(ByVal id As String, ByVal pg As String) As String
        Dim ret As String
        Dim covl As New codeovlibs
        ret = covl.GetCodeOVLib(pg, id) '"test"
        Return ret
    End Function
    Public Function getxlbl(ByVal id As String, Optional ByVal pg As String = "") As String
        Dim ret As String
        Dim xlab As New xlabs
        ret = xlab.GetXLab(pg, id) '"test"
        Return ret
    End Function
End Class
