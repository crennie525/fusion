

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMWOALLTPM

    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pma As New Utilities
    Dim filt As String
    Dim pmidarr As ArrayList = New ArrayList

    Public Function GetWOPMReport(ByVal filt As String) As String

        sql = "select distinct pm.wonum from tpm pm " _
             + "left join equipment e on e.eqid = pm.eqid " _
        + "left join workorder w on w.wonum = pm.wonum " _
        + "left join pmroutes pr on pr.rid = pm.rid " _
        + "left join pmtracktpm pt on pt.pmid = pm.pmid " & filt
        ' left join equipment e on e.eqid = pm.eqid left join workorder w on w.wonum = pm.wonum " & filt
        pma.Open()
        dr = pma.GetRdrData(sql)
        While dr.Read
            If dr.Item("wonum").ToString <> "" Then
                pmidarr.Add(dr.Item("wonum"))
            End If
        End While
        dr.Close()

        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subcnth As Integer = 0
        Dim func, funcchk, task, tasknum, subtask, pm, pm1, psite, wo, pmid As String
        Dim sb As New System.Text.StringBuilder

        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")

        Dim i As Integer
        For i = 0 To pmidarr.Count - 1
            flag = 0
            subcnt = 0
            subcnth = 0
            wo = pmidarr(i)
            sql = "select w.siteid, s.sitename, w.tpmid from workorder w left join sites s on s.siteid = w.siteid where w.wonum = '" & wo & "'"
            dr = pma.GetRdrData(sql)
            While dr.Read
                psite = dr.Item("sitename").ToString
                pmid = dr.Item("tpmid").ToString
            End While
            dr.Close()
            'Dim func, funcchk, task, tasknum, subtask As String
            'Dim sb As New System.Text.StringBuilder

            sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
            sb.Append("<tr><td width=""120""></td><td width=""160""></td><td width=""130""></td><td width=""170""></td><td width=""90""></td></tr>")

            sql = "usp_getwoprint '" & wo & "'"

            dr = pma.GetRdrData(sql)
            Dim headhold As String = "0"
            While dr.Read
                Dim pms As String = dr.Item("pm").ToString
                sb.Append("<tr height=""30"" valign=""top""><td class=""bigbold1"" colspan=""5"" align=""center"">" & psite & " TPM Work Request</td></tr>")
                sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center"">" & Now & "</td></tr>")
                sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""5"" align=""center"">" & pms & "</td></tr>")
                sb.Append("<tr height=""20"" valign=""top""><td class=""plainlabel"" colspan=""5"" align=""center""><b>Work Order#</b>&nbsp;&nbsp;" & wo & "</td></tr>")
                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl228", "woprint.aspx.vb") & "</td></tr>")
                Dim start As String = dr.Item("schedstart").ToString
                If start = "" Then
                    start = dr.Item("targstartdate").ToString
                    If start = "" Then
                        start = Now
                    End If
                End If
                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl229", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & start & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl230", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("actfinish").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl231", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("reportedby").ToString & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl232", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("reportdate").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl233", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("status").ToString & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl234", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("chargenum").ToString & "</td></tr>")

                'sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")

                sb.Append("<tr height=""30""><td class=""label"">" & tmod.getlbl("cdlbl235", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("supervisor").ToString & "</td>")
                sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl236", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("leadcraft").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl237", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""4"">" & dr.Item("description").ToString & "</td></tr>")

                sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl238", "woprint.aspx.vb") & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl239", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("dept_line").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("dept_desc").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl240", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("cell_name").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("cell_desc").ToString & "</td></tr>")

                sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl241", "woprint.aspx.vb") & "</td>")
                sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
                sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("ldesc").ToString & "</td></tr>")

                If dr.Item("eqnum").ToString <> "" Then
                    sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl242", "woprint.aspx.vb") & "</td>")
                    sb.Append("<td class=""plainlabel"">" & dr.Item("eqnum").ToString & "</td>")
                    sb.Append("<td class=""plainlabel"" colspan=""3"">" & dr.Item("eqdesc").ToString & "</td></tr>")
                End If
            End While
            dr.Close()
            'sb.Append("<tr><td>&nbsp;</td></tr>")

            sb.Append("<tr><td colspan=""5""><Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
            sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""640""></td></tr>")
            sql = "usp_getpmmantpm '" & pmid & "'"
            dr = pma.GetRdrData(sql)
            While dr.Read
                If headhold = "0" Then
                    headhold = "1"
                    pm = dr.Item("pm").ToString
                    sb.Append("<tr><td class=""bigbold"" colspan=""5"">&nbsp;</td></tr>")
                    sb.Append("<tr><td class=""bigbold graybot"" colspan=""5"">" & tmod.getlbl("cdlbl243", "woprint.aspx.vb") & "</td></tr>")

                End If

                func = dr.Item("func").ToString
                If funcchk <> func Then 'flag = 0 Then
                    If flag = 0 Then
                        flag = 1
                    Else
                        'sb.Append("<tr><td>&nbsp;</td></tr>")
                    End If

                    funcchk = func
                    sb.Append("<tr height=""20""><td class=""label"" colspan=""3""><u>Function: " & func & "</u></td>")
                    'sb.Append("<td class=""plainlabel"" colspan=""2"">" & func & "</td></tr>")
                    'sb.Append("<tr><td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl244" , "woprint.aspx.vb") & "</td>")
                    'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl245" , "woprint.aspx.vb") & "</td>")
                    'sb.Append("<td class=""btmmenu plainlabel"">" & tmod.getlbl("cdlbl246" , "woprint.aspx.vb") & "</td></tr>")
                End If
                task = dr.Item("task").ToString
                tasknum = dr.Item("tasknum").ToString
                subtask = dr.Item("subtask").ToString
                subcnt = dr.Item("subcnt").ToString


                If subtask = "0" Then
                    'sb.Append("<tr height=""20""><td></td><td class=""label"">" & subcnt & "</td></tr>")
                    sb.Append("<tr><td class=""plainlabel"">[" & tasknum & "]</td>")
                    If Len(dr.Item("task").ToString) > 0 Then
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task</b> :" & task & "</td></tr>")
                    Else
                        sb.Append("<td colspan=""2"" class=""plainlabel""><b>Task:</b> No Task Description Provided</td></tr>")
                    End If
                    'sb.Append("<tr><td>&nbsp;</td></tr>")
                    sb.Append("<tr height=""20""><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel"">Check: OK(___) " & dr.Item("fm1").ToString & "</td></tr>")
                    If dr.Item("lube") <> "none" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">Lubes: " & dr.Item("lube").ToString & "</td></tr>")
                    End If
                    If dr.Item("tool") <> "none" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">Tools: " & dr.Item("tool").ToString & "</td></tr>")
                    End If
                    If dr.Item("part") <> "none" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">Parts: " & dr.Item("part").ToString & "</td></tr>")
                    End If
                    If dr.Item("meas") <> "none" Then
                        sb.Append("<tr><td></td>")
                        sb.Append("<td colspan=""2"" class=""plainlabel"">Measurements: " & dr.Item("meas").ToString & "</td></tr>")
                    End If
                    If subcnt = 0 Then
                        sb.Append("<tr><td>&nbsp;</td></tr>")
                    Else
                        If subcnt <> 0 Then

                            sb.Append("<tr height=""20""><td></td><td class=""label"" colspan=""2""><u>Sub Tasks</u></td></tr>")
                        End If

                    End If

                Else
                    'sb.Append("<tr><td>" & subcnt & "</td></tr>")
                    sb.Append("<tr height=""20""><td></td><td class=""plainlabel"">[" & subtask & "]</td>")
                    If Len(dr.Item("subt").ToString) > 0 Then
                        sb.Append("<td class=""plainlabel""><b>Task</b>: " & dr.Item("subt").ToString & "</td></tr>")
                    Else
                        sb.Append("<td class=""plainlabel""><b>Task</b>: No Sub Task Description Provided</td></tr>")
                    End If
                    sb.Append("<tr><td></td><td></td>")
                    sb.Append("<td class=""plainlabel"">Check: OK(___)</td></tr>")
                    sb.Append("<tr><td>&nbsp;</td></tr>")
                End If
            End While
            dr.Close()

            sb.Append("<tr><td colspan=""5"" class=""plainlabel"">Work Performed by:  ________________________   Date: __________</td></tr>")
            sb.Append("<tr><td colspan=""5"">")
            sb.Append("<table border=""0"">")
            sb.Append("<tr><td class=""plainlabel"">Time&nbsp;(min)</td>")
            sb.Append("<td height=""24"" width=""100"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">REG</td>")
            sb.Append("<td width=""100"" align=""center"" class=""plainlabel"">OT</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Actual:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("<tr><td class=""plainlabel"">Other:</td>")
            sb.Append("<td height=""24"" style=""BORDER-RIGHT: black 1px solid"" align=""center"" class=""plainlabel"">________</td>")
            sb.Append("<td align=""center"" class=""plainlabel"">________</td></tr>")
            sb.Append("</table>")
            sb.Append("</td></tr>")

            sb.Append("</table></td></tr>")
            sb.Append("</table>")
        Next
        sb.Append("</body>")
        pma.Dispose()
        Dim longstring As String = sb.ToString
        Return longstring
    End Function
End Class
