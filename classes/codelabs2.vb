Public Class codelabs2
    Dim dlang As New mmenu_utils
    Dim dlab As New codelabvals
    Public Function GetCodePage(ByVal aspxpage As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Dim lang As String
        Try
            lang = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            lang = dlang.AppDfltLang
        End Try
        Select Case aspxpage
            Case "altitem.aspx.vb"
                ret = dlab.getaltitemlabval(lang, aspxlabel)
            Case "AppSetApprPlan.aspx.vb"
                ret = dlab.getAppSetApprPlanlabval(lang, aspxlabel)
            Case "Assessment_Master.aspx.vb"
                ret = dlab.getAssessment_Masterlabval(lang, aspxlabel)
            Case "assetclass.aspx.vb"
                ret = dlab.getassetclasslabval(lang, aspxlabel)
            Case "AssignmentReport.aspx.vb"
                ret = dlab.getAssignmentReportlabval(lang, aspxlabel)
            Case "AssignmentReportPics.aspx.vb"
                ret = dlab.getAssignmentReportPicslabval(lang, aspxlabel)
            Case "AssignmentReportTPM.aspx.vb"
                ret = dlab.getAssignmentReportTPMlabval(lang, aspxlabel)
            Case "Benchmark_Findings.aspx.vb"
                ret = dlab.getBenchmark_Findingslabval(lang, aspxlabel)
            Case "binlook.aspx.vb"
                ret = dlab.getbinlooklabval(lang, aspxlabel)
            Case "catalog.aspx.vb"
                ret = dlab.getcataloglabval(lang, aspxlabel)
            Case "cclasslookup.aspx.vb"
                ret = dlab.getcclasslookuplabval(lang, aspxlabel)
            Case "cget.aspx.vb"
                ret = dlab.getcgetlabval(lang, aspxlabel)
            Case "clientproc.aspx.vb"
                ret = dlab.getclientproclabval(lang, aspxlabel)
            Case "cltproc.vb"
                ret = dlab.getcltproclabval(lang, aspxlabel)
            Case "commontasks.aspx.vb"
                ret = dlab.getcommontaskslabval(lang, aspxlabel)
            Case "commontaskstpm.aspx.vb"
                ret = dlab.getcommontaskstpmlabval(lang, aspxlabel)
            Case "compclassmain.aspx.vb"
                ret = dlab.getcompclassmainlabval(lang, aspxlabel)
            Case "complibuploadimage.aspx.vb"
                ret = dlab.getcomplibuploadimagelabval(lang, aspxlabel)
            Case "complistdet.aspx.vb"
                ret = dlab.getcomplistdetlabval(lang, aspxlabel)
            Case "complook.aspx.vb"
                ret = dlab.getcomplooklabval(lang, aspxlabel)
            Case "complookup.aspx.vb"
                ret = dlab.getcomplookuplabval(lang, aspxlabel)
            Case "conflicts.aspx.vb"
                ret = dlab.getconflictslabval(lang, aspxlabel)
            Case "CustomCMMShtml.aspx.vb"
                ret = dlab.getCustomCMMShtmllabval(lang, aspxlabel)
            Case "CustomCMMShtmlTPM.aspx.vb"
                ret = dlab.getCustomCMMShtmlTPMlabval(lang, aspxlabel)
            Case "CustomCMMShtmlTPMpics.aspx.vb"
                ret = dlab.getCustomCMMShtmlTPMpicslabval(lang, aspxlabel)
            Case "DeptArch.aspx.vb"
                ret = dlab.getDeptArchlabval(lang, aspxlabel)
            Case "doc.aspx.vb"
                ret = dlab.getdoclabval(lang, aspxlabel)
            Case "doctpm.aspx.vb"
                ret = dlab.getdoctpmlabval(lang, aspxlabel)
            Case "eqtab.aspx.vb"
                ret = dlab.geteqtablabval(lang, aspxlabel)
            Case "eqtab2.aspx.vb"
                ret = dlab.geteqtab2labval(lang, aspxlabel)
            Case "equploadimage.aspx.vb"
                ret = dlab.getequploadimagelabval(lang, aspxlabel)
            Case "FailTrack.aspx.vb"
                ret = dlab.getFailTracklabval(lang, aspxlabel)
            Case "FailTracktpm.aspx.vb"
                ret = dlab.getFailTracktpmlabval(lang, aspxlabel)
            Case "FieldReport.aspx.vb"
                ret = dlab.getFieldReportlabval(lang, aspxlabel)
            Case "FieldReportTPM.aspx.vb"
                ret = dlab.getFieldReportTPMlabval(lang, aspxlabel)
            Case "ForumMain.aspx.vb"
                ret = dlab.getForumMainlabval(lang, aspxlabel)
            Case "funclistdet.aspx.vb"
                ret = dlab.getfunclistdetlabval(lang, aspxlabel)
            Case "inprog.aspx.vb"
                ret = dlab.getinproglabval(lang, aspxlabel)
            Case "invlookup.aspx.vb"
                ret = dlab.getinvlookuplabval(lang, aspxlabel)
            Case "jobplanhtml.aspx.vb"
                ret = dlab.getjobplanhtmllabval(lang, aspxlabel)
            Case "JobPlanList.aspx.vb"
                ret = dlab.getJobPlanListlabval(lang, aspxlabel)
            Case "JobPlanRefReq.aspx.vb"
                ret = dlab.getJobPlanRefReqlabval(lang, aspxlabel)
            Case "LocDets.aspx.vb"
                ret = dlab.getLocDetslabval(lang, aspxlabel)
            Case "logtrack.aspx.vb"
                ret = dlab.getlogtracklabval(lang, aspxlabel)
            Case "lotlook.aspx.vb"
                ret = dlab.getlotlooklabval(lang, aspxlabel)
            Case "lubesmain.aspx.vb"
                ret = dlab.getlubesmainlabval(lang, aspxlabel)
            Case "maxsearch.aspx.vb"
                ret = dlab.getmaxsearchlabval(lang, aspxlabel)
            Case "measoutroutesadd.aspx.vb"
                ret = dlab.getmeasoutroutesaddlabval(lang, aspxlabel)
            Case "measuremain.aspx.vb"
                ret = dlab.getmeasuremainlabval(lang, aspxlabel)
            Case "mmenu1.ascx.vb"
                ret = dlab.getmmenu1labval(lang, aspxlabel)
            Case "OVRollup.aspx.vb"
                ret = dlab.getOVRolluplabval(lang, aspxlabel)
            Case "OVRollup_OP.aspx.vb"
                ret = dlab.getOVRollup_OPlabval(lang, aspxlabel)
            Case "OVRollup_Tot.aspx.vb"
                ret = dlab.getOVRollup_Totlabval(lang, aspxlabel)
            Case "OVRollupTPM.aspx.vb"
                ret = dlab.getOVRollupTPMlabval(lang, aspxlabel)
            Case "PGEMShtml.aspx.vb"
                ret = dlab.getPGEMShtmllabval(lang, aspxlabel)
            Case "PMAcceptancePkg.aspx.vb"
                ret = dlab.getPMAcceptancePkglabval(lang, aspxlabel)
            Case "PMALL.vb"
                ret = dlab.getPMALLlabval(lang, aspxlabel)
            Case "PMALLTPM.vb"
                ret = dlab.getPMALLTPMlabval(lang, aspxlabel)
            Case "PMApproval.aspx.vb"
                ret = dlab.getPMApprovallabval(lang, aspxlabel)
            Case "pmcosts.aspx.vb"
                ret = dlab.getpmcostslabval(lang, aspxlabel)
            Case "pmcoststpm.aspx.vb"
                ret = dlab.getpmcoststpmlabval(lang, aspxlabel)
            Case "PMDivMan.aspx.vb"
                ret = dlab.getPMDivManlabval(lang, aspxlabel)
            Case "PMDivMantpm.aspx.vb"
                ret = dlab.getPMDivMantpmlabval(lang, aspxlabel)
            Case "PMFailMan.aspx.vb"
                ret = dlab.getPMFailManlabval(lang, aspxlabel)
            Case "PMFailMantpm.aspx.vb"
                ret = dlab.getPMFailMantpmlabval(lang, aspxlabel)
            Case "pmlocmain.aspx.vb"
                ret = dlab.getpmlocmainlabval(lang, aspxlabel)
            Case "pmmail.vb"
                ret = dlab.getpmmaillabval(lang, aspxlabel)
            Case "pmportfolio.aspx.vb"
                ret = dlab.getpmportfoliolabval(lang, aspxlabel)
            Case "PMRationaleAll.aspx.vb"
                ret = dlab.getPMRationaleAlllabval(lang, aspxlabel)
            Case "PMRationaleAllTPM.aspx.vb"
                ret = dlab.getPMRationaleAllTPMlabval(lang, aspxlabel)
            Case "PMRationaleReport.aspx.vb"
                ret = dlab.getPMRationaleReportlabval(lang, aspxlabel)
            Case "PMRouteList.aspx.vb"
                ret = dlab.getPMRouteListlabval(lang, aspxlabel)
            Case "PMRouteLU.aspx.vb"
                ret = dlab.getPMRouteLUlabval(lang, aspxlabel)
            Case "pmtaskrationale.aspx.vb"
                ret = dlab.getpmtaskrationalelabval(lang, aspxlabel)
            Case "pmuploadimage.aspx.vb"
                ret = dlab.getpmuploadimagelabval(lang, aspxlabel)
            Case "PMWOALL.vb"
                ret = dlab.getPMWOALLlabval(lang, aspxlabel)
            Case "PMWOALLTPM.vb"
                ret = dlab.getPMWOALLTPMlabval(lang, aspxlabel)
            Case "PrintMeas.aspx.vb"
                ret = dlab.getPrintMeaslabval(lang, aspxlabel)
            Case "PrintMeastpm.aspx.vb"
                ret = dlab.getPrintMeastpmlabval(lang, aspxlabel)
            Case "printmroute.aspx.vb"
                ret = dlab.getprintmroutelabval(lang, aspxlabel)
            Case "printoutmeas.aspx.vb"
                ret = dlab.getprintoutmeaslabval(lang, aspxlabel)
            Case "printpicklist.aspx.vb"
                ret = dlab.getprintpicklistlabval(lang, aspxlabel)
            Case "PrintPM.aspx.vb"
                ret = dlab.getPrintPMlabval(lang, aspxlabel)
            Case "PrintPMtpm.aspx.vb"
                ret = dlab.getPrintPMtpmlabval(lang, aspxlabel)
            Case "printroute.aspx.vb"
                ret = dlab.getprintroutelabval(lang, aspxlabel)
            Case "PrintSched.aspx.vb"
                ret = dlab.getPrintSchedlabval(lang, aspxlabel)
            Case "PrintSchedtpm.aspx.vb"
                ret = dlab.getPrintSchedtpmlabval(lang, aspxlabel)
            Case "purchreq.aspx.vb"
                ret = dlab.getpurchreqlabval(lang, aspxlabel)
            Case "purchreqprint.aspx.vb"
                ret = dlab.getpurchreqprintlabval(lang, aspxlabel)
            Case "reclookup.aspx.vb"
                ret = dlab.getreclookuplabval(lang, aspxlabel)
            Case "reorderdetails.aspx.vb"
                ret = dlab.getreorderdetailslabval(lang, aspxlabel)
            Case "reports2.aspx.vb"
                ret = dlab.getreports2labval(lang, aspxlabel)
            Case "sclasslookup.aspx.vb"
                ret = dlab.getsclasslookuplabval(lang, aspxlabel)
            Case "storeroom.aspx.vb"
                ret = dlab.getstoreroomlabval(lang, aspxlabel)
            Case "totlook.aspx.vb"
                ret = dlab.gettotlooklabval(lang, aspxlabel)
            Case "TPMAlert.aspx.vb"
                ret = dlab.getTPMAlertlabval(lang, aspxlabel)
            Case "tpmnonweekly.aspx.vb"
                ret = dlab.gettpmnonweeklylabval(lang, aspxlabel)
            Case "tpmsub.aspx.vb"
                ret = dlab.gettpmsublabval(lang, aspxlabel)
            Case "tpmuploadimage.aspx.vb"
                ret = dlab.gettpmuploadimagelabval(lang, aspxlabel)
            Case "tpmweekly2.aspx.vb"
                ret = dlab.gettpmweekly2labval(lang, aspxlabel)
            Case "tpmwr.aspx.vb"
                ret = dlab.gettpmwrlabval(lang, aspxlabel)
            Case "units.aspx.vb"
                ret = dlab.getunitslabval(lang, aspxlabel)
            Case "UserAdmin.aspx.vb"
                ret = dlab.getUserAdminlabval(lang, aspxlabel)
            Case "woact.aspx.vb"
                ret = dlab.getwoactlabval(lang, aspxlabel)
            Case "wocomp.aspx.vb"
                ret = dlab.getwocomplabval(lang, aspxlabel)
            Case "wocosts.aspx.vb"
                ret = dlab.getwocostslabval(lang, aspxlabel)
            Case "wojpcompman.aspx.vb"
                ret = dlab.getwojpcompmanlabval(lang, aspxlabel)
            Case "wojppart.aspx.vb"
                ret = dlab.getwojppartlabval(lang, aspxlabel)
            Case "wolist.aspx.vb"
                ret = dlab.getwolistlabval(lang, aspxlabel)
            Case "wolistmini.aspx.vb"
                ret = dlab.getwolistminilabval(lang, aspxlabel)
            Case "woprint.aspx.vb"
                ret = dlab.getwoprintlabval(lang, aspxlabel)
            Case "woprint_pmpics.aspx.vb"
                ret = dlab.getwoprint_pmpicslabval(lang, aspxlabel)
            Case "wrdown.vb"
                ret = dlab.getwrdownlabval(lang, aspxlabel)
            Case "wrmail.vb"
                ret = dlab.getwrmaillabval(lang, aspxlabel)
        End Select
        Return ret
    End Function
End Class
