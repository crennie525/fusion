

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class TPMHTM2

    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid As String
    Public Function WritePMHTM(ByVal eqid As String, Optional ByVal pdm As String = "") As String

        Dim sb As New System.Text.StringBuilder
        If pdm <> "" Then
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        Else
            sql = "usp_getWITotalTPM '" & eqid & "'"
        End If

        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim task, lube, meas, revtask, revtask2, revtask3, revtask4 As String
        Dim tlem, splen, bklen, splen2, bklen2, splen3, bklen3, splen4, bklen4 As Integer
        Dim parts, tools, lubes, tasknum As String
        parts = ""
        tools = ""
        lubes = ""
        tasknum = ""
        Dim lines As Integer = 1
        pms.Open()
        dr = pms.GetRdrData(sql)
        Dim pretech, pretechchk As String
        While dr.Read
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            'pretech = dr.Item("ptid").ToString
            'parts = dr.Item("parts").ToString
            'tools = dr.Item("tools").ToString
            'lubes = dr.Item("lubes").ToString
            Dim skchk, skchk2 As String
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            If skchk <> skchk2 Then 'OrElse pretech <> pretechchk Then
                If skillchk <> "" Then
                    sb.Append("<br>")
                    sb.Append(tmod.getxlbl("xlb136" , "TPMHTM2.vb") & "<br>")
                    'parts = dr.Item("parts").ToString
                    Dim partarr As String() = Split(parts, ";")
                    Dim p As Integer
                    For p = 0 To partarr.Length - 1
                        If partarr(p) = "none" Then 'Len(partarr(p)) = 0 Or 
                            sb.Append(tmod.getxlbl("xlb137" , "TPMHTM2.vb") & "<br>")
                        Else
                            sb.Append(partarr(p) & "<br>")
                        End If
                    Next

                    sb.Append("<br>")
                    sb.Append(tmod.getxlbl("xlb138" , "TPMHTM2.vb") & "<br>")
                    'tools = dr.Item("tools").ToString
                    Dim toolarr As String() = Split(tools, ";")
                    Dim t As Integer
                    For t = 0 To toolarr.Length - 1
                        If toolarr(t) = "none" Then 'Len(toolarr(t)) = 0 Or 
                            sb.Append(tmod.getxlbl("xlb139" , "TPMHTM2.vb") & "<br>")
                        Else
                            sb.Append(toolarr(t) & "<br>")
                        End If
                    Next

                    sb.Append("<br>")
                    sb.Append(tmod.getxlbl("xlb140" , "TPMHTM2.vb") & "<br>")
                    'lubes = dr.Item("lubes").ToString
                    Dim lubearr As String() = Split(lubes, ";")
                    Dim l As Integer
                    For l = 0 To lubearr.Length - 1
                        If lubearr(l) = "none" Then 'Len(lubearr(l)) = 0 Or 
                            sb.Append(tmod.getxlbl("xlb141" , "TPMHTM2.vb") & "<br>")
                        Else
                            sb.Append(lubearr(l) & "<br>")
                        End If

                    Next
                    parts = ""
                    tools = ""
                    lubes = ""
                    sb.Append("<br>")
                    sb.Append("**********************************************************************************************************<br>")
                    sb.Append("**********************************************************************************************************")
                    sb.Append("<br>")

                End If
                skillchk = skill
                start = 0
                flag = 0
                If pdm <> "" Then
                    pretechchk = pretech
                    sb.Append("PdM - " & dr.Item("pretech").ToString & " - " & dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString & "<br>")
                Else
                    sb.Append(dr.Item("freq").ToString & " / " & tmod.getxlbl("xlb144" , "TPMHTM2.vb") & " / " & dr.Item("rd").ToString & "<br>")
                End If
                sb.Append(tmod.getxlbl("xlb142" , "TPMHTM2.vb") & "  " & dr.Item("down").ToString & " " & tmod.getxlbl("xlb143" , "TPMHTM2.vb") & "<br>")
                sb.Append(dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "<br><br>")
                sb.Append("<br>")
                sb.Append(tmod.getxlbl("xlb145" , "TPMHTM2.vb"))
                sb.Append("<br>")
            End If
            If start <> 0 Then
                sb.Append("<br>")
                sb.Append("**********************************************************************************************************")
            End If
            func = dr.Item("func").ToString
            If flag = 0 Then
                flag = 1
                funcchk = func
                sb.Append("<br>")
                sb.Append(dr.Item("func").ToString)
                sb.Append("<br><br>")
            Else
                If func <> funcchk Then

                    funcchk = func
                    sb.Append("<br>")
                    sb.Append("**********************************************************************************************************")
                    sb.Append("<br>")
                    sb.Append(dr.Item("func").ToString)
                    sb.Append("<br><br>")
                End If
            End If
            Dim subtask = dr.Item("subtask").ToString
            Dim subt = dr.Item("subt").ToString
            task = dr.Item("task").ToString
            If dr.Item("lubes").ToString <> "none" Or Len(dr.Item("lubes").ToString) = 0 Then
                lube = "; " & dr.Item("lubes").ToString
            Else
                lube = ""
            End If
            If dr.Item("meas").ToString <> "none" Then
                meas = "; " & dr.Item("meas").ToString
            Else
                meas = ""
            End If
            If subtask = 0 Then
                task = task & lube & meas
                tlem = Len(task)
            Else
                task = subt
                tlem = Len(subt)
            End If
            If tlem <= 103 Then
                lines = 1
            End If
            If tlem > 103 Then
                lines = 2
                revtask = StrReverse(Mid(task, 1, 103))
                revtask = Trim(revtask)
                Dim rev As Integer = Len(revtask)
                splen = revtask.IndexOf(" ")
                bklen = rev - splen
            End If
            If tlem > 206 Then
                lines = 3
                revtask2 = StrReverse(Mid(task, bklen, 103))
                revtask2 = Trim(revtask2)
                Dim rev2 As Integer = Len(revtask2)
                splen2 = revtask2.IndexOf(" ")
                bklen2 = (rev2) - splen2 + 1
            End If
            If tlem > 309 Then
                lines = 4
                revtask3 = StrReverse(Mid(task, bklen2, 103))
                revtask3 = Trim(revtask3)
                Dim rev3 As Integer = Len(revtask3)
                splen3 = revtask3.IndexOf(" ")
                bklen3 = (rev3) - splen3 + 1
            End If
            If tlem > 412 Then
                lines = 5
                revtask4 = StrReverse(Mid(task, bklen3, 103))
                revtask4 = Trim(revtask4)
                Dim rev4 As Integer = Len(revtask4)
                splen4 = revtask4.IndexOf(" ")
                bklen4 = (rev4) - splen4 + 1
            End If
            Select Case lines
                Case 1
                    If subtask <> 0 Then
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & task)
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & task)
                    End If
                Case 2
                    If subtask <> 0 Then
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sb.Append("  " & Mid(task, bklen, tlem))
                Case 3
                    If subtask <> 0 Then
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sb.Append("  " & Mid(task, bklen, bklen2))
                    sb.Append("  " & Mid(task, bklen + bklen2, tlem))
                Case 4
                    If subtask <> 0 Then
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sb.Append("  " & Mid(task, bklen, bklen2))
                    sb.Append("  " & Mid(task, bklen + bklen2, bklen3))
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3, tlem))
                Case 5
                    If subtask <> 0 Then
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sb.Append("  " & Mid(task, bklen, bklen2))
                    sb.Append("  " & Mid(task, bklen + bklen2, bklen3))
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3, bklen4))
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem))
                Case Else
                    If subtask <> 0 Then
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen))
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen))
                    End If
                    sb.Append("  " & Mid(task, bklen, bklen2))
                    sb.Append("  " & Mid(task, bklen + bklen2, bklen3))
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3, bklen4))
                    sb.Append("  " & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem))
            End Select
            sb.Append("   OK(___) " & dr.Item("fm1").ToString & "<br>")
            tasknum = dr.Item("tasknum").ToString
            If dr.Item("parts").ToString <> "none" Then 'Or Len(dr.Item("parts").ToString) = 0 Then
                If Len(parts) = 0 Then
                    parts += dr.Item("parts").ToString
                Else
                    parts += ";" & dr.Item("parts").ToString
                End If
            End If

            If dr.Item("tools").ToString <> "none" Then 'Or Len(dr.Item("tools").ToString) <> 0 Then
                If Len(tools) = 0 Then
                    tools += dr.Item("tools").ToString
                Else
                    tools += ";" & dr.Item("tools").ToString
                End If

            End If

            If dr.Item("lubes").ToString <> "none" Then 'Or Len(dr.Item("lubes").ToString) <> 0 Then
                If Len(lubes) = 0 Then
                    lubes += dr.Item("lubes").ToString
                Else
                    lubes += ";" & dr.Item("lubes").ToString
                End If

            End If

        End While
        dr.Close()
        sb.Append("<br>")
        sb.Append(tmod.getxlbl("xlb146" , "TPMHTM2.vb") & "<br>")

        Dim partarr1 As String() = Split(parts, ";")
        Dim p1 As Integer
        For p1 = 0 To partarr1.Length - 1
            If Len(partarr1(p1)) = 0 Or partarr1(p1) = "none" Then
                sb.Append(tmod.getxlbl("xlb147" , "TPMHTM2.vb") & "<br>")
            Else
                sb.Append(partarr1(p1) & "<br>")
            End If
        Next

        sb.Append("<br>")
        sb.Append(tmod.getxlbl("xlb148" , "TPMHTM2.vb") & "<br>")

        Dim toolarr1 As String() = Split(tools, ";")
        Dim t1 As Integer
        For t1 = 0 To toolarr1.Length - 1
            If Len(toolarr1(t1)) = 0 Or toolarr1(t1) = "none" Then
                sb.Append(tmod.getxlbl("xlb149" , "TPMHTM2.vb") & "<br>")
            Else
                sb.Append(toolarr1(t1) & "<br>")
            End If
        Next

        sb.Append("<br>")
        sb.Append(tmod.getxlbl("xlb150" , "TPMHTM2.vb") & "<br>")

        Dim lubearr1 As String() = Split(lubes, ";")
        Dim l1 As Integer
        For l1 = 0 To lubearr1.Length - 1
            If Len(lubearr1(l1)) = 0 Or lubearr1(l1) = "none" Then
                sb.Append(tmod.getxlbl("xlb151" , "TPMHTM2.vb") & "<br>")
            Else
                sb.Append(lubearr1(l1) & "<br>")
            End If

        Next
        sb.Append("                                                                                                          ")
        sb.Append("**********************************************************************************************************<br>")
        sb.Append("**********************************************************************************************************")
        sb.Append("                                                                                                          ")

        pms.Dispose()
        Dim longstring As String = sb.ToString
        Return longstring
    End Function
End Class

