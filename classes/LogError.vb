

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Web.SessionState
Imports System.Web
Imports System.Configuration
Imports System.IO
Imports System.Text

Public Class LogError
    Private mobjException As Exception
    Private mobjHTTPSessionState As HttpSessionState
    Private mobjHTTPRequest As HttpRequest
    Private mstrErrorAsString As String

    Public Sub New(ByVal pobjException As Exception, ByVal pobjHTTPSessionState As HttpSessionState, ByVal pobjHTTPRequest As HttpRequest)
        mobjException = pobjException
        mobjHTTPSessionState = pobjHTTPSessionState
        mobjHTTPRequest = pobjHTTPRequest
    End Sub
    ReadOnly Property ErrorAsString() As String
        Get
            'if this text hasn't been set yet, then fill it in.
            If mstrErrorAsString Is Nothing Then mstrErrorAsString = Me.GetErrorAsString
            Return mstrErrorAsString
        End Get

    End Property

    Public Sub LogError()
        'read in the XML config settings and see what logging options should be used.
        Dim strEventLog As String = System.Configuration.ConfigurationManager.AppSettings.Item("ErrorLoggingEventLogType")
        Dim strLogFile As String = System.Configuration.ConfigurationManager.AppSettings.Item("ErrorLoggingLogFile")

        Try
            If System.Configuration.ConfigurationManager.AppSettings.Item("ErrorLoggingLogToFile").ToUpper = "TRUE" Then LogToEmail() 'LogToFile(strLogFile)
        Catch
            'don't bomb out
        End Try

        Try
            If System.Configuration.ConfigurationManager.AppSettings.Item("ErrorLoggingLogToDB").ToUpper = "TRUE" Then LogToDB()
        Catch ex As Exception
            'don't bomb out
        End Try


    End Sub

   

    Private Sub LogToDB()
        Dim objTempException As Exception
        Dim intExceptionLevel As Integer
        Dim strConnect As String = System.Configuration.ConfigurationManager.AppSettings.Item("ErrorLoggingConnectString")
        Dim objSqlCmd As New SqlCommand("Proc_CreateErrorEntry", New SqlConnection(strConnect))
        Dim strFormData As String
        Dim innerex As String
        Dim stackstr As String

        objSqlCmd.CommandType = CommandType.StoredProcedure
        Dim objSQLParam As SqlParameter

        objSQLParam = objSqlCmd.Parameters.Add("@pstrSessionID", SqlDbType.VarChar, 40)
        objSQLParam.Value = mobjHTTPSessionState.SessionID

        objSQLParam = objSqlCmd.Parameters.Add("@pstrRequestMethod", SqlDbType.VarChar, 5)
        objSQLParam.Value = mobjHTTPRequest.ServerVariables("REQUEST_METHOD")

        objSQLParam = objSqlCmd.Parameters.Add("@pintServerPort", SqlDbType.Int)
        objSQLParam.Value = mobjHTTPRequest.ServerVariables("SERVER_PORT")

        objSQLParam = objSqlCmd.Parameters.Add("@pstrHTTPS", SqlDbType.VarChar, 3)
        objSQLParam.Value = mobjHTTPRequest.ServerVariables("HTTPS")

        objSQLParam = objSqlCmd.Parameters.Add("@pstrLocalAddr", SqlDbType.VarChar, 15)
        objSQLParam.Value = mobjHTTPRequest.ServerVariables("LOCAL_ADDR")

        objSQLParam = objSqlCmd.Parameters.Add("@pstrHostAddress", SqlDbType.VarChar, 15)
        objSQLParam.Value = mobjHTTPRequest.ServerVariables("REMOTE_ADDR")

        objSQLParam = objSqlCmd.Parameters.Add("@pstrUserAgent", SqlDbType.VarChar, 255)
        objSQLParam.Value = mobjHTTPRequest.ServerVariables("HTTP_USER_AGENT")

        objSQLParam = objSqlCmd.Parameters.Add("@pstrURL", SqlDbType.VarChar, 400)
        objSQLParam.Value = mobjHTTPRequest.ServerVariables("URL")

        objSQLParam = objSqlCmd.Parameters.Add("@pstrCustomerRefID", SqlDbType.VarChar, 40)
        objSQLParam.Value = mobjHTTPSessionState.SessionID

        objSQLParam = objSqlCmd.Parameters.Add("@pstrFormData", SqlDbType.VarChar, 2000)
        'this field is 2000 chars long. The form data may be longer, so to avoid an error, return only a portion of it.
        'if you require a longer field, modify the database and the 2000 above and below and make sure if using sql server
        'that your total record length does not go over sql server's 8k limit unless you change the FormData field to a a text data type.
        strFormData = Me.GetStringFromArray(mobjHTTPRequest.Form.AllKeys)

        If strFormData.Length > 2000 Then
            objSQLParam.Value = strFormData.Substring(0, 2000)
        Else
            objSQLParam.Value = strFormData
        End If

        objSQLParam = objSqlCmd.Parameters.Add("@pstrAllHTTP", SqlDbType.VarChar, 2000)
        objSQLParam.Value = Replace(mobjHTTPRequest.ServerVariables("ALL_HTTP"), vbLf, vbCrLf)

        objSQLParam = objSqlCmd.Parameters.Add("@pdteInsertDate", SqlDbType.DateTime)
        objSQLParam.Value = System.DateTime.Now

        objSQLParam = objSqlCmd.Parameters.Add("@pblnIsCookieLess", SqlDbType.Bit)
        objSQLParam.Value = mobjHTTPSessionState.IsCookieless

        objSQLParam = objSqlCmd.Parameters.Add("@pblnIsNewSession", SqlDbType.Bit)
        objSQLParam.Value = mobjHTTPSessionState.IsNewSession
        Dim intID As Integer
        'Try
        objSqlCmd.Connection.Open()
        intID = CInt(objSqlCmd.ExecuteScalar())
        'Catch ex As Exception
        'Throw ex
        'End Try


        'Each exception can have an inner exception, providing more details as to the source of the error.
        'loop through these and log to the database. Each one will be related to it's parent exception by
        'use of a integer "ExceptionLevel" flag. Level 1 is the top level, 2 is a child to 1, 3 is a child to 2, etc etc
        objTempException = mobjException
        While Not (objTempException Is Nothing)
            'is this the 1st, 2nd, etc exception in the hierarchy
            intExceptionLevel += 1
            objSqlCmd.CommandText = "Proc_LogException1"
            objSqlCmd.Parameters.Clear()

            objSQLParam = objSqlCmd.Parameters.Add("@pintSessionErrorID", SqlDbType.Int)
            objSQLParam.Value = intID

            objSQLParam = objSqlCmd.Parameters.Add("@pintExceptionLevel", SqlDbType.Int)
            objSQLParam.Value = intExceptionLevel

            objSQLParam = objSqlCmd.Parameters.Add("@pstrMessage", SqlDbType.VarChar, 1000)
            objSQLParam.Value = objTempException.Message

            objSQLParam = objSqlCmd.Parameters.Add("@pstrSource", SqlDbType.VarChar, 200)
            objSQLParam.Value = objTempException.Source

            objSQLParam = objSqlCmd.Parameters.Add("@pstrStackTrace", SqlDbType.VarChar, 4000)
            objSQLParam.Value = objTempException.StackTrace.ToString

            objSQLParam = objSqlCmd.Parameters.Add("@pstrTargetSite", SqlDbType.VarChar, 100)
            objSQLParam.Value = objTempException.TargetSite.ToString

            objSQLParam = objSqlCmd.Parameters.Add("@userid", SqlDbType.VarChar, 50)
            objSQLParam.Value = HttpContext.Current.Session("username").ToString()

            objSQLParam = objSqlCmd.Parameters.Add("@errorapp", SqlDbType.VarChar, 50)
            objSQLParam.Value = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString()

            objSQLParam = objSqlCmd.Parameters.Add("@inner", SqlDbType.VarChar, 4000)
            innerex = objTempException.InnerException.ToString
            If innerex.Length > 4000 Then
                objSQLParam.Value = innerex.Substring(0, 4000)
            Else
                objSQLParam.Value = innerex
            End If



            'Try
            objSqlCmd.ExecuteNonQuery()
            'Catch ex As Exception
            'log to event log that the db logging failed.
            'Throw ex
            'End Try

            'get the next exception to log
            objTempException = objTempException.InnerException
        End While

        objSqlCmd.Connection.Close()

    End Sub
    Private Sub LogToEmail()
        Dim objSB As New StringBuilder
        objSB.Append(Me.ErrorAsString)
        Dim email As New System.Web.Mail.MailMessage
        email.To = "system_admin@laisoftware.com"
        email.From = "system_admin@laisoftware.com"
        'email.Cc = "estanekjr@laireliability.com"
        'email.Bcc = TextBox5.Text '""

        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "mail.laisoftware.com"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = "system_admin"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "sysadm1"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
        email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = 25


        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If

        email.Body = objSB.ToString
        email.Subject = "More Good News From The Fat Guy"
        email.BodyFormat = Web.Mail.MailFormat.Text
        System.Web.Mail.SmtpMail.SmtpServer.Insert(0, "mail.laisoftware.com")

        System.Web.Mail.SmtpMail.Send(email)
    End Sub
    Private Sub LogToFile(ByVal pstrFileName As String)
        Dim objSRLogFile As New StreamWriter(pstrFileName, True)

        Try
            objSRLogFile.Write(Me.ErrorAsString)
            objSRLogFile.Close()
        Catch ex As Exception
            'hopefully you also have the log to event log selected and that may have worked. If not, you will probably
            'not know the error information since the attempt to log to file failed. 
            'It may have failed for some reason writing to a file (this could be because of an exclusive lock on the file
            'or security permissions, aspnet_wp may not have permissions to the file, etc etc. 

            'one option here may be shell off an email or net send, depending on your system requirements.
            Throw ex
        End Try


    End Sub


    Private Function GetErrorAsString() As String
        Dim objTempException As Exception
        Dim intExceptionLevel As Integer
        Dim strIndent As String

        Dim app As String = System.Configuration.ConfigurationManager.AppSettings.Item("custAppName")
        Dim username As String = HttpContext.Current.Session("username").ToString()
        Dim uid As String = HttpContext.Current.Session("uid").ToString()

        Dim objSB As New StringBuilder
        Dim strFormData As String
        objSB.Append("-----------------" & System.DateTime.Now.ToString & "-----------------" & vbCrLf)
        objSB.Append("SessionID:" & mobjHTTPSessionState.SessionID & vbCrLf)
        objSB.Append("Application:" & app & vbCrLf)
        objSB.Append("User Name:" & username & vbCrLf)
        objSB.Append("User ID:" & uid & vbCrLf)

        strFormData = vbTab & Me.GetStringFromArray(mobjHTTPRequest.Form.AllKeys).Replace(vbCrLf, vbCrLf & vbTab)
        If strFormData.Length > 1 Then '1 because if the form is empty it will just contain the tab prefixed to the line.
            objSB.Append("Form Data:" & vbCrLf)
            'remove the last tab so it doesn't screw up formatting on the line after it.
            objSB.Append(strFormData.Substring(0, strFormData.Length - 1))
        Else
            objSB.Append("Form Data: No Form Data Found")
        End If

        objTempException = mobjException
        While Not (objTempException Is Nothing)
            'is this the 1st, 2nd, etc exception in the hierarchy
            intExceptionLevel += 1
            objSB.Append(intExceptionLevel & ": Error Description:" & objTempException.Message & vbCrLf)

            objSB.Append(intExceptionLevel & ": Source:" & Replace(objTempException.Source, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            objSB.Append(intExceptionLevel & ": Stack Trace:" & Replace(objTempException.StackTrace, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            objSB.Append(intExceptionLevel & ": Target Site:" & Replace(objTempException.TargetSite.ToString, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            'get the next exception to log
            objTempException = objTempException.InnerException

        End While

        Return objSB.ToString()

    End Function

    'There may be a better way in .Net to do this, but as of now I am not aware of one. 
    'since Array.ToString doesn't yet exist (and why should it, since it is would have to assume 
    'all types of array and be converted to a string, which is not a true statement (unless 
    'each class is coded to provide its own ToString)
    Private Function GetStringFromArray(ByVal pobjArray As String()) As String
        Dim strItem As String
        Dim i As Integer
        Dim objSB As New StringBuilder
        Dim strKey As String

        For i = pobjArray.GetLowerBound(0) To pobjArray.GetUpperBound(0)
            strKey = CType(pobjArray.GetValue(i), String)
            objSB.Append(strKey & " - " & mobjHTTPRequest.Form.Item(strKey) & vbCrLf)
        Next i
        Return objSB.ToString
    End Function
End Class
