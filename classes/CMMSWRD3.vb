Imports System.Data.SqlClient
Imports System.text
Public Class CMMSWRD3
    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid, mode As String
    Public Sub CMMSToWord(ByVal eqid As String, ByVal mode As String, ByVal inout As String, ByVal response As HttpResponse)
        HttpContext.Current.Response.ContentType = "application/msword"

        HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8

        HttpContext.Current.Response.Charset = "UTF-8"

        If inout = "yes" Then
            response.AddHeader("Content-Disposition", "attachment; filename=" & "CMMS Output to Word.doc")
        End If

        response.Write("<html>")

        response.Write("<head>")

        response.Write("<META HTTP-EQUIV=""Content-Type"" CONTENT=""text/html; charset=UTF-8"">")

        response.Write("<meta name=ProgId content=Word.Document>")

        response.Write("<meta name=Generator content=""Microsoft Word 9"">")

        response.Write("<meta name=Originator content=""Microsoft Word 9"">")

        response.Write("<style>")

        response.Write("@page Section1 {size:595.45pt 841.7pt; margin:1.0in .5in 1.2in .5in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")

        response.Write("div.Section1 {page:Section1;}")

        response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:1.25in 1.0in 1.25in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")

        response.Write("div.Section2 {page:Section2;}")

        response.Write("</style>")

        response.Write("</head>")

        response.Write("<body>")

        response.Write("<div class=Section1>")


        Dim test As String = WritePMHTM(eqid, mode)
        response.Write(test)

        response.Write("</div>")

        response.Write("</body>")

        response.Write("</html>")

        HttpContext.Current.Response.Flush()


    End Sub
    Public Function WritePMHTM(ByVal eqid As String, Optional ByVal pdm As String = "") As String
        pms.Open()
        sql = "usp_UpdateFMRep '" & eqid & "'"
        pms.Update(sql)

        Dim sb As New System.Text.StringBuilder
        If pdm <> "" Then
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        Else
            sql = "usp_getWITotalPGEMS '" & eqid & "'"
        End If

        Dim top As Integer = 0

        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subhold As Integer = 0
        Dim task, lube, meas, revtask, revtask2, revtask3, revtask4 As String
        Dim tlem, splen, bklen, splen2, bklen2, splen3, bklen3, splen4, bklen4 As Integer
        Dim parts, tools, lubes, tasknum, running, down As String
        parts = ""
        tools = ""
        lubes = ""
        tasknum = ""
        Dim parts_plus As String = ""
        Dim tools_plus As String = ""
        Dim lubes_plus As String = ""
        Dim lines As Integer = 1

        dr = pms.GetRdrData(sql)
        Dim pretech, pretechchk As String
        sb.Append("<h2>CMMS Datastream Output to Word</h2>")
        'sb.Append("<br>")
        sb.Append(Now)
        sb.Append("<br><br>")
        While dr.Read
            If top = 0 Then
                top = 1
                sb.Append("<h3>" & dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "</h3><br>")
            End If
            skill = dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString
            pretech = dr.Item("ptid").ToString

            Dim skchk, skchk2 As String
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            If skchk <> skchk2 Then 'OrElse pretech <> pretechchk Then
                If skillchk <> "" Then
                    sb.Append("<br>")
                    sb.Append("Parts" & "<br>")
                    'parts = dr.Item("parts").ToString
                    Dim partarr As String() = Split(parts, ";")
                    Dim p As Integer
                    For p = 0 To partarr.Length - 1
                        If partarr(p) = "none" Then 'Len(partarr(p)) = 0 Or 
                            sb.Append("No Parts Found<br>")
                        Else
                            sb.Append(partarr(p) & "<br>")
                        End If
                    Next

                    sb.Append("<br>")
                    sb.Append("Tools" & "<br>")
                    'tools = dr.Item("tools").ToString
                    Dim toolarr As String() = Split(tools, ";")
                    Dim t As Integer
                    For t = 0 To toolarr.Length - 1
                        If toolarr(t) = "none" Then 'Len(toolarr(t)) = 0 Or 
                            sb.Append("No Tools Found<br>")
                        Else
                            sb.Append(toolarr(t) & "<br>")
                        End If
                    Next

                    sb.Append("<br>")
                    sb.Append("Lubes" & "<br>")
                    'lubes = dr.Item("lubes").ToString
                    Dim lubearr As String() = Split(lubes, ";")
                    Dim l As Integer
                    For l = 0 To lubearr.Length - 1
                        If lubearr(l) = "none" Then 'Len(lubearr(l)) = 0 Or 
                            sb.Append("No Lubricants Found<br>")
                        Else
                            sb.Append(lubearr(l) & "<br>")
                        End If

                    Next
                    parts = ""
                    tools = ""
                    lubes = ""

                    parts_plus = ""
                    tools_plus = ""
                    lubes_plus = ""

                    sb.Append("<br>")
                    sb.Append("Work Performed by:  ________________________   Date: __________<br><br>")

                    sb.Append("<table border=""0"">")
                    sb.Append("<tr><td>Time&nbsp;(min)</td>")
                    sb.Append("<td height=""26"" width=""100"" style=""BORDER-RIGHT: black 1px solid"" align=""center"">REG</td>")
                    sb.Append("<td width=""100"" align=""center"">OT</td></tr>")
                    sb.Append("<tr><td>Actual:</td>")
                    sb.Append("<td height=""26"" style=""BORDER-RIGHT: black 1px solid"" align=""center"">________</td>")
                    sb.Append("<td align=""center"">________</td></tr>")
                    sb.Append("<tr><td>Other:</td>")
                    sb.Append("<td height=""26"" style=""BORDER-RIGHT: black 1px solid"" align=""center"">________</td>")
                    sb.Append("<td align=""center"">________</td></tr>")
                    sb.Append("</table>")

                    'sb.Append("Time:&nbsp;(min)	REG  	   |        OT<br>")
                    'sb.Append("Actual:&nbsp;&nbsp;&nbsp;&nbsp;_________&nbsp;|&nbsp;_________<br>")
                    'sb.Append("Other:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________&nbsp;|&nbsp;_________<br>")

                    sb.Append("<br><br>")

                End If
                skillchk = skill
                start = 0
                flag = 0
                If pdm <> "" Then
                    pretechchk = pretech
                    sb.Append("PdM - " & dr.Item("pretech").ToString & " - " & dr.Item("skill").ToString & " / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString & "<br>")
                Else
                    sb.Append(skill & "<br>")  'sb.Append("Operator / " & dr.Item("freq").ToString & " / " & dr.Item("rd").ToString & "<br>")
                End If
                down = dr.Item("down").ToString
                running = dr.Item("running").ToString
                If down <> "0" Then
                    sb.Append("Estimated Total Task Time:  " & dr.Item("down").ToString & " Minutes<br>")
                Else
                    sb.Append("Estimated Total Task Time:  " & dr.Item("running").ToString & " Minutes<br>")
                End If
                'sb.Append("Department: " & dr.Item("dept_line").ToString & "&nbsp;&nbsp;&nbsp&nbsp;Station\Cell " & dr.Item("cell_name").ToString & "<br>")

                sb.Append(dr.Item("eqnum").ToString & " - " & dr.Item("eqdesc").ToString & "<br>")
                'sb.Append("<br>")
                'sb.Append("Work Instructions")
                'sb.Append("<br>")
            End If
            If start <> 0 Then
                sb.Append("<br>")
                'sb.Append("***************************************************************************************")
                'sb.Append("**********************************************************************************************************")
            End If
            func = dr.Item("func").ToString
            If flag = 0 Then
                flag = 1
                funcchk = func
                sb.Append("<br>")
                sb.Append("<b>" & dr.Item("func").ToString & "</b> - <i>Function Routing (" & dr.Item("routing").ToString & ")</i>")
                sb.Append("<br><br>")
            Else
                If func <> funcchk Then

                    funcchk = func
                    'sb.Append("<br>")
                    'sb.Append("**********************************************************************************************************")
                    'sb.Append("***************************************************************************************")
                    'sb.Append("<br>")
                    sb.Append("<b>" & dr.Item("func").ToString & "</b> - <i>Function Routing (" & dr.Item("routing").ToString & ")</i>")
                    sb.Append("<br><br>")
                End If
            End If
            Dim subtask = dr.Item("subtask").ToString
            Dim subt = dr.Item("subt").ToString
            task = dr.Item("task").ToString
            If dr.Item("lubes").ToString <> "none" Or Len(dr.Item("lubes").ToString) = 0 Then
                lube = "" '"; " & dr.Item("lubes").ToString
            Else
                lube = ""
            End If

            meas = ""

            If subtask = 0 Then
                task = task & lube & meas
                tlem = Len(task)
            Else
                task = subt
                tlem = Len(subt)
            End If
            If tlem <= 103 Then
                lines = 1
            End If
            If tlem > 103 Then
                lines = 2
                revtask = StrReverse(Mid(task, 1, 103))
                revtask = Trim(revtask)
                Dim rev As Integer = Len(revtask)
                splen = revtask.IndexOf(" ")
                bklen = rev - splen
            End If
            If tlem > 206 Then
                lines = 3
                revtask2 = StrReverse(Mid(task, bklen, 103))
                revtask2 = Trim(revtask2)
                Dim rev2 As Integer = Len(revtask2)
                splen2 = revtask2.IndexOf(" ")
                bklen2 = (rev2) - splen2 + 1
            End If
            If tlem > 309 Then
                lines = 4
                revtask3 = StrReverse(Mid(task, bklen2, 103))
                revtask3 = Trim(revtask3)
                Dim rev3 As Integer = Len(revtask3)
                splen3 = revtask3.IndexOf(" ")
                bklen3 = (rev3) - splen3 + 1
            End If
            If tlem > 412 Then
                lines = 5
                revtask4 = StrReverse(Mid(task, bklen3, 103))
                revtask4 = Trim(revtask4)
                Dim rev4 As Integer = Len(revtask4)
                splen4 = revtask4.IndexOf(" ")
                bklen4 = (rev4) - splen4 + 1
            End If
            Select Case lines
                Case 1
                    If subtask <> 0 Then
                        sb.Append("&nbsp;&nbsp;" & dr.Item("tasknum").ToString & "." & "" & subtask & "&nbsp;" & task & "<br>")
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & task & "<br>")
                    End If
                Case 2
                    If subtask <> 0 Then
                        sb.Append("&nbsp;&nbsp;" & dr.Item("tasknum").ToString & "." & "" & subtask & "&nbsp;" & Mid(task, 1, bklen) & "<br>")
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    End If
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen, tlem) & "<br>")
                Case 3
                    If subtask <> 0 Then
                        sb.Append("&nbsp;&nbsp;" & dr.Item("tasknum").ToString & "." & "" & subtask & "&nbsp;" & Mid(task, 1, bklen) & "<br>")
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    End If
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen, bklen2) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2, tlem) & "<br>")
                Case 4
                    If subtask <> 0 Then
                        sb.Append("&nbsp;&nbsp;" & dr.Item("tasknum").ToString & "." & "" & subtask & "&nbsp;" & Mid(task, 1, bklen) & "<br>")
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    End If
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen, bklen2) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2, bklen3) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2 + bklen3, tlem) & "<br>")
                Case 5
                    If subtask <> 0 Then
                        sb.Append("&nbsp;&nbsp;" & dr.Item("tasknum").ToString & "." & "" & subtask & "&nbsp;" & Mid(task, 1, bklen) & "<br>")
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    End If
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen, bklen2) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2, bklen3) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2 + bklen3, bklen4) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem) & "<br>")
                Case Else
                    If subtask <> 0 Then
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & "[" & subtask & "]" & Mid(task, 1, bklen) & "<br>")
                    Else
                        sb.Append("[" & dr.Item("tasknum").ToString & "]" & Mid(task, 1, bklen) & "<br>")
                    End If
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen, bklen2) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2, bklen3) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2 + bklen3, bklen4) & "<br>")
                    sb.Append("&nbsp;&nbsp;&nbsp;" & Mid(task, bklen + bklen2 + bklen3 + bklen4, tlem) & "<br>")
            End Select

            meas = dr.Item("meas").ToString
            If meas <> "none" Then
                sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & meas & "<br>")
            Else
                meas = ""
            End If
            subcnt = dr.Item("subcnt").ToString
            If subtask = 0 Then
                sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OK(___)&nbsp;&nbsp;&nbsp;&nbsp;" & dr.Item("fm1").ToString & "<br><br>")
            Else
                If subcnt <> 0 Then
                    subhold += 1
                Else
                    subhold = 0
                End If
                If subcnt = subhold Then
                    subhold = 0
                    sb.Append("<br>")
                Else
                    'sb.Append("<br>")
                End If
            End If
            Dim invi As Integer
            tasknum = dr.Item("tasknum").ToString
            If dr.Item("parts").ToString <> "none" Then 'Or Len(dr.Item("parts").ToString) = 0 Then
                Dim parts_str As String = dr.Item("parts").ToString
                Dim parr() As String = parts_str.Split(";")
                Dim parts_plus1 As String
                For invi = 0 To parr.Length - 1
                    If parts_plus = "" Then
                        parts_plus = dr.Item("func").ToString & " - " & parr(invi)
                    Else
                        parts_plus1 = dr.Item("func").ToString & " - " & parr(invi)
                        Dim ppi As Integer = parts_plus.IndexOf(parts_plus1)
                        If ppi = -1 Then
                            parts_plus += ";" & dr.Item("func").ToString & " - " & parr(invi)
                        End If

                    End If
                Next
                If Len(parts) = 0 Then
                    parts = parts_plus 'dr.Item("parts").ToString
                Else
                    Dim parts_arr() As String = parts.Split(";")
                    Dim parts_plus_arr() As String = parts_plus.Split(";")
                    Dim i As Integer
                    For i = 0 To parts_plus_arr.Length - 1
                        Dim lpi As Integer = parts.IndexOf(parts_plus_arr(i))
                        If lpi = -1 Then
                            parts += ";" & parts_plus_arr(i) 'dr.Item("lubes").ToString
                        End If
                    Next
                    'parts += ";" & parts_plus 'dr.Item("parts").ToString
                End If
            End If

            If dr.Item("tools").ToString <> "none" Then 'Or Len(dr.Item("tools").ToString) <> 0 Then
                Dim tools_str As String = dr.Item("tools").ToString
                Dim tarr() As String = tools_str.Split(";")
                Dim tools_plus1 As String
                For invi = 0 To tarr.Length - 1
                    If tools_plus = "" Then
                        tools_plus = dr.Item("func").ToString & " - " & tarr(invi)
                    Else
                        tools_plus1 = dr.Item("func").ToString & " - " & tarr(invi)
                        Dim tpi As Integer = tools_plus.IndexOf(tools_plus1)
                        If tpi = -1 Then
                            tools_plus += ";" & dr.Item("func").ToString & " - " & tarr(invi)
                        End If

                    End If
                Next
                If Len(tools) = 0 Then
                    tools = tools_plus 'dr.Item("tools").ToString
                Else
                    Dim tools_arr() As String = tools.Split(";")
                    Dim tools_plus_arr() As String = tools_plus.Split(";")
                    Dim i As Integer
                    For i = 0 To tools_plus_arr.Length - 1
                        Dim lpi As Integer = tools.IndexOf(tools_plus_arr(i))
                        If lpi = -1 Then
                            tools += ";" & tools_plus_arr(i) 'dr.Item("lubes").ToString
                        End If
                    Next
                    'tools += ";" & tools_plus 'dr.Item("tools").ToString
                End If

            End If

            If dr.Item("lubes").ToString <> "none" Then 'Or Len(dr.Item("lubes").ToString) <> 0 Then
                Dim lubes_str As String = dr.Item("lubes").ToString
                Dim larr() As String = lubes_str.Split(";")
                Dim lubes_plus1 As String
                For invi = 0 To larr.Length - 1
                    If lubes_plus = "" Then
                        lubes_plus = dr.Item("func").ToString & " - " & larr(invi)
                    Else
                        lubes_plus1 = dr.Item("func").ToString & " - " & larr(invi)
                        Dim lpi As Integer = lubes_plus.IndexOf(lubes_plus1)
                        If lpi = -1 Then
                            lubes_plus += ";" & dr.Item("func").ToString & " - " & larr(invi)
                        End If

                    End If
                Next
                If Len(lubes) = 0 Then
                    lubes = lubes_plus 'dr.Item("lubes").ToString
                Else
                    Dim lubes_arr() As String = lubes.Split(";")
                    Dim lubes_plus_arr() As String = lubes_plus.Split(";")
                    Dim i As Integer
                    For i = 0 To lubes_plus_arr.Length - 1
                        Dim lpi As Integer = lubes.IndexOf(lubes_plus_arr(i))
                        If lpi = -1 Then
                            lubes += ";" & lubes_plus_arr(i) 'dr.Item("lubes").ToString
                        End If
                    Next
                End If
            End If

        End While
        dr.Close()
        sb.Append("<br>")
        sb.Append("Parts" & "<br>")

        Dim partarr1 As String() = Split(parts, ";")
        Dim p1 As Integer
        For p1 = 0 To partarr1.Length - 1
            If Len(partarr1(p1)) = 0 Or partarr1(p1) = "none" Then
                sb.Append("<br>")
            Else
                sb.Append(partarr1(p1) & "<br>")
            End If
        Next

        sb.Append("<br>")
        sb.Append("Tools" & "<br>")

        Dim toolarr1 As String() = Split(tools, ";")
        Dim t1 As Integer
        For t1 = 0 To toolarr1.Length - 1
            If Len(toolarr1(t1)) = 0 Or toolarr1(t1) = "none" Then
                sb.Append("<br>")
            Else
                sb.Append(toolarr1(t1) & "<br>")
            End If
        Next

        sb.Append("<br>")
        sb.Append("Lubes" & "<br>")

        Dim lubearr1 As String() = Split(lubes, ";")
        Dim l1 As Integer
        For l1 = 0 To lubearr1.Length - 1
            If Len(lubearr1(l1)) = 0 Or lubearr1(l1) = "none" Then
                sb.Append("<br>")
            Else
                sb.Append(lubearr1(l1) & "<br>")
            End If

        Next
        sb.Append("                                                                                                          ")
        'sb.Append("**********************************************************************************************************<br>")
        'sb.Append("**********************************************************************************************************")
        'sb.Append("***************************************************************************************<br>")
        'sb.Append("***************************************************************************************")
        sb.Append("                                                                                                          ")

        pms.Dispose()
        Dim longstring As String = sb.ToString
        Return longstring
    End Function
End Class
