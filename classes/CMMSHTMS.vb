﻿Imports System.Data.SqlClient
Imports System.Text
Public Class CMMSHTMS
    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid, mode As String
    Public Function WritePMHTM(ByVal eqid As String, Optional ByVal pdm As String = "") As String
        Dim sb As New System.Text.StringBuilder
        If pdm <> "" Then
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        Else
            sql = "usp_getWITotalSpring '" & eqid & "'"
        End If

        sb.Append("<table width=""780"" style=""table-layout:fixed"">")

        Dim skillid, ptid, rdid, freq As String
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subhold As Integer = 0
        Dim task, lube, meas, revtask, revtask2, revtask3, revtask4 As String
        Dim tlem, splen, bklen, splen2, bklen2, splen3, bklen3, splen4, bklen4 As Integer
        Dim parts, tools, lubes, tasknum, loto As String
        Dim pretech, pretechchk As String
        Dim eqnum, eqdesc, est, down As String
        Dim skchk, skchk2 As String
        parts = ""
        tools = ""
        lubes = ""
        tasknum = ""
        Dim partin, toolin, lubein As String
        Dim lines As Integer = 1
        pms.Open()

        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = pms.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim row As DataRow
        Dim pmtskid, pmtskidhold As String
        pmtskidhold = 0
        Dim tparts, ttools, tlubes, invstring As String
        For Each row In dt.Rows
            'flag = 0
            pmtskid = row("pmtskid").ToString
            skillid = row("skillid").ToString
            ptid = row("ptid").ToString
            rdid = row("rdid").ToString
            freq = row("freq").ToString
            skill = row("skill").ToString & " / " & row("freq").ToString & " / " & row("rd").ToString
            est = row("est").ToString
            down = row("down").ToString
            eqnum = row("eqnum").ToString
            eqdesc = row("eqdesc").ToString
            loto = row("loto1").ToString
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            Dim freqi As String
            If skchk <> skchk2 Then
                skillchk = skchk
                partin = ""
                toolin = ""
                lubein = ""
                parts = ""
                tools = ""
                lubes = ""
                tparts = ""
                ttools = ""
                tlubes = ""
                If skchk2 <> "" Then
                    sb.Append("<tr><td align=""center"">")
                    sb.Append("<hr><br>")
                    sb.Append("</td></tr>")

                    sb.Append("<tr><td align=""left"">")
                    sb.Append("Work Performed by: ________________________ Date: __________<br><br>")
                    sb.Append("</td></tr>")

                    sb.Append("<tr><td align=""left"">")
                    sb.Append("<table border=""0"">")
                    sb.Append("<tr><td align=""center"" width=""100""><u>Time (min)</u></td>")
                    sb.Append("<td align=""center"" width=""100""><u>Reg</u></td>")
                    sb.Append("<td align=""center"" width=""100""><u>OT</u></td></tr>")

                    sb.Append("<tr><td align=""center"" width=""100"" height=""26"">Actual</td>")
                    sb.Append("<td align=""center"" width=""100"">_______</td>")
                    sb.Append("<td align=""center"" width=""100"">_______</td></tr>")

                    sb.Append("<tr><td align=""center"" width=""100"" height=""26"">Other</td>")
                    sb.Append("<td align=""center"" width=""100"">_______</td>")
                    sb.Append("<td align=""center"" width=""100"">_______</td></tr>")

                    sb.Append("</table>")
                    sb.Append("</td></tr>")


                    sb.Append("<tr><td align=""center"">")
                    sb.Append("<hr>")
                    sb.Append("</td></tr>")
                    'sb.Append("</table>")
                End If
                sb.Append("<tr><td align=""center"" width=""780"">")
                sb.Append("<b>" & eqnum & " - " & eqdesc & "</b><br /><br />")
                sb.Append("</td></tr>")
                sb.Append("<tr><td align=""center"">")
                sb.Append(skill & "<br />")
                sb.Append("</td></tr>")
                sb.Append("<tr><td align=""center"">")
                sb.Append("Total PM Time " & est & " Minutes<br />")
                sb.Append("</td></tr>")
                sb.Append("<tr><td align=""center"">")
                sb.Append("<hr>")
                sb.Append("</td></tr>")
                sb.Append("<tr><td align=""left"">")
                sb.Append("<b>TOOLS<b /><br />")
                sb.Append("</td></tr>")
                freqi = freq.Replace(" DAYS", "")
                sql = "exec usp_getWITotalSpring_inv '" & eqid & "','" & skillid & "','" & rdid & "','" & freqi & "'"
                dr = pms.GetRdrData(sql)
                While dr.Read
                    partin = dr.Item("part").ToString
                    toolin = dr.Item("toolnum").ToString
                    lubein = dr.Item("lubenum").ToString
                    If partin <> "0" Then
                        If parts = "" Then
                            parts = "(" & dr.Item("partqty").ToString & ")" & dr.Item("partdesc").ToString & " - " & dr.Item("part").ToString & " - " & dr.Item("partloc").ToString
                            'tparts = "(" & dr.Item("part").ToString
                        Else
                            parts += ";(" & dr.Item("partqty").ToString & ")" & dr.Item("partdesc").ToString & " - " & dr.Item("part").ToString & " - " & dr.Item("partloc").ToString
                            'tparts += ", " & dr.Item("part").ToString
                        End If
                    End If

                    If toolin <> "0" Then
                        If tools = "" Then
                            tools = "(" & dr.Item("toolqty").ToString & ")" & dr.Item("tooldesc").ToString & " - " & dr.Item("toolnum").ToString & " - " & dr.Item("toolloc").ToString
                            'ttools = "(" & dr.Item("toolnum").ToString
                        Else
                            tools += ";(" & dr.Item("toolqty").ToString & ")" & dr.Item("tooldesc").ToString & " - " & dr.Item("toolnum").ToString & " - " & dr.Item("toolloc").ToString
                            'ttools += ", " & dr.Item("toolnum").ToString
                        End If
                    End If



                    If lubein <> "0" Then
                        If lubes = "" Then
                            lubes = "(" & dr.Item("lubeqty").ToString & ")" & dr.Item("lubedesc").ToString & " - " & dr.Item("lubenum").ToString & " - " & dr.Item("lubeloc").ToString
                            'tlubes = "(" & dr.Item("lubedesc").ToString & " " & dr.Item("lubenum").ToString
                        Else
                            lubes += ";(" & dr.Item("lubeqty").ToString & ")" & dr.Item("lubedesc").ToString & " - " & dr.Item("lubenum").ToString & " - " & dr.Item("lubeloc").ToString
                            'tlubes += ", " & dr.Item("lubedesc").ToString & " " & dr.Item("lubenum").ToString
                        End If
                    End If



                End While
                dr.Close()

                If tparts <> "" Then
                    tparts = tparts & ")"
                End If
                If ttools <> "" Then
                    ttools = ttools & ")"
                End If
                If tlubes <> "" Then
                    tlubes = tlubes & ")"
                End If

                'invstring = ttools & tlubes & tparts

                sb.Append("<tr><td align=""left"" width=""780""><table width=""770"" style=""table-layout:fixed"">")

                Dim toolarr1 As String() = Split(tools, ";")
                Dim t1 As Integer
                Dim dtool, ntool As String
                For t1 = 0 To toolarr1.Length - 1
                    If Len(toolarr1(t1)) = 0 Then
                        sb.Append("<tr><td width=""50"">&nbsp;</td><td align=""left"" width=""720"">")
                        sb.Append("No Tools Found<br />")
                        sb.Append("</td></tr>")
                    Else
                        If t1 = 0 Then
                            dtool = toolarr1(t1)
                            sb.Append("<tr><td width=""50"">&nbsp;</td><td align=""left"" width=""720"">")
                            sb.Append(toolarr1(t1))
                            sb.Append("</td></tr>")
                        Else
                            ntool = toolarr1(t1)
                            If dtool <> ntool Then
                                dtool = toolarr1(t1)
                                sb.Append("<tr><td width=""50"">&nbsp;</td><td align=""left"" width=""720"">")
                                sb.Append(toolarr1(t1))
                                sb.Append("</td></tr>")
                            End If
                        End If

                    End If
                Next

                sb.Append("<tr><td align=""left"" colspan=""2"">")
                sb.Append("<br /><b>LUBES<b /><br />")
                sb.Append("</td></tr>")

                Dim lubearr1 As String() = Split(lubes, ";")
                Dim l1 As Integer
                For l1 = 0 To lubearr1.Length - 1
                    If Len(lubearr1(l1)) = 0 Then
                        sb.Append("<tr><td width=""50"">&nbsp;</td><td align=""left"" width=""720"">")
                        sb.Append("No Lubricants Found<br />")
                        sb.Append("</td></tr>")
                    Else
                        sb.Append("<tr><td width=""50"">&nbsp;</td><td align=""left"" width=""720"">")
                        sb.Append(lubearr1(l1))
                        sb.Append("</td></tr>")
                    End If

                Next

                sb.Append("<tr><td align=""left"" colspan=""2"">")
                sb.Append("<br /><b>PARTS<b /><br />")
                sb.Append("</td></tr>")

                Dim partarr1 As String() = Split(parts, ";")
                Dim p1 As Integer
                For p1 = 0 To partarr1.Length - 1
                    If Len(partarr1(p1)) = 0 Then
                        sb.Append("<tr><td width=""50"">&nbsp;</td><td align=""left"" width=""720"">")
                        sb.Append("No Parts Found<br />")
                        sb.Append("</td></tr>")
                    Else
                        sb.Append("<tr><td width=""50"">&nbsp;</td><td align=""left"" width=""720"">")
                        sb.Append(partarr1(p1))
                        sb.Append("</td></tr>")

                    End If
                Next

                sb.Append("<tr><td align=""left"" colspan=""2"">")
                sb.Append("<br /><b>LOCK OUT/TAG OUT<b />")
                sb.Append("</td></tr>")

                sb.Append("<tr><td></td><td align=""left"" width=""720"">")
                sb.Append(loto)
                sb.Append("</td></tr>")

                sb.Append("</table></td></tr>")

                sb.Append("<tr><td align=""center"">")
                sb.Append("<hr>")
                sb.Append("</td></tr>")

               

            End If

            func = row("func").ToString
            If flag = 0 Then
                flag = 1
                funcchk = func
                sb.Append("<tr><td align=""left"">")
                sb.Append("<b>" & row("func").ToString & "</b> - <i>Function Routing (" & row("routing").ToString & ")</i>")
                sb.Append("</td></tr>")
            Else
                'tmod.getxlbl("xlb10", "CMMSWRD2.vb")
                If func <> funcchk Then
                    funcchk = func
                    sb.Append("<tr><td align=""left"">")
                    sb.Append("<b>" & row("func").ToString & "</b> - <i>Function Routing (" & row("routing").ToString & ")</i>")
                    sb.Append("</td></tr>")
                End If
            End If

            Dim subtask = row("subtask").ToString
            Dim subt = row("subt").ToString
            task = row("task").ToString

            'task parts here *******************
            tparts = ""
            ttools = ""
            tlubes = ""
            sql = "exec usp_getWITotalSpring_inv_tsk '" & pmtskid & "','" & eqid & "'"
            dr = pms.GetRdrData(sql)
            While dr.Read
                partin = dr.Item("part").ToString
                toolin = dr.Item("toolnum").ToString
                lubein = dr.Item("lubenum").ToString
                If partin <> "0" Then
                    If tparts = "" Then
                        'parts = "(" & dr.Item("partqty").ToString & ")" & dr.Item("partdesc").ToString & " - " & dr.Item("part").ToString & " - " & dr.Item("partloc").ToString
                        tparts = "(" & dr.Item("part").ToString
                    Else
                        'parts += ";(" & dr.Item("partqty").ToString & ")" & dr.Item("partdesc").ToString & " - " & dr.Item("part").ToString & " - " & dr.Item("partloc").ToString
                        tparts += ", " & dr.Item("part").ToString
                    End If
                End If

                If toolin <> "0" Then
                    If ttools = "" Then
                        'tools = "(" & dr.Item("toolqty").ToString & ")" & dr.Item("tooldesc").ToString & " - " & dr.Item("toolnum").ToString & " - " & dr.Item("toolloc").ToString
                        ttools = "(" & dr.Item("toolnum").ToString
                    Else
                        'tools += ";(" & dr.Item("toolqty").ToString & ")" & dr.Item("tooldesc").ToString & " - " & dr.Item("toolnum").ToString & " - " & dr.Item("toolloc").ToString
                        ttools += ", " & dr.Item("toolnum").ToString
                    End If
                End If



                If lubein <> "0" Then
                    If tlubes = "" Then
                        'lubes = "(" & dr.Item("lubeqty").ToString & ")" & dr.Item("lubedesc").ToString & " - " & dr.Item("lubenum").ToString & " - " & dr.Item("lubeloc").ToString
                        tlubes = "(" & dr.Item("lubedesc").ToString & " " & dr.Item("lubenum").ToString
                    Else
                        'lubes += ";(" & dr.Item("lubeqty").ToString & ")" & dr.Item("lubedesc").ToString & " - " & dr.Item("lubenum").ToString & " - " & dr.Item("lubeloc").ToString
                        tlubes += ", " & dr.Item("lubedesc").ToString & " " & dr.Item("lubenum").ToString
                    End If
                End If



            End While
            dr.Close()

            If tparts <> "" Then
                tparts = tparts & ")"
            End If
            If ttools <> "" Then
                ttools = ttools & ")"
            End If
            If tlubes <> "" Then
                tlubes = tlubes & ")"
            End If

            invstring = ttools & tlubes & tparts

            If subtask <> 0 Then
                sb.Append("<tr><td align=""left"">")
                sb.Append("[" & row("tasknum").ToString & "." & subtask & "]" & subt & " " & invstring & "<br>")
                sb.Append("</td></tr>")
            Else
                sb.Append("<tr><td align=""left"">")
                sb.Append("[" & row("tasknum").ToString & "]" & task & " " & invstring & "<br>")
                sb.Append("</td></tr>")
            End If

            If subtask = 0 Then
                sb.Append("<tr><td align=""left"">")
                sb.Append("&nbsp;&nbsp;&nbsp;OK(___)&nbsp;&nbsp;&nbsp;" & row("fm1").ToString & "<br><br>")
                sb.Append("</td></tr>")

            End If
            skchk2 = skill
        Next
        sb.Append("</table>")
        pms.Dispose()
        Return sb.ToString
    End Function
End Class
