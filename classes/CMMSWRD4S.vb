﻿Imports System.Data.SqlClient
Imports System.Text
Public Class CMMSWRD4S
    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim eqid, mode As String
    Public Sub CMMSToWord(ByVal eqid As String, ByVal mode As String, ByVal inout As String, ByVal response As HttpResponse)
        HttpContext.Current.Response.ContentType = "application/msword"

        HttpContext.Current.Response.ContentEncoding = System.Text.UnicodeEncoding.UTF8

        HttpContext.Current.Response.Charset = "UTF-8"

        If inout = "yes" Then
            response.AddHeader("Content-Disposition", "attachment; filename=" & "CMMS Output to Word.doc")
        End If

        response.Write("<html>")

        response.Write("<head>")

        response.Write("<META HTTP-EQUIV=""Content-Type"" CONTENT=""text/html; charset=UTF-8"">")

        response.Write("<meta name=ProgId content=Word.Document>")

        response.Write("<meta name=Generator content=""Microsoft Word 9"">")

        response.Write("<meta name=Originator content=""Microsoft Word 9"">")

        response.Write("<style>")

        response.Write("@page Section1 {size:595.45pt 841.7pt; margin:1.0in .5in 1.2in .5in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")

        response.Write("div.Section1 {page:Section1;}")

        response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:1.25in 1.0in 1.25in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")

        response.Write("div.Section2 {page:Section2;}")

        response.Write("</style>")

        response.Write("</head>")

        response.Write("<body>")

        response.Write("<div class=Section1>")


        Dim test As String = WritePMHTM(eqid, mode)
        response.Write(test)

        response.Write("</div>")

        response.Write("</body>")

        response.Write("</html>")

        HttpContext.Current.Response.Flush()


    End Sub
    Public Function WritePMHTM(ByVal eqid As String, Optional ByVal pdm As String = "") As String
        Dim sb As New System.Text.StringBuilder
        If pdm <> "" Then
            sql = "usp_getWITotalPGEMSPdM '" & eqid & "'"
        Else
            sql = "usp_getWITotalPGEMS '" & eqid & "'"
        End If

        Dim skillid, ptid, rdid, freq As String
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim funcchk As String = ""
        Dim func As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim subcnt As Integer = 0
        Dim subhold As Integer = 0
        Dim task, lube, meas, revtask, revtask2, revtask3, revtask4 As String
        Dim tlem, splen, bklen, splen2, bklen2, splen3, bklen3, splen4, bklen4 As Integer
        Dim parts, tools, lubes, tasknum As String
        Dim pretech, pretechchk As String
        Dim eqnum, eqdesc, est, down As String
        Dim skchk, skchk2 As String
        parts = ""
        tools = ""
        lubes = ""
        tasknum = ""
        Dim lines As Integer = 1
        pms.Open()

        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = pms.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim row As DataRow

        For Each row In dt.Rows
            skillid = row("skillid").ToString
            ptid = row("ptid").ToString
            rdid = row("rdid").ToString
            freq = row("freq").ToString
            skill = row("skill").ToString & " / " & row("freq").ToString & " / " & row("rd").ToString
            est = row("est").ToString
            est = row("down").ToString
            eqnum = row("eqnum").ToString
            eqdesc = row("eqdesc").ToString
            skchk = skill.ToLower
            skchk2 = skillchk.ToLower
            If skchk <> skchk2 Then
                sb.Append("<b>" & eqnum & " - " & eqdesc & "<b /><br /><b />")

            End If
        Next
    End Function
End Class
