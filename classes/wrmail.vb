

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrmail

    Dim tmod As New transmod

    Dim sql As String
    Dim mail As New Utilities
    Dim dr As SqlDataReader
    Public Sub GetWRMail(ByVal wonum As String)
        sql = "select top 1 s.username, s.email, w.*, Convert(char(10),w.targstartdate,101) as 'tstart', " _
        + "d.dept_line, c.cell_name, l.location, " _
        + "Convert(char(10),w.targcompdate,101) as 'tcomp' " _
        + "from workorder w " _
        + "left join pmsysusers s on s.userid = w.superid " _
        + "left join dept d on d.dept_id = w.deptid " _
        + "left join cells c on c.dept_id = d.dept_id " _
        + "left join pmlocations l on l.locid = w.locid " _
        + "left join equipment e on e.eqid = w.eqid " _
        + "where w.wonum = '" & wonum & "'"
        mail.Open()
        dr = mail.GetRdrData(sql)
        Dim repby, repdate, wodesc, tts, ttc, eqnum, super, smail As String
        Dim did, clid, lid, sid As String
        While dr.Read
            super = dr.Item("username").ToString
            smail = dr.Item("email").ToString

            repby = dr.Item("reportedby").ToString
            repdate = dr.Item("reportdate").ToString
            wodesc = dr.Item("description").ToString

            did = dr.Item("dept_line").ToString
            lid = dr.Item("location").ToString
            clid = dr.Item("cell_name").ToString

            tts = dr.Item("tstart").ToString

            ttc = dr.Item("tcomp").ToString

        End While
        dr.Close()

        Dim em As New System.Web.Mail.MailMessage 'System.Web.Mail.MailMessage
        Dim suser, spass, sserv, sport, suse, sfrom As String
        sserv = System.Configuration.ConfigurationManager.AppSettings("custSMTP")
        sport = System.Configuration.ConfigurationManager.AppSettings("custSMTPPort")
        sfrom = System.Configuration.ConfigurationManager.AppSettings("custSendFrom")
        suser = System.Configuration.ConfigurationManager.AppSettings("custSendUser")
        spass = System.Configuration.ConfigurationManager.AppSettings("custSendPass")

        em.To = smail
        em.From = sfrom
        em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
        em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = sserv
        em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = suser
        em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = spass
        em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
        em.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = sport

        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If


        If smail <> "" Then
            em.Body = AlertBody(wonum, eqnum, did, clid, lid, wodesc)
            em.Subject = tmod.getxlbl("xlb175", "wrmail.vb") & " - " & wonum
            em.BodyFormat = Web.Mail.MailFormat.Html
            System.Web.Mail.SmtpMail.SmtpServer.Insert(0, sserv)
            Try
                System.Web.Mail.SmtpMail.Send(em)
            Catch ex As Exception

            End Try

        End If

        mail.Dispose()
    End Sub
    Private Function AlertBody(ByVal wonum As String, ByVal eqnum As String, ByVal dept As String, ByVal cell As String, _
       ByVal loc As String, ByVal desc As String) As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrle")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")

        Dim body As String
        body = "<table width='800px' style='font-size:10pt; font-family:Verdana;'>"
        body &= "<tr><td><b>" & tmod.getxlbl("xlb176", "wrmail.vb") & "</b></td><td>" & eqnum & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b>" & tmod.getxlbl("xlb177", "wrmail.vb") & "</b></td><td>" & dept & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b>" & tmod.getxlbl("xlb178", "wrmail.vb") & "</b></td><td>" & cell & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b>" & tmod.getxlbl("xlb179", "wrmail.vb") & "</b></td><td>" & loc & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2""><b>" & tmod.getxlbl("xlb180", "wrmail.vb") & "</b><br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2""><b>" & desc & "</b><br><br></td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td colspan=""2"">" & tmod.getlbl("cdlbl406", "wrmail.vb") & "<a href='" + urlname + appname + "/appswo/woprint.aspx?typ=wo&wo=" + wonum + "'>" & tmod.getxlbl("xlb181", "wrmail.vb") & " " + wonum + "</td></tr>" & vbCrLf & vbCrLf

        body &= "</table>"

        Return body
    End Function
End Class
