Public Class dglabvals
Public Function getAddCompval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgcomp"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAddEditFuncPopval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgfunc"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Sequence#"
Case "fre"
ret = "N� de s�quence"
Case "ger"
ret = "Folgen-Nr."
Case "ita"
ret = "Sequenza#"
Case "spa"
ret = "N�. Secuencial"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Function#"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion"
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Function#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Special Designation"
Case "fre"
ret = "Special Designation"
Case "ger"
ret = "Special Designation"
Case "ita"
ret = "Special Designation"
Case "spa"
ret = "Special Designation"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetApprHLTasksval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgphases"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Order"
Case "fre"
ret = "Commande"
Case "ger"
ret = "Reihenfolge"
Case "ita"
ret = "Ordine"
Case "spa"
ret = "Orden"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Approval Phase High Level Tasks"
Case "fre"
ret = "T�ches de Haut Niveau de la Phase d`Approbation"
Case "ger"
ret = "Zulassungsphase �bergeordnete Aufgaben "
Case "ita"
ret = "Approvazione Fase Compiti Livello Superiore"
Case "spa"
ret = "Tareas de alto nivel para la fase de aprobaci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Delete"
Case "fre"
ret = "Effacer"
Case "ger"
ret = "L�schen"
Case "ita"
ret = "Cancella"
Case "spa"
ret = "Borrar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetApprLLTasksval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgphases"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Order"
Case "fre"
ret = "Commande"
Case "ger"
ret = "Reihenfolge"
Case "ita"
ret = "Ordine"
Case "spa"
ret = "Orden"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Approval Phase Sub Level Tasks"
Case "fre"
ret = "T�ches de Sous Niveau de la Phase d`Approbation"
Case "ger"
ret = "Zulassungsphase Unteraufgaben "
Case "ita"
ret = "Approvazione Fase Compiti Sotto Livello"
Case "spa"
ret = "Tareas de sub nivel para la fase de aprobaci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Delete"
Case "fre"
ret = "Effacer"
Case "ger"
ret = "L�schen"
Case "ita"
ret = "Cancella"
Case "spa"
ret = "Borrar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetApprPhasesval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgphases"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Order"
Case "fre"
ret = "Commande"
Case "ger"
ret = "Reihenfolge"
Case "ita"
ret = "Ordine"
Case "spa"
ret = "Orden"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Approval Phase"
Case "fre"
ret = "Phase d`Approbation"
Case "ger"
ret = "Zulassungsphase "
Case "ita"
ret = "Fase di Approvazione"
Case "spa"
ret = "Fase de aprobaci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Approval Group"
Case "fre"
ret = "Groupe d`Approbation"
Case "ger"
ret = "Zulassungsgruppe "
Case "ita"
ret = "Gruppo di Approvazione"
Case "spa"
ret = "Grupo de aprobaci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Delete"
Case "fre"
ret = "Effacer"
Case "ger"
ret = "L�schen"
Case "ita"
ret = "Cancella"
Case "spa"
ret = "Borrar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetAssetClassval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgac"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Asset Class"
Case "fre"
ret = "Classe d`Actif"
Case "ger"
ret = "Anlagenklasse"
Case "ita"
ret = "Classe Asset"
Case "spa"
ret = "Clase de activo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetCellTabval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgcells"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Cell"
Case "fre"
ret = "Cellule"
Case "ger"
ret = "Zelle"
Case "ita"
ret = "Cella"
Case "spa"
ret = "C�lula"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetComTabval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgfail"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetDeptTabval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Department"
Case "fre"
ret = "D�partement"
Case "ger"
ret = "Abteilung "
Case "ita"
ret = "Reparto"
Case "spa"
ret = "Departamento"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetEqTabval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgeqstat"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Equipment Status"
Case "fre"
ret = "�tat d`�quipement"
Case "ger"
ret = "Ausr�stungsstatus"
Case "ita"
ret = "Stato Attrezzatura"
Case "spa"
ret = "Estatus del equipo "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetInvListsval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "List Value"
Case "fre"
ret = "Valeur de liste"
Case "ger"
ret = "Listen-Wert"
Case "ita"
ret = "Valore elenco"
Case "spa"
ret = "Valor de la lista"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetLocval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetLTval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Location Type"
Case "fre"
ret = "Type de localisation"
Case "ger"
ret = "Einsatzort-Art"
Case "ita"
ret = "Tipo di posizione"
Case "spa"
ret = "Tipo de la Ubicaci�n"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetpmApprGrpsval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgac"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Level"
Case "fre"
ret = "Niveau"
Case "ger"
ret = "Ebene"
Case "ita"
ret = "Livello"
Case "spa"
ret = "Nivel"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Approval Group"
Case "fre"
ret = "Groupe d`Approbation"
Case "ger"
ret = "Zulassungsgruppe "
Case "ita"
ret = "Gruppo di Approvazione"
Case "spa"
ret = "Grupo de aprobaci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Add/Remove"
Case "fre"
ret = "Ajouter/ supprimer"
Case "ger"
ret = "F�ge hinzu/entferne"
Case "ita"
ret = "Aggiungi/elimina"
Case "spa"
ret = "Agregar/Remover"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Add Users"
Case "fre"
ret = "Ajouter des Utilisateurs"
Case "ger"
ret = "Hinzuf�gen von Benutzern"
Case "ita"
ret = "Aggiungi Utenti"
Case "spa"
ret = "A�adir usuarios"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetSYSval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "System ID"
Case "fre"
ret = "Identifiant du syst�me"
Case "ger"
ret = "System-ID"
Case "ita"
ret = "ID sistema"
Case "spa"
ret = "Ident. del Sistema"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Networked?"
Case "fre"
ret = "En r�seau�?"
Case "ger"
ret = "Vernetzt?"
Case "ita"
ret = "Condivisibile?"
Case "spa"
ret = "�Conectado a Red?"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetTaskTabETval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgstat"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "PM Eq Status"
Case "fre"
ret = "�tat d`�q de MP"
Case "ger"
ret = "Phasenmodulationsausr�stungsstatus"
Case "ita"
ret = "Stato Attr PM"
Case "spa"
ret = "estatus del eq PM"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetTaskTabPTval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgpre"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "PdM Tech"
Case "fre"
ret = "Tech MPd"
Case "ger"
ret = "Pulsdauermodulations-Technik"
Case "ita"
ret = "PdM Tec"
Case "spa"
ret = "Tecnolog�a PdM"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetTaskTabSTval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgskill"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Skill"
Case "fre"
ret = "Comp�tence"
Case "ger"
ret = "Kompetenz"
Case "ita"
ret = "Skill"
Case "spa"
ret = "Habilidad"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetTaskTabTTval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetWoChargeTypeval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Charge Number Type"
Case "fre"
ret = "Type de num�ro de charge"
Case "ger"
ret = "Nummerntyp des Beschickungsguts"
Case "ita"
ret = "Caricare tipo numero"
Case "spa"
ret = "Tipo de N�ero de Cargo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Active"
Case "fre"
ret = "active"
Case "ger"
ret = "aktiv"
Case "ita"
ret = "attivo"
Case "spa"
ret = "activo"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetWoStatusval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Work Order Status"
Case "fre"
ret = "�tat de bon de commande"
Case "ger"
ret = "Auftrags-Status"
Case "ita"
ret = "Stato ordine di lavoro"
Case "spa"
ret = "Estado de Orden de trabajo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Active"
Case "fre"
ret = "active"
Case "ger"
ret = "aktiv"
Case "ita"
ret = "attivo"
Case "spa"
ret = "activo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAppSetWoTypeval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Work Order Type"
Case "fre"
ret = "Type de bon de commande"
Case "ger"
ret = "Auftrags-Art"
Case "ita"
ret = "Tipo ordine di lavoro"
Case "spa"
ret = "Tipo de Orden de trabajo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Active"
Case "fre"
ret = "active"
Case "ger"
ret = "aktiv"
Case "ita"
ret = "attivo"
Case "spa"
ret = "activo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getarchtasklubelistval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getarchtasklubelisttpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getarchtaskpartlistval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getarchtaskpartlisttpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getarchtasktoollistval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
End Select
Return ret
End Function
Public Function getarchtasktoollisttpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
End Select
Return ret
End Function
Public Function getAssessment_Adminval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Check#"
Case "fre"
ret = "N� de contr�le"
Case "ger"
ret = "Pr�f-Nr."
Case "ita"
ret = "Controllo#"
Case "spa"
ret = "N� de Verificaci�n"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Check Point"
Case "fre"
ret = "Point de contr�le"
Case "ger"
ret = "Kontrollpunkt"
Case "ita"
ret = "Punto di controllo"
Case "spa"
ret = "Punto del Verificaci�n "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Weight"
Case "fre"
ret = "Poids"
Case "ger"
ret = "Gewichtung"
Case "ita"
ret = "Peso"
Case "spa"
ret = "Peso"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getAssessment_Detailsval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Check#"
Case "fre"
ret = "N� de contr�le"
Case "ger"
ret = "Pr�f-Nr."
Case "ita"
ret = "Controllo#"
Case "spa"
ret = "N� de Verificaci�n"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Check Point"
Case "fre"
ret = "Point de contr�le"
Case "ger"
ret = "Kontrollpunkt"
Case "ita"
ret = "Punto di controllo"
Case "spa"
ret = "Punto del Verificaci�n "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Score"
Case "fre"
ret = "Score"
Case "ger"
ret = "Wertung"
Case "ita"
ret = "Punto"
Case "spa"
ret = "Conteo F�sico "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Comments"
Case "fre"
ret = "Commentaires"
Case "ger"
ret = "Bemerkungen"
Case "ita"
ret = "Commenti"
Case "spa"
ret = "Comentarios "
End Select
End Select
End Select
Return ret
End Function
Public Function getcompclassval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgac"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component Class"
Case "fre"
ret = "Cat�gorie de composants"
Case "ger"
ret = "Bauteil-Klasse"
Case "ita"
ret = "Classe componente"
Case "spa"
ret = "Clase de componente "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getCompGridval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgcomp"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Sequence#"
Case "fre"
ret = "N� de s�quence"
Case "ger"
ret = "Folgen-Nr."
Case "ita"
ret = "Sequenza#"
Case "spa"
ret = "N�. Secuencial"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component#"
Case "fre"
ret = "composan"
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Component#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
End Select
End Select
Return ret
End Function
Public Function getcomplibtasksval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Failure Modes This Task Will Address"
Case "fre"
ret = "Modes de d�faillance que cette t�che va traiter"
Case "ger"
ret = "Modusfehler diese Aufgabe bezieht sich auf"
Case "ita"
ret = "Modalit� d`errore questa attivit� verr� indirizzata"
Case "spa"
ret = "Modos de Falla que Esta Tarea Atender�"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Time"
Case "fre"
ret = "Temps"
Case "ger"
ret = "Zeit"
Case "ita"
ret = "Tempo"
Case "spa"
ret = "Tiempo"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "PM Equipment Status"
Case "fre"
ret = "Statut de l`�quipement PM"
Case "ger"
ret = "PM Ausr�stungs-Status"
Case "ita"
ret = "Stato apparecchiatura PM"
Case "spa"
ret = "PM Estado del Equipo "
End Select
Case "17"
Select Case lang
Case "eng"
ret = "PM Equipment Status"
Case "fre"
ret = "Statut de l`�quipement PM"
Case "ger"
ret = "PM Ausr�stungs-Status"
Case "ita"
ret = "Stato apparecchiatura PM"
Case "spa"
ret = "PM Estado del Equipo "
End Select
Case "20"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "21"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getcomplibtaskviewval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Step#"
Case "fre"
ret = "N� d`�tape"
Case "ger"
ret = "Schritt Nr. "
Case "ita"
ret = "Passaggio#"
Case "spa"
ret = "N� de Paso "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Task Description"
Case "fre"
ret = "Description de la T�che "
Case "ger"
ret = "aufgabenbeschreibung"
Case "ita"
ret = "Descrizione Compito"
Case "spa"
ret = "Descripci�n de la Tarea"
End Select
End Select
End Select
Return ret
End Function
Public Function getcomplibtaskview2val(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Task Description"
Case "fre"
ret = "Description de la T�che "
Case "ger"
ret = "aufgabenbeschreibung"
Case "ita"
ret = "Descrizione Compito"
Case "spa"
ret = "Descripci�n de la Tarea"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill"
Case "fre"
ret = "Comp�tence"
Case "ger"
ret = "Kompetenz"
Case "ita"
ret = "Skill"
Case "spa"
ret = "Habilidad"
End Select
End Select
End Select
Return ret
End Function
Public Function getcomplookupval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Company"
Case "fre"
ret = "Compagnie"
Case "ger"
ret = "Unternehmen"
Case "ita"
ret = "Societ�"
Case "spa"
ret = "Compa��a"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Name"
Case "fre"
ret = "Nom"
Case "ger"
ret = "Name"
Case "ita"
ret = "Nome"
Case "spa"
ret = "Nombre"
End Select
End Select
End Select
Return ret
End Function
Public Function getcompmainval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dglabor"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Contact"
Case "fre"
ret = "Contact"
Case "ger"
ret = "Kontakt"
Case "ita"
ret = "Contatti"
Case "spa"
ret = "Contacto"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Position"
Case "fre"
ret = "Position"
Case "ger"
ret = "Position"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Posici�n"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Phone"
Case "fre"
ret = "T�l�phone"
Case "ger"
ret = "Telefon"
Case "ita"
ret = "Telefono"
Case "spa"
ret = "Phone"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Fax"
Case "fre"
ret = "Fax"
Case "ger"
ret = "Fax"
Case "ita"
ret = "Fax"
Case "spa"
ret = "Fax"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Email"
Case "fre"
ret = "E-mail"
Case "ger"
ret = "E-Mail"
Case "ita"
ret = "Email"
Case "spa"
ret = "Correo electr�nico"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Delete"
Case "fre"
ret = "Effacer"
Case "ger"
ret = "L�schen"
Case "ita"
ret = "Cancella"
Case "spa"
ret = "Borrar"
End Select
End Select
End Select
Return ret
End Function
Public Function getcompseqval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgcomp"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Sequence#"
Case "fre"
ret = "N� de s�quence"
Case "ger"
ret = "Folgen-Nr."
Case "ita"
ret = "Sequenza#"
Case "spa"
ret = "N�. Secuencial"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component#"
Case "fre"
ret = "composan"
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Component#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Special Description"
Case "fre"
ret = "Special Description"
Case "ger"
ret = "Special Description"
Case "ita"
ret = "Special Description"
Case "spa"
ret = "Special Description"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Designation"
Case "fre"
ret = "D�signation"
Case "ger"
ret = "Benennung"
Case "ita"
ret = "Designazione"
Case "spa"
ret = "Designaci�n"
End Select
End Select
End Select
Return ret
End Function
Public Function getcomptypesval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgskill"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Delete"
Case "fre"
ret = "Effacer"
Case "ger"
ret = "L�schen"
Case "ita"
ret = "Cancella"
Case "spa"
ret = "Borrar"
End Select
End Select
End Select
Return ret
End Function
Public Function getcsubval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Sub Task Description"
Case "fre"
ret = "Description de sous-t�che"
Case "ger"
ret = "Beschreibung der Unteraufgabe"
Case "ita"
ret = "Descrizione sottoattivit�"
Case "spa"
ret = "Descripci�n de Sub-Tarea"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Down Time"
Case "fre"
ret = "Temps d`Arr�t"
Case "ger"
ret = "Ausfallzeit"
Case "ita"
ret = "Tempo di Fermo"
Case "spa"
ret = "Tiempo fuera de servicio"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getdevTaskLubeListval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getdevTaskLubeListtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getdevTaskPartListval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getdevTaskPartListtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getdevTaskToolListval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
End Select
Return ret
End Function
Public Function getdevTaskToolListtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
End Select
Return ret
End Function
Public Function getEqGridval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgeq"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Equipment#"
Case "fre"
ret = "Num�ro d`�quipement"
Case "ger"
ret = "Ausr�stung#"
Case "ita"
ret = "Apparecchiatura n."
Case "spa"
ret = "Equipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Model"
Case "fre"
ret = "Mod�le"
Case "ger"
ret = "Modell"
Case "ita"
ret = "Modello"
Case "spa"
ret = "Modelo"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Model"
Case "fre"
ret = "Mod�le"
Case "ger"
ret = "Modell"
Case "ita"
ret = "Modello"
Case "spa"
ret = "Modelo"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Serial#"
Case "fre"
ret = "Publication en S�rie"
Case "ger"
ret = "Serie"
Case "ita"
ret = "Numero di serie"
Case "spa"
ret = "Serial#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Serial#"
Case "fre"
ret = "Publication en S�rie"
Case "ger"
ret = "Serie"
Case "ita"
ret = "Numero di serie"
Case "spa"
ret = "Serial#"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Equipment Status"
Case "fre"
ret = "�tat d`�quipement"
Case "ger"
ret = "Ausr�stungsstatus"
Case "ita"
ret = "Stato Attrezzatura"
Case "spa"
ret = "Estatus del equipo "
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Equipment Status"
Case "fre"
ret = "�tat d`�quipement"
Case "ger"
ret = "Ausr�stungsstatus"
Case "ita"
ret = "Stato Attrezzatura"
Case "spa"
ret = "Estatus del equipo "
End Select
End Select
End Select
Return ret
End Function
Public Function geteqlookupval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Equipment#"
Case "fre"
ret = "Num�ro d`�quipement"
Case "ger"
ret = "Ausr�stung#"
Case "ita"
ret = "Apparecchiatura n."
Case "spa"
ret = "Equipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
End Select
End Select
Return ret
End Function
Public Function getFuncGridval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgfunc"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Sequence#"
Case "fre"
ret = "N� de s�quence"
Case "ger"
ret = "Folgen-Nr."
Case "ita"
ret = "Sequenza#"
Case "spa"
ret = "N�. Secuencial"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Function#"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion"
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Function#"
End Select
End Select
End Select
Return ret
End Function
Public Function getfuncseqval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgfunc"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Sequence#"
Case "fre"
ret = "N� de s�quence"
Case "ger"
ret = "Folgen-Nr."
Case "ita"
ret = "Sequenza#"
Case "spa"
ret = "N�. Secuencial"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Function#"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion"
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Function#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Special Designation"
Case "fre"
ret = "Special Designation"
Case "ger"
ret = "Special Designation"
Case "ita"
ret = "Special Designation"
Case "spa"
ret = "Special Designation"
End Select
End Select
End Select
Return ret
End Function
Public Function getGSubval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Sub Task Description"
Case "fre"
ret = "Description de sous-t�che"
Case "ger"
ret = "Beschreibung der Unteraufgabe"
Case "ita"
ret = "Descrizione sottoattivit�"
Case "spa"
ret = "Descripci�n de Sub-Tarea"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Down Time"
Case "fre"
ret = "Temps d`Arr�t"
Case "ger"
ret = "Ausfallzeit"
Case "ita"
ret = "Tempo di Fermo"
Case "spa"
ret = "Tiempo fuera de servicio"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getgsubarchval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task Description"
Case "fre"
ret = "Description de sous-t�che"
Case "ger"
ret = "Beschreibung der Unteraufgabe"
Case "ita"
ret = "Descrizione sottoattivit�"
Case "spa"
ret = "Descripci�n de Sub-Tarea"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Down Time"
Case "fre"
ret = "Temps d`Arr�t"
Case "ger"
ret = "Ausfallzeit"
Case "ita"
ret = "Tempo di Fermo"
Case "spa"
ret = "Tiempo fuera de servicio"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getgsubarchtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task Description"
Case "fre"
ret = "Description de sous-t�che"
Case "ger"
ret = "Beschreibung der Unteraufgabe"
Case "ita"
ret = "Descrizione sottoattivit�"
Case "spa"
ret = "Descripci�n de Sub-Tarea"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Down Time"
Case "fre"
ret = "Temps d`Arr�t"
Case "ger"
ret = "Ausfallzeit"
Case "ita"
ret = "Tempo di Fermo"
Case "spa"
ret = "Tiempo fuera de servicio"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getGSubtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Sub Task Description"
Case "fre"
ret = "Description de sous-t�che"
Case "ger"
ret = "Beschreibung der Unteraufgabe"
Case "ita"
ret = "Descrizione sottoattivit�"
Case "spa"
ret = "Descripci�n de Sub-Tarea"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Down Time"
Case "fre"
ret = "Temps d`Arr�t"
Case "ger"
ret = "Ausfallzeit"
Case "ita"
ret = "Tempo di Fermo"
Case "spa"
ret = "Tiempo fuera de servicio"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getGTasksFunc2val(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Failure Modes This Task Will Address"
Case "fre"
ret = "Modes de d�faillance que cette t�che va traiter"
Case "ger"
ret = "Modusfehler diese Aufgabe bezieht sich auf"
Case "ita"
ret = "Modalit� d`errore questa attivit� verr� indirizzata"
Case "spa"
ret = "Modos de Falla que Esta Tarea Atender�"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Time"
Case "fre"
ret = "Temps"
Case "ger"
ret = "Zeit"
Case "ita"
ret = "Tempo"
Case "spa"
ret = "Tiempo"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "PM Equipment Status"
Case "fre"
ret = "Statut de l`�quipement PM"
Case "ger"
ret = "PM Ausr�stungs-Status"
Case "ita"
ret = "Stato apparecchiatura PM"
Case "spa"
ret = "PM Estado del Equipo "
End Select
Case "19"
Select Case lang
Case "eng"
ret = "PM Equipment Status"
Case "fre"
ret = "Statut de l`�quipement PM"
Case "ger"
ret = "PM Ausr�stungs-Status"
Case "ita"
ret = "Stato apparecchiatura PM"
Case "spa"
ret = "PM Estado del Equipo "
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "24"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getGTasksFunctpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Failure Modes This Task Will Address"
Case "fre"
ret = "Modes de d�faillance que cette t�che va traiter"
Case "ger"
ret = "Modusfehler diese Aufgabe bezieht sich auf"
Case "ita"
ret = "Modalit� d`errore questa attivit� verr� indirizzata"
Case "spa"
ret = "Modos de Falla que Esta Tarea Atender�"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Time"
Case "fre"
ret = "Temps"
Case "ger"
ret = "Zeit"
Case "ita"
ret = "Tempo"
Case "spa"
ret = "Tiempo"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "TPM Equipment Status"
Case "fre"
ret = "Statut d`�quipement TPM"
Case "ger"
ret = "TPM Ausr�stungsstatus"
Case "ita"
ret = "Stato TPM apparecchiatura"
Case "spa"
ret = "TPM Equipment Status"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "TPM Equipment Status"
Case "fre"
ret = "Statut d`�quipement TPM"
Case "ger"
ret = "TPM Ausr�stungsstatus"
Case "ita"
ret = "Stato TPM apparecchiatura"
Case "spa"
ret = "TPM Equipment Status"
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "24"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getGTasksFunctpm2val(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Failure Modes This Task Will Address"
Case "fre"
ret = "Modes de d�faillance que cette t�che va traiter"
Case "ger"
ret = "Modusfehler diese Aufgabe bezieht sich auf"
Case "ita"
ret = "Modalit� d`errore questa attivit� verr� indirizzata"
Case "spa"
ret = "Modos de Falla que Esta Tarea Atender�"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Time"
Case "fre"
ret = "Temps"
Case "ger"
ret = "Zeit"
Case "ita"
ret = "Tempo"
Case "spa"
ret = "Tiempo"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "PM Equipment Status"
Case "fre"
ret = "Statut de l`�quipement PM"
Case "ger"
ret = "PM Ausr�stungs-Status"
Case "ita"
ret = "Stato apparecchiatura PM"
Case "spa"
ret = "PM Estado del Equipo "
End Select
Case "19"
Select Case lang
Case "eng"
ret = "PM Equipment Status"
Case "fre"
ret = "Statut de l`�quipement PM"
Case "ger"
ret = "PM Ausr�stungs-Status"
Case "ita"
ret = "Stato apparecchiatura PM"
Case "spa"
ret = "PM Estado del Equipo "
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "24"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getInventoryval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getInventoryLubesval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dglube"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Lubricant#"
Case "fre"
ret = "Lubrifiant# "
Case "ger"
ret = "Schmiermittelnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getInventoryToolsval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtools"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getissueinvval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
End Select
End Select
Return ret
End Function
Public Function getJobPlanval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Task Description"
Case "fre"
ret = "Description de la T�che "
Case "ger"
ret = "aufgabenbeschreibung"
Case "ita"
ret = "Descrizione Compito"
Case "spa"
ret = "Descripci�n de la Tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Failure Mode"
Case "fre"
ret = "Mode de d�faillance"
Case "ger"
ret = "Ausfallart"
Case "ita"
ret = "Modalit� errore"
Case "spa"
ret = "Modo de Falla"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes/Meas"
Case "fre"
ret = "Pi�ces/outils/lubrifiants/mesures"
Case "ger"
ret = "Teile / Werkzeuge / Schmiermittel / Messungen"
Case "ita"
ret = "parti/strumenti/ lubrificanti/mis."
Case "spa"
ret = "Parts/Tools/Lubes/Meas"
End Select
End Select
End Select
Return ret
End Function
Public Function getJobPlanTaskListval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task Description"
Case "fre"
ret = "Description de la T�che "
Case "ger"
ret = "aufgabenbeschreibung"
Case "ita"
ret = "Descrizione Compito"
Case "spa"
ret = "Descripci�n de la Tarea"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Failure Mode"
Case "fre"
ret = "Mode de d�faillance"
Case "ger"
ret = "Ausfallart"
Case "ita"
ret = "Modalit� errore"
Case "spa"
ret = "Modo de Falla"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
End Select
End Select
Return ret
End Function
Public Function getJSubval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Sub Task Description"
Case "fre"
ret = "Description de sous-t�che"
Case "ger"
ret = "Beschreibung der Unteraufgabe"
Case "ita"
ret = "Descrizione sottoattivit�"
Case "spa"
ret = "Descripci�n de Sub-Tarea"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getlabshedmainval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgsched"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Week Of"
Case "fre"
ret = "Semaine de"
Case "ger"
ret = "Woche"
Case "ita"
ret = "Settimana di"
Case "spa"
ret = "Week Of"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Hours Monday"
Case "fre"
ret = "Heures lundi"
Case "ger"
ret = "�ffnungszeiten Montag"
Case "ita"
ret = "Orari Luned�"
Case "spa"
ret = "Hours Monday"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Assigned Skill"
Case "fre"
ret = "Comp�tence affect�e"
Case "ger"
ret = "Zugeordnete F�higkeit"
Case "ita"
ret = "Competenza assegnata"
Case "spa"
ret = "Assigned Skill"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Hours Tuesday"
Case "fre"
ret = "Heures mardi"
Case "ger"
ret = "�ffnungszeiten Dienstag"
Case "ita"
ret = "Orari Marted�"
Case "spa"
ret = "Hours Tuesday"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Assigned Skill"
Case "fre"
ret = "Comp�tence affect�e"
Case "ger"
ret = "Zugeordnete F�higkeit"
Case "ita"
ret = "Competenza assegnata"
Case "spa"
ret = "Assigned Skill"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Hours Wednesday"
Case "fre"
ret = "Heures mercredi"
Case "ger"
ret = "�ffnungszeiten Mittwoch"
Case "ita"
ret = "orari Mercoled�"
Case "spa"
ret = "Hours Wednesday"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Assigned Skill"
Case "fre"
ret = "Comp�tence affect�e"
Case "ger"
ret = "Zugeordnete F�higkeit"
Case "ita"
ret = "Competenza assegnata"
Case "spa"
ret = "Assigned Skill"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Hours Thursday"
Case "fre"
ret = "Heures jeudi"
Case "ger"
ret = "�ffnungszeiten Donnerstag"
Case "ita"
ret = "Orari Gioved�"
Case "spa"
ret = "Hours Thursday"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Assigned Skill"
Case "fre"
ret = "Comp�tence affect�e"
Case "ger"
ret = "Zugeordnete F�higkeit"
Case "ita"
ret = "Competenza assegnata"
Case "spa"
ret = "Assigned Skill"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Hours Friday"
Case "fre"
ret = "Hours Friday"
Case "ger"
ret = "Hours Friday"
Case "ita"
ret = "Hours Friday"
Case "spa"
ret = "Hours Friday"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Assigned Skill"
Case "fre"
ret = "Comp�tence affect�e"
Case "ger"
ret = "Zugeordnete F�higkeit"
Case "ita"
ret = "Competenza assegnata"
Case "spa"
ret = "Assigned Skill"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Hours Saturday"
Case "fre"
ret = "Heures samedi"
Case "ger"
ret = "�ffnungszeiten Samstag"
Case "ita"
ret = "Orari Sabato"
Case "spa"
ret = "Hours Saturday"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Assigned Skill"
Case "fre"
ret = "Comp�tence affect�e"
Case "ger"
ret = "Zugeordnete F�higkeit"
Case "ita"
ret = "Competenza assegnata"
Case "spa"
ret = "Assigned Skill"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Hours Sunday"
Case "fre"
ret = "Heures dimanche"
Case "ger"
ret = "�ffnungszeiten Sonntag"
Case "ita"
ret = "Orari Domenica"
Case "spa"
ret = "Hours Sunday"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Assigned Skill"
Case "fre"
ret = "Comp�tence affect�e"
Case "ger"
ret = "Zugeordnete F�higkeit"
Case "ita"
ret = "Competenza assegnata"
Case "spa"
ret = "Assigned Skill"
End Select
End Select
End Select
Return ret
End Function
Public Function getlubeouteditval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Title"
Case "fre"
ret = "Titre"
Case "ger"
ret = "Titel"
Case "ita"
ret = "Titolo"
Case "spa"
ret = "T�tulo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Lubricant"
Case "fre"
ret = "Lubrifiant# "
Case "ger"
ret = "Erforderliche Schmiermittel"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricant"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Lube Task"
Case "fre"
ret = "T�che de lubrification"
Case "ger"
ret = "Schmiermittel Aufgaben"
Case "ita"
ret = "Attivit� lubrificante"
Case "spa"
ret = "Lube Task"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "History"
Case "fre"
ret = "Historique"
Case "ger"
ret = "Vorgeschichte"
Case "ita"
ret = "Cronologia"
Case "spa"
ret = "Historia "
End Select
End Select
End Select
Return ret
End Function
Public Function getlubeoutroutesval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Route Name"
Case "fre"
ret = "Nom de chemin"
Case "ger"
ret = "Strecken-Name"
Case "ita"
ret = "Nome instradamento"
Case "spa"
ret = "Ruta "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Last Date"
Case "fre"
ret = "Derni�re date"
Case "ger"
ret = "Letztes Datum"
Case "ita"
ret = "Ultima data"
Case "spa"
ret = "�ltima Fecha "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Next Date"
Case "fre"
ret = "Prochaine date"
Case "ger"
ret = "N�chstes Datum"
Case "ita"
ret = "Data successiva"
Case "spa"
ret = "Siguiente Fecha "
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Tasks"
Case "fre"
ret = "T�ches"
Case "ger"
ret = "Aufgaben"
Case "ita"
ret = "Attivit�"
Case "spa"
ret = "Tareas"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getmatlookupval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Item"
Case "fre"
ret = "Article"
Case "ger"
ret = "Artikel"
Case "ita"
ret = "Articolo"
Case "spa"
ret = "Art�culo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Store Room"
Case "fre"
ret = "Local de service "
Case "ger"
ret = "Lagerraum"
Case "ita"
ret = "Deposito"
Case "spa"
ret = "Almac�n "
End Select
End Select
End Select
Return ret
End Function
Public Function getmeasouteditval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Title"
Case "fre"
ret = "Titre"
Case "ger"
ret = "Titel"
Case "ita"
ret = "Titolo"
Case "spa"
ret = "T�tulo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Measure Type"
Case "fre"
ret = "Type de Mesure"
Case "ger"
ret = "Messwerttyp"
Case "ita"
ret = "Tipo di Misura"
Case "spa"
ret = "Tipo de medici�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Characteristic"
Case "fre"
ret = "Characteristic"
Case "ger"
ret = "Characteristic"
Case "ita"
ret = "Caratteristica:"
Case "spa"
ret = "Characteristic"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "10"
Select Case lang
Case "eng"
ret = "History"
Case "fre"
ret = "Historique"
Case "ger"
ret = "Vorgeschichte"
Case "ita"
ret = "Cronologia"
Case "spa"
ret = "Historia "
End Select
End Select
End Select
Return ret
End Function
Public Function getmeasoutflyval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Task Description"
Case "fre"
ret = "Description de la T�che "
Case "ger"
ret = "aufgabenbeschreibung"
Case "ita"
ret = "Descrizione Compito"
Case "spa"
ret = "Descripci�n de la Tarea"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "History"
Case "fre"
ret = "Historique"
Case "ger"
ret = "Vorgeschichte"
Case "ita"
ret = "Cronologia"
Case "spa"
ret = "Historia "
End Select
End Select
End Select
Return ret
End Function
Public Function getmeasouthisteditval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Measure Date"
Case "fre"
ret = "Date de mesure"
Case "ger"
ret = "Mess-Datum"
Case "ita"
ret = "Data misura"
Case "spa"
ret = "Fecha de la Medici�n "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getmeasoutroutesval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Route Name"
Case "fre"
ret = "Nom de chemin"
Case "ger"
ret = "Strecken-Name"
Case "ita"
ret = "Nome instradamento"
Case "spa"
ret = "Ruta "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Last Date"
Case "fre"
ret = "Derni�re date"
Case "ger"
ret = "Letztes Datum"
Case "ita"
ret = "Ultima data"
Case "spa"
ret = "�ltima Fecha "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Next Date"
Case "fre"
ret = "Prochaine date"
Case "ger"
ret = "N�chstes Datum"
Case "ita"
ret = "Data successiva"
Case "spa"
ret = "Siguiente Fecha "
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Tasks"
Case "fre"
ret = "T�ches"
Case "ger"
ret = "Aufgaben"
Case "ita"
ret = "Attivit�"
Case "spa"
ret = "Tareas"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getmeasoutroutesminival(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Route Name"
Case "fre"
ret = "Nom de chemin"
Case "ger"
ret = "Strecken-Name"
Case "ita"
ret = "Nome instradamento"
Case "spa"
ret = "Ruta "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
End Select
End Select
Return ret
End Function
Public Function getmeasroutesaddbotval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Title"
Case "fre"
ret = "Titre"
Case "ger"
ret = "Titel"
Case "ita"
ret = "Titolo"
Case "spa"
ret = "T�tulo"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Measure Type"
Case "fre"
ret = "Type de Mesure"
Case "ger"
ret = "Messwerttyp"
Case "ita"
ret = "Tipo di Misura"
Case "spa"
ret = "Tipo de medici�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Characteristic"
Case "fre"
ret = "Characteristic"
Case "ger"
ret = "Characteristic"
Case "ita"
ret = "Caratteristica:"
Case "spa"
ret = "Characteristic"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Title"
Case "fre"
ret = "Titre"
Case "ger"
ret = "Titel"
Case "ita"
ret = "Titolo"
Case "spa"
ret = "T�tulo"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Equipment"
Case "fre"
ret = "Num�ro d`�quipement"
Case "ger"
ret = "Ausr�stung#"
Case "ita"
ret = "Apparecchiatura n."
Case "spa"
ret = "Equipo"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Measure Type"
Case "fre"
ret = "Type de Mesure"
Case "ger"
ret = "Messwerttyp"
Case "ita"
ret = "Tipo di Misura"
Case "spa"
ret = "Tipo de medici�n"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Characteristic"
Case "fre"
ret = "Characteristic"
Case "ger"
ret = "Characteristic"
Case "ita"
ret = "Caratteristica:"
Case "spa"
ret = "Characteristic"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "20"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "21"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "24"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
Case "dgmeasrt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Title"
Case "fre"
ret = "Titre"
Case "ger"
ret = "Titel"
Case "ita"
ret = "Titolo"
Case "spa"
ret = "T�tulo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Equipment"
Case "fre"
ret = "Num�ro d`�quipement"
Case "ger"
ret = "Ausr�stung#"
Case "ita"
ret = "Apparecchiatura n."
Case "spa"
ret = "Equipo"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Measure Type"
Case "fre"
ret = "Type de Mesure"
Case "ger"
ret = "Messwerttyp"
Case "ita"
ret = "Tipo di Misura"
Case "spa"
ret = "Tipo de medici�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Characteristic"
Case "fre"
ret = "Characteristic"
Case "ger"
ret = "Characteristic"
Case "ita"
ret = "Caratteristica:"
Case "spa"
ret = "Characteristic"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getoptTaskLubeListval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "21"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getoptTaskLubeListtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "21"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Lube#"
Case "fre"
ret = "Lubrification#  "
Case "ger"
ret = "Schmier�lnummer#"
Case "ita"
ret = "N.Lubrificante"
Case "spa"
ret = "Lubricante#"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getoptTaskPartListval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "21"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getoptTaskPartListtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "21"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getoptTaskToolListval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "21"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
End Select
Return ret
End Function
Public Function getoptTaskToolListtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "2"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "21"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Tool#"
Case "fre"
ret = "Num�ro d`Outil"
Case "ger"
ret = "Tool#"
Case "ita"
ret = "strumento n."
Case "spa"
ret = "herramienta#"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Rate"
Case "fre"
ret = "Taux"
Case "ger"
ret = "Menge"
Case "ita"
ret = "Passo"
Case "spa"
ret = "Velocidad"
End Select
End Select
End Select
Return ret
End Function
Public Function getpartsmainval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getpmactval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dghours"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Actual Time (Min)"
Case "fre"
ret = "Temps r�el (min)"
Case "ger"
ret = "Aktuelle Zeit (min)"
Case "ita"
ret = "Orario effettivo (min)"
Case "spa"
ret = "Actual Time (Min)"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Actual R\D (Min)"
Case "fre"
ret = "R\D r�el (min)"
Case "ger"
ret = "Tats�chliche R \ D (min.)"
Case "ita"
ret = "R\D effettivo (min)"
Case "spa"
ret = "Actual R\D (Min)"
End Select
End Select
End Select
Return ret
End Function
Public Function getpmactmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Cost\Rate"
Case "fre"
ret = "Co�t\taux"
Case "ger"
ret = "Kosten \ Quote"
Case "ita"
ret = "Costo\tariffa"
Case "spa"
ret = "Costo/Tarifa"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Used"
Case "fre"
ret = "Utilis�"
Case "ger"
ret = "In Gebrauch"
Case "ita"
ret = "Utilizzato"
Case "spa"
ret = "Usado "
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Qty Used"
Case "fre"
ret = "Qt� utilis�e"
Case "ger"
ret = "Anzahl verbraucht"
Case "ita"
ret = "Quantit� usata"
Case "spa"
ret = "Cantidad Utilizada"
End Select
End Select
End Select
Return ret
End Function
Public Function getpmactmtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Cost\Rate"
Case "fre"
ret = "Co�t\taux"
Case "ger"
ret = "Kosten \ Quote"
Case "ita"
ret = "Costo\tariffa"
Case "spa"
ret = "Costo/Tarifa"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Used"
Case "fre"
ret = "Utilis�"
Case "ger"
ret = "In Gebrauch"
Case "ita"
ret = "Utilizzato"
Case "spa"
ret = "Usado "
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Qty Used"
Case "fre"
ret = "Qt� utilis�e"
Case "ger"
ret = "Anzahl verbraucht"
Case "ita"
ret = "Quantit� usata"
Case "spa"
ret = "Cantidad Utilizada"
End Select
End Select
End Select
Return ret
End Function
Public Function getpmacttpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dghours"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Actual Time (Min)"
Case "fre"
ret = "Temps r�el (min)"
Case "ger"
ret = "Aktuelle Zeit (min)"
Case "ita"
ret = "Orario effettivo (min)"
Case "spa"
ret = "Actual Time (Min)"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Actual R\D (Min)"
Case "fre"
ret = "R\D r�el (min)"
Case "ger"
ret = "Tats�chliche R \ D (min.)"
Case "ita"
ret = "R\D effettivo (min)"
Case "spa"
ret = "Actual R\D (Min)"
End Select
End Select
End Select
Return ret
End Function
Public Function getPMLeadManval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Name"
Case "fre"
ret = "Nom"
Case "ger"
ret = "Name"
Case "ita"
ret = "Nome"
Case "spa"
ret = "Nombre"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Phone"
Case "fre"
ret = "T�l�phone"
Case "ger"
ret = "Telefon"
Case "ita"
ret = "Telefono"
Case "spa"
ret = "Phone"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Email Address"
Case "fre"
ret = "Adresse �lectronique"
Case "ger"
ret = "e-Mailadresse"
Case "ita"
ret = "indirizzo email"
Case "spa"
ret = "direcci�n de email"
End Select
End Select
End Select
Return ret
End Function
Public Function getpmlibnewtaskgridval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Task Type"
Case "fre"
ret = "type de la t�che"
Case "ger"
ret = "Aufgabentyp"
Case "ita"
ret = "Tipo di Compito"
Case "spa"
ret = "Tipo de tarea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Frequency"
Case "fre"
ret = "Fr�quence"
Case "ger"
ret = "Frequenz"
Case "ita"
ret = "Frequenza"
Case "spa"
ret = "Frecuencia"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Failure Modes This Task Will Address"
Case "fre"
ret = "Modes de d�faillance que cette t�che va traiter"
Case "ger"
ret = "Modusfehler diese Aufgabe bezieht sich auf"
Case "ita"
ret = "Modalit� d`errore questa attivit� verr� indirizzata"
Case "spa"
ret = "Modos de Falla que Esta Tarea Atender�"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Time"
Case "fre"
ret = "Temps"
Case "ger"
ret = "Zeit"
Case "ita"
ret = "Tempo"
Case "spa"
ret = "Tiempo"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Predictive Technology"
Case "fre"
ret = "Technologie pr�dictive"
Case "ger"
ret = "Vorausschauende Technologie"
Case "ita"
ret = "Tecnologia predittiva"
Case "spa"
ret = "Tecnolog�a de Predictivo"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "PM Equipment Status"
Case "fre"
ret = "Statut de l`�quipement PM"
Case "ger"
ret = "PM Ausr�stungs-Status"
Case "ita"
ret = "Stato apparecchiatura PM"
Case "spa"
ret = "PM Estado del Equipo "
End Select
Case "19"
Select Case lang
Case "eng"
ret = "PM Equipment Status"
Case "fre"
ret = "Statut de l`�quipement PM"
Case "ger"
ret = "PM Ausr�stungs-Status"
Case "ita"
ret = "Stato apparecchiatura PM"
Case "spa"
ret = "PM Estado del Equipo "
End Select
Case "22"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "23"
Select Case lang
Case "eng"
ret = "Confined Spaced"
Case "fre"
ret = "Espace confin�"
Case "ger"
ret = "Begrenzte Platzm�glichkeit"
Case "ita"
ret = "Spazio confinato"
Case "spa"
ret = "Confined Spaced"
End Select
Case "24"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getpmmeval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "History"
Case "fre"
ret = "Historique"
Case "ger"
ret = "Vorgeschichte"
Case "ita"
ret = "Cronologia"
Case "spa"
ret = "Historia "
End Select
End Select
End Select
Return ret
End Function
Public Function getPMMeasval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "History"
Case "fre"
ret = "Historique"
Case "ger"
ret = "Vorgeschichte"
Case "ita"
ret = "Cronologia"
Case "spa"
ret = "Historia "
End Select
End Select
End Select
Return ret
End Function
Public Function getPMMeastpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "History"
Case "fre"
ret = "Historique"
Case "ger"
ret = "Vorgeschichte"
Case "ita"
ret = "Cronologia"
Case "spa"
ret = "Historia "
End Select
End Select
End Select
Return ret
End Function
Public Function getpmmetpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "History"
Case "fre"
ret = "Historique"
Case "ger"
ret = "Vorgeschichte"
Case "ita"
ret = "Cronologia"
Case "spa"
ret = "Historia "
End Select
End Select
End Select
Return ret
End Function
Public Function getPMRoutes2val(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dglist"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Equipment#"
Case "fre"
ret = "Num�ro d`�quipement"
Case "ger"
ret = "Ausr�stung#"
Case "ita"
ret = "Apparecchiatura n."
Case "spa"
ret = "Equipo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Misc Asset"
Case "fre"
ret = "Divers"
Case "ger"
ret = "Verschiedenes"
Case "ita"
ret = "Varie prodotto:"
Case "spa"
ret = "Misc Asset"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Misc Asset"
Case "fre"
ret = "Divers"
Case "ger"
ret = "Verschiedenes"
Case "ita"
ret = "Varie prodotto:"
Case "spa"
ret = "Misc Asset"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Department\Cell"
Case "fre"
ret = "D�partement\cellule"
Case "ger"
ret = "Abteilung \ Zelle"
Case "ita"
ret = "Dipartimento\Cella"
Case "spa"
ret = "Departmento/C�lula"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Department\Cell"
Case "fre"
ret = "D�partement\cellule"
Case "ger"
ret = "Abteilung \ Zelle"
Case "ita"
ret = "Dipartimento\Cella"
Case "spa"
ret = "Departmento/C�lula"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Job Plan"
Case "fre"
ret = "Plan de travail"
Case "ger"
ret = "Arbeitsplan"
Case "ita"
ret = "Job plan"
Case "spa"
ret = "Plan de trabajo"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Job Plan"
Case "fre"
ret = "Plan de travail"
Case "ger"
ret = "Arbeitsplan"
Case "ita"
ret = "Job plan"
Case "spa"
ret = "Plan de trabajo"
End Select
End Select
End Select
Return ret
End Function
Public Function getPMSuperManval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Name"
Case "fre"
ret = "Nom"
Case "ger"
ret = "Name"
Case "ita"
ret = "Nome"
Case "spa"
ret = "Nombre"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Phone"
Case "fre"
ret = "T�l�phone"
Case "ger"
ret = "Telefon"
Case "ita"
ret = "Telefono"
Case "spa"
ret = "Phone"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Email Address"
Case "fre"
ret = "Adresse �lectronique"
Case "ger"
ret = "e-Mailadresse"
Case "ita"
ret = "indirizzo email"
Case "spa"
ret = "direcci�n de email"
End Select
End Select
End Select
Return ret
End Function
Public Function getpmtaskschedval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dghours"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
End Select
End Select
Return ret
End Function
Public Function getpmtaskschedtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dghours"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Function"
Case "fre"
ret = "Fonction"
Case "ger"
ret = "Funktion "
Case "ita"
ret = "Funzione"
Case "spa"
ret = "Funci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Component"
Case "fre"
ret = "composant "
Case "ger"
ret = "komponente"
Case "ita"
ret = "Componente"
Case "spa"
ret = "Componente"
End Select
End Select
End Select
Return ret
End Function
Public Function getPrime_Selectionval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgdepts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Order"
Case "fre"
ret = "Commande"
Case "ger"
ret = "Reihenfolge"
Case "ita"
ret = "Ordine"
Case "spa"
ret = "Orden"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Prime Measure"
Case "fre"
ret = "Mesure principale"
Case "ger"
ret = "Grundma�"
Case "ita"
ret = "Prima misurazione"
Case "spa"
ret = "Medida Primaria"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getResourcesval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgrec"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Expert"
Case "fre"
ret = "Expert"
Case "ger"
ret = "Experte"
Case "ita"
ret = "Esperto"
Case "spa"
ret = "Expert"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Subject Matter"
Case "fre"
ret = "Objet"
Case "ger"
ret = "Thema:"
Case "ita"
ret = "Argomento"
Case "spa"
ret = "Materia"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Subject Matter"
Case "fre"
ret = "Objet"
Case "ger"
ret = "Thema:"
Case "ita"
ret = "Argomento"
Case "spa"
ret = "Materia"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Phone#"
Case "fre"
ret = "N� de t�l�phone"
Case "ger"
ret = "Telefonnummer:"
Case "ita"
ret = "Telefono#"
Case "spa"
ret = "Phone#"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Email Address"
Case "fre"
ret = "Adresse �lectronique"
Case "ger"
ret = "e-Mailadresse"
Case "ita"
ret = "indirizzo email"
Case "spa"
ret = "direcci�n de email"
End Select
End Select
End Select
Return ret
End Function
Public Function getschedpartsval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
End Select
Return ret
End Function
Public Function getsparepartlistval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Spare Part Description (Default is Part Description)"
Case "fre"
ret = "Description de pi�ces de rechange (Le d�faut est la description des pi�ces)"
Case "ger"
ret = "Ersatzteilbeschreibung (Standard ist Teilebeschreibung)"
Case "ita"
ret = "Descrizione parti di ricambio (Default � descrizione parte)"
Case "spa"
ret = "Descripci�n de la Parte de repuesto (el Valor predeterminado es la Descripci�n de la Parte) "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Location"
Case "fre"
ret = "Emplacement"
Case "ger"
ret = "Einsatzort"
Case "ita"
ret = "Posizione"
Case "spa"
ret = "Ubicaci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
End Select
End Select
Return ret
End Function
Public Function getsparepartpicharchtpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Spare Part Description (Default is Part Description)"
Case "fre"
ret = "Description de pi�ces de rechange (Le d�faut est la description des pi�ces)"
Case "ger"
ret = "Ersatzteilbeschreibung (Standard ist Teilebeschreibung)"
Case "ita"
ret = "Descrizione parti di ricambio (Default � descrizione parte)"
Case "spa"
ret = "Descripci�n de la Parte de repuesto (el Valor predeterminado es la Descripci�n de la Parte) "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
Case "dgtaskparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
End Select
Return ret
End Function
Public Function getsparepartpickval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Spare Part Description (Default is Part Description)"
Case "fre"
ret = "Description de pi�ces de rechange (Le d�faut est la description des pi�ces)"
Case "ger"
ret = "Ersatzteilbeschreibung (Standard ist Teilebeschreibung)"
Case "ita"
ret = "Descrizione parti di ricambio (Default � descrizione parte)"
Case "spa"
ret = "Descripci�n de la Parte de repuesto (el Valor predeterminado es la Descripci�n de la Parte) "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
Case "dgtaskparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
End Select
Return ret
End Function
Public Function getsparepartpickarchval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Spare Part Description (Default is Part Description)"
Case "fre"
ret = "Description de pi�ces de rechange (Le d�faut est la description des pi�ces)"
Case "ger"
ret = "Ersatzteilbeschreibung (Standard ist Teilebeschreibung)"
Case "ita"
ret = "Descrizione parti di ricambio (Default � descrizione parte)"
Case "spa"
ret = "Descripci�n de la Parte de repuesto (el Valor predeterminado es la Descripci�n de la Parte) "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "12"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "14"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
Case "dgtaskparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
End Select
Return ret
End Function
Public Function getsparepartpicktpmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgoparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
Case "dgparttasks"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Spare Part Description (Default is Part Description)"
Case "fre"
ret = "Description de pi�ces de rechange (Le d�faut est la description des pi�ces)"
Case "ger"
ret = "Ersatzteilbeschreibung (Standard ist Teilebeschreibung)"
Case "ita"
ret = "Descrizione parti di ricambio (Default � descrizione parte)"
Case "spa"
ret = "Descripci�n de la Parte de repuesto (el Valor predeterminado es la Descripci�n de la Parte) "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "11"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "15"
Select Case lang
Case "eng"
ret = "Cost"
Case "fre"
ret = "Co�t"
Case "ger"
ret = "Kosten"
Case "ita"
ret = "Costo"
Case "spa"
ret = "costo"
End Select
Case "16"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "17"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
Case "18"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "19"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
Case "dgtaskparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Part#"
Case "fre"
ret = "Pi�ce# "
Case "ger"
ret = "Teilnummer#"
Case "ita"
ret = "N.Pezzi"
Case "spa"
ret = "# de parte"
End Select
End Select
End Select
Return ret
End Function
Public Function getSubjectMatterval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgsm"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Subject Matter"
Case "fre"
ret = "Objet"
Case "ger"
ret = "Thema:"
Case "ita"
ret = "Argomento"
Case "spa"
ret = "Materia"
End Select
End Select
End Select
Return ret
End Function
Public Function gettaskschedval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparttasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
End Select
End Select
Return ret
End Function
Public Function gettotpmsval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtots"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Total Time"
Case "fre"
ret = "Temps total"
Case "ger"
ret = "Gesamtzeit"
Case "ita"
ret = "Tempo totale"
Case "spa"
ret = "Tiempo Total"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Down Time"
Case "fre"
ret = "Temps d`Arr�t"
Case "ger"
ret = "Ausfallzeit"
Case "ita"
ret = "Tempo di Fermo"
Case "spa"
ret = "Tiempo fuera de servicio"
End Select
End Select
End Select
Return ret
End Function
Public Function gettpmlubesval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Used"
Case "fre"
ret = "Utilis�"
Case "ger"
ret = "In Gebrauch"
Case "ita"
ret = "Utilizzato"
Case "spa"
ret = "Usado "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Qty Used"
Case "fre"
ret = "Qt� utilis�e"
Case "ger"
ret = "Anzahl verbraucht"
Case "ita"
ret = "Quantit� usata"
Case "spa"
ret = "Cantidad Utilizada"
End Select
End Select
End Select
Return ret
End Function
Public Function gettpmmeasval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
End Select
End Select
Return ret
End Function
Public Function gettpmpartsval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Used"
Case "fre"
ret = "Utilis�"
Case "ger"
ret = "In Gebrauch"
Case "ita"
ret = "Utilizzato"
Case "spa"
ret = "Usado "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Qty Used"
Case "fre"
ret = "Qt� utilis�e"
Case "ger"
ret = "Anzahl verbraucht"
Case "ita"
ret = "Quantit� usata"
Case "spa"
ret = "Cantidad Utilizada"
End Select
End Select
End Select
Return ret
End Function
Public Function gettpmtimeval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task Time"
Case "fre"
ret = "Temps de t�che"
Case "ger"
ret = "Aufgabenzeit"
Case "ita"
ret = "Ora attivit�"
Case "spa"
ret = "Task Time"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Minutes"
Case "fre"
ret = "Minutes"
Case "ger"
ret = "Minuten"
Case "ita"
ret = "Minuti"
Case "spa"
ret = "Minutos"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Down Time"
Case "fre"
ret = "Temps d`Arr�t"
Case "ger"
ret = "Ausfallzeit"
Case "ita"
ret = "Tempo di Fermo"
Case "spa"
ret = "Tiempo fuera de servicio"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Minutes"
Case "fre"
ret = "Minutes"
Case "ger"
ret = "Minuten"
Case "ita"
ret = "Minuti"
Case "spa"
ret = "Minutos"
End Select
End Select
Case "dgparts2"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Down Time"
Case "fre"
ret = "Temps d`Arr�t"
Case "ger"
ret = "Ausfallzeit"
Case "ita"
ret = "Tempo di Fermo"
Case "spa"
ret = "Tiempo fuera de servicio"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Minutes"
Case "fre"
ret = "Minutes"
Case "ger"
ret = "Minuten"
Case "ita"
ret = "Minuti"
Case "spa"
ret = "Minutos"
End Select
End Select
End Select
Return ret
End Function
Public Function gettpmtoolsval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Used"
Case "fre"
ret = "Utilis�"
Case "ger"
ret = "In Gebrauch"
Case "ita"
ret = "Utilizzato"
Case "spa"
ret = "Usado "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Qty Used"
Case "fre"
ret = "Qt� utilis�e"
Case "ger"
ret = "Anzahl verbraucht"
Case "ita"
ret = "Quantit� usata"
Case "spa"
ret = "Cantidad Utilizada"
End Select
End Select
End Select
Return ret
End Function
Public Function getwhereusedval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgitems"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Equipment#"
Case "fre"
ret = "Num�ro d`�quipement"
Case "ger"
ret = "Ausr�stung#"
Case "ita"
ret = "Apparecchiatura n."
Case "spa"
ret = "Equipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Notes"
Case "fre"
ret = "Remarques"
Case "ger"
ret = "Anmerkungen"
Case "ita"
ret = "Note"
Case "spa"
ret = "Notas "
End Select
End Select
End Select
Return ret
End Function
Public Function getwjsubval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Sub Task#"
Case "fre"
ret = "Sous-t�che#"
Case "ger"
ret = "Unteraufgabennummer#"
Case "ita"
ret = "N. Sotto Compito"
Case "spa"
ret = "# de sub tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Sub Task Description"
Case "fre"
ret = "Description de sous-t�che"
Case "ger"
ret = "Beschreibung der Unteraufgabe"
Case "ita"
ret = "Descrizione sottoattivit�"
Case "spa"
ret = "Descripci�n de Sub-Tarea"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getwoactmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Cost\Rate"
Case "fre"
ret = "Co�t\taux"
Case "ger"
ret = "Kosten \ Quote"
Case "ita"
ret = "Costo\tariffa"
Case "spa"
ret = "Costo/Tarifa"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Used"
Case "fre"
ret = "Utilis�"
Case "ger"
ret = "In Gebrauch"
Case "ita"
ret = "Utilizzato"
Case "spa"
ret = "Usado "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Qty Used"
Case "fre"
ret = "Qt� utilis�e"
Case "ger"
ret = "Anzahl verbraucht"
Case "ita"
ret = "Quantit� usata"
Case "spa"
ret = "Cantidad Utilizada"
End Select
End Select
End Select
Return ret
End Function
Public Function getWoChargeConfigval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Field Name"
Case "fre"
ret = "Nom de champ"
Case "ger"
ret = "Feldname"
Case "ita"
ret = "Nome del campo"
Case "spa"
ret = "Nombre del Campo"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Field Order"
Case "fre"
ret = "Ordre de champ"
Case "ger"
ret = "Feld-Rangordnung"
Case "ita"
ret = "Ordine del campo"
Case "spa"
ret = "Orden del Campo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Data Type"
Case "fre"
ret = "Type de donn�es"
Case "ger"
ret = "Datentyp"
Case "ita"
ret = "Tipo di dati"
Case "spa"
ret = "Tipo de Datos"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Length"
Case "fre"
ret = "Longueur"
Case "ger"
ret = "L�nge"
Case "ita"
ret = "Lunghezza"
Case "spa"
ret = "Longitud"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Required?"
Case "fre"
ret = "Obligatoire�?"
Case "ger"
ret = "Erforderlich?"
Case "ita"
ret = "Richiesto?"
Case "spa"
ret = "�Requerido?"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Delimiter"
Case "fre"
ret = "D�limiteur"
Case "ger"
ret = "Begrenzer"
Case "ita"
ret = "Delimitatore"
Case "spa"
ret = "Delimitador"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "To DB?"
Case "fre"
ret = "Vers la base de donn�es�?"
Case "ger"
ret = "Zur DB?"
Case "ita"
ret = "Al database?"
Case "spa"
ret = "�A DB?"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getWoChargeEntryval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtt"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Charge Type"
Case "fre"
ret = "Type de charge"
Case "ger"
ret = "Art des Beschickungsguts"
Case "ita"
ret = "Caricare tipo"
Case "spa"
ret = "Tipo de Cargo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Charge Number"
Case "fre"
ret = "Num�ro de charge"
Case "ger"
ret = "Nummer des Beschickungsguts"
Case "ita"
ret = "Caricare numero"
Case "spa"
ret = "N�ero de Cargo"
End Select
Case "13"
Select Case lang
Case "eng"
ret = "Remove"
Case "fre"
ret = "Enlever"
Case "ger"
ret = "entfernen"
Case "ita"
ret = "rimuovere"
Case "spa"
ret = "retirar"
End Select
End Select
End Select
Return ret
End Function
Public Function getwofm2val(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dltasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Failure Mode"
Case "fre"
ret = "Mode de d�faillance"
Case "ger"
ret = "Ausfallart"
Case "ita"
ret = "Modalit� errore"
Case "spa"
ret = "Modo de Falla"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Pass\Fail"
Case "fre"
ret = "Mdp\d�f"
Case "ger"
ret = "Freigabe/Sperre"
Case "ita"
ret = "Passa/non passa"
Case "spa"
ret = "Pass\Fail"
End Select
End Select
End Select
Return ret
End Function
Public Function getwofmme2val(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
End Select
End Select
Return ret
End Function
Public Function getwojpact2val(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dghours"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
End Select
End Select
Return ret
End Function
Public Function getwojpactmval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Item#"
Case "fre"
ret = "N� d`article"
Case "ger"
ret = "Artikel-Nr."
Case "ita"
ret = "Articolo#"
Case "spa"
ret = "N� de Art�culo  "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Description"
Case "fre"
ret = "Description"
Case "ger"
ret = "Beschreibung"
Case "ita"
ret = "Descrizione"
Case "spa"
ret = "Descripci�n"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Cost\Rate"
Case "fre"
ret = "Co�t\taux"
Case "ger"
ret = "Kosten \ Quote"
Case "ita"
ret = "Costo\tariffa"
Case "spa"
ret = "Costo/Tarifa"
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Total"
Case "fre"
ret = "Total"
Case "ger"
ret = "Total"
Case "ita"
ret = "Totale"
Case "spa"
ret = "Total"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Used"
Case "fre"
ret = "Utilis�"
Case "ger"
ret = "In Gebrauch"
Case "ita"
ret = "Utilizzato"
Case "spa"
ret = "Usado "
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Qty Used"
Case "fre"
ret = "Qt� utilis�e"
Case "ger"
ret = "Anzahl verbraucht"
Case "ita"
ret = "Quantit� usata"
Case "spa"
ret = "Cantidad Utilizada"
End Select
End Select
End Select
Return ret
End Function
Public Function getwojpeditval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgtasks"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Task Description"
Case "fre"
ret = "Description de la T�che "
Case "ger"
ret = "aufgabenbeschreibung"
Case "ita"
ret = "Descrizione Compito"
Case "spa"
ret = "Descripci�n de la Tarea"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Failure Mode"
Case "fre"
ret = "Mode de d�faillance"
Case "ger"
ret = "Ausfallart"
Case "ita"
ret = "Modalit� errore"
Case "spa"
ret = "Modo de Falla"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Qty"
Case "fre"
ret = "Qt de Comp�tence"
Case "ger"
ret = "Kompetenzmenge"
Case "ita"
ret = "Qt� Skill"
Case "spa"
ret = "Cant de habilidad"
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes/Meas"
Case "fre"
ret = "Pi�ces/outils/lubrifiants/mesures"
Case "ger"
ret = "Teile / Werkzeuge / Schmiermittel / Messungen"
Case "ita"
ret = "parti/strumenti/ lubrificanti/mis."
Case "spa"
ret = "Parts/Tools/Lubes/Meas"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Delete"
Case "fre"
ret = "Effacer"
Case "ger"
ret = "L�schen"
Case "ita"
ret = "Cancella"
Case "spa"
ret = "Borrar"
End Select
End Select
End Select
Return ret
End Function
Public Function getwojpme2val(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "6"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "7"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
End Select
End Select
Return ret
End Function
Public Function getwojpmeasval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgmeas"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Type"
Case "fre"
ret = "Type"
Case "ger"
ret = "Typ"
Case "ita"
ret = "Tipo"
Case "spa"
ret = "Tipo"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Hi"
Case "fre"
ret = "�lev�"
Case "ger"
ret = "Ober"
Case "ita"
ret = "Alto"
Case "spa"
ret = "M�ximo"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Lo"
Case "fre"
ret = "Bas"
Case "ger"
ret = "Unter"
Case "ita"
ret = "Basso"
Case "spa"
ret = "M�nimo "
End Select
Case "4"
Select Case lang
Case "eng"
ret = "Spec"
Case "fre"
ret = "Sp�c"
Case "ger"
ret = "Spezifikation"
Case "ita"
ret = "Spec"
Case "spa"
ret = "Especificaci�n "
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Measurement"
Case "fre"
ret = "Mesure"
Case "ger"
ret = "Bemessung"
Case "ita"
ret = "Misurazione"
Case "spa"
ret = "Medici�n"
End Select
End Select
End Select
Return ret
End Function
Public Function getwojpschedval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dghours"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Task#"
Case "fre"
ret = "Num�ro de t�che"
Case "ger"
ret = "Aufgaben-Nummer"
Case "ita"
ret = "Numero attivit�"
Case "spa"
ret = "N�mero de la tarea "
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Skill Required"
Case "fre"
ret = "Comp�tence Requise"
Case "ger"
ret = "Kompetenz erforderlich"
Case "ita"
ret = "Skill Richiesto"
Case "spa"
ret = "Se requiere habilidad"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Skill Min Ea"
Case "fre"
ret = "Comp�tence Min Chq"
Case "ger"
ret = "Kompetenz jede Minute"
Case "ita"
ret = "Min Skill cad."
Case "spa"
ret = "Habilidad Min Ea"
End Select
Case "8"
Select Case lang
Case "eng"
ret = "Schedule Date"
Case "fre"
ret = "Date de programmation�:"
Case "ger"
ret = "Termin Datum"
Case "ita"
ret = "Data programma"
Case "spa"
ret = "Fecha Programada"
End Select
Case "9"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
Case "10"
Select Case lang
Case "eng"
ret = "Parts/Tools/Lubes"
Case "fre"
ret = "Pi�ces/Outils/Lubrifiants"
Case "ger"
ret = "Teile/Tools/Schmier�le"
Case "ita"
ret = "Pezzi/Strumenti/Lubrificanti"
Case "spa"
ret = "Partes/Herramientas/Lubricantes"
End Select
End Select
End Select
Return ret
End Function
Public Function getwolabtransval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dgparts"
Select Case tcol
Case "1"
Select Case lang
Case "eng"
ret = "Name"
Case "fre"
ret = "Nom"
Case "ger"
ret = "Name"
Case "ita"
ret = "Nome"
Case "spa"
ret = "Nombre"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Work Date"
Case "fre"
ret = "Date du travail"
Case "ger"
ret = "Arbeitsdatum"
Case "ita"
ret = "Data lavoro"
Case "spa"
ret = "Fecha de trabajo"
End Select
End Select
End Select
Return ret
End Function
Public Function getwoschedval(byVal lang as String, byVal dgid as String, byVal tcol as String) as String
Dim ret as String
Select Case dgid
Case "dghours"
Select Case tcol
Case "0"
Select Case lang
Case "eng"
ret = "Edit"
Case "fre"
ret = "�diter"
Case "ger"
ret = "editieren"
Case "ita"
ret = "modifica"
Case "spa"
ret = "editar"
End Select
Case "1"
Select Case lang
Case "eng"
ret = "Activity (Reference)"
Case "fre"
ret = "Activit� (r�f�rence)"
Case "ger"
ret = "Aktivit�t (Referenz)"
Case "ita"
ret = "Riferimento attivit�"
Case "spa"
ret = "Activity (Reference)"
End Select
Case "2"
Select Case lang
Case "eng"
ret = "Schedule Date"
Case "fre"
ret = "Date de programmation�:"
Case "ger"
ret = "Termin Datum"
Case "ita"
ret = "Data programma"
Case "spa"
ret = "Fecha Programada"
End Select
Case "3"
Select Case lang
Case "eng"
ret = "Minutes"
Case "fre"
ret = "Minutes"
Case "ger"
ret = "Minuten"
Case "ita"
ret = "Minuti"
Case "spa"
ret = "Minutos"
End Select
Case "5"
Select Case lang
Case "eng"
ret = "Minutes"
Case "fre"
ret = "Minutes"
Case "ger"
ret = "Minuten"
Case "ita"
ret = "Minuti"
Case "spa"
ret = "Minutos"
End Select
End Select
End Select
Return ret
End Function
End Class
