Public Class codemsgs2
    Dim dlang As New mmenu_utils
    Dim dlab As New codemsgsvals
    Public Function GetCodePage(ByVal aspxpage As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Dim lang As String
        Try
            lang = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            lang = dlang.AppDfltLang
        End Try
        Select Case aspxpage
            Case "AddComp.aspx.vb"
                ret = dlab.getAddCompval(lang, aspxlabel)
            Case "addcomp2.aspx.vb"
                ret = dlab.getaddcomp2val(lang, aspxlabel)
            Case "AddEditFuncDialog.aspx.vb"
                ret = dlab.getAddEditFuncDialogval(lang, aspxlabel)
            Case "AddEditFuncPop.aspx.vb"
                ret = dlab.getAddEditFuncPopval(lang, aspxlabel)
            Case "AddFail.aspx.vb"
                ret = dlab.getAddFailval(lang, aspxlabel)
            Case "adjinv.aspx.vb"
                ret = dlab.getadjinvval(lang, aspxlabel)
            Case "altitem.aspx.vb"
                ret = dlab.getaltitemval(lang, aspxlabel)
            Case "AppSetApprHLTasks.aspx.vb"
                ret = dlab.getAppSetApprHLTasksval(lang, aspxlabel)
            Case "AppSetApprLLTasks.aspx.vb"
                ret = dlab.getAppSetApprLLTasksval(lang, aspxlabel)
            Case "AppSetApprPhases.aspx.vb"
                ret = dlab.getAppSetApprPhasesval(lang, aspxlabel)
            Case "AppSetAssetClass.aspx.vb"
                ret = dlab.getAppSetAssetClassval(lang, aspxlabel)
            Case "AppSetCellTab.aspx.vb"
                ret = dlab.getAppSetCellTabval(lang, aspxlabel)
            Case "AppSetComTab.aspx.vb"
                ret = dlab.getAppSetComTabval(lang, aspxlabel)
            Case "AppSetDeptTab.aspx.vb"
                ret = dlab.getAppSetDeptTabval(lang, aspxlabel)
            Case "AppSetDeptTrans.aspx.vb"
                ret = dlab.getAppSetDeptTransval(lang, aspxlabel)
            Case "AppSetEmail.aspx.vb"
                ret = dlab.getAppSetEmailval(lang, aspxlabel)
            Case "AppSetEqTab.aspx.vb"
                ret = dlab.getAppSetEqTabval(lang, aspxlabel)
            Case "AppSetInvLists.aspx.vb"
                ret = dlab.getAppSetInvListsval(lang, aspxlabel)
            Case "AppSetLoc.aspx.vb"
                ret = dlab.getAppSetLocval(lang, aspxlabel)
            Case "AppSetLT.aspx.vb"
                ret = dlab.getAppSetLTval(lang, aspxlabel)
            Case "AppSetpmApprGrps.aspx.vb"
                ret = dlab.getAppSetpmApprGrpsval(lang, aspxlabel)
            Case "AppSetSYS.aspx.vb"
                ret = dlab.getAppSetSYSval(lang, aspxlabel)
            Case "AppSetTaskTabET.aspx.vb"
                ret = dlab.getAppSetTaskTabETval(lang, aspxlabel)
            Case "AppSetTaskTabPT.aspx.vb"
                ret = dlab.getAppSetTaskTabPTval(lang, aspxlabel)
            Case "AppSetTaskTabST.aspx.vb"
                ret = dlab.getAppSetTaskTabSTval(lang, aspxlabel)
            Case "AppSetTaskTabTT.aspx.vb"
                ret = dlab.getAppSetTaskTabTTval(lang, aspxlabel)
            Case "AppSetWoChargeType.aspx.vb"
                ret = dlab.getAppSetWoChargeTypeval(lang, aspxlabel)
            Case "AppSetWoStatus.aspx.vb"
                ret = dlab.getAppSetWoStatusval(lang, aspxlabel)
            Case "AppSetWoType.aspx.vb"
                ret = dlab.getAppSetWoTypeval(lang, aspxlabel)
            Case "appsetwovars.aspx.vb"
                ret = dlab.getappsetwovarsval(lang, aspxlabel)
            Case "archnotes.aspx.vb"
                ret = dlab.getarchnotesval(lang, aspxlabel)
            Case "archnotestpm.aspx.vb"
                ret = dlab.getarchnotestpmval(lang, aspxlabel)
            Case "archrationale.aspx.vb"
                ret = dlab.getarchrationaleval(lang, aspxlabel)
            Case "archrationale2.aspx.vb"
                ret = dlab.getarchrationale2val(lang, aspxlabel)
            Case "archrationaledialog.aspx.vb"
                ret = dlab.getarchrationaledialogval(lang, aspxlabel)
            Case "archrationaledialogtpm.aspx.vb"
                ret = dlab.getarchrationaledialogtpmval(lang, aspxlabel)
            Case "archrationaletpm.aspx.vb"
                ret = dlab.getarchrationaletpmval(lang, aspxlabel)
            Case "archrationaletpm2.aspx.vb"
                ret = dlab.getarchrationaletpm2val(lang, aspxlabel)
            Case "archtasklubelist.aspx.vb"
                ret = dlab.getarchtasklubelistval(lang, aspxlabel)
            Case "archtasklubelisttpm.aspx.vb"
                ret = dlab.getarchtasklubelisttpmval(lang, aspxlabel)
            Case "archtaskpartlist.aspx.vb"
                ret = dlab.getarchtaskpartlistval(lang, aspxlabel)
            Case "archtaskpartlisttpm.aspx.vb"
                ret = dlab.getarchtaskpartlisttpmval(lang, aspxlabel)
            Case "archtasktoollist.aspx.vb"
                ret = dlab.getarchtasktoollistval(lang, aspxlabel)
            Case "archtasktoollisttpm.aspx.vb"
                ret = dlab.getarchtasktoollisttpmval(lang, aspxlabel)
            Case "Assessment_Admin.aspx.vb"
                ret = dlab.getAssessment_Adminval(lang, aspxlabel)
            Case "Assessment_Details.aspx.vb"
                ret = dlab.getAssessment_Detailsval(lang, aspxlabel)
            Case "catalog.aspx.vb"
                ret = dlab.getcatalogval(lang, aspxlabel)
            Case "cclasslookup.aspx.vb"
                ret = dlab.getcclasslookupval(lang, aspxlabel)
            Case "cget.aspx.vb"
                ret = dlab.getcgetval(lang, aspxlabel)
            Case "commontasks.aspx.vb"
                ret = dlab.getcommontasksval(lang, aspxlabel)
            Case "commontaskstpm.aspx.vb"
                ret = dlab.getcommontaskstpmval(lang, aspxlabel)
            Case "compclass.aspx.vb"
                ret = dlab.getcompclassval(lang, aspxlabel)
            Case "compclassmain.aspx.vb"
                ret = dlab.getcompclassmainval(lang, aspxlabel)
            Case "CompCopyMini.aspx.vb"
                ret = dlab.getCompCopyMinival(lang, aspxlabel)
            Case "CompCopyMiniDialog.aspx.vb"
                ret = dlab.getCompCopyMiniDialogval(lang, aspxlabel)
            Case "CompCopyMiniDialogtpm.aspx.vb"
                ret = dlab.getCompCopyMiniDialogtpmval(lang, aspxlabel)
            Case "CompCopyMinitpm.aspx.vb"
                ret = dlab.getCompCopyMinitpmval(lang, aspxlabel)
            Case "CompDialog.aspx.vb"
                ret = dlab.getCompDialogval(lang, aspxlabel)
            Case "CompDiv.aspx.vb"
                ret = dlab.getCompDivval(lang, aspxlabel)
            Case "CompDivGrid.aspx.vb"
                ret = dlab.getCompDivGridval(lang, aspxlabel)
            Case "CompFailDialog.aspx.vb"
                ret = dlab.getCompFailDialogval(lang, aspxlabel)
            Case "CompGrid.aspx.vb"
                ret = dlab.getCompGridval(lang, aspxlabel)
            Case "complib.aspx.vb"
                ret = dlab.getcomplibval(lang, aspxlabel)
            Case "complibadd.aspx.vb"
                ret = dlab.getcomplibaddval(lang, aspxlabel)
            Case "complibedit.aspx.vb"
                ret = dlab.getcomplibeditval(lang, aspxlabel)
            Case "complibsrchkey.aspx.vb"
                ret = dlab.getcomplibsrchkeyval(lang, aspxlabel)
            Case "complibtaskeadittpm.aspx.vb"
                ret = dlab.getcomplibtaskeadittpmval(lang, aspxlabel)
            Case "complibtasks.aspx.vb"
                ret = dlab.getcomplibtasksval(lang, aspxlabel)
            Case "complibtasks2.aspx.vb"
                ret = dlab.getcomplibtasks2val(lang, aspxlabel)
            Case "complibtasks3.aspx.vb"
                ret = dlab.getcomplibtasks3val(lang, aspxlabel)
            Case "complibtasksedit.aspx.vb"
                ret = dlab.getcomplibtaskseditval(lang, aspxlabel)
            Case "complibtasksedit2.aspx.vb"
                ret = dlab.getcomplibtasksedit2val(lang, aspxlabel)
            Case "complibuploadimage.aspx.vb"
                ret = dlab.getcomplibuploadimageval(lang, aspxlabel)
            Case "complookup.aspx.vb"
                ret = dlab.getcomplookupval(lang, aspxlabel)
            Case "compmain.aspx.vb"
                ret = dlab.getcompmainval(lang, aspxlabel)
            Case "compseq.aspx.vb"
                ret = dlab.getcompseqval(lang, aspxlabel)
            Case "comptypes.aspx.vb"
                ret = dlab.getcomptypesval(lang, aspxlabel)
            Case "csub.aspx.vb"
                ret = dlab.getcsubval(lang, aspxlabel)
            Case "csubdialog.aspx.vb"
                ret = dlab.getcsubdialogval(lang, aspxlabel)
            Case "devTaskLubeList.aspx.vb"
                ret = dlab.getdevTaskLubeListval(lang, aspxlabel)
            Case "devTaskLubeListtpm.aspx.vb"
                ret = dlab.getdevTaskLubeListtpmval(lang, aspxlabel)
            Case "devTaskPartList.aspx.vb"
                ret = dlab.getdevTaskPartListval(lang, aspxlabel)
            Case "devTaskPartListtpm.aspx.vb"
                ret = dlab.getdevTaskPartListtpmval(lang, aspxlabel)
            Case "devTaskToolList.aspx.vb"
                ret = dlab.getdevTaskToolListval(lang, aspxlabel)
            Case "devTaskToolListtpm.aspx.vb"
                ret = dlab.getdevTaskToolListtpmval(lang, aspxlabel)
            Case "doc.aspx.vb"
                ret = dlab.getdocval(lang, aspxlabel)
            Case "doctpm.aspx.vb"
                ret = dlab.getdoctpmval(lang, aspxlabel)
            Case "dragndropno.aspx.vb"
                ret = dlab.getdragndropnoval(lang, aspxlabel)
            Case "EditProfile.aspx.vb"
                ret = dlab.getEditProfileval(lang, aspxlabel)
            Case "EditProfile_v2.aspx.vb"
                ret = dlab.getEditProfile_v2val(lang, aspxlabel)
            Case "EQBot.aspx.vb"
                ret = dlab.getEQBotval(lang, aspxlabel)
            Case "EqBotGrid.aspx.vb"
                ret = dlab.getEqBotGridval(lang, aspxlabel)
            Case "eqcopy2.aspx.vb"
                ret = dlab.geteqcopy2val(lang, aspxlabel)
            Case "EQCopyMini.aspx.vb"
                ret = dlab.getEQCopyMinival(lang, aspxlabel)
            Case "EQCopyMiniDialog.aspx.vb"
                ret = dlab.getEQCopyMiniDialogval(lang, aspxlabel)
            Case "EQCopyMiniDialogtpm.aspx.vb"
                ret = dlab.getEQCopyMiniDialogtpmval(lang, aspxlabel)
            Case "EQCopyMinitpm.aspx.vb"
                ret = dlab.getEQCopyMinitpmval(lang, aspxlabel)
            Case "EqGrid.aspx.vb"
                ret = dlab.getEqGridval(lang, aspxlabel)
            Case "eqlookup.aspx.vb"
                ret = dlab.geteqlookupval(lang, aspxlabel)
            Case "EQMain.aspx.vb"
                ret = dlab.getEQMainval(lang, aspxlabel)
            Case "EqPopDialog.aspx.vb"
                ret = dlab.getEqPopDialogval(lang, aspxlabel)
            Case "eqtab.aspx.vb"
                ret = dlab.geteqtabval(lang, aspxlabel)
            Case "eqtab2.aspx.vb"
                ret = dlab.geteqtab2val(lang, aspxlabel)
            Case "equploadimage.aspx.vb"
                ret = dlab.getequploadimageval(lang, aspxlabel)
            Case "ForumMain.aspx.vb"
                ret = dlab.getForumMainval(lang, aspxlabel)
            Case "FuncCopyMini.aspx.vb"
                ret = dlab.getFuncCopyMinival(lang, aspxlabel)
            Case "FuncCopyMiniDialog.aspx.vb"
                ret = dlab.getFuncCopyMiniDialogval(lang, aspxlabel)
            Case "FuncCopyMiniDialogtpm.aspx.vb"
                ret = dlab.getFuncCopyMiniDialogtpmval(lang, aspxlabel)
            Case "FuncCopyMinitpm.aspx.vb"
                ret = dlab.getFuncCopyMinitpmval(lang, aspxlabel)
            Case "FuncDiv.aspx.vb"
                ret = dlab.getFuncDivval(lang, aspxlabel)
            Case "FuncDivGrid.aspx.vb"
                ret = dlab.getFuncDivGridval(lang, aspxlabel)
            Case "funcedit.aspx.vb"
                ret = dlab.getfunceditval(lang, aspxlabel)
            Case "FuncGrid.aspx.vb"
                ret = dlab.getFuncGridval(lang, aspxlabel)
            Case "funcseq.aspx.vb"
                ret = dlab.getfuncseqval(lang, aspxlabel)
            Case "GSub.aspx.vb"
                ret = dlab.getGSubval(lang, aspxlabel)
            Case "gsubarch.aspx.vb"
                ret = dlab.getgsubarchval(lang, aspxlabel)
            Case "gsubarchtpm.aspx.vb"
                ret = dlab.getgsubarchtpmval(lang, aspxlabel)
            Case "GSubDialog.aspx.vb"
                ret = dlab.getGSubDialogval(lang, aspxlabel)
            Case "gsubdialogarch.aspx.vb"
                ret = dlab.getgsubdialogarchval(lang, aspxlabel)
            Case "gsubdialogarchtpm.aspx.vb"
                ret = dlab.getgsubdialogarchtpmval(lang, aspxlabel)
            Case "GSubDialogtpm.aspx.vb"
                ret = dlab.getGSubDialogtpmval(lang, aspxlabel)
            Case "GSubtpm.aspx.vb"
                ret = dlab.getGSubtpmval(lang, aspxlabel)
            Case "gtaskfail.aspx.vb"
                ret = dlab.getgtaskfailval(lang, aspxlabel)
            Case "GTasksFunc2.aspx.vb"
                ret = dlab.getGTasksFunc2val(lang, aspxlabel)
            Case "GTasksFunctpm.aspx.vb"
                ret = dlab.getGTasksFunctpmval(lang, aspxlabel)
            Case "GTasksFunctpm2.aspx.vb"
                ret = dlab.getGTasksFunctpm2val(lang, aspxlabel)
            Case "Inventory.aspx.vb"
                ret = dlab.getInventoryval(lang, aspxlabel)
            Case "InventoryLubes.aspx.vb"
                ret = dlab.getInventoryLubesval(lang, aspxlabel)
            Case "InventoryTools.aspx.vb"
                ret = dlab.getInventoryToolsval(lang, aspxlabel)
            Case "issueinv.aspx.vb"
                ret = dlab.getissueinvval(lang, aspxlabel)
            Case "JobLoc.aspx.vb"
                ret = dlab.getJobLocval(lang, aspxlabel)
            Case "JobPlan.aspx.vb"
                ret = dlab.getJobPlanval(lang, aspxlabel)
            Case "JobPlanList.aspx.vb"
                ret = dlab.getJobPlanListval(lang, aspxlabel)
            Case "JobPlanRefReq.aspx.vb"
                ret = dlab.getJobPlanRefReqval(lang, aspxlabel)
            Case "jpfaillist.aspx.vb"
                ret = dlab.getjpfaillistval(lang, aspxlabel)
            Case "JSub.aspx.vb"
                ret = dlab.getJSubval(lang, aspxlabel)
            Case "JSubDialog.aspx.vb"
                ret = dlab.getJSubDialogval(lang, aspxlabel)
            Case "laborassign.aspx.vb"
                ret = dlab.getlaborassignval(lang, aspxlabel)
            Case "LocDialog1.aspx.vb"
                ret = dlab.getLocDialog1val(lang, aspxlabel)
            Case "Login.aspx.vb"
                ret = dlab.getLoginval(lang, aspxlabel)
            Case "logtrack.aspx.vb"
                ret = dlab.getlogtrackval(lang, aspxlabel)
            Case "lubegridedit.aspx.vb"
                ret = dlab.getlubegrideditval(lang, aspxlabel)
            Case "lubelistmini.aspx.vb"
                ret = dlab.getlubelistminival(lang, aspxlabel)
            Case "lubeout.aspx.vb"
                ret = dlab.getlubeoutval(lang, aspxlabel)
            Case "lubeoutroutes.aspx.vb"
                ret = dlab.getlubeoutroutesval(lang, aspxlabel)
            Case "lubesmain.aspx.vb"
                ret = dlab.getlubesmainval(lang, aspxlabel)
            Case "matlookup.aspx.vb"
                ret = dlab.getmatlookupval(lang, aspxlabel)
            Case "maxsearch.aspx.vb"
                ret = dlab.getmaxsearchval(lang, aspxlabel)
            Case "measflyadd.aspx.vb"
                ret = dlab.getmeasflyaddval(lang, aspxlabel)
            Case "measoutadd.aspx.vb"
                ret = dlab.getmeasoutaddval(lang, aspxlabel)
            Case "measoutaddrt.aspx.vb"
                ret = dlab.getmeasoutaddrtval(lang, aspxlabel)
            Case "measoutfly.aspx.vb"
                ret = dlab.getmeasoutflyval(lang, aspxlabel)
            Case "measouthistedit.aspx.vb"
                ret = dlab.getmeasouthisteditval(lang, aspxlabel)
            Case "measoutroutes.aspx.vb"
                ret = dlab.getmeasoutroutesval(lang, aspxlabel)
            Case "measoutroutesadd.aspx.vb"
                ret = dlab.getmeasoutroutesaddval(lang, aspxlabel)
            Case "measoutroutesmini.aspx.vb"
                ret = dlab.getmeasoutroutesminival(lang, aspxlabel)
            Case "measroutesaddbot.aspx.vb"
                ret = dlab.getmeasroutesaddbotval(lang, aspxlabel)
            Case "measuremain.aspx.vb"
                ret = dlab.getmeasuremainval(lang, aspxlabel)
            Case "measureout.aspx.vb"
                ret = dlab.getmeasureoutval(lang, aspxlabel)
            Case "measureoutedit.aspx.vb"
                ret = dlab.getmeasureouteditval(lang, aspxlabel)
            Case "MissingData.aspx.vb"
                ret = dlab.getMissingDataval(lang, aspxlabel)
            Case "NCBot.aspx.vb"
                ret = dlab.getNCBotval(lang, aspxlabel)
            Case "NCGrid.aspx.vb"
                ret = dlab.getNCGridval(lang, aspxlabel)
            Case "NewLogin.aspx.vb"
                ret = dlab.getNewLoginval(lang, aspxlabel)
            Case "optTaskLubeList.aspx.vb"
                ret = dlab.getoptTaskLubeListval(lang, aspxlabel)
            Case "optTaskLubeListtpm.aspx.vb"
                ret = dlab.getoptTaskLubeListtpmval(lang, aspxlabel)
            Case "optTaskPartList.aspx.vb"
                ret = dlab.getoptTaskPartListval(lang, aspxlabel)
            Case "optTaskPartListtpm.aspx.vb"
                ret = dlab.getoptTaskPartListtpmval(lang, aspxlabel)
            Case "optTaskToolList.aspx.vb"
                ret = dlab.getoptTaskToolListval(lang, aspxlabel)
            Case "optTaskToolListtpm.aspx.vb"
                ret = dlab.getoptTaskToolListtpmval(lang, aspxlabel)
            Case "OVRollup.aspx.vb"
                ret = dlab.getOVRollupval(lang, aspxlabel)
            Case "OVRollup_OP.aspx.vb"
                ret = dlab.getOVRollup_OPval(lang, aspxlabel)
            Case "OVRollup_Tot.aspx.vb"
                ret = dlab.getOVRollup_Totval(lang, aspxlabel)
            Case "OVRollupTPM.aspx.vb"
                ret = dlab.getOVRollupTPMval(lang, aspxlabel)
            Case "partsmain.aspx.vb"
                ret = dlab.getpartsmainval(lang, aspxlabel)
            Case "PFAdj.aspx.vb"
                ret = dlab.getPFAdjval(lang, aspxlabel)
            Case "PFAdjtpm.aspx.vb"
                ret = dlab.getPFAdjtpmval(lang, aspxlabel)
            Case "PFInt.aspx.vb"
                ret = dlab.getPFIntval(lang, aspxlabel)
            Case "PFIntDialog.aspx.vb"
                ret = dlab.getPFIntDialogval(lang, aspxlabel)
            Case "pmact.aspx.vb"
                ret = dlab.getpmactval(lang, aspxlabel)
            Case "pmactm.aspx.vb"
                ret = dlab.getpmactmval(lang, aspxlabel)
            Case "pmactmtpm.aspx.vb"
                ret = dlab.getpmactmtpmval(lang, aspxlabel)
            Case "pmacttpm.aspx.vb"
                ret = dlab.getpmacttpmval(lang, aspxlabel)
            Case "PMAltMan.aspx.vb"
                ret = dlab.getPMAltManval(lang, aspxlabel)
            Case "PMAltMantpm.aspx.vb"
                ret = dlab.getPMAltMantpmval(lang, aspxlabel)
            Case "pmarchget.aspx.vb"
                ret = dlab.getpmarchgetval(lang, aspxlabel)
            Case "pmarchgettpm.aspx.vb"
                ret = dlab.getpmarchgettpmval(lang, aspxlabel)
            Case "PMArchMan.aspx.vb"
                ret = dlab.getPMArchManval(lang, aspxlabel)
            Case "PMArchMantpm.aspx.vb"
                ret = dlab.getPMArchMantpmval(lang, aspxlabel)
            Case "PMArchRT.aspx.vb"
                ret = dlab.getPMArchRTval(lang, aspxlabel)
            Case "pmarchtasks.aspx.vb"
                ret = dlab.getpmarchtasksval(lang, aspxlabel)
            Case "pmarchtaskstpm.aspx.vb"
                ret = dlab.getpmarchtaskstpmval(lang, aspxlabel)
            Case "PMAttach.aspx.vb"
                ret = dlab.getPMAttachval(lang, aspxlabel)
            Case "PMAttachtpm.aspx.vb"
                ret = dlab.getPMAttachtpmval(lang, aspxlabel)
            Case "PMDivMan.aspx.vb"
                ret = dlab.getPMDivManval(lang, aspxlabel)
            Case "PMDivMantpm.aspx.vb"
                ret = dlab.getPMDivMantpmval(lang, aspxlabel)
            Case "PMFailMan.aspx.vb"
                ret = dlab.getPMFailManval(lang, aspxlabel)
            Case "PMFailMantpm.aspx.vb"
                ret = dlab.getPMFailMantpmval(lang, aspxlabel)
            Case "PMGetPMMan.aspx.vb"
                ret = dlab.getPMGetPMManval(lang, aspxlabel)
            Case "PMGetPMMantpm.aspx.vb"
                ret = dlab.getPMGetPMMantpmval(lang, aspxlabel)
            Case "PMGetTasksFunc.aspx.vb"
                ret = dlab.getPMGetTasksFuncval(lang, aspxlabel)
            Case "PMLeadMan.aspx.vb"
                ret = dlab.getPMLeadManval(lang, aspxlabel)
            Case "pmlibnewcodets.aspx.vb"
                ret = dlab.getpmlibnewcodetsval(lang, aspxlabel)
            Case "pmlibneweqdets.aspx.vb"
                ret = dlab.getpmlibneweqdetsval(lang, aspxlabel)
            Case "pmlibnewfudets.aspx.vb"
                ret = dlab.getpmlibnewfudetsval(lang, aspxlabel)
            Case "pmlibnewtaskgrid.aspx.vb"
                ret = dlab.getpmlibnewtaskgridval(lang, aspxlabel)
            Case "pmlocmain.aspx.vb"
                ret = dlab.getpmlocmainval(lang, aspxlabel)
            Case "PMMainMan.aspx.vb"
                ret = dlab.getPMMainManval(lang, aspxlabel)
            Case "PMMainMantpm.aspx.vb"
                ret = dlab.getPMMainMantpmval(lang, aspxlabel)
            Case "pmme.aspx.vb"
                ret = dlab.getpmmeval(lang, aspxlabel)
            Case "PMMeas.aspx.vb"
                ret = dlab.getPMMeasval(lang, aspxlabel)
            Case "PMMeastpm.aspx.vb"
                ret = dlab.getPMMeastpmval(lang, aspxlabel)
            Case "pmMeasure.aspx.vb"
                ret = dlab.getpmMeasureval(lang, aspxlabel)
            Case "pmMeasureTypes.aspx.vb"
                ret = dlab.getpmMeasureTypesval(lang, aspxlabel)
            Case "pmmetpm.aspx.vb"
                ret = dlab.getpmmetpmval(lang, aspxlabel)
            Case "PMOptMain.aspx.vb"
                ret = dlab.getPMOptMainval(lang, aspxlabel)
            Case "PMOptRationale2.aspx.vb"
                ret = dlab.getPMOptRationale2val(lang, aspxlabel)
            Case "PMOptRationaleTPM2.aspx.vb"
                ret = dlab.getPMOptRationaleTPM2val(lang, aspxlabel)
            Case "PMOptTasks.aspx.vb"
                ret = dlab.getPMOptTasksval(lang, aspxlabel)
            Case "PMRationaleDialog.aspx.vb"
                ret = dlab.getPMRationaleDialogval(lang, aspxlabel)
            Case "PMRationaleDialogtpm.aspx.vb"
                ret = dlab.getPMRationaleDialogtpmval(lang, aspxlabel)
            Case "PMRouteList.aspx.vb"
                ret = dlab.getPMRouteListval(lang, aspxlabel)
            Case "PMRoutes.aspx.vb"
                ret = dlab.getPMRoutesval(lang, aspxlabel)
            Case "PMRoutes2.aspx.vb"
                ret = dlab.getPMRoutes2val(lang, aspxlabel)
            Case "pmselect.aspx.vb"
                ret = dlab.getpmselectval(lang, aspxlabel)
            Case "PMSetAdjMan.aspx.vb"
                ret = dlab.getPMSetAdjManval(lang, aspxlabel)
            Case "PMSetAdjManPRE.aspx.vb"
                ret = dlab.getPMSetAdjManPREval(lang, aspxlabel)
            Case "PMSetAdjManPREtpm.aspx.vb"
                ret = dlab.getPMSetAdjManPREtpmval(lang, aspxlabel)
            Case "PMSetAdjMantpm.aspx.vb"
                ret = dlab.getPMSetAdjMantpmval(lang, aspxlabel)
            Case "pmSiteFM.aspx.vb"
                ret = dlab.getpmSiteFMval(lang, aspxlabel)
            Case "pmSiteSkills.aspx.vb"
                ret = dlab.getpmSiteSkillsval(lang, aspxlabel)
            Case "pmSiteSkillsDialog.aspx.vb"
                ret = dlab.getpmSiteSkillsDialogval(lang, aspxlabel)
            Case "PMSuperMan.aspx.vb"
                ret = dlab.getPMSuperManval(lang, aspxlabel)
            Case "PMTaskDivFunc.aspx.vb"
                ret = dlab.getPMTaskDivFuncval(lang, aspxlabel)
            Case "pmTaskMeasurements.aspx.vb"
                ret = dlab.getpmTaskMeasurementsval(lang, aspxlabel)
            Case "pmuploadimage.aspx.vb"
                ret = dlab.getpmuploadimageval(lang, aspxlabel)
            Case "pmuploadimage1.aspx.vb"
                ret = dlab.getpmuploadimage1val(lang, aspxlabel)
            Case "PopAddEq.aspx.vb"
                ret = dlab.getPopAddEqval(lang, aspxlabel)
            Case "PopAddPart.aspx.vb"
                ret = dlab.getPopAddPartval(lang, aspxlabel)
            Case "Prime_Selection.aspx.vb"
                ret = dlab.getPrime_Selectionval(lang, aspxlabel)
            Case "purchreq.aspx.vb"
                ret = dlab.getpurchreqval(lang, aspxlabel)
            Case "purchreqedit.aspx.vb"
                ret = dlab.getpurchreqeditval(lang, aspxlabel)
            Case "qtime.aspx.vb"
                ret = dlab.getqtimeval(lang, aspxlabel)
            Case "query.aspx.vb"
                ret = dlab.getqueryval(lang, aspxlabel)
            Case "reclookup.aspx.vb"
                ret = dlab.getreclookupval(lang, aspxlabel)
            Case "recvinv.aspx.vb"
                ret = dlab.getrecvinvval(lang, aspxlabel)
            Case "reorderdetails.aspx.vb"
                ret = dlab.getreorderdetailsval(lang, aspxlabel)
            Case "reports2.aspx.vb"
                ret = dlab.getreports2val(lang, aspxlabel)
            Case "Resources.aspx.vb"
                ret = dlab.getResourcesval(lang, aspxlabel)
            Case "ScheduledTasks.aspx.vb"
                ret = dlab.getScheduledTasksval(lang, aspxlabel)
            Case "sclasslookup.aspx.vb"
                ret = dlab.getsclasslookupval(lang, aspxlabel)
            Case "sfail.aspx.vb"
                ret = dlab.getsfailval(lang, aspxlabel)
            Case "siteassets2.aspx.vb"
                ret = dlab.getsiteassets2val(lang, aspxlabel)
            Case "siteassets3.aspx.vb"
                ret = dlab.getsiteassets3val(lang, aspxlabel)
            Case "sparepartlist.aspx.vb"
                ret = dlab.getsparepartlistval(lang, aspxlabel)
            Case "sparepartpicharchtpm.aspx.vb"
                ret = dlab.getsparepartpicharchtpmval(lang, aspxlabel)
            Case "sparepartpick.aspx.vb"
                ret = dlab.getsparepartpickval(lang, aspxlabel)
            Case "sparepartpickarch.aspx.vb"
                ret = dlab.getsparepartpickarchval(lang, aspxlabel)
            Case "sparepartpicktpm.aspx.vb"
                ret = dlab.getsparepartpicktpmval(lang, aspxlabel)
            Case "squery.aspx.vb"
                ret = dlab.getsqueryval(lang, aspxlabel)
            Case "storeroom.aspx.vb"
                ret = dlab.getstoreroomval(lang, aspxlabel)
            Case "SubjectMatter.aspx.vb"
                ret = dlab.getSubjectMatterval(lang, aspxlabel)
            Case "taskedit2.aspx.vb"
                ret = dlab.gettaskedit2val(lang, aspxlabel)
            Case "taskimagepm.aspx.vb"
                ret = dlab.gettaskimagepmval(lang, aspxlabel)
            Case "taskimagetpm.aspx.vb"
                ret = dlab.gettaskimagetpmval(lang, aspxlabel)
            Case "TaskNotes.aspx.vb"
                ret = dlab.getTaskNotesval(lang, aspxlabel)
            Case "TaskNotestpm.aspx.vb"
                ret = dlab.getTaskNotestpmval(lang, aspxlabel)
            Case "tmdialog.aspx.vb"
                ret = dlab.gettmdialogval(lang, aspxlabel)
            Case "totpms.aspx.vb"
                ret = dlab.gettotpmsval(lang, aspxlabel)
            Case "tpmget.aspx.vb"
                ret = dlab.gettpmgetval(lang, aspxlabel)
            Case "tpmgettasks.aspx.vb"
                ret = dlab.gettpmgettasksval(lang, aspxlabel)
            Case "tpmlubes.aspx.vb"
                ret = dlab.gettpmlubesval(lang, aspxlabel)
            Case "tpmmeas.aspx.vb"
                ret = dlab.gettpmmeasval(lang, aspxlabel)
            Case "tpmoprun.aspx.vb"
                ret = dlab.gettpmoprunval(lang, aspxlabel)
            Case "tpmoptgetmain.aspx.vb"
                ret = dlab.gettpmoptgetmainval(lang, aspxlabel)
            Case "tpmopttasks.aspx.vb"
                ret = dlab.gettpmopttasksval(lang, aspxlabel)
            Case "tpmparts.aspx.vb"
                ret = dlab.gettpmpartsval(lang, aspxlabel)
            Case "tpmtask.aspx.vb"
                ret = dlab.gettpmtaskval(lang, aspxlabel)
            Case "tpmtasks.aspx.vb"
                ret = dlab.gettpmtasksval(lang, aspxlabel)
            Case "tpmtime.aspx.vb"
                ret = dlab.gettpmtimeval(lang, aspxlabel)
            Case "tpmtools.aspx.vb"
                ret = dlab.gettpmtoolsval(lang, aspxlabel)
            Case "tpmuploadimage.aspx.vb"
                ret = dlab.gettpmuploadimageval(lang, aspxlabel)
            Case "tpmuploadimage1.aspx.vb"
                ret = dlab.gettpmuploadimage1val(lang, aspxlabel)
            Case "tpmvars.aspx.vb"
                ret = dlab.gettpmvarsval(lang, aspxlabel)
            Case "tpmwr.aspx.vb"
                ret = dlab.gettpmwrval(lang, aspxlabel)
            Case "TranEq.aspx.vb"
                ret = dlab.getTranEqval(lang, aspxlabel)
            Case "upload.aspx.vb"
                ret = dlab.getuploadval(lang, aspxlabel)
            Case "uploadtpm.aspx.vb"
                ret = dlab.getuploadtpmval(lang, aspxlabel)
            Case "UserAdmin.aspx.vb"
                ret = dlab.getUserAdminval(lang, aspxlabel)
            Case "useradmin2.aspx.vb"
                ret = dlab.getuseradmin2val(lang, aspxlabel)
            Case "webimage.aspx.vb"
                ret = dlab.getwebimageval(lang, aspxlabel)
            Case "whereused.aspx.vb"
                ret = dlab.getwhereusedval(lang, aspxlabel)
            Case "wjsub.aspx.vb"
                ret = dlab.getwjsubval(lang, aspxlabel)
            Case "wjsubdialog.aspx.vb"
                ret = dlab.getwjsubdialogval(lang, aspxlabel)
            Case "woactm.aspx.vb"
                ret = dlab.getwoactmval(lang, aspxlabel)
            Case "woadd.aspx.vb"
                ret = dlab.getwoaddval(lang, aspxlabel)
            Case "WoChargeConfig.aspx.vb"
                ret = dlab.getWoChargeConfigval(lang, aspxlabel)
            Case "WoChargeEntry.aspx.vb"
                ret = dlab.getWoChargeEntryval(lang, aspxlabel)
            Case "wodet.aspx.vb"
                ret = dlab.getwodetval(lang, aspxlabel)
            Case "woexpd.aspx.vb"
                ret = dlab.getwoexpdval(lang, aspxlabel)
            Case "wofail.aspx.vb"
                ret = dlab.getwofailval(lang, aspxlabel)
            Case "wofaillist.aspx.vb"
                ret = dlab.getwofaillistval(lang, aspxlabel)
            Case "wofmme2.aspx.vb"
                ret = dlab.getwofmme2val(lang, aspxlabel)
            Case "wojpact.aspx.vb"
                ret = dlab.getwojpactval(lang, aspxlabel)
            Case "wojpact2.aspx.vb"
                ret = dlab.getwojpact2val(lang, aspxlabel)
            Case "wojpactm.aspx.vb"
                ret = dlab.getwojpactmval(lang, aspxlabel)
            Case "wojpcompman.aspx.vb"
                ret = dlab.getwojpcompmanval(lang, aspxlabel)
            Case "wojpedit.aspx.vb"
                ret = dlab.getwojpeditval(lang, aspxlabel)
            Case "wojpfaillist.aspx.vb"
                ret = dlab.getwojpfaillistval(lang, aspxlabel)
            Case "wojpfm.aspx.vb"
                ret = dlab.getwojpfmval(lang, aspxlabel)
            Case "wojpme2.aspx.vb"
                ret = dlab.getwojpme2val(lang, aspxlabel)
            Case "wojpmeas.aspx.vb"
                ret = dlab.getwojpmeasval(lang, aspxlabel)
            Case "wolabtrans.aspx.vb"
                ret = dlab.getwolabtransval(lang, aspxlabel)
            Case "wolist.aspx.vb"
                ret = dlab.getwolistval(lang, aspxlabel)
            Case "wolistmini.aspx.vb"
                ret = dlab.getwolistminival(lang, aspxlabel)
            Case "woman.aspx.vb"
                ret = dlab.getwomanval(lang, aspxlabel)
            Case "woplans.aspx.vb"
                ret = dlab.getwoplansval(lang, aspxlabel)
            Case "wosched.aspx.vb"
                ret = dlab.getwoschedval(lang, aspxlabel)
            Case "wradd.aspx.vb"
                ret = dlab.getwraddval(lang, aspxlabel)
            Case "wrdet.aspx.vb"
                ret = dlab.getwrdetval(lang, aspxlabel)
            Case "wrexpd.aspx.vb"
                ret = dlab.getwrexpdval(lang, aspxlabel)
            Case "wrman.aspx.vb"
                ret = dlab.getwrmanval(lang, aspxlabel)
            Case "wrselect.aspx.vb"
                ret = dlab.getwrselectval(lang, aspxlabel)
            Case "wrskills.aspx.vb"
                ret = dlab.getwrskillsval(lang, aspxlabel)
            Case "wrskillsdialog.aspx.vb"
                ret = dlab.getwrskillsdialogval(lang, aspxlabel)
        End Select
        Return ret
    End Function
End Class
