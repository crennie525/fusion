

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrdown

    Dim tmod As New transmod

    Dim wr As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim did, clid, lid, dept, cell, loc, wdesc, reqemail, sid, eqnum As String

    Public Function GetDownMail(ByVal wonum As String) As Integer

        Dim em As New System.Web.Mail.MailMessage
        Dim suser, spass, sserv, sport, suse, sfrom As String
        sserv = System.Configuration.ConfigurationManager.AppSettings("custSMTP")
        sport = System.Configuration.ConfigurationManager.AppSettings("custSMTPPort")
        sfrom = System.Configuration.ConfigurationManager.AppSettings("custSendFrom")
        suser = System.Configuration.ConfigurationManager.AppSettings("custSendUser")
        spass = System.Configuration.ConfigurationManager.AppSettings("custSendPass")

        sql = "select w.deptid, d.dept_line, w.cellid, c.cell_name, w.locid, l.location, w.description, w.siteid, w.eqid, e.eqnum " _
        + "from workorder w " _
        + "left join dept d on d.dept_id = w.deptid " _
        + "left join cells c on c.dept_id = d.dept_id " _
        + "left join pmlocations l on l.locid = w.locid " _
        + "left join equipment e on e.eqid = w.eqid " _
        + "where w.wonum = '" & wonum & "'"

        wr.Open()

        dr = wr.GetRdrData(sql)
        While dr.Read
            did = dr.Item("deptid").ToString
            dept = dr.Item("dept_line").ToString

            clid = dr.Item("cellid").ToString
            cell = dr.Item("cell_name").ToString

            lid = dr.Item("locid").ToString
            loc = dr.Item("location").ToString

            wdesc = dr.Item("description").ToString

            sid = dr.Item("siteid").ToString
            eqnum = dr.Item("eqnum").ToString
        End While
        dr.Close()

        Dim scnt As Integer = 0
        If did <> "" Then
            sql = "select s.superid, u.email from pmsuperlocs sl " _
            + "left join pmsysusers u on u.userid = sl.superid " _
            + "left join pmsuper s on s.superid = sl.superid " _
            + "where(sl.siteid = '" & sid & "' and sl.dept_id = '" & did & "')"
        ElseIf lid <> "" Then
            sql = "select s.superid, u.email from pmsuperlocs sl " _
            + "left join pmsysusers u on u.userid = sl.superid " _
            + "left join pmsuper s on s.superid = sl.superid " _
            + "where(sl.siteid = '" & sid & "' and sl.locid = '" & lid & "')"
        Else
            sql = ""
        End If
        dr = wr.GetRdrData(sql)
        While dr.Read
            scnt += 1
            reqemail = dr.Item("email").ToString

            em.To = reqemail
            em.From = sfrom
            em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
            em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = sserv
            em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = suser
            em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = spass
            em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
            em.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = sport

            Dim intralog As New mmenu_utils_a
            Dim isintra As Integer = intralog.INTRA
            If isintra <> 1 Then
                em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
            End If


            em.Body = AlertBody(wonum, eqnum, dept, cell, loc, wdesc)
            em.Subject = tmod.getxlbl("xlb168" , "wrdown.vb") & " " & eqnum & " is Down"
            em.BodyFormat = Web.Mail.MailFormat.Html
            System.Web.Mail.SmtpMail.SmtpServer.Insert(0, sserv)
            System.Web.Mail.SmtpMail.Send(em)

        End While
        dr.Close()
        If scnt = 0 Then
            sql = "select email from pmsysusers where isdefault = 1 and uid in (select uid from " _
            + "pmusersites where siteid = '" & sid & "')"
            dr = wr.GetRdrData(sql)
            While dr.Read
                scnt += 1
                reqemail = dr.Item("email").ToString

                em.To = reqemail
                em.From = sfrom
                em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
                em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = sserv
                em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = suser
                em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = spass
                em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
                em.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = sport

                Dim intralog As New mmenu_utils_a
                Dim isintra As Integer = intralog.INTRA
                If isintra <> 1 Then
                    em.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
                End If


                em.Body = AlertBody(wonum, eqnum, dept, cell, loc, wdesc)
                em.Subject = tmod.getxlbl("xlb169" , "wrdown.vb") & " " & eqnum & " is Down"
                em.BodyFormat = Web.Mail.MailFormat.Html
                System.Web.Mail.SmtpMail.SmtpServer.Insert(0, sserv)
                System.Web.Mail.SmtpMail.Send(em)

            End While
            dr.Close()
        End If

        wr.Dispose()

        Return scnt

    End Function
    Private Function AlertBody(ByVal wonum As String, ByVal eqnum As String, ByVal dept As String, ByVal cell As String, _
    ByVal loc As String, ByVal desc As String) As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrle")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")

        Dim body As String
        body = "<table width='800px' style='font-size:10pt; font-family:Verdana;'>"
        body &= "<tr><td colspan=""2""><b>" & tmod.getxlbl("xlb170" , "wrdown.vb") & " " & eqnum & " " & tmod.getxlbl("xlb183" , "wrdown.vb") & "</b><br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b>" & tmod.getxlbl("xlb171" , "wrdown.vb") & "</b></td><td>" & dept & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b>" & tmod.getxlbl("xlb172" , "wrdown.vb") & "</b></td><td>" & cell & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td><b>" & tmod.getxlbl("xlb173" , "wrdown.vb") & "</b></td><td>" & loc & "<br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2""><b>" & tmod.getxlbl("xlb174" , "wrdown.vb") & "</b><br><br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td colspan=""2""><b>" & desc & "</b><br><br></td></tr>" & vbCrLf & vbCrLf

        body &= "<tr><td colspan=""2"">" & tmod.getlbl("cdlbl405" , "wrdown.vb") & "<a href='" + urlname + appname + "/appswo/woprint.aspx?typ=wo&wo=" + wonum + "'>" & tmod.getxlbl("xlb182" , "wrdown.vb") & " " + wonum + "</td></tr>" & vbCrLf & vbCrLf

        body &= "</table>"

        Return body
    End Function
End Class
