Public Class codeovlibvals
    Public Function getaltitemovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov284"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Alternate Item"
                    Case "fre"
                        ret = "Rechercher l`article alternatif"
                    Case "ger"
                        ret = "Alternativ-Artikelsuche"
                    Case "ita"
                        ret = "Ricerca articolo alternativo"
                    Case "spa"
                        ret = "Buscar Art�culo  Alternativo "
                End Select
            Case "cov285"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Precaution"
                    Case "fre"
                        ret = "Rechercher la pr�caution"
                    Case "ger"
                        ret = "Suche nach Sicherheitsvorkehrungen"
                    Case "ita"
                        ret = "Ricerca precauzioni"
                    Case "spa"
                        ret = "Buscar Precauci�n "
                End Select
        End Select
        Return ret
    End Function
    Public Function getarchrationale2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov25"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes and Return"
                    Case "fre"
                        ret = "Sauvegarder les modifications et revenir"
                    Case "ger"
                        ret = "�nderungen speichern und zur�ckkehren"
                    Case "ita"
                        ret = "Salva modifiche e ritorno"
                    Case "spa"
                        ret = "Guardar Cambios y Regresar "
                End Select
        End Select
        Return ret
    End Function
    Public Function getarchrationaletpm2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov34"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes and Return"
                    Case "fre"
                        ret = "Sauvegarder les modifications et revenir"
                    Case "ger"
                        ret = "�nderungen speichern und zur�ckkehren"
                    Case "ita"
                        ret = "Salva modifiche e ritorno"
                    Case "spa"
                        ret = "Guardar Cambios y Regresar "
                End Select
        End Select
        Return ret
    End Function
    Public Function getcatalogovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov286"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Alternate Item"
                    Case "fre"
                        ret = "Rechercher l`article alternatif"
                    Case "ger"
                        ret = "Alternativ-Artikelsuche"
                    Case "ita"
                        ret = "Ricerca articolo alternativo"
                    Case "spa"
                        ret = "Buscar Art�culo  Alternativo "
                End Select
            Case "cov287"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Precaution"
                    Case "fre"
                        ret = "Rechercher la pr�caution"
                    Case "ger"
                        ret = "Suche nach Sicherheitsvorkehrungen"
                    Case "ita"
                        ret = "Ricerca precauzioni"
                    Case "spa"
                        ret = "Buscar Precauci�n "
                End Select
        End Select
        Return ret
    End Function
    Public Function getcommontasksovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov1"
                Select Case lang
                    Case "eng"
                        ret = "Edit Task"
                    Case "fre"
                        ret = "Modifier t�che"
                    Case "ger"
                        ret = "Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica attivit�"
                    Case "spa"
                        ret = "Edit Task"
                End Select
            Case "cov2"
                Select Case lang
                    Case "eng"
                        ret = "Edit Task"
                    Case "fre"
                        ret = "Modifier t�che"
                    Case "ger"
                        ret = "Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica attivit�"
                    Case "spa"
                        ret = "Edit Task"
                End Select
        End Select
        Return ret
    End Function
    Public Function getcommontaskstpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov147"
                Select Case lang
                    Case "eng"
                        ret = "Edit Task"
                    Case "fre"
                        ret = "Modifier t�che"
                    Case "ger"
                        ret = "Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica attivit�"
                    Case "spa"
                        ret = "Edit Task"
                End Select
            Case "cov148"
                Select Case lang
                    Case "eng"
                        ret = "Edit Task"
                    Case "fre"
                        ret = "Modifier t�che"
                    Case "ger"
                        ret = "Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica attivit�"
                    Case "spa"
                        ret = "Edit Task"
                End Select
        End Select
        Return ret
    End Function
    Public Function getCompDivovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov265"
                Select Case lang
                    Case "eng"
                        ret = "Save changes for this Component"
                    Case "fre"
                        ret = "Enregistrer les changements de ce composant"
                    Case "ger"
                        ret = "Speichern Sie die �nderungen f�r diese Komponente"
                    Case "ita"
                        ret = "Salva modifiche per questo componente"
                    Case "spa"
                        ret = "Guardar los cambios para este Componente"
                End Select
            Case "cov266"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Component"
                    Case "fre"
                        ret = "Supprimer ce composant"
                    Case "ger"
                        ret = "L�schen dieser Komponente"
                    Case "ita"
                        ret = "Elimina questo componente"
                    Case "spa"
                        ret = "Borrar este Componente"
                End Select
        End Select
        Return ret
    End Function
    Public Function getcomplibtaskeadittpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov175"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
            Case "cov176"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov177"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov178"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov179"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov180"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov181"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov182"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov183"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov184"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov185"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov186"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov187"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov188"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov189"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
        End Select
        Return ret
    End Function
    Public Function getcomplibtasksovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov190"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov191"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov192"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
        End Select
        Return ret
    End Function
    Public Function getcomplibtasks2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov193"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
            Case "cov194"
                Select Case lang
                    Case "eng"
                        ret = "Send Task to Operator Care Module (TPM)"
                    Case "fre"
                        ret = "Envoyer la t�che au module de contr�le de l`op�rateur (MPT)"
                    Case "ger"
                        ret = "Aufgabe an Bediener-Wartungs-Modul senden (TPM)"
                    Case "ita"
                        ret = "Invia attivit� al modulo assistenza operatore (TPM)"
                    Case "spa"
                        ret = "Env�e la Tarea a M�dulo de Cuidado por el Operador (Aut�nomo - TPM)"
                End Select
            Case "cov195"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov196"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov197"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov198"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov199"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov200"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov201"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov202"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov203"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov204"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov205"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov206"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov207"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov208"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
        End Select
        Return ret
    End Function
    Public Function getcomplibtasks3ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov209"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
            Case "cov210"
                Select Case lang
                    Case "eng"
                        ret = "Send Task to Operator Care Module (TPM)"
                    Case "fre"
                        ret = "Envoyer la t�che au module de contr�le de l`op�rateur (MPT)"
                    Case "ger"
                        ret = "Aufgabe an Bediener-Wartungs-Modul senden (TPM)"
                    Case "ita"
                        ret = "Invia attivit� al modulo assistenza operatore (TPM)"
                    Case "spa"
                        ret = "Env�e la Tarea a M�dulo de Cuidado por el Operador (Aut�nomo - TPM)"
                End Select
            Case "cov211"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov212"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov213"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov214"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov215"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov216"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov217"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov218"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov219"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov220"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov221"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov222"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov223"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov224"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
        End Select
        Return ret
    End Function
    Public Function getcomplibtaskseditovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov225"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
            Case "cov226"
                Select Case lang
                    Case "eng"
                        ret = "Send Task to Operator Care Module (TPM)"
                    Case "fre"
                        ret = "Envoyer la t�che au module de contr�le de l`op�rateur (MPT)"
                    Case "ger"
                        ret = "Aufgabe an Bediener-Wartungs-Modul senden (TPM)"
                    Case "ita"
                        ret = "Invia attivit� al modulo assistenza operatore (TPM)"
                    Case "spa"
                        ret = "Env�e la Tarea a M�dulo de Cuidado por el Operador (Aut�nomo - TPM)"
                End Select
            Case "cov227"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov228"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov229"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov230"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov231"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov232"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov233"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov234"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov235"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov236"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov237"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov238"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov239"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov240"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
        End Select
        Return ret
    End Function
    Public Function getcomplibtasksedit2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov241"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
            Case "cov242"
                Select Case lang
                    Case "eng"
                        ret = "Send Task to Operator Care Module (TPM)"
                    Case "fre"
                        ret = "Envoyer la t�che au module de contr�le de l`op�rateur (MPT)"
                    Case "ger"
                        ret = "Aufgabe an Bediener-Wartungs-Modul senden (TPM)"
                    Case "ita"
                        ret = "Invia attivit� al modulo assistenza operatore (TPM)"
                    Case "spa"
                        ret = "Env�e la Tarea a M�dulo de Cuidado por el Operador (Aut�nomo - TPM)"
                End Select
            Case "cov243"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov244"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov245"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov246"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov247"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov248"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov249"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov250"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov251"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov252"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov253"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov254"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov255"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Sub-Task"
                    Case "fre"
                        ret = "Aller � la sous-t�che pr�c�dente"
                    Case "ger"
                        ret = "Zur vorherigen Unteraufgabe"
                    Case "ita"
                        ret = "Vai al sotto attivit� precedente"
                    Case "spa"
                        ret = "Go to Previous Sub-Task"
                End Select
            Case "cov256"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Sub-Task"
                    Case "fre"
                        ret = "Aller � la sous-t�che suivante"
                    Case "ger"
                        ret = "Zur n�chsten Unteraufgabe"
                    Case "ita"
                        ret = "Vai al sotto attivit� successivo"
                    Case "spa"
                        ret = "Go to Next Sub-Task"
                End Select
            Case "cov257"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov258"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
            Case "cov259"
                Select Case lang
                    Case "eng"
                        ret = "Add An Original Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un Mode de D�faillance d`Origine � cette T�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Original-Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore originale a questa attivit�"
                    Case "spa"
                        ret = "Agregar Un Modo de Falla Original a esta Tarea "
                End Select
            Case "cov260"
                Select Case lang
                    Case "eng"
                        ret = "Remove An Original Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever un Mode de D�faillance d`Origine de cette T�che"
                    Case "ger"
                        ret = "Entferne aus dieser Aufgabe eine Original-Ausfallart"
                    Case "ita"
                        ret = "Elimina una modalit� errore iniziale da questa attivit�"
                    Case "spa"
                        ret = "Quitar Un Modo de Falla Original de esta Tarea "
                End Select
        End Select
        Return ret
    End Function
    Public Function getcomplibuploadimageovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov261"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Image"
                    Case "fre"
                        ret = "Supprimer cette image"
                    Case "ger"
                        ret = "Diese Abbildung l�schen"
                    Case "ita"
                        ret = "Elimina questa immagine"
                    Case "spa"
                        ret = "Anular Esta Imagen"
                End Select
            Case "cov262"
                Select Case lang
                    Case "eng"
                        ret = "Save Image Order"
                    Case "fre"
                        ret = "Sauvegarder l`ordre d`image"
                    Case "ger"
                        ret = "Abbildungs-Reihenfolge speichern"
                    Case "ita"
                        ret = "Salva ordine immagine"
                    Case "spa"
                        ret = "Guardar el Orden de Im�genes"
                End Select
            Case "cov263"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Image"
                    Case "fre"
                        ret = "Supprimer cette image"
                    Case "ger"
                        ret = "Diese Abbildung l�schen"
                    Case "ita"
                        ret = "Elimina questa immagine"
                    Case "spa"
                        ret = "Anular Esta Imagen"
                End Select
            Case "cov264"
                Select Case lang
                    Case "eng"
                        ret = "Save Image Order"
                    Case "fre"
                        ret = "Sauvegarder l`ordre d`image"
                    Case "ger"
                        ret = "Abbildungs-Reihenfolge speichern"
                    Case "ita"
                        ret = "Salva ordine immagine"
                    Case "spa"
                        ret = "Guardar el Orden de Im�genes"
                End Select
        End Select
        Return ret
    End Function
    Public Function getDeptArchovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov267"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov268"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov269"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getDevArchovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov3"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov4"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov5"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getDevArchtpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov149"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov150"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov151"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getdragndropnoovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov43"
                Select Case lang
                    Case "eng"
                        ret = "View Current Tasks for this Function"
                    Case "fre"
                        ret = "Afficher les t�ches actuelles de cette fonction"
                    Case "ger"
                        ret = "Aktuelle Aufgaben f�r diese Funktion anzeigen"
                    Case "ita"
                        ret = "Visualizza attivit� correnti per questa funzione"
                    Case "spa"
                        ret = "View Current Tasks for this Function"
                End Select
            Case "cov44"
                Select Case lang
                    Case "eng"
                        ret = "Expand this Component"
                    Case "fre"
                        ret = "D�velopper ce composant"
                    Case "ger"
                        ret = "Erweitern dieser Komponente"
                    Case "ita"
                        ret = "Espandi questo componente"
                    Case "spa"
                        ret = "Expand this Component"
                End Select
            Case "cov45"
                Select Case lang
                    Case "eng"
                        ret = "Add a Task to this Component"
                    Case "fre"
                        ret = "Ajouter une t�che � ce composant"
                    Case "ger"
                        ret = "Hinzuf�gen einer Aufgabe dieser Komponente"
                    Case "ita"
                        ret = "Aggiungi un attivit� a questo componente"
                    Case "spa"
                        ret = "Add a Task to this Component"
                End Select
            Case "cov46"
                Select Case lang
                    Case "eng"
                        ret = "View Current Tasks for this Component"
                    Case "fre"
                        ret = "Afficher les t�ches actuelles de ce composant"
                    Case "ger"
                        ret = "Aktuelle Aufgaben f�r diese Komponente anzeigen"
                    Case "ita"
                        ret = "Visualizza attivit� correnti per questo componente"
                    Case "spa"
                        ret = "View Current Tasks for this Component"
                End Select
            Case "cov47"
                Select Case lang
                    Case "eng"
                        ret = "No Tasks Available for this Component in this PM"
                    Case "fre"
                        ret = "Aucune t�che disponible pour ce composant dans ce PM"
                    Case "ger"
                        ret = "Keine Aufgaben f�r diese Komponente in diesem PM verf�gbar"
                    Case "ita"
                        ret = "Nessun attivit� disponibile per questo componente in questo PM"
                    Case "spa"
                        ret = "No Tasks Available for this Component in this PM"
                End Select
            Case "cov48"
                Select Case lang
                    Case "eng"
                        ret = "Add a Task to this Component"
                    Case "fre"
                        ret = "Ajouter une t�che � ce composant"
                    Case "ger"
                        ret = "Hinzuf�gen einer Aufgabe dieser Komponente"
                    Case "ita"
                        ret = "Aggiungi un attivit� a questo componente"
                    Case "spa"
                        ret = "Add a Task to this Component"
                End Select
            Case "cov49"
                Select Case lang
                    Case "eng"
                        ret = "View Current Tasks for this Component"
                    Case "fre"
                        ret = "Afficher les t�ches actuelles de ce composant"
                    Case "ger"
                        ret = "Aktuelle Aufgaben f�r diese Komponente anzeigen"
                    Case "ita"
                        ret = "Visualizza attivit� correnti per questo componente"
                    Case "spa"
                        ret = "View Current Tasks for this Component"
                End Select
            Case "cov50"
                Select Case lang
                    Case "eng"
                        ret = "Edit This Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
        End Select
        Return ret
    End Function
    Public Function getEQBotovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov270"
                Select Case lang
                    Case "eng"
                        ret = "Lock This Record For Editing"
                    Case "fre"
                        ret = "Verrouiller cet enregistrement pour modification"
                    Case "ger"
                        ret = "Sperre diese Eintragung f�r die Bearbeitung"
                    Case "ita"
                        ret = "Blocca la modifica di questo record"
                    Case "spa"
                        ret = "Bloquear Este Registro contra Ediciones"
                End Select
            Case "cov271"
                Select Case lang
                    Case "eng"
                        ret = "UnLock This Record For Editing by Other Users"
                    Case "fre"
                        ret = "D�verrouiller cet enregistrement pour qu`il soit modifi� par d`autres utilisateurs"
                    Case "ger"
                        ret = "Entsperren dieser Eintragung f�r die Bearbeitung durch andere Benutzer"
                    Case "ita"
                        ret = "Sblocca questo record affinch� gli altri utenti possano modificarlo"
                    Case "spa"
                        ret = "Desbloquear Este Registro Para Edici�n por Otros Usuarios"
                End Select
            Case "cov272"
                Select Case lang
                    Case "eng"
                        ret = "Save changes for this Equipment"
                    Case "fre"
                        ret = "Enregistrer les changements de cet �quipement"
                    Case "ger"
                        ret = "Speichern Sie die �nderungen f�r diese Ausr�stung"
                    Case "ita"
                        ret = "Salva modifiche per questa apparecchiatura"
                    Case "spa"
                        ret = "Guardar los cambios para este Equipo"
                End Select
            Case "cov273"
                Select Case lang
                    Case "eng"
                        ret = "Jump to Task Mode for this Equipment"
                    Case "fre"
                        ret = "Passer au mode de t�che de cet �quipement"
                    Case "ger"
                        ret = "Gehe zu Aufgaben-Modus f�r dieses Ger�t"
                    Case "ita"
                        ret = "Vai alla modalit� attivit� per questa apparecchiatura"
                    Case "spa"
                        ret = "Salte a `Modo de Tarea` para este Equipo"
                End Select
            Case "cov274"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Equipment Record"
                    Case "fre"
                        ret = "Supprimer cet enregistrement d`�quipement"
                    Case "ger"
                        ret = "L�schen der (Keine Vorschl�ge)"
                    Case "ita"
                        ret = "Elimina questo record"
                    Case "spa"
                        ret = "Delete this Equipment Record"
                End Select
        End Select
        Return ret
    End Function
    Public Function getequploadimageovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov275"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Image"
                    Case "fre"
                        ret = "Supprimer cette image"
                    Case "ger"
                        ret = "Diese Abbildung l�schen"
                    Case "ita"
                        ret = "Elimina questa immagine"
                    Case "spa"
                        ret = "Anular Esta Imagen"
                End Select
            Case "cov276"
                Select Case lang
                    Case "eng"
                        ret = "Save Image Order"
                    Case "fre"
                        ret = "Sauvegarder l`ordre d`image"
                    Case "ger"
                        ret = "Abbildungs-Reihenfolge speichern"
                    Case "ita"
                        ret = "Salva ordine immagine"
                    Case "spa"
                        ret = "Guardar el Orden de Im�genes"
                End Select
            Case "cov277"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Image"
                    Case "fre"
                        ret = "Supprimer cette image"
                    Case "ger"
                        ret = "Diese Abbildung l�schen"
                    Case "ita"
                        ret = "Elimina questa immagine"
                    Case "spa"
                        ret = "Anular Esta Imagen"
                End Select
            Case "cov278"
                Select Case lang
                    Case "eng"
                        ret = "Save Image Order"
                    Case "fre"
                        ret = "Sauvegarder l`ordre d`image"
                    Case "ger"
                        ret = "Abbildungs-Reihenfolge speichern"
                    Case "ita"
                        ret = "Salva ordine immagine"
                    Case "spa"
                        ret = "Guardar el Orden de Im�genes"
                End Select
        End Select
        Return ret
    End Function
    Public Function getFuncDivovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov279"
                Select Case lang
                    Case "eng"
                        ret = "Save changes for this Function"
                    Case "fre"
                        ret = "Enregistrer les changements de cette fonction"
                    Case "ger"
                        ret = "Speichern Sie die �nderungen f�r diese Funktion"
                    Case "ita"
                        ret = "Salva modifiche per questa funzione"
                    Case "spa"
                        ret = "Guardar los cambios para esta Funci�n"
                End Select
            Case "cov280"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Function"
                    Case "fre"
                        ret = "Supprimer cette fonction"
                    Case "ger"
                        ret = "L�schen Sie diese Funktion"
                    Case "ita"
                        ret = "Elimina questa funzione"
                    Case "spa"
                        ret = "Borrar esta Funci�n"
                End Select
            Case "cov281"
                Select Case lang
                    Case "eng"
                        ret = "Add/Edit Components for this Function"
                    Case "fre"
                        ret = "Ajouter/modifier les composants de cette fonction"
                    Case "ger"
                        ret = "Hinzuf�gen / Bearbeiten von Komponenten f�r diese Funktion"
                    Case "ita"
                        ret = "Aggiungi/Modifica componenti per questa funzione"
                    Case "spa"
                        ret = "Add/Edit Components for this Function"
                End Select
            Case "cov282"
                Select Case lang
                    Case "eng"
                        ret = "Jump to Task Mode for this Function"
                    Case "fre"
                        ret = "Passer au mode de t�che de cette fonction"
                    Case "ger"
                        ret = "Gehe zu Aufgaben-Modus f�r diese Funktion"
                    Case "ita"
                        ret = "Vai alla modalit� attivit� per questa funzione"
                    Case "spa"
                        ret = "Salte a `Modo de Tarea` para esta Funci�n"
                End Select
        End Select
        Return ret
    End Function
    Public Function getgtaskfailovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov6"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov7"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov8"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
        End Select
        Return ret
    End Function
    Public Function getGTasksFunctpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov152"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov153"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov154"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
        End Select
        Return ret
    End Function
    Public Function getJobPlanListovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov139"
                Select Case lang
                    Case "eng"
                        ret = "Print or Review This Job Plan"
                    Case "fre"
                        ret = "Imprimer ou revoir ce Plan de travail"
                    Case "ger"
                        ret = "Drucken oder �berpr�fung dieses Arbeitsplans"
                    Case "ita"
                        ret = "Stampa o Rivedi questo job plan"
                    Case "spa"
                        ret = "Print or Review This Job Plan"
                End Select
        End Select
        Return ret
    End Function
    Public Function getLocDetsovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov299"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Locations"
                    Case "fre"
                        ret = "Rechercher les localisations"
                    Case "ger"
                        ret = "Einsatzort-Suche"
                    Case "ita"
                        ret = "Ricerca posizioni"
                    Case "spa"
                        ret = "Buscar Ubicaciones "
                End Select
            Case "cov300"
                Select Case lang
                    Case "eng"
                        ret = "View This Location"
                    Case "fre"
                        ret = "Afficher cette localisation"
                    Case "ger"
                        ret = "Diesen Einsatzort anzeigen"
                    Case "ita"
                        ret = "Visualizza questa posizione"
                    Case "spa"
                        ret = "Ver Esta Ubicaci�n "
                End Select
            Case "cov301"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Locations"
                    Case "fre"
                        ret = "Rechercher les localisations"
                    Case "ger"
                        ret = "Einsatzort-Suche"
                    Case "ita"
                        ret = "Ricerca posizioni"
                    Case "spa"
                        ret = "Buscar Ubicaciones "
                End Select
            Case "cov302"
                Select Case lang
                    Case "eng"
                        ret = "View This Location"
                    Case "fre"
                        ret = "Afficher cette localisation"
                    Case "ger"
                        ret = "Diesen Einsatzort anzeigen"
                    Case "ita"
                        ret = "Visualizza questa posizione"
                    Case "spa"
                        ret = "Ver Esta Ubicaci�n "
                End Select
        End Select
        Return ret
    End Function
    Public Function getmmenu1ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov316"
                Select Case lang
                    Case "eng"
                        ret = "View Training Videos"
                    Case "fre"
                        ret = "Afficher les vid�os de formation"
                    Case "ger"
                        ret = "Training-Videos anzeigen"
                    Case "ita"
                        ret = "Visualizza video di formazione"
                    Case "spa"
                        ret = "View Training Videos"
                End Select
            Case "cov317"
                Select Case lang
                    Case "eng"
                        ret = "Log Out and Close Reliability Fusion"
                    Case "fre"
                        ret = "Se d�connecter et fermer Reliability Fusion"
                    Case "ger"
                        ret = "Logge aus und schlie�e die Fusion-Verl�sslichkeit"
                    Case "ita"
                        ret = "Esci e chiudi il software Reliability Fusion"
                    Case "spa"
                        ret = "Salir y Cerrar Confiabilidad Fusi�n"
                End Select
            Case "cov318"
                Select Case lang
                    Case "eng"
                        ret = "View Training Videos"
                    Case "fre"
                        ret = "Afficher les vid�os de formation"
                    Case "ger"
                        ret = "Training-Videos anzeigen"
                    Case "ita"
                        ret = "Visualizza video di formazione"
                    Case "spa"
                        ret = "View Training Vidoes"
                End Select
            Case "cov319"
                Select Case lang
                    Case "eng"
                        ret = "Change Menu Header and Background Settings"
                    Case "fre"
                        ret = "Modifier l`en-t�te du menu et les param�tres d`arri�re-plan"
                    Case "ger"
                        ret = "�ndern Sie die Kopf- und  Hintergrundeinstellungen des Men�s"
                    Case "ita"
                        ret = "Modifica men� iniziale e impostazioni dello sfondo"
                    Case "spa"
                        ret = "Change Menu Header and Background Settings"
                End Select
            Case "cov320"
                Select Case lang
                    Case "eng"
                        ret = "Log Out and Close Reliability Fusion"
                    Case "fre"
                        ret = "Se d�connecter et fermer Reliability Fusion"
                    Case "ger"
                        ret = "Logge aus und schlie�e die Fusion-Verl�sslichkeit"
                    Case "ita"
                        ret = "Esci e chiudi il software Reliability Fusion"
                    Case "spa"
                        ret = "Salir y Cerrar Confiabilidad Fusi�n"
                End Select
        End Select
        Return ret
    End Function
    Public Function getNCBotovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov283"
                Select Case lang
                    Case "eng"
                        ret = "Save changes for this Asset"
                    Case "fre"
                        ret = "Enregistrer les changements de cet actif"
                    Case "ger"
                        ret = "Speichern Sie die �nderungen f�r diese Anlage"
                    Case "ita"
                        ret = "Salva modifiche per questo asset"
                    Case "spa"
                        ret = "Save changes for this Asset"
                End Select
        End Select
        Return ret
    End Function
    Public Function getOptDevArchovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov92"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov93"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov94"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getOptDevArchtpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov116"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov117"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov118"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMApprovalEqovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov363"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getpmarchovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov26"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov27"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov28"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMArchManovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov60"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov61"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMArchMantpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov77"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov78"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMArchRTovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov140"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov141"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
        End Select
        Return ret
    End Function
    Public Function getpmarchtasksovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov29"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov30"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov31"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov32"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov33"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
        End Select
        Return ret
    End Function
    Public Function getpmarchtaskstpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov35"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov36"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov37"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov38"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov39"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
        End Select
        Return ret
    End Function
    Public Function getpmarchtpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov40"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov41"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov42"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMDivManovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov63"
                Select Case lang
                    Case "eng"
                        ret = "This PM is Past Due!"
                    Case "fre"
                        ret = "Ce PM a expir�!"
                    Case "ger"
                        ret = "Diese PM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo PM � scaduto!"
                    Case "spa"
                        ret = "This PM is Past Due!"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMDivMantpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov80"
                Select Case lang
                    Case "eng"
                        ret = "This PM is Past Due!"
                    Case "fre"
                        ret = "Ce PM a expir�!"
                    Case "ger"
                        ret = "Diese PM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo PM � scaduto!"
                    Case "spa"
                        ret = "This PM is Past Due!"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMFailManovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov64"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes and Return"
                    Case "fre"
                        ret = "Sauvegarder les modifications et revenir"
                    Case "ger"
                        ret = "�nderungen speichern und zur�ckkehren"
                    Case "ita"
                        ret = "Salva modifiche e ritorno"
                    Case "spa"
                        ret = "Guardar Cambios y Regresar "
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMGetPMManovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov65"
                Select Case lang
                    Case "eng"
                        ret = "No New Revisions for this Equipment Record"
                    Case "fre"
                        ret = "Aucune nouvelle r�vision pour cet enregistrement d`�quipement"
                    Case "ger"
                        ret = "Keine neuen Revisionen f�r diese (Keine Vorschl�ge)"
                    Case "ita"
                        ret = "Nessuna nuova revisione per questo record apparecchiatura"
                    Case "spa"
                        ret = "No New Revisions for this Equipment Record"
                End Select
            Case "cov66"
                Select Case lang
                    Case "eng"
                        ret = "A New Revision is Available for this Equipment Record"
                    Case "fre"
                        ret = "Une nouvelle r�vision est disponible pour cet enregistrement d`�quipement"
                    Case "ger"
                        ret = "Eine neue Version ist f�r diese (Keine Vorschl�ge) verf�gbar"
                    Case "ita"
                        ret = "Una nuova revisione � disponibile per questo record apparecchiatura"
                    Case "spa"
                        ret = "A New Revision is Available for this Equipment Record"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMGetPMMantpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov81"
                Select Case lang
                    Case "eng"
                        ret = "No New Revisions for this Equipment Record"
                    Case "fre"
                        ret = "Aucune nouvelle r�vision pour cet enregistrement d`�quipement"
                    Case "ger"
                        ret = "Keine neuen Revisionen f�r diese (Keine Vorschl�ge)"
                    Case "ita"
                        ret = "Nessuna nuova revisione per questo record apparecchiatura"
                    Case "spa"
                        ret = "No New Revisions for this Equipment Record"
                End Select
            Case "cov82"
                Select Case lang
                    Case "eng"
                        ret = "A New Revision is Available for this Equipment Record"
                    Case "fre"
                        ret = "Une nouvelle r�vision est disponible pour cet enregistrement d`�quipement"
                    Case "ger"
                        ret = "Eine neue Version ist f�r diese (Keine Vorschl�ge) verf�gbar"
                    Case "ita"
                        ret = "Una nuova revisione � disponibile per questo record apparecchiatura"
                    Case "spa"
                        ret = "A New Revision is Available for this Equipment Record"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMGridManovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov67"
                Select Case lang
                    Case "eng"
                        ret = "This PM is Past Due!"
                    Case "fre"
                        ret = "Ce PM a expir�!"
                    Case "ger"
                        ret = "Diese PM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo PM � scaduto!"
                    Case "spa"
                        ret = "This PM is Past Due!"
                End Select
            Case "cov68"
                Select Case lang
                    Case "eng"
                        ret = "PM Status OK"
                    Case "fre"
                        ret = "Statut PM OK"
                    Case "ger"
                        ret = "PM-Status OK"
                    Case "ita"
                        ret = "Stato PM OK"
                    Case "spa"
                        ret = "PM Status OK"
                End Select
            Case "cov69"
                Select Case lang
                    Case "eng"
                        ret = "This PM is Past Due!"
                    Case "fre"
                        ret = "Ce PM a expir�!"
                    Case "ger"
                        ret = "Diese PM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo PM � scaduto!"
                    Case "spa"
                        ret = "This PM is Past Due!"
                End Select
            Case "cov70"
                Select Case lang
                    Case "eng"
                        ret = "PM Status OK"
                    Case "fre"
                        ret = "Statut PM OK"
                    Case "ger"
                        ret = "PM-Status OK"
                    Case "ita"
                        ret = "Stato PM OK"
                    Case "spa"
                        ret = "PM Status OK"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMGridMantpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov83"
                Select Case lang
                    Case "eng"
                        ret = "This TPM is Past Due!"
                    Case "fre"
                        ret = "Ce TPM a expir�!"
                    Case "ger"
                        ret = "Das TPM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo TPM � scaduto!"
                    Case "spa"
                        ret = "This TPM is Past Due!"
                End Select
            Case "cov84"
                Select Case lang
                    Case "eng"
                        ret = "TPM Status OK"
                    Case "fre"
                        ret = "Statut TPM OK"
                    Case "ger"
                        ret = "TPM Status OK"
                    Case "ita"
                        ret = "Stato TPM OK"
                    Case "spa"
                        ret = "TPM Status OK"
                End Select
            Case "cov85"
                Select Case lang
                    Case "eng"
                        ret = "This TPM is Past Due!"
                    Case "fre"
                        ret = "Ce TPM a expir�!"
                    Case "ger"
                        ret = "Das TPM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo TPM � scaduto!"
                    Case "spa"
                        ret = "This TPM is Past Due!"
                End Select
            Case "cov86"
                Select Case lang
                    Case "eng"
                        ret = "TPM Status OK"
                    Case "fre"
                        ret = "Statut TPM OK"
                    Case "ger"
                        ret = "TPM Status OK"
                    Case "ita"
                        ret = "Stato TPM OK"
                    Case "spa"
                        ret = "TPM Status OK"
                End Select
        End Select
        Return ret
    End Function
    Public Function getpmlocmainovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov303"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Locations"
                    Case "fre"
                        ret = "Rechercher les localisations"
                    Case "ger"
                        ret = "Einsatzort-Suche"
                    Case "ita"
                        ret = "Ricerca posizioni"
                    Case "spa"
                        ret = "Buscar Ubicaciones "
                End Select
            Case "cov304"
                Select Case lang
                    Case "eng"
                        ret = "View This Location"
                    Case "fre"
                        ret = "Afficher cette localisation"
                    Case "ger"
                        ret = "Diesen Einsatzort anzeigen"
                    Case "ita"
                        ret = "Visualizza questa posizione"
                    Case "spa"
                        ret = "Ver Esta Ubicaci�n "
                End Select
            Case "cov305"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Locations"
                    Case "fre"
                        ret = "Rechercher les localisations"
                    Case "ger"
                        ret = "Einsatzort-Suche"
                    Case "ita"
                        ret = "Ricerca posizioni"
                    Case "spa"
                        ret = "Buscar Ubicaciones "
                End Select
            Case "cov306"
                Select Case lang
                    Case "eng"
                        ret = "View This Location"
                    Case "fre"
                        ret = "Afficher cette localisation"
                    Case "ger"
                        ret = "Diesen Einsatzort anzeigen"
                    Case "ita"
                        ret = "Visualizza questa posizione"
                    Case "spa"
                        ret = "Ver Esta Ubicaci�n "
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMMainManovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov71"
                Select Case lang
                    Case "eng"
                        ret = "Apply Filter Selections"
                    Case "fre"
                        ret = "Appliquer des s�lections de filtre"
                    Case "ger"
                        ret = "Filterauswahl anwenden"
                    Case "ita"
                        ret = "Applica selezioni filtro"
                    Case "spa"
                        ret = "Aplique las Selecciones del Filtro"
                End Select
            Case "cov72"
                Select Case lang
                    Case "eng"
                        ret = "This PM is Past Due!"
                    Case "fre"
                        ret = "Ce PM a expir�!"
                    Case "ger"
                        ret = "Diese PM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo PM � scaduto!"
                    Case "spa"
                        ret = "This PM is Past Due!"
                End Select
            Case "cov73"
                Select Case lang
                    Case "eng"
                        ret = "PM Status OK"
                    Case "fre"
                        ret = "Statut PM OK"
                    Case "ger"
                        ret = "PM-Status OK"
                    Case "ita"
                        ret = "Stato PM OK"
                    Case "spa"
                        ret = "PM Status OK"
                End Select
            Case "cov74"
                Select Case lang
                    Case "eng"
                        ret = "This PM is Past Due!"
                    Case "fre"
                        ret = "Ce PM a expir�!"
                    Case "ger"
                        ret = "Diese PM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo PM � scaduto!"
                    Case "spa"
                        ret = "This PM is Past Due!"
                End Select
            Case "cov75"
                Select Case lang
                    Case "eng"
                        ret = "PM Status OK"
                    Case "fre"
                        ret = "Statut PM OK"
                    Case "ger"
                        ret = "PM-Status OK"
                    Case "ita"
                        ret = "Stato PM OK"
                    Case "spa"
                        ret = "PM Status OK"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMMainMantpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov87"
                Select Case lang
                    Case "eng"
                        ret = "Apply Filter Selections"
                    Case "fre"
                        ret = "Appliquer des s�lections de filtre"
                    Case "ger"
                        ret = "Filterauswahl anwenden"
                    Case "ita"
                        ret = "Applica selezioni filtro"
                    Case "spa"
                        ret = "Aplique las Selecciones del Filtro"
                End Select
            Case "cov88"
                Select Case lang
                    Case "eng"
                        ret = "This TPM is Past Due!"
                    Case "fre"
                        ret = "Ce TPM a expir�!"
                    Case "ger"
                        ret = "Das TPM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo TPM � scaduto!"
                    Case "spa"
                        ret = "This TPM is Past Due!"
                End Select
            Case "cov89"
                Select Case lang
                    Case "eng"
                        ret = "TPM Status OK"
                    Case "fre"
                        ret = "Statut TPM OK"
                    Case "ger"
                        ret = "TPM Status OK"
                    Case "ita"
                        ret = "Stato TPM OK"
                    Case "spa"
                        ret = "TPM Status OK"
                End Select
            Case "cov90"
                Select Case lang
                    Case "eng"
                        ret = "This TPM is Past Due!"
                    Case "fre"
                        ret = "Ce TPM a expir�!"
                    Case "ger"
                        ret = "Das TPM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo TPM � scaduto!"
                    Case "spa"
                        ret = "This TPM is Past Due!"
                End Select
            Case "cov91"
                Select Case lang
                    Case "eng"
                        ret = "TPM Status OK"
                    Case "fre"
                        ret = "Statut TPM OK"
                    Case "ger"
                        ret = "TPM Status OK"
                    Case "ita"
                        ret = "Stato TPM OK"
                    Case "spa"
                        ret = "TPM Status OK"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMOptRationale2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov95"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes and Return"
                    Case "fre"
                        ret = "Sauvegarder les modifications et revenir"
                    Case "ger"
                        ret = "�nderungen speichern und zur�ckkehren"
                    Case "ita"
                        ret = "Salva modifiche e ritorno"
                    Case "spa"
                        ret = "Guardar Cambios y Regresar "
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMOptRationaleTPM2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov119"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes and Return"
                    Case "fre"
                        ret = "Sauvegarder les modifications et revenir"
                    Case "ger"
                        ret = "�nderungen speichern und zur�ckkehren"
                    Case "ita"
                        ret = "Salva modifiche e ritorno"
                    Case "spa"
                        ret = "Guardar Cambios y Regresar "
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMOptTasksovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov100"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov101"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov102"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov103"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov104"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov105"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov106"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov107"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov108"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov109"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov110"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Sub-Task"
                    Case "fre"
                        ret = "Aller � la sous-t�che pr�c�dente"
                    Case "ger"
                        ret = "Zur vorherigen Unteraufgabe"
                    Case "ita"
                        ret = "Vai al sotto attivit� precedente"
                    Case "spa"
                        ret = "Go to Previous Sub-Task"
                End Select
            Case "cov111"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Sub-Task"
                    Case "fre"
                        ret = "Aller � la sous-t�che suivante"
                    Case "ger"
                        ret = "Zur n�chsten Unteraufgabe"
                    Case "ita"
                        ret = "Vai al sotto attivit� successivo"
                    Case "spa"
                        ret = "Go to Next Sub-Task"
                End Select
            Case "cov112"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov113"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
            Case "cov114"
                Select Case lang
                    Case "eng"
                        ret = "Add An Original Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un Mode de D�faillance d`Origine � cette T�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Original-Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore originale a questa attivit�"
                    Case "spa"
                        ret = "Agregar Un Modo de Falla Original a esta Tarea "
                End Select
            Case "cov115"
                Select Case lang
                    Case "eng"
                        ret = "Remove An Original Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever un Mode de D�faillance d`Origine de cette T�che"
                    Case "ger"
                        ret = "Entferne aus dieser Aufgabe eine Original-Ausfallart"
                    Case "ita"
                        ret = "Elimina una modalit� errore iniziale da questa attivit�"
                    Case "spa"
                        ret = "Quitar Un Modo de Falla Original de esta Tarea "
                End Select
            Case "cov96"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
            Case "cov97"
                Select Case lang
                    Case "eng"
                        ret = "Send Task to Operator Care Module (TPM)"
                    Case "fre"
                        ret = "Envoyer la t�che au module de contr�le de l`op�rateur (MPT)"
                    Case "ger"
                        ret = "Aufgabe an Bediener-Wartungs-Modul senden (TPM)"
                    Case "ita"
                        ret = "Invia attivit� al modulo assistenza operatore (TPM)"
                    Case "spa"
                        ret = "Env�e la Tarea a M�dulo de Cuidado por el Operador (Aut�nomo - TPM)"
                End Select
            Case "cov98"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov99"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMRouteListovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov143"
                Select Case lang
                    Case "eng"
                        ret = "Print This Route"
                    Case "fre"
                        ret = "Imprimer cet itin�raire"
                    Case "ger"
                        ret = "Drucken Sie diese Route"
                    Case "ita"
                        ret = "Stampa questo percorso"
                    Case "spa"
                        ret = "Print This Route"
                End Select
            Case "cov144"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Route"
                    Case "fre"
                        ret = "Supprimer cet itin�raire"
                    Case "ger"
                        ret = "Diese Route l�schen"
                    Case "ita"
                        ret = "Elimina questo percorso"
                    Case "spa"
                        ret = "Delete This Route"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMRoutes2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov145"
                Select Case lang
                    Case "eng"
                        ret = "Start Over"
                    Case "fre"
                        ret = "Recommencer"
                    Case "ger"
                        ret = "Noch einmal starten"
                    Case "ita"
                        ret = "Ricomincia"
                    Case "spa"
                        ret = "Start Over"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMSuperSelectovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov296"
                Select Case lang
                    Case "eng"
                        ret = "Add Lead Craft"
                    Case "fre"
                        ret = "Ajouter un m�tier principal"
                    Case "ger"
                        ret = "Handwerk hinzuf�gen"
                    Case "ita"
                        ret = "Aggiungi abilit� primaria"
                    Case "spa"
                        ret = "Add Lead Craft"
                End Select
            Case "cov297"
                Select Case lang
                    Case "eng"
                        ret = "Add Supervisor"
                    Case "fre"
                        ret = "Ajouter un superviseur"
                    Case "ger"
                        ret = "Vorgesetzter hinzuf�gen"
                    Case "ita"
                        ret = "Aggiungi supervisore"
                    Case "spa"
                        ret = "Add Supervisor"
                End Select
            Case "cov298"
                Select Case lang
                    Case "eng"
                        ret = "Add Planner"
                    Case "fre"
                        ret = "Ajouter un planificateur"
                    Case "ger"
                        ret = "Planer hinzuf�gen"
                    Case "ita"
                        ret = "Aggiungi pianificatore"
                    Case "spa"
                        ret = "Add Planner"
                End Select
        End Select
        Return ret
    End Function
    Public Function getPMTaskDivFuncovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov10"
                Select Case lang
                    Case "eng"
                        ret = "Send Task to Operator Care Module (TPM)"
                    Case "fre"
                        ret = "Envoyer la t�che au module de contr�le de l`op�rateur (MPT)"
                    Case "ger"
                        ret = "Aufgabe an Bediener-Wartungs-Modul senden (TPM)"
                    Case "ita"
                        ret = "Invia attivit� al modulo assistenza operatore (TPM)"
                    Case "spa"
                        ret = "Env�e la Tarea a M�dulo de Cuidado por el Operador (Aut�nomo - TPM)"
                End Select
            Case "cov11"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov12"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov13"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov14"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov15"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov16"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov17"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov18"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov19"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov20"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov21"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov22"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov23"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov24"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
            Case "cov9"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
        End Select
        Return ret
    End Function
    Public Function getpmuploadimageovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov321"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Image"
                    Case "fre"
                        ret = "Supprimer cette image"
                    Case "ger"
                        ret = "Diese Abbildung l�schen"
                    Case "ita"
                        ret = "Elimina questa immagine"
                    Case "spa"
                        ret = "Anular Esta Imagen"
                End Select
            Case "cov322"
                Select Case lang
                    Case "eng"
                        ret = "Save Image Order"
                    Case "fre"
                        ret = "Sauvegarder l`ordre d`image"
                    Case "ger"
                        ret = "Abbildungs-Reihenfolge speichern"
                    Case "ita"
                        ret = "Salva ordine immagine"
                    Case "spa"
                        ret = "Guardar el Orden de Im�genes"
                End Select
            Case "cov323"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Image"
                    Case "fre"
                        ret = "Supprimer cette image"
                    Case "ger"
                        ret = "Diese Abbildung l�schen"
                    Case "ita"
                        ret = "Elimina questa immagine"
                    Case "spa"
                        ret = "Anular Esta Imagen"
                End Select
            Case "cov324"
                Select Case lang
                    Case "eng"
                        ret = "Save Image Order"
                    Case "fre"
                        ret = "Sauvegarder l`ordre d`image"
                    Case "ger"
                        ret = "Abbildungs-Reihenfolge speichern"
                    Case "ita"
                        ret = "Salva ordine immagine"
                    Case "spa"
                        ret = "Guardar el Orden de Im�genes"
                End Select
        End Select
        Return ret
    End Function
    Public Function getreorderdetailsovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov288"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Precaution"
                    Case "fre"
                        ret = "Rechercher la pr�caution"
                    Case "ger"
                        ret = "Suche nach Sicherheitsvorkehrungen"
                    Case "ita"
                        ret = "Ricerca precauzioni"
                    Case "spa"
                        ret = "Buscar Precauci�n "
                End Select
            Case "cov289"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Precaution"
                    Case "fre"
                        ret = "Rechercher la pr�caution"
                    Case "ger"
                        ret = "Suche nach Sicherheitsvorkehrungen"
                    Case "ita"
                        ret = "Ricerca precauzioni"
                    Case "spa"
                        ret = "Buscar Precauci�n "
                End Select
        End Select
        Return ret
    End Function
    Public Function getScheduledTasksovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov146"
                Select Case lang
                    Case "eng"
                        ret = "Apply Filter Selections"
                    Case "fre"
                        ret = "Appliquer des s�lections de filtre"
                    Case "ger"
                        ret = "Filterauswahl anwenden"
                    Case "ita"
                        ret = "Applica selezioni filtro"
                    Case "spa"
                        ret = "Aplique las Selecciones del Filtro"
                End Select
        End Select
        Return ret
    End Function
    Public Function getSiteAssetsovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov307"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov308"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
        End Select
        Return ret
    End Function
    Public Function getsiteassets2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov310"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov311"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
        End Select
        Return ret
    End Function
    Public Function getsiteassets3ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov313"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov314"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
        End Select
        Return ret
    End Function
    Public Function getsparepartpicharchtpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov290"
                Select Case lang
                    Case "eng"
                        ret = "Add Spare Parts to Current Equipment Record"
                    Case "fre"
                        ret = "Ajouter des pi�ces de rechange � l`enregistrement d`�quipement actuel"
                    Case "ger"
                        ret = "Ersatzteile auf aktuelle (Keine Vorschl�ge) hinzuf�gen"
                    Case "ita"
                        ret = "Aggiungi parti di ricambio ai recordi di apparecchiatura correnti"
                    Case "spa"
                        ret = "Add Spare Parts to Current Equipment Record"
                End Select
        End Select
        Return ret
    End Function
    Public Function getsparepartpickovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov291"
                Select Case lang
                    Case "eng"
                        ret = "Add Spare Parts to Current Equipment Record"
                    Case "fre"
                        ret = "Ajouter des pi�ces de rechange � l`enregistrement d`�quipement actuel"
                    Case "ger"
                        ret = "Ersatzteile auf aktuelle (Keine Vorschl�ge) hinzuf�gen"
                    Case "ita"
                        ret = "Aggiungi parti di ricambio ai recordi di apparecchiatura correnti"
                    Case "spa"
                        ret = "Add Spare Parts to Current Equipment Record"
                End Select
        End Select
        Return ret
    End Function
    Public Function getsparepartpickarchovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov292"
                Select Case lang
                    Case "eng"
                        ret = "Add Spare Parts to Current Equipment Record"
                    Case "fre"
                        ret = "Ajouter des pi�ces de rechange � l`enregistrement d`�quipement actuel"
                    Case "ger"
                        ret = "Ersatzteile auf aktuelle (Keine Vorschl�ge) hinzuf�gen"
                    Case "ita"
                        ret = "Aggiungi parti di ricambio ai recordi di apparecchiatura correnti"
                    Case "spa"
                        ret = "Add Spare Parts to Current Equipment Record"
                End Select
        End Select
        Return ret
    End Function
    Public Function getsparepartpicktpmovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov293"
                Select Case lang
                    Case "eng"
                        ret = "Add Spare Parts to Current Equipment Record"
                    Case "fre"
                        ret = "Ajouter des pi�ces de rechange � l`enregistrement d`�quipement actuel"
                    Case "ger"
                        ret = "Ersatzteile auf aktuelle (Keine Vorschl�ge) hinzuf�gen"
                    Case "ita"
                        ret = "Aggiungi parti di ricambio ai recordi di apparecchiatura correnti"
                    Case "spa"
                        ret = "Add Spare Parts to Current Equipment Record"
                End Select
        End Select
        Return ret
    End Function
    Public Function getstoreroomovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov294"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Precaution"
                    Case "fre"
                        ret = "Rechercher la pr�caution"
                    Case "ger"
                        ret = "Suche nach Sicherheitsvorkehrungen"
                    Case "ita"
                        ret = "Ricerca precauzioni"
                    Case "spa"
                        ret = "Buscar Precauci�n "
                End Select
            Case "cov295"
                Select Case lang
                    Case "eng"
                        ret = "Lookup Precaution"
                    Case "fre"
                        ret = "Rechercher la pr�caution"
                    Case "ger"
                        ret = "Suche nach Sicherheitsvorkehrungen"
                    Case "ita"
                        ret = "Ricerca precauzioni"
                    Case "spa"
                        ret = "Buscar Precauci�n "
                End Select
        End Select
        Return ret
    End Function
    Public Function gettaskedit2ovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov51"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov52"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov53"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov54"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov55"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov56"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov57"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
            Case "cov58"
                Select Case lang
                    Case "eng"
                        ret = "Add An Original Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un Mode de D�faillance d`Origine � cette T�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Original-Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore originale a questa attivit�"
                    Case "spa"
                        ret = "Agregar Un Modo de Falla Original a esta Tarea "
                End Select
            Case "cov59"
                Select Case lang
                    Case "eng"
                        ret = "Remove An Original Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever un Mode de D�faillance d`Origine de cette T�che"
                    Case "ger"
                        ret = "Entferne aus dieser Aufgabe eine Original-Ausfallart"
                    Case "ita"
                        ret = "Elimina una modalit� errore iniziale da questa attivit�"
                    Case "spa"
                        ret = "Quitar Un Modo de Falla Original de esta Tarea "
                End Select
        End Select
        Return ret
    End Function
    Public Function gettpmarchovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov325"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov326"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov327"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov328"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov329"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov330"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function gettpmgridovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov331"
                Select Case lang
                    Case "eng"
                        ret = "This TPM is Past Due!"
                    Case "fre"
                        ret = "Ce TPM a expir�!"
                    Case "ger"
                        ret = "Das TPM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo TPM � scaduto!"
                    Case "spa"
                        ret = "This TPM is Past Due!"
                End Select
            Case "cov332"
                Select Case lang
                    Case "eng"
                        ret = "TPM Status OK"
                    Case "fre"
                        ret = "Statut TPM OK"
                    Case "ger"
                        ret = "TPM Status OK"
                    Case "ita"
                        ret = "Stato TPM OK"
                    Case "spa"
                        ret = "TPM Status OK"
                End Select
            Case "cov333"
                Select Case lang
                    Case "eng"
                        ret = "This TPM is Past Due!"
                    Case "fre"
                        ret = "Ce TPM a expir�!"
                    Case "ger"
                        ret = "Das TPM ist bereits f�llig!"
                    Case "ita"
                        ret = "Questo TPM � scaduto!"
                    Case "spa"
                        ret = "This TPM is Past Due!"
                End Select
            Case "cov334"
                Select Case lang
                    Case "eng"
                        ret = "TPM Status OK"
                    Case "fre"
                        ret = "Statut TPM OK"
                    Case "ger"
                        ret = "TPM Status OK"
                    Case "ita"
                        ret = "Stato TPM OK"
                    Case "spa"
                        ret = "TPM Status OK"
                End Select
        End Select
        Return ret
    End Function
    Public Function gettpmopttasksovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov120"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
            Case "cov121"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov122"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov123"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov124"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov125"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov126"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov127"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov128"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov129"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov130"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov131"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov132"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov133"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Sub-Task"
                    Case "fre"
                        ret = "Aller � la sous-t�che pr�c�dente"
                    Case "ger"
                        ret = "Zur vorherigen Unteraufgabe"
                    Case "ita"
                        ret = "Vai al sotto attivit� precedente"
                    Case "spa"
                        ret = "Go to Previous Sub-Task"
                End Select
            Case "cov134"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Sub-Task"
                    Case "fre"
                        ret = "Aller � la sous-t�che suivante"
                    Case "ger"
                        ret = "Zur n�chsten Unteraufgabe"
                    Case "ita"
                        ret = "Vai al sotto attivit� successivo"
                    Case "spa"
                        ret = "Go to Next Sub-Task"
                End Select
            Case "cov135"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov136"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
            Case "cov137"
                Select Case lang
                    Case "eng"
                        ret = "Add An Original Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un Mode de D�faillance d`Origine � cette T�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Original-Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore originale a questa attivit�"
                    Case "spa"
                        ret = "Agregar Un Modo de Falla Original a esta Tarea "
                End Select
            Case "cov138"
                Select Case lang
                    Case "eng"
                        ret = "Remove An Original Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever un Mode de D�faillance d`Origine de cette T�che"
                    Case "ger"
                        ret = "Entferne aus dieser Aufgabe eine Original-Ausfallart"
                    Case "ita"
                        ret = "Elimina una modalit� errore iniziale da questa attivit�"
                    Case "spa"
                        ret = "Quitar Un Modo de Falla Original de esta Tarea "
                End Select
        End Select
        Return ret
    End Function
    Public Function gettpmtaskovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov335"
                Select Case lang
                    Case "eng"
                        ret = "Failure Modes Provided for this Task"
                    Case "fre"
                        ret = "Modes de d�faillance fournis pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe vorgesehene Ausfallarten"
                    Case "ita"
                        ret = "Modalit� errore fornite per questa attivit�"
                    Case "spa"
                        ret = "Modos de Falla provistos para esta Tarea "
                End Select
            Case "cov336"
                Select Case lang
                    Case "eng"
                        ret = "No Failure Modes Provided for this Task"
                    Case "fre"
                        ret = "Aucun mode de d�faillance fourni pour cette t�che"
                    Case "ger"
                        ret = "Keine f�r diese Aufgabe vorgesehenen Ausfallarten"
                    Case "ita"
                        ret = "Nessuna modalit� di errore fornita per questa attivit�"
                    Case "spa"
                        ret = "Ning�n Modo de Falla Provisto para esta Tarea "
                End Select
            Case "cov337"
                Select Case lang
                    Case "eng"
                        ret = "Lubricants Required for this Task"
                    Case "fre"
                        ret = "Lubrifiants obligatoires pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe erforderliche Schmiermittel"
                    Case "ita"
                        ret = "Lubrificanti necessari per questa attivit�"
                    Case "spa"
                        ret = "Lubricantes Requeridos para esta Tarea "
                End Select
            Case "cov338"
                Select Case lang
                    Case "eng"
                        ret = "No Lubricants Required for this Task"
                    Case "fre"
                        ret = "Aucun lubrifiant obligatoire pour cette t�che"
                    Case "ger"
                        ret = "Keine f�r diese Aufgabe erforderlichen Schmiermittel"
                    Case "ita"
                        ret = "Non sono richiesti lubrificanti per questa attivit�"
                    Case "spa"
                        ret = "Ning�n Lubricante Requirido para esta Tarea "
                End Select
            Case "cov339"
                Select Case lang
                    Case "eng"
                        ret = "Tools Required for this Task"
                    Case "fre"
                        ret = "Outils obligatoires pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe erforderliche Werkzeuge"
                    Case "ita"
                        ret = "Strumenti necessari per questa attivit�"
                    Case "spa"
                        ret = "Herramientas Requeridas para esta Tarea "
                End Select
            Case "cov340"
                Select Case lang
                    Case "eng"
                        ret = "No Tools Required for this Task"
                    Case "fre"
                        ret = "Aucun outil obligatoire pour cette t�che"
                    Case "ger"
                        ret = "Keine Werkzeuge f�r diese Aufgabe erforderlich"
                    Case "ita"
                        ret = "Non sono necessari Strumenti per questa attivit�"
                    Case "spa"
                        ret = "Ninguna Herramienta Requerida para esta Tarea "
                End Select
            Case "cov341"
                Select Case lang
                    Case "eng"
                        ret = "Parts Required for this Task"
                    Case "fre"
                        ret = "Pi�ces obligatoires pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe erforderliche Teile"
                    Case "ita"
                        ret = "Parti necessarie per questa attivit�"
                    Case "spa"
                        ret = "Partes Requeridas para esta Tarea "
                End Select
            Case "cov342"
                Select Case lang
                    Case "eng"
                        ret = "No Parts Required for this Task"
                    Case "fre"
                        ret = "Aucune pi�ce obligatoire pour cette t�che"
                    Case "ger"
                        ret = "Keine Teile f�r diese Aufgabe erforderlich"
                    Case "ita"
                        ret = "Non sono necessarie Parti per questa attivit�"
                    Case "spa"
                        ret = "Ninguna Parte Requerida para esta Tarea "
                End Select
            Case "cov343"
                Select Case lang
                    Case "eng"
                        ret = "Measurements Required for this Task"
                    Case "fre"
                        ret = "Mesures obligatoires pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe erforderliche Messungen"
                    Case "ita"
                        ret = "Misurazioni necessarie per questa attivit�"
                    Case "spa"
                        ret = "Mediciones Requeridas para esta Tarea "
                End Select
            Case "cov344"
                Select Case lang
                    Case "eng"
                        ret = "No Measurements Required for this Task"
                    Case "fre"
                        ret = "Aucune mesure obligatoire pour cette t�che"
                    Case "ger"
                        ret = "Keine f�r diese Aufgabe erforderlichen Messungen"
                    Case "ita"
                        ret = "Non sono richieste misurazioni per questa attivit�"
                    Case "spa"
                        ret = "Ninguna Medici�n Requerida para esta Tarea "
                End Select
            Case "cov345"
                Select Case lang
                    Case "eng"
                        ret = "No Sub Tasks for this Task"
                    Case "fre"
                        ret = "Aucune sous-t�che pour cette t�che"
                    Case "ger"
                        ret = "Keine Unteraufgaben f�r diese Aufgabe"
                    Case "ita"
                        ret = "Non ci sono sottoattivit� per questa attivit�"
                    Case "spa"
                        ret = "Ninguna Sub-Tarea para esta Tarea "
                End Select
            Case "cov346"
                Select Case lang
                    Case "eng"
                        ret = "View Sub Tasks for this Task"
                    Case "fre"
                        ret = "Afficher les sous-t�ches pour cette t�che"
                    Case "ger"
                        ret = "Unteraufgaben f�r diese Aufgabe anzeigen"
                    Case "ita"
                        ret = "Visualizza sottoattivit� per questa attivit�"
                    Case "spa"
                        ret = "Ver las Sub-Tareas para esta Tarea "
                End Select
            Case "cov347"
                Select Case lang
                    Case "eng"
                        ret = "Failure Modes Provided for this Task"
                    Case "fre"
                        ret = "Modes de d�faillance fournis pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe vorgesehene Ausfallarten"
                    Case "ita"
                        ret = "Modalit� errore fornite per questa attivit�"
                    Case "spa"
                        ret = "Modos de Falla provistos para esta Tarea "
                End Select
            Case "cov348"
                Select Case lang
                    Case "eng"
                        ret = "No Failure Modes Provided for this Task"
                    Case "fre"
                        ret = "Aucun mode de d�faillance fourni pour cette t�che"
                    Case "ger"
                        ret = "Keine f�r diese Aufgabe vorgesehenen Ausfallarten"
                    Case "ita"
                        ret = "Nessuna modalit� di errore fornita per questa attivit�"
                    Case "spa"
                        ret = "Ning�n Modo de Falla Provisto para esta Tarea "
                End Select
            Case "cov349"
                Select Case lang
                    Case "eng"
                        ret = "Lubricants Required for this Task"
                    Case "fre"
                        ret = "Lubrifiants obligatoires pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe erforderliche Schmiermittel"
                    Case "ita"
                        ret = "Lubrificanti necessari per questa attivit�"
                    Case "spa"
                        ret = "Lubricantes Requeridos para esta Tarea "
                End Select
            Case "cov350"
                Select Case lang
                    Case "eng"
                        ret = "No Lubricants Required for this Task"
                    Case "fre"
                        ret = "Aucun lubrifiant obligatoire pour cette t�che"
                    Case "ger"
                        ret = "Keine f�r diese Aufgabe erforderlichen Schmiermittel"
                    Case "ita"
                        ret = "Non sono richiesti lubrificanti per questa attivit�"
                    Case "spa"
                        ret = "Ning�n Lubricante Requirido para esta Tarea "
                End Select
            Case "cov351"
                Select Case lang
                    Case "eng"
                        ret = "Tools Required for this Task"
                    Case "fre"
                        ret = "Outils obligatoires pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe erforderliche Werkzeuge"
                    Case "ita"
                        ret = "Strumenti necessari per questa attivit�"
                    Case "spa"
                        ret = "Herramientas Requeridas para esta Tarea "
                End Select
            Case "cov352"
                Select Case lang
                    Case "eng"
                        ret = "No Tools Required for this Task"
                    Case "fre"
                        ret = "Aucun outil obligatoire pour cette t�che"
                    Case "ger"
                        ret = "Keine Werkzeuge f�r diese Aufgabe erforderlich"
                    Case "ita"
                        ret = "Non sono necessari Strumenti per questa attivit�"
                    Case "spa"
                        ret = "Ninguna Herramienta Requerida para esta Tarea "
                End Select
            Case "cov353"
                Select Case lang
                    Case "eng"
                        ret = "Parts Required for this Task"
                    Case "fre"
                        ret = "Pi�ces obligatoires pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe erforderliche Teile"
                    Case "ita"
                        ret = "Parti necessarie per questa attivit�"
                    Case "spa"
                        ret = "Partes Requeridas para esta Tarea "
                End Select
            Case "cov354"
                Select Case lang
                    Case "eng"
                        ret = "No Parts Required for this Task"
                    Case "fre"
                        ret = "Aucune pi�ce obligatoire pour cette t�che"
                    Case "ger"
                        ret = "Keine Teile f�r diese Aufgabe erforderlich"
                    Case "ita"
                        ret = "Non sono necessarie Parti per questa attivit�"
                    Case "spa"
                        ret = "Ninguna Parte Requerida para esta Tarea "
                End Select
            Case "cov355"
                Select Case lang
                    Case "eng"
                        ret = "Measurements Required for this Task"
                    Case "fre"
                        ret = "Mesures obligatoires pour cette t�che"
                    Case "ger"
                        ret = "F�r diese Aufgabe erforderliche Messungen"
                    Case "ita"
                        ret = "Misurazioni necessarie per questa attivit�"
                    Case "spa"
                        ret = "Mediciones Requeridas para esta Tarea "
                End Select
            Case "cov356"
                Select Case lang
                    Case "eng"
                        ret = "No Measurements Required for this Task"
                    Case "fre"
                        ret = "Aucune mesure obligatoire pour cette t�che"
                    Case "ger"
                        ret = "Keine f�r diese Aufgabe erforderlichen Messungen"
                    Case "ita"
                        ret = "Non sono richieste misurazioni per questa attivit�"
                    Case "spa"
                        ret = "Ninguna Medici�n Requerida para esta Tarea "
                End Select
            Case "cov357"
                Select Case lang
                    Case "eng"
                        ret = "No Sub Tasks for this Task"
                    Case "fre"
                        ret = "Aucune sous-t�che pour cette t�che"
                    Case "ger"
                        ret = "Keine Unteraufgaben f�r diese Aufgabe"
                    Case "ita"
                        ret = "Non ci sono sottoattivit� per questa attivit�"
                    Case "spa"
                        ret = "Ninguna Sub-Tarea para esta Tarea "
                End Select
            Case "cov358"
                Select Case lang
                    Case "eng"
                        ret = "View Sub Tasks for this Task"
                    Case "fre"
                        ret = "Afficher les sous-t�ches pour cette t�che"
                    Case "ger"
                        ret = "Unteraufgaben f�r diese Aufgabe anzeigen"
                    Case "ita"
                        ret = "Visualizza sottoattivit� per questa attivit�"
                    Case "spa"
                        ret = "Ver las Sub-Tareas para esta Tarea "
                End Select
        End Select
        Return ret
    End Function
    Public Function gettpmtasksovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov155"
                Select Case lang
                    Case "eng"
                        ret = "Save Changes for this Task"
                    Case "fre"
                        ret = "Sauvegarder les Changements de cette T�che"
                    Case "ger"
                        ret = "�nderungen f�r diese Aufgabe speichern"
                    Case "ita"
                        ret = "Salva le modifiche per questa attivit�"
                    Case "spa"
                        ret = "Guardar los Cambios para esta Tarea"
                End Select
            Case "cov156"
                Select Case lang
                    Case "eng"
                        ret = "Delete this Task"
                    Case "fre"
                        ret = "Supprimer cette t�che"
                    Case "ger"
                        ret = "Diese Aufgabe l�schen"
                    Case "ita"
                        ret = "Elimina questa attivit�"
                    Case "spa"
                        ret = "Anular esta Tarea"
                End Select
            Case "cov157"
                Select Case lang
                    Case "eng"
                        ret = "Add a Sub-Task to this Task"
                    Case "fre"
                        ret = "Ajouter une sous-t�che � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Unteraufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una sottoattivit� a questa attivit�"
                    Case "spa"
                        ret = "Agregar una Sub-Tarea a esta Tarea"
                End Select
            Case "cov158"
                Select Case lang
                    Case "eng"
                        ret = "Add a New Task"
                    Case "fre"
                        ret = "Ajouter une nouvelle t�che"
                    Case "ger"
                        ret = "F�ge eine neue Aufgabe hinzu"
                    Case "ita"
                        ret = "Aggiungi una Nuova attivit�"
                    Case "spa"
                        ret = "Agregar una Nueva Tarea"
                End Select
            Case "cov159"
                Select Case lang
                    Case "eng"
                        ret = "Edit this Task"
                    Case "fre"
                        ret = "�diter cette T�che"
                    Case "ger"
                        ret = "Diese Aufgabe bearbeiten"
                    Case "ita"
                        ret = "Modifica questo Compito"
                    Case "spa"
                        ret = "Editar esta tarea"
                End Select
            Case "cov160"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea"
                End Select
            Case "cov161"
                Select Case lang
                    Case "eng"
                        ret = "Add a Failure Mode to this Task that was addessed in another Task"
                    Case "fre"
                        ret = "Ajouter un mode de d�faillance � cette t�che qui a �t� adress�e dans une autre t�che"
                    Case "ger"
                        ret = "F�ge dieser Aufgabe eine Ausfallart hinzu, die einer anderen Aufgabe zugeordnet wurde"
                    Case "ita"
                        ret = "Aggiungi una modalit� errore a questa attivit� che era stata aggiunta in un`altra attivit�"
                    Case "spa"
                        ret = "Agregar un Modo de Falla a esta Tarea que fue comprendido en otra Tarea"
                End Select
            Case "cov162"
                Select Case lang
                    Case "eng"
                        ret = "Cancel Changes"
                    Case "fre"
                        ret = "Annuler les Changements"
                    Case "ger"
                        ret = "�nderungen abbrechen"
                    Case "ita"
                        ret = "Elimina Modifiche"
                    Case "spa"
                        ret = "Cancelar cambios"
                End Select
            Case "cov163"
                Select Case lang
                    Case "eng"
                        ret = "Remove a Failure Mode from this Task"
                    Case "fre"
                        ret = "Enlever le Mode de D�faillance de cette T�che"
                    Case "ger"
                        ret = "Entfernen einer Fehlerm�glichkeit aus dieser Aufgabe"
                    Case "ita"
                        ret = "Rimuovere una Modalit� Guasto da questo Compito"
                    Case "spa"
                        ret = "Retire un modo de falla de esta tarea"
                End Select
            Case "cov164"
                Select Case lang
                    Case "eng"
                        ret = "Go to Previous Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che pr�c�dente de premier niveau"
                    Case "ger"
                        ret = "Gehe zur vorhergehende Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� precedente di livello massimo"
                    Case "spa"
                        ret = "Ir a la Previa Tarea de Alto Nivel"
                End Select
            Case "cov165"
                Select Case lang
                    Case "eng"
                        ret = "Go to First Top-Level Task"
                    Case "fre"
                        ret = "Aller � la premi�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur ersten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a prima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la Primera Tarea de Alto Nivel"
                End Select
            Case "cov166"
                Select Case lang
                    Case "eng"
                        ret = "Go to Next Top-Level Task"
                    Case "fre"
                        ret = "Aller � la t�che suivante de premier niveau"
                    Case "ger"
                        ret = "Gehe zur n�chsten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a attivit� successiva di livello massimo"
                    Case "spa"
                        ret = "Ir a la Siguiente Tarea de Alto Nivel"
                End Select
            Case "cov167"
                Select Case lang
                    Case "eng"
                        ret = "Go to Last Top-Level Task"
                    Case "fre"
                        ret = "Aller � la derni�re t�che de premier niveau"
                    Case "ger"
                        ret = "Gehe zur letzten Spitzen-Aufgabe"
                    Case "ita"
                        ret = "Vai a ultima attivit� di livello massimo"
                    Case "spa"
                        ret = "Ir a la �ltima Tarea de Alto Nivel"
                End Select
            Case "cov168"
                Select Case lang
                    Case "eng"
                        ret = "Get Task Descriptions Similar to this one. - Use keywords for best results"
                    Case "fre"
                        ret = "Obtenir les descriptions de t�ches identiques � celle-ci. - Utiliser des mots-cl�s pour de meilleurs r�sultats"
                    Case "ger"
                        ret = "Beschaffe Aufgabenbeschreibungen, die dieser �hneln. - Verwende Schl�sselworte f�r bessere Ergebnisse"
                    Case "ita"
                        ret = "Ottenere descrizioni di attivit� simili a questa. - Utilizzare le parole chiave per risultati migliori"
                    Case "spa"
                        ret = "Consiga Descripciones de Tarea Similar a esta. - Use palabras claves para mejores resultados"
                End Select
            Case "cov169"
                Select Case lang
                    Case "eng"
                        ret = "Open the Common Tasks Dialog"
                    Case "fre"
                        ret = "Ouvrir la bo�te de dialogue des t�ches communes"
                    Case "ger"
                        ret = "�ffne den Dialog Gew�hnliche Aufgaben"
                    Case "ita"
                        ret = "Aprire la finestra di dialogo delle attivit� comuni"
                    Case "spa"
                        ret = "Abrir el Di�logo de las Tareas Comunes"
                End Select
        End Select
        Return ret
    End Function
    Public Function gettpmuploadimageovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov359"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Image"
                    Case "fre"
                        ret = "Supprimer cette image"
                    Case "ger"
                        ret = "Diese Abbildung l�schen"
                    Case "ita"
                        ret = "Elimina questa immagine"
                    Case "spa"
                        ret = "Anular Esta Imagen"
                End Select
            Case "cov360"
                Select Case lang
                    Case "eng"
                        ret = "Save Image Order"
                    Case "fre"
                        ret = "Sauvegarder l`ordre d`image"
                    Case "ger"
                        ret = "Abbildungs-Reihenfolge speichern"
                    Case "ita"
                        ret = "Salva ordine immagine"
                    Case "spa"
                        ret = "Guardar el Orden de Im�genes"
                End Select
            Case "cov361"
                Select Case lang
                    Case "eng"
                        ret = "Delete This Image"
                    Case "fre"
                        ret = "Supprimer cette image"
                    Case "ger"
                        ret = "Diese Abbildung l�schen"
                    Case "ita"
                        ret = "Elimina questa immagine"
                    Case "spa"
                        ret = "Anular Esta Imagen"
                End Select
            Case "cov362"
                Select Case lang
                    Case "eng"
                        ret = "Save Image Order"
                    Case "fre"
                        ret = "Sauvegarder l`ordre d`image"
                    Case "ger"
                        ret = "Abbildungs-Reihenfolge speichern"
                    Case "ita"
                        ret = "Salva ordine immagine"
                    Case "spa"
                        ret = "Guardar el Orden de Im�genes"
                End Select
        End Select
        Return ret
    End Function
    Public Function getwoaltmanovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov76"
                Select Case lang
                    Case "eng"
                        ret = "Calculate Scheduled Complete Date based on Original"
                    Case "fre"
                        ret = "Calculer la date d`ach�vement programm�e d`apr�s l`original"
                    Case "ger"
                        ret = "Berechnen des geplanten Vervollst�ndigungsdatum auf der Grundlage des Originals"
                    Case "ita"
                        ret = "Calcola tutte le date complete programmate in base all`originale "
                    Case "spa"
                        ret = "Calcule Fecha Programada para Completar basada en fecha Original"
                End Select
        End Select
        Return ret
    End Function
    Public Function getwoarchovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov170"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
            Case "cov171"
                Select Case lang
                    Case "eng"
                        ret = "Translation Process For This Record Is Not Complete"
                    Case "fre"
                        ret = "Le processus de traduction pour cet enregistrement n`est pas complet"
                    Case "ger"
                        ret = "�bersetzungsprozess f�r diesen Eintrag nicht abgeschlossen"
                    Case "ita"
                        ret = "Il processo di traduzione per questo record non � completo"
                    Case "spa"
                        ret = "El Proceso de traducci�n Para Este Registro no Est� Completo "
                End Select
            Case "cov172"
                Select Case lang
                    Case "eng"
                        ret = "This Record For Editing by"
                    Case "fre"
                        ret = "Cet enregistrement � �diter par"
                    Case "ger"
                        ret = "Diese Eintragung f�r die Bearbeitung durch"
                    Case "ita"
                        ret = "Questo record per modifiche da"
                    Case "spa"
                        ret = "This Record For Editing by"
                End Select
        End Select
        Return ret
    End Function
    Public Function getwojppartovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov173"
                Select Case lang
                    Case "eng"
                        ret = "Save and Return"
                    Case "fre"
                        ret = "Enregistrer et revenir"
                    Case "ger"
                        ret = "Speichern und zur�ck"
                    Case "ita"
                        ret = "Salva e Indietro"
                    Case "spa"
                        ret = "Save and Return"
                End Select
        End Select
        Return ret
    End Function
    Public Function getwolistovval(ByVal lang As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Select Case aspxlabel
            Case "cov174"
                Select Case lang
                    Case "eng"
                        ret = "Print This Work Order"
                    Case "fre"
                        ret = "Imprimer cet ordre de travail"
                    Case "ger"
                        ret = "Drucken dieses Arbeitsauftrags"
                    Case "ita"
                        ret = "Stampa questo ordine di lavoro"
                    Case "spa"
                        ret = "Print This Work Order"
                End Select
        End Select
        Return ret
    End Function
End Class
