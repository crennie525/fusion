Public Class dglabs
    Dim dlang As New mmenu_utils
    Dim dlab As New dglabvals
    Public Function GetDGPage(ByVal aspxpage As String, ByVal dgid As String, ByVal tcol As String) As String
        Dim ret As String
        Dim lang As String
        Try
            lang = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            lang = dlang.AppDfltLang
        End Try
        Select Case aspxpage
            Case "AddComp.aspx"
                ret = dlab.getAddCompval(lang, dgid, tcol)
            Case "AddEditFuncPop.aspx"
                ret = dlab.getAddEditFuncPopval(lang, dgid, tcol)
            Case "AppSetApprHLTasks.aspx"
                ret = dlab.getAppSetApprHLTasksval(lang, dgid, tcol)
            Case "AppSetApprLLTasks.aspx"
                ret = dlab.getAppSetApprLLTasksval(lang, dgid, tcol)
            Case "AppSetApprPhases.aspx"
                ret = dlab.getAppSetApprPhasesval(lang, dgid, tcol)
            Case "AppSetAssetClass.aspx"
                ret = dlab.getAppSetAssetClassval(lang, dgid, tcol)
            Case "AppSetCellTab.aspx"
                ret = dlab.getAppSetCellTabval(lang, dgid, tcol)
            Case "AppSetComTab.aspx"
                ret = dlab.getAppSetComTabval(lang, dgid, tcol)
            Case "AppSetDeptTab.aspx"
                ret = dlab.getAppSetDeptTabval(lang, dgid, tcol)
            Case "AppSetEqTab.aspx"
                ret = dlab.getAppSetEqTabval(lang, dgid, tcol)
            Case "AppSetInvLists.aspx"
                ret = dlab.getAppSetInvListsval(lang, dgid, tcol)
            Case "AppSetLoc.aspx"
                ret = dlab.getAppSetLocval(lang, dgid, tcol)
            Case "AppSetLT.aspx"
                ret = dlab.getAppSetLTval(lang, dgid, tcol)
            Case "AppSetpmApprGrps.aspx"
                ret = dlab.getAppSetpmApprGrpsval(lang, dgid, tcol)
            Case "AppSetSYS.aspx"
                ret = dlab.getAppSetSYSval(lang, dgid, tcol)
            Case "AppSetTaskTabET.aspx"
                ret = dlab.getAppSetTaskTabETval(lang, dgid, tcol)
            Case "AppSetTaskTabPT.aspx"
                ret = dlab.getAppSetTaskTabPTval(lang, dgid, tcol)
            Case "AppSetTaskTabST.aspx"
                ret = dlab.getAppSetTaskTabSTval(lang, dgid, tcol)
            Case "AppSetTaskTabTT.aspx"
                ret = dlab.getAppSetTaskTabTTval(lang, dgid, tcol)
            Case "AppSetWoChargeType.aspx"
                ret = dlab.getAppSetWoChargeTypeval(lang, dgid, tcol)
            Case "AppSetWoStatus.aspx"
                ret = dlab.getAppSetWoStatusval(lang, dgid, tcol)
            Case "AppSetWoType.aspx"
                ret = dlab.getAppSetWoTypeval(lang, dgid, tcol)
            Case "archtasklubelist.aspx"
                ret = dlab.getarchtasklubelistval(lang, dgid, tcol)
            Case "archtasklubelisttpm.aspx"
                ret = dlab.getarchtasklubelisttpmval(lang, dgid, tcol)
            Case "archtaskpartlist.aspx"
                ret = dlab.getarchtaskpartlistval(lang, dgid, tcol)
            Case "archtaskpartlisttpm.aspx"
                ret = dlab.getarchtaskpartlisttpmval(lang, dgid, tcol)
            Case "archtasktoollist.aspx"
                ret = dlab.getarchtasktoollistval(lang, dgid, tcol)
            Case "archtasktoollisttpm.aspx"
                ret = dlab.getarchtasktoollisttpmval(lang, dgid, tcol)
            Case "Assessment_Admin.aspx"
                ret = dlab.getAssessment_Adminval(lang, dgid, tcol)
            Case "Assessment_Details.aspx"
                ret = dlab.getAssessment_Detailsval(lang, dgid, tcol)
            Case "compclass.aspx"
                ret = dlab.getcompclassval(lang, dgid, tcol)
            Case "CompGrid.aspx"
                ret = dlab.getCompGridval(lang, dgid, tcol)
            Case "complibtasks.aspx"
                ret = dlab.getcomplibtasksval(lang, dgid, tcol)
            Case "complibtaskview.aspx"
                ret = dlab.getcomplibtaskviewval(lang, dgid, tcol)
            Case "complibtaskview2.aspx"
                ret = dlab.getcomplibtaskview2val(lang, dgid, tcol)
            Case "complookup.aspx"
                ret = dlab.getcomplookupval(lang, dgid, tcol)
            Case "compmain.aspx"
                ret = dlab.getcompmainval(lang, dgid, tcol)
            Case "compseq.aspx"
                ret = dlab.getcompseqval(lang, dgid, tcol)
            Case "comptypes.aspx"
                ret = dlab.getcomptypesval(lang, dgid, tcol)
            Case "csub.aspx"
                ret = dlab.getcsubval(lang, dgid, tcol)
            Case "devTaskLubeList.aspx"
                ret = dlab.getdevTaskLubeListval(lang, dgid, tcol)
            Case "devTaskLubeListtpm.aspx"
                ret = dlab.getdevTaskLubeListtpmval(lang, dgid, tcol)
            Case "devTaskPartList.aspx"
                ret = dlab.getdevTaskPartListval(lang, dgid, tcol)
            Case "devTaskPartListtpm.aspx"
                ret = dlab.getdevTaskPartListtpmval(lang, dgid, tcol)
            Case "devTaskToolList.aspx"
                ret = dlab.getdevTaskToolListval(lang, dgid, tcol)
            Case "devTaskToolListtpm.aspx"
                ret = dlab.getdevTaskToolListtpmval(lang, dgid, tcol)
            Case "EqGrid.aspx"
                ret = dlab.getEqGridval(lang, dgid, tcol)
            Case "eqlookup.aspx"
                ret = dlab.geteqlookupval(lang, dgid, tcol)
            Case "FuncGrid.aspx"
                ret = dlab.getFuncGridval(lang, dgid, tcol)
            Case "funcseq.aspx"
                ret = dlab.getfuncseqval(lang, dgid, tcol)
            Case "GSub.aspx"
                ret = dlab.getGSubval(lang, dgid, tcol)
            Case "gsubarch.aspx"
                ret = dlab.getgsubarchval(lang, dgid, tcol)
            Case "gsubarchtpm.aspx"
                ret = dlab.getgsubarchtpmval(lang, dgid, tcol)
            Case "GSubtpm.aspx"
                ret = dlab.getGSubtpmval(lang, dgid, tcol)
            Case "GTasksFunc2.aspx"
                ret = dlab.getGTasksFunc2val(lang, dgid, tcol)
            Case "GTasksFunctpm.aspx"
                ret = dlab.getGTasksFunctpmval(lang, dgid, tcol)
            Case "GTasksFunctpm2.aspx"
                ret = dlab.getGTasksFunctpm2val(lang, dgid, tcol)
            Case "Inventory.aspx"
                ret = dlab.getInventoryval(lang, dgid, tcol)
            Case "InventoryLubes.aspx"
                ret = dlab.getInventoryLubesval(lang, dgid, tcol)
            Case "InventoryTools.aspx"
                ret = dlab.getInventoryToolsval(lang, dgid, tcol)
            Case "issueinv.aspx"
                ret = dlab.getissueinvval(lang, dgid, tcol)
            Case "JobPlan.aspx"
                ret = dlab.getJobPlanval(lang, dgid, tcol)
            Case "JobPlanTaskList.aspx"
                ret = dlab.getJobPlanTaskListval(lang, dgid, tcol)
            Case "JSub.aspx"
                ret = dlab.getJSubval(lang, dgid, tcol)
            Case "labshedmain.aspx"
                ret = dlab.getlabshedmainval(lang, dgid, tcol)
            Case "lubeoutedit.aspx"
                ret = dlab.getlubeouteditval(lang, dgid, tcol)
            Case "lubeoutroutes.aspx"
                ret = dlab.getlubeoutroutesval(lang, dgid, tcol)
            Case "matlookup.aspx"
                ret = dlab.getmatlookupval(lang, dgid, tcol)
            Case "measoutedit.aspx"
                ret = dlab.getmeasouteditval(lang, dgid, tcol)
            Case "measoutfly.aspx"
                ret = dlab.getmeasoutflyval(lang, dgid, tcol)
            Case "measouthistedit.aspx"
                ret = dlab.getmeasouthisteditval(lang, dgid, tcol)
            Case "measoutroutes.aspx"
                ret = dlab.getmeasoutroutesval(lang, dgid, tcol)
            Case "measoutroutesmini.aspx"
                ret = dlab.getmeasoutroutesminival(lang, dgid, tcol)
            Case "measroutesaddbot.aspx"
                ret = dlab.getmeasroutesaddbotval(lang, dgid, tcol)
            Case "optTaskLubeList.aspx"
                ret = dlab.getoptTaskLubeListval(lang, dgid, tcol)
            Case "optTaskLubeListtpm.aspx"
                ret = dlab.getoptTaskLubeListtpmval(lang, dgid, tcol)
            Case "optTaskPartList.aspx"
                ret = dlab.getoptTaskPartListval(lang, dgid, tcol)
            Case "optTaskPartListtpm.aspx"
                ret = dlab.getoptTaskPartListtpmval(lang, dgid, tcol)
            Case "optTaskToolList.aspx"
                ret = dlab.getoptTaskToolListval(lang, dgid, tcol)
            Case "optTaskToolListtpm.aspx"
                ret = dlab.getoptTaskToolListtpmval(lang, dgid, tcol)
            Case "partsmain.aspx"
                ret = dlab.getpartsmainval(lang, dgid, tcol)
            Case "pmact.aspx"
                ret = dlab.getpmactval(lang, dgid, tcol)
            Case "pmactm.aspx"
                ret = dlab.getpmactmval(lang, dgid, tcol)
            Case "pmactmtpm.aspx"
                ret = dlab.getpmactmtpmval(lang, dgid, tcol)
            Case "pmacttpm.aspx"
                ret = dlab.getpmacttpmval(lang, dgid, tcol)
            Case "PMLeadMan.aspx"
                ret = dlab.getPMLeadManval(lang, dgid, tcol)
            Case "pmlibnewtaskgrid.aspx"
                ret = dlab.getpmlibnewtaskgridval(lang, dgid, tcol)
            Case "pmme.aspx"
                ret = dlab.getpmmeval(lang, dgid, tcol)
            Case "PMMeas.aspx"
                ret = dlab.getPMMeasval(lang, dgid, tcol)
            Case "PMMeastpm.aspx"
                ret = dlab.getPMMeastpmval(lang, dgid, tcol)
            Case "pmmetpm.aspx"
                ret = dlab.getpmmetpmval(lang, dgid, tcol)
            Case "PMRoutes2.aspx"
                ret = dlab.getPMRoutes2val(lang, dgid, tcol)
            Case "PMSuperMan.aspx"
                ret = dlab.getPMSuperManval(lang, dgid, tcol)
            Case "pmtasksched.aspx"
                ret = dlab.getpmtaskschedval(lang, dgid, tcol)
            Case "pmtaskschedtpm.aspx"
                ret = dlab.getpmtaskschedtpmval(lang, dgid, tcol)
            Case "Prime_Selection.aspx"
                ret = dlab.getPrime_Selectionval(lang, dgid, tcol)
            Case "Resources.aspx"
                ret = dlab.getResourcesval(lang, dgid, tcol)
            Case "schedparts.aspx"
                ret = dlab.getschedpartsval(lang, dgid, tcol)
            Case "sparepartlist.aspx"
                ret = dlab.getsparepartlistval(lang, dgid, tcol)
            Case "sparepartpicharchtpm.aspx"
                ret = dlab.getsparepartpicharchtpmval(lang, dgid, tcol)
            Case "sparepartpick.aspx"
                ret = dlab.getsparepartpickval(lang, dgid, tcol)
            Case "sparepartpickarch.aspx"
                ret = dlab.getsparepartpickarchval(lang, dgid, tcol)
            Case "sparepartpicktpm.aspx"
                ret = dlab.getsparepartpicktpmval(lang, dgid, tcol)
            Case "SubjectMatter.aspx"
                ret = dlab.getSubjectMatterval(lang, dgid, tcol)
            Case "tasksched.aspx"
                ret = dlab.gettaskschedval(lang, dgid, tcol)
            Case "totpms.aspx"
                ret = dlab.gettotpmsval(lang, dgid, tcol)
            Case "tpmlubes.aspx"
                ret = dlab.gettpmlubesval(lang, dgid, tcol)
            Case "tpmmeas.aspx"
                ret = dlab.gettpmmeasval(lang, dgid, tcol)
            Case "tpmparts.aspx"
                ret = dlab.gettpmpartsval(lang, dgid, tcol)
            Case "tpmtime.aspx"
                ret = dlab.gettpmtimeval(lang, dgid, tcol)
            Case "tpmtools.aspx"
                ret = dlab.gettpmtoolsval(lang, dgid, tcol)
            Case "whereused.aspx"
                ret = dlab.getwhereusedval(lang, dgid, tcol)
            Case "wjsub.aspx"
                ret = dlab.getwjsubval(lang, dgid, tcol)
            Case "woactm.aspx"
                ret = dlab.getwoactmval(lang, dgid, tcol)
            Case "WoChargeConfig.aspx"
                ret = dlab.getWoChargeConfigval(lang, dgid, tcol)
            Case "WoChargeEntry.aspx"
                ret = dlab.getWoChargeEntryval(lang, dgid, tcol)
            Case "wofm2.aspx"
                ret = dlab.getwofm2val(lang, dgid, tcol)
            Case "wofmme2.aspx"
                ret = dlab.getwofmme2val(lang, dgid, tcol)
            Case "wojpact2.aspx"
                ret = dlab.getwojpact2val(lang, dgid, tcol)
            Case "wojpactm.aspx"
                ret = dlab.getwojpactmval(lang, dgid, tcol)
            Case "wojpedit.aspx"
                ret = dlab.getwojpeditval(lang, dgid, tcol)
            Case "wojpme2.aspx"
                ret = dlab.getwojpme2val(lang, dgid, tcol)
            Case "wojpmeas.aspx"
                ret = dlab.getwojpmeasval(lang, dgid, tcol)
            Case "wojpsched.aspx"
                ret = dlab.getwojpschedval(lang, dgid, tcol)
            Case "wolabtrans.aspx"
                ret = dlab.getwolabtransval(lang, dgid, tcol)
            Case "wosched.aspx"
                ret = dlab.getwoschedval(lang, dgid, tcol)
        End Select
        Return ret
    End Function
End Class
