Public Class codeovlibs
    Dim dlang As New mmenu_utils
    Dim dlab As New codeovlibvals
    Public Function GetCodeOVLib(ByVal aspxpage As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Dim lang As String
        Try
            lang = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            lang = dlang.AppDfltLang
        End Try
        Select Case aspxpage
            Case "altitem.aspx.vb"
                ret = dlab.getaltitemovval(lang, aspxlabel)
            Case "archrationale2.aspx.vb"
                ret = dlab.getarchrationale2ovval(lang, aspxlabel)
            Case "archrationaletpm2.aspx.vb"
                ret = dlab.getarchrationaletpm2ovval(lang, aspxlabel)
            Case "catalog.aspx.vb"
                ret = dlab.getcatalogovval(lang, aspxlabel)
            Case "commontasks.aspx.vb"
                ret = dlab.getcommontasksovval(lang, aspxlabel)
            Case "commontaskstpm.aspx.vb"
                ret = dlab.getcommontaskstpmovval(lang, aspxlabel)
            Case "CompDiv.aspx.vb"
                ret = dlab.getCompDivovval(lang, aspxlabel)
            Case "complibtaskeadittpm.aspx.vb"
                ret = dlab.getcomplibtaskeadittpmovval(lang, aspxlabel)
            Case "complibtasks.aspx.vb"
                ret = dlab.getcomplibtasksovval(lang, aspxlabel)
            Case "complibtasks2.aspx.vb"
                ret = dlab.getcomplibtasks2ovval(lang, aspxlabel)
            Case "complibtasks3.aspx.vb"
                ret = dlab.getcomplibtasks3ovval(lang, aspxlabel)
            Case "complibtasksedit.aspx.vb"
                ret = dlab.getcomplibtaskseditovval(lang, aspxlabel)
            Case "complibtasksedit2.aspx.vb"
                ret = dlab.getcomplibtasksedit2ovval(lang, aspxlabel)
            Case "complibuploadimage.aspx.vb"
                ret = dlab.getcomplibuploadimageovval(lang, aspxlabel)
            Case "DeptArch.aspx.vb"
                ret = dlab.getDeptArchovval(lang, aspxlabel)
            Case "DevArch.aspx.vb"
                ret = dlab.getDevArchovval(lang, aspxlabel)
            Case "DevArchtpm.aspx.vb"
                ret = dlab.getDevArchtpmovval(lang, aspxlabel)
            Case "dragndropno.aspx.vb"
                ret = dlab.getdragndropnoovval(lang, aspxlabel)
            Case "EQBot.aspx.vb"
                ret = dlab.getEQBotovval(lang, aspxlabel)
            Case "equploadimage.aspx.vb"
                ret = dlab.getequploadimageovval(lang, aspxlabel)
            Case "FuncDiv.aspx.vb"
                ret = dlab.getFuncDivovval(lang, aspxlabel)
            Case "gtaskfail.aspx.vb"
                ret = dlab.getgtaskfailovval(lang, aspxlabel)
            Case "GTasksFunctpm.aspx.vb"
                ret = dlab.getGTasksFunctpmovval(lang, aspxlabel)
            Case "JobPlanList.aspx.vb"
                ret = dlab.getJobPlanListovval(lang, aspxlabel)
            Case "LocDets.aspx.vb"
                ret = dlab.getLocDetsovval(lang, aspxlabel)
            Case "mmenu1.ascx.vb"
                ret = dlab.getmmenu1ovval(lang, aspxlabel)
            Case "NCBot.aspx.vb"
                ret = dlab.getNCBotovval(lang, aspxlabel)
            Case "OptDevArch.aspx.vb"
                ret = dlab.getOptDevArchovval(lang, aspxlabel)
            Case "OptDevArchtpm.aspx.vb"
                ret = dlab.getOptDevArchtpmovval(lang, aspxlabel)
            Case "PMApprovalEq.aspx.vb"
                ret = dlab.getPMApprovalEqovval(lang, aspxlabel)
            Case "pmarch.aspx.vb"
                ret = dlab.getpmarchovval(lang, aspxlabel)
            Case "PMArchMan.aspx.vb"
                ret = dlab.getPMArchManovval(lang, aspxlabel)
            Case "PMArchMantpm.aspx.vb"
                ret = dlab.getPMArchMantpmovval(lang, aspxlabel)
            Case "PMArchRT.aspx.vb"
                ret = dlab.getPMArchRTovval(lang, aspxlabel)
            Case "pmarchtasks.aspx.vb"
                ret = dlab.getpmarchtasksovval(lang, aspxlabel)
            Case "pmarchtaskstpm.aspx.vb"
                ret = dlab.getpmarchtaskstpmovval(lang, aspxlabel)
            Case "pmarchtpm.aspx.vb"
                ret = dlab.getpmarchtpmovval(lang, aspxlabel)
            Case "PMDivMan.aspx.vb"
                ret = dlab.getPMDivManovval(lang, aspxlabel)
            Case "PMDivMantpm.aspx.vb"
                ret = dlab.getPMDivMantpmovval(lang, aspxlabel)
            Case "PMFailMan.aspx.vb"
                ret = dlab.getPMFailManovval(lang, aspxlabel)
            Case "PMGetPMMan.aspx.vb"
                ret = dlab.getPMGetPMManovval(lang, aspxlabel)
            Case "PMGetPMMantpm.aspx.vb"
                ret = dlab.getPMGetPMMantpmovval(lang, aspxlabel)
            Case "PMGridMan.aspx.vb"
                ret = dlab.getPMGridManovval(lang, aspxlabel)
            Case "PMGridMantpm.aspx.vb"
                ret = dlab.getPMGridMantpmovval(lang, aspxlabel)
            Case "pmlocmain.aspx.vb"
                ret = dlab.getpmlocmainovval(lang, aspxlabel)
            Case "PMMainMan.aspx.vb"
                ret = dlab.getPMMainManovval(lang, aspxlabel)
            Case "PMMainMantpm.aspx.vb"
                ret = dlab.getPMMainMantpmovval(lang, aspxlabel)
            Case "PMOptRationale2.aspx.vb"
                ret = dlab.getPMOptRationale2ovval(lang, aspxlabel)
            Case "PMOptRationaleTPM2.aspx.vb"
                ret = dlab.getPMOptRationaleTPM2ovval(lang, aspxlabel)
            Case "PMOptTasks.aspx.vb"
                ret = dlab.getPMOptTasksovval(lang, aspxlabel)
            Case "PMRouteList.aspx.vb"
                ret = dlab.getPMRouteListovval(lang, aspxlabel)
            Case "PMRoutes2.aspx.vb"
                ret = dlab.getPMRoutes2ovval(lang, aspxlabel)
            Case "PMSuperSelect.aspx.vb"
                ret = dlab.getPMSuperSelectovval(lang, aspxlabel)
            Case "PMTaskDivFunc.aspx.vb"
                ret = dlab.getPMTaskDivFuncovval(lang, aspxlabel)
            Case "pmuploadimage.aspx.vb"
                ret = dlab.getpmuploadimageovval(lang, aspxlabel)
            Case "reorderdetails.aspx.vb"
                ret = dlab.getreorderdetailsovval(lang, aspxlabel)
            Case "ScheduledTasks.aspx.vb"
                ret = dlab.getScheduledTasksovval(lang, aspxlabel)
            Case "SiteAssets.aspx.vb"
                ret = dlab.getSiteAssetsovval(lang, aspxlabel)
            Case "siteassets2.aspx.vb"
                ret = dlab.getsiteassets2ovval(lang, aspxlabel)
            Case "siteassets3.aspx.vb"
                ret = dlab.getsiteassets3ovval(lang, aspxlabel)
            Case "sparepartpicharchtpm.aspx.vb"
                ret = dlab.getsparepartpicharchtpmovval(lang, aspxlabel)
            Case "sparepartpick.aspx.vb"
                ret = dlab.getsparepartpickovval(lang, aspxlabel)
            Case "sparepartpickarch.aspx.vb"
                ret = dlab.getsparepartpickarchovval(lang, aspxlabel)
            Case "sparepartpicktpm.aspx.vb"
                ret = dlab.getsparepartpicktpmovval(lang, aspxlabel)
            Case "storeroom.aspx.vb"
                ret = dlab.getstoreroomovval(lang, aspxlabel)
            Case "taskedit2.aspx.vb"
                ret = dlab.gettaskedit2ovval(lang, aspxlabel)
            Case "tpmarch.aspx.vb"
                ret = dlab.gettpmarchovval(lang, aspxlabel)
            Case "tpmgrid.aspx.vb"
                ret = dlab.gettpmgridovval(lang, aspxlabel)
            Case "tpmopttasks.aspx.vb"
                ret = dlab.gettpmopttasksovval(lang, aspxlabel)
            Case "tpmtask.aspx.vb"
                ret = dlab.gettpmtaskovval(lang, aspxlabel)
            Case "tpmtasks.aspx.vb"
                ret = dlab.gettpmtasksovval(lang, aspxlabel)
            Case "tpmuploadimage.aspx.vb"
                ret = dlab.gettpmuploadimageovval(lang, aspxlabel)
            Case "woaltman.aspx.vb"
                ret = dlab.getwoaltmanovval(lang, aspxlabel)
            Case "woarch.aspx.vb"
                ret = dlab.getwoarchovval(lang, aspxlabel)
            Case "wojppart.aspx.vb"
                ret = dlab.getwojppartovval(lang, aspxlabel)
            Case "wolist.aspx.vb"
                ret = dlab.getwolistovval(lang, aspxlabel)
        End Select
        Return ret
    End Function
End Class
