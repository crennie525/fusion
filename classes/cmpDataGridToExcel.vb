

'********************************************************
'*
'********************************************************



Public Class cmpDataGridToExcel
    Public Shared Sub DataGridToExcel(ByVal dgexport As DataGrid, ByVal dlhd As DataList, ByVal response As HttpResponse)
        'clean up the response.object
        response.Clear()
        response.Charset = ""
        'set the response mime type for excel
        response.ContentType = "application/vnd.ms-excel"
        'create a string writer
        Dim stringWrite As New System.IO.StringWriter
        'create an htmltextwriter which uses the stringwriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        Dim dl As New DataList

        dl = dlhd
        'instantiate a datagrid
        Dim dg As New DataGrid
        ' just set the input datagrid = to the new dg grid
        dg = dgexport

        ' I want to make sure there are no annoying gridlines
        'dg.GridLines = GridLines.None
        ' Make the header text bold
        dl.HeaderStyle.Font.Bold = True

        ' If needed, here's how to change colors/formatting at the component level
        'dg.HeaderStyle.ForeColor = System.Drawing.System.Drawing.Color.Black
        'dg.ItemStyle.ForeColor = System.Drawing.System.Drawing.Color.Black

        'bind the modified datagrid
        dl.DataBind()
        dg.DataBind()
        'tell the datagrid to render itself to our htmltextwriter
        dl.RenderControl(htmlWrite)
        dg.RenderControl(htmlWrite)
        'output the html
        response.Write(stringWrite.ToString)
        response.End()
    End Sub
    Public Shared Sub DataGridToExcelNHD(ByVal dgexport As DataGrid, ByVal response As HttpResponse)
        response.Clear()
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=ClientCopy.xls")
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        Dim dg As New DataGrid
        dg = dgexport
        dg.HeaderStyle.Font.Bold = True
        dg.ItemStyle.ForeColor = System.Drawing.Color.Black
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        response.Write(stringWrite.ToString)
        response.End()
    End Sub
    Public Shared Sub DataGridToExcelNHD1(ByVal dgexport As DataGrid, ByVal response As HttpResponse)
        response.Clear()
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=ClientCopy.xls")
        response.Charset = ""

        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        Dim dg As New DataGrid
        dg = dgexport
        dg.HeaderStyle.Font.Bold = True
        dg.ItemStyle.ForeColor = System.Drawing.Color.Black
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        response.Write(stringWrite.ToString)
        response.End()
    End Sub
End Class

