Public Class xlabs
    Dim dlang As New mmenu_utils
    Dim dlab As New xlabsvals
    Public Function GetXLab(ByVal aspxpage As String, ByVal aspxlabel As String) As String
        Dim ret As String
        Dim lang As String
        Try
            lang = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            lang = dlang.AppDfltLang
        End Try
        Select Case aspxpage
            Case "AssignmentReport.aspx.vb"
                ret = dlab.getAssignmentReportxval(lang, aspxlabel)
            Case "AssignmentReportPics.aspx.vb"
                ret = dlab.getAssignmentReportPicsxval(lang, aspxlabel)
            Case "AssignmentReportTPM.aspx.vb"
                ret = dlab.getAssignmentReportTPMxval(lang, aspxlabel)
            Case "clientproc.aspx.vb"
                ret = dlab.getclientprocxval(lang, aspxlabel)
            Case "CMMSWRD2.vb"
                ret = dlab.getCMMSWRD2xval(lang, aspxlabel)
            Case "compnotasks.aspx.vb"
                ret = dlab.getcompnotasksxval(lang, aspxlabel)
            Case "CustomCMMShtml.aspx.vb"
                ret = dlab.getCustomCMMShtmlxval(lang, aspxlabel)
            Case "CustomCMMShtmlTPM.aspx.vb"
                ret = dlab.getCustomCMMShtmlTPMxval(lang, aspxlabel)
            Case "CustomCMMShtmlTPMpics.aspx.vb"
                ret = dlab.getCustomCMMShtmlTPMpicsxval(lang, aspxlabel)
            Case "EQReport.aspx.vb"
                ret = dlab.getEQReportxval(lang, aspxlabel)
            Case "failnotasks.aspx.vb"
                ret = dlab.getfailnotasksxval(lang, aspxlabel)
            Case "FieldReport.aspx.vb"
                ret = dlab.getFieldReportxval(lang, aspxlabel)
            Case "FieldReportTPM.aspx.vb"
                ret = dlab.getFieldReportTPMxval(lang, aspxlabel)
            Case "inprog.aspx.vb"
                ret = dlab.getinprogxval(lang, aspxlabel)
            Case "intra_utils.vb"
                ret = dlab.getintra_utilsxval(lang, aspxlabel)
            Case "jobplanhtml.aspx.vb"
                ret = dlab.getjobplanhtmlxval(lang, aspxlabel)
            Case "MissingData.aspx.vb"
                ret = dlab.getMissingDataxval(lang, aspxlabel)
            Case "mmenu_utils.vb"
                ret = dlab.getmmenu_utilsxval(lang, aspxlabel)
            Case "OVRollup.aspx.vb"
                ret = dlab.getOVRollupxval(lang, aspxlabel)
            Case "OVRollup_OP.aspx.vb"
                ret = dlab.getOVRollup_OPxval(lang, aspxlabel)
            Case "OVRollup_Tot.aspx.vb"
                ret = dlab.getOVRollup_Totxval(lang, aspxlabel)
            Case "OVRollupTPM.aspx.vb"
                ret = dlab.getOVRollupTPMxval(lang, aspxlabel)
            Case "PGEMSHTM.vb"
                ret = dlab.getPGEMSHTMxval(lang, aspxlabel)
            Case "PGEMSHTM2.vb"
                ret = dlab.getPGEMSHTM2xval(lang, aspxlabel)
            Case "PGEMShtml.aspx.vb"
                ret = dlab.getPGEMShtmlxval(lang, aspxlabel)
            Case "PGEMSTXT.vb"
                ret = dlab.getPGEMSTXTxval(lang, aspxlabel)
            Case "PMAcceptancePkg.aspx.vb"
                ret = dlab.getPMAcceptancePkgxval(lang, aspxlabel)
            Case "PMALL.vb"
                ret = dlab.getPMALLxval(lang, aspxlabel)
            Case "PMALLTPM.vb"
                ret = dlab.getPMALLTPMxval(lang, aspxlabel)
            Case "pmmail.vb"
                ret = dlab.getpmmailxval(lang, aspxlabel)
            Case "PMRationaleAll.aspx.vb"
                ret = dlab.getPMRationaleAllxval(lang, aspxlabel)
            Case "PMRationaleAllTPM.aspx.vb"
                ret = dlab.getPMRationaleAllTPMxval(lang, aspxlabel)
            Case "PMRationaleReport.aspx.vb"
                ret = dlab.getPMRationaleReportxval(lang, aspxlabel)
            Case "pmtaskrationale.aspx.vb"
                ret = dlab.getpmtaskrationalexval(lang, aspxlabel)
            Case "PMWOALL.vb"
                ret = dlab.getPMWOALLxval(lang, aspxlabel)
            Case "PMWOALLTPM.vb"
                ret = dlab.getPMWOALLTPMxval(lang, aspxlabel)
            Case "TPMAlert.aspx.vb"
                ret = dlab.getTPMAlertxval(lang, aspxlabel)
            Case "TPMHTM2.vb"
                ret = dlab.getTPMHTM2xval(lang, aspxlabel)
            Case "tpmnonweekly.aspx.vb"
                ret = dlab.gettpmnonweeklyxval(lang, aspxlabel)
            Case "TPMTXT.vb"
                ret = dlab.getTPMTXTxval(lang, aspxlabel)
            Case "tpmweekly2.aspx.vb"
                ret = dlab.gettpmweekly2xval(lang, aspxlabel)
            Case "ttno.aspx.vb"
                ret = dlab.getttnoxval(lang, aspxlabel)
            Case "wrdown.vb"
                ret = dlab.getwrdownxval(lang, aspxlabel)
            Case "wrmail.vb"
                ret = dlab.getwrmailxval(lang, aspxlabel)
        End Select
        Return ret
    End Function
End Class
