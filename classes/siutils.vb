

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class siutils

    Dim tmod As New transmod

    Public Shared Sub GetAscii(ByRef aspxPage As System.Web.UI.Page, ByVal str As String, Optional ByVal typ As String = "main")
        Dim itst As Integer
        itst = str.IndexOf(";")
        itst += str.IndexOf("%3b")
        itst += str.IndexOf("%3B")
        itst += str.IndexOf("sp_")
        itst += str.IndexOf("sp%5F")
        itst += str.IndexOf("sp%5f")
        itst += str.IndexOf("--")
        itst += str.IndexOf("%2D%2D")
        itst += str.IndexOf("%2d%2d")
        If itst = -9 Then
            itst = -1
        Else

            Dim sessid As String = HttpContext.Current.Session.SessionID
            Dim myConn As String = System.Configuration.ConfigurationManager.AppSettings.Item("strConn")
            Dim cmd As New SqlCommand("update logtrack set si = 1 where sessionid = @sessid", New SqlConnection(myConn))
            Dim param01 = New SqlParameter("@sessid", SqlDbType.VarChar)
            param01.Value = sessid
            cmd.Parameters.Add(param01)
            cmd.Connection.Open()
            cmd.ExecuteNonQuery()
            cmd.Connection.Close()
            cmd.Connection.Dispose()

            If typ = "main" Then
                Dim redir As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
                aspxPage.Response.Redirect(redir)
            Else
                Dim strScript As String = "<script language=JavaScript>sqlalert();</script>"
                Dim strKey As String = "strKey1"
                If (Not aspxPage.IsStartupScriptRegistered(strKey)) Then
                    aspxPage.RegisterStartupScript(strKey, strScript)
                End If
            End If
        End If
    End Sub

End Class
