

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class clsHead
    Dim sql As String
    Dim dr As SqlDataReader
    Dim head As New Utilities
    Public Function GetFuncHead(ByVal tli As String, ByVal cid As String, ByVal cell As String, _
    ByVal comid As String, ByVal funid As String) As SqlClient.SqlDataReader
        Try
            head.Open()
        Catch ex As Exception

        End Try

        If comid <> "no" And comid <> "0" Then
            If cell = "yes" Then
                sql = "select " _
                + "c.compname, co.compnum, " _
                + "co.compdesc, f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, cl.cellid, cl.cell_name, " _
                + "cl.cell_desc, d.dept_id, d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc " _
                + "from company c, components co, functions f, equipment e, cells cl, dept d, sites s " _
                + "where s.compid = c.compid and f.func_id = co.func_id and f.eqid = e.eqid and " _
                + "e.cellid = cl.cellid and cl.Dept_ID = d.Dept_ID and " _
                + "d.siteid = s.siteid and co.comid = '" & comid & "'"
                dr = head.GetRdrData(sql)
            ElseIf cell = "no" Then
                sql = "select " _
                 + "c.compname, co.compnum, " _
                + "co.compdesc, f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, " _
                + "d.dept_id, d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                + "'N/A' as cell_name, '' as cell_desc " _
                + "from company c, components co, functions f, equipment e, dept d, sites s " _
                + "where s.compid = c.compid and f.func_id = co.func_id and f.eqid = e.eqid and " _
                + "e.Dept_ID = d.Dept_ID and " _
                + "d.siteid = s.siteid and co.comid = '" & comid & "'"
                dr = head.GetRdrData(sql)
            Else
                sql = "select " _
                + "c.compname, co.compnum, " _
                + "co.compdesc, f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, " _
                + "'N/A' as dept_line, '' as dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                + "'N/A' as cell_name, '' as cell_desc " _
                + "from company c, components co, functions f, equipment e, sites s " _
                + "where s.compid = c.compid and f.func_id = co.func_id and f.eqid = e.eqid and " _
                + "co.comid = '" & comid & "'"
                dr = head.GetRdrData(sql)
            End If
        Else
            If cell = "yes" Then
                sql = "select " _
                + "c.compname, " _
                + "f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, cl.cellid, cl.cell_name, " _
                + "cl.cell_desc, d.dept_id, d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                + "'N/A' as compnum, '' as compdesc " _
                + "from company c, functions f, equipment e, cells cl, dept d, sites s " _
                + "where s.compid = c.compid and f.eqid = e.eqid and " _
                + "e.cellid = cl.cellid and cl.Dept_ID = d.Dept_ID and " _
                + "d.siteid = s.siteid and f.func_id = '" & funid & "'"
                dr = head.GetRdrData(sql)
            ElseIf cell = "no" Then
                sql = "select " _
                + "c.compname, " _
               + "f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, " _
               + "d.dept_id, d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc, " _
               + "'N/A' as cell_name, '' as cell_desc, " _
               + "'N/A' as compnum, '' as compdesc " _
               + "from company c, functions f, equipment e, dept d, sites s " _
               + "where s.compid = c.compid and f.eqid = e.eqid and " _
               + "e.Dept_ID = d.Dept_ID and " _
               + "d.siteid = s.siteid and f.func_id = '" & funid & "'"
                dr = head.GetRdrData(sql)
            Else
                sql = "select c.compname, f.func_id, f.func, f.func_desc, e.eqid, e.eqnum, e.eqdesc, 'N/A' " _
                + "as dept_line, '' as dept_desc, " _
                + "s.siteid, s.sitename, s.sitedesc, 'N/A' as cell_name, '' as cell_desc, 'N/A' as compnum, '' as compdesc " _
                + "from functions f " _
                + "left join equipment e on f.eqid = e.eqid " _
                + "left join sites s on e.siteid = s.siteid " _
                + "left join company c on s.compid = c.compid " _
                + "where  f.func_id = '" & funid & "'"
                dr = head.GetRdrData(sql)
            End If
        End If
        'head.Dispose()
        Return dr
    End Function
End Class
