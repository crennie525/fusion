

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class cltproc

    Dim tmod As New transmod

    Dim sql As String
    Dim dr As SqlDataReader
    Dim old As New Utilities
    Public Function PopOLD(ByVal oldid As String, ByVal typ As String)
        old.Open()
        Dim fid As String = ""
        Dim skillchk As String = ""
        Dim skill As String = ""
        Dim start As Integer = 0
        Dim flag As Integer = 0
        Dim subtask As String = ""
        Dim sb As New System.Text.StringBuilder

        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""620""></td></tr>")
        sb.Append("<tr><td class=""bigbold"" align=""center"">" & tmod.getlbl("cdlbl355" , "cltproc.vb") & "</td></tr>")

        sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")
        sb.Append("<tr><td width=""120"">&nbsp;</td>")
        sb.Append("<td width=""500"">&nbsp;</td></tr>")

        sql = "select cp.pmtitle, " _
        + "cp.eqnum, cp.skill from client_procs cp " _
        + "where cp.oldid = '" & oldid & "'"

        Dim pmt, eq, sk As String

        dr = old.GetRdrData(sql)
        While dr.Read
            pmt = dr.Item("pmtitle").ToString
            eq = dr.Item("eqnum").ToString
            sk = dr.Item("skill").ToString
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl356" , "cltproc.vb") & "</td>")
            sb.Append("<td width=""500"" class=""plainlabel"">" & pmt & "</td></tr>")
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl357" , "cltproc.vb") & "</td>")
            sb.Append("<td width=""500"" class=""plainlabel"">" & eq & "</td></tr>")
            sb.Append("<tr><td width=""120"" class=""label"">" & tmod.getlbl("cdlbl358" , "cltproc.vb") & "</td>")
            sb.Append("<td width=""500"" class=""plainlabel"">" & sk & "</td></tr></table></td></tr>")
            sb.Append("<tr><td>&nbsp;</td></tr>")
        End While
        dr.Close()
        sb.Append("<tr><td>&nbsp;</td></tr>")
        sb.Append("<tr><td><Table cellSpacing=""0"" cellPadding=""2"" width=""620"">")

        Dim tnum, td, fm, sk1, qt, st, lube, tool, part, bp, bm As String

        If typ = "2" Then
            sb.Append("<tr><td class=""label"" colspan=""3""><u>Job Tasks:</u></td></tr>")
            sb.Append("<tr><td width=""20""></td><td width=""20""></td><td width=""580""></td></tr>")

            sql = "select cps.stepnum, cps.task, cps.skill, cps.qty, cps.time, cpp.parts, cpt.tools, cpl.lubes " _
                   + "from client_procs_steps cps " _
                   + "left join client_procs_lubes cpl on cps.stepid = cpl.stepid " _
                   + "left join client_procs_tools cpt on cps.stepid = cpt.stepid " _
                   + "left join client_procs_parts cpp on cps.stepid = cpp.stepid " _
                   + "where cps.oldid = '" & oldid & "' order by cps.stepnum"


            dr = old.GetRdrData(sql)
            While dr.Read
                tnum = dr.Item("stepnum").ToString
                td = dr.Item("task").ToString
                sk = dr.Item("skill").ToString
                qt = dr.Item("qty").ToString
                st = dr.Item("time").ToString
                lube = dr.Item("lubes").ToString
                tool = dr.Item("tools").ToString
                part = dr.Item("parts").ToString
                sb.Append("<tr><td class=""plainlabel"">[" & tnum & "]</td>")
                If Len(td) > 0 Then
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & td & "</td></tr>")
                Else
                    sb.Append("<td colspan=""2"" class=""plainlabel"">" & tmod.getlbl("cdlbl359" , "cltproc.vb") & "</td></tr>")
                End If
                If sk <> "" And sk <> "Select" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Skill:&nbsp;&nbsp;</b>" & sk)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Qty:&nbsp;&nbsp;</b>" & qt)
                    sb.Append("&nbsp;&nbsp;&nbsp;<b>Time:&nbsp;&nbsp;</b>" & st & "</td></tr>")
                End If
                If lube <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Lubes:&nbsp;&nbsp;</b>" & lube & "</td></tr>")
                End If
                If tool <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Tools:&nbsp;&nbsp;</b>" & tool & "</td></tr>")
                End If
                If part <> "" Then
                    sb.Append("<tr><td></td>")
                    sb.Append("<td colspan=""2"" class=""plainlabel""><b>Parts:&nbsp;&nbsp;</b>" & part & "</td></tr>")
                End If
                sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>")
            End While
            dr.Close()
            sb.Append("</table></td></tr></table>")
            sb.Append("</body></html>")
            'Response.Write(sb.ToString)
        Else
            sql = "select cpmb.matbulk from client_procs_mb cpmb " _
                  + "where cpmb.oldid = '" & oldid & "'"

            dr = old.GetRdrData(sql)
            While dr.Read
                sb.Append("<tr><td class=""label"" colspan=""3""><u>Materials Required:</u></td></tr>")
                sb.Append("<tr><td width=""620""></td></tr>")
                bm = dr.Item("matbulk").ToString
                sb.Append("<td colspan=""3"" class=""plainlabel"">" & bm & "</td></tr>")
                sb.Append("</table></td></tr></table>")
            End While
            dr.Close()

            sb.Append("<tr><td class=""label"" colspan=""3""><u>Procedure:</u></td></tr>")
            sb.Append("<tr><td width=""620""></td></tr>")

            sql = "select cpld.bulkproc from client_procs_ld cpld " _
                   + "where cpld.oldid = '" & oldid & "'"

            dr = old.GetRdrData(sql)
            While dr.Read
                bp = dr.Item("bulkproc").ToString
                sb.Append("<td colspan=""3"" class=""plainlabel"">" & bp & "</td></tr>")
                sb.Append("</table></td></tr></table>")
            End While
            dr.Close()
            sb.Append("</body></html>")
            'Response.Write(sb.ToString)
        End If
        Dim longstring As String = sb.ToString
        old.Dispose()
        Return longstring
    End Function
End Class
