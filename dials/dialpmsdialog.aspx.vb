﻿Public Class dialpmsdialog
    Inherits System.Web.UI.Page
    Dim dial, desc, dialid, sid, did, cid, locid, typ, eqid, eqnum, eqdesc, dname, cname, lname As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            dialid = Request.QueryString("dialid").ToString
            dial = Request.QueryString("dial").ToString
            desc = Request.QueryString("desc").ToString
            eqid = Request.QueryString("eqid").ToString
            eqnum = Request.QueryString("eqnum").ToString
            eqdesc = Request.QueryString("eqdesc").ToString
            ifmeter.Attributes.Add("src", "dialpms.aspx?dialid=" + dialid + "&dial=" + dial + "&desc=" + desc + "&eqid=" + eqid + "&eqdesc=" + eqdesc + "&eqnum=" + eqnum)
        End If
    End Sub

End Class