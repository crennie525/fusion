﻿Imports System.Data.SqlClient
Imports System.Text
Public Class dialmain
    Inherits System.Web.UI.Page
    Dim eca As New Utilities
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = "meterdials"
    Dim PK As String = "dialid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = "dialname desc"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eca.Open()
            getdials(PageNumber)
            eca.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                eca.Open()
                GetNext()
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                eca.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                getdials(PageNumber)
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                eca.Open()
                GetPrev()
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                eca.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                getdials(PageNumber)
                eca.Dispose()
                lblret.Value = "goback"
            ElseIf Request.Form("lblret") = "getdial" Then
                eca.Open()
                PageNumber = txtpg.Value
                getdials(PageNumber)
                getdial()
                eca.Dispose()
                lblret.Value = "godial"
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getdials(PageNumber)
        Catch ex As Exception
            eca.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getdials(PageNumber)
        Catch ex As Exception
            eca.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getdial()
        Dim dialid As String = lbldialid.Value
        Dim dname, ddesc As String
        sql = "select dialname, dialdesc from meterdials where dialid = '" & dialid & "'"
        dr = eca.GetRdrData(sql)
        While dr.Read
            dname = dr.Item("dialname").ToString
            ddesc = dr.Item("dialdesc").ToString
        End While
        dr.Close()
        lbldial.Value = dname
        lbldesc.Value = ddesc
    End Sub
    Private Sub getdials(ByVal PageNumber As Integer)
        sql = "select count(*) from meterdials"
        Dim intPgNav, intPgCnt As Integer
        intPgCnt = eca.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "50"
        intPgNav = eca.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If

        dr = eca.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        GridView1.DataSource = dr
        GridView1.DataBind()
        dr.Close()
    End Sub

    Private Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        GridView1.EditIndex = -1
        eca.Open()
        PageNumber = txtpg.Value
        getdials(PageNumber)
        eca.Dispose()
    End Sub

    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                Dim id As String = DataBinder.Eval(e.Row.DataItem, "dialid", "")
                Dim dialname As String = DataBinder.Eval(e.Row.DataItem, "dialname", "")
                Dim dialdesc As String = DataBinder.Eval(e.Row.DataItem, "dialdesc", "")
                Dim sid As String = DataBinder.Eval(e.Row.DataItem, "siteid", "")
                Dim jump As HtmlAnchor = CType(e.Row.FindControl("lnkdial"), HtmlAnchor)
                jump.Attributes("onclick") = "getdial('" & id & "','" & dialname & "','" & dialdesc & "','" & sid & "');"
                Dim jump2 As HtmlAnchor = CType(e.Row.FindControl("lnkdialper"), HtmlAnchor)
                jump2.Attributes("onclick") = "getdemand('" & id & "','" & dialname & "','" & dialdesc & "');"

                Dim deleteButton As ImageButton = CType(e.Row.FindControl("ImageButton1"), ImageButton)
                deleteButton.Attributes("onclick") = "javascript:return " & _
                "confirm('Are you sure you want to delete Dial " & _
                DataBinder.Eval(e.Row.DataItem, "dialname") & "?')"
            Catch ex As Exception
                'No way to tell if in Edit Mode?
                Try
                    Dim deleteButton As ImageButton = CType(e.Row.FindControl("ImageButton1"), ImageButton)
                    deleteButton.Attributes("onclick") = "javascript:return " & _
                    "confirm('Are you sure you want to delete Dial " & _
                    DataBinder.Eval(e.Row.DataItem, "dialname") & "?')"
                Catch ex1 As Exception

                End Try
            End Try
        End If
    End Sub

    Private Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        Dim index As Integer = Convert.ToInt32(e.RowIndex)
        Dim row As GridViewRow = GridView1.Rows(index)
        Dim dialid, dialp As String
        Try
            dialid = CType(row.FindControl("lbldialidi"), Label).Text
            dialp = CType(row.FindControl("lblpi"), Label).Text
        Catch ex As Exception
            dialid = CType(row.FindControl("lbldialide"), Label).Text
            dialp = CType(row.FindControl("lblpe"), Label).Text
        End Try
        Dim dint As Long
        Try
            dint = System.Convert.ToDecimal(dialp)
        Catch ex As Exception
            Dim strMessage As String = "Problem Converting Dial Percent"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        'If dint < 100 Then
        eca.Open()
        sql = "usp_dialpm_undodial '" & dialid & "'"
        eca.Update(sql)
        GridView1.EditIndex = -1
        PageNumber = txtpg.Value
        getdials(PageNumber)
        eca.Dispose()

        'Else

        'End If
    End Sub

    Private Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        PageNumber = txtpg.Value
        GridView1.EditIndex = e.NewEditIndex
        eca.Open()
        getdials(PageNumber)
        eca.Dispose()
    End Sub

    Private Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        Dim index As Integer = Convert.ToInt32(e.RowIndex)
        Dim row As GridViewRow = GridView1.Rows(index)
        Dim dialid, dialname, dialdesc, odial As String
        dialid = CType(row.FindControl("lbldialide"), Label).Text
        odial = CType(row.FindControl("lbldialnamee"), Label).Text
        dialname = CType(row.FindControl("txtdial"), TextBox).Text
        dialdesc = CType(row.FindControl("txtdesc"), TextBox).Text
        dialname = Replace(dialname, "'", Chr(180), , , vbTextCompare)
        dialdesc = Replace(dialdesc, "'", Chr(180), , , vbTextCompare)
        dialname = Replace(dialname, "#", "No.", , , vbTextCompare)
        dialdesc = Replace(dialdesc, "#", "No.", , , vbTextCompare)
        If Len(dialname) > 50 Then
            Dim strMessage As String = "Dial Name Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(dialdesc) > 100 Then
            Dim strMessage As String = "Dial Description Limited to 100 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eca.Open()
        Dim dcnt As Integer = 0
        If odial <> dialname Then
            sql = "select count(*) from meterdials where dialname = '" & dialname & "'"
            dcnt = eca.Scalar(sql)
        End If
        If dcnt = 0 Then
            sql = "update meterdials set dialname = '" & dialname & "', dialdesc = '" & dialdesc & "' where dialid = '" & dialid & "'"
            eca.Update(sql)
            GridView1.EditIndex = -1
            PageNumber = txtpg.Value
            getdials(PageNumber)
        Else
            Dim strMessage As String = "Revised Dial Name is Already in Use"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If



        eca.Dispose()

    End Sub
End Class