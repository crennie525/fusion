﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dialadd2.aspx.vb" Inherits="lucy_r12.dialadd2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Location</title>
<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function checkret() {
            var ret = document.getElementById("lblret").value;
            if (ret == "dup") {
                window.returnValue = "dup";
                window.close();
            }
            else {

            }
        }
        function checkrb(who) {
            if (who == "rbd") {
                dodept();
            }
            else if (who == "rbl") {
                doloc();
            }
            else {
                var dialid = document.getElementById("lblid").value;
                window.returnValue = dialid;
                window.close();
            }
        }
        function goback() {
            var dialid = document.getElementById("lblid").value;
            window.returnValue = dialid;
            window.close();
        }
        function dodept() {
            var sid = document.getElementById("lblsid").value;
            var dialid = document.getElementById("lblid").value;
            var dial = document.getElementById("lbldial").value;
            var desc = document.getElementById("lbldesc").value;
            var eReturn = window.showModalDialog("dialdeptdialog.aspx?sid=" + sid + "&dialid=" + dialid, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~")
                var typ = ret[0];
                var did = ret[1];
                if (typ == "cells") {
                    var cid = ret[2];
                }
                if (typ == "depts") {
                    var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=old&typ=depts&sid=" + sid + "&did=" + did + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                    if (eReturn2) {
                        if (eReturn == "ok") {
                            var decision = confirm("Add Records From Another Department or Cell?");
                            if (decision == true) {
                                dodept();
                            }
                            else {
                                window.returnValue = dialid;
                                window.close();
                            }
                        }
                    }
                }
                else if (typ == "cells") {
                    var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=old&typ=cells&sid=" + sid + "&did=" + did + "&cid=" + cid + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                    if (eReturn2) {
                        if (eReturn == "ok") {
                            var decision = confirm("Add Records From Another Department or Cell?");
                            if (decision == true) {
                                dodept();
                            }
                            else {
                                window.returnValue = dialid;
                                window.close();
                            }
                        }
                    }
                }


            }
        }
        function doloc() {
            var sid = document.getElementById("lblsid").value;
            var dialid = document.getElementById("lblid").value;
            var dial = document.getElementById("lbldial").value;
            var desc = document.getElementById("lbldesc").value;
            var eReturn = window.showModalDialog("diallocdialog.aspx?sid=" + sid + "&dialid=" + dialid, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                var locids = eReturn;
                var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=old&typ=locs&sid=" + sid + "&locid=" + locids + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn2) {
                    if (eReturn == "ok") {
                        var decision = confirm("Add Records From Another Location?");
                        if (decision == true) {
                            doloc();
                        }
                        else {
                            window.returnValue = "go";
                            window.close();
                        }
                    }
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
     <td class="label" height="22">Dial ID</td>
     <td id="tdid" runat="server" class="plainlabel"></td>
    </tr>
    <tr>
     <td class="label" height="22">Dial Name</td>
     <td id="tddial" runat="server" class="plainlabel"></td>
    </tr>
    <tr>
     <td class="label" height="22">Dial Description</td>
     <td id="tddesc" runat="server" class="plainlabel"></td>
    </tr>
    <tr>
    <td class="bluelabel" colspan="2" height="22"><input type="radio" id="rbd" name="rbcoll" onclick="checkrb('rbd');" />Use Departments</td>
    </tr>
    <tr>
    <td class="bluelabel" colspan="2" height="22"><input type="radio" id="rbl" name="rbcoll" onclick="checkrb('rbl');" />Use Locations</td>
    </tr>
     <tr>
    <td class="plainlabelblue" colspan="2" height="22" align="right"><a href="#" onclick="goback();">Return</a></td>
    </tr>
    </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblid" runat="server" />
    <input type="hidden" id="lbldial" runat="server" />
    <input type="hidden" id="lbldesc" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbldialid" runat="server" />
    </form>
</body>
</html>
