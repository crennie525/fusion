﻿Public Class variproddialog
    Inherits System.Web.UI.Page
    Dim dial, desc, dialid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            dialid = Request.QueryString("dialid").ToString
            dial = Request.QueryString("dial").ToString
            desc = Request.QueryString("desc").ToString
            ifmeter.Attributes.Add("src", "variprod.aspx?dialid=" + dialid + "&dial=" + dial + "&desc=" + desc)
        End If
    End Sub

End Class