﻿Imports System.Data.SqlClient
Imports System.Text
Public Class dialeq
    Inherits System.Web.UI.Page
    Dim di As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim dial, desc, dialid, sid, did, cid, locid, typ, eqid, eqnum, eqdesc, who As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            dialid = Request.QueryString("dialid").ToString
            lblid.Value = dialid
            dial = Request.QueryString("dial").ToString
            lbldial.Value = dial
            desc = Request.QueryString("desc").ToString
            lbldesc.Value = desc
            tdname.InnerHtml = dial
            tddesc.InnerHtml = desc
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            di.Open()
            If typ = "depts" Or typ = "cells" Then
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                If typ = "cells" Then
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    getcelleq(PageNumber, did, cid, who)
                Else
                    getdepteq(PageNumber, did, who)
                End If
            ElseIf typ = "locs" Then
                locid = Request.QueryString("locid").ToString
                lbllocid.Value = locid
                getloceq(PageNumber, locid, who)
            End If
            di.Dispose()
        Else
            typ = lbltyp.Value
            If Request.Form("lblret") = "next" Then
                di.Open()
                GetNext()
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                di.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                If typ = "depts" Then
                    who = lblwho.Value
                    did = lbldid.Value
                    getdepteq(PageNumber, did, who)
                ElseIf typ = "cells" Then
                    did = lbldid.Value
                    cid = lblcid.Value
                    who = lblwho.Value
                    getcelleq(PageNumber, did, cid, who)
                ElseIf typ = "locs" Then
                    locid = lbllocid.Value
                    who = lblwho.Value
                    getloceq(PageNumber, locid, who)
                End If
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                di.Open()
                GetPrev()
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                di.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                If typ = "depts" Then
                    did = lbldid.Value
                    who = lblwho.Value
                    getdepteq(PageNumber, did, who)
                ElseIf typ = "cells" Then
                    did = lbldid.Value
                    cid = lblcid.Value
                    who = lblwho.Value
                    getcelleq(PageNumber, did, cid, who)
                ElseIf typ = "locs" Then
                    locid = lbllocid.Value
                    who = lblwho.Value
                    getloceq(PageNumber, locid, who)
                End If
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "getall" Then
                di.Open()
                getall(typ)
                di.Dispose()
            ElseIf Request.Form("lblret") = "getchecked" Then
                di.Open()
                getchecked(typ)
                di.Dispose()
            End If
            dial = lbldial.Value
            desc = lbldesc.Value
            tdname.InnerHtml = dial
            tddesc.InnerHtml = desc
        End If
    End Sub
    Private Sub getdial(ByVal dialid)

    End Sub
    Private Sub getall(ByVal typ As String)
        dialid = lblid.Value
        did = lbldid.Value
        cid = lblcid.Value
        locid = lbllocid.Value
        If typ = "depts" Then
            sql = "usp_adddialeqall 'dept','" & did & "','" & cid & "','" & locid & "','" & dialid & "'"
        ElseIf typ = "cells" Then
            sql = "usp_adddialeqall 'cell','" & did & "','" & cid & "','" & locid & "','" & dialid & "'"
        Else
            sql = "usp_adddialeqall 'loc','" & did & "','" & cid & "','" & locid & "','" & dialid & "'"
        End If
        di.Update(sql)
        lblret.Value = "return"
    End Sub
    Private Sub getchecked(ByVal typ As String)
        dialid = lblid.Value
        did = lbldid.Value
        cid = lblcid.Value
        locid = lbllocid.Value
        Dim estr As String = lbleqinstr.Value
        If estr <> "" Then
            If typ = "depts" Then
                sql = "usp_adddialeqpick 'dept','" & did & "','" & cid & "','" & locid & "','" & dialid & "','" & estr & "'"
            ElseIf typ = "cells" Then
                sql = "usp_adddialeqpick 'cell','" & did & "','" & cid & "','" & locid & "','" & dialid & "','" & estr & "'"
            Else
                sql = "usp_adddialeqpick 'loc','" & did & "','" & cid & "','" & locid & "','" & dialid & "','" & estr & "'"
            End If
            di.Update(sql)
            lblret.Value = "return"
        Else
            lblret.Value = ""
        End If
        
    End Sub
    Private Sub GetNext()
        typ = lbltyp.Value
        who = lblwho.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            If typ = "depts" Then
                did = lbldid.Value
                getdepteq(PageNumber, did, who)
            ElseIf typ = "cells" Then
                did = lbldid.Value
                cid = lblcid.Value
                getcelleq(PageNumber, did, cid, who)
            ElseIf typ = "locs" Then
                locid = lbllocid.Value
                getloceq(PageNumber, locid, who)
            End If
        Catch ex As Exception
            di.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        typ = lbltyp.Value
        who = lblwho.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            If typ = "depts" Then
                did = lbldid.Value
                getdepteq(PageNumber, did, who)
            ElseIf typ = "cells" Then
                did = lbldid.Value
                cid = lblcid.Value
                getcelleq(PageNumber, did, cid, who)
            ElseIf typ = "locs" Then
                locid = lbllocid.Value
                getloceq(PageNumber, locid, who)
            End If
        Catch ex As Exception
            di.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getdepteq(ByVal PageNumber As Integer, ByVal did As String, ByVal who As String)
        Dim intPgNav, intPgCnt As Integer
        dialid = lblid.Value
        If who = "new" Then
            sql = "select count(*) from equipment where dept_id = '" & did & "'"
        Else
            sql = "select count(*) from equipment where dept_id = '" & did & "' and eqid not in (select eqid from meterdialeq where dialid = '" & dialid & "')"
        End If

        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim cid As Integer = 0
        Dim cbib As String = ""
        Dim ein As String = lbleqinstr.Value
        Dim sb As New StringBuilder
        sb.Append("<table>")
        If who = "new" Then
            sql = "usp_getalldialeqpg '" & did & "','" & PageNumber & "','" & PageSize & "','dept',''"
        Else
            sql = "usp_getalldialeqpg '" & did & "','" & PageNumber & "','" & PageSize & "','dept','" & dialid & "'"
        End If

        dr = di.GetRdrData(sql)
        While dr.Read
            cid += 1
            cbib = "cb" & cid
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            sb.Append("<tr>")
            If ein <> "" Then
                Dim ei As Integer = -1
                ei = ein.IndexOf(eqid)
                If ei <> -1 Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                End If
            Else
                sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
            End If

            sb.Append("<td class=""plainlabel"">" & eqnum & "</td>")
            sb.Append("<td class=""plainlabel"">" & eqdesc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub getcelleq(ByVal PageNumber As Integer, ByVal did As String, ByVal cid As String, ByVal who As String)
        Dim intPgNav, intPgCnt As Integer
        dialid = lblid.Value
        If who = "new" Then
            sql = "select count(*) from equipment where cellid = '" & cid & "'"
        Else
            sql = "select count(*) from equipment where cellid = '" & cid & "' and eqid not in (select eqid from meterdialeq where dialid = '" & dialid & "')"
        End If

        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim cidi As Integer = 0
        Dim cbib As String = ""
        Dim ein As String = lbleqinstr.Value
        Dim sb As New StringBuilder
        sb.Append("<table>")
        If who = "new" Then
            sql = "usp_getalldialeqpg '" & cid & "','" & PageNumber & "','" & PageSize & "','cell',''"
            'sql = "usp_getalldialeqpg '" & cid & "','" & PageNumber & "','" & PageSize & "','cell','" & dialid & "'"
        Else
            sql = "usp_getalldialeqpg '" & cid & "','" & PageNumber & "','" & PageSize & "','cell','" & dialid & "'"
        End If

        dr = di.GetRdrData(sql)
        While dr.Read
            cidi += 1
            cbib = "cb" & cidi
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            sb.Append("<tr>")
            If ein <> "" Then
                Dim ei As Integer = -1
                ei = ein.IndexOf(eqid)
                If ei <> -1 Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                End If
            Else
                sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
            End If

            sb.Append("<td class=""plainlabel"">" & eqnum & "</td>")
            sb.Append("<td class=""plainlabel"">" & eqdesc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString

    End Sub
    Private Sub getloceq(ByVal PageNumber As Integer, ByVal locid As String, ByVal who As String)
        Dim intPgNav, intPgCnt As Integer
        dialid = lblid.Value
        If who = "new" Then
            sql = "select count(*) from equipment where locid in (" & locid & ")"
        Else
            sql = "select count(*) from equipment where locid in (" & locid & ") and eqid not in (select eqid from meterdialeq where dialid = '" & dialid & "')"
        End If

        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim cid As Integer = 0
        Dim cbib As String = ""
        Dim ein As String = lbleqinstr.Value
        Dim sb As New StringBuilder
        sb.Append("<table>")
        If who = "new" Then
            sql = "usp_getalldialeqpg '" & locid & "','" & PageNumber & "','" & PageSize & "','loc',''"
        Else
            sql = "usp_getalldialeqpg '" & locid & "','" & PageNumber & "','" & PageSize & "','loc','" & dialid & "'"
        End If

        dr = di.GetRdrData(sql)
        While dr.Read
            cid += 1
            cbib = "cb" & cid
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            sb.Append("<tr>")
            If ein <> "" Then
                Dim ei As Integer = -1
                ei = ein.IndexOf(eqid)
                If ei <> -1 Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                End If
            Else
                sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
            End If

            sb.Append("<td class=""plainlabel"">" & eqnum & "</td>")
            sb.Append("<td class=""plainlabel"">" & eqdesc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString
    End Sub
End Class