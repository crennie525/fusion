﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dialeq.aspx.vb" Inherits="lucy_r12.dialeq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function addeq(eid, cbid) {
            var eqstr = document.getElementById("lbleqinstr").value;
            var eqstro = document.getElementById("lbleqoutstr").value;
            var cb = document.getElementById(cbid);
            if (cb.checked == true) {
                if (eqstr == "") {
                    eqstr = eid;
                }
                else {
                    eqstr += "," + eid;
                }
            }
            else {
                eqstr = eqstr.replace("," + eid, "");
                eqstr = eqstr.replace(eid, "");
                /*
                if (eqstro == "") {
                    eqstro = eid;
                }
                else {
                    eqstro += "," + eid;
                }
                */
            }
            document.getElementById("lbleqinstr").value = eqstr;
        }

        function getall() {
            document.getElementById("lbleqinstr").value = "";
            document.getElementById("lblret").value = "getall";
            document.getElementById("form1").submit();

        }
        function getchecked() {
            document.getElementById("lblret").value = "getchecked";
            document.getElementById("form1").submit();
        }
        function checkret() {
            var chk = document.getElementById("lblret").value;
            if (chk == "return") {
                window.parent.handlereturn();
            }
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    
    <div>
    <table>
    <tr>
    <td>
    <table>
    <tr>
    <td class="bluelabel">Dial Name</td>
    <td class="plainlabel" id="tdname" runat="server"></td>
    </tr>
    <tr>
    <td class="bluelabel">Dial Description</td>
    <td class="plainlabel" id="tddesc" runat="server"></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td class="thdrsing label">Equipment Records</td>
    </tr>
    <tr>
    <td class="plainlabel"><a href="#" onclick="getall();">Select All and Submit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="getchecked();">Submit Checked Records Only</a></td>
    </tr>
    <tr>
    <td>
    <div style="WIDTH: 450px; HEIGHT: 400px; OVERFLOW: auto; border: 1px solid gray" id="spdiv" runat="server">
    </div>
    </td>
    </tr>
    <tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
    </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblid" runat="server" />
    <input type="hidden" id="lbldial" runat="server" />
    <input type="hidden" id="lbldesc" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblcid" runat="server" />
    <input type="hidden" id="lbllocid" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lbleqinstr" runat="server" />
    <input type="hidden" id="lbleqoutstr" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    </form>
</body>
</html>
