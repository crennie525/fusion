﻿
Public Class dialadd2
    Inherits System.Web.UI.Page
    Dim dial, desc, dialid, sid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            dial = Request.QueryString("dial").ToString
            lbldial.Value = dial
            dialid = Request.QueryString("dialid").ToString
            lblid.Value = dialid
            desc = Request.QueryString("desc").ToString
            lbldesc.Value = desc
            tdid.InnerHtml = dialid
            tddial.InnerHtml = dial
            tddesc.InnerHtml = desc
        End If
    End Sub

End Class