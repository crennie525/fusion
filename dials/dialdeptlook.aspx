﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dialdeptlook.aspx.vb" Inherits="lucy_r12.dialdeptlook" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function getecd1(did, dept, ok) {
            document.getElementById("lbldid").value = did;
            document.getElementById("lbldept").value = dept;
            if (ok == "yes") {
                document.getElementById("lblsubmit").value = "checkdept";
                document.getElementById("form1").submit();
            }
        }
        function getecd2(cid, cell, ok) {
            document.getElementById("lblcid").value = cid;
            document.getElementById("lblcell").value = cell;
            document.getElementById("tdecd2").innerHTML = cell;
            if (ok == "yes") {
                document.getElementById("lblsubmit").value = "checkcell";
                //document.getElementById("form1").submit();
            }
        }
        function getecd3(eqid, eq, ok) {
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("lbleq").value = eq;
            document.getElementById("tdecd3").innerHTML = eq;
            if (ok == "yes") {
                document.getElementById("lblsubmit").value = "checkeq";
                document.getElementById("form1").submit();
            }
        }
        function getecd4(fuid, fu, ok) {
            document.getElementById("lblfuid").value = fuid;
            document.getElementById("lblfu").value = fu;
            document.getElementById("tdecd4").innerHTML = fu;
            if (ok == "yes") {
                document.getElementById("lblsubmit").value = "checkfu";
                document.getElementById("form1").submit();
            }
        }
        function getecd5(comid, comp) {
            document.getElementById("lblcomid").value = comid;
            document.getElementById("lblcomp").value = comp;
            document.getElementById("tdecd5").innerHTML = comp;
            //document.getElementById("lblsubmit").value="checkfu";
            //document.getElementById("form1").submit();
        }
        function getecd6(ncid, nc, ok) {
            document.getElementById("lblncid").value = ncid;
            document.getElementById("lblnc").value = nc;
            document.getElementById("tdecd6").innerHTML = nc;
            //document.getElementById("lblsubmit").value="checkfu";
            //document.getElementById("form1").submit();
        }
        function refit() {
            var sid = document.getElementById("lblsid").value;
            var wo = document.getElementById("lblwonum").value;
            var typ = document.getElementById("lbltyp").value;
            var id = document.getElementById("lblid").value;

            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblcid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var who = document.getElementById("lblwho").value;

            window.location = "dialdeptlook.aspx?typ=" + typ + "&sid=" + sid + "&dialid=" + id + "&did=" + did + "&dept=" + dept + "&clid=" + clid + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&loc=" + loc + "&who=" + who + "&date=" + Date();
        }
        function retit() {
            var did = document.getElementById("lbldid").value;
            if (did != "") {
                document.getElementById("lblsubmit").value = "savedial";
                document.getElementById("form1").submit();
            }
        }

        function goback() {
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var cid = document.getElementById("lblcid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            var fu = document.getElementById("lblfu").value;
            var comid = document.getElementById("lblcomid").value;
            var comp = document.getElementById("lblcomp").value;
            var ncid = document.getElementById("lblncid").value;
            var nc = document.getElementById("lblnc").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var ret;
            ret = did + "~" + dept + "~" + cid + "~" + cell + "~" + eqid + "~" + eq + "~" + fuid + "~" + fu + "~" + comid + "~" + comp + "~" + ncid + "~" + nc + "~" + lid + "~" + loc;
            //alert(ret)
            window.parent.handlereturn(ret);
        }

        function checkit() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "return") {
                //goback();
                var ret = document.getElementById("lblret").value;
                window.parent.handlereturn(ret);
            }
        }
        function gettab(who) {

            var chk = document.getElementById("lbltabs").value;
            var chkarr = chk.split(",");
            if (who == "dept") {
                closeall();
                closedepts();
                document.getElementById("tdec").className = "view";
                document.getElementById("tdtab").innerHTML = "Departments";
            }
            else if (who == "cell") {
                if (chkarr[1] == "ok") {
                    closeall();
                    closecells();
                    document.getElementById("tdec1").className = "view";
                    document.getElementById("tdtab").innerHTML = "Stations\Cells";
                }
            }
            else if (who == "eq") {
                if (chkarr[2] == "ok") {
                    closeall();
                    closeeq();
                    document.getElementById("tdec2").className = "view";
                    document.getElementById("tdtab").innerHTML = "Equipment";
                }
            }
            else if (who == "fu") {
                if (chkarr[3] == "ok") {
                    closeall();
                    closefu();
                    document.getElementById("tdec3").className = "view";
                    document.getElementById("tdtab").innerHTML = "Functions";
                }
            }
            else if (who == "co") {
                if (chkarr[4] == "ok") {
                    closeall();

                    document.getElementById("tdec4").className = "view";
                    document.getElementById("tdtab").innerHTML = "Components";
                }
            }
            else if (who == "nc") {
                if (chkarr[5] == "ok") {
                    var nc = document.getElementById("lblnccnt").value;
                    if (nc != 0) {
                        closeall();
                        document.getElementById("tdec5").className = "view";
                        document.getElementById("tdtab").innerHTML = "Misc Assets";
                    }
                }
            }
        }
        function closedepts() {
            document.getElementById("lblcid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdecd2").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdecd3").innerHTML = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdecd4").innerHTML = "";
            document.getElementById("lblcomid").value = "";
            document.getElementById("lblcomp").value = "";
            document.getElementById("tdecd5").innerHTML = "";
            document.getElementById("lblncid").value = "";
            document.getElementById("lblnc").value = "";
            document.getElementById("tdecd6").innerHTML = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc").innerHTML = "";
        }
        function closecells() {
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("tdecd3").innerHTML = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdecd4").innerHTML = "";
            document.getElementById("lblcomid").value = "";
            document.getElementById("lblcomp").value = "";
            document.getElementById("tdecd5").innerHTML = "";
            document.getElementById("lblncid").value = "";
            document.getElementById("lblnc").value = "";
            document.getElementById("tdecd6").innerHTML = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc").innerHTML = "";
        }
        function closeeq() {
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblfu").value = "";
            document.getElementById("tdecd4").innerHTML = "";
            document.getElementById("lblcomid").value = "";
            document.getElementById("lblcomp").value = "";
            document.getElementById("tdecd5").innerHTML = "";
        }
        function closefu() {
            document.getElementById("lblcomid").value = "";
            document.getElementById("lblcomp").value = "";
            document.getElementById("tdecd5").innerHTML = "";
        }

        function closeall() {
            document.getElementById("tdec").className = "details";
            document.getElementById("tdec1").className = "details";
            document.getElementById("tdec1a").className = "details";
            document.getElementById("tdtab2").className = "details";
            document.getElementById("tdec2").className = "details";
            document.getElementById("tdec3").className = "details";
            document.getElementById("tdec4").className = "details";
            document.getElementById("tdec5").className = "details";
        }
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="plainlabelred" id="tdmsg" runat="server" align="center">
            </td>
        </tr>
        <tr>
            <td id="tdtab" class="label" runat="server">
                Departments
            </td>
             <td id="tdtab2" class="details" runat="server">
                Equipment No Cells
            </td>
        </tr>
        <tr>
            <td id="tdec" runat="server">
            </td>
            <td id="tdec1" class="details" runat="server">
            </td>
            <td id="tdec1a" class="details" runat="server">
            </td>
            <td id="tdec2" class="details" runat="server">
            </td>
            <td id="tdec3" class="details" runat="server">
            </td>
            <td id="tdec4" class="details" runat="server">
            </td>
            <td id="tdec5" class="details" runat="server">
            </td>
            <td id="tdec6" class="details" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="bluelabel" height="20" width="80">
                            <a onclick="gettab('dept');" href="#">Department</a>
                        </td>
                        <td id="tdecd1" class="plainlabel" width="150" runat="server">
                        </td>
                        <td width="20">
                            <img onclick="refit();" src="../images/appbuttons/minibuttons/refreshit.gif">
                        </td>
                        
                    </tr>
                    <tr>
                        <td class="bluelabel" height="20">
                            <a onclick="gettab('cell');" href="#">Station\Cell</a>
                        </td>
                        <td id="tdecd2" class="plainlabel" runat="server">
                        </td>
                    </tr>
                    
                    
                    <tr id="trsave" class="details" runat="server">
                        <td colspan="3" align="right">
                            <img onclick="retit();" src="../images/appbuttons/minibuttons/savedisk1.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server">
    <input id="lblecd1" type="hidden" name="lblecd1" runat="server">
    <input id="lblecd2" type="hidden" name="lblecd2" runat="server">
    <input id="lblecd3" type="hidden" name="lblecd3" runat="server">
    <input id="lblsid" type="hidden" runat="server">
    <input id="lbldid" type="hidden" runat="server">
    <input id="lbldept" type="hidden" runat="server">
    <input id="lblcid" type="hidden" runat="server">
    <input id="lblcell" type="hidden" runat="server">
    <input id="lblchk" type="hidden" runat="server">
    <input id="lblpar2" type="hidden" runat="server">
    <input id="lbleqid" type="hidden" runat="server">
    <input id="lbleq" type="hidden" runat="server">
    <input id="lblfuid" type="hidden" runat="server">
    <input id="lblfu" type="hidden" runat="server"><input id="lblcomp" type="hidden"
        runat="server">
    <input id="lblcomid" type="hidden" runat="server"><input id="lbltabs" type="hidden"
        runat="server">
    <input id="lbllid" type="hidden" runat="server">
    <input id="lblloc" type="hidden" runat="server">
    <input id="lblncid" type="hidden" runat="server">
    <input id="lblnc" type="hidden" runat="server">
    <input id="lblnccnt" type="hidden" runat="server">
    <input type="hidden" id="lblwonum" runat="server">
    <input type="hidden" id="lbltyp" runat="server">
    <input type="hidden" id="lbljpid" runat="server" />
    <input type="hidden" id="lblid" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />

    </form>
</body>
</html>

