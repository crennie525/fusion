﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="diallocs.aspx.vb" Inherits="lucy_r12.diallocs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    .plainlabelredu
{
	font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:12px;
	text-decoration:underline;
	color:red;
}
    </style>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function addloc() {
        //alert()
            var dialid = document.getElementById("lblid").value;
            var dial = document.getElementById("lbldial").value;
            //dial = modstring(dial);
            var desc = document.getElementById("lbldesc").value;
            //desc = modstring(desc);
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("dialadd2.aspx?sid=" + sid + "&dial=" + dial + "&desc=" + desc + "&dialid=" + dialid, "", "dialogHeight:400px; dialogWidth:500px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                var dialid = eReturn;
                if (dialid != "") {
                    document.getElementById("lbldialid").value = dialid;
                    document.getElementById("lblret").value = "first";
                    document.getElementById("form1").submit();
                }
            }
        }
        function geteq(who, id1, id2, n1, n2) {

            var ret = who + "," + id1 + "," + id2 + "," + n1 + "," + n2;
            window.parent.handleloc(ret);
        }
        function addeq(eid, cbid) {
            var eqstr = document.getElementById("lbleqinstr").value;
            var eqstro = document.getElementById("lbleqoutstr").value;
            var cb = document.getElementById(cbid);
            if (cb.checked == true) {
                if (eqstr == "") {
                    eqstr = eid;
                }
                else {
                    eqstr += "," + eid;
                }
            }
            else {
                eqstr = eqstr.replace("," + eid, "");
                eqstr = eqstr.replace(eid, "");
                /*
                if (eqstro == "") {
                eqstro = eid;
                }
                else {
                eqstro += "," + eid;
                }
                */
            }
            document.getElementById("lbleqinstr").value = eqstr;
        }

        function getall() {
            //document.getElementById("lbleqinstr").value = "";
            document.getElementById("lblret").value = "getall";
            document.getElementById("form1").submit();

        }

        function getneweq(typ, id1, id2) {
        //alert(typ)
            var sid = document.getElementById("lblsid").value;
            var dialid = document.getElementById("lblid").value;
            var dial = document.getElementById("lbldial").value;
            var desc = document.getElementById("lbldesc").value;
            if (typ == "depts") {
            did = id1
                var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=old&typ=depts&sid=" + sid + "&did=" + did + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn2) {
                    //alert(eReturn2)
                    if (eReturn2 == "ok") {
                        window.parent.handlereset();
                    }
                }
            }
            else if (typ == "cells") {
            //alert("cells")
                did = id1
                cid = id2
                var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=old&typ=cells&sid=" + sid + "&did=" + did + "&cid=" + cid + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn2) {
                //alert(eReturn2)
                    if (eReturn2 == "ok") {
                        window.parent.handlereset();
                    }
                }
            }
            else if (typ == "locs") {
            locids = id1
                var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=old&typ=locs&sid=" + sid + "&locid=" + locids + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn2) {
                    //alert(eReturn2)
                    if (eReturn2 == "ok") {
                        window.parent.handlereset();
                    }
                }
            }
        }
        function checkret() {
            var ret = document.getElementById("lblret").value;
            if (ret == "goback") {
                window.parent.handlereset();
            }
        }
        </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div style="position: absolute; top: 0px; left: 0px;">
    <table>
    <tr>
    <td class="plainlabelblue"><a href="#" class="plainlabelredu" onclick="getall();">Delete Selected</a>&nbsp;&nbsp;&nbsp;<a href="#" onclick="addloc();">Add a Location</a>&nbsp;&nbsp;&nbsp;</td>
    </tr>
    <tr>
    <td>
    <div style="WIDTH: 430px; HEIGHT: 70px; OVERFLOW: auto; border: 1px solid gray" id="spdiv" runat="server">
    </div>
    </td>
    </tr>
    <tr>
    <td class="plainlabelblue" align="center">*Click Location to Filter Equipment Records Below</td>
    </tr>
    <tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>

    </table>
    </div>
     <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblid" runat="server" />
    <input type="hidden" id="lbldial" runat="server" />
    <input type="hidden" id="lbldesc" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblcid" runat="server" />
    <input type="hidden" id="lbllocid" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lbleqinstr" runat="server" />
    <input type="hidden" id="lbleqoutstr" runat="server" />
    <input type="hidden" id="lbldialid" runat="server" />
    <input type="hidden" id="lblnormdle" runat="server" />
    <input type="hidden" id="lblnormdri" runat="server" />
    </form>
</body>
</html>
