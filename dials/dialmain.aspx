﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dialmain.aspx.vb" Inherits="lucy_r12.dialmain" %>

<%@ Register src="../menu/mmenu1.ascx" tagname="mmenu1" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function getnew() {
            var id = document.getElementById("txtnewecd").value;
            var desc = document.getElementById("txtnewdesc").value;
            if (id != "" && desc != "") {
                var eReturn = window.showModalDialog("ecadddialog.aspx?ecd1=" + id + "&e1desc=" + desc, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    document.getElementById("txtsearch").value = id;
                    document.getElementById("lblret").value = "first"
                    document.getElementById("form1").submit();
                }
            }
            else {
                alert("Error Code and Description Required")
            }
        }
        function newdial() {
            var dial = document.getElementById("txtdial").value;
            dial = modstring(dial);
            var desc = document.getElementById("txtdesc").value;
            desc = modstring(desc);
            var sid = "12"; // document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("dialadd.aspx?sid=" + sid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:400px; dialogWidth:500px; resizable=yes");
            if (eReturn) {
                var dialid = eReturn;
                if (dialid != "") {
                    document.getElementById("lbldialid").value = dialid;
                    document.getElementById("lblret").value = "getdial";
                    document.getElementById("form1").submit();
                }
            }
            else {
                document.getElementById("lblret").value = "getdial";
                document.getElementById("form1").submit();
            }
        }
        function getdemand(id, dname, ddesc) {
            //alert(id)
            document.getElementById("lbldialid").value = id;
            document.getElementById("lbldial").value = dname;
            document.getElementById("lbldesc").value = ddesc;
        var eReturn = window.showModalDialog("variproddialog.aspx?dialid=" + id + "&dial=" + dname + "&desc=" + ddesc, "", "dialogHeight:600px; dialogWidth:600px; resizable=yes");
        if (eReturn) {
            //alert(eReturn)
            document.getElementById("lbldialid").value = id;
            document.getElementById("lblret").value = "getdial";
            document.getElementById("form1").submit();
        }
        }

        function modstring(who) {
            who = who.replace("#", "No.");
            who = who.replace("'", "`");
            return who;
        }
        function GetHeight() {
            var y = 0;
            if (self.innerHeight) {
                y = self.innerHeight;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                y = document.documentElement.clientHeight;
            }
            else if (document.body) {
                y = document.body.clientHeight;
            }
            return y;
        }
        var flag = 5
        function upsize(who) {
            if (who != flag) {
                flag = who;
                document.getElementById("ifmeter").height = GetHeight();
                window.scrollTo(0, top);
                //alert(who)
            }
            //genhold.htm

        }
        function getdial(id, dname, ddesc, sid) {
            document.getElementById("ifmeter").src = "dialsub.aspx?typ=all&sid=" + sid + "&dialid=" + id + "&dial=" + dname + "&desc=" + ddesc;
        }
        function checkret() {
            var ret = document.getElementById("lblret").value;
            if (ret == "goback") {
                document.getElementById("lblret").value = "";
                document.getElementById("ifmeter").src = "genhold.htm";
            }
            else if (ret == "godial") {
                var id = document.getElementById("lbldialid").value;
                var dname = document.getElementById("lbldial").value;
                var ddesc = document.getElementById("lbldesc").value;
                var sid = document.getElementById("lblsid").value;
                getdial(id, dname, ddesc, sid);
            }
        }
        //
    </script>
</head>
<body onload="upsize('1');checkret();">
<uc1:mmenu1 ID="mmenu11" runat="server" />
    <form id="form1" runat="server">
    <div style="Z-INDEX: 1; LEFT: 0px; POSITION: absolute; TOP: 76px">
    <table>
     <tr>
    <td class="thdrsing label" width="570">Variable Production Dials</td>
    <td class="thdrsing label" width="470">Dial Details</td>
    </tr>
    <tr>
    <td valign="top">
    <table>
    <tr>
    <td class="bluelabel" height="22">New Dial Name</td>
    <td><asp:TextBox ID="txtdial" runat="server"></asp:TextBox></td>  
    <td class="bluelabel">New Description</td>
    <td><asp:TextBox ID="txtdesc" runat="server"></asp:TextBox></td>
    <td><img src="../images/appbuttons/minibuttons/addmod.gif" alt="" onclick="newdial();" /></td>
    
    </tr>
    <tr>
    <td class="plainlabelblue" colspan="5" align="center" height="22">*PM Records for Removed Dials will be Reset to 100%</td>
    </tr>
    <tr>
    <td colspan="5">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
        <AlternatingRowStyle CssClass="ptransrowblue"></AlternatingRowStyle>
                        <RowStyle CssClass="ptransrow"></RowStyle>
                        <Columns>
                        <asp:TemplateField HeaderText="Edit">
                                <HeaderStyle Height="20px" Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton7" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dial Name">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <a href="#" ID="lnkdial" runat="server" >
                                    <%# DataBinder.Eval(Container, "DataItem.dialname")%>
                                    </a>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtdial" runat="server" Width="170px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.dialname") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dial Description">
                                <HeaderStyle Width="230px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                     <asp:Label ID="lbldesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dialdesc") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtdesc" runat="server" Width="250px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.dialdesc") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Percent">
                                <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <a href="#" ID="lnkdialper" runat="server" >
                                    <%# DataBinder.Eval(Container, "DataItem.dialpercent")%>
                                    </a>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblpercent" runat="server" Width="170px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.dialpercent") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbldialidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dialid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lbldialide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dialid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblsiteidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.siteid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblsiteide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.siteid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbldialnamei" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dialname") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lbldialnamee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dialname") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblpi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dialpercent") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblpe" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dialpercent") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderImageUrl="../images/appbuttons/minibuttons/del.gif" ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Height="20px" Width="30px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
											CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                               
                            </asp:TemplateField>
                        </Columns>
        </asp:GridView>
        </td>
    </tr>
     <tr>
					<td align="center"  colspan="5">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
    </table>
    </td>
    <td rowspan="4">
    <iframe id="ifmeter" src="../genhold.htm" runat="server" width="100%" height="100%" frameborder="0">
        </iframe>
    </td>
    </tr>
    
    </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblid" runat="server" />
    <input type="hidden" id="lbldial" runat="server" />
    <input type="hidden" id="lbldesc" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbldialid" runat="server" />
    <input id="txtpg" type="hidden" runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input id="spdivy" type="hidden" runat="server"/>
			<input id="xCoord" type="hidden"/><input id="yCoord" type="hidden"/>
    
    </form>
</body>
</html>
