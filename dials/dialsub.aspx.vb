﻿Imports System.Data.SqlClient
Imports System.Text
Public Class dialsub
    Inherits System.Web.UI.Page
    Dim di As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim dial, desc, dialid, sid, did, cid, locid, typ, eqid, eqnum, eqdesc, dname, cname, lname As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            dialid = Request.QueryString("dialid").ToString
            lblid.Value = dialid
            dial = Request.QueryString("dial").ToString
            lbldial.Value = dial
            desc = Request.QueryString("desc").ToString
            lbldesc.Value = desc
            tdname.InnerHtml = dial
            tddesc.InnerHtml = desc
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            ifmeter.Attributes.Add("src", "diallocs.aspx?sid=" + sid + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc)
            di.Open()
            getnorm()
            If typ = "depts" Then
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                dname = Request.QueryString("dname").ToString
                lbldname.Value = dname
                tdloc.InnerHtml = "Current Department: " & dname
                hadd.Disabled = False
                getdepteq(PageNumber, did, dialid)
            ElseIf typ = "cells" Then
                did = Request.QueryString("did").ToString
                cid = Request.QueryString("cid").ToString
                lbldid.Value = did
                lblcid.Value = cid
                dname = Request.QueryString("dname").ToString
                lbldname.Value = dname
                cname = Request.QueryString("cname").ToString
                lblcname.Value = cname
                tdloc.InnerHtml = "Current Department: " & dname & "  Cell: " & cname
                hadd.Disabled = False
                getcelleq(PageNumber, did, cid, dialid)
            ElseIf typ = "locs" Then
                locid = Request.QueryString("locid").ToString
                lbllocid.Value = locid
                lname = Request.QueryString("lname").ToString
                lbllname.Value = lname
                tdloc.InnerHtml = "Current Location: " & lname
                hadd.Disabled = False
                getloceq(PageNumber, locid, dialid)
            ElseIf typ = "all" Then
                dialid = lblid.Value
                tdloc.InnerHtml = "Current Location: ALL"
                hadd.Disabled = True
                getalleq(PageNumber, dialid)
            End If
            di.Dispose()
        Else
            typ = lbltyp.Value
            dialid = lblid.Value
            If Request.Form("lblret") = "next" Then
                di.Open()
                GetNext()
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                di.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                If typ = "depts" Then
                    did = lbldid.Value
                    getdepteq(PageNumber, did, dialid)
                ElseIf typ = "cells" Then
                    did = lbldid.Value
                    cid = lblcid.Value
                    getcelleq(PageNumber, did, cid, dialid)
                ElseIf typ = "locs" Then
                    locid = lbllocid.Value
                    getloceq(PageNumber, locid, dialid)
                ElseIf typ = "all" Then
                    dialid = lblid.Value
                    getalleq(PageNumber, dialid)
                End If
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                di.Open()
                GetPrev()
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                di.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                If typ = "depts" Then
                    did = lbldid.Value
                    getdepteq(PageNumber, did, dialid)
                ElseIf typ = "cells" Then
                    did = lbldid.Value
                    cid = lblcid.Value
                    getcelleq(PageNumber, did, cid, dialid)
                ElseIf typ = "locs" Then
                    locid = lbllocid.Value
                    getloceq(PageNumber, locid, dialid)
                ElseIf typ = "all" Then
                    dialid = lblid.Value
                    getalleq(PageNumber, dialid)
                End If
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "getall" Then
                di.Open()
                PageNumber = txtpg.Value
                deleq()
                If typ = "depts" Then
                    did = lbldid.Value
                    getdepteq(PageNumber, did, dialid)
                ElseIf typ = "cells" Then
                    did = lbldid.Value
                    cid = lblcid.Value
                    getcelleq(PageNumber, did, cid, dialid)
                ElseIf typ = "locs" Then
                    locid = lbllocid.Value
                    getloceq(PageNumber, locid, dialid)
                ElseIf typ = "all" Then
                    dialid = lblid.Value
                    getalleq(PageNumber, dialid)
                End If
                di.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub getnorm()
        sql = "select dleft, dright from meterdialvals where dialval = 100"
        Dim dp, dle, dri As String
        dr = di.GetRdrData(sql)
        While dr.Read
            dp = "100"
            dle = dr.Item("dleft").ToString
            dri = dr.Item("dright").ToString
        End While
        dr.Close()
        lblnormdle.Value = dle
        lblnormdri.Value = dri

    End Sub
    Private Sub deleq()
        Dim dp, dps, dle, dri As String
        Dim odle, odri As String
        odle = lblnormdle.Value
        odri = lblnormdri.Value
        Dim ein As String = lbleqinstr.Value
        dialid = lblid.Value
        Dim i As Integer
        Dim einarr() As String = ein.Split(",")
        For i = 0 To einarr.Length - 1

            Dim cmd0 As New SqlCommand
            cmd0.CommandText = "exec usp_dialpm_eq @dialid, @dp, @dps, @dle, @dri, @eqidin"
            Dim param010 = New SqlParameter("@dialid", SqlDbType.VarChar)
            If dialid = "" Then
                param010.Value = System.DBNull.Value
            Else
                param010.Value = dialid
            End If
            cmd0.Parameters.Add(param010)
            Dim param020 = New SqlParameter("@dp", SqlDbType.VarChar)
            If dp = "" Then
                param020.Value = System.DBNull.Value
            Else
                param020.Value = "100"
            End If
            cmd0.Parameters.Add(param020)
            Dim param030 = New SqlParameter("@dps", SqlDbType.VarChar)
            If dps = "" Then
                param030.Value = System.DBNull.Value
            Else
                param030.Value = "100"
            End If
            cmd0.Parameters.Add(param030)
            Dim param040 = New SqlParameter("@dle", SqlDbType.VarChar)
            If dle = "" Then
                param040.Value = System.DBNull.Value
            Else
                param040.Value = odle
            End If
            cmd0.Parameters.Add(param040)
            Dim param050 = New SqlParameter("@dri", SqlDbType.VarChar)
            If dri = "" Then
                param050.Value = System.DBNull.Value
            Else
                param050.Value = odri
            End If
            cmd0.Parameters.Add(param050)

            Dim param051 = New SqlParameter("@eqidin", SqlDbType.VarChar)
            If einarr(i) = "" Then
                param051.Value = System.DBNull.Value
            Else
                param051.Value = einarr(i)
            End If
            cmd0.Parameters.Add(param051)

            di.UpdateHack(cmd0)

            sql = "usp_dialpm_undodialeq '" & dialid & "','" & einarr(i) & "'"
            di.Update(sql)

        Next
    End Sub
    Private Sub GetNext()
        typ = lbltyp.Value
        dialid = lblid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            If typ = "depts" Then
                did = lbldid.Value
                getdepteq(PageNumber, did, dialid)
            ElseIf typ = "cells" Then
                did = lbldid.Value
                cid = lblcid.Value
                getcelleq(PageNumber, did, cid, dialid)
            ElseIf typ = "locs" Then
                locid = lbllocid.Value
                getloceq(PageNumber, locid, dialid)
            ElseIf typ = "all" Then
                dialid = lblid.Value
                getalleq(PageNumber, dialid)
            End If
        Catch ex As Exception
            di.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        typ = lbltyp.Value
        dialid = lblid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            If typ = "depts" Then
                did = lbldid.Value
                getdepteq(PageNumber, did, dialid)
            ElseIf typ = "cells" Then
                did = lbldid.Value
                cid = lblcid.Value
                getcelleq(PageNumber, did, cid, dialid)
            ElseIf typ = "locs" Then
                locid = lbllocid.Value
                getloceq(PageNumber, locid, dialid)
            ElseIf typ = "all" Then
                dialid = lblid.Value
                getalleq(PageNumber, dialid)
            End If
        Catch ex As Exception
            di.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getalleq(ByVal PageNumber As Integer, ByVal dialid As String)
        Dim intPgNav, intPgCnt As Integer
        sql = "select count(*) from meterdialeq where dialid = '" & dialid & "'"
        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            'Dim strMessage As String = "No Records Found"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim cid As Integer = 0
        Dim cbib As String = ""
        Dim ein As String = lbleqinstr.Value
        Dim sb As New StringBuilder
        sb.Append("<table>")
        sql = "usp_getdialeqpg_rvw '" & dialid & "','" & PageNumber & "','" & PageSize & "','all'"
        dr = di.GetRdrData(sql)
        While dr.Read
            cid += 1
            cbib = "cb" & cid
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            sb.Append("<tr>")
            If ein <> "" Then
                Dim ei As Integer = -1
                ei = ein.IndexOf(eqid)
                If ei <> -1 Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                End If
            Else
                sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
            End If
            sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getpm('" & eqid & "','" & eqnum & "','" & eqdesc & "');"">" & eqnum & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & eqdesc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub getdepteq(ByVal PageNumber As Integer, ByVal did As String, ByVal dialid As String)
        Dim intPgNav, intPgCnt As Integer
        sql = "select count(*) from meterdialeq where deptid = '" & did & "'"
        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim cid As Integer = 0
        Dim cbib As String = ""
        Dim ein As String = lbleqinstr.Value
        Dim sb As New StringBuilder
        sb.Append("<table>")
        sql = "usp_getdialeqpg_rvw '" & did & "','" & PageNumber & "','" & PageSize & "','dept'"
        dr = di.GetRdrData(sql)
        While dr.Read
            cid += 1
            cbib = "cb" & cid
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            sb.Append("<tr>")
            If ein <> "" Then
                Dim ei As Integer = -1
                ei = ein.IndexOf(eqid)
                If ei <> -1 Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                End If
            Else
                sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
            End If
            sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getpm('" & eqid & "','" & eqnum & "','" & eqdesc & "');"">" & eqnum & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & eqdesc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub getcelleq(ByVal PageNumber As Integer, ByVal did As String, ByVal cid As String, ByVal dialid As String)
        Dim intPgNav, intPgCnt As Integer
        sql = "select count(distinct cellid) from meterdialeq where cellid = '" & cid & "'"
        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim cidi As Integer = 0
        Dim cbib As String = ""
        Dim ein As String = lbleqinstr.Value
        Dim sb As New StringBuilder
        sb.Append("<table>")
        sql = "usp_getdialeqpg_rvw '" & cid & "','" & PageNumber & "','" & PageSize & "','cell'"
        dr = di.GetRdrData(sql)
        While dr.Read
            cidi += 1
            cbib = "cb" & cidi
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            sb.Append("<tr>")
            If ein <> "" Then
                Dim ei As Integer = -1
                ei = ein.IndexOf(eqid)
                If ei <> -1 Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                End If
            Else
                sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
            End If
            sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getpm('" & eqid & "','" & eqnum & "','" & eqdesc & "');"">" & eqnum & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & eqdesc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString

    End Sub
    Private Sub getloceq(ByVal PageNumber As Integer, ByVal locid As String, ByVal dialid As String)
        Dim intPgNav, intPgCnt As Integer
        sql = "select count(*) from meterdialeq where locid in (" & locid & ")"
        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim cid As Integer = 0
        Dim cbib As String = ""
        Dim ein As String = lbleqinstr.Value
        Dim sb As New StringBuilder
        sb.Append("<table>")
        sql = "usp_getdialeqpg_rvw '" & locid & "','" & PageNumber & "','" & PageSize & "','loc'"
        dr = di.GetRdrData(sql)
        While dr.Read
            cid += 1
            cbib = "cb" & cid
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            sb.Append("<tr>")
            If ein <> "" Then
                Dim ei As Integer = -1
                ei = ein.IndexOf(eqid)
                If ei <> -1 Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                Else
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                End If
            Else
                sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & eqid & "','" & cbib & "');"" id=""" & cbib & """></td>")
            End If
            sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getpm('" & eqid & "','" & eqnum & "','" & eqdesc & "');"">" & eqnum & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & eqdesc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString
    End Sub
End Class