﻿Imports System.Data.SqlClient
Imports System.Text
Public Class diallocs
    Inherits System.Web.UI.Page
    Dim di As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim dial, desc, dialid, sid, did, cid, locid, typ, eqid, eqnum, eqdesc As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            dialid = Request.QueryString("dialid").ToString
            lblid.Value = dialid
            dial = Request.QueryString("dial").ToString
            lbldial.Value = dial
            desc = Request.QueryString("desc").ToString
            lbldesc.Value = desc
            di.Open()
            getnorm()
            getalleq(PageNumber, dialid)
            di.Dispose()
        Else
            dialid = lblid.Value
            If Request.Form("lblret") = "next" Then
                di.Open()
                GetNext()
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                di.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber

                getalleq(PageNumber, dialid)
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                di.Open()
                GetPrev()
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                di.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                getalleq(PageNumber, dialid)
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "getall" Then
                di.Open()
                PageNumber = txtpg.Value
                deleq()
                di.Dispose()
                lblret.Value = "goback"
            End If
        End If
    End Sub
    Private Sub getnorm()
        sql = "select dleft, dright from meterdialvals where dialval = 100"
        Dim dp, dle, dri As String
        dr = di.GetRdrData(sql)
        While dr.Read
            dp = "100"
            dle = dr.Item("dleft").ToString
            dri = dr.Item("dright").ToString
        End While
        dr.Close()
        lblnormdle.Value = dle
        lblnormdri.Value = dri

    End Sub
    Private Sub deleq()
        Dim dp, dps, dle, dri As String
        Dim odle, odri As String
        odle = lblnormdle.Value
        odri = lblnormdri.Value
        Dim styp As String = ""
        Dim ein As String = lbleqinstr.Value
        dialid = lblid.Value
        Dim i As Integer
        Dim einarr() As String = ein.Split(",")
        For i = 0 To einarr.Length - 1
            Dim ftyp As String = einarr(i)
            Dim ftyparr() As String = ftyp.Split("_")
            Dim typ As String = ftyparr(0)
            If typ = "c" Then
                styp = "cell"
            ElseIf typ = "d" Then
                styp = "dept"
            ElseIf typ = "l" Then
                styp = "loc"
            End If

            Dim cmd0 As New SqlCommand
            cmd0.CommandText = "exec usp_dialpm_loc @dialid, @dp, @dps, @dle, @dri, @who, @did, @cid, @locid"
            Dim param010 = New SqlParameter("@dialid", SqlDbType.VarChar)
            If dialid = "" Then
                param010.Value = System.DBNull.Value
            Else
                param010.Value = dialid
            End If
            cmd0.Parameters.Add(param010)
            Dim param020 = New SqlParameter("@dp", SqlDbType.VarChar)
            If dp = "" Then
                param020.Value = System.DBNull.Value
            Else
                param020.Value = "100"
            End If
            cmd0.Parameters.Add(param020)
            Dim param030 = New SqlParameter("@dps", SqlDbType.VarChar)
            If dps = "" Then
                param030.Value = System.DBNull.Value
            Else
                param030.Value = "100"
            End If
            cmd0.Parameters.Add(param030)
            Dim param040 = New SqlParameter("@dle", SqlDbType.VarChar)
            If dle = "" Then
                param040.Value = System.DBNull.Value
            Else
                param040.Value = odle
            End If
            cmd0.Parameters.Add(param040)
            Dim param050 = New SqlParameter("@dri", SqlDbType.VarChar)
            If dri = "" Then
                param050.Value = System.DBNull.Value
            Else
                param050.Value = odri
            End If
            cmd0.Parameters.Add(param050)

            Dim param051 = New SqlParameter("@who", SqlDbType.VarChar)
            If styp = "" Then
                param051.Value = System.DBNull.Value
            Else
                param051.Value = styp
            End If
            cmd0.Parameters.Add(param051)

            Dim param052 = New SqlParameter("@did", SqlDbType.VarChar)
            If styp = "dept" Or styp = "cell" Then
                param052.Value = ftyparr(1)
            Else
                param052.Value = System.DBNull.Value
            End If
            cmd0.Parameters.Add(param052)

            Dim param053 = New SqlParameter("@cid", SqlDbType.VarChar)
            If styp = "dept" Or styp = "cell" Then
                param053.Value = ftyparr(2)
            Else
                param053.Value = System.DBNull.Value
            End If
            cmd0.Parameters.Add(param053)

            Dim param054 = New SqlParameter("@locid", SqlDbType.VarChar)
            If styp = "loc" Then
                param054.Value = ftyparr(1)
            Else
                param054.Value = System.DBNull.Value
            End If
            cmd0.Parameters.Add(param054)

            di.UpdateHack(cmd0)

            If typ = "c" Then
                sql = "usp_dialpm_undodialcell '" & dialid & "','" & ftyparr(1) & "','" & ftyparr(2) & "','" & ftyparr(3) & "'"
                di.Update(sql)
            ElseIf typ = "d" Then
                sql = "usp_dialpm_undodialdept '" & dialid & "','" & ftyparr(1) & "','" & ftyparr(2) & "'"
                di.Update(sql)
            ElseIf typ = "l" Then
                sql = "usp_dialpm_undodialloc '" & dialid & "','" & ftyparr(1) & "','" & ftyparr(2) & "'"
                di.Update(sql)
            End If
        Next
    End Sub
    Private Sub GetNext()
        dialid = lblid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getalleq(PageNumber, dialid)
        Catch ex As Exception
            di.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        dialid = lblid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getalleq(PageNumber, dialid)
        Catch ex As Exception
            di.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getalleq(ByVal PageNumber As Integer, ByVal dialid As String)
        Dim intPgNav, intPgCnt As Integer
        sql = "select count(*) from meterdiallocs where dialid = '" & dialid & "'"
        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim cidi As Integer = 0
        Dim cbib As String = ""
        Dim ein As String = lbleqinstr.Value
        Dim mlocid As String
        Dim sb As New StringBuilder
        sb.Append("<table>")
        sql = "usp_getalldiallocspg '" & dialid & "','" & PageNumber & "','" & PageSize & "'"
        Dim dept_name, cell_name, location As String
        dr = di.GetRdrData(sql)
        While dr.Read
            cidi += 1
            cbib = "cb" & cidi
            did = dr.Item("deptid").ToString
            cid = dr.Item("cellid").ToString
            locid = dr.Item("locid").ToString
            dept_name = dr.Item("dept_line").ToString
            cell_name = dr.Item("cell_name").ToString
            location = dr.Item("location").ToString
            mlocid = dr.Item("mlocid").ToString
            Dim dualid As String = ""
            sb.Append("<tr>")
            If ein <> "" Then
                Dim ei As Integer = -1
                If did <> "" And cid <> "" Then
                    dualid = "c_" & did & "_" & cid & "_" & mlocid
                    ei = ein.IndexOf(dualid)
                ElseIf did <> "" And cid = "0" Then
                    dualid = "d_" & did & "_" & mlocid
                    ei = ein.IndexOf(dualid)
                ElseIf locid <> "" Then
                    dualid = "l_" & locid & "_" & mlocid
                    ei = ein.IndexOf(dualid)
                End If

                If ei <> -1 Then
                    If did <> "" And cid <> "" Then
                        sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('cells','" & did & "','" & cid & "','" & dept_name & "','" & cell_name & "');"">")
                        sb.Append("Department: " & dept_name & "  Cell: " & cell_name & "</td>")
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('cells','" & did & "','" & cid & "');""></td>")
                    ElseIf did <> "" And cid = "0" Then
                        sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('depts','" & did & "','','" & dept_name & "');"">")
                        sb.Append("Department: " & dept_name & "</td>")
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('depts','" & did & "');""></td>")
                    ElseIf locid <> "" Then
                        sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """ checked></td>")
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('locs','" & locid & "','','" & location & "');"">")
                        sb.Append("Location: " & location & "</td>")
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('locs','" & locid & "');""></td>")
                    End If
                Else
                    If did <> "" And cid <> "" Then
                        sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('cells','" & did & "','" & cid & "','" & dept_name & "','" & cell_name & "');"">")
                        sb.Append("Department: " & dept_name & "  Cell: " & cell_name & "</td>")
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('cells','" & did & "','" & cid & "');""></td>")
                    ElseIf did <> "" And cid = "0" Then
                        sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('depts','" & did & "','','" & dept_name & "');"">")
                        sb.Append("Department: " & dept_name & "</td>")
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('depts','" & did & "');""></td>")
                    ElseIf locid <> "" Then
                        sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                        sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('locs','" & locid & "','','" & location & "');"">")
                        sb.Append("Location: " & location & "</td>")
                        sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('locs','" & locid & "');""></td>")
                    End If
                End If
            Else
                If did <> "" And cid <> "" Then
                    dualid = "c_" & did & "_" & cid & "_" & mlocid
                ElseIf did <> "" And cid = "0" Then
                    dualid = "d_" & did & "_" & mlocid
                ElseIf locid <> "" Then
                    dualid = "l_" & locid & "_" & mlocid
                End If

                If did <> "" And cid <> "" Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                    sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('cells','" & did & "','" & cid & "','" & dept_name & "','" & cell_name & "');"">")
                    sb.Append("Department: " & dept_name & "  Cell: " & cell_name & "</td>")
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('cells','" & did & "','" & cid & "');""></td>")
                ElseIf did <> "" And cid = "0" Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                    sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('depts','" & did & "','','" & dept_name & "');"">")
                    sb.Append("Department: " & dept_name & "</td>")
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('depts','" & did & "');""></td>")
                ElseIf locid <> "" Then
                    sb.Append("<td class=""plainlabel""><input type=""checkbox"" onclick=""addeq('" & dualid & "','" & cbib & "');"" id=""" & cbib & """></td>")
                    sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""geteq('locs','" & locid & "','','" & location & "');"">")
                    sb.Append("Location: " & location & "</td>")
                    sb.Append("<td><img src=""../images/appbuttons/minibuttons/addlil.gif"" onclick=""getneweq('locs','" & locid & "');""></td>")
                End If
            End If
            
            
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString
    End Sub
End Class