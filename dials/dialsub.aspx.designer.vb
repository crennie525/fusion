﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class dialsub

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''tdname control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdname As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tddesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tddesc As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''ifmeter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifmeter As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''tdloc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdloc As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''hadd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hadd As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''spdiv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents spdiv As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ifirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifirst As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''iprev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iprev As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''inext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents inext As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''ilast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ilast As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblsid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldial control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldial As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldesc As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblret control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblret As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbltyp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltyp As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllocid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllocid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpg As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtpgcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpgcnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqinstr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqinstr As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbleqoutstr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbleqoutstr As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbldname control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldname As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcname control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcname As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllname control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllname As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblnormdle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblnormdle As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblnormdri control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblnormdri As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
