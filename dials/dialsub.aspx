﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dialsub.aspx.vb" Inherits="lucy_r12.dialsub" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    .plainlabelredu
{
	font-family:MS Sans Serif, arial, sans-serif, Verdana;
	font-size:12px;
	text-decoration:underline;
	color:red;
}
    </style>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function addeq(eid, cbid) {
            var eqstr = document.getElementById("lbleqinstr").value;
            var eqstro = document.getElementById("lbleqoutstr").value;
            var cb = document.getElementById(cbid);
            if (cb.checked == true) {
                if (eqstr == "") {
                    eqstr = eid;
                }
                else {
                    eqstr += "," + eid;
                }
            }
            else {
                eqstr = eqstr.replace("," + eid, "");
                eqstr = eqstr.replace(eid, "");
                /*
                if (eqstro == "") {
                eqstro = eid;
                }
                else {
                eqstro += "," + eid;
                }
                */
            }
            document.getElementById("lbleqinstr").value = eqstr;
        }

        function getall() {
            //document.getElementById("lbleqinstr").value = "";
            document.getElementById("lblret").value = "getall";
            document.getElementById("form1").submit();

        }
        function getchecked() {
            document.getElementById("lblret").value = "getchecked";
            document.getElementById("form1").submit();
        }
        function checkret() {
            var chk = document.getElementById("lblret").value;
            if (chk == "return") {
                window.parent.handlereturn();
            }
        }
        function handleloc(ret) {
        //alert(ret)
            var sid = document.getElementById("lblsid").value;
            var id = document.getElementById("lblid").value;
            var dname = document.getElementById("lbldial").value;
            var ddesc = document.getElementById("lbldesc").value;
            var retarr = ret.split(",");
            var typ = retarr[0];
            var did;
            var cid;
            var locid;
            var dpname;
            var cname;
            var lname;
            if (typ == "depts") {
                did = retarr[1];
                dpname = retarr[3];
            }
            else if (typ == "cells") {
                did = retarr[1];
                cid = retarr[2];
                dpname = retarr[3];
                cname = retarr[4];
            }
            else if (typ == "locs") {
                locid = retarr[1];
                lname = retarr[3];
            }

            //document.getElementById("ifmeter").src
             window.location = "dialsub.aspx?typ=" + typ + "&sid=" + sid + "&dialid=" + id + "&dial=" + dname + "&desc=" + ddesc + "&did=" + did + "&cid=" + cid + "&locid=" + locid + "&dname=" + dpname + "&cname=" + cname + "&lname=" + lname;
         }
         function addneweq() {
             var typ = document.getElementById("lbltyp").value;

             var did = document.getElementById("lbldid").value;
             var cid = document.getElementById("lblcid").value;
             var locid = document.getElementById("lbllocid").value;

             var sid = document.getElementById("lblsid").value;
             var dialid = document.getElementById("lblid").value;
             var dial = document.getElementById("lbldial").value;
             var desc = document.getElementById("lbldesc").value;
             if (typ == "depts") {
                 //did = id1
                 var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=new&typ=depts&sid=" + sid + "&did=" + did + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                 if (eReturn2) {
                     if (eReturn == "ok") {
                         var decision = confirm("Add Records From Another Department or Cell?");
                         if (decision == true) {
                             dodept();
                         }
                         else {
                             window.returnValue = dialid;
                             window.close();
                         }
                     }
                 }
             }
             else if (typ == "cells") {
                 //did = id1
                 //cid = id2
                 var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=new&typ=cells&sid=" + sid + "&did=" + did + "&cid=" + cid + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                 if (eReturn2) {
                     if (eReturn == "ok") {
                         var decision = confirm("Add Records From Another Department or Cell?");
                         if (decision == true) {
                             dodept();
                         }
                         else {
                             window.returnValue = dialid;
                             window.close();
                         }
                     }
                 }
             }
             else if (typ == "locs") {
             locids = locid; // id1
                 var eReturn2 = window.showModalDialog("dialeqdialog.aspx?who=new&typ=locs&sid=" + sid + "&locid=" + locids + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                 if (eReturn2) {
                     if (eReturn == "ok") {
                         var decision = confirm("Add Records From Another Location?");
                         if (decision == true) {
                             doloc();
                         }
                         else {
                             window.returnValue = "go";
                             window.close();
                         }
                     }
                 }
             }
         }
         function getpm(eqid, eqnum, eqdesc) {
             var dialid = document.getElementById("lblid").value;
             var dial = document.getElementById("lbldial").value;
             var desc = document.getElementById("lbldesc").value;
             var eReturn = window.showModalDialog("dialpmsdialog.aspx?dialid=" + dialid + "&dial=" + dial + "&desc=" + desc + "&eqid=" + eqid + "&eqdesc=" + eqdesc + "&eqnum=" + eqnum, "", "dialogHeight:450px; dialogWidth:1140px; resizable=yes");
             if (eReturn) {

             }
         }
         function handlereset() {
         //alert()
             var sid = document.getElementById("lblsid").value;
             var id = document.getElementById("lblid").value;
             var dial = document.getElementById("lbldial").value;
             var desc = document.getElementById("lbldesc").value;
             var typ = "all";
             window.location = "dialsub.aspx?typ=all&sid=" + sid + "&dialid=" + id + "&dial=" + dial + "&desc=" + desc;
         }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="position: absolute; top: 0px;">
    <table>
    <tr>
    <td>
    <table>
    <tr>
    <td class="bluelabel" width="60">Dial</td>
    <td class="plainlabel" id="tdname" runat="server"></td>
    <td class="bluelabel" width="90">Description</td>
    <td class="plainlabel" id="tddesc" runat="server"></td>
    </tr>
   
    </table>
    </td>
    </tr>
    <tr>
    <td class="thdrsing label">Location Records</td>
    </tr>
    <tr>
    <td>
    <iframe id="ifmeter" src="../genhold.htm" runat="server" width="100%" height="150px" frameborder="0">
        </iframe>
    </td>
    </tr>
    <tr>
    <td class="thdrsing label">Equipment Records</td>
    </tr>
    <tr>
    <td class="bluelabel" align="center" id="tdloc" runat="server">Current Location: ALL</td>
    </tr>
    <tr>
    <td class="plainlabel"><a href="#" class="plainlabelredu" onclick="getall();">Delete Selected</a>&nbsp;&nbsp;&nbsp;<a href="#" onclick="addneweq();" id="hadd" runat="server">Add Equipment Records to Current Location</a></td>
    </tr>
    <tr>
    <td>
    <div style="WIDTH: 450px; HEIGHT: 240px; OVERFLOW: auto; border: 1px solid gray" id="spdiv" runat="server">
    </div>
    </td>
    </tr>
    <tr>
    <td class="plainlabelblue" align="center">*Click Equipment Record to View PM Records</td>
    </tr>
    <tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>

    </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblid" runat="server" />
    <input type="hidden" id="lbldial" runat="server" />
    <input type="hidden" id="lbldesc" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblcid" runat="server" />
    <input type="hidden" id="lbllocid" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lbleqinstr" runat="server" />
    <input type="hidden" id="lbleqoutstr" runat="server" />
    <input type="hidden" id="lbldname" runat="server" />
    <input type="hidden" id="lblcname" runat="server" />
    <input type="hidden" id="lbllname" runat="server" />
    <input type="hidden" id="lblnormdle" runat="server" />
    <input type="hidden" id="lblnormdri" runat="server" />
    </form>
</body>
</html>
