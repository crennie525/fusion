﻿Imports System.Data.SqlClient
Imports System.Text
Public Class dialadd
    Inherits System.Web.UI.Page
    Dim di As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim dial, desc, dialid, sid As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            dial = Request.QueryString("dial").ToString
            lbldial.Value = dial
            desc = Request.QueryString("desc").ToString
            lbldesc.Value = desc
            di.Open()
            MakeDial(dial, desc)
            di.Dispose()

        End If
    End Sub
    Private Sub MakeDial(ByVal dial As String, ByVal desc As String)
        Dim dcnt As Integer = 0
        sid = lblsid.Value

        sql = "select count(*) from meterdials where dialname = '" & dial & "'"
        dcnt = di.Scalar(sql)
        If dcnt = 0 Then
            sql = "insert into meterdials (dialname, dialdesc, siteid, dialpercent) values ('" & dial & "','" & desc & "','" & sid & "','100.00') select @@identity"
            dialid = di.Scalar(sql)
            lblid.Value = dialid
            tdid.InnerHtml = dialid
            tddial.InnerHtml = dial
            tddesc.InnerHtml = desc
        Else
            lblret.Value = "dup"
        End If

    End Sub
End Class