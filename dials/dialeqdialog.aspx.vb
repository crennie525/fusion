﻿Public Class dialeqdialog
    Inherits System.Web.UI.Page
    Dim dial, desc, dialid, sid, did, cid, locid, typ, eqid, eqnum, eqdesc, who As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            dialid = Request.QueryString("dialid").ToString
            dial = Request.QueryString("dial").ToString
            desc = Request.QueryString("desc").ToString
            typ = Request.QueryString("typ").ToString
            who = Request.QueryString("who").ToString
            If typ = "depts" Or typ = "cells" Then
                did = Request.QueryString("did").ToString
                If typ = "cells" Then
                    cid = Request.QueryString("cid").ToString
                    ifmeter.Attributes.Add("src", "dialeq.aspx?who=" + who + "&typ=cells&sid=" + sid + "&did=" + did + "&cid=" + cid + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc)
                Else
                    ifmeter.Attributes.Add("src", "dialeq.aspx?who=" + who + "&typ=depts&sid=" + sid + "&did=" + did + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc)
                End If
            ElseIf typ = "locs" Then
                locid = Request.QueryString("locid").ToString
                ifmeter.Attributes.Add("src", "dialeq.aspx?who=" + who + "&typ=locs&sid=" + sid + "&locid=" + locid + "&dialid=" + dialid + "&dial=" + dial + "&desc=" + desc)
            End If
        End If
    End Sub

End Class