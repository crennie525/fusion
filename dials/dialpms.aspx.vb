﻿Imports System.Data.SqlClient
Imports System.Text
Public Class dialpms
    Inherits System.Web.UI.Page
    Dim di As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim dial, desc, dialid, sid, did, cid, locid, typ, eqid, eqnum, eqdesc, dname, cname, lname As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            dialid = Request.QueryString("dialid").ToString
            lblid.Value = dialid
            dial = Request.QueryString("dial").ToString
            lbldial.Value = dial
            desc = Request.QueryString("desc").ToString
            lbldesc.Value = desc
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            eqnum = Request.QueryString("eqnum").ToString
            lbleqnum.Value = eqnum
            eqdesc = Request.QueryString("eqdesc").ToString
            lbleqdesc.Value = eqdesc
            tdname.InnerHtml = dial
            tddesc.InnerHtml = desc
            tdeq.InnerHtml = eqnum
            tdeqdesc.InnerHtml = eqdesc
            di.Open()
            getpms(PageNumber, dialid, eqid)
            di.Dispose()
        Else
            dialid = lblid.Value
            eqid = lbleqid.Value

            If Request.Form("lblret") = "next" Then
                di.Open()
                GetNext()
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                di.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber

                getpms(PageNumber, dialid, eqid)
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                di.Open()
                GetPrev()
                di.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                di.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                getpms(PageNumber, dialid, eqid)
                di.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        dialid = lblid.Value
        eqid = lbleqid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getpms(PageNumber, dialid, eqid)
        Catch ex As Exception
            di.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        dialid = lblid.Value
        eqid = lbleqid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getpms(PageNumber, dialid, eqid)
        Catch ex As Exception
            di.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getpms(ByVal PageNumber As Integer, ByVal dialid As String, ByVal eqid As String)
        Dim intPgNav, intPgCnt As Integer
        sql = "select count(*) from pm where dialid = '" & dialid & "' and eqid = '" & eqid & "'"
        intPgCnt = di.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "20"
        intPgNav = di.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Dim sb As New StringBuilder
        Dim pm, mpm, pms, ofreq, omfreq, omaxdays, ndate, ondate, pfreq, fixed As String
        Dim bg As String = "ptransrowblue"
        Dim rint As Integer = 0
        sb.Append("<table width=""1060"">")
        sb.Append("<tr>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""490"">PM</td>")

        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">Next Date</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">Orig Date</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""110"">Current Frequency</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""110"">Original Frequency</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""140"">Original Meter Frequency</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""110"">Original Max Days</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""40"">Fixed</td>")
        sb.Append("</tr>")
        sql = "usp_getdialpmpg_rvw '" & dialid & "','" & eqid & "','" & PageNumber & "','" & PageSize & "'"
        dr = di.GetRdrData(sql)
        While dr.Read
            If rint = 0 Then
                rint = 1
            Else
                rint = 0
            End If
            pm = dr.Item("pm").ToString
            mpm = dr.Item("mpm").ToString
            ofreq = dr.Item("ofreq").ToString
            omfreq = dr.Item("omfreq").ToString
            omaxdays = dr.Item("omaxdays").ToString
            ndate = dr.Item("nextdate").ToString
            ondate = dr.Item("onextdate").ToString
            pfreq = dr.Item("freq").ToString
            fixed = dr.Item("fixed").ToString
            If fixed = "1" Then
                fixed = "YES"
            Else
                fixed = "NO"
            End If
            If mpm <> "" Then
                pms = pm & " - " & mpm
            Else
                pms = pm
            End If
            sb.Append("<tr>")
            If rint = 0 Then
                sb.Append("<td class=""plainlabel"">" & pms & "</td>")

                sb.Append("<td class=""plainlabel"">" & ndate & "</td>")
                sb.Append("<td class=""plainlabel"">" & ondate & "</td>")
                sb.Append("<td class=""plainlabel"" align=""center"">" & pfreq & "</td>")
                sb.Append("<td class=""plainlabel"" align=""center"">" & ofreq & "</td>")
                sb.Append("<td class=""plainlabel"" align=""center"">" & omfreq & "</td>")
                sb.Append("<td class=""plainlabel"" align=""center"">" & omaxdays & "</td>")
                sb.Append("<td class=""plainlabel"" align=""center"">" & fixed & "</td>")
            Else
                sb.Append("<td class=""plainlabel ptransrowblue"">" & pms & "</td>")

                sb.Append("<td class=""plainlabel ptransrowblue"">" & ndate & "</td>")
                sb.Append("<td class=""plainlabel ptransrowblue"">" & ondate & "</td>")
                sb.Append("<td class=""plainlabel ptransrowblue"" align=""center"">" & pfreq & "</td>")
                sb.Append("<td class=""plainlabel ptransrowblue"" align=""center"">" & ofreq & "</td>")
                sb.Append("<td class=""plainlabel ptransrowblue"" align=""center"">" & omfreq & "</td>")
                sb.Append("<td class=""plainlabel ptransrowblue"" align=""center"">" & omaxdays & "</td>")
                sb.Append("<td class=""plainlabel ptransrowblue"" align=""center"">" & fixed & "</td>")
            End If
           
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        spdiv.InnerHtml = sb.ToString
    End Sub
End Class