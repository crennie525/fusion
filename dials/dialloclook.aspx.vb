﻿Imports System.Data.SqlClient
Imports System.Text
Public Class dialloclook
    Inherits System.Web.UI.Page
    Dim ec As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim sid, did, dept, cid, cell, eqid, eq, fuid, fu, comid, comp, lid, loc, ncid, nc, sys, lev, lidi, lidid, dialid As String
    Dim filt, dt, val, Filter, wonum, typ, who, jpid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            dialid = Request.QueryString("dialid").ToString
            lblid.Value = dialid
            tdmsg.InnerHtml = "Location# Only Required for Return"
            ec.Open()
            PopSys()
            PopTypes()
            lbllocsys.Value = "PRIMARY"
            ddsys.SelectedValue = "PRIMARY"
            Dim mm1 As New mmenu_utils_a
            Dim appver As String = mm1.AppVer
            If appver = "2" Then
                lblloctyp.Value = "POSITION"
                ddtype.SelectedValue = "POSITION"
            End If

            GetL1()
            If typ = "ret" Then
                who = Request.QueryString("who").ToString
                eqid = Request.QueryString("eqid").ToString
                eq = Request.QueryString("eq").ToString
                fuid = Request.QueryString("fuid").ToString
                fu = Request.QueryString("fu").ToString
                comid = Request.QueryString("coid").ToString
                comp = Request.QueryString("comp").ToString
                lid = Request.QueryString("lid").ToString
                loc = Request.QueryString("loc").ToString
                lev = Request.QueryString("lev").ToString
                lblsid.Value = sid
                lbleqid.Value = eqid
                lbleq.Value = eq
                lblfuid.Value = fuid
                lblfunc.Value = fu
                lblcoid.Value = comid
                lblcomp.Value = comp
                lbllid.Value = lid
                lblloc.Value = loc
                lbllevel.Value = lev
                If who = "checkl1" Then
                    lblsubmit.Value = ""
                    lev = lbllevel.Value
                    GetL2(lev)
                ElseIf who = "getfunc" Then
                    lblsubmit.Value = ""
                    
                ElseIf who = "getco" Then
                    lblsubmit.Value = ""
                    
                End If
            End If
            ec.Dispose()
        Else
            If Request.Form("lblsubmit") = "checkl1" Then
                lblsubmit.Value = ""
                lev = lbllevel.Value
                ec.Open()
                GetL2(lev)
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "savedial" Then
                ec.Open()
                savedial()
                ec.Dispose()
            End If
            tdmsg.InnerHtml = "Location# Only Required for Return"
        End If
    End Sub
    Private Sub savedial()
        lidi = lblretlidi.Value
        dialid = lblid.Value
        sid = lblsid.Value
        Dim dcnt As Integer = 0
        sql = "select count(*) from meterdiallocs where dialid = '" & dialid & "' and parlocid = '" & lidi & "'"
        dcnt = ec.Scalar(sql)
        If dcnt = 0 Then
            Dim locids As String = ""
            sql = "usp_getlocids '" & lidi & "','" & sid & "'"
            locids = ec.strScalar(sql)
            Dim cmd As New SqlCommand
            cmd.CommandText = "insert into meterdiallocs(dialid, parlocid, locid) values(@dialid, @parlocid, @locid)"
            Dim param = New SqlParameter("@dialid", SqlDbType.VarChar)
            param.Value = dialid
            cmd.Parameters.Add(param)
            Dim param01 = New SqlParameter("@parlocid", SqlDbType.VarChar)
            If lidi = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = lidi
            End If
            cmd.Parameters.Add(param01)

            Dim param02 = New SqlParameter("@locid", SqlDbType.VarChar)
            If locids = "" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = locids
            End If
            cmd.Parameters.Add(param02)

            ec.UpdateHack(cmd)

            lblret.Value = locids 'lidi "locs~" & 

            lblsubmit.Value = "return"
        End If

        

    End Sub
    Private Sub ddsys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsys.SelectedIndexChanged
        If ddsys.SelectedIndex <> 0 And ddsys.SelectedIndex <> -1 Then
            lbllocsys.Value = ddsys.SelectedValue
            ec.Open()
            GetL1()
            ec.Dispose()
        Else
            lbllocsys.Value = "PRIMARY"
            ddsys.SelectedValue = "PRIMARY"
            GetL1()
        End If
    End Sub
    Private Sub ddtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtype.SelectedIndexChanged
        If ddtype.SelectedIndex <> 0 And ddtype.SelectedIndex <> -1 Then
            lblloctyp.Value = ddtype.SelectedValue
        End If
        ec.Open()
        GetL1()
        ec.Dispose()
    End Sub
    Private Sub PopSys()
        sql = "select * from pmlocsys"
        dr = ec.GetRdrData(sql)
        ddsys.DataSource = dr
        ddsys.DataTextField = "systemid"
        ddsys.DataValueField = "systemid"
        ddsys.DataBind()
        dr.Close()
        ddsys.Items.Insert(0, "Select System")
    End Sub
    Private Sub PopTypes()
        sql = "select * from pmloctypes where loctype <> 'EQUIPMENT'"
        dr = ec.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataTextField = "loctype"
        ddtype.DataValueField = "loctype"
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, "Select Type")
    End Sub
    Private Sub GetL2(ByVal level As String)
        'sys = "PRIMARY"
        sys = ddsys.SelectedValue
        Dim loctyp As String = ""
        If ddtype.SelectedIndex <> 0 And ddtype.SelectedIndex <> -1 Then
            loctyp = ddtype.SelectedValue
        End If
        Dim appver As String = lblappver.Value
        Dim atyp As String
        atyp = "lul" 'lbltyp.Value
        Select Case level
            Case "1"
                lid = lbllid.Value
                lidid = lblretlidi.Value
            Case "2"
                lid = lbllid1.Value
                lidid = lbllidi1.Value
            Case "3"
                lid = lbllid2.Value
                lidid = lbllidi2.Value
            Case "4"
                lid = lbllid3.Value
                lidid = lbllidi3.Value
            Case "5"
                lid = lbllid4.Value
            Case "6"
                lid = lbllid5.Value
            Case "7"
                lid = lbllid6.Value
            Case "8"
                lid = lbllid7.Value
            Case "9"
                lid = lbllid8.Value
            Case "10"
                lid = lbllid9.Value
            Case "11"
                lid = lbllid10.Value
            Case "12"
                lid = lbllid11.Value
            Case "13"
                lid = lbllid12.Value
            Case "14"
                lid = lbllid13.Value
            Case "15"
                lid = lbllid14.Value
            Case "16"
                lid = lbllid15.Value
            Case "17"
                lid = lbllid16.Value
            Case "18"
                lid = lbllid17.Value
            Case "19"
                lid = lbllid18.Value
            Case "20"
                lid = lbllid19.Value
            Case "21"
                lid = lbllid20.Value
            Case "22"
                lid = lbllid21.Value
            Case "23"
                lid = lbllid22.Value
            Case "24"
                lid = lbllid23.Value
            Case "25"
                lid = lbllid24.Value
        End Select
        Dim ccnt As Integer
        Dim eqid, typ, eqnum, eqdesc As String
        Dim getlev, tdlev, geteq As String
        getlev = "getecd" & level
        tdlev = "tdec" & level
        'sql = "select count(*) from pmlocations where locid in (select locid from pmlocations where location in " _
        '+ "(select location from pmlocheir where parent = '" & lid & "')) " _
        '+ "and type = 'EQUIPMENT'"
        sid = lblsid.Value
        sql = "select count(*) " _
        + "from pmlocations l " _
        + "left join equipment e on e.locid = l.locid " _
        + "left join pmlocheir h on h.location = l.location " _
        + "where h.systemid = '" & sys & "' and h.parent = '" & lid & "' and l.siteid = '" & sid & "' and l.type <> 'EQUIPMENT'"
        Dim lcnt As Integer
        Dim lidhold, lididhold As String
        lidhold = lblhold.Value
        lididhold = lblholdi.Value
        lblhold.Value = lid
        lblholdi.Value = lidid
        lcnt = ec.Scalar(sql)
        If lcnt > 0 Then
            sql = "select h.location, l.description, isnull(e.eqid, 0) as eqid, e.eqnum, e.eqdesc, l.type, l.locid, " _
            + "ccnt = (select count(*) from pmlocheir h1 where h1.parent = l.location and h1.systemid = '" & sys & "' and h1.siteid = '" & sid & "') " _
            + "from pmlocations l " _
            + "left join equipment e on e.locid = l.locid " _
            + "left join pmlocheir h on h.location = l.location " _
            + "where h.systemid = '" & sys & "' and h.parent = '" & lid & "' and l.siteid = '" & sid & "' and h.siteid = '" & sid & "'"
            'appver = "2" And 
            If atyp = "lul" Then
                sql += " and l.type <> 'EQUIPMENT' order by h.location, e.eqnum"
            Else
                If loctyp <> "" Then
                    sql += " and l.type = '" & loctyp & "' order by h.location, e.eqnum"
                Else
                    sql += " order by h.location, e.eqnum"
                End If
            End If

        Else
            'l.type
            If atyp <> "lul" Then
                sql = "select distinct h.location, l.description, isnull(e.eqid, 0) as eqid, e.eqnum, e.eqdesc, l.type, l.locid, " _
                + "ccnt = (select count(*) from pmlocheir h1 where h1.parent = l.location and h1.systemid = '" & sys & "') " _
                + "from pmlocations l " _
                + "left join equipment e on e.locid = l.locid " _
                + "left join pmlocheir h on h.location = l.location " _
                + "where h.systemid = '" & sys & "' and h.parent = '" & lid & "' and l.siteid = '" & sid & "' order by h.location, e.eqnum"
            Else
                FillLevels()
                Dim strMessage As String
                strMessage = "No Locations Beyond This Level"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

        End If
        dr = ec.GetRdrData(sql)

        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 530px;  HEIGHT: 270px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""500"" cellpadding=""1"">")
        sb.Append("<tr>")
        sb.Append("<td width=""150""></td>")
        sb.Append("<td width=""280""></td>")
        sb.Append("<td width=""70""></td>")
        sb.Append("</tr>")
        While dr.Read
            lid = dr.Item("location").ToString
            loc = dr.Item("description").ToString
            ccnt = dr.Item("ccnt").ToString
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            If eqid <> "0" Then
                Dim echk As String = eqnum
            End If

            typ = dr.Item("type").ToString
            lidi = dr.Item("locid").ToString
            sb.Append("<tr>")
            If ccnt = 0 And ccnt = 0 Then
                If eqid = "0" Then
                    sb.Append("<td class=""label""><a href=""#"" class=""A1R""  onclick=""" & getlev & "('" & lidi & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','no','no');"">" & lid & "</td>")
                    sb.Append("<td class=""plainlabelblue"">" & loc & "</td>")
                    sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                Else
                    If lcnt > 0 Then
                        sb.Append("<td class=""label""><a href=""#"" class=""A1G""  onclick=""" & getlev & "('" & lidid & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','no','yes');"">" & eqnum & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & eqdesc & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                    Else
                        sb.Append("<td class=""label""><a href=""#"" class=""A1G""  onclick=""" & getlev & "('" & lidid & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','no','yes');"">" & eqnum & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & eqdesc & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                    End If

                End If
            Else
                'Dim dummy As String = "ok"
                If eqid = "0" Then
                    sb.Append("<td class=""label""><a href=""#"" onclick=""" & getlev & "('" & lidi & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','yes','no');"">" & lid & "</td>")
                    sb.Append("<td class=""plainlabelblue"">" & loc & "</td>")
                    sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                Else
                    If lcnt > 0 Then
                        sb.Append("<td class=""label""><a href=""#"" class=""A1G""  onclick=""" & getlev & "('" & lidid & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','no','yes');"">" & eqnum & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & eqdesc & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                    Else
                        sb.Append("<td class=""label""><a href=""#"" class=""A1G""  onclick=""" & getlev & "('" & lidid & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','no','yes');"">" & eqnum & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & eqdesc & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                    End If
                End If

            End If
            sb.Append("</tr>")
        End While
        dr.Close()

        sb.Append("</table>")

        tdec.Attributes.Add("class", "details")
        HideAll()
        Select Case level
            Case "1"
                lbltabs.Value = "ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec1.InnerHtml = sb.ToString
                tdec1.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 1"
            Case "2"
                lbltabs.Value = "ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec2.InnerHtml = sb.ToString
                tdec2.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 2"
            Case "3"
                lbltabs.Value = "ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec3.InnerHtml = sb.ToString
                tdec3.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 3"
            Case "4"
                lbltabs.Value = "ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec4.InnerHtml = sb.ToString
                tdec4.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 4"
            Case "5"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec5.InnerHtml = sb.ToString
                tdec5.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 5"
            Case "6"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec6.InnerHtml = sb.ToString
                tdec6.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 6"
            Case "7"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec7.InnerHtml = sb.ToString
                tdec7.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 7"
            Case "8"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec8.InnerHtml = sb.ToString
                tdec8.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 8"
            Case "9"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec9.InnerHtml = sb.ToString
                tdec9.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 9"
            Case "10"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec10.InnerHtml = sb.ToString
                tdec10.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 10"
            Case "11"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec11.InnerHtml = sb.ToString
                tdec11.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 11"
            Case "12"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec12.InnerHtml = sb.ToString
                tdec12.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 12"
            Case "13"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec13.InnerHtml = sb.ToString
                tdec13.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 13"
            Case "14"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no"
                tdec14.InnerHtml = sb.ToString
                tdec14.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 14"
            Case "15"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no"
                tdec15.InnerHtml = sb.ToString
                tdec15.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 15"
            Case "16"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no"
                tdec16.InnerHtml = sb.ToString
                tdec16.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 16"
            Case "17"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no"
                tdec17.InnerHtml = sb.ToString
                tdec17.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 17"
            Case "18"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no"
                tdec18.InnerHtml = sb.ToString
                tdec18.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 18"
            Case "19"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no"
                tdec19.InnerHtml = sb.ToString
                tdec19.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 19"
            Case "20"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no"
                tdec20.InnerHtml = sb.ToString
                tdec20.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 20"
            Case "21"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no"
                tdec21.InnerHtml = sb.ToString
                tdec21.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 21"
            Case "22"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no"
                tdec22.InnerHtml = sb.ToString
                tdec22.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 22"
            Case "23"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no"
                tdec23.InnerHtml = sb.ToString
                tdec23.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 23"
            Case "24"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no"
                tdec24.InnerHtml = sb.ToString
                tdec24.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 24"
            Case "25"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok"
                tdec25.InnerHtml = sb.ToString
                tdec25.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 25"
        End Select
        FillLevels()


    End Sub
    Private Sub UnDoEq()
        lbleqid.Value = ""
        lbleq.Value = ""
        
    End Sub
    Private Sub FillLevels(Optional ByVal iseq As Integer = 0)
        lev = lbllevel.Value
        If iseq = 1 Then
            Dim levi As Integer
            Try
                levi = CType(lev, Integer)
                lev = levi - 1
            Catch ex As Exception

            End Try
        End If
        Select Case lev
            Case "0"
                lbllid.Value = ""
                lblloc.Value = ""

                lbllid1.Value = ""
                lblloc1.Value = ""
                lbllid2.Value = ""
                lblloc2.Value = ""
                lbllid3.Value = ""
                lblloc3.Value = ""
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd.InnerHtml = lbleq.Value
                    tdecd.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "1"
                lbllid1.Value = ""
                lblloc1.Value = ""
                lbllid2.Value = ""
                lblloc2.Value = ""
                lbllid3.Value = ""
                lblloc3.Value = ""
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd1.InnerHtml = lbleq.Value
                    tdecd1.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "2"
                lbllid2.Value = ""
                lblloc2.Value = ""
                lbllid3.Value = ""
                lblloc3.Value = ""
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd2.InnerHtml = lbleq.Value
                    tdecd2.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "3"
                lbllid3.Value = ""
                lblloc3.Value = ""
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd3.InnerHtml = lbleq.Value
                    tdecd3.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "4"
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd4.InnerHtml = lbleq.Value
                    tdecd4.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "5"
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd5.InnerHtml = lbleq.Value
                    tdecd5.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "6"
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd6.InnerHtml = lbleq.Value
                    tdecd6.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "7"
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd7.InnerHtml = lbleq.Value
                    tdecd7.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "8"
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd8.InnerHtml = lbleq.Value
                    tdecd8.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "9"
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd9.InnerHtml = lbleq.Value
                    tdecd9.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "10"
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd10.InnerHtml = lbleq.Value
                    tdecd10.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "11"
                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd11.InnerHtml = lbleq.Value
                    tdecd11.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "12"
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd12.InnerHtml = lbleq.Value
                    tdecd12.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "13"
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd13.InnerHtml = lbleq.Value
                    tdecd13.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "14"
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd14.InnerHtml = lbleq.Value
                    tdecd14.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "15"
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd15.InnerHtml = lbleq.Value
                    tdecd15.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "16"
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd16.InnerHtml = lbleq.Value
                    tdecd16.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "17"
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd17.InnerHtml = lbleq.Value
                    tdecd17.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "18"
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd18.InnerHtml = lbleq.Value
                    tdecd18.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "19"
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd19.InnerHtml = lbleq.Value
                    tdecd19.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "20"
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd20.InnerHtml = lbleq.Value
                    tdecd20.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "21"
                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd21.InnerHtml = lbleq.Value
                    tdecd21.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "22"
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd22.InnerHtml = lbleq.Value
                    tdecd22.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "23"
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd23.InnerHtml = lbleq.Value
                    tdecd23.Attributes.Add("class", "greenlabel")
                End If
            Case "24"
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd24.InnerHtml = lbleq.Value
                    tdecd24.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
            Case "25"
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd25.InnerHtml = lbleq.Value
                    tdecd25.Attributes.Add("class", "greenlabel")
                Else
                    UnDoEq()
                End If
        End Select

    End Sub
    Private Sub FillAll()
        tdecd.InnerHtml = lbllid.Value
        tdecd1.InnerHtml = lbllid1.Value
        tdecd2.InnerHtml = lbllid2.Value
        tdecd3.InnerHtml = lbllid3.Value
        tdecd4.InnerHtml = lbllid4.Value
        tdecd5.InnerHtml = lbllid5.Value
        tdecd6.InnerHtml = lbllid6.Value
        tdecd7.InnerHtml = lbllid7.Value
        tdecd8.InnerHtml = lbllid8.Value
        tdecd9.InnerHtml = lbllid9.Value
        tdecd10.InnerHtml = lbllid10.Value
        tdecd11.InnerHtml = lbllid11.Value
        tdecd12.InnerHtml = lbllid12.Value
        tdecd13.InnerHtml = lbllid13.Value
        tdecd14.InnerHtml = lbllid14.Value
        tdecd15.InnerHtml = lbllid15.Value
        tdecd16.InnerHtml = lbllid16.Value
        tdecd17.InnerHtml = lbllid17.Value
        tdecd18.InnerHtml = lbllid18.Value
        tdecd19.InnerHtml = lbllid19.Value
        tdecd20.InnerHtml = lbllid20.Value
        tdecd21.InnerHtml = lbllid21.Value
        tdecd22.InnerHtml = lbllid22.Value
        tdecd23.InnerHtml = lbllid23.Value
        tdecd24.InnerHtml = lbllid24.Value
        tdecd25.InnerHtml = lbllid25.Value

        tdecd.Attributes.Add("class", "plainlabel")
        tdecd1.Attributes.Add("class", "plainlabel")
        tdecd2.Attributes.Add("class", "plainlabel")
        tdecd3.Attributes.Add("class", "plainlabel")
        tdecd4.Attributes.Add("class", "plainlabel")
        tdecd5.Attributes.Add("class", "plainlabel")
        tdecd6.Attributes.Add("class", "plainlabel")
        tdecd7.Attributes.Add("class", "plainlabel")
        tdecd8.Attributes.Add("class", "plainlabel")
        tdecd9.Attributes.Add("class", "plainlabel")
        tdecd10.Attributes.Add("class", "plainlabel")
        tdecd11.Attributes.Add("class", "plainlabel")
        tdecd12.Attributes.Add("class", "plainlabel")
        tdecd13.Attributes.Add("class", "plainlabel")
        tdecd14.Attributes.Add("class", "plainlabel")
        tdecd15.Attributes.Add("class", "plainlabel")
        tdecd16.Attributes.Add("class", "plainlabel")
        tdecd17.Attributes.Add("class", "plainlabel")
        tdecd18.Attributes.Add("class", "plainlabel")
        tdecd19.Attributes.Add("class", "plainlabel")
        tdecd20.Attributes.Add("class", "plainlabel")
        tdecd21.Attributes.Add("class", "plainlabel")
        tdecd22.Attributes.Add("class", "plainlabel")
        tdecd23.Attributes.Add("class", "plainlabel")
        tdecd24.Attributes.Add("class", "plainlabel")
        tdecd25.Attributes.Add("class", "plainlabel")
    End Sub
    Private Sub HideAll()
        tdec1.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdec5.Attributes.Add("class", "details")
        tdec6.Attributes.Add("class", "details")
        tdec7.Attributes.Add("class", "details")
        tdec8.Attributes.Add("class", "details")
        tdec9.Attributes.Add("class", "details")
        tdec10.Attributes.Add("class", "details")
        tdec11.Attributes.Add("class", "details")
        tdec12.Attributes.Add("class", "details")
        tdec13.Attributes.Add("class", "details")
        tdec14.Attributes.Add("class", "details")
        tdec15.Attributes.Add("class", "details")
        tdec16.Attributes.Add("class", "details")
        tdec17.Attributes.Add("class", "details")
        tdec18.Attributes.Add("class", "details")
        tdec19.Attributes.Add("class", "details")
        tdec20.Attributes.Add("class", "details")
        tdec21.Attributes.Add("class", "details")
        tdec22.Attributes.Add("class", "details")
        tdec23.Attributes.Add("class", "details")
        tdec24.Attributes.Add("class", "details")
        tdec25.Attributes.Add("class", "details")
    End Sub
    Private Sub GetL1()
        sid = lblsid.Value
        'sys = "PRIMARY"
        sys = ddsys.SelectedValue
        sql = "select h.location, l.description, l.locid from pmlocheir h " _
        + "left join pmlocations l on l.location = h.location " _
        + "where h.systemid = '" & sys & "' and h.parent is null and l.siteid = '" & sid & "'"
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 520px;  HEIGHT: 250px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""500"" cellpadding=""1"">")
        sb.Append("<tr>")
        sb.Append("<td width=""150""></td>")
        sb.Append("<td width=""280""></td>")
        sb.Append("<td width=""70""></td>")
        sb.Append("</tr>")
        While dr.Read
            lid = dr.Item("location").ToString
            loc = dr.Item("description").ToString
            lidi = dr.Item("locid").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""label""><a href=""#"" onclick=""getecd0('" & lidi & "','" & lid & "','" & loc & "','yes');"">" & lid & "</td>")
            sb.Append("<td class=""plainlabelblue"">" & loc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        lbltabs.Value = "ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
        sb.Append("</table>")
        tdec.InnerHtml = sb.ToString

        tdec.Attributes.Add("class", "view")
        tdec1.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdec5.Attributes.Add("class", "details")
        tdec6.Attributes.Add("class", "details")
        tdec7.Attributes.Add("class", "details")
        tdec8.Attributes.Add("class", "details")
        tdec9.Attributes.Add("class", "details")
        tdec10.Attributes.Add("class", "details")
        tdec11.Attributes.Add("class", "details")
        tdec12.Attributes.Add("class", "details")
        tdec13.Attributes.Add("class", "details")
        tdec14.Attributes.Add("class", "details")
        tdec15.Attributes.Add("class", "details")
        tdec16.Attributes.Add("class", "details")
        tdec17.Attributes.Add("class", "details")
        tdec18.Attributes.Add("class", "details")
        tdec19.Attributes.Add("class", "details")
        tdec20.Attributes.Add("class", "details")
        tdec21.Attributes.Add("class", "details")
        tdec22.Attributes.Add("class", "details")
        tdec23.Attributes.Add("class", "details")
        tdec24.Attributes.Add("class", "details")
        tdec25.Attributes.Add("class", "details")

        tdtab.InnerHtml = "Level 0"
        lbllevel.Value = "0"
        HideAll()
        FillLevels()
    End Sub
End Class
