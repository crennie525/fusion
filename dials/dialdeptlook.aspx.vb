﻿Imports System.Data.SqlClient
Imports System.Text
Public Class dialdeptlook
    Inherits System.Web.UI.Page
    Dim ec As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim sid, did, dept, cid, cell, eqid, eq, fuid, fu, comid, comp, lid, loc, ncid, nc, dialid As String
    Dim filt, dt, val, Filter, wonum, typ, who, jpid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            dialid = Request.QueryString("dialid").ToString
            lblid.Value = dialid
            tdmsg.InnerHtml = "Department and\or Cell Only Required for Return"
            ec.Open()
            getdepts(sid)
            If typ = "ret" Or typ = "wrret" Then
                who = Request.QueryString("who").ToString
                did = Request.QueryString("did").ToString
                Dim retdid As String = did
                dept = Request.QueryString("dept").ToString
                cid = Request.QueryString("clid").ToString
                Dim retcid As String = cid
                cell = Request.QueryString("cell").ToString
                eqid = Request.QueryString("eqid").ToString
                lid = Request.QueryString("lid").ToString
                loc = Request.QueryString("loc").ToString

                lblsid.Value = sid
                lbldid.Value = did
                lbldept.Value = dept

                lblcid.Value = cid
                lblcell.Value = cell

                lbleqid.Value = eqid

                lbllid.Value = lid
                lblloc.Value = loc

                lblwho.Value = who

                If who = "checkcell" Then
                    lblsubmit.Value = ""
                    checkdept(retdid)
                    dept = lbldept.Value
                    If dept = "" And retdid <> "" Then
                        dept = getdept(retdid)
                        lbldid.Value = retdid
                        'lblcid.Value = retcid
                    End If
                    tdecd1.InnerHtml = dept

                ElseIf who = "checkdept" Then
                    lblsubmit.Value = ""
                    checkdept(retdid)
                ElseIf who = "checkeq" Then
                    lblsubmit.Value = ""
                    checkdept(retdid)
                    If cid <> "" Then
                        checkcell(retcid)
                    End If
                    dept = lbldept.Value
                    If dept = "" And retdid <> "" Then
                        Dim cret As String
                        cret = getcell(retdid, retcid)
                        Dim cretarr() As String = cret.Split(",")
                        dept = cretarr(0)
                        cell = cretarr(1)
                        lbldid.Value = retdid
                        lblcid.Value = retcid
                    End If
                    tdecd1.InnerHtml = dept
                    tdecd2.InnerHtml = cell
                End If
            End If
            ec.Dispose()
        Else
            If Request.Form("lblsubmit") = "checkcell" Then
                closecells()
                lblsubmit.Value = ""
                ec.Open()
                cid = lblcid.Value
                checkcell(cid)
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "checkdept" Then
                closedepts()
                lblsubmit.Value = ""
                ec.Open()
                did = lbldid.Value
                checkdept(did)
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "savedial" Then
                ec.Open()
                savedial()
                ec.Dispose()
            End If
                tdmsg.InnerHtml = "Department and\or Cell Only Required for Return"
        End If
    End Sub
    Private Sub savedial()
        did = lbldid.Value
        cid = lblcid.Value
        dialid = lblid.Value
        Dim dcnt As Integer = 0
        sql = "select count(*) from meterdiallocs where dialid = '" & dialid & "' and deptid = '" & did & "' and cellid = '" & cid & "'"
        dcnt = ec.Scalar(sql)
        If dcnt = 0 Then
            Dim cmd As New SqlCommand
            cmd.CommandText = "insert into meterdiallocs(dialid, deptid, cellid) values(@dialid, @deptid, @cellid)"
            Dim param = New SqlParameter("@dialid", SqlDbType.VarChar)
            param.Value = dialid
            cmd.Parameters.Add(param)
            Dim param01 = New SqlParameter("@deptid", SqlDbType.VarChar)
            If did = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = did
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@cellid", SqlDbType.VarChar)
            If cid = "0" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = cid
            End If
            cmd.Parameters.Add(param02)
            ec.UpdateHack(cmd)

        End If
        If cid = "0" Then
            lblret.Value = "depts~" & did
        Else
            lblret.Value = "cells~" & did & "~" & cid
        End If
        lblsubmit.Value = "return"

    End Sub
    Private Sub closedepts()
        lblcid.Value = ""
        lblcell.Value = ""
        tdecd2.InnerHtml = ""
        lbleqid.Value = ""
        lbleq.Value = ""
        
    End Sub
    Private Sub closecells()

        lbleqid.Value = ""
        lbleq.Value = ""
        
    End Sub
    Private Function getcell(ByVal did As String, ByVal cid As String) As String
        Dim ret As String
        sql = "select d.dept_line, c.cell_name from dept d " _
            + "left join cells c on c.dept_id = d.dept_id where d.dept_id = '" & did & "'"
        dr = ec.GetRdrData(sql)
        While dr.Read
            dept = dr.Item("dept_line").ToString
            cell = dr.Item("cell_name").ToString
        End While
        ret = dept & "," & cell
        Return ret
    End Function
    Private Function getdept(ByVal did As String) As String
        Dim ret As String
        sql = "select dept_line from dept where dept_id = '" & did & "'"
        ret = ec.strScalar(sql)
        Return ret
    End Function
    Private Sub checkcell(ByVal cis As String)
        cid = lblcid.Value
        cell = lblcell.Value
       
        tdecd2.InnerHtml = cell
        lbltabs.Value = "ok,ok,ok,no,no,ok"
        tdtab.InnerHtml = "Equipment"
        trsave.Attributes.Add("class", "view")
        tdec1a.Attributes.Add("class", "details")
    End Sub
    Private Sub checkdept(ByVal did As String)
        'did = lbldid.Value
        dept = lbldept.Value
        Dim tl As Integer
        'and cell_name = 'No Cells'"
        sql = "select count(*) from Cells where dept_id = '" & did & "' and cell_name <> 'No Cells'"
        tl = ec.Scalar(sql)
        Dim enc As Integer
        sql = "select count(*) from equipment where dept_id = '" & did & "' and (cellid = 0 or cellid is null)"
        enc = "0" 'ec.Scalar(sql)
        If tl <> 0 Then
            PopCells(did)
            
        End If

        If tl <> 0 Then
            tdtab.InnerHtml = "Stations\Cells"
            tdec1.Attributes.Add("class", "view")
            tdec2.Attributes.Add("class", "details")
            If enc <> 0 Then
                tdtab2.Attributes.Add("class", "label")
                tdec1a.Attributes.Add("class", "view")
            End If
        Else
            Dim strMessage As String = "No Cells Issued to this Department"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

        End If
        lbltabs.Value = "ok,ok,ok,no,no,ok"

        tdecd1.InnerHtml = dept
        trsave.Attributes.Add("class", "view")
    End Sub
    Private Sub PopCells(ByVal dept As String)
        Dim ecnt As Integer
        sql = "select cellid , cell_name, ecnt = (select count(*) from equipment e where e.cellid = cells.cellid) " _
        + "from cells where dept_id = " & dept
        'filt = " where dept_id = " & dept
        'dt = "Cells"
        'val = "cellid , cell_name "
        'dr = ec.GetList(dt, val, filt)
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""230"" cellpadding=""1"">")
        sb.Append("<tr>")
        'sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""230""></td>")
        sb.Append("</tr>")
        While dr.Read
            cid = dr.Item("cellid").ToString
            cell = dr.Item("cell_name").ToString
            ecnt = dr.Item("ecnt").ToString
            sb.Append("<tr>")
            If ecnt > 0 Then
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd2('" & cid & "','" & cell & "','yes');"">" & cell & "</td>")
            Else
                sb.Append("<td class=""plainlabelred""><a href=""#"" class=""A1R"" onclick=""getecd2('" & cid & "','" & cell & "','no');"">" & cell & "</td>")
            End If

            '
            sb.Append("</tr>")
        End While
        dr.Close()

        sb.Append("</table>")
        tdec1.InnerHtml = sb.ToString
        tdec.Attributes.Add("class", "details")
        tdec1.Attributes.Add("class", "view")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdec5.Attributes.Add("class", "details")
    End Sub
    Private Sub getdepts(ByVal sid As String)
        Dim ccnt, ecnt As Integer
        sql = "select dept_id, dept_line, ccnt = (select count(*) from cells c where c.dept_id = dept.dept_id), " _
        + "ecnt = (select count(*) from equipment e where e.dept_id = dept.dept_id) from dept where siteid = '" & sid & "'"
        'filt = " where siteid = '" & sid & "'"
        'dt = "Dept"
        'val = "dept_id , dept_line "
        'dr = ec.GetList(dt, val, filt)
        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""230"" cellpadding=""1"">")
        sb.Append("<tr>")
        'sb.Append("<td width=""80""></td>")
        sb.Append("<td width=""230""></td>")
        sb.Append("</tr>")
        While dr.Read
            did = dr.Item("dept_id").ToString
            dept = dr.Item("dept_line").ToString
            ccnt = dr.Item("ccnt").ToString
            ecnt = dr.Item("ecnt").ToString
            sb.Append("<tr>")
            If ccnt = 0 And ecnt = 0 Then
                sb.Append("<td class=""plainlabel""><a href=""#"" class=""A1R""  onclick=""getecd1('" & did & "','" & dept & "','no');"">" & dept & "</td>")
            Else
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getecd1('" & did & "','" & dept & "','yes');"">" & dept & "</td>")
            End If

            'sb.Append("<td class=""plainlabel"">" & ecd & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        lbltabs.Value = "ok,no,no,no,no,no"
        'tdec1.InnerHtml = ""
        'tdec2.InnerHtml = ""
        'tdec3.InnerHtml = ""
        'tdec4.InnerHtml = ""
        sb.Append("</table>")
        tdec.InnerHtml = sb.ToString
        tdec.Attributes.Add("class", "view")
        tdec1.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdtab.InnerHtml = "Departments"
    End Sub
End Class