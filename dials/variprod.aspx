﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="variprod.aspx.vb" Inherits="lucy_r12.variprod" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <link rel="stylesheet" href="wheel6.css" type="text/css" media="screen" />
		<script type="text/javascript" src="jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="wheel5.js"></script>
		<script language="javascript" type="text/javascript">
		    function checkret() {
		        var ret = "ok"; // document.getElementById("lblret").value;
		        if (ret == "ok") {
                //alert(ret)
		            window.parent.handlereturn();
		        }
		    }

        </script>
</head>
<body onload="getpos();">
    <form id="form1" runat="server">
    <div>
    <div id="wheel">
		<div class="val" id="val_angle" runat="server"></div>
        <a id="slider"></a>
    </div>
    <div><table class="table">
    <tr>
    <td class="bluelabel" height="22" width="100">Dial</td>
    <td class="plainlabel" id="tdname" runat="server" colspan="2"></td>
    </tr>
    <tr>
    <td class="bluelabel" height="22">Description</td>
    <td class="plainlabel" id="tddesc" runat="server" colspan="2"></td>
    </tr>
    <tr>
    <td colspan="3" class="plainlabelblue" align="center" height="32">
    Edit Percent Value below if necessary and Submit
    </td>
    </tr>
    <tr>
        
    <td class="label">Percent</td>
    <td width="80"><asp:TextBox ID="txtper" runat="server" Width="60px"></asp:TextBox></td>
    <td width="120"><asp:Button ID="btnsubmit" runat="server" Text="Submit" Width="70px" /></td>
    </tr>
      
    <tr>
    <td colspan="3" class="plainlabelred" align="center" height="32" id="tdmsg" runat="server">
    
    </td>
    </tr>
    <tr>
    <td colspan="3" class="plainlabel" align="right" height="32">
    <a href="#" onclick="checkret();">Return</a>
    </td>
    </tr>    
    </table></div>
    <input type="hidden" id="Text3" />
    <input type="hidden" id="Text4" />
    <input type="hidden" id="Text5" />
    <input type="hidden" id="Text2" runat="server" />
    <input type="hidden" id="Text1" runat="server" />
    <input type="hidden" id="origper" runat="server" />
    <input type="hidden" id="lblid" runat="server" />
    <input type="hidden" id="lbldial" runat="server" />
    <input type="hidden" id="lbldesc" runat="server" />
    <input type="hidden" id="lblleft" runat="server" />
    <input type="hidden" id="lblright" runat="server" />
    <input type="hidden" id="lblper" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblnormdle" runat="server" />
    <input type="hidden" id="lblnormdri" runat="server" />

    </div>
    </form>
</body>
</html>
