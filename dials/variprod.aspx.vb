﻿Imports System.Data.SqlClient
Public Class variprod
    Inherits System.Web.UI.Page
    Dim di As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim dialid, dial, desc As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            dialid = Request.QueryString("dialid").ToString
            lblid.Value = dialid
            dial = Request.QueryString("dial").ToString
            lbldial.Value = dial
            desc = Request.QueryString("desc").ToString
            lbldesc.Value = desc
            tdname.InnerHtml = dial
            tddesc.InnerHtml = desc
            di.Open()
            getdial(dialid)
            di.Dispose()

        Else

        End If

    End Sub
    Private Sub getdial(ByVal dialid As String)
        sql = "select dialpercent, dleft, dright from meterdials where dialid = '" & dialid & "'"
        Dim dp, dle, dri As String
        dr = di.GetRdrData(sql)
        While dr.Read
            dp = dr.Item("dialpercent").ToString
            dle = dr.Item("dleft").ToString
            dri = dr.Item("dright").ToString
        End While
        dr.Close()
        Dim dpd As Decimal
        dpd = System.Convert.ToDecimal(dp)
        Dim dpi As Integer
        dpi = Math.Round(dpd)
        lblper.Value = dpi & "%"
        origper.Value = dpi
        txtper.Text = dpi
        Text1.Value = dle
        Text2.Value = dri
        getnorm()
    End Sub
    Private Sub getnorm()
        sql = "select dleft, dright from meterdialvals where dialval = 100"
        Dim dp, dle, dri As String
        dr = di.GetRdrData(sql)
        While dr.Read
            dp = "100"
            dle = dr.Item("dleft").ToString
            dri = dr.Item("dright").ToString
        End While
        dr.Close()
        lblnormdle.Value = dle
        lblnormdri.Value = dri

    End Sub
   
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsubmit.Click
        Dim dp, dps, dle, dri As String
        dle = Text1.Value
        dri = Text2.Value
        dp = txtper.Text
        dps = lblper.Value
        dps = dps.Replace("%", "")
        dialid = lblid.Value
        If dp = "" Or dps = "" Then
            Exit Sub
        End If
        If dp = "0" Or dps = "0" Then
            Dim strMessage As String = "Cannot Calculate Frequencies for a Dial Value of Zero"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        'If dps <> dp Then
        'dps = dp
        'dle = ""
        'dri = ""
        'End If
        di.Open()
        'added to reset to 100
        Dim origdps As String = origper.Value
        If origdps <> dps And origdps <> "100" Then
            Dim odle, odri As String
            odle = lblnormdle.Value
            odri = lblnormdri.Value
            Dim cmd0 As New SqlCommand
            cmd0.CommandText = "exec usp_dialpm @dialid, @dp, @dps, @dle, @dri"
            Dim param010 = New SqlParameter("@dialid", SqlDbType.VarChar)
            If dialid = "" Then
                param010.Value = System.DBNull.Value
            Else
                param010.Value = dialid
            End If
            cmd0.Parameters.Add(param010)
            Dim param020 = New SqlParameter("@dp", SqlDbType.VarChar)
            If dp = "" Then
                param020.Value = System.DBNull.Value
            Else
                param020.Value = "100"
            End If
            cmd0.Parameters.Add(param020)
            Dim param030 = New SqlParameter("@dps", SqlDbType.VarChar)
            If dps = "" Then
                param030.Value = System.DBNull.Value
            Else
                param030.Value = "100"
            End If
            cmd0.Parameters.Add(param030)
            Dim param040 = New SqlParameter("@dle", SqlDbType.VarChar)
            If dle = "" Then
                param040.Value = System.DBNull.Value
            Else
                param040.Value = odle
            End If
            cmd0.Parameters.Add(param040)
            Dim param050 = New SqlParameter("@dri", SqlDbType.VarChar)
            If dri = "" Then
                param050.Value = System.DBNull.Value
            Else
                param050.Value = odri
            End If
            cmd0.Parameters.Add(param050)
            di.UpdateHack(cmd0)
        End If
        'usp_dialpm(@dialid int, @dp int, @dps int, @dle decimal(10,2), @dri decimal(10,2))
        'sql = "usp_dialpm '" & dialid & "','" & dp & "','" & dps & "','" & dle & "','" & dri & "'"
        'Dim tmp As String = sql
        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_dialpm @dialid, @dp, @dps, @dle, @dri"
        Dim param01 = New SqlParameter("@dialid", SqlDbType.VarChar)
        If dialid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = dialid
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@dp", SqlDbType.VarChar)
        If dp = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = dp
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@dps", SqlDbType.VarChar)
        If dps = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = dps
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@dle", SqlDbType.VarChar)
        If dle = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = dle
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@dri", SqlDbType.VarChar)
        If dri = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = dri
        End If
        cmd.Parameters.Add(param05)



        'di.Update(sql)
        di.UpdateHack(cmd)
        di.Dispose()
        tdmsg.InnerHtml = "Dial Updated to " & dp & "%"
    End Sub
End Class