﻿Imports System.Data.SqlClient
Imports System.IO
Public Class pmo123newco
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim eqid, fuid, db, start, coid, oloc, typ, sid, Login As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                fuid = Request.QueryString("fuid").ToString
                db = Request.QueryString("db").ToString
                oloc = Request.QueryString("oloc").ToString
                lblfuid.Value = fuid
                lbldb.Value = db
                lbloloc.Value = oloc
                copy.Open()
                PopCompRev(fuid)
                copy.Dispose()
            End If
        Else
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = ""
                fuid = lblfuid.Value
                copy.Open()
                PopCompRev(fuid)
                copy.Dispose()
                lblupret.Value = "yes"
            ElseIf Request.Form("lblcompchk") = "delimg" Then
                lblcompchk.Value = ""
                copy.Open()
                DelImg()
                LoadEqPics()
                copy.Dispose()
                'lblchksav.Value = "yes"
            ElseIf Request.Form("lblcompchk") = "checkpic" Then
                lblcompchk.Value = ""
                copy.Open()
                LoadEqPics()
                copy.Dispose()
            ElseIf Request.Form("lblcompchk") = "savedets" Then
                lblcompchk.Value = ""
                copy.Open()
                SaveDets()
                LoadEqPics()
                copy.Dispose()
            End If
        End If

    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr995", "pmlibnewcodets.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim old As String = oldiord 'lbloldorder.Value
            Dim neword As String = txtiorder.Text
            coid = lblcoid.Value
            typ = "co"
            sql = "usp_reordereqimg '" & typ & "','" & coid & "','" & neword & "','" & old & "'"
            copy.Update(sql)
        End If
    End Sub
    Private Sub DelImg()
        coid = lblcoid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        typ = "co"
        sql = "usp_deleqimg '" & typ & "','" & coid & "','" & pid & "','" & old & "'"
        copy.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
        lblcurrp.Value = "0"

    End Sub
    Private Sub PopCompRev(ByVal funcid As String)
        lblfuid.Value = funcid
        Dim eqid As String
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
       
        sql = "select eqid from [" & srvr & "].[" & mdb & "].[dbo].[functions] where func_id = '" & funcid & "'"
        eqid = copy.strScalar(sql)
        lbleqid.Value = eqid
        Dim did, clid As String
        sql = "select dept_id, cellid from [" & srvr & "].[" & mdb & "].[dbo].[equipment] where eqid = '" & eqid & "'"
        dr = copy.GetRdrData(sql)
        While dr.Read
            did = dr.Item("dept_id").ToString
            clid = dr.Item("cellid").ToString
        End While
        dr.Close()
        lbldid.Value = did
        lblclid.Value = clid

        sql = "select * from [" & srvr & "].[" & mdb & "].[dbo].[components] where func_id = '" & funcid & "' order by crouting"
        dr = copy.GetRdrData(sql)
        rptrcomprev.DataSource = dr
        rptrcomprev.DataBind()
        rptrfailrev.DataSource = Nothing
        rptrfailrev.DataBind()
        dr.Close()
    End Sub
    Public Function HighlightRowCP(ByVal rowid As Integer) As String
        Dim bgColor = "#FFFFFF"
        Try
            Dim id As Integer = lblcoid.Value
            If rowid = id Then
                bgColor = "#D6D4FB"
            End If
        Catch ex As Exception

        End Try
        Return bgColor
    End Function

    Private Sub rptrcomprev_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrcomprev.ItemCommand
        Dim ci, pic, pcnt, cp, fi As String
        If e.CommandName = "Select" Then
            ci = CType(e.Item.FindControl("lblcomprevid"), Label).Text
            cp = CType(e.Item.FindControl("lblparco1"), Label).Text
            pic = CType(e.Item.FindControl("lblpurl3"), Label).Text
            pcnt = CType(e.Item.FindControl("lblpcnt3"), Label).Text
            lblcoid.Value = ci
            fi = lblfuid.Value
            copy.Open()
            PopCompRev(fi)

            PopFailRev(ci)
            tdfailrev.Attributes.Add("class", "view")

            LoadEqPics()
            copy.Dispose()

        End If
    End Sub
    Private Sub PopFailRev(ByVal comid As String)
        '*** Multi Add ***
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
       
        '*** Multi Add End***
        sql = "select * from [" & srvr & "].[" & mdb & "].[dbo].[componentfailmodes] where comid = '" & comid & "'"
        sql = "usp_getcfall '" & comid & "'"
        dr = copy.GetRdrData(sql)
        rptrfailrev.DataSource = dr
        rptrfailrev.DataBind()
        dr.Close()
    End Sub
    Private Sub LoadEqPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgco.Attributes.Add("src", "../images/appimages/compimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgco.Attributes.Add("src", "../images2/eng/compimg.gif")
        ElseIf lang = "fre" Then
            imgco.Attributes.Add("src", "../images2/fre/compimg.gif")
        ElseIf lang = "ger" Then
            imgco.Attributes.Add("src", "../images2/ger/compimg.gif")
        ElseIf lang = "ita" Then
            imgco.Attributes.Add("src", "../images2/ita/compimg.gif")
        ElseIf lang = "spa" Then
            imgco.Attributes.Add("src", "../images2/spa/compimg.gif")
        End If

        Dim img, imgs, picid, eqid As String
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
       
        coid = lblcoid.Value
        Dim pcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where comid is not null and comid = '" & coid & "'"
        'sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        pcnt = copy.Scalar(sql)
        lblpcntc.Value = pcnt

        Dim currp As String = lblcurrpc.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpgc.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpgc.Text = "Image 1 of " & pcnt
                lblcurrpc.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] p where p.comid is not null and p.comid = '" & coid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = copy.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)

                If mdb = "laipm3_presdb2" Then
                    img = Replace(img, "..", "http://www.laisoftware.com/laidemo")
                End If

                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                If mdb = "laipm3_presdb2" Then
                    bimg = Replace(bimg, "..", "http://www.laisoftware.com/laidemo")
                End If

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                If mdb = "laipm3_presdb2" Then
                    timg = Replace(timg, "..", "http://www.laisoftware.com/laidemo")
                End If

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimgc.Value = ovimg
                    lblcurrbimgc.Value = ovbimg
                    lblcurrtimgc.Value = ovtimg
                    imgco.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgidc.Value = picid

                    'txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgsc.Value = imgs
            lblbimgsc.Value = bimgs
            lblovimgsc.Value = ovimgs
            lblovbimgsc.Value = ovbimgs
            lblovtimgsc.Value = ovtimgs
            lbliordersc.Value = iorders
        End If
    End Sub

    Private Sub ibtnremcomp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnremcomp.Click
        Dim cb As CheckBox
        Dim ci As String
        copy.Open()
        For Each i As RepeaterItem In rptrcomprev.Items
            If i.ItemType = ListItemType.Item Or i.ItemType = ListItemType.AlternatingItem Then
                cb = CType(i.FindControl("cb2"), CheckBox)
                If cb.Checked Then '
                    ci = CType(i.FindControl("lblcomprevid"), Label).Text 'CType(i.FindControl("lblcomid"), Label).Text
                    sql = "usp_delComp '" & ci & "'"
                    copy.Update(sql)
                    lblcoid.Value = ci
                    DelImg()
                End If
            End If
        Next

        'lblchk.Value = "rem"
        Dim fi As String = lblfuid.Value
        PopCompRev(fi)

        copy.Dispose()
    End Sub

    Private Sub rptrcomprev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrcomprev.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim comid As String = DataBinder.Eval(e.Item.DataItem, "comid").ToString
            Dim imgcoedit As HtmlImage = CType(e.Item.FindControl("imgcoedit"), HtmlImage)
            imgcoedit.Attributes.Add("onclick", "getcompedit('" & comid & "');")

        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2459 As Label
                lang2459 = CType(e.Item.FindControl("lang2459"), Label)
                lang2459.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2459")
            Catch ex As Exception
            End Try
            Try
                Dim lang2460 As Label
                lang2460 = CType(e.Item.FindControl("lang2460"), Label)
                lang2460.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2460")
            Catch ex As Exception
            End Try
            Try
                Dim lang2461 As Label
                lang2461 = CType(e.Item.FindControl("lang2461"), Label)
                lang2461.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2461")
            Catch ex As Exception
            End Try
            Try
                Dim lang2462 As Label
                lang2462 = CType(e.Item.FindControl("lang2462"), Label)
                lang2462.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2462")
            Catch ex As Exception
            End Try
            Try
                Dim lang2463 As Label
                lang2463 = CType(e.Item.FindControl("lang2463"), Label)
                lang2463.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2463")
            Catch ex As Exception
            End Try
            Try
                Dim lang2464 As Label
                lang2464 = CType(e.Item.FindControl("lang2464"), Label)
                lang2464.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2464")
            Catch ex As Exception
            End Try
            Try
                Dim lang2465 As Label
                lang2465 = CType(e.Item.FindControl("lang2465"), Label)
                lang2465.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2465")
            Catch ex As Exception
            End Try
            Try
                Dim lblpgc As Label
                lblpgc = CType(e.Item.FindControl("lblpgc"), Label)
                lblpgc.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lblpgc")
            Catch ex As Exception
            End Try

        End If


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2459 As Label
                lang2459 = CType(e.Item.FindControl("lang2459"), Label)
                lang2459.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2459")
            Catch ex As Exception
            End Try
            Try
                Dim lang2460 As Label
                lang2460 = CType(e.Item.FindControl("lang2460"), Label)
                lang2460.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2460")
            Catch ex As Exception
            End Try
            Try
                Dim lang2461 As Label
                lang2461 = CType(e.Item.FindControl("lang2461"), Label)
                lang2461.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2461")
            Catch ex As Exception
            End Try
            Try
                Dim lang2462 As Label
                lang2462 = CType(e.Item.FindControl("lang2462"), Label)
                lang2462.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2462")
            Catch ex As Exception
            End Try
            Try
                Dim lang2463 As Label
                lang2463 = CType(e.Item.FindControl("lang2463"), Label)
                lang2463.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2463")
            Catch ex As Exception
            End Try
            Try
                Dim lang2464 As Label
                lang2464 = CType(e.Item.FindControl("lang2464"), Label)
                lang2464.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2464")
            Catch ex As Exception
            End Try
            Try
                Dim lang2465 As Label
                lang2465 = CType(e.Item.FindControl("lang2465"), Label)
                lang2465.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2465")
            Catch ex As Exception
            End Try
            Try
                Dim lblpgc As Label
                lblpgc = CType(e.Item.FindControl("lblpgc"), Label)
                lblpgc.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lblpgc")
            Catch ex As Exception
            End Try

        End If
    End Sub
    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2459.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2459")
        Catch ex As Exception
        End Try
        Try
            'lang2460.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2460")
        Catch ex As Exception
        End Try
        Try
            'lang2461.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2461")
        Catch ex As Exception
        End Try
        Try
            'lang2462.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2462")
        Catch ex As Exception
        End Try
        Try
            'lang2463.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2463")
        Catch ex As Exception
        End Try
        Try
            'lang2464.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2464")
        Catch ex As Exception
        End Try
        Try
            lang2465.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lang2465")
        Catch ex As Exception
        End Try
        Try
            lblpgc.Text = axlabs.GetASPXPage("pmlibnewcodets.aspx", "lblpgc")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddcomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewcodets.aspx", "btnaddcomp") & "')")
            btnaddcomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btncopycomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewcodets.aspx", "btncopycomp") & "')")
            btncopycomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ggrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewcodets.aspx", "ggrid") & "')")
            ggrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdel.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewcodets.aspx", "imgdel") & "', ABOVE, LEFT)")
            imgdel.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewcodets.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid260.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewcodets.aspx", "ovid260") & "', ABOVE, LEFT)")
            'ovid260.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub
End Class