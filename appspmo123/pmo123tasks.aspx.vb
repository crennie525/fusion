﻿Imports System.Data.SqlClient
Public Class pmo123tasks
    Inherits System.Web.UI.Page
    Dim pmo As New Utilities
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim eqid, eqnum, sid, ustr, did, clid, locid, fuid, coid, comp, db As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            eqnum = Request.QueryString("eqnum").ToString
            lbleq.Value = eqnum
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            ustr = Request.QueryString("ustr").ToString
            lblustr.Value = ustr
            locid = Request.QueryString("locid").ToString
            lbllocid.Value = locid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            db = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            lbldb.Value = db
        Else
            If Request.Form("lblsubmit") = "addtask" Then
                lblsubmit.Value = ""
                pmo.Open()
                addtask()
                pmo.Dispose()
            ElseIf Request.Form("lblsubmit") = "gettasks" Then
                lblsubmit.Value = ""
                pmo.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                loadtasks(PageNumber)
                pmo.Dispose()
            ElseIf Request.Form("lblsubmit") = "savetask" Then
                lblsubmit.Value = ""
                pmo.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                loadtasks(PageNumber)
                pmo.Dispose()
            End If

            If Request.Form("lblret") = "next" Then
                pmo.Open()
                GetNext()
                pmo.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                pmo.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                loadtasks(PageNumber)
                pmo.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                pmo.Open()
                GetPrev()
                pmo.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                pmo.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                loadtasks(PageNumber)
                pmo.Dispose()
                lblret.Value = ""

            End If
            tdcomp.InnerHtml = lblcomp.Value
            tdfunc.InnerHtml = lblfu.Value
        End If
    End Sub
    Private Sub savetask()
        Dim pmtskid As String = lblpmtskid.Value
        Dim orig = txtotaske.Text
        Dim rev = txttaske.Text
        orig = pmo.ModString2(orig)
        rev = pmo.ModString2(rev)
        If orig <> "" Then
            sql = "update pmtasks set otaskdesc = '" & orig & "' where pmtskid = '" & pmtskid & "'"
            pmo.Update(sql)
        End If
        If rev <> "" Then
            sql = "update pmtasks set taskdesc = '" & rev & "' where pmtskid = '" & pmtskid & "'"
            pmo.Update(sql)
        End If

    End Sub
    Private Sub addtask()
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value
        comp = lblcomp.Value
        ustr = lblustr.Value
        If coid = "" Then
            sql = "usp_AddTask '0', '0', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
        Else
            sql = "usp_AddTask2 '0','0','" & sid & "','" & did & "','" & clid & "','" & eqid & "','" & fuid & "','" & ustr & "','" & coid & "','" & comp & "'"
        End If


        Dim ttid As Integer = pmo.Scalar(sql)
        lblpmtskid.Value = ttid
        db = lbldb.Value
        ifnewpmtaskdets.Attributes.Add("src", "pmo123newtasks.aspx?start=yes&fuid=" + fuid + "&db=" + db)
    End Sub
    Private Sub GetNext()
        eqid = lbleqid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            loadtasks(PageNumber)
        Catch ex As Exception
            pmo.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        eqid = lbleqid.Value
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            loadtasks(PageNumber)
        Catch ex As Exception
            pmo.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub loadtasks(ByVal PageNumber As Integer)
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        coid = lblcoid.Value
        db = lbldb.Value
        If coid <> "" Then
            FilterCNT = "funcid = '" & fuid & "' and comid = '" & coid & "'"
            Filter = "funcid = ''" & fuid & "'' and comid = ''" & coid & "''"
        Else
            FilterCNT = "funcid = '" & fuid & "'"
            Filter = "funcid = ''" & fuid & "''"
        End If
        sql = "select count(*) from pmtasks where " & FilterCNT
        Dim intPgNav, intPgCnt As Integer
        intPgCnt = pmo.Scalar(sql)
        PageSize = "1"
        intPgNav = pmo.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgCnt
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        If intPgCnt <> 0 Then
            Tables = "pmtasks"
            PK = "pmtskid"
            dr = pmo.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            While dr.Read
                txtotaske.Text = dr.Item("otaskdesc").ToString
                txttaske.Text = dr.Item("taskdesc").ToString
                lblpmtskid.Value = dr.Item("pmtskid").ToString
            End While
            dr.Close()
            ifnewpmtaskdets.Attributes.Add("src", "pmo123newtasks.aspx?start=yes&fuid=" + fuid + "&db=" + db)
        End If
    End Sub
End Class