﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123meter.aspx.vb" Inherits="lucy_r12.pmo123meter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        function getmeter(mtrid, mtr, uni, wku) {
            var ret = mtrid + "," + mtr + "," + uni + "," + wku;
            window.returnValue = ret;
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
        <tr>
        <td id="tdmeters" runat="server"></td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
