﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123newco.aspx.vb" Inherits="lucy_r12.pmo123newco" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmlibnewcodetsaspx_a.js"></script>
    <script language="javascript" type="text/javascript">
        function addpic() {
            var coid = document.getElementById("lblcoid").value;
            var ro = "0"; //document.getElementById("lblro").value;
            if (coid != "") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../equip/equploadimagedialog.aspx?typ=co&eqid=&funcid=&comid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
                if (eReturn == "ok") {
                    document.getElementById("lblcompchk").value = "checkpic";
                    document.getElementById("form1").submit()
                }
            }
            else {
                alert("No Component Selected")
            }

        }
        function GetGrid() {
            fuid = document.getElementById("lblfuid").value;
            try {
                var eReturn = window.showModalDialog("../equip/compseqdialog.aspx?fuid=" + fuid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:700px; resizable=yes");
                if (eReturn) {
                    if (eReturn == "log") {
                        window.parent.doref();
                    }
                    else {
                        document.getElementById("lblcompchk").value = "1";
                        document.getElementById("form1").submit()
                    }
                }
                else {
                    document.getElementById("lblcompchk").value = "1";
                    document.getElementById("form1").submit()
                }
            }
            catch (err) {

            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="980" style="position: absolute; top: 0px; left: 0px">
            <tr>
                <td class="labelibl">
                    <asp:Label ID="lang2459" runat="server">Select a Component to view Component Image and Failure Mode Details</asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <asp:Repeater ID="rptrcomprev" runat="server">
                        <HeaderTemplate>
                            <table>
                                <tr>
                                    <td class="btmmenu plainlabel" align="center">
                                        <img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16">
                                    </td>
                                    <td class="btmmenu plainlabel" width="50px">
                                        <asp:Label ID="lang2460" runat="server">Edit</asp:Label>
                                    </td>
                                    <td class="btmmenu plainlabel" width="150px">
                                        <asp:Label ID="lang2461" runat="server">Component</asp:Label>
                                    </td>
                                    <td class="btmmenu plainlabel" width="200px">
                                        <asp:Label ID="lang2462" runat="server">Description</asp:Label>
                                    </td>
                                    <td class="btmmenu plainlabel" width="200px">
                                        <asp:Label ID="lang2463" runat="server">Special Identifier</asp:Label>
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="coselrow" runat="server" bgcolor='<%# HighlightRowCP(DataBinder.Eval(Container.DataItem, "comid"))%>'>
                                <td class="plainlabel">
                                    <asp:CheckBox ID="cb2" runat="server"></asp:CheckBox>
                                </td>
                                <td>
                                    <img src="../images/appbuttons/minibuttons/lilpentrans.gif" id="imgcoedit" runat="server">
                                </td>
                                <td class="plainlabel">
                                    <asp:LinkButton CommandName="Select" ID="Linkbutton3" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>'
                                        runat="server">
                                    </asp:LinkButton>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="Label10" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="Label25" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lblcomprevid" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lblpurl3" Text='<%# DataBinder.Eval(Container.DataItem,"picurl")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lblpcnt3" Text='<%# DataBinder.Eval(Container.DataItem,"piccnt")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lblparco1" Text='<%# DataBinder.Eval(Container.DataItem,"origparent")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
                <td valign="top" align="center" width="200" id="tdfailrev" runat="server">
                    <asp:Repeater ID="rptrfailrev" runat="server">
                        <HeaderTemplate>
                            <table>
                                <tr>
                                    <td class="btmmenu plainlabel" width="200px">
                                        <asp:Label ID="lang2464" runat="server">Failure Modes</asp:Label>
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="tbg">
                                <td class="plainlabel">
                                    <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"failuremode")%>' runat="server"
                                        ID="Label8">
                                    </asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
                <td valign="top" align="center" width="230">
                    <table>
                        <tr>
                            <td valign="top" align="center" colspan="2" height="40">
                                <img id="imgco" onclick="getbig();" height="206" src="../images/appimages/compimg.gif"
                                    width="216" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <table>
                                    <tr>
                                        <td class="bluelabel" width="80">
                                            <asp:Label ID="lang2465" runat="server">Order</asp:Label>
                                        </td>
                                        <td width="60">
                                            <asp:TextBox ID="txtiorder" runat="server" Width="40px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                        <td width="20">
                                            <img onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif">
                                        </td>
                                        <td width="20">
                                            <img onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)"
                                                onclick="addpic();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif">
                                        </td>
                                        <td width="20">
                                            <img id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
                                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/del.gif" runat="server">
                                        </td>
                                        <td width="20">
                                            <img id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)"
                                                onclick="savdets();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="ifirstc" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="iprevc" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="140">
                                            <asp:Label ID="lblpgc" runat="server" CssClass="bluelabel">Image 0 of 0</asp:Label>
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="inextc" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                                runat="server">
                                        </td>
                                        <td width="20" style="border-right: blue 1px solid">
                                            <img id="ilastc" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="ibtnremcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif">
                    </asp:ImageButton><img id="btnaddcomp" onmouseover="return overlib('Add a New Component Record')"
                        onclick="GetCompDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                        width="20" runat="server"><img id="btncopycomp" onmouseover="return overlib('Lookup Component Records to Copy')"
                            onclick="GetCompCopy();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                            width="20" runat="server">
                    <img id="ggrid" onmouseover="return overlib('Edit in Grid View')" onclick="GetGrid();"
                        onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/grid1.gif"
                        width="20" runat="server">
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblcoid" runat="server" NAME="lblcoid">
			<input type="hidden" id="lblfuid" runat="server" NAME="lblfuid"> <input type="hidden" id="lbldb" runat="server" NAME="lbldb">
			<input id="lblpcntc" type="hidden" runat="server" NAME="lblpcntc"> <input id="lblcurrpc" type="hidden" runat="server" NAME="lblcurrpc">
			<input id="lblimgsc" type="hidden" name="lblimgsc" runat="server"> <input id="lblimgidc" type="hidden" name="lblimgidc" runat="server">
			<input id="lblovimgsc" type="hidden" name="lblovimgsc" runat="server"> <input id="lblovbimgsc" type="hidden" name="lblovbimgsc" runat="server">
			<input id="lblcurrimgc" type="hidden" name="lblcurrimgc" runat="server"> <input id="lblcurrbimgc" type="hidden" name="lblcurrbimgc" runat="server">
			<input id="lblbimgsc" type="hidden" name="lblbimgsc" runat="server"><input id="lbliordersc" type="hidden" name="lbliordersc" runat="server">
			<input id="lbloldorderc" type="hidden" runat="server" NAME="lbloldorderc"> <input id="lblovtimgsc" type="hidden" name="lblovtimgsc" runat="server">
			<input id="lblcurrtimgc" type="hidden" name="lblcurrtimgc" runat="server"> <input type="hidden" id="lbloloc" runat="server">
			<input id="lblpcnt" type="hidden" runat="server" NAME="lblpcnt"> <input id="lblcurrp" type="hidden" runat="server" NAME="lblcurrp">
			<input id="lblimgs" type="hidden" name="lblimgs" runat="server"> <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
			<input id="lblovimgs" type="hidden" name="lblovimgs" runat="server"> <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
			<input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server"> <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
			<input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"><input type="hidden" id="lblcnt" runat="server" NAME="lblcnt">
			<input type="hidden" id="lblcompchk" runat="server"><input type="hidden" id="lblchk" runat="server">
			<input type="hidden" id="lbleqid" runat="server"> <input type="hidden" id="lblsid" runat="server">
			<input type="hidden" id="lbldid" runat="server"> <input type="hidden" id="lblclid" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog"> <input type="hidden" id="lblupret" runat="server">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
