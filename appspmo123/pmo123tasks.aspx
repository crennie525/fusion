﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123tasks.aspx.vb" Inherits="lucy_r12.pmo123tasks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="javascript" type="text/javascript">
        function doref() {

        }
        function setref() {

        }
        function getco() {
            fuid = document.getElementById("lblfuid").value;  
            if (fuid != "") {
                var eReturn = window.showModalDialog("../equip/complistdialog.aspx?fuid=" + fuid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    //returns coid and comp
                    var ret = eReturn;
                    var retarr = ret.split(",");
                    var coid = retarr[0];
                    var comp = retarr[1];
                    document.getElementById("tdcomp").innerHTML = comp;
                    document.getElementById(lblcoid).value = coid;
                    document.getElementById("lblcomp").value = comp;
                    //need postback
                    document.getElementById("lblsubmit").value = "gettasks"
                    document.getElementById("form1").submit();
                    

                }
            }

        }
        function savetask() {
        var ttid = document.getElementById("lblpmtskid").value;
        if (ttid != "") {
            var o = document.getElementById("txtotaske").value;
            var r = document.getElementById("txttaske").value;
            if (o.length > 500) {
                alert("Original Task Limited to 500 Characters")
            }
            else if (r.length > 500) {
                alert("Revised Task Limited to 500 Characters")
            }
            else {
                document.getElementById("lblsubmit").value = "savetask"
                document.getElementById("form1").submit();
            }
        }
        }
        function getdt(who) {
            //ttid, eqid, coid,
            var ttid = document.getElementById("lblpmtskid").value;
            if (ttid != "") {
                var eqid = document.getElementById("lbleqid").value;
                var coid = document.getElementById("lblcoid").value;
                var sid = document.getElementById("lblsid").value;
                var ustr = document.getElementById("lblustr").value;
                var eReturn = window.showModalDialog("pmo123e2rdetsdialog.aspx?eqid=" + eqid + "&ttid=" + ttid + "&coid=" + coid + "&who=" + who + "&sid=" + sid + "&ustr=" + ustr + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:600px; resizable=yes");
                if (eReturn) {

                }
            }
        }
        function getfu2() {
            var eqnum = document.getElementById("lbleq").value;
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("../equip/fulist2dialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn;
                    var retarr = ret.split(",");
                    var fid = retarr[0];
                    var cid = retarr[1];
                    var func = retarr[2];
                    var comp = retarr[3];
                    document.getElementById("lblcoid").value = cid;
                    document.getElementById("lblcomp").value = comp;
                    document.getElementById("lblfuid").value = fid;
                    document.getElementById("lblfu").value = func;

                    document.getElementById("tdfunc").innerHTML = func;
                    document.getElementById("tdcomp").innerHTML = comp;
  
                    //need postback
                    document.getElementById("lblsubmit").value = "gettasks"
                    document.getElementById("form1").submit();
                }
            }
        }
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="POSITION: absolute; TOP: 0px; LEFT: 0px;">
    <table>
    <tr>
    <td>
    <table>
    <tr>
    <td class="bluelabel" height="22">Current Function</td>
    <td class="plainlabel" id="tdfunc" runat="server" width="220"></td>
    <td>
    <img id="img1" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getfu2();" />
    </td>
    <td>
    <img id="img2" runat="server" src="../images/appbuttons/minibuttons/addmod.gif" onclick="addtask();" />
    </td>
    </tr>
    <tr>
    <td class="bluelabel">Current Component</td>
    <td class="plainlabel" id="tdcomp" runat="server" width="220"></td>
    <td>
    <img id="img4" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getco();" />
    </td>
    <td>
    <img id="img3" runat="server" src="../images/appbuttons/minibuttons/addmod.gif" onclick="addtask();" />
    </td>
    </tr>
    </table>
    </td>
    </tr>
    
    <tr>
    <td><table>
            <tr>
                <td class="label">
                    Original Task
                </td>
                <td>
                </td>
                <td class="label">
                    Revised Task
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtotaske" runat="server" Width="320px" TextMode="MultiLine" 
                       Height="80px"></asp:TextBox>
                </td>
                <td>
                    <img id="imgotde" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getdt('orig');" />
                </td>
                <td>
                    <asp:TextBox ID="txttaske" runat="server" Width="320px" TextMode="MultiLine" 
                        Height="80px"></asp:TextBox>
                </td>
                <td>
                    <img id="imgtde" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getdt('rev');" />
                </td>
                
            </tr>
            <tr>
            <td class="plainlabel" align="right" colspan="4"><a href="#" onclick="savetask();">Save Changes</a></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0" width="300">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table></td>
    </tr>
    <tr>
    <td class="bluelabel" height="22">Current Tasks</td>
    </tr>
    <tr>
					<td><iframe style="WIDTH: 700px; HEIGHT: 300px" id="ifnewpmtaskdets" src="../sbhold.htm"
							frameborder="0" runat="server"></iframe>
					</td>
				</tr>
    </table>
        
    </div>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblfu" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lbltasknum" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lbllocid" runat="server" />
    <input id="txtpg" type="hidden" runat="server" />
    <input id="txtpgcnt" type="hidden" runat="server" />
    <input type="hidden" id="lblcnt" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbldb" runat="server" />
    </form>
</body>
</html>
