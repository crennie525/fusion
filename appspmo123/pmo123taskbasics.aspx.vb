﻿Imports System.Data.SqlClient
Public Class pmo123taskbasics
    Inherits System.Web.UI.Page
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Dim tmod As New transmod
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim sql As String
    Dim eqid, sid, coid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            tasks.Open()
            GetLists()
            getmetercnt(eqid)
            tasks.Dispose()
            txtqty.Text = "1"
            ddtype.Attributes.Add("onchange", "getstep('6');")
            ddpt.Attributes.Add("onchange", "getstep('7');")
            ddskill.Attributes.Add("onchange", "getstep('8');")
            ddeqstat.Attributes.Add("onchange", "getstep('11');")
        Else
            If Request.Form("lblsubmit") = "gettasks" Then
                lblsubmit.Value = ""
                coid = lblcoid.Value
                tasks.Open()
                PopFailList(coid)
                tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub getmetercnt(ByVal eqid As String)
        sql = "select count(*) from meters where eqid = '" & eqid & "'"
        Dim mcnt As Integer = tasks.Scalar(sql)
        If mcnt > 0 Then
            lblhasmeter.Value = "yes"
        Else
            lblhasmeter.Value = "no"
        End If
    End Sub
    Private Sub PopFailList(ByVal comp As String)
        sql = "select count(*) from componentfailmodes where comid = '" & comp & "'"
        Dim ccnt As Integer = tasks.Scalar(sql)
        If ccnt <> 0 Then
            Dim tfail As String = lbltaskfail.Value
            If tfail <> "" Then
                sql = "select compfailid, failuremode " _
            + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" & tfail & ")"
            Else
                sql = "select compfailid, failuremode " _
            + "from componentfailmodes where comid = '" & comp & "'"
            End If

            'sql = "usp_getcfall_tskna '" & comp & "'"
            'Try

            dr = tasks.GetRdrData(sql)
            'Catch ex As Exception
            'dr = tasksadd.GetRdrData(sql)
            'End Try

            lbfailmaster.DataSource = dr
            lbfailmaster.DataTextField = "failuremode"
            lbfailmaster.DataValueField = "compfailid"
            lbfailmaster.DataBind()
            dr.Close()
        Else
            lblret.Value = "nocompfail"
        End If

    End Sub
    Private Sub GetLists()

        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where tasktype <> 'Select' order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0

        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = tasks.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where siteid = '" & sid & "'"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills"
        End If
        dr = tasks.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0


        sql = "select ptid, pretech " _
        + "from pmPreTech"
        dr = tasks.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        ddpt.DataBind()
        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0


        sql = "select statid, status " _
        + "from pmStatus"
        dr = tasks.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        ddeqstat.DataBind()
        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0

    End Sub

    Protected Sub btntocomp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        Dim Item As ListItem
        Dim f, fi As String
        Dim ipar As Integer = 0
        Dim comp As String = lblcoid.Value
        tasks.Open()

        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(f, fi)
            End If
        Next

        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        Dim tfail As String = lbltaskfail.Value
        Dim tfails As String = lbltaskfailstr.Value
        Dim tfailarr() As String = tfail.Split(",")
        Dim tfailsarr() As String = tfails.Split(",")
        Dim i As Integer
        For i = 0 To tfailarr.Length - 1
            lbfailmodes.Items.Add(tfailsarr(i))
        Next

    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String)
        Dim tfail As String = lbltaskfail.Value
        Dim tfails As String = lbltaskfailstr.Value
        tfail = tfail.Replace(fi, "")
        tfail = tfail.Replace("," + fi, "")
        tfails = tfails.Replace(f, "")
        tfails = tfails.Replace("," + f, "")
        lbltaskfail.Value = tfail
        lbltaskfailstr.Value = tfails
    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String)
        Dim tfail As String = lbltaskfail.Value
        Dim tfails As String = lbltaskfailstr.Value
        If tfail = "" Then
            tfail = fi
            tfails = f
        Else
            tfail += "," & fi
            tfails += "," & f
        End If
        lbltaskfail.Value = tfail
        lbltaskfailstr.Value = tfails
    End Sub

    Protected Sub btnfromcomp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        Dim Item As ListItem
        Dim f, fi As String
        Dim ipar As Integer = 0
        Dim comp As String = lblcoid.Value
        tasks.Open()

        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(f, fi)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            

        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
End Class