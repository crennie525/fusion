﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123comp.aspx.vb" Inherits="lucy_r12.pmo123comp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
	<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
	
	<script language="JavaScript" type="text/javascript">
	    function addcomp() {
	        var co = document.getElementById("txtconame").value;
	        if (length.co > 100) {
                alert("Component Name Limited to 100 Characters")
	        }
	        else {
	            document.getElementById("lblsubmit").value = "addcomp";
	            document.getElementById("form1").submit();
	        }
	    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="d1">
    <table>
    <tr>
    <td class="bluelabel">Enter a New Component Name</td>
	<td><asp:textbox id="txtconame" runat="server" Width="180px" cssclass="plainlabel" MaxLength="100"></asp:textbox></td>
	<td><img src="../images/appbuttons/minibuttons/addmod.gif" alt="" onclick="addcomp();" /></td>				
    </tr>
    </table>
    </div>
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />

    </form>
</body>
</html>
