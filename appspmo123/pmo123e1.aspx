﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123e1.aspx.vb" Inherits="lucy_r12.pmo123e1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <link rel="stylesheet" type="text/css" href="../styles/reports.css" />
    <script language="javascript" type="text/javascript" src="../scripts/smartscroll4.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="JavaScript" type="text/javascript">
        function clearall() {

            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblcoid").value = "";
            document.getElementById("lblncid").value = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            document.getElementById("lblrettyp").value = "";
        }
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lu";
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                var did = ret[0];
                document.getElementById("lbldchk").value = "yes";
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                var clid = ret[2];
                document.getElementById("lblclid").value = ret[2];
                if (ret[2] != "") {
                    document.getElementById("lblchk").value = "yes";
                }
                else {
                    document.getElementById("lblchk").value = "yes";
                }
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                var eqid = ret[4];
                document.getElementById("lbleqid").value = ret[4];
                var fuid = ret[6];
                document.getElementById("lblfuid").value = ret[6];
                var coid = ret[8];
                document.getElementById("lblcoid").value = ret[8];
                document.getElementById("lblncid").value = ret[10];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";
                if (coid != "") {
                    document.getElementById("lbllvl").value = "co";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (fuid != "") {
                    document.getElementById("lbllvl").value = "fu";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (eqid != "") {
                    document.getElementById("lbllvl").value = "eq";
                    //alert(document.getElementById("lbllvl").value)
                    document.getElementById("lblgototasks").value = "1";
                }
                else {
                    document.getElementById("lbllvl").value = "loc";
                    document.getElementById("lblgototasks").value = "1";
                }
                if (did != "") {
                    document.getElementById("lbldchk").value = "yes";
                }
                if (clid != "") {
                    document.getElementById("lblchk").value = "yes";
                }
                else {
                    document.getElementById("lblchk").value = "no";
                }
                checkret();
            }
        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lu"
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lu&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                alert(eReturn)
                var ret = eReturn.split("~");
                var lid = ret[0];
                document.getElementById("lbldchk").value = "no";
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc2").innerHTML = ret[2];
                var eqid = ret[3];
                document.getElementById("lbleqid").value = ret[3];
                var fuid = ret[5];
                document.getElementById("lblfuid").value = ret[5];
                var coid = ret[7];
                document.getElementById("lblcoid").value = ret[7];
                document.getElementById("trlocs").className = "view";
                document.getElementById("lblrettyp").value = "locs";
                if (coid != "") {
                    document.getElementById("lbllvl").value = "co";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (fuid != "") {
                    document.getElementById("lbllvl").value = "fu";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (eqid != "") {
                    document.getElementById("lbllvl").value = "eq";
                    document.getElementById("lblgototasks").value = "1";
                }
                else {
                    document.getElementById("lbllvl").value = "loc";
                    document.getElementById("lblgototasks").value = "1";
                }
                if (lid != "") {
                    document.getElementById("lbldchk").value = "no";
                }
                checkret();

            }
        }
        function checkret() {
            var lid = document.getElementById("lbllid").value;
            var did = document.getElementById("lbldid").value;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc"
                }
                else {
                    typ = "reg"
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc"
                }
            }
            document.getElementById("lbltyp").value = typ;
            //handlearch();
            document.getElementById("lblpar").value = "0"
            //document.getElementById("lblgototasks").value= "1";
            //document.getElementById("lbllvl").value = "";
            checkeq();
        }
        function checkeq() {
            var chkit = document.getElementById("lbllvl").value;
            //alert(chkit)
            var eqid = document.getElementById("lbleqid").value;
            var db = document.getElementById("lbldb").value;
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;

            var typ = document.getElementById("lbltyp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var sid = document.getElementById("lblsid").value;
            var cid = "0";
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            if (chkit == "eq") {
            //alert("check on return from locs")
                document.getElementById("ifneweq").src = "pmo123neweq.aspx?start=yes&eqid=" + eqid + "&db=" + db + "&chk=" + chk + "&dchk=" + dchk;
            }
            else if (chkit == "loc") {
                document.getElementById("ifneweq").src = "pmo123neweqgrid.aspx?typ=" + typ + "&lid=" + lid + "&loc=" + loc + "&sid=" + sid + "&cid=" + cid + "&did=" + did + "&clid=" + clid + "&db=" + db + "&dchk=" + dchk + "&chk=" + chk;
            }
        }
        function handleret2() {
        
            var db = document.getElementById("lbldb").value;
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;

            var typ = document.getElementById("lbltyp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var sid = document.getElementById("lblsid").value;
            var cid = "0";
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            //document.getElementById("ifneweq").src = "";
            document.getElementById("ifneweq").src = "pmo123neweq.aspx?start=no";
            var timer = window.setTimeout("handleret3();", 3000);
           
        }
        function handleret3() {

            var db = document.getElementById("lbldb").value;
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;

            var typ = document.getElementById("lbltyp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var sid = document.getElementById("lblsid").value;
            var cid = "0";
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            //document.getElementById("ifneweq").src = "";
            document.getElementById("ifneweq").src = "pmo123neweq.aspx?start=no";
            //var timer = window.setTimeout("handleret2();", 5000);
            document.getElementById("ifneweq").src = "pmo123neweqgrid.aspx?typ=" + typ + "&lid=" + lid + "&loc=" + loc + "&sid=" + sid + "&cid=" + cid + "&did=" + did + "&clid=" + clid + "&db=" + db + "&chk=" + chk + "&dchk=" + dchk;
            //document.getElementById("ifnewfu").src = "";
            document.getElementById("ifnewfu").src = "pmo123newfu.aspx?start=no";
        }
        function handleneweq(eqid) {
        
            var db = document.getElementById("lbldb").value;
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;

            var typ = document.getElementById("lbltyp").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var sid = document.getElementById("lblsid").value;
            var cid = "0";
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            document.getElementById("ifnewfu").src = "pmo123newfu.aspx?start=yes&typ=" + typ + "&lid=" + lid + "&loc=" + loc + "&sid=" + sid + "&cid=" + cid + "&did=" + did + "&clid=" + clid + "&db=" + db + "&eqid=" + eqid;

        }
        function handlecompnew(fuid) {
            document.getElementById("lblfuid").value = fuid;
            //document.getElementById("lblsubmit").value = "getcomprev";
            //document.getElementById("form1").submit();
            var db = document.getElementById("lbldb").value;
            var oloc = ""; // document.getElementById("lbloloc").value;
            var sid = document.getElementById("lblsid").value;
            document.getElementById("ifnewcodets").src = "pmo123newco.aspx?start=yes&fuid=" + fuid + "&db=" + db + "&oloc=" + oloc + "&sid=" + sid;
            document.getElementById("ifnewpmtaskdets").src = "pmo123newtasks.aspx?start=yes&fuid=" + fuid + "&db=" + db;
            //document.getElementById("ifnewtpmtaskdets").src = "pmlibnewtpmtaskdets.aspx?start=yes&fuid=" + fuid + "&db=" + db;

        }

        function handlecompnewrem() {
            var db = document.getElementById("lbldb").value;
            var oloc = ""; // document.getElementById("lbloloc").value;
            document.getElementById("ifnewcodets").src = "pmo123newco.aspx?start=no";
            document.getElementById("ifpmtaskdets").src = "pmo123newtasks.aspx?start=no";
            //document.getElementById("iftpmtaskdets").src = "pmlibnewtpmtaskdets.aspx?start=no";
        }
        function setref() {
            //window.parent.setref();
        }
        function handleret() {
            var retval = document.getElementById("lblretval").value
            //alert("../mainmenu/NewMainMenu2.aspx" + retval)
            window.location = "../mainmenu/NewMainMenu2.aspx?dummy=yes" + retval;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellspacing="0">
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                </td>
                <td class="thdrsingrt label" width="978" colspan="2">
                    <asp:Label ID="lang2289" runat="server">Add/Edit Equipment, Functions/Assemblies &amp; Components</asp:Label>
                </td>
            </tr>
            <tr>
					<td colspan="3">
						<table>
							<tr>
								<td id="tddepts" class="bluelabel" runat="server">Use Departments</td>
								<td><IMG onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								<td id="tdlocs1" class="bluelabel" runat="server">Use Locations</td>
								<td><IMG onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
                                <td class="plainlabel">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="handleret();">Return to Main Menu</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trdepts" class="details" runat="server">
					<td colspan="2">
						<table>
							<tr>
								<td class="label" width="110">Department</td>
								<td id="tddept" class="plainlabel" width="170" runat="server"></td>
								<td width="50"></td>
							</tr>
							<tr>
								<td class="label">Station\Cell</td>
								<td id="tdcell" class="plainlabel" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trlocs" class="details" runat="server">
					<td colspan="2">
						<table>
							<tr>
								<td class="label">Location</td>
								<td id="tdloc2" class="plainlabel" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="traddeq" runat="server" class="details">
                <td colspan="3">
                <table>
                <tr>
                <td class="label tbg" width="160" align="left"><asp:Label id="lang2167" runat="server">Add New Equipment#</asp:Label></td>
									<td width="140" class="tbg"><asp:textbox id="txtneweq" runat="server" Width="130px" MaxLength="50"></asp:textbox></td>
									<td width="60" class="tbg"><asp:imagebutton id="btnAddEq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton></td>
                </tr>
                </table>
                </td>
                
                </tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="1" width="1000">
							<tr>
								<td class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="978"><asp:label id="lang2197" runat="server">Equipment Details</asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td colspan="3"><iframe style="BACKGROUND-COLOR: white; WIDTH: 1000px; HEIGHT: 330px" id="ifneweq"
							src="pmo123neweq.aspx?start=no" frameborder="0" runat="server"></iframe>
					</td>
				</tr>
                <tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="1" width="1000">
							<tr>
								<td class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="978"><asp:label id="lang2198" runat="server">Function Details</asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><iframe style="WIDTH: 1000px; HEIGHT: 350px" id="ifnewfu" src="pmo123newfu.aspx?start=no"
							frameBorder="no" runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="1" width="1000">
							<tr>
								<td class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="978"><asp:label id="lang2199" runat="server">Component Details</asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><iframe style="WIDTH: 1000px; HEIGHT: 350px" id="ifnewcodets" src=""
							frameBorder="no" runat="server"></iframe>
					</td>
				</tr>
                <tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="1" width="1000">
							<tr>
								<td class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="978"><asp:label id="lang2200" runat="server">PM Task Details</asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><iframe style="WIDTH: 1000px; HEIGHT: 330px" id="ifnewpmtaskdets" src=""
							frameBorder="no" runat="server"></iframe>
					</td>
				</tr>
        </table>
    </div>
    <input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden" runat="server" />
    <input id="lblcid" type="hidden" value="0" name="lblcid" runat="server"> <input id="lbltab" type="hidden" name="lbltab" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"><input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"><input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="lbldchk" type="hidden" name="lbldchk" runat="server"><input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblcoid" type="hidden" name="lblcoid" runat="server"> <input id="lblapp" type="hidden" runat="server" NAME="lblapp">
			<input id="lblpar2" type="hidden" value="0" name="lblpar2" runat="server"><input id="lblpar" type="hidden" value="0" name="lblpar" runat="server">
			<input id="lblgototasks" type="hidden" name="lblgototasks" runat="server"><input id="appchk" type="hidden" name="appchk" runat="server">
			<input id="lbllvl" type="hidden" runat="server" NAME="lbllvl"> <input id="lbllid" type="hidden" runat="server" NAME="lbllid">
			<input id="lblsubmit" type="hidden" runat="server" NAME="lblsubmit"><input id="lbltyp" type="hidden" runat="server" NAME="lbltyp">
			<input id="lblstart" type="hidden" runat="server" NAME="lblstart"><input id="lbllochold" type="hidden" runat="server" NAME="lbllochold">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog"> <input type="hidden" id="lblfslang" runat="server" NAME="lblfslang">
			<input type="hidden" id="lbldept" runat="server"> <input type="hidden" id="lblcell" runat="server"><input id="lblrettyp" type="hidden" runat="server" NAME="lblrettyp">
			<input type="hidden" id="lblncid" runat="server"><input type="hidden" id="lblloc" runat="server">
            <input type="hidden" id="lbldb" runat="server" />
            <input type="hidden" id="lblretval" runat="server" />
    </form>
</body>
</html>
