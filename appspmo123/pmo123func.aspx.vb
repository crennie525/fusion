﻿Imports System.Data.SqlClient
Imports System.Text

Public Class pmo123func
    Inherits System.Web.UI.Page
    Dim fun As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid, ustr As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            ustr = Request.QueryString("ustr").ToString
            lbleqid.Value = eqid
            fun.Open()
            getfunc()
            fun.Dispose()
        Else
            If Request.Form("lblsubmit") = "addfunc" Then
                lblsubmit.Value = ""
                ifimge.Attributes.Add("src", "")
                fun.Open()
                addfunc()
                getfunc()
                fun.Dispose()
            End If
        End If
    End Sub
    Private Sub getfunc()
        eqid = lbleqid.Value
        sql = "select func_id, func, spl from functions where eqid = '" & eqid & "'"
        Dim sb As New StringBuilder
        sb.Append("<table>")
        Dim fid, func, spl As String
        dr = fun.GetRdrData(sql)
        While dr.Read
            fid = dr.Item("func_id").ToString
            func = dr.Item("func").ToString
            spl = dr.Item("spl").ToString
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a href=""#"" onclick=""getpics('" & fid & "','" & func & "');"">" & func & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & spl & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divfu.InnerHtml = sb.ToString


    End Sub

    Private Sub addfunc()
        eqid = lbleqid.Value
        Dim func As String = txtfunc.Text
        Dim spl As String = txtspl.Text
        Dim fcnt As Integer = 0
        sql = "select count(*) from functions where func = '" & func & "' and eqid = '" & eqid & "'"
        fcnt = fun.Scalar(sql)
        If fcnt = 0 Then
            If spl <> "" Then
                sql = "insert into functions (func, spl, createdby, createdate, eqid) values ('" & func & "','" & spl & "','" & ustr & "',getDate(),'" & eqid & "')"
            Else
                sql = "insert into functions (func, createdby, createdate, eqid) values ('" & func & "','" & ustr & "',getDate(),'" & eqid & "')"
            End If
            fun.Update(sql)
            txtfunc.Text = ""
            txtspl.Text = ""
        Else
            Dim strMessage As String = "Cannot Enter Duplicate Function Name for an Equipment Record"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
End Class