﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123neweqgrid.aspx.vb"
    Inherits="lucy_r12.pmo123neweqgrid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/EqBotGridaspx.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="position: absolute; top: 0px; left: 0px" width="960">
            <tr>
                <td colspan="3">
                    <table>
                        <tr height="20">
                            <td class="label tbg" width="160" align="left">
                                <asp:Label ID="lang2167" runat="server">Add New Equipment#</asp:Label>
                            </td>
                            <td width="140" class="tbg">
                                <asp:TextBox ID="txtneweq" runat="server" Width="130px" MaxLength="50"></asp:TextBox>
                            </td>
                            <td width="60" class="tbg">
                                <asp:ImageButton ID="btnAddEq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif">
                                </asp:ImageButton>
                            </td>
                            <td width="360" class="bluelabel tbg" id="tduse" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Repeater ID="rptreq" runat="server">
                        <HeaderTemplate>
                            <table cellspacing="0" width="960">
                                <tr class="tbg" height="20">
                                    <td class="btmmenu plainlabel" width="295">
                                        <asp:LinkButton OnClick="SortEq" ID="lbsorteq" runat="server">
                                            <asp:Label ID="lang2168" runat="server">Equipment#</asp:Label></asp:LinkButton>
                                    </td>
                                    <td class="btmmenu plainlabel" width="295">
                                        <asp:Label ID="lang2169" runat="server">Description</asp:Label>
                                    </td>
                                    <td class="btmmenu plainlabel" width="295">
                                        <asp:Label ID="lang2170" runat="server">Special Identifier</asp:Label>
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="tbg" height="20">
                                <td class="plainlabel">
                                    &nbsp;
                                    <asp:LinkButton ID="Label4" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                        runat="server">
                                    </asp:LinkButton>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel">
                                    <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lbleqloctypei" Text='<%# DataBinder.Eval(Container.DataItem,"loctype")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="transrowblue" height="20">
                                <td class="plainlabel transrowblue">
                                    &nbsp;
                                    <asp:LinkButton ID="Linkbutton1" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                        runat="server">
                                    </asp:LinkButton>
                                </td>
                                <td class="plainlabel transrowblue">
                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="plainlabel transrowblue">
                                    <asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td class="details">
                                    <asp:Label ID="lbleqloctypea" Text='<%# DataBinder.Eval(Container.DataItem,"loctype")%>'
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <input type="hidden" id="lblrd" runat="server">
			<input type="hidden" id="lbleqid" runat="server"> <input type="hidden" id="lblchk" runat="server">
			<input type="hidden" id="lbldchk" runat="server"> <input type="hidden" id="lblsid" runat="server">
			<input type="hidden" id="lblcid" runat="server"> <input type="hidden" id="lbldid" runat="server">
			<input type="hidden" id="lblclid" runat="server"><input type="hidden" id="lbluser" runat="server">
			<input type="hidden" id="lbladdchk" runat="server"><input type="hidden" id="appchk" runat="server" NAME="appchk">
			<input type="hidden" id="lbllid" runat="server"><input type="hidden" id="lbltyp" runat="server">
			<input type="hidden" id="lbllog" runat="server"><input type="hidden" id="lblcurreqsort" runat="server" NAME="lblcurreqsort">
            <input type="hidden" id="lblpar" runat="server" />
		<input type="hidden" id="lblloc" runat="server" />
        <input type="hidden" id="lbldb" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
    </div>
    </form>
</body>
</html>
