﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123ecr.aspx.vb" Inherits="lucy_r12.pmo123ecr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="../scripts/pmo123ecradm1.js"></script>
	<script language="JavaScript" type="text/javascript" src="../scripts1/ECRCalcaspx.js"></script>
    <script language="JavaScript" type="text/javascript">
        function handleecr(ecr) {
            //window.parent.handleecr(ecr);
            //window.close();
        }
        function setref() {
            //window.parent.handleexit();
        }
        function updateecr(ecr) {
            //window.parent.updateecr(ecr);
        }
        function CalcTab(id) {
            var cd = document.getElementById("lblcid").innerHTML;
            //var idlbl = "f" + id
            //var lbl = document.getElementById(idlbl).innerHTML;
            document.getElementById("lblcatid").value = id;
            closehref();

            var lbl = document.getElementById(id).innerHTML;
            fio = lbl.indexOf(">")
            fstr = lbl.substring(0, fio + 1)
            fstr = lbl.replace(fstr, "")
            fstr = fstr.replace("</SPAN>", "")
            fstr = fstr.replace("</span>", "")
            var eqid = document.getElementById("lbleqid").value;
            var ro = document.getElementById("lblro").value;
            if (id != "change") {
                var lbl = document.getElementById(id).innerHTML;
                fio = lbl.indexOf(">")
                fstr = lbl.substring(0, fio + 1)
                fstr = lbl.replace(fstr, "")
                fstr = fstr.replace("</SPAN>", "")
                fstr = fstr.replace("</span>", "")
            }
            document.getElementById("ifwt").src = "pmo123levels.aspx?cid=" + cd + "&id=" + id + "&lbl=" + fstr + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date();
        }
    </script>
</head>
<body onload="startrow();">
    <form id="form1" runat="server">
    <div style="LEFT: 0px; POSITION: absolute; TOP: 0px">
    <table  cellSpacing="0" cellPadding="2"
				width="700">
				
			
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td width="100"><asp:label id="Label7" runat="server" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
										Font-Bold="True">Equipment#</asp:label></td>
								<td width="150"><asp:label id="lbleqnum" runat="server" ForeColor="Black" Font-Size="X-Small" Font-Names="Arial"
										Font-Bold="True"></asp:label></td>
								<td width="100"><asp:label id="Label8" runat="server" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
										Font-Bold="True">Description:</asp:label></td>
								<td width="350"><asp:label id="lbleqdesc" runat="server" ForeColor="Black" Font-Size="X-Small" Font-Names="Arial"
										Font-Bold="True"></asp:label></td>
							</tr>
							<tr>
								<td colspan="4">
									<hr style="BORDER-RIGHT: #0000ff 2px solid; BORDER-TOP: #0000ff 2px solid; BORDER-LEFT: #0000ff 2px solid; BORDER-BOTTOM: #0000ff 2px solid">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td vAlign="top">
									<table width="350">
										<tr>
											<td class="hdr1" colSpan="2"><asp:Label id="lang2110" runat="server">Criteria Impacted by the Equipment</asp:Label></td>
										</tr>
										<tr>
											<td><IMG id="row1" onclick="controlrow();" height="20" alt="" src="../images/appbuttons/optdown.gif"
													width="20"></td>
											<td class="hdr2"><asp:Label id="lang2111" runat="server">Environmental, Health &amp; Safety</asp:Label></td>
										</tr>
										<tr id="r1">
											<td></td>
											<td class="hdr3"><A class="ecrlinksel" id="11A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('11A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11A')" href="javascript:void(0)"><asp:Label id="lang2112" runat="server">Health and Safety Impact</asp:Label></A></td>
										</tr>
										<tr id="r1a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="11B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('11B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11B')" href="javascript:void(0)"><asp:Label id="lang2113" runat="server">Environmental Impact</asp:Label></A></td>
										</tr>
										<tr id="r1b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="11C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('11C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11C')" href="javascript:void(0)"><asp:Label id="lang2114" runat="server">Quality Impact on the Customer</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row2" onclick="controlrow2();" height="20" alt="" src="../images/appbuttons/optup.gif"
													width="20"></td>
											<td class="hdr2"><asp:Label id="lang2115" runat="server">Business Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r2">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('12A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12A')" href="javascript:void(0)"><asp:Label id="lang2116" runat="server">Compliance Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r2a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('12B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12B')" href="javascript:void(0)"><asp:Label id="lang2117" runat="server">Facility/Utility Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r2b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('12C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12C')" href="javascript:void(0)"><asp:Label id="lang2118" runat="server">Personnel Impact - Number of People Impacted</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row3" onclick="controlrow3();" height="20" alt="" src="../images/appbuttons/optup.gif"
													width="20"></td>
											<td class="hdr2"><asp:Label id="lang2119" runat="server">Production Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r3">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('13A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13A')" href="javascript:void(0)"><asp:Label id="lang2120" runat="server">Product/Value Stream Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('13B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13B')" href="javascript:void(0)"><asp:Label id="lang2121" runat="server">Constraint/Bottleneck � Demand vs. Capacity</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('13C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13C')" href="javascript:void(0)"><asp:Label id="lang2122" runat="server">Redundant Resource Availability</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13D" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('13D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13D')" href="javascript:void(0)"><asp:Label id="lang2123" runat="server">Controlled Environment Impact for Production</asp:Label></A></td>
										</tr>
										<tr>
											<td class="hdr1" colSpan="2"><asp:Label id="lang2124" runat="server">Criteria that Impacts the Equipment</asp:Label></td>
										</tr>
										<tr>
											<td><IMG id="row4" onclick="controlrow4();" height="20" alt="" src="../images/appbuttons/optup.gif"
													width="20"></td>
											<td class="hdr2"><asp:Label id="lang2125" runat="server">Equipment Condition</asp:Label></td>
										</tr>
										<tr class="details" id="r4">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('21A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21A')" href="javascript:void(0)"><asp:Label id="lang2126" runat="server">Current condition</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('21B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21B')" href="javascript:void(0)"><asp:Label id="lang2127" runat="server">Age</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('21C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21C')" href="javascript:void(0)"><asp:Label id="lang2128" runat="server">Robustness</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21D" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('21D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21D')" href="javascript:void(0)"><asp:Label id="lang2129" runat="server">Equipment Quality</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row5" onclick="controlrow5();" height="20" alt="" src="../images/appbuttons/optup.gif"
													width="20"></td>
											<td class="hdr2"><asp:Label id="lang2130" runat="server">Operational Context</asp:Label></td>
										</tr>
										<tr class="details" id="r5">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('22A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22A')" href="javascript:void(0)"><asp:Label id="lang2131" runat="server">Demand vs. capability</asp:Label></A></td>
										</tr>
										<tr class="details" id="r5a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('22B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22B')" href="javascript:void(0)"><asp:Label id="lang2132" runat="server">Operating Environment</asp:Label></A></td>
										</tr>
										<tr class="details" id="r5b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('22C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22C')" href="javascript:void(0)"><asp:Label id="lang2133" runat="server">Equipment Availability for Maintenance Access vs. Planned Work Required</asp:Label></A>
											</td>
										</tr>
										<tr>
											<td><IMG id="row6" onclick="controlrow6();" height="20" alt="" src="../images/appbuttons/optup.gif"
													width="20"></td>
											<td class="hdr2"><asp:Label id="lang2134" runat="server">Equipment Maintainability</asp:Label></td>
										</tr>
										<tr class="details" id="r6">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('23A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23A')" href="javascript:void(0)"><asp:Label id="lang2135" runat="server">Ease of Maintenance</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('23B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23B')" href="javascript:void(0)"><asp:Label id="lang2136" runat="server">Availability of Parts</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('23C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23C')" href="javascript:void(0)"><asp:Label id="lang2137" runat="server">Availability of Vendor Resources</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23D" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('23D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23D')" href="javascript:void(0)"><asp:Label id="lang2138" runat="server">Availability of Maintenance Skills to Service</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row7" onclick="controlrow7();" height="20" alt="" src="../images/appbuttons/optup.gif"
													width="20"></td>
											<td class="hdr2"><asp:Label id="lang2139" runat="server">Equipment Financial Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r7">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="24A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('24A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('24A')" href="javascript:void(0)"><asp:Label id="lang2140" runat="server">Replacement Cost</asp:Label></A></td>
										</tr>
										<tr class="details" id="r7a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="24B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('24B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('24B')" href="javascript:void(0)"><asp:Label id="lang2141" runat="server">Replaceability</asp:Label></A></td>
										</tr>
									</table>
								</td>
								<td vAlign="top">
									<table width="350">
										<TBODY>
											<tr>
												<td vAlign="top" align="left"><iframe id="ifwt" src="pmo123levels.aspx" frameborder="0" width="350" scrolling="no" height="400"
														runat="server"></iframe>
												</td>
											</tr>
										</TBODY>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblcatid" type="hidden" runat="server" NAME="lblcatid"> <input type="hidden" id="lblro" runat="server">
            <input type="hidden" id="lblfslang" runat="server" />
    </div>
    </form>
</body>
</html>
