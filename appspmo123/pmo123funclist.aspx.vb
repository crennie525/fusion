﻿Imports System.Data.SqlClient
Imports System.Text
Public Class pmo123funclist
    Inherits System.Web.UI.Page
    Dim fun As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid, ustr As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fun.Open()
            eqid = Request.QueryString("eqid").ToString
            getfunc(eqid)
            fun.Dispose()
        End If
    End Sub
    Private Sub getfunc(ByVal eqid As String)

        sql = "select func_id, func, spl from functions where eqid = '" & eqid & "'"
        Dim sb As New StringBuilder
        sb.Append("<table>")
        Dim fid, func, spl As String
        dr = fun.GetRdrData(sql)
        While dr.Read
            fid = dr.Item("func_id").ToString
            func = dr.Item("func").ToString
            spl = dr.Item("spl").ToString
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a href=""#"" onclick=""getfunc('" & fid & "');"">" & func & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & spl & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divfu.InnerHtml = sb.ToString


    End Sub
End Class