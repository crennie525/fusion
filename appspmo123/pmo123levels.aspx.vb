﻿Imports System.Data.SqlClient
Public Class pmo123levels
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim cid, ecid, sql, lbl, eqid, lev, ro As String
    Dim levels As New Utilities
    Dim dr As SqlDataReader
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSLangs()

        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btnsave.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                btnsave.Enabled = False
            End If
            cid = Request.QueryString("cid").ToString
            ecid = Request.QueryString("id").ToString
            lbl = Request.QueryString("lbl").ToString
            If lbl <> "change" Then
                eqid = Request.QueryString("eqid").ToString
                lblcid.Value = cid
                lblecrid.Value = ecid
                lbleqid.Value = eqid
                lblcat.Text = lbl
                levels.Open()
                CheckNew(eqid, cid)
                GetLev(cid, ecid, eqid)
                GetECR(eqid)
                levels.Dispose()
                lblchk.Value = "0"
            End If

        End If
        ddecm.Attributes.Add("onchange", "checkecm();")
        'btnsave.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'btnsave.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
        'Image1.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'Image1.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub GetEq()
        eqid = lbleqid.Value

    End Sub
    Private Sub CheckNew(ByVal eqid As String, ByVal cid As String)
        Dim ecrcnt As Integer
        sql = " select count(*) from ecrcatdata where eqid = '" & eqid & "'"
        ecrcnt = levels.Scalar(sql)
        If ecrcnt = 0 Then
            'insert a new ecr record
            sql = "insert into ecrcatdata " _
            + "(compid, eqid, ecrcatid, weight, ecrlevel) " _
            + "select '" & cid & "', '" & eqid & "', ecrcatid, weight, '3' from ecrcatmaster " _
            + "where compid = '" & cid & "' order by ecrcatid"
            levels.Update(sql)
        Else
            'update weights
            sql = "update ecrcatdata " _
            + "set weight = m.weight " _
            + "from ecrcatmaster m, ecrcatdata d " _
            + "where d.compid = d.compid and " _
            + "d.ecrcatid = m.ecrcatid and " _
            + "d.eqid = '" & eqid & "'"
            levels.Update(sql)
        End If
    End Sub
    Private Sub GetECR(ByVal eqid As String)
        sql = "select ecr from equipment where eqid = '" & eqid & "'"
        dr = levels.GetRdrData(sql)
        If dr.Read Then
            tc.Text = dr.Item("ecr").ToString
        Else
            tc.Text = "0"
        End If
        dr.Close()
        CheckECM()

    End Sub
    Private Sub CheckECM()
        Dim ecr As Decimal
        Try
            ecr = System.Convert.ToDecimal(tc.Text)
        Catch ex As Exception
            ecr = 0
        End Try

        If ecr > 8.9 Then
            tdrcm.InnerHtml = "Equipment with criticality ratings in this range are candidates " _
            + "for further analysis.<br> If export software was included with your installation of PM " _
            + "you can select your destination software from the list below."
            trdd.Attributes.Add("class", "view")
        Else
            tdrcm.InnerHtml = ""
            trdd.Attributes.Add("class", "details")
        End If

    End Sub
    Private Sub GetLev(ByVal cid As String, ByVal ecid As String, ByVal eqid As String)
        sql = "select * from ecrcatmaster where " _
                        + "ecrcatid = '" & ecid & "' and compid = '" & cid & "'"
        dr = levels.GetRdrData(sql)
        If dr.Read Then
            lblecrid.Value = dr.Item("ecrcatid").ToString
            t5.Text = dr.Item("level5").ToString
            t4.Text = dr.Item("level4").ToString
            t3.Text = dr.Item("level3").ToString
            t2.Text = dr.Item("level2").ToString
            t1.Text = dr.Item("level1").ToString
        End If
        dr.Close()
        'get level for initial ecr category
        sql = "select ecrlevel from ecrcatdata where " _
        + "eqid = '" & eqid & "' and compid = '" & cid & "' and ecrcatid = '" & ecid & "'"
        dr = levels.GetRdrData(sql)
        If dr.Read Then
            lev = dr.Item(0).ToString
        End If
        dr.Close()
        If lev = "1" Then
            rb1.Checked = True
        ElseIf lev = "2" Then
            rb2.Checked = True
        ElseIf lev = "3" Then
            rb3.Checked = True
        ElseIf lev = "4" Then
            rb4.Checked = True
        ElseIf lev = "5" Then
            rb5.Checked = True
        End If
        lbllev.Value = lev
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        levels.Open()
        lev = lbllev.Value
        eqid = lbleqid.Value
        ecid = lblecrid.Value
        cid = lblcid.Value
        sql = "update ecrcatdata set " _
              + "ecrlevel = '" & lev & "' where eqid = '" & eqid & "' and ecrcatid = '" & ecid & "'" ' and compid = '" & cid & "'"
        levels.Update(sql)
        DoCalc(eqid, cid)
        levels.Dispose()
    End Sub
    Private Sub DoCalc(ByVal eqid As String, ByVal cid As String)
        ecid = lblecrid.Value
        Dim t, tc As Decimal
        Dim e, ec As Integer
        sql = "select sum(ecrlevel * (weight * weight * weight)) / sum(5 * (weight * weight * weight)) * 10 " _
        + "from ecrcatdata where eqid = '" & eqid & "' and compid = '" & cid & "' " _
        + "and (ecrcatid = '11A' or ecrcatid = '11B')"
        'sql = "select sum(ecrlevel) as ecrlevel, sum(weight * weight * weight) as wc " _
        '+ "from ecrcatdata where eqid = '" & eqid & "' and compid = '" & cid & "' " _
        '+ "and (ecrcatid = '11A' or ecrcatid = '11B')"
        dr = levels.GetRdrData(sql)
        If dr.Read Then
            tc = CType(dr.Item(0).ToString, Decimal)
            'ec = CType(dr.Item("ecrlevel").ToString, Integer)
        End If
        dr.Close()
        'sql = "select sum(ecrlevel) as ecrlevel, sum(square(weight)) as ws " _
        '+ "from ecrcatdata where eqid = '" & eqid & "' and compid = '" & cid & "' " _
        '+ "and (ecrcatid <> '11A' and ecrcatid <> '11B')"
        sql = "select sum(ecrlevel * square(weight)) / sum(5 * square(weight)) * 10 " _
        + "from ecrcatdata where eqid = '" & eqid & "' and compid = '" & cid & "' " _
        + "and (ecrcatid <> '11A' and ecrcatid <> '11B')"
        'sql = "select " _
        '+ "(((sum(weight) * 5)-((sum(weight) * 5)-sum(weight * ecrlevel)))/(sum(weight) * 5)*10) " _
        '+ "from ecrcatdata where eqid = '" & eqid & "' and compid = '" & cid & "'"
        dr = levels.GetRdrData(sql)
        If dr.Read Then
            t = CType(dr.Item(0).ToString, Decimal)
            'e = CType(dr.Item("ecrlevel").ToString, Integer)
        End If
        dr.Close()
        t = (t + tc) / 2
        'e = e + ec
        'sql = "select sum(" & e & " * " & t & ") / sum(5 * " & t & ") * 10 as ecr"
        'dr = levels.GetRdrData(sql)
        'If dr.Read Then
        't = CType(dr.Item("ecr").ToString, Decimal)
        'End If
        'dr.Close()
        sql = "update equipment set " _
                + "ecr = '" & t & "' where eqid = '" & eqid & "'"
        levels.Update(sql)
        GetECR(eqid)
    End Sub
    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label1.Text = axlabs.GetASPXPage("levels.aspx", "Label1")
        Catch ex As Exception
        End Try
        Try
            Label2.Text = axlabs.GetASPXPage("levels.aspx", "Label2")
        Catch ex As Exception
        End Try
        Try
            Label3.Text = axlabs.GetASPXPage("levels.aspx", "Label3")
        Catch ex As Exception
        End Try
        Try
            Label4.Text = axlabs.GetASPXPage("levels.aspx", "Label4")
        Catch ex As Exception
        End Try
        Try
            Label5.Text = axlabs.GetASPXPage("levels.aspx", "Label5")
        Catch ex As Exception
        End Try
        Try
            Label6.Text = axlabs.GetASPXPage("levels.aspx", "Label6")
        Catch ex As Exception
        End Try
        Try
            lang2407.Text = axlabs.GetASPXPage("levels.aspx", "lang2407")
        Catch ex As Exception
        End Try
        Try
            lang2408.Text = axlabs.GetASPXPage("levels.aspx", "lang2408")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnsave.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                btnsave.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                btnsave.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                btnsave.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                btnsave.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try
        

    End Sub
End Class