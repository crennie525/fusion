﻿Public Class pmo123e1
    Inherits System.Web.UI.Page
    Dim sid, retval As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            retval = Request.QueryString("retval").ToString
            lblretval.Value = retval.Replace("~", "&")
            Dim db As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            lbldb.Value = db
        End If
    End Sub

End Class