﻿Imports System.Data.SqlClient
Imports System.IO
Public Class pmo123neweq
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim eq, chk, dchk, cid, sid, did, clid, sql, filt, dt, df, val, msg, eqid, lid, typ, chrg, ro, appstr, db As String
    Dim dr As SqlDataReader
    Dim echk As Integer
    Dim newstr, strto, saveas As String
    Dim eqr As New Utilities
    Dim Login, usr As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim eoem As New mmenu_utils_a
        Dim isoem As String = eoem.OEM
        If isoem = "1" Then
            ddoem.Visible = False
            txtoem.Visible = True
        End If
        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            usr = HttpContext.Current.Session("username").ToString
            lbluser.Value = usr
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try



        If Not IsPostBack Then
            ibtnreturn.Attributes.Add("onclick", "handleret2();")
            Dim start As String = Request.QueryString("start").ToString
            lblstart.Value = start
            If start = "yes" Then
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                db = Request.QueryString("db").ToString
                lbldb.Value = db

                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                dchk = Request.QueryString("dchk").ToString
                lbldchk.Value = dchk

                eqr.Open()
                PopAC()
                PopOEM()
                PopStatus()
                CheckStat(cid)
                PopPage()
                eqr.Dispose()
            End If
        Else
            If Request.Form("txtecr") = "return" Then
                txtecr.Text = ""
                SaveEq()
                PopPage()
            End If
            If Request.Form("lblpchk") = "ac" Then
                lblpchk.Value = ""
                eqr.Open()
                SaveEq()
                PopPage()
                eqr.Dispose()
            ElseIf Request.Form("lblpchk") = "delimg" Then
                lblpchk.Value = ""
                eqr.Open()
                DelImg()
                LoadPics()
                eqr.Dispose()
                'lblchksav.Value = "yes"
            ElseIf Request.Form("lblpchk") = "checkpic" Then
                lblpchk.Value = ""
                eqr.Open()
                LoadPics()
                eqr.Dispose()
            ElseIf Request.Form("lblpchk") = "savedets" Then
                lblpchk.Value = ""
                eqr.Open()
                SaveDets()
                LoadPics()
                eqr.Dispose()
            End If
        End If

    End Sub
    Private Sub PopOEM()
        'eqr.Open()
        sql = "select * from invcompanies where type = 'M' order by name"
        dr = eqr.GetRdrData(sql)
        ddoem.DataSource = dr
        ddoem.DataTextField = "name"
        ddoem.DataValueField = "name"
        ddoem.DataBind()
        dr.Close()
        ddoem.Items.Insert(0, "Select OEM")
    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr996", "pmlibneweqdets.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim old As String = oldiord 'lbloldorder.Value
            Dim neword As String = txtiorder.Text
            eqid = lbleqid.Value
            typ = "eq"
            sql = "usp_reordereqimg '" & typ & "','" & eqid & "','" & neword & "','" & old & "'"
            eqr.Update(sql)
        End If
    End Sub
    Private Sub DelImg()
        eqid = lbleqid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        typ = "eq"
        sql = "usp_deleqimg '" & typ & "','" & eqid & "','" & pid & "','" & old & "'"
        eqr.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'ile.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
        lblcurrp.Value = "0"
    End Sub

    Private Sub CheckStat(ByVal cid As String)
        Dim cnt As Integer
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        
        sql = "select ecrflg from [" + srvr + "].[" + mdb + "].[dbo].[pmsysvars] where compid = '" & cid & "'"
        cnt = eqr.Scalar(sql)
        If cnt = 0 Then
            btnecr.Src = "../images/appbuttons/minibuttons/cant.gif"
            btnecr.Attributes.Add("title", "ECR levels and weights have not been defined")
            lblecrflg.Value = "noecr"
        End If
    End Sub
    Private Sub PopStatus()
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
       
        sql = "select * from [" + srvr + "].[" + mdb + "].[dbo].[eqstatus] where compid = '" & cid & "'"
        dr = eqr.GetRdrData(sql)
        ddlstat.DataSource = dr
        ddlstat.DataTextField = "status"
        ddlstat.DataValueField = "statid"
        ddlstat.DataBind()
        dr.Close()
        ddlstat.Items.Insert(0, "Select Status")
    End Sub

    Public Function GetEqRecord(ByVal eq As String) As SqlDataReader
        eqid = lbleqid.Value
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
       
        sql = "select e.*, " _
        + "cph = (select s.phonenum from  [" + srvr + "].[" + mdb + "].[dbo].[pmsysusers] s  where  s.username = e.createdby), " _
        + "cm = (select s.email from  [" + srvr + "].[" + mdb + "].[dbo].[pmsysusers] s  where  s.username = e.createdby), " _
        + "eph = (select s.phonenum from  [" + srvr + "].[" + mdb + "].[dbo].[pmsysusers] s  where  s.username = e.modifiedby), " _
        + "em = (select s.email from  [" + srvr + "].[" + mdb + "].[dbo].[pmsysusers] s  where  s.username = e.modifiedby) " _
        + "from  [" & srvr & "].[" & mdb & "].[dbo].[equipment] e where e.eqid = '" & eqid & "'"
        dr = eqr.GetRdrData(sql)
        Return dr
    End Function


    Private Sub PopPage()
        PopAC()
        'eqr.Open()
        'eq = ddeq.SelectedValue
        eq = lbleqid.Value '= eq
        lblpar.Value = "eq"
        'GetEqDesc(eq)
        'If dr.Read Then
        'lbleqdesc.Text = dr.Item("eqdesc").ToString
        'End If
        'dr.Close()
        Dim stati As String
        dr = GetEqRecord(eq)
        Dim ac As String

        '*** Multi Add ***
        Dim mdb, trans, transdate, transstatus, transstatusdate, language As String
        '*** Multi Add End ***

        If dr.Read Then
            txteqname.Text = dr.Item("eqnum").ToString
            lbleqnum.Value = dr.Item("eqnum").ToString
            txteqdesc.Text = dr.Item("eqdesc").ToString
            txtspl.Text = dr.Item("spl").ToString
            txtloc.Text = dr.Item("location").ToString
            Try
                ddoem.SelectedValue = dr.Item("oem").ToString
            Catch ex As Exception

            End Try
            txtmod.Text = dr.Item("model").ToString
            txtser.Text = dr.Item("serial").ToString
            lblpic.Value = dr.Item("picurl").ToString
            'lblcnt.Text = dr.Item("piccnt").ToString
            txtecr.Text = dr.Item("ecr").ToString
            tdcby.InnerHtml = dr.Item("createdby").ToString
            tdcdate.InnerHtml = dr.Item("createdate").ToString
            tdmby.InnerHtml = dr.Item("modifiedby").ToString
            tdmdate.InnerHtml = dr.Item("modifieddate").ToString
            txtcharge.Text = dr.Item("chargenum").ToString
            Try
                ac = dr.Item("acid").ToString
                ddac.SelectedValue = dr.Item("acid").ToString
            Catch ex As Exception
                ddac.SelectedIndex = 0
            End Try

            lblpareqid.Value = dr.Item("origpareqid").ToString
            Try
                stati = dr.Item("statid").ToString
                ddlstat.SelectedValue = stati
            Catch ex As Exception

            End Try
            lbllock.Value = dr.Item("locked").ToString
            lbllockedby.Value = dr.Item("lockedby").ToString

            '*** Multi Add ***
            mdb = dr.Item("mdb").ToString
            'lbldb.Value = dr.Item("mdbname").ToString
            trans = dr.Item("trans").ToString
            transdate = dr.Item("transdate").ToString
            transstatus = dr.Item("transstatus").ToString
            transstatusdate = dr.Item("transstatusdate").ToString
            language = dr.Item("mdblang").ToString
            '*** Multi Add End ***
        End If
        dr.Close()

        '*** Multi Add ***
        Try
            If mdb = "1" Then
                If trans = "0" Then
                    'cbtrans.Visible = True
                Else
                    'cbtrans.Visible = False
                    tdtrans.InnerHtml = ""
                End If
            Else
                'cbtrans.Visible = False
                tdtrans.InnerHtml = ""
            End If
        Catch ex As Exception

        End Try

        '*** Multi Trans End ***

        Dim user As String = lbluser.Value
        Dim lockby As String = lbllockedby.Value
        Dim lock As String = lbllock.Value
        ro = lblro.Value

        LoadPics()

    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgeq.Attributes.Add("src", "../images/appimages/eqimg1.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/eqimg1.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/eqimg1.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/eqimg1.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/eqimg1.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/eqimg1.gif")
        End If

        Dim img, imgs, picid As String

        eqid = lbleqid.Value
        Dim pcnt As Integer
        sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        pcnt = eqr.Scalar(sql)
        lblpcnt.Value = pcnt

        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from pmpictures p where p.funcid is null and p.comid is null and p.eqid = '" & eqid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = eqr.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    imgeq.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub
    Private Sub PopAC()
        cid = lblcid.Value
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        
        sql = "select * from [" + srvr + "].[" + mdb + "].[dbo].[pmAssetClass]" ' where compid = '" & cid & "'"
        dr = eqr.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        ddac.DataBind()
        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Public Function chkcnt(ByVal eqid As String, ByVal eq As String) As Integer
        chk = lblchk.Value
        dchk = lbldchk.Value
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        
        If dchk = "yes" Then
            clid = lblclid.Value
            If chk = "yes" And clid <> "no" Then
                clid = lblclid.Value
                filt = "where eqnum = '" & eq & "'" ' and cellid = '" & clid & "'"
            ElseIf chk = "no" Then
                did = lbldept.Value
                filt = "where eqnum = '" & eq & "'" ' and dept_id = '" & did & "'"
            End If
        Else
            sid = lblsid.Value
            filt = "where eqnum = '" & eq & "'" ' and siteid = '" & sid & "'"
        End If

        sql = "select count(*) from equipment " & filt
        echk = eqr.Scalar(sql)

        Return echk
    End Function
    Private Sub SaveEq()
        Dim eqid, eq, eqd, loc, oem, model, ser, spl, ecr, ac, acid, lid, chrg, oeq As String
        eq = txteqname.Text
        oeq = lbleqnum.Value
        If Len(eq) <> 0 Then
            eq = eqr.ModString3(eq)
            ecr = txtecr.Text
            Try
                If Len(ecr = 0) Then
                    ecr = "0"
                End If
            Catch ex As Exception
                ecr = "0"
            End Try
            
            Dim eqechk As Long
            Try
                eqechk = System.Convert.ToDecimal(ecr)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr896", "EQCopyMini.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            'eqid = ddeq.SelectedValue
            eqid = lbleqid.Value
            If oeq <> eq Then
                echk = chkcnt(eqid, eq)
            Else
                echk = "0"
            End If

            eqd = txteqdesc.Text
            eqd = Replace(eqd, "'", Chr(180), , , vbTextCompare)
            eqd = Replace(eqd, "--", "-", , , vbTextCompare)
            eqd = Replace(eqd, ";", ":", , , vbTextCompare)
            chrg = txtcharge.Text
            chrg = Replace(chrg, "'", Chr(180), , , vbTextCompare)
            chrg = Replace(chrg, "--", "-", , , vbTextCompare)
            chrg = Replace(chrg, ";", ":", , , vbTextCompare)
            loc = txtloc.Text
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
            loc = Replace(loc, "--", "-", , , vbTextCompare)
            loc = Replace(loc, ";", ":", , , vbTextCompare)
            oem = ddoem.SelectedValue.ToString
            If oem <> "0" And oem <> "Select OEM" Then
                oem = eqr.ModString1(oem)
            Else
                oem = txtoem.Text
                oem = eqr.ModString1(oem)
                'oem = ""
            End If
            oem = Replace(oem, "'", Chr(180), , , vbTextCompare)
            oem = Replace(oem, "--", "-", , , vbTextCompare)
            oem = Replace(oem, ";", ":", , , vbTextCompare)
            model = txtmod.Text
            model = Replace(model, "'", Chr(180), , , vbTextCompare)
            model = Replace(model, "--", "-", , , vbTextCompare)
            model = Replace(model, ";", ":", , , vbTextCompare)
            ser = txtser.Text
            ser = Replace(ser, "'", Chr(180), , , vbTextCompare)
            ser = Replace(ser, "--", "-", , , vbTextCompare)
            ser = Replace(ser, ";", ":", , , vbTextCompare)
            spl = txtspl.Text
            spl = Replace(spl, "'", Chr(180), , , vbTextCompare)
            spl = Replace(spl, "--", "-", , , vbTextCompare)
            spl = Replace(spl, ";", ":", , , vbTextCompare)
            eqid = lbleqid.Value
            If ddac.SelectedIndex <> 0 Then
                ac = ddac.SelectedItem.ToString
                acid = ddac.SelectedValue.ToString
                ac = Replace(ac, "'", Chr(180), , , vbTextCompare)
            Else
                ac = "Select"
                acid = "0"
            End If
            '*** Multi Add ***
            Dim t As Integer
            'If cbtrans.Visible = True Then
            'If cbtrans.Checked = True Then
            't = 1
            'Else
            t = 0
            'End If
            'End If
            '*** Multi Add End ***
            If ddlstat.SelectedIndex <> 0 Then
                Dim stati As String = ddlstat.SelectedValue.ToString
                Dim stat As String = ddlstat.SelectedItem.ToString
                stat = Replace(stat, "'", Chr(180), , , vbTextCompare)

                sql = "update equipment set " _
                + "eqnum = '" & eq & "', " _
                + "eqdesc = '" & eqd & "', " _
                + "location = '" & loc & "', " _
                + "oem = '" & oem & "', " _
                + "model = '" & model & "', " _
                + "serial = '" & ser & "', " _
                + "spl = '" & spl & "', " _
                + "ecr = '" & ecr & "', " _
                + "statid = '" & stati & "', " _
                + "status = '" & stat & "', " _
                + "acid = '" & acid & "', " _
                + "assetclass = '" & ac & "', " _
                + "chargenum = '" & chrg & "', " _
                + "trans = '" & t & "' " _
                + "where eqid = '" & eqid & "' " _
                + "update equipment  set eqstatindex = (" _
                + "select eqstatindex from eqstatus where " _
                + "statid = '" & stati & "') where eqid = '" & eqid & "'"
            Else
                sql = "update equipment set " _
               + "eqnum = '" & eq & "', " _
               + "eqdesc = '" & eqd & "', " _
               + "location = '" & loc & "', " _
               + "oem = '" & oem & "', " _
               + "model = '" & model & "', " _
               + "serial = '" & ser & "', " _
               + "spl = '" & spl & "', " _
               + "ecr = '" & ecr & "', " _
               + "acid = '" & acid & "', " _
                + "assetclass = '" & ac & "', " _
                + "chargenum = '" & chrg & "', " _
                + "trans = '" & t & "' " _
               + "where eqid = '" & eqid & "'"
            End If
            'Try
            If echk = "0" Then
                eqr.Update(sql)
            End If
            'Catch ex As Exception
            'Dim strMessage As String =  tmod.getmsg("cdstr997" , "pmlibneweqdets.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'End Try
            If ddac.SelectedIndex <> 0 Then
                sql = "update equipment  set acindex = (" _
                + "select acindex from pmAssetClass where " _
                + "acid = '" & acid & "') where eqid = '" & eqid & "'"
                eqr.Update(sql)
            End If
            'If cbtrans.Checked = True Then
            'ToTrans()
            'End If
        Else
            Dim strMessage As String = tmod.getmsg("cdstr998", "pmlibneweqdets.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function

    Protected Sub ibtnsaveneweq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsaveneweq.Click
        eqr.Open()
        SaveEq()
        Try
            eqr.Dispose()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgdel.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibneweqdets.aspx", "imgdel") & "', ABOVE, LEFT)")
            imgdel.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibneweqdets.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'ovid261.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibneweqdets.aspx", "ovid261") & "', ABOVE, LEFT)")
            'ovid261.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub
    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            'lang2466.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2466")
        Catch ex As Exception
        End Try
        Try
            lang2467.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2467")
        Catch ex As Exception
        End Try
        Try
            lang2468.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2468")
        Catch ex As Exception
        End Try
        Try
            lang2469.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2469")
        Catch ex As Exception
        End Try
        Try
            lang2470.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2470")
        Catch ex As Exception
        End Try
        Try
            lang2471.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2471")
        Catch ex As Exception
        End Try
        Try
            lang2472.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2472")
        Catch ex As Exception
        End Try
        Try
            lang2473.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2473")
        Catch ex As Exception
        End Try
        Try
            lang2474.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2474")
        Catch ex As Exception
        End Try
        Try
            lang2475.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2475")
        Catch ex As Exception
        End Try
        Try
            lang2476.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2476")
        Catch ex As Exception
        End Try
        Try
            lang2477.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2477")
        Catch ex As Exception
        End Try
        Try
            lang2478.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2478")
        Catch ex As Exception
        End Try
        Try
            lang2479.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2479")
        Catch ex As Exception
        End Try
        Try
            lang2480.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2480")
        Catch ex As Exception
        End Try
        Try
            lang2481.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2481")
        Catch ex As Exception
        End Try
        Try
            lang2482.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2482")
        Catch ex As Exception
        End Try
        Try
            lang2483.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2483")
        Catch ex As Exception
        End Try
        Try
            lang2484.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2484")
        Catch ex As Exception
        End Try
        Try
            'lang2485.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lang2485")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("pmlibneweqdets.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub

    Protected Sub ibtnreturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnreturn.Click

    End Sub
End Class