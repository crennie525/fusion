﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123newfu.aspx.vb" Inherits="lucy_r12.pmo123newfu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmlibnewfudetsaspx_3.js"></script>
    <script language="JavaScript" type="text/javascript">
        function getcompedit(fuid, eqid) {

            try {
                var eReturn = window.showModalDialog("../equip/funceditdialog.aspx?fuid=" + fuid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:400px; resizable=yes");
                if (eReturn) {
                    if (eReturn == "log") {
                        window.parent.doref();
                    }
                    else {
                        document.getElementById("lblpchk").value = "func";
                        document.getElementById("form1").submit()
                    }
                }
                else {
                    document.getElementById("lblpchk").value = "func";
                    document.getElementById("form1").submit()
                }
            }
            catch (err) {

            }
        }
        function GetFuncDiv() {
            window.parent.setref();
            cid = document.getElementById("lblcid").value;
            eqid = document.getElementById("lbleqid").value;
            db = document.getElementById("lbldb").value;
            oloc = document.getElementById("lbloloc").value;
            ro = "0"; //document.getElementById("lblro").value;
            var eReturn = window.showModalDialog("../equip/AddEditFuncDialog.aspx?cid=" + cid + "&eqid=" + eqid + "&date=" + Date() + "&ro=" + ro + "&db=" + db + "&oloc=" + oloc, "", "dialogHeight:600px; dialogWidth:750px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblpchk").value = "func";
                document.getElementById("form1").submit();
            }
        }
        function addpic() {
            var fuid = document.getElementById("lblfuid").value;
            var ro = "0"; //document.getElementById("lblro").value;
            if (fuid != "") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../equip/equploadimagedialog.aspx?typ=fu&eqid=&comid=&funcid=" + fuid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
                if (eReturn == "ok") {
                    document.getElementById("lblpchk").value = "checkpic";
                    document.getElementById("form1").submit()
                }
            }
            else {
                alert("No Function Selected")
            }

        }
        function GetGrid() {
            var eqid = document.getElementById("lbleqid").value;
            try {
                var eReturn = window.showModalDialog("../equip/funcseqdialog.aspx?eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:700px; resizable=yes");
                if (eReturn) {
                    if (eReturn == "log") {
                        window.parent.doref();
                    }
                    else {
                        document.getElementById("lblpchk").value = "func";
                        document.getElementById("form1").submit()
                    }
                }
                else {
                    document.getElementById("lblpchk").value = "func";
                    document.getElementById("form1").submit()
                }
            }
            catch (err) {

            }
        }
    </script>
</head>
<body onload="GetsScroll();">
    <form id="form1" runat="server">
    <div>
        <table style="position: absolute; top: 0px; left: 0px" width="980">
            <tr>
                <td class="labelibl" colspan="2">
                    <asp:Label ID="lang2486" runat="server">Select a Function to view Function Image, Component, and Task Details</asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center" width="750">
                    <div id="spdiv" style="width: 500px; height: 260px; overflow: auto" onscroll="SetsDivPosition();">
                        <asp:Repeater ID="rptrfuncnew" runat="server">
                            <HeaderTemplate>
                                <table>
                                    <tr>
                                        <td class="btmmenu plainlabel" align="center">
                                            <img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16">
                                        </td>
                                        <td class="btmmenu plainlabel" width="50px">
                                            <asp:Label ID="lang2487" runat="server">Edit</asp:Label>
                                        </td>
                                        <td class="btmmenu plainlabel" width="250px">
                                            <asp:Label ID="lang2488" runat="server">Function</asp:Label>
                                        </td>
                                        <td class="btmmenu plainlabel" width="250px">
                                            <asp:Label ID="lang2489" runat="server">Special Identifier</asp:Label>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="selrow" runat="server" bgcolor='<%# HighlightRowFR(DataBinder.Eval(Container.DataItem, "func_id"))%>'>
                                    <td class="plainlabel">
                                        <asp:CheckBox ID="cb1" runat="server"></asp:CheckBox>
                                    </td>
                                    <td>
                                        <img src="../images/appbuttons/minibuttons/lilpentrans.gif" id="imgcoedit" runat="server">
                                    </td>
                                    <td class="plainlabel">
                                        <asp:LinkButton CommandName="Select" ID="Linkbutton2" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="Label9" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblfuncrevid" Text='<%# DataBinder.Eval(Container.DataItem,"func_id")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lblparfu1" Text='<%# DataBinder.Eval(Container.DataItem,"origparent")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
                <td valign="top" align="center" width="230">
                    <table>
                        <tr>
                            <td valign="top" align="center" colspan="2">
                                <a onclick="getbig();" href="#">
                                    <img id="imgfu" height="206" src="../images/appimages/funcimg.gif" width="216" border="0"
                                        runat="server"></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td class="bluelabel" width="80" id="tst" runat="server">
                                            <asp:Label ID="lang2490" runat="server">Order</asp:Label>
                                        </td>
                                        <td width="60">
                                            <asp:TextBox ID="txtiorder" runat="server" Width="40px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                        <td width="20">
                                            <img onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif">
                                        </td>
                                        <td width="20">
                                            <img onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)"
                                                onclick="addpic();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif">
                                        </td>
                                        <td width="20">
                                            <img id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
                                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/del.gif" runat="server">
                                        </td>
                                        <td width="20">
                                            <img id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)"
                                                onclick="savdets();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="ifirstf" onclick="getpfirstf();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="iprevf" onclick="getpprevf();" src="../images/appbuttons/minibuttons/lprev.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="140">
                                            <asp:Label ID="lblpgf" runat="server" CssClass="bluelabel">Image 0 of 0</asp:Label>
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="inextf" onclick="getpnextf();" src="../images/appbuttons/minibuttons/lnext.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" width="20">
                                            <img id="ilastf" onclick="getplastf();" src="../images/appbuttons/minibuttons/llast.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:ImageButton ID="ibtnremove" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif">
                    </asp:ImageButton>
                    <img onmouseover="return overlib('Add a New Function Record')" onclick="GetFuncDiv();"
                        onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                        width="20">
                    <img onmouseover="return overlib('Lookup Function Records to Copy')" onclick="GetFuncCopy();"
                        onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                        width="20">
                    <img id="ggrid" onmouseover="return overlib('Edit in Grid View')" onclick="GetGrid();"
                        onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/grid1.gif"
                        width="20" runat="server">
                </td>
            </tr>
        </table>
    </div>
    <input id="lblfuid" type="hidden" runat="server" NAME="lblfuid">
			<input id="lbleqid" type="hidden" runat="server" NAME="lbleqid"><input id="lbldb" type="hidden" runat="server" NAME="lbldb">
			<input id="lblpcntf" type="hidden" name="lblpcntf" runat="server"> <input id="lblcurrpf" type="hidden" name="lblcurrpf" runat="server">
			<input id="lblimgsf" type="hidden" name="lblimgsf" runat="server"> <input id="lblimgidf" type="hidden" name="lblimgidf" runat="server">
			<input id="lblovimgsf" type="hidden" name="lblovimgsf" runat="server"> <input id="lblovbimgsf" type="hidden" name="lblovbimgsf" runat="server">
			<input id="lblcurrimgf" type="hidden" name="lblcurrimgf" runat="server"> <input id="lblcurrbimgf" type="hidden" name="lblcurrbimgf" runat="server">
			<input id="lblbimgsf" type="hidden" name="lblbimgsf" runat="server"><input id="lbliordersf" type="hidden" name="lbliordersf" runat="server">
			<input id="lbloldorderf" type="hidden" name="lbloldorderf" runat="server"> <input id="lblovtimgsf" type="hidden" name="lblovtimgsf" runat="server">
			<input id="lblcurrtimgf" type="hidden" name="lblcurrtimgf" runat="server"> <input id="spdivy" type="hidden" runat="server" NAME="spdivy">
			<input type="hidden" id="lblchk" runat="server" NAME="lblchk"> <input type="hidden" id="lblpchk" runat="server">
			<input id="lblcurrp" type="hidden" runat="server" NAME="lblcurrp"> <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
			<input id="lblimgid" type="hidden" name="lblimgid" runat="server"> <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
			<input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server"> <input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server">
			<input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server"> <input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"> <input type="hidden" id="lblcid" runat="server">
			<input type="hidden" id="lblsid" runat="server"> <input type="hidden" id="lbldid" runat="server">
			<input type="hidden" id="lblclid" runat="server"><input type="hidden" id="lbloloc" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
