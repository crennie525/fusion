﻿Imports System.Data.SqlClient
Public Class pmo123equip
    Inherits System.Web.UI.Page
    Dim eqr As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sid, ustr As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            ustr = Request.QueryString("usrname").ToString
            lblsid.Value = sid
            lblustr.Value = ustr
            eqr.Open()
            PopOEM()
            PopStatus()
            PopAC()
            eqr.Dispose()
            'ifimge.Attributes.Add("src", "../equip/equploadimage.aspx?typ=eq&funcid=&comid=&eqid=&ro=0&date=" + Now)
        Else
            If Request.Form("lblsubmit") = "geteq" Then
                lblsubmit.Value = ""
                eqr.Open()
                geteq()
                eqr.Dispose()
                lblsubmit.Value = "ret"
            End If
        End If
    End Sub
    Private Sub geteq()
        Dim eqid As String = lbleqid.Value
        sql = "select * from equipment where eqid = '" & eqid & "'"
        dr = eqr.GetRdrData(sql)
        While dr.Read
            tdeqnum.InnerHtml = dr.Item("eqnum").ToString
            lbleqnum.Value = dr.Item("eqnum").ToString
            txteqdesc.Text = dr.Item("eqdesc").ToString
            txtspl.Text = dr.Item("spl").ToString
            txtloc.Text = dr.Item("location").ToString
            Try
                ddoem.SelectedValue = dr.Item("oem").ToString
            Catch ex As Exception

            End Try
            txtmod.Text = dr.Item("model").ToString
            txtser.Text = dr.Item("serial").ToString
            txtcharge.Text = dr.Item("chargenum").ToString
            Try
                ddac.SelectedValue = dr.Item("acid").ToString
            Catch ex As Exception
                ddac.SelectedIndex = 0
            End Try
            Try
                ddlstat.SelectedValue = dr.Item("statid").ToString
            Catch ex As Exception

            End Try
            txtecr.Text = dr.Item("ecr").ToString
        End While
        dr.Close()

    End Sub
    Private Sub PopAC()
        sql = "select * from pmAssetClass"
        dr = eqr.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        Try
            ddac.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Private Sub PopOEM()
        'eqr.Open()
        sql = "select * from invcompanies where type = 'M' order by name"
        dr = eqr.GetRdrData(sql)
        ddoem.DataSource = dr
        ddoem.DataTextField = "name"
        ddoem.DataValueField = "name"
        ddoem.DataBind()
        dr.Close()
        ddoem.Items.Insert(0, "Select OEM")
    End Sub
    Private Sub PopStatus()
        'eqr.Open()
        sql = "select * from eqstatus"
        dr = eqr.GetRdrData(sql)
        ddlstat.DataSource = dr
        ddlstat.DataTextField = "status"
        ddlstat.DataValueField = "statid"
        ddlstat.DataBind()
        dr.Close()
        ddlstat.Items.Insert(0, "Select Status")
    End Sub
End Class