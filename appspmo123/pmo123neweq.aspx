﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123neweq.aspx.vb" Inherits="lucy_r12.pmo123neweq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript" src="../scripts/equipment_1.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmlibneweqdetsaspx_c.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function addpic() {
            var eqid = document.getElementById("lbleqid").value;
            var ro = "0"; //document.getElementById("lblro").value;
            if (eqid != "") {
                window.parent.setref();
                var eReturn = window.showModalDialog("../equip/equploadimagedialog.aspx?typ=eq&funcid=&comid=&eqid=" + eqid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
                if (eReturn == "ok") {
                    document.getElementById("lblpchk").value = "checkpic";
                    document.getElementById("form1").submit()
                }
            }
            else {
                alert("No Equipment Selected")
            }

        }
        function getmeter() {
            eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("emeterdialog.aspx?eqid=" + eqid, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes")
            }
        }
        function checkeq() {
            var start = document.getElementById("lblstart").value;
            var eqid = document.getElementById("lbleqid").value;
            if (start == "yes") {
                window.parent.handleneweq(eqid);
            }

        }
        function handleret2() {
        //alert()
            window.parent.handleret2();
        }
    </script>
</head>
<body onload="checkeq();">
    <form id="form1" runat="server">
    <div class="FreezePaneOff" id="FreezePane" style="width: 720px" align="center">
        <div class="InnerFreezePane" id="InnerFreezePane">
        </div>
    </div>
    <div id="overDiv" style="z-index: 1000; position: absolute; visibility: hidden">
    </div>
    <div>
        <table id="eqdetdiv" style="position: absolute; top: 0px; left: 0px" cellspacing="0"
            cellpadding="0" width="980">
            <tr>
                <td width="102">
                </td>
                <td width="175">
                </td>
                <td width="102">
                </td>
                <td width="371">
                </td>
                <td width="150">
                </td>
                <td width="80">
                </td>
            </tr>
            <tr>
					<td colSpan="2">&nbsp;</td>
					<td align="right" class="bluelabel" colSpan="2" id="tdtrans" runat="server"></td>
					<td colSpan="2" align="right">
						<asp:imagebutton id="ibtnsaveneweq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="ibtnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"></asp:imagebutton></td>
					</TD>
				</tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang2467" runat="server">Equipment#</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txteqname" runat="server" Width="120px" MaxLength="50"></asp:TextBox><img
                        onclick="GetPartDiv();" src="../images/appbuttons/minibuttons/spareparts.gif"
                        class="details">
                    <img onclick="getmeter();" onmouseover="return overlib('Add\Edit Meters for this Asset', ABOVE, LEFT)"
                        onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif">
                </td>
                <td class="redlabel">
                    ECR:
                </td>
                <td>
                    <asp:TextBox ID="txtecr" runat="server" Width="48px" Font-Size="9pt"></asp:TextBox>&nbsp;<img
                        id="btnecr" title="Calculate ECR" onclick="getcalc();" height="20" alt="" src="../images/appbuttons/minibuttons/ecrbutton.gif"
                        width="26" align="absMiddle" runat="server">
                </td>
                <td align="center" colspan="2" rowspan="10">
                    <a onclick="getbig();" href="#">
                        <img id="imgeq" height="216" src="../images/appimages/eqimg1.gif" width="216" border="0"
                            runat="server"></a>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang2468" runat="server">Description</asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txteqdesc" runat="server" Width="357px" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label" style="height: 23px">
                    <asp:Label ID="lang2469" runat="server">Spl Identifier</asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtspl" runat="server" Width="357px" MaxLength="250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang2470" runat="server">Location</asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtloc" runat="server" Width="358px" MaxLength="50" Font-Size="9pt"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    OEM
                </td>
                <td>
                    <asp:DropDownList ID="ddoem" runat="server" Width="150px" CssClass="plainlabel">
                    </asp:DropDownList><asp:textbox id="txtoem" runat="server" Width="150px" MaxLength="50" CssClass="plainlabel"></asp:textbox>
                </td>
                <td class="label">
                    <asp:Label ID="lang2471" runat="server">Model#/Type</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtmod" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang2472" runat="server">Serial#</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtser" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
                </td>
                <td class="label">
                    <asp:Label ID="lang2473" runat="server">Status</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlstat" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang2474" runat="server">Asset Class</asp:Label>
                </td>
                <td align="left" colspan="3">
                    <asp:DropDownList ID="ddac" runat="server">
                        <asp:ListItem Value="Select Asset Class">Select Asset Class</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr height="20">
                <td class="bluelabel">
                    <asp:Label ID="lang2475" runat="server">Charge#</asp:Label>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtcharge" runat="server" Width="165px" ReadOnly="True"></asp:TextBox><img
                        onclick="getcharge('wo');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                        border="0">
                </td>
            </tr>
            <tr height="20">
                <td class="label" id="Td8" runat="server">
                    <asp:Label ID="lang2476" runat="server">Created By</asp:Label>
                </td>
                <td class="plainlabel" id="tdcby" runat="server">
                </td>
                <td class="label">
                    <asp:Label ID="lang2477" runat="server">Create Date</asp:Label>
                </td>
                <td class="plainlabel" id="tdcdate" runat="server">
                </td>
            </tr>
            <tr height="20">
                <td class="label" id="Td2" runat="server">
                    <asp:Label ID="lang2478" runat="server">Phone</asp:Label>
                </td>
                <td class="plainlabel" id="Td3" runat="server">
                </td>
                <td class="label">
                    <asp:Label ID="lang2479" runat="server">Email Address</asp:Label>
                </td>
                <td class="plainlabel" id="Td4" runat="server">
                </td>
            </tr>
            <tr height="20">
                <td class="label" id="Td1" runat="server">
                    <asp:Label ID="lang2480" runat="server">Modified By</asp:Label>
                </td>
                <td class="plainlabel" id="tdmby" runat="server">
                </td>
                <td class="label">
                    <asp:Label ID="lang2481" runat="server">Modified Date</asp:Label>
                </td>
                <td class="plainlabel" id="tdmdate" runat="server">
                </td>
                <td align="center" colspan="2">
                    <table>
                        <tr>
                            <td class="bluelabel" width="80">
                                <asp:Label ID="lang2482" runat="server">Order</asp:Label>
                            </td>
                            <td width="60">
                                <asp:TextBox ID="txtiorder" runat="server" Width="40px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td width="20">
                                <img onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif">
                            </td>
                            <td width="20">
                                <img onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)"
                                    onclick="addpic();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif">
                            </td>
                            <td width="20">
                                <img id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/del.gif" runat="server">
                            </td>
                            <td width="20">
                                <img id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)"
                                    onclick="savdets();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="20">
                <td class="label" id="Td5" runat="server">
                    <asp:Label ID="lang2483" runat="server">Phone</asp:Label>
                </td>
                <td class="plainlabel" runat="server" id="Td6" name="Td6">
                    &nbsp;
                </td>
                <td class="label">
                    <asp:Label ID="lang2484" runat="server">Email Address</asp:Label>
                </td>
                <td class="plainlabel" id="Td7" runat="server">
                    &nbsp;
                </td>
                <td align="center" colspan="2">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" align="center" width="134">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input id="lblclid" type="hidden" name="lblclid" runat="server"><input id="lbldept"
            type="hidden" name="lbldept" runat="server">
        <input id="lblchk" type="hidden" name="lblchk" runat="server"><input id="lblsid"
            type="hidden" name="lblsid" runat="server">
        <input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lbleqid"
            type="hidden" name="lbleqid" runat="server">
        <input id="lblpar" type="hidden" name="lblpar" runat="server"><input id="lbldchk"
            type="hidden" name="lbldchk" runat="server">
        <input id="lblecrflg" type="hidden" name="lblecrflg" runat="server"><input id="lblpic"
            type="hidden" name="lblpic" runat="server">
        <input id="lblpchk" type="hidden" runat="server" name="lblpchk"><input id="lbluser"
            type="hidden" runat="server" name="lbluser">
        <input id="appchk" type="hidden" name="appchk" runat="server"><input id="lblpareqid"
            type="hidden" runat="server" name="lblpareqid">
        <input id="lbladdchk" type="hidden" runat="server" name="lbladdchk"><input id="lbllog"
            type="hidden" runat="server" name="lbllog">
        <input id="lbllock" type="hidden" runat="server" name="lbllock"><input id="lbllockedby"
            type="hidden" runat="server" name="lbllockedby">
        <input id="lbllockchk" type="hidden" runat="server" name="lbllockchk"><input id="lbldb"
            type="hidden" runat="server" name="lbldb">
        <input id="lbllid" type="hidden" runat="server" name="lbllid"><input id="lbltyp"
            type="hidden" runat="server" name="lbltyp">
        <input id="lblro" type="hidden" runat="server" name="lblro"><input id="lblappstr"
            type="hidden" runat="server" name="lblappstr">
        <input id="lblapp" type="hidden" runat="server" name="lblapp"><input id="lbldel"
            type="hidden" runat="server" name="lbldel">
        <input id="lblpcnt" type="hidden" runat="server" name="lblpcnt">
        <input id="lblcurrp" type="hidden" runat="server" name="lblcurrp">
        <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
        <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
        <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
        <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
        <input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server">
        <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
        <input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders"
            type="hidden" name="lbliorders" runat="server">
        <input id="lbloldorder" type="hidden" runat="server" name="lbloldorder">
        <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
        <input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server">
        <input type="hidden" id="lblfslang" runat="server" />
        <input type="hidden" id="lblstart" runat="server" />
        <input type="hidden" id="lbleqnum" runat="server" />
        </table>
    </div>
    </form>
</body>
</html>
