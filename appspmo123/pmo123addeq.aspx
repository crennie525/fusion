﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123addeq.aspx.vb" Inherits="lucy_r12.pmo123addeq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function checkret() {
            var ret = document.getElementById("lblret").value;
            if (ret != "") {
                window.returnValue = ret;
                window.close();
            }
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="bluelabel">Saving Data</td>
    </tr>
    </table>
    </div>
    <input type="hidden" id="lblret" runat="server" />
    
    </form>
</body>
</html>
