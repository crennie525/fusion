﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123newtasks.aspx.vb"
    Inherits="lucy_r12.pmo123newtasks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmlibnewpmtaskdetsaspx_2.js"></script>
    <script language="JavaScript" type="text/javascript">
        function gettasks() {
            var funid = document.getElementById("lblfuid").value;
            var eReturn = window.showModalDialog("../equip/pmlibnewtaskgriddialog.aspx?funid=" + funid, "", "dialogHeight:700px; dialogWidth:1240px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblsubmit").value = "go";
                document.getElementById("form1").submit();
            }
            else {
                document.getElementById("lblsubmit").value = "go";
                document.getElementById("form1").submit();
            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="position: absolute; top: 0px; left: 0px">
            <tr id="trmsg" runat="server" height="30" class="details">
                <td colspan="7" id="tdmsg" runat="server" class="plainlabelblue">
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    
                        <asp:Repeater ID="rptrtaskrev" runat="server">
                            <HeaderTemplate>
                                <table width="1400">
                                    <tr>
                                        <td align="center" width="20" class="btmmenu plainlabel">
                                            <img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16">
                                        </td>
                                        <td width="40" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2491" runat="server">Task#</asp:Label>
                                        </td>
                                        <td width="70" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2492" runat="server">Sub Task#</asp:Label>
                                        </td>
                                        <td width="200" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2493" runat="server">Component Addressed</asp:Label>
                                        </td>
                                        <td width="30" class="btmmenu plainlabel">
                                            Qty
                                        </td>
                                        <td width="220" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2494" runat="server">Failure Modes Addressed</asp:Label>
                                        </td>
                                        <td width="260" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2495" runat="server">Task Description</asp:Label>
                                        </td>
                                        <td width="90" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2496" runat="server">Task Type</asp:Label>
                                        </td>
                                        <td width="90" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2497" runat="server">Pdm</asp:Label>
                                        </td>
                                        <td width="90" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2498" runat="server">Skill</asp:Label>
                                        </td>
                                        <td width="30" class="btmmenu plainlabel">
                                            Qty
                                        </td>
                                        <td width="40" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2499" runat="server">Time</asp:Label>
                                        </td>
                                        <td width="80" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2500" runat="server">Frequency</asp:Label>
                                        </td>
                                        <td width="80" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2501" runat="server">Eq Status</asp:Label>
                                        </td>
                                        <td width="80" class="btmmenu plainlabel">
                                            <asp:Label ID="lang2502" runat="server">Down Time</asp:Label>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg">
                                    <td class="plainlabel">
                                        <asp:CheckBox ID="cb4" runat="server"></asp:CheckBox>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' runat="server"
                                            ID="lbltasknum">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"subtask")%>' runat="server"
                                            ID="lblsubtasknum">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' runat="server"
                                            ID="Label11">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"cqty")%>' runat="server"
                                            ID="Label12">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"fm1")%>' runat="server"
                                            ID="Label13">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' runat="server"
                                            ID="Label14">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasktype")%>' runat="server"
                                            ID="Label15">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"pretech")%>' runat="server"
                                            ID="Label16">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"skill")%>' runat="server"
                                            ID="Label17">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"qty")%>' runat="server"
                                            ID="Label18">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"ttime")%>' runat="server"
                                            ID="Label19">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"freq")%>' runat="server"
                                            ID="Label20">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rd")%>' runat="server" ID="Label21">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rdt")%>' runat="server"
                                            ID="Label22">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>' runat="server"
                                            ID="lbltaskid">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                   
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <asp:ImageButton ID="remtask" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif">
                    </asp:ImageButton>
                    <img src="../images/appbuttons/minibuttons/lilpentrans.gif" onclick="gettasks();"
                        onmouseover="return overlib('Add/Edit Tasks for Selected Function', LEFT)" onmouseout="return nd()">
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lbldb" runat="server" name="lbldb">
    <input type="hidden" id="lblfuid" runat="server" name="lblfuid">
    <input type="hidden" id="lbllog" runat="server" name="lbllog">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
