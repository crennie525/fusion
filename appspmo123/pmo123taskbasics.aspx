﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123taskbasics.aspx.vb"
    Inherits="lucy_r12.pmo123taskbasics" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
    //Need to come back to Meters for calc
        function getmeters() {
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("pmo123meter.aspx?typ=eqid=" + eqid, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn.split(",");
                    var mtrid = ret[0];
                    var mtr = ret[1];
                    var uni = ret[2];
                    var wku = ret[3];
                    document.getElementById("lblmeterid").value = mtrid;
                    document.getElementById("lblmeter").value = mtr;
                    document.getElementById("lblunits").value = uni;
                    document.getElementById("lblweekuse").value = wku;
                    closeall();
                    document.getElementById('d17').className = "view";
                }
            }

            //mtrid + "," + mtr + "," + uni + "," + wku;
        }
        function retfunc() {
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("../equip/retfuncdialog.aspx?typ=fu&eqid=" + eqid, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    var fret = eReturn.split(",")
                    var fuid = fret[0]; // eReturn;
                    var fu = fret[1];
                    document.getElementById("lblfuid").value = fuid;
                    document.getElementById("lblfu").value = fu;
                    document.getElementById("tdfunc").innerHTML = fu;
                    closeall();
                    document.getElementById('d2').className = "view";
                }
            }
        }
        function getco() {
            fuid = document.getElementById("lblfuid").value;
            if (fuid != "") {
                var eReturn = window.showModalDialog("../equip/complistdialog.aspx?fuid=" + fuid + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    //returns coid and comp
                    var ret = eReturn;
                    var retarr = ret.split(",");
                    var coid = retarr[0];
                    var comp = retarr[1];
                    document.getElementById("tdcomp").innerHTML = comp;
                    document.getElementById("lblcoid").value = coid;
                    document.getElementById("lblcomp").value = comp;
                    //need postback
                    document.getElementById("lblstep").value = "3";
                    document.getElementById("lblsubmit").value = "gettasks"
                    document.getElementById("form1").submit();


                }
            }

        }
        function getstep(who) {
            document.getElementById("lblstep").value = who;
            if (who == "4") {
                closeall();
                document.getElementById('d4').className = "view";
            }
            else if (who == "5") {
                closeall();
                document.getElementById('d5').className = "view";
            }
            else if (who == "6") {
                closeall();
                var typlst = document.getElementById("ddtype");
                var ptlst = document.getElementById("ddpt");
                var sstr = typlst.options[typlst.selectedIndex].text
                if (sstr == "4 - Cond Monitoring") {
                    document.getElementById('d6').className = "view";
                }
                else {
                    document.getElementById('d7').className = "view";
                }

            }
            else if (who == "7") {
                closeall();
                document.getElementById('d7').className = "view";
            }
            else if (who == "8") {
                closeall();
                document.getElementById('d8').className = "view";
            }
            else if (who == "9") {
                closeall();
                document.getElementById('d9').className = "view";
            }
            else if (who == "10") {
                closeall();
                document.getElementById('d10').className = "view";
            }
            else if (who == "15") {
                closeall();
                document.getElementById('d15').className = "view";
            }
            else if (who == "16") {
                closeall();
                document.getElementById('d16').className = "view";
            }
            else if (who == "11") {
                closeall();
                var typlst = document.getElementById("ddeqstat");
                var sstr = typlst.options[typlst.selectedIndex].text
                if (sstr == "Down") {
                    document.getElementById('d11').className = "view";
                }
                else {
                    document.getElementById('d16').className = "view";
                }
            }
            else if (who == "12") {
                closeall();
                var hasm = document.getElementById("lblhasmeter").value
                if (hasm == "yes") {
                    document.getElementById('d12').className = "view";
                }
                else {
                    document.getElementById('d13').className = "view";
                }
            }
        }
        function checkret() {
            var step = document.getElementById("lblstep").value;
            if (step == "3") {
                closeall();
                document.getElementById('d3').className = "view";
            }

        }
        function closeall() {
            document.getElementById('d1').className = "details";
            document.getElementById('d2').className = "details";
            document.getElementById('d3').className = "details";
            document.getElementById('d4').className = "details";
            document.getElementById('d5').className = "details";
            document.getElementById('d6').className = "details";
            document.getElementById('d7').className = "details";
            document.getElementById('d8').className = "details";
            document.getElementById('d9').className = "details";
            document.getElementById('d10').className = "details";
            document.getElementById('d11').className = "details";
            document.getElementById('d12').className = "details";
            document.getElementById('d13').className = "details";
            document.getElementById('d14').className = "details";
            document.getElementById('d15').className = "details";
            document.getElementById('d16').className = "details";
            document.getElementById('d17').className = "details";
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
    <table width="420">
    <tr>
    <td class="thdrsing label" width="420">Task Basics Walkthrough</td>
    </tr>
    </table>
    </div>
    <div>
        <div>
            <table>
                <tr>
                    <td class="plainlabelblue">
                    </td>
                </tr>
            </table>
        </div>
        <div id="d1">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        All Tasks in Fusion are Grouped at the Functional Assembly Level.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To begin select a Function by Clicking the Look Up Button below.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    Select a Function
                                </td>
                                <td class="plainlabel" id="tdfunc" runat="server">
                                </td>
                                <td>
                                    <img id="img1" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif"
                                        onclick="retfunc();" alt="" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d2" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Most Tasks in Fusion will be based on a Component and Address one or more Failure
                        Modes associated with that Component.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To continue select a Component by Clicking the Look Up Button below.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    Select a Component
                                </td>
                                <td class="plainlabel" id="tdcomp" runat="server">
                                </td>
                                <td>
                                    <img id="img4" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif"
                                        onclick="getco();" alt="" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d3" class="details">
            <table width="420">
                <tr>
                    <td class="plainlabelblue" colspan="3" height="22">
                        Select Failure Modes This Task Will Address and Click the Submit Button
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="label" align="center">
                                    <asp:Label ID="lang1159" runat="server">Component Failure Modes</asp:Label>
                                </td>
                                <td>
                                </td>
                                <td class="label" align="center">
                                    <asp:Label ID="lang1160" runat="server">Task Failure Modes</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="200">
                                    <asp:ListBox ID="lbfailmaster" runat="server" Height="60px" SelectionMode="Multiple"
                                        Width="170px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center" width="22">
                                    <img class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                        width="20">
                                    <img class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                        width="20">
                                    <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                                    </asp:ImageButton><asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                                    </asp:ImageButton><img class="details" id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
                                        onclick="getss();" onmouseout="return nd()" height="20" src="../images/appbuttons/minibuttons/plusminus.gif"
                                        width="20">
                                </td>
                                <td align="center" width="200">
                                    <asp:ListBox ID="lbfailmodes" runat="server" Height="60px" SelectionMode="Multiple"
                                        Width="170px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right" class="plainlabel">
                                    <input type="button" value="Submit" onclick="getstep('4');" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d4" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Task Descriptions should be short and to the point. If a Task is complex it should
                        be broken down into Sub Tasks to simplify the process.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        For the purposes of this Demonstration we will only concentrate on a Top Level Task.
                    </td>
                </tr>
                <tr>
                    <td class="plainlabel" colspan="3" height="22">
                        To continue enter a Task Description and Click the Submit Button
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="txtotaske" runat="server" Width="320px" TextMode="MultiLine" Height="80px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right" class="plainlabel">
                        <input type="button" value="Submit" onclick="getstep('5');" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="d5" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Task Types are used to indicate the type of work that is being performed (e.g. Inspection,
                        Cleaning, etc.).
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Note that Task Type 4 - Cond Monitoring requires an additional selection for a Predictive
                        Technology
                    </td>
                </tr>
                <tr>
                    <td class="plainlabel" colspan="3" height="22">
                        Select a Task Type to continue.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    Select a Task Type.
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" Width="160px"
                                        Rows="1" DataTextField="tasktype" DataValueField="ttid" AutoPostBack="False">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d6" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        You selected Task Type 4 - Cond Monitoring.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To continue select the Predictive Technology that will be required to perform this
                        Task.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    Select a PdM Tech
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList ID="ddpt" runat="server" CssClass="plainlabel" Width="160px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d7" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Each Task requires a Top Level Skill.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Note PM Records created in the Fusion Manager are based on Skill, Skill Quantity,
                        Frequency, and a Running or Down status.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel">
                        To continue select the Top Level Skill that will be required to perform this Task.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    <asp:Label ID="lang259" runat="server">Select a Skill</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList ID="ddskill" runat="server" Width="160px" CssClass="plainlabel"
                                        AutoPostBack="False">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d8" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Will this Task require more than one idividual with the selected Skill?
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To continue verify the Skill Qty below and click the Submit Button.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel" width="120">
                                    Enter a Skill Qty
                                </td>
                                <td colspan="2" width="200">
                                    <asp:TextBox ID="txtqty" runat="server" CssClass="plainlabel" Width="30px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right" class="plainlabel">
                        <input type="button" value="Submit" onclick="getstep('9');" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="d9" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        How many minutes will it take to perform this task?
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        To continue enter the number of minutes required below and click the Submit Button.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    <asp:Label ID="lang260" runat="server">Min Ea</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txttr" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right" class="plainlabel">
                        <input type="button" value="Submit" onclick="getstep('10');" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="d10" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Each Task requires an Equipment Status.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Note PM Records created in the Fusion Manager are based on Skill, Skill Quantity,
                        Frequency, and a Running or Down status.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To continue select an Equipment Status
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    <asp:Label ID="lang263" runat="server">Equipment Status</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList ID="ddeqstat" runat="server" CssClass="plainlabel" Width="90px"
                                        AutoPostBack="False">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d11" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        You selected an Equipment Status of Down.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        By default Down Time is assigned the number of minutes you assigned to perform this
                        task.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To continue verify the Down Time below and Click the Submit Button.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    <asp:Label ID="lang264" runat="server">Down Time</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txtrdt" runat="server" CssClass="plainlabel" Width="35px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right" class="plainlabel">
                        <input type="button" value="Submit" onclick="getstep('16');" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="d12" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        This Equipment Record has Meters.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        Will the Frequency this Task is required to be performed be Meter or Calendar Based?
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="bluelabel" align="center">
                        <input type="radio" id="rbm" name="rbf" onclick="checkfreq('m');" />Meter Based
                        &nbsp;&nbsp;&nbsp;<input type="radio" id="rbc" name="rbf" onclick="checkfreq('c');" />Calendar
                        Based
                    </td>
                </tr>
            </table>
        </div>
        <div id="d13" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Each Task requires a Frequency.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Note PM Records created in the Fusion Manager are based on Skill, Skill Quantity,
                        Frequency, and a Running or Down status.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Note that checking the Fixed Frequency Check Box indicates that the frequency cannot
                        be adjusted in the Demand Based PM Module.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To continue enter a Calandar Frequency in Days and Click the Submit Button.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    <asp:Label ID="lang265" runat="server">Frequency</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="60px"></asp:TextBox>
                                    <input id="cbfixed" runat="server" type="checkbox" onmouseover="return overlib('Check if Frequency is Fixed (Calendar Frequency Only)')"
                                        onmouseout="return nd()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right" class="plainlabel">
                        <input type="button" value="Submit" onclick="getstep('15');" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="d14" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To begin choose a Meter using the Look Up Button below.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    Choose A Meter
                                </td>
                                <td>
                                    <img alt="" src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getmeters();" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d17" class="details">
            <table width="420">
                <tr>
                    <td class="plainlabelblue" align="left" valign="middle">
                        Frequency should be the required number of Units between PMs.
                        <br />
                        <br />
                        The Fusion PM Manager will calculate a projected schedule date based on the Weekly
                        Use value provided.<br />
                        <br />
                        If a Meter Reading Entry exceeds the selected frequency the Fusion PM Manager will
                        automatically update PM Records so they will appear in the current schedule.<br />
                        <br />
                        The Fusion PM Manager will not schedule a PM past the Last Date and Max Days value
                        provided.
                        <br />
                        <br />
                        If a Meter Reading Entry exceeds the selected frequency the Fusion PM Manager will
                        automatically update PM Records so they will appear in the current schedule.
                        <br />
                        <br />
                        For PM Development and Optimization purposes Fusion calculates a frequency in days
                        based on the above criteria.
                    </td>
                </tr>
                <tr>
                    <td class="plainlabel" height="20">
                        To continue enter a Meter Frequency for this Task and Click the Submit Button.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    <asp:Label ID="Label1" runat="server">Frequency</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txtmfreq" runat="server" CssClass="plainlabel" Width="60px"></asp:TextBox>
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right" class="plainlabel">
                        <input type="button" value="Submit" onclick="getstep('15');" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="d15" class="details">
            <table>
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        This Step is only Required in the Optimization Process and is important to determine
                        if any improvements have been accomplished in the Optimization Overview.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        UnChanged indicates that no changes were made to the Original Task.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        Add indicates that there was no Original Task.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        Revised indicates that changes were made to the Original Task.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        Delete indicates that the Original Task will not be included in any new PM Records.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To continue select a Task Status for this Task.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td class="bluelabel">
                                    <asp:Label ID="lang1033" runat="server">Task Status</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:DropDownList ID="ddtaskstat" runat="server" CssClass="plainlabel" AutoPostBack="False">
                                        <asp:ListItem Value="Select">Select</asp:ListItem>
                                        <asp:ListItem Value="UnChanged">UnChanged</asp:ListItem>
                                        <asp:ListItem Value="Add">Add</asp:ListItem>
                                        <asp:ListItem Value="Revised">Revised</asp:ListItem>
                                        <asp:ListItem Value="Delete">Delete</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="d16" class="details">
            <table width="420">
                <tr>
                    <td colspan="3" class="plainlabelblue" height="20">
                        Will this Task require Lock Out Tag Out (LOTO) or Confined Space (CS) protection?
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="plainlabel" height="20">
                        To continue make a selection if required and Click the Submit Button.
                    </td>
                </tr>
                <tr>
                    <td class="bluelabel" colspan="3" align="center">
                        <asp:CheckBox ID="cbloto" runat="server"></asp:CheckBox>LOTO&nbsp;&nbsp;<asp:CheckBox
                            ID="cbcs" runat="server"></asp:CheckBox>CS
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right" class="plainlabel">
                        <input type="button" value="Submit" onclick="getstep('12');" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblcoid" runat="server" />
    <input type="hidden" id="lblcomp" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblfu" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lbltasknum" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lbllocid" runat="server" />
    <input id="txtpg" type="hidden" runat="server" />
    <input id="txtpgcnt" type="hidden" runat="server" />
    <input type="hidden" id="lblcnt" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbldb" runat="server" />
    <input type="hidden" id="lblhasmeter" runat="server" />
    <input type="hidden" id="lbltaskfail" runat="server" />
    <input type="hidden" id="lbltaskfailstr" runat="server" />
    <input type="hidden" id="lblstep" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblmeter" runat="server" />
    <input type="hidden" id="lblunits" runat="server" />
    <input type="hidden" id="lblweekuse" runat="server" />
    </form>
</body>
</html>
