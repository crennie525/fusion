﻿Imports System.Data.SqlClient
Public Class pmo123neweqgrid
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim ds As DataSet
    Dim sql, sqlcnt As String
    Dim eqg As New Utilities
    Dim dept, cell, which As String
    Dim chk, dchk, sid, cid As String
    Dim did, clid, eqid, filt, loc, lid, typ, ro, Login As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
        End Try
        If Not IsPostBack Then
            Dim db As String = Request.QueryString("db").ToString
            lbldb.Value = db
            Dim user As String
            Try
                user = HttpContext.Current.Session("username").ToString
            Catch ex As Exception
                user = "PMADMIN"
            End Try

            lbluser.Value = user
            'Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                btnAddEq.Enabled = False
            End If
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            Try
                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                Try
                    loc = Request.QueryString("loc").ToString
                    lblloc.Value = loc
                Catch exin As Exception

                End Try
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                dept = Request.QueryString("did").ToString
                lbldid.Value = dept
                'Dim strMessage As String = lbldid.Value
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                cell = Request.QueryString("clid").ToString
                lblclid.Value = cell
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                dchk = Request.QueryString("dchk").ToString
                lbldchk.Value = dchk

            Catch ex As Exception
                lblclid.Value = "na"
            End Try
            lblcurreqsort.Value = "eqnum asc"
            'tdcurrsort.InnerHtml = "Equipment# Ascending"
            If typ = "loc" Or typ = "dloc" Then
                eqg.Open()
                LoadEq(dept, cell, lid)
                eqg.Dispose()
                'tduse.InnerHtml = "Using Locations"
            Else
                eqg.Open()
                LoadEq(dept, cell)
                eqg.Dispose()
                lbltyp.Value = "reg"
                'tduse.InnerHtml = "Using Departments"
            End If
            'Catch ex As Exception
            '*** Need to determine why this is loading twice, once without a querystring
            'End Try
            If did <> "" Then
                If lid <> "" And lid <> "0" Then
                    typ = "dloc"
                Else
                    typ = "reg"
                End If
                If clid = "" Or clid = "0" Then
                    lblpar.Value = "cell"
                Else
                    lblpar.Value = "dept"
                End If
            Else
                If lid <> "" And lid <> "0" Then
                    typ = "loc"
                    lbldchk.Value = "no"
                    lblpar.Value = "loc"
                End If
            End If
        Else '***???***
            Try
                dept = Request.QueryString("did").ToString
                lbldid.Value = dept
                Try
                    cell = Request.QueryString("clid").ToString
                    lblclid.Value = cell
                Catch ex As Exception
                    lblclid.Value = "na"
                End Try
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                dchk = Request.QueryString("dchk").ToString
                lbldchk.Value = dchk
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                If did <> "" Then
                    If lid <> "" And lid <> "0" Then
                        typ = "dloc"
                    Else
                        typ = "reg"
                    End If
                    If clid = "" Or clid = "0" Then
                        lblpar.Value = "cell"
                    Else
                        lblpar.Value = "dept"
                    End If
                Else
                    If lid <> "" And lid <> "0" Then
                        typ = "loc"
                        lbldchk.Value = "no"
                        lblpar.Value = "loc"
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub
    Public Sub SortEq(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sort As String = lblcurreqsort.Value
        If sort = "eqnum asc" Then
            sort = "eqnum desc"
            'tdcurrsort.InnerHtml = "Equipment# Descending"
        Else
            sort = "eqnum asc"
            'tdcurrsort.InnerHtml = "Equipment# Ascending"
        End If
        lblcurreqsort.Value = sort
        typ = lbltyp.Value
        dept = lbldid.Value
        cell = lblclid.Value
        lid = lbllid.Value
        If typ = "loc" Or typ = "dloc" Then
            eqg.Open()
            LoadEq(dept, cell, lid)
            eqg.Dispose()
            'tduse.InnerHtml = "Using Locations"
        Else
            eqg.Open()
            LoadEq(dept, cell)
            eqg.Dispose()
            lbltyp.Value = "reg"
            'tduse.InnerHtml = "Using Departments"
        End If
    End Sub
    Private Sub LoadEq(ByVal dept As String, ByVal cell As String, Optional ByVal loc As String = "0")
        Dim cnt As Integer '= 0
        Dim sort As String = lblcurreqsort.Value

        If loc = "0" Then
            If dept = "na" Then
                sql = "select * from equipment where eqid = 0" 'deptid = '" & dept & "'"
                cnt = 0 'sqlcnt = "select count(*) from equipment where eqid = 0"
            Else
                cnt = 1
                If cell = "no" Or cell = "" Or Len(cell) = 0 Then
                    sql = "select * from equipment where dept_id = '" & dept & "' order by " & sort
                    sqlcnt = "select count(*) from equipment where dept_id = '" & dept & "'"
                Else
                    sql = "select * from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' order by " & sort
                    sqlcnt = "select count(*) from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "'"
                End If
            End If
        Else
            cnt = 1
            If dept <> "" Then
                If cell = "no" Or cell = "" Or Len(cell) = 0 Then
                    sql = "select * from equipment where dept_id = '" & dept & "' and locid = '" & loc & "' order by " & sort
                    sqlcnt = "select count(*) from equipment where dept_id = '" & dept & "' and locid = '" & loc & "'"
                Else
                    sql = "select * from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & loc & "' order by " & sort
                    sqlcnt = "select count(*) from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & loc & "'"
                End If
            Else
                sid = lblsid.Value
                Dim loc2 As String
                loc2 = lblloc.Value
                If loc2 = "" Or Len(loc2) = 0 Then
                    Dim sql1 As String = "select location from pmlocations where locid = '" & loc & "'"
                    loc2 = eqg.strScalar(sql1)
                    lblloc.Value = loc2
                End If
                Dim lcnt As Integer
                If loc2 <> "" Then
                    sql = "select count(*) from pmlocations where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                        + "and type = 'EQUIPMENT'"
                    lcnt = eqg.Scalar(sql)
                    If lcnt > 0 Then
                        sql = "select * from equipment where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                        + "and siteid = '" & sid & "' and dept_id is null order by " & sort
                        sqlcnt = "select count(*) from equipment where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                        + "and siteid = '" & sid & "' and dept_id is null"
                    Else
                        sql = "select * from equipment where locid = '" & loc & "' and siteid = '" & sid & "' and dept_id is null order by " & sort
                        sqlcnt = "select count(*) from equipment where locid = '" & loc & "' and siteid = '" & sid & "' and dept_id is null"
                    End If
                Else
                    sql = "select * from equipment where locid = '" & loc & "' and siteid = '" & sid & "' and dept_id is null order by " & sort
                    sqlcnt = "select count(*) from equipment where locid = '" & loc & "' and siteid = '" & sid & "' and dept_id is null"
                End If




            End If
        End If

        If cnt <> 0 Then
            cnt = eqg.Scalar(sqlcnt)
        End If
        ds = eqg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptreq.DataSource = dt
        Dim i, eint As Integer
        eint = 15
        'sql = "select count(*) from equipment where deptid = '" & dept & "'"
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptreq.DataBind()

    End Sub

    Private Sub rptreq_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptreq.ItemCommand
        If e.CommandName = "Select" Then
            did = lbldid.Value
            clid = lblclid.Value
            chk = lblchk.Value
            dchk = lbldchk.Value
            sid = lblsid.Value
            cid = lblcid.Value
            lid = lbllid.Value
            loc = lbltyp.Value
            Dim locs As String = lblloc.Value
            Try
                eqid = CType(e.Item.FindControl("lbleqiditem"), Label).Text
            Catch ex As Exception
                eqid = CType(e.Item.FindControl("lbleqidalt"), Label).Text
            End Try
            lbleqid.Value = eqid
            'Dim strMessage As String = "rptreq"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Dim db As String = lbldb.Value
            Response.Redirect("pmo123neweq.aspx?start=yes&dchk=" & dchk & "&chk=" & chk & "&did=" & did & _
            "&clid=" & clid & "&sid=" & sid & "&cid=" & cid & "&eqid=" & eqid & "&lid=" & lid & "&typ=" & loc & "&loc=" + locs + "&db=" + db)
            'DataBinder.Eval(e.Item.DataItem, "func")
        End If
    End Sub

    Protected Sub btnAddEq_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddEq.Click
        'If lbldid.Value <> "na" Or lbllid.Value <> "" Then 'AndAlso lblclid.Value <> "na" Then
        did = lbldid.Value
        lid = lbllid.Value
        'Dim strMessage0 As String = lbldid.Value
        'Utilities.CreateMessageAlert(Me, strMessage0, "strKey1")
        If (did <> "" Or lid <> "") And (did <> "na" Or lid <> "na") Then
            Dim eq As String = txtneweq.Text
            eq = eqg.ModString1(eq)
            If Len(eq) > 0 Then
                eqg.Open()
                chk = lblchk.Value
                dchk = lbldchk.Value
                sid = lblsid.Value
                cid = lblcid.Value
                lid = lbllid.Value
                did = lbldid.Value
                'If lid = "" Then lid = "0"
                loc = lbltyp.Value
                Dim user As String = lbluser.Value
                Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)

                If loc = "reg" Or loc = "dloc" Then
                    If dchk = "yes" Then
                        did = lbldid.Value
                        If chk = "yes" Then
                            clid = lblclid.Value
                        ElseIf chk = "no" Then
                            CheckDummy("cell")
                            clid = lblclid.Value
                            If clid = "no" Then
                                clid = "0"
                            End If
                        End If

                    Else
                        CheckDummy("dept")
                        did = lbldid.Value
                    End If
                End If

                filt = "where eqnum = '" & eq & "' and compid = '" & cid & "'"
                sql = "select count(*) from equipment " & filt
                Dim echk As Integer
                echk = eqg.Scalar(sql)
                If echk = 0 And lid <> "" Then
                    sql = "select count(*) from pmlocations where location = '" & eq & "'"
                    echk = eqg.Scalar(sql)
                End If
                Dim eqid As Integer
                If echk = 0 Then
                    'create procedure addneweq (@compid int, @eqnum varchar(50), @cellid int, @siteid int, @dept_id int,
                    '@lid int, @createdby varchar(50), @lockedby varchar(50), @typ varchar(10))
                    sql = "addneweq '" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & did & "', '" & lid & " ', '" & ustr & "', '" & ustr & "', '" & loc & "'"
                    'If typ = "reg" Then
                    'sql = "insert into equipment (compid, eqnum, cellid, siteid, dept_id, createdby, createdate, locked, lockedby) values " _
                    '+ "('" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & did & "', '" & ustr & "', getDate(), 1, '" & ustr & "')" _
                    '+ " select @@identity as 'identity'"
                    'ElseIf typ = "loc" Then
                    'sql = "insert into equipment (compid, eqnum, siteid, locid, createdby, createdate, locked, lockedby) values " _
                    '+ "('" & cid & "', '" & eq & "', '" & sid & "', '" & lid & "', '" & ustr & "', getDate(), 1, '" & ustr & "')" _
                    '+ " select @@identity as 'identity'"
                    'ElseIf typ = "dloc" Then
                    'sql = "insert into equipment (compid, eqnum, cellid, siteid, locid, dept_id, createdby, createdate, locked, lockedby) values " _
                    '+ "('" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & lid & "', '" & did & " ', '" & ustr & "', getDate(), 1, '" & ustr & "')" _
                    '+ " select @@identity as 'identity'"
                    'End If

                    eqid = eqg.Scalar(sql)
                    lbleqid.Value = eqid
                    lbladdchk.Value = "yes"
                    'sql = "update admintasks set eq = (select count(*) from equipment where compid = '" & cid & "') where cid = '" & cid & "'"
                    'eqg.Update(sql)
                    If loc = "loc" Or loc = "dloc" Then
                        LoadEq(did, clid, lid)
                    Else
                        LoadEq(did, clid)
                    End If
                    did = lbldid.Value
                    clid = lblclid.Value
                    chk = lblchk.Value
                    dchk = lbldchk.Value
                    sid = lblsid.Value
                    cid = lblcid.Value
                    lid = lbllid.Value
                    loc = lbltyp.Value
                    Dim locs As String = lblloc.Value
                    Dim db As String = lbldb.Value
                    Response.Redirect("pmo123neweq.aspx?start=yes&dchk=" & dchk & "&chk=" & chk & "&did=" & did & _
                    "&clid=" & clid & "&sid=" & sid & "&cid=" & cid & "&eqid=" & eqid & "&lid=" & lid & "&typ=" & loc & "&loc=" + locs + "&db=" + db)
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr872", "EqBotGrid.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                eqg.Dispose()
            Else
                Dim strMessage As String = tmod.getmsg("cdstr873", "EqBotGrid.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If


        Else
            Dim strMessage As String = tmod.getmsg("cdstr874", "EqBotGrid.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub CheckDummy(ByVal field As String)
        sid = lblsid.Value
        cid = lblcid.Value
        Dim cnt As Integer
        If field = "cell" Then
            did = lbldid.Value
            sql = "Select count(*) from cells where compid = '" & cid & "' " _
            + "and siteid = '" & sid & "' and dept_id = '" & did & "' " _
            + "and cell_name = 'No Cells'"
            cnt = eqg.Scalar(sql)
            If cnt = 0 Then
                sql = "insert into cells (compid, siteid, dept_id, cell_name, cell_desc) values " _
                + "('" & cid & "', '" & sid & "', '" & did & "', 'No Cells', 'Not Using Cells') select @@identity as 'identity'"
                clid = eqg.Scalar(sql)
                lblclid.Value = clid
            End If
        ElseIf field = "dept" Then
            sql = "Select count(*) from dept where compid = '" & cid & "' " _
            + "and siteid = '" & sid & "' " _
            + "and dept_line = 'No Depts'"
            cnt = eqg.Scalar(sql)
            If cnt = 0 Then
                sql = "insert into dept (compid, siteid, dept_line, dept_desc) values " _
                + "('" & cid & "', '" & sid & "', 'No Depts', 'Not Using Departments') select @@identity as 'identity'"
                did = eqg.Scalar(sql)
                lbldid.Value = did
            End If
        End If
    End Sub

    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2167 As Label
                lang2167 = CType(e.Item.FindControl("lang2167"), Label)
                lang2167.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2167")
            Catch ex As Exception
            End Try
            Try
                Dim lang2168 As Label
                lang2168 = CType(e.Item.FindControl("lang2168"), Label)
                lang2168.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2168")
            Catch ex As Exception
            End Try
            Try
                Dim lang2169 As Label
                lang2169 = CType(e.Item.FindControl("lang2169"), Label)
                lang2169.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2169")
            Catch ex As Exception
            End Try
            Try
                Dim lang2170 As Label
                lang2170 = CType(e.Item.FindControl("lang2170"), Label)
                lang2170.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2170")
            Catch ex As Exception
            End Try

        End If
    End Sub
    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2167.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2167")
        Catch ex As Exception
        End Try
        Try
            'lang2168.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2168")
        Catch ex As Exception
        End Try
        Try
            'lang2169.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2169")
        Catch ex As Exception
        End Try
        Try
            'lang2170.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2170")
        Catch ex As Exception
        End Try

    End Sub
    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnAddEq.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnAddEq.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnAddEq.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnAddEq.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnAddEq.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub
End Class