﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123levels.aspx.vb"
    Inherits="lucy_r12.pmo123levels" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/levelsaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="JavaScript" type="text/javascript">
    
    </script>
</head>
<body onload="chkref();">
    <form id="form1" runat="server">
    <div style="z-index: 101; position: absolute; top: 0px; left: 0px">
        <table width="350">
            <tr>
                <td colspan="4">
                    <asp:Label ID="lblcat" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
                        ForeColor="Blue"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="70">
                </td>
                <td width="30">
                </td>
                <td width="80">
                </td>
                <td width="170">
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <hr style="border-bottom: #0000ff 2px solid; border-left: #0000ff 2px solid; border-top: #0000ff 2px solid;
                        border-right: #0000ff 2px solid">
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 5</asp:Label><input
                        id="rb5" onclick="document.getElementById('lbllev').value='5';" type="radio"
                        value="rb5" name="rbw" runat="server">
                </td>
                <td colspan="3">
                    <asp:TextBox ID="t5" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
                        Width="290px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 4</asp:Label><input
                        id="rb4" onclick="document.getElementById('lbllev').value='4';" type="radio"
                        value="rb4" name="rbw" runat="server">
                </td>
                <td colspan="3">
                    <asp:TextBox ID="t4" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
                        Width="290px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 3</asp:Label><input
                        id="rb3" onclick="document.getElementById('lbllev').value='3';" type="radio"
                        value="rb3" name="rbw" runat="server">
                </td>
                <td colspan="3">
                    <asp:TextBox ID="t3" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
                        Width="290px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 2</asp:Label><input
                        id="rb2" onclick="document.getElementById('lbllev').value='2';" type="radio"
                        value="rb2" name="rbw" runat="server">
                </td>
                <td colspan="3">
                    <asp:TextBox ID="t2" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
                        Width="290px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 1</asp:Label><input
                        id="rb1" onclick="document.getElementById('lbllev').value='1';" type="radio"
                        value="rb1" name="rbw" runat="server">
                </td>
                <td colspan="3">
                    <asp:TextBox ID="t1" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
                        Width="290px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="plainlabelblue" colspan="4">
                    <p>
                        <asp:Label ID="lang2407" runat="server">Choose a level for this category</asp:Label></p>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Current ECR:</asp:Label>
                </td>
                <td colspan="2">
                    <asp:Label ID="tc" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="4">
                    <asp:ImageButton ID="btnsave" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif">
                    </asp:ImageButton>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="plainlabelblue" id="tdrcm" colspan="4" runat="server">
                </td>
            </tr>
            <tr class="details" id="trdd" runat="server">
                <td class="label" colspan="2">
                    <asp:Label ID="lang2408" runat="server">RCM Software</asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddecm" runat="server">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">Reliasoft RCM++</asp:ListItem>
                        <asp:ListItem Value="2">Relex Reliability</asp:ListItem>
                        <asp:ListItem Value="3">Isograph RCMCost</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lblecrid"
        type="hidden" name="lblecrid" runat="server"><input id="lbllev" type="hidden" name="lbllev"
            runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server"><input id="lblchk"
        type="hidden" name="lblchk" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
