﻿Imports System.Data.SqlClient
Public Class pmo123addeq
    Inherits System.Web.UI.Page
    Dim adeq As New Utilities
    Dim sql As String
    Dim who As String
    Dim eqnum, did, clid, locid, sid, ustr, eqid As String
    Dim desc, spl, loc, oem, oemid, model, ser, stat, ac, acid, charge As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        who = Request.QueryString("who").ToString
        If who = "add" Then
            eqnum = Request.QueryString("eqnum").ToString
            did = Request.QueryString("did").ToString
            clid = Request.QueryString("clid").ToString
            locid = Request.QueryString("locid").ToString
            sid = Request.QueryString("sid").ToString
            ustr = Request.QueryString("ustr").ToString
            Dim ecnt As Integer = 0
            sql = "select count(*) from equipment where eqnum = '" & eqnum & "'"
            adeq.Open()
            ecnt = adeq.Scalar(sql)
            If ecnt = 0 Then
                addeq(did, clid, locid, sid, eqnum, ustr)
            Else
                lblret.Value = "0"
            End If
            adeq.Dispose()
        ElseIf who = "dets" Then
            ustr = Request.QueryString("ustr").ToString
            eqid = Request.QueryString("eqid").ToString
            desc = Request.QueryString("desc").ToString
            spl = Request.QueryString("spl").ToString
            loc = Request.QueryString("loc").ToString
            'oem = Request.QueryString("oem").ToString
            oemid = Request.QueryString("oemid").ToString
            model = Request.QueryString("model").ToString
            ser = Request.QueryString("ser").ToString
            stat = Request.QueryString("stat").ToString
            'ac = Request.QueryString("ac").ToString
            acid = Request.QueryString("acid").ToString
            charge = Request.QueryString("charge").ToString
            adeq.Open()
            upeq(eqid, desc, spl, loc, oemid, model, ser, stat, ac, charge, ustr)
            adeq.Dispose()
        End If
        
    End Sub
    Private Sub upeq(ByVal eqid As String, ByVal desc As String, ByVal spl As String, ByVal loc As String, _
                     ByVal oemid As String, ByVal model As String, ByVal ser As String, ByVal stat As String, _
                     ByVal ac As String, ByVal charge As String, ByVal ustr As String)

        Dim statstr As String = ""
        If stat <> "0" And stat <> "Select Status" Then
            sql = "select status from eqststus where statid = '" & stat & "'"
            statstr = adeq.strScalar(sql)
        End If
        Dim acstr As String = ""
        If ac <> "0" And ac <> "Select Asset Class" Then
            sql = "select assetclass from pmassetclass where acid = '" & ac & "'"
            acstr = adeq.strScalar(sql)
        End If
        Dim cmd As New SqlCommand
        cmd.CommandText = "update equipment set " _
                + "eqdesc = @desc, " _
                + "location = @loc, " _
                + "oem = @oemid, " _
                + "model = @model, " _
                + "serial = @ser, " _
                + "spl = @spl, " _
                + "statid = @stat, " _
                + "status = @statstr, " _
                + "acid = @ac, " _
                + "assetclass = @acstr, " _
                + "chargenum = @charge, " _
                + "modifiedby = @ustr, " _
                + "modifieddate = getDate() " _
                + "where eqid = @eqid " _
                + "update equipment  set eqstatindex = (" _
                + "select eqstatindex from eqstatus where " _
                + "statid = @stat) where eqid = @eqid"

        Dim param0 = New SqlParameter("@eqid", SqlDbType.VarChar)
        param0.Value = eqid
        cmd.Parameters.Add(param0)
        Dim param01 = New SqlParameter("@desc", SqlDbType.VarChar)
        If Len(desc) > 0 Then
            param01.Value = desc
        Else
            param01.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@loc", SqlDbType.VarChar)
        If Len(loc) > 0 Then
            param02.Value = loc
        Else
            param02.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@oemid", SqlDbType.VarChar)
        If Len(oemid) > 0 Then
            param03.Value = oemid
        Else
            param03.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@model", SqlDbType.VarChar)
        If Len(model) > 0 Then
            param04.Value = model
        Else
            param04.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@ser", SqlDbType.VarChar)
        If Len(ser) > 0 Then
            param05.Value = ser
        Else
            param05.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@spl", SqlDbType.VarChar)
        If Len(spl) > 0 Then
            param06.Value = spl
        Else
            param06.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@stat", SqlDbType.VarChar)
        If Len(stat) > 0 Then
            param07.Value = stat
        Else
            param07.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@statstr", SqlDbType.VarChar)
        If Len(statstr) > 0 Then
            param08.Value = statstr
        Else
            param08.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@ac", SqlDbType.VarChar)
        If Len(ac) > 0 Then
            param09.Value = ac
        Else
            param09.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param09)
        Dim param10 = New SqlParameter("@acstr", SqlDbType.VarChar)
        If Len(acstr) > 0 Then
            param10.Value = acstr
        Else
            param10.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@charge", SqlDbType.VarChar)
        If Len(charge) > 0 Then
            param11.Value = charge
        Else
            param11.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param11)
        Dim param12 = New SqlParameter("@ustr", SqlDbType.VarChar)
        If Len(ustr) > 0 Then
            param12.Value = ustr
        Else
            param12.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param12)
        adeq.UpdateHack(cmd)
        lblret.Value = eqid
    End Sub
    Private Sub addeq(ByVal did As String, ByVal clid As String, ByVal locid As String, ByVal sid As String, ByVal eqnum As String, ByVal ustr As String)
        Dim cid As String = "0"
        Dim typ As String
        If did <> "" And locid = "0" Then
            typ = "reg"
        ElseIf did = "" And locid <> "" Then
            typ = "loc"
        ElseIf did <> "" And locid <> "" Then
            typ = "dloc"
        End If
        sql = "addneweq '" & cid & "', '" & eqnum & "', '" & clid & "', '" & sid & "', '" & did & "', '" & locid & " ', '" & ustr & "', '" & ustr & "', '" & typ & "'"
        eqid = adeq.Scalar(sql)
        lblret.Value = eqid
    End Sub

End Class