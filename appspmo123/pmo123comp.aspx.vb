﻿Imports System.Data.SqlClient
Imports System.Text
Public Class pmo123comp
    Inherits System.Web.UI.Page
    Dim fun As New Utilities
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid, fuid, ustr, sid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            ustr = Request.QueryString("ustr").ToString
            lblustr.Value = ustr
            sid = Request.QueryString("lblsid").ToString
            lblsid.Value = sid
        Else
            If Request.Form("lblsubmit") = "addcomp" Then
                lblsubmit.Value = ""
                'ifimge.Attributes.Add("src", "")
                fun.Open()
                addcomp()
                'getfunc()
                fun.Dispose()
            End If
        End If
    End Sub
    Private Sub addcomp()
        fuid = lblfuid.Value
        Dim co As String = txtconame.Text
        co = fun.ModString1(co)
        sid = lblsid.Value
        Dim ccnt As Integer = 0
        sql = "select count(*) from components where compnum = '" & co & "' and func_id = '" & fuid & "'"
        ccnt = fun.Scalar(sql)
        If ccnt = 0 Then
            sql = "sp_addComponent 0, " & fuid & ", '" & co & "', '', '', '','','" & sid & "'"
            Dim coid As Integer
            coid = fun.Scalar(sql)

        Else
            Dim strMessage As String = tmod.getmsg("cdstr845", "CompDivGrid.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
End Class