﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123equip.aspx.vb" Inherits="lucy_r12.pmo123equip" %>

<%@ Register src="../menu/mmenu1.ascx" tagname="mmenu1" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        var d1 = 1;
        var d2 = 0;
        var d3 = 0;
        var d4 = 0;

        var d4a = 0;

        var d5 = 0;
        var d6 = 0;
        var d7 = 0;
        var d8 = 0;
        var d9 = 0;

        function getminsrch() {
        //var did = document.getElementById("lbldid").value;
                //alert(did)
        //if (did != "") {
        //    retminsrch();
        //}
        //else {
            var sid = document.getElementById("lblsid").value;
            var cb = document.getElementById("cbret");
            var wo = "";
            var typ = "lul";
            if (cb.checked == true) {
                typ = "lup";
            }
            else {
                typ = "lul";
            }

            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];

                document.getElementById("lblrettyp").value = "depts";
                document.getElementById("lbllocstat").value = "ok";

                document.getElementById("trdepts").className = "view";


                if (typ == "lup") {
                    document.getElementById("lbleqid").value = ret[4];
                    document.getElementById("lblsubmit").value = "geteq";
                    document.getElementById("form1").submit();
                }
                else {
                    controlrow("d2");
                    checkpage()
                }
            }
        //}
        }

        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var cb = document.getElementById("cbret");
            var wo = "";
            var typ = "lul";
            if (cb.checked == true) {
                typ = "lup";
            }
            else {
                typ = "lul";
            }
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                var ret = eReturn.split("~");
                document.getElementById("lbllocid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                document.getElementById("tdloc3").innerHTML = ret[2];

                document.getElementById("lblrettyp").value = "locs";
                document.getElementById("lbllocstat").value = "ok";

                document.getElementById("trlocs").className = "view";

                if (typ == "lup") {
                    document.getElementById("lbleqid").value = ret[3];
                    document.getElementById("lblsubmit").value = "geteq";
                    document.getElementById("form1").submit();
                }
                else {
                    controlrow("d2");
                    checkpage()
                }
            }
        }
        function clearall() {
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";

            document.getElementById("lbllocid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("tdloc3").innerHTML = "";

            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            document.getElementById("lblrettyp").value = "";
            document.getElementById("lbllocstat").value = "";

            //need to clear ifs here

        }

        function addeq() {
            var locstat = document.getElementById("lbllocstat").value;
            if (locstat == "ok") {
                var eqnum = document.getElementById("txtneweq").value;
                eqnum = modstring(eqnum);
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var clid = document.getElementById("lblclid").value;
                var locid = document.getElementById("lbllocid").value;
                var ustr = document.getElementById("lblustr").value;
                if (eqnum.length > 50) {
                    alert("Equipment Number Limited to 50 Characters")
                }
                else {

                    var eReturn = window.showModalDialog("pmo123addeq.aspx?who=add&did=" + did + "&sid=" + sid + "&clid=" + clid + "&locid=" + locid + "&eqnum=" + eqnum + "&ustr=" + ustr, "", "dialogHeight:50px; dialogWidth:50px; resizable=yes");
                    if (eReturn) {
                        if (eReturn != "" && eReturn != "0") {
                            document.getElementById("lbleqid").value = eReturn;
                            document.getElementById("lbleqnum").value = eqnum;
                            document.getElementById("tdeqnum").innerHTML = eqnum;
                            document.getElementById("txtneweq").value = "";
                            document.getElementById("lblsubmit").value = "ret";
                            checkreturn();
                            controlrow("d3");
                        }
                        else if (eReturn == "0") {
                            alert("Cannot Enter Duplicate Equipment Name")
                        }
                    }
                }
            }
        }
        function upeq() {
            //desc, spl, loc, oem, oemid, model, ser, stat, ac, acid, charge
            var eqid = document.getElementById("lbleqid").value;
            var desc = document.getElementById("txteqdesc").value;
            desc = modstring(desc);
            var spl = document.getElementById("txtspl").value;
            spl = modstring(spl);
            var loc = document.getElementById("txtloc").value;
            loc = modstring(loc);
            var oemid = document.getElementById("ddoem").value;
            if (oemid == "Select OEM") {
                oemid = "0";
            }
            var model = document.getElementById("txtmod").value;
            model = modstring(model);
            var ser = document.getElementById("txtser").value;
            ser = modstring(ser);
            var stat = document.getElementById("ddlstat").value;
            if (stat == "Select Status") {
                stat = "0";
            }
            var ac = document.getElementById("ddac").value;
            if (ac == "Select Asset Class") {
                ac = "0";
            }
            var charge = document.getElementById("txtcharge").value;
            charge = modstring(charge);
            var ustr = document.getElementById("lblustr").value;
            var eReturn = window.showModalDialog("pmo123addeq.aspx?who=dets&eqid=" + eqid + "&desc=" + desc + "&spl=" + spl + "&loc=" + loc + "&oemid=" + oemid + "&model=" + model + "&ser=" + ser + "&stat=" + stat + "&acid=" + ac + "&charge=" + charge + "&ustr=" + ustr, "", "dialogHeight:50px; dialogWidth:50px; resizable=yes");
            if (eReturn) {
            
                controlrow("d4");
            }
        }
        function modstring(who) {
            who = who.replace("#", "");
            who = who.replace("'", "`");
            return who;
        }


        function zeroall() {
            d1 = 0;
            d2 = 0;
            d3 = 0;
            d4 = 0;

            d4a = 0;

            d5 = 0;
            d6 = 0;
            d7 = 0;
            d8 = 0;
            d9 = 0;
        }
        function controlrowa() {

        }
        function controlrow(who) {

            var chk;
            var u = '../images/appbuttons/optup.gif';
            var d = '../images/appbuttons/optdown.gif'
            closerows();
            if (who == "d1") {
                if (d1 == 0) {
                    d1 = 1;
                    d2 = 0;
                    d3 = 0;
                    d4 = 0;

                    d4a = 0;

                    d5 = 0;
                    d6 = 0;
                    d7 = 0;
                    d8 = 0;
                    d9 = 0;
                    chk = d;
                    document.getElementById('d1').className = "view";
                    //document.getElementById('row1').src = chk;
                    document.getElementById('s1').className = "view";
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d2") {
                if (d2 == 0) {
                    d1 = 0;
                    d2 = 1;
                    d3 = 0;
                    d4 = 0;

                    d4a = 0;

                    d5 = 0;
                    d6 = 0;
                    d7 = 0;
                    d8 = 0;
                    d9 = 0;
                    chk = d;
                    document.getElementById('d2').className = "view";
                    //document.getElementById('row2').src = chk;
                    document.getElementById('s2').className = "view";
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d3") {
                if (d3 == 0) {
                    d1 = 0;
                    d2 = 0;
                    d3 = 1;
                    d4 = 0;

                    d4a = 0;

                    d5 = 0;
                    d6 = 0;
                    d7 = 0;
                    d8 = 0;
                    d9 = 0;
                    chk = d;
                    document.getElementById('d3').className = "view";
                    //document.getElementById('row3').src = chk;
                    document.getElementById('s3').className = "view";
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d4") {
                if (d4 == 0) {
                    d1 = 0;
                    d2 = 0;
                    d3 = 0;
                    d4 = 1;

                    d4a = 0;

                    d5 = 0;
                    d6 = 0;
                    d7 = 0;
                    d8 = 0;
                    d9 = 0;
                    chk = d;
                    document.getElementById('d4').className = "view";
                    document.getElementById('s4').className = "view";
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d4a") {
                if (d4a == 0) {
                    d1 = 0;
                    d2 = 0;
                    d3 = 0;
                    d4 = 0;

                    d4a = 1;

                    d5 = 0;
                    d6 = 0;
                    d7 = 0;
                    d8 = 0;
                    d9 = 0;
                    chk = d;
                    chk = d;
                    document.getElementById('d4a').className = "view";
                    //document.getElementById('row4').src = chk;
                    document.getElementById('s4a').className = "view";
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d5") {
                if (d5 == 0) {
                    d1 = 0;
                    d2 = 0;
                    d3 = 0;
                    d4 = 0;

                    d4a = 0;

                    d5 = 1;
                    d6 = 0;
                    d7 = 0;
                    d8 = 0;
                    d9 = 0;
                    chk = d;
                    document.getElementById('d5').className = "view";
                    //document.getElementById('row5').src = chk;
                    document.getElementById('s5').className = "view";
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d6") {
                if (d6 == 0) {
                    d1 = 0;
                    d2 = 0;
                    d3 = 0;
                    d4 = 0;

                    d4a = 0;

                    d5 = 0;
                    d6 = 1;
                    d7 = 0;
                    d8 = 0;
                    d9 = 0;
                    chk = d;
                    document.getElementById('d6').className = "view";
                    //document.getElementById('row6').src = chk;
                    document.getElementById('s6').className = "view";
                    /*
                    document.getElementById('s1').className = "details";
                    document.getElementById('s2').className = "details";
                    document.getElementById('s3').className = "details";
                    document.getElementById('s4').className = "details";
                    document.getElementById('s5').className = "details";
                    */
                    //document.getElementById('d6').className = "details";
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d7") {
                if (d7 == 0) {
                    d1 = 0;
                    d2 = 0;
                    d3 = 0;
                    d4 = 0;

                    d4a = 0;

                    d5 = 0;
                    d6 = 0;
                    d7 = 1;
                    d8 = 0;
                    d9 = 0;
                    chk = d;
                    document.getElementById('d7').className = "view";
                    //document.getElementById('row7').src = chk;
                    document.getElementById('s7').className = "view";
                    /*
                    document.getElementById('s1').className = "details";
                    document.getElementById('s2').className = "details";
                    document.getElementById('s3').className = "details";
                    document.getElementById('s4').className = "details";
                    document.getElementById('s5').className = "details";
                    document.getElementById('s6').className = "details";
                    */
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d8") {
                if (d8 == 0) {
                    d1 = 0;
                    d2 = 0;
                    d3 = 0;
                    d4 = 0;

                    d4a = 0;

                    d5 = 0;
                    d6 = 0;
                    d7 = 0;
                    d8 = 1;
                    d9 = 0;
                    chk = d;
                    document.getElementById('d8').className = "view";
                    //document.getElementById('row8').src = chk;
                    document.getElementById('s8').className = "view";
                }
                else {
                    zeroall();
                }
            }
            else if (who == "d9") {
                if (d9 == 0) {
                    d1 = 0;
                    d2 = 0;
                    d3 = 0;
                    d4 = 0;

                    d4a = 0;

                    d5 = 0;
                    d6 = 0;
                    d7 = 0;
                    d8 = 0;
                    d9 = 1;
                    chk = d;
                    document.getElementById('d9').className = "view";
                    //document.getElementById('row9').src = chk;
                    document.getElementById('s9').className = "view";
                }
                else {
                    zeroall();
                }
            }
        }
        function closerows() {
        /*
            document.getElementById('row1').src = '../images/appbuttons/optup.gif';
            document.getElementById('row2').src = '../images/appbuttons/optup.gif';
            document.getElementById('row3').src = '../images/appbuttons/optup.gif';
            document.getElementById('row4').src = '../images/appbuttons/optup.gif';
            document.getElementById('row5').src = '../images/appbuttons/optup.gif';
            document.getElementById('row6').src = '../images/appbuttons/optup.gif';
            document.getElementById('row7').src = '../images/appbuttons/optup.gif';
            document.getElementById('row8').src = '../images/appbuttons/optup.gif';
            document.getElementById('row9').src = '../images/appbuttons/optup.gif';
            document.getElementById('row10').src = '../images/appbuttons/optup.gif';
            */
            document.getElementById('d1').className = "details";
            document.getElementById('d2').className = "details";
            document.getElementById('d3').className = "details";
            document.getElementById('d4').className = "details";

            document.getElementById('d4a').className = "details";

            document.getElementById('d5').className = "details";
            document.getElementById('d6').className = "details";
            document.getElementById('d7').className = "details";
            document.getElementById('d8').className = "details";
            document.getElementById('d9').className = "details";


            document.getElementById('s1').className = "details";
            document.getElementById('s2').className = "details";
            document.getElementById('s3').className = "details";
            document.getElementById('s4').className = "details";

            document.getElementById('s4a').className = "details";

            document.getElementById('s5').className = "details";
            document.getElementById('s6').className = "details";
            document.getElementById('s7').className = "details";
            document.getElementById('s8').className = "details";
            document.getElementById('s9').className = "details";

        }
        function checkreturn() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "ret") {
                var eqid = document.getElementById("lbleqid").value;
                var eqnum = document.getElementById("lbleqnum").value;
                var ustr = document.getElementById("lblustr").value;
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var clid = document.getElementById("lblclid").value;
                var locid = document.getElementById("lbllocid").value;
                
                if (chk == "ret") {
                    document.getElementById("ifimge").src = "../equip/equploadimage.aspx?typ=eq&funcid=&comid=&eqid=" + eqid + "&ro=0&date=" + Date();
                    document.getElementById("iffu").src = "pmo123func.aspx?eqid=" + eqid + "&ustr=" + ustr + "&date=" + Date();
                    document.getElementById("ifful").src = "pmo123funclist.aspx?eqid=" + eqid + "&ustr=" + ustr + "&date=" + Date();

                    document.getElementById("ifecr").src = "pmo123ecr.aspx?eqid=" + eqid + "&ustr=" + ustr + "&date=" + Date();

                    document.getElementById("ifsm").src = "pmo123tasks.aspx?eqid=" + eqid + "&ustr=" + ustr + "&eqnum=" + eqnum + "&sid=" + sid + "&locid=" + locid + "&did=" + did + "&clid=" + clid + "&date=" + Date();
                    document.getElementById("ifwt").src = "pmo123taskbasics.aspx?eqid=" + eqid + "&sid=" + sid + "&date=" + Date();

                    document.getElementById("ifmeter").src = "../equip/emeter.aspx?eqid=" + eqid + "&ustr=" + ustr + "&date=" + Date();

                    document.getElementById("ifprocs").src = "../appsopt/upload.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date();
                    controlrow("d3");
                }
            }
            checkpage();
        }
        function checkpage() {
            var rettyp = document.getElementById("lblrettyp").value;
            //alert(rettyp)
            if (rettyp == "depts") {
                document.getElementById("trdepts").className = "view";
                var dept = document.getElementById("lbldept").value;
                var cell = document.getElementById("lblcell").value;
                document.getElementById("tddept").innerHTML = document.getElementById("lbldept").value;
                document.getElementById("tdcell").innerHTML = document.getElementById("lblcell").value;
                //document.getElementById("tdeq").innerHTML = document.getElementById("lbleq").value;
                //document.getElementById("tdfu").innerHTML = document.getElementById("lblfu").value;
                //document.getElementById("tdco").innerHTML = document.getElementById("lblcomp").value;
                //tddeptstat
                document.getElementById("tddeptstat").innerHTML = dept
                document.getElementById("tdcellstat").innerHTML = cell
                //document.getElementById("tdstats").innerHTML += "<br><b>Cell:</b> " + cell + "<br>"
            }
            else if (rettyp == "locs") {
                document.getElementById("trlocs").className = "view";
                var loc = document.getElementById("lblloc").value;
                document.getElementById("tdloc3").innerHTML = document.getElementById("lblloc").value;
                //document.getElementById("tdeq3").innerHTML = document.getElementById("lbleq").value;
                //document.getElementById("tdfu3").innerHTML = document.getElementById("lblfu").value;
                //document.getElementById("tdco3").innerHTML = document.getElementById("lblcomp").value;
                //document.getElementById("tdstats").innerHTML = "<b>Location:</b> " + loc + "<br>"
                document.getElementById("tdlocstat").innerHTML = loc
            }
            var eqnum = document.getElementById("lbleqnum").value;
            if (eqnum != "") {
                //document.getElementById("tdstats").innerHTML += "<br><b>Equipment:</b> " + eqnum + "<br>"
                document.getElementById("tdeqstat").innerHTML = eqnum
            }
        }
        function handlefuncret(fid) {
            var sid = document.getElementById("lblsid").value;
            var ustr = document.getElementById("lblustr").value;
            document.getElementById("ifco").src = "../equip/addcomp3.aspx?fuid=" + fid + "&sid=" + sid + "&ustr=" + ustr + "&date=" + Date();
            document.getElementById("ifcolist").src = "pmo123complist.aspx?fuid=" + fid + "&date=" + Date();
        }
        function getwt() {
            document.getElementById("trwt").className = "view";
            document.getElementById("trsm").className = "details";
        }
        function getsm() {
            document.getElementById("trsm").className = "view";
            document.getElementById("trwt").className = "details";
        }
        function getpmo() {
            alert("This Link is Under Construction")
        }
        function getdad() {
            alert("This Link is Under Construction")
        }
        function getgal() {
            alert("This Link is Under Construction")
        }
    </script>
    <style type="text/css">
        .box
        {
            border-bottom: black 1px solid;
            border-left: black 1px solid;
            border-right: black 1px solid;
            border-top: black 1px solid;
        }
    </style>
</head>
<body onload="checkreturn();">
    <form id="form1" runat="server">
    <table border="0"  style="position: absolute; top: 68px; left: 4px; z-index: 2">
        <tr>
            <td width="720" valign="top">
                <table>
                    <tr>
                        <td class="thdrsing label" width="720" height="18">
                            PM Development and Optimization Walkthrough
                        </td>
                    </tr>
                </table>
                <div id="s1" class="view">
                    <table>
                        <tr>
                            <td>
                                
                                <img id="Img1" onclick="controlrow('d2');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #1:</b> Select a Location
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d1" class="view">
                    <table>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td id="tddepts" class="bluelabel" runat="server">
                                            Use Departments
                                        </td>
                                        <td>
                                            <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                        <td id="tdlocs1" class="bluelabel" runat="server">
                                            Use Locations
                                        </td>
                                        <td>
                                            <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                        </td>
                                        <td class="plainlabelblue">
                                            &nbsp;
                                            <input type="checkbox" id="cbret" runat="server" />Check If You Are Returning to
                                            an Existing Equipment Record
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trdepts" class="details" runat="server">
                            <td>
                                <table>
                                    <tr>
                                        <td class="label" width="110">
                                            Department
                                        </td>
                                        <td id="tddept" class="plainlabel" width="170" runat="server">
                                        </td>
                                        <td class="label">
                                            Station\Cell
                                        </td>
                                        <td id="tdcell" class="plainlabel" runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trlocs" class="details" runat="server">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td class="label" width="110">
                                            Location
                                        </td>
                                        <td id="tdloc3" class="plainlabel" width="170" runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s2" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row2" onclick="controlrow('d1');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img2" onclick="controlrow('d3');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #2:</b> Enter A New Equipment Number
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d2" class="details">
                    <table>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="tbg">
                                <asp:TextBox ID="txtneweq" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                            </td>
                            <td class="tbg">
                                <img id="btnAddEq" runat="server" src="../images/appbuttons/minibuttons/addmod.gif"
                                    alt="" onclick="addeq();" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s3" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row3" onclick="controlrow('d2');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img3" onclick="controlrow('d4');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #3:</b> Enter Any Equipment Details Required
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d3" class="details">
                    <table>
                        <tr>
                            <td class="label" height="22">
                                <asp:Label ID="lang2144" runat="server">Equipment#</asp:Label>
                            </td>
                            <td class="plainlabel" id="tdeqnum" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2145" runat="server">Description</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txteqdesc" runat="server" Width="388px" MaxLength="100" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="height: 23px">
                                <asp:Label ID="lang2146" runat="server">Spl Identifier</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtspl" runat="server" Width="388px" MaxLength="250" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2147" runat="server">Location</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtloc" runat="server" Width="388px" MaxLength="50" Font-Size="9pt"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                OEM
                            </td>
                            <td>
                                <asp:DropDownList ID="ddoem" runat="server" Width="150px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2148" runat="server">Model#/Type</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtmod" runat="server" Width="150px" MaxLength="50" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2149" runat="server">Serial#</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtser" runat="server" Width="150px" MaxLength="50" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2150" runat="server">Status</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlstat" runat="server" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2151" runat="server">Asset Class</asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddac" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="Select Asset Class">Select Asset Class</asp:ListItem>
                                </asp:DropDownList>
                                <img class="details" id="btnaddasset" onmouseover="return overlib('Add/Edit Asset Class', LEFT)"
                                    onclick="getACDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                                    width="20">
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                <asp:Label ID="lang2152" runat="server">Charge#</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtcharge" runat="server" Width="165px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox><img
                                    onclick="getcharge('wo');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    border="0">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="plainlabel" align="right">
                                <a href="#" onclick="upeq();">Submit Changes</a>&nbsp;&nbsp;<a href="#" onclick="controlrow('d4a');">Skip
                                    This Step</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s4a" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="Img10" onclick="controlrow('d3');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img11" onclick="controlrow('d5');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #4:</b> Add Equipment Meters
                            </td>
                        </tr>
                    </table>
                </div>
                 <div id="d4a" class="details">
                    <table>
                        <tr>
                            <td>
                                <iframe id="ifmeter" runat="server" width="580" height="380" frameborder="0"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="plainlabel" align="right">
                                <a href="#" onclick="controlrow('d4');">Submit Changes</a>&nbsp;&nbsp;<a href="#"
                                    onclick="controlrow('d4');">Skip This Step</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s4" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row4" onclick="controlrow('d3');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img4" onclick="controlrow('d5');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #5:</b> Determine Your Equipment Criticality Rating
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d4" class="details">
                    <table>
                        <tr class="details">
                            <td>
                                <asp:TextBox ID="txtecr" runat="server" Width="48px" Font-Size="9pt" CssClass="plainlabel"></asp:TextBox>&nbsp;<img
                                    id="btnecr" title="Calculate ECR" onclick="getcalc();" height="20" alt="" src="../images/appbuttons/minibuttons/ecrbutton.gif"
                                    width="26" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe src="../sbhold.htm" id="ifecr" runat="server" width="720" height="510"
                                    frameborder="0"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="plainlabel" align="right">
                                <a href="#" onclick="controlrow('d5');" class="details">Submit Changes</a>&nbsp;&nbsp;<a href="#"
                                    onclick="controlrow('d5');">Go To Next Step</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s5" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row5" onclick="controlrow('d4');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img5" onclick="controlrow('d6');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #6:</b> Upload Equipment Images
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d5" class="details">
                    <table>
                        <tr>
                            <td>
                                <iframe id="ifimge" runat="server" width="580" height="380" frameborder="0"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="plainlabel" align="right">
                                <a href="#" onclick="controlrow('d6');">Go To Next Step</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s6" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row6" onclick="controlrow('d5');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img6" onclick="controlrow('d7');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #7:</b> Add Functional Assemblies
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d6" class="details">
                    <table>
                        <tr>
                            <td>
                                <iframe id="iffu" runat="server" width="620" height="620" frameborder="0"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="plainlabel" align="right">
                                <a href="#" onclick="controlrow('d7');">Go To Next Step</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s7" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row7" onclick="controlrow('d6');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img7" onclick="controlrow('d8');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #8:</b> Add Components
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d7" class="details">
                    <table>
                        <tr>
                            <td class="bluelabel" height="22">
                                Current Functions
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe id="ifful" runat="server" width="720" height="110" frameborder="0"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe src="../sbhold.htm" id="ifco" runat="server" width="720" height="430" frameborder="0">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel" height="22">
                                Current Components
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe src="../sbhold.htm" id="ifcolist" runat="server" width="720" height="110"
                                    frameborder="0"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="plainlabel" align="right">
                                <a href="#" onclick="controlrow('d8');">Go To Next Step</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s8" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row8" onclick="controlrow('d7');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img8" onclick="controlrow('d9');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #9:</b> Upload Any Existing PM Procedure Documentation
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d8" class="details">
                    <table>
                        <tr>
                            <td>
                                <iframe id="ifprocs" runat="server" width="580" height="380" frameborder="0"></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="plainlabel" align="right">
                                <a href="#" onclick="controlrow('d9');">Go To Next Step</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="s9" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row9" onclick="controlrow('d8');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                                <img id="Img9" onclick="controlrow('d10');" height="20" alt="" src="../images/appbuttons/optdown.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #10:</b> Enter PM Tasks
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="d9" class="details">
                    <table>
                        <tr>
                            <td colspan="2">
                            <table>
                            <tr>
                            <td class="plainlabel" height="22"><a href="#" onclick="getwt();">View Task Basics Walkthrough</a></td>
                            </tr>
                            <tr>
                            <td class="plainlabel" height="22"><a href="#" onclick="getsm();">Use Single Task Method</a></td>
                            </tr>
                            <tr>
                            <td class="plainlabel" height="22"><a href="#" onclick="getpmo();">Jump to PM Optimizer</a></td>
                            </tr>
                            <tr>
                            <td class="plainlabel" height="22"><a href="#" onclick="getdad();">Jump to Drag and Drop</a></td>
                            </tr>
                            <tr>
                            <td class="plainlabel" height="22"><a href="#" onclick="getgal();">Use PMO All-In-One Grid View</a></td>
                            </tr>
                            </table>
                            </td>
                        </tr>
                        <tr id="trwt" class="details">
                        <td>
                        <iframe src="pmo123taskbasics.aspx" id="ifwt" runat="server" width="740" height="560"
                                    frameborder="0"></iframe>
                        </td>
                        </tr>
                        <tr id="trsm" class="details">
                        <td>
                        <iframe src="pmo123tasks.aspx" id="ifsm" runat="server" width="740" height="560"
                                    frameborder="0"></iframe>
                        </td>
                        </tr>
                    </table>
                </div>
                <div id="s10" class="details">
                    <table>
                        <tr>
                            <td>
                                <img id="row10" onclick="controlrowa('d10a');" height="20" alt="" src="../images/appbuttons/optup.gif" />
                            </td>
                            <td class="hdr2" height="22">
                                <b>Step #11:</b> View Reports
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td width="370" valign="top">
                <table>
                    <tr>
                        <td class="thdrsing label" width="370" height="18">
                            Step Navigation
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="plainlabel box">
                                <table>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d1');">Step#1: Select a Location</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d2');">Step#2: Enter A New Equipment Number</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d3');">Step#3: Enter Any Equipment Details Required</a>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d4a');">Step#4: Add Equipment Meters</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d4');">Step#5: Determine Your Equipment Criticality
                                                Rating</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d5');">Step#6: Upload Equipment Images</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d6');">Step#7: Add Functional Assemblies</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d7');">Step#8: Add Components</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d8');">Step#9: Upload Any Existing PM Procedure Documentation</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d9');">Step#10: Enter PM Tasks</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            <a href="#" onclick="controlrow('d10');">Step#11: View Reports</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="thdrsing label" width="370" height="18">
                            Current Equipment Record Details
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <div class="plainlabel box" id="tdstats">
                            <table>
                            <tr>
                            <td class="plainlabelblue">Department: </td>
                            <td class="plainlabel" id="tddeptstat"></td>
                            </tr>
                            <tr>
                            <td class="plainlabelblue">Cell: </td>
                            <td class="plainlabel" id="tdcellstat"></td>
                            </tr>
                            <tr>
                            <td class="plainlabelblue">Location: </td>
                            <td class="plainlabel" id="tdlocstat"></td>
                            </tr>
                            <tr>
                            <td class="plainlabelblue">Equipment: </td>
                            <td class="plainlabel" id="tdeqstat"></td>
                            </tr>
                            </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lbllocid" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lbllocstat" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input type="hidden" id="lblustr" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblfcnt" runat="server" />
    <input type="hidden" id="lblccnt" runat="server" />
    <input type="hidden" id="lblftcnt" runat="server" />
    <input type="hidden" id="lblctcnt" runat="server" />
    <input type="hidden" id="lbletcnt" runat="server" />
    <uc1:mmenu1 ID="mmenu11" runat="server" />
    </form>
</body>
</html>
