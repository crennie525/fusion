﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123tabeqmain.aspx.vb"
    Inherits="lucy_r12.pmo123tabeqmain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function handlereturn() {
            var did = document.getElementById("lbldid").value;
            var dept = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cell = document.getElementById("lblcell").value;
            var eqid = document.getElementById("lbleqid").value;
            var eq = document.getElementById("lbleq").value;
            var lid = document.getElementById("lbllid").value;
            var loc = document.getElementById("lblloc").value;
            var typ = document.getElementById("lblrettyp").value;
            var ret = did + "~" + dept + "~" + clid + "~" + cell + "~" + eqid + "~" + eq + "~" + lid + "~" + loc + "~" + typ;
            window.parent.handlereturn(ret);
        }
        function handleswitch(val) {
            document.getElementById("lblapp").value = val;
        }
        function setref() {

        }
        function doref() {

        }

        function handlegeteq(href) {
            //alert(href + " handleeq")
            document.getElementById("ifeq").src = href;
        }

        function getminsrch() {
        var did = document.getElementById("lbldid").value;
                //alert(did)
        if (did != "") {
            retminsrch();
        }
        else {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lu";
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                var did = ret[0];
                document.getElementById("lbldchk").value = "yes";
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("tddept").innerHTML = ret[1];
                var clid = ret[2];
                document.getElementById("lblclid").value = ret[2];
                if (ret[2] != "") {
                    document.getElementById("lblchk").value = "yes";
                }
                else {
                    document.getElementById("lblchk").value = "yes";
                }
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("tdcell").innerHTML = ret[3];
                var eqid = ret[4];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                var fuid = ret[6];
                document.getElementById("lblfuid").value = ret[6];
                var coid = ret[8];
                document.getElementById("lblcoid").value = ret[8];
                document.getElementById("lblncid").value = ret[10];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("trdepts").className = "view";
                document.getElementById("lblrettyp").value = "depts";
                if (coid != "") {
                    document.getElementById("lbllvl").value = "co";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (fuid != "") {
                    document.getElementById("lbllvl").value = "fu";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (eqid != "") {
                    document.getElementById("lbllvl").value = "eq";
                    //alert(document.getElementById("lbllvl").value)
                    document.getElementById("lblgototasks").value = "1";
                }
                else {
                    document.getElementById("lblgototasks").value = "1";
                }
                if (did != "") {
                    document.getElementById("lbldchk").value = "yes";
                }
                if (clid != "") {
                    document.getElementById("lblchk").value = "yes";
                }
                else {
                    document.getElementById("lblchk").value = "no";
                }
                checkret();
            }
        }
    }
    function retminsrch() {
        //alert()
        var sid = document.getElementById("lblsid").value;
        var wo = "";
        var typ = "ret";
        var did = document.getElementById("lbldid").value;
        var dept = document.getElementById("lbldept").value;
        var clid = document.getElementById("lblclid").value;
        var cell = document.getElementById("lblcell").value;
        var eqid = document.getElementById("lbleqid").value;
        var eq = document.getElementById("lbleq").value;
        var fuid = document.getElementById("lblfuid").value;
        var fu = ""; // document.getElementById("lblfu").value;
        var coid = document.getElementById("lblcoid").value;
        var comp = ""; // document.getElementById("lblcomp").value;
        var lid = document.getElementById("lbllid").value;
        var loc = ""; // document.getElementById("lblloc").value;
        //if (cell == "" && who != "checkfu" && who != "checkeq") {
        who = "deptret";
        //}
        //alert(cell)
        var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&sid=" + sid + "&did=" + did + "&dept=" + dept + "&eqid=" + eqid + "&eq=" + eq + "&clid=" + clid + "&cell=" + cell + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
        if (eReturn) {
            clearall();
            //alert(eReturn)
            var ret = eReturn.split("~");
            var did = ret[0];
            document.getElementById("lbldchk").value = "yes";
            document.getElementById("lbldid").value = ret[0];
            document.getElementById("lbldept").value = ret[1];
            document.getElementById("tddept").innerHTML = ret[1];
            var clid = ret[2];
            document.getElementById("lblclid").value = ret[2];
            if (ret[2] != "") {
                document.getElementById("lblchk").value = "yes";
            }
            else {
                document.getElementById("lblchk").value = "yes";
            }
            document.getElementById("lblcell").value = ret[3];
            document.getElementById("tdcell").innerHTML = ret[3];
            //alert(ret[3])
            var eqid = ret[4];
            document.getElementById("lbleqid").value = ret[4];
            document.getElementById("lbleq").value = ret[5];
            var fuid = ret[6];
            document.getElementById("lblfuid").value = ret[6];
            var coid = ret[8];
            document.getElementById("lblcoid").value = ret[8];
            document.getElementById("lblncid").value = ret[10];
            document.getElementById("lbllid").value = ret[12];
            document.getElementById("trdepts").className = "view";
            document.getElementById("lblrettyp").value = "depts";
            if (coid != "") {
                document.getElementById("lbllvl").value = "co";
                document.getElementById("lblgototasks").value = "1";
            }
            else if (fuid != "") {
                document.getElementById("lbllvl").value = "fu";
                document.getElementById("lblgototasks").value = "1";
            }
            else if (eqid != "") {
                document.getElementById("lbllvl").value = "eq";
                //alert(document.getElementById("lbllvl").value)
                document.getElementById("lblgototasks").value = "1";
            }
            else {
                document.getElementById("lblgototasks").value = "1";
            }
            if (did != "") {
                document.getElementById("lbldchk").value = "yes";
            }
            if (clid != "") {
                document.getElementById("lblchk").value = "yes";
            }
            else {
                document.getElementById("lblchk").value = "no";
            }
            checkret();
        }
    }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = "lu"
            //alert(lid)
            if (lid != "") {
                typ = "retloc"
            }
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            //alert(fuid)
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                clearall();
                //alert(eReturn)
                var ret = eReturn.split("~");
                var lid = ret[0];
                document.getElementById("lbldchk").value = "no";
                document.getElementById("lbllid").value = ret[0];
                document.getElementById("lblloc").value = ret[1];
                //alert(ret[1])
                document.getElementById("tdloc2").innerHTML = ret[2];
                var eqid = ret[3];
                document.getElementById("lbleqid").value = ret[3];
                document.getElementById("lbleq").value = ret[3];
                var fuid = ret[5];
                document.getElementById("lblfuid").value = ret[5];
                var coid = ret[7];
                document.getElementById("lblcoid").value = ret[7];
                document.getElementById("trlocs").className = "view";
                document.getElementById("lblrettyp").value = "locs";
                if (coid != "") {
                    document.getElementById("lbllvl").value = "co";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (fuid != "") {
                    document.getElementById("lbllvl").value = "fu";
                    document.getElementById("lblgototasks").value = "1";
                }
                else if (eqid != "") {
                    document.getElementById("lbllvl").value = "eq";
                    document.getElementById("lblgototasks").value = "1";
                }
                else {
                    document.getElementById("lblgototasks").value = "1";
                }
                if (lid != "") {
                    document.getElementById("lbldchk").value = "no";
                }
                checkret();

            }
        }
        function clearall() {
            document.getElementById("deq").className = 'view';
            document.getElementById("dfu").className = 'details';
            document.getElementById("dco").className = 'details';
            document.getElementById("dot").className = 'details';
            //alert("1")
            handlegeteq("../equip/EQBotGrid.aspx?start=no&who=tab&typ=na&sid=na&cid=na&lid=na&did=na&clid=na");
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("tddept").innerHTML = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("tdcell").innerHTML = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lblfuid").value = "";
            document.getElementById("lblcoid").value = "";
            document.getElementById("lblncid").value = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("trdepts").className = "details";
            document.getElementById("trlocs").className = "details";
            document.getElementById("lblrettyp").value = "";
        }
        function checkret() {
            var lid = document.getElementById("lbllid").value;
            var did = document.getElementById("lbldid").value;
            if (did != "") {
                if (lid != "" && lid != "0") {
                    typ = "dloc"
                }
                else {
                    typ = "reg"
                }
            }
            else {
                if (lid != "" && lid != "0") {
                    typ = "loc"
                }
            }
            document.getElementById("lbltyp").value = typ;
            handlearch();
            document.getElementById("lblpar").value = "0"
            checkeq();
        }
        function handletask(tli) {
            var cid = document.getElementById("lblcid").value;
            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            var eqid = document.getElementById("lbleqid").value;
            var funid = document.getElementById("lblfuid").value;
            var comid;
            if (tli == "func") {
                comid = ""
            }
            else {
                comid = document.getElementById("lblcoid").value;
            }
            var lid = document.getElementById("lbllid").value;
            var typ = document.getElementById("lbltyp").value;
            var cell = document.getElementById("lblchk").value;
            var app = document.getElementById("lblapp").value;
            window.parent.handletask(cid, sid, did, clid, clid, eqid, funid, comid, lid, typ, lvl, cell, app, tli);
        }
        function gotoeq(eid) {
            var who = document.getElementById("lblwho").value;
            //alert(who)
            document.getElementById("lblgototasks").value = "0";
            document.getElementById("lbleqid").value = eid;
            document.getElementById("lblfuid").value = ""
            document.getElementById("lblcoid").value = "";
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;
            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            var cid = document.getElementById("lblcid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = document.getElementById("lbltyp").value;
            //alert(dchk)
            //alert(chk)
            if (dchk == "yes" && chk == "yes") {
                handlegeteq("../equip/EQBot.aspx?who=" + who + "&eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=" + clid + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ);
            }
            else if (dchk == "yes" && chk == "no") {
                //alert("../equip/EQBot.aspx?who=" + who + "&eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=no&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ)
                handlegeteq("../equip/EQBot.aspx?who=" + who + "&eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=no&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ);
            }
            else if (dchk == "no") {
                chk = "no"
                handlegeteq("../equip/EQBot.aspx?who=" + who + "&eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=no&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ);
            }
            document.getElementById("lbltab").value = "eq"
            closeall();
            document.getElementById("tdeq").className = "thdrhov plainlabel";
            document.getElementById("deq").style.display = 'block';
            document.getElementById("deq").style.visibility = 'visible';
            document.getElementById("lblapp").value = "pmopt";
            window.parent.pageScroll();
        }
        function gotofu(fid, eid) {
            //alert(fid)
            document.getElementById("lblfuid").value = fid;
            document.getElementById("lbleqid").value = eid;
            document.getElementById("lblcoid").value = "";
            document.getElementById("lblgototasks").value = "0";
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;
            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            var cid = document.getElementById("lblcid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = document.getElementById("lbltyp").value;
            var who = document.getElementById("lblwho").value;
            //alert(dchk + ", " + chk)
            if (dchk == "yes" && chk == "yes") {
                handlegeteq("../equip/EQBot.aspx?who=" + who + "&eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=" + clid + "&sid=" + sid + "&lid=" + lid + "&cid=" + cid + "&typ=" + typ)
            }
            else if (dchk == "yes" && chk == "no") {
                handlegeteq("../equip/EQBot.aspx?who=" + who + "&eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=no&sid=" + sid + "&lid=" + lid + "&cid=" + cid + "&typ=" + typ)
            }
            else if (dchk == "no") {
                //alert("2")
                handlegeteq("../equip/EQBotGrid.aspx?who=" + who + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&sid=" + sid + "&lid=" + lid + "&cid=" + cid + "&typ=" + typ)
            }
            document.getElementById("lbltab").value = "fu";
            closeall();
            var cd = document.getElementById("lblcid").value;
            document.getElementById("iffu").src = "../equip/FuncDiv.aspx?who=tab&fuid=" + fid + "&eqid=" + eid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid;
            document.getElementById("tdfu").className = "thdrhov plainlabel";
            document.getElementById("dfu").style.display = 'block';
            document.getElementById("dfu").style.visibility = 'visible';
            window.parent.pageScroll();
        }
        function gotoco(cid, fid, eid) {
            //alert()
            document.getElementById("lblfuid").value = fid;
            //document.getElementById("lbleqid").value = eid;
            document.getElementById("lblcoid").value = cid;
            document.getElementById("lblgototasks").value = "0";
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;
            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = document.getElementById("lbltyp").value;
            var who = document.getElementById("lblwho").value;
            //var cid = document.getElementById("lblcoid").value;		
            if (dchk == "yes" && chk == "yes") {
                //alert("EQBot.aspx?eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=" + clid + "&sid=" + sid + "&cid=" + cid)
                handlegeteq("../equip/EQBot.aspx?who=" + who + "&eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=" + clid + "&sid=" + sid + "&lid=" + lid + "&cid=" + cid + "&typ=" + typ)
            }
            else if (dchk == "yes" && chk == "no") {
                handlegeteq("../equip/EQBot.aspx?who=" + who + "&eqid=" + eid + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=no&sid=" + sid + "&lid=" + lid + "&cid=" + cid + "&typ=" + typ)
            }
            else if (dchk == "no") {
                //alert("3")
                handlegeteq("../equip/EQBotGrid.aspx?who=" + who + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&sid=" + sid + "&lid=" + lid + "&cid=" + cid + "&typ=" + typ)
            }
            document.getElementById("lbltab").value = "fu";
            closeall();
            var cd = document.getElementById("lblcid").value;
            document.getElementById("iffu").src = "../equip/FuncDiv.aspx?who=tab&fuid=" + fid + "&eqid=" + eid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid;
            document.getElementById("tdfu").className = "thdrhov plainlabel";
            document.getElementById("dfu").style.display = 'block';
            document.getElementById("dfu").style.visibility = 'visible';
            document.getElementById("lbltab").value = "co";
            closeall();
            //alert()
            document.getElementById("ifco").src = "../equip/CompDiv.aspx?who=tab&coid=" + cid + "&eqid=" + eid + "&fuid=" + fid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid;
            document.getElementById("tdco").className = "thdrhov plainlabel";
            document.getElementById("dco").style.display = 'block';
            document.getElementById("dco").style.visibility = 'visible';
            window.parent.pageScroll();
        }
        function handlearch() {
            //alert()
            chk = document.getElementById("lblchk").value;
            var did = document.getElementById("lbldid").value;
            var clid = document.getElementById("lblclid").value;
            var lid = document.getElementById("lbllid").value;
            var sid = document.getElementById("lblsid").value;
            var typ = document.getElementById("lbltyp").value;
            var eqid = document.getElementById("lbleqid").value;
            var fuid = document.getElementById("lblfuid").value;
            var coid = document.getElementById("lblcoid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var ustr = document.getElementById("lbluser").value;
            window.parent.handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum);
            document.getElementById("ifarch").src = "../equip/DeptArch.aspx?start=yes&did=" + did + "&clid=" + clid + "&chk=" + chk + "&lid=" + lid + "&typ=" + typ + "&sid=" + sid + "&ustr=" + ustr;
        }
        function handleeq(par, chk, val, eqnum, chld) {
        alert("hey!")
            if (par == "eq") {
                document.getElementById("lbleqid").value = val;
                document.getElementById("lbleqnum").value = eqnum;
                document.getElementById("lblchld").value = chld;
                if (chk == "add") {
                    document.getElementById("lblfuid").value = "";
                    document.getElementById("lblcoid").value = "";
                }
                handlearch()
            }
            if (par == "func") {
                document.getElementById("lblfuid").value = val;
            }
            if (par == "comp") {
                document.getElementById("lblcoid").value = val;
            }
            if (par == "dept") {
                document.getElementById("lbldid").value = val;
                document.getElementById("lblchk").value = chk;
            }
            else if (par == "cell") {
                document.getElementById("lblclid").value = val;
                document.getElementById("lblchk").value = chk;
            }
            else if (par == "loc") {
                document.getElementById("lbllid").value = val;
                document.getElementById("lblchk").value = chk;
            }
            handlearch();
        }
        function checkeq() {
            var log = document.getElementById("lbllog").value;
            if (log == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
            var app = document.getElementById("appchk").value;
            if (app == "switch") window.parent.handleapp(app);
            var lhld = document.getElementById("lbllochold").value;
            if (lhld != "") {
                document.getElementById("lblloc").innerHTML = lhld;
            }
            var gototasks = "1"; //document.getElementById("lblgototasks").value;
            var chk = document.getElementById("lblpar").value;
            var chk2 = document.getElementById("lblchk").value;
            var valu = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var lid = document.getElementById("lbllid").value;
            var lvl = document.getElementById("lbllvl").value;
            var par = document.getElementById("lblpar").value;
            //alert(chk)
            if (chk == "dept") {
                valu = document.getElementById("lbldid").value;
                handleeq(par, chk2, valu);
            }
            else if (chk == "cell") {
                valu = document.getElementById("lblclid").value;
                handleeq(par, chk2, valu);
            }
            else if (chk == "loc") {
                valu = document.getElementById("lbllid").value;
                handleeq(par, "no", valu);
            }
            //alert(gototasks)
            if (gototasks == "1") {
                document.getElementById("lblgototasks").value = "0";
                var chk = document.getElementById("lblchk").value;
                var dchk = document.getElementById("lbldchk").value;
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var clid = document.getElementById("lblclid").value;
                var cid = document.getElementById("lblcid").value;
                var lvl = document.getElementById("lbllvl").value;
                var eqid = document.getElementById("lbleqid").value;
                var fuid = document.getElementById("lblfuid").value;
                var coid = document.getElementById("lblcoid").value;
                var lid = document.getElementById("lbllid").value;
                var typ = document.getElementById("lbltyp").value;
                var start = document.getElementById("lblstart").value;
                var loc = document.getElementById("lblloc").value;
                //alert(lvl)
                if (lvl == "eq") {
                    gotoeq(eqid);
                }
                else if (lvl == "fu") {
                    gotofu(fuid, eqid);
                }
                else if (lvl == "co") {
                    gotoco(coid, fuid, eqid);
                }
                else {
                    if (dchk == "yes" && chk == "yes") {
                        //alert("4")
                        handlegeteq("../equip/EQBotGrid.aspx?who=tab&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=" + clid + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc)
                    }
                    else if (dchk == "yes" && chk == "no") {
                        //alert("5")
                        handlegeteq("../equip/EQBotGrid.aspx?who=tab&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=no&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc)
                    }
                    else if (dchk == "no") {
                        //alert("6")
                        handlegeteq("../equip/EQBotGrid.aspx?who=tab&start=yes&dchk=" + dchk + "&chk=" + chk + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc)
                    }
                    else {
                        //alert("7")
                        handlegeteq("../equip/EQBotGrid.aspx?who=tab&start=no&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=" + clid + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc)
                    }
                }
            }
            else {
                //alert("8")
                handlegeteq("../equip/EQBotGrid.aspx?start=no&who=tab&typ=na&sid=na&cid=na&lid=na&did=na&clid=na&loc=");
            }
        }
        function CheckTask(type) {
            var ustr = document.getElementById("lbluser").value;
            //alert(ustr)
            var cid = document.getElementById("lblcid").value;
            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;
            var coid = document.getElementById("lblcoid").value;
            var clid = document.getElementById("lblclid").value;
            var lid = document.getElementById("lbllid").value;
            var eqid = document.getElementById("lbleqid").value;
            //alert(eqid)
            var eq = document.getElementById("lbleq").value;
            var fuid = document.getElementById("lblfuid").value;
            if (did == "") {
                document.getElementById("lbldchk").value = "no";
                dchk = "no";
            }
            if (type == "eqfilt") {
                if (dchk == "yes") {
                    if (chk == "yes") {
                        clid = document.getElementById("lblclid").value;
                        var href = "../equip/EqGrid.aspx?who=tab&dchk=yes&chk=yes&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid + "&ustr=" + ustr + "&eqid=" + eqid + "&eq=" + eq + "&dept=&cell=&loc=";
                        window.parent.handlelocation(href);
                    }
                    else if (chk == "no") {
                        clid = document.getElementById("lblclid").value;
                        var href = "../equip/EqGrid.aspx?who=tab&dchk=yes&chk=yes&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid + "&ustr=" + ustr + "&eqid=" + eqid + "&eq=" + eq + "&dept=&cell=&loc=";
                        window.parent.handlelocation(href);
                    }
                    else {
                        alert("No Department or Cell Selected")
                    }
                }
                else if (dchk == "no") {
                    var href = "../equip/EqGrid.aspx?who=tab&dchk=no&chk=nn&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid + "&ustr=" + ustr + "&eqid=" + eqid + "&eq=" + eq + "&dept=&cell=&loc=";
                    window.parent.handlelocation(href);
                }
                else {
                    alert("No Department or Location Selected")
                }
            }
            else if (type == "eqall") {
                window.location = "../equip/EqGridAll.aspx?cid=" + cid + "&ustr=" + ustr;
            }
            else if (type == "fufilt") {
                var cid = document.getElementById("lblcid").value;
                //eqid = document.getElementById("lbleqid").value;
                //alert(eqid)
                var e = eqid.length
                if (e != 0) {
                    if (dchk == "yes") {
                        if (chk == "yes") {
                            var href = "../equip/FuncGrid.aspx?who=tab&dchk=yes&chk=yes&cid=" + cid + "&eqid=" + eqid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid + "&ustr=" + ustr + "&eq=" + eq + "&dept=&cell=&loc=&fuid=" + fuid;
                            window.parent.handlelocation(href);
                        }
                        else if (chk == "no") {
                            var href = "../equip/FuncGrid.aspx?who=tab&dchk=yes&chk=yes&cid=" + cid + "&eqid=" + eqid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid + "&ustr=" + ustr + "&eq=" + eq + "&dept=&cell=&loc=&fuid=" + fuid;
                            window.parent.handlelocation(href);
                        }
                    }
                    else if (dchk == "no") {
                        var href = "../equip/FuncGrid.aspx?who=tab&dchk=no&chk=nn&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid + "&eqid=" + eqid + "&ustr=" + ustr + "&eq=" + eq + "&dept=&cell=&loc=&fuid=" + fuid;
                        window.parent.handlelocation(href);
                    }
                }
                else {
                    alert("No Equipment Record Selected")
                }
            }
            else if (type == "cofilt") {
                var cid = document.getElementById("lblcid").value;
                //eqid = document.getElementById("lbleqid").value;
                var coid = document.getElementById("lblcoid").value;

                var e = fuid.length
                if (e != 0) {
                    var href = "../equip/CompGrid.aspx?who=tab&dchk=yes&chk=yes&cid=" + cid + "&eqid=" + eqid + "&fuid=" + fuid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid + "&ustr=" + ustr + "&eq=" + eq + "&dept=&cell=&loc=&coid=" + coid;
                    window.parent.handlelocation(href);
                }
                else {
                    alert("No Function Record Selected")
                }
            }

        }
        function gettab(id) {
            var chld = document.getElementById("lblchld").value;
            alert(chld)
            if (id == "tdeq") {
                var cid = document.getElementById("lblcid").value;
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var clid = document.getElementById("lblclid").value;
                
                document.getElementById("lbltab").value = "eq"
                document.getElementById("lbllvl").value = "eq"
                closeall();
                document.getElementById("tdeq").className = "thdrhov plainlabel";
                document.getElementById("deq").style.display = 'block';
                document.getElementById("deq").style.visibility = 'visible';
                document.getElementById("lblapp").value = "pmdev";
            }
            else if (id == "tdot") {
                document.getElementById("lbltab").value = "ot"
                closeall();
                document.getElementById("tdot").className = "thdrhov plainlabel";
                document.getElementById("dot").style.display = 'block';
                document.getElementById("dot").style.visibility = 'visible';
                document.getElementById("lblapp").value = "pmdev";
                var chk = document.getElementById("lblchk").value;
                var dchk = document.getElementById("lbldchk").value;
                var cid = document.getElementById("lblcid").value;
                var sid = document.getElementById("lblsid").value;
                var did = document.getElementById("lbldid").value;
                var clid = document.getElementById("lblclid").value;
                var lid = document.getElementById("lbllid").value;
                document.getElementById("ifot").src = "../equip/NCGrid.aspx?&dchk=" + dchk + "&chk=" + chk + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid;
            }
            else if (id == "tdfu") {
                var fuid = document.getElementById("lblfuid").value;
                var eqid = document.getElementById("lbleqid").value;
                if (eqid != "") {
                    var cid = document.getElementById("lblcid").value;
                    var sid = document.getElementById("lblsid").value;
                    var did = document.getElementById("lbldid").value;
                    var clid = document.getElementById("lblclid").value;
                    document.getElementById("lbltab").value = "fu";
                    document.getElementById("lbllvl").value = "fu"
                    closeall();
                    var cd = document.getElementById("lblcid").value;
                    document.getElementById("iffu").src = "../equip/FuncDivGrid.aspx?who=tab&eqid=" + eqid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&chld=" + chld;
                    document.getElementById("tdfu").className = "thdrhov plainlabel";
                    document.getElementById("dfu").style.display = 'block';
                    document.getElementById("dfu").style.visibility = 'visible';
                }
                else {
                    alert("No Equipment Record Selected!");
                }
            }
            else if (id == "tdco") {
                var fuid = document.getElementById("lblfuid").value;
                if (fuid != "") {
                    var cid = document.getElementById("lblcid").value;
                    var sid = document.getElementById("lblsid").value;
                    var did = document.getElementById("lbldid").value;
                    var clid = document.getElementById("lblclid").value;
                    var eqid = document.getElementById("lbleqid").value;
                    document.getElementById("lbltab").value = "co";
                    document.getElementById("lbllvl").value = "co"
                    closeall();
                    var cd = document.getElementById("lblcid").value;
                    document.getElementById("ifco").src = "../equip/CompDivGrid.aspx?who=tab&eqid=" + eqid + "&fuid=" + fuid + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid;
                    document.getElementById("tdco").className = "thdrhov plainlabel";
                    document.getElementById("dco").style.display = 'block';
                    document.getElementById("dco").style.visibility = 'visible';
                }
                else alert("No Function Record Selected!");
            }
        }
        function closeall() {
            document.getElementById("tdeq").className = "thdr plainlabel";
            document.getElementById("tdfu").className = "thdr plainlabel";
            document.getElementById("tdco").className = "thdr plainlabel";
            //document.getElementById("tdot").className = "thdr plainlabel";
            document.getElementById("deq").style.display = 'none';
            document.getElementById("deq").style.visibility = 'hidden';
            document.getElementById("dfu").style.display = 'none';
            document.getElementById("dfu").style.visibility = 'hidden';
            document.getElementById("dco").style.display = 'none';
            document.getElementById("dco").style.visibility = 'hidden';
            document.getElementById("dot").style.display = 'none';
            document.getElementById("dot").style.visibility = 'hidden';
        }

    </script>
</head>
<body onload="checkeq();">
    <form id="form1" runat="server">
    <div style="position: absolute; top: 0px; left: 0px">
       <table>
      <tr id="trreturn" runat="server" class="details">
                <td colspan="3">
                    <table>
                        <tr>
                            <td class="plainlabel" align="right" width="790">
                                <a href="#" onclick="handlereturn();">Return</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr id="trpick" runat="server">
                <td colspan="2">
                    <table>
                        <tr>
                            <td id="tddepts" class="bluelabel" runat="server">
                                Use Departments
                            </td>
                            <td>
                                <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td id="tdlocs1" class="bluelabel" runat="server">
                                Use Locations
                            </td>
                            <td>
                                <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td class="plainlabel" align="right" width="540" id="tdreturn" runat="server">
                                <a href="#" onclick="handlereturn();">Return</a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td rowspan="5" valign="top" id="tdarch" runat="server">
                    <table cellspacing="0" cellpadding="0" width="260">
                        <tr>
                            <td class="thdrsinglft" align="left" width="26" height="20">
                                <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                            </td>
                            <td class="thdrsingrt label" align="left" width="234">
                                <asp:Label ID="lang2284" runat="server">Asset Hierarchy</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <iframe id="ifarch" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                                    background-color: transparent; margin: 0px; padding-left: 0px; padding-right: 0px;
                                    border-top-style: none; border-left-style: none; padding-top: 0px" src="../equip/DeptArch.aspx?start=no"
                                    frameborder="no" width="260" scrolling="yes" height="380" runat="server" allowtransparency>
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="thdrsinglft" width="26">
                                <img src="../images/appbuttons/minibuttons/gridsmmenu.gif" border="0">
                            </td>
                            <td class="thdrsingrt label">
                                <asp:Label ID="lang2285" runat="server">Grid Views</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="btmmenu plainlabel hand" onclick="CheckTask('eqfilt');" colspan="2" height="22">
                                <asp:Label ID="lang2286" runat="server">Equipment Grid</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="btmmenu plainlabel hand" onclick="CheckTask('fufilt');" colspan="2" height="22">
                                <asp:Label ID="lang2287" runat="server">Function Grid</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="btmmenu plainlabel hand" onclick="CheckTask('cofilt');" colspan="2" height="22">
                                <asp:Label ID="lang2288" runat="server">Component Grid</asp:Label>
                            </td>
                        </tr>
                       
                    </table>
                </td>
            </tr>
            <tr id="trdepts" class="details" runat="server">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="label" width="110">
                                Department
                            </td>
                            <td id="tddept" class="plainlabel" width="170" runat="server">
                            </td>
                            <td width="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                Station\Cell
                            </td>
                            <td id="tdcell" class="plainlabel" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trlocs" class="details" runat="server">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="label">
                                Location
                            </td>
                            <td id="tdloc2" class="plainlabel" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td valign="top" colspan="2" height="20">
                    <table cellpadding="0" width="770" border="0">
                        <tr>
                            <td class="thdrhov plainlabel" id="tdeq" onclick="gettab('tdeq');" width="140" height="18">
                                <asp:Label ID="lang2293" runat="server">Equipment</asp:Label>
                            </td>
                            <td class="thdr plainlabel" id="tdfu" onclick="gettab('tdfu');" width="140">
                                <asp:Label ID="lang2294" runat="server">Functions</asp:Label>
                            </td>
                            <td class="thdr plainlabel" id="tdco" onclick="gettab('tdco');" width="140">
                                <asp:Label ID="lang2295" runat="server">Components</asp:Label>
                            </td>
                            <td class="details" id="tdot" onclick="gettab('tdot');" width="140">
                                <asp:Label ID="lang2296" runat="server">Misc. Assets</asp:Label>
                            </td>
                            <td width="182">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <div id="deq" style="z-index: 1; border-bottom: 2px groove; border-left: 1px groove;
                        border-top: 1px groove; border-right: 2px groove">
                        <iframe id="ifeq" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                            border-top-style: none; border-left-style: none" src="#" frameborder="0" width="760"
                            height="415" runat="server" allowtransparency></iframe>
                    </div>
                    <div class="details" id="dfu" style="z-index: 1; border-bottom: 2px groove; border-left: 1px groove;
                        border-top: 1px groove; border-right: 2px groove">
                        <iframe id="iffu" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                            border-top-style: none; border-left-style: none" src="#" frameborder="0" width="760"
                            height="415" runat="server" allowtransparency scrolling="no"></iframe>
                    </div>
                    <div class="details" id="dco" style="z-index: 1; border-bottom: 2px groove; border-left: 1px groove;
                        border-top: 1px groove; border-right: 2px groove">
                        <iframe id="ifco" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                            border-top-style: none; border-left-style: none" src="#" frameborder="0" width="760"
                            height="415" runat="server" allowtransparency></iframe>
                    </div>
                    <div class="details" id="dot" style="z-index: 1; border-bottom: 2px groove; border-left: 1px groove;
                        border-top: 1px groove; border-right: 2px groove">
                        <iframe id="ifot" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                            border-top-style: none; border-left-style: none" src="#" frameborder="0" width="760"
                            height="415" runat="server" allowtransparency></iframe>
                    </div>
                </td>
            </tr>
     </table>
    </div>
    <input id="lblcid" type="hidden" value="0" name="lblcid" runat="server" />
    <input id="lbltab" type="hidden" name="lbltab" runat="server" />
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server" /><input id="lblsid"
        type="hidden" name="lblsid" runat="server" />
    <input id="lbldid" type="hidden" name="lbldid" runat="server" /><input id="lblclid"
        type="hidden" name="lblclid" runat="server" />
    <input id="lblret" type="hidden" name="lblret" runat="server" />
    <input id="lblchk" type="hidden" name="lblchk" runat="server" />
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server" /><input id="lblfuid"
        type="hidden" name="lblfuid" runat="server" />
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server" />
    <input id="lblapp" type="hidden" runat="server" name="lblapp" />
    <input id="lblpar2" type="hidden" value="0" name="lblpar2" runat="server" /><input
        id="lblpar" type="hidden" value="0" name="lblpar" runat="server" />
    <input id="lblgototasks" type="hidden" name="lblgototasks" runat="server" /><input
        id="appchk" type="hidden" name="appchk" runat="server" />
    <input id="lbllvl" type="hidden" runat="server" name="lbllvl" />
    <input id="lbllid" type="hidden" runat="server" name="lbllid" />
    <input id="lblsubmit" type="hidden" runat="server" name="lblsubmit" /><input id="lbltyp"
        type="hidden" runat="server" name="lbltyp" />
    <input id="lblstart" type="hidden" runat="server" name="lblstart" /><input id="lbllochold"
        type="hidden" runat="server" name="lbllochold" />
    <input type="hidden" id="lbllog" runat="server" name="lbllog" />
    <input type="hidden" id="lblfslang" runat="server" name="lblfslang" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblcell" runat="server" /><input id="lblrettyp" type="hidden"
        runat="server" name="lblrettyp" />
    <input type="hidden" id="lblncid" runat="server" /><input type="hidden" id="lblloc"
        runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lblchld" runat="server" />
    </form>
</body>
</html>
