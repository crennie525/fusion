﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmo123func.aspx.vb" Inherits="lucy_r12.pmo123func" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function modstring(who) {
            who = who.replace("#", "");
            who = who.replace("'", "`");
            return who;
        }
        function addfunc() {
            var func = document.getElementById("txtfunc").value;
            var spl = document.getElementById("txtspl").value;
            func = modstring(func);
            spl = modstring(spl);
            if (func.length > 100) {
                alert("Function Name Limited to 100 Characters")
            }
            else if (spl.length > 100) {
                alert("Special Identifier Limited to 100 Characters")
            }
            else {
                document.getElementById("lblsubmit").value = "addfunc";
                document.getElementById("form1").submit();
            }
        }
        function getpics(fid, func) {
            //"../equip/equploadimage.aspx?typ=eq&funcid=&comid=&eqid=&ro=0&date=" +
            document.getElementById("tdcurr").innerHTML = func;
            document.getElementById("ifimge").src = "../equip/equploadimage.aspx?typ=fu&funcid=" + fid + "&comid=&eqid=&ro=0&date=" + Date();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td width="200"></td>
    <td width="220"></td>
    <td width="220"></td>
    </tr>
    <tr>
    <td class="bluelabel">Enter New Function Name</td>
    <td><asp:TextBox ID="txtfunc" runat="server" Width="220"></asp:TextBox></td>
     
    </tr>
    <tr>
    <td class="bluelabel">and Special Identifier if Required</td> 
      <td><asp:TextBox ID="txtspl" runat="server" Width="220"></asp:TextBox></td>  
    <td><img src="../images/appbuttons/minibuttons/addmod.gif" alt="" onclick="addfunc();" /></td>
    </tr>
    <tr>
    <td colspan="3">
    <div id="divfu" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 600px; HEIGHT: 118px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
    </td>
    </tr>
    <tr>
    <td colspan="3" align="center" class="plainlabelblue" id="tdcurr"></td>
    </tr>
     <tr>
            <td colspan="3">
            <iframe id="ifimge" runat="server" width="580" height="380" frameborder="0"></iframe>
            </td>
            </tr>
    </table>
    </div>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    </form>
</body>
</html>
