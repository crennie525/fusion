﻿Imports System.Data.SqlClient
Imports System.Text
Public Class pmo123meter
    Inherits System.Web.UI.Page
    Dim cm As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim meterid, meter, eqid, eqnum, retmeterid, retmeter, retunit, retwkuse, pmtskid, mfid As String
    Dim mtr, mtrid, uni, wku As String
    Dim skillid, skillqty, fuid, rdid As String
    Dim typ, skillo, skillido, skillqtyo, rdo, rdido, meterido, mfido, metero As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            cm.Open()
            GetMeters(eqid)
            cm.Dispose()

        End If
    End Sub
    Private Sub GetMeters(ByVal eqid As String)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 330px;  HEIGHT: 300px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table>")
        sb.Append("<tr>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">Meter Id</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""100"">Units</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">Weekly Use</td>")
        sb.Append("</tr>")
        sql = "select * from meters where eqid = '" & eqid & "'"
        dr = cm.GetRdrData(sql)
        While dr.Read
            mtrid = dr.Item("meterid").ToString
            mtr = dr.Item("meter").ToString
            uni = dr.Item("unit").ToString
            wku = dr.Item("wkuse").ToString

            sb.Append("<tr>")
            sb.Append("<td class=""plainlabel"" width=""120""><a href=""#"" onclick=""getmeter('" & mtrid & "','" & mtr & "','" & uni & "','" & wku & "');"">" & mtr & "</a></td>")
            sb.Append("<td class=""plainlabel"" width=""100"">" & uni & "</td>")
            sb.Append("<td class=""plainlabel"" width=""90"">" & wku & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table></div>")
        tdmeters.InnerHtml = sb.ToString
    End Sub
End Class