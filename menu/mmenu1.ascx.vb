

'********************************************************
'*
'********************************************************



Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Web
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Public Class mmenu1
    Inherits System.Web.UI.UserControl
    Dim tmod As New transmod
    Dim mutils As New mmenu_utils_a
    Dim app As New AppUtils
    Dim usite As New Utilities
    Dim dr As SqlDataReader
    Private ptst As String
    Private cpg, cimg, psite, usrname, uid, df, df_hold, hdr, mbg, mb, hopt, lpg, sess, sql, usites, pmadmin, npd As String
    Dim sessid, comp, compname, userlevel, cadm, grpndx, optsort, ms, Logged_In, ua, apps, email, curlang As String
    Dim appname, appstr, practice, ro, rostr, apprgrp, islabor, isplanner, issuper, userid As String
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdsid As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblnpb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompname As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserlevel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgrpndx As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusrname As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpsite As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblms As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblLogged_In As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblua As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapps As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblemail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmadmin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapprgrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhdr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmbg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblissuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblislabor As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisplanner As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloptsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpractice As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrostr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurlang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsessid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents menulit As System.Web.UI.WebControls.Literal

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim lang As String
        Try
            lang = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            lang = mutils.AppDfltLang
        End Try

        ptst = mutils.AppVer '"1"
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ThisPage As String = Request.ServerVariables("URL")

        Dim cimgfile As String
        Dim cimglst As Integer = ThisPage.LastIndexOf("/")
        If cimglst <> -1 Then
            cimgfile = ThisPage.Substring(cimglst + 1)
        End If
        If Len(cimgfile) = 0 Then
            cimgfile = "~"
        End If
        Dim ThisQS As String = Request.QueryString.ToString

        appname = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        hdr = "4" 'HttpContext.Current.Session("hdr").ToString() '"2"
        'appstr = HttpContext.Current.Session("appstr").ToString()
        'psite = HttpContext.Current.Session("psite").ToString() '"Practice Site" '
        'pmadmin = HttpContext.Current.Session("pmadmin").ToString()
        'usrname = HttpContext.Current.Session("username").ToString() '"PM Admin" '
        'uid = HttpContext.Current.Session("uid").ToString() '"pmadmin1" '
        'df = HttpContext.Current.Session("dfltps").ToString() '"12" '
        'mbg = HttpContext.Current.Session("mbg").ToString() '"1"
        'mb = HttpContext.Current.Session("mb").ToString() '"2"



        If Not IsPostBack Then
            getsess()
            npd = "no"
            If mb = "2" Then
                hopt = "3"
            Else
                hopt = hdr
            End If
            If hdr = "0" Then
                hdr = ""
            End If
            If df_hold <> df Then
                df_hold = df
            End If
            Select Case ThisPage '

                Case "/" & appname & "/reporting/ReportList.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/rep.gif"
                    End If

                    cpg = "../reporting/ReportList.aspx"""
                    lpg = "Izenda Reports"
                Case "/reporting/ReportList.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/rep.gif"
                    End If

                    cpg = "../reporting/ReportList.aspx"
                    lpg = "Izenda Reports"

                Case "/reporting/ReportDesigner.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/rep.gif"
                    End If

                    cpg = "../reporting/ReportDesigner.aspx"
                    lpg = "Izenda Reports"

                Case "/reporting/Settings.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/rep.gif"
                    End If

                    cpg = "../reporting/Settings.aspx"
                    lpg = "Izenda Reports"

                Case "/" & appname & "/appspmo123/pmo123e1.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../appspmo123/pmo123e1.aspx"
                    lpg = "PMO Scrolled"

                Case "/" & appname & "/appsman/pmpsmodmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../appsman/pmpsmodmain.aspx"
                    lpg = "Pass/Fail Thresholds"

                Case "/" & appname & "/appspmo123/pmo123equip.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../appspmo123/pmo123equip.aspx"
                    lpg = "PMO Stepped"

                Case "/" & appname & "/appspmo123tab/pmo123tabmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/pmotabbed.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/pmotabbed.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/pmotabbed.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/pmotabbed.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/pmotabbed.gif"
                    End If

                    cpg = "../appspmo123tab/pmo123tabmain.aspx"
                    lpg = "PMO Tabbed"

                Case "/" & appname & "/dials/dialmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/dbpm.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/dbpm.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/dbpm.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/dbpm.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/dbpm.gif"
                    End If
                    cpg = "../dials/dialmain.aspx"
                    lpg = "Demand Based PM"

                Case "/" & appname & "/equip/meterroutesmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/metermenu.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/metermenu.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/metermenu.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/metermenu.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/metermenu.gif"
                    End If
                    cpg = "../equip/meterroutesmain.aspx"
                    lpg = "Meters"
                Case "/" & appname & "/complib/complib.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/complib.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/complib.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/complib.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/complib.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/complib.gif"
                    End If
                    cpg = "../complib/complibmain.aspx"
                    lpg = "Component Library"
                Case "/" & appname & "/appsopttpm/tpmopttasksmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/tpmopt.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/tpmopt.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/tpmopt.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/tpmopt.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/tpmopt.gif"
                    End If

                    cpg = "../appsopttpm/tpmopttasksmain.aspx"
                    lpg = "TPM Optimizer"

                Case "/" & appname & "/appstpm/tpmtasksmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/tpmdev.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/tpmdev.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/tpmdev.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/tpmdev.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/tpmdev.gif"
                    End If

                    cpg = "../appstpm/tpmtasksmain.aspx"
                    lpg = "TPM Developer"

                Case "/" & appname & "/appsarch/pmarchmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/arch.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/arch.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/arch.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/arch.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/arch.gif"
                    End If

                    cpg = "../appsarch/pmarchmain.aspx"
                    lpg = "PM Archive"

                Case "/" & appname & "/appsarchtpm/pmarchmaintpm.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/archt.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/archt.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/archt.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/archt.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/archt.gif"
                    End If

                    cpg = "../appsarchtpm/pmarchmaintpm.aspx"
                    lpg = "TPM Archive"

                Case "/" & appname & "/equip/eqmain2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/eqa.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/eqa.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/eqa.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/eqa.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/eqa.gif"
                    End If

                    cpg = "../equip/eqmain2.aspx"
                    lpg = "Equipment"

                Case "/" & appname & "/equip/ECRAdm.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/ecr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/ecr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/ecr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/ecr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/ecr.gif"
                    End If

                    cpg = "../equip/ECRAdm.aspx"
                    lpg = "ECR Administration"

                Case "/" & appname & "/inv/invmainmenu.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/inv.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/inv.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/inv.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/inv.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/inv.gif"
                    End If

                    cpg = "../inv/invmainmenu.aspx"
                    lpg = "Inventory"

                Case "/" & appname & "/inv/invmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/inv.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/inv.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/inv.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/inv.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/inv.gif"
                    End If

                    cpg = "../inv/invmainmenu.aspx"
                    lpg = "Inventory"

                Case "/" & appname & "/apps/PMTasks.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/dev1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/dev1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/dev1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/dev1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/dev1.gif"
                    End If

                    cpg = "../apps/PMTasks.aspx?jump=no"
                    lpg = "PM Developer"

                Case "/" & appname & "/admin/Appset.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/appset.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/appset.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/appset.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/appset.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/appset.gif"
                    End If

                    cpg = "../admin/Appset.aspx"
                    lpg = "Application Set-up"


                Case "/" & appname & "/asmstr/Assessment_Main.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/asm.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/asm.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/asm.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/asm.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/asm.gif"
                    End If

                    cpg = "../asmstr/Assessment_Main.aspx?atyp=u"
                    lpg = "Assessment Master"

                Case "/" & appname & "/asmstr/Assessment_Main2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/asa.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/asa.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/asa.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/asa.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/asa.gif"
                    End If

                    cpg = "../asmstr/Assessment_Main2.aspx?atyp=a"
                    lpg = "Assessment Admin"


                Case "/" & appname & "/equip/eqcopymain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/lib.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/lib.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/lib.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/lib.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/lib.gif"
                    End If

                    cpg = "../equip/eqcopymain.aspx"
                    lpg = "PM Library"

                Case "/" & appname & "/appsopt/PM3OptMain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../appsopt/PM3OptMain.aspx"
                    lpg = "PM Optimizer"

                Case "/" & appname & "/appsrt/PMRoutesMain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/route.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/route.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/route.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/route.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/route.gif"
                    End If

                    cpg = "../appsrt/PMRoutesMain.aspx"
                    lpg = "PM Routes Main Page"

                Case "/" & appname & "/appsrt/PMRoutes2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/routedev.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/routedev.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/routedev.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/routedev.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/routedev.gif"
                    End If

                    cpg = "../appsrt/PMRoutes2.aspx"
                    lpg = "PM Routes"

                Case "/" & appname & "/appsman/PMManager.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/man.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/man.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/man.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/man.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/man.gif"
                    End If

                    cpg = "../appsman/PMManager.aspx"
                    lpg = "PM Manager"


                Case "/" & appname & "/appsmax/pmjpmanager.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/man.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/man.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/man.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/man.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/man.gif"
                    End If

                    cpg = "../appsmax/pmjpmanager.aspx"
                    lpg = "PM Job Plan Manager"

                Case "/" & appname & "/appsmantpm/TPMManager.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/tman.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/tman.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/tman.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/tman.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/tman.gif"
                    End If

                    cpg = "../appsmantpm/TPMManager.aspx"
                    lpg = "TPM Manager"

                Case "/" & appname & "/appswo/wolabmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/workman.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/workman.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/workman.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/workman.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/workman.gif"
                    End If

                    cpg = "../appswo/wolabmain.aspx"
                    lpg = "Work Manager"

                Case "/" & appname & "/appswo/wolist1_fs.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/workman.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/workman.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/workman.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/workman.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/workman.gif"
                    End If

                    cpg = "../appswo/wolist1_fs.aspx"
                    lpg = "Work Manager - Full Screen"

                Case "/" & appname & "/appswo/wojpeditmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/jobplan.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/jobplan.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/jobplan.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/jobplan.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/jobplan.gif"
                    End If

                    cpg = "../appswo/wojpeditmain.aspx"
                    lpg = "WO Job Plan"

                Case "/" & appname & "/appsrt/JobPlanMain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/jobplan.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/jobplan.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/jobplan.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/jobplan.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/jobplan.gif"
                    End If

                    cpg = "../appsrt/JobPlanMain.aspx"
                    lpg = "Job Plan Main"

                Case "/" & appname & "/appsrt/jobplanmain2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/jobplandev.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/jobplandev.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/jobplandev.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/jobplandev.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/jobplandev.gif"
                    End If

                    cpg = "../appsrt/jobplanmain2.aspx"
                    lpg = "Job Plan"



                Case "/" & appname & "/security/useradminmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/usr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/usr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/usr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/usr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/usr.gif"
                    End If

                    cpg = "../security/useradminmain.aspx"
                    lpg = "User Administration"

                Case "/" & appname & "/locs/pmlocmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/loc.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/loc.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/loc.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/loc.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/loc.gif"
                    End If

                    cpg = "../locs/pmlocmain.aspx"
                    lpg = "Locations"

                Case "/" & appname & "/mainmenu/NewMainMenu2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/newgrayhdr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/newgrayhdr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/newgrayhdr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/newgrayhdr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/newgrayhdr.gif"
                    End If

                    cpg = "../mainmenu/NewMainMenu2.aspx"
                    lpg = "Main Menu"



                Case "/" & appname & "/forum/forummain2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/forum.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/forum.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/forum.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/forum.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/forum.gif"
                    End If

                    cpg = "../forum/forummain2.aspx"
                    lpg = "Forum"

                Case "/" & appname & "/security/editprofilemain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/ep.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/ep.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/ep.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/ep.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/ep.gif"
                    End If

                    cpg = "../security/editprofilemain.aspx"
                    lpg = "Edit Profile"

                Case "/" & appname & "/admin/AppSetpmAppr.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/appr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/appr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/appr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/appr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/appr.gif"
                    End If

                    cpg = "../admin/AppSetpmAppr.aspx"
                    lpg = "PM Approval Set-up"

                Case "/" & appname & "/utils/PMApprovalMain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/appr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/appr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/appr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/appr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/appr.gif"
                    End If

                    cpg = "../utils/PMApprovalMain.aspx"
                    lpg = "PM Approval"

                Case "/" & appname & "/utils/SQLAdmin.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/sqla.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/sqla.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/sqla.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/sqla.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/sqla.gif"
                    End If

                    cpg = "../utils/SQLAdmin.aspx"
                    lpg = "SQL Analyzer"

                Case "/" & appname & "/utils/resourcesmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/res.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/res.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/res.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/res.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/res.gif"
                    End If

                    cpg = "../utils/resourcesmain.aspx"
                    lpg = "Resources"

                Case "/" & appname & "/labor/wrassignmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/wra.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/wra.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/wra.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/wra.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/wra.gif"
                    End If

                    cpg = "../labor/wrassignmain.aspx"
                    lpg = "Work Request Assignments"

                Case "/" & appname & "/appswo/wrmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/workreq.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/workreq.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/workreq.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/workreq.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/workreq.gif"
                    End If

                    cpg = "../appswo/wrmain.aspx"
                    lpg = "Work Requests"

                Case "/" & appname & "/tpmperf/tpmmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/tpmperf.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/tpmperf.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/tpmperf.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/tpmperf.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/tpmperf.gif"
                    End If

                    cpg = "../tpmperf/tpmmain.aspx"
                    lpg = "TPM Performer"

                Case Else
                    cpg = "no"
            End Select
            If cimg = "" And cimgfile <> "~" Then
                Select Case cimgfile '
                    Case "pmo123e1.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appspmo123/pmo123e1.aspx"
                        lpg = "PMO Scrolled"

                    Case "/" & appname & "/appsman/pmpsmodmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appsman/pmpsmodmain.aspx"
                        lpg = "Pass/Fail Thresholds"

                    Case "pmo123equip.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appspmo123/pmo123equip.aspx"
                        lpg = "PMO Stepped"

                    Case "pmo123tabmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/pmotabbed.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/pmotabbed.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/pmotabbed.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/pmotabbed.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/pmotabbed.gif"
                        End If

                        cpg = "../appspmo123tab/pmo123tabmain.aspx"
                        lpg = "PMO Tabbed"

                    Case "dialmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/dbpm.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/eng/dbpm.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/eng/dbpm.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/eng/dbpm.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/eng/dbpm.gif"
                        End If
                        cpg = "../dials/dialmain.aspx"
                        lpg = "Demand Based PM"

                    Case "meterroutesmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/metermenu.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/eng/metermenu.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/eng/metermenu.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/eng/metermenu.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/eng/metermenu.gif"
                        End If
                        cpg = "../equip/meterroutesmain.aspx"
                        lpg = "Meters"
                    Case "complib.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/complib.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/complib.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/complib.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/complib.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/complib.gif"
                        End If
                        cpg = "../complib/complibmain.aspx"
                        lpg = "Component Library"
                    Case "tpmopttasksmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/tpmopt.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/tpmopt.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/tpmopt.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/tpmopt.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/tpmopt.gif"
                        End If

                        cpg = "../appsopttpm/tpmopttasksmain.aspx"
                        lpg = "TPM Optimizer"

                    Case "tpmtasksmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/tpmdev.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/tpmdev.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/tpmdev.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/tpmdev.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/tpmdev.gif"
                        End If

                        cpg = "../appstpm/tpmtasksmain.aspx"
                        lpg = "TPM Developer"

                    Case "pmarchmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/arch.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/arch.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/arch.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/arch.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/arch.gif"
                        End If

                        cpg = "../appsarch/pmarchmain.aspx"
                        lpg = "PM Archive"

                    Case "pmarchmaintpm.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/archt.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/archt.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/archt.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/archt.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/archt.gif"
                        End If

                        cpg = "../appsarchtpm/pmarchmaintpm.aspx"
                        lpg = "TPM Archive"

                    Case "eqmain2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/eqa.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/eqa.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/eqa.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/eqa.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/eqa.gif"
                        End If

                        cpg = "../equip/eqmain2.aspx"
                        lpg = "Equipment"

                    Case "ECRAdm.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/ecr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/ecr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/ecr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/ecr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/ecr.gif"
                        End If

                        cpg = "../equip/ECRAdm.aspx"
                        lpg = "ECR Administration"

                    Case "invmainmenu.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/inv.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/inv.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/inv.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/inv.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/inv.gif"
                        End If

                        cpg = "../inv/invmainmenu.aspx"
                        lpg = "Inventory"

                    Case "invmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/inv.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/inv.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/inv.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/inv.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/inv.gif"
                        End If

                        cpg = "../inv/invmainmenu.aspx"
                        lpg = "Inventory"

                    Case "PMTasks.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/dev1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/dev1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/dev1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/dev1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/dev1.gif"
                        End If

                        cpg = "../apps/PMTasks.aspx?jump=no"
                        lpg = "PM Developer"

                    Case "Appset.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/appset.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/appset.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/appset.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/appset.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/appset.gif"
                        End If

                        cpg = "../admin/Appset.aspx"
                        lpg = "Application Set-up"


                    Case "Assessment_Main.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/asm.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/asm.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/asm.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/asm.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/asm.gif"
                        End If

                        cpg = "../asmstr/Assessment_Main.aspx?atyp=u"
                        lpg = "Assessment Master"

                    Case "Assessment_Main2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/asa.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/asa.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/asa.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/asa.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/asa.gif"
                        End If

                        cpg = "../asmstr/Assessment_Main2.aspx?atyp=a"
                        lpg = "Assessment Admin"


                    Case "eqcopymain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/lib.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/lib.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/lib.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/lib.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/lib.gif"
                        End If

                        cpg = "../equip/eqcopymain.aspx"
                        lpg = "PM Library"

                    Case "PM3OptMain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appsopt/PM3OptMain.aspx"
                        lpg = "PM Optimizer"

                    Case "PMRoutesMain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/route.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/route.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/route.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/route.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/route.gif"
                        End If

                        cpg = "../appsrt/PMRoutesMain.aspx"
                        lpg = "PM Routes Main Page"

                    Case "PMRoutes2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/routedev.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/routedev.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/routedev.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/routedev.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/routedev.gif"
                        End If

                        cpg = "../appsrt/PMRoutes2.aspx"
                        lpg = "PM Routes"

                    Case "PMManager.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/man.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/man.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/man.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/man.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/man.gif"
                        End If

                        cpg = "../appsman/PMManager.aspx"
                        lpg = "PM Manager"

                    Case "pmjpmanager.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/man.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/man.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/man.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/man.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/man.gif"
                        End If

                        cpg = "../appsmax/pmjpmanager.aspx"
                        lpg = "PMJP Manager"

                    Case "TPMManager.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/tman.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/tman.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/tman.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/tman.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/tman.gif"
                        End If

                        cpg = "../appsmantpm/TPMManager.aspx"
                        lpg = "TPM Manager"

                    Case "wolabmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/workman.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/workman.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/workman.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/workman.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/workman.gif"
                        End If

                        cpg = "../appswo/wolabmain.aspx"
                        lpg = "Work Manager"

                    Case "wolist1_fs.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/workman.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/workman.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/workman.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/workman.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/workman.gif"
                        End If

                        cpg = "../appswo/wolist1_fs.aspx"
                        lpg = "Work Manager - Full Screen"

                    Case "wojpeditmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/jobplan.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/jobplan.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/jobplan.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/jobplan.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/jobplan.gif"
                        End If

                        cpg = "../appswo/wojpeditmain.aspx"
                        lpg = "WO Job Plan"

                    Case "JobPlanMain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/jobplan.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/jobplan.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/jobplan.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/jobplan.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/jobplan.gif"
                        End If

                        cpg = "../appsrt/JobPlanMain.aspx"
                        lpg = "Job Plan Main"

                    Case "jobplanmain2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/jobplandev.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/jobplandev.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/jobplandev.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/jobplandev.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/jobplandev.gif"
                        End If

                        cpg = "../appsrt/jobplanmain2.aspx"
                        lpg = "Job Plan"



                    Case "useradminmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/usr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/usr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/usr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/usr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/usr.gif"
                        End If

                        cpg = "../security/useradminmain.aspx"
                        lpg = "User Administration"

                    Case "pmlocmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/loc.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/loc.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/loc.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/loc.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/loc.gif"
                        End If

                        cpg = "../locs/pmlocmain.aspx"
                        lpg = "Locations"

                    Case "NewMainMenu2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/newgrayhdr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/newgrayhdr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/newgrayhdr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/newgrayhdr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/newgrayhdr.gif"
                        End If

                        cpg = "../mainmenu/NewMainMenu2.aspx"
                        lpg = "Main Menu"



                    Case "forummain2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/forum.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/forum.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/forum.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/forum.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/forum.gif"
                        End If

                        cpg = "../forum/forummain2.aspx"
                        lpg = "Forum"

                    Case "editprofilemain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/ep.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/ep.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/ep.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/ep.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/ep.gif"
                        End If

                        cpg = "../security/editprofilemain.aspx"
                        lpg = "Edit Profile"

                    Case "AppSetpmAppr.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/appr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/appr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/appr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/appr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/appr.gif"
                        End If

                        cpg = "../admin/AppSetpmAppr.aspx"
                        lpg = "PM Approval Set-up"

                    Case "PMApprovalMain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/appr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/appr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/appr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/appr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/appr.gif"
                        End If

                        cpg = "../utils/PMApprovalMain.aspx"
                        lpg = "PM Approval"

                    Case "SQLAdmin.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/sqla.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/sqla.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/sqla.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/sqla.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/sqla.gif"
                        End If

                        cpg = "../utils/SQLAdmin.aspx"
                        lpg = "SQL Analyzer"

                    Case "resourcesmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/res.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/res.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/res.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/res.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/res.gif"
                        End If

                        cpg = "../utils/resourcesmain.aspx"
                        lpg = "Resources"

                    Case "wrassignmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/wra.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/wra.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/wra.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/wra.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/wra.gif"
                        End If

                        cpg = "../labor/wrassignmain.aspx"
                        lpg = "Work Request Assignments"

                    Case "wrmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/workreq.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/workreq.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/workreq.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/workreq.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/workreq.gif"
                        End If

                        cpg = "../appswo/wrmain.aspx"
                        lpg = "Work Requests"

                    Case "tpmmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/tpmperf.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/tpmperf.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/tpmperf.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/tpmperf.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/tpmperf.gif"
                        End If

                        cpg = "../tpmperf/tpmmain.aspx"
                        lpg = "TPM Performer"

                    Case Else
                        cpg = "no"
                End Select
            End If
            Try
                sess = app.Log(lpg)
            Catch ex As Exception

            End Try
            'menulit.Text = BuildMenu(ThisPage, appname, appstr)
            'Dim cimg1 As String = cimg
            tdsid.InnerHtml = BuildMenu(ThisPage, appname, appstr, cimg)
        Else

            npd = "yes"
            getsess()
            'Try
            'sess = HttpContext.Current.Session.SessionID

            'Catch ex As Exception
            'app.LogOut(sess)
            'Exit Sub
            'End Try
            If mb = "2" Then
                hopt = "3"
            Else
                hopt = hdr
            End If
            If hdr = "0" Then
                hdr = ""
            End If
            If df_hold <> df Then
                df_hold = df
            End If
            Select Case ThisPage '
                Case "/" & appname & "/appspmo123/pmo123e1.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../appspmo123/pmo123e1.aspx"
                    lpg = "PMO Scrolled"

                Case "/" & appname & "/appsman/pmpsmodmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../appsman/pmpsmodmain.aspx"
                    lpg = "Pass/Fail Thresholds"

                Case "/" & appname & "/appspmo123/pmo123equip.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../appspmo123/pmo123equip.aspx"
                    lpg = "PMO Stepped"

                Case "/" & appname & "/appspmo123tab/pmo123tabmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../pmo123tab/pmo123tabmain.aspx"
                    lpg = "PMo Tabbed"

                Case "/" & appname & "/dials/dialmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/dbpm.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/dbpm.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/dbpm.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/dbpm.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/dbpm.gif"
                    End If
                    cpg = "../dials/dialmain.aspx"
                    lpg = "Demand Based PM"
                Case "/" & appname & "/equip/meterroutesmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/metermenu.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/metermenu.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/metermenu.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/metermenu.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/metermenu.gif"
                    End If
                    cpg = "../equip/meterroutesmain.aspx"
                    lpg = "Meters"
                Case "/" & appname & "/complib/complib.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/complib.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/complib.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/complib.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/complib.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/complib.gif"
                    End If
                    cpg = "../complib/complibmain.aspx"
                    lpg = "Component Library"
                Case "/" & appname & "/appsopttpm/tpmopttasksmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/tpmopt.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/tpmopt.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/tpmopt.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/tpmopt.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/tpmopt.gif"
                    End If

                    cpg = "../appsopttpm/tpmopttasksmain.aspx"
                    lpg = "TPM Optimizer"

                Case "/" & appname & "/appstpm/tpmtasksmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/tpmdev.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/tpmdev.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/tpmdev.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/tpmdev.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/tpmdev.gif"
                    End If

                    cpg = "../appstpm/tpmtasksmain.aspx"
                    lpg = "TPM Developer"

                Case "/" & appname & "/appsarch/pmarchmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/arch.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/arch.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/arch.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/arch.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/arch.gif"
                    End If

                    cpg = "../appsarch/pmarchmain.aspx"
                    lpg = "PM Archive"

                Case "/" & appname & "/appsarchtpm/pmarchmaintpm.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/archt.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/archt.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/archt.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/archt.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/archt.gif"
                    End If

                    cpg = "../appsarchtpm/pmarchmaintpm.aspx"
                    lpg = "TPM Archive"

                Case "/" & appname & "/equip/eqmain2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/eqa.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/eqa.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/eqa.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/eqa.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/eqa.gif"
                    End If

                    cpg = "../equip/eqmain2.aspx"
                    lpg = "Equipment"

                Case "/" & appname & "/equip/ECRAdm.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/ecr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/ecr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/ecr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/ecr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/ecr.gif"
                    End If

                    cpg = "../equip/ECRAdm.aspx"
                    lpg = "ECR Administration"

                Case "/" & appname & "/inv/invmainmenu.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/inv.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/inv.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/inv.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/inv.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/inv.gif"
                    End If

                    cpg = "../inv/invmainmenu.aspx"
                    lpg = "Inventory"

                Case "/" & appname & "/inv/invmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/inv.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/inv.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/inv.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/inv.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/inv.gif"
                    End If

                    cpg = "../inv/invmainmenu.aspx"
                    lpg = "Inventory"


                Case "/" & appname & "/apps/PMTasks.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/dev1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/dev1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/dev1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/dev1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/dev1.gif"
                    End If

                    cpg = "../apps/PMTasks.aspx?jump=no"
                    lpg = "PM Developer"

                Case "/" & appname & "/admin/Appset.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/appset.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/appset.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/appset.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/appset.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/appset.gif"
                    End If

                    cpg = "../admin/Appset.aspx"
                    lpg = "Application Set-up"


                Case "/" & appname & "/asmstr/Assessment_Main.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/asm.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/asm.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/asm.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/asm.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/asm.gif"
                    End If

                    cpg = "../asmstr/Assessment_Main.aspx?atyp=u"
                    lpg = "Assessment Master"

                Case "/" & appname & "/asmstr/Assessment_Main2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/asa.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/asa.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/asa.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/asa.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/asa.gif"
                    End If

                    cpg = "../asmstr/Assessment_Main2.aspx?atyp=a"
                    lpg = "Assessment Admin"


                Case "/" & appname & "/equip/eqcopymain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/lib.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/lib.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/lib.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/lib.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/lib.gif"
                    End If

                    cpg = "../equip/eqcopymain.aspx"
                    lpg = "PM Library"

                Case "/" & appname & "/appsopt/PM3OptMain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/opt1.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/opt1.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/opt1.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/opt1.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/opt1.gif"
                    End If

                    cpg = "../appsopt/PM3OptMain.aspx"
                    lpg = "PM Optimizer"

                Case "/" & appname & "/appsrt/PMRoutesMain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/route.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/route.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/route.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/route.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/route.gif"
                    End If

                    cpg = "../appsrt/PMRoutesMain.aspx"
                    lpg = "PM Routes Main Page"

                Case "/" & appname & "/appsrt/PMRoutes2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/routedev.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/routedev.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/routedev.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/routedev.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/routedev.gif"
                    End If

                    cpg = "../appsrt/PMRoutes2.aspx"
                    lpg = "PM Routes"

                Case "/" & appname & "/appsman/PMManager.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/man.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/man.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/man.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/man.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/man.gif"
                    End If

                    cpg = "../appsman/PMManager.aspx"
                    lpg = "PM Manager"

                Case "/" & appname & "/appsmax/pmjpmanager.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/man.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/man.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/man.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/man.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/man.gif"
                    End If

                    cpg = "../appsmax/pmjpmanager.aspx"
                    lpg = "PMJP Manager"

                Case "/" & appname & "/appsmantpm/TPMManager.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/tman.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/tman.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/tman.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/tman.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/tman.gif"
                    End If

                    cpg = "../appsmantpm/TPMManager.aspx"
                    lpg = "TPM Manager"

                Case "/" & appname & "/appswo/wolabmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/workman.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/workman.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/workman.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/workman.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/workman.gif"
                    End If

                    cpg = "../appswo/wolabmain.aspx"
                    lpg = "Work Manager"

                Case "/" & appname & "/appswo/wolist1_fs.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/workman.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/workman.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/workman.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/workman.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/workman.gif"
                    End If

                    cpg = "../appswo/wolist1_fs.aspx"
                    lpg = "Work Manager - Full Screen"

                Case "/" & appname & "/appswo/wojpeditmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/jobplan.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/jobplan.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/jobplan.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/jobplan.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/jobplan.gif"
                    End If

                    cpg = "../appswo/wojpeditmain.aspx"
                    lpg = "WO Job Plan"

                Case "/" & appname & "/appsrt/JobPlanMain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/jobplan.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/jobplan.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/jobplan.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/jobplan.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/jobplan.gif"
                    End If

                    cpg = "../appsrt/JobPlanMain.aspx"
                    lpg = "Job Plan Main"

                Case "/" & appname & "/appsrt/jobplanmain2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/jobplandev.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/jobplandev.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/jobplandev.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/jobplandev.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/jobplandev.gif"
                    End If

                    cpg = "../appsrt/jobplanmain2.aspx"
                    lpg = "Job Plan"



                Case "/" & appname & "/security/useradminmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/usr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/usr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/usr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/usr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/usr.gif"
                    End If

                    cpg = "../security/useradminmain.aspx"
                    lpg = "User Administration"

                Case "/" & appname & "/locs/pmlocmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/loc.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/loc.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/loc.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/loc.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/loc.gif"
                    End If

                    cpg = "../locs/pmlocmain.aspx"
                    lpg = "Locations"

                Case "/" & appname & "/mainmenu/NewMainMenu2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/newgrayhdr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/newgrayhdr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/newgrayhdr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/newgrayhdr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/newgrayhdr.gif"
                    End If

                    cpg = "../mainmenu/NewMainMenu2.aspx"
                    lpg = "Main Menu"



                Case "/" & appname & "/forum/forummain2.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/forum.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/forum.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/forum.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/forum.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/forum.gif"
                    End If

                    cpg = "../forum/forummain2.aspx"
                    lpg = "Forum"

                Case "/" & appname & "/security/editprofilemain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/ep.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/ep.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/ep.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/ep.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/ep.gif"
                    End If

                    cpg = "../security/editprofilemain.aspx"
                    lpg = "Edit Profile"

                Case "/" & appname & "/admin/AppSetpmAppr.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/appr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/appr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/appr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/appr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/appr.gif"
                    End If

                    cpg = "../admin/AppSetpmAppr.aspx"
                    lpg = "PM Approval Set-up"

                Case "/" & appname & "/utils/PMApprovalMain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/appr.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/appr.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/appr.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/appr.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/appr.gif"
                    End If

                    cpg = "../utils/PMApprovalMain.aspx"
                    lpg = "PM Approval"

                Case "/" & appname & "/utils/SQLAdmin.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/sqla.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/sqla.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/sqla.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/sqla.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/sqla.gif"
                    End If

                    cpg = "../utils/SQLAdmin.aspx"
                    lpg = "SQL Analyzer"

                Case "/" & appname & "/utils/resourcesmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/res.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/res.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/res.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/res.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/res.gif"
                    End If

                    cpg = "../utils/resourcesmain.aspx"
                    lpg = "Resources"

                Case "/" & appname & "/labor/wrassignmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/wra.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/wra.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/wra.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/wra.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/wra.gif"
                    End If

                    cpg = "../labor/wrassignmain.aspx"
                    lpg = "Work Request Assignments"

                Case "/" & appname & "/appswo/wrmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/workreq.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/workreq.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/workreq.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/workreq.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/workreq.gif"
                    End If

                    cpg = "../appswo/wrmain.aspx"
                    lpg = "Work Requests"

                Case "/" & appname & "/tpmperf/tpmmain.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/tpmperf.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/fre/tpmperf.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/ger/tpmperf.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/ita/tpmperf.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/spa/tpmperf.gif"
                    End If

                    cpg = "../tpmperf/tpmmain.aspx"
                    lpg = "TPM Performer"

                Case "ReportList.aspx"
                    If lang = "eng" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "fre" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ger" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "ita" Then
                        cimg = "../images2/eng/rep.gif"
                    ElseIf lang = "spa" Then
                        cimg = "../images2/eng/rep.gif"
                    End If

                    cpg = "../reporting/ReportList.aspx"
                    lpg = "Izenda Reports"

                Case Else
                    cpg = "no"
            End Select
            If cimg = "" And cimgfile <> "~" Then
                Select Case cimgfile '
                    Case "pmo123e1.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appspmo123/pmo123e1.aspx"
                        lpg = "PMO Scrolled"


                    Case "/" & appname & "/appsman/pmpsmodmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appsman/pmpsmodmain.aspx"
                        lpg = "Pass/Fail Thresholds"

                    Case "pmo123equip.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appspmo123/pmo123equip.aspx"
                        lpg = "PMO Stepped"

                    Case "pmo123tabmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appspmo123tab/pmo123tabmain.aspx"
                        lpg = "PMO Tabbed"

                    Case "dialmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/dbpm.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/eng/dbpm.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/eng/dbpm.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/eng/dbpm.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/eng/dbpm.gif"
                        End If
                        cpg = "../dials/dialmain.aspx"
                        lpg = "Demand Based PM"

                    Case "meterroutesmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/metermenu.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/eng/metermenu.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/eng/metermenu.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/eng/metermenu.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/eng/metermenu.gif"
                        End If
                        cpg = "../equip/meterroutesmain.aspx"
                        lpg = "Meters"
                    Case "complib.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/complib.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/complib.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/complib.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/complib.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/complib.gif"
                        End If
                        cpg = "../complib/complibmain.aspx"
                        lpg = "Component Library"
                    Case "tpmopttasksmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/tpmopt.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/tpmopt.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/tpmopt.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/tpmopt.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/tpmopt.gif"
                        End If

                        cpg = "../appsopttpm/tpmopttasksmain.aspx"
                        lpg = "TPM Optimizer"

                    Case "tpmtasksmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/tpmdev.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/tpmdev.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/tpmdev.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/tpmdev.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/tpmdev.gif"
                        End If

                        cpg = "../appstpm/tpmtasksmain.aspx"
                        lpg = "TPM Developer"

                    Case "pmarchmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/arch.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/arch.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/arch.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/arch.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/arch.gif"
                        End If

                        cpg = "../appsarch/pmarchmain.aspx"
                        lpg = "PM Archive"

                    Case "pmarchmaintpm.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/archt.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/archt.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/archt.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/archt.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/archt.gif"
                        End If

                        cpg = "../appsarchtpm/pmarchmaintpm.aspx"
                        lpg = "TPM Archive"

                    Case "eqmain2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/eqa.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/eqa.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/eqa.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/eqa.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/eqa.gif"
                        End If

                        cpg = "../equip/eqmain2.aspx"
                        lpg = "Equipment"

                    Case "ECRAdm.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/ecr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/ecr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/ecr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/ecr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/ecr.gif"
                        End If

                        cpg = "../equip/ECRAdm.aspx"
                        lpg = "ECR Administration"

                    Case "invmainmenu.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/inv.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/inv.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/inv.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/inv.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/inv.gif"
                        End If

                        cpg = "../inv/invmainmenu.aspx"
                        lpg = "Inventory"

                    Case "invmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/inv.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/inv.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/inv.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/inv.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/inv.gif"
                        End If

                        cpg = "../inv/invmainmenu.aspx"
                        lpg = "Inventory"

                    Case "PMTasks.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/dev1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/dev1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/dev1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/dev1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/dev1.gif"
                        End If

                        cpg = "../apps/PMTasks.aspx?jump=no"
                        lpg = "PM Developer"

                    Case "Appset.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/appset.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/appset.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/appset.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/appset.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/appset.gif"
                        End If

                        cpg = "../admin/Appset.aspx"
                        lpg = "Application Set-up"


                    Case "Assessment_Main.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/asm.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/asm.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/asm.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/asm.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/asm.gif"
                        End If

                        cpg = "../asmstr/Assessment_Main.aspx?atyp=u"
                        lpg = "Assessment Master"

                    Case "Assessment_Main2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/asa.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/asa.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/asa.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/asa.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/asa.gif"
                        End If

                        cpg = "../asmstr/Assessment_Main2.aspx?atyp=a"
                        lpg = "Assessment Admin"


                    Case "eqcopymain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/lib.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/lib.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/lib.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/lib.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/lib.gif"
                        End If

                        cpg = "../equip/eqcopymain.aspx"
                        lpg = "PM Library"

                    Case "PM3OptMain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/opt1.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/opt1.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/opt1.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/opt1.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/opt1.gif"
                        End If

                        cpg = "../appsopt/PM3OptMain.aspx"
                        lpg = "PM Optimizer"

                    Case "PMRoutesMain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/route.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/route.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/route.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/route.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/route.gif"
                        End If

                        cpg = "../appsrt/PMRoutesMain.aspx"
                        lpg = "PM Routes Main Page"

                    Case "PMRoutes2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/routedev.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/routedev.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/routedev.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/routedev.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/routedev.gif"
                        End If

                        cpg = "../appsrt/PMRoutes2.aspx"
                        lpg = "PM Routes"

                    Case "PMManager.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/man.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/man.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/man.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/man.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/man.gif"
                        End If

                        cpg = "../appsman/PMManager.aspx"
                        lpg = "PM Manager"

                    Case "pmjpmanager.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/man.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/man.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/man.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/man.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/man.gif"
                        End If

                        cpg = "../appsmax/pmjpmanager.aspx"
                        lpg = "PMJP Manager"

                    Case "TPMManager.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/tman.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/tman.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/tman.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/tman.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/tman.gif"
                        End If

                        cpg = "../appsmantpm/TPMManager.aspx"
                        lpg = "TPM Manager"

                    Case "wolabmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/workman.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/workman.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/workman.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/workman.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/workman.gif"
                        End If

                        cpg = "../appswo/wolabmain.aspx"
                        lpg = "Work Manager"

                    Case "wolist1_fs.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/workman.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/workman.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/workman.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/workman.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/workman.gif"
                        End If

                        cpg = "../appswo/wolist1_fs.aspx"
                        lpg = "Work Manager - Full Screen"

                    Case "wojpeditmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/jobplan.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/jobplan.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/jobplan.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/jobplan.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/jobplan.gif"
                        End If

                        cpg = "../appswo/wojpeditmain.aspx"
                        lpg = "WO Job Plan"

                    Case "JobPlanMain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/jobplan.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/jobplan.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/jobplan.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/jobplan.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/jobplan.gif"
                        End If

                        cpg = "../appsrt/JobPlanMain.aspx"
                        lpg = "Job Plan Main"

                    Case "jobplanmain2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/jobplandev.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/jobplandev.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/jobplandev.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/jobplandev.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/jobplandev.gif"
                        End If

                        cpg = "../appsrt/jobplanmain2.aspx"
                        lpg = "Job Plan"



                    Case "useradminmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/usr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/usr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/usr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/usr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/usr.gif"
                        End If

                        cpg = "../security/useradminmain.aspx"
                        lpg = "User Administration"

                    Case "pmlocmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/loc.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/loc.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/loc.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/loc.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/loc.gif"
                        End If

                        cpg = "../locs/pmlocmain.aspx"
                        lpg = "Locations"

                    Case "NewMainMenu2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/newgrayhdr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/newgrayhdr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/newgrayhdr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/newgrayhdr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/newgrayhdr.gif"
                        End If

                        cpg = "../mainmenu/NewMainMenu2.aspx"
                        lpg = "Main Menu"



                    Case "forummain2.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/forum.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/forum.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/forum.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/forum.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/forum.gif"
                        End If

                        cpg = "../forum/forummain2.aspx"
                        lpg = "Forum"

                    Case "editprofilemain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/ep.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/ep.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/ep.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/ep.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/ep.gif"
                        End If

                        cpg = "../security/editprofilemain.aspx"
                        lpg = "Edit Profile"

                    Case "AppSetpmAppr.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/appr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/appr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/appr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/appr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/appr.gif"
                        End If

                        cpg = "../admin/AppSetpmAppr.aspx"
                        lpg = "PM Approval Set-up"

                    Case "PMApprovalMain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/appr.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/appr.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/appr.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/appr.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/appr.gif"
                        End If

                        cpg = "../utils/PMApprovalMain.aspx"
                        lpg = "PM Approval"

                    Case "SQLAdmin.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/sqla.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/sqla.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/sqla.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/sqla.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/sqla.gif"
                        End If

                        cpg = "../utils/SQLAdmin.aspx"
                        lpg = "SQL Analyzer"

                    Case "resourcesmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/res.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/res.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/res.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/res.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/res.gif"
                        End If

                        cpg = "../utils/resourcesmain.aspx"
                        lpg = "Resources"

                    Case "wrassignmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/wra.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/wra.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/wra.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/wra.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/wra.gif"
                        End If

                        cpg = "../labor/wrassignmain.aspx"
                        lpg = "Work Request Assignments"

                    Case "wrmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/workreq.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/workreq.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/workreq.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/workreq.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/workreq.gif"
                        End If

                        cpg = "../appswo/wrmain.aspx"
                        lpg = "Work Requests"

                    Case "tpmmain.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/tpmperf.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/fre/tpmperf.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/ger/tpmperf.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/ita/tpmperf.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/spa/tpmperf.gif"
                        End If

                        cpg = "../tpmperf/tpmmain.aspx"
                        lpg = "TPM Performer"

                    Case "ReportList.aspx"
                        If lang = "eng" Then
                            cimg = "../images2/eng/rep.gif"
                        ElseIf lang = "fre" Then
                            cimg = "../images2/eng/rep.gif"
                        ElseIf lang = "ger" Then
                            cimg = "../images2/eng/rep.gif"
                        ElseIf lang = "ita" Then
                            cimg = "../images2/eng/rep.gif"
                        ElseIf lang = "spa" Then
                            cimg = "../images2/eng/rep.gif"
                        End If

                        cpg = "../reporting/ReportList.aspx"
                        lpg = "Izenda Reports"

                    Case Else
                        cpg = "no"
                End Select
            End If
            Try
                sess = app.Log(lpg)
            Catch ex As Exception

            End Try
            'menulit.Text = BuildMenu(ThisPage, appname, appstr)
            'Dim cimg3 As String = cimg
            tdsid.InnerHtml = BuildMenu(ThisPage, appname, appstr, cimg)
        End If
    End Sub
    Private Function BuildMenu(ByVal thispage As String, ByVal appname As String, ByVal appstr As String, ByVal cimg As String)
        Dim sb As New StringBuilder
        Dim ds As New DataSet
        ds = mutils.MainMenuList
        Dim dt As New DataTable
        dt = ds.Tables(0)


        usite.Open()
        'sql = "insert into sesstest (thispage) values ('" & thispage & "')"
        'usite.Update(sql)

        Dim dss As New DataSet
        Try
            dss = mutils.MainMenuApps(thispage, appname, appstr)
        Catch ex As Exception
            'gotolocnew2(urlname1)
            Dim urlname1 As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
            Dim strScript As String
            Dim cs As ClientScriptManager = Page.ClientScript
            Dim csname1 As [String] = "PopupScript"
            Dim cstype As Type = Me.[GetType]()
            If Not cs.IsStartupScriptRegistered(cstype, csname1) Then
                strScript = "<script language=JavaScript>window.location = '" & urlname1 & "'</script>"
                cs.RegisterStartupScript(cstype, csname1, strScript)
                Exit Function
            End If
        End Try

        Dim dts As New DataTable
        dts = dss.Tables(0)

        Dim dsu As New DataSet
        sql = "select siteid from pmusersites where uid = '" & uid & "'"
        'dsu = usite.GetDSData(sql)
        'Dim dtu As New DataTable
        'dtu = dsu.Tables(0)
        usites = ""

        Dim dtscnt As Integer

        dr = usite.GetRdrData(sql)
        While dr.Read
            If usites = "" Then
                dtscnt = 1
                usites = dr.Item("siteid").ToString
            Else
                dtscnt += 1
                usites += "," & dr.Item("siteid").ToString
            End If
        End While
        dr.Close()
        Try
            sql = "usp_optvaladja"
            'usite.Open()
            usite.Update(sql)
            'usite.Dispose()
        Catch ex As Exception

        End Try
        usite.Dispose()

        Dim micon, mname, mapp, mids, mact, lclass, mclass, mclick, sclick, imapp, hicon, lft, wid, mloc, act, dicon, hdicon As String

        'sb.Append("<html>" & vbCrLf)
        'sb.Append("<head>" & vbCrLf)
        'sb.Append("<script language=""javascript"" src=""../scripts/mmenu11.js""></script>" & vbCrLf)
        'sb.Append("<script language=""JavaScript"" src=""../scripts/overlib.js""></script>" & vbCrLf)

        'sb.Append("<link rel=""stylesheet"" type=""text/css"" href=""../styles/pmcssa1.css"">" & vbCrLf)
        'sb.Append("</head>" & vbCrLf)
        'sb.Append("<body onbeforeunload=""ConfirmClose()"" onunload=""HandleOnClose()"" onload=""checkit();"" backgroundColor=""white"">" & vbCrLf) 'onunload=""checkout();""

        'sb.Append("<form id=""form1"" runat=""server"">" & vbCrLf)



        'page background
        If mbg <> "0" And ptst <> "1" Then
            'sb.Append("<table style=""z-index: 0; LEFT: 7px; POSITION: absolute; TOP: 12px"" height=""600"" width=""980"" background=""../menu/mimages/bigatom.gif"">")
            'sb.Append("<tr><td>&nbsp;</td></tr></table>")
        End If

        'top menu
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim logouturl As String = urlname & "?logout=yes"
        hdr = ""
        '<td>&nbsp;</td>
        '<td></td>
        'usrname = "Something that is really really really"



        If ptst = "1" Then
            usrname = Mid(usrname, 1, 30)
            sb.Append("<table id=""tdtop"" class=""btmmenuhdr" & hdr & """>" & vbCrLf)
            sb.Append("<td valign=""top"" width=""1070"" height=""62"">" & vbCrLf)
            sb.Append("<table width=""1070"" cellpadding=""0"" cellspacing=""0"" border=""0"">" & vbCrLf)
            sb.Append("<tr><td width=""260""></td><td width=""290""></td><td width=""410""></td>")
            sb.Append("<td width=""6""></td><td width=""22""></td><td width=""22""></td></tr>")
            sb.Append("<TR>")
            sb.Append("<TD valign=""top"" ROWSPAN=""3"">")
            sb.Append("<IMG SRC=""../menu/mimages/images/grpsm5.gif"" ALT=""""></TD>")
            sb.Append("<TD ROWSPAN=""3"" valign=""top"">")
            sb.Append("<table cellpadding=""1"" cellspacing=""1"" border=""0"">" & vbCrLf)
            sb.Append("<tr><td colspan=""2"" height=""4""><img src=""../images/appbuttons/minibuttons/2PX.gif""></td></tr>")

            sb.Append("<tr><td class=""graylabel"" valign=""bottom"" width=""144"">" & tmod.getlbl("cdlbl639", "mmenu1.ascx.vb") & "</td>")
            sb.Append("<td class=""graylabel"" valign=""bottom"" width=""142"">" & psite & "</td></tr>")


            sb.Append("<tr><td class=""graylabel"">" & tmod.getlbl("cdlbl640", "mmenu1.ascx.vb") & "</td>")
            '
            sb.Append("<td class=""graylabel"">" & usrname & "</td></tr>")
            Dim imgtest1 As String = cimg
            Dim imgtest2 As String = cimg

            sb.Append("</table></td>" & vbCrLf)
            sb.Append("<td align=""right"" vAlign=""top"" height=""20""><img src=""" & cimg & """></td>")
            sb.Append("<TD ROWSPAN=""3"" valign=""top"">&nbsp;</td>")

            sb.Append("<td rowspan=""3"" valign=""top""><a href=""http://www.laireliability.com/fusion-training/fusion-video-training-overview"" target=""_blank""><img height=""20"" width=""20"" src=""../images/appbuttons/minibuttons/Q.gif"" onmouseover=""return overlib('" & tmod.getov("cov316", "mmenu1.ascx.vb") & "', BELOW, LEFT)"" onmouseout=""return nd()"" border=""0""></a></td>")

            sb.Append("<td rowspan=""3"" valign=""top"" ><A href=""" & logouturl & """><img  height=""20"" width=""20"" src=""../images/appbuttons/minibuttons/LO.gif"" onmouseover=""return overlib('" & tmod.getov("cov317", "mmenu1.ascx.vb") & "', BELOW, LEFT)"" onmouseout=""return nd()"" border=""0""></A></td>")

            sb.Append("</TR>")
            sb.Append("<TR>")
            sb.Append("<TD height=""20"">")
            sb.Append("&nbsp;</TD></tr>")
            sb.Append("<TR>")
            sb.Append("<TD height=""20"">")
            sb.Append("&nbsp;</TD></tr>")

            sb.Append("</table>" & vbCrLf)

            sb.Append("</td>" & vbCrLf)
            sb.Append("</tr>" & vbCrLf)
            sb.Append("</table>" & vbCrLf)

            'Main Menu Row
            Dim drM As DataRow
            For Each drM In dt.Select()
                mids = drM("mid").ToString
                imapp = "im" & mids
                mids = "mm" & mids
                micon = drM("micon").ToString
                hicon = drM("hovicon").ToString
                mname = drM("mname").ToString
                If mids = "mm0" Then
                    lft = "570px"
                ElseIf mids = "mm1" Then
                    lft = "689px"
                ElseIf mids = "mm2" Then
                    lft = "806px"
                ElseIf mids = "mm3" Then
                    lft = "933px"
                End If
                mloc += "sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                                + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
                                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
                If mids = "mm0" Then
                    sb.Append("<div style=""z-index: 3; position: absolute; top: 43px; left: " & lft & ";BORDER-RIGHT: gray 1px dotted;BORDER-TOP: gray 1px dotted;BORDER-BOTTOM: gray 1px dotted;BORDER-LEFT: gray 1px dotted"" ")
                    sb.Append("id=""" & mids & """ onmouseover=""showmenu('" & hicon & "','" & mids & "','" & imapp & "');"" ")
                    sb.Append("onmouseleave=""hidemenu_mm();"">")
                    sb.Append("<table cellspacing=""0"" cellpadding=""1"">")
                    sb.Append("<tr height=""24px"" >")

                    lclass = "plainlabelau"
                    sb.Append("<td width=""20""><a href=""../mainmenu/NewMainMenu2.aspx""><img src=""" & micon & """ border=""0""></a></td>")
                    sb.Append("<td width=""90"" >")
                    'sb.Append("<a href=""../mainmenu/NewMainMenu2.aspx"" class=""" & lclass & """>" & mname & "</a></td>")
                    sb.Append("<a href=""../mainmenu/NewMainMenu2.aspx?" & mloc & """ class=""" & lclass & """>" & mname & "</a></td>")

                    sb.Append("</tr>")
                    sb.Append("</table>")
                    sb.Append("</div>")
                Else
                    sb.Append("<div style=""z-index: 3; position: absolute; top: 43px; left: " & lft & ";BORDER-RIGHT: gray 1px dotted;BORDER-TOP: gray 1px dotted;BORDER-BOTTOM: gray 1px dotted;BORDER-LEFT: gray 1px dotted"" ")
                    sb.Append("id=""" & mids & """ onmouseover=""showmenu('" & hicon & "','" & mids & "','" & imapp & "');"" ")
                    If mids = "mm1" Then
                        sb.Append("onmouseleave=""hidemenu_m1();"">")
                    ElseIf mids = "mm2" Then
                        sb.Append("onmouseleave=""hidemenu_m2();"">")
                    ElseIf mids = "mm3" Then
                        sb.Append("onmouseleave=""hidemenu_m3();"">")
                    End If
                    ','" & dtscnt & "'
                    sb.Append("<table cellspacing=""0"" cellpadding=""1"">")
                    sb.Append("<tr height=""24px"" >")

                    lclass = "plbox"
                    If mids = "mm3" Then
                        sb.Append("<td width=""20""><img src=""" & micon & """ id=""" & imapp & """></td>")
                        sb.Append("<td width=""114"" class=""plainlabel"">" & mname & "</td>")
                    ElseIf mids = "mm2" Then
                        sb.Append("<td width=""20""><img src=""" & micon & """ id=""" & imapp & """></td>")
                        sb.Append("<td width=""98"" class=""plainlabel"">" & mname & "</td>")
                    Else
                        sb.Append("<td width=""20""><img src=""" & micon & """ id=""" & imapp & """></td>")
                        sb.Append("<td width=""88"" class=""plainlabel"">" & mname & "</td>")
                    End If


                    sb.Append("</tr>")
                    sb.Append("</table>")
                    sb.Append("</div>")
                End If


            Next
        Else
            Dim tstinmg As String = cimg
            usrname = Mid(usrname, 1, 40)
            sb.Append("<table id=""tdtop"" class=""btmmenuhdr" & hdr & """>" & vbCrLf)
            sb.Append("<td valign=""top"" width=""230"" height=""62"">" & vbCrLf)

            sb.Append("<img src=""../menu/mimages/fusionsm" & hdr & ".gif""  onmouseover=""checkmenus();"">")

            sb.Append("</td>")
            sb.Append("<td valign=""top"" width=""715"">" & vbCrLf)
            sb.Append("<table cellspacing=""0""  onmouseover=""checkmenus();""><tr><td colspan=""3"" align=""right""><img src=""" & cimg & """ ></td></tr>")
            sb.Append("<tr><td width=""5""></td><td width=""134""></td><td width=""576""></td></tr>")
            sb.Append("<tr><td class=""graylabel"" colspan=""2"">" & tmod.getlbl("cdlbl639", "mmenu1.ascx.vb") & "</td><td class=""plainlabel"">" & psite & "</td></tr>")
            sb.Append("<tr><td class=""graylabel"" colspan=""2"">" & tmod.getlbl("cdlbl640", "mmenu1.ascx.vb") & "</td><td class=""plainlabel"">" & usrname & "</td>")
            sb.Append("</tr></table>")
            sb.Append("</td>")

            sb.Append("<td valign=""top"" width=""95"" align=""right"">" & vbCrLf)
            sb.Append("<table width=""95""  onmouseover=""checkmenus();""><tr>")
            sb.Append("<td width=""24"">&nbsp;</td>")
            sb.Append("<td width=""23"">&nbsp;</td><td width=""24""><a href=""http://www.laireliability.com/fusion-training/fusion-video-training-overview"" target=""_blank""><img src=""../images/appbuttons/minibuttons/Q.gif"" onmouseover=""return overlib('" & tmod.getov("cov318", "mmenu1.ascx.vb") & "', BELOW, LEFT)"" onmouseout=""return nd()"" border=""0""></a></td>")
            'sb.Append("<td  width=""24""><img onclick=""getopts('" & cpg & "','" & urlname & "','" & uid & "','" & hopt & "','" & mbg & "');"" src=""../images/appbuttons/minibuttons/MS.gif"" onmouseover=""return overlib('" & tmod.getov("cov319", "mmenu1.ascx.vb") & "', BELOW, LEFT)"" onmouseout=""return nd()""></td>")
            sb.Append("<td  width=""24""><A href=""" & logouturl & """><img src=""../images/appbuttons/minibuttons/LO.gif"" onmouseover=""return overlib('" & tmod.getov("cov320", "mmenu1.ascx.vb") & "', BELOW, LEFT)"" onmouseout=""return nd()"" border=""0""></A></td>")

            sb.Append("</tr>")
            sb.Append("</table></td>")

            sb.Append("</tr>" & vbCrLf)
            sb.Append("</table>" & vbCrLf)

            'Main Menu Row
            Dim drM As DataRow
            For Each drM In dt.Select()
                mids = drM("mid").ToString
                imapp = "im" & mids
                mids = "mm" & mids
                micon = drM("micon").ToString
                hicon = drM("hovicon").ToString
                mname = drM("mname").ToString
                If mids = "mm0" Then
                    lft = "558px"
                ElseIf mids = "mm1" Then
                    lft = "675px"
                ElseIf mids = "mm2" Then
                    lft = "792px"
                ElseIf mids = "mm3" Then
                    lft = "929px"
                End If
                mloc += "sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                                + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
                                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
                If mids = "mm0" Then
                    sb.Append("<div style=""z-index: 3; position: absolute; top: 43px; left: " & lft & ";BORDER-RIGHT: gray 1px dotted;BORDER-TOP: gray 1px dotted;BORDER-BOTTOM: gray 1px dotted;BORDER-LEFT: gray 1px dotted"" ")
                    sb.Append("id=""" & mids & """ onmouseover=""showmenu('" & hicon & "','" & mids & "','" & imapp & "');"" ")
                    sb.Append("onmouseleave=""hidemenu_mm();"">") '
                    sb.Append("<table cellspacing=""0"" cellpadding=""1"">")
                    sb.Append("<tr height=""24px"" >")
                    lclass = "plainlabelau"
                    sb.Append("<td width=""20""><a href=""../mainmenu/NewMainMenu2.aspx?" & mloc & """><img src=""" & micon & """ border=""0""></a></td>")
                    sb.Append("<td width=""88"" >")
                    sb.Append("<a href=""../mainmenu/NewMainMenu2.aspx?" & mloc & """ class=""" & lclass & """>" & mname & "</a></td>")

                    sb.Append("</tr>")
                    sb.Append("</table>")
                    sb.Append("</div>")
                Else
                    sb.Append("<div style=""z-index: 3; position: absolute; top: 43px; left: " & lft & ";BORDER-RIGHT: gray 1px dotted;BORDER-TOP: gray 1px dotted;BORDER-BOTTOM: gray 1px dotted;BORDER-LEFT: gray 1px dotted"" ")
                    sb.Append("id=""" & mids & """ onmouseover=""showmenu('" & hicon & "','" & mids & "','" & imapp & "');"" ")
                    If mids = "mm1" Then
                        sb.Append("onmouseleave=""hidemenu_m1();"">")
                    ElseIf mids = "mm2" Then
                        sb.Append("onmouseleave=""hidemenu_m2();"">")
                    ElseIf mids = "mm3" Then
                        sb.Append("onmouseleave=""hidemenu_m3();"">")
                    End If

                    sb.Append("<table cellspacing=""0"" cellpadding=""1"">")
                    sb.Append("<tr height=""24px"" >")

                    lclass = "plbox"
                    If mids = "mm3" Then
                        sb.Append("<td width=""20""><img src=""" & micon & """ id=""" & imapp & """></td>")
                        sb.Append("<td width=""114"" class=""plainlabel"">" & mname & "</td>")
                    ElseIf mids = "mm2" Then
                        sb.Append("<td width=""20""><img src=""" & micon & """ id=""" & imapp & """></td>")
                        sb.Append("<td width=""108"" class=""plainlabel"">" & mname & "</td>")
                    Else
                        sb.Append("<td width=""20""><img src=""" & micon & """ id=""" & imapp & """></td>")
                        sb.Append("<td width=""88"" class=""plainlabel"">" & mname & "</td>")
                    End If


                    sb.Append("</tr>")
                    sb.Append("</table>")
                    sb.Append("</div>")
                End If
            Next
        End If





        'Sub Menus
        Dim atleftmain As String
        If ptst = "1" Then
            atleftmain = "558px"
        Else
            atleftmain = "534px"
        End If
        Dim appflag As Integer = 0
        'Main
        sb.Append("<iframe class=""details"" id=""ifmains"" ")
        sb.Append("style=""height: 100px; width: 180px; z-index: 3; position: absolute; top: 72px; left: " & atleftmain & "; MARGIN: 0px; BORDER-BOTTOM: blue 1px solid;"" frameBorder=""no""></iframe>")
        sb.Append("<div style=""z-index: 4; position: absolute; top: 71px; left:" & atleftmain & """" & " id=""tdmains"" ")
        sb.Append("class=""details"" onmouseleave=""hidemenu3();"">")
        sb.Append("<table ")
        sb.Append("cellspacing=""0"" cellpadding=""0"">")
        Dim drS0 As DataRow

        For Each drS0 In dts.Select("menuid = 0")
            micon = drS0("appicon").ToString
            dicon = drS0("disicon").ToString
            mname = drS0("appname").ToString
            mapp = drS0("app").ToString
            imapp = "i" & mapp
            mapp = "sm" & mapp
            hicon = drS0("hovicon").ToString
            hdicon = drS0("hovdisicon").ToString
            mloc = drS0("apploc").ToString
            Dim mlocs As String = ""
            Dim locindx As Integer = mloc.IndexOf("?")
            If locindx = -1 And mapp <> "smoscroll" Then
                'mloc += "?&sid=" + df + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro
                mloc += "?sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
            ElseIf locindx <> -1 And mapp <> "smoscroll" Then
                'mloc += "&sid=" + df + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro
                mloc += "&sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
            Else
                Dim retloc As String = "&sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
               + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
               + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
               + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
               + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
               + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
               + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
                mlocs = mloc + "&sid=" + df + "&retval=" + retloc.Replace("&", "~") + "&date=" + Now

            End If

            act = drS0("active").ToString
            If mapp = "smrep" Then
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""getreport()""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""158"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""158"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If


            ElseIf mapp = "smoscroll" Then
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""gotolocnew2('" & mlocs & "')""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""158"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""158"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If
            ElseIf mapp = "smogall" Then
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""getogall('" & df & "','" & usrname & "')""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""158"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""158"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If
            Else


                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""gotolocnew2('" & mloc & "')""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""158"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hdicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""158"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If
            End If

        Next
        sb.Append("</table>")
        sb.Append("</div>")

        'Admin
        Dim atleftadmin As String
        If ptst = "1" Then
            atleftadmin = "678px"
        Else
            atleftadmin = "652px"
        End If

        Dim ahgt As Integer = dts.Select("menuid = 1").Length
        ahgt = ahgt * 20
        sb.Append("<iframe class=""details"" id=""ifadmin"" ")
        sb.Append("style=""height: " & ahgt & "px; width: 154px; z-index: 3; position: absolute; top: 72px; left: " & atleftadmin & "; MARGIN: 0px; BORDER-BOTTOM: blue 1px solid;"" frameBorder=""no""></iframe>")
        sb.Append("<div style=""z-index: 4; position: absolute; top: 71px; left: " & atleftadmin & """" & " id=""tdadmin"" ")
        sb.Append("class=""details"" onmouseleave=""hidemenu3();"">")
        sb.Append("<table ")
        sb.Append("cellspacing=""0"" cellpadding=""0"">")
        Dim drS1 As DataRow
        For Each drS1 In dts.Select("menuid = 1")
            micon = drS1("appicon").ToString
            mname = drS1("appname").ToString
            mapp = drS1("app").ToString
            imapp = "i" & mapp
            mapp = "sm" & mapp
            hicon = drS1("hovicon").ToString
            mloc = drS1("apploc").ToString
            Dim locindx As Integer = mloc.IndexOf("?")
            If locindx = -1 Then
                'mloc += "?&sid=" + df + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro
                mloc += "?sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
            Else
                'mloc += "&sid=" + df + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro
                mloc += "&sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
            End If
            act = drS1("active").ToString
            If act = "yes" Then
                sb.Append("<tr height=""24px"" >")
                sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""gotolocnew2('" & mloc & "')""><table><tr>")
                sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                sb.Append("<td class=""plainlabel"" width=""122"">" & mname & "</td>")
                sb.Append("</tr></table></div></td>")
                sb.Append("</tr>")
            Else
                sb.Append("<tr height=""24px"" >")
                sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');""><table><tr>")
                sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                sb.Append("<td class=""plainlabelgray"" width=""122"">" & mname & "</td>")
                sb.Append("</tr></table></div></td>")
                sb.Append("</tr>")
            End If
        Next
        sb.Append("</table>")
        sb.Append("</div>")

        'Add-ons

        Dim atleftadd As String
        If ptst = "1" Then
            atleftadd = "788px"
        Else
            atleftadd = "768px"
        End If

        sb.Append("<iframe class=""details"" id=""ifadd"" ")
        sb.Append("style=""height: 100px; width: 174px; z-index: 3; position: absolute; top: 72px; left: " & atleftadd & "; MARGIN: 0px; BORDER-BOTTOM: blue 1px solid;"" frameBorder=""no""></iframe>")
        sb.Append("<div style=""z-index: 4; position: absolute; top: 71px; left: " & atleftadd & """" & " id=""tdadd"" ")
        sb.Append("class=""details"" onmouseleave=""hidemenu3();"">")
        sb.Append("<table ")
        sb.Append("cellspacing=""0"" cellpadding=""0"">")
        Dim drS2 As DataRow
        For Each drS2 In dts.Select("menuid = 2")
            micon = drS2("appicon").ToString
            mname = drS2("appname").ToString
            mapp = drS2("app").ToString
            imapp = "i" & mapp
            mapp = "sm" & mapp
            hicon = drS2("hovicon").ToString
            mloc = drS2("apploc").ToString
            Dim locindx As Integer = mloc.IndexOf("?")
            If locindx = -1 Then
                'mloc += "?&sid=" + df + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro
                mloc += "?sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
            Else
                'mloc += "&sid=" + df + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro
                mloc += "&sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                + usrname + "&uid=" + uid + "&sid=" + df + "&psite=" + psite + "&ms=" + ms + "&Logged_In=" _
                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper
            End If
            act = drS2("active").ToString
            If mapp = "smsch" Then
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""getsched('" & df & "','" & userid & "','" & usrname & "','" & islabor & "','" & isplanner & "','" & issuper & "','" & Logged_In & "','" & ro & "','" & ms & "','" + appstr + "')""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If
            ElseIf mapp = "smmea" Then
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""getmeas()""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If
            ElseIf mapp = "smfwp" Then
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""getfwp('" & df & "','" & userid & "','" & usrname & "','" & islabor & "','" & isplanner & "','" & issuper & "','" & Logged_In & "','" & ro & "','" & ms & "','" + appstr + "')""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If
            ElseIf mapp = "smsdp" Then
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""getsdp('" & df & "','" & userid & "','" & usrname & "','" & islabor & "','" & isplanner & "','" & issuper & "','" & Logged_In & "','" & ro & "','" & ms & "','" + appstr + "')""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If
            ElseIf mapp = "smwob" Then
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""getwob('" & df & "','" & userid & "','" & usrname & "','" & islabor & "','" & isplanner & "','" & issuper & "','" & Logged_In & "','" & ro & "','" & ms & "','" + appstr + "')""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" ><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If
            Else
                If act = "yes" Then
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""gotolocnew2('" & mloc & "')""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                    sb.Append("<td class=""plainlabel"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr height=""24px"" >")
                    sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');""><table><tr>")
                    sb.Append("<td width=""22""><img src=""" & dicon & """ id=""" & imapp & """ class=""smdis""></td>")
                    sb.Append("<td class=""plainlabelgray"" width=""142"">" & mname & "</td>")
                    sb.Append("</tr></table></div></td>")
                    sb.Append("</tr>")
                End If

            End If

        Next
        sb.Append("</table>")
        sb.Append("</div>")

        'Sites

        If pmadmin = "yes" Then
            dtscnt = mutils.GetPSCNT
            'dtscnt = 20
        End If

        Dim atleftst As String
        If ptst = "1" Then
            atleftst = "828px"
        Else
            atleftst = "804px"
        End If

        '***make gradient2.gif bigger if sites are added***
        sb.Append("<iframe class=""details"" id=""ifsites"" ")
        sb.Append("style=""height: 24px; width: 262px; z-index: 3; position: absolute; top: 72px;  overflow: scroll; left: " & atleftst & "; MARGIN: 0px; BORDER-BOTTOM: blue 1px solid;"" frameBorder=""no""></iframe>")
        If dtscnt <= 15 Then
            sb.Append("<div style=""width: 262px; z-index: 4; position: absolute; top: 71px; left: " & atleftst & """" & " id=""tdsites"" ")
            sb.Append("class=""details"" onmouseleave=""hidemenu3();"">")
        Else
            sb.Append("<div style=""width: 262px; height: 340px; z-index: 4; position: absolute;  overflow: scroll; top: 71px; left: " & atleftst & """" & " id=""tdsites"" ")
            sb.Append("class=""details"" onmouseleave=""hidemenu3();"">")
        End If

        sb.Append("<table width=""242"" ")
        sb.Append("cellspacing=""0"" cellpadding=""0"">")
        Dim drS3 As DataRow
        For Each drS3 In dts.Select("menuid = 3")
            micon = drS3("appicon").ToString
            mname = drS3("appname").ToString
            mapp = drS3("app").ToString
            imapp = "i" & mapp
            mapp = "sm" & mapp
            hicon = drS3("hovicon").ToString
            '& "?cps=yes&psid=" & myRow("siteid").ToString() & "&psite=" & myRow("subMenuTitle").ToString() & "&tli=5&jump=no
            Dim sitearr() As String = usites.Split(",")
            Dim u As Integer
            Dim siteu As String = "no"
            If pmadmin = "yes" Then
                siteu = "ok"
            Else
                For u = 0 To sitearr.Length - 1
                    If sitearr(u) = drS3("app").ToString Then
                        siteu = "ok"
                        Exit For
                    End If
                Next
            End If
            If siteu = "ok" Then
                'mloc = cpg & "?cps=yes&psid=" & drS3("app").ToString & "&psite=" & mname & "&tli=5&jump=no"
                cpg = "../mainmenu/NewMainMenu2.aspx"


                'mloc += "?&sid=" + df + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&ro=" + ro
                mloc = cpg + "?sessid=" + sessid + "&comp=" + comp + "&compname=" + compname + "&userlevel=" _
                + userlevel + "&cadm=" + cadm + "&grpndx=" + grpndx + "&optsort=" + optsort + "&usrname=" _
                + usrname + "&uid=" + uid + "&sid=" + drS3("app").ToString + "&psite=" + mname + "&ms=" + ms + "&Logged_In=" _
                + Logged_In + "&ua=" + ua + "&apps=" + apps + "&appstr=" + appstr + "&email=" + email + "&practice=" _
                + practice + "&pmadmin=" + pmadmin + "&ro=" + ro + "&rostr=" + rostr + "&apprgrp=" _
                + apprgrp + "&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&islabor=" _
                + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper

                sb.Append("<tr height=""24px"" >")
                sb.Append("<td><div id=""" & mapp & """ onmouseover=""showsubmenu('" & hicon & "','" & mapp & "','" & imapp & "');"" onclick=""gotolocnew2('" & mloc & "')""><table><tr>")
                sb.Append("<td width=""22""><img src=""" & micon & """ id=""" & imapp & """></td>")
                sb.Append("<td class=""plainlabel"" width=""222"">" & mname & "</td>")
                sb.Append("</tr></table></div></td>")
                sb.Append("</tr>")
            End If
        Next
        sb.Append("</table>")
        sb.Append("</div>")

        sb.Append("<iframe id=""ifsession"" class=""details"" src="" frameBorder=""no"" runat=""server"" style=""z-index: 0;""></iframe>")

        sb.Append("<script type=""text/javascript"">")
        sb.Append("document.getElementById(""ifsession"").src=""../session.aspx?who=mm"";")
        sb.Append("</script>")

        sb.Append("<input type=""hidden"" id=""lbldfhold"" value=""" & df_hold & """><input type=""hidden"" id=""lblsubmit"" runat=""server"">")
        sb.Append("<input type=""hidden"" id=""lblnpb"" value=""" & npd & """>")


        Return sb.ToString()
    End Function
    Private Sub getsess()
        Dim chk As String = lblsessid.Value
        If chk <> "" Then
            Try
                sessid = lblsessid.Value
            Catch ex As Exception
                sessid = HttpContext.Current.Session.SessionID
                Session("sess") = sessid
                lblsessid.Value = sessid
            End Try

            Try
                comp = lblcomp.Value
            Catch ex As Exception
                comp = HttpContext.Current.Session("comp").ToString
                Session("comp") = comp
                lblcomp.Value = comp
            End Try

            Try
                compname = lblcompname.Value
            Catch ex As Exception
                compname = HttpContext.Current.Session("compname").ToString
                Session("compname") = compname
                lblcompname.Value = compname
            End Try

            Try
                userlevel = lbluserlevel.Value
            Catch ex As Exception
                userlevel = HttpContext.Current.Session("userlevel").ToString
                Session("userlevel") = userlevel
                lbluserlevel.Value = userlevel
            End Try

            Try
                cadm = lblcadm.Value
            Catch ex As Exception
                cadm = HttpContext.Current.Session("cadm").ToString
                Session("cadm") = cadm
                lblcadm.Value = cadm
            End Try

            Try
                grpndx = lblgrpndx.Value
            Catch ex As Exception
                Try
                    grpndx = HttpContext.Current.Session("grpndx").ToString
                    Session("grpndx") = grpndx
                Catch ex1 As Exception
                    Session("grpndx") = "1"
                    grpndx = "1"
                End Try
                lblgrpndx.Value = grpndx
            End Try

            Try
                optsort = lbloptsort.Value
            Catch ex As Exception
                Try
                    optsort = HttpContext.Current.Session("optsort").ToString
                    Session("optsort") = optsort
                Catch ex1 As Exception
                    Session("optsort") = "tasknum"
                    optsort = "tasknum"
                End Try
                lbloptsort.Value = optsort
            End Try

            Try
                usrname = lblusrname.Value
            Catch ex As Exception
                usrname = HttpContext.Current.Session("username").ToString
                Session("username") = usrname
                lblusrname.Value = usrname
            End Try

            Try
                uid = lbluid.Value
            Catch ex As Exception
                uid = HttpContext.Current.Session("uid").ToString
                Session("uid") = uid
                lbluid.Value = uid
            End Try

            Try
                df = lbldf.Value
            Catch ex As Exception
                df = HttpContext.Current.Session("dfltps").ToString
                Session("dfltps") = df
                lbldf.Value = df
            End Try

            Try
                psite = lblpsite.Value
            Catch ex As Exception
                psite = HttpContext.Current.Session("psite").ToString
                Session("psite") = psite
                lblpsite.Value = psite
            End Try

            Try
                ms = lblms.Value
            Catch ex As Exception
                ms = HttpContext.Current.Session("ms").ToString
                Session("ms") = ms
                lblms.Value = ms
            End Try

            Try
                Logged_In = lblLogged_In.Value
            Catch ex As Exception
                Logged_In = HttpContext.Current.Session("Logged_In").ToString
                Session("Logged_In") = Logged_In
                lblLogged_In.Value = Logged_In
            End Try

            Try
                ua = lblua.Value
            Catch ex As Exception
                ua = HttpContext.Current.Session("ua").ToString
                Session("ua") = ua
                lblua.Value = ua
            End Try

            Try
                apps = lblapps.Value
            Catch ex As Exception
                Try
                    apps = HttpContext.Current.Session("app").ToString
                    Session("app") = app
                Catch ex1 As Exception
                    Session("app") = ""
                    apps = ""
                End Try
                lblapps.Value = apps
            End Try

            Try
                appstr = lblappstr.Value
            Catch ex As Exception
                appstr = HttpContext.Current.Session("appstr").ToString
                Session("appstr") = appstr
                lblappstr.Value = appstr
            End Try

            Try
                email = lblemail.Value
            Catch ex As Exception
                Try
                    email = HttpContext.Current.Session("email").ToString
                    Session("email") = email
                Catch ex1 As Exception
                    Session("email") = ""
                    email = ""
                End Try
                lblemail.Value = email
            End Try

            Try
                practice = lblpractice.Value
            Catch ex As Exception
                practice = HttpContext.Current.Session("practice").ToString
                Session("practice") = practice
                lblpractice.Value = practice
            End Try

            Try
                pmadmin = lblpmadmin.Value
            Catch ex As Exception
                pmadmin = HttpContext.Current.Session("pmadmin").ToString
                Session("pmadmin") = pmadmin
                lblpmadmin.Value = pmadmin
            End Try

            Try
                ro = lblro.Value
            Catch ex As Exception
                ro = HttpContext.Current.Session("ro").ToString
                Session("ro") = ro
                lblro.Value = ro
            End Try

            Try
                rostr = lblrostr.Value
            Catch ex As Exception
                rostr = HttpContext.Current.Session("rostr").ToString
                Session("rostr") = rostr
                lblrostr.Value = rostr
            End Try

            Try
                apprgrp = lblapprgrp.Value
            Catch ex As Exception
                apprgrp = HttpContext.Current.Session("apprgrp").ToString
                Session("apprgrp") = apprgrp
                lblapprgrp.Value = apprgrp
            End Try

            Try
                hdr = lblhdr.Value
            Catch ex As Exception
                hdr = HttpContext.Current.Session("hdr").ToString
                Session("hdr") = hdr
                lblhdr.Value = hdr
            End Try

            Try
                mb = lblmb.Value
            Catch ex As Exception
                mb = HttpContext.Current.Session("mb").ToString
                Session("mb") = mb
                lblmb.Value = mb
            End Try

            Try
                mbg = lblmbg.Value
            Catch ex As Exception
                mbg = HttpContext.Current.Session("mbg").ToString
                Session("mbg") = mbg
                lblmbg.Value = mbg
            End Try

            Try
                islabor = lblislabor.Value
            Catch ex As Exception
                islabor = HttpContext.Current.Session("islabor").ToString
                Session("islabor") = islabor
                lblislabor.Value = islabor
            End Try

            Try
                isplanner = lblisplanner.Value
            Catch ex As Exception
                isplanner = HttpContext.Current.Session("isplanner").ToString
                Session("isplanner") = isplanner
                lblisplanner.Value = isplanner
            End Try

            Try
                issuper = lblissuper.Value
            Catch ex As Exception
                issuper = HttpContext.Current.Session("issuper").ToString
                Session("issuper") = issuper
                lblissuper.Value = issuper
            End Try

            Try
                curlang = lblcurlang.Value
            Catch ex As Exception
                Try
                    curlang = HttpContext.Current.Session("issuper").ToString
                    Session("curlang") = curlang
                    lblcurlang.Value = curlang
                Catch ex1 As Exception
                    Try
                        Dim dlang As New mmenu_utils_a
                        lblcurlang.Value = dlang.AppDfltLang
                    Catch ex2 As Exception

                    End Try
                End Try
            End Try
        Else
            getsess2()
        End If

    End Sub
    Private Sub getsess2()
        Try
            sessid = HttpContext.Current.Session.SessionID
            Session("sess") = sessid
            lblsessid.Value = sessid
        Catch ex As Exception

        End Try

        Try
            comp = HttpContext.Current.Session("comp").ToString
            Session("comp") = comp
            lblcomp.Value = comp
        Catch ex As Exception

        End Try

        Try
            compname = HttpContext.Current.Session("compname").ToString
            Session("compname") = compname
            lblcompname.Value = compname
        Catch ex As Exception

        End Try

        Try
            userlevel = HttpContext.Current.Session("userlevel").ToString
            Session("userlevel") = userlevel
            lbluserlevel.Value = userlevel
        Catch ex As Exception

        End Try

        Try
            cadm = HttpContext.Current.Session("cadm").ToString
            Session("cadm") = cadm
            lblcadm.Value = cadm
        Catch ex As Exception

        End Try

        Try
            Try
                grpndx = HttpContext.Current.Session("grpndx").ToString
                Session("grpndx") = grpndx
            Catch ex1 As Exception
                Session("grpndx") = "1"
                grpndx = "1"
            End Try
            lblgrpndx.Value = grpndx
        Catch ex As Exception

        End Try

        Try
            Try
                optsort = HttpContext.Current.Session("optsort").ToString
                Session("optsort") = optsort
            Catch ex1 As Exception
                Session("optsort") = "tasknum"
                optsort = "tasknum"
            End Try
            lbloptsort.Value = optsort
        Catch ex As Exception

        End Try

        Try
            usrname = HttpContext.Current.Session("username").ToString
            Session("username") = usrname
            lblusrname.Value = usrname
        Catch ex As Exception

        End Try

        Try
            uid = HttpContext.Current.Session("uid").ToString
            Session("uid") = uid
            lbluid.Value = uid
        Catch ex As Exception

        End Try

        Try
            df = HttpContext.Current.Session("dfltps").ToString
            Session("dfltps") = df
            lbldf.Value = df
        Catch ex As Exception

        End Try

        Try
            psite = HttpContext.Current.Session("psite").ToString
            Session("psite") = psite
            lblpsite.Value = psite
        Catch ex As Exception

        End Try

        Try
            ms = HttpContext.Current.Session("ms").ToString
            Session("ms") = ms
            lblms.Value = ms
        Catch ex As Exception

        End Try

        Try
            Logged_In = HttpContext.Current.Session("Logged_In").ToString
            Session("Logged_In") = Logged_In
            lblLogged_In.Value = Logged_In
        Catch ex As Exception

        End Try

        Try
            ua = HttpContext.Current.Session("ua").ToString
            Session("ua") = ua
            lblua.Value = ua
        Catch ex As Exception

        End Try

        Try
            Try
                apps = HttpContext.Current.Session("app").ToString
                Session("app") = app
            Catch ex1 As Exception
                Session("app") = ""
                apps = ""
            End Try
            lblapps.Value = apps
        Catch ex As Exception

        End Try

        Try
            appstr = HttpContext.Current.Session("appstr").ToString
            Session("appstr") = appstr
            lblappstr.Value = appstr
        Catch ex As Exception

        End Try

        Try
            Try
                email = HttpContext.Current.Session("email").ToString
                Session("email") = email
            Catch ex1 As Exception
                Session("email") = ""
                email = ""
            End Try
            lblemail.Value = email
        Catch ex As Exception

        End Try

        Try
            practice = HttpContext.Current.Session("practice").ToString
            Session("practice") = practice
            lblpractice.Value = practice
        Catch ex As Exception

        End Try

        Try
            pmadmin = HttpContext.Current.Session("pmadmin").ToString
            Session("pmadmin") = pmadmin
            lblpmadmin.Value = pmadmin
        Catch ex As Exception

        End Try

        Try
            ro = HttpContext.Current.Session("ro").ToString
            Session("ro") = ro
            lblro.Value = ro
        Catch ex As Exception

        End Try

        Try
            rostr = HttpContext.Current.Session("rostr").ToString
            Session("rostr") = rostr
            lblrostr.Value = rostr
        Catch ex As Exception

        End Try

        Try
            apprgrp = HttpContext.Current.Session("apprgrp").ToString
            Session("apprgrp") = apprgrp
            lblapprgrp.Value = apprgrp
        Catch ex As Exception

        End Try

        Try
            hdr = HttpContext.Current.Session("hdr").ToString
            Session("hdr") = hdr
            lblhdr.Value = hdr
        Catch ex As Exception

        End Try

        Try
            mb = HttpContext.Current.Session("mb").ToString
            Session("mb") = mb
            lblmb.Value = mb
        Catch ex As Exception

        End Try

        Try
            mbg = HttpContext.Current.Session("mbg").ToString
            Session("mbg") = mbg
            lblmbg.Value = mbg
        Catch ex As Exception

        End Try

        Try
            islabor = HttpContext.Current.Session("islabor").ToString
            Session("islabor") = islabor
            lblislabor.Value = islabor
        Catch ex As Exception

        End Try

        Try
            isplanner = HttpContext.Current.Session("isplanner").ToString
            Session("isplanner") = isplanner
            lblisplanner.Value = isplanner
        Catch ex As Exception

        End Try

        Try
            issuper = HttpContext.Current.Session("issuper").ToString
            Session("issuper") = issuper
            lblissuper.Value = issuper
        Catch ex As Exception

        End Try

        Try
            curlang = HttpContext.Current.Session("curlang").ToString
            Session("curlang") = curlang
            lblcurlang.Value = curlang
        Catch ex As Exception
            Try
                Dim dlang As New mmenu_utils_a
                lblcurlang.Value = dlang.AppDfltLang
            Catch ex1 As Exception

            End Try
        End Try

    End Sub
End Class
