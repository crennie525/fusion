<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="mmenu1.ascx.vb" Inherits="lucy_r12.mmenu1"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<html>
<head>
    <!--<asp:literal id="menulit" runat="server" EnableViewState="False"></asp:literal>-->
    <script language="javascript" type="text/javascript" src="../scripts/mmenu11ka.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function gotolocnew2(loc) {
            //alert(loc)
            window.location = loc;
        }

        var sideflag = 0;
        var mainflag = 0;
        var subflag = 0;
        var hovflag = 0;
        var delay = 100;
        var timeout;

        var myclose = false;
        function checkmenus() {
            hideall();
            closemhov();

        }

        function checkit() {
            //var sid = document.getElementById("lblnpd").value;
            //if(sid=="yes") {
            //alert(sid)
            //}
        }
        function startsess() {

        }
        function resetsess() {
            document.getElementById("ifsession").src = "../session.aspx?who=mm";
            var stimer = window.setTimeout("resetsess();", 720000);
        }

        function hidemenu_mmT() {
            //alert() 
        }

        function ConfirmClose() {
            if (event.clientY < 0) {
                setTimeout('myclose=false', 100);
                myclose = true;
            }
        }

        function HandleOnClose() {
            if (myclose == true) {
                //alert("Window is closed");
                document.getElementById("form1").submit();
            }
        }


        function getopts(cpg, urlname, uid, hopt, mbg) {
            var eReturn = window.showModalDialog("../menu/menuoptsdialog.aspx?cpg=" + cpg + "&urlname=" + urlname + "&uid=" + uid + "&hopt=" + hopt + "&mbg=" + mbg + "&date=" + Date(), "", "dialogHeight:300px; dialogWidth:830px; resizable=yes");
            if (eReturn) {
                window.location = eReturn;
            }
        }

        function gotoloc(loc) {
            //alert("this is gotoloc")
            window.location = loc;
        }

        function gotolocnew(loc) {
            window.location = loc;
        }


        function getsched(sid, userid, usrname, islabor, isplanner, issuper, Logged_In, ro, ms, appstr) {
            window.location = "../appsman/PMScheduling2.aspx?jump=no&typ=main&sid=" + sid + "&uid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        //'" & df & "','" & userid & "','" & usrname & "','" & islabor & "','" & isplanner & "','" & issuper & "'
        // & "','" & Logged_In & "','" & ro & "','" & ms & "','" & uid & "','" & usrname & "')""><table><tr>")
        function getfwp(sid, userid, usrname, islabor, isplanner, issuper, Logged_In, ro, ms, appstr) {
            window.location = "../appswo/wopmlist3.aspx?who=main&sid=" + sid + "&uid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function getsdp(sid, userid, usrname, islabor, isplanner, issuper, Logged_In, ro, ms, appstr) {
            window.location = "../appswo/wopmlist7.aspx?who=main&sid=" + sid + "&uid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function getwob(sid, userid, usrname, islabor, isplanner, issuper, Logged_In, ro, ms, appstr) {
            window.location = "../appswo/wobacklog.aspx?who=main&sid=" + sid + "&uid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "&issuper=" + issuper + "&Logged_In=" + Logged_In + "&ro=" + ro + "&ms=" + ms + "&appstr=" + appstr;
        }
        function getprofile() {

            var eReturn = window.showModalDialog("../security/EditProfileDialog.aspx?date=" + Date(), "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
        }
        function getreport() {
            var dflt = document.getElementById("lbldfhold").value;
            var ht = "1000px";
            var wd = "2000px";
            var eReturn = window.showModalDialog("../reports/reportdialog.aspx?dflt=" + dflt + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
            if (eReturn) {
                if (eReturn == "1") {
                    window.location = "../NewLogin.aspx";
                }
                else {
                    var ret = eReturn.split(",")
                    if (ret[0] == "href") {
                        window.location = ret[1];
                    }
                }
            }
        }

        function getogall(sid, usrname) {
            /*
            var ht = "4000px";
            var wd = "2000px";
            var eReturn = window.showModalDialog("../appspmo123grid/pmo123e2dialog.aspx?sid=" + sid + "&usrname=" + usrname + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
            if (eReturn) {
            if (eReturn == "1") {
            window.location = "../NewLogin.aspx";
            }
            else {
            var ret = eReturn.split(",")
            if (ret[0] == "href") {
            window.location = ret[1];
            }
            }
            }
            */
            var popwin = "directories=0,height=4000,width=3000,location=0,menubar=0,resizable=0,status=0,toolbar=0,scrollbars=1";
            window.open("../appspmo123grid/pmo123e2.aspx?sid=" + sid + "&usrname=" + usrname + "&date=" + Date(), "repWin", popwin)
        }
        function getwom() {
            var dflt = document.getElementById("lbldfhold").value;
            var ht = "1000px";
            var wd = "2000px";
            var eReturn = window.showModalDialog("../appswo/wolabmain.aspx?dflt=" + dflt + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
            if (eReturn) {
                if (eReturn == "1") {
                    window.location = "../NewLogin.aspx";
                }
            }
        }

        function getmeas() {
            var dflt = document.getElementById("lbldfhold").value;
            var ht = screen.Height - 20;
            var wd = screen.Width - 20;
            var eReturn = window.showModalDialog("../measout/measuremaindialog.aspx?dflt=" + dflt + "&date=" + Date(), "", "dialogHeight:1000px; dialogWidth:2000px;resizable=yes");
            if (eReturn) {
                if (eReturn == "1") {
                    window.location = "../NewLogin.aspx";
                }
            }
        }

        function gethelp() {
            //alert("gethelp");
            window.open("../docs/PM3_Optimization_software_manual.pdf");
        }


        function hidemenu_mm() {
            setTimeout('hideit_mm()', delay);
        }
        function hidemenu_m1() {
            setTimeout('hideit_m1()', delay);
        }
        function hidemenu_m2() {
            setTimeout('hideit_m2()', delay);
        }
        function hidemenu_m3() {
            setTimeout('hideit_m3()', delay);
        }

        function hidemenu3() {
            setTimeout('hideit3()', delay);
        }

        function hideit_mm() {
            if (mainflag == 1 && subflag == 0) {
                sideflag = 0;
                mainflag = 0;
                subflag = 0;
                document.getElementById("tdmains").className = "details";
                document.getElementById("ifmains").className = "details";
                document.getElementById("mm0").className = "";
                closeshov();
            }
        }
        function hideit_m1() {
            if (mainflag == 1 && subflag == 0) {
                sideflag = 0;
                mainflag = 0;
                subflag = 0;
                document.getElementById("tdadmin").className = "details";
                document.getElementById("ifadmin").className = "details";
                document.getElementById("mm1").className = "";
                var ig1 = document.getElementById("im1");
                if (ig1.className == "smdis") {
                    document.getElementById("im1").src = "../images/appbuttons/minibuttons/admind.gif";
                }
                closeshov();
            }
        }
        function hideit_m2() {
            if (mainflag == 1 && subflag == 0) {
                sideflag = 0;
                mainflag = 0;
                subflag = 0;
                document.getElementById("tdadd").className = "details";
                document.getElementById("ifadd").className = "details";
                document.getElementById("mm2").className = "";
                closeshov();
            }
        }
        function hideit_m3() {
            if (mainflag == 1 && subflag == 0) {
                sideflag = 0;
                mainflag = 0;
                subflag = 0;
                document.getElementById("tdsites").className = "details";
                document.getElementById("ifsites").className = "details";
                document.getElementById("mm3").className = "";
                var ig = document.getElementById("im3")
                if (ig.className == "smdis") {
                    document.getElementById("im3").src = "../images/appbuttons/minibuttons/globed.gif";
                }
                else if (ig.className == "smreg") {
                    document.getElementById("im3").src = "../images/appbuttons/minibuttons/globel.gif";
                }
                else {
                    document.getElementById("im3").src = "../images/appbuttons/minibuttons/globe.gif";
                }
                closeshov();
            }
        }

        function hideit3() {
            if (mainflag == 0 && subflag == 1) {
                sideflag = 0;
                mainflag = 0;
                subflag = 0;

                hideall();
                document.getElementById("mm0").className = "";
                document.getElementById("mm1").className = "";
                var ig1 = document.getElementById("im1");

                if (ig1.className == "smdis") {
                    document.getElementById("im1").src = "../images/appbuttons/minibuttons/admind.gif";
                }
                document.getElementById("mm2").className = "";
                document.getElementById("mm3").className = "";

                var ig = document.getElementById("im3")

                if (ig.className == "smdis") {
                    document.getElementById("im3").src = "../images/appbuttons/minibuttons/globed.gif";
                }
                else if (ig.className == "smreg") {
                    document.getElementById("im3").src = "../images/appbuttons/minibuttons/globe1.gif";
                }
                else {
                    document.getElementById("im3").src = "../images/appbuttons/minibuttons/globe.gif";
                }
                //document.getElementById("i3").src="../images/appbuttons/minibuttons/globe.gif";
                closeshov();
            }
        }
        function hideall() {
            document.getElementById("tdmains").className = "details";
            document.getElementById("ifmains").className = "details";

            document.getElementById("tdadmin").className = "details";
            document.getElementById("ifadmin").className = "details";

            document.getElementById("tdadd").className = "details";
            document.getElementById("ifadd").className = "details";

            document.getElementById("tdsites").className = "details";
            document.getElementById("ifsites").className = "details";
        }

        function showmenu(id, mid, imapp) {
            mainflag = 1;
            subflag = 0;
            hovflag = 1;
            hideall();
            closemhov();
            if (mid == "mm0") {
                document.getElementById(mid).className = "btover";
                document.getElementById("tdmains").className = "btmmenu plainlabel view";
                document.getElementById("ifmains").className = "view";
            }
            else if (mid == "mm1") {
                document.getElementById(mid).className = "btover";
                document.getElementById(imapp).src = id;
                document.getElementById("tdadmin").className = "btmmenu plainlabel view";
                document.getElementById("ifadmin").className = "view"; ;
            }
            else if (mid == "mm2") {
                document.getElementById(mid).className = "btover";
                document.getElementById("tdadd").className = "btmmenu plainlabel view";
                document.getElementById("ifadd").className = "view";
            }
            else if (mid == "mm3") {
                document.getElementById(mid).className = "btover";
                document.getElementById(imapp).src = id;
                document.getElementById("tdsites").className = "btmmenu plainlabel view";
                document.getElementById("ifsites").className = "view";
            }
        }

        function showsubmenu(id, mapp, imapp) {
            subflag = 1;
            mainflag = 0;
            hovflag = 0;
            closeshov();
            document.getElementById(mapp).className = "stover";
            document.getElementById(imapp).src = id;
        }

        //hover functions
        function closemhov() {
            document.getElementById("mm0").className = "";
            document.getElementById("mm1").className = "";
            var ig1 = document.getElementById("im1");
            if (ig1.className == "smdis") {
                document.getElementById("im1").src = "../images/appbuttons/minibuttons/admind.gif";
            }
            document.getElementById("mm2").className = "";
            document.getElementById("mm3").className = "";

            var ig = document.getElementById("im3");
            if (ig.className == "smdis") {
                document.getElementById("im3").src = "../images/appbuttons/minibuttons/globed.gif";
            }
            else if (ig.className == "smreg") {
                document.getElementById("im3").src = "../images/appbuttons/minibuttons/globe1.gif";
            }
            else {
                document.getElementById("im3").src = "../images/appbuttons/minibuttons/globe.gif";
            }
        }

        function closeshov() {
            var idev;
            try {
                document.getElementById("smotab").className = "";
                iotab = document.getElementById("iotab");
                if (iotab.className == "smdis") {
                    iotab.src = "../images/appbuttons/minibuttons/3gearstrans_dis.gif";
                }
                else {
                    iotab.src = "../images/appbuttons/minibuttons/3gearstrans.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smostep").className = "";
                iotab = document.getElementById("iostep");
                if (iostep.className == "smdis") {
                    iostep.src = "../images/appbuttons/minibuttons/3gearstrans_dis.gif";
                }
                else {
                    iostep.src = "../images/appbuttons/minibuttons/3gearstrans.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smoscroll").className = "";
                iotab = document.getElementById("ioscroll");
                if (ioscroll.className == "smdis") {
                    ioscroll.src = "../images/appbuttons/minibuttons/3gearstrans_dis.gif";
                }
                else {
                    ioscroll.src = "../images/appbuttons/minibuttons/3gearstrans.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smogall").className = "";
                iotab = document.getElementById("iogall");
                if (iogall.className == "smdis") {
                    iogall.src = "../images/appbuttons/minibuttons/3gearstrans_dis.gif";
                }
                else {
                    iogall.src = "../images/appbuttons/minibuttons/3gearstrans.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smdev").className = "";
                idev = document.getElementById("idev");
                if (idev.className == "smdis") {
                    idev.src = "../images/appbuttons/minibuttons/3gearstrans_dis.gif";
                }
                else {
                    idev.src = "../images/appbuttons/minibuttons/3gearstrans.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smopt").className = "";
                iopt = document.getElementById("iopt");
                if (iopt.className == "smdis") {
                    iopt.src = "../images/appbuttons/minibuttons/compress_dis.gif";
                }
                else {
                    iopt.src = "../images/appbuttons/minibuttons/compress.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smtpd").className = "";
                itpd = document.getElementById("itpd");
                if (itpd.className == "smdis") {
                    itpd.src = "../images/appbuttons/minibuttons/3gearstranstpm_dis.gif";
                }
                else {
                    itpd.src = "../images/appbuttons/minibuttons/3gearstranstpm.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smtpo").className = "";
                itpo = document.getElementById("itpo");
                if (itpo.className == "smdis") {
                    itpo.src = "../images/appbuttons/minibuttons/compresstpm_dis.gif";
                }
                else {
                    itpo.src = "../images/appbuttons/minibuttons/compresstpm.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smman").className = "";
                iman = document.getElementById("iman");
                if (iman.className == "smdis") {
                    iman.src = "../images/appbuttons/minibuttons/cal1_dis.gif";
                }
                else {
                    iman.src = "../images/appbuttons/minibuttons/cal1.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smpmj").className = "";
                iman = document.getElementById("ipmj");
                if (ipmj.className == "smdis") {
                    ipmj.src = "../images/appbuttons/minibuttons/cal1_dis.gif";
                }
                else {
                    ipmj.src = "../images/appbuttons/minibuttons/cal1.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smtan").className = "";
                itan = document.getElementById("itan");
                if (itan.className == "smdis") {
                    itan.src = "../images/appbuttons/minibuttons/cal1_dis.gif";
                }
                else {
                    itan.src = "../images/appbuttons/minibuttons/cal1.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smtpp").className = "";
                itpp = document.getElementById("itpp");
                if (itpp.className == "smdis") {
                    itpp.src = "../images/appbuttons/minibuttons/cal1_dis.gif";
                }
                else {
                    itpp.src = "../images/appbuttons/minibuttons/cal1.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smwo").className = "";
                iwo = document.getElementById("iwo");
                if (iwo.className == "smdis") {
                    iwo.src = "../images/appbuttons/minibuttons/wo_dis.gif";
                }
                else {
                    iwo.src = "../images/appbuttons/minibuttons/wo.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smwolfs").className = "";
                iwo = document.getElementById("iwolfs");
                if (iwolfs.className == "smdis") {
                    iwolfs.src = "../images/appbuttons/minibuttons/wo_dis.gif";
                }
                else {
                    iwolfs.src = "../images/appbuttons/minibuttons/wo.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smwrr").className = "";
                iwrr = document.getElementById("iwrr");
                if (iwrr.className == "smdis") {
                    iwrr.src = "../images/appbuttons/minibuttons/woreq_dis.gif";
                }
                else {
                    iwrr.src = "../images/appbuttons/minibuttons/woreq.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smprc").className = "";
                iprc = document.getElementById("iprc");
                if (iprc.className == "smdis") {
                    iprc.src = "../images/appbuttons/minibuttons/archtop1transpm_dis.gif";
                }
                else {
                    iprc.src = "../images/appbuttons/minibuttons/archtop1transpm.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smtrc").className = "";
                itrc = document.getElementById("itrc");
                if (itrc.className == "smdis") {
                    itrc.src = "../images/appbuttons/minibuttons/archtop1trans_dis.gif";
                }
                else {
                    itrc.src = "../images/appbuttons/minibuttons/archtop1trans.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smrep").className = "";
                irep = document.getElementById("irep");
                if (irep.className == "smdis") {
                    irep.src = "../images/appbuttons/minibuttons/lilreport1trans_dis.gif";
                }
                else {
                    irep.src = "../images/appbuttons/minibuttons/lilreport1trans.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smizenda").className = "";
                izrep = document.getElementById("iizenda");
                if (izrep.className == "smdis") {
                    izrep.src = "../images/appbuttons/minibuttons/lilreport1trans_dis.gif";
                }
                else {
                    izrep.src = "../images/appbuttons/minibuttons/lilreport1trans.gif";
                }
            }
            catch (err) {

            }

            //Admin
            try {
                document.getElementById("smsetup").className = "";
                isetup = document.getElementById("isetup");
                if (isetup.className == "smdis") {
                    isetup.src = "../images/appbuttons/minibuttons/projects_dis.gif";
                }
                else {
                    isetup.src = "../images/appbuttons/minibuttons/projects.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smeq").className = "";
                ieq = document.getElementById("ieq");
                if (ieq.className == "smdis") {
                    ieq.src = "../images/appbuttons/minibuttons/eqtrans_dis.gif";
                }
                else {
                    ieq.src = "../images/appbuttons/minibuttons/eqtrans.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smecr").className = "";
                iecr = document.getElementById("iecr");
                if (iecr.className == "smdis") {
                    iecr.src = "../images/appbuttons/minibuttons/calctrans_dis.gif";
                }
                else {
                    iecr.src = "../images/appbuttons/minibuttons/calctrans.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("sminv").className = "";
                iinv = document.getElementById("iinv");
                if (iinv.className == "smdis") {
                    iinv.src = "../images/appbuttons/minibuttons/invtrans_dis.gif";
                }
                else {
                    iinv.src = "../images/appbuttons/minibuttons/invtrans.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("sminc").className = "";
                iinc = document.getElementById("iinc");
                if (iinc.className == "smdis") {
                    iinc.src = "../images/appbuttons/minibuttons/invtrans_dis.gif";
                }
                else {
                    iinc.src = "../images/appbuttons/minibuttons/invtrans.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smua").className = "";
                iua = document.getElementById("iua");
                if (iua.className == "smdis") {
                    iua.src = "../images/appbuttons/minibuttons/magnifier_dis.gif";
                }
                else {
                    iua.src = "../images/appbuttons/minibuttons/magnifier.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smep").className = "";
                iep = document.getElementById("iep");
                if (iep.className == "smdis") {
                    iep.src = "../images/appbuttons/minibuttons/pencil_dis.gif";
                }
                else {
                    iep.src = "../images/appbuttons/minibuttons/pencil.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smasa").className = "";
                iasa = document.getElementById("iasa");
                if (iasa.className == "smdis") {
                    iasa.src = "../images/appbuttons/minibuttons/write_dis.gif";
                }
                else {
                    iasa.src = "../images/appbuttons/minibuttons/write.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smasam").className = "";
                iasam = document.getElementById("iasam");
                if (iasam.className == "smdis") {
                    iasam.src = "../images/appbuttons/minibuttons/magnifier_dis.gif";
                }
                else {
                    iasam.src = "../images/appbuttons/minibuttons/magnifier.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smasm").className = "";
                iasm = document.getElementById("iasm");
                if (iasam.className == "smdis") {
                    iasm.src = "../images/appbuttons/minibuttons/pencil_dis.gif";
                }
                else {
                    iasm.src = "../images/appbuttons/minibuttons/pencil.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smwra").className = "";
                iwra = document.getElementById("iwra");
                if (iwra.className == "smdis") {
                    iwra.src = "../images/appbuttons/minibuttons/woreq_dis.gif";
                }
                else {
                    iwra.src = "../images/appbuttons/minibuttons/woreq.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smpmps").className = "";
                iwra = document.getElementById("ipmps");
                if (iwra.className == "smdis") {
                    iwra.src = "../images/appbuttons/minibuttons/woreq_dis.gif";
                }
                else {
                    iwra.src = "../images/appbuttons/minibuttons/woreq.gif";
                }
            }
            catch (err) {

            }
            //Add-Ons
            try {
                document.getElementById("smlib").className = "";
                ilib = document.getElementById("ilib");
                if (ilib.className == "smdis") {
                    ilib.src = "../images/appbuttons/minibuttons/lillib_dis.gif";
                }
                else {
                    ilib.src = "../images/appbuttons/minibuttons/lillib.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smclib").className = "";
                iclib = document.getElementById("iclib");
                if (iclib.className == "smdis") {
                    iclib.src = "../images/appbuttons/minibuttons/lillib_dis.gif";
                }
                else {
                    iclib.src = "../images/appbuttons/minibuttons/lillib.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smsch").className = "";
                isch = document.getElementById("isch");
                if (isch.className == "smdis") {
                    isch.src = "../images/appbuttons/minibuttons/sched_dis.gif";
                }
                else {
                    isch.src = "../images/appbuttons/minibuttons/sched.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smfwp").className = "";
                ifwp = document.getElementById("ifwp");
                if (ifwp.className == "smdis") {
                    ifwp.src = "../images/appbuttons/minibuttons/sched_dis.gif";
                }
                else {
                    ifwp.src = "../images/appbuttons/minibuttons/sched.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smsdp").className = "";
                isdp = document.getElementById("isdp");
                if (isdp.className == "smdis") {
                    isdp.src = "../images/appbuttons/minibuttons/sched_dis.gif";
                }
                else {
                    isdp.src = "../images/appbuttons/minibuttons/sched.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smwob").className = "";
                iwob = document.getElementById("iwob");
                if (iwob.className == "smdis") {
                    iwob.src = "../images/appbuttons/minibuttons/sched_dis.gif";
                }
                else {
                    iwob.src = "../images/appbuttons/minibuttons/sched.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smrte").className = "";
                irte = document.getElementById("irte");
                if (irte.className == "smdis") {
                    irte.src = "../images/appbuttons/minibuttons/routepms_dis.gif";
                }
                else {
                    irte.src = "../images/appbuttons/minibuttons/routepms.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smmtr").className = "";
                imtr = document.getElementById("imtr");
                if (imtr.className == "smdis") {
                    imtr.src = "../images/appbuttons/minibuttons/routepms_dis.gif";
                }
                else {
                    imtr.src = "../images/appbuttons/minibuttons/routepms.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smdbp").className = "";
                idbp = document.getElementById("idbp");
                if (idbp.className == "smdis") {
                    idbp.src = "../images/appbuttons/minibuttons/routepms_dis.gif";
                }
                else {
                    idbp.src = "../images/appbuttons/minibuttons/routepms.gif";
                }
            }
            catch (err) {

            }

            try {
                document.getElementById("smloc").className = "";
                iloc = document.getElementById("iloc");
                if (iloc.className == "smdis") {
                    iloc.src = "../images/appbuttons/minibuttons/loctrans_dis.gif";
                }
                else {
                    iloc.src = "../images/appbuttons/minibuttons/loctrans.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smasu").className = "";
                iasu = document.getElementById("iasu");
                if (iasu.className == "smdis") {
                    iasu.src = "../images/appbuttons/minibuttons/write_dis.gif";
                }
                else {
                    iasu.src = "../images/appbuttons/minibuttons/write.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smjob").className = "";
                ijob = document.getElementById("ijob");
                if (ijob.className == "smdis") {
                    ijob.src = "../images/appbuttons/minibuttons/planb1_dis.gif";
                }
                else {
                    ijob.src = "../images/appbuttons/minibuttons/planb1.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smres").className = "";
                ires = document.getElementById("ires");
                if (ires.className == "smdis") {
                    ires.src = "../images/appbuttons/minibuttons/people2_dis.gif";
                }
                else {
                    ires.src = "../images/appbuttons/minibuttons/people2.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smforum").className = "";
                iforum = document.getElementById("iforum");
                if (iforum.className == "smdis") {
                    iforum.src = "../images/appbuttons/minibuttons/forum_dis.gif";
                }
                else {
                    iforum.src = "../images/appbuttons/minibuttons/forum.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smsqla").className = "";
                isqla = document.getElementById("isqla");
                if (isqla.className == "smdis") {
                    isqla.src = "../images/appbuttons/minibuttons/dbf.gif";
                }
                else {
                    isqla.src = "../images/appbuttons/minibuttons/db.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("smmea").className = "";
                imea = document.getElementById("imea");
                if (imea.className == "smdis") {
                    imea.src = "../images/appbuttons/minibuttons/measmenu_dis.gif";
                }
                else {
                    imea.src = "../images/appbuttons/minibuttons/measmenu.gif";
                }
            }
            catch (err) {

            }
            try {
                document.getElementById("sm12").className = "";
                document.getElementById("i12").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm2").className = "";
                document.getElementById("i2").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm3").className = "";
                document.getElementById("i3").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm4").className = "";
                document.getElementById("i4").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm5").className = "";
                document.getElementById("i5").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm6").className = "";
                document.getElementById("i6").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm7").className = "";
                document.getElementById("i7").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm8").className = "";
                document.getElementById("i8").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm9").className = "";
                document.getElementById("i9").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm10").className = "";
                document.getElementById("i10").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm11").className = "";
                document.getElementById("i11").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm13").className = "";
                document.getElementById("i13").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm14").className = "";
                document.getElementById("i14").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm15").className = "";
                document.getElementById("i15").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm16").className = "";
                document.getElementById("i16").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm17").className = "";
                document.getElementById("i17").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm18").className = "";
                document.getElementById("i18").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm19").className = "";
                document.getElementById("i19").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm20").className = "";
                document.getElementById("i20").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm21").className = "";
                document.getElementById("i21").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm22").className = "";
                document.getElementById("i22").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm23").className = "";
                document.getElementById("i23").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm24").className = "";
                document.getElementById("i24").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm25").className = "";
                document.getElementById("i25").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm26").className = "";
                document.getElementById("i26").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm27").className = "";
                document.getElementById("i27").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm28").className = "";
                document.getElementById("i28").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm29").className = "";
                document.getElementById("i29").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm30").className = "";
                document.getElementById("i30").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm31").className = "";
                document.getElementById("i31").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm32").className = "";
                document.getElementById("i32").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm33").className = "";
                document.getElementById("i33").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm34").className = "";
                document.getElementById("i34").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm35").className = "";
                document.getElementById("i35").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm36").className = "";
                document.getElementById("i36").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm37").className = "";
                document.getElementById("i37").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm38").className = "";
                document.getElementById("i38").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm39").className = "";
                document.getElementById("i39").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm40").className = "";
                document.getElementById("i40").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm41").className = "";
                document.getElementById("i41").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm42").className = "";
                document.getElementById("i42").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm43").className = "";
                document.getElementById("i43").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm44").className = "";
                document.getElementById("i44").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm45").className = "";
                document.getElementById("i45").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm46").className = "";
                document.getElementById("i46").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm47").className = "";
                document.getElementById("i47").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm48").className = "";
                document.getElementById("i48").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm49").className = "";
                document.getElementById("i49").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm50").className = "";
                document.getElementById("i50").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
            try {
                document.getElementById("sm51").className = "";
                document.getElementById("i50").src = "../images/appbuttons/minibuttons/globetrans.gif";
            }
            catch (err) {

            }
        }
    </script>
    <style type="text/css">
    .btmmenu {
	background-image: url(../images/appbuttons/minibuttons/gradient4.gif);
	background-repeat: repeat-x;
	background-position: top left;
	
}
    </style>
</head>
<body>
    <table>
        <tr>
            <td id="tdsid" runat="server">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblnpb" runat="server">
    <input type="hidden" id="lblsessid" runat="server">
    <input type="hidden" id="lblcomp" runat="server">
    <input type="hidden" id="lblcompname" runat="server">
    <input type="hidden" id="lbluserlevel" runat="server">
    <input type="hidden" id="lblcadm" runat="server">
    <input type="hidden" id="lblgrpndx" runat="server">
    <input type="hidden" id="lblusrname" runat="server">
    <input type="hidden" id="lbluid" runat="server">
    <input type="hidden" id="lbldf" runat="server">
    <input type="hidden" id="lblpsite" runat="server">
    <input type="hidden" id="lblms" runat="server">
    <input type="hidden" id="lblLogged_In" runat="server">
    <input type="hidden" id="lblua" runat="server">
    <input type="hidden" id="lblapps" runat="server">
    <input type="hidden" id="lblappstr" runat="server">
    <input type="hidden" id="lblemail" runat="server">
    <input type="hidden" id="lblpmadmin" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblapprgrp" runat="server">
    <input type="hidden" id="lblhdr" runat="server">
    <input type="hidden" id="lblmb" runat="server">
    <input type="hidden" id="lblmbg" runat="server">
    <input type="hidden" id="lblislabor" runat="server">
    <input type="hidden" id="lblisplanner" runat="server">
    <input type="hidden" id="lblissuper" runat="server">
    <input type="hidden" id="lbluserid" runat="server">
    <input type="hidden" id="lbloptsort" runat="server">
    <input type="hidden" id="lblpractice" runat="server">
    <input type="hidden" id="lblrostr" runat="server"><input type="hidden" id="lblcurlang"
        runat="server">
</body>
</html>
