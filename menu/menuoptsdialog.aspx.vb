

'********************************************************
'*
'********************************************************



Public Class menuoptsdialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cpg, urlname, uid, hopt, mbg As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifmo As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            cpg = Request.QueryString("cpg").ToString
            urlname = Request.QueryString("urlname").ToString
            uid = Request.QueryString("uid").ToString
            hopt = Request.QueryString("hopt").ToString
            mbg = Request.QueryString("mbg").ToString
            ifmo.Attributes.Add("src", "mmenuopts.aspx?cpg=" & cpg & "&urlname=" & urlname & "&uid=" & uid & "&hopt=" & hopt & "&mbg=" & mbg & "&date=" & Now)
        End If
    End Sub

End Class
