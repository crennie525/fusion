

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class mmenuopts
    Inherits System.Web.UI.Page


    Dim cpg, urlname, uid, hopt, mbg, nhopt, nmbg, nmb As String
    Dim opt As New Utilities
    Dim sql As String
    Protected WithEvents lblcpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblurl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhdropt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbgopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhdroptcurr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbgoptcurr As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cbbg As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        'Put user code to initialize the page here
        If Not IsPostBack Then
            'ddopts.Attributes.Add("onchange", "getpic(this.value);")
            cpg = Request.QueryString("cpg").ToString
            urlname = System.Configuration.ConfigurationManager.AppSettings("custAppUrl") 'Request.QueryString("urlname").ToString
            uid = Request.QueryString("uid").ToString
            hopt = Request.QueryString("hopt").ToString
            mbg = Request.QueryString("mbg").ToString
            lblhdroptcurr.Value = hopt
            lblbgoptcurr.Value = mbg
            lbluid.Value = uid
            lblurl.Value = urlname
            If mbg = "1" Then
                cbbg.Checked = True
            Else
                cbbg.Checked = False
            End If
            lblbgopt.Value = mbg
            lblcpg.Value = cpg
            'ddopts.SelectedValue = hopt
            If hopt = "0" Then
                'mi.Attributes.Add("src", "../menu/mimages/mopts/header0.gif")
            ElseIf hopt = "1" Then
                'mi.Attributes.Add("src", "../menu/mimages/mopts/header1.gif")
            ElseIf hopt = "2" Then
                'mi.Attributes.Add("src", "../menu/mimages/mopts/header2.gif")
            ElseIf hopt = "3" Then
                'mi.Attributes.Add("src", "../menu/mimages/mopts/header3.gif")
            End If
            lblret.Value = "0"
        Else
            If Request.Form("lblret") = "1" Then
                opt.Open()
                UpOpts()
                opt.Dispose()
            End If
        End If
    End Sub
    Private Sub UpOpts()
        nhopt = lblhdropt.Value
        If nhopt = "" Then
            nhopt = lblhdroptcurr.Value
            lblhdropt.Value = nhopt
        End If
        If cbbg.Checked = True Then
            nmbg = "1"
        Else
            nmbg = "0"
        End If
        lblbgopt.Value = nmbg
        If nmbg = "" Then
            nmbg = lblbgoptcurr.Value
            lblbgopt.Value = nmbg
        End If
        If nhopt = "3" Then
            nhopt = "2"
            nmb = "2"
        Else
            nmb = "0"
        End If
        uid = lbluid.Value
        Dim ucnt As Integer
        sql = "select count(*) from mopts where uid = '" & uid & "'"
        ucnt = opt.Scalar(sql)
        If ucnt = 0 Then
            sql = "insert into mopts (uid, hdr, mbg, mb) values " _
            + "('" & uid & "','" & nhopt & "','" & nmbg & "','" & nmb & "')"
        Else
            sql = "update mopts set hdr = '" & nhopt & "', mbg = '" & nmbg & "', mb = '" & nmb & "' where uid = '" & uid & "'"
        End If
        opt.Update(sql)
        lblret.Value = "2"
    End Sub


End Class
