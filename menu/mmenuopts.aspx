<%@ Page Language="vb" AutoEventWireup="false" Codebehind="mmenuopts.aspx.vb" Inherits="lucy_r12.mmenuopts" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Background Option</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" type="text/javascript">
		function getpic(dval) {
			document.getElementById("lblhdropt").value = dval
			if(dval=="0") {
			document.getElementById("mi").src="../menu/mimages/mopts/header0.gif";
			}
			else if(dval=="1") {
			document.getElementById("mi").src="../menu/mimages/mopts/header1.gif";
			}
			else if(dval=="2") {
			document.getElementById("mi").src="../menu/mimages/mopts/header2.gif";
			}
			else if(dval=="3") {
			document.getElementById("mi").src="../menu/mimages/mopts/header3.gif";
			}
			else if(dval=="4") {
			document.getElementById("mi").src="../menu/mimages/mopts/header4.gif";
			}
		}
		function checkret() {
		var ret = document.getElementById("lblret").value;
		if(ret=="2") {
		var hdr;
		var mb;
		var mbg;
		var urlname;
		var cpg;
			var hopt = document.getElementById("lblhdropt").value;
			if(hopt=="3") {
				hdr = "2";
				mb = "2";
			}
			else {
				hdr = hopt;
				mb = "0";
			}
			mbg = document.getElementById("lblbgopt").value;
			urlname = document.getElementById("lblurl").value;
			cpg = document.getElementById("lblcpg").value;
			var retval;
			retval = urlname; 
            //+ "?mup=yes&hdr=" + hdr + "&mb=" + mb + "&mbg=" + mbg + "&cpg=" + cpg
			//retval = hdr + "," + mb + "," + mbg + "," + urlname + "," + cpg;
			window.parent.handleret(retval);
		}
		}
		function savopts() {
		document.getElementById("lblret").value = "1";
		document.getElementById("form1").submit();
		}
		</script>
	</HEAD>
	<body  onload="checkret();">
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" width="220">
				<tr height="24">
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/pm3.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="194">Background Option</td>
				</tr>
				<tr>
					<td align="center" colSpan="2">
						<table>
							<tr>
								<td class="plainlabelblue"><asp:checkbox id="cbbg" runat="server" Checked="True" Text="Include Page Background"></asp:checkbox></td>
								<td><img src="../images/appbuttons/minibuttons/saveDisk1.gif" onclick="savopts();" onmouseover="return overlib('Save Changes and Exit')"
										onmouseout="return nd()"></td>
							</tr>
							<tr>
							<td class="plainlabelblue">Saving Changes Returns You to the Login Page</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblcpg" runat="server"> <input type="hidden" id="lblurl" runat="server">
			<input type="hidden" id="lbluid" runat="server"> <input type="hidden" id="lblhdropt" runat="server">
			<input type="hidden" id="lblbgopt" runat="server"> <input type="hidden" id="lblhdroptcurr" runat="server">
			<input type="hidden" id="lblbgoptcurr" runat="server"> <input type="hidden" id="lblret" runat="server">
		</form>
	</body>
</HTML>
