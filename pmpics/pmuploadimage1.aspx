<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmuploadimage1.aspx.vb"
    Inherits="lucy_r12.pmuploadimage1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmuploadimage1</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/tpmimgtasknav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmuploadimage1aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function checkit() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "return") {
                window.parent.handleexit();
            }
        }
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table width="520">
        <tbody>
            <tr>
                <td class="thdrsing label" colspan="3">
                    <asp:Label ID="lang3302" runat="server">Image Upload Dialog</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="bluelabel" width="150">
                    <asp:Label ID="lang3303" runat="server">Choose Image to Upload</asp:Label>
                </td>
                <td class="plainlabel" width="240">
                    <input id="MyFile" style="width: 230px" type="file" size="5" name="MyFile" runat="Server">
                </td>
                <td width="130">
                    <asp:Button ID="btnimgup" runat="server" Text="Upload" />
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lblfuncid" type="hidden" runat="server" />
    <input type="hidden" id="lbltasknum" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" /><input id="lblimgid" type="hidden"
        runat="server" />
    <input id="lblro" type="hidden" runat="server" /><input id="spdivy" type="hidden"
        name="spdivy" runat="server" />
    <input id="lblpicorder" type="hidden" runat="server" />
    <input id="lblsubmit" type="hidden" runat="server" />
    <input id="lblneworder" type="hidden" runat="server" />
    <input id="lbloldorder" type="hidden" runat="server" />
    <input id="lblmaxorder" type="hidden" runat="server" />
    <input id="lblititles" type="hidden" runat="server" />
    <input id="lblref" type="hidden" runat="server" /><input type="hidden" id="lbloldtask"
        runat="server" />
    <input type="hidden" id="lblmaxtask" runat="server" /><input type="hidden" id="lbledit"
        runat="server" />
    <input type="hidden" id="lblimg" runat="server" />
    <input type="hidden" id="lblbimg" runat="server" />
    <input type="hidden" id="lblnimg" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
