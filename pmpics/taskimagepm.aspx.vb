

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class taskimagepm
    Inherits System.Web.UI.Page
	Protected WithEvents ovid288 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3304 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblhost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhostflag As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim funcid, func, ro, tasknum, eqid
    Dim sql As String
    Dim dr As SqlDataReader
    Dim news As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtiorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgov As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgdel As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsavdet As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstrfrom As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnsimage As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblititles As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretmed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretbig As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbledit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load

        GetFSOVLIBS()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                eqid = Request.QueryString("eqid").ToString
                funcid = Request.QueryString("funcid").ToString '"0" ' 
                'func = "Test Fuction" 'Request.QueryString("func").ToString '"0" ' 
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                tasknum = Request.QueryString("tasknum").ToString '"0" ' 
                lbleqid.Value = eqid
                lblfuncid.Value = funcid
                lblfunc.Value = func
                lblro.Value = ro
                lbltasknum.Value = tasknum
                If ro = "1" Then
                    imgdel.Disabled = True
                    imgsavdet.Disabled = True
                    imgsavdet.Attributes.Add("src", "../images/appbuttons/minibuttons/saveDisk1dis.gif")
                    imgdel.Attributes.Add("src", "../images/appbuttons/minibuttons/deldis.gif")
                    lbledit.Value = "no"
                End If

                Dim cid, tli, sid, did, clid, coid, chk, typ, lid As String

                'cid = Request.QueryString("cid").ToString
                'sid = Request.QueryString("sid").ToString
                'did = Request.QueryString("did").ToString
                'clid = Request.QueryString("clid").ToString
                'chk = "" 'Request.QueryString("chk").ToString
                'typ = Request.QueryString("typ").ToString
                'lid = Request.QueryString("lid").ToString

                'lblcid.Value = cid
                'lblsid.Value = sid
                'lbldid.Value = did
                'lblclid.Value = clid
                'lblchk.Value = chk
                'lbltyp.Value = typ
                'lbllid.Value = lid

                news.Open()
                LoadPics()
                news.Dispose()
            Catch ex As Exception
                lbleqid.Value = ""
            End Try

        Else
            If Request.Form("lblsubmit") = "delimg" Then
                lblsubmit.Value = ""
                news.Open()
                DelImg()
                LoadPics()
                news.Dispose()
                'lblchksav.Value = "yes"
            ElseIf Request.Form("lblsubmit") = "checkpic" Then
                lblsubmit.Value = ""
                news.Open()
                LoadPics()
                news.Dispose()
            ElseIf Request.Form("lblsubmit") = "changepic" Then
                lblsubmit.Value = ""
                news.Open()
                SavePics()
                LoadPics()
                news.Dispose()
                lblsubmit.Value = "getdummy"
            ElseIf Request.Form("lblsubmit") = "savedets" Then
                lblsubmit.Value = ""
                news.Open()
                SaveDets()
                LoadPics()
                news.Dispose()
            End If
        End If
    End Sub
    Private Sub SavePics()
        Dim retmed As String = lblretmed.Value
        retmed = retmed.Replace("\", "/")
        Dim retbig As String = lblretbig.Value
        retbig = retbig.Replace("\", "/")
        Dim rmarr() As String = retmed.Split("/")
        Dim rbarr() As String = retbig.Split("/")
        Dim med = rmarr(rmarr.Length - 1)
        Dim big = rbarr(rbarr.Length - 1)
        Dim nurl As String = System.Configuration.ConfigurationManager.AppSettings("tpmurl")
        'Dim hostflag As String = lblhostflag.Value
        'If hostflag = "1" Then
        'Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        'Dim ns1i As Integer = nurl.IndexOf("//")
        'Dim nsstr As String = Mid(nurl, ns1i + 3)
        'Dim ns2i As Integer = nsstr.IndexOf("/")
        'nsstr = Mid(nsstr, 1, ns2i)
        'End If

        Dim savestr As String = nurl & "/pmimages/"
        med = savestr & med
        big = savestr & big
        Dim picid As String = lblimgid.Value
        sql = "update pmimages set pm_image = '" & big & "', pm_image_med = '" & med & "' where pic_id = '" & picid & "'"
        news.Update(sql)
        lblretbig.Value = big
    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1599" , "taskimagepm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim funcid As String = lblfuncid.Value
            Dim old As String = lbloldorder.Value
            Dim neword As String = txtiorder.Text
            tasknum = lbltasknum.Value
            sql = "pm_reorderimg '" & funcid & "','" & neword & "','" & old & "','" & tasknum & "'"
            news.Update(sql)
        End If
    End Sub
    Private Sub DelImg()
        Dim funcid As String = lblfuncid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        tasknum = lbltasknum.Value
        sql = "pm_delimg '" & funcid & "','" & pid & "','" & old & "','" & tasknum & "'"
        news.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        Try
            If File.Exists(fpic) Then
                File.Delete(fpic)
            End If
        Catch ex As Exception

        End Try
        Try
            If File.Exists(fpicm) Then
                File.Delete(fpicm)
            End If
        Catch ex As Exception

        End Try
        Try
            If File.Exists(fpicn) Then
                File.Delete(fpicn)
            End If
        Catch ex As Exception

        End Try
        lblcurrbimg.Value = ""
        lblcurrtimg.Value = ""
        lblcurrimg.Value = ""
    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ns1i As Integer = nsimage.IndexOf("//")
        Dim nsstr As String = Mid(nsimage, ns1i + 3)
        Dim ns2i As Integer = nsstr.IndexOf("/")
        nsstr = Mid(nsstr, 1, ns2i)
        Dim hostflag As Integer = 0
        If nsstr <> ThisPage1 Then
            nsimage = nsimage.Replace(nsstr, ThisPage1)
            hostflag = 1
            lblhostflag.Value = "1"
            lblhost.Value = nsstr

        Else
            lblhostflag.Value = "0"
        End If
        lblnsimage.Value = nsimage
        lblstrfrom.Value = strfrom
        'imgeq.Attributes.Add("src", "../images/appimages/pmimg1.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/pmimg1.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/pmimg1.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/pmimg1.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/pmimg1.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/pmimg1.gif")
        End If

        Dim img, imgs, picid As String

        Dim funcid As String = lblfuncid.Value
        If funcid <> "" Then
            tasknum = lbltasknum.Value
            Dim pcnt As Integer
            sql = "select count(*) from pmimages where funcid = '" & funcid & "' and tasknum = '" & tasknum & "'"
            pcnt = news.Scalar(sql)
            lblpcnt.Value = pcnt
            Dim currp As String = lblcurrp.Value
            Dim rcnt As Integer = 0
            Dim iflag As Integer = 0
            Dim oldiord As Integer
            If pcnt > 0 Then
                If currp <> "" Then
                    oldiord = System.Convert.ToInt32(currp) + 1
                    lblpg.Text = "Image " & oldiord & " of " & pcnt
                Else
                    oldiord = 1
                    lblpg.Text = "Image 1 of " & pcnt
                    lblcurrp.Value = "0"
                End If

                sql = "select p.pic_id, p.pm_image, p.pm_image_thumb, p.pm_image_med, p.pm_image_order, " _
                + "p.pm_image_title, f.func " _
                + "from pmimages p " _
                + "left join functions f on f.func_id = p.funcid where p.funcid = '" & funcid & "' and p.tasknum = '" & tasknum & "' order by p.pm_image_order"
                Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
                Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
                Dim tlink, ttext, iloc1, pcss As String
                Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
                dr = news.GetRdrData(sql)
                While dr.Read
                    iflag += 1
                    lblfunc.Value = dr.Item("func").ToString
                    img = dr.Item("pm_image_med").ToString
                    Dim imgarr() As String = img.Split("/")
                    ovimg = imgarr(imgarr.Length - 1)
                    bimg = dr.Item("pm_image").ToString
                    Dim bimgarr() As String = bimg.Split("/")
                    ovbimg = bimgarr(bimgarr.Length - 1)
                    timg = dr.Item("pm_image_thumb").ToString
                    Dim timgarr() As String = timg.Split("/")
                    ovtimg = timgarr(imgarr.Length - 1)
                    'check host here
                    If hostflag = 1 Then
                        img = img.Replace(nsstr, ThisPage1)
                        bimg = bimg.Replace(nsstr, ThisPage1)
                        timg = timg.Replace(nsstr, ThisPage1)
                    End If
                    picid = dr.Item("pic_id").ToString
                    order = dr.Item("pm_image_order").ToString
                    ititle = dr.Item("pm_image_title").ToString
                    If iorders = "" Then
                        iorders = order
                    Else
                        iorders += "," & order
                    End If
                    If bimgs = "" Then
                        bimgs = bimg
                    Else
                        bimgs += "," & bimg
                    End If
                    If ovbimgs = "" Then
                        ovbimgs = ovbimg
                    Else
                        ovbimgs += "," & ovbimg
                    End If

                    If ovtimgs = "" Then
                        ovtimgs = ovtimg
                    Else
                        ovtimgs += "," & ovtimg
                    End If


                    If ovimgs = "" Then
                        ovimgs = ovimg
                    Else
                        ovimgs += "," & ovimg
                    End If

                    If ititles = "" Then
                        ititles = ititle
                    Else
                        ititles += "," & ititle
                    End If
                    If iflag = oldiord Then 'was 0
                        'iflag = 1

                        lblcurrimg.Value = ovimg
                        lblcurrbimg.Value = ovbimg
                        lblcurrtimg.Value = ovtimg
                        'If File.Exists(img) Then
                        imgeq.Attributes.Add("src", img)
                        'End If
                        'imgeq.Attributes.Add("src", img)
                        'imgeq.Attributes.Add("onclick", "getbig();")
                        lblimgid.Value = picid

                        'txtpictitle.Text = ititle
                        'tdpline2.InnerHtml = ititle


                        txtiorder.Text = order
                    End If
            If imgs = "" Then
                imgs = picid & ";" & img
            Else
                imgs += "~" & picid & ";" & img
            End If
                End While
                dr.Close()
                lblimgs.Value = imgs
                lblbimgs.Value = bimgs
                lblovimgs.Value = ovimgs
                lblovbimgs.Value = ovbimgs
                lblovtimgs.Value = ovtimgs
                lblititles.Value = ititles
                lbliorders.Value = iorders
            End If

        End If
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3304.Text = axlabs.GetASPXPage("taskimagepm.aspx", "lang3304")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("taskimagepm.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("taskimagepm.aspx", "Img1") & "', ABOVE, LEFT)")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdel.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("taskimagepm.aspx", "imgdel") & "', ABOVE, LEFT)")
            imgdel.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            'imgov.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("taskimagepm.aspx", "imgov") & "', ABOVE, LEFT)")
            'imgov.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("taskimagepm.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid288.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("taskimagepm.aspx", "ovid288") & "', ABOVE, LEFT)")
            ovid288.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
