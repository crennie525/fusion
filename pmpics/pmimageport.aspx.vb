

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class pmimageport
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, funcid, tasknum As String
    Dim news As New Utilities
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            funcid = Request.QueryString("funcid").ToString
            tasknum = Request.QueryString("tasknum").ToString
            news.Open()
            GetPics(funcid, tasknum)
            news.Dispose()

        End If
    End Sub
    Private Sub GetPics(ByVal funcid As String, ByVal tasknum As String)
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ns1i As Integer = nsimage.IndexOf("//")
        Dim nsstr As String = Mid(nsimage, ns1i + 3)
        Dim ns2i As Integer = nsstr.IndexOf("/")
        nsstr = Mid(nsstr, 1, ns2i)
        Dim hostflag As Integer = 0
        If nsstr <> ThisPage1 Then
            nsimage = nsimage.Replace(nsstr, ThisPage1)
            hostflag = 1
            'lblhostflag.Value = "1"
            'lblhost.Value = nsstr

        Else
            'lblhostflag.Value = "0"
        End If
        Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, tnum As String
        Dim x As Integer
        Dim sb As New StringBuilder
        sb.Append("<table>")
        Dim maxord As Integer

        Dim iheights, iwidths, ititles, ilocs, pcols, pdecs, pstyles, ilinks As String
        sql = "select tasknum, pic_id, pm_image, pm_image_thumb, pm_image_med, pm_image_order, " _
        + "pm_image_title " _
        + "from pmimages where funcid = '" & funcid & "' and tasknum = '" & tasknum & "' order by pm_image_order"
        dr = news.GetRdrData(sql)
        sb.Append("<tr>")
        x = 1
        'id=""" + pic + """
        While dr.Read
            If x = 1 Then
                sb.Append("<tr>")
            End If
            pic = dr.Item("pic_id").ToString
            order = dr.Item("pm_image_order").ToString
            ititle = dr.Item("pm_image_title").ToString
            tnum = dr.Item("tasknum").ToString
            Dim img As String = dr.Item("pm_image").ToString
            Dim timg As String = dr.Item("pm_image_thumb").ToString
            If hostflag = 1 Then
                img = img.Replace(nsstr, ThisPage1)
                'bimg = bimg.Replace(nsstr, ThisPage1)
                timg = timg.Replace(nsstr, ThisPage1)
            End If
            sb.Append("<td><table><tr><td colspan=""2"">")
            sb.Append("<a href=""#"" onclick=""getbig('" + img + "');""" _
            + ">" _
            + "<img border=""0"" src=""" + timg + """>" _
            + "</a></td>" _
            + "</tr><tr><td class=""bluelabel"" align=""center"">Task# " & tnum & "</td></tr></table></td>")
            x = x + 1
            If x = 7 Then
                sb.Append("</tr>")
                x = 1
            End If
        End While
        dr.Close()
        Dim Remainder As Integer
        Remainder = x Mod 4
        If Remainder > 0 Then
            sb.Append("</tr>")
        End If
        sb.Append("</table>")
        'tblpics.InnerHtml = sb.ToString
        'picdiv.InnerHtml = sb.ToString
        Response.Write(sb.ToString)

    End Sub
End Class
