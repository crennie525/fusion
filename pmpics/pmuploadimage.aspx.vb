

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports System.Random
Public Class pmuploadimage
    Inherits System.Web.UI.Page
	Protected WithEvents lang3301 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3300 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3299 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3298 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3297 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3296 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3295 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3294 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3293 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, funcid, func, ro, tasknum As String
    Dim news As New Utilities
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtpictitle As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtasks As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblbpage As System.Web.UI.WebControls.Label
    Protected WithEvents MyFile As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents btnupload As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgsavdet As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgref As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Div1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tblpics As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents picdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents bfirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents bnext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents blast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpicorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblneworder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmaxorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblititles As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmaxtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbledit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnimg As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            funcid = Request.QueryString("funcid").ToString '"1073" '
            lblfuncid.Value = funcid
            func = Request.QueryString("func").ToString '"0" ' 
            lblfunc.Value = func
            tasknum = Request.QueryString("tasknum").ToString '"0" ' 
            lbltasknum.Value = tasknum
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                btnupload.Disabled = True
                lbledit.Value = "no"
                imgsavdet.Disabled = True
                imgsavdet.Attributes.Add("src", "../images/appbuttons/minibuttons/saveDisk1dis.gif")
            End If
            news.Open()
            PopTasks(funcid)
            GetPics()
            news.Dispose()
            tdcurr.InnerHtml = "Enter New Image Details"
            imgsavdet.Attributes.Add("class", "details")
        Else
            If Request.Form("lblsubmit") = "savord" Then
                lblsubmit.Value = ""
                news.Open()
                SaveOrd()
                GetPics()
                news.Dispose()
            ElseIf Request.Form("lblsubmit") = "savedets" Then
                lblsubmit.Value = ""
                news.Open()
                SaveDets()
                GetPics()
                news.Dispose()
            ElseIf Request.Form("lblsubmit") = "delimg" Then
                lblsubmit.Value = ""
                news.Open()
                DelImg()
                GetPics()
                news.Dispose()
            ElseIf Request.Form("lblsubmit") = "next" Then
                news.Open()
                GetNext()
                news.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                news.Open()
                PageNumber = lblmaxtask.Value
                lbltasknum.Value = PageNumber
                GetPics()
                news.Dispose()
                lblsubmit.Value = ""
                ddtasks.SelectedValue = PageNumber
                Dim maxtask As String = lblmaxtask.Value
                lblbpage.Text = "Task " & PageNumber & " of " & maxtask
            ElseIf Request.Form("lblsubmit") = "prev" Then
                news.Open()
                GetPrev()
                news.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                news.Open()
                PageNumber = 1
                lbltasknum.Value = PageNumber
                GetPics()
                news.Dispose()
                lblsubmit.Value = ""
                ddtasks.SelectedValue = PageNumber
                Dim maxtask As String = lblmaxtask.Value
                lblbpage.Text = "Task " & PageNumber & " of " & maxtask
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = lbltasknum.Value
            PageNumber = pg + 1
            lbltasknum.Value = PageNumber
            GetPics()
            ddtasks.SelectedValue = PageNumber
            Dim maxtask As String = lblmaxtask.Value
            lblbpage.Text = "Task " & PageNumber & " of " & maxtask
        Catch ex As Exception
            news.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1589" , "pmuploadimage.aspx.vb")
 
            news.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = lbltasknum.Value
            PageNumber = pg - 1
            lbltasknum.Value = PageNumber
            GetPics()
            ddtasks.SelectedValue = PageNumber
            Dim maxtask As String = lblmaxtask.Value
            lblbpage.Text = "Task " & PageNumber & " of " & maxtask
        Catch ex As Exception
            news.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1590" , "pmuploadimage.aspx.vb")
 
            news.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopTasks(ByVal funcid As String)
        sql = "select distinct tasknum from pmtasks where funcid = '" & funcid & "'"
        dr = news.GetRdrData(sql)
        ddtasks.DataSource = dr
        ddtasks.DataTextField = "tasknum"
        ddtasks.DataValueField = "tasknum"
        ddtasks.DataBind()
        dr.Close()
        'dddepts.Items.Insert(0, "Select Department")
        tasknum = lbltasknum.Value
        ddtasks.SelectedValue = tasknum
        Dim maxtask As Integer
        sql = "select count(distinct tasknum) from pmtasks where funcid = '" & funcid & "'"
        maxtask = news.Scalar(sql)
        lblmaxtask.Value = maxtask
        lblbpage.Text = "Task " & tasknum & " of " & maxtask
    End Sub
    Private Sub DelImg()
        Dim funcid As String = lblfuncid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        tasknum = lbltasknum.Value
        sql = "pm_delimg '" & funcid & "','" & pid & "','" & old & "','" & tasknum & "'"
        news.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim pic, picn, picm As String
        pic = lblbimg.Value
        picn = lblnimg.Value
        picm = lblimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        Try
            If File.Exists(fpic) Then
                File.Delete(fpic)
            End If
        Catch ex As Exception

        End Try
        Try
            If File.Exists(fpicm) Then
                File.Delete(fpicm)
            End If
        Catch ex As Exception

        End Try
        Try
            If File.Exists(fpicn) Then
                File.Delete(fpicn)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SaveOrd()
        Dim funcid As String = lblfuncid.Value
        Dim old As String = lbloldorder.Value
        Dim neword As String = lblneworder.Value
        tasknum = lbltasknum.Value
        sql = "pm_reorderimg '" & funcid & "','" & neword & "','" & old & "','" & tasknum & "'"
        news.Update(sql)

    End Sub

    Private Sub GetPics()
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        Dim ThisPage1 As String = Request.ServerVariables("HTTP_HOST")
        Dim ns1i As Integer = nsimage.IndexOf("//")
        Dim nsstr As String = Mid(nsimage, ns1i + 3)
        Dim ns2i As Integer = nsstr.IndexOf("/")
        nsstr = Mid(nsstr, 1, ns2i)
        Dim hostflag As Integer = 0
        If nsstr <> ThisPage1 Then
            nsimage = nsimage.Replace(nsstr, ThisPage1)
            hostflag = 1
            'lblhostflag.Value = "1"
            'lblhost.Value = nsstr

        Else
            'lblhostflag.Value = "0"
        End If
        txtpictitle.Text = ""
        funcid = lblfuncid.Value
        tasknum = lbltasknum.Value
        ro = lblro.Value
        Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink As String
        Dim x As Integer
        Dim sb As New StringBuilder
        sb.Append("<table>")
        Dim maxord As Integer

        sql = "select max(pm_image_order) as maxord from pmimages where funcid = '" & funcid & "' and tasknum = '" & tasknum & "'"
        Try
            maxord = news.Scalar(sql)
        Catch ex As Exception
            maxord = 0
        End Try

        lblmaxorder.Value = maxord
        Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts As String
        Dim tlink, ttext, iloc1, pcss As String
        sql = "select pic_id, pm_image, pm_image_med, pm_image_thumb, pm_image_order, " _
        + "pm_image_title, tasknum  " _
        + "from pmimages where funcid = '" & funcid & "' and tasknum = '" & tasknum & "' order by pm_image_order"
        dr = news.GetRdrData(sql)
        sb.Append("<tr>")
        x = 1
        'id=""" + pic + """
        Dim img, bimg, nimg As String
        While dr.Read
            If x = 1 Then
                sb.Append("<tr>")
            End If
            pic = dr.Item("pic_id").ToString
            order = dr.Item("pm_image_order").ToString
            ititle = dr.Item("pm_image_title").ToString
            img = dr.Item("pm_image_med").ToString
            bimg = dr.Item("pm_image").ToString
            nimg = dr.Item("pm_image_thumb").ToString
            If hostflag = 1 Then
                img = img.Replace(nsstr, ThisPage1)
                bimg = bimg.Replace(nsstr, ThisPage1)
                nimg = nimg.Replace(nsstr, ThisPage1)
            End If
            sb.Append("<td><table><tr><td colspan=""2"">")
            If ro = "1" Then
                sb.Append("<a href=""#"" ondblclick=""getbig('" + bimg + "');""" _
           + "onclick=""getdets('" + order + "','" + pic + "','" + order + "');"">" _
           + "<img border=""0"" src=""" + nimg + """>" _
           + "</a></td><tr><td class=""bluelabel"">" & tmod.getlbl("cdlbl644", "pmuploadimage.aspx.vb") & "</td><td>" _
           + "<input size=""2"" value=""" + order + """ id=""" + pic + """>" _
           + "</td></tr><tr><td colspan=""2"" align=""right"">" _
           + "<img src=""../images/appbuttons/minibuttons/deldis.gif"" " _
           + " onmouseover=""return overlib('" & tmod.getov("cov321", "pmuploadimage.aspx.vb") & "', ABOVE, LEFT)"" onmouseout='return nd()'>" _
           + "<img src=""../images/appbuttons/minibuttons/saveDisk1dis.gif"" " _
           + "onmouseover=""return overlib('" & tmod.getov("cov322", "pmuploadimage.aspx.vb") & "', ABOVE, LEFT)"" onmouseout='return nd()'></td></tr></table></td>")
            Else
                sb.Append("<a href=""#"" ondblclick=""getbig('" + bimg + "');""" _
                          + "onclick=""getdets('" + order + "','" + pic + "','" + order + "');"">" _
                          + "<img border=""0"" src=""" + nimg + """>" _
                          + "</a></td><tr><td class=""bluelabel"">" & tmod.getlbl("cdlbl647", "pmuploadimage.aspx.vb") & "</td><td>" _
                          + "<input size=""2"" value=""" + order + """ id=""" + pic + """>" _
                          + "</td></tr><tr><td colspan=""2"" align=""right"">" _
                          + "<img src=""../images/appbuttons/minibuttons/del.gif"" onclick=""delimg('" + pic + "','" + order + "','" + img + "','" + bimg + "','" + nimg + "');"" " _
                          + " onmouseover=""return overlib('" & tmod.getov("cov323", "pmuploadimage.aspx.vb") & "', ABOVE, LEFT)"" onmouseout='return nd()'>" _
                          + "<img src=""../images/appbuttons/minibuttons/saveDisk1.gif"" onclick=""saveord('" + pic + "','" + order + "');"" " _
                          + "onmouseover=""return overlib('" & tmod.getov("cov324", "pmuploadimage.aspx.vb") & "', ABOVE, LEFT)"" onmouseout='return nd()'></td></tr></table></td>")
            End If

            If picorder = "" Then
                picorder = pic & "-" & order
            Else
                picorder += "~" & pic & "-" & order
            End If
            If ititles = "" Then
                ititles = ititle
            Else
                ititles += "," & ititle
            End If
            x = x + 1
            If x = 5 Then
                sb.Append("</tr>")
                x = 1
            End If
        End While
        dr.Close()
        lblpicorder.Value = picorder
        'iheights, iwidths, ititles, ilocs, pcols, pdecs, pstyles
        lblititles.Value = ititles
        Dim Remainder As Integer
        Remainder = x Mod 4
        If Remainder > 0 Then
            sb.Append("</tr>")
        End If
        sb.Append("</table>")
        'tblpics.InnerHtml = sb.ToString
        picdiv.InnerHtml = sb.ToString

    End Sub
    Private Sub btnupload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.ServerClick
        AddPic()
    End Sub
    Private Sub SaveDets()

        Dim ptitle As String
        ptitle = txtpictitle.Text '= dr.Item("news_image_title").ToString
        If Len(ptitle) > 250 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1591" , "pmuploadimage.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        tasknum = ddtasks.SelectedValue 'txttasknum.Text
        'Try
        'Dim ph As Integer = System.Convert.ToInt32(tasknum)
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr1592" , "pmuploadimage.aspx.vb")
 
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End Try

        Dim pid As String = lblimgid.Value
        Dim otask As String = lbltasknum.Value
        funcid = lblfuncid.Value

        Dim cmd As New SqlCommand
        cmd.CommandText = "exec usp_updateimgpm @funcid, @picid, @ptitle, @task, @oldtask"

        Dim param = New SqlParameter("@funcid", SqlDbType.Int)
        param.Value = funcid
        cmd.Parameters.Add(param)

        Dim param1 = New SqlParameter("@picid", SqlDbType.Int)
        param1.Value = pid
        cmd.Parameters.Add(param1)

        Dim param8 = New SqlParameter("@ptitle", SqlDbType.VarChar)
        If ptitle = "" Then
            param8.Value = System.DBNull.Value
        Else
            param8.Value = ptitle
        End If
        cmd.Parameters.Add(param8)

        Dim param2 = New SqlParameter("@task", SqlDbType.Int)
        param2.Value = tasknum
        cmd.Parameters.Add(param2)

        Dim param3 = New SqlParameter("@oldtask", SqlDbType.Int)
        param3.Value = otask
        cmd.Parameters.Add(param3)

        news.UpdateHack(cmd)

    End Sub
    Private Sub AddPic()
        If Not (MyFile.PostedFile Is Nothing) Then
            'Check to make sure we actually have a file to upload
            Dim strLongFilePath As String = MyFile.PostedFile.FileName
            Dim intFileNameLength As Integer = InStr(1, StrReverse(strLongFilePath), "\")
            Dim strFileName As String = Mid(strLongFilePath, (Len(strLongFilePath) - intFileNameLength) + 2)
            'Dim uid As String = lblblockid.Value
            Dim ptitle As String
            ptitle = txtpictitle.Text '= dr.Item("news_image_title").ToString
            If Len(ptitle) > 250 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1593" , "pmuploadimage.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

            Dim icnt As Integer
            funcid = lblfuncid.Value
            tasknum = ddtasks.SelectedValue

            news.Open()
            sql = "select count(*) from pmimages where funcid = '" & funcid & "' and tasknum = '" & tasknum & "'"
            icnt = news.Scalar(sql)
            icnt += 1
            Dim deli As Integer
            Dim random As New Random
            deli = random.Next(1000)

            Dim newstr As String = "a-newsImg" & funcid & "-" & tasknum & "-" & deli & ".jpg"
            Dim thumbstr As String = "atn-newsImg" & funcid & "-" & tasknum & "-" & deli & ".jpg"
            Dim medstr As String = "atm-newsImg" & funcid & "-" & tasknum & "-" & deli & ".jpg"

            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim strto As String = appstr + "/pmimages/"
            Select Case MyFile.PostedFile.ContentType
                Case "image/pjpeg", "image/jpeg"  'Make sure we are getting a valid JPG image
                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & newstr)
                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & thumbstr)
                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & medstr)
                    Dim fsimg As System.Drawing.Image
                    'Response.ContentType = "image/jpeg"
                    fsimg = System.Drawing.Image.FromFile(Server.MapPath("\") & strto & medstr)
                    Dim biw, bih As Integer
                    biw = fsimg.Width
                    bih = fsimg.Height
                    If biw > 500 Then
                        'biw = 500
                    End If
                    If bih > 382 Then
                        'bih = 382
                    End If
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                    Dim tnImg As System.Drawing.Image
                    Dim iw, ih As Integer
                    iw = 100
                    ih = 100
                    tnImg = fsimg.GetThumbnailImage(iw, ih, dummyCallBack, IntPtr.Zero)
                    tnImg.Save(Server.MapPath("\") & strto & thumbstr)

                    Dim tmImg As System.Drawing.Image
                    Dim iwm, ihm As Integer
                    iwm = System.Convert.ToInt32(biw)
                    ihm = System.Convert.ToInt32(bih)

                    If iwm > 220 Then
                        Dim iper As Decimal
                        iper = ihm / iwm
                        iwm = 220
                        ihm = iwm * iper
                        ihm = Math.Round(ihm, 0)
                    End If
                    If ihm > 220 Then
                        Dim iper As Decimal
                        iper = iwm / ihm
                        ihm = 220
                        iwm = ihm * iper
                        iwm = Math.Round(iwm, 0)
                    End If

                    tmImg = fsimg.GetThumbnailImage(iwm, ihm, dummyCallBack, IntPtr.Zero)
                    tmImg.Save(Server.MapPath("\") & strto & medstr)

                    Dim tbImg As System.Drawing.Image
                    Dim iwb, ihb, iwb1, ihb1 As Integer
                    iwb = fsimg.Width
                    ihb = fsimg.Height
                    iwb1 = System.Convert.ToInt32(iwb)
                    ihb1 = System.Convert.ToInt32(ihb)
                    If iwb1 > 500 Then

                        Dim iper As Decimal
                        iper = ihb1 / iwb1
                        iwb1 = 500
                        ihb1 = iwb1 * iper
                        ihb1 = Math.Round(ihb1, 0)

                    End If
                    If ihb1 > 500 Then
                        Dim iper As Decimal
                        iper = iwb1 / ihb1
                        ihb1 = 500
                        iwb1 = ihb1 * iper
                        iwb1 = Math.Round(iwb1, 0)
                    End If

                    tbImg = fsimg.GetThumbnailImage(biw, bih, dummyCallBack, IntPtr.Zero)
                    tbImg.Save(Server.MapPath("\") & strto & newstr)


                    'tnImg.Dispose()
                    'tmImg.Dispose()
                    tbImg.Dispose()
                    fsimg.Dispose()

                    Dim nurl As String = System.Configuration.ConfigurationManager.AppSettings("tpmurl")
                    Dim savstr As String = nurl & "/pmimages/" & newstr
                    Dim savtnstr As String = nurl & "/pmimages/" & thumbstr
                    Dim savtmstr As String = nurl & "/pmimages/" & medstr
                    Dim cmd As New SqlCommand
                    cmd.CommandText = "exec usp_addnewimgpm @funcid, @ni, @nit, @nim, " _
                    + "@ptitle, @order, @task"

                    Dim param1 = New SqlParameter("@funcid", SqlDbType.Int)
                    param1.Value = funcid
                    cmd.Parameters.Add(param1)
                    Dim param2 = New SqlParameter("@ni", SqlDbType.VarChar)
                    param2.Value = savstr
                    cmd.Parameters.Add(param2)
                    Dim param3 = New SqlParameter("@nit", SqlDbType.VarChar)
                    param3.Value = savtnstr
                    cmd.Parameters.Add(param3)
                    Dim param4 = New SqlParameter("@nim", SqlDbType.VarChar)
                    param4.Value = savtmstr
                    cmd.Parameters.Add(param4)
                    Dim param8 = New SqlParameter("@ptitle", SqlDbType.VarChar)
                    If ptitle = "" Then
                        param8.Value = System.DBNull.Value
                    Else
                        param8.Value = ptitle
                    End If
                    cmd.Parameters.Add(param8)
                    Dim param5 = New SqlParameter("@order", SqlDbType.Int)
                    param5.Value = icnt
                    cmd.Parameters.Add(param5)
                    Dim param6 = New SqlParameter("@task", SqlDbType.Int)
                    param6.Value = tasknum
                    cmd.Parameters.Add(param6)

                    news.UpdateHack(cmd)
                    lblref.Value = "yes"
                Case Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1594" , "pmuploadimage.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Select
            GetPics()
            news.Dispose()
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1595" , "pmuploadimage.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3293.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3293")
        Catch ex As Exception
        End Try
        Try
            lang3294.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3294")
        Catch ex As Exception
        End Try
        Try
            lang3295.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3295")
        Catch ex As Exception
        End Try
        Try
            lang3296.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3296")
        Catch ex As Exception
        End Try
        Try
            lang3297.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3297")
        Catch ex As Exception
        End Try
        Try
            lang3298.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3298")
        Catch ex As Exception
        End Try
        Try
            lang3299.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3299")
        Catch ex As Exception
        End Try
        Try
            lang3300.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3300")
        Catch ex As Exception
        End Try
        Try
            lang3301.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lang3301")
        Catch ex As Exception
        End Try
        Try
            lblbpage.Text = axlabs.GetASPXPage("pmuploadimage.aspx", "lblbpage")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgref.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmuploadimage.aspx", "imgref") & "', ABOVE, LEFT)")
            imgref.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmuploadimage.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
