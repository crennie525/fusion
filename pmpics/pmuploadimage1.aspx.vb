

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports System.Random
Public Class pmuploadimage1
    Inherits System.Web.UI.Page
	Protected WithEvents lang3303 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3302 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, funcid, func, ro, tasknum As String
    Dim news As New Utilities
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents MyFile As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents btnupload As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpicorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblneworder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmaxorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblititles As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmaxtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbledit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnimgup As System.Web.UI.WebControls.Button
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            funcid = Request.QueryString("funcid").ToString '"1073" '
            lblfuncid.Value = funcid
            func = Request.QueryString("func").ToString '"0" ' 
            lblfunc.Value = func
            tasknum = Request.QueryString("tasknum").ToString '"0" ' 
            lbltasknum.Value = tasknum
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                btnupload.Disabled = True
                lbledit.Value = "no"
                
            End If
        End If
    End Sub

    Protected Sub btnimgup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnimgup.Click
        AddPic()
    End Sub
    Private Sub DelImg()
        Dim funcid As String = lblfuncid.Value
        tasknum = lbltasknum.Value
        Dim pid As String
        Dim old As String
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim pic, picn, picm As String
        sql = "select p.pic_id, p.pm_image, p.pm_image_thumb, p.pm_image_med, p.pm_image_order, " _
               + "p.pm_image_title, f.func " _
               + "from pmimages p " _
               + "left join functions f on f.func_id = p.funcid where p.funcid = '" & funcid & "' and p.tasknum = '" & tasknum & "' order by p.pm_image_order"
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = news.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim row As DataRow
        For Each row In dt.Rows
            pid = row("pic_id").ToString
            old = row("pm_image_order").ToString
            pic = row("pm_image").ToString
            picn = row("pm_image_thumb").ToString
            picm = row("pm_image_med").ToString

            sql = "pm_delimg '" & funcid & "','" & pid & "','" & old & "','" & tasknum & "'"
            news.Update(sql)

            Dim picarr() As String = pic.Split("/")
            Dim picnarr() As String = picn.Split("/")
            Dim picmarr() As String = picm.Split("/")
            Dim dpic, dpicn, dpicm As String
            dpic = picarr(picarr.Length - 1)
            dpicn = picnarr(picnarr.Length - 1)
            dpicm = picmarr(picmarr.Length - 1)
            Dim fpic, fpicn, fpicm As String
            fpic = strfrom + dpic
            fpicn = strfrom + dpicn
            fpicm = strfrom + dpicm
            Try
                If File.Exists(fpic) Then
                    File.Delete(fpic)
                End If
            Catch ex As Exception

            End Try
            Try
                If File.Exists(fpicm) Then
                    File.Delete(fpicm)
                End If
            Catch ex As Exception

            End Try
            Try
                If File.Exists(fpicn) Then
                    File.Delete(fpicn)
                End If
            Catch ex As Exception

            End Try
        Next

    End Sub
    Private Sub AddPic()

        If Not (MyFile.PostedFile Is Nothing) Then
            'Check to make sure we actually have a file to upload
            Dim strLongFilePath As String = MyFile.PostedFile.FileName
            Dim intFileNameLength As Integer = InStr(1, StrReverse(strLongFilePath), "\")
            Dim strFileName As String = Mid(strLongFilePath, (Len(strLongFilePath) - intFileNameLength) + 2)
            'Dim uid As String = lblblockid.Value
            Dim ptitle As String
            ptitle = "" 'txtpictitle.Text '= dr.Item("news_image_title").ToString
            If Len(ptitle) > 250 Then
                Dim strMessage As String = tmod.getmsg("cdstr1596", "pmuploadimage1.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

            Dim icnt As Integer
            funcid = lblfuncid.Value
            tasknum = lbltasknum.Value 'ddtasks.SelectedValue

            news.Open()
            sql = "select count(*) from pmimages where funcid = '" & funcid & "' and tasknum = '" & tasknum & "'"
            icnt = news.Scalar(sql)
            If icnt > 1 Then
                DelImg()
            End If
            'icnt += 1
            icnt = 1
            Dim deli As Integer
            Dim random As New Random
            deli = random.Next(1000)

            Dim newstr As String = "a-newsImg" & funcid & "-" & tasknum & "-" & deli & ".jpg"
            Dim thumbstr As String = "atn-newsImg" & funcid & "-" & tasknum & "-" & deli & ".jpg"
            Dim medstr As String = "atm-newsImg" & funcid & "-" & tasknum & "-" & deli & ".jpg"

            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim strto As String = appstr + "/pmimages/"
            '***uncomment for local testing
            'strto = "/pmimages/"
            Select Case MyFile.PostedFile.ContentType
                Case "image/pjpeg", "image/jpeg"  'Make sure we are getting a valid JPG image
                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & newstr)
                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & thumbstr)
                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & medstr)
                    Dim fsimg As System.Drawing.Image
                    'Response.ContentType = "image/jpeg"
                    fsimg = System.Drawing.Image.FromFile(Server.MapPath("\") & strto & medstr)
                    Dim biw, bih As Integer
                    biw = fsimg.Width
                    bih = fsimg.Height
                    If biw > 500 Then
                        'biw = 500
                    End If
                    If bih > 382 Then
                        'bih = 382
                    End If
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                    Dim tnImg As System.Drawing.Image
                    Dim iw, ih As Integer
                    iw = 100
                    ih = 100
                    tnImg = fsimg.GetThumbnailImage(iw, ih, dummyCallBack, IntPtr.Zero)
                    tnImg.Save(Server.MapPath("\") & strto & thumbstr)

                    Dim tmImg As System.Drawing.Image
                    Dim iwm, ihm As Integer
                    iwm = System.Convert.ToInt32(biw)
                    ihm = System.Convert.ToInt32(bih)

                    If iwm > 220 Then
                        Dim iper As Decimal
                        iper = 220 / iwm
                        iwm = Math.Round(iwm * iper, 0)
                        ihm = Math.Round(ihm * iper, 0)
                    End If
                    If ihm > 220 Then
                        Dim iper As Decimal
                        iper = 220 / ihm
                        iwm = Math.Round(iwm * iper, 0)
                        ihm = Math.Round(ihm * iper, 0)
                    End If

                    tmImg = fsimg.GetThumbnailImage(iwm, ihm, dummyCallBack, IntPtr.Zero)
                    tmImg.Save(Server.MapPath("\") & strto & medstr)

                    Dim tbImg As System.Drawing.Image
                    Dim iwb, ihb, iwb1, ihb1 As Integer
                    iwb = fsimg.Width
                    ihb = fsimg.Height
                    iwb1 = System.Convert.ToInt32(iwb)
                    ihb1 = System.Convert.ToInt32(ihb)
                    If iwb1 > 500 Then
                        Dim iper As Decimal
                        iper = 500 / iwb1
                        iwb1 = Math.Round(iwb1 * iper, 0)
                        ihb1 = Math.Round(ihb1 * iper, 0)
                    End If
                    If ihb1 > 500 Then
                        Dim iper As Decimal
                        iper = 500 / ihb1
                        iwb1 = Math.Round(iwb1 * iper, 0)
                        ihb1 = Math.Round(ihb1 * iper, 0)
                    End If

                    'tbImg = fsimg.GetThumbnailImage(biw, bih, dummyCallBack, IntPtr.Zero)
                    tbImg = fsimg.GetThumbnailImage(iwb1, ihb1, dummyCallBack, IntPtr.Zero)
                    tbImg.Save(Server.MapPath("\") & strto & newstr)

                    


                    'tnImg.Dispose()
                    'tmImg.Dispose()
                    tbImg.Dispose()
                    fsimg.Dispose()

                    Dim nurl As String = System.Configuration.ConfigurationManager.AppSettings("tpmurl")
                    Dim savstr As String = nurl & "/pmimages/" & newstr
                    Dim savtnstr As String = nurl & "/pmimages/" & thumbstr
                    Dim savtmstr As String = nurl & "/pmimages/" & medstr
                    Dim cmd As New SqlCommand
                    cmd.CommandText = "exec usp_addnewimgpm @funcid, @ni, @nit, @nim, " _
                    + "@ptitle, @order, @task"

                    Dim param1 = New SqlParameter("@funcid", SqlDbType.Int)
                    param1.Value = funcid
                    cmd.Parameters.Add(param1)
                    Dim param2 = New SqlParameter("@ni", SqlDbType.VarChar)
                    param2.Value = savstr
                    cmd.Parameters.Add(param2)
                    Dim param3 = New SqlParameter("@nit", SqlDbType.VarChar)
                    param3.Value = savtnstr
                    cmd.Parameters.Add(param3)
                    Dim param4 = New SqlParameter("@nim", SqlDbType.VarChar)
                    param4.Value = savtmstr
                    cmd.Parameters.Add(param4)
                    Dim param8 = New SqlParameter("@ptitle", SqlDbType.VarChar)
                    If ptitle = "" Then
                        param8.Value = System.DBNull.Value
                    Else
                        param8.Value = ptitle
                    End If
                    cmd.Parameters.Add(param8)
                    Dim param5 = New SqlParameter("@order", SqlDbType.Int)
                    param5.Value = icnt
                    cmd.Parameters.Add(param5)
                    Dim param6 = New SqlParameter("@task", SqlDbType.Int)
                    param6.Value = tasknum
                    cmd.Parameters.Add(param6)

                    news.UpdateHack(cmd)
                    lblref.Value = "yes"
                Case Else
                    Dim strMessage As String = tmod.getmsg("cdstr1597", "pmuploadimage1.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Select

            news.Dispose()
            lblsubmit.Value = "return"
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1598", "pmuploadimage1.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3302.Text = axlabs.GetASPXPage("pmuploadimage1.aspx", "lang3302")
        Catch ex As Exception
        End Try
        Try
            lang3303.Text = axlabs.GetASPXPage("pmuploadimage1.aspx", "lang3303")
        Catch ex As Exception
        End Try

    End Sub

    
End Class
