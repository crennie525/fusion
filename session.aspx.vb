Public Class session
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim sess As New Utilities
    Dim who, curlang As String
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsess As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblstest As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            '
            lblsess.Value = "600000"
            Try
                who = Request.QueryString("who").ToString
            Catch ex As Exception
                who = "other"
            End Try
            lblwho.Value = who
            Try
                Dim sessid As String

                sessid = HttpContext.Current.Session.SessionID
                Session("sess") = sessid
                Dim comp As String = HttpContext.Current.Session("comp").ToString
                Session("comp") = comp
                Dim compname As String = HttpContext.Current.Session("compname").ToString
                Session("compname") = compname
                Dim userlevel As String = HttpContext.Current.Session("userlevel").ToString
                Session("userlevel") = userlevel
                Dim cadm As String = HttpContext.Current.Session("cadm").ToString
                Session("cadm") = cadm
                Dim islabor As String = HttpContext.Current.Session("islabor").ToString
                Session("islabor") = islabor
                Dim isplanner As String = HttpContext.Current.Session("isplanner").ToString
                Session("isplanner") = isplanner
                Try
                    Dim grpndx As String = HttpContext.Current.Session("grpndx").ToString
                    Session("grpndx") = grpndx
                Catch ex As Exception
                    Session("grpndx") = "1"
                End Try
                Try
                    Dim optsort As String = HttpContext.Current.Session("optsort").ToString
                    Session("optsort") = optsort
                Catch ex As Exception
                    Session("optsort") = "tasknum"
                End Try
                Dim username As String = HttpContext.Current.Session("username").ToString
                Session("username") = username
                Dim uid As String = HttpContext.Current.Session("uid").ToString
                Session("uid") = uid
                Dim dfltps As String = HttpContext.Current.Session("dfltps").ToString
                Session("dfltps") = dfltps
                Dim psite As String = HttpContext.Current.Session("psite").ToString
                Session("psite") = psite
                Dim ms As String = HttpContext.Current.Session("ms").ToString
                Session("ms") = ms
                Dim Logged_In As String = HttpContext.Current.Session("Logged_In").ToString
                Session("Logged_In") = Logged_In
                Dim ua As String = HttpContext.Current.Session("ua").ToString
                Session("ua") = ua
                Try
                    Dim app As String = HttpContext.Current.Session("app").ToString
                    Session("app") = app
                Catch ex As Exception
                    Session("app") = ""
                End Try
                Dim appstr As String = HttpContext.Current.Session("appstr").ToString
                Session("appstr") = appstr
                Try
                    Dim email As String = HttpContext.Current.Session("email").ToString
                    Session("email") = email
                Catch ex As Exception
                    Session("email") = ""
                End Try

                Dim practice As String = HttpContext.Current.Session("practice").ToString
                Session("practice") = practice
                Dim pmadmin As String = HttpContext.Current.Session("pmadmin").ToString
                Session("pmadmin") = pmadmin
                Dim ro As String = HttpContext.Current.Session("ro").ToString
                Session("ro") = ro
                Dim rostr As String = HttpContext.Current.Session("rostr").ToString
                Session("rostr") = rostr
                Dim apprgrp As String = HttpContext.Current.Session("apprgrp").ToString
                Session("apprgrp") = apprgrp
                Dim hdr As String = HttpContext.Current.Session("hdr").ToString
                Session("hdr") = hdr
                Dim mb As String = HttpContext.Current.Session("mb").ToString
                Session("mb") = mb
                Dim mbg As String = HttpContext.Current.Session("mbg").ToString
                Session("mbg") = mbg
                Try
                    curlang = HttpContext.Current.Session("curlang").ToString
                    Session("curlang") = curlang
                Catch ex As Exception
                    Try
                        Dim dlang As New mmenu_utils_a
                        curlang = dlang.AppDfltLang
                        Session("curlang") = curlang
                    Catch ex1 As Exception

                    End Try
                End Try
                sess.Open()
                sql = "select count(*) from logtrack where sessionid = '" & sessid & "'"
                Dim sesscnt As String = sess.strScalar(sql)
                'sql = "insert into sesstest (who, sessid, reftime) values ('" & who & "','" & sessid & "','" & Now & "')"
                'sess.Update(sql)
                sess.Dispose()
                'Dim strMessage As String = sessid + "," + comp + "," + compname + "," + userlevel + "," + cadm + "," + username + "," + uid + "," + dfltps + "," + psite + "," + ms + "," + Logged_In + "," + ua + "," + appstr + "," + practice + "," + pmadmin + "," + ro + "," + rostr + "," + apprgrp + "," + hdr + "," + mb + "," + mbg
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                If who = "mm" Then
                    'Dim strMessage As String = "mm reset " & Now
                    'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            Catch ex As Exception
                'Dim exmsg As String = ex.Message
                'exmsg = Replace(exmsg, "'", Chr(180), , , vbTextCompare)
                'exmsg = Replace(exmsg, """", Chr(180), , , vbTextCompare)
                Dim strMessage As String = "Your Session has Expired or a Session Variable could not be retrieved.\n\n" _
                + "Please close all windows, log out of Fusion, and log back in to continue."
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try

        End If
    End Sub

End Class
