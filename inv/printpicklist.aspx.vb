

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class printpicklist
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ppl As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim wonum, transid, store, typ As String
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            wonum = Request.QueryString("wonum").ToString
            transid = Request.QueryString("seed").ToString
            store = Request.QueryString("store").ToString
            typ = Request.QueryString("typ").ToString
            ppl.Open()
            PopReport(wonum, transid, store, typ)
            ppl.Dispose()
        End If
    End Sub
    Private Sub PopReport(ByVal wonum As String, ByVal transid As String, ByVal store As String, ByVal typ As String)
        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""120""></td><td width=""180""></td><td width=""120""></td>")
        sb.Append("<td width=""80""></td><td width=""50""></td><td width=""50""></td><td width=""50""></td></tr>")

        sb.Append("<tr height=""30""><td colspan=""7"" class=""bigbold"" align=""center"">" & tmod.getlbl("cdlbl512" , "printpicklist.aspx.vb") & "</td></tr>")

        If typ = "wo" Then
            sb.Append("<tr><td colspan=""7"" class=""label"" align=""center"">To Work Order#&nbsp;&nbsp;" & wonum & "'</td></tr>")
        Else
            sb.Append("<tr><td colspan=""7"" class=""label"" align=""center"">To Store Room:&nbsp;&nbsp;" & store & "'</td></tr>")
        End If

        sb.Append("<tr><td colspan=""7"" class=""label"" align=""center"">&nbsp;</td></tr>")

        sb.Append("<tr><td class=""label""><u>Item#</u></td>")
        sb.Append("<td class=""label""><u>Description</u></td>")
        sb.Append("<td class=""label""><u>Store Room</u></td>")
        sb.Append("<td class=""label""><u>Bin</u></td>")
        sb.Append("<td class=""label""><u>Qty</u></td>")
        sb.Append("<td class=""label""><u>Cost</u></td>")
        sb.Append("<td class=""label""><u>Total</u></td></tr>")

        sql = "select * from invpicklist where transid = '" & transid & "' order by location, bin, itemnum"
        dr = ppl.GetRdrData(sql)
        While dr.read
            sb.Append("<tr><td class=""plainlabel"">" & dr.Item("itemnum").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("itemdesc").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("location").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("bin").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("qty").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("itemcost").ToString & "</td>")
            sb.Append("<td class=""plainlabel"">" & dr.Item("total").ToString & "</td></tr>")

        End While
        dr.Close()

        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString


    End Sub
End Class
