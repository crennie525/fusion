<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddPartDialog.aspx.vb"
    Inherits="lucy_r12.AddPartDialog" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Add Inventory</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" src="../scripts1/AddPartDialogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function handleexit(ret) {
            document.getElementById("lblchk").value = ret;
            window.close();
        }
        function handleunload() {
            ret = document.getElementById("lblchk").value;
            if (ret == "") {
                ret = "can";
            }
            window.returnValue = ret;
            window.close();
        }
        function pageScroll() {
            window.scrollTo(0, top);
            //scrolldelay = setTimeout('pageScroll()', 200); 
        } 
    </script>
</head>
<body  bgcolor="white" onunload="handleunload();" onload="resetsess();pageScroll();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifpart" runat="server" width="100%" height="100%" frameborder="no"></iframe>
   <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblchk" runat="server" />
    </form>
</body>
</html>
