

'********************************************************
'*
'********************************************************




Public Class invmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang2741 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2740 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2739 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2738 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2737 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2736 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2735 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ro As String
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbloldcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifinv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txttab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblneweq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchild As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldfail As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim Login As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            'Dim df, ps As String
            'df = Request.QueryString("psid").ToString
            'ps = Request.QueryString("psite").ToString
            'Session("dfltps") = df
            'Session("psite") = ps
            'Response.Redirect("invmain.aspx")
        Catch ex As Exception

        End Try
        Try
            ro = HttpContext.Current.Session("ro").ToString
        Catch ex As Exception
            ro = "0"
        End Try
        lblro.Value = ro
        If ro = "1" Then
            'pgtitle.InnerHtml = "PM Inventory Control (Read Only)"
        Else
            'pgtitle.InnerHtml = "PM Inventory Control"
        End If
        If Not IsPostBack Then
            ifinv.Attributes.Add("src", "catalog.aspx?ro=" + ro)
            txttab.Value = "inv"
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2735.Text = axlabs.GetASPXPage("invmain.aspx", "lang2735")
        Catch ex As Exception
        End Try
        Try
            lang2736.Text = axlabs.GetASPXPage("invmain.aspx", "lang2736")
        Catch ex As Exception
        End Try
        Try
            lang2737.Text = axlabs.GetASPXPage("invmain.aspx", "lang2737")
        Catch ex As Exception
        End Try
        Try
            lang2738.Text = axlabs.GetASPXPage("invmain.aspx", "lang2738")
        Catch ex As Exception
        End Try
        Try
            lang2739.Text = axlabs.GetASPXPage("invmain.aspx", "lang2739")
        Catch ex As Exception
        End Try
        Try
            lang2740.Text = axlabs.GetASPXPage("invmain.aspx", "lang2740")
        Catch ex As Exception
        End Try
        Try
            lang2741.Text = axlabs.GetASPXPage("invmain.aspx", "lang2741")
        Catch ex As Exception
        End Try

    End Sub

End Class
