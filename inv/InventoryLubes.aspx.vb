

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class InventoryLubes
    Inherits System.Web.UI.Page
	Protected WithEvents lang2733 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = "lubenum"
    Dim rowcnt As Integer
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim cid, dept, sql, ro, Login As String
    Dim inv As New Utilities
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbllube As System.Web.UI.WebControls.Label
    Protected WithEvents dglube As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dglube.Columns(0).Visible = False
                dglube.Columns(7).Visible = False
            End If
            Try
                cid = Request.QueryString("cid")
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try
            If cid <> "" Then
                lblcid.Value = Request.QueryString("cid")
                inv.Open()
                LoadLubes(PageNumber)
                inv.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                inv.Open()
                GetNext()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                inv.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadLubes(PageNumber)
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                inv.Open()
                GetPrev()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                inv.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadLubes(PageNumber)
                inv.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub
    ' Lubes
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        inv.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadLubes(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub LoadLubes(ByVal PageNumber As Integer)

        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, """", Chr(180) & Chr(180), , , vbTextCompare)
        If Len(srch) > 0 Then
            Filter = "lubenum like ''%" & srch & "%''"
            FilterCnt = "lubenum like '%" & srch & "%'"
        Else
            Filter = ""
            FilterCnt = ""
        End If
        Dim sql As String
        If FilterCnt <> "" Then
            sql = "select Count(*) from Lubricants where " & FilterCnt
        Else
            sql = "select Count(*) from Lubricants"
        End If

        dglube.VirtualItemCount = inv.Scalar(sql)
        If dglube.VirtualItemCount = 0 Then
            Tables = "Lubricants"
            PK = "lubeid"
            PageSize = "10"
            PageSize = "10"
            Fields = "*"
            Group = ""

            dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dglube.DataSource = dr
            Try
                dglube.DataBind()
            Catch ex As Exception
                dglube.CurrentPageIndex = 0
                dglube.DataBind()
            End Try
            dr.Close()
            inv.Dispose()
            lbllube.Text = "No Lubricants Inventory Records Found"
            txtpg.Value = PageNumber
            txtpgcnt.Value = dglube.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dglube.PageCount
        Else
            Tables = "Lubricants"
            PK = "lubeid"
            PageSize = "10"
            PageSize = "10"
            Fields = "*"
            Group = ""
            'Sort = ""
            dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dglube.DataSource = dr
            Try
                dglube.DataBind()
            Catch ex As Exception
                dglube.CurrentPageIndex = 0
                dglube.DataBind()
            End Try
            dr.Close()
            lbllube.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dglube.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dglube.PageCount
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadLubes(PageNumber)
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1211" , "InventoryLubes.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadLubes(PageNumber)
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1212" , "InventoryLubes.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dglube_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglube.UpdateCommand

        inv.Open()
        cid = lblcid.Value
        Dim id, lube, desc, loc, cost As String
        Dim flag As Integer = 1
        id = CType(e.Item.FindControl("lbllubeid"), Label).Text
        lube = CType(e.Item.Cells(3).Controls(1), TextBox).Text
        lube = Replace(lube, "'", Chr(180), , , vbTextCompare)
        lube = Replace(lube, """", Chr(180) & Chr(180), , , vbTextCompare)
        lube = Replace(lube, "--", "-", , , vbTextCompare)
        lube = Replace(lube, ";", ":", , , vbTextCompare)

        lube = Replace(lube, ";", " ", , , vbTextCompare)
        'lube = Replace(lube, "/", " ", , , vbTextCompare)
        'lube = Replace(lube, "\", " ", , , vbTextCompare)
        lube = Replace(lube, "&", " ", , , vbTextCompare)
        lube = Replace(lube, "%", " ", , , vbTextCompare)
        lube = Replace(lube, "~", " ", , , vbTextCompare)
        lube = Replace(lube, "^", " ", , , vbTextCompare)
        lube = Replace(lube, "*", " ", , , vbTextCompare)
        lube = Replace(lube, "$", " ", , , vbTextCompare)

        desc = CType(e.Item.Cells(4).Controls(1), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, """", Chr(180) & Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)

        desc = Replace(desc, ";", " ", , , vbTextCompare)
        'desc = Replace(desc, "/", " ", , , vbTextCompare)
        'desc = Replace(desc, "\", " ", , , vbTextCompare)
        desc = Replace(desc, "&", " ", , , vbTextCompare)
        desc = Replace(desc, "%", " ", , , vbTextCompare)
        desc = Replace(desc, "~", " ", , , vbTextCompare)
        desc = Replace(desc, "^", " ", , , vbTextCompare)
        desc = Replace(desc, "*", " ", , , vbTextCompare)
        desc = Replace(desc, "$", " ", , , vbTextCompare)

        If Len(desc) = 0 Then desc = ""
        If Len(desc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1213" , "InventoryLubes.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        loc = CType(e.Item.Cells(5).Controls(1), TextBox).Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, """", Chr(180) & Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)

        loc = Replace(loc, ";", " ", , , vbTextCompare)
        loc = Replace(loc, "/", " ", , , vbTextCompare)
        loc = Replace(loc, "\", " ", , , vbTextCompare)
        loc = Replace(loc, "&", " ", , , vbTextCompare)
        loc = Replace(loc, "%", " ", , , vbTextCompare)
        loc = Replace(loc, "~", " ", , , vbTextCompare)
        loc = Replace(loc, "^", " ", , , vbTextCompare)
        loc = Replace(loc, "*", " ", , , vbTextCompare)
        loc = Replace(loc, "$", " ", , , vbTextCompare)

        If Len(loc) = 0 Then loc = ""
        If Len(loc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1214" , "InventoryLubes.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        cost = CType(e.Item.FindControl("txtcost"), TextBox).Text
        Dim costchk As Decimal
        Try
            costchk = System.Convert.ToDecimal(cost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1215" , "InventoryLubes.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If Len(lube) <= 50 And Len(lube) <> 0 And lube <> "" Then
            Dim old As String = lblold.Value
            Dim icnt As Integer
            sql = "usp_updateinv 'lube','" & id & "','" & lube & "','" & desc & "','" & old & "','" & loc & "', '" & cost & "', '" & flag & "'"
            icnt = inv.Scalar(sql)
            If icnt = 0 Then
                dglube.EditItemIndex = -1
                PageNumber = txtpg.Value
                LoadLubes(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1216" , "InventoryLubes.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            ' sql = "update Lubricants set description = " _
            '        + "'" & desc & "', lubenum = '" & lube & "', location = '" & loc & "' " _
            '        + "where lubeid = '" & id & "'"
            'inv.Update(sql)

        Else
            If Len(lube) > 50 Then
                If Len(lube) = 0 OrElse lube = "" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1217" , "InventoryLubes.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1218" , "InventoryLubes.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If

        inv.Dispose()
    End Sub
    Private Sub dglube_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglube.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("Label12"), Label).Text
        dglube.EditItemIndex = e.Item.ItemIndex
        inv.Open()
        LoadLubes(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub dglube_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglube.CancelCommand
        dglube.EditItemIndex = -1
        inv.Open()
        PageNumber = txtpg.Value
        LoadLubes(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub dglube_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglube.DeleteCommand
        inv.Open()
        Dim id, item, old As String
        Dim flag As Integer = 0
        Try
            id = CType(e.Item.FindControl("lbllubeidn"), Label).Text
            item = CType(e.Item.FindControl("Label12"), Label).Text
            old = CType(e.Item.FindControl("Label1"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbllubeid"), Label).Text
            item = CType(e.Item.FindControl("TextBox10"), TextBox).Text
            old = CType(e.Item.FindControl("Label2"), Label).Text
            flag = 1
        End Try
        If flag = 1 Then
            If old <> item Then
                Dim strMessage As String =  tmod.getmsg("cdstr1219" , "InventoryLubes.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                inv.Dispose()
                Exit Sub
            End If
        End If
        Dim icnt As Integer
        sql = "select count(*) from pmtasklubes where lubenum = '" & item & "'"
        icnt = inv.Scalar(sql)
        If icnt = 0 Then
            sql = "delete from Lubricants where lubeid = '" & id & "'"
            inv.Update(sql)
            dglube.EditItemIndex = -1
            cid = lblcid.Value
            sql = "select Count(*) from lubricants " _
            + "where compid = '" & cid & "'"
            PageNumber = inv.PageCount(sql, PageSize)

            sql = "update AdminTasks " _
               + "set lube = '" & PageNumber & "' where cid = '" & cid & "'"
            inv.Update(sql)

            'inv.Dispose()
            If dglube.CurrentPageIndex > PageNumber Then
                dglube.CurrentPageIndex = PageNumber - 1
            End If
            If dglube.CurrentPageIndex < PageNumber - 2 Then
                dglube.CurrentPageIndex = PageNumber + 1
            End If
            dglube.EditItemIndex = -1
            LoadLubes(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1220" , "InventoryLubes.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        inv.Dispose()
    End Sub


    Private Sub dglube_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglube.ItemCommand
        If e.CommandName = "Add" Then
            Dim num, des, loc, cost As String
            Dim nt, dt, lt As TextBox
            'lnamei = CType(e.Item.FindControl("txtnewstat"), TextBox)
            'lname = lnamei.Text
            nt = CType(e.Item.FindControl("txtlname"), TextBox)
            dt = CType(e.Item.FindControl("txtldesc"), TextBox)
            lt = CType(e.Item.FindControl("txtlloc"), TextBox)
            num = CType(e.Item.FindControl("txtlname"), TextBox).Text
            num = Replace(num, "'", Chr(180), , , vbTextCompare)

            num = Replace(num, """", Chr(180) & Chr(180), , , vbTextCompare)
            num = Replace(num, "--", "-", , , vbTextCompare)
            num = Replace(num, ";", ":", , , vbTextCompare)

            num = Replace(num, ";", " ", , , vbTextCompare)
            'num = Replace(num, "/", " ", , , vbTextCompare)
            'num = Replace(num, "\", " ", , , vbTextCompare)
            num = Replace(num, "&", " ", , , vbTextCompare)
            num = Replace(num, "%", " ", , , vbTextCompare)
            num = Replace(num, "~", " ", , , vbTextCompare)
            num = Replace(num, "^", " ", , , vbTextCompare)
            num = Replace(num, "*", " ", , , vbTextCompare)
            num = Replace(num, "$", " ", , , vbTextCompare)

            des = CType(e.Item.FindControl("txtldesc"), TextBox).Text
            des = Replace(des, "'", Chr(180), , , vbTextCompare)
            des = Replace(des, """", Chr(180) & Chr(180), , , vbTextCompare)
            des = Replace(des, "--", "-", , , vbTextCompare)
            des = Replace(des, ";", ":", , , vbTextCompare)

            des = Replace(des, ";", " ", , , vbTextCompare)
            'des = Replace(des, "/", " ", , , vbTextCompare)
            'des = Replace(des, "\", " ", , , vbTextCompare)
            des = Replace(des, "&", " ", , , vbTextCompare)
            des = Replace(des, "%", " ", , , vbTextCompare)
            des = Replace(des, "~", " ", , , vbTextCompare)
            des = Replace(des, "^", " ", , , vbTextCompare)
            des = Replace(des, "*", " ", , , vbTextCompare)
            des = Replace(des, "$", " ", , , vbTextCompare)

            loc = CType(e.Item.FindControl("txtlloc"), TextBox).Text
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)

            loc = Replace(loc, """", Chr(180) & Chr(180), , , vbTextCompare)
            loc = Replace(loc, "--", "-", , , vbTextCompare)
            loc = Replace(loc, ";", ":", , , vbTextCompare)

            loc = Replace(loc, ";", " ", , , vbTextCompare)
            loc = Replace(loc, "/", " ", , , vbTextCompare)
            loc = Replace(loc, "\", " ", , , vbTextCompare)
            loc = Replace(loc, "&", " ", , , vbTextCompare)
            loc = Replace(loc, "%", " ", , , vbTextCompare)
            loc = Replace(loc, "~", " ", , , vbTextCompare)
            loc = Replace(loc, "^", " ", , , vbTextCompare)
            loc = Replace(loc, "*", " ", , , vbTextCompare)
            loc = Replace(loc, "$", " ", , , vbTextCompare)

            cost = CType(e.Item.FindControl("txtcostf"), TextBox).Text
            If cost = "" Then
                cost = "0"
            End If
            Dim costchk As Decimal
            Try
                costchk = System.Convert.ToDecimal(cost)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1221" , "InventoryLubes.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If Len(num) <= 50 And Len(num) <> 0 And num <> "" Then
                inv.Open()
                cid = lblcid.Value
                sql = "usp_insertinv 'lube', '" & num & "','" & des & "','" & loc & "','" & cost & "','oinv'"
                Dim icnt As Integer
                icnt = inv.Scalar(sql)
                'sql = "insert into item " _
                '+ "(compid, itemnum, description, location) values " _
                '+ "('" & cid & "', '" & num & "', '" & des & "', '" & loc & "')"
                'inv.Update(sql)
                If icnt = 0 Then
                    sql = "select Count(*) from lubricants " '_
                    '+ "where compid = '" & cid & "'"
                    PageNumber = inv.PageCount(sql, PageSize)
                    LoadLubes(PageNumber)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1222" , "InventoryLubes.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                'sql = "insert into lubricants " _
                '+ "(compid, lubenum, description, location) values " _
                '+ "('" & cid & "', '" & num & "', '" & des & "', '" & loc & "')"
                'inv.Update(sql)
                'Dim statcnt As Integer = dglube.VirtualItemCount + 1

                inv.Dispose()
            Else
                If Len(num) = 0 OrElse num = "" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1223" , "InventoryLubes.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1224" , "InventoryLubes.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If
    End Sub


	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dglube.Columns(0).HeaderText = dlabs.GetDGPage("InventoryLubes.aspx", "dglube", "0")
        Catch ex As Exception
        End Try
        Try
            dglube.Columns(2).HeaderText = dlabs.GetDGPage("InventoryLubes.aspx", "dglube", "2")
        Catch ex As Exception
        End Try
        Try
            dglube.Columns(3).HeaderText = dlabs.GetDGPage("InventoryLubes.aspx", "dglube", "3")
        Catch ex As Exception
        End Try
        Try
            dglube.Columns(4).HeaderText = dlabs.GetDGPage("InventoryLubes.aspx", "dglube", "4")
        Catch ex As Exception
        End Try
        Try
            dglube.Columns(6).HeaderText = dlabs.GetDGPage("InventoryLubes.aspx", "dglube", "5")
        Catch ex As Exception
        End Try
        Try
            dglube.Columns(7).HeaderText = dlabs.GetDGPage("InventoryLubes.aspx", "dglube", "6")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2733.Text = axlabs.GetASPXPage("InventoryLubes.aspx", "lang2733")
        Catch ex As Exception
        End Try

    End Sub

End Class
