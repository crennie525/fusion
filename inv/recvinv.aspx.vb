

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class recvinv
    Inherits System.Web.UI.Page
	Protected WithEvents ovid271 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid270 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid269 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid268 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2883 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2882 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2881 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2880 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2879 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2878 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2877 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2876 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2875 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2874 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2873 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2872 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2871 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2870 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2869 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2868 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2867 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2866 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2865 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2864 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2863 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2862 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2861 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2860 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim inv As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents tdprcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtoutpo As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtactcost As System.Web.UI.WebControls.TextBox

    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divcode As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcodedesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divdesc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divpr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblrecdate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divrecdate As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblorderdate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divorderdate As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbllottype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divlottype As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbluseby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divuseby As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divunit As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divqtyordered As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblqtyordered As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstore As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divstore As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divpvend As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divman As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divvdesc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divmandesc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpvend As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblman As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblcatnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divcatnum As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblmod As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divmod As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim invs, ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList

    Protected WithEvents Tr5 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchild As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblneweq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcodeid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchildid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblaltid As System.Web.UI.HtmlControls.HtmlInputHidden






    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents txtqtyrecvd As System.Web.UI.WebControls.TextBox

    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage



    Protected WithEvents txtlot As System.Web.UI.WebControls.TextBox

    Protected WithEvents imglot As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                Img1.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                Img1.Attributes.Add("onclick", "")
            End If
            Try
                invs = Request.QueryString("inv").ToString
                lblcode.Value = invs
                divcode.InnerHtml = invs
                lbloldcode.Value = invs
                inv.Open()
                SrchInv()
                inv.Dispose()
            Catch ex As Exception

            End Try
        Else
            If Request.Form("lblsubmit") = "get" Then
                inv.Open()
                SrchInv()
                'GetAlt()
                'Dim iid As String = lblcodeid.Value
                'CheckPR(iid)
                inv.Dispose()
            ElseIf Request.Form("lblsubmit") = "saverec" Then
                inv.Open()
                SaveInv()

                'GetAlt()
                Dim iid As String = lblcodeid.Value
                If iid <> "" Then
                    CheckPR(iid)
                End If
                SrchInv()
                inv.Dispose()
            End If
        End If
        'ddtype.Attributes.Add("onchange", "cleanit();")
    End Sub
    Private Sub SrchInv()
        Dim typ As String = ddtype.SelectedValue.ToString
        Dim invs As String = lblcode.Value
        If typ = "part" Then
            sql = "select i.*, v.*, l.description as 'ldesc', b.curbal from item i " _
            + "left join inventory v on v.itemnum = i.itemnum " _
            + "left join invvallist l on l.value = v.location " _
            + "left join invbalances b on b.itemnum = i.itemnum where i.itemnum = '" & invs & "'"
        ElseIf typ = "tool" Then
            sql = "select i.*, v.*, l.description as 'ldesc', b.curbal from tool i " _
            + "left join inventory v on v.itemnum = i.toolnum " _
            + "left join invvallist l on l.value = v.location " _
            + "left join invbalances b on b.itemnum = i.toolnum where i.toolnum = '" & invs & "'"
        ElseIf typ = "lube" Then
            sql = "select i.*, v.*, l.description as 'ldesc', b.curbal from lubricants i " _
            + "left join inventory v on v.itemnum = i.lubenum " _
            + "left join invvallist l on l.value = v.location " _
            + "left join invbalances b on b.itemnum = i.lubenum where i.lubenum = '" & invs & "'"
        End If

        dr = inv.GetRdrData(sql)
        While dr.Read
            lblcode.Value = dr.Item("itemnum").ToString
            divcode.InnerHtml = dr.Item("itemnum").ToString
            lblcodeid.Value = dr.Item("itemid").ToString
            lbloldcode.Value = dr.Item("itemnum").ToString
            lblcodedesc.Value = dr.Item("description").ToString
            divdesc.InnerHtml = dr.Item("description").ToString
            lblcatnum.Value = dr.Item("catcode").ToString
            divcatnum.InnerHtml = dr.Item("catcode").ToString
            lblmod.Value = dr.Item("model").ToString
            divmod.InnerHtml = dr.Item("model").ToString
            'txtstk.Text = dr.Item("stocktype").ToString
            If typ = "tool" Then
                lbllottype.Value = ""
                divlottype.InnerHtml = ""
                imglot.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                imglot.Attributes.Add("onclick", "")

                'txthaz.Text = ""
                'imghaz.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                'imghaz.Attributes.Add("onclick", "")

                'txtmsds.Text = ""
                'imgmsds.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                'imgmsds.Attributes.Add("onclick", "")

                'ddspare.Enabled = False
                'ddinsp.Enabled = False
                'ddrot.Enabled = False
                'ddout.Enabled = False
                'ddcap.Enabled = False

            ElseIf typ = "lube" Then
                lbllottype.Value = dr.Item("lottype").ToString
                divlottype.InnerHtml = dr.Item("lottype").ToString
                imglot.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                imglot.Attributes.Add("onclick", "GetItem('lottype', 'txtlot')")

                'txthaz.Text = dr.Item("hazardid").ToString
                'imghaz.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                'imghaz.Attributes.Add("onclick", "GetItem('hazard', 'txthaz')")

                'txtmsds.Text = dr.Item("msdsnum").ToString
                'imgmsds.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                'imgmsds.Attributes.Add("onclick", "GetItem('msds', 'txtmsds')")

                'ddspare.Enabled = False
                'ddinsp.Enabled = False
                'ddrot.Enabled = False
                'ddout.Enabled = False
                'ddcap.Enabled = False


            Else
                lbllottype.Value = dr.Item("lottype").ToString
                divlottype.InnerHtml = dr.Item("lottype").ToString
                imglot.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                imglot.Attributes.Add("onclick", "GetItem('lottype', 'txtlot')")

                'txthaz.Text = dr.Item("hazardid").ToString
                'imghaz.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                'imghaz.Attributes.Add("onclick", "GetItem('hazard', 'txthaz')")

                'txtmsds.Text = dr.Item("msdsnum").ToString
                'imgmsds.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifier.gif")
                'imgmsds.Attributes.Add("onclick", "GetItem('msds', 'txtmsds')")

                'ddspare.SelectedValue = dr.Item("sparepartautoadd").ToString
                'ddinsp.SelectedValue = dr.Item("inspectionrequired").ToString
                'ddrot.SelectedValue = dr.Item("rotating").ToString
                'ddout.SelectedValue = dr.Item("outside").ToString
                'ddcap.SelectedValue = dr.Item("capitalized").ToString

                'ddspare.Enabled = True
                'ddinsp.Enabled = True
                'ddrot.Enabled = True
                'ddout.Enabled = True
                'ddcap.Enabled = True
            End If

            'lblstore.Value = dr.Item("location").ToString
            'divstore.InnerHtml = dr.Item("location").ToString
            'txtstdesc.Text = dr.Item("ldesc").ToString
            'txtbin.Text = dr.Item("bin").ToString
            Try
                'ddcap.SelectedValue = dr.Item("category").ToString
            Catch ex As Exception

            End Try
            'txtcurbal.Text = dr.Item("curbal").ToString
            'txtstd.Text = dr.Item("stdcost").ToString
            'txtavg.Text = dr.Item("avgcost").ToString
            'txtlast.Text = dr.Item("lastcost").ToString
            'txtord.Text = dr.Item("orderunit").ToString
            lblsubmit.Value = "new"
            lbltype.Value = typ
        End While
        dr.Close()
        Dim iid = lblcodeid.Value
        If iid <> "" Then
            CheckPR(iid)
        End If

    End Sub
    Private Sub CheckPR(ByVal iid As String)
        Dim prcnt As Integer
        sql = "select count(*) from invpr " _
        + "where itemid = '" & iid & "' and (status <> 'COMP' and status <> 'CAN') and ponum is not null"
        prcnt = inv.Scalar(sql)
        If prcnt <> 0 Then
            tdprcnt.InnerHtml = prcnt & " Open Purchase Requisitions Found for this Item"
            GetPR(iid)
        Else
            'Dim strMessage As String =  tmod.getmsg("cdstr1430" , "recvinv.aspx.vb")
 
            'inv.CreateMessageAlert(Me, strMessage, "strKey1")
            tdprcnt.InnerHtml = "No Open Purchase Requisitions with an Assigned Purchase Order Found for this Item"
        End If
       
    End Sub
    Private Sub GetPR(ByVal iid As String)
        sql = "select top 1 prnum, itemcost, actcost, itemnum, orderqty, Convert(char(10),orderdate, 101) as 'orderdate', povendor, ponum, " _
            + "mfg, issuedate,Convert(char(10),recdate, 101) as recdate, requireddate, requestedby, vendor, status, statusdate, totalcost from invpr " _
        + "where itemid = '" & iid & "' and (status <> 'COMP' and status <> 'CAN') and ponum is not null " _
        + "order by recdate desc"
        dr = inv.GetRdrData(sql)
        Dim pvend, ponum As String
        While dr.Read
            lblpr.Value = dr.Item("prnum").ToString
            divpr.InnerHtml = dr.Item("prnum").ToString
            txtoutpo.Text = dr.Item("ponum").ToString
            divqtyordered.InnerHtml = dr.Item("orderqty").ToString
            lblqtyordered.Value = dr.Item("orderqty").ToString
            lblorderdate.Value = dr.Item("orderdate").ToString
            divorderdate.InnerHtml = dr.Item("orderdate").ToString
            lblrecdate.Value = dr.Item("recdate").ToString
            divrecdate.InnerHtml = dr.Item("recdate").ToString
            'lbluseby.Value = dr.Item("useby").ToString
            'divuseby.InnerHtml = dr.Item("useby").ToString
            'pvend = dr.Item("povendor").ToString
            lblpvend.Value = dr.Item("povendor").ToString
            divpvend.InnerHtml = dr.Item("povendor").ToString
            lblman.Value = dr.Item("mfg").ToString
            divman.InnerHtml = dr.Item("mfg").ToString
            divunit.InnerHtml = dr.Item("itemcost").ToString
            txtactcost.Text = dr.Item("actcost").ToString
        End While
        dr.Close()

    End Sub

    Private Sub SaveInv()
        Dim iid, inum, desc, lot, lott, pr, qo, od, pvend, mfg, uby, qr, qd, cat, model, po, act, store As String
        iid = lblcodeid.Value
        inum = lbloldcode.Value
        desc = lblcodedesc.Value
        lott = lbllottype.Value
        pr = lblpr.Value
        qo = lblqtyordered.Value
        od = lblorderdate.Value
        pvend = lblpvend.Value
        mfg = lblman.Value

        po = txtoutpo.Text
        po = Replace(po, "'", Chr(180), , , vbTextCompare)
        po = Replace(po, "--", "-", , , vbTextCompare)
        po = Replace(po, ";", ":", , , vbTextCompare)
        If Len(po) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1431" , "recvinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        lot = txtlot.Text
        lot = Replace(lot, "'", Chr(180), , , vbTextCompare)
        lot = Replace(lot, "--", "-", , , vbTextCompare)
        lot = Replace(lot, ";", ":", , , vbTextCompare)
        If Len(lot) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1432" , "recvinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        cat = lblcatnum.Value
        cat = Replace(cat, "'", Chr(180), , , vbTextCompare)
        cat = Replace(cat, "--", "-", , , vbTextCompare)
        cat = Replace(cat, ";", ":", , , vbTextCompare)
        If Len(cat) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1433" , "recvinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        model = lblmod.Value
        model = Replace(model, "'", Chr(180), , , vbTextCompare)
        model = Replace(model, "--", "-", , , vbTextCompare)
        model = Replace(model, ";", ":", , , vbTextCompare)
        If Len(model) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1434" , "recvinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        uby = lbluseby.Value
        qd = lblrecdate.Value
        qr = txtqtyrecvd.Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1435" , "recvinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        act = txtactcost.Text
        Dim actchk As Decimal
        Try
            actchk = System.Convert.ToDecimal(act)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1436" , "recvinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        store = lblstore.Value
        '@itemid int, @pr int, @lot varchar(50), @lott varchar(50), @useby datetime,
        '@qtyrecvd int, @daterecvd datetime, @vend varchar(50), @man varchar(50), @mod varchar(50), @cat varchar(50)
        sql = "usp_recvinv '" & iid & "','" & pr & "','" & lot & "','" & lott & "','" & uby & "', " _
        + "'" & qr & "','" & qd & "','" & pvend & "','" & mfg & "','" & model & "','" & cat & "','" & po & "','" & act & "','" & store & "'"
        inv.Update(sql)
    End Sub


	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2860.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2860")
        Catch ex As Exception
        End Try
        Try
            lang2861.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2861")
        Catch ex As Exception
        End Try
        Try
            lang2862.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2862")
        Catch ex As Exception
        End Try
        Try
            lang2863.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2863")
        Catch ex As Exception
        End Try
        Try
            lang2864.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2864")
        Catch ex As Exception
        End Try
        Try
            lang2865.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2865")
        Catch ex As Exception
        End Try
        Try
            lang2866.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2866")
        Catch ex As Exception
        End Try
        Try
            lang2867.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2867")
        Catch ex As Exception
        End Try
        Try
            lang2868.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2868")
        Catch ex As Exception
        End Try
        Try
            lang2869.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2869")
        Catch ex As Exception
        End Try
        Try
            lang2870.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2870")
        Catch ex As Exception
        End Try
        Try
            lang2871.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2871")
        Catch ex As Exception
        End Try
        Try
            lang2872.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2872")
        Catch ex As Exception
        End Try
        Try
            lang2873.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2873")
        Catch ex As Exception
        End Try
        Try
            lang2874.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2874")
        Catch ex As Exception
        End Try
        Try
            lang2875.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2875")
        Catch ex As Exception
        End Try
        Try
            lang2876.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2876")
        Catch ex As Exception
        End Try
        Try
            lang2877.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2877")
        Catch ex As Exception
        End Try
        Try
            lang2878.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2878")
        Catch ex As Exception
        End Try
        Try
            lang2879.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2879")
        Catch ex As Exception
        End Try
        Try
            lang2880.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2880")
        Catch ex As Exception
        End Try
        Try
            lang2881.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2881")
        Catch ex As Exception
        End Try
        Try
            lang2882.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2882")
        Catch ex As Exception
        End Try
        Try
            lang2883.Text = axlabs.GetASPXPage("recvinv.aspx", "lang2883")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "Img1") & "')")
            Img1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "Img2") & "')")
            Img2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img4.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "Img4") & "')")
            Img4.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img5.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "Img5") & "')")
            Img5.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imglot.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "imglot") & "')")
            imglot.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsrch.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "imgsrch") & "')")
            imgsrch.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid268.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "ovid268") & "')")
            ovid268.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid269.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "ovid269") & "')")
            ovid269.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid270.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "ovid270") & "')")
            ovid270.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid271.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("recvinv.aspx", "ovid271") & "')")
            ovid271.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class

