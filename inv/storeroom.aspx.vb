

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class storeroom1
    Inherits System.Web.UI.Page
	

	Protected WithEvents ovid274 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid273 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2955 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2954 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2953 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2952 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2951 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2950 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2949 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2948 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2947 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2946 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2945 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2944 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2943 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2942 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2941 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2940 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim inv As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList

    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow



    Protected WithEvents lblcurbal As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstore As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divcode As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divdesc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divstore As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divstdesc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divcurval As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divlast As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divres As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divexp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divavail As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents divbin As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divcat As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim invs, ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents Tr5 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdalt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchild As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblneweq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox

    Protected WithEvents txtbin As System.Web.UI.WebControls.TextBox




    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                Img1.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                Img1.Attributes.Add("onclick", "")
            End If
            Try
                invs = Request.QueryString("inv").ToString
                lblcode.Value = invs
                divcode.InnerHtml = invs
                lbloldcode.Value = invs
                inv.Open()
                SrchInv()
                GetAlt()
                inv.Dispose()
            Catch ex As Exception

            End Try
        Else
            If Request.Form("lblsubmit") = "get" Then
                lblsubmit.Value = ""
                inv.Open()
                SrchInv()
                GetAlt()
                inv.Dispose()
            ElseIf Request.Form("lblsubmit") = "getstore" Then
                lblsubmit.Value = ""
                inv.Open()
                SrchInv()
                GetAlt()
                inv.Dispose()
            ElseIf Request.Form("lblsubmit") = "save" Then
                lblsubmit.Value = ""
                inv.Open()
                SaveSRD()
                SrchInv()
                GetAlt()
                inv.Dispose()

            End If
        End If
    End Sub
    Private Sub SaveSRD()
        Dim item, store, cat, bin, typ As String
        item = lbloldcode.Value
        store = lblstore.Value
        cat = divcat.InnerHtml
        bin = txtbin.Text
        bin = Replace(bin, "'", Chr(180), , , vbTextCompare)
        If Len(bin) > 10 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1508" , "storeroom.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        typ = lbltype.Value
        If typ = "item" Then
            typ = "part"
        ElseIf typ = "lubricants" Then
            typ = "lube"
        End If
        sql = "update inventory set pm3type = '" & typ & "', bin = '" & bin & "', category = '" & cat & "' " _
        + "where itemnum = '" & item & "'"
        inv.Update(sql)
    End Sub
    Private Sub SaveInv()
        Dim invs As String = lblcode.Value
        Dim desc As String = divdesc.InnerHtml
        invs = Replace(invs, "'", Chr(180), , , vbTextCompare)
        If Len(invs) > 30 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1509" , "storeroom.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        If Len(desc) > 150 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1510" , "storeroom.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim oinvs As String = lbloldcode.Value
        Dim cnt As Integer
        If invs <> oinvs Then
            sql = "select count(*) from item where itemnum = '" & invs & "'"
            cnt = inv.Scalar(sql)
            If cnt <> 0 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1511" , "storeroom.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If
        Dim store, cat, bin, abc, ccf As String
        store = lblstore.Value
        cat = divcat.InnerHtml
        bin = txtbin.Text
        bin = Replace(bin, "'", Chr(180), , , vbTextCompare)
        If Len(bin) > 10 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1512" , "storeroom.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        'abc = txtabc.Text
        'ccf = txtcnt.Text
        If Len(ccf) > 0 Then
            Try
                If Double.IsNaN(ccf) Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1513" , "storeroom.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1514" , "storeroom.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        Else
            ccf = "0"
        End If
        sql = "update item set itemnum = '" & invs & "', description = '" & desc & "' where itemnum = '" & invs & "' " _
        + "update inventory set itemnum = '" & invs & "', location = '" & store & "', binnum = '" & bin & "', category = '" & cat & "', " _
        + "abctype = '" & abc & "', ccf = '" & ccf & "' where itemnum = '" & invs & "' " _
        + "update invbalances set itemnum = '" & invs & "', location = '" & store & "',  binnum = '" & bin & "' where itemnum = '" & invs & "' "
        inv.Update(sql)

    End Sub

    Private Sub SrchInv()
        'Dim invs As String = txtcode.Text
        'sql = "select i.*, v.*, l.description as 'ldesc', b.curbal from item i " _
        '+ "left join inventory v on v.itemnum = i.itemnum " _
        '+ "left join locations l on l.location = v.location " _
        '+ "left join invbalances b on b.itemnum = i.itemnum where i.itemnum = '" & invs & "'"
        Dim typ As String = ddtype.SelectedValue.ToString
        Dim invs As String = lblcode.Value
        Dim filt As String = lblstore.Value
        If typ = "part" Then
            If filt = "" Then
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, v.location as loc, l.description as 'ldesc', b.curbal from item i " _
                + "left join inventory v on v.itemnum = i.itemnum " _
                + "left join invvallist l on l.value = v.location " _
                + "left join invbalances b on b.itemnum = i.itemnum and b.location = v.location where i.itemnum = '" & invs & "'"
            Else
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.location as loc, l.description as 'ldesc', b.curbal from item i " _
                + "left join inventory v on v.itemnum = i.itemnum " _
                + "left join invbalances b on b.itemnum = i.itemnum " _
                + "left join invvallist l on l.value = b.location " _
                + "where i.itemnum = '" & invs & "'"
                sql += " and b.location = '" & filt & "'"
            End If

        ElseIf typ = "tool" Then
            If filt = "" Then
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, v.location as loc, l.description as 'ldesc', b.curbal from tool i " _
                + "left join inventory v on v.itemnum = i.toolnum " _
                + "left join invvallist l on l.value = v.location " _
                + "left join invbalances b on b.itemnum = i.toolnum and b.location = v.location where i.toolnum = '" & invs & "'"
            Else
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.location as loc, l.description as 'ldesc', b.curbal from tool i " _
               + "left join inventory v on v.itemnum = i.toolnum " _
               + "left join invbalances b on b.itemnum = i.toolnum " _
               + "left join invvallist l on l.value = b.location " _
               + "where i.toolnum = '" & invs & "'"
                sql += " and b.location = '" & filt & "'"
            End If

        ElseIf typ = "lube" Then
            If filt = "" Then
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, v.location as loc, l.description as 'ldesc', b.curbal from lubricants i " _
                + "left join inventory v on v.itemnum = i.lubenum " _
                + "left join invvallist l on l.value = v.location " _
                + "left join invbalances b on b.itemnum = i.lubenum and b.location = v.location where i.lubenum = '" & invs & "'"
            Else
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.location as loc, l.description as 'ldesc', b.curbal from lubricants i " _
                + "left join inventory v on v.itemnum = i.lubenum " _
                + "left join invbalances b on b.itemnum = i.lubenum " _
                + "left join invvallist l on l.value = b.location " _
                + "where i.lubenum = '" & invs & "'"
                sql += " and b.location = '" & filt & "'"

            End If
           
        End If



        dr = inv.GetRdrData(sql)
        While dr.Read
            lblitemid.Value = dr.Item("itemid").ToString
            lblcode.Value = dr.Item("itemnum").ToString
            divcode.InnerHtml = dr.Item("itemnum").ToString
            lbloldcode.Value = dr.Item("itemnum").ToString
            divdesc.InnerHtml = dr.Item("description").ToString
            'txtstore.Text = dr.Item("location").ToString
            lblstore.Value = dr.Item("loc").ToString
            divstore.InnerHtml = dr.Item("loc").ToString
            divstdesc.InnerHtml = dr.Item("ldesc").ToString
            txtbin.Text = dr.Item("bin").ToString
            Try
                divcat.InnerHtml = dr.Item("category").ToString
            Catch ex As Exception

            End Try
            divcurval.InnerHtml = dr.Item("curbal").ToString
            lblcurbal.Value = dr.Item("curbal").ToString
            divlast.InnerHtml = dr.Item("last").ToString
            'txtytd.Text = dr.Item("issueytd").ToString
            'txtabc.Text = dr.Item("abctype").ToString
            'txtcnt.Text = dr.Item("ccf").ToString
            'txt1yr.Text = dr.Item("issue1yrago").ToString
            'txt2yr.Text = dr.Item("issue2yrago").ToString
            'txt3yr.Text = dr.Item("issue3yrago").ToString
            lblsubmit.Value = "new"
            lbltype.Value = typ
        End While
        dr.Close()

        Dim res, exp, avail, cb As Decimal
        Dim cbstr As String '= txtcurbal.Text
        'cb = CType(cbstr, Decimal)
        Try
            sql = "select sum(curbal) from invbalances where itemnum = '" & invs & "'"
            cb = inv.Scalar(sql)
        Catch ex As Exception
            cb = 0
        End Try
        Try
            sql = "select sum(reserved) from invbalances where itemnum = '" & invs & "'"
            res = inv.Scalar(sql)
        Catch ex As Exception
            res = 0
        End Try
        Try
            sql = "select sum(curbal) from invlot where itemnum = '" & invs & "' and Convert(char(10),useby, 101) <= Convert(char(10),getDate(), 101)"
            exp = inv.Scalar(sql)
        Catch ex As Exception
            exp = 0
        End Try

        avail = cb - (res + exp)
        divres.InnerHtml = res
        divexp.InnerHtml = exp
        divavail.InnerHtml = avail



    End Sub
    Private Sub GetAlt()

        Dim invs As String = lbloldcode.Value
        Dim img As String = "../images/magnifier.gif"
        Dim img1 As String = "../images/2PX.gif"
        Dim img3 As String = "../images/magnifierdis.gif"
        Dim nimg As String = "<img src=""" & img3 & """>"
        Dim msg As String = "onmouseover = ""return overlib('" & tmod.getov("cov294" , "storeroom.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim msg1 As String = "onmouseover = ""return overlib('" & tmod.getov("cov295" , "storeroom.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim simg As String = "<img src=""" & img & """ onclick=""hlookup("
        Dim simg1 As String = ");""" & msg & ">"
        'Dim spimg As String = "<img src=""" & img & """ onclick=""jumpto('" & planstr & "');""" & msg1 & ">"

        Dim sb As New System.Text.StringBuilder
        sb.Append("<table width=""710"" cellpadding=""0"">")
        'sb.Append("<tr><td width=""90""></td><td width=""90""></td><td width=""90""></td><td width=""90""></td><td width=""90""></td><td width=""90""></td><td width=""90""></td><td width=""90""></td><td width=""90""></td><td width=""90""></td><td width=""90""></td>")

        'sb.Append("<td width=""20"">&nbsp;</td></tr>")

        sb.Append("<tr><td colspan=""11"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 690px;  HEIGHT: 110px"">")
        sb.Append("<table width=""1110"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl568" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl569" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl570" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl571" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl572" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl573" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl574" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl575" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl576" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl577" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl578" , "storeroom.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">" & tmod.getlbl("cdlbl579" , "storeroom.aspx.vb") & "</td></tr>")


        Dim start As Integer = 0
        Dim lineid As Integer = 0


        sql = "select distinct b.*, l.*, Convert(char(10),b.physcntdate, 101) as phyd, Convert(char(10),l.useby, 101) as exp " _
        + "from invbalances b left join invlot l on l.lotnum = b.lotnum where b.itemnum = '" & invs & "'"


        Dim bin, lot, cur, phy, phyd, rec, exp, shelf, mlot, mfg, vend, store As String
        dr = inv.GetRdrData(sql)
        While dr.Read
            store = dr.Item("location").ToString
            If Len(store) = 0 Then
                store = "&nbsp;"
            End If
            bin = dr.Item("bin").ToString
            If Len(bin) = 0 Then
                bin = "&nbsp;"
            End If
            lot = dr.Item("lotnum").ToString
            If Len(lot) = 0 Then
                lot = "&nbsp;"
            End If
            cur = dr.Item("curbal").ToString
            phy = dr.Item("physcnt").ToString
            phyd = dr.Item("phyd").ToString
            If Len(phyd) = 0 Then
                phyd = "&nbsp;"
            End If
            rec = dr.Item("reconciled").ToString
            shelf = dr.Item("shelflife").ToString
            If Len(shelf) = 0 Then
                shelf = "&nbsp;"
            End If
            exp = dr.Item("exp").ToString
            If Len(exp) = 0 Then
                exp = "&nbsp;"
            End If
            mlot = dr.Item("mfglotnum").ToString
            If Len(mlot) = 0 Then
                mlot = "&nbsp;"
            End If
            mfg = dr.Item("mfg").ToString
            If Len(mfg) = 0 Then
                mfg = "&nbsp;"
            End If
            vend = dr.Item("vendor").ToString
            If Len(vend) = 0 Then
                vend = "&nbsp;"
            End If


            sb.Append("<tr><td class=""plainlabel grayborder"">" & store & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & bin & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & lot & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & cur & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & phy & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & phyd & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & rec & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & shelf & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & exp & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & mlot & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & vend & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & mfg & "</td><tr>")


        End While

        dr.Close()

        'Dim i As Integer
        'If start = 0 Then
        'If invs <> "" Then
        'lineid += 1
        'sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdbloc1"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype1"">&nbsp;</td></tr>")
        'End If
        'For i = 0 To 6
        'sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
        'Next
        'Else
        'For i = 0 To 5
        'lineid += 1
        'sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdbloc" & lineid & """>&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        'sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype" & lineid & """>&nbsp;</td></tr>")
        'Next
        'End If

        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdalt.InnerHtml = sb.ToString
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2940.Text = axlabs.GetASPXPage("storeroom.aspx","lang2940")
		Catch ex As Exception
		End Try
		Try
			lang2941.Text = axlabs.GetASPXPage("storeroom.aspx","lang2941")
		Catch ex As Exception
		End Try
		Try
			lang2942.Text = axlabs.GetASPXPage("storeroom.aspx","lang2942")
		Catch ex As Exception
		End Try
		Try
			lang2943.Text = axlabs.GetASPXPage("storeroom.aspx","lang2943")
		Catch ex As Exception
		End Try
		Try
			lang2944.Text = axlabs.GetASPXPage("storeroom.aspx","lang2944")
		Catch ex As Exception
		End Try
		Try
			lang2945.Text = axlabs.GetASPXPage("storeroom.aspx","lang2945")
		Catch ex As Exception
		End Try
		Try
			lang2946.Text = axlabs.GetASPXPage("storeroom.aspx","lang2946")
		Catch ex As Exception
		End Try
		Try
			lang2947.Text = axlabs.GetASPXPage("storeroom.aspx","lang2947")
		Catch ex As Exception
		End Try
		Try
			lang2948.Text = axlabs.GetASPXPage("storeroom.aspx","lang2948")
		Catch ex As Exception
		End Try
		Try
			lang2949.Text = axlabs.GetASPXPage("storeroom.aspx","lang2949")
		Catch ex As Exception
		End Try
		Try
			lang2950.Text = axlabs.GetASPXPage("storeroom.aspx","lang2950")
		Catch ex As Exception
		End Try
		Try
			lang2951.Text = axlabs.GetASPXPage("storeroom.aspx","lang2951")
		Catch ex As Exception
		End Try
		Try
			lang2952.Text = axlabs.GetASPXPage("storeroom.aspx","lang2952")
		Catch ex As Exception
		End Try
		Try
			lang2953.Text = axlabs.GetASPXPage("storeroom.aspx","lang2953")
		Catch ex As Exception
		End Try
		Try
			lang2954.Text = axlabs.GetASPXPage("storeroom.aspx","lang2954")
		Catch ex As Exception
		End Try
		Try
			lang2955.Text = axlabs.GetASPXPage("storeroom.aspx","lang2955")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			Img1.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("storeroom.aspx","Img1") & "')")
			Img1.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgsrch.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("storeroom.aspx","imgsrch") & "')")
			imgsrch.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid273.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("storeroom.aspx","ovid273") & "')")
			ovid273.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			ovid274.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("storeroom.aspx","ovid274") & "')")
			ovid274.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		

	End Sub

End Class
