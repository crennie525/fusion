<%@ Register TagPrefix="uc1" TagName="menu2" Src="../menu/menu2.ascx" %>
<%@ Register TagPrefix="uc1" TagName="menu1" Src="../menu/menu1.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Inventory.aspx.vb" Inherits="lucy_r12.Inventory" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Inventory</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<style>.details { DISPLAY: none; VISIBILITY: hidden; FONT-FAMILY: Verdana; BACKGROUND-COLOR: white }
		</style>
		<script language="javascript" src="../scripts/inventory.js"></script>
		<script language="JavaScript" src="../scripts1/Inventoryaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body bgColor="white" >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 140px; POSITION: absolute; TOP: 81px" cellSpacing="2" cellPadding="2"
				width="700">
				<tr>
					<td colspan="7">
						<table cellspacing="0">
							<tr height="22">
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="724"><asp:Label id="lang2729" runat="server">PM Inventory Administration</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><IMG height="6" src="../images/appbuttons/minibuttons/6PX.gif" width="6"></td>
				</tr>
				<tr>
					<td class="details" colSpan="7">&nbsp;
						<asp:label id="lbltab" runat="server" ForeColor="white">part</asp:label></td>
				</tr>
				<tr>
					<td class="thdrhov plainlabel" id="parttab" onclick="gettab('part');" width="100"><asp:Label id="lang2730" runat="server">Parts</asp:Label></td>
					<td class="thdr plainlabel" id="tooltab" onclick="gettab('tool');" width="100"><asp:Label id="lang2731" runat="server">Tools</asp:Label></td>
					<td class="thdr plainlabel" id="lubetab" onclick="gettab('lube');" width="100"><asp:Label id="lang2732" runat="server">Lubricants</asp:Label></td>
					<td width="100"></td>
					<td width="100"></td>
					<td width="100"></td>
					<td width="100"></td>
				</tr>
			</table>
			<div id="partdiv" style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; LEFT: 138px; MARGIN-LEFT: 4px; BORDER-LEFT: 2px groove; WIDTH: 700px; BORDER-BOTTOM: 2px groove; POSITION: absolute; TOP: 142px; HEIGHT: 400px">
				<table cellSpacing="0" cellPadding="0" width="690">
					<tr>
						<td colSpan="2">&nbsp;</td>
					</tr>
					<tr>
						<td width="10"></td>
						<td><asp:label id="lblpart" runat="server" ForeColor="Red" Font-Size="X-Small" Font-Names="Arial"
								Font-Bold="True" Width="480px"></asp:label></td>
					</tr>
					<tr>
						<td></td>
						<TD width="680"><asp:datagrid id="dgparts" runat="server" ShowFooter="True" AutoGenerateColumns="False" AllowCustomPaging="True"
								AllowPaging="True" GridLines="None" CellPadding="1">
								<FooterStyle BackColor="White"></FooterStyle>
								<EditItemStyle Height="15px"></EditItemStyle>
								<AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD"></AlternatingItemStyle>
								<ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight"></ItemStyle>
								<HeaderStyle Height="22px"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit">
										<HeaderStyle Height="20px" Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="ImageButton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
										</ItemTemplate>
										<FooterTemplate>
											<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
												CommandName="Add"></asp:ImageButton>
										</FooterTemplate>
										<EditItemTemplate>
											<asp:ImageButton id="ImageButton13" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
											<asp:ImageButton id="ImageButton14" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lblitemidn" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lblitemid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Part#">
										<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<FooterTemplate>
											<asp:textbox id="txtpname" runat="server" Width="100px" MaxLength="50"></asp:textbox>
										</FooterTemplate>
										<EditItemTemplate>
											<asp:TextBox id=TextBox1 runat="server" Width="75px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label5 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
										<FooterTemplate>
											<asp:textbox id="txtpdesc" runat="server" Width="240px" MaxLength="100"></asp:textbox>
										</FooterTemplate>
										<EditItemTemplate>
											<asp:TextBox id=TextBox2 runat="server" Width="245px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>' TextMode="MultiLine">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Location">
										<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label6 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
											</asp:Label>
										</ItemTemplate>
										<FooterTemplate>
											<asp:textbox id="txtploc" runat="server" Width="150px" MaxLength="50"></asp:textbox>
										</FooterTemplate>
										<EditItemTemplate>
											<asp:TextBox id=Textbox3 runat="server" Width="135px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>' TextMode="MultiLine">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remove">
										<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
												CommandName="Delete"></asp:ImageButton>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
									ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
							</asp:datagrid></TD>
					</tr>
				</table>
				<table id="noparthdr" cellSpacing="0" cellPadding="0" width="690" runat="server">
					<tr>
						<td width="10"></td>
						<td width="80"></td>
						<td width="112"></td>
						<td width="252"></td>
						<td width="236"></td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="690">
					<tr>
						<td colSpan="4">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><asp:label id="lblpgparts" runat="server" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
								Font-Bold="True"></asp:label></td>
						<td align="right"><asp:imagebutton id="partPrev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton></td>
						<td align="right"><asp:imagebutton id="partNext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton></td>
					</tr>
					<tr>
						<td width="10"></td>
						<td width="540"></td>
						<td width="70"></td>
						<td width="70"></td>
					</tr>
				</table>
			</div>
			<div class="details" id="tooldiv" style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; LEFT: 138px; MARGIN-LEFT: 3px; BORDER-LEFT: 2px groove; WIDTH: 700px; BORDER-BOTTOM: 2px groove; POSITION: absolute; TOP: 638px; HEIGHT: 400px"><iframe id="iftool" style="WIDTH: 700px; HEIGHT: 400px" name="iftool" src="" frameBorder="no"
					runat="server"></iframe>
			</div>
			<div class="details" id="lubediv" style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; LEFT: 138px; MARGIN-LEFT: 3px; BORDER-LEFT: 2px groove; WIDTH: 700px; BORDER-BOTTOM: 2px groove; POSITION: absolute; TOP: 638px; HEIGHT: 400px"><iframe id="iflube" style="WIDTH: 700px; HEIGHT: 400px" name="iflube" src="" frameBorder="no"
					runat="server"></iframe>
			</div>
			<table class="details">
			</table>
			<input id="lblcid" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:menu1 id="Menu11" runat="server"></uc1:menu1>
	</body>
</HTML>
