<%@ Page Language="vb" AutoEventWireup="false" Codebehind="whereused.aspx.vb" Inherits="lucy_r12.whereused" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>whereused</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/whereusedaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function GetItem(typ, fld1, fld2) {
             if (typ == "srch") {
                 list = document.getElementById("ddtype").value;
                 document.getElementById("lbltype").value = list;
             }
             else {
                 list = typ;
             }
             var eReturn = window.showModalDialog("invdialog.aspx?filt=no&list=" + list + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
             if (eReturn) {
                 if (eReturn == "log") {
                     window.parent.handlelogout();
                 }
                 else if (eReturn != "can") {
                     var ret = eReturn;
                     var retarr = ret.split(";")
                     if (typ == "srch") {
                         if (fld1 == "alt") {
                             document.getElementById("lblchildid").value = retarr[0];
                             document.getElementById("lblchild").value = retarr[1];
                             var ro = document.getElementById("lblro").value;
                             if (ro != "1") {
                                 document.getElementById("lblsubmit").value = "addchild";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else {
                             document.getElementById("lblcode").value = retarr[1];
                             document.getElementById("lbloldcode").value = retarr[1];
                             document.getElementById("lblret").value = "get";
                             //alert(document.getElementById("lblsubmit").value)
                             document.getElementById("form1").submit();
                         }
                     }
                     else if (typ == "storeroom") {
                         document.getElementById(fld1).value = retarr[1];
                         document.getElementById(fld2).value = retarr[2];
                     }
                     else {
                         document.getElementById(fld1).value = retarr[1];
                     }

                 }
             }
         }
         function checkinv() {
             var chk = document.getElementById("lblsubmit").value;
             if (chk == "new") {
                 document.getElementById("lblsubmit").value == "";
                 var ni = document.getElementById("lblcode").value;
                 var ty = document.getElementById("lbltype").value;
                 window.parent.handlenew(ni, ty);
             }
         }
     //-->
     </script>
     <style type="text/css">
          .readonly50
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 50px;
}
         .readonly90
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 90px;
}
     .readonly140
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 140px;
}
.readonly340
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 340px;
}
.readonly440
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 530px;
}
</style>
	</HEAD>
	<body  onload="checkinv();checkit();" class="tbg">
		<form id="form1" method="post" runat="server">
		<div style="position: absolute; top: 4px; left: 4px">
			<table cellSpacing="0" width="800">
			<tr>
				<td align="center" valign="middle" class="plainlabelred" colspan="2"><asp:Label id="lang2959" runat="server">Use the standard PM Inventory module if your Inventory is controlled outside of Fusion.</asp:Label></td>
				</tr>
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2960" runat="server">Current Item</asp:Label></td>
				</TR>
				
				<tr>
					<td vAlign="top" colSpan="2">
						<table cellPadding="0" width="770" border="0">
							<tr>
								<td width="100"></td>
								<td width="180"></td>
								<td width="80"></td>
								<td width="80"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="172"></td>
							</tr>
							<tr id="Tr5" runat="server">
								<td class="label"><asp:Label id="lang2961" runat="server">Item Code</asp:Label></td>
								<td><div class="readonly140" id="divcode" runat="server">
                                        </div></td>
								<td class="label"><asp:Label id="lang2962" runat="server" CssClass="details">Item Type</asp:Label></td>
								<td><asp:dropdownlist id="ddtype" runat="server" Enabled="False" CssClass="details">
										<asp:ListItem Value="part" Selected="True">Part</asp:ListItem>
										<asp:ListItem Value="tool">Tool</asp:ListItem>
										<asp:ListItem Value="lube">Lube</asp:ListItem>
									</asp:dropdownlist></td>
								<td><IMG id="imgsrch" onclick="GetItem('srch');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('Look up Inventory Items')" onmouseout="return nd()"></td>
								<td><IMG onclick="jumpto('');" src="../images/appbuttons/minibuttons/refreshit.gif" onmouseover="return overlib('Clear Page')"
										onmouseout="return nd()"></td>
								<td></td>
								<td></td>
							</tr>
							<tr id="deptdiv" runat="server">
								<td class="label"><asp:Label id="lang2963" runat="server">Description</asp:Label></td>
								<td colSpan="10"><div class="readonly440" id="divdesc" runat="server">
                                        </div></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2964" runat="server">Where Used</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr height="22">
								<td class="bluelabel"><asp:Label id="lang2965" runat="server">Add an Equipment Record</asp:Label></td>
								<td><IMG id="Img1" onclick="GetLocEq();" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<div style="OVERFLOW: auto; HEIGHT: 310px; width: 770px;">
							<asp:datagrid id="dgitems" runat="server" GridLines="None" AutoGenerateColumns="False" cellPadding="1"
								cellSpacing="1" ShowFooter="True" AllowCustomPaging="True" PageSize="50" AllowPaging="True"
								BackColor="transparent">
								<AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
								<ItemStyle CssClass="transrow plainlabel"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit">
										<HeaderStyle Height="24px" Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:imagebutton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
											<asp:imagebutton id="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Equipment#">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=itid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="180px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label12 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqdesc") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqdesc") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty">
										<HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.quantity") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txtqty" width="40" CssClass="plainlabel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.quantity") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Notes">
										<HeaderStyle Width="330px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label1 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.notes") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id=txtnote runat="server" Width="320px" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.notes") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Notes" Visible="False">
										<HeaderStyle Width="380px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.spid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lblspid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.spid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Visible="False"></PagerStyle>
							</asp:datagrid></div>
					</td>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img2" onclick="getfirst('new');" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img3" onclick="getprev('new');" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblnpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img4" onclick="getnext('new');" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="Img5" onclick="getlast('new');" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
			<input type="hidden" id="txtpg" runat="server" NAME="txtpg"> <input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit"> <input type="hidden" id="lbleqid" runat="server" NAME="lbleqid">
			<input type="hidden" id="lbloldcode" runat="server" NAME="lbloldcode"> <input type="hidden" id="lbltype" runat="server" NAME="lbltype">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"><input type="hidden" id="lbleqnum" runat="server">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblcode" runat="server" />
</form>
	</body>
</HTML>
