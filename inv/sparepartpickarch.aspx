<%@ Page Language="vb" AutoEventWireup="false" Codebehind="sparepartpickarch.aspx.vb" Inherits="lucy_r12.sparepartpickarch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>sparepartpickarch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnavopt_1.js"></script>
		<script language="JavaScript" src="../scripts1/sparepartpickarchaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkscroll();" >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" width="600">
				<tr>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/sparepartshdr.gif" border="0"></td>
								<td class="thdrsingrt label" width="574"><asp:Label id="lang2930" runat="server">Choose Spare Parts Mini Dialog</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="24">
					<td class="label" width="120"><asp:Label id="lang2931" runat="server">Current Equipment#</asp:Label></td>
					<td class="label" id="tdtask" width="400" runat="server"></td>
					<td align="right" width="80"></td>
				</tr>
				<tr height="24">
					<td class="bluelabel" colSpan="3"><asp:Label id="lang2932" runat="server">Equipment Spare Parts</asp:Label></td>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<div id="spdiv" style="OVERFLOW: auto; HEIGHT: 100px" onscroll="SetsDivPosition();"><asp:datagrid id="dgparttasks" runat="server" cellSpacing="1" cellPadding="3" AutoGenerateColumns="False"
								GridLines="None">
								<AlternatingItemStyle BackColor="#E7F1FD" CssClass="plainlabel"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit" Visible="False">
										<HeaderStyle Width="80px" CssClass="btmmenu plainlabel" Height="24px"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:imagebutton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
											<asp:imagebutton id="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Part#">
										<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label27" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Part#" Visible="False">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lblspid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.spid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lblspide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.spid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lblsitemid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.spid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Spare Part Description (Default is Part Description)">
										<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:label id="txtsdesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="lblsqty1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.quantity") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="lblsqty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.quantity") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Cost">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total" Visible="False">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label19" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remove" Visible="False">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											&nbsp;&nbsp;&nbsp;
											<asp:ImageButton id="Imagebutton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
												CommandName="Delete"></asp:ImageButton>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></div>
					</td>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img1" onclick="getfirst('new');" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img2" onclick="getprev('new');" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblnpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img3" onclick="getnext('new');" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="Img4" onclick="getlast('new');" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<hr color="blue">
					</td>
				</tr>
				<tr id="olab" height="24" runat="server">
					<td class="bluelabel" colSpan="3"><asp:Label id="lang2933" runat="server">Selected Spare Parts (Original)</asp:Label></td>
				</tr>
				<tr id="odiv" runat="server">
					<td colSpan="3">
						<div id="ospdiv" style="OVERFLOW: auto; HEIGHT: 100px" onscroll="SetoDivPosition();"><asp:datagrid id="dgoparttasks" runat="server" cellSpacing="1" AutoGenerateColumns="False" GridLines="None"
								CellPadding="1">
								<FooterStyle BackColor="White"></FooterStyle>
								<AlternatingItemStyle BackColor="#E7F1FD" CssClass="plainlabel"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
								<Columns>
									
									<asp:TemplateColumn HeaderText="Part#">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lbltskprtid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tskpartid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lbltskprtide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tskpartid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label13" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label21" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="dgoqty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Cost">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label22" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label23" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label24" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label25" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									
								</Columns>
							</asp:datagrid></div>
					</td>
				</tr>
				<tr id="onav" runat="server">
					<td align="center" colSpan="3">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst('old');" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev('old');" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext('old');" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast('old');" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="osep" runat="server">
					<td colSpan="3">
						<hr color="blue">
					</td>
				</tr>
				<tr height="24">
					<td class="bluelabel" colSpan="3"><asp:Label id="lang2934" runat="server">Selected Spare Parts (Revised)</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="3">
						<div id="ispdiv" style="OVERFLOW: auto; HEIGHT: 100px" onscroll="SetiDivPosition();"><asp:datagrid id="dgtaskparts" runat="server" cellSpacing="1" cellPadding="1" AutoGenerateColumns="False"
								GridLines="None">
								<AlternatingItemStyle BackColor="#E7F1FD" CssClass="plainlabel"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
								<Columns>
									
									<asp:TemplateColumn HeaderText="Part#">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lbltskprtidr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lineid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lbltskprtidre" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lineid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="dgqty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Cost">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label10" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label12" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label26" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									
								</Columns>
							</asp:datagrid></div>
					</td>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img5" onclick="getfirst('i');" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img6" onclick="getprev('i');" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblipg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img7" onclick="getnext('i');" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="Img8" onclick="getlast('i');" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="txtnpg" type="hidden" name="Hidden1" runat="server"><input id="txtnpgcnt" type="hidden" name="txtnpgcnt" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="txtipgcnt" type="hidden" name="txtipgcnt" runat="server">
			<input id="txtipg" type="hidden" name="Hidden2" runat="server"> <input id="lblptid" type="hidden" name="lblptid" runat="server">
			<input id="lbltyp" type="hidden" name="lbltyp" runat="server"> <input id="lblwo" type="hidden" name="lblwo" runat="server">
			<input id="lblshow" type="hidden" name="lblshow" runat="server"> <input id="lbloflag" type="hidden" name="lbloflag" runat="server">
			<input id="lbljpnum" type="hidden" name="lbljpnum" runat="server"> <input id="lbladdid" type="hidden" name="lbladdid" runat="server">
			<input id="lbladdnum" type="hidden" name="lbladdnum" runat="server"> <input id="lbladdqty" type="hidden" name="lbladdqty" runat="server">
			<input id="txtopgcnt" type="hidden" name="Hidden1" runat="server"> <input id="txtopg" type="hidden" name="Hidden1" runat="server">
			<input id="spdivy" type="hidden" name="spdivy" runat="server"> <input id="ospdivy" type="hidden" name="Hidden1" runat="server">
			<input id="ispdivy" type="hidden" name="Hidden1" runat="server"> <input id="lbleqnum" type="hidden" name="lbleqnum" runat="server">
			<input id="xCoord" type="hidden" name="xCoord" runat="server"> <input id="yCoord" type="hidden" name="yCoord" runat="server">
			<input id="lblro" type="hidden" name="lblro" runat="server"> <input id="lblrev" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
