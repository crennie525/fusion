<%@ Page Language="vb" AutoEventWireup="false" Codebehind="binlook.aspx.vb" Inherits="lucy_r12.binlook" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>binlook</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/binlookaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="300" cellspacing="0">
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="264"><asp:Label id="lang2666" runat="server">Bin Look Up</asp:Label></td>
				</TR>
				<tr>
					<td colspan="2" id="tdbin" runat="server">
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblstore" runat="server" NAME="lblstore"> <input type="hidden" id="lblitemid" runat="server" NAME="lblitemid">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
