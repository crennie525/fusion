

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class whereused
    Inherits System.Web.UI.Page
	Protected WithEvents ovid276 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2965 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2964 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2963 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2962 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2961 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2960 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2959 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim iPageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim cid, cnm, typ As String
    Dim sort As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim parts As New Utilities
    Dim eqid, eqnum, item, invs, ty, ro As String
    Dim decPgNav As Decimal
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divcode As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim intPgNav As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList

    Protected WithEvents Tr5 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents dgitems As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblnpg As System.Web.UI.WebControls.Label
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents divdesc As System.Web.UI.HtmlControls.HtmlGenericControl
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                Img1.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                Img1.Attributes.Add("onclick", "")
                dgitems.Columns(0).Visible = False
            End If
            Try
                invs = Request.QueryString("inv").ToString
                lblcode.Value = invs
                divcode.InnerHtml = invs
                lbloldcode.Value = invs
                ty = Request.QueryString("typ").ToString
                lbltype.Value = ty
                txtpg.Value = "1"
                If ty = "part" Then
                    parts.Open()
                    SrchInv()
                    PopTasks(PageNumber)
                    parts.Dispose()
                End If

            Catch ex As Exception

            End Try

        Else
            If Request.Form("lblret") = "next" Then
                parts.Open()
                GetNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                parts.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                parts.Open()
                GetPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                parts.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "get" Then
                lblret.Value = ""
                parts.Open()
                SrchInv()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
            ElseIf Request.Form("lblret") = "saveeq" Then
                lblret.Value = ""
                parts.Open()
                SaveEq()
                PageNumber = txtpg.Value
                PopTasks(PageNumber)
                parts.Dispose()
            End If
        End If
    End Sub
    Private Sub SaveEq()
        eqid = lbleqid.Value
        item = lbloldcode.Value
        Dim eqnum As String = lbleqnum.Value
        Dim cnt As Integer
        sql = "select count(*) from invsparepart where eqid = '" & eqid & "' and itemnum = '" & item & "'"
        cnt = parts.Scalar(sql)
        If cnt = 0 Then
            sql = "insert into invsparepart (eqid, eqnum, itemid, itemnum, quantity, cost, total) select '" & eqid & "', " _
            + "'" & eqnum & "', itemid, itemnum, '1', cost, cost from item where itemnum = '" & item & "'"
            parts.Update(sql)
        Else
            Dim strMessage As String = "This Equipment Number is already in the list"

            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub SrchInv()
        Dim typ As String = ddtype.SelectedValue.ToString
        Dim invs As String = lbloldcode.Value
        If typ = "part" Then
            sql = "select i.*, v.*, l.description as 'ldesc', b.curbal from item i " _
            + "left join inventory v on v.itemnum = i.itemnum " _
            + "left join invvallist l on l.value = v.location " _
            + "left join invbalances b on b.itemnum = i.itemnum where i.itemnum = '" & invs & "'"
        Else
            Exit Sub
        End If

        dr = parts.GetRdrData(sql)
        While dr.Read
            lblcode.Value = dr.Item("itemnum").ToString
            divcode.InnerHtml = dr.Item("itemnum").ToString
            lbloldcode.Value = dr.Item("itemnum").ToString
            divdesc.innerhtml = dr.Item("description").ToString
            lblsubmit.Value = "new"
            lbltype.Value = typ
        End While
        dr.Close()
        PageNumber = 1
        PopTasks(PageNumber)
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        txtpg.Value = PageNumber
        Dim intPgCnt As Integer
        item = lblcode.Value
        sql = "select count(*) from invsparepart where itemnum = '" & item & "'"
        Tables = "invsparepart"
        PK = "spid"
        Filter = "itemnum = ''" & item & "''"
        Fields = "*, eqdesc = (select eqdesc from equipment where eqid = invsparepart.eqid)"
        intPgCnt = parts.Scalar(sql)
        intPgNav = parts.PageCount(intPgCnt, PageSize)
        dr = parts.GetPage(Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group)
        dgitems.DataSource = dr
        dgitems.DataBind()
        dr.Close()
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblnpg.Text = "Page 0 of 0"
        Else
            lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        ro = lblro.Value
        If ro = "1" Then
            dgitems.Columns(0).Visible = False
        End If
    End Sub
    Private Sub GetnNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1515" , "whereused.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetnPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1516" , "whereused.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgitems_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.EditCommand
        dgitems.EditItemIndex = e.Item.ItemIndex
        PageNumber = txtpg.Value
        parts.Open()
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgitems_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.CancelCommand
        parts.Open()
        dgitems.EditItemIndex = -1
        PageNumber = txtpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgitems_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.UpdateCommand
        Dim desc, spid, qty As String
        spid = CType(e.Item.FindControl("lblspid"), Label).Text
        desc = CType(e.Item.FindControl("txtnote"), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        qty = CType(e.Item.FindControl("txtqty"), TextBox).Text
        Dim qtychk As Integer
        Try
            qtychk = System.Convert.ToInt32(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1517" , "whereused.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        If Len(desc) > 500 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1518" , "whereused.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            parts.Open()
            sql = "update invsparepart set notes = '" & desc & "', quantity = '" & qty & "' where spid = '" & spid & "'"
            parts.Update(sql)
            dgitems.EditItemIndex = -1
            PageNumber = txtpg.Value
            PopTasks(PageNumber)
            parts.Dispose()
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1519" , "whereused.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1520" , "whereused.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgitems.Columns(0).HeaderText = dlabs.GetDGPage("whereused.aspx", "dgitems", "0")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(1).HeaderText = dlabs.GetDGPage("whereused.aspx", "dgitems", "1")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(2).HeaderText = dlabs.GetDGPage("whereused.aspx", "dgitems", "2")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(4).HeaderText = dlabs.GetDGPage("whereused.aspx", "dgitems", "4")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2959.Text = axlabs.GetASPXPage("whereused.aspx", "lang2959")
        Catch ex As Exception
        End Try
        Try
            lang2960.Text = axlabs.GetASPXPage("whereused.aspx", "lang2960")
        Catch ex As Exception
        End Try
        Try
            lang2961.Text = axlabs.GetASPXPage("whereused.aspx", "lang2961")
        Catch ex As Exception
        End Try
        Try
            lang2962.Text = axlabs.GetASPXPage("whereused.aspx", "lang2962")
        Catch ex As Exception
        End Try
        Try
            lang2963.Text = axlabs.GetASPXPage("whereused.aspx", "lang2963")
        Catch ex As Exception
        End Try
        Try
            lang2964.Text = axlabs.GetASPXPage("whereused.aspx", "lang2964")
        Catch ex As Exception
        End Try
        Try
            lang2965.Text = axlabs.GetASPXPage("whereused.aspx", "lang2965")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgsrch.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("whereused.aspx", "imgsrch") & "')")
            imgsrch.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid276.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("whereused.aspx", "ovid276") & "')")
            ovid276.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
