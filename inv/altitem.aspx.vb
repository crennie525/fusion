

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class altitem
    Inherits System.Web.UI.Page
	Protected WithEvents lang2617 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2616 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2615 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim inv As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim typ, invs, invid As String
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchildid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchild As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcodeid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldcode As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdalt As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            invs = Request.QueryString("invs").ToString
            lbloldcode.Value = invs
            invid = Request.QueryString("invid").ToString
            lblcodeid.Value = invid
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            inv.Open()
            GetAlt()
            inv.Dispose()
        Else
            If Request.Form("lblsubmit") = "addchild" Then
                inv.Open()
                AddAlt()
                GetAlt()
                inv.Dispose()
                lblsubmit.Value = ""
            End If
        End If
    End Sub
    Private Sub AddAlt()
        Dim invs As String = lbloldcode.Value
        Dim child As String = lblchild.Value
        Dim invsid As String = lblcodeid.Value
        Dim childid As String = lblchildid.Value
        lblchild.Value = ""
        If invsid <> childid Then
            Dim cnt As Integer
            sql = "select count(*) from invaltitem where altitemnum = '" & child & "' and altitemid = '" & childid & "' and itemnum = '" & invs & "' and itemid = '" & invsid & "'"
            cnt = inv.Scalar(sql)
            If cnt = 0 Then
                sql = "insert into invaltitem(altitemid, altitemnum, itemid, itemnum) values ('" & childid & "','" & child & "','" & invsid & " ','" & invs & "')"
                inv.Update(sql)
            End If
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1012" , "altitem.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
    Private Sub GetAlt()
        Dim item, desc, altid, avail, sloc, lot As String
        Dim invs As String = lbloldcode.Value
        Dim img As String = "../images/appbuttons/minibuttons/magnifier.gif"
        Dim img1 As String = "../images/2PX.gif"
        Dim img3 As String = "../images/appbuttons/minibuttons/magnifierdis.gif"
        Dim nimg As String = "<img src=""" & img3 & """>"
        Dim msg As String = "onmouseover = ""return overlib('" & tmod.getov("cov284" , "altitem.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim msg1 As String = "onmouseover = ""return overlib('" & tmod.getov("cov285" , "altitem.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim simg As String = "<img src=""" & img & """ onclick=""hlookup("
        Dim simg1 As String = ");""" & msg & ">"
        'Dim spimg As String = "<img src=""" & img & """ onclick=""jumpto('" & planstr & "');""" & msg1 & ">"

        Dim sb As New System.Text.StringBuilder

        'sb.Append("<td width=""20"">&nbsp;</td>")
        'sb.Append("<td width=""20"">&nbsp;</td></tr>")
        '<tr><td colspan=""4"" valign=""top"">
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 820px;  HEIGHT: 310px"">")
        'sb.Append("<table ><tr><td width=""148""><td width=""22""><td width=""456""><td width=""22""></tr>")
        sb.Append("<table width=""680"" cellpadding=""0"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl498" , "altitem.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=""../images/appbuttons/minibuttons/magnifier.gif""></td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""400"">" & tmod.getlbl("cdlbl499" , "altitem.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">" & tmod.getlbl("cdlbl500" , "altitem.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl501" , "altitem.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl502" , "altitem.aspx.vb") & "</td></tr>")


        Dim start As Integer = 0
        Dim lineid As Integer = 0

        Dim typ As String = lbltyp.Value

        If typ = "part" Then
            sql = "select a.altitemid, a.altitemnum, i.description, v.curbal, v.location as sloc, v.lotnum from invaltitem a " _
            + "left join item i on i.itemnum = a.altitemnum " _
            + "left join invbalances v on v.itemid = a.altitemid " _
            + "where a.itemnum = '" & invs & "'"
        ElseIf typ = "tool" Then
            sql = "select a.altitemid, a.altitemnum, i.description, v.curbal, v.location as sloc, v.lotnum from invaltitem a " _
            + "left join tool i on i.toolnum = a.altitemnum " _
            + "left join invbalances v on v.itemid = a.altitemid " _
            + "where a.itemnum = '" & invs & "'"
        ElseIf typ = "lube" Then
            sql = "select a.altitemid, a.altitemnum, i.description, v.curbal, v.location as sloc, v.lotnum from invaltitem a " _
            + "left join lubricants i on i.lubenum = a.altitemnum " _
            + "left join invbalances v on v.itemid = a.altitemid " _
            + "where a.itemnum = '" & invs & "'"
        End If

        dr = inv.GetRdrData(sql)
        While dr.Read
            altid = dr.Item("altitemid").ToString
            item = dr.Item("altitemnum").ToString
            desc = dr.Item("description").ToString
            avail = dr.Item("curbal").ToString
            sloc = dr.Item("sloc").ToString
            lot = dr.Item("lotnum").ToString
            If desc = "" Then
                desc = "&nbsp;"
            End If

            sb.Append("<tr><td class=""plainlabel grayborder""><a href=""#"" class=""A1"" onclick=""handlemat('" & altid & "','" & item & "')"">" & item & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & nimg & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & desc & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & avail & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & sloc & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & lot & "</td></tr>")
            'sb.Append("<td><img src=""../images/appbuttons/minibuttons/del.gif"" onclick=""delhaz('" & altid & "');""></td></tr>")

        End While

        dr.Close()

        Dim i As Integer
        If start = 0 Then
            If invs <> "" Then
                lineid += 1
                sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdbloc1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & simg & "'" & item & "'" & simg1 & "</td>")
                'sb.Append("<td class=""plainlabel grayborder"" id=""tdbdesc1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype1"">&nbsp;</td></tr>")
            End If
            For i = 0 To 1
                sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & nimg & "</td>")
                'sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        Else
            For i = 0 To 5
                lineid += 1
                sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdbloc" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & simg & lineid & simg1 & "</td>")
                'sb.Append("<td class=""plainlabel grayborder"" id=""tdbdesc" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype" & lineid & """>&nbsp;</td></tr>")
            Next
        End If

        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdalt.InnerHtml = sb.ToString
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2615.Text = axlabs.GetASPXPage("altitem.aspx", "lang2615")
        Catch ex As Exception
        End Try
        Try
            lang2616.Text = axlabs.GetASPXPage("altitem.aspx", "lang2616")
        Catch ex As Exception
        End Try
        Try
            lang2617.Text = axlabs.GetASPXPage("altitem.aspx", "lang2617")
        Catch ex As Exception
        End Try

    End Sub

End Class
