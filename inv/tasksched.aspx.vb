

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tasksched
    Inherits System.Web.UI.Page
	Protected WithEvents lang2958 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2957 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2956 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim parts As New Utilities
    Dim ptid, tnum, wonum, typ, jpid, pmid, ptyp, ro As String
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblalttyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblaltid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblaltnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrepid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrepnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblptyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents mdiv As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            ptyp = Request.QueryString("ptyp").ToString
            lblptyp.Value = ptyp
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            ptid = Request.QueryString("ptid").ToString
            lblptid.Value = ptid
            wonum = Request.QueryString("wonum").ToString
            lblwo.Value = wonum
            jpid = Request.QueryString("jpid").ToString
            lbljpid.Value = jpid
            pmid = Request.QueryString("pmid").ToString
            lblpmid.Value = pmid
            parts.Open()
            GetTasks()
            parts.Dispose()
        Else
            If Request.Form("lblsubmit") = "altitem" Then
                lblsubmit.Value = ""
                parts.Open()
                AltItem()
                GetTasks()
                parts.Dispose()
            End If
        End If
    End Sub
    Private Sub AltItem()
        ptid = lblptid.Value
        typ = lbltyp.Value
        ptyp = lblptyp.Value
        wonum = lblwo.Value
        pmid = lblpmid.Value
        jpid = lbljpid.Value

        Dim rid, rnum, aid, anum, atyp, pid, qty As String
        rid = lblrepid.Value
        rnum = lblrepnum.Value
        aid = lblaltid.Value
        anum = lblaltnum.Value
        atyp = lblalttyp.Value
        pid = lblpid.Value
        qty = lblqty.Value
        If ptyp = "part" Then
            If typ = "wo" Then
                sql = "delete from woparts where wopartid = '" & pid & "' and wonum = '" & wonum & "'; " _
               + "update workorder set estmatcost = (select sum(total) from woparts where wonum = '" & wonum & "') " _
               + "where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwopart '" & lblwo.Value & "'"
                parts.Update(sql)
                sql = "usp_insertwoparts '" & wonum & "', '" & aid & "', '" & anum & "', '" & qty & "', '0'"
                parts.Update(sql)
            ElseIf typ = "jp" Then
                sql = "delete from wojpparts where wotskpartid = '" & pid & "' and wonum = '" & wonum & "'; " _
                 + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wonum & "' and " _
                 + "jpid = '" & jpid & "') where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwjppart '" & ptid & "','" & wonum & "','" & jpid & "'"
                parts.Update(sql)
                sql = "usp_insertwjpparts '" & jpid & "', '" & ptid & "', '" & aid & "', " _
                            + "'" & anum & "', '" & qty & "', '0','" & wonum & "'"
                parts.Update(sql)
            ElseIf typ = "pm" Then
                sql = "delete from wopmparts where wotskpartid = '" & pid & "' and wonum = '" & wonum & "'; " _
                 + "update workorder set estmatcost = (select sum(total) from wopmparts where wonum = '" & wonum & "' and " _
                 + "pmid = '" & jpid & "') where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwpmpart '" & ptid & "','" & wonum & "','" & pmid & "'"
                parts.Update(sql)
                sql = "usp_insertwpmparts '" & pmid & "', '" & ptid & "', '" & aid & "', " _
                           + "'" & anum & "', '" & qty & "', '0','" & wonum & "'"
                parts.Update(sql)
            End If
        ElseIf ptyp = "tool" Then
            If typ = "wo" Then
                sql = "delete from wotools where wotoolid = '" & pid & "' and wonum = '" & wonum & "'; " _
               + "update workorder set estmatcost = (select sum(total) from wotools where wonum = '" & wonum & "') " _
               + "where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwotool '" & lblwo.Value & "'"
                parts.Update(sql)
                sql = "usp_insertwotools '" & wonum & "', '" & aid & "', '" & anum & "', '" & qty & "', '0'"
                parts.Update(sql)
            ElseIf typ = "jp" Then
                sql = "delete from wojptools where wotsktoolid = '" & pid & "' and wonum = '" & wonum & "'; " _
                 + "update workorder set estjpmatcost = (select sum(total) from wojptools where wonum = '" & wonum & "' and " _
                 + "jpid = '" & jpid & "') where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwjptool '" & ptid & "','" & wonum & "','" & jpid & "'"
                parts.Update(sql)
                sql = "usp_insertwjptools '" & jpid & "', '" & ptid & "', '" & aid & "', " _
                            + "'" & anum & "', '" & qty & "', '0','" & wonum & "'"
                parts.Update(sql)
            ElseIf typ = "pm" Then
                sql = "delete from wopmtools where wotsktoolid = '" & pid & "' and wonum = '" & wonum & "'; " _
                 + "update workorder set estmatcost = (select sum(total) from wopmtools where wonum = '" & wonum & "' and " _
                 + "pmid = '" & jpid & "') where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwpmtool '" & ptid & "','" & wonum & "','" & pmid & "'"
                parts.Update(sql)
                sql = "usp_insertwpmtools '" & jpid & "', '" & ptid & "', '" & aid & "', " _
                           + "'" & anum & "', '" & qty & "', '0','" & wonum & "'"
                parts.Update(sql)
            End If
        ElseIf ptyp = "lube" Then
            If typ = "wo" Then
                sql = "delete from wolubes where wolubeid = '" & pid & "' and wonum = '" & wonum & "'; " _
               + "update workorder set estmatcost = (select sum(total) from wolubes where wonum = '" & wonum & "') " _
               + "where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwolube '" & lblwo.Value & "'"
                parts.Update(sql)
                sql = "usp_insertwolubes '" & wonum & "', '" & aid & "', '" & anum & "', '" & qty & "', '0'"
                parts.Update(sql)
            ElseIf typ = "jp" Then
                sql = "delete from wojplubes where wotsklubeid = '" & pid & "' and wonum = '" & wonum & "'; " _
                 + "update workorder set estjpmatcost = (select sum(total) from wojplubes where wonum = '" & wonum & "' and " _
                 + "jpid = '" & jpid & "') where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwjplube '" & ptid & "','" & wonum & "','" & jpid & "'"
                parts.Update(sql)
                sql = "usp_insertwjplubes '" & jpid & "', '" & ptid & "', '" & aid & "', " _
                            + "'" & anum & "', '" & qty & "', '0','" & wonum & "'"
                parts.Update(sql)

            ElseIf typ = "pm" Then
                sql = "delete from wopmlubes where wotsklubeid = '" & pid & "' and wonum = '" & wonum & "'; " _
                 + "update workorder set estmatcost = (select sum(total) from wopmlubes where wonum = '" & wonum & "' and " _
                 + "pmid = '" & jpid & "') where wonum = '" & wonum & "'"
                parts.Update(sql)
                sql = "usp_delwpmlube '" & ptid & "','" & wonum & "','" & pmid & "'"
                parts.Update(sql)
                sql = "usp_insertwpmlubes '" & jpid & "', '" & ptid & "', '" & aid & "', " _
                           + "'" & anum & "', '" & qty & "', '0','" & wonum & "'"
                parts.Update(sql)
            End If
        End If

        lblrepid.Value = ""
        lblrepnum.Value = ""
        lblaltid.Value = ""
        lblaltnum.Value = ""
        lblalttyp.Value = ""
        lblpid.Value = ""
        lblqty.Value = ""

    End Sub
    Private Sub GetTasks()
        ptid = lblptid.Value
        typ = lbltyp.Value
        ptyp = lblptyp.Value
        wonum = lblwo.Value
        If ptyp = "part" Then
            If typ = "wo" Then
                sql = "select i.*, i.itemid as rid, i.wopartid as pid, v.curbal, v.location as sloc, v.lotnum from woparts i left join invbalances v on v.itemid = i.itemid where i.wonum = '" & lblwo.Value & "'"
            ElseIf typ = "jp" Then
                sql = "select i.*, i.itemid as rid, i.wotskpartid as pid, v.curbal, v.location as sloc, v.lotnum from wojpparts i left join invbalances v on v.itemid = i.itemid where i.wojtid = '" & ptid & "' and i.wonum = '" & lblwo.Value & "'"
            ElseIf typ = "pm" Then
                sql = "select i.*, i.itemid as rid, i.wotskpartid as pid, v.curbal, v.location as sloc, v.lotnum from wopmparts i left join invbalances v on v.itemid = i.itemid where i.pmtskid = '" & ptid & "' and i.wonum = '" & lblwo.Value & "'"
            End If
        ElseIf ptyp = "tool" Then
            If typ = "wo" Then
                sql = "select i.*, i.itemid as rid, i.wotoolid as pid, v.curbal, v.location as sloc, v.lotnum from wotools i left join invbalances v on v.itemid = i.itemid where i.wonum = '" & lblwo.Value & "'"
            ElseIf typ = "jp" Then
                sql = "select i.*, i.itemid as rid, i.wotsktoolid as pid, v.curbal, v.location as sloc, v.lotnum from wojptools i left join invbalances v on v.itemid = i.itemid where i.wojtid = '" & ptid & "' and i.wonum = '" & lblwo.Value & "'"
            ElseIf typ = "pm" Then
                sql = "select i.*, i.itemid as rid, i.wotsktoolid as pid, v.curbal, v.location as sloc, v.lotnum from wopmtools i left join invbalances v on v.itemid = i.itemid where i.pmtskid = '" & ptid & "' and i.wonum = '" & lblwo.Value & "'"
            End If
        ElseIf ptyp = "lube" Then
            If typ = "wo" Then
                sql = "select i.*, i.itemid as rid, i.wolubeid as pid, v.curbal, v.location as sloc, v.lotnum from wolubes i left join invbalances v on v.itemid = i.itemid where i.wonum = '" & lblwo.Value & "'"
            ElseIf typ = "jp" Then
                sql = "select i.*, i.itemid as rid, i.wotsklubeid as pid, v.curbal, v.location as sloc, v.lotnum from wojplubes i left join invbalances v on v.itemid = i.itemid where i.wojtid = '" & ptid & "' and i.wonum = '" & lblwo.Value & "'"
            ElseIf typ = "pm" Then
                sql = "select i.*, i.itemid as rid, i.wotsklubeid as pid, v.curbal, v.location as sloc, v.lotnum from wopmlubes i left join invbalances v on v.itemid = i.itemid where i.pmtskid = '" & ptid & "' and i.wonum = '" & lblwo.Value & "'"
            End If
        End If

        dr = parts.GetRdrData(sql)
        dgparttasks.DataSource = dr
        dgparttasks.DataBind()
        dr.Close()

    End Sub

    Private Sub dgparttasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparttasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            typ = lblptyp.Value
            Dim img As HtmlImage = CType(e.Item.FindControl("imggetalt"), HtmlImage)
            Dim item As String = DataBinder.Eval(e.Item.DataItem, "itemnum").ToString
            Dim itemid As String = DataBinder.Eval(e.Item.DataItem, "rid").ToString
            Dim pid As String = DataBinder.Eval(e.Item.DataItem, "pid").ToString
            Dim qty As String = DataBinder.Eval(e.Item.DataItem, "qty").ToString
            img.Attributes.Add("onclick", "getalt('" & itemid & "','" & item & "','" & typ & "','" & pid & "','" & qty & "');")


        End If
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgparttasks.Columns(0).HeaderText = dlabs.GetDGPage("tasksched.aspx","dgparttasks","0")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2956.Text = axlabs.GetASPXPage("tasksched.aspx","lang2956")
		Catch ex As Exception
		End Try
		Try
			lang2957.Text = axlabs.GetASPXPage("tasksched.aspx","lang2957")
		Catch ex As Exception
		End Try
		Try
			lang2958.Text = axlabs.GetASPXPage("tasksched.aspx","lang2958")
		Catch ex As Exception
		End Try

	End Sub

End Class
