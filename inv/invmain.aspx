<%@ Page Language="vb" AutoEventWireup="false" Codebehind="invmain.aspx.vb" Inherits="lucy_r12.invmain" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title runat="server" id="pgtitle">invmain</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/invtab.js"></script>
		<script language="JavaScript" src="../scripts1/invmainaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checktab();" >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 7px; POSITION: absolute; TOP: 81px; z-index: 1;" cellSpacing="0" cellPadding="0"
				width="722">
				<tr>
					<td colSpan="2">
						<table>
							<tr height="20">
								<td class="thdrhov plainlabel" id="tdinv" onclick="gettab('inv');" width="110"><asp:Label id="lang2735" runat="server">Catalog</asp:Label></td>
								<td class="thdr plainlabel" id="tdstor" onclick="gettab('stor');" width="110"><asp:Label id="lang2736" runat="server">Storeroom</asp:Label></td>
								<td class="thdr plainlabel" id="tdre" onclick="gettab('re');" width="110"><asp:Label id="lang2737" runat="server">Reorder Details</asp:Label></td>
								<td class="thdr plainlabel" id="tdsp" onclick="gettab('sp');" width="110"><asp:Label id="lang2738" runat="server">Spare Parts</asp:Label></td>
								<td class="thdr plainlabel" id="tdit" onclick="gettab('it');" width="110"><asp:Label id="lang2739" runat="server">Issue or Transfer</asp:Label></td>
								<td class="thdr plainlabel" id="tdrec" onclick="gettab('rec');" width="110"><asp:Label id="lang2740" runat="server">Receive</asp:Label></td>
								<td class="thdr plainlabel" id="tdpr" onclick="gettab('pr');" width="110"><asp:Label id="lang2741" runat="server">Purchase Reqs</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<div id="invdiv" style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; Z-INDEX: 1; LEFT: 6px; VISIBILITY: hidden; BORDER-LEFT: 2px groove; WIDTH: 860px; BORDER-BOTTOM: 2px groove; HEIGHT: 520px; BACKGROUND-COLOR: transparent"><iframe id="ifinv" style="WIDTH: 860px; HEIGHT: 520px; BACKGROUND-COLOR: transparent" src=""
								frameBorder="no" allowTransparency runat="server"> </iframe>
						</div>
					</td>
				</tr>
			</table>
			<input id="txttab" type="hidden" name="appchk" runat="server"> <input id="appchk" type="hidden" name="appchk" runat="server">
			<input id="lblcode" type="hidden" runat="server"> <input id="lbltype" type="hidden" runat="server">
			<input id="lblro" type="hidden" runat="server">
			
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
