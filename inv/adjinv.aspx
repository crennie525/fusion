<%@ Page Language="vb" AutoEventWireup="false" Codebehind="adjinv.aspx.vb" Inherits="lucy_r12.adjinv" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>adjinv</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/adjinvaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkret();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td width="120"></td>
					<td width="180"></td>
				</tr>
				<TR>
					<td colSpan="2">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="264"><asp:Label id="lang2606" runat="server">Adjust Balance Dialog</asp:Label></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr height="22">
					<td class="label"><asp:Label id="lang2607" runat="server">Item Code</asp:Label></td>
					<td class="plainlabel" id="tditem" runat="server"></td>
				</tr>
				<tr height="22">
					<td class="label"><asp:Label id="lang2608" runat="server">Store Room</asp:Label></td>
					<td class="plainlabel" id="tdstore" runat="server"></td>
				</tr>
				<tr height="22">
					<td class="label"><asp:Label id="lang2609" runat="server">Current Balance</asp:Label></td>
					<td class="plainlabel" id="tdbal" runat="server"></td>
				</tr>
				<tr height="22">
					<td class="label"><asp:Label id="lang2610" runat="server">New Balance</asp:Label></td>
					<td class="plainlabel"><asp:textbox id="txtnewbal" runat="server" Width="90px" CssClass="plainlabelblue"></asp:textbox></td>
				</tr>
				<tr height="22">
					<td class="label"><asp:Label id="lang2611" runat="server">Reconciled?</asp:Label></td>
					<td class="plainlabel"><asp:dropdownlist id="ddrec" runat="server">
							<asp:ListItem Value="Y" Selected="True">Y</asp:ListItem>
							<asp:ListItem Value="N">N</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td colspan="2" align="center" class="plainlabelblue"><input type="radio" id="rbphy" name="rbg" onclick="changeadjust('phys')" checked><asp:Label id="lang2612" runat="server">Physical Count</asp:Label><input type="radio" id="rbadj" name="rbg" onclick="changeadjust('adj');"><asp:Label id="lang2613" runat="server">Adjust Only</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2614" runat="server">Comments</asp:Label></td>
					<td class="plainlabel"></td>
				</tr>
				<tr>
					<td colSpan="2"><asp:textbox id="txtcomments" runat="server" Width="300px" CssClass="plainlabel" Height="100px"
							TextMode="MultiLine"></asp:textbox></td>
				</tr>
				<tr>
					<td></td>
					<td align="right"><IMG id="Img1" onclick="saveloc();" src="../images/appbuttons/minibuttons/savedisk1.gif"
							runat="server"></td>
				</tr>
			</table>
			<input type="hidden" id="lblitemid" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lblitemnum" runat="server"> <input type="hidden" id="lblstore" runat="server">
			<input type="hidden" id="lblcurbal" runat="server"><input type="hidden" id="lbladjtype" runat="server">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
