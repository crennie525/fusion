<%@ Page Language="vb" AutoEventWireup="false" Codebehind="reclookup.aspx.vb" Inherits="lucy_r12.reclookup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>reclookup</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/reclookupaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" width="600">
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2857" runat="server">Purchase Requisition Lookup</asp:Label></td>
				</TR>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td class="label" width="60"><asp:Label id="lang2858" runat="server">Item#</asp:Label></td>
								<td width="145"><asp:textbox id="txtsrch" runat="server" Width="145px" ReadOnly="True"></asp:textbox></td>
								<td width="60" class="label"><asp:Label id="lang2859" runat="server">Vendor</asp:Label></td>
								<td width="145"><asp:textbox id="txtpvend" runat="server" CssClass="plainlabel" Width="145px" ReadOnly="True"></asp:textbox></td>
								<td width="20"><IMG id="Img4" onclick="getcomp('V', 'txtpvend');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2"><div id="tdpr" runat="server" style="OVERFLOW: auto; WIDTH: 600px; HEIGHT: 250px">
						</div>
					</td>
				</tr>
				<tr>
					<td align="center" colSpan="2">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit"> <input type="hidden" id="lblret" runat="server" NAME="lblret">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
