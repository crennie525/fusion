

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class purchreq
    Inherits System.Web.UI.Page
	Protected WithEvents lang2831 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2830 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2829 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2828 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim inv As New Utilities
    Dim sql As String
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = "invpr"
    Dim PK As String = "prnum"


    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divsrch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpvend As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divpvend As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim Sort As String
    Dim ro As String
    ', itemnum1 = (select itemnum from inventory where itemid = invpr.itemid)"
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdpr As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            inv.Open()
            txtpg.Value = "1"
            GetPR()
            inv.Dispose()
        Else
            If Request.Form("lblsubmit") = "get" Then
                lblsubmit.Value = ""
                inv.Open()
                GetPR()
                inv.Dispose()
            End If

            If Request.Form("lblret") = "next" Then
                inv.Open()
                GetNext()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                inv.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetPR()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                inv.Open()
                GetPrev()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                inv.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetPR()
                inv.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetPR()
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1424" , "purchreq.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetPR()
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1425" , "purchreq.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPR()
        'Dim itemid As String = lblitemid.Value
        sql = "select prnum, issuedate, requireddate, requestedby, vendor, status, statusdate, totalcost from invpr " '_
        '+ "where itemid = '" & itemid & "' order by issuedate desc"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table width=""800"" cellpadding=""0"">")

        sb.Append("<tr><td colspan=""11"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 780px;  HEIGHT: 380px"">")
        sb.Append("<table width=""1040"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""40"">" & tmod.getlbl("cdlbl513" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">" & tmod.getlbl("cdlbl514" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl515" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""100"">" & tmod.getlbl("cdlbl516" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl517" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl518" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl519" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl520" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl521" , "purchreq.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""100"">" & tmod.getlbl("cdlbl522" , "purchreq.aspx.vb") & "</td></tr>")
        Dim srch As String = lblsrch.Value
        If srch <> "" Then
            Filter = "itemnum = ''" & srch & "''"
            FilterCnt = "itemnum = '" & srch & "'"
        End If
        Dim pvend = lblpvend.Value
        If pvend <> "" Then
            If Filter <> "" Then
                Filter = " and (vendor = ''" & pvend & "'' or povendor = ''" & pvend & "'')"
                FilterCnt = " and (vendor = '" & pvend & "' or povendor = '" & pvend & "')"
            Else
                Filter = "(vendor = ''" & pvend & "'' or povendor = ''" & pvend & "'')"
                FilterCnt = "(vendor = '" & pvend & "' or povendor = '" & pvend & "')"
            End If
        End If
        Dim stat As String = ddstat.SelectedValue
        If stat <> "ALL" Then
            If Filter <> "" Then
                Filter = " and status = ''" & stat & "''"
                FilterCnt = " and status = '" & stat & "'"
            Else
                Filter = "status = ''" & stat & "''"
                FilterCnt = "status = '" & stat & "'"
            End If
        End If
        If FilterCnt <> "" Then
            sql = "select Count(*) from invpr where " & FilterCnt
        Else
            sql = "select Count(*) from invpr"
        End If

        Dim dc As Integer = inv.Scalar(sql)
        Dim intPgCnt, intPgNav As Integer
        PageNumber = txtpg.Value
        intPgCnt = inv.Scalar(sql)
        intPgNav = inv.PageCount(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        Dim ven, inum As String
        'dr = inv.GetRdrData(sql)
        While dr.Read
            ven = dr.Item("vendor").ToString
            If Len(ven) = 0 Then
                ven = "&nbsp;"
            End If
            inum = dr.Item("itemnum").ToString
            sb.Append("<tr><td class=""plainlabel grayborder"" width=""40""><img src=""../images/appbuttons/minibuttons/printex.gif"" onclick=""printpr('" & dr.Item("prnum").ToString & "');""></td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""80""><a href=""#"" class=""A1"" onclick=""getpr('" & dr.Item("prnum").ToString & "');"">" & dr.Item("prnum").ToString & "</a></td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("itemnum").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""100"">" & dr.Item("status").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("statusdate").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("requireddate").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("requestedby").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("issuedate").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & ven & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""100"">" & dr.Item("totalcost").ToString & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdpr.InnerHtml = sb.ToString
    End Sub

    Private Sub ddstat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddstat.SelectedIndexChanged
        inv.Open()
        PageNumber = "1"
        txtpg.Value = PageNumber
        GetPR()
        inv.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2828.Text = axlabs.GetASPXPage("purchreq.aspx", "lang2828")
        Catch ex As Exception
        End Try
        Try
            lang2829.Text = axlabs.GetASPXPage("purchreq.aspx", "lang2829")
        Catch ex As Exception
        End Try
        Try
            lang2830.Text = axlabs.GetASPXPage("purchreq.aspx", "lang2830")
        Catch ex As Exception
        End Try
        Try
            lang2831.Text = axlabs.GetASPXPage("purchreq.aspx", "lang2831")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img4.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("purchreq.aspx", "Img4") & "')")
            Img4.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsrch.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("purchreq.aspx", "imgsrch") & "')")
            imgsrch.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
