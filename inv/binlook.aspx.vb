

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class binlook
    Inherits System.Web.UI.Page
	Protected WithEvents lang2666 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim bin As New Utilities
    Dim itemid, store As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdbin As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblstore As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            itemid = Request.QueryString("itemid").ToString
            lblitemid.Value = itemid
            store = Request.QueryString("store").ToString
            lblstore.Value = store
            bin.Open()
            GetBin()
            bin.Dispose()

        End If
    End Sub
    Private Sub GetBin()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table width=""800"" cellpadding=""0"">")

        sb.Append("<tr><td colspan=""11"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 220px;  HEIGHT: 200px"">")
        sb.Append("<table width=""200"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""200"">" & tmod.getlbl("cdlbl503" , "binlook.aspx.vb") & "</td></tr>")

        itemid = lblitemid.Value
        store = lblstore.Value

        sql = "select i.bin from invbalances i where i.location = '" & store & "' and i.itemid = '" & itemid & "' and bin is not null"

        Dim binnum As String
        Dim cnt As Integer = 0
        dr = bin.GetRdrData(sql)

        While dr.Read
            cnt += 1
            binnum = dr.Item("bin").ToString

            sb.Append("<tr><td class=""plainlabel grayborder"" ><a href=""#"" class=""A1"" onclick=""getbin('" & binnum & "');"">" & binnum & "</a></td></tr>")


        End While
        dr.Close()
        If cnt = 0 Then
            sb.Append("<tr><td class=""plainlabelred"">" & tmod.getlbl("cdlbl504" , "binlook.aspx.vb") & "</td></tr>")
        End If
        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdbin.InnerHtml = sb.ToString

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2666.Text = axlabs.GetASPXPage("binlook.aspx","lang2666")
		Catch ex As Exception
		End Try

	End Sub

End Class
