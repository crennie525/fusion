<%@ Page Language="vb" AutoEventWireup="false" Codebehind="invlookup.aspx.vb" Inherits="lucy_r12.invlookup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>invlookup</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<LINK href="../css/maxcss.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../scripts/common.js"></script>
		<script language="JavaScript" src="../scripts1/invlookupaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td>
						<div style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid"
							id="tdmat" runat="server"></div>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbllist" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
