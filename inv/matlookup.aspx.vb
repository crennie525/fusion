

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class matlookup
    Inherits System.Web.UI.Page
	Protected WithEvents lang2773 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2772 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstore As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim mat As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Protected WithEvents lbllist As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblps As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divstore As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txttab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents td3 As System.Web.UI.HtmlControls.HtmlTableCell
    Dim list, filt As String
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            list = Request.QueryString("list").ToString '"lubricants" '
            lbllist.Value = list
            filt = Request.QueryString("filt").ToString '"lubricants" '
            lblfilt.Value = filt
            mat.Open()
            GetMat(PageNumber)
            mat.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                mat.Open()
                GetNext()
                mat.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                mat.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetMat(PageNumber)
                mat.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                mat.Open()
                GetPrev()
                mat.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                mat.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetMat(PageNumber)
                mat.Dispose()
                lblret.Value = ""
            End If
            divstore.InnerHtml = lblstore.Value
        End If
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        If txtsrch.Text <> "" Or lblstore.Value <> "" Then
            mat.Open()
            PageNumber = 1
            txtpg.Value = PageNumber
            GetMat(PageNumber)
            mat.Dispose()
            divstore.InnerHtml = lblstore.Value
        End If
       
    End Sub
    Private Sub GetMat(ByVal PageNumber As Integer)
        list = lbllist.Value
        Dim f1, f2, filtstr, filtstrcnt As String
        If list = "part" Then
            list = "item"
            Tables = "item"
            PK = "itemid"
            Fields = "valid = itemid, value = itemnum, description, storeroom"
            f1 = "itemnum"
            Sort = "itemnum"
            dgdepts.Columns(0).HeaderText = "Part#"
            dgdepts.Columns(3).Visible = True
            td1.Visible = True
            td2.Visible = True
            td3.Visible = True
        ElseIf list = "tool" Then
            Tables = list
            PK = "toolid"
            Fields = "valid = toolid, value = toolnum, description, storeroom"
            f1 = "toolnum"
            Sort = "toolnum"
            dgdepts.Columns(0).HeaderText = "Tool#"
            dgdepts.Columns(3).Visible = True
            td1.Visible = True
            td2.Visible = True
            td3.Visible = True
        ElseIf list = "lube" Then
            list = "lubricants"
            Tables = "lubricants"
            PK = "lubeid"
            Fields = "valid = lubeid, value = lubenum, description, location as storeroom"
            f1 = "lubenum"
            Sort = "lubenum"
            dgdepts.Columns(0).HeaderText = "Lube#"
            dgdepts.Columns(3).Visible = True
            td1.Visible = True
            td2.Visible = True
            td3.Visible = True
        ElseIf list = "storeroom" Or list = "stocktype" Or list = "lottype" Or list = "stockcat" Or list = "hazard" Then

            Filter = "listname = ''" & list & "''"
            FilterCnt = "listname = '" & list & "'"
            filt = lblfilt.Value
            If filt <> "" And filt <> "no" Then
                Filter += " and value in (select location from invbalances where itemid = ''" + filt + "'')"
                FilterCnt += " and value in (select location from invbalances where itemid = '" + filt + "')"
            End If
            'old code 1 here 
           
            If list = "storeroom" Then
                dgdepts.Columns(0).HeaderText = "Store Room"
            ElseIf list = "stocktype" Then
                dgdepts.Columns(0).HeaderText = "Stock Type"
            ElseIf list = "lottype" Then
                dgdepts.Columns(0).HeaderText = "Lot Type"
            ElseIf list = "stockcat" Then
                dgdepts.Columns(0).HeaderText = "Stock Category"
            ElseIf list = "hazard" Then
                dgdepts.Columns(0).HeaderText = "Hazard"
                dgdepts.Columns(3).Visible = False
            End If
            dgdepts.Columns(3).Visible = False
            td1.Visible = False
            td2.Visible = False
            td3.Visible = False

            list = "invvallist"
            Tables = "invvallist"
            PK = "valid"
            Fields = "valid, value, description, ''0'' as storeroom"
            f1 = "value"
        ElseIf list = "hazard1" Then 'Or list = "msds"
            'list = "invvallist"
            Tables = "invvallist"
            PK = "valid"
            'Tables = list
            'PK = "hazid"
            Fields = "valid = hazard, value = hazard, description, ''0'' as storeroom"
            f1 = "hazard"
            dgdepts.Columns(0).HeaderText = "Hazard"
            dgdepts.Columns(3).Visible = False
            td1.Visible = False
            td2.Visible = False
            td3.Visible = False
        ElseIf list = "orderunit" Then
            Tables = "invorderunit"
            list = "invorderunit"
            PK = "orderunit"
            Fields = "valid = orderunit, value = orderunit, description = conversion, ''0'' as storeroom"
            f1 = "orderunit"
            dgdepts.Columns(0).HeaderText = "Order Unit"
            dgdepts.Columns(2).HeaderText = "Conversion"
            dgdepts.Columns(3).Visible = False
            td1.Visible = False
            td2.Visible = False
            td3.Visible = False
        End If


        'Or list = "orderunit"
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        If Len(srch) > 0 Then
            If list = "invorderunit" Then
                If Filter = "" Then
                    Filter = f1 & " like ''%" & srch & "%'' or conversion like ''%" & srch & "%''"
                    FilterCnt = f1 & " like '%" & srch & "%' or conversion like '%" & srch & "%'"
                Else
                    Filter += " and (" & f1 & " like ''%" & srch & "%'' or conversion like ''%" & srch & "%'')"
                    FilterCnt += " and (" & f1 & " like '%" & srch & "%' or conversion like '%" & srch & "%')"
                End If
            Else
                If Filter = "" Then
                    Filter = f1 & " like ''%" & srch & "%'' or description like ''%" & srch & "%''"
                    FilterCnt = f1 & " like '%" & srch & "%' or description like '%" & srch & "%'"
                Else
                    Filter += " and (" & f1 & " like ''%" & srch & "%'' or description like ''%" & srch & "%'')"
                    FilterCnt += " and (" & f1 & " like '%" & srch & "%' or description like '%" & srch & "%')"
                End If
            End If
            
        End If
        Dim store As String = lblstore.Value

        If store <> "" Then
            If Len(srch) > 0 Then
                Filter += " and storeroom = ''" & store & "''"
                FilterCnt += " and storeroom = '" & store & "'"
            Else
                Filter += "storeroom = ''" & store & "''"
                FilterCnt += "storeroom = '" & store & "'"
            End If

        End If

        If Filter = "" Then
            Filter = ""
            FilterCnt = ""
        End If

        If FilterCnt <> "" Then
            sql = "select Count(*) from " & list & " where " & FilterCnt
        Else
            sql = "select Count(*) from " & list
        End If

        Dim dc As Integer = mat.Scalar(sql)

        dgdepts.VirtualItemCount = dc
        If dc = 0 Then
            lblps.Visible = True
            lblps.Text = "No Records Found"
            dgdepts.Visible = True

            PageSize = "100"
            dr = mat.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgdepts.DataSource = dr
            Try
                dgdepts.DataBind()
            Catch ex As Exception
                dgdepts.CurrentPageIndex = 0
                dgdepts.DataBind()
            End Try
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            If dgdepts.PageCount = 0 Then
                lblpg.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            End If
        Else
            dgdepts.Visible = True
            lblps.Visible = False

            PageSize = "100"
            dr = mat.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgdepts.DataSource = dr
            'Try
            dgdepts.DataBind()
            'Catch ex As Exception
            'dgdepts.CurrentPageIndex = 0
            'dgdepts.DataBind()
            'End Try
            dr.Close()
            lblps.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            If dgdepts.PageCount = 0 Then
                lblpg.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetMat(PageNumber)
        Catch ex As Exception
            mat.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1243" , "matlookup.aspx.vb")
 
            mat.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetMat(PageNumber)
        Catch ex As Exception
            mat.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1244" , "matlookup.aspx.vb")
 
            mat.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub


    Private Sub dgdepts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdepts.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lb As HtmlAnchor
            lb = CType(e.Item.FindControl("lbv"), HtmlAnchor)
            Dim mid, mi, md As String
            mid = CType(e.Item.FindControl("Label1"), Label).Text
            mi = CType(e.Item.FindControl("lblv"), Label).Text
            md = CType(e.Item.FindControl("lbld"), Label).Text
            lb.Attributes.Add("onclick", "getmat('" & mid & "','" & mi & "','" & md & "')")
        End If
    End Sub


	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("matlookup.aspx", "dgdepts", "0")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(2).HeaderText = dlabs.GetDGPage("matlookup.aspx", "dgdepts", "2")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(3).HeaderText = dlabs.GetDGPage("matlookup.aspx", "dgdepts", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2772.Text = axlabs.GetASPXPage("matlookup.aspx", "lang2772")
        Catch ex As Exception
        End Try
        Try
            lang2773.Text = axlabs.GetASPXPage("matlookup.aspx", "lang2773")
        Catch ex As Exception
        End Try

    End Sub

End Class
