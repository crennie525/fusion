

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class sparepartlist
    Inherits System.Web.UI.Page
	Protected WithEvents lang2919 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2918 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2917 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2916 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2915 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2914 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim iPageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim cid, cnm, typ As String
    Dim sort As String = "itemnum"
    Dim sql As String
    Dim dr As SqlDataReader
    Dim parts As New Utilities
    Dim eqid, eqnum As String
    Dim decPgNav As Decimal
    Protected WithEvents txtopgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtopg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim intPgNav As Integer
    Dim ap As New AppUtils
    Dim ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblnpg As System.Web.UI.WebControls.Label
    Protected WithEvents btnradd As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtpart As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsrch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgitems As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblipg As System.Web.UI.WebControls.Label
    Protected WithEvents ibtnaddtolist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Imagebutton4 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbchecked As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdtask As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents mdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents div1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img7 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img8 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblparts As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilterwd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        RetainItems()
        If Request.Form("lbladdflag") = "ok" Then
            lbladdflag.Value = ""
            parts.Open()
            iPageNumber = txtipg.Value
            PopParts(iPageNumber)
            parts.Dispose()
        End If
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgparttasks.Columns(0).Visible = False
                dgparttasks.Columns(7).Visible = False
                dgitems.Columns(1).Visible = False
                ibtnaddtolist.Enabled = False
                ibtnaddtolist.ImageUrl = "../images/appbuttons/minibuttons/savedisk1dis.gif"
            End If
            Dim icost As String = ap.InvEntry
            If icost = "ext" Or icost = "inv" Then
                dgitems.ShowFooter = False
            End If
            parts.Open()
            eqid = Request.QueryString("eqid")
            lblptid.Value = eqid
            cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            eqnum = Request.QueryString("eqn")
            tdtask.InnerHtml = eqnum
            PopTasks(PageNumber)
            lbloflag.Value = "3"
            div1.Visible = False
            parts.Dispose()
        Else
            If Request.Form("lblret") = "nnext" Then
                parts.Open()
                GetnNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nlast" Then
                parts.Open()
                PageNumber = txtnpgcnt.Value
                txtnpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nprev" Then
                parts.Open()
                GetnPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nfirst" Then
                parts.Open()
                PageNumber = 1
                txtnpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "inext" Then
                parts.Open()
                GetiNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ilast" Then
                parts.Open()
                PageNumber = txtipgcnt.Value
                txtnpg.Value = PageNumber
                PopParts(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "iprev" Then
                parts.Open()
                GetiPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ifirst" Then
                parts.Open()
                PageNumber = 1
                txtipg.Value = PageNumber
                PopParts(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        txtnpg.Value = PageNumber
        eqid = lblptid.Value
        Dim intPgCnt As Integer
        sql = "select count(*) from invsparepart where eqid = '" & eqid & "'"
        Tables = "invsparepart"
        PK = "spid"
        Filter = "eqid = " & eqid
        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblnpg.Text = "Page 0 of 0"
        Else
            lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtnpgcnt.Value = intPgNav

        dr = parts.GetPage(Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group)
        dgparttasks.DataSource = dr
        dgparttasks.DataBind()
        dr.Close()
        txtnpg.Value = PageNumber
        'txtnpgcnt.Value = intPgNav
        'lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
    End Sub
    Private Sub GetnNext()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg + 1
            txtnpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1454" , "sparepartlist.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetnPrev()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg - 1
            txtnpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1455" , "sparepartlist.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgparttasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.EditCommand
        dgparttasks.EditItemIndex = e.Item.ItemIndex
        PageNumber = txtnpg.Value
        parts.Open()
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgparttasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.CancelCommand
        parts.Open()
        dgparttasks.EditItemIndex = -1
        PageNumber = txtnpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgparttasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.UpdateCommand
        Dim qty As String = CType(e.Item.FindControl("dgqty"), TextBox).Text
        If qty = "" Then
            qty = "1"
        End If
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt32(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1456" , "sparepartlist.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim desc As String = ""
        'desc = CType(e.Item.FindControl("txtsdesc"), TextBox).Text
        'desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        'desc = Replace(desc, "--", "-", , , vbTextCompare)
        'desc = Replace(desc, ";", ":", , , vbTextCompare)
        'If Len(desc) = 0 Then desc = ""
        'If Len(desc) > 100 Then
        'Dim strMessage As String =  tmod.getmsg("cdstr1457" , "sparepartlist.aspx.vb")
 
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        Dim id As String = CType(e.Item.FindControl("lbltskprtidre"), Label).Text
        eqid = lblptid.Value
        'description = '" & desc & "',
        sql = "update invsparepart set quantity = '" & qty & "',  cost = " & qty & " * cost where spid = '" & id & "'"
        parts.Open()
        parts.Update(sql)
        dgparttasks.EditItemIndex = -1
        PageNumber = txtnpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgparttasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.DeleteCommand
        parts.Open()
        eqid = lblptid.Value
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lbltskprtidr"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbltskprtidre"), Label).Text
        End Try
        sql = "delete from invsparepart where spid = '" & id & "'"
        parts.Update(sql)
        dgparttasks.EditItemIndex = -1
        sql = "select Count(*) from invsparepart " _
        + "where spid = '" & id & "'"
        PageNumber = parts.PageCount(sql, PageSize)
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub

    '*** Inventory
    Private Sub PopParts(ByVal iPageNumber As Integer)
        txtipg.Value = iPageNumber

        'Get Count
        Dim Filt As String = lblfilter.Value
        'Filt = Replace(Filt, "'", Chr(180), , , vbTextCompare)
        If Len(Filt) > 0 Then
            Filter = lblfilterwd.Value
            sql = "select count(*)from item where " & Filt
        Else
            sql = "select count(*)from item"
        End If
        Dim intPgCnt As Integer

        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblipg.Text = "Page 0 of 0"
        Else
            lblipg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtipgcnt.Value = intPgNav

        'Get Page
        Tables = "item"
        PK = "itemid"
        Dim dr As SqlDataReader
        dr = parts.GetPage(Tables, PK, sort, iPageNumber, PageSize, Fields, Filter, Group)
        dgitems.DataSource = dr
        dgitems.DataBind()
        dr.Close()
        txtipg.Value = iPageNumber
        'txtipgcnt.Value = intPgNav
        'lblipg.Text = "Page " & iPageNumber & " of " & intPgNav
        'End Try
    End Sub

    Private Sub GetiNext()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg + 1
            txtipg.Value = PageNumber
            PopParts(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1458" , "sparepartlist.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetiPrev()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg - 1
            txtipg.Value = PageNumber
            PopParts(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1459" , "sparepartlist.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgitems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgitems.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim chkS As CheckBox = CType(e.Item.FindControl("cb1"), CheckBox)
                Dim lbl As Label = CType(e.Item.FindControl("itid"), Label)
                Dim lbli As Label = CType(e.Item.FindControl("itemid"), Label)
                'Dim txt As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
                chkS.Attributes.Add("onclick", "getqty('" & lbl.Text & "', '" & chkS.ClientID & "', '" & lbli.Text & "');")
                Dim item As ListItem = New ListItem(lbl.Text, lbl.Text)
                If lbchecked.Items.Contains(item) Then
                    chkS.Checked = True
                    'txt.Text = item.Value.ToString
                Else
                    chkS.Checked = False
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub RetainItems()
        Dim dgI As DataGridItem
        Dim chkS As CheckBox

        For Each dgI In dgitems.Items
            chkS = CType(dgI.FindControl("cb1"), CheckBox)
            If chkS.Checked Then
                Try
                    Dim lbl As Label = CType(dgI.FindControl("itid"), Label)
                    'Dim txt As TextBox = CType(dgI.FindControl("txtqty"), TextBox)
                    Dim item As ListItem = New ListItem(lbl.Text, lbl.Text)
                    If Not lbchecked.Items.Contains(item) Then
                        lbchecked.Items.Add(item)
                    End If
                Catch ex As Exception

                End Try

            Else
                Try
                    Dim lbl As Label = CType(dgI.FindControl("itid"), Label)
                    'Dim txt As TextBox = CType(dgI.FindControl("txtqty"), TextBox)
                    Dim item As ListItem = New ListItem(lbl.Text, lbl.Text)
                    If lbchecked.Items.Contains(item) Then
                        lbchecked.Items.Remove(item)
                    End If
                Catch ex As Exception

                End Try

            End If
        Next

    End Sub

    Private Sub ibtnaddtolist_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnaddtolist.Click
        Dim partstr As String = lblparts.Value

        Dim qtystr As String = lblqty.Value
        Dim idstr As String = lblitemid.Value
        Dim partarr As String() = Split(partstr, "~")
        Dim qtyarr As String() = Split(qtystr, "/")
        Dim idarr As String() = Split(idstr, "/")
        eqid = lblptid.Value
        typ = lbltyp.Value
        Dim oflag As Integer
        parts.Open()
        oflag = 0
        lbloflag.Value = "3"
        Dim i As Integer
        Dim num As Decimal
        Dim int As Integer
        If idarr.Length > 0 Then
            For i = 0 To idarr.Length - 1
                If partarr(i) <> "" Then
                    num = qtyarr(i)
                    int = Math.Round(num, 0)
                    partarr(i) = Replace(partarr(i), "~", "-", , , vbTextCompare)
                    partarr(i) = Replace(partarr(i), "'", Chr(180), , , vbTextCompare)
                    partarr(i) = Replace(partarr(i), "--", "-", , , vbTextCompare)
                    partarr(i) = Replace(partarr(i), ";", ":", , , vbTextCompare)
                    '@eqid int, @itemid int, @itemnum varchar(50), @qty int
                    sql = "usp_insertspareparts '" & eqid & "', '" & idarr(i) & "', " _
                    + "'" & partarr(i) & "', '" & int & "'"
                    parts.Update(sql)
                End If
            Next
        End If
        div1.Visible = False
        mdiv.Visible = True
        lblparts.Value = ""
        lblqty.Value = ""
        lblitemid.Value = ""
        PageNumber = txtnpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgitems_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.EditCommand
        iPageNumber = txtipg.Value
        lblold.Value = CType(e.Item.FindControl("itid"), Label).Text
        dgitems.EditItemIndex = e.Item.ItemIndex
        parts.Open()
        PopParts(iPageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgitems_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.CancelCommand
        iPageNumber = txtipg.Value
        dgitems.EditItemIndex = -1
        parts.Open()
        PopParts(iPageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgitems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.ItemCommand
        If e.CommandName = "Add" Then
            Dim num, des, loc, cost As String
            Dim nt, dt, lt As TextBox
            nt = CType(e.Item.FindControl("itnumf"), TextBox)
            dt = CType(e.Item.FindControl("txtdescf"), TextBox)
            lt = CType(e.Item.FindControl("txtlocf"), TextBox)
            num = CType(e.Item.FindControl("itnumf"), TextBox).Text
            num = Replace(num, "'", Chr(180), , , vbTextCompare)
            num = Replace(num, "--", "-", , , vbTextCompare)
            num = Replace(num, ";", ":", , , vbTextCompare)
            des = CType(e.Item.FindControl("txtdescf"), TextBox).Text
            des = Replace(des, "'", Chr(180), , , vbTextCompare)
            des = Replace(des, "--", "-", , , vbTextCompare)
            des = Replace(des, ";", ":", , , vbTextCompare)
            If Len(des) = 0 Then des = ""
            If Len(des) > 100 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1460" , "sparepartlist.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            loc = CType(e.Item.FindControl("txtlocf"), TextBox).Text
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
            loc = Replace(loc, "--", "-", , , vbTextCompare)
            loc = Replace(loc, ";", ":", , , vbTextCompare)
            If Len(loc) = 0 Then loc = ""
            If Len(loc) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1461" , "sparepartlist.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            cost = CType(e.Item.FindControl("txtcostf"), TextBox).Text
            If cost = "" Then
                cost = "0"
            End If
            Dim costchk As Decimal
            Try
                costchk = System.Convert.ToDecimal(cost)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1462" , "sparepartlist.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If Len(num) <= 30 And Len(num) <> 0 And num <> "" Then
                parts.Open()
                cid = lblcid.Value
                sql = "usp_insertinv 'part', '" & num & "','" & des & "','" & loc & "','" & cost & "','oinv'"
                Dim icnt As Integer
                icnt = parts.Scalar(sql)
                'sql = "insert into item " _
                '+ "(compid, itemnum, description, location) values " _
                '+ "('" & cid & "', '" & num & "', '" & des & "', '" & loc & "')"
                'inv.Update(sql)
                If icnt = 0 Then
                    sql = "select Count(*) from item " ' _
                    '+ "where compid = '" & cid & "'"
                    iPageNumber = parts.PageCount(sql, PageSize)
                    PopParts(iPageNumber)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1463" , "sparepartlist.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If

                parts.Dispose()
            Else

                If Len(num) = 0 OrElse num = "" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1464" , "sparepartlist.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                ElseIf Len(num) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1465" , "sparepartlist.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If
    End Sub

    Private Sub dgitems_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.UpdateCommand
        'Try
        parts.Open()
        Dim cost As String
        cid = lblcid.Value
        Dim id, num, part, desc, loc As String
        Dim flag As Integer = 1
        id = CType(e.Item.FindControl("txtid"), Label).Text
        num = CType(e.Item.FindControl("itnum"), Label).Text
        num = Replace(num, "'", Chr(180), , , vbTextCompare)
        num = Replace(num, "--", "-", , , vbTextCompare)
        num = Replace(num, ";", ":", , , vbTextCompare)
        cost = CType(e.Item.FindControl("txtcost"), TextBox).Text
        Dim costchk As Decimal
        Try
            costchk = System.Convert.ToDecimal(cost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1466" , "sparepartlist.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)

        If Len(desc) = 0 Then desc = ""
        If Len(desc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1467" , "sparepartlist.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        loc = CType(e.Item.FindControl("txtloc"), TextBox).Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)

        If Len(loc) = 0 Then loc = ""
        If Len(loc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1468" , "sparepartlist.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(num) <= 30 And Len(num) <> 0 And num <> "" Then
            Dim old As String = lblold.Value
            Dim icnt As Integer
            sql = "usp_updateinv 'part','" & id & "','" & num & "','" & desc & "','" & old & "','" & loc & "', '" & cost & "', '" & flag & "'"
            icnt = parts.Scalar(sql)
            If icnt = 0 Then
                dgitems.EditItemIndex = -1
                iPageNumber = txtipg.Value
                PopParts(iPageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1469" , "sparepartlist.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            'sql = "usp_updatePartsInv '" & id & "', '" & num & "', '" & desc & "', '" & loc & "', '" & cost & "', '" & flag & "'"
            'parts.Update(sql)

        Else
            If Len(num) = 0 OrElse num = "" Then
                Dim strMessage As String =  tmod.getmsg("cdstr1470" , "sparepartlist.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1471" , "sparepartlist.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If

        parts.Dispose()
    End Sub

    Private Sub btnradd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnradd.Click
        lbloflag.Value = "r"
        parts.Open()
        PopParts(PageNumber)
        parts.Dispose()
        div1.Style.Add("TOP", "1px")
        div1.Visible = True
        mdiv.Visible = False
    End Sub

    Private Sub Imagebutton4_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton4.Click
        parts.Open()
        PageNumber = txtnpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
        div1.Visible = False
        mdiv.Visible = True
        lbloflag.Value = "3"
        lblparts.Value = ""
        lblqty.Value = ""
        lblitemid.Value = ""
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgitems.Columns(0).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgitems", "0")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(1).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgitems", "1")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(3).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgitems", "3")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(5).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgitems", "5")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(6).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgitems", "6")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(7).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgitems", "7")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(0).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(1).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(3).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(5).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(6).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(7).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "7")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(8).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "8")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(9).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "9")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(11).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "11")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(13).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "13")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(14).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "14")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(15).HeaderText = dlabs.GetDGPage("sparepartlist.aspx", "dgparttasks", "15")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2914.Text = axlabs.GetASPXPage("sparepartlist.aspx", "lang2914")
        Catch ex As Exception
        End Try
        Try
            lang2915.Text = axlabs.GetASPXPage("sparepartlist.aspx", "lang2915")
        Catch ex As Exception
        End Try
        Try
            lang2916.Text = axlabs.GetASPXPage("sparepartlist.aspx", "lang2916")
        Catch ex As Exception
        End Try
        Try
            lang2917.Text = axlabs.GetASPXPage("sparepartlist.aspx", "lang2917")
        Catch ex As Exception
        End Try
        Try
            lang2918.Text = axlabs.GetASPXPage("sparepartlist.aspx", "lang2918")
        Catch ex As Exception
        End Try
        Try
            lang2919.Text = axlabs.GetASPXPage("sparepartlist.aspx", "lang2919")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub dgparttasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparttasks.ItemDataBound
        'Imagebutton6
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("Imagebutton6"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this record?')"
        End If
    End Sub
End Class
