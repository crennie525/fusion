

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class devTaskPartListtpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang2716 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2715 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2714 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2713 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2712 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2711 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim iPageNumber As Integer = 1
    Dim PageSize As Integer = 5
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim cid, cnm, typ As String
    Dim sort As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim parts As New Utilities
    Dim ptid, tnum As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Dim ap As New AppUtils
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnpg As System.Web.UI.WebControls.Label
    Protected WithEvents lblipg As System.Web.UI.WebControls.Label
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img7 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img8 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Imagebutton4 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login, ro As String
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnradd As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtpart As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsrch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgitems As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ibtnaddtolist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbchecked As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdtask As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents mdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents div1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtopg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblparts As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilterwd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        RetainItems()
        If Request.Form("lbladdflag") = "ok" Then
            lbladdflag.Value = ""
            parts.Open()
            iPageNumber = txtipg.Value
            PopParts(iPageNumber)
            parts.Dispose()
        End If
        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    dgparttasks.Columns(0).Visible = False
                    dgparttasks.Columns(7).Visible = False
                    dgitems.Columns(1).Visible = False
                    ibtnaddtolist.Enabled = False
                    ibtnaddtolist.ImageUrl = "../images/appbuttons/minibuttons/savedisk1dis.gif"
                End If
                Dim icost As String = ap.InvEntry
                If icost = "ext" Or icost = "inv" Then
                    dgitems.ShowFooter = False
                End If
                parts.Open()
                ptid = Request.QueryString("ptid")
                lblptid.Value = ptid
                Try
                    typ = Request.QueryString("typ")
                Catch ex As Exception
                    typ = "no"
                End Try
                lbltyp.Value = typ
                If typ = "jp" Or typ = "wjp" Then
                    lbljpnum.Value = Request.QueryString("jp").ToString
                    lblwo.Value = Request.QueryString("wo").ToString
                    tnum = Request.QueryString("tnum")
                    tdtask.InnerHtml = tnum
                    cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                    lblcid.Value = cid
                    PopTasks(PageNumber)
                    lbloflag.Value = "3"
                    div1.Visible = False
                ElseIf typ = "wo" Then
                    lblwo.Value = Request.QueryString("wo").ToString
                    tdtask.InnerHtml = "N/A"
                    cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                    lblcid.Value = cid
                    PopTasks(PageNumber)
                    lbloflag.Value = "3"
                    div1.Visible = False
                Else
                    If Len(ptid) <> 0 AndAlso ptid <> "" AndAlso ptid <> "0" Then
                        tnum = Request.QueryString("tnum")
                        tdtask.InnerHtml = tnum
                        cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                        lblcid.Value = cid
                        PopTasks(PageNumber)
                        lbloflag.Value = "3"
                        div1.Visible = False
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1136" , "devTaskPartListtpm.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "nodeptid"
                    End If
                End If
                parts.Dispose()
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1137" , "devTaskPartListtpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "nodeptid"
            End Try
        Else
            If Request.Form("lblret") = "nnext" Then
                parts.Open()
                GetnNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nlast" Then
                parts.Open()
                PageNumber = txtnpgcnt.Value
                txtnpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nprev" Then
                parts.Open()
                GetnPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nfirst" Then
                parts.Open()
                PageNumber = 1
                txtnpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "inext" Then
                parts.Open()
                GetiNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ilast" Then
                parts.Open()
                PageNumber = txtipgcnt.Value
                txtnpg.Value = PageNumber
                PopParts(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "iprev" Then
                parts.Open()
                GetiPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ifirst" Then
                parts.Open()
                PageNumber = 1
                txtipg.Value = PageNumber
                PopParts(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    '*** Revised
    Private Sub PopTasks(ByVal PageNumber As Integer)
        txtnpg.Value = PageNumber
        ptid = lblptid.Value
        typ = lbltyp.Value
        'parts.Open()
        'Get Count
        Dim intPgCnt As Integer
        If typ = "wo" Then
            sql = "select count(*) from woparts where wonum = '" & lblwo.Value & "'"
            Tables = "woparts"
            sort = "itemnum"
            PK = "wopartid"
            Fields = "*, wopartid as lineid"
            Filter = "wonum = " & lblwo.Value
        ElseIf typ = "jp" Then
            sql = "select count(*) from jpparts where pmtskid = '" & ptid & "'"
            Tables = "jpparts"
            sort = "itemnum"
            PK = "tskpartid"
            Fields = "*, tskpartid as lineid"
            Filter = "pmtskid = " & ptid
        ElseIf typ = "wjp" Then
            sql = "select count(*) from wojpparts where wojtid = '" & ptid & "'"
            Tables = "wojpparts"
            sort = "itemnum"
            PK = "wotskpartid"
            Fields = "*, wotskpartid as lineid"
            Filter = "wojtid = " & ptid
        Else
            sql = "select count(*) from pmTaskPartstpm where pmtskid = '" & ptid & "'"
            Tables = "pmTaskPartstpm"
            sort = "itemnum"
            PK = "tskpartid"
            Fields = "*, tskpartid as lineid"
            Filter = "pmtskid = " & ptid
        End If
        intPgCnt = parts.Scalar(sql)
        intPgNav = parts.PageCount(intPgCnt, PageSize)
        dr = parts.GetPage(Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group)
        dgparttasks.DataSource = dr
        dgparttasks.DataBind()
        dr.Close()
        txtnpg.Value = PageNumber
        txtnpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblnpg.Text = "Page 0 of 0"
        Else
            lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
    End Sub
    Private Sub GetnNext()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg + 1
            txtnpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1138" , "devTaskPartListtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetnPrev()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg - 1
            txtnpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1139" , "devTaskPartListtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgparttasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.DeleteCommand
        parts.Open()
        ptid = lblptid.Value
        typ = lbltyp.Value
        Dim wo As String = lblwo.Value
        Dim jpid As String = lbljpnum.Value
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lbltskprtidr"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbltskprtidre"), Label).Text
        End Try
        If typ = "wo" Then
            sql = "delete from woparts where wopartid = '" & id & "' and wonum = '" & wo & "'; " _
            + "update workorder set estmatcost = (select sum(total) from woparts where wonum = '" & wo & "') " _
            + "where wonum = '" & wo & "'"
            parts.Update(sql)
            sql = "usp_delwopart '" & lblwo.Value & "'"
            parts.Update(sql)
            dgparttasks.EditItemIndex = -1
            sql = "select Count(*) from woparts " _
                   + "where wonum = '" & lblwo.Value & "'"
        ElseIf typ = "jp" Then
            sql = "delete from jpparts where tskpartid = '" & id & "'"
            parts.Update(sql)
            If wo <> "" Then
                sql = "delete from wojpparts where tskpartid = '" & id & "' and wonum = '" & wo & "'; " _
                + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wo & "' and " _
                + "jpid = '" & jpid & "') where wonum = '" & wo & "'"
                parts.Update(sql)
            End If
            sql = "usp_deljppart '" & ptid & "','" & wo & "','" & jpid & "'"
            parts.Update(sql)
            dgparttasks.EditItemIndex = -1
            sql = "select Count(*) from jpparts " _
                   + "where pmtskid = '" & ptid & "'"
        ElseIf typ = "wjp" Then
            sql = "delete from wojpparts where wotskpartid = '" & id & "' and wonum = '" & wo & "'; " _
            + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wo & "' and " _
            + "jpid = '" & jpid & "') where wonum = '" & wo & "'"
            parts.Update(sql)
            sql = "usp_delwjppart '" & ptid & "','" & wo & "','" & jpid & "'"
            parts.Update(sql)
            dgparttasks.EditItemIndex = -1
            sql = "select Count(*) from wojpparts " _
                   + "where wojtid = '" & ptid & "'"
        Else
            sql = "delete from pmtaskpartstpm where tskpartid = '" & id & "'"
            parts.Update(sql)
            sql = "usp_delTaskParttpm '" & ptid & "'"
            parts.Update(sql)
            dgparttasks.EditItemIndex = -1
            sql = "select Count(*) from pmtaskpartstpm " _
                   + "where pmtskid = '" & ptid & "'"
        End If
        PageNumber = parts.PageCount(sql, PageSize)
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub
    Private Sub dgparttasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.EditCommand
        dgparttasks.EditItemIndex = e.Item.ItemIndex
        PageNumber = txtnpg.Value
        parts.Open()
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub
    Private Sub dgparttasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.UpdateCommand
        Dim qty As String = CType(e.Item.FindControl("dgqty"), TextBox).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt32(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1140" , "devTaskPartListtpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim id As String = CType(e.Item.FindControl("lbltskprtidre"), Label).Text
        ptid = lblptid.Value
        typ = lbltyp.Value
        Dim jpid As String = lbljpnum.Value
        Dim wo As String = lblwo.Value
        If typ = "wo" Then
            sql = "update woparts set qty = '" & qty & "', total = qty * cost where wopartid = '" & id & "'; " _
            + "update workorder set estmatcost = (select sum(total) from woparts where wonum = '" & wo & "') " _
            + "where wonum = '" & wo & "'"
            parts.Open()
            parts.Update(sql)
            sql = "usp_delwopart '" & lblwo.Value & "'"
            parts.Update(sql)
        ElseIf typ = "jp" Then
            sql = "update jpparts set qty = '" & qty & "', total = qty * cost where tskpartid = '" & id & "'"
            parts.Open()
            If wo <> "" Then
                sql = "update wojpparts set qty = '" & qty & "', total = qty * cost where tskpartid = '" & id & "' and wonum = '" & wo & "'; " _
                + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wo & "' and " _
                + "jpid = '" & jpid & "') where wonum = '" & wo & "'"
                parts.Open()
            End If
            parts.Update(sql)
            sql = "usp_deljppart '" & ptid & "','" & wo & "','" & jpid & "'"
            parts.Update(sql)
        ElseIf typ = "wjp" Then
            sql = "update wojpparts set qty = '" & qty & "', total = qty * cost where wotskpartid = '" & id & "' and wonum = '" & wo & "'; " _
            + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wo & "' and " _
            + "jpid = '" & jpid & "') where wonum = '" & wo & "'"
            parts.Open()
            parts.Update(sql)
            sql = "usp_delwjppart '" & ptid & "','" & wo & "','" & jpid & "'"
            parts.Update(sql)
        Else
            sql = "update pmtaskpartspm set qty = '" & qty & "', total = qty * cost where tskpartid = '" & id & "'"
            parts.Open()
            parts.Update(sql)
            sql = "usp_delTaskPart '" & ptid & "'"
            parts.Update(sql)
        End If
        dgparttasks.EditItemIndex = -1
        PageNumber = txtnpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub
    Private Sub dgparttasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.CancelCommand
        parts.Open()
        dgparttasks.EditItemIndex = -1
        PageNumber = txtnpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
    End Sub
    '*** Add Items

    Private Sub btnradd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnradd.Click
        lbloflag.Value = "r"
        parts.Open()
        PopParts(PageNumber)
        parts.Dispose()
        div1.Style.Add("TOP", "1px")
        div1.Visible = True
        mdiv.Visible = False
    End Sub

    '*** Inventory
    Private Sub PopParts(ByVal iPageNumber As Integer)
        txtipg.Value = iPageNumber

        'Get Count
        Dim Filt As String = lblfilter.Value
        'Filt = Replace(Filt, "'", Chr(180), , , vbTextCompare)
        If Len(Filt) > 0 Then
            Filter = lblfilterwd.Value
            sql = "select count(*)from item where " & Filt
        Else
            sql = "select count(*)from item"
        End If
        Dim intPgCnt As Integer

        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        PageSize = "100"

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblipg.Text = "Page 0 of 0"
        Else
            lblipg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtipgcnt.Value = intPgNav

        'Get Page
        Tables = "item"
        PK = "itemid"
        sort = "itemnum"
        Dim dr As SqlDataReader
        dr = parts.GetPage(Tables, PK, sort, iPageNumber, PageSize, Fields, Filter, Group)
        dgitems.DataSource = dr
        dgitems.DataBind()
        dr.Close()
        txtipg.Value = iPageNumber
        'txtipgcnt.Value = intPgNav
        'lblipg.Text = "Page " & iPageNumber & " of " & intPgNav
        'End Try
    End Sub

    Private Sub GetiNext()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg + 1
            txtipg.Value = PageNumber
            PopParts(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1141" , "devTaskPartListtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetiPrev()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg - 1
            txtipg.Value = PageNumber
            PopParts(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1142" , "devTaskPartListtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgitems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgitems.ItemDataBound

        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim chkS As CheckBox = CType(e.Item.FindControl("cb1"), CheckBox)
                Dim lbl As Label = CType(e.Item.FindControl("itid"), Label)
                Dim lbli As Label = CType(e.Item.FindControl("itemid"), Label)
                'Dim txt As TextBox = CType(e.Item.FindControl("txtqty"), TextBox)
                chkS.Attributes.Add("onclick", "getqty('" & lbl.Text & "', '" & chkS.ClientID & "', '" & lbli.Text & "');")
                Dim item As ListItem = New ListItem(lbl.Text, lbl.Text)
                If lbchecked.Items.Contains(item) Then
                    chkS.Checked = True
                    'txt.Text = item.Value.ToString
                Else
                    chkS.Checked = False
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub RetainItems()
        Dim dgI As DataGridItem
        Dim chkS As CheckBox

        For Each dgI In dgitems.Items
            chkS = CType(dgI.FindControl("cb1"), CheckBox)
            If chkS.Checked Then
                Try
                    Dim lbl As Label = CType(dgI.FindControl("itid"), Label)
                    'Dim txt As TextBox = CType(dgI.FindControl("txtqty"), TextBox)
                    Dim item As ListItem = New ListItem(lbl.Text, lbl.Text)
                    If Not lbchecked.Items.Contains(item) Then
                        lbchecked.Items.Add(item)
                    End If
                Catch ex As Exception

                End Try

            Else
                Try
                    Dim lbl As Label = CType(dgI.FindControl("itid"), Label)
                    'Dim txt As TextBox = CType(dgI.FindControl("txtqty"), TextBox)
                    Dim item As ListItem = New ListItem(lbl.Text, lbl.Text)
                    If lbchecked.Items.Contains(item) Then
                        lbchecked.Items.Remove(item)
                    End If
                Catch ex As Exception

                End Try

            End If
        Next

    End Sub

    Private Sub ibtnaddtolist_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnaddtolist.Click
        Dim partstr As String = lblparts.Value

        Dim qtystr As String = lblqty.Value
        Dim idstr As String = lblitemid.Value
        Dim partarr As String() = Split(partstr, "~")
        Dim qtyarr As String() = Split(qtystr, "/")
        Dim idarr As String() = Split(idstr, "/")
        ptid = lblptid.Value
        typ = lbltyp.Value
        Dim oflag As Integer
        'If lbloflag.Value = "o" Then
        'oflag = 1
        ' Else
        'oflag = 0
        'End If
        parts.Open()
        oflag = 0
        If typ <> "wo" And typ <> "jp" Then
            parts.UpModTask(ptid)
        End If
        lbloflag.Value = "3"

        Dim i As Integer
        'One for one for now
        'Look at bulk insert later - ref eq copy
        'usp_insertTaskParts (@taskid int, @itemid int, @itemnum varchar(50),  @qty int, @flag int) as
        Dim num As Decimal
        Dim int As Integer
        Dim wo As String = lblwo.Value
        If idarr.Length > 0 Then
            For i = 0 To idarr.Length - 1
                If partarr(i) <> "" Then
                    num = qtyarr(i)
                    int = Math.Round(num, 0)
                    partarr(i) = Replace(partarr(i), "~", "-", , , vbTextCompare)
                    partarr(i) = Replace(partarr(i), "'", Chr(180), , , vbTextCompare)
                    partarr(i) = Replace(partarr(i), "--", "-", , , vbTextCompare)
                    partarr(i) = Replace(partarr(i), ";", ":", , , vbTextCompare)
                    partarr(i) = Replace(partarr(i), "'", Chr(180), , , vbTextCompare)
                    partarr(i) = Replace(partarr(i), """", Chr(180) & Chr(180), , , vbTextCompare)
                    If typ = "wo" Then
                        sql = "usp_insertwoparts '" & lblwo.Value & "', '" & idarr(i) & "', " _
                        + "'" & partarr(i) & "', '" & int & "', '" & oflag & "'"
                    ElseIf typ = "jp" Then
                        Dim jpid As String = lbljpnum.Value
                        sql = "usp_insertjpparts '" & jpid & "', '" & ptid & "', '" & idarr(i) & "', " _
                        + "'" & partarr(i) & "', '" & int & "', '" & oflag & "','" & wo & "'"
                    ElseIf typ = "wjp" Then
                        Dim jpid As String = lbljpnum.Value
                        sql = "usp_insertwjpparts '" & jpid & "', '" & ptid & "', '" & idarr(i) & "', " _
                        + "'" & partarr(i) & "', '" & int & "', '" & oflag & "','" & wo & "'"
                    Else
                        sql = "usp_insertTaskPartstpm '" & ptid & "', '" & idarr(i) & "', " _
                        + "'" & partarr(i) & "', '" & int & "', '" & oflag & "'"
                    End If
                    parts.Update(sql)
                End If
            Next
        End If
        PageNumber = txtnpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
        div1.Visible = False
        mdiv.Visible = True
        lblparts.Value = ""
        lblqty.Value = ""
        lblitemid.Value = ""


    End Sub


    Private Sub btnsrch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsrch.Click
        Dim Filt As String = txtpart.Text
        Filt = Replace(Filt, "'", Chr(180), , , vbTextCompare)
        Filt = Replace(Filt, "--", "-", , , vbTextCompare)
        Filt = Replace(Filt, ";", ":", , , vbTextCompare)
        Filt = Replace(Filt, "'", Chr(180), , , vbTextCompare)
        Filt = Replace(Filt, """", Chr(180) & Chr(180), , , vbTextCompare)
        If Len(Filt) > 0 Then
            lblfilter.Value = "itemnum like '%" & Filt & "%' or description like '%" & Filt & "%' or location like '%" & Filt & "%'"
            lblfilterwd.Value = "itemnum like ''%" & Filt & "%'' or description like ''%" & Filt & "%'' or location like ''%" & Filt & "%''"
        End If
        parts.Open()
        iPageNumber = "1" 'txtipg.Value
        PopParts(iPageNumber)
        parts.Dispose()
    End Sub

    Private Sub ibtnaddnew_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim pcnt As Integer
        Dim pnum As String = txtpart.Text
        If Len(pnum) <= 30 AndAlso Len(pnum) > 0 Then
            parts.Open()
            pnum = Replace(pnum, "~", "-", , , vbTextCompare)
            pnum = Replace(pnum, "'", Chr(180), , , vbTextCompare)
            pnum = Replace(pnum, "--", "-", , , vbTextCompare)
            pnum = Replace(pnum, ";", ":", , , vbTextCompare)
            pnum = Replace(pnum, "'", Chr(180), , , vbTextCompare)
            pnum = Replace(pnum, """", Chr(180) & Chr(180), , , vbTextCompare)
            sql = "select count(*) from item where itemnum = '" & pnum & "'"
            pcnt = parts.Scalar(sql)
            If pcnt = 0 Then
                lbladdflag.Value = "yes"
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1143" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            parts.Dispose()
        Else
            If Len(pnum) = 0 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1144" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1145" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If


    End Sub

    Private Sub dgitems_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.EditCommand
        iPageNumber = txtipg.Value
        lblold.Value = CType(e.Item.FindControl("itid"), Label).Text
        dgitems.EditItemIndex = e.Item.ItemIndex
        parts.Open()
        PopParts(iPageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgitems_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.UpdateCommand
        'Try
        parts.Open()
        Dim cost As String
        cid = lblcid.Value
        Dim id, num, part, desc, loc As String
        Dim flag As Integer = 1
        id = CType(e.Item.FindControl("txtid"), Label).Text
        num = CType(e.Item.FindControl("itnum"), Label).Text
        num = Replace(num, "'", Chr(180), , , vbTextCompare)
        num = Replace(num, "--", "-", , , vbTextCompare)
        num = Replace(num, ";", ":", , , vbTextCompare)
        num = Replace(num, "'", Chr(180), , , vbTextCompare)
        num = Replace(num, """", Chr(180) & Chr(180), , , vbTextCompare)
        cost = CType(e.Item.FindControl("txtcost"), TextBox).Text
        Dim costchk As Decimal
        Try
            costchk = System.Convert.ToDecimal(cost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1146" , "devTaskPartListtpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, """", Chr(180) & Chr(180), , , vbTextCompare)
        If Len(desc) = 0 Then desc = ""
        If Len(desc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1147" , "devTaskPartListtpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        loc = CType(e.Item.FindControl("txtloc"), TextBox).Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, """", Chr(180) & Chr(180), , , vbTextCompare)
        If Len(loc) = 0 Then loc = ""
        If Len(loc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1148" , "devTaskPartListtpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(num) <= 30 And Len(num) <> 0 And num <> "" Then
            Dim old As String = lblold.Value
            Dim icnt As Integer
            sql = "usp_updateinv 'part','" & id & "','" & num & "','" & desc & "','" & old & "','" & loc & "', '" & cost & "', '" & flag & "'"
            icnt = parts.Scalar(sql)
            If icnt = 0 Then
                dgitems.EditItemIndex = -1
                iPageNumber = txtipg.Value
                PopParts(iPageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1149" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            'sql = "usp_updatePartsInv '" & id & "', '" & num & "', '" & desc & "', '" & loc & "', '" & cost & "', '" & flag & "'"
            'parts.Update(sql)

        Else
            If Len(num) = 0 OrElse num = "" Then
                Dim strMessage As String =  tmod.getmsg("cdstr1150" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1151" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If

        parts.Dispose()
    End Sub

    Private Sub dgitems_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.CancelCommand
        iPageNumber = txtipg.Value
        dgitems.EditItemIndex = -1
        parts.Open()
        PopParts(iPageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgitems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgitems.ItemCommand
        If e.CommandName = "Add" Then
            Dim num, des, loc, cost As String
            Dim nt, dt, lt As TextBox
            nt = CType(e.Item.FindControl("itnumf"), TextBox)
            dt = CType(e.Item.FindControl("txtdescf"), TextBox)
            lt = CType(e.Item.FindControl("txtlocf"), TextBox)
            num = CType(e.Item.FindControl("itnumf"), TextBox).Text
            num = Replace(num, "'", Chr(180), , , vbTextCompare)
            num = Replace(num, "--", "-", , , vbTextCompare)
            num = Replace(num, ";", ":", , , vbTextCompare)
            num = Replace(num, "'", Chr(180), , , vbTextCompare)
            num = Replace(num, """", Chr(180) & Chr(180), , , vbTextCompare)
            des = CType(e.Item.FindControl("txtdescf"), TextBox).Text
            des = Replace(des, "'", Chr(180), , , vbTextCompare)
            des = Replace(des, "--", "-", , , vbTextCompare)
            des = Replace(des, ";", ":", , , vbTextCompare)
            des = Replace(des, "'", Chr(180), , , vbTextCompare)
            des = Replace(des, """", Chr(180) & Chr(180), , , vbTextCompare)
            If Len(des) = 0 Then des = ""
            If Len(des) > 100 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1152" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            loc = CType(e.Item.FindControl("txtlocf"), TextBox).Text
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
            loc = Replace(loc, "--", "-", , , vbTextCompare)
            loc = Replace(loc, ";", ":", , , vbTextCompare)
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
            loc = Replace(loc, """", Chr(180) & Chr(180), , , vbTextCompare)
            If Len(loc) = 0 Then loc = ""
            If Len(loc) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1153" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            cost = CType(e.Item.FindControl("txtcostf"), TextBox).Text
            If cost = "" Then
                cost = "0"
            End If
            Dim costchk As Decimal
            Try
                costchk = System.Convert.ToDecimal(cost)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1154" , "devTaskPartListtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If Len(num) <= 30 And Len(num) <> 0 And num <> "" Then
                parts.Open()
                cid = lblcid.Value
                sql = "usp_insertinv 'part', '" & num & "','" & des & "','" & loc & "','" & cost & "','oinv'"
                Dim icnt As Integer
                icnt = parts.Scalar(sql)
                'sql = "insert into item " _
                '+ "(compid, itemnum, description, location) values " _
                '+ "('" & cid & "', '" & num & "', '" & des & "', '" & loc & "')"
                'inv.Update(sql)
                If icnt = 0 Then
                    sql = "select Count(*) from item " '_
                    '+ "where compid = '" & cid & "'"
                    iPageNumber = parts.PageCount(sql, PageSize)
                    PopParts(iPageNumber)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1155" , "devTaskPartListtpm.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If

                parts.Dispose()
            Else

                If Len(num) = 0 OrElse num = "" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1156" , "devTaskPartListtpm.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                ElseIf Len(num) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1157" , "devTaskPartListtpm.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If
    End Sub

    Private Sub Imagebutton4_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton4.Click
        parts.Open()
        PageNumber = txtnpg.Value
        PopTasks(PageNumber)
        parts.Dispose()
        div1.Visible = False
        mdiv.Visible = True
        lbloflag.Value = "3"
        lblparts.Value = ""
        lblqty.Value = ""
        lblitemid.Value = ""
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgitems.Columns(0).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgitems", "0")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(1).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgitems", "1")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(3).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgitems", "3")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(5).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgitems", "5")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(6).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgitems", "6")
        Catch ex As Exception
        End Try
        Try
            dgitems.Columns(7).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgitems", "7")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(0).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(1).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(3).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(5).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(6).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(7).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "7")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(8).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "8")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(9).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "9")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(11).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "11")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(13).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "13")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(14).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "14")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(15).HeaderText = dlabs.GetDGPage("devTaskPartListtpm.aspx", "dgparttasks", "15")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2711.Text = axlabs.GetASPXPage("devTaskPartListtpm.aspx", "lang2711")
        Catch ex As Exception
        End Try
        Try
            lang2712.Text = axlabs.GetASPXPage("devTaskPartListtpm.aspx", "lang2712")
        Catch ex As Exception
        End Try
        Try
            lang2713.Text = axlabs.GetASPXPage("devTaskPartListtpm.aspx", "lang2713")
        Catch ex As Exception
        End Try
        Try
            lang2714.Text = axlabs.GetASPXPage("devTaskPartListtpm.aspx", "lang2714")
        Catch ex As Exception
        End Try
        Try
            lang2715.Text = axlabs.GetASPXPage("devTaskPartListtpm.aspx", "lang2715")
        Catch ex As Exception
        End Try
        Try
            lang2716.Text = axlabs.GetASPXPage("devTaskPartListtpm.aspx", "lang2716")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub dgparttasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparttasks.ItemDataBound
        'Imagebutton6
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("Imagebutton6"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this record?')"
        End If
    End Sub
End Class
