﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="invuploadimage.aspx.vb"
    Inherits="lucy_r12.invuploadimage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/tpmimgtasknav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmuploadimage1aspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function checkit() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "return") {
                window.parent.handleexit();
            }
        }
    </script>
</head>
<body onload="checkit();">
    <form id="form1" runat="server">
    <div>
        <table width="520">
            <tbody>
                <tr>
                    <td class="thdrsing label" colspan="3">
                        <asp:Label ID="lang3302" runat="server">Image Upload Dialog</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="bluelabel" width="150">
                        <asp:Label ID="lang3303" runat="server">Choose Image to Upload</asp:Label>
                    </td>
                    <td class="plainlabel" width="240">
                        <input id="MyFile" style="width: 230px" type="file" size="5" name="MyFile" runat="Server">
                    </td>
                    <td width="130">
                        <input class="plainlabel" id="btnupload" type="button" value="Upload" name="btnupload"
                            runat="server">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <input id="lblitemid" type="hidden" runat="server">
    <input type="hidden" id="lbltasknum" runat="server" name="lbltasknum">
    <input type="hidden" id="lblfunc" runat="server" name="lblfunc"><input id="lblimgid"
        type="hidden" runat="server" name="lblimgid">
    <input id="lblro" type="hidden" runat="server" name="lblro"><input id="spdivy" type="hidden"
        name="spdivy" runat="server">
    <input id="lblpicorder" type="hidden" runat="server" name="lblpicorder">
    <input id="lblsubmit" type="hidden" runat="server" name="lblsubmit">
    <input id="lblneworder" type="hidden" runat="server" name="lblneworder">
    <input id="lbloldorder" type="hidden" runat="server" name="lbloldorder">
    <input id="lblmaxorder" type="hidden" runat="server" name="lblmaxorder">
    <input id="lblititles" type="hidden" runat="server" name="lblititles">
    <input id="lblref" type="hidden" runat="server" name="lblref"><input type="hidden"
        id="lbloldtask" runat="server" name="lbloldtask">
    <input type="hidden" id="lblmaxtask" runat="server" name="lblmaxtask"><input type="hidden"
        id="lbledit" runat="server" name="lbledit">
    <input type="hidden" id="lblimg" runat="server" name="lblimg">
    <input type="hidden" id="lblbimg" runat="server" name="lblbimg">
    <input type="hidden" id="lblnimg" runat="server" name="lblnimg">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
