<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="catalog.aspx.vb" Inherits="lucy_r12.catalog" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>catalog</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/catalogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
     <!--
        function addloc() {
            var code = document.getElementById("txtcode").value;
            var store = document.getElementById("lblstore").value;
            if (code == "") {
                alert("No Item Entered")
            }
            else if (store == "") {
                alert("No Store Room Entered")
            }
            else {
                var ro = document.getElementById("lblro").value;
                if (ro != "1") {
                    document.getElementById("lblsubmit").value = "add";
                    document.getElementById("form1").submit();
                }
            }
        }
         function addpic() {
             var itemid = document.getElementById("lblcodeid").value;
             var imgid = document.getElementById("lblimgid").value;
             if (itemid != "") {
                 if (imgid == "") {
                     var eReturn = window.showModalDialog("invuploadimagedialog.aspx?itemid=" + itemid + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:600px; resizable=yes");
                     if (eReturn == "ok") {
                         document.getElementById("lblsubmit").value = "checkpic";
                         document.getElementById("form1").submit()
                     }
                 }
                 else {
                     alert("Please Delete the Current Image Before Selecting a New Image for this Item")
                 }
             }
         }
         function delpic() {
             var imgid = document.getElementById("lblimgid").value;
             //alert(imgid)
             if (imgid != "") {
                 document.getElementById("lblsubmit").value = "delimg";
                 document.getElementById("form1").submit()
             }
         }
                  
         function GetItem(typ, fld1, fld2, fld3, fld4) {
             var chk = document.getElementById("lblitemid").value;
             var chk2 = document.getElementById("txtcode").value;
             if (chk != "" || chk2 != "" || typ == "srch") {
                 if (typ == "srch") {
                     list = document.getElementById("ddtype").value;
                 }
                 else {
                     list = typ;
                 }
                 var eReturn = window.showModalDialog("invdialog.aspx?filt=no&list=" + list + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
                 if (eReturn) {
                     if (eReturn == "log") {
                         window.parent.handlelogout();
                     }
                     else if (eReturn != "can") {
                         var ret = eReturn;
                         var retarr = ret.split(";")
                         if (typ == "srch") {
                             if (fld1 == "alt") {
                                 document.getElementById("lblchildid").value = retarr[0];
                                 document.getElementById("lblchild").value = retarr[1];
                                 var ro = document.getElementById("lblro").value;
                                 if (ro != "1") {
                                     document.getElementById("lblsubmit").value = "addchild";
                                     document.getElementById("form1").submit();
                                 }
                             }
                             else {
                                 document.getElementById("txtcode").value = retarr[1];
                                 document.getElementById("lblitemid").value = retarr[0];
                                 document.getElementById("lblsubmit").value = "get";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else if (typ == "storeroom") {
                             document.getElementById(fld1).value = retarr[1];
                             document.getElementById(fld2).value = retarr[2];
                             document.getElementById(fld3).innerHTML = retarr[1];
                             document.getElementById(fld4).innerHTML = retarr[2];
                         }
                         else {
                             document.getElementById(fld1).value = retarr[1];
                             document.getElementById(fld2).innerHTML = retarr[1];
                         }

                     }
                 }
             }
             else {
                alert("No Item Selected")
             }
         }
         //-->
    </script>
    <style type="text/css">
        .readonly90
        {
            border: 1px solid #CBCCCE;
            background-color: #ECF0F3;
            font-family: Arial,MS Sans Serif;
            font-size: 12px;
            vertical-align: middle;
            display: table-cell;
            text-indent: 1px;
            height: 20px;
            width: 90px;
        }
        .readonly140
        {
            border: 1px solid #CBCCCE;
            background-color: #ECF0F3;
            font-family: Arial,MS Sans Serif;
            font-size: 12px;
            vertical-align: middle;
            display: table-cell;
            text-indent: 1px;
            height: 20px;
            width: 140px;
        }
        .readonly340
        {
            border: 1px solid #CBCCCE;
            background-color: #ECF0F3;
            font-family: Arial,MS Sans Serif;
            font-size: 12px;
            vertical-align: middle;
            display: table-cell;
            text-indent: 1px;
            height: 20px;
            width: 340px;
        }
    </style>
</head>
<body class="tbg" onload="checkinv();">
    <form id="form1" method="post" runat="server">
    <div style="position: absolute; top: 4px; left: 4px">
        <table cellspacing="0" width="800">
            <tr>
                <td align="center" valign="middle" class="plainlabelred" colspan="2">
                    <asp:Label ID="lang2667" runat="server">Use the standard PM Inventory module if your Inventory is controlled outside of Fusion.</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" width="26">
                    <img src="../images/appbuttons/minibuttons/lilparts2.gif" border="0">
                </td>
                <td class="thdrsingrt label" align="left" width="764">
                    <asp:Label ID="lang2668" runat="server">Add/Edit Items</asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2">
                    <table cellpadding="0" width="770" border="0">
                        <tr>
                            <td width="100">
                            </td>
                            <td width="180">
                            </td>
                            <td width="90">
                            </td>
                            <td width="80">
                            </td>
                            <td width="22">
                            </td>
                            <td width="22">
                            </td>
                            <td width="22">
                            </td>
                            <td width="22">
                            </td>
                            <td width="22">
                            </td>
                            <td width="22">
                            </td>
                            <td width="252">
                            </td>
                        </tr>
                        <tr>
                            <td height="32" class="plainlabelblue" align="center" colspan="10">
                                <asp:Label ID="lang2669" runat="server">*To Add a New Item Enter an Item Code, Description, and select a Default Store Room Only.</asp:Label><br>
                                <asp:Label ID="lang2670" runat="server">Item Details and other Store Room Information can be updated after an Item Record has been Established.</asp:Label>
                            </td>
                            <td colspan="2" rowspan="3" align="right">
                                <table>
                                    <tr>
                                        <td>
                                            <img alt="" src="../images/appimages/filmstripsm.gif" id="imginv" runat="server" />
                                        </td>
                                        <td valign="middle">
                                            <img alt="" id="img3" onmouseover="return overlib('Add an Image for this Item')"
                                                onclick="addpic();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif"
                                                runat="server" /><br />
                                            <img alt="" id="img4" onmouseover="return overlib('Delete this Image')" onclick="delpic();"
                                                onmouseout="return nd()" src="../images/appbuttons/minibuttons/del.gif" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="Tr5" runat="server">
                            <td class="label">
                                <asp:Label ID="lang2671" runat="server">Item Code</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtcode" runat="server" Width="180px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang2672" runat="server">Item Type</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="part" Selected="True">Part</asp:ListItem>
                                    <asp:ListItem Value="tool">Tool</asp:ListItem>
                                    <asp:ListItem Value="lube">Lube</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <img id="imgadd" onmouseover="return overlib('Add a New Item with the Item Code, Description, and Store Room values you selected')"
                                    onclick="addloc();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif"
                                    runat="server">
                            </td>
                            <td>
                                <img id="imgsrch" onmouseover="return overlib('Look up Inventory Items')" onclick="GetItem('srch');"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                            </td>
                            <td>
                                <img id="Img1" onmouseover="return overlib('Save selected details')" onclick="saveloc();"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                    runat="server">
                            </td>
                            <td>
                                <img onmouseover="return overlib('Clear Page')" onclick="jumpto('');" onmouseout="return nd()"
                                    src="../images/appbuttons/minibuttons/refreshit.gif">
                            </td>
                        </tr>
                        <tr id="deptdiv" runat="server">
                            <td class="label">
                                <asp:Label ID="lang2673" runat="server">Description</asp:Label>
                            </td>
                            <td colspan="7">
                                <asp:TextBox ID="txtdesc" runat="server" Width="490px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" width="26">
                    <img src="../images/appbuttons/minibuttons/lilparts2.gif" border="0">
                </td>
                <td class="thdrsingrt label" align="left" width="764">
                    <asp:Label ID="lang2674" runat="server">Item Details</asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2">
                    <table width="760">
                        <tr>
                            <td class="label" width="100">
                                <asp:Label ID="lang2675" runat="server">Stock Type</asp:Label>
                            </td>
                            <td width="140">
                                <div class="readonly140" id="divstk" runat="server">
                                </div>
                            </td>
                            <td width="30">
                                <img id="Img2" onmouseover="return overlib('Select Stock Type')" onclick="GetItem('stocktype', 'lblstk', 'divstk');"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                            </td>
                            <td class="label" width="60">
                                <asp:Label ID="lang2676" runat="server">Hazard</asp:Label>
                            </td>
                            <td width="140">
                                <div class="readonly140" id="divhaz" runat="server">
                                </div>
                            </td>
                            <td width="30">
                                <img id="imghaz" onmouseover="return overlib('Select a Hazard for this Item')" onclick="GetItem('hazard', 'lblhaz', 'divhaz');"
                                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                            </td>
                            <td class="label" width="80">
                                <asp:Label ID="lang2677" runat="server">Spare part?</asp:Label>
                            </td>
                            <td width="50">
                                <asp:DropDownList ID="ddspare" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="N">N</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="label" width="80">
                                <asp:Label ID="lang2678" runat="server">Rotating?</asp:Label>
                            </td>
                            <td width="50">
                                <asp:DropDownList ID="ddrot" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="N">N</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2679" runat="server">Lot Type</asp:Label>
                            </td>
                            <td>
                                <div class="readonly140" id="divlot" runat="server">
                                </div>
                            </td>
                            <td>
                                <img id="imglot" onmouseover="return overlib('Select a Lot Type for this Item')"
                                    onclick="GetItem('lottype', 'lbllot', 'divlot');" onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                            </td>
                            <td class="label">
                                MSDS
                            </td>
                            <td>
                                <asp:TextBox ID="txtmsds" runat="server" Width="130px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td>
                                <img class="details" id="imgmsds" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                            </td>
                            <td class="label">
                                <asp:Label ID="lang2680" runat="server">Inspect?</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddinsp" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="N">N</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang2681" runat="server">Outside?</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddout" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="N">N</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2682" runat="server">Order Unit</asp:Label>
                            </td>
                            <td>
                                <div class="readonly140" id="divord" runat="server">
                                </div>
                            </td>
                            <td>
                                <img id="Img7" onmouseover="return overlib('Select an Order Unit for this Item')"
                                    onclick="GetItem('orderunit', 'lblord', 'divord');" onmouseout="return nd()"
                                    src="../images/appbuttons/minibuttons/magnifier.gif" runat="server">
                            </td>
                            <td class="label">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="label">
                            </td>
                            <td>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang2683" runat="server">Capitalized?</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddcap" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="N">N</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" width="26">
                    <img src="../images/appbuttons/minibuttons/lilparts2.gif" border="0">
                </td>
                <td class="thdrsingrt label" align="left" width="764">
                    <asp:Label ID="lang2684" runat="server">Storeroom Information</asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2">
                    <table cellspacing="1" cellpadding="1" width="770">
                        <tr>
                            <td width="140">
                            </td>
                            <td width="140">
                            </td>
                            <td width="30">
                            </td>
                            <td width="100">
                            </td>
                            <td width="130">
                            </td>
                            <td width="30">
                            </td>
                            <td width="120">
                            </td>
                            <td width="100">
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2685" runat="server">Default Store Room</asp:Label>
                            </td>
                            <td>
                                <div class="readonly140" id="divstore" runat="server">
                                </div>
                            </td>
                            <td>
                                <img onmouseover="return overlib('Select a Default Store Room for this Item')" onclick="GetItem('storeroom', 'lblstore', 'lblstdesc', 'divstore', 'divstdesc');"
                                    onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td colspan="4">
                                <div class="readonly340" id="divstdesc" runat="server">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2686" runat="server">Default Bin</asp:Label>
                            </td>
                            <td>
                                <div class="readonly140" id="divbin" runat="server">
                                </div>
                            </td>
                            <td class="label" colspan="2">
                                <asp:Label ID="lang2687" runat="server">Stock Category</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddcat" runat="server">
                                    <asp:ListItem Value="STK">STK</asp:ListItem>
                                    <asp:ListItem Value="NS">NS</asp:ListItem>
                                    <asp:ListItem Value="SPL">SPL</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang2688" runat="server">Current Balance</asp:Label>
                            </td>
                            <td>
                                <div class="readonly90" id="divcurbal" runat="server">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang2689" runat="server">Standard Cost</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtstd" runat="server" Width="90px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td class="label" colspan="2">
                                <asp:Label ID="lang2690" runat="server">Average Cost</asp:Label>
                            </td>
                            <td>
                                <div class="readonly90" id="divavg" runat="server">
                                </div>
                            </td>
                            <td>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang2691" runat="server">Last Cost</asp:Label>
                            </td>
                            <td>
                                <div class="readonly90" id="divlast" runat="server">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" width="26">
                    <img src="../images/appbuttons/minibuttons/lilparts2.gif" border="0">
                </td>
                <td class="thdrsingrt label" align="left" width="764">
                    <asp:Label ID="lang2692" runat="server">Alternate Items</asp:Label>
                </td>
            </tr>
            <tr>
                <td id="tdalt" colspan="2" runat="server">
                </td>
            </tr>
        </table>
    </div>
    <input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblfail" type="hidden"
        name="lblfail" runat="server">
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="lblchild"
        type="hidden" name="lblchild" runat="server">
    <input id="lbltype" type="hidden" name="lbltype" runat="server">
    <input id="lbloldcode" type="hidden" name="lbloldfail" runat="server">
    <input id="lblneweq" type="hidden" name="lblneweq" runat="server">
    <input id="lblcodeid" type="hidden" runat="server">
    <input id="lblchildid" type="hidden" runat="server"><input id="lblaltid" type="hidden"
        runat="server">
    <input id="lblitemid" type="hidden" runat="server"><input id="lblostore" type="hidden"
        runat="server">
    <input id="lblro" type="hidden" runat="server">
    <input type="hidden" id="lblimgid" runat="server" /><input type="hidden" id="lblbigimg"
        runat="server" />
    <input type="hidden" id="lblstk" runat="server" />
    <input type="hidden" id="lblhaz" runat="server" />
    <input type="hidden" id="lbllot" runat="server" />
    <input type="hidden" id="lblord" runat="server" />
    <input type="hidden" id="lblstore" runat="server" />
    <input type="hidden" id="lblstdesc" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblcurbal" runat="server" />
    <input type="hidden" id="lblavg" runat="server" />
    <input type="hidden" id="lbllast" runat="server" />
    <input type="hidden" id="lblbin" runat="server" />
    </form>
</body>
</html>
