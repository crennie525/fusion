<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PopQty.aspx.vb" Inherits="lucy_r12.PopQty" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Add Qty</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/PopQtyaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function handleexit() {
             var chk = document.getElementById("lblchk").value
             if (chk == "0") {
                 window.returnValue = "can";
                 window.close();
             }
         }
         function handlecancel() {
             document.getElementById("lblchk").value = "1"
             window.returnValue = "can";
             window.close();
         }
         function handleqty() {
             var qty = document.getElementById("txtqty").value;
             var who = document.getElementById("lblwho").value;
             if (isNaN(qty)) {
                 alert("Please Enter a Numeric Value")
             }
             else {
                 document.getElementById("lblchk").value = "1"
                 window.returnValue = qty;
                 window.close();
             }
         }
         function showFocus() {
             document.getElementById("txtqty").focus();
             document.getElementById("lblchk").value = "0"
         }
		
		
     </script>
	</HEAD>
	<body  bgcolor="white" onload="showFocus();" onunload="handleexit();">
		<form id="form1" method="post" runat="server">
			<table align="center" >
				<tr>
					<td class="bluelabel" align="right" width="80" height="40"><asp:Label id="lang2827" runat="server">Enter Qty</asp:Label></td>
					<td width="80">
						<asp:TextBox id="txtqty" runat="server" Width="48px"></asp:TextBox></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><INPUT type="button" onclick="handleqty();" value="OK" style="WIDTH: 70px"> 
						&nbsp; <INPUT type="button" onclick="handlecancel();" value="Cancel" style="WIDTH: 70px"></td>
				</tr>
			</table>
			<input type="hidden" id="lblchk">
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblwho" runat="server" />
</form>
	</body>
</HTML>
