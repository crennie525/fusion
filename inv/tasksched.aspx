<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tasksched.aspx.vb" Inherits="lucy_r12.tasksched" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>tasksched</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/taskschedaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table id="mdiv" cellSpacing="0" cellPadding="2" width="820" runat="server" style="LEFT: 4px; POSITION: absolute; TOP: 4px">
				<TBODY>
					<tr>
						<td colspan="3">
							<table width="820">
								<tr>
									<td colSpan="3">
										<table cellSpacing="0">
											<tr>
												<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
												<td class="thdrsingrt label" width="774"><asp:Label id="lang2956" runat="server">Item Availability Dialog</asp:Label></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr height="20">
						<td colspan="3" align="center" class="plainlabelblue"><asp:Label id="lang2957" runat="server">*Items are displayed to indicate Current Balances per Store Room and per Lot.</asp:Label><br><asp:Label id="lang2958" runat="server">If an item is stored in multiple Store Rooms or has multiple lots choose any to select an Alternate Item.</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="3"><asp:datagrid id="dgparttasks" runat="server" cellSpacing="1" cellPadding="1" AutoGenerateColumns="False"
								GridLines="None">
								<AlternatingItemStyle BackColor="#E7F1FD" CssClass="plainlabel"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Item#">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lbltskprtidr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tskpartid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lbltskprtidre" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tskpartid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty Required">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="dgqty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty In Stock">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.curbal") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="Textbox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.curbal") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Store Room">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sloc") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="Textbox2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sloc") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Lot#">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lotnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="Textbox3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lotnum") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/magnifier.gif">
										<HeaderStyle Width="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<img src="../images/appbuttons/minibuttons/magnifier.gif" id="imggetalt" runat="server">
										</ItemTemplate>
										<EditItemTemplate>
											<img src="../images/appbuttons/minibuttons/btn_calendar.jpg" id="Img2" runat="server">
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
				</TBODY>
			</table>
			<input id="lbltyp" type="hidden" runat="server"> <input id="lblptid" type="hidden" runat="server">
			<input id="lblwo" type="hidden" runat="server"> <input id="lblalttyp" type="hidden" runat="server">
			<input id="lblaltid" type="hidden" runat="server"> <input id="lblaltnum" type="hidden" runat="server">
			<input id="lblrepid" type="hidden" runat="server"> <input id="lblrepnum" type="hidden" runat="server">
			<input id="lblsubmit" type="hidden" runat="server"> <input type="hidden" id="lblpid" runat="server">
			<input type="hidden" id="lbljpid" runat="server"> <input type="hidden" id="lblpmid" runat="server">
			<input type="hidden" id="lblqty" runat="server"> <input type="hidden" id="lblptyp" runat="server">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
