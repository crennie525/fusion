

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class Inventory
    Inherits System.Web.UI.Page
	Protected WithEvents lang2732 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2731 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2730 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2729 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim rowcnt As Integer
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim cid, dept, sql As String
    Dim inv As New Utilities
    Dim dr As SqlDataReader
    Dim Login As String
    Protected WithEvents dgparts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgparts As System.Web.UI.WebControls.Label
    Protected WithEvents partPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents partNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents addpart As System.Web.UI.WebControls.ImageButton
    Protected WithEvents noparthdr As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lbltool As System.Web.UI.WebControls.Label
    Protected WithEvents dgtools As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Image17 As System.Web.UI.WebControls.Image
    Protected WithEvents Image18 As System.Web.UI.WebControls.Image
    Protected WithEvents Image19 As System.Web.UI.WebControls.Image
    Protected WithEvents addtool As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txttname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgtools As System.Web.UI.WebControls.Label
    Protected WithEvents toolPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents toolNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents notoolhdr As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lbllube As System.Web.UI.WebControls.Label
    Protected WithEvents dglube As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Image25 As System.Web.UI.WebControls.Image
    Protected WithEvents Image26 As System.Web.UI.WebControls.Image
    Protected WithEvents Image27 As System.Web.UI.WebControls.Image
    Protected WithEvents addlube As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtlname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtldesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpglubes As System.Web.UI.WebControls.Label
    Protected WithEvents lubePrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lubeNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents nolubehdr As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents iftool As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iflube As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpart As System.Web.UI.WebControls.Label
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbltab As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            Try
                cid = "0"
                lblcid.Value = cid
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try
            If cid <> "" Then
                inv.Open()
                LoadParts(PageNumber)
                inv.Dispose()
                lbltab.Text = "part"
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If

        End If
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("Inventory.aspx")
        Catch ex As Exception

        End Try
        'partPrev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'partPrev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'partNext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'partNext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    Private Sub LoadParts(ByVal PageNumber As Integer)

        Dim Group As String = ""
        cid = lblcid.Value
        Dim sql As String = "select Count(*) from item " '_
        '+ "where compid = '" & cid & "'"
        dgparts.VirtualItemCount = inv.Scalar(sql)
        If dgparts.VirtualItemCount = 0 Then
            Tables = "item"
            PK = "itemid"
            PageSize = "10"
            Fields = "*"
            Filter = "" '"compid = " & cid
            dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgparts.DataSource = dr
            Try
                dgparts.DataBind()
            Catch ex As Exception
                dgparts.CurrentPageIndex = 0
                dgparts.DataBind()
            End Try
            dr.Close()
            inv.Dispose()
            lblpart.Text = "No Parts Inventory Records Found"
            noparthdr.Visible = True
            partPrev.Visible = False
            partNext.Visible = False
        Else

            Tables = "item"
            PK = "itemid"
            PageSize = "10"
            Fields = "*"
            Filter = "" '"compid = " & cid
            dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgparts.DataSource = dr
            Try
                dgparts.DataBind()
            Catch ex As Exception
                dgparts.CurrentPageIndex = 0
                dgparts.DataBind()
            End Try
            dr.Close()
            'inv.Dispose()
            lblpgparts.Text = "Page " & dgparts.CurrentPageIndex + 1 & " of " & dgparts.PageCount
            lblpart.Text = ""
            noparthdr.Visible = False
            partPrev.Visible = True
            partNext.Visible = True
            checkPartPgCnt()
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub checkPartPgCnt()
        If (dgparts.CurrentPageIndex) = 0 Or dgparts.PageCount = 1 Then
            partPrev.Enabled = False
        Else
            partPrev.Enabled = True
        End If
        If dgparts.PageCount > 1 And (dgparts.CurrentPageIndex + 1 < dgparts.PageCount) Then
            partNext.Enabled = True
        Else
            partNext.Enabled = False
        End If
    End Sub
    Private Sub partPrev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles partPrev.Click
        If (dgparts.CurrentPageIndex > 0) Then
            dgparts.CurrentPageIndex = dgparts.CurrentPageIndex - 1
            lbltab.Text = "part"
            inv.Open()
            LoadParts(dgparts.CurrentPageIndex - 1)
            inv.Dispose()
        End If
        checkPartPgCnt()
    End Sub
    Private Sub partNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles partNext.Click
        If ((dgparts.CurrentPageIndex) < (dgparts.PageCount - 1)) Then
            dgparts.CurrentPageIndex = dgparts.CurrentPageIndex + 1
            lbltab.Text = "part"
            inv.Open()
            LoadParts(dgparts.CurrentPageIndex + 1)
            inv.Dispose()
            checkPartPgCnt()
        End If
        checkPartPgCnt()
    End Sub
    Private Sub dgparts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        'Try
        inv.Open()
        cid = lblcid.Value
        Dim id, part, desc, loc As String
        'id = CType(e.Item.Cells(1).Controls(1), TextBox).Text
        id = CType(e.Item.FindControl("lblitemid"), Label).Text
        part = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        part = Replace(part, "'", Chr(180), , , vbTextCompare)

        part = Replace(part, """", Chr(180) & Chr(180), , , vbTextCompare)
        part = Replace(part, "--", "-", , , vbTextCompare)
        part = Replace(part, ";", ":", , , vbTextCompare)

        part = Replace(part, ";", " ", , , vbTextCompare)
        'part = Replace(part, "/", " ", , , vbTextCompare)
        'part = Replace(part, "\", " ", , , vbTextCompare)
        part = Replace(part, "&", " ", , , vbTextCompare)
        part = Replace(part, "%", " ", , , vbTextCompare)
        part = Replace(part, "~", " ", , , vbTextCompare)
        part = Replace(part, "^", " ", , , vbTextCompare)
        part = Replace(part, "*", " ", , , vbTextCompare)
        part = Replace(part, "$", " ", , , vbTextCompare)

        desc = CType(e.Item.Cells(3).Controls(1), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)

        desc = Replace(desc, """", Chr(180) & Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)

        desc = Replace(desc, ";", " ", , , vbTextCompare)
        'desc = Replace(desc, "/", " ", , , vbTextCompare)
        'desc = Replace(desc, "\", " ", , , vbTextCompare)
        desc = Replace(desc, "&", " ", , , vbTextCompare)
        desc = Replace(desc, "%", " ", , , vbTextCompare)
        desc = Replace(desc, "~", " ", , , vbTextCompare)
        desc = Replace(desc, "^", " ", , , vbTextCompare)
        desc = Replace(desc, "*", " ", , , vbTextCompare)
        desc = Replace(desc, "$", " ", , , vbTextCompare)

        If Len(desc) = 0 Then desc = ""
        If Len(desc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1202" , "Inventory.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        loc = CType(e.Item.Cells(4).Controls(1), TextBox).Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)

        loc = Replace(loc, """", Chr(180) & Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)

        loc = Replace(loc, ";", " ", , , vbTextCompare)
        loc = Replace(loc, "/", " ", , , vbTextCompare)
        loc = Replace(loc, "\", " ", , , vbTextCompare)
        loc = Replace(loc, "&", " ", , , vbTextCompare)
        loc = Replace(loc, "%", " ", , , vbTextCompare)
        loc = Replace(loc, "~", " ", , , vbTextCompare)
        loc = Replace(loc, "^", " ", , , vbTextCompare)
        loc = Replace(loc, "*", " ", , , vbTextCompare)
        loc = Replace(loc, "$", " ", , , vbTextCompare)

        If Len(loc) = 0 Then loc = ""
        If Len(loc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1203" , "Inventory.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(part) <= 50 OrElse Len(part) <> 0 OrElse part <> "" Then
            'Dim old As String = lblold.Value
            Dim icnt As Integer
            sql = "update item set description = " _
                   + "'" & desc & "', itemnum = '" & part & "', location = '" & loc & "' " _
                   + "where itemid = '" & id & "'"
            'sql = "usp_updateinv 'part','" & id & "','" & part & "','" & desc & "','" & old & "','" & loc & "', '', 'oinv'"
            inv.Update(sql)
            If icnt = 0 Then
                'PageNumber = txtpg.Value
                dgparts.EditItemIndex = -1
                LoadParts(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1204" , "Inventory.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            'dgparts.EditItemIndex = -1
            'lbltab.Text = "part"
            'LoadParts(PageNumber)
        Else
            If Len(part) > 50 Then
                If Len(part) = 0 OrElse part = "" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1205" , "Inventory.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1206" , "Inventory.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If
        inv.Dispose()
       
    End Sub
    Private Sub dgparts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand
        PageNumber = dgparts.CurrentPageIndex + 1
        dgparts.EditItemIndex = e.Item.ItemIndex
        lbltab.Text = "part"
        inv.Open()
        LoadParts(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub dgparts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        dgparts.EditItemIndex = -1
        lbltab.Text = "part"
        inv.Open()
        LoadParts(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub dgparts_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.DeleteCommand
        inv.Open()
        Dim id As String
        'id = CType(e.Item.Cells(1).Controls(1), Label).Text
        Try
            id = CType(e.Item.FindControl("lblitemid"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblitemidn"), Label).Text
        End Try

        sql = "delete from item where itemid = '" & id & "'"
        inv.Update(sql)
        dgparts.EditItemIndex = -1
        lbltab.Text = "part"
        cid = lblcid.Value

        sql = "select Count(*) from item " _
        + "where compid = '" & cid & "'"
        PageNumber = inv.PageCount(sql, PageSize)

        sql = "update AdminTasks " _
           + "set parts = '" & PageNumber & "' where cid = '" & cid & "'"
        inv.Update(sql)

        If dgparts.CurrentPageIndex > PageNumber Then
            dgparts.CurrentPageIndex = PageNumber - 1
        End If
        If dgparts.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgparts.CurrentPageIndex + 1
        End If
        dgparts.EditItemIndex = -1
        LoadParts(PageNumber) 'dgparts.CurrentPageIndex
        inv.Dispose()
    End Sub
    
   
    Private Sub dgparts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.ItemCommand

        If e.CommandName = "Add" Then
            Dim num, des, loc As String
            Dim nt, dt, lt As TextBox
            'lnamei = CType(e.Item.FindControl("txtnewstat"), TextBox)
            'lname = lnamei.Text
            nt = CType(e.Item.FindControl("txtpname"), TextBox)
            dt = CType(e.Item.FindControl("txtpdesc"), TextBox)
            lt = CType(e.Item.FindControl("txtploc"), TextBox)
            num = CType(e.Item.FindControl("txtpname"), TextBox).Text
            num = Replace(num, "'", Chr(180), , , vbTextCompare)

            num = Replace(num, """", Chr(180) & Chr(180), , , vbTextCompare)
            num = Replace(num, "--", "-", , , vbTextCompare)
            num = Replace(num, ";", ":", , , vbTextCompare)

            num = Replace(num, ";", " ", , , vbTextCompare)
            'num = Replace(num, "/", " ", , , vbTextCompare)
            'num = Replace(num, "\", " ", , , vbTextCompare)
            num = Replace(num, "&", " ", , , vbTextCompare)
            num = Replace(num, "%", " ", , , vbTextCompare)
            num = Replace(num, "~", " ", , , vbTextCompare)
            num = Replace(num, "^", " ", , , vbTextCompare)
            num = Replace(num, "*", " ", , , vbTextCompare)
            num = Replace(num, "$", " ", , , vbTextCompare)

            des = CType(e.Item.FindControl("txtpdesc"), TextBox).Text
            des = Replace(des, "'", Chr(180), , , vbTextCompare)

            des = Replace(des, """", Chr(180) & Chr(180), , , vbTextCompare)
            des = Replace(des, "--", "-", , , vbTextCompare)
            des = Replace(des, ";", ":", , , vbTextCompare)

            des = Replace(des, ";", " ", , , vbTextCompare)
            'des = Replace(des, "/", " ", , , vbTextCompare)
            'des = Replace(des, "\", " ", , , vbTextCompare)
            des = Replace(des, "&", " ", , , vbTextCompare)
            des = Replace(des, "%", " ", , , vbTextCompare)
            des = Replace(des, "~", " ", , , vbTextCompare)
            des = Replace(des, "^", " ", , , vbTextCompare)
            des = Replace(des, "*", " ", , , vbTextCompare)
            des = Replace(des, "$", " ", , , vbTextCompare)

            If Len(des) = 0 Then des = ""
            If Len(des) > 100 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1207" , "Inventory.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            loc = CType(e.Item.FindControl("txtploc"), TextBox).Text
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)

            loc = Replace(loc, """", Chr(180) & Chr(180), , , vbTextCompare)
            loc = Replace(loc, "--", "-", , , vbTextCompare)
            loc = Replace(loc, ";", ":", , , vbTextCompare)

            loc = Replace(loc, ";", " ", , , vbTextCompare)
            loc = Replace(loc, "/", " ", , , vbTextCompare)
            loc = Replace(loc, "\", " ", , , vbTextCompare)
            loc = Replace(loc, "&", " ", , , vbTextCompare)
            loc = Replace(loc, "%", " ", , , vbTextCompare)
            loc = Replace(loc, "~", " ", , , vbTextCompare)
            loc = Replace(loc, "^", " ", , , vbTextCompare)
            loc = Replace(loc, "*", " ", , , vbTextCompare)
            loc = Replace(loc, "$", " ", , , vbTextCompare)

            If Len(loc) = 0 Then loc = ""
            If Len(loc) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1208" , "Inventory.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            If Len(num) <= 30 OrElse Len(num) <> 0 OrElse num <> "" Then
                inv.Open()
                cid = lblcid.Value
                sql = "insert into item " _
                + "(compid, itemnum, description, location) values " _
                + "('" & cid & "', '" & num & "', '" & des & "', '" & loc & "')"
                sql = "usp_insertinv 'part', '" & num & "','" & des & "','" & loc & "','0','oinv'"
                inv.Update(sql)
                Dim statcnt As Integer = dgparts.VirtualItemCount + 1
                sql = "select Count(*) from item " _
                                + "where compid = '" & cid & "'"
                PageNumber = inv.PageCount(sql, PageSize)
                LoadParts(PageNumber)
                inv.Dispose()
            Else
                If Len(num) > 50 Then
                    If Len(num) = 0 OrElse num = "" Then
                        Dim strMessage As String =  tmod.getmsg("cdstr1209" , "Inventory.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr1210" , "Inventory.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                End If
            End If



        End If
    End Sub

    Private Sub dgparts_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparts.ItemCreated

    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgparts.Columns(0).HeaderText = dlabs.GetDGPage("Inventory.aspx","dgparts","0")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(2).HeaderText = dlabs.GetDGPage("Inventory.aspx","dgparts","2")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(3).HeaderText = dlabs.GetDGPage("Inventory.aspx","dgparts","3")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(4).HeaderText = dlabs.GetDGPage("Inventory.aspx","dgparts","4")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(5).HeaderText = dlabs.GetDGPage("Inventory.aspx","dgparts","5")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2729.Text = axlabs.GetASPXPage("Inventory.aspx","lang2729")
		Catch ex As Exception
		End Try
		Try
			lang2730.Text = axlabs.GetASPXPage("Inventory.aspx","lang2730")
		Catch ex As Exception
		End Try
		Try
			lang2731.Text = axlabs.GetASPXPage("Inventory.aspx","lang2731")
		Catch ex As Exception
		End Try
		Try
			lang2732.Text = axlabs.GetASPXPage("Inventory.aspx","lang2732")
		Catch ex As Exception
		End Try
		Try
			lbltab.Text = axlabs.GetASPXPage("Inventory.aspx","lbltab")
		Catch ex As Exception
		End Try

	End Sub

End Class
