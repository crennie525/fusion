

'********************************************************
'*
'********************************************************



Public Class invmainmenu
    Inherits System.Web.UI.Page
	Protected WithEvents lang2745 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2744 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2743 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2742 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ro As String
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifmain As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("invmainmenu.aspx")
        Catch ex As Exception

        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            lblcid.Value = "0"
            ifmain.Attributes.Add("src", "partsmain.aspx?ro=" & ro)
            If ro = "1" Then
                'pgtitle.InnerHtml = "PM Inventory (Read Only)"
            Else
                'pgtitle.InnerHtml = "PM Inventory"
            End If
        End If
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2742.Text = axlabs.GetASPXPage("invmainmenu.aspx","lang2742")
		Catch ex As Exception
		End Try
		Try
			lang2743.Text = axlabs.GetASPXPage("invmainmenu.aspx","lang2743")
		Catch ex As Exception
		End Try
		Try
			lang2744.Text = axlabs.GetASPXPage("invmainmenu.aspx","lang2744")
		Catch ex As Exception
		End Try
		Try
			lang2745.Text = axlabs.GetASPXPage("invmainmenu.aspx","lang2745")
		Catch ex As Exception
		End Try

	End Sub

End Class
