

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class purchreqprint
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim pms As New Utilities
    Dim pr As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdwi As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            pr = Request.QueryString("pr").ToString
            pms.Open()
            PopReport(pr)
            pms.Dispose()
        End If
    End Sub
    Private Sub PopReport(ByVal pr As String)

        Dim sb As New System.Text.StringBuilder
        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""650"">")
        sb.Append("<tr><td width=""150""></td><td width=""170""></td><td width=""150""></td><td width=""170""></td>")

        sb.Append("<tr height=""30""><td class=""bigbold"" colspan=""4"" align=""center"">" & tmod.getlbl("cdlbl523" , "purchreqprint.aspx.vb") & "</td></tr>")
        sb.Append("<tr height=""20""><td class=""plainlabel"" colspan=""4"" align=""center"">" & Now & "</td></tr>")
        sb.Append("<tr><td class=""bigbold"" colspan=""6"">&nbsp;</td></tr>")

        sql = "select r.*, i.catcode, i.model, i.mfg " _
        + "from invpr r left join inventory i on i.itemid = r.itemid where r.prnum = '" & pr & "'"

        dr = pms.GetRdrData(sql)
        Dim headhold As String = "0"
        Dim item, desc, svend, stat, shipto, reqby, reqdt, cont, attn, ounit, oqty, icost, tcost, po, odate As String
        Dim pvend, aicost, atcost, ordate, rdate, reqdate, mfg, cat, model As String
        While dr.Read
            pr = dr.Item("prnum").ToString
            item = dr.Item("itemnum").ToString
            desc = dr.Item("itemdesc").ToString
            stat = dr.Item("status").ToString
            svend = dr.Item("vendor").ToString
            shipto = dr.Item("shipto").ToString
            reqby = dr.Item("requestedby").ToString
            reqdt = dr.Item("requireddate").ToString
            cont = dr.Item("contact").ToString
            attn = dr.Item("shiptoattn").ToString
            ounit = dr.Item("orderunit").ToString
            oqty = dr.Item("orderqty").ToString
            icost = dr.Item("itemcost").ToString
            tcost = dr.Item("totalcost").ToString
            reqdate = dr.Item("issuedate").ToString
            mfg = dr.Item("mfg").ToString
            cat = dr.Item("catcode").ToString
            model = dr.Item("model").ToString
            
            
            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl524" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & pr & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl525" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & reqdate & "</td></tr>")
            sb.Append("<tr><td class=""bigbold"" colspan=""6""><hr></td></tr>")

            sb.Append("<tr height=""24""><td class=""label"">" & tmod.getlbl("cdlbl526" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & item & "</td></tr>")
            sb.Append("<tr height=""24""><td class=""label"">" & tmod.getlbl("cdlbl527" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & desc & "</td></tr>")
            sb.Append("<tr height=""24""><td class=""label"">" & tmod.getlbl("cdlbl528" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & mfg & "</td></tr>")
            sb.Append("<tr height=""24""><td class=""label"">" & tmod.getlbl("cdlbl529" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & cat & "</td></tr>")
            sb.Append("<tr height=""24""><td class=""label"">" & tmod.getlbl("cdlbl530" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & model & "</td></tr>")


            sb.Append("<tr><td colspan=""4""><table>")
            sb.Append("<tr height=""20""><td class=""label"" width=""120""><u>Order Unit</u></td>")
            sb.Append("<td class=""label"" width=""120""><u>Order Qty</u></td>")
            sb.Append("<td class=""label"" width=""120""><u>Item Cost</u></td>")
            sb.Append("<td class=""label"" width=""120""><u>Total Cost</u></td></tr>")

            sb.Append("<tr height=""20""><td class=""plainlabel"">" & ounit & "</td>")
            sb.Append("<td class=""plainlabel"">" & oqty & "</td>")
            sb.Append("<td class=""plainlabel"">" & icost & "</td>")
            sb.Append("<td class=""plainlabel"">" & tcost & "</td></tr>")

            sb.Append("</table></td></tr>")

            sb.Append("<tr><td class=""bigbold"" colspan=""6"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold"" colspan=""6""><hr></td></tr>")

            sb.Append("<tr height=""20""><td class=""label"" colspan=""2""><u>Suggested Vendor</u></td>")
            sb.Append("<td class=""label"" colspan=""2""><u>Ship To</u></td></tr>")

            If svend = "" Then
                svend = "Not Provided"
            End If
            If shipto = "" Then
                shipto = "Not Provided"
            End If
            sb.Append("<tr height=""20""><td class=""plainlabel"" colspan=""2"">" & svend & "</td>")
            sb.Append("<td class=""plainlabel"" colspan=""2"">" & shipto & "</td></tr>")

            If cont = "" Then
                cont = "Not Provided"
            End If
            If attn = "" Then
                attn = "Not Provided"
            End If
            sb.Append("<tr height=""20""><td class=""label"">" & tmod.getlbl("cdlbl531" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & cont & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl532" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & attn & "</td></tr>")

            sb.Append("<tr><td class=""bigbold"" colspan=""6"">&nbsp;</td></tr>")
            sb.Append("<tr><td class=""bigbold"" colspan=""6""><hr></td></tr>")

            sb.Append("<tr><td class=""bigbold"" colspan=""6"">" & tmod.getlbl("cdlbl533" , "purchreqprint.aspx.vb") & "</td></tr>")

            po = dr.Item("ponum").ToString
            odate = dr.Item("orderdate").ToString
            rdate = dr.Item("recdate").ToString
            aicost = dr.Item("actcost").ToString
            atcost = dr.Item("acttotal").ToString
            pvend = dr.Item("povendor").ToString

            sb.Append("<tr height=""28""><td class=""label"">" & tmod.getlbl("cdlbl534" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & pvend & "</td></tr>")
            sb.Append("<tr height=""28""><td class=""label"">" & tmod.getlbl("cdlbl535" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & po & "</td>")
            sb.Append("<td class=""label"">" & tmod.getlbl("cdlbl536" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & odate & "</td></tr>")
            sb.Append("<tr height=""28""><td class=""label"">" & tmod.getlbl("cdlbl537" , "purchreqprint.aspx.vb") & "</td>")
            sb.Append("<td class=""plainlabel"">" & aicost & "</td></tr>")
            'sb.Append("<tr height=""28""><td class=""label"">" & tmod.getlbl("cdlbl538" , "purchreqprint.aspx.vb") & "</td>")
            'sb.Append("<td class=""plainlabel"">" & rdate & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        tdwi.InnerHtml = sb.ToString

    End Sub
End Class
