

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class lotlook
    Inherits System.Web.UI.Page
	Protected WithEvents lang2771 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim lot As New Utilities
    Dim itemid, store As String
    Protected WithEvents lblstore As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdlot As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            itemid = Request.QueryString("itemid").ToString
            lblitemid.Value = itemid
            store = Request.QueryString("store").ToString
            lblstore.Value = store
            lot.Open()
            GetLot()
            lot.Dispose()

        End If
    End Sub
    Private Sub GetLot()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table width=""800"" cellpadding=""0"">")

        sb.Append("<tr><td colspan=""11"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 320px;  HEIGHT: 200px"">")
        sb.Append("<table width=""300"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""100"">" & tmod.getlbl("cdlbl508" , "lotlook.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">Qty</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl510" , "lotlook.aspx.vb") & "</td></tr>")

        itemid = lblitemid.Value
        store = lblstore.Value

        sql = "select l.* from invlot l where l.itemid = '" & itemid & "' " _
        + "left join invbalances i on i.lotnum = l.lotnum and i.location = '" & store & "'"
        Dim cnt As Integer = 0
        Dim lotnum, qty, exp As String
        dr = lot.GetRdrData(sql)

        While dr.Read
            cnt += 1
            lotnum = dr.Item("lotnum").ToString
            qty = dr.Item("curbal").ToString
            exp = dr.Item("useby").ToString

            sb.Append("<tr><td class=""plainlabel grayborder"" ><a href=""#"" class=""A1"" onclick=""getlot('" & lotnum & "');"">" & lotnum & "</a></td>")
            sb.Append("<td class=""plainlabel grayborder"">" & qty & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & exp & "</td></tr>")

        End While
        dr.Close()
        If cnt = 0 Then
            sb.Append("<tr><td class=""plainlabelred"">" & tmod.getlbl("cdlbl511" , "lotlook.aspx.vb") & "</td></tr>")
        End If
        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdlot.InnerHtml = sb.ToString

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2771.Text = axlabs.GetASPXPage("lotlook.aspx","lang2771")
		Catch ex As Exception
		End Try

	End Sub

End Class
