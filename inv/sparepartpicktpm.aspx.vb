

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class sparepartpicktpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang2939 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2938 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2937 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2936 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2935 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim iPageNumber As Integer = 1
    Dim oPageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim cid, cnm, typ, ptid, tnum, origflag As String
    Dim sort As String = "itemnum"
    Dim sql As String
    Dim dr As SqlDataReader
    Dim parts As New Utilities
    Dim eqid, eqnum, ro As String
    Dim decPgNav As Decimal
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtask As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents dgparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblnpg As System.Web.UI.WebControls.Label
    Protected WithEvents btnradd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtnpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgtaskparts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents txtipgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblipg As System.Web.UI.WebControls.Label
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img7 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img8 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgoparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents btnoadd As System.Web.UI.WebControls.ImageButton
    Protected WithEvents odiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents onav As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents osep As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents olab As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtopgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtopg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ospdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim intPgNav As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            'parts.Open()
            eqid = Request.QueryString("eqid").ToString '"266" '
            lbleqid.Value = eqid

            origflag = Request.QueryString("oflag").ToString '"o" '
            lbloflag.Value = origflag
            'typ = "na" 'Request.QueryString("typ").ToString
            'lbltyp.Value = typ
            'ptid = "1" 'Request.QueryString("typ").ToString
            'lblptid.Value = ptid

            'PopTasks(iPageNumber)


            'parts.Dispose()
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                If ro = "1" Then
                    dgparttasks.Columns(0).Visible = False
                    dgparttasks.Columns(7).Visible = False
                    dgoparttasks.Columns(0).Visible = False
                    dgoparttasks.Columns(7).Visible = False
                    'ibtnaddtolist.Enabled = False
                    'ibtnaddtolist.ImageUrl = "../images/appbuttons/minibuttons/savedisk1dis.gif"
                End If
                parts.Open()
                GetEq(eqid)
                PopSpare(PageNumber)
                ptid = Request.QueryString("ptid").ToString '"1" '
                lblptid.Value = ptid
                Try
                    typ = Request.QueryString("typ").ToString '"na" '
                Catch ex As Exception
                    typ = "no"
                End Try
                lbltyp.Value = typ
                If typ = "jp" Or typ = "wjp" Then
                    lbljpnum.Value = Request.QueryString("jp").ToString
                    lblwo.Value = Request.QueryString("wo").ToString
                    tnum = Request.QueryString("tnum")
                    tdtask.InnerHtml = tnum
                    cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                    'lblcid.Value = cid
                    PopTasks(PageNumber)
                    lbloflag.Value = "3"
                    'div1.Visible = False
                ElseIf typ = "wo" Then
                    lblwo.Value = Request.QueryString("wo").ToString
                    'tdtask.InnerHtml = "N/A"
                    cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                    'lblcid.Value = cid
                    PopTasks(PageNumber)
                    lbloflag.Value = "3"
                    'div1.Visible = False
                Else
                    If Len(ptid) <> 0 AndAlso ptid <> "" AndAlso ptid <> "0" Then
                        tnum = Request.QueryString("tnum")
                        'tdtask.InnerHtml = tnum
                        cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                        'lblcid.Value = cid
                        PopTasks(iPageNumber)
                        'lbloflag.Value = "3"
                        'div1.Visible = False
                        If origflag = "o" Then
                            olab.Attributes.Add("class", "view")
                            odiv.Attributes.Add("class", "view")
                            onav.Attributes.Add("class", "view")
                            osep.Attributes.Add("class", "view")
                            oPopTasks(oPageNumber)
                        Else
                            olab.Attributes.Add("class", "details")
                            odiv.Attributes.Add("class", "details")
                            onav.Attributes.Add("class", "details")
                            osep.Attributes.Add("class", "details")
                        End If
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr1498" , "sparepartpicktpm.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        'lbllog.Value = "nodeptid"
                    End If
                End If
                parts.Dispose()
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1499" , "sparepartpicktpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'lbllog.Value = "nodeptid"
            End Try
            btnradd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov293" , "sparepartpicktpm.aspx.vb") & "', ABOVE)")
            btnradd.Attributes.Add("onmouseout", "return nd()")
        Else
            If Request.Form("lblret") = "nnext" Then
                parts.Open()
                GetnNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nlast" Then
                parts.Open()
                PageNumber = txtnpgcnt.Value
                txtnpg.Value = PageNumber
                PopSpare(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nprev" Then
                parts.Open()
                GetnPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nfirst" Or Request.Form("lblret") = "spreturn" Then
                parts.Open()
                PageNumber = 1
                txtnpg.Value = PageNumber
                PopSpare(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "inext" Then
                parts.Open()
                GetiNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ilast" Then
                parts.Open()
                iPageNumber = txtipgcnt.Value
                txtipg.Value = iPageNumber
                PopTasks(iPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "iprev" Then
                parts.Open()
                GetiPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ifirst" Then
                parts.Open()
                iPageNumber = 1
                txtipg.Value = iPageNumber
                PopTasks(iPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "addspare" Then
                parts.Open()
                AddSpare()
                If lbloflag.Value = "o" Then
                    oPageNumber = 1
                    txtopg.Value = oPageNumber
                    oPopTasks(oPageNumber)
                End If
                iPageNumber = 1
                txtipg.Value = iPageNumber
                PopTasks(iPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "next" Then
                parts.Open()
                GetNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                parts.Open()
                oPageNumber = txtopgcnt.Value
                txtopg.Value = oPageNumber
                oPopTasks(oPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                parts.Open()
                GetPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                parts.Open()
                oPageNumber = 1
                txtopg.Value = oPageNumber
                oPopTasks(oPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "addspare" Then
                parts.Open()
                oPageNumber = 1
                txtopg.Value = oPageNumber
                oPopTasks(oPageNumber)
                iPageNumber = 1
                txtipg.Value = iPageNumber
                PopTasks(iPageNumber)
                parts.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetEq(ByVal eqid As String)
        'eqnum = Request.QueryString("eqn").ToString '"test entry" '
        sql = "select eqnum from equipment where eqid = '" & eqid & "'"
        eqnum = parts.strScalar(sql)
        tdtask.InnerHtml = eqnum
        lbleqnum.Value = eqnum
    End Sub
    Private Sub AddSpare()
        ptid = lblptid.Value
        typ = lbltyp.Value
        Dim oflag As Integer = 3
        If lbloflag.Value = "o" Then
            'oflag = 1
        Else
            'oflag = 0
        End If
        Dim itemid, itemnum, qty As String
        itemid = lbladdid.Value
        itemnum = lbladdnum.Value
        qty = lbladdqty.Value
        If typ = "wo" Then
            sql = "usp_insertwoparts '" & lblwo.Value & "', '" & itemid & "', " _
            + "'" & itemnum & "', '" & qty & "', '" & oflag & "'"
        ElseIf typ = "jp" Then
            'Dim jpid As String = lbljpnum.Value
            'sql = "usp_insertjpparts '" & jpid & "', '" & ptid & "', '" & idarr(i) & "', " _
            '+ "'" & partarr(i) & "', '" & Int() & "', '" & oflag & "','" & wo & "'"
        ElseIf typ = "wjp" Then
            'Dim jpid As String = lbljpnum.Value
            'sql = "usp_insertwjpparts '" & jpid & "', '" & ptid & "', '" & idarr(i) & "', " _
            '+ "'" & partarr(i) & "', '" & Int() & "', '" & oflag & "','" & wo & "'"
        Else
            sql = "usp_insertTaskPartstpm '" & ptid & "', '" & itemid & "', " _
            + "'" & itemnum & "', '" & qty & "', '" & oflag & "'"
        End If
        parts.Update(sql)
    End Sub
    Private Sub PopSpare(ByVal PageNumber As Integer)
        txtnpg.Value = PageNumber
        eqid = lbleqid.Value
        Dim intPgCnt As Integer
        sql = "select count(*) from invsparepart where eqid = '" & eqid & "'"
        Tables = "invsparepart"
        PK = "spid"
        Filter = "eqid = " & eqid
        Fields = "*"
        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblnpg.Text = "Page 0 of 0"
        Else
            lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtnpgcnt.Value = intPgNav

        dr = parts.GetPage(Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group)
        dgparttasks.DataSource = dr
        dgparttasks.DataBind()
        dr.Close()
        txtnpg.Value = PageNumber
        'txtnpgcnt.Value = intPgNav
        'lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
    End Sub
    Private Sub GetnNext()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg + 1
            txtnpg.Value = PageNumber
            PopSpare(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1500" , "sparepartpicktpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetnPrev()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg - 1
            txtnpg.Value = PageNumber
            PopSpare(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1501" , "sparepartpicktpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgparttasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgparttasks.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lb As HtmlAnchor
            lb = CType(e.Item.FindControl("lbv"), HtmlAnchor)
            Dim mid, mi, md As String
            mid = CType(e.Item.FindControl("Label1"), Label).Text
            mi = CType(e.Item.FindControl("lblsitemid"), Label).Text
            md = CType(e.Item.FindControl("lblsqty1"), Label).Text
            lb.Attributes.Add("onclick", "getmat('" & mid & "','" & mi & "','" & md & "')")
        End If
    End Sub
    '*** Original
    Private Sub oPopTasks(ByVal oPageNumber As Integer)
        txtopg.Value = oPageNumber
        ptid = lblptid.Value
        'parts.Open()
        'Get Count
        Dim intPgCnt As Integer
        sql = "select count(*) from pmoTaskPartstpm where pmtskid = '" & ptid & "' and sparepart = 'Y'"
        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtopgcnt.Value = intPgNav

        'Get Page
        Tables = "pmoTaskPartstpm"
        PK = "tskpartid"
        Filter = "pmtskid = " & ptid & " and sparepart = ''Y''"
        Dim dr As SqlDataReader
        dr = parts.GetPage(Tables, PK, sort, oPageNumber, PageSize, Fields, Filter, Group)
        dgoparttasks.DataSource = dr
        dgoparttasks.DataBind()
        dr.Close()
        'parts.Dispose()
        txtopg.Value = oPageNumber
        'txtopgcnt.Value = intPgNav
        'lblpg.Text = "Page " & oPageNumber & " of " & intPgNav
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtopg.Value
            oPageNumber = pg + 1
            txtopg.Value = oPageNumber
            oPopTasks(oPageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1502" , "sparepartpicktpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtopg.Value
            oPageNumber = pg - 1
            txtopg.Value = oPageNumber
            oPopTasks(oPageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1503" , "sparepartpicktpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgoparttasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgoparttasks.DeleteCommand
        parts.Open()
        ptid = lblptid.Value
        Dim id As String 'lbltskprtid
        'id = CType(e.Item.Cells(1).Controls(1), Label).Text
        Try
            id = CType(e.Item.FindControl("lbltskprtid"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbltskprtide"), Label).Text
        End Try

        sql = "delete from pmotaskpartstpm where tskpartid = '" & id & "' " _
        + "delete from pmtaskpartstpm where tskpartid = '" & id & "'"
        parts.Update(sql)

        dgoparttasks.EditItemIndex = -1
        sql = "select Count(*) from pmotaskpartstpm " _
               + "where pmtskid = '" & ptid & "'"
        oPageNumber = parts.PageCount(sql, PageSize)
        oPopTasks(oPageNumber)
        PageNumber = txtopg.Value
        PopTasks(oPageNumber)
        parts.Dispose()
    End Sub
    Private Sub dgoparttasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgoparttasks.EditCommand
        dgoparttasks.EditItemIndex = e.Item.ItemIndex
        oPageNumber = txtopg.Value
        parts.Open()
        oPopTasks(oPageNumber)
        parts.Dispose()
    End Sub
    Private Sub dgoparttasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgoparttasks.UpdateCommand
        Dim qty As String = CType(e.Item.FindControl("dgoqty"), TextBox).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt32(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1504" , "sparepartpicktpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim id As String = CType(e.Item.FindControl("lbltskprtide"), Label).Text
        sql = "update pmotaskpartstpm set qty = '" & qty & "' where tskpartid = '" & id & "' " _
        + "update pmtaskpartstpm set qty = '" & qty & "' where tskpartid = '" & id & "'"
        parts.Open()
        parts.Update(sql)

        dgoparttasks.EditItemIndex = -1
        oPageNumber = txtopg.Value
        oPopTasks(oPageNumber)
        iPageNumber = txtipg.Value
        PopTasks(iPageNumber)
        parts.Dispose()
    End Sub
    Private Sub dgoparttasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgoparttasks.CancelCommand
        parts.Open()
        dgoparttasks.EditItemIndex = -1
        oPageNumber = txtopg.Value
        oPopTasks(oPageNumber)

        parts.Dispose()
    End Sub
    '*********************
    Private Sub PopTasks(ByVal PageNumber As Integer)
        txtipg.Value = PageNumber
        ptid = lblptid.Value
        typ = lbltyp.Value
        'parts.Open()
        'Get Count
        Dim intPgCnt As Integer
        If typ = "wo" Then
            sql = "select count(*) from woparts where wonum = '" & lblwo.Value & "' and sparepart = 'Y'"
            Tables = "woparts"
            PK = "wopartid"
            Fields = "*, wopartid as lineid"
            Filter = "wonum = " & lblwo.Value & " and sparepart = ''Y''"
        ElseIf typ = "jp" Then
            sql = "select count(*) from jpparts where pmtskid = '" & ptid & "'"
            Tables = "jpparts"
            PK = "tskpartid"
            Fields = "*, tskpartid as lineid"
            Filter = "pmtskid = " & ptid
        ElseIf typ = "wjp" Then
            sql = "select count(*) from wojpparts where wojtid = '" & ptid & "'"
            Tables = "wojpparts"
            PK = "wotskpartid"
            Fields = "*, wotskpartid as lineid"
            Filter = "wojtid = " & ptid
        Else
            sql = "select count(*) from pmTaskPartstpm where pmtskid = '" & ptid & "' and sparepart = 'Y'"
            Tables = "pmTaskPartstpm"
            PK = "tskpartid"
            Fields = "*, tskpartid as lineid"
            Filter = "pmtskid = " & ptid & " and sparepart = ''Y''"
        End If
        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblipg.Text = "Page 0 of 0"
        Else
            lblipg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtipgcnt.Value = intPgNav

        dr = parts.GetPage(Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group)
        dgtaskparts.DataSource = dr
        dgtaskparts.DataBind()
        dr.Close()
        txtipg.Value = PageNumber
        'txtipgcnt.Value = intPgNav
        'lblipg.Text = "Page " & PageNumber & " of " & intPgNav
    End Sub
    Private Sub GetiNext()
        Try
            Dim pg As Integer = txtipg.Value
            iPageNumber = pg + 1
            txtipg.Value = iPageNumber
            PopTasks(iPageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1505" , "sparepartpicktpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetiPrev()
        Try
            Dim pg As Integer = txtipg.Value
            iPageNumber = pg - 1
            txtipg.Value = iPageNumber
            PopTasks(iPageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1506" , "sparepartpicktpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgtaskparts_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtaskparts.DeleteCommand
        parts.Open()
        ptid = lblptid.Value
        typ = lbltyp.Value
        Dim wo As String = lblwo.Value
        Dim jpid As String = lbljpnum.Value
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lbltskprtidr"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbltskprtidre"), Label).Text
        End Try
        If typ = "wo" Then
            sql = "delete from woparts where wopartid = '" & id & "' and wonum = '" & wo & "'; " _
            + "update workorder set estmatcost = (select sum(total) from woparts where wonum = '" & wo & "') " _
            + "where wonum = '" & wo & "'"
            parts.Update(sql)
            sql = "usp_delwopart '" & lblwo.Value & "'"
            parts.Update(sql)
            dgparttasks.EditItemIndex = -1
            sql = "select Count(*) from woparts " _
                   + "where wonum = '" & lblwo.Value & "'"
        ElseIf typ = "jp" Then
            sql = "delete from jpparts where tskpartid = '" & id & "'"
            parts.Update(sql)
            If wo <> "" Then
                sql = "delete from wojpparts where tskpartid = '" & id & "' and wonum = '" & wo & "'; " _
                + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wo & "' and " _
                + "jpid = '" & jpid & "') where wonum = '" & wo & "'"
                parts.Update(sql)
            End If
            sql = "usp_deljppart '" & ptid & "','" & wo & "','" & jpid & "'"
            parts.Update(sql)
            dgparttasks.EditItemIndex = -1
            sql = "select Count(*) from jpparts " _
                   + "where pmtskid = '" & ptid & "'"
        ElseIf typ = "wjp" Then
            sql = "delete from wojpparts where wotskpartid = '" & id & "' and wonum = '" & wo & "'; " _
            + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wo & "' and " _
            + "jpid = '" & jpid & "') where wonum = '" & wo & "'"
            parts.Update(sql)
            sql = "usp_delwjppart '" & ptid & "','" & wo & "','" & jpid & "'"
            parts.Update(sql)
            dgparttasks.EditItemIndex = -1
            sql = "select Count(*) from wojpparts " _
                   + "where wojtid = '" & ptid & "'"
        Else
            sql = "delete from pmtaskpartstpm where tskpartid = '" & id & "'"
            parts.Update(sql)
            sql = "usp_delTaskPart '" & ptid & "'"
            parts.Update(sql)
            dgparttasks.EditItemIndex = -1
            sql = "select Count(*) from pmtaskpartstpm " _
                   + "where pmtskid = '" & ptid & "' and sparepart = 'Y'"
        End If
        iPageNumber = parts.PageCount(sql, PageSize)
        PopTasks(iPageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgtaskparts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtaskparts.EditCommand
        dgtaskparts.EditItemIndex = e.Item.ItemIndex
        iPageNumber = txtipg.Value
        parts.Open()
        PopTasks(iPageNumber)
        parts.Dispose()
    End Sub

    Private Sub dgtaskparts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtaskparts.UpdateCommand
        Dim qty As String = CType(e.Item.FindControl("dgqty"), TextBox).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt32(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1507" , "sparepartpicktpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim id As String = CType(e.Item.FindControl("lbltskprtidre"), Label).Text
        ptid = lblptid.Value
        typ = lbltyp.Value
        Dim jpid As String = lbljpnum.Value
        Dim wo As String = lblwo.Value
        If typ = "wo" Then
            sql = "update woparts set qty = '" & qty & "', total = qty * cost where wopartid = '" & id & "'; " _
            + "update workorder set estmatcost = (select sum(total) from woparts where wonum = '" & wo & "') " _
            + "where wonum = '" & wo & "'"
            parts.Open()
            parts.Update(sql)
            sql = "usp_delwopart '" & lblwo.Value & "'"
            parts.Update(sql)
        ElseIf typ = "jp" Then
            sql = "update jpparts set qty = '" & qty & "', total = qty * cost where tskpartid = '" & id & "'"
            parts.Open()
            If wo <> "" Then
                sql = "update wojpparts set qty = '" & qty & "', total = qty * cost where tskpartid = '" & id & "' and wonum = '" & wo & "'; " _
                + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wo & "' and " _
                + "jpid = '" & jpid & "') where wonum = '" & wo & "'"
                parts.Open()
            End If
            parts.Update(sql)
            sql = "usp_deljppart '" & ptid & "','" & wo & "','" & jpid & "'"
            parts.Update(sql)
        ElseIf typ = "wjp" Then
            sql = "update wojpparts set qty = '" & qty & "', total = qty * cost where wotskpartid = '" & id & "' and wonum = '" & wo & "'; " _
            + "update workorder set estjpmatcost = (select sum(total) from wojpparts where wonum = '" & wo & "' and " _
            + "jpid = '" & jpid & "') where wonum = '" & wo & "'"
            parts.Open()
            parts.Update(sql)
            sql = "usp_delwjppart '" & ptid & "','" & wo & "','" & jpid & "'"
            parts.Update(sql)
        Else
            sql = "update pmtaskpartstpm set qty = '" & qty & "', total = qty * cost where tskpartid = '" & id & "'"
            parts.Open()
            parts.Update(sql)
            sql = "usp_delTaskParttpm '" & ptid & "'"
            parts.Update(sql)
        End If
        dgtaskparts.EditItemIndex = -1
        iPageNumber = txtipg.Value
        PopTasks(iPageNumber)
        parts.Dispose()
    End Sub





	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgoparttasks.Columns(0).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgoparttasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(1).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgoparttasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(3).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgoparttasks", "3")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(5).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgoparttasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(6).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgoparttasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(7).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgoparttasks", "7")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(8).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgoparttasks", "8")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(9).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgoparttasks", "9")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(1).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(5).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(7).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "7")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(10).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "10")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(11).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "11")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(13).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "13")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(15).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "15")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(16).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "16")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(17).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "17")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(18).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "18")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(19).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgparttasks", "19")
        Catch ex As Exception
        End Try
        Try
            dgtaskparts.Columns(0).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgtaskparts", "0")
        Catch ex As Exception
        End Try
        Try
            dgtaskparts.Columns(1).HeaderText = dlabs.GetDGPage("sparepartpicktpm.aspx", "dgtaskparts", "1")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2935.Text = axlabs.GetASPXPage("sparepartpicktpm.aspx", "lang2935")
        Catch ex As Exception
        End Try
        Try
            lang2936.Text = axlabs.GetASPXPage("sparepartpicktpm.aspx", "lang2936")
        Catch ex As Exception
        End Try
        Try
            lang2937.Text = axlabs.GetASPXPage("sparepartpicktpm.aspx", "lang2937")
        Catch ex As Exception
        End Try
        Try
            lang2938.Text = axlabs.GetASPXPage("sparepartpicktpm.aspx", "lang2938")
        Catch ex As Exception
        End Try
        Try
            lang2939.Text = axlabs.GetASPXPage("sparepartpicktpm.aspx", "lang2939")
        Catch ex As Exception
        End Try

    End Sub
    Private Sub dgoparttasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgoparttasks.ItemDataBound
        'Imagebutton10
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("Imagebutton10"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this record?')"
        End If
    End Sub
    Private Sub dgtaskparts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtaskparts.ItemDataBound
        'Imagebutton8
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("Imagebutton8"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this record?')"
        End If
    End Sub
End Class
