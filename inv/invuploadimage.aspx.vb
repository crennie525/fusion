﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports System.Random
Public Class invuploadimage
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql, itemid As String
    Dim news As New Utilities
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            itemid = Request.QueryString("itemid").ToString '"1073" '
            lblitemid.Value = itemid
          
           
        End If
    End Sub
    Private Sub btnupload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.ServerClick
        'Dim str As String
        'str = "ok"
        AddPic()
    End Sub
    Private Sub AddPic()
        If Not (MyFile.PostedFile Is Nothing) Then
            'Check to make sure we actually have a file to upload
            Dim strLongFilePath As String = MyFile.PostedFile.FileName
            Dim intFileNameLength As Integer = InStr(1, StrReverse(strLongFilePath), "\")
            Dim strFileName As String = Mid(strLongFilePath, (Len(strLongFilePath) - intFileNameLength) + 2)
            'Dim uid As String = lblblockid.Value
            Dim ptitle As String
            ptitle = "" 'txtpictitle.Text '= dr.Item("news_image_title").ToString
            If Len(ptitle) > 250 Then
                Dim strMessage As String = tmod.getmsg("cdstr1596", "pmuploadimage1.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

            Dim icnt As Integer
            itemid = lblitemid.Value


            news.Open()
            sql = "select count(*) from invimages where invid = '" & itemid & "'"
            icnt = news.Scalar(sql)
            If icnt > 0 Then
                Dim strMessage As String = "Please Delete the Current Image Before Selecting a New Image for this Item"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            icnt += 1
            Dim deli As Integer
            Dim random As New Random
            deli = random.Next(1000)

            Dim newstr As String = "a-newsImg" & itemid & "-" & deli & ".jpg"
            Dim thumbstr As String = "atn-newsImg" & itemid & "-" & deli & ".jpg"
            Dim medstr As String = "atm-newsImg" & itemid & "-" & deli & ".jpg"

            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim strto As String = appstr + "/invimages/"
            Select Case MyFile.PostedFile.ContentType
                Case "image/pjpeg", "image/jpeg"  'Make sure we are getting a valid JPG image
                    Try
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & newstr)
                    Catch ex As Exception
                        strto = "/invimages/"
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & newstr)
                        'Exit Sub
                    End Try

                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & thumbstr)
                    MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & medstr)
                    Dim fsimg As System.Drawing.Image
                    'Response.ContentType = "image/jpeg"
                    fsimg = System.Drawing.Image.FromFile(Server.MapPath("\") & strto & medstr)
                    Dim biw, bih As Integer
                    biw = fsimg.Width
                    bih = fsimg.Height
                    If biw > 500 Then
                        'biw = 500
                    End If
                    If bih > 382 Then
                        'bih = 382
                    End If
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                    Dim tnImg As System.Drawing.Image
                    Dim iw, ih As Integer
                    iw = 100
                    ih = 100
                    tnImg = fsimg.GetThumbnailImage(iw, ih, dummyCallBack, IntPtr.Zero)
                    tnImg.Save(Server.MapPath("\") & strto & thumbstr)

                    Dim tmImg As System.Drawing.Image
                    Dim iwm, ihm As Integer
                    iwm = System.Convert.ToInt32(biw)
                    ihm = System.Convert.ToInt32(bih)

                    If iwm > 220 Then
                        Dim iper As Decimal
                        iper = 220 / iwm
                        iwm = Math.Round(iwm * iper, 0)
                        ihm = Math.Round(ihm * iper, 0)
                    End If
                    If ihm > 220 Then
                        Dim iper As Decimal
                        iper = 220 / ihm
                        iwm = Math.Round(iwm * iper, 0)
                        ihm = Math.Round(ihm * iper, 0)
                    End If

                    tmImg = fsimg.GetThumbnailImage(iwm, ihm, dummyCallBack, IntPtr.Zero)
                    tmImg.Save(Server.MapPath("\") & strto & medstr)

                    Dim tbImg As System.Drawing.Image
                    Dim iwb, ihb, iwb1, ihb1 As Integer
                    iwb = fsimg.Width
                    ihb = fsimg.Height
                    iwb1 = System.Convert.ToInt32(iwb)
                    ihb1 = System.Convert.ToInt32(ihb)
                    If iwb1 > 500 Then
                        Dim iper As Decimal
                        iper = 500 / iwb1
                        iwb1 = Math.Round(iwb1 * iper, 0)
                        ihb1 = Math.Round(ihb1 * iper, 0)
                    End If
                    If ihb1 > 500 Then
                        Dim iper As Decimal
                        iper = 500 / ihb1
                        iwb1 = Math.Round(iwb1 * iper, 0)
                        ihb1 = Math.Round(ihb1 * iper, 0)
                    End If

                    'tbImg = fsimg.GetThumbnailImage(biw, bih, dummyCallBack, IntPtr.Zero)
                    tbImg = fsimg.GetThumbnailImage(iwb1, ihb1, dummyCallBack, IntPtr.Zero)
                    tbImg.Save(Server.MapPath("\") & strto & newstr)


                    'tnImg.Dispose()
                    'tmImg.Dispose()
                    tbImg.Dispose()
                    fsimg.Dispose()

                    Dim nurl As String = System.Configuration.ConfigurationManager.AppSettings("tpmurl")
                    Dim savstr As String = nurl & "/invimages/" & newstr
                    Dim savtnstr As String = nurl & "/invimages/" & thumbstr
                    Dim savtmstr As String = nurl & "/invimages/" & medstr
                    Dim cmd As New SqlCommand
                    cmd.CommandText = "exec usp_addnewimginv @invid, @ni, @nit, @nim"

                    Dim param1 = New SqlParameter("@invid", SqlDbType.Int)
                    param1.Value = itemid
                    cmd.Parameters.Add(param1)
                    Dim param2 = New SqlParameter("@ni", SqlDbType.VarChar)
                    param2.Value = savstr
                    cmd.Parameters.Add(param2)
                    Dim param3 = New SqlParameter("@nit", SqlDbType.VarChar)
                    param3.Value = savtnstr
                    cmd.Parameters.Add(param3)
                    Dim param4 = New SqlParameter("@nim", SqlDbType.VarChar)
                    param4.Value = savtmstr
                    cmd.Parameters.Add(param4)

                    news.UpdateHack(cmd)
                    lblref.Value = "yes"
                Case Else
                    Dim strMessage As String = tmod.getmsg("cdstr1597", "pmuploadimage1.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Select

            news.Dispose()
            lblsubmit.Value = "return"
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1598", "pmuploadimage1.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3302.Text = axlabs.GetASPXPage("pmuploadimage1.aspx", "lang3302")
        Catch ex As Exception
        End Try
        Try
            lang3303.Text = axlabs.GetASPXPage("pmuploadimage1.aspx", "lang3303")
        Catch ex As Exception
        End Try

    End Sub
End Class