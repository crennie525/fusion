<%@ Page Language="vb" AutoEventWireup="false" Codebehind="devTaskPartList.aspx.vb" Inherits="lucy_r12.devTaskPartList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Add\Edit Parts</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/sessrefdialog.js"></script>
		<script language="JavaScript" src="../scripts/gridnavopt_1.js"></script>
		<script language="JavaScript" src="../scripts1/devTaskPartListaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function getqty(id, cid, itid) {
             var chk = document.getElementById(cid).checked;
             if (chk == true) {
                 var eReturn = window.showModalDialog("PopQty.aspx", "", "dialogHeight:150px; dialogWidth:220px; resizable=yes");
                 if (eReturn) {
                     if (eReturn == "can") {
                         document.getElementById(cid).checked = false;
                     }
                     else {
                         document.getElementById("trcart").className = "view";
                         document.getElementById("trempty").className = "details";
                         var qty = eReturn;
                         var td1 = document.getElementById("td1");
                         var td2 = document.getElementById("td2");
                         td1.innerHTML += id + '<br>';
                         td2.innerHTML += qty + '<br>';
                         var lblp = document.getElementById("lblparts");
                         lblp.value += id + '~'
                         var lblq = document.getElementById("lblqty");
                         lblq.value += qty + '/'
                         var lbli = document.getElementById("lblitemid");
                         lbli.value += itid + '/'
                     }
                 }
             }
             else {
                 var partstr = document.getElementById("lblparts").value;
                 var qtystr = document.getElementById("lblqty").value;
                 var itemstr = document.getElementById("lblqty").value;
                 var partarray = partstr.split("/");
                 var qtyarray = qtystr.split("/");
                 var itemarray = itemstr.split("/");
                 document.getElementById("lblparts").value = "";
                 document.getElementById("lblqty").value = "";
                 document.getElementById("lblitemid").value = "";
                 for (var i = 0; i < partarray.length - 1; i++) {
                     var lblp = document.getElementById("lblparts");
                     var lblq = document.getElementById("lblqty");
                     var lbli = document.getElementById("lblitemid");
                     if (partarray[i] == id) {
                         partarray.slice(i);
                         qtyarray.slice(i);
                         itemarray.slice(i);
                     }
                     else {
                         lblp.value += partarray[i] + '~';
                         lblq.value += qtyarray[i] + '/';
                         lbli.value += itemarray[i] + '/';
                     }
                 }
                 getcart();
             }
         }
         function addpart() {
             //document.getElementById("lbladdflag").value=="yes"
             var pnum = document.getElementById("txtpart").value;
             var pchk = pnum.match("#")
             try {
                 if (pchk.length > 0) {
                     pnum = pnum.replace(/#/, "%23")
                 }
             }
             catch (err) {

             }
             var eReturn = window.showModalDialog("AddPartDialog.aspx?pnum=" + pnum + "&pg=parts", "", "dialogHeight:270px; dialogWidth:400px; resizable=yes");
             if (eReturn) {
                 if (eReturn == "can") {
                     document.getElementById("lbladdflag").value = "can";

                 }
                 else {
                     document.getElementById("lbladdflag").value = "ok";

                     document.getElementById("form1").submit();

                 }
             }
         }
     </script>
	</HEAD>
	<body bgColor="white" onload="getcart();checkit();" MS_POSITIONING="FlowLayout">
		<form id="form1" method="post" runat="server">
			<table width="700">
				<tr>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td colspan="2" align="right"><IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
										runat="server"></td>
							</tr>
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" width="674"><asp:Label id="lang2705" runat="server">Add/Edit Parts Mini Dialog</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="600">&nbsp;</td>
					<td class="label" width="60"><asp:Label id="lang2706" runat="server">Task#</asp:Label></td>
					<td class="label" id="tdtask" width="40" runat="server"></td>
				</tr>
			</table>
			<table id="mdiv" cellSpacing="0" cellPadding="2" width="700" runat="server">
				<TBODY>
					<tr>
						<td colSpan="3"><asp:datagrid id="dgparttasks" runat="server" GridLines="None" AutoGenerateColumns="False" cellPadding="1"
								cellSpacing="1">
								<AlternatingItemStyle BackColor="#E7F1FD" CssClass="plainlabel"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit">
										<HeaderStyle Width="80px" CssClass="btmmenu plainlabel" Height="24"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:imagebutton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
											<asp:imagebutton id="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Part#">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lbltskprtidr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lineid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lbltskprtidre" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lineid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Qty">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="dgqty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Cost">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label19" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remove">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											&nbsp;&nbsp;&nbsp;
											<asp:ImageButton id="Imagebutton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
												CommandName="Delete"></asp:ImageButton>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td align="center" colSpan="3">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img1" onclick="getfirst('new');" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img2" onclick="getprev('new');" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblnpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img3" onclick="getnext('new');" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td width="20"><IMG id="Img4" onclick="getlast('new');" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
									<td><asp:imagebutton id="btnradd" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnew.gif"></asp:imagebutton></td>
								</tr>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
			<div id="div1" runat="server">
				<table>
					<tr>
						<td class="bluelabel"><asp:Label id="lang2707" runat="server">Part#</asp:Label></td>
						<td><asp:textbox id="txtpart" runat="server" MaxLength="50"></asp:textbox></td>
						<td><asp:imagebutton id="btnsrch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton>
						<img src="../images/appbuttons/minibuttons/addwhite.gif" onclick="addpart();"></td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0">
					<tr>
						<td colSpan="2"><div style="HEIGHT: 200px; OVERFLOW: auto" id="ispdiv" onscroll="SetiDivPosition();"><asp:datagrid id="dgitems" runat="server" GridLines="None" AutoGenerateColumns="False" cellPadding="1"
									cellSpacing="1" ShowFooter="True" AllowCustomPaging="True" AllowPaging="True" BackColor="White" PageSize="100">
									<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
									<ItemStyle CssClass="plainlabel"></ItemStyle>
									<Columns>
										<asp:TemplateColumn  HeaderText="Select">
                                            <HeaderStyle Height="24px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox id="cb1" runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Edit">
											<HeaderStyle Height="24px" Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:ImageButton id="ImageButton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
													ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
											</ItemTemplate>
											<FooterTemplate>
												<asp:ImageButton id="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
													CommandName="Add"></asp:ImageButton>
											</FooterTemplate>
											<EditItemTemplate>
												&nbsp;
												<asp:ImageButton id="ImageButton13" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
													ToolTip="Save Changes" CommandName="Update"></asp:ImageButton>
												<asp:ImageButton id="ImageButton14" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
													ToolTip="Cancel Changes" CommandName="Cancel"></asp:ImageButton>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn Visible="False" HeaderText="Qty">
											<ItemTemplate>
												<asp:TextBox id="txtqty" runat="server" Width="30px"></asp:TextBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Part#">
											<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=itid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:TextBox id="itnumf" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
												</asp:TextBox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:TextBox id=itnum runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn Visible="False" HeaderText="itemid">
											<ItemTemplate>
												<asp:Label id=itemid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:Label id=txtid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
												</asp:Label>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Description">
											<HeaderStyle Width="180px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=Label12 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:TextBox id="txtdescf" runat="server" Width="170px" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
												</asp:TextBox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:TextBox id=txtdesc runat="server" Width="170px" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Location">
											<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=Label13 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:TextBox id="txtlocf" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
												</asp:TextBox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:TextBox id=txtloc runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Cost">
											<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=Label21 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:TextBox id="txtcostf" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
												</asp:TextBox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:TextBox id=txtcost runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle Visible="False"></PagerStyle>
								</asp:datagrid></div>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="3">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img5" onclick="getfirst('i');" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img6" onclick="getprev('i');" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblipg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img7" onclick="getnext('i');" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td width="20"><IMG id="Img8" onclick="getlast('i');" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="600">
					<tr>
						<td width="200">&nbsp;</td>
						<td width="200"></td>
						<td width="50"></td>
						<td width="200">&nbsp;</td>
					</tr>
					<tr>
						<td></td>
						<td class="thdrsing label" align="center" bgColor="#0000cc" colSpan="2"><asp:Label id="lang2708" runat="server">Part Cart</asp:Label></td>
						<td></td>
					</tr>
					<tr class="details" id="trempty">
						<td class="label" align="center" colSpan="4"><asp:Label id="lang2709" runat="server">Your Cart Is Empty</asp:Label></td>
					</tr>
					<tr class="details" id="trcart">
						<td width="200"></td>
						<td class="label" align="center" width="100"><u><asp:Label id="lang2710" runat="server">Part#</asp:Label></u>
						</td>
						<td class="label" align="center" width="100"><u>Qty</u>
						</td>
						<td width="200"></td>
					</tr>
					<tr>
						<td></td>
						<td class="labellt" id="td1" bgColor="white" runat="server"></td>
						<td class="labellt" id="td2" align="center" bgColor="white"></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td align="right"><asp:imagebutton id="ibtnaddtolist" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:imagebutton><asp:imagebutton id="Imagebutton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
								ToolTip="Cancel and Return" CommandName="Cancel"></asp:imagebutton></td>
						<td></td>
					</tr>
				</table>
				<table class="details">
					<tr>
						<td><asp:listbox id="lbchecked" runat="server"></asp:listbox></td>
					</tr>
				</table>
			</div>
			<iframe id="ifsession" class="details" src="" frameBorder="no" runat="server" style="Z-INDEX: 0">
			</iframe><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input id="lblptid" type="hidden" name="lblptid" runat="server"> <input id="txtopg" type="hidden" name="txtopg" runat="server">
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"> <input id="txtipg" type="hidden" name="Hidden2" runat="server">
			<input id="lbloflag" type="hidden" name="Hidden1" runat="server"> <input id="lblparts" type="hidden" name="lblparts" runat="server">
			<input id="lblqty" type="hidden" name="lblqty" runat="server"><input id="lblitemid" type="hidden" name="lblitemid" runat="server">
			<input id="lblfilter" type="hidden" name="lblfilter" runat="server"><input id="lblfilterwd" type="hidden" name="lblfilterwd" runat="server">
			<input id="lbladdflag" type="hidden" name="lbladdflag" runat="server"><input id="lblcid" type="hidden" name="lblcid" runat="server">
			<input id="lbllog" type="hidden" runat="server"> <input id="lblwo" type="hidden" name="lblwo" runat="server"><input id="lbljpnum" type="hidden" name="lbljpnum" runat="server">
			<input id="lbltyp" type="hidden" runat="server"> <input id="lblret" type="hidden" name="lblret" runat="server"><input id="txtnpgcnt" type="hidden" name="txtnpgcnt" runat="server">
			<input id="txtipgcnt" type="hidden" name="txtipgcnt" runat="server"><input id="txtnpg" type="hidden" name="Hidden1" runat="server">
			<input id="lblold" type="hidden" runat="server"><input type="hidden" id="ispdivy" runat="server">
			<input type="hidden" id="lblcoid" runat="server">
		<input type="hidden" id="lblwojtid" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
