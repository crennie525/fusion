

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class partsmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang2822 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = "itemnum"
    Dim rowcnt As Integer
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim cid, dept, sql, ro As String
    Dim inv As New Utilities
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpart As System.Web.UI.WebControls.Label
    Protected WithEvents dgparts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgparts As System.Web.UI.WebControls.Label
    Protected WithEvents partPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents partNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents noparthdr As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgparts.Columns(0).Visible = False
                dgparts.Columns(6).Visible = False
            End If
            Try
                cid = "0" 
                lblcid.Value = cid
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try
            If cid <> "" Then
                inv.Open()
                LoadParts(PageNumber)
                inv.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                inv.Open()
                GetNext()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                inv.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadParts(PageNumber)
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                inv.Open()
                GetPrev()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                inv.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadParts(PageNumber)
                inv.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        inv.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadParts(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub LoadParts(ByVal PageNumber As Integer)

        Dim Group As String = ""
        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        If Len(srch) > 0 Then
            Filter = "itemnum like ''%" & srch & "%''"
            FilterCnt = "itemnum like '%" & srch & "%'"
        Else
            Filter = ""
            FilterCnt = ""
        End If
        Dim sql As String
        If FilterCnt <> "" Then
            sql = "select Count(*) from item where " & FilterCnt
        Else
            sql = "select Count(*) from item"
        End If

        dgparts.VirtualItemCount = inv.Scalar(sql)
        If dgparts.VirtualItemCount = 0 Then
            Tables = "item"
            PK = "itemid"
            PageSize = "10"
            Fields = "*"
            dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgparts.DataSource = dr
            Try
                dgparts.DataBind()
            Catch ex As Exception
                dgparts.CurrentPageIndex = 0
                dgparts.DataBind()
            End Try
            dr.Close()
            lblpart.Text = "No Parts Inventory Records Found"
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgparts.PageCount
            If dgparts.PageCount = 0 Then
                lblpg.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & dgparts.PageCount
            End If
        Else
            Tables = "item"
            PK = "itemid"
            PageSize = "10"
            Fields = "*"
            dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgparts.DataSource = dr
            Try
                dgparts.DataBind()
            Catch ex As Exception
                dgparts.CurrentPageIndex = 0
                dgparts.DataBind()
            End Try
            dr.Close()
            lblpart.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgparts.PageCount
            If dgparts.PageCount = 0 Then
                lblpg.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & dgparts.PageCount
            End If
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadParts(PageNumber)
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1395" , "partsmain.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadParts(PageNumber)
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1396" , "partsmain.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgparts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("Label3"), Label).Text
        dgparts.EditItemIndex = e.Item.ItemIndex
        inv.Open()
        LoadParts(PageNumber)
        inv.Dispose()
    End Sub

    Private Sub dgparts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.CancelCommand
        PageNumber = txtpg.Value
        dgparts.EditItemIndex = -1
        inv.Open()
        LoadParts(PageNumber)
        inv.Dispose()
    End Sub

    Private Sub dgparts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.UpdateCommand
        inv.Open()
        cid = lblcid.Value
        Dim id, part, desc, loc, cost As String
        Dim flag As Integer = 1
        'id = CType(e.Item.Cells(1).Controls(1), TextBox).Text
        id = CType(e.Item.FindControl("lblitemid"), Label).Text
        part = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        part = Replace(part, "'", Chr(180), , , vbTextCompare)
        part = Replace(part, "--", "-", , , vbTextCompare)
        part = Replace(part, ";", ":", , , vbTextCompare)
        desc = CType(e.Item.Cells(3).Controls(1), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        If Len(desc) = 0 Then desc = ""
        If Len(desc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1397" , "partsmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        loc = CType(e.Item.Cells(4).Controls(1), TextBox).Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)
        If Len(loc) = 0 Then loc = ""
        If Len(loc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1398" , "partsmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        cost = CType(e.Item.FindControl("txtcost"), TextBox).Text
        Dim costchk As Decimal
        Try
            costchk = System.Convert.ToDecimal(cost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1399" , "partsmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If Len(part) <= 50 And Len(part) <> 0 And part <> "" Then
            'need sp here
            Dim old As String = lblold.Value
            Dim icnt As Integer
            sql = "usp_updateinv 'part','" & id & "','" & part & "','" & desc & "','" & old & "','" & loc & "', '" & cost & "', '" & flag & "'"
            icnt = inv.Scalar(sql)
            If icnt = 0 Then
                PageNumber = txtpg.Value
                dgparts.EditItemIndex = -1
                LoadParts(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1400" , "partsmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

            'sql = "update item set description = " _
            '       + "'" & desc & "', itemnum = '" & part & "', location = '" & loc & "' " _
            '       + "where itemid = '" & id & "'"
            'inv.Update(sql)

        Else
            If Len(part) = 0 OrElse part = "" Then
                Dim strMessage As String =  tmod.getmsg("cdstr1401" , "partsmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1402" , "partsmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
        inv.Dispose()
    End Sub

    Private Sub dgparts_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.DeleteCommand
        inv.Open()
        Dim id, item, old As String
        Dim flag As Integer = 0
        Try
            id = CType(e.Item.FindControl("lblitemidn"), Label).Text
            item = CType(e.Item.FindControl("Label3"), Label).Text
            old = CType(e.Item.FindControl("Label1"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblitemid"), Label).Text
            item = CType(e.Item.FindControl("TextBox1"), TextBox).Text
            old = CType(e.Item.FindControl("Label2"), Label).Text
            flag = 1
        End Try
        If flag = 1 Then
            If old <> item Then
                Dim strMessage As String =  tmod.getmsg("cdstr1403" , "partsmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                inv.Dispose()
                Exit Sub
            End If
        End If
        Dim icnt As Integer
        sql = "select count(*) from pmtaskparts where itemnum = '" & item & "'"
        icnt = inv.Scalar(sql)
        If icnt = 0 Then
            sql = "delete from item where itemid = '" & id & "'"
            inv.Update(sql)
            dgparts.EditItemIndex = -1
            cid = lblcid.Value
            sql = "select Count(*) from item " _
            + "where compid = '" & cid & "'"
            PageNumber = inv.PageCount(sql, PageSize)
            If dgparts.CurrentPageIndex > PageNumber Then
                dgparts.CurrentPageIndex = PageNumber - 1
            End If
            If dgparts.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgparts.CurrentPageIndex + 1
            End If
            dgparts.EditItemIndex = -1
            LoadParts(PageNumber) 'dgparts.CurrentPageIndex
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1404" , "partsmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        inv.Dispose()
    End Sub

    Private Sub dgparts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparts.ItemCommand
        If e.CommandName = "Add" Then
            Dim num, des, loc, cost As String
            Dim nt, dt, lt As TextBox
            nt = CType(e.Item.FindControl("txtpname"), TextBox)
            dt = CType(e.Item.FindControl("txtpdesc"), TextBox)
            lt = CType(e.Item.FindControl("txtploc"), TextBox)
            num = CType(e.Item.FindControl("txtpname"), TextBox).Text
            num = Replace(num, "'", Chr(180), , , vbTextCompare)
            num = Replace(num, "--", "-", , , vbTextCompare)
            num = Replace(num, ";", ":", , , vbTextCompare)
            des = CType(e.Item.FindControl("txtpdesc"), TextBox).Text
            des = Replace(des, "'", Chr(180), , , vbTextCompare)
            des = Replace(des, "--", "-", , , vbTextCompare)
            des = Replace(des, ";", ":", , , vbTextCompare)
            If Len(des) = 0 Then des = ""
            If Len(des) > 100 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1405" , "partsmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            loc = CType(e.Item.FindControl("txtploc"), TextBox).Text
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
            loc = Replace(loc, "--", "-", , , vbTextCompare)
            loc = Replace(loc, ";", ":", , , vbTextCompare)
            If Len(loc) = 0 Then loc = ""
            If Len(loc) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1406" , "partsmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            cost = CType(e.Item.FindControl("txtcostf"), TextBox).Text
            If cost = "" Then
                cost = "0"
            End If
            Dim costchk As Decimal
            Try
                costchk = System.Convert.ToDecimal(cost)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1407" , "partsmain.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If Len(num) <= 30 And Len(num) <> 0 And num <> "" Then
                inv.Open()
                cid = lblcid.Value
                sql = "usp_insertinv 'part', '" & num & "','" & des & "','" & loc & "','" & cost & "','oinv'"
                Dim icnt As Integer
                icnt = inv.Scalar(sql)
                'sql = "insert into item " _
                '+ "(compid, itemnum, description, location) values " _
                '+ "('" & cid & "', '" & num & "', '" & des & "', '" & loc & "')"
                'inv.Update(sql)
                If icnt = 0 Then
                    sql = "select Count(*) from item " '_
                    '+ "where compid = '" & cid & "'"
                    PageNumber = inv.PageCount(sql, PageSize)
                    LoadParts(PageNumber)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1408" , "partsmain.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If

                inv.Dispose()
            Else

                If Len(num) = 0 OrElse num = "" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1409" , "partsmain.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                ElseIf Len(num) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1410" , "partsmain.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgparts.Columns(0).HeaderText = dlabs.GetDGPage("partsmain.aspx","dgparts","0")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(2).HeaderText = dlabs.GetDGPage("partsmain.aspx","dgparts","2")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(3).HeaderText = dlabs.GetDGPage("partsmain.aspx","dgparts","3")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(4).HeaderText = dlabs.GetDGPage("partsmain.aspx","dgparts","4")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(5).HeaderText = dlabs.GetDGPage("partsmain.aspx","dgparts","5")
		Catch ex As Exception
		End Try
		Try
			dgparts.Columns(6).HeaderText = dlabs.GetDGPage("partsmain.aspx","dgparts","6")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2822.Text = axlabs.GetASPXPage("partsmain.aspx","lang2822")
		Catch ex As Exception
		End Try

	End Sub

End Class
