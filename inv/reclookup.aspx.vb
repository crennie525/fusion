

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class reclookup
    Inherits System.Web.UI.Page
	Protected WithEvents lang2859 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2858 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2857 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim inv As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = "invpr"
    Dim PK As String = "prnum"
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpvend As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdpr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String
    Dim item As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            item = Request.QueryString("item").ToString
            txtsrch.Text = item
            inv.Open()
            txtpg.Value = "1"
            GetPR()
            inv.Dispose()
        Else
            If Request.Form("lblsubmit") = "get" Then
                lblsubmit.Value = ""
                inv.Open()
                GetPR()
                inv.Dispose()
            End If

            If Request.Form("lblret") = "next" Then
                inv.Open()
                GetNext()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                inv.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetPR()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                inv.Open()
                GetPrev()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                inv.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetPR()
                inv.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetPR()
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1428" , "reclookup.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetPR()
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1429" , "reclookup.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPR()
        'Dim itemid As String = lblitemid.Value
        sql = "select prnum, issuedate, requireddate, requestedby, vendor, status, statusdate, totalcost from invpr " '_
        '+ "where itemid = '" & itemid & "' order by issuedate desc"
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table width=""800"" cellpadding=""0"">")

        sb.Append("<tr><td colspan=""11"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 780px;  HEIGHT: 380px"">")
        sb.Append("<table width=""990"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""80"">" & tmod.getlbl("cdlbl539" , "reclookup.aspx.vb") & "</td>")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""80"">" & tmod.getlbl("cdlbl540" , "reclookup.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl541" , "reclookup.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""100"">" & tmod.getlbl("cdlbl542" , "reclookup.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl543" , "reclookup.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl544" , "reclookup.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""200"">" & tmod.getlbl("cdlbl545" , "reclookup.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">" & tmod.getlbl("cdlbl546" , "reclookup.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""100"">" & tmod.getlbl("cdlbl547" , "reclookup.aspx.vb") & "</td></tr>")
        Dim srch As String = txtsrch.Text
        If srch <> "" Then
            Filter = "itemnum = ''" & srch & "''"
            FilterCnt = "itemnum = '" & srch & "'"
        End If
        Dim pvend = txtpvend.Text
        If pvend <> "" Then
            If Filter <> "" Then
                Filter += " and (vendor = ''" & pvend & "'' or povendor = ''" & pvend & "'')"
                FilterCnt += " and (vendor = '" & pvend & "' or povendor = '" & pvend & "')"
            Else
                Filter = "(vendor = ''" & pvend & "'' or povendor = ''" & pvend & "'')"
                FilterCnt = "(vendor = '" & pvend & "' or povendor = '" & pvend & "')"
            End If
        End If
        If Filter <> "" Then
            Filter += " and (status <> ''COMP'' and status <> ''CAN'')"
            FilterCnt += " and (status <> 'COMP' and status <> 'CAN')"
        Else
            Filter = "(status <> ''COMP'' and status <> ''CAN'')"
            FilterCnt = "(status <> 'COMP' and status <> 'CAN')"
        End If
        If FilterCnt <> "" Then
            sql = "select Count(*) from invpr where " & FilterCnt
        Else
            sql = "select Count(*) from invpr"
        End If

        Dim dc As Integer = inv.Scalar(sql)
        Dim intPgCnt, intPgNav As Integer
        PageNumber = txtpg.Value
        intPgCnt = inv.Scalar(sql)
        intPgNav = inv.PageCount(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        Dim ven, inum As String
        'dr = inv.GetRdrData(sql)
        While dr.Read
            ven = dr.Item("vendor").ToString
            If Len(ven) = 0 Then
                ven = "&nbsp;"
            End If
            inum = dr.Item("itemnum").ToString
            sb.Append("<tr><td class=""plainlabel grayborder"" width=""80""><a href=""#"" class=""A1"" onclick=""getpr('" & dr.Item("prnum").ToString & "');"">" & dr.Item("prnum").ToString & "</a></td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("itemnum").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""100"">" & dr.Item("status").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("statusdate").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("requireddate").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("requestedby").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & dr.Item("issuedate").ToString & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""120"">" & ven & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" width=""100"">" & dr.Item("totalcost").ToString & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdpr.InnerHtml = sb.ToString
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2857.Text = axlabs.GetASPXPage("reclookup.aspx","lang2857")
		Catch ex As Exception
		End Try
		Try
			lang2858.Text = axlabs.GetASPXPage("reclookup.aspx","lang2858")
		Catch ex As Exception
		End Try
		Try
			lang2859.Text = axlabs.GetASPXPage("reclookup.aspx","lang2859")
		Catch ex As Exception
		End Try

	End Sub

End Class
