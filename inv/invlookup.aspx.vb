

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class invlookup
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim lab As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsrch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdmat As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim sql As String
    Protected WithEvents lbllist As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim list As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            list = Request.QueryString("list").ToString
            lbllist.Value = list
            lab.Open()
            GetLabor(list)
            lab.Dispose()

        End If
    End Sub
    
    Private Sub GetLabor(ByVal list As String)
        Dim sb As New StringBuilder
        sb.Append("<table><tr height=""20"">")
        sb.Append("<td class=""tblmenurt"" width=""300"">" & tmod.getlbl("cdlbl507" , "invlookup.aspx.vb") & "</td></tr>")

        Dim lb, nm As String

        sql = "select value from valuelist where listname = '" & list & "'"
        dr = lab.GetRdrData(sql)
        While dr.Read
            lb = dr.Item("value").ToString
            lb = Replace(lb, "'", Chr(180), , , vbTextCompare)
            lb = Replace(lb, """", Chr(180), , , vbTextCompare)

            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""hlab('" & lb & "');"">" & lb & "</a></td></tr>")


        End While
        dr.Close()
        sb.Append("</table>")
        tdmat.InnerHtml = sb.ToString
    End Sub

End Class
