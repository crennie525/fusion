<%@ Page Language="vb" AutoEventWireup="false" Codebehind="purchreqedit.aspx.vb" Inherits="lucy_r12.purchreqedit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>purchreqedit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/purchreqeditaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function getcal(fld, fld2) {
             var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
             if (eReturn) {
                 document.getElementById(fld).value = eReturn;
                 document.getElementById(fld2).innerHTML = eReturn;
             }
         }
         function savepurch() {
             //alert("Not Active in Demo Mode")
             var po = document.getElementById("txtponum").value;
             var expt = document.getElementById("lblrecv").value;
             if (po == "") {
                 ("Purchase Order Number Required")
             }
             else if (expt == "") {
                 ("Expected Receive Date Required")
             }
             else {
                 document.getElementById("lblsubmit").value = "savepo";
                 document.getElementById("form1").submit();
             }
         }
         function getcomp(typ, tbox1, tbox2) {
             var eReturn = window.showModalDialog("../admin/admindialog.aspx?list=comp&typ=" + typ, "", "dialogHeight:500px; dialogWidth:640px; resizable=yes");
             if (eReturn) {
                 if (eReturn != "") {
                     var eret = eReturn.split(";");
                     document.getElementById(tbox1).value = eret[0];
                     document.getElementById(tbox2).innerHTML = eret[0];
                 }
             }
         }
     //-->
     </script>
      <style type="text/css">
          .readonly50
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 50px;
}
         .readonly90
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 90px;
}
     .readonly140
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 140px;
}
.readonly340
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 340px;
}
.readonly440
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 530px;
}
</style>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" width="800">
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2832" runat="server">Purchase Requisition Details</asp:Label></td>
				</TR>
				<tr>
					<td colSpan="2">
						<table cellPadding="3">
							<tr height="24">
								<td class="label" width="120"><asp:Label id="lang2833" runat="server">Purchase Req#</asp:Label></td>
								<td class="plainlabel" id="tdprnum" width="120" runat="server"></td>
								<td class="label" width="120"><asp:Label id="lang2834" runat="server">Status</asp:Label></td>
								<td width="200"><asp:dropdownlist id="ddstat" runat="server" AutoPostBack="True" CssClass="plainlabel">
										<asp:ListItem Value="WAPPR">WAPPR - Waiting to be Approved</asp:ListItem>
										<asp:ListItem Value="APPR">APPR - Approved</asp:ListItem>
										<asp:ListItem Value="INPRG">INPRG - In Progress</asp:ListItem>
										<asp:ListItem Value="COMP">COMP - Complete</asp:ListItem>
										<asp:ListItem Value="CAN">CAN - Cancelled</asp:ListItem>
									</asp:dropdownlist></td>
								<td align="right" width="250"><A class="A1" onclick="retpr();" href="#"><asp:Label id="lang2835" runat="server">Return</asp:Label></A></td>
							</tr>
							<tr height="24">
								<td class="label"><asp:Label id="lang2836" runat="server">Requested By</asp:Label></td>
								<td class="plainlabel" id="tdrby" runat="server"></td>
								<td class="label"><asp:Label id="lang2837" runat="server">Required Date</asp:Label></td>
								<td class="plainlabel" id="tdrbydt" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2838" runat="server">Vendor \ Ship To Details</asp:Label></td>
				</TR>
				<tr>
					<td colSpan="2">
						<table>
							<tr height="24">
								<td class="label" width="120"><asp:Label id="lang2839" runat="server">Suggested Vendor:</asp:Label></td>
								<td class="plainlabel" id="tdsvend" width="250" runat="server"></td>
								<td class="label" width="120"><asp:Label id="lang2840" runat="server">Ship To:</asp:Label></td>
								<td class="plainlabel" id="tdshipto" width="250" runat="server"></td>
							</tr>
							<tr height="24">
								<td class="label"><asp:Label id="lang2841" runat="server">Contact:</asp:Label></td>
								<td class="plainlabel" id="tdsvcont" runat="server"></td>
								<td class="label"><asp:Label id="lang2842" runat="server">Attention:</asp:Label></td>
								<td class="plainlabel" id="tdattn" runat="server"></td>
							</tr>
							<tr height="24">
								<td class="label"><asp:Label id="lang2843" runat="server">Phone:</asp:Label></td>
								<td class="plainlabel" id="tdvphone" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2844" runat="server">Item Details</asp:Label></td>
				</TR>
				<tr>
					<td colSpan="2">
						<table>
							<tr height="24">
								<td class="label" width="120"><asp:Label id="lang2845" runat="server">Item#:</asp:Label></td>
								<td class="plainlabel" id="tditem" runat="server"></td>
							</tr>
							<tr height="24">
								<td class="label" width="120"><asp:Label id="lang2846" runat="server">Description:</asp:Label></td>
								<td class="plainlabel" id="tditemdesc" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr height="24">
								<td class="label" width="80"><asp:Label id="lang2847" runat="server">Order Unit:</asp:Label></td>
								<td class="plainlabel" id="tdunit" width="80" runat="server"></td>
								<td class="label" width="80"><asp:Label id="lang2848" runat="server">Qty:</asp:Label></td>
								<td class="plainlabel" id="tdqty" width="80" runat="server"></td>
								<td class="label" width="80"><asp:Label id="lang2849" runat="server">Unit Price:</asp:Label></td>
								<td class="plainlabel" id="tdunitpr" width="80" runat="server"></td>
								<td class="label" width="80"><asp:Label id="lang2850" runat="server">Line Cost:</asp:Label></td>
								<td class="plainlabel" id="tdlcost" width="80" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2851" runat="server">Purchasing Use Only</asp:Label></td>
				</TR>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td class="label" width="100"><asp:Label id="lang2852" runat="server">Vendor</asp:Label></td>
								<td width="145"><div class="readonly140" id="divpvend" runat="server">
                                        </div></td>
								<td width="20"><IMG id="Img4" onclick="getcomp('V', 'lblpvend', 'divpvend');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('Look up Vendor')" onmouseout="return nd()"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td class="label" width="100"><asp:Label id="lang2853" runat="server">Purchase Order#</asp:Label></td>
								<td width="145"><asp:textbox id="txtponum" runat="server" CssClass="plainlabel" Width="145px"></asp:textbox></td>
								<td></td>
								<td class="label"><asp:Label id="lang2854" runat="server">Actual Unit Price</asp:Label></td>
								<td><asp:textbox id="txtactcost" runat="server" CssClass="plainlabel" Width="80px"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang2855" runat="server">Order Date</asp:Label></td>
								<td><div class="readonly90" id="divorderdate" runat="server">
                                        </div></td>
								<td><IMG onclick="getcal('lblorderdate', 'divorderdate');" src="../images/appbuttons/minibuttons/btn_calendar.jpg"></td>
								<td class="label"><asp:Label id="lang2856" runat="server">Expected Receive Date</asp:Label></td>
								<td><div class="readonly90" id="divrecv" runat="server">
                                        </div></td>
								<td><IMG onclick="getcal('lblrecv', 'divrecv');" src="../images/appbuttons/minibuttons/btn_calendar.jpg"></td>
								<td align="right" width="30"><IMG id="Img1" onclick="savepurch();" src="../images/appbuttons/minibuttons/savedisk1.gif"
										runat="server" onmouseover="return overlib('Save Purchasing Updates')" onmouseout="return nd()"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input id="lblprnum" type="hidden" runat="server"><input id="lblsubmit" type="hidden" runat="server">
			<input id="lbloldcost" type="hidden" runat="server"> <input type="hidden" id="lblwho" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblpvend" runat="server" />
<input type="hidden" id="lblorderdate" runat="server" />
<input type="hidden" id="lblrecv" runat="server" />
</form>
	</body>
</HTML>
