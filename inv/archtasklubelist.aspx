<%@ Page Language="vb" AutoEventWireup="false" Codebehind="archtasklubelist.aspx.vb" Inherits="lucy_r12.archtasklubelist" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>archtasklubelist</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/gridnavopt_1.js"></script>
		<script language="JavaScript" src="../scripts1/archtasklubelistaspx_1.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body bgColor="white" onload="getcart();checkit();" MS_POSITIONING="FlowLayout">
		<form id="form1" method="post" runat="server">
			<table width="700">
				<TR>
					<td colspan="3">
						<table cellspacing="0">
							<tr>
								<td colspan="2" align="right"><IMG id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
										runat="server"></td>
							</tr>
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" width="674"><asp:Label id="lang2618" runat="server">Add/Edit Lubricants Mini Dialog</asp:Label></td>
							</tr>
						</table>
					</td>
				<tr>
					<td width="600">&nbsp;</td>
					<td class="label" width="60"><asp:Label id="lang2619" runat="server">Task#</asp:Label></td>
					<td class="label" id="tdtask" width="40" runat="server"></td>
				</tr>
			</table>
			<table id="mdiv" cellSpacing="0" cellPadding="2" width="700" runat="server">
				<TBODY>
					<tr>
						<td class="bluelabel" colSpan="3"><asp:Label id="lang2620" runat="server">Original</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="3" align="center"><asp:datagrid id="dgoparttasks" cellSpacing="1" cellPadding="1" runat="server" AutoGenerateColumns="False"
								GridLines="None">
								<AlternatingItemStyle BackColor="#E7F1FD" CssClass="plainlabel"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
								<HeaderStyle CssClass="plainlabel" BackColor="White"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Lube#">
										<HeaderStyle Width="140px" CssClass="btmmenu plainlabel" Height="24"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label2 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lbltskprtid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tsklubeid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lbltskprtide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tsklubeid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label4 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id=Label5 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False" HeaderText="qty">
										<ItemTemplate>
											<asp:Label id=Label6 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="dgoqty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Cost">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label7 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id=Label8 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label9 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id=Label10 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td colspan="3" align="center">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst('old');" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev('old');" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext('old');" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td width="20"><IMG id="ilast" onclick="getlast('old');" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="3">
							<hr color="blue">
						</td>
					</tr>
					<tr>
						<td class="bluelabel" colSpan="3"><asp:Label id="lang2621" runat="server">Revised</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="3" align="center"><asp:datagrid id="dgparttasks" cellSpacing="1" cellPadding="1" runat="server" AutoGenerateColumns="False"
								GridLines="None">
								<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel" BackColor="White"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Lube#">
										<HeaderStyle Width="140px" CssClass="btmmenu plainlabel" Height="24"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lbltskprtidr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tsklubeid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lbltskprtidre" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tsklubeid") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False" HeaderImageUrl="../images/apptabs/bgtabs/qty.gif" HeaderText="qty">
										<ItemTemplate>
											<asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id="dgqty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="37px">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Cost">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label19" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td colspan="3" align="center">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img1" onclick="getfirst('new');" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img2" onclick="getprev('new');" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblnpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img3" onclick="getnext('new');" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td width="20"><IMG id="Img4" onclick="getlast('new');" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
			<div id="div1" runat="server">
				<table>
					<tr>
						<td class="bluelabel"><asp:Label id="lang2622" runat="server">Lube#</asp:Label></td>
						<td><asp:textbox id="txtpart" runat="server" MaxLength="50"></asp:textbox></td>
						<td><asp:imagebutton id="btnsrch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0">
					<tr>
						<td colSpan="2"><div style="HEIGHT: 200px; OVERFLOW: auto" id="ispdiv" onscroll="SetiDivPosition();"><asp:datagrid id="dgitems" cellSpacing="1" cellPadding="1" runat="server" AutoGenerateColumns="False"
									GridLines="None" BackColor="White" AllowPaging="True" AllowCustomPaging="True" ShowFooter="True">
									<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
									<ItemStyle CssClass="plainlabel"></ItemStyle>
									<Columns>
										<asp:TemplateColumn>
											<ItemTemplate>
												<asp:CheckBox id="cb1" runat="server"></asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn Visible="False" HeaderText="Qty">
											<ItemTemplate>
												<asp:TextBox id="txtqty" runat="server" Width="30px"></asp:TextBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Edit">
											<HeaderStyle Height="24px" Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:ImageButton id="ImageButton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
													ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
											</ItemTemplate>
											<FooterTemplate>
												<asp:ImageButton id="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
													CommandName="Add"></asp:ImageButton>
											</FooterTemplate>
											<EditItemTemplate>
												&nbsp;
												<asp:ImageButton id="ImageButton13" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
													ToolTip="Save Changes" CommandName="Update"></asp:ImageButton>
												<asp:ImageButton id="ImageButton14" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
													ToolTip="Cancel Changes" CommandName="Cancel"></asp:ImageButton>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Lube#">
											<HeaderStyle Height="24px" Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=itid runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:TextBox id="itnumf" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
												</asp:TextBox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:TextBox id=itnum runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.lubenum") %>'>
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn Visible="False" HeaderText="itemid">
											<ItemTemplate>
												<asp:Label id="itemid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lubeid") %>' >
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lubeid") %>' ID="Textbox3">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Description">
											<HeaderStyle Width="180px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=Label12 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:TextBox id="txtdescf" runat="server" Width="170px" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
												</asp:TextBox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:TextBox id=txtdesc runat="server" Width="170px" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Location">
											<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=Label13 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:TextBox id="txtlocf" runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
												</asp:TextBox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:TextBox id=txtloc runat="server" Width="140px" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Cost">
											<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
											<ItemTemplate>
												<asp:Label id=Label21 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterTemplate>
												<asp:TextBox id="txtcostf" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
												</asp:TextBox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:TextBox id=txtcost runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle Visible="False"></PagerStyle>
								</asp:datagrid></div>
						</td>
					</tr>
					<tr>
						<td colspan="3" align="center">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img5" onclick="getfirst('i');" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img6" onclick="getprev('i');" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblipg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img7" onclick="getnext('i');" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td width="20"><IMG id="Img8" onclick="getlast('i');" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="600">
					<tr>
						<td width="200">&nbsp;</td>
						<td width="200"></td>
						<td width="50"></td>
						<td width="200">&nbsp;</td>
					</tr>
					<tr>
						<td></td>
						<td class="thdrsing label" align="center" bgColor="#0000cc" colSpan="2"><asp:Label id="lang2623" runat="server">LubeCart</asp:Label></td>
						<td></td>
					</tr>
					<tr class="details" id="trempty">
						<td class="label" align="center" colSpan="4"><asp:Label id="lang2624" runat="server">Your Cart Is Empty</asp:Label></td>
					</tr>
					<tr class="details" id="trcart">
						<td width="200"></td>
						<td class="label" align="center" width="200" colspan="2"><u><asp:Label id="lang2625" runat="server">Lube#</asp:Label></u>
						</td>
						<td width="200"></td>
					</tr>
					<tr>
						<td></td>
						<td class="labellt" id="td1" bgColor="white" runat="server"></td>
						<td class="labellt" id="td2" align="center" bgColor="white"></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td align="right"><asp:imagebutton id="ibtnaddtolist" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:imagebutton><asp:imagebutton id="Imagebutton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
								ToolTip="Cancel and Return" CommandName="Cancel"></asp:imagebutton></td>
						<td></td>
					</tr>
				</table>
				<table class="details">
					<tr>
						<td><asp:listbox id="lbchecked" runat="server"></asp:listbox></td>
					</tr>
				</table>
			</div>
			<input id="lblptid" type="hidden" name="lblptid" runat="server"> <input id="txtopg" type="hidden" runat="server" NAME="txtopg">
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"> <input id="txtipg" type="hidden" name="Hidden2" runat="server">
			<input id="lbloflag" type="hidden" name="Hidden1" runat="server"> <input id="lblparts" type="hidden" runat="server" NAME="lblparts">
			<input id="lblqty" type="hidden" runat="server" NAME="lblqty"><input id="lblitemid" type="hidden" runat="server" NAME="lblitemid">
			<input id="lblfilter" type="hidden" runat="server" NAME="lblfilter"><input id="lblfilterwd" type="hidden" runat="server" NAME="lblfilterwd">
			<input type="hidden" id="lbladdflag" runat="server" NAME="lbladdflag"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="txtnpg" runat="server" NAME="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"><input type="hidden" id="txtnpgcnt" runat="server" NAME="txtnpgcnt">
			<input type="hidden" id="txtipgcnt" runat="server" NAME="txtipgcnt"><input type="hidden" id="lblold" runat="server" NAME="lblold">
			<input type="hidden" id="ispdivy" runat="server" NAME="ispdivy"> <input type="hidden" id="lblrev" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
