

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class adjinv
    Inherits System.Web.UI.Page
	Protected WithEvents lang2614 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2613 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2612 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2611 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2610 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2609 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2608 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2607 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2606 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim item, itemid, store, curbal, ro As String
    Dim sql As String
    Dim adj As New Utilities

    Protected WithEvents txtnewbal As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstore As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurbal As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladjtype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tditem As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdstore As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdbal As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ddrec As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtcomments As System.Web.UI.WebControls.TextBox
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                Img1.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                Img1.Attributes.Add("onclick", "")
            End If
            item = Request.QueryString("item").ToString
            itemid = Request.QueryString("itemid").ToString
            store = Request.QueryString("store").ToString
            curbal = Request.QueryString("curbal").ToString
            tditem.InnerHtml = item
            lblitemnum.Value = item
            tdstore.InnerHtml = store
            lblstore.Value = store
            tdbal.InnerHtml = curbal
            lblcurbal.Value = curbal
            lblitemid.Value = itemid
            lbladjtype.Value = "phys"
        Else
            If Request.Form("lblsubmit") = "nbal" Then
                lblsubmit.Value = ""
                adj.Open()
                AdjBal()
                adj.Dispose()

            End If
        End If
    End Sub
    Private Sub AdjBal()
        itemid = lblitemid.Value
        item = lblitemnum.Value
        curbal = lblcurbal.Value
        store = lblstore.Value
        Dim nbal, rec, coms, typ As String
        nbal = txtnewbal.Text
        rec = ddrec.SelectedValue.ToString
        coms = txtcomments.Text
        Dim qtychk As Decimal
        Try
            qtychk = System.Convert.ToDecimal(nbal)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1011" , "adjinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        coms = Replace(coms, "'", Chr(180), , , vbTextCompare)
        coms = Replace(coms, "--", "-", , , vbTextCompare)
        coms = Replace(coms, ";", ":", , , vbTextCompare)
        typ = lbladjtype.Value
        If typ = "adj" Then
            sql = "insert into invadjhist (itemid, itemnum, location, origbal, newbal, adjdate, comments) values (" _
            + "'" & itemid & "','" & item & "','" & store & "','" & curbal & "','" & nbal & "',getDate(),'" & coms & "')"

            sql += ";update invbalances set curbal = '" & nbal & "', adjdate = getDate() where itemid = '" & itemid & "' and " _
            + "location = '" & store & "'" ' " _
            '+ ";update inventory set curbal = (select sum(curbal) from invbalances where itemid = '" & itemid & "') " _
            '+ "where itemid = '" & itemid & "'"
        Else
            sql = "insert into invadjhist (itemid, itemnum, location, origbal, physcnt, physcntdate, comments) values (" _
            + "'" & itemid & "','" & item & "','" & store & "','" & curbal & "','" & nbal & "',getDate(),'" & coms & "')"

            sql += ";update invbalances set curbal = '" & nbal & "', physcnt = '" & nbal & "', physcntdate = getDate() " _
            + "where itemid = '" & itemid & "' and location = '" & store & "'" ' " _
            '+ ";update inventory set curbal = (select sum(curbal) from invbalances where itemid = '" & itemid & "') " _
            '+ "where itemid = '" & itemid & "'"
        End If
        

        adj.Update(sql)
        lblsubmit.Value = "return"
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2606.Text = axlabs.GetASPXPage("adjinv.aspx","lang2606")
		Catch ex As Exception
		End Try
		Try
			lang2607.Text = axlabs.GetASPXPage("adjinv.aspx","lang2607")
		Catch ex As Exception
		End Try
		Try
			lang2608.Text = axlabs.GetASPXPage("adjinv.aspx","lang2608")
		Catch ex As Exception
		End Try
		Try
			lang2609.Text = axlabs.GetASPXPage("adjinv.aspx","lang2609")
		Catch ex As Exception
		End Try
		Try
			lang2610.Text = axlabs.GetASPXPage("adjinv.aspx","lang2610")
		Catch ex As Exception
		End Try
		Try
			lang2611.Text = axlabs.GetASPXPage("adjinv.aspx","lang2611")
		Catch ex As Exception
		End Try
		Try
			lang2612.Text = axlabs.GetASPXPage("adjinv.aspx","lang2612")
		Catch ex As Exception
		End Try
		Try
			lang2613.Text = axlabs.GetASPXPage("adjinv.aspx","lang2613")
		Catch ex As Exception
		End Try
		Try
			lang2614.Text = axlabs.GetASPXPage("adjinv.aspx","lang2614")
		Catch ex As Exception
		End Try

	End Sub

End Class
