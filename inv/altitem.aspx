<%@ Page Language="vb" AutoEventWireup="false" Codebehind="altitem.aspx.vb" Inherits="lucy_r12.altitem" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>altitem</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/altitemaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" width="820">
				<tr>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2615" runat="server">Alternate Items</asp:Label></td>
				</tr>
				<tr height="20">
					<td colspan="2" align="center" class="plainlabelblue"><asp:Label id="lang2616" runat="server">*Items are displayed to indicate Current Balances per Store Room.</asp:Label><br><asp:Label id="lang2617" runat="server">If an item is stored in multiple Store Rooms choose any to select as an Alternate Item.</asp:Label></td>
				</tr>
				<tr>
					<td id="tdalt" colSpan="2" runat="server"></td>
				</tr>
			</table>
			<input id="lbloldcode" type="hidden" runat="server"> <input id="lbltyp" type="hidden" runat="server">
			<input type="hidden" id="lblchildid" runat="server"> <input type="hidden" id="lblchild" runat="server">
			<input type="hidden" id="lblsubmit" runat="server"> <input type="hidden" id="lblcodeid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
