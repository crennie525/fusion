

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class issueinv
    Inherits System.Web.UI.Page
	Protected WithEvents lang2770 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2769 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2768 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2767 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2766 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2765 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2764 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2763 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2762 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2761 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2760 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2759 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2758 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2757 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2756 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2755 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2754 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2753 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2752 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2751 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2750 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2749 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2748 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2747 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2746 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim inv As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim ap As New AppUtils



    Protected WithEvents trwo As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdst As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtqty As System.Web.UI.WebControls.TextBox
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage



    Protected WithEvents lblitemid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltransid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstdcost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnsrch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgbuild As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgprint As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdstat As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltransstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltranstype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtojp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbtojp As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblstore As System.Web.UI.HtmlControls.HtmlInputHidden





    Protected WithEvents imglot As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllotcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim invs, ro As String

    Protected WithEvents lblpickseeds As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divcode As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divdesc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divstore As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbllot As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divlot As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcurbal As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divcurbal As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsexp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divsexp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsavail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divsavail As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltotbal As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divtotbal As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblres As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divres As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblexp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divexp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblavail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divavail As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divnstdesc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divwo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblnewstore As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divnewstore As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblstorechange As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divbin As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcodedesc As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList

    Protected WithEvents Tr5 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldcode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltype As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents txtstdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchild As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblneweq As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                Img1.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                Img1.Attributes.Add("onclick", "")
            End If
            'Try
            invs = Request.QueryString("inv").ToString
            lblcode.Value = invs
            divcode.InnerHtml = invs
            lbloldcode.Value = invs

            lbltransstatus.Value = "0"
            lbltranstype.Value = "wo"

            inv.Open()
            SrchInv()
            GetPickList()
            inv.Dispose()
            
            'Catch ex As Exception

            'End Try    
        Else
            If Request.Form("lblsubmit") = "get" Then
                lblsubmit.Value = ""
                inv.Open()
                SrchInv()
                GetPickList()
                inv.Dispose()

            ElseIf Request.Form("lblsubmit") = "save" Then
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "addinv" Then
                lblsubmit.Value = ""
                inv.Open()
                AddInv()
                inv.Dispose()
            ElseIf Request.Form("lblsubmit") = "issueitems" Then
                lblsubmit.Value = ""
                inv.Open()
                issueinv()
                inv.Dispose()
            ElseIf Request.Form("lblsubmit") = "buildlist" Then
                lblsubmit.Value = ""
                inv.Open()
                buildpicklist()
                inv.Dispose()
            End If
        End If
    End Sub
    Private Sub buildpicklist()
        Dim wonum, seed As String
        seed = lbltransid.Value
        wonum = lblwo.Value
        sql = "usp_buildpicklist '" & wonum & "', '" & seed & "'"
        inv.Update(sql)

    End Sub
    Private Sub issueinv()
        'usp_issueinv(@transid int, @transtype varchar(2), @wonum int, @enterby varchar(50))
        Dim seed, pmtyp, wonum, enterby, store, typ, ostore, lot, bin As String
        seed = lbltransid.Value
        pmtyp = lbltype.Value
        wonum = lblwo.Value
        store = lblnewstore.Value
        ostore = lblstore.Value
        typ = lbltranstype.Value
        lot = lbllot.Value
        'bin = txtbin.text
        enterby = HttpContext.Current.Session("username").ToString()
        Dim icost As String = ap.InvEntry
        sql = "usp_issueinv2 '" & typ & "','" & seed & "','" & pmtyp & "','" & wonum & "','" & enterby & "','" & store & "','" & icost & "','" & ostore & "','" & lot & "'" ','" & bin & "'
        seed = inv.strScalar(sql)
        lbltransid.Value = seed
        GetPickList()
    End Sub
    Private Sub AddInv()
        '@invid int, @pmtyp varchar(4), @invs varchar(50), @desc varchar(50), @loc varchar(50) = null,
        '@bin varchar(50), @cost decimal(10,2) = null, @seed int = null
        Dim invid, pmtyp, invs, desc, loc, bin, cost, seed, qty, jp, pm, wo, ns, typ, tojp, lot As String
        invid = lblitemid.Value
        pmtyp = lbltype.Value
        invs = lblcode.Value
        desc = lblcodedesc.Value
        loc = lblstore.Value
        bin = lblbin.Value
        cost = lblstdcost.Value
        seed = lbltransid.Value
        qty = txtqty.Text
        jp = lbljpid.Value
        pm = lblpmid.Value
        wo = lblwo.Value
        ns = lblnewstore.Value
        typ = lbltranstype.Value
        lot = lbllot.Value
        If cbtojp.Checked = True Then
            tojp = "yes"
        Else
            tojp = "no"
        End If
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt64(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1241" , "issueinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "usp_invpicklist2 '" & qty & "','" & invid & "','" & pmtyp & "','" & invs & "','" & desc & "','" & loc & "', " _
        + "'" & bin & "','" & cost & "','" & seed & "','" & typ & "','" & jp & "','" & pm & "','" & wo & "','" & ns & "', " _
        + "'" & tojp & "','" & lot & "'"
        seed = inv.strScalar(sql)
        lbltransid.Value = seed
        GetPickList()
    End Sub
    Private Sub GetPickList()
        Dim seed, wonum, newloc, oldloc As String '= lbltransid.Value
        Dim transtype As String = lbltranstype.Value
        Dim transfilt As String = ""
        If transtype = "wo" Then
            transfilt = " wonum is not null"
        Else
            transfilt = " newlocation is not null"
        End If
        sql = "select * from invpicklist where " & transfilt
        dr = inv.GetRdrData(sql)
        While dr.Read
            seed = dr.Item("transid").ToString
            lbltransid.Value = seed
            wonum = dr.Item("wonum").ToString
            newloc = dr.Item("newlocation").ToString
            oldloc = dr.Item("location").ToString
        End While
        dr.Close()

        If transtype = "wo" Then
            lblwo.Value = wonum
            divwo.InnerHtml = wonum
            
            lblnewstore.Value = ""
            divnewstore.InnerHtml = ""
        Else
            lblnewstore.Value = newloc
            divnewstore.InnerHtml = newloc

            lblwo.Value = ""
            divwo.InnerHtml = ""
            
        End If
        sql = "select * from invpicklist where transid = '" & seed & "'"
        dr = inv.GetRdrData(sql)
        dgparttasks.DataSource = dr
        dgparttasks.DataBind()
        dr.Close()
        If seed = "" Or seed = "0" Then
            tdstat.InnerHtml = "No Items to Issue or Transfer"
        Else
            tdstat.InnerHtml = "Current Issue\Transfer Incomplete"
        End If
    End Sub
    Private Sub SrchInv()
        'Dim invs As String = txtcode.Text
        'sql = "select i.*, v.*, l.description as 'ldesc', b.curbal from item i " _
        '+ "left join inventory v on v.itemnum = i.itemnum " _
        '+ "left join locations l on l.location = v.location " _
        '+ "left join invbalances b on b.itemnum = i.itemnum where i.itemnum = '" & invs & "'"
        Dim storechange As String = lblstorechange.Value
        Dim typ As String = ddtype.SelectedValue.ToString
        Dim invs As String = lblcode.Value
        Dim filt As String
        If storechange = "yes" Then
            filt = lblstore.Value
            lblstorechange.Value = ""
        End If

        Dim transtype As String = lbltranstype.Value
        Dim transfilt As String = ""
        If transtype = "wo" Then
            transfilt = " and p.wonum is not null"
        ElseIf transtype = "sr" Then
            transfilt = " and p.newlocation is not null"
        End If
        Dim seed As String
        If typ = "part" Then
            If filt = "" Then
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.lotnum, v.location as loc, l.description as 'ldesc', b.curbal, " _
               + "seed = (select top 1 p.transid from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "wonum = (select top 1 p.wonum from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "newloc = (select top 1 p.newlocation from invpicklist p where p.itemnum = '" & invs & "') from item i " _
                + "left join inventory v on v.itemnum = i.itemnum " _
                + "left join invvallist l on l.value = v.location " _
                + "left join invpicklist p on p.itemnum = i.itemnum " _
                + "left join invbalances b on b.itemnum = i.itemnum and b.location = v.location where i.itemnum = '" & invs & "'"
            Else
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.lotnum, b.location as loc, l.description as 'ldesc', b.curbal, " _
                + "seed = (select top 1 p.transid from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "wonum = (select top 1 p.wonum from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "newloc = (select top 1 p.newlocation from invpicklist p where p.itemnum = '" & invs & "') from item i " _
                + "left join inventory v on v.itemnum = i.itemnum " _
                + "left join invbalances b on b.itemnum = i.itemnum " _
                + "left join invvallist l on l.value = b.location " _
                + "where i.itemnum = '" & invs & "'"
                sql += " and b.location = '" & filt & "'"
            End If

        ElseIf typ = "tool" Then
            If filt = "" Then
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.lotnum, v.location as loc, l.description as 'ldesc', b.curbal, " _
                + "seed = (select top 1 p.transid from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "wonum = (select top 1 p.wonum from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "newloc = (select top 1 p.newlocation from invpicklist p where p.itemnum = '" & invs & "')  from tool i " _
                + "left join inventory v on v.itemnum = i.toolnum " _
                + "left join invvallist l on l.value = v.location " _
                + "left join invbalances b on b.itemnum = i.toolnum and b.location = v.location where i.toolnum = '" & invs & "'"
            Else
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.lotnum, b.location as loc, l.description as 'ldesc', b.curbal, " _
                + "seed = (select top 1 p.transid from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "wonum = (select top 1 p.wonum from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "newloc = (select top 1 p.newlocation from invpicklist p where p.itemnum = '" & invs & "')  from tool i " _
               + "left join inventory v on v.itemnum = i.toolnum " _
               + "left join invbalances b on b.itemnum = i.toolnum " _
               + "left join invvallist l on l.value = b.location " _
               + "where i.toolnum = '" & invs & "'"
                sql += " and b.location = '" & filt & "'"
            End If

        ElseIf typ = "lube" Then
            If filt = "" Then
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.lotnum, v.location as loc, l.description as 'ldesc', b.curbal, " _
                + "seed = (select top 1 p.transid from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "wonum = (select top 1 p.wonum from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "newloc = (select top 1 p.newlocation from invpicklist p where p.itemnum = '" & invs & "')  from lubricants i " _
                + "left join inventory v on v.itemnum = i.lubenum " _
                + "left join invvallist l on l.value = v.location " _
                + "left join invbalances b on b.itemnum = i.lubenum and b.location = v.location where i.lubenum = '" & invs & "'"
            Else
                sql = "select i.*, v.*, v.lastissuedate as alast, b.lastissuedate as last, b.lotnum, b.location as loc, l.description as 'ldesc', b.curbal, " _
                + "seed = (select top 1 p.transid from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "wonum = (select top 1 p.wonum from invpicklist p where p.itemnum = '" & invs & "'), " _
                + "newloc = (select top 1 p.newlocation from invpicklist p where p.itemnum = '" & invs & "')  from lubricants i " _
                + "left join inventory v on v.itemnum = i.lubenum " _
                + "left join invbalances b on b.itemnum = i.lubenum " _
                + "left join invvallist l on l.value = b.location " _
                + "where i.lubenum = '" & invs & "'"
                sql += " and b.location = '" & filt & "'"

            End If

        End If

        'Dim store As String = txtstore.Text
        'If store <> "" Then
        'sql += " and i.location = '" & store & "'"
        'End If
        Dim loc, lot, wonum, newloc, itemid As String
        dr = inv.GetRdrData(sql)
        While dr.Read
            lblitemid.Value = dr.Item("itemid").ToString
            itemid = dr.Item("itemid").ToString
            lblcode.Value = dr.Item("itemnum").ToString
            divcode.InnerHtml = dr.Item("itemnum").ToString
            lbloldcode.Value = dr.Item("itemnum").ToString
            divdesc.InnerHtml = dr.Item("description").ToString
            lblcodedesc.Value = dr.Item("description").ToString
            lblstore.Value = dr.Item("loc").ToString
            divstore.InnerHtml = dr.Item("loc").ToString
            loc = dr.Item("loc").ToString
            lot = dr.Item("lotnum").ToString
            'txtstdesc.Text = dr.Item("ldesc").ToString
            lblbin.Value = dr.Item("bin").ToString
            lblstdcost.Value = dr.Item("stdcost").ToString
            lbltransid.Value = dr.Item("seed").ToString
            'txtres.Text = dr.Item("reserved").ToString
            'Try
            'ddcat.SelectedValue = dr.Item("category").ToString
            'Catch ex As Exception

            'End Try
            lblcurbal.Value = dr.Item("curbal").ToString
            divcurbal.InnerHtml = dr.Item("curbal").ToString
            'txtlast.Text = dr.Item("lastissuedate").ToString
            'txtytd.Text = dr.Item("issueytd").ToString
            'txtabc.Text = dr.Item("abctype").ToString
            'txtcnt.Text = dr.Item("ccf").ToString
            'txt1yr.Text = dr.Item("issue1yrago").ToString
            'txt2yr.Text = dr.Item("issue2yrago").ToString
            'txt3yr.Text = dr.Item("issue3yrago").ToString
            lblsubmit.Value = "new"
            lbltype.Value = typ
            wonum = dr.Item("wonum").ToString
            newloc = dr.Item("newloc").ToString
        End While
        dr.Close()



        Dim sexp, savail, scb As Decimal
        Dim slot As Integer

        Try
            If itemid <> "" Then
                sql = "select count(*) from invlot l  " _
            + "left join invbalances i on i.lotnum = l.lotnum and i.location = '" & loc & "' " _
            + "where l.itemid = '" & itemid & "'"
                slot = inv.Scalar(sql)
                lbllotcnt.Value = slot
            Else
                slot = 0
                lbllotcnt.Value = slot
            End If
            
            
        Catch ex As Exception
            slot = 0
            lbllotcnt.Value = slot
        End Try
        If slot = 0 Or slot = 1 Then
            imglot.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
        End If
        If slot = 1 Then
            lbllot.Value = lot
            divlot.InnerHtml = lot
        End If
        Try
            If itemid <> "" Then
                sql = "select sum(l.curbal) from invlot l  " _
           + "left join invbalances i on i.lotnum = l.lotnum and i.location = '" & loc & "' " _
           + "where l.itemid = '" & itemid & "' and Convert(char(10),l.useby, 101) <= Convert(char(10),getDate(), 101)"
                sexp = inv.Scalar(sql)
            Else
                sexp = 0
            End If
           
        Catch ex As Exception

        End Try
        lblsexp.Value = sexp
        divsexp.InnerHtml = sexp
        Dim cbstr As String = lblcurbal.Value
        Try
            scb = CType(cbstr, Decimal)
        Catch ex As Exception
            scb = 0
        End Try

        savail = scb - sexp
        lblsavail.Value = savail
        divsavail.InnerHtml = savail
        Dim res, exp, avail, cb As Decimal
        'Dim cbstr As String '= txtcurbal.Text
        'cb = CType(cbstr, Decimal)
        Try
            sql = "select sum(curbal) from invbalances where itemnum = '" & invs & "'"
            cb = inv.Scalar(sql)
        Catch ex As Exception
            cb = 0
        End Try
        lbltotbal.Value = cb
        divtotbal.innerhtml = cb
        Try
            sql = "select sum(reserved) from invbalances where itemnum = '" & invs & "'"
            res = inv.Scalar(sql)
        Catch ex As Exception
            res = 0
        End Try
        Try
            sql = "select sum(curbal) from invlot where itemnum = '" & invs & "' and Convert(char(10),useby, 101) <= Convert(char(10),getDate(), 101)"
            exp = inv.Scalar(sql)
        Catch ex As Exception
            exp = 0
        End Try

        avail = cb - (res + exp)
        lblres.Value = res
        divres.InnerHtml = res
        lblexp.Value = exp
        divexp.InnerHtml = exp
        lblavail.Value = avail
        divavail.Visible = avail
    End Sub

    Private Sub dgparttasks_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.EditCommand
        dgparttasks.EditItemIndex = e.Item.ItemIndex
        inv.Open()
        GetPickList()
        inv.Dispose()
    End Sub

    Private Sub dgparttasks_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.CancelCommand
        dgparttasks.EditItemIndex = -1
        inv.Open()
        GetPickList()
        inv.Dispose()
    End Sub

    Private Sub dgparttasks_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.UpdateCommand
        Dim qty As String = CType(e.Item.FindControl("dgqty"), TextBox).Text
        Dim qtychk As Long
        Try
            qtychk = System.Convert.ToInt32(qty)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1242" , "issueinv.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim id As String = CType(e.Item.FindControl("lbltskprtidre"), Label).Text
        sql = "update invpicklist set qty = '" & qty & "' where plid = '" & id & "'"
        inv.Open()
        inv.Update(sql)
        sql = "update invpicklist set total = qty * itemcost where plid = '" & id & "'"
        Try

        Catch ex As Exception
            inv.Update(sql)
        End Try
        dgparttasks.EditItemIndex = -1
        GetPickList()
        inv.Dispose()
    End Sub

    Private Sub dgparttasks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgparttasks.DeleteCommand
        Dim id As String
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            id = CType(e.Item.FindControl("lbltskprtidr"), Label).Text
        Else
            id = CType(e.Item.FindControl("lbltskprtidre"), Label).Text
        End If
        sql = "delete from invpicklist where plid = '" & id & "'"
        inv.Open()
        inv.Update(sql)
        dgparttasks.EditItemIndex = -1
        GetPickList()
        inv.Dispose()
    End Sub
	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgparttasks.Columns(0).HeaderText = dlabs.GetDGPage("issueinv.aspx","dgparttasks","0")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(1).HeaderText = dlabs.GetDGPage("issueinv.aspx","dgparttasks","1")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2746.Text = axlabs.GetASPXPage("issueinv.aspx","lang2746")
		Catch ex As Exception
		End Try
		Try
			lang2747.Text = axlabs.GetASPXPage("issueinv.aspx","lang2747")
		Catch ex As Exception
		End Try
		Try
			lang2748.Text = axlabs.GetASPXPage("issueinv.aspx","lang2748")
		Catch ex As Exception
		End Try
		Try
			lang2749.Text = axlabs.GetASPXPage("issueinv.aspx","lang2749")
		Catch ex As Exception
		End Try
		Try
			lang2750.Text = axlabs.GetASPXPage("issueinv.aspx","lang2750")
		Catch ex As Exception
		End Try
		Try
			lang2751.Text = axlabs.GetASPXPage("issueinv.aspx","lang2751")
		Catch ex As Exception
		End Try
		Try
			lang2752.Text = axlabs.GetASPXPage("issueinv.aspx","lang2752")
		Catch ex As Exception
		End Try
		Try
			lang2753.Text = axlabs.GetASPXPage("issueinv.aspx","lang2753")
		Catch ex As Exception
		End Try
		Try
			lang2754.Text = axlabs.GetASPXPage("issueinv.aspx","lang2754")
		Catch ex As Exception
		End Try
		Try
			lang2755.Text = axlabs.GetASPXPage("issueinv.aspx","lang2755")
		Catch ex As Exception
		End Try
		Try
			lang2756.Text = axlabs.GetASPXPage("issueinv.aspx","lang2756")
		Catch ex As Exception
		End Try
		Try
			lang2757.Text = axlabs.GetASPXPage("issueinv.aspx","lang2757")
		Catch ex As Exception
		End Try
		Try
			lang2758.Text = axlabs.GetASPXPage("issueinv.aspx","lang2758")
		Catch ex As Exception
		End Try
		Try
			lang2759.Text = axlabs.GetASPXPage("issueinv.aspx","lang2759")
		Catch ex As Exception
		End Try
		Try
			lang2760.Text = axlabs.GetASPXPage("issueinv.aspx","lang2760")
		Catch ex As Exception
		End Try
		Try
			lang2761.Text = axlabs.GetASPXPage("issueinv.aspx","lang2761")
		Catch ex As Exception
		End Try
		Try
			lang2762.Text = axlabs.GetASPXPage("issueinv.aspx","lang2762")
		Catch ex As Exception
		End Try
		Try
			lang2763.Text = axlabs.GetASPXPage("issueinv.aspx","lang2763")
		Catch ex As Exception
		End Try
		Try
			lang2764.Text = axlabs.GetASPXPage("issueinv.aspx","lang2764")
		Catch ex As Exception
		End Try
		Try
			lang2765.Text = axlabs.GetASPXPage("issueinv.aspx","lang2765")
		Catch ex As Exception
		End Try
		Try
			lang2766.Text = axlabs.GetASPXPage("issueinv.aspx","lang2766")
		Catch ex As Exception
		End Try
		Try
			lang2767.Text = axlabs.GetASPXPage("issueinv.aspx","lang2767")
		Catch ex As Exception
		End Try
		Try
			lang2768.Text = axlabs.GetASPXPage("issueinv.aspx","lang2768")
		Catch ex As Exception
		End Try
		Try
			lang2769.Text = axlabs.GetASPXPage("issueinv.aspx","lang2769")
		Catch ex As Exception
		End Try
		Try
			lang2770.Text = axlabs.GetASPXPage("issueinv.aspx","lang2770")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			Img1.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("issueinv.aspx","Img1") & "')")
			Img1.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgbuild.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("issueinv.aspx","imgbuild") & "')")
			imgbuild.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgcomp.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("issueinv.aspx","imgcomp") & "', ABOVE, LEFT)")
			imgcomp.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgprint.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("issueinv.aspx","imgprint") & "', ABOVE, LEFT)")
			imgprint.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
