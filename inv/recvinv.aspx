<%@ Page Language="vb" AutoEventWireup="false" Codebehind="recvinv.aspx.vb" Inherits="lucy_r12.recvinv" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>recvinv</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/recvinvaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--

         function GetItem(typ, fld1, fld2) {
             if (typ == "srch") {
                 list = document.getElementById("ddtype").value;
             }
             else {
                 list = typ;
             }
             var eReturn = window.showModalDialog("invdialog.aspx?filt=no&list=" + list + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
             if (eReturn) {
                 if (eReturn == "log") {
                     window.parent.handlelogout();
                 }
                 else if (eReturn != "can") {
                     var ret = eReturn;
                     var retarr = ret.split(";")
                     if (typ == "srch") {
                         if (fld1 == "alt") {
                             document.getElementById("lblchildid").value = retarr[0];
                             document.getElementById("lblchild").value = retarr[1];
                             var ro = document.getElementById("lblro").value;
                             if (ro != "1") {
                                 document.getElementById("lblsubmit").value = "addchild";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else {
                             document.getElementById("lblcode").value = retarr[1];
                             document.getElementById("lblsubmit").value = "get";
                             document.getElementById("form1").submit();
                         }
                     }
                     else if (typ == "storeroom") {
                         document.getElementById("lblstore").value = retarr[1];
                         document.getElementById("divstore").innerHTML = retarr[1];
                         //document.getElementById(fld2).value = retarr[2];
                     }
                     else {
                         document.getElementById(fld1).value = retarr[1];
                         document.getElementById(fld1).innerHTML = retarr[1];
                     }

                 }
             }
         }
         function GetPR() {
             var item = document.getElementById("lblcode").value;
             var eReturn = window.showModalDialog("reclookupdialog.aspx?item=" + item, "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
             if (eReturn) {
                 if (eReturn != "") {
                     document.getElementById("lblpr").value = eReturn;
                     document.getElementById("divpr").innerHTML = eReturn;
                     var ro = document.getElementById("lblro").value;
                     if (ro != "1") {
                         document.getElementById("lblsubmit").value = "get";
                         document.getElementById("form1").submit();
                     }
                 }
             }
         }
         function getcal(fld, fld2) {
             var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
             if (eReturn) {
                 document.getElementById(fld).value = eReturn;
                 document.getElementById(fld2).innerHTML = eReturn;
             }
         }
         function checkinv() {
             var chk = document.getElementById("lblsubmit").value;
             if (chk == "new") {
                 document.getElementById("lblsubmit").value == "";
                 var ni = document.getElementById("lblcode").value;
                 var ty = document.getElementById("lbltype").value;
                 window.parent.handlenew(ni, ty);
             }
         }
         function saveloc() {
             var pr = document.getElementById("lblpr").value;
             var po = document.getElementById("txtoutpo").value;
             var rd = document.getElementById("lblrecdate").value;
             var qr = document.getElementById("txtqtyrecvd").value;
             var act = document.getElementById("txtactcost").value;
             var store = document.getElementById("lblstore").value;
             if (pr == "") {
                 if (po == "") {
                     alert("No Purchase Requisition or Purchase Order Provided")
                 }
                 else if (act == "") {
                     alert("Actual Unit Price Required")
                 }
                 else if (rd == "") {
                     alert("Received Date Required")
                 }
                 else if (qr == "") {
                     alert("Qty Received Required")
                 }
                 else if (store == "") {
                     alert("Store Room to Receive Items Required")
                 }
                 else {
                     var ro = document.getElementById("lblro").value;
                     if (ro != "1") {
                         document.getElementById("lblsubmit").value = "saverec";
                         document.getElementById("form1").submit();
                     }
                 }
             }
             else if (act == "") {
                 alert("Actual Unit Price Required")
             }
             else if (rd == "") {
                 alert("Received Date Required")
             }
             else if (qr == "") {
                 alert("Qty Received Required")
             }
             else if (store == "") {
                 alert("Store Room to Receive Items Required")
             }
             else {
                 var ro = document.getElementById("lblro").value;
                 if (ro != "1") {
                     document.getElementById("lblsubmit").value = "saverec";
                     document.getElementById("form1").submit();
                 }
             }
         }
         function checkit() {
             window.setTimeout("setref();", 1205000);
         }
         function setref() {
             window.parent.setref();
         }
         
     //--> 
     </script>
      <style type="text/css">
          .readonly50
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 50px;
}
         .readonly90
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 90px;
}
     .readonly140
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 140px;
}
.readonly340
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 340px;
}
.readonly440
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 530px;
}
</style>
	</HEAD>
	<body class="tbg" onload="checkinv();checkit();" >
		<form id="form1" method="post" runat="server">
		<div style="position: absolute; top: 4px; left: 4px">
			<table cellSpacing="0" width="800">
			<tr>
				<td align="center" valign="middle" class="plainlabelred" colspan="2"><asp:Label id="lang2860" runat="server">Use the standard PM Inventory module if your Inventory is controlled outside of Fusion.</asp:Label></td>
				</tr>
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2861" runat="server">Item Details</asp:Label></td>
				</TR>
				
				<tr>
					<td vAlign="top" colSpan="2">
						<table cellPadding="0" width="680" border="0">
							<tr>
								<td width="100"></td>
								<td width="180"></td>
								<td width="80"></td>
								<td width="80"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="172"></td>
							</tr>
							<tr id="Tr5" runat="server">
								<td class="label"><asp:Label id="lang2862" runat="server">Item Code</asp:Label></td>
								<td><div class="readonly140" id="divcode" runat="server">
                                        </div></td>
								<td class="label"><asp:Label id="lang2863" runat="server">Item Type</asp:Label></td>
								<td><asp:dropdownlist id="ddtype" runat="server" CssClass="plainlabel">
										<asp:ListItem Value="part" Selected="True">Part</asp:ListItem>
										<asp:ListItem Value="tool">Tool</asp:ListItem>
										<asp:ListItem Value="lube">Lube</asp:ListItem>
									</asp:dropdownlist></td>
								<td><IMG id="imgsrch" onmouseover="return overlib('Look up Inventory Items')" onclick="GetItem('srch');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif" runat="server"></td>
								<td></td>
								<td><IMG onmouseover="return overlib('Clear Page')" onclick="jumpto('');" onmouseout="return nd()"
										src="../images/appbuttons/minibuttons/refreshit.gif"></td>
								<td></td>
							</tr>
							<tr id="deptdiv" runat="server">
								<td class="label"><asp:Label id="lang2864" runat="server">Description</asp:Label></td>
								<td colSpan="10"><div class="readonly440" id="divdesc" runat="server">
                                        </div></td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2865" runat="server">Order Details</asp:Label></td>
				</TR>
				<tr>
					<td colSpan="2">
						<table>
							<TBODY>
								<tr>
									<td class="plainlabelred" id="tdprcnt" align="center" colSpan="8" runat="server"></td>
								</tr>
								<tr>
									<td width="120"></td>
									<td width="140"></td>
									<td width="20"></td>
									<td width="120"></td>
									<td width="110"></td>
									<td width="100"></td>
									<td width="110"></td>
									<td width="20"></td>
									<td width="10"></td>
									<td width="20"></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang2866" runat="server">Purchase Req#</asp:Label></td>
									<td><div class="readonly140" id="divpr" runat="server">
                                        </div></td>
									<td><IMG id="Img2" onmouseover="return overlib('Look up current Purchase Requisitions')"
											onclick="GetPR();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif"
											runat="server"></td>
									<td class="label"><asp:Label id="lang2867" runat="server">Order Date</asp:Label></td>
									<td><div class="readonly90" id="divorderdate" runat="server">
                                        </div></td>
									<td class="label"><asp:Label id="lang2868" runat="server">Receive Date</asp:Label></td>
									<td><div class="readonly90" id="divrecdate" runat="server">
                                        </div></td>
									<td><IMG onmouseover="return overlib('Select Receive Date')" onclick="getcal('lblrecdate', 'divrecdate');"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
									</td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang2869" runat="server">Purchase Order#</asp:Label></td>
									<td><asp:textbox id="txtoutpo" runat="server" Width="140px" CssClass="plainlabel"></asp:textbox></td>
									<td class="plainlabelblue" colSpan="6"><asp:Label id="lang2870" runat="server">*Required if no Purchase Requisition# Available or Provided</asp:Label></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang2871" runat="server">Lot Type</asp:Label></td>
									<td><div class="readonly140" id="divlottype" runat="server">
                                        </div></td>
									<td><IMG id="imglot" onmouseover="return overlib('Select a Lot Type')" onclick="GetItem('lottype', 'lbllottype', 'divloctype');"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif" runat="server"></td>
									<td class="label"><asp:Label id="lang2872" runat="server">Lot Number</asp:Label></td>
									<td><asp:textbox id="txtlot" runat="server" Width="100px" CssClass="plainlabel"></asp:textbox></td>
									<td class="label"><asp:Label id="lang2873" runat="server">Use By</asp:Label></td>
									<td><div class="readonly90" id="divuseby" runat="server">
                                        </div></td>
									<td><IMG onmouseover="return overlib('Select Use By Date')" onclick="getcal('lbluseby', 'divuseby');"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
									</td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang2874" runat="server">Unit Price</asp:Label></td>
									<td class="plainlabel" runat="server"><div class="readonly90" id="divunit" runat="server">
                                        </div></td>
									<td></td>
									<td class="label"><asp:Label id="lang2875" runat="server">Actual Unit Price</asp:Label></td>
									<td><asp:textbox id="txtactcost" runat="server" Width="80px" CssClass="plainlabel"></asp:textbox></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang2876" runat="server">Qty Ordered</asp:Label></td>
									<td><div class="readonly90" id="divqtyordered" runat="server">
                                        </div></td>
									<td></td>
									<td class="label"><asp:Label id="lang2877" runat="server">Qty Received</asp:Label></td>
									<td><asp:textbox id="txtqtyrecvd" runat="server" Width="50px" CssClass="plainlabel"></asp:textbox></td>
									<td class="label"><asp:Label id="lang2878" runat="server">Store Room</asp:Label></td>
									<td colSpan="4"><div class="readonly140" id="divstore" runat="server">
                                        </div></td>
									<td><IMG onmouseover="return overlib('Select Store Room where this item will be received')"
											onclick="GetItem('storeroom', 'lblstore');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
									<td align="right"><IMG id="Img1" onmouseover="return overlib('Save Receive Information')" onclick="saveloc();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif" runat="server"></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2879" runat="server">Vendor \ Mfg Details</asp:Label></td>
				</TR>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td class="label" width="90"><asp:Label id="lang2880" runat="server">Vendor</asp:Label></td>
								<td width="145"><div class="readonly140" id="divpvend" runat="server">
                                        </div></td>
								<td width="20"><IMG id="Img4" onmouseover="return overlib('Look up Vendor')" onclick="getcomp('V', 'lblpvend', 'divvdesc');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif" runat="server" class="details"></td>
								<td width="295"><div class="readonly340" id="divvdesc" runat="server">
                                        </div></td>
								<td class="label" width="60"><asp:Label id="lang2881" runat="server">Catalog#</asp:Label></td>
								<td width="80"><div class="readonly90" id="divcatnum" runat="server">
                                        </div></td>
							</tr>
							<tr>
								<td class="label" width="90"><asp:Label id="lang2882" runat="server">Manufacturer</asp:Label></td>
								<td width="145"><div class="readonly140" id="divman" runat="server">
                                        </div></td>
								<td width="20"><IMG id="Img5" onmouseover="return overlib('Look up Manufacturer')" onclick="getcomp('M', 'lblman', 'divmandesc');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif" runat="server" class="details"></td>
								<td width="295"><div class="readonly340" id="divmandesc" runat="server">
                                        </div></td>
								<td class="label" width="60"><asp:Label id="lang2883" runat="server">Model#</asp:Label></td>
								<td width="80"><div class="readonly90" id="divmod" runat="server">
                                        </div></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
			<input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblfail" type="hidden" name="lblfail" runat="server">
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="lblchild" type="hidden" name="lblchild" runat="server">
			<input id="lbltype" type="hidden" name="lbltype" runat="server"> <input id="lbloldcode" type="hidden" name="lbloldfail" runat="server">
			<input id="lblneweq" type="hidden" name="lblneweq" runat="server"> <input id="lblcodeid" type="hidden" name="lblcodeid" runat="server">
			<input id="lblchildid" type="hidden" name="lblchildid" runat="server"><input id="lblaltid" type="hidden" name="lblaltid" runat="server">
			<input id="lblro" type="hidden" runat="server">
            <input type="hidden" id="lblcode" runat="server" />
            <input type="hidden" id="lblcodedesc" runat="server" />
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblpr" runat="server" />
<input type="hidden" id="lblrecdate" runat="server" />
<input type="hidden" id="lblorderdate" runat="server" />
<input type="hidden" id="lbllottype" runat="server" />
<input type="hidden" id="lbluseby" runat="server" />
<input type="hidden" id="lblqtyordered" runat="server" />
<input type="hidden" id="lblstore" runat="server" />
<input type="hidden" id="lblpvend" runat="server" />
<input type="hidden" id="lblman" runat="server" />
<input type="hidden" id="lblcatnum" runat="server" />
<input type="hidden" id="lblmod" runat="server" />

</form>
	</body>
</HTML>
