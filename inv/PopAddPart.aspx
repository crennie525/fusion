<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PopAddPart.aspx.vb" Inherits="lucy_r12.PopAddPart" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PopAddPart</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/PopAddPartaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function checkadd() {
             var chk = document.getElementById("lblchk").value;
             if (chk == "ok") {
                 window.parent.handleexit("ok");
             }
             
         }
     </script>
	</HEAD>
	<body bgColor="white"  onload="checkadd();">
		<form id="form1" method="post" runat="server">
			<table align="center">
				<tr>
					<td class="bluelabel" width="80" height="26" id="tdplbl" runat="server"><asp:Label id="lang2823" runat="server">New Part#</asp:Label></td>
					<td width="250">
						<asp:textbox style="Z-INDEX: 0" id="txtpart" runat="server" MaxLength="50" Width="200px"></asp:textbox></td>
				</tr>
				<tr>
					<td class="bluelabel" width="80"><asp:Label id="lang2824" runat="server">Description</asp:Label></td>
					<td width="250"><asp:textbox id="txtdesc" runat="server" Width="250px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td class="bluelabel" width="80"><asp:Label id="lang2825" runat="server">Location</asp:Label></td>
					<td width="250"><asp:textbox id="txtloc" runat="server" Width="200px" MaxLength="50"></asp:textbox></td>
				</tr>
				<tr>
					<td class="bluelabel" width="80" id="tdclbl" runat="server"><asp:Label id="lang2826" runat="server">Cost</asp:Label></td>
					<td width="250"><asp:textbox id="txtcost" runat="server" Width="40px"></asp:textbox></td>
				</tr>
				<tr>
					<td align="center" colSpan="2"><INPUT style="WIDTH: 70px" onclick="checkcost();" type="button" value="OK" id="btnadd"
							name="Button1" runat="server"> &nbsp; <INPUT style="WIDTH: 70px" onclick="handlecancel();" type="button" value="Cancel"></td>
				</tr>
			</table>
			<input id="lblchk" type="hidden" runat="server"> <input type="hidden" id="lblpage" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
