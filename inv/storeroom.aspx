<%@ Page Language="vb" AutoEventWireup="false" Codebehind="storeroom.aspx.vb" Inherits="lucy_r12.storeroom1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>storeroom</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/storeroomaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function checkinv() {
             var chk = document.getElementById("lblsubmit").value;
             if (chk == "new") {
                 document.getElementById("lblsubmit").value == "";
                 var ni = document.getElementById("lblcode").value;
                 var ty = document.getElementById("lbltype").value;
                 window.parent.handlenew(ni, ty);
             }
         }
         function GetItem(typ, fld1, fld2) {
             if (typ == "srch") {
                 list = document.getElementById("ddtype").value;
                 filt = "no"
             }
             else {
                 list = typ;
                 filt = document.getElementById("lblitemid").value;
             }
             if (typ == "storeroom" && filt == "") {
                 alert("No Item Selected")
             }
             else {
                 var eReturn = window.showModalDialog("invdialog.aspx?list=" + list + "&filt=" + filt + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
                 if (eReturn) {
                     if (eReturn == "log") {
                         window.parent.handlelogout();
                     }
                     else if (eReturn != "can") {
                         var ret = eReturn;
                         var retarr = ret.split(";")
                         if (typ == "srch") {
                             if (fld1 == "alt") {
                                 document.getElementById("lblchildid").value = retarr[0];
                                 document.getElementById("lblchild").value = retarr[1];
                                 document.getElementById("lblsubmit").value = "addchild";
                                 document.getElementById("form1").submit();
                             }
                             else {
                                 document.getElementById("lblcode").value = retarr[1];
                                 document.getElementById("lblsubmit").value = "get";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else if (typ == "storeroom") {
                             document.getElementById(fld1).value = retarr[1];
                             document.getElementById("lblstore").value = retarr[1];
                             document.getElementById("lblsubmit").value = "getstore";
                             document.getElementById("form1").submit();
                             //document.getElementById(fld2).value = retarr[2];
                         }
                         else {
                             document.getElementById(fld1).value = retarr[1];
                         }

                     }
                 }
             }
         }
         function getadj() {
             var itemid = document.getElementById("lblitemid").value;
             var item = document.getElementById("lblcode").value;
             var store = document.getElementById("lblstore").value;
             var curbal = document.getElementById("lblcurbal").value;
             var ro = document.getElementById("lblro").value;
             var eReturn = window.showModalDialog("adjinvdialog.aspx?itemid=" + itemid + "&item=" + item + "&store=" + store + "&curbal=" + curbal + "&date=" + Date() + "&ro=" + ro, "", "dialogHeight:520px; dialogWidth:370px; resizable=yes");
             if (eReturn) {
                 document.getElementById("lblsubmit").value = "getstore";
                 document.getElementById("form1").submit();
             }
         }
     //-->
     </script>
     <style type="text/css">
          .readonly50
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 50px;
}
         .readonly90
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 90px;
}
     .readonly140
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 140px;
}
.readonly340
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 340px;
}
.readonly440
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 530px;
}
</style>
	</HEAD>
	<body onload="checkinv();checkit();"  class="tbg">
		<form id="form1" method="post" runat="server">
		<div style="position: absolute; top: 4px; left: 4px">
			<table style="LEFT: 2px; POSITION: absolute; TOP: 2px" cellSpacing="0" cellPadding="2"
				width="722">
				<tr>
					<td width="280"></td>
					<td width="5"></td>
					<td width="437"></td>
				</tr>
				<tr>
				<td align="center" valign="middle" class="plainlabelred" colspan="3"><asp:Label id="lang2940" runat="server">Use the standard PM Inventory module if your Inventory is controlled outside of Fusion.</asp:Label></td>
				</tr>
				<TR>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2941" runat="server">Storeroom</asp:Label></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td vAlign="top" colSpan="3">
						<table cellPadding="0" border="0">
							<tr>
								<td width="110"></td>
								<td width="80"></td>
								<td width="80"></td>
								<td width="32"></td>
								<td width="80"></td>
								<td width="80"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="172"></td>
							</tr>
							<tr id="Tr5" runat="server">
								<td class="label"><asp:Label id="lang2942" runat="server">Item Code</asp:Label></td>
								<td colSpan="3"><div class="readonly140" id="divcode" runat="server">
                                        </div></td>
								<td class="label"><asp:Label id="lang2943" runat="server">Item Type</asp:Label></td>
								<td><asp:dropdownlist id="ddtype" runat="server" CssClass="plainlabel">
										<asp:ListItem Value="part" Selected="True">Part</asp:ListItem>
										<asp:ListItem Value="tool">Tool</asp:ListItem>
										<asp:ListItem Value="lube">Lube</asp:ListItem>
									</asp:dropdownlist></td>
								<td><IMG id="imgsrch" onclick="GetItem('srch');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('Look up Inventory Items')" onmouseout="return nd()"></td>
								<td><IMG onclick="jumpto('');" src="../images/appbuttons/minibuttons/refreshit.gif" onmouseover="return overlib('Clear Page')"
										onmouseout="return nd()"></td>
								<td><IMG id="Img1" class="details" onclick="saveloc();" src="../images/appbuttons/minibuttons/savedisk1.gif"
										runat="server" onmouseover="return overlib('Save Stock Category and Default Bin Changes')"
										onmouseout="return nd()"></td>
							</tr>
							<tr id="deptdiv" runat="server">
								<td class="label"><asp:Label id="lang2944" runat="server">Description</asp:Label></td>
								<td colSpan="11"><div class="readonly440" id="divdesc" runat="server">
                                        </div></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang2945" runat="server">Store Room</asp:Label></td>
								<td colSpan="2"><div class="readonly140" id="divstore" runat="server">
                                        </div></td>
								<td><IMG onclick="GetItem('storeroom', 'divstore');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
										onmouseover="return overlib('Switch Store Room for this Item if it is stored in multiple locations')"
										onmouseout="return nd()"></td>
								<td colSpan="8"><div class="readonly340" id="divstdesc" runat="server">
                                        </div></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang2946" runat="server">Stock Category</asp:Label></td>
								<td><div class="readonly50" id="divcat" runat="server">
                                        </div></td>
								<td class="label" colSpan="2"><asp:Label id="lang2947" runat="server">Default Bin</asp:Label></td>
								<td colSpan="2">
                                    <asp:TextBox ID="txtbin" runat="server" CssClass="plainlabel" Width="150"></asp:TextBox></td>
                                <td><IMG id="Img2" onmouseover="return overlib('Save Default Bin Changes')" onclick="saveloc();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif" runat="server" ></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2948" runat="server">Balance Summary</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td vAlign="top" colSpan="3">
						<table width="710">
							<tr>
								<td width="120"></td>
								<td width="100"></td>
								<td width="30"></td>
								<td width="120"></td>
								<td width="120"></td>
								<td width="120"></td>
								<td width="100"></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang2949" runat="server">Current Balance</asp:Label></td>
								<td><div class="readonly90" id="divcurval" runat="server">
                                        </div></td>
								<td><img src="../images/appbuttons/minibuttons/adjscrew.gif" onclick="getadj();" onmouseover="return overlib('Adjust Current Balance (Physical Count Difference, Removed Expired Items) - Does not apply to Issues, Transfers, or Receiving Transactions')"
										onmouseout="return nd()"></td>
								<td class="label"><asp:Label id="lang2950" runat="server">Last Issue</asp:Label></td>
								<td colSpan="3"><div class="readonly140" id="divlast" runat="server">
                                        </div></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang2951" runat="server">Qty Reserved</asp:Label><font class="plainlabelblue">*</font></td>
								<td><div class="readonly90" id="divres" runat="server">
                                        </div></td>
								<td></td>
								<td class="label"><asp:Label id="lang2952" runat="server">Qty Expired</asp:Label><font class="plainlabelblue">*</font></td>
								<td><div class="readonly90" id="divexp" runat="server">
                                        </div></td>
								<td class="label"><asp:Label id="lang2953" runat="server">Qty Available</asp:Label><font class="plainlabelblue">*</font></td>
								<td><div class="readonly90" id="divavail" runat="server">
                                        </div></td>
							</tr>
							<tr height="20">
								<td class="plainlabelblue" align="center" colSpan="7"><asp:Label id="lang2954" runat="server">*Quantities Reserved, Expired, and Available refers to all stock on hand</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2955" runat="server">Balances</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdalt" colSpan="3" runat="server"></td>
				</tr>
			</table>
			</div>
			<input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblfail" type="hidden" name="lblfail" runat="server">
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="lblchild" type="hidden" name="lblchild" runat="server">
			<input id="lbltype" type="hidden" name="lbltype" runat="server"> <input id="lbloldcode" type="hidden" name="lbloldfail" runat="server">
			<input id="lblneweq" type="hidden" name="lblneweq" runat="server"> <input type="hidden" id="lblitemid" runat="server">
			<input type="hidden" id="lblstore" runat="server"><input type="hidden" id="lblro" runat="server">
		<input type="hidden" id="lblcode" runat="server" />
        <input type="hidden" id="lblcurbal" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
