<%@ Page Language="vb" AutoEventWireup="false" Codebehind="partsmain.aspx.vb" Inherits="lucy_r12.partsmain" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>partsmain</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 4px; POSITION: absolute; TOP: 0px" cellSpacing="0" cellPadding="2"
				width="760">
				<tr>
					<td width="120"></td>
					<td width="180"></td>
					<td width="460"></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:label id="lblpart" runat="server" Width="480px" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"
							ForeColor="Red"></asp:label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2822" runat="server">Search Part#</asp:Label></td>
					<td><asp:textbox id="txtsrch" runat="server" Width="170px"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
							CssClass="imgbutton"></asp:imagebutton></td>
				</tr>
				<tr>
					<TD colSpan="3"><asp:datagrid id="dgparts" runat="server" CellSpacing="2" CellPadding="1" GridLines="None" AllowPaging="True"
							AllowCustomPaging="True" AutoGenerateColumns="False" ShowFooter="True" BackColor="transparent">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle cssclass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle cssclass="ptransrow"></ItemStyle>
							<HeaderStyle Height="22px"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Height="20px" Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="ImageButton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<FooterTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
											CommandName="Add"></asp:ImageButton>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="ImageButton13" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="ImageButton14" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id="lblitemidn" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblitemid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Part#">
									<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label3 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:textbox id="txtpname" runat="server" Width="100px" MaxLength="50"></asp:textbox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=TextBox1 runat="server" Width="75px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Description">
									<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label5 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:textbox id="txtpdesc" runat="server" Width="240px" MaxLength="100"></asp:textbox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=TextBox2 runat="server" Width="245px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>' TextMode="MultiLine">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Location">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label6 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:textbox id="txtploc" runat="server" Width="150px" MaxLength="50"></asp:textbox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=Textbox3 runat="server" Width="135px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>' TextMode="MultiLine">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Cost">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label21 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="txtcostf" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtcost runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.cost") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remove">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
											CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Part#" Visible="False">
									<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</tr>
				<tr>
					<td colSpan="3">&nbsp;</td>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblold" type="hidden" name="lblold" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
