<%@ Page Language="vb" AutoEventWireup="false" Codebehind="invmainmenu.aspx.vb" Inherits="lucy_r12.invmainmenu" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="pgtitle" runat="server">invmainmenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" src="../scripts/inventory2.js"></script>
		<script language="JavaScript" src="../scripts1/invmainmenuaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 4px; POSITION: absolute; TOP: 81px; z-index: 1;" cellSpacing="1" cellPadding="0"
				width="860">
				<tr>
					<td colSpan="7">
						<table cellSpacing="0">
							<tr height="22">
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2742" runat="server">PM Inventory Administration</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><IMG height="6" src="../images/appbuttons/minibuttons/6PX.gif" width="6"></td>
				</tr>
				<tr>
					<td class="thdrhov plainlabel" id="parttab" onclick="gettab('part');" width="100">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td><IMG src="../images/appbuttons/minibuttons/parttrans2.gif" border="0"></td>
								<td class="plainlabel" vAlign="middle"><asp:Label id="lang2743" runat="server">Parts</asp:Label></td>
							</tr>
						</table>
					</td>
					<td class="thdr plainlabel" id="tooltab" onclick="gettab('tool');" width="100">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td><IMG src="../images/appbuttons/minibuttons/tooltrans2.gif" border="0"></td>
								<td class="plainlabel" valign="middle"><asp:Label id="lang2744" runat="server">Tools</asp:Label></td>
							</tr>
						</table>
					</td>
					<td class="thdr plainlabel" id="lubetab" onclick="gettab('lube');" width="100">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td><IMG src="../images/appbuttons/minibuttons/lubetrans2.gif" border="0"></td>
								<td class="plainlabel" valign="middle"><asp:Label id="lang2745" runat="server">Lubricants</asp:Label></td>
							</tr>
						</table>
					</td>
					<td width="140"></td>
					<td width="140"></td>
					<td width="140"></td>
					<td width="140"></td>
				</tr>
				<tr>
					<td colspan="7">
						<iframe id="ifmain" src="partsmain.aspx?ro=" frameBorder="no" scrolling="auto" style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; LEFT: 138px; BORDER-LEFT: 2px groove; WIDTH: 780px; BORDER-BOTTOM: 2px groove; HEIGHT: 450px; BACKGROUND-COLOR: transparent"
							runat="server" allowtransparency></iframe>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblcid" runat="server"> <input type="hidden" id="lblro" runat="server">
			
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
