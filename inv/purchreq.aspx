<%@ Page Language="vb" AutoEventWireup="false" Codebehind="purchreq.aspx.vb" Inherits="lucy_r12.purchreq" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>purchreq</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/purchreqaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function GetItem(typ, fld1, fld2) {
             if (typ == "srch") {
                 list = document.getElementById("ddtype").value;
             }
             else {
                 list = typ;
             }
             var eReturn = window.showModalDialog("invdialog.aspx?filt=no&list=" + list + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
             if (eReturn) {
                 if (eReturn == "log") {
                     window.parent.handlelogout();
                 }
                 else if (eReturn != "can") {
                     var ret = eReturn;
                     var retarr = ret.split(";")
                     if (typ == "srch") {
                         if (fld1 == "alt") {
                             document.getElementById("lblchildid").value = retarr[0];
                             document.getElementById("lblchild").value = retarr[1];
                             var ro = document.getElementById("lblro").value;
                             if (ro != "1") {
                                 document.getElementById("lblsubmit").value = "addchild";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else {
                             document.getElementById("lblsrch").value = retarr[1];
                             document.getElementById("lblsubmit").value = "get";
                             document.getElementById("form1").submit();
                         }
                     }
                     else if (typ == "storeroom") {
                         document.getElementById(fld1).value = retarr[1];
                         document.getElementById(fld2).value = retarr[2];
                     }
                     else {
                         document.getElementById(fld1).value = retarr[1];
                     }

                 }
             }
         }
         function getcomp(typ, tbox1, tbox2) {
             var eReturn = window.showModalDialog("../admin/admindialog.aspx?list=comp&typ=" + typ, "", "dialogHeight:500px; dialogWidth:640px; resizable=yes");
             if (eReturn) {
                 if (eReturn != "") {
                     var eret = eReturn.split(";");
                     document.getElementById(tbox1).value = eret[0];
                     document.getElementById(tbox2).innerHTML = eret[0];
                     document.getElementById("lblsubmit").value = "get";
                     document.getElementById("form1").submit();
                 }
             }
         }
     //-->
     
     </script>
      <style type="text/css">
          .readonly50
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 50px;
}
         .readonly90
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 90px;
}
     .readonly140
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 140px;
}
.readonly340
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 340px;
}
.readonly440
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 530px;
}
</style>
	</HEAD>
	<body  onload="checkit();" class="tbg">
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" width="800">
				<TR>
					<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
					<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2828" runat="server">Purchase Requisitions</asp:Label></td>
				</TR>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td class="label" width="60"><asp:Label id="lang2829" runat="server">Item#</asp:Label></td>
								<td width="145"><div class="readonly140" id="divsrch" runat="server">
                                        </div></td>
								<td><asp:dropdownlist id="ddtype" runat="server">
										<asp:ListItem Value="part" Selected="True">Part</asp:ListItem>
										<asp:ListItem Value="tool">Tool</asp:ListItem>
										<asp:ListItem Value="lube">Lube</asp:ListItem>
									</asp:dropdownlist></td>
								<td width="20"><IMG id="imgsrch" onclick="GetItem('srch');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('Look up Inventory Items')" onmouseout="return nd()"></td>
								<td class="label" width="60"><asp:Label id="lang2830" runat="server">Vendor</asp:Label></td>
								<td width="145"><div class="readonly140" id="divpvend" runat="server">
                                        </div></td>
								<td width="20"><IMG id="Img4" onclick="getcomp('V', 'lblpvend', 'divpvend');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('Look up Vendor')" onmouseout="return nd()"></td>
								<td class="label" width="70"><asp:Label id="lang2831" runat="server">Status</asp:Label></td>
								<td width="200"><asp:dropdownlist id="ddstat" runat="server" CssClass="plainlabel" AutoPostBack="True">
										<asp:ListItem Value="ALL">ALL</asp:ListItem>
										<asp:ListItem Value="WAPPR">WAPPR - Waiting to be Approved</asp:ListItem>
										<asp:ListItem Value="APPR">APPR - Approved</asp:ListItem>
										<asp:ListItem Value="INPRG">INPRG - In Progress</asp:ListItem>
										<asp:ListItem Value="COMP">COMP - Complete</asp:ListItem>
										<asp:ListItem Value="CAN">CAN - Cancelled</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdpr" colSpan="2" runat="server"></td>
				</tr>
				<tr>
					<td align="center" colSpan="2">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input type="hidden" id="lblsubmit" runat="server"> <input type="hidden" id="lblret" runat="server" NAME="lblret">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblsrch" runat="server" />
<input type="hidden" id="lblpvend" runat="server" />
</form>
	</body>
</HTML>
