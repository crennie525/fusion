<%@ Page Language="vb" AutoEventWireup="false" Codebehind="InventoryTools.aspx.vb" Inherits="lucy_r12.InventoryTools" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>InventoryTools</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/InventoryToolsaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="2" width="760" style="LEFT: 4px; POSITION: absolute; TOP: 0px">
				<tr>
					<td width="120"></td>
					<td width="180"></td>
					<td width="460"></td>
				</tr>
				<tr>
					<td colspan="3"><asp:label id="lbltool" runat="server" ForeColor="Red" Width="480px" Font-Bold="True" Font-Names="Arial"
							Font-Size="X-Small"></asp:label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2734" runat="server">Search Tool#</asp:Label></td>
					<td><asp:textbox id="txtsrch" runat="server" Width="170px"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<TD colspan="3"><asp:datagrid id="dgtools" runat="server" CellPadding="1" CellSpacing="1" GridLines="None" AllowPaging="True"
							AllowCustomPaging="True" AutoGenerateColumns="False" ShowFooter="True" BackColor="transparent">
							<FooterStyle BackColor="transparent"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle cssclass="ptransrowblue"></AlternatingItemStyle>
							<ItemStyle cssclass="ptransrow"></ItemStyle>
							<HeaderStyle Height="22px"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Height="20px" Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<FooterTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
											CommandName="Add"></asp:ImageButton>
									</FooterTemplate>
									<EditItemTemplate>
										&nbsp;
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label id="lbltoolidn" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.toolid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lbltoolid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.toolid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Tool#">
									<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label8 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.toolnum") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:textbox id="txttname" runat="server" Width="100px" MaxLength="50"></asp:textbox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=Textbox6 runat="server" Width="75px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.toolnum") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Description">
									<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label9 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:textbox id="txttdesc" runat="server" Width="240px" MaxLength="100"></asp:textbox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=Textbox7 runat="server" Width="245px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>' TextMode="MultiLine">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Location">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label10 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:textbox id="txttloc" runat="server" Width="150px" MaxLength="50"></asp:textbox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=Textbox8 runat="server" Width="135px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>' TextMode="MultiLine">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Rate">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label21 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.toolrate") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="txtcostf" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.toolrate") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtcost runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.toolrate") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remove">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
											CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Tool#" Visible="False">
									<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.toolnum") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.toolnum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblcid" runat="server" NAME="lblcid"><input type="hidden" id="txtpg" runat="server" NAME="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"><input type="hidden" id="lblold" runat="server" NAME="lblold">
			<input type="hidden" id="lbllog" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
