<%@ Page Language="vb" AutoEventWireup="false" Codebehind="issueinv.aspx.vb" Inherits="lucy_r12.issueinv" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>issueinv</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/issueinvaspx1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function checkinv() {
             var chk = document.getElementById("lblsubmit").value;
             if (chk == "new") {
                 document.getElementById("lblsubmit").value == "";
                 var ni = document.getElementById("lblcode").value;
                 var ty = document.getElementById("lbltype").value;
                 window.parent.handlenew(ni, ty);
             }
         }
         function GetItem(typ, fld1, fld2) {
         var typchk = document.getElementById("lbltranstype").value;
         if (typchk == "wo" && fld1 == "lblnewstore") {
             alert("Transfer Type is to Work Order")
         }
         else {
             if (typ == "srch") {
                 list = document.getElementById("ddtype").value;
                 filt = "no"
             }
             else {
                 list = typ;
                 if (fld1 == "lblnewstore") {
                     filt = "no"
                 }
                 else {
                     filt = document.getElementById("lblitemid").value;
                 }

             }
             if (typ == "storeroom" && filt == "") {
                 alert("No Item Selected")
             }
             else {
                 var eReturn = window.showModalDialog("invdialog.aspx?list=" + list + "&filt=" + filt + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
                 if (eReturn) {
                     if (eReturn == "log") {
                         window.parent.handlelogout();
                     }
                     else if (eReturn != "can") {
                         var ret = eReturn;
                         var retarr = ret.split(";")
                         if (typ == "srch") {
                             if (fld1 == "alt") {
                                 document.getElementById("lblchildid").value = retarr[0];
                                 document.getElementById("lblchild").value = retarr[1];
                                 var ro = document.getElementById("lblro").value;
                                 if (ro != "1") {
                                     document.getElementById("lblsubmit").value = "addchild";
                                     document.getElementById("form1").submit();
                                 }
                             }
                             else {
                                 document.getElementById("lblcode").value = retarr[1];
                                 document.getElementById("lblsubmit").value = "get";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else if (typ == "storeroom") {
                             document.getElementById(fld1).value = retarr[1];
                             if (fld1 == "lblnewstore") {
                                 document.getElementById("divnewstore").innerHTML = retarr[1];
                             }
                             document.getElementById(fld2).value = retarr[2];
                             //alert(retarr[2])
                             if (fld1 == "lblstore") {
                                 document.getElementById("lblstore").value = retarr[1];
                                 document.getElementById("lblsubmit").value = "get";
                                 document.getElementById("lblstorechange").value = "yes";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else {
                             document.getElementById(fld1).value = retarr[1];
                         }

                     }
                 }
             }
         }
         }
         function GetLocEq() {
             var item = document.getElementById("lbloldcode").value;
             var typ = document.getElementById("lbltype").value;
             if (item != "" && typ == "part") {
                 var eReturn = window.showModalDialog("admindialog.aspx?list=eq&typ=eq", "", "dialogHeight:500px; dialogWidth:640px; resizable=yes");
                 if (eReturn) {
                     var ret = eReturn.split(";");
                     document.getElementById("lbleqid").value = ret[3];
                     var ro = document.getElementById("lblro").value;
                     if (ro != "1") {
                         document.getElementById("lblsubmit").value = "saveeq";
                         document.getElementById("form1").submit();
                     }
                 }
             }
         }
         function checkcomp() {
             var decision = confirm("You will no longer be able to print this Pick List\nAre you sure you want to Continue?")
             if (decision == true) {
                 var chk = document.getElementById("lbltransid").value;
                 var stat = document.getElementById("lbltransstatus").value;
                 var typ = document.getElementById("lbltranstype").value;
                 var wo = document.getElementById("lblwo").value;
                 var ns = document.getElementById("txtnewstore").value;
                 if (chk == "") {
                     alert("No Issue\Transfer Items Entered")
                 }

                 else if (stat == "1") {
                     alert("These Items have already been Issued or Transferred")
                 }
                 else {
                     if (typ == "wo") {
                         if (wo != "") {
                             var ro = document.getElementById("lblro").value;
                             if (ro != "1") {
                                 document.getElementById("lblsubmit").value = "issueitems";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else {
                             alert("No Work Order Selected")
                         }
                     }
                     else if (typ == "sr") {
                         if (ns != "") {
                             var ro = document.getElementById("lblro").value;
                             if (ro != "1") {
                                 document.getElementById("lblsubmit").value = "issueitems";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else {
                             alert("No Store Room Selected")
                         }
                     }

                 }
             }
             else {
                 alert("Action Cancelled")
             }
             
         }
         function buildwo() {
             var wo = document.getElementById("lblwo").value;
             if (wo != "") {
                 var ro = document.getElementById("lblro").value;
                 if (ro != "1") {
                     document.getElementById("lblsubmit").value = "buildlist";
                     document.getElementById("form1").submit();
                 }
             }
             else {
                 alert("No Work Order Selected")
             }
         }
         function printwo() {
             var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=0,scrollbars=1";
             var chk = document.getElementById("lbltransid").value;
             var seed = document.getElementById("lbltransid").value;
             var typ = document.getElementById("lbltranstype").value;
             var wo = document.getElementById("lblwo").value;
             var ns = document.getElementById("txtnewstore").value;
             //alert(wo)
             if (chk == "") {
                 alert("No Issue\Transfer Items Entered")
             }
             else {
                 window.open("printpicklist.aspx?seed=" + seed + "&wonum=" + wo + "&store=" + ns + "&typ=" + typ + "&date=" + Date(), "repWin", popwin);
             }
         }
         function lookwo() {
             var typchk = document.getElementById("lbltranstype").value;
             if (typchk == "wo") {
                 var eReturn = window.showModalDialog("../appswo/wolistminidialog.aspx?date=" + Date(), "", "dialogHeight:520px; dialogWidth:760px; resizable=yes");
                 if (eReturn) {
                     var ret = eReturn;
                     var retarr = ret.split(",")
                     document.getElementById("divwo").innerHTML = retarr[0];
                     document.getElementById("lblwo").value = retarr[0];
                     //document.getElementById("txtwodesc").value = retarr[1];
                     document.getElementById("lbljpid").value = retarr[2];
                     document.getElementById("lblpmid").value = retarr[3];
                     if (retarr[2] != "0") {
                         document.getElementById("tdtojp").className = "view";
                     }
                     //document.getElementById("lblsubmit").value = "getlist";
                     //document.getElementById("form1").submit();
                 }
             }
             else {
                alert("Transfer Type is to Another Storeroom")
             }
        }
        function lotlook() {
            var chk = document.getElementById("lbllotcnt").value;
            if (chk != "0") {
                var itemid = document.getElementById("lblitemid").value;
                var store = document.getElementById("lblstore").value;
                var eReturn = window.showModalDialog("lotlookdialog.aspx?itemid=" + itemid + "&store=" + store + "&date=" + Date(), "", "dialogHeight:520px; dialogWidth:370px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "") {
                        document.getElementById("lbllot").value = eReturn
                        document.getElementById("divlot").innerHTML = eReturn
                    }
                }
            }
        }
        function checkcomp() {
            var chk = document.getElementById("lbltransid").value;
            var stat = document.getElementById("lbltransstatus").value;
            var typ = document.getElementById("lbltranstype").value;
            var wo = document.getElementById("lblwo").value;
            var ns = document.getElementById("lblnewstore").value;
            if (chk == "") {
                alert("No Issue\Transfer Items Entered")
            }
            else if (stat == "1") {
                alert("These Items have already been Issued or Transferred")
            }
            else {
                if (typ == "wo") {
                    if (wo != "") {
                        var ro = document.getElementById("lblro").value;
                        if (ro != "1") {
                            document.getElementById("lblsubmit").value = "issueitems";
                            document.getElementById("form1").submit();
                        }
                    }
                    else {
                        alert("No Work Order Selected")
                    }
                }
                else if (typ == "sr") {
                    if (ns != "") {
                        var ro = document.getElementById("lblro").value;
                        if (ro != "1") {
                            document.getElementById("lblsubmit").value = "issueitems";
                            document.getElementById("form1").submit();
                        }
                    }
                    else {
                        alert("No Store Room Selected")
                    }
                }

            }
        }
        
        function addloc() {
            var item = document.getElementById("lblcode").value;
            var qty = document.getElementById("txtqty").value;
            if (item == "") {
                alert("No Item Selected")
            }
            else if (qty == "") {
                alert("Qty Required")
            }
            else {
                var tt = document.getElementById("lbltranstype").value;
                var wo = document.getElementById("lblwo").value;
                var sr = document.getElementById("lblnewstore").value;
                if (tt == "wo" && wo == "") {
                    alert("Please Choose a Work Order# to Issue Items To")
                }
                else if (tt == "sr" && sr == "") {
                    alert("Please Choose a Store Room to Transfer Items To")
                }
                else {
                    var ro = document.getElementById("lblro").value;
                    if (ro != "1") {
                        document.getElementById("lblsubmit").value = "addinv";
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
         //-->
     </script>
     <style type="text/css">
          .readonly50
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 50px;
}
         .readonly90
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 90px;
}
     .readonly140
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 140px;
}
.readonly340
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 340px;
}
.readonly440
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 530px;
}
</style>
	</HEAD>
	<body class="tbg" onload="checkinv();checkit();" >
		<form id="form1" method="post" runat="server">
		<div style="position: absolute; top: 4px; left: 4px">
			<table style="LEFT: 2px; POSITION: absolute; TOP: 2px" cellSpacing="0" cellPadding="2"
				width="722">
				<tr>
					<td width="280"></td>
					<td width="5"></td>
					<td width="437"></td>
				</tr>
				<tr>
				<td align="center" valign="middle" class="plainlabelred" colspan="3"><asp:Label id="lang2746" runat="server">Use the standard PM Inventory module if your Inventory is controlled outside of Fusion.</asp:Label></td>
				</tr>
				<TR>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2747" runat="server">Item\Storeroom Details</asp:Label></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td vAlign="top" colSpan="3">
						<table cellPadding="0" border="0">
							<tr>
								<td width="120"></td>
								<td width="60"></td>
								<td width="80"></td>
								<td width="22"></td>
								<td width="70"></td>
								<td width="20"></td>
								<td width="50"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="60"></td>
								<td width="40"></td>
								<td width="122"></td>
							</tr>
							<tr id="Tr5" runat="server">
								<td class="label"><asp:Label id="lang2748" runat="server">Item Code</asp:Label></td>
								<td colSpan="3"><div class="readonly140" id="divcode" runat="server">
                                        </div></td>
								<td class="label"><asp:Label id="lang2749" runat="server">Item Type</asp:Label></td>
								<td colSpan="2"><asp:dropdownlist id="ddtype" runat="server" CssClass="plainlabel">
										<asp:ListItem Value="part" Selected="True">Part</asp:ListItem>
										<asp:ListItem Value="tool">Tool</asp:ListItem>
										<asp:ListItem Value="lube">Lube</asp:ListItem>
									</asp:dropdownlist></td>
								<td><IMG id="imgsrch" onclick="GetItem('srch');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server"></td>
								<td></td>
								<td class="label" colSpan="3"><asp:Label id="lang2750" runat="server">Issue Qty</asp:Label></td>
								<td><asp:textbox id="txtqty" runat="server" Width="40px" CssClass="plainlabelblue"></asp:textbox></td>
								<td><IMG id="Img1" onmouseover="return overlib('Add Item Qty to Issue List')" onclick="addloc();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif" runat="server"></td>
							</tr>
							<tr id="deptdiv" runat="server">
								<td class="label"><asp:Label id="lang2751" runat="server">Description</asp:Label></td>
								<td colSpan="13"><div class="readonly440" id="divdesc" runat="server">
                                        </div></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang2752" runat="server">Store Room</asp:Label></td>
								<td colSpan="3"><div class="readonly140" id="divstore" runat="server">
                                        </div></td>
								<td><IMG id="Img3" onclick="GetItem('storeroom', 'txtstore');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server"></td>
								<td></td>
								<td class="label" colSpan="3"><asp:Label id="lang2753" runat="server">Lot Number</asp:Label></td>
								<td colSpan="4"><div class="readonly90" id="divlot" runat="server">
                                        </div></td>
								<td><IMG id="imglot" onclick="lotlook();" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server"></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang2754" runat="server">Current Balance</asp:Label></td>
								<td><div class="readonly50" id="divcurbal" runat="server">
                                        </div></td>
								<td class="label" colSpan="2"><asp:Label id="lang2755" runat="server">Qty Expired</asp:Label></td>
								<td><div class="readonly50" id="divsexp" runat="server">
                                        </div></td>
								<td></td>
								<td class="label" colSpan="3"><asp:Label id="lang2756" runat="server">Qty Available</asp:Label></td>
								<td colSpan="4"><div class="readonly50" id="divsavail" runat="server">
                                        </div></td>
							</tr>
							<tr>
								<td class="bluelabel"><asp:Label id="lang2757" runat="server">Total Balance</asp:Label></td>
								<td><div class="readonly50" id="divtotbal" runat="server">
                                        </div></td>
								<td class="bluelabel" colSpan="2"><asp:Label id="lang2758" runat="server">Total Reserved</asp:Label><font class="plainlabelblue">*</font></td>
								<td><div class="readonly50" id="divres" runat="server">
                                        </div></td>
								<td></td>
								<td class="bluelabel" colSpan="3"><asp:Label id="lang2759" runat="server">Total Expired</asp:Label></td>
								<td colSpan="3"><div class="readonly50" id="divexp" runat="server">
                                        </div></td>
								<td class="bluelabel" colSpan="2"><asp:Label id="lang2760" runat="server">Total Available</asp:Label></td>
								<td><div class="readonly50" id="divavail" runat="server">
                                        </div></td>
							</tr>
							<tr height="20">
								<td class="plainlabelblue" align="center" colSpan="15"><asp:Label id="lang2761" runat="server">*Reserved Quantities Apply to Items Designated for any Work Order that is not Cancelled or Complete</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2762" runat="server">Issue\Transfer Details</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="3">
						<table width="740">
							<tr>
								<td width="120"></td>
								<td width="120"></td>
								<td width="20"></td>
								<td width="20"></td>
								<td width="400"></td>
								<td width="20"></td>
								<td width="20"></td>
							</tr>
							<tr height="20">
								<td class="bluelabel"><asp:Label id="lang2763" runat="server">Issue\Transfer To:</asp:Label></td>
								<td class="plainlabelblue" colSpan="4"><INPUT id="ro4" onclick="settrans();" type="radio" CHECKED name="rgrp2"><asp:Label id="lang2764" runat="server">Work Order</asp:Label><INPUT id="ro5" onclick="settrans();" type="radio" name="rgrp2"><asp:Label id="lang2765" runat="server">Another Store Room</asp:Label></td>
							</tr>
							<tr id="trwo" runat="server">
								<td class="label"><asp:Label id="lang2766" runat="server">Work Order#</asp:Label></td>
								<td><div class="readonly140" id="divwo" runat="server">
                                        </div></td>
								<td><IMG id="Img2" onclick="lookwo();" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server"></td>
								<td><IMG id="imgbuild" onmouseover="return overlib('Build Pick List from selected Work Order#')"
										onclick="buildwo();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/3pack.gif"
										runat="server"></td>
								<td class="plainlabelblue" id="tdtojp" runat="server"><input id="cbtojp" type="checkbox" runat="server"><asp:Label id="lang2767" runat="server">Issue Item to Job Plan (Single Item Entry Only)</asp:Label></td>
							</tr>
							<tr id="tdst" runat="server">
								<td class="label"><asp:Label id="lang2768" runat="server">Store Room</asp:Label></td>
								<td><div class="readonly140" id="divnewstore" runat="server">
                                        </div></td>
								<td colSpan="2"><IMG onclick="GetItem('storeroom', 'lblnewstore', 'divnstdesc');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								<td><div class="readonly340" id="divnstdesc" runat="server">
                                        </div></td>
								<td><IMG id="imgprint" onmouseover="return overlib('Print Current Pick List', ABOVE, LEFT)"
										onclick="printwo();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printex.gif"
										runat="server"></td>
								<td><IMG id="imgcomp" onmouseover="return overlib('Complete this Issue or Transfer', ABOVE, LEFT)"
										onclick="checkcomp();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/comp.gif"
										runat="server"></td>
							</tr>
							<tr class="details">
								<td class="label"><asp:Label id="lang2769" runat="server">Bin#</asp:Label></td>
								<td colSpan="3"><div class="readonly50" id="divbin" runat="server">
                                        </div></td>
								<td><IMG id="Img4" onclick="GetBin();" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server"></td>
							</tr>
							<tr height="20">
								<td class="plainlabelred" id="tdstat" align="center" colSpan="7" runat="server"><asp:Label id="lang2770" runat="server">Current Issue\Transfer Incomplete</asp:Label></td>
							</tr>
							<tr>
								<td colSpan="6">
									<div style="OVERFLOW: auto; WIDTH: 720px; HEIGHT: 180px"><asp:datagrid id="dgparttasks" runat="server" BackColor="transparent" cellSpacing="1" cellPadding="1"
											GridLines="None" AutoGenerateColumns="False">
											<AlternatingItemStyle cssclass="ptransrowblue"></AlternatingItemStyle>
											<ItemStyle CssClass="ptransrow"></ItemStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="Edit">
													<HeaderStyle Width="80px" CssClass="btmmenu plainlabel" Height="24"></HeaderStyle>
													<ItemTemplate>
														<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
															CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:imagebutton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
															CommandName="Update" ToolTip="Save Changes"></asp:imagebutton>
														<asp:imagebutton id="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
															CommandName="Cancel" ToolTip="Cancel Changes"></asp:imagebutton>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Item#">
													<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label id="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemnum") %>'>
														</asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn Visible="False">
													<ItemTemplate>
														<asp:Label id="lbltskprtidr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.plid") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label id="lbltskprtidre" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.plid") %>'>
														</asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Description">
													<HeaderStyle Width="270px" CssClass="btmmenu plainlabel"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemdesc") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label id="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemdesc") %>'>
														</asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Store Room">
													<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
														</asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Bin#">
													<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bin") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bin") %>'>
														</asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Qty">
													<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:TextBox id="dgqty" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="37px">
														</asp:TextBox>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Cost">
													<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemcost") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label id="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.itemcost") %>'>
														</asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Total">
													<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id="Label19" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label id="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.total") %>'>
														</asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Remove">
													<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
													<ItemTemplate>
														&nbsp;&nbsp;&nbsp;
														<asp:ImageButton id="Imagebutton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
															CommandName="Delete"></asp:ImageButton>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
			<input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblfail" type="hidden" name="lblfail" runat="server">
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="lblchild" type="hidden" name="lblchild" runat="server">
			<input id="lbltype" type="hidden" name="lbltype" runat="server"> <input id="lbloldcode" type="hidden" name="lbloldfail" runat="server">
			<input id="lblneweq" type="hidden" name="lblneweq" runat="server"> <input id="lblitemid" type="hidden" runat="server">
			<input id="lblbin" type="hidden" runat="server"> <input id="lbltransid" type="hidden" runat="server">
			<input id="lblstdcost" type="hidden" runat="server"> <input id="lblwo" type="hidden" runat="server">
			<input id="lbltransstatus" type="hidden" runat="server"> <input id="lbltranstype" type="hidden" runat="server">
			<input id="lblpmid" type="hidden" runat="server"> <input id="lbljpid" type="hidden" runat="server">
			<input id="lblstore" type="hidden" name="lblstore" runat="server"> <input id="lbllotcnt" type="hidden" runat="server">
			<input id="lblro" type="hidden" runat="server"> <input type="hidden" id="lblpickseeds" runat="server">
		<input type="hidden" id="lbllot" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblcode" runat="server" />
<input type="hidden" id="lblcurbal" runat="server" />
<input type="hidden" id="lblsexp" runat="server" />
<input type="hidden" id="lblsavail" runat="server" />
<input type="hidden" id="lbltotbal" runat="server" />
<input type="hidden" id="lblres" runat="server" />
<input type="hidden" id="lblexp" runat="server" />
<input type="hidden" id="lblavail" runat="server" />
<input type="hidden" id="lblnewstore" runat="server" />
<input type="hidden" id="lblstorechange" runat="server" />
<input type="hidden" id="lblcodedesc" runat="server" />
</form>
	</body>
</HTML>
