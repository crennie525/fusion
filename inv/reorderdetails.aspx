<%@ Page Language="vb" AutoEventWireup="false" Codebehind="reorderdetails.aspx.vb" Inherits="lucy_r12.reorderdetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>reorderdetails</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/reorderdetailsaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function getcomp(typ, tbox1, tbox2) {
             var eReturn = window.showModalDialog("../admin/admindialog.aspx?list=comp&typ=" + typ, "", "dialogHeight:500px; dialogWidth:640px; resizable=yes");
             if (eReturn) {
                 if (eReturn != "") {
                     var eret = eReturn.split(",");
                     document.getElementById(tbox1).value = eret[0];
                     document.getElementById(tbox2).value = eret[1];
                     if (typ == "V") {
                         document.getElementById("lblpvend").value = eret[0];
                         document.getElementById("lblpvdesc").value = eret[1];
                     }
                     else if (typ == "M") {
                         document.getElementById("lblman").value = eret[0];
                         document.getElementById("lblmandesc").value = eret[1];
                     }
                 }
             }
         }
         function GetItem(typ, fld1, fld2) {
             if (typ == "srch") {
                 list = document.getElementById("ddtype").value;
                 filt = "no"
             }
             else {
                 list = typ;
                 filt = document.getElementById("lblitemid").value;
             }
             if (typ == "storeroom" && filt == "") {
                 alert("No Item Selected")
             }
             else {
                 var eReturn = window.showModalDialog("invdialog.aspx?list=" + list + "&filt=" + filt + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
                 if (eReturn) {
                     if (eReturn == "log") {
                         window.parent.handlelogout();
                     }
                     else if (eReturn != "can") {
                         var ret = eReturn;
                         var retarr = ret.split(";")
                         if (typ == "srch") {
                             if (fld1 == "alt") {
                                 document.getElementById("lblchildid").value = retarr[0];
                                 document.getElementById("lblchild").value = retarr[1];
                                 document.getElementById("lblsubmit").value = "addchild";
                                 document.getElementById("form1").submit();
                             }
                             else {
                                 document.getElementById("lblcode").value = retarr[1];
                                 document.getElementById("lblsubmit").value = "get";
                                 document.getElementById("form1").submit();
                             }
                         }
                         else if (typ == "storeroom") {
                             document.getElementById(fld1).value = retarr[1];
                             document.getElementById("lblstore").value = retarr[1];
                             document.getElementById("lblsubmit").value = "getstore";
                             document.getElementById("form1").submit();
                             //document.getElementById(fld2).value = retarr[2];
                         }
                         else {
                             document.getElementById(fld1).value = retarr[1];
                             document.getElementById(fld2).innerHTML = retarr[1];
                             if (typ == "orderunit") {
                                 document.getElementById("lblconv").value = retarr[2];
                                 document.getElementById("divconv").innerHTML = retarr[2];
                             }
                         }

                     }
                 }
             }
         }
         function saveloc() {
             if (document.getElementById("lblcode").value != "") {
                 document.getElementById("lblsubmit").value = "save";
                 document.getElementById("form1").submit();
             }
         }
         function getcal(fld) {
             var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
             if (eReturn) {
                 document.getElementById(fld).value = eReturn;
                 document.getElementById("divrecdate").innerHTML = eReturn;
             }
         }
         function jumpto1(id) {
             var ro = document.getElementById("lblro").value;
             window.location = "reorderdetails.aspx?ro=" + ro;
         }
         function addpr() {
             var recd = document.getElementById("lblrecdate").value;
             if (recd != "") {
             /*
                 var pr = document.getElementById("lblprnum").value;
                 if (pr != "") {
                     var conf = confirm("There is an Open Purchase Requisition for this Item\nAre you sure you want to continue?")
                     if (conf == true) {
                         document.getElementById("lblsubmit").value = "addpr";
                         document.getElementById("form1").submit();
                     }
                     else {
                         alert("Action Cancelled")
                     }
                 }
                 else {
                 */
                     document.getElementById("lblsubmit").value = "addpr";
                     document.getElementById("form1").submit();
                 /*}*/
             }
             else {
                 alert("Request By Date Required")
             }
         }
         function checkinv() {
             var chk = document.getElementById("lblsubmit").value;
             if (chk == "new") {
                 document.getElementById("lblsubmit").value == "";
                 var ni = document.getElementById("lblcode").value;
                 var ty = document.getElementById("lbltype").value;
                 window.parent.handlenew(ni, ty);
             }
         }
         function getpr(prnum) {
             var invs = document.getElementById("lblcode").value;
             window.location = "purchreqedit.aspx?prnum=" + prnum + "&who=" + "reorderdetails.aspx?inv=" + invs;
         }
         function getcomp(typ, tbox1, tbox2, tbox3) {
             var eReturn = window.showModalDialog("../admin/admindialog.aspx?list=comp&typ=" + typ, "", "dialogHeight:500px; dialogWidth:640px; resizable=yes");
             if (eReturn) {
                 if (eReturn != "") {
                     var eret = eReturn.split(";");
                     document.getElementById(tbox1).value = eret[0];
                     document.getElementById(tbox3).innerHTML = eret[0];
                     document.getElementById(tbox2).innerHTML = eret[1];
                 }
             }
         }
         //-->
     </script>
     <style type="text/css">
          .readonly50
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 50px;
}
         .readonly90
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 90px;
}
     .readonly140
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 140px;
}
.readonly340
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 340px;
}
.readonly440
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 530px;
}
</style>
	</HEAD>
	<body onload="checkinv();"  class="tbg">
		<form id="form1" method="post" runat="server">
		<div style="position: absolute; top: 4px; left: 4px">
			<table style="LEFT: 2px; POSITION: absolute; TOP: 2px" cellSpacing="0" cellPadding="2"
				width="800">
				<tr>
					<td width="280"></td>
					<td width="5"></td>
					<td width="437"></td>
				</tr>
				<tr>
				<td align="center" valign="middle" class="plainlabelred" colspan="3"><asp:Label id="lang2884" runat="server">Use the standard PM Inventory module if your Inventory is controlled outside of Fusion.</asp:Label></td>
				</tr>
				<TR>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2885" runat="server">Storeroom</asp:Label></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td vAlign="top" colSpan="3">
						<table cellPadding="0" border="0">
							<tr>
								<td width="100"></td>
								<td width="180"></td>
								<td width="80"></td>
								<td width="80"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="22"></td>
								<td width="172"></td>
							</tr>
							<tr id="Tr5" runat="server">
								<td class="label"><asp:Label id="lang2886" runat="server">Item Code</asp:Label></td>
								<td><div class="readonly140" id="divcode" runat="server">
                                        </div></td>
								<td class="label"><asp:Label id="lang2887" runat="server">Item Type</asp:Label></td>
								<td><asp:dropdownlist id="ddtype" runat="server" CssClass="plainlabel">
										<asp:ListItem Value="part" Selected="True">Part</asp:ListItem>
										<asp:ListItem Value="tool">Tool</asp:ListItem>
										<asp:ListItem Value="lube">Lube</asp:ListItem>
									</asp:dropdownlist></td>
								<td><IMG id="imgsrch" onmouseover="return overlib('Look up Inventory Items')" onclick="GetItem('srch');"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/magnifier.gif" runat="server"></td>
								<td><IMG id="Img1" onmouseover="return overlib('Save selected reorder datails')" onclick="saveloc();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/savedisk1.gif" runat="server"></td>
								<td><IMG onclick="jumpto1('');" src="../images/appbuttons/minibuttons/refreshit.gif"></td>
								<td><IMG class="details" id="imgadd" onclick="addloc();" src="../images/appbuttons/minibuttons/addnew.gif"
										runat="server"></td>
							</tr>
							<tr id="deptdiv" runat="server">
								<td class="label" style="HEIGHT: 20px"><asp:Label id="lang2888" runat="server">Description</asp:Label></td>
								<td style="HEIGHT: 20px" colSpan="10"><div class="readonly440" id="divdesc" runat="server">
                                        </div></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang2889" runat="server">Store Room</asp:Label></td>
								<td><div class="readonly140" id="divstore" runat="server">
                                        </div></td>
								
								<td colSpan="9"><div class="readonly340" id="divstdesc" runat="server">
                                        </div></td>
							
                            </tr>
                            <tr class="details">
                            <td><IMG onclick="GetItem('storeroom', 'txtstore');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
										onmouseover="return overlib('Switch Store Room for this Item if it is stored in multiple locations')"
										onmouseout="return nd()"></td>
                            </tr>
						</table>
					</td>
				</tr>
				<TR>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2890" runat="server">Reorder Details</asp:Label></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td colSpan="3">
						<table>
							<tr>
								<td class="label" width="100"><asp:Label id="lang2891" runat="server">Reorder Point</asp:Label></td>
								<td width="100"><asp:textbox id="txtre" runat="server" Width="90px" CssClass="plainlabel"></asp:textbox></td>
								<td width="20"></td>
								<td class="label" width="100"><asp:Label id="lang2892" runat="server">Lead Time</asp:Label></td>
								<td width="100"><asp:textbox id="txtlead" runat="server" Width="90px" CssClass="plainlabel"></asp:textbox></td>
								<td width="20"></td>
								<td class="details" width="80"><asp:Label id="lang2893" runat="server">Issue Units</asp:Label></td>
								<td width="50" class="details"><asp:textbox id="txtiss" runat="server" ReadOnly="True" Width="40px" CssClass="plainlabel"></asp:textbox></td>
								<td width="20" class="details"><IMG id="Img2" onclick="GetItem('orderunit', 'txtiss');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('Select Order Unit')" onmouseout="return nd()"></td>
							</tr>
							<tr>
								<td class="label" width="100"><asp:Label id="lang2894" runat="server">Safety Stock</asp:Label></td>
								<td width="100"><asp:textbox id="txtsafe" runat="server" Width="90px" CssClass="plainlabel"></asp:textbox></td>
								<td width="20"></td>
								<td class="label" width="100"><asp:Label id="lang2895" runat="server">Order Qty</asp:Label></td>
								<td width="100"><asp:textbox id="txtqty" runat="server" Width="90px" CssClass="plainlabel"></asp:textbox></td>
								<td width="20"></td>
								<td class="label" width="80"><asp:Label id="lang2896" runat="server">Order Units</asp:Label></td>
								<td width="50"><div class="readonly50" id="divord" runat="server">
                                        </div></td>
								<td width="20"><IMG id="Img3" onclick="GetItem('orderunit', 'lblord', 'divord');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('Select Order Unit')" onmouseout="return nd()"></td>
								<td class="label" width="80"><asp:Label id="lang2897" runat="server">Conversion</asp:Label></td>
								<td width="50"><div class="readonly50" id="divconv" runat="server">
                                        </div></td>
							</tr>
							<tr>
								<td class="label" width="100"></td>
								<td width="100"></td>
								<td width="20"></td>
								<td class="label" width="100"></td>
								<td width="100"></td>
								<td width="20"></td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2898" runat="server">Vendors</asp:Label></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td colSpan="3">
						<table>
							<tr>
								<td class="label" width="90"><asp:Label id="lang2899" runat="server">Primary Vendor</asp:Label></td>
								<td width="145"><div class="readonly140" id="divpvend" runat="server">
                                        </div></td>
								<td width="20"><IMG id="Img4" onclick="getcomp('V', 'lblpvend', 'divvdesc', 'divpvend');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('Look up Primary Vendor')" onmouseout="return nd()"></td>
								<td width="295"><div class="readonly340" id="divvdesc" runat="server">
                                        </div></td>
								<td class="label" width="60"><asp:Label id="lang2900" runat="server">Catalog#</asp:Label></td>
								<td width="80"><asp:textbox id="txtcatnum" runat="server" Width="80px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
							<tr>
								<td class="label" width="90"><asp:Label id="lang2901" runat="server">Manufacturer</asp:Label></td>
								<td width="145"><div class="readonly140" id="divman" runat="server">
                                        </div></td>
								<td width="20"><IMG id="Img5" onclick="getcomp('M', 'lblman', 'divmandesc', 'divman');" src="../images/appbuttons/minibuttons/magnifier.gif"
										runat="server" onmouseover="return overlib('look up Manufacturer')" onmouseout="return nd()"></td>
								<td width="295"><div class="readonly340" id="divmandesc" runat="server">
                                        </div></td>
								<td class="label" width="60"><asp:Label id="lang2902" runat="server">Model#</asp:Label></td>
								<td width="80"><asp:textbox id="txtmod" runat="server" Width="80px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdalt" colSpan="3" runat="server"></td>
				</tr>
				<TR>
					<td colSpan="3">
						<table cellSpacing="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/lilparts2.gif" border="0"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2903" runat="server">Purchase Requisition Details</asp:Label></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td colSpan="3">
						<table>
							<tr>
								<td class="label"><asp:Label id="lang2904" runat="server">Add Purchase Rec</asp:Label></td>
								<td class="details"><asp:textbox id="txtpr" runat="server" ReadOnly="True" Width="100px" CssClass="plainlabel"></asp:textbox></td>
								<td><IMG id="Img6" onclick="addpr();" src="../images/appbuttons/minibuttons/addnew.gif" runat="server"></td>
								<td class="label"><asp:Label id="lang2905" runat="server">Request By Date</asp:Label></td>
								<td><div class="readonly140" id="divrecdate" runat="server">
                                        </div></td>
								<td><IMG onclick="getcal('lblrecdate');" src="../images/appbuttons/minibuttons/btn_calendar.jpg">
								</td>
								<td class="label"><asp:Label id="lang2906" runat="server">Ship To:</asp:Label></td>
								<td><asp:textbox id="txtshipto" runat="server" ReadOnly="False" Width="100px" CssClass="plainlabel"></asp:textbox></td>
								<td class="label"><asp:Label id="lang2907" runat="server">Attention:</asp:Label></td>
								<td><asp:textbox id="txtattn" runat="server" ReadOnly="False" Width="100px" CssClass="plainlabel"></asp:textbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdpr" colSpan="3" runat="server"></td>
				</tr>
			</table>
			</div>
			<div class="details" id="stordiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 400px; BORDER-BOTTOM: black 1px solid; HEIGHT: 320px">
				<table cellSpacing="0" cellPadding="0" width="400" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="whtlbl"><asp:Label id="lang2908" runat="server">Storeroom Lookup</asp:Label></td>
						<td align="right"><IMG onclick="closestore();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
								width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifstor" style="WIDTH: 400px; HEIGHT: 300px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<div class="details" id="matdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 400px; BORDER-BOTTOM: black 1px solid; HEIGHT: 320px">
				<table cellSpacing="0" cellPadding="0" width="400" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="whtlbl"><asp:Label id="lang2909" runat="server">Material Lookup</asp:Label></td>
						<td align="right"><IMG onclick="closemat();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
								width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifmat" style="WIDTH: 400px; HEIGHT: 300px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<div class="details" id="valdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 400px; BORDER-BOTTOM: black 1px solid; HEIGHT: 320px">
				<table cellSpacing="0" cellPadding="0" width="400" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="whtlbl"><asp:Label id="lang2910" runat="server">Value List Lookup</asp:Label></td>
						<td align="right"><IMG onclick="closeval();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
								width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifval" style="WIDTH: 400px; HEIGHT: 300px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<div class="details" id="subdiv1" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 470px; BORDER-BOTTOM: black 1px solid; HEIGHT: 350px">
				<table cellSpacing="0" cellPadding="0" width="470" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="whtlbl"><asp:Label id="lang2911" runat="server">Hazard Lookup</asp:Label></td>
						<td align="right"><IMG onclick="closecraft1();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
								width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifcraft1" style="WIDTH: 470px; HEIGHT: 350px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<div class="details" id="subdiv2" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 470px; BORDER-BOTTOM: black 1px solid; HEIGHT: 350px">
				<table cellSpacing="0" cellPadding="0" width="470" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="whtlbl"><asp:Label id="lang2912" runat="server">Order Unit Lookup</asp:Label></td>
						<td align="right"><IMG onclick="closecraft2();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
								width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifcraft2" style="WIDTH: 470px; HEIGHT: 350px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<div class="details" id="subdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; Z-INDEX: 999; BORDER-LEFT: black 1px solid; WIDTH: 400px; BORDER-BOTTOM: black 1px solid; HEIGHT: 320px">
				<table cellSpacing="0" cellPadding="0" width="400" bgcolor="white">
					<tr bgColor="blue" height="20">
						<td class="whtlbl"><asp:Label id="lang2913" runat="server">Company Lookup</asp:Label></td>
						<td align="right"><IMG onclick="closecraft();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
								width="18"><br>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><iframe id="ifcraft" style="WIDTH: 400px; HEIGHT: 300px" src="" frameBorder="no" runat="server">
							</iframe>
						</td>
					</tr>
				</table>
			</div>
			<input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblfail" type="hidden" name="lblfail" runat="server">
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="lblchild" type="hidden" name="lblchild" runat="server">
			<input id="lbltype" type="hidden" name="lbltype" runat="server"> <input id="lbloldcode" type="hidden" name="lbloldfail" runat="server">
			<input id="lblneweq" type="hidden" name="lblneweq" runat="server"><input id="lblretid" type="hidden" runat="server">
			<input id="lblprnum" type="hidden" runat="server"> <input id="lblitemid" type="hidden" runat="server">
			<input id="lblstore" type="hidden" runat="server"><input type="hidden" id="lblro" runat="server">
            <input type="hidden" id="lblpvend" runat="server" /><input type="hidden" id="lblpvdesc" runat="server" />
            <input type="hidden" id="lblman" runat="server" />
            <input type="hidden" id="lblmandesc" runat="server" />
		<input type="hidden" id="lblcode" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblord" runat="server" />
<input type="hidden" id="lblrecdate" runat="server" />
<input type="hidden" id="lblconv" runat="server" />

</form>
	</body>
</HTML>
