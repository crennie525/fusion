

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class PopAddPart
    Inherits System.Web.UI.Page
	Protected WithEvents lang2826 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2825 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2824 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2823 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim add As New Utilities
    Dim sql As String
    Dim pnum, pdesc, ploc, pcost As String
    Protected WithEvents btnadd As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdplbl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdclbl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpage As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpart As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcost As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            pnum = Request.QueryString("pnum").ToString
            'tdpart.InnerHtml = pnum
            txtpart.Text = pnum
            Dim page As String = Request.QueryString("pg").ToString
            If page = "parts" Then
                lblpage.value = "parts"
            ElseIf page = "tools" Then
                tdplbl.InnerHtml = "New Tool#"
                tdclbl.InnerHtml = "Tool Rate"
                lblpage.Value = "tools"
            ElseIf page = "lubes" Then
                tdplbl.InnerHtml = "New Lube#"
                lblpage.Value = "lubes"
            End If
        End If
        If Request.Form("lblchk") = "add" Then
            add.Open()
            If lblpage.Value = "parts" Then
                AddPart()
            ElseIf lblpage.Value = "tools" Then
                AddTool()
            ElseIf lblpage.Value = "lubes" Then
                AddLube()
            End If
            add.Dispose()
        End If
    End Sub
    Private Sub AddPart()
        pnum = txtpart.Text 'tdpart.InnerHtml
        pnum = Replace(pnum, "~", "-", , , vbTextCompare)
        pnum = Replace(pnum, "'", Chr(180), , , vbTextCompare)
        pnum = Replace(pnum, """", Chr(180) & Chr(180), , , vbTextCompare)
        pnum = Replace(pnum, "--", "-", , , vbTextCompare)
        pnum = Replace(pnum, ";", ":", , , vbTextCompare)
        If Len(pnum) > 30 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1411" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pdesc = txtdesc.Text
        pdesc = Replace(pdesc, "'", Chr(180), , , vbTextCompare)
        pdesc = Replace(pdesc, "--", "-", , , vbTextCompare)
        pdesc = Replace(pdesc, ";", ":", , , vbTextCompare)
        If Len(pdesc) = 0 Then pdesc = ""
        If Len(pdesc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1412" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ploc = txtloc.Text
        ploc = Replace(ploc, "'", Chr(180), , , vbTextCompare)
        ploc = Replace(ploc, "--", "-", , , vbTextCompare)
        ploc = Replace(ploc, ";", ":", , , vbTextCompare)
        If Len(ploc) = 0 Then ploc = ""
        If Len(ploc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1413" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pcost = txtcost.Text
        Dim costchk As Decimal
        Try
            costchk = System.Convert.ToDecimal(pcost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1414" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If Len(pcost) = 0 OrElse pcost = "" Then
            pcost = "0"
        End If
        'sql = "insert into item (itemnum, description, location, cost) values " _
        '+ "('" & pnum & "', '" & pdesc & "', '" & ploc & "', '" & pcost & "')"

        sql = "usp_insertinv 'part', '" & pnum & "','" & pdesc & "','" & ploc & "','" & pcost & "','oinv'"

        add.Update(sql)
        lblchk.Value = "ok"

    End Sub
    Private Sub AddTool()
        pnum = txtpart.Text 'tdpart.InnerHtml
        pnum = Replace(pnum, "~", "-", , , vbTextCompare)
        pnum = Replace(pnum, "'", Chr(180), , , vbTextCompare)
        pnum = Replace(pnum, """", Chr(180) & Chr(180), , , vbTextCompare)
        pnum = Replace(pnum, "--", "-", , , vbTextCompare)
        pnum = Replace(pnum, ";", ":", , , vbTextCompare)
        If Len(pnum) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1415" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pdesc = txtdesc.Text
        pdesc = Replace(pdesc, "'", Chr(180), , , vbTextCompare)
        pdesc = Replace(pdesc, "--", "-", , , vbTextCompare)
        pdesc = Replace(pdesc, ";", ":", , , vbTextCompare)
        If Len(pdesc) = 0 Then pdesc = ""
        If Len(pdesc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1416" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ploc = txtloc.Text
        ploc = Replace(ploc, "'", Chr(180), , , vbTextCompare)
        ploc = Replace(ploc, "--", "-", , , vbTextCompare)
        ploc = Replace(ploc, ";", ":", , , vbTextCompare)
        If Len(ploc) = 0 Then ploc = ""
        If Len(ploc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1417" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pcost = txtcost.Text
        Dim costchk As Decimal
        Try
            costchk = System.Convert.ToDecimal(pcost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1418" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If Len(pcost) = 0 OrElse pcost = "" Then
            pcost = "0"
        End If
        Dim txtchk As Long
        Try
            txtchk = System.Convert.ToDecimal(pcost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1419" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        sql = "insert into tool (toolnum, description, location, toolrate) values " _
        + "('" & pnum & "', '" & pdesc & "', '" & ploc & "', '" & pcost & "')"

        sql = "usp_insertinv 'tool', '" & pnum & "','" & pdesc & "','" & ploc & "','" & pcost & "','oinv'"
        add.Update(sql)
        lblchk.Value = "ok"

    End Sub
    Private Sub AddLube()
        pnum = txtpart.Text 'tdpart.InnerHtml
        pnum = Replace(pnum, "~", "-", , , vbTextCompare)
        pnum = Replace(pnum, "'", Chr(180), , , vbTextCompare)
        pnum = Replace(pnum, """", Chr(180) & Chr(180), , , vbTextCompare)
        pnum = Replace(pnum, "--", "-", , , vbTextCompare)
        pnum = Replace(pnum, ";", ":", , , vbTextCompare)
        If Len(pnum) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1420" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pdesc = txtdesc.Text
        pdesc = Replace(pdesc, "'", Chr(180), , , vbTextCompare)
        pdesc = Replace(pdesc, "--", "-", , , vbTextCompare)
        pdesc = Replace(pdesc, ";", ":", , , vbTextCompare)
        If Len(pdesc) = 0 Then pdesc = ""
        If Len(pdesc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1421" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ploc = txtloc.Text
        ploc = Replace(ploc, "'", Chr(180), , , vbTextCompare)
        ploc = Replace(ploc, "--", "-", , , vbTextCompare)
        ploc = Replace(ploc, ";", ":", , , vbTextCompare)
        If Len(ploc) = 0 Then ploc = ""
        If Len(ploc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1422" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pcost = txtcost.Text
        Dim costchk As Decimal
        Try
            costchk = System.Convert.ToDecimal(pcost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1423" , "PopAddPart.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If Len(pcost) = 0 OrElse pcost = "" Then
            pcost = "0"
        End If
        sql = "insert into lubricants (lubenum, description, location, cost) values " _
        + "('" & pnum & "', '" & pdesc & "', '" & ploc & "', '" & pcost & "')"

        sql = "usp_insertinv 'lube', '" & pnum & "','" & pdesc & "','" & ploc & "','" & pcost & "','oinv'"

        add.Update(sql)
        lblchk.Value = "ok"

    End Sub

    
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2823.Text = axlabs.GetASPXPage("PopAddPart.aspx", "lang2823")
        Catch ex As Exception
        End Try
        Try
            lang2824.Text = axlabs.GetASPXPage("PopAddPart.aspx", "lang2824")
        Catch ex As Exception
        End Try
        Try
            lang2825.Text = axlabs.GetASPXPage("PopAddPart.aspx", "lang2825")
        Catch ex As Exception
        End Try
        Try
            lang2826.Text = axlabs.GetASPXPage("PopAddPart.aspx", "lang2826")
        Catch ex As Exception
        End Try

    End Sub

End Class
