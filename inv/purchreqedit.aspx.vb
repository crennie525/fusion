

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class purchreqedit
    Inherits System.Web.UI.Page
	Protected WithEvents lang2856 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2855 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2854 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2853 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2852 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2851 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2850 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2849 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2848 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2847 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2846 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2845 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2844 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2843 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2842 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2841 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2840 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2839 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2838 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2837 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2836 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2835 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2834 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2833 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2832 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim inv As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents ddstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdrby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrbydt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsvend As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsvcont As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdshipto As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdattn As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtponum As System.Web.UI.WebControls.TextBox


    Protected WithEvents tdvphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tditem As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tditemdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdunit As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdqty As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdunitpr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlcost As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblprnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtactcost As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbloldcost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpvend As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divpvend As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblorderdate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divorderdate As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblrecv As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divrecv As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim prnum, who As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdprnum As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            prnum = Request.QueryString("prnum").ToString '"1000" '
            tdprnum.InnerHtml = prnum
            who = Request.QueryString("who").ToString '"1000" '
            lblwho.Value = who
            lblprnum.Value = prnum
            inv.Open()
            GetPR()
            inv.Dispose()
        Else
            If Request.Form("lblsubmit") = "savepo" Then
                lblsubmit.Value = ""
                inv.Open()
                SavePO()
                GetPR()
                inv.Dispose()
            End If
        End If
    End Sub
    Private Sub GetPR()
        prnum = lblprnum.Value
        sql = "select r.*, Convert(char(10),requireddate, 101) as 'requireddate1', Convert(char(10),orderdate, 101) as 'orderdate1', i.catcode, i.model, " _
            + " Convert(char(10),recdate, 101) as 'recdate1' " _
        + "from invpr r left join inventory i on i.itemid = r.itemid where r.prnum = '" & prnum & "'"
        dr = inv.GetRdrData(sql)
        While dr.Read
            ddstat.SelectedValue = dr.Item("status").ToString
            tdsvend.InnerHtml = dr.Item("vendor").ToString
            tdshipto.InnerHtml = dr.Item("shipto").ToString
            tdrby.InnerHtml = dr.Item("requestedby").ToString
            tdrbydt.InnerHtml = dr.Item("requireddate1").ToString
            tdsvcont.InnerHtml = dr.Item("contact").ToString
            tdattn.InnerHtml = dr.Item("shiptoattn").ToString

            tditem.InnerHtml = dr.Item("itemnum").ToString
            tditemdesc.InnerHtml = dr.Item("itemdesc").ToString
            tdunit.InnerHtml = dr.Item("orderunit").ToString
            tdqty.InnerHtml = dr.Item("orderqty").ToString

            tdunitpr.InnerHtml = dr.Item("itemcost").ToString
            lbloldcost.Value = dr.Item("itemcost").ToString
            tdlcost.InnerHtml = dr.Item("totalcost").ToString

            txtponum.Text = dr.Item("ponum").ToString
            lblorderdate.Value = dr.Item("orderdate1").ToString
            divorderdate.InnerHtml = dr.Item("orderdate1").ToString
            lblrecv.Value = dr.Item("recdate1").ToString
            divrecv.InnerHtml = dr.Item("recdate1").ToString
            lblpvend.Value = dr.Item("povendor").ToString
            divpvend.InnerHtml = dr.Item("povendor").ToString


        End While
        dr.Close()
    End Sub
    Private Sub SavePO()
        prnum = lblprnum.Value
        Dim po, od, rd, nvend, act, old As String
        po = txtponum.Text
        po = Replace(po, "'", Chr(180), , , vbTextCompare)
        po = Replace(po, "--", "-", , , vbTextCompare)
        po = Replace(po, ";", ":", , , vbTextCompare)
        If Len(po) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1426" , "purchreqedit.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        od = lblorderdate.Value
        rd = lblrecv.Value
        nvend = lblpvend.Value
        If od = "" Then
            od = Now
        End If
        act = txtactcost.Text
        old = lbloldcost.Value
        If act = "" Then
            act = old
        Else
            Dim actchk As Decimal
            Try
                actchk = System.Convert.ToDecimal(act)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1427" , "purchreqedit.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If

        If nvend = "" Then
            sql = "update invpr set ponum = '" & po & "', orderdate = '" & od & "', recdate = '" & rd & "', " _
            + "povendor = vendor, actcost = '" & act & "' where prnum = '" & prnum & "'"
        Else
            sql = "update invpr set ponum = '" & po & "', orderdate = '" & od & "', recdate = '" & rd & "', " _
            + "povendor = '" & nvend & "', actcost = '" & act & "' where prnum = '" & prnum & "'"
        End If
        inv.Update(sql)
        sql = "update invpr set acttotal = orderqty * actcost where prnum = '" & prnum & "'"
    End Sub
    Private Sub ddstat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddstat.SelectedIndexChanged
        prnum = lblprnum.Value
        Dim stat As String = ddstat.SelectedValue.ToString
        sql = "update invpr set status = '" & stat & "', statusdate = getDate() where prnum = '" & prnum & "'"
        inv.Open()
        inv.Update(sql)
        GetPR()
        inv.Dispose()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2832.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2832")
		Catch ex As Exception
		End Try
		Try
			lang2833.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2833")
		Catch ex As Exception
		End Try
		Try
			lang2834.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2834")
		Catch ex As Exception
		End Try
		Try
			lang2835.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2835")
		Catch ex As Exception
		End Try
		Try
			lang2836.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2836")
		Catch ex As Exception
		End Try
		Try
			lang2837.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2837")
		Catch ex As Exception
		End Try
		Try
			lang2838.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2838")
		Catch ex As Exception
		End Try
		Try
			lang2839.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2839")
		Catch ex As Exception
		End Try
		Try
			lang2840.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2840")
		Catch ex As Exception
		End Try
		Try
			lang2841.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2841")
		Catch ex As Exception
		End Try
		Try
			lang2842.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2842")
		Catch ex As Exception
		End Try
		Try
			lang2843.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2843")
		Catch ex As Exception
		End Try
		Try
			lang2844.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2844")
		Catch ex As Exception
		End Try
		Try
			lang2845.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2845")
		Catch ex As Exception
		End Try
		Try
			lang2846.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2846")
		Catch ex As Exception
		End Try
		Try
			lang2847.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2847")
		Catch ex As Exception
		End Try
		Try
			lang2848.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2848")
		Catch ex As Exception
		End Try
		Try
			lang2849.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2849")
		Catch ex As Exception
		End Try
		Try
			lang2850.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2850")
		Catch ex As Exception
		End Try
		Try
			lang2851.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2851")
		Catch ex As Exception
		End Try
		Try
			lang2852.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2852")
		Catch ex As Exception
		End Try
		Try
			lang2853.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2853")
		Catch ex As Exception
		End Try
		Try
			lang2854.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2854")
		Catch ex As Exception
		End Try
		Try
			lang2855.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2855")
		Catch ex As Exception
		End Try
		Try
			lang2856.Text = axlabs.GetASPXPage("purchreqedit.aspx","lang2856")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			Img1.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("purchreqedit.aspx","Img1") & "')")
			Img1.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			Img4.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("purchreqedit.aspx","Img4") & "')")
			Img4.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
