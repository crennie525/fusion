

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class InventoryTools
    Inherits System.Web.UI.Page
	Protected WithEvents lang2734 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = "toolnum"
    Dim rowcnt As Integer
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim cid, dept, sql, ro, Login As String
    Dim inv As New Utilities
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbltool As System.Web.UI.WebControls.Label
    Protected WithEvents dgtools As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtools.Columns(0).Visible = False
                dgtools.Columns(6).Visible = False
            End If
            Try
                cid = Request.QueryString("cid")
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try
            If cid <> "" Then
                lblcid.Value = Request.QueryString("cid")
                inv.Open()
                LoadTools(PageNumber)
                inv.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                inv.Open()
                GetNext()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                inv.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadTools(PageNumber)
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                inv.Open()
                GetPrev()
                inv.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                inv.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadTools(PageNumber)
                inv.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub
    ' Tools
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        inv.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadTools(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub LoadTools(ByVal PageNumber As Integer)

        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", " ", , , vbTextCompare)
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, """", Chr(180) & Chr(180), , , vbTextCompare)
        If Len(srch) > 0 Then
            Filter = "toolnum like ''%" & srch & "%''"
            FilterCnt = "toolnum like '%" & srch & "%'"
        Else
            Filter = ""
            FilterCnt = ""
        End If
        Dim sql As String
        If FilterCnt <> "" Then
            sql = "select Count(*) from tool where " & FilterCnt
        Else
            sql = "select Count(*) from tool"
        End If

        dgtools.VirtualItemCount = inv.Scalar(sql)
        If dgtools.VirtualItemCount = 0 Then
            Tables = "tool"
            PK = "toolid"
            PageSize = "10"
            Fields = "*"
            Group = ""

            dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtools.DataSource = dr
            Try
                dgtools.DataBind()
            Catch ex As Exception
                dgtools.CurrentPageIndex = 0
                dgtools.DataBind()
            End Try
            dr.Close()
            lbltool.Text = "No Tools Inventory Records Found"
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtools.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtools.PageCount
        Else
            Tables = "tool"
            PK = "toolid"
            PageSize = "10"
            Fields = "*"
            Group = ""
            'Sort = ""
            dr = inv.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtools.DataSource = dr
            Try
                dgtools.DataBind()
            Catch ex As Exception
                dgtools.CurrentPageIndex = 0
                dgtools.DataBind()
            End Try
            dr.Close()
            lbltool.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtools.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtools.PageCount
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadTools(PageNumber)
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1225" , "InventoryTools.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadTools(PageNumber)
        Catch ex As Exception
            inv.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1226" , "InventoryTools.aspx.vb")
 
            inv.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgtools_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtools.UpdateCommand
        'Try
        inv.Open()
        cid = lblcid.Value
        Dim id, tool, desc, loc, cost As String
        Dim flag As Integer = 1
        'id = CType(e.Item.Cells(1).Controls(1), TextBox).Text
        id = CType(e.Item.FindControl("lbltoolid"), Label).Text
        tool = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        tool = Replace(tool, "'", Chr(180), , , vbTextCompare)

        tool = Replace(tool, """", Chr(180) & Chr(180), , , vbTextCompare)
        tool = Replace(tool, "--", "-", , , vbTextCompare)
        tool = Replace(tool, ";", ":", , , vbTextCompare)

        tool = Replace(tool, ";", " ", , , vbTextCompare)
        'tool = Replace(tool, "/", " ", , , vbTextCompare)
        'tool = Replace(tool, "\", " ", , , vbTextCompare)
        tool = Replace(tool, "&", " ", , , vbTextCompare)
        tool = Replace(tool, "%", " ", , , vbTextCompare)
        tool = Replace(tool, "~", " ", , , vbTextCompare)
        tool = Replace(tool, "^", " ", , , vbTextCompare)
        tool = Replace(tool, "*", " ", , , vbTextCompare)
        tool = Replace(tool, "$", " ", , , vbTextCompare)

        desc = CType(e.Item.Cells(3).Controls(1), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)

        desc = Replace(desc, """", Chr(180) & Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)

        desc = Replace(desc, ";", " ", , , vbTextCompare)
        'desc = Replace(desc, "/", " ", , , vbTextCompare)
        'desc = Replace(desc, "\", " ", , , vbTextCompare)
        desc = Replace(desc, "&", " ", , , vbTextCompare)
        desc = Replace(desc, "%", " ", , , vbTextCompare)
        desc = Replace(desc, "~", " ", , , vbTextCompare)
        desc = Replace(desc, "^", " ", , , vbTextCompare)
        desc = Replace(desc, "*", " ", , , vbTextCompare)
        desc = Replace(desc, "$", " ", , , vbTextCompare)

        If Len(desc) = 0 Then desc = ""
        If Len(desc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1227" , "InventoryTools.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        loc = CType(e.Item.Cells(4).Controls(1), TextBox).Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)

        loc = Replace(loc, """", Chr(180) & Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)

        loc = Replace(loc, ";", " ", , , vbTextCompare)
        loc = Replace(loc, "/", " ", , , vbTextCompare)
        loc = Replace(loc, "\", " ", , , vbTextCompare)
        loc = Replace(loc, "&", " ", , , vbTextCompare)
        loc = Replace(loc, "%", " ", , , vbTextCompare)
        loc = Replace(loc, "~", " ", , , vbTextCompare)
        loc = Replace(loc, "^", " ", , , vbTextCompare)
        loc = Replace(loc, "*", " ", , , vbTextCompare)
        loc = Replace(loc, "$", " ", , , vbTextCompare)

        If Len(loc) = 0 Then loc = ""
        If Len(loc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1228" , "InventoryTools.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        cost = CType(e.Item.FindControl("txtcost"), TextBox).Text
        Dim costchk As Decimal
        Try
            costchk = System.Convert.ToDecimal(cost)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1229" , "InventoryTools.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If Len(tool) <= 50 And Len(tool) <> 0 And tool <> "" Then
            Dim old As String = lblold.Value
            Dim icnt As Integer
            sql = "usp_updateinv 'tool','" & id & "','" & tool & "','" & desc & "','" & old & "','" & loc & "', '" & cost & "', '" & flag & "'"
            icnt = inv.Scalar(sql)
            If icnt = 0 Then
                PageNumber = txtpg.Value
                dgtools.EditItemIndex = -1
                LoadTools(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1230" , "InventoryTools.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            'sql = "update tool set description = " _
            '        + "'" & desc & "', toolnum = '" & tool & "', location = '" & loc & "' " _
            '       + "where toolid = '" & id & "'"
            'inv.Update(sql)

        Else

            If Len(tool) = 0 OrElse tool = "" Then
                Dim strMessage As String =  tmod.getmsg("cdstr1231" , "InventoryTools.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1232" , "InventoryTools.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        End If

        inv.Dispose()
    End Sub
    Private Sub dgtools_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtools.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("Label8"), Label).Text
        dgtools.EditItemIndex = e.Item.ItemIndex
        inv.Open()
        LoadTools(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub dgtools_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtools.CancelCommand
        dgtools.EditItemIndex = -1
        inv.Open()
        PageNumber = txtpg.Value
        LoadTools(PageNumber)
        inv.Dispose()
    End Sub
    Private Sub dgtools_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtools.DeleteCommand
        inv.Open()
        Dim id, item, old As String
        Dim flag As Integer = 0
        Try
            id = CType(e.Item.FindControl("lbltoolidn"), Label).Text
            item = CType(e.Item.FindControl("Label8"), Label).Text
            old = CType(e.Item.FindControl("Label1"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbltoolid"), Label).Text
            item = CType(e.Item.FindControl("TextBox6"), TextBox).Text
            old = CType(e.Item.FindControl("Label2"), Label).Text
            flag = 1
        End Try
        If flag = 1 Then
            If old <> item Then
                Dim strMessage As String =  tmod.getmsg("cdstr1233" , "InventoryTools.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                inv.Dispose()
                Exit Sub
            End If
        End If
        Dim icnt As Integer
        sql = "select count(*) from pmtasktools where toolnum = '" & item & "'"
        icnt = inv.Scalar(sql)
        If icnt = 0 Then
            sql = "delete from tool where toolid = '" & id & "'"
            inv.Update(sql)
            dgtools.EditItemIndex = -1
            cid = lblcid.Value
            sql = "select Count(*) from tool " _
            + "where compid = '" & cid & "'"
            PageNumber = inv.PageCount(sql, PageSize)

            sql = "update AdminTasks " _
               + "set tools = '" & PageNumber & "' where cid = '" & cid & "'"

            inv.Update(sql)
            If dgtools.CurrentPageIndex > PageNumber Then
                dgtools.CurrentPageIndex = PageNumber - 1
            End If
            If dgtools.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgtools.CurrentPageIndex + 1
            End If
            dgtools.EditItemIndex = -1
            LoadTools(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1234" , "InventoryTools.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        inv.Dispose()
    End Sub


    Private Sub dgtools_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtools.ItemCommand


        If e.CommandName = "Add" Then
            Dim num, des, loc, cost As String
            Dim nt, dt, lt As TextBox
            'lnamei = CType(e.Item.FindControl("txtnewstat"), TextBox)
            'lname = lnamei.Text
            nt = CType(e.Item.FindControl("txttname"), TextBox)
            dt = CType(e.Item.FindControl("txttdesc"), TextBox)
            lt = CType(e.Item.FindControl("txttloc"), TextBox)
            num = CType(e.Item.FindControl("txttname"), TextBox).Text
            num = Replace(num, "'", Chr(180), , , vbTextCompare)

            num = Replace(num, """", Chr(180) & Chr(180), , , vbTextCompare)
            num = Replace(num, "--", "-", , , vbTextCompare)
            num = Replace(num, ";", ":", , , vbTextCompare)

            num = Replace(num, ";", " ", , , vbTextCompare)
            'num = Replace(num, "/", " ", , , vbTextCompare)
            'num = Replace(num, "\", " ", , , vbTextCompare)
            num = Replace(num, "&", " ", , , vbTextCompare)
            num = Replace(num, "%", " ", , , vbTextCompare)
            num = Replace(num, "~", " ", , , vbTextCompare)
            num = Replace(num, "^", " ", , , vbTextCompare)
            num = Replace(num, "*", " ", , , vbTextCompare)
            num = Replace(num, "$", " ", , , vbTextCompare)

            des = CType(e.Item.FindControl("txttdesc"), TextBox).Text
            des = Replace(des, "'", Chr(180), , , vbTextCompare)

            des = Replace(des, """", Chr(180) & Chr(180), , , vbTextCompare)
            des = Replace(des, "--", "-", , , vbTextCompare)
            des = Replace(des, ";", ":", , , vbTextCompare)

            des = Replace(des, ";", " ", , , vbTextCompare)
            'des = Replace(des, "/", " ", , , vbTextCompare)
            'des = Replace(des, "\", " ", , , vbTextCompare)
            des = Replace(des, "&", " ", , , vbTextCompare)
            des = Replace(des, "%", " ", , , vbTextCompare)
            des = Replace(des, "~", " ", , , vbTextCompare)
            des = Replace(des, "^", " ", , , vbTextCompare)
            des = Replace(des, "*", " ", , , vbTextCompare)
            des = Replace(des, "$", " ", , , vbTextCompare)

            If Len(des) = 0 Then des = ""
            If Len(des) > 100 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1235" , "InventoryTools.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            loc = CType(e.Item.FindControl("txttloc"), TextBox).Text
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)

            loc = Replace(loc, """", Chr(180) & Chr(180), , , vbTextCompare)
            loc = Replace(loc, "--", "-", , , vbTextCompare)
            loc = Replace(loc, ";", ":", , , vbTextCompare)

            loc = Replace(loc, ";", " ", , , vbTextCompare)
            loc = Replace(loc, "/", " ", , , vbTextCompare)
            loc = Replace(loc, "\", " ", , , vbTextCompare)
            loc = Replace(loc, "&", " ", , , vbTextCompare)
            loc = Replace(loc, "%", " ", , , vbTextCompare)
            loc = Replace(loc, "~", " ", , , vbTextCompare)
            loc = Replace(loc, "^", " ", , , vbTextCompare)
            loc = Replace(loc, "*", " ", , , vbTextCompare)
            loc = Replace(loc, "$", " ", , , vbTextCompare)

            If Len(loc) = 0 Then loc = ""
            If Len(loc) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1236" , "InventoryTools.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            cost = CType(e.Item.FindControl("txtcostf"), TextBox).Text
            If cost = "" Then
                cost = "0"
            End If
            Dim costchk As Decimal
            Try
                costchk = System.Convert.ToDecimal(cost)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1237" , "InventoryTools.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If Len(num) <= 50 And Len(num) <> 0 And num <> "" Then
                inv.Open()
                cid = lblcid.Value
                sql = "usp_insertinv 'tool', '" & num & "','" & des & "','" & loc & "','" & cost & "','oinv'"
                Dim icnt As Integer
                icnt = inv.Scalar(sql)
                'sql = "insert into item " _
                '+ "(compid, itemnum, description, location) values " _
                '+ "('" & cid & "', '" & num & "', '" & des & "', '" & loc & "')"
                'inv.Update(sql)
                If icnt = 0 Then
                    sql = "select Count(*) from tool" ' where compid = '" & cid & "'"
                    PageNumber = inv.PageCount(sql, PageSize)
                    LoadTools(PageNumber)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1238" , "InventoryTools.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                'sql = "insert into tool " _
                '+ "(compid, toolnum, description, location) values " _
                '+ "('" & cid & "', '" & num & "', '" & des & "', '" & loc & "')"
                'inv.Update(sql)

                inv.Dispose()
            Else

                If Len(num) = 0 OrElse num = "" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr1239" , "InventoryTools.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1240" , "InventoryTools.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If
    End Sub


	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtools.Columns(0).HeaderText = dlabs.GetDGPage("InventoryTools.aspx", "dgtools", "0")
        Catch ex As Exception
        End Try
        Try
            dgtools.Columns(2).HeaderText = dlabs.GetDGPage("InventoryTools.aspx", "dgtools", "2")
        Catch ex As Exception
        End Try
        Try
            dgtools.Columns(3).HeaderText = dlabs.GetDGPage("InventoryTools.aspx", "dgtools", "3")
        Catch ex As Exception
        End Try
        Try
            dgtools.Columns(4).HeaderText = dlabs.GetDGPage("InventoryTools.aspx", "dgtools", "4")
        Catch ex As Exception
        End Try
        Try
            dgtools.Columns(5).HeaderText = dlabs.GetDGPage("InventoryTools.aspx", "dgtools", "5")
        Catch ex As Exception
        End Try
        Try
            dgtools.Columns(6).HeaderText = dlabs.GetDGPage("InventoryTools.aspx", "dgtools", "6")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2734.Text = axlabs.GetASPXPage("InventoryTools.aspx", "lang2734")
        Catch ex As Exception
        End Try

    End Sub

End Class
