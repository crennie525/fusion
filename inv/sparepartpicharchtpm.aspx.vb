

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class sparepartpicharchtpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang2924 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2923 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2922 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2921 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2920 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim iPageNumber As Integer = 1
    Dim oPageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim cid, cnm, typ, ptid, tnum, origflag, rev As String
    Dim sort As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim parts As New Utilities
    Dim eqid, eqnum, ro As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblnpg As System.Web.UI.WebControls.Label
    Protected WithEvents dgoparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents dgtaskparts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblipg As System.Web.UI.WebControls.Label
    Protected WithEvents tdtask As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents olab As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents odiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents onav As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents osep As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img7 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img8 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtopgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtopg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ospdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then

            eqid = Request.QueryString("eqid").ToString '"266" '
            lbleqid.Value = eqid

            origflag = "o" 'Request.QueryString("oflag").ToString '"o" '
            lbloflag.Value = origflag
            'typ = "na" 'Request.QueryString("typ").ToString
            'lbltyp.Value = typ
            'ptid = "1" 'Request.QueryString("typ").ToString
            'lblptid.Value = ptid

            'PopTasks(iPageNumber)


            'parts.Dispose()
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                rev = Request.QueryString("rev").ToString
                lblrev.Value = rev
                If ro = "1" Then
                    'dgparttasks.Columns(0).Visible = False
                    'dgparttasks.Columns(7).Visible = False
                    'dgoparttasks.Columns(0).Visible = False
                    'dgoparttasks.Columns(7).Visible = False
                    'ibtnaddtolist.Enabled = False
                    'ibtnaddtolist.ImageUrl = "../images/appbuttons/minibuttons/savedisk1dis.gif"
                End If
                parts.Open()
                GetEq(eqid)
                PopSpare(PageNumber)
                ptid = Request.QueryString("ptid").ToString '"1" '
                lblptid.Value = ptid
                Try
                    typ = Request.QueryString("typ").ToString '"na" '
                Catch ex As Exception
                    typ = "no"
                End Try
                lbltyp.Value = typ
                If typ = "jp" Or typ = "wjp" Then
                    lbljpnum.Value = Request.QueryString("jp").ToString
                    lblwo.Value = Request.QueryString("wo").ToString
                    tnum = Request.QueryString("tnum")
                    tdtask.InnerHtml = tnum
                    cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                    'lblcid.Value = cid
                    PopTasks(PageNumber)
                    lbloflag.Value = "3"
                    'div1.Visible = False
                ElseIf typ = "wo" Then
                    lblwo.Value = Request.QueryString("wo").ToString
                    'tdtask.InnerHtml = "N/A"
                    cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                    'lblcid.Value = cid
                    PopTasks(PageNumber)
                    lbloflag.Value = "3"
                    'div1.Visible = False
                Else
                    If Len(ptid) <> 0 AndAlso ptid <> "" AndAlso ptid <> "0" Then
                        tnum = Request.QueryString("tnum")
                        'tdtask.InnerHtml = tnum
                        cid = Request.QueryString("cid") 'HttpContext.Current.Session("comp").ToString
                        'lblcid.Value = cid
                        PopTasks(iPageNumber)
                        'lbloflag.Value = "3"
                        'div1.Visible = False
                        If origflag = "o" Then
                            olab.Attributes.Add("class", "view")
                            odiv.Attributes.Add("class", "view")
                            onav.Attributes.Add("class", "view")
                            osep.Attributes.Add("class", "view")
                            oPopTasks(oPageNumber)
                        Else
                            olab.Attributes.Add("class", "details")
                            odiv.Attributes.Add("class", "details")
                            onav.Attributes.Add("class", "details")
                            osep.Attributes.Add("class", "details")
                        End If
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr1472" , "sparepartpicharchtpm.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        'lbllog.Value = "nodeptid"
                    End If
                End If
                parts.Dispose()
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr1473" , "sparepartpicharchtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'lbllog.Value = "nodeptid"
            End Try
            'btnradd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov290" , "sparepartpicharchtpm.aspx.vb") & "', ABOVE)")
            'btnradd.Attributes.Add("onmouseout", "return nd()")
        Else
            If Request.Form("lblret") = "nnext" Then
                parts.Open()
                GetnNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nlast" Then
                parts.Open()
                PageNumber = txtnpgcnt.Value
                txtnpg.Value = PageNumber
                PopSpare(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nprev" Then
                parts.Open()
                GetnPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nfirst" Or Request.Form("lblret") = "spreturn" Then
                parts.Open()
                PageNumber = 1
                txtnpg.Value = PageNumber
                PopSpare(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "inext" Then
                parts.Open()
                GetiNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ilast" Then
                parts.Open()
                iPageNumber = txtipgcnt.Value
                txtipg.Value = iPageNumber
                PopTasks(iPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "iprev" Then
                parts.Open()
                GetiPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ifirst" Then
                parts.Open()
                iPageNumber = 1
                txtipg.Value = iPageNumber
                PopTasks(iPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "addspare" Then
                parts.Open()
                'AddSpare()
                If lbloflag.Value = "o" Then
                    oPageNumber = 1
                    txtopg.Value = oPageNumber
                    oPopTasks(oPageNumber)
                End If
                iPageNumber = 1
                txtipg.Value = iPageNumber
                PopTasks(iPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "next" Then
                parts.Open()
                GetNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                parts.Open()
                oPageNumber = txtopgcnt.Value
                txtopg.Value = oPageNumber
                oPopTasks(oPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                parts.Open()
                GetPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                parts.Open()
                oPageNumber = 1
                txtopg.Value = oPageNumber
                oPopTasks(oPageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "addspare" Then
                parts.Open()
                oPageNumber = 1
                txtopg.Value = oPageNumber
                oPopTasks(oPageNumber)
                iPageNumber = 1
                txtipg.Value = iPageNumber
                PopTasks(iPageNumber)
                parts.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetEq(ByVal eqid As String)
        'eqnum = Request.QueryString("eqn").ToString '"test entry" '
        rev = lblrev.Value
        sql = "select eqnum from equipmenttpmarch where eqid = '" & eqid & "' and rev = '" & rev & "'"
        eqnum = parts.strScalar(sql)
        tdtask.InnerHtml = eqnum
        lbleqnum.Value = eqnum
    End Sub

    Private Sub PopSpare(ByVal PageNumber As Integer)
        txtnpg.Value = PageNumber
        eqid = lbleqid.Value
        Dim intPgCnt As Integer
        sql = "select count(*) from invsparepart where eqid = '" & eqid & "'"
        Tables = "invsparepart"
        PK = "spid"
        Filter = "eqid = " & eqid
        Fields = "*"
        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblnpg.Text = "Page 0 of 0"
        Else
            lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtnpgcnt.Value = intPgNav

        dr = parts.GetPage(Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group)
        dgparttasks.DataSource = dr
        dgparttasks.DataBind()
        dr.Close()
        txtnpg.Value = PageNumber
        'txtnpgcnt.Value = intPgNav
        'lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
    End Sub
    Private Sub GetnNext()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg + 1
            txtnpg.Value = PageNumber
            PopSpare(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1474" , "sparepartpicharchtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetnPrev()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg - 1
            txtnpg.Value = PageNumber
            PopSpare(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1475" , "sparepartpicharchtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    '*** Original
    Private Sub oPopTasks(ByVal oPageNumber As Integer)
        txtopg.Value = oPageNumber
        ptid = lblptid.Value
        'parts.Open()
        'Get Count
        rev = lblrev.Value
        Dim intPgCnt As Integer
        sql = "select count(*) from pmoTaskPartstpmarch where pmtskid = '" & ptid & "' and sparepart = 'Y' and rev = '" & rev & "'"
        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtopgcnt.Value = intPgNav

        'Get Page
        Tables = "pmoTaskPartstpmarch"
        PK = "tskpartid"
        Filter = "pmtskid = " & ptid & " and sparepart = ''Y'' and rev = ''" & rev & "''"
        Dim dr As SqlDataReader
        dr = parts.GetPage(Tables, PK, sort, oPageNumber, PageSize, Fields, Filter, Group)
        dgoparttasks.DataSource = dr
        dgoparttasks.DataBind()
        dr.Close()
        'parts.Dispose()
        txtopg.Value = oPageNumber
        'txtopgcnt.Value = intPgNav
        'lblpg.Text = "Page " & oPageNumber & " of " & intPgNav
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtopg.Value
            oPageNumber = pg + 1
            txtopg.Value = oPageNumber
            oPopTasks(oPageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1476" , "sparepartpicharchtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtopg.Value
            oPageNumber = pg - 1
            txtopg.Value = oPageNumber
            oPopTasks(oPageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1477" , "sparepartpicharchtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        txtipg.Value = PageNumber
        ptid = lblptid.Value
        rev = lblrev.Value
        typ = lbltyp.Value
        'parts.Open()
        'Get Count
        Dim intPgCnt As Integer
        If typ = "wo" Then
            sql = "select count(*) from woparts where wonum = '" & lblwo.Value & "' and sparepart = 'Y'"
            Tables = "woparts"
            PK = "wopartid"
            Fields = "*, wopartid as lineid"
            Filter = "wonum = " & lblwo.Value & " and sparepart = ''Y''"
        ElseIf typ = "jp" Then
            sql = "select count(*) from jpparts where pmtskid = '" & ptid & "'"
            Tables = "jpparts"
            PK = "tskpartid"
            Fields = "*, tskpartid as lineid"
            Filter = "pmtskid = " & ptid
        ElseIf typ = "wjp" Then
            sql = "select count(*) from wojpparts where wojtid = '" & ptid & "'"
            Tables = "wojpparts"
            PK = "wotskpartid"
            Fields = "*, wotskpartid as lineid"
            Filter = "wojtid = " & ptid
        Else
            sql = "select count(*) from pmTaskPartstpmarch where pmtskid = '" & ptid & "' and sparepart = 'Y' and rev = '" & rev & "'"
            Tables = "pmTaskPartstpmarch"
            PK = "tskpartid"
            Fields = "*, tskpartid as lineid"
            Filter = "pmtskid = " & ptid & " and sparepart = ''Y'' and rev = ''" & rev & "''"
        End If
        'intPgCnt = parts.Scalar(sql)
        'intPgNav = parts.PageCount(intPgCnt, PageSize)

        intPgNav = parts.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblipg.Text = "Page 0 of 0"
        Else
            lblipg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtipgcnt.Value = intPgNav

        dr = parts.GetPage(Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group)
        dgtaskparts.DataSource = dr
        dgtaskparts.DataBind()
        dr.Close()
        txtipg.Value = PageNumber
        'txtipgcnt.Value = intPgNav
        'lblipg.Text = "Page " & PageNumber & " of " & intPgNav
    End Sub
    Private Sub GetiNext()
        Try
            Dim pg As Integer = txtipg.Value
            iPageNumber = pg + 1
            txtipg.Value = iPageNumber
            PopTasks(iPageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1478" , "sparepartpicharchtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetiPrev()
        Try
            Dim pg As Integer = txtipg.Value
            iPageNumber = pg - 1
            txtipg.Value = iPageNumber
            PopTasks(iPageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1479" , "sparepartpicharchtpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub


	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgoparttasks.Columns(0).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgoparttasks", "0")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(2).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgoparttasks", "2")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(4).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgoparttasks", "4")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(5).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgoparttasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgoparttasks.Columns(6).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgoparttasks", "6")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(1).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgparttasks", "1")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(5).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgparttasks", "5")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(7).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgparttasks", "7")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(10).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgparttasks", "10")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(12).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgparttasks", "12")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(14).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgparttasks", "14")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(15).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgparttasks", "15")
        Catch ex As Exception
        End Try
        Try
            dgparttasks.Columns(16).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgparttasks", "16")
        Catch ex As Exception
        End Try
        Try
            dgtaskparts.Columns(0).HeaderText = dlabs.GetDGPage("sparepartpicharchtpm.aspx", "dgtaskparts", "0")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2920.Text = axlabs.GetASPXPage("sparepartpicharchtpm.aspx", "lang2920")
        Catch ex As Exception
        End Try
        Try
            lang2921.Text = axlabs.GetASPXPage("sparepartpicharchtpm.aspx", "lang2921")
        Catch ex As Exception
        End Try
        Try
            lang2922.Text = axlabs.GetASPXPage("sparepartpicharchtpm.aspx", "lang2922")
        Catch ex As Exception
        End Try
        Try
            lang2923.Text = axlabs.GetASPXPage("sparepartpicharchtpm.aspx", "lang2923")
        Catch ex As Exception
        End Try
        Try
            lang2924.Text = axlabs.GetASPXPage("sparepartpicharchtpm.aspx", "lang2924")
        Catch ex As Exception
        End Try

    End Sub

End Class
