<%@ Page Language="vb" AutoEventWireup="false" Codebehind="matlookup.aspx.vb" Inherits="lucy_r12.matlookup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>matlookup</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/matlookupaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function GetItem(typ, fld1, fld2) {
             if (typ == "srch") {
                 list = document.getElementById("ddtype").value;
             }
             else {
                 list = typ;
             }
             var eReturn = window.showModalDialog("invdialog.aspx?filt=no&list=" + list + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:650px; resizable=yes");
             if (eReturn) {
                 if (eReturn == "log") {
                     window.parent.handlelogout();
                 }
                 else if (eReturn != "can") {
                     var ret = eReturn;
                     var retarr = ret.split(";")
                     if (typ == "srch") {
                         if (fld1 == "alt") {
                             document.getElementById("lblchildid").value = retarr[0];
                             document.getElementById("lblchild").value = retarr[1];
                             document.getElementById("lblsubmit").value = "addchild";
                             document.getElementById("form1").submit();
                         }
                         else {
                             document.getElementById("txtcode").value = retarr[1];
                             document.getElementById("lblsubmit").value = "get";
                             document.getElementById("form1").submit();
                         }
                     }
                     else if (typ == "storeroom") {
                         document.getElementById(fld1).value = retarr[1];
                         document.getElementById("divstore").innerHTML = retarr[1];
                         document.getElementById("lblret").value = "first";
                         document.getElementById("form1").submit();
                     }
                     else {
                         document.getElementById(fld1).value = retarr[1];
                     }

                 }
             }
         }
         function refit() {
             document.getElementById("txtsrch").value = "";
             document.getElementById("lblstore").value = "";
             document.getElementById("lblret").value = "first";
             document.getElementById("form1").submit();
         }
         //-->
     </script>
     <style type="text/css">
     .readonly150
{
    border: 1px solid #CBCCCE;
    background-color: #ECF0F3;
    font-family: Arial,MS Sans Serif;
    font-size:12px;
    vertical-align: middle; 
    display: table-cell; 
    text-indent: 1px;
    height: 20px; 
    width: 160px;
}
     </style>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px" cellSpacing="2" cellPadding="2"
				width="620">
				<tr>
					<td width="70"></td>
					<td width="150"></td>
					<td width="30"></td>
					<td width="90"></td>
					<td width="150"></td>
					<td width="130"></td>
				</tr>
				<tr>
					<td colSpan="6"><asp:label id="lblps" runat="server" Width="376px" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
							ForeColor="Red"></asp:label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2772" runat="server">Search</asp:Label></td>
					<td><asp:textbox id="txtsrch" runat="server" Width="140px" CssClass="plainlabel"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td class="label" id="td1" runat="server"><asp:Label id="lang2773" runat="server">Store Room</asp:Label></td>
					<td id="td2" runat="server"><div class="readonly150" id="divstore" runat="server">
                                        </div>
                                        </td>
					<td id="td3" runat="server"><IMG onclick="GetItem('storeroom', 'lblstore');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                    <img alt="" src="../images/appbuttons/minibuttons/refreshit.gif" onclick="refit();" /></td>
				</tr>
				<tr>
					<TD colSpan="6" align="center">
						<div style="OVERFLOW: auto; HEIGHT: 250px"><asp:datagrid id="dgdepts" runat="server" PageSize="100" CellSpacing="1" ShowFooter="True" CellPadding="0"
								GridLines="None" AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False">
								<FooterStyle BackColor="White"></FooterStyle>
								<EditItemStyle Height="15px"></EditItemStyle>
								<AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD"></AlternatingItemStyle>
								<ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="20px" BackColor="ControlLightLight"></ItemStyle>
								<HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Item">
										<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<a href="#" id="lbv" runat="server">
												<%# DataBinder.Eval(Container, "DataItem.value") %>
											</a>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Item" Visible="False">
										<HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="lblv" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="320px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="lbld" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Store Room">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label2" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.storeroom") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<HeaderStyle Width="220px"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=Label1 runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.valid") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
									ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
							</asp:datagrid></div>
					</TD>
				</tr>
				<tr>
					<td align="center" colSpan="6">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbllist" type="hidden" runat="server"> <input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input id="lblret" type="hidden" runat="server"><input type="hidden" id="lblfilt" runat="server">
		<input type="hidden" id="lblstore" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
