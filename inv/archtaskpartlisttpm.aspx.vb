

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class archtaskpartlisttpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang2649 As System.Web.UI.WebControls.Label
    '
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
	Protected WithEvents lang2648 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2647 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2646 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2645 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2644 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2643 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2642 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim oPageNumber As Integer = 1
    Dim iPageNumber As Integer = 1
    Dim PageSize As Integer = 5
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim cid, cnm As String
    Dim sort As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim parts As New Utilities
    Dim ptid, tnum As String
    Dim decPgNav As Decimal
    Dim ap As New AppUtils
    Dim intPgNav As Integer
    Dim Login, ro, rev As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgoparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents dgparttasks As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblnpg As System.Web.UI.WebControls.Label
    Protected WithEvents txtpart As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsrch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgitems As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblipg As System.Web.UI.WebControls.Label
    Protected WithEvents ibtnaddtolist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Imagebutton9 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbchecked As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdtask As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents mdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents div1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img7 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img8 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtopg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblparts As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblitemid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilterwd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetDGLangs()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        'RetainItems()
        If Request.Form("lbladdflag") = "ok" Then
            lbladdflag.Value = ""
            parts.Open()
            iPageNumber = txtipg.Value
            'PopParts(iPageNumber)
            parts.Dispose()
        End If
        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                rev = Request.QueryString("rev").ToString
                lblrev.Value = rev
                If ro = "1" Then
                    'dgparttasks.Columns(0).Visible = False
                    'dgparttasks.Columns(7).Visible = False
                    'dgoparttasks.Columns(0).Visible = False
                    'dgoparttasks.Columns(7).Visible = False
                    'dgitems.Columns(1).Visible = False
                    ibtnaddtolist.Enabled = False
                    ibtnaddtolist.ImageUrl = "../images/appbuttons/minibuttons/savedisk1dis.gif"
                End If
                Dim icost As String = ap.InvEntry
                If icost = "ext" Or icost = "inv" Then
                    dgitems.ShowFooter = False
                End If
                ptid = Request.QueryString("ptid")
                lblptid.Value = ptid
                If Len(ptid) <> 0 AndAlso ptid <> "" AndAlso ptid <> "0" Then
                    tnum = Request.QueryString("tnum")
                    tdtask.InnerHtml = tnum
                    cid = HttpContext.Current.Session("comp").ToString
                    lblcid.Value = cid
                    'div1.Visible = False
                    parts.Open()
                    PopTasks(PageNumber)
                    oPopTasks(oPageNumber)
                    parts.Dispose()
                    lbloflag.Value = "3"
                    div1.Visible = False
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr1031" , "archtaskpartlisttpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "nodeptid"
                End If
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1032" , "archtaskpartlisttpm.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "nodeptid"
            End Try
        Else
            If Request.Form("lblret") = "next" Then
                parts.Open()
                GetNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                parts.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                oPopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                parts.Open()
                GetPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                parts.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                oPopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nnext" Then
                parts.Open()
                GetnNext()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nlast" Then
                parts.Open()
                PageNumber = txtnpgcnt.Value
                txtnpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nprev" Then
                parts.Open()
                GetnPrev()
                parts.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "nfirst" Then
                parts.Open()
                PageNumber = 1
                txtnpg.Value = PageNumber
                PopTasks(PageNumber)
                parts.Dispose()
                lblret.Value = ""

            End If
        End If

    End Sub
    '*** Original
    Private Sub oPopTasks(ByVal oPageNumber As Integer)
        txtopg.Value = oPageNumber
        ptid = lblptid.Value
        rev = lblrev.Value
        'parts.Open()
        'Get Count
        Dim intPgCnt As Integer
        sql = "select count(*) from pmoTaskPartstpmarch where pmtskid = '" & ptid & "' and rev = '" & rev & "'"
        intPgCnt = parts.Scalar(sql)
        intPgNav = parts.PageCount(intPgCnt, PageSize)
        'Get Page
        Tables = "pmoTaskPartstpmarch"
        PK = "tskpartid"
        Filter = "pmtskid = " & ptid
        Dim dr As SqlDataReader
        dr = parts.GetPage(Tables, PK, sort, oPageNumber, PageSize, Fields, Filter, Group)
        dgoparttasks.DataSource = dr
        dgoparttasks.DataBind()
        dr.Close()
        'parts.Dispose()
        txtpg.Value = oPageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & oPageNumber & " of " & intPgNav
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            oPopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1033" , "archtaskpartlisttpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            oPopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1034" , "archtaskpartlisttpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        txtnpg.Value = PageNumber
        ptid = lblptid.Value
        rev = lblrev.Value
        'parts.Open()
        'Get Count
        Dim intPgCnt As Integer
        sql = "select count(*) from pmTaskPartstpmarch where pmtskid = '" & ptid & "' and rev = '" & rev & "'"
        intPgCnt = parts.Scalar(sql)
        intPgNav = parts.PageCount(intPgCnt, PageSize)
        'Get Page
        Tables = "pmTaskPartstpmarch"
        PK = "tskpartid"
        Filter = "pmtskid = " & ptid
        dr = parts.GetPage(Tables, PK, sort, PageNumber, PageSize, Fields, Filter, Group)
        dgparttasks.DataSource = dr
        dgparttasks.DataBind()
        dr.Close()
        txtnpg.Value = PageNumber
        txtnpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblnpg.Text = "Page 0 of 0"
        Else
            lblnpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
    End Sub
    Private Sub GetnNext()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg + 1
            txtnpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1035" , "archtaskpartlisttpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetnPrev()
        Try
            Dim pg As Integer = txtnpg.Value
            PageNumber = pg - 1
            txtnpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            parts.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1036" , "archtaskpartlisttpm.aspx.vb")
 
            parts.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

	

	

	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgitems.Columns(0).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgitems","0")
		Catch ex As Exception
		End Try
		Try
			dgitems.Columns(1).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgitems","1")
		Catch ex As Exception
		End Try
		Try
			dgitems.Columns(3).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgitems","3")
		Catch ex As Exception
		End Try
		Try
			dgitems.Columns(5).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgitems","5")
		Catch ex As Exception
		End Try
		Try
			dgitems.Columns(6).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgitems","6")
		Catch ex As Exception
		End Try
		Try
			dgitems.Columns(7).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgitems","7")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(0).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","0")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(2).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","2")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(4).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","4")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(5).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","5")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(6).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","6")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(8).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","8")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(10).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","10")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(11).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","11")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(12).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","12")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(13).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","13")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(15).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","15")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(17).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","17")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(18).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","18")
		Catch ex As Exception
		End Try
		Try
			dgoparttasks.Columns(19).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgoparttasks","19")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(0).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","0")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(2).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","2")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(4).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","4")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(5).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","5")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(6).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","6")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(7).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","7")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(9).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","9")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(11).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","11")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(12).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","12")
		Catch ex As Exception
		End Try
		Try
			dgparttasks.Columns(13).HeaderText = dlabs.GetDGPage("archtaskpartlisttpm.aspx","dgparttasks","13")
		Catch ex As Exception
		End Try

	End Sub

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2642.Text = axlabs.GetASPXPage("archtaskpartlisttpm.aspx","lang2642")
		Catch ex As Exception
		End Try
		Try
			lang2643.Text = axlabs.GetASPXPage("archtaskpartlisttpm.aspx","lang2643")
		Catch ex As Exception
		End Try
		Try
			lang2644.Text = axlabs.GetASPXPage("archtaskpartlisttpm.aspx","lang2644")
		Catch ex As Exception
		End Try
		Try
			lang2645.Text = axlabs.GetASPXPage("archtaskpartlisttpm.aspx","lang2645")
		Catch ex As Exception
		End Try
		Try
			lang2646.Text = axlabs.GetASPXPage("archtaskpartlisttpm.aspx","lang2646")
		Catch ex As Exception
		End Try
		Try
			lang2647.Text = axlabs.GetASPXPage("archtaskpartlisttpm.aspx","lang2647")
		Catch ex As Exception
		End Try
		Try
			lang2648.Text = axlabs.GetASPXPage("archtaskpartlisttpm.aspx","lang2648")
		Catch ex As Exception
		End Try
		Try
			lang2649.Text = axlabs.GetASPXPage("archtaskpartlisttpm.aspx","lang2649")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			btnreturn.Attributes.Add("src" , "../images2/eng/bgbuttons/return.gif")
			ElseIf lang = "fre" Then
			btnreturn.Attributes.Add("src" , "../images2/fre/bgbuttons/return.gif")
			ElseIf lang = "ger" Then
			btnreturn.Attributes.Add("src" , "../images2/ger/bgbuttons/return.gif")
			ElseIf lang = "ita" Then
			btnreturn.Attributes.Add("src" , "../images2/ita/bgbuttons/return.gif")
			ElseIf lang = "spa" Then
			btnreturn.Attributes.Add("src" , "../images2/spa/bgbuttons/return.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class
