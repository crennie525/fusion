﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmrrtelkup.aspx.vb" Inherits="lucy_r12.pmrrtelkup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Based Route Lookup</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function rtretwo(typ, rtid) {
            document.getElementById("lblrtid").value = rtid
            document.getElementById("lblret").value = "wo"
            document.getElementById("form1").submit();
            //window.close();
        }
        function retact() {
            if (document.getElementById("lblret").value == "wo") {
                window.close();
            }

        }
        function rtret(typ, rtid) {
            window.parent.handleprint(typ, rtid);
        }
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
     </script>
</head>
<body onload="retact();">
    <form id="form1" runat="server">
    <table width="480">
				<tr>
					<td class="label"><asp:Label id="lang1179" runat="server">Search Records</asp:Label><asp:textbox id="txtsrch" runat="server"></asp:textbox><asp:imagebutton id="btnsrch1" runat="server" Height="20px" Width="20px" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td id="tdlist" runat="server"></td>
				</tr>
                <tr id="trnav" runat="server">
				<td align="center" class="details">
					<table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
						border-right: blue 1px solid" cellspacing="0" cellpadding="0">
						<tr>
							<td style="border-right: blue 1px solid" width="20">
								<img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
									runat="server">
							</td>
							<td style="border-right: blue 1px solid" width="20">
								<img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
									runat="server">
							</td>
							<td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
								<asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
							</td>
							<td style="border-right: blue 1px solid" width="20">
								<img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
									runat="server">
							</td>
							<td width="20">
								<img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
									runat="server">
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</table>
            <input type="hidden" id="lblsid" runat="server" />
            <input type="hidden" id="lbltyp" runat="server" />
            <input type="hidden" id="lbltypx" runat="server" />
            <input type="hidden" id="lblrtid" runat="server" />
            <input type="hidden" id="lblret" runat="server" />
            <input type="hidden" id="lblpmid" runat="server" />
    </form>
</body>
</html>
