﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tpmopttasksmainnohdr.aspx.vb"
    Inherits="lucy_r12.tpmopttasksmainnohdr" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" src="../scripts/taskgridtpm.js"></script>
    <script language="JavaScript" src="../scripts1/tpmopttasksmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script language="javascript" type="text/javascript">
        function handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc) {

            document.getElementById("lbldid").value = did;
            document.getElementById("lblclid").value = clid;
            document.getElementById("lbllid").value = lid;
            document.getElementById("lblsid").value = sid;
            document.getElementById("lbltyp").value = typ;
            document.getElementById("lbleqid").value = eqid;
            document.getElementById("lblfuid").value = fuid;
            document.getElementById("lblcoid").value = coid;
            document.getElementById("lbleqnum").value = eqnum;

            document.getElementById("lbldept").value = dept;
            document.getElementById("lblcell").value = cell;
            document.getElementById("lblloc").value = loc;
            //alert(eqid + " from arch tpm")
            window.parent.handlearchret(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc);
        }
        var procflag = 0
        var navflag = 0
        function opennav() {
            if (procflag == 0) {
                if (navflag == 0) {
                    navflag = 1;
                    //procflag=1;
                    document.getElementById("tdnav").className = "viewcell";
                    document.getElementById("tdnavtop").className = "thdrsinglft viewcell";
                    document.getElementById("tdnavtoprt").className = "thdrsingrt label viewcell";
                    //document.getElementById("tdnavtop").style.width="305px";
                    document.getElementById("tdproc").className = "details";
                    document.getElementById("tdproctop").className = "details";
                    document.getElementById("tdproctoplft").className = "details";
                    document.getElementById("tdproctoplft1").className = "details";
                    document.getElementById("tdproctop").style.width = "0px";
                    document.getElementById("tblopt").style.width = "915px";
                    document.getElementById("imgnav").src = "../images/appbuttons/minibuttons/close1.gif";
                }
                else {
                    navflag = 0;
                    //procflag=0;
                    document.getElementById("tdnav").className = "details";
                    document.getElementById("tdnavtop").className = "details";
                    document.getElementById("tdnavtoprt").className = "details";
                    document.getElementById("tdnavtop").style.width = "0px";
                    document.getElementById("tdproc").className = "viewcell";
                    document.getElementById("tdproctoplft").className = "thdrsingtbo viewcell";
                    document.getElementById("tdproctop").className = "thdrsingrt label viewcell";
                    document.getElementById("tdproctoplft1").className = "thdrsingtbo viewcell";
                    document.getElementById("tdproctop").style.width = "580px";
                    document.getElementById("tblopt").style.width = "1190px";
                    document.getElementById("imgnav").src = "../images/appbuttons/minibuttons/open.gif";
                }
            }
        }
    </script>
</head>
<body onload="checkarch();opennav();">
    <form id="form1" runat="server">
    <div>
        <table id="tblopt" style="z-index: 1; left: 2px; position: absolute; top: 0px" cellspacing="0"
            cellpadding="0" width="1316">
            <tr>
                <td id="tdqs" runat="server" colspan="2">
                </td>
            </tr>
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/compresstpm.gif" border="0" id="imgopt"
                        runat="server">
                </td>
                <td class="thdrsingrt label" align="left" width="720" id="tdloctop">
                    <asp:Label ID="lang1113" runat="server">Location/Equipment/Function Details</asp:Label>
                </td>
                <td class="thdrsingtbo" valign="top" align="left" width="22">
                    <img onmouseover="return overlib('Show/Hide Asset Information', ABOVE, LEFT)" onmouseout="return nd()"
                        id="imgnav" onclick="opennav();" src="../images/appbuttons/minibuttons/open.gif"
                        runat="server">
                </td>
                <td class="thdrsinglft details" id="tdnavtop" width="22">
                    <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                </td>
                <td class="thdrsingrt label details" id="tdnavtoprt" align="left" width="268">
                    <font class="label">
                        <asp:Label ID="lang1114" runat="server">Asset Hierarchy</asp:Label></font>
                </td>
                <td class="thdrsingtbo" valign="top" align="left" width="22" id="tdproctoplft1">
                    <img onmouseover="return overlib('Expand/Shrink Procedure to Optimize', ABOVE, LEFT)"
                        onmouseout="return nd()" id="imghide" onclick="hiderev();" src="../images/appbuttons/minibuttons/close1.gif"
                        runat="server">
                </td>
                <td class="thdrsingtbo" align="left" width="26" id="tdproctoplft">
                    <img src="../images/appbuttons/minibuttons/compresstpm.gif" border="0">
                </td>
                <td class="thdrsingrt label" id="tdproctop" width="552">
                    <font class="label">
                        <asp:Label ID="lang1115" runat="server">TPM Procedure to Optimize</asp:Label></font>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <table cellspacing="0" cellpadding="0" id="tbopt" runat="server">
                        <tr>
                            <td>
                                <iframe id="geteq" style="border-top-style: none; border-right-style: none; border-left-style: none;
                                    background-color: transparent; border-bottom-style: none" src="tpmgetopt.aspx?tli=5&amp;jump=no"
                                    frameborder="no" width="746" scrolling="no" height="114" runat="server" allowtransparency>
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe id="iftaskdet" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                    margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                    border-left-style: none; background-color: transparent; border-bottom-style: none"
                                    src="tpmopttaskgrid.aspx?start=no" frameborder="no" width="746" scrolling="no"
                                    height="454" runat="server" allowtransparency></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" align="center" width="22" class="viewcell">
                    <img id="Img1" onclick="opennav();" src="../images/appbuttons/minibuttons/archlabel.gif"
                        runat="server">
                </td>
                <td class="details" id="tdnav" colspan="2" valign="top">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="2">
                                <iframe id="ifarch" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                    margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                    border-left-style: none; background-color: transparent; border-bottom-style: none"
                                    src="OptDevArchtpm.aspx?start=no" frameborder="no" width="270" scrolling="no"
                                    height="180" runat="server" allowtransparency></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="thdrsinglft" width="22">
                                <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                            </td>
                            <td class="thdrsingrt label" width="258">
                                <asp:Label ID="lang1116" runat="server">Task Images</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <iframe id="ifimg" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                    margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                    border-left-style: none; background-color: transparent; border-bottom-style: none"
                                    src="../tpmpics/taskimagetpm.aspx?eqid=0" frameborder="no" width="270" scrolling="no"
                                    height="280" runat="server" allowtransparency></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
                <td id="tdproc" valign="top" colspan="3">
                    <iframe id="ifpmdoc" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                        margin: 0px; padding-top: 0px; background-color: transparent" src="OptHoldertpm.aspx"
                        frameborder="yes" width="600" scrolling="no" height="480" runat="server" allowtransparency>
                    </iframe>
                </td>
            </tr>
        </table>
        <input id="lbltab" type="hidden" name="lbltab" runat="server">
        <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
        <input id="lblsid" type="hidden" name="lblsid" runat="server">
        <input id="lbldid" type="hidden" name="lbldid" runat="server">
        <input id="lblclid" type="hidden" name="lblclid" runat="server">
        <input id="lblchk" type="hidden" name="lblchk" runat="server">
        <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
        <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
        <input id="lblcid" type="hidden" name="lblcid" runat="server">
        <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
        <input type="hidden" id="lbltyp" runat="server" name="lbltyp">
        <input type="hidden" id="lbllid" runat="server" name="lbllid">
        <input id="lblret" type="hidden" name="lblret" runat="server">
        <input id="lbldchk" type="hidden" name="lbldchk" runat="server">
        <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
        <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
        <input id="tasknum" type="hidden" name="tasknum" runat="server">
        <input id="taskcnt" type="hidden" name="taskcnt" runat="server">
        <input type="hidden" id="retqs" runat="server">
        <input type="hidden" id="lblsave" runat="server">
        <input type="hidden" id="lblsavetasks" runat="server"><input type="hidden" id="lblro"
            runat="server">
        <input type="hidden" id="lblsubmit" runat="server">
        <input type="hidden" id="lblwho" runat="server" />
        <input type="hidden" id="lbleqnum" runat="server" />
        <input type="hidden" id="lbldept" runat="server" />
        <input type="hidden" id="lblcell" runat="server" />
        <input type="hidden" id="lblloc" runat="server" />
        <input type="hidden" id="lbluser" runat="server" />
        <input type="hidden" id="lblfslang" runat="server" />
    </div>
    </form>
</body>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
</html>
