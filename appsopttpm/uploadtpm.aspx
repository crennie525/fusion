<%@ Page Language="vb" AutoEventWireup="false" Codebehind="uploadtpm.aspx.vb" Inherits="lucy_r12.uploadtpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>upload</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table width="480">
				<tr>
					<td class="thdrsingrt label"><asp:Label id="lang1117" runat="server">Procedure Upload Dialog</asp:Label></td>
				</tr>
				<tr height="26">
					<td style="BORDER-BOTTOM: #7ba4e0 thin solid" colspan="4" class="label" id="tdhdr" runat="server"></td>
				</tr>
			</table>
			<table width="480">
				<tr>
					<td class="bluelabel"><asp:Label id="lang1118" runat="server">Choose PM´s to Upload</asp:Label></td>
					<td><input class="3DButton" id="btnupload" type="button" value="Upload" name="btnupload" runat="server"></td>
					<td class="bluelabel"><asp:Label id="lang1119" runat="server">Select Files to Delete</asp:Label></td>
					<td>
						<asp:ImageButton id="ibdelproc" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"></asp:ImageButton></td>
				</tr>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File1" type="file" size="32" name="MyFile" RunAt="Server"></td>
					<td class="labellt" id="tbfiles" vAlign="top" colSpan="2" rowSpan="10" runat="server"><asp:checkboxlist id="cblproc" cssclass="labellt" runat="server"></asp:checkboxlist></td>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File2" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File3" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File4" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File5" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
				<tr>
					<td class="label" colSpan="2" style="HEIGHT: 1px"><INPUT class="3DButton" id="File6" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File7" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File8" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File9" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
				<tr>
					<td class="label" colSpan="2"><INPUT class="3DButton" id="File10" type="file" size="32" name="MyFile" RunAt="Server"></td>
				</tr>
			</table>
			<input id="lbldoctype" type="hidden" runat="server"><input id="lbleqid" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
