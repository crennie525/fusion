

'********************************************************
'*
'********************************************************



Public Class doctpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'If Not IsPostBack Then
            Dim type, file As String
            type = Request.QueryString("type").ToString
            file = Request.QueryString("file").ToString

            If type = "1" Or type = "2" Then
                Response.Clear()
                Dim cp As New cltproc
                Dim longstring As String
                longstring = cp.PopOLD(file, type)
                Response.Write(longstring)
            Else

                If type = "pdf" Then
                    Response.ContentType = "Application/pdf"
                    Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.pdf")
                    Try
                        Dim FilePath As String = MapPath("..\eqimages\" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex As Exception
                        Try
                            Dim FilePath As String = MapPath("../eqimages/" & file)
                            Response.WriteFile(FilePath)
                            Response.End()
                        Catch ex1 As Exception
                            Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                            Response.End()
                        End Try
                    End Try
                ElseIf type = "msword" Or type = "ms-word" Then
                    Response.ContentType = "Application/msword"
                    Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.doc")
                    Try
                        Dim FilePath As String = MapPath("..\eqimages\" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex As Exception
                        Try
                            Dim FilePath As String = MapPath("../eqimages/" & file)
                            Response.WriteFile(FilePath)
                            Response.End()
                        Catch ex1 As Exception
                            Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                            Response.End()
                        End Try
                    End Try
                ElseIf type = "mswordx" Or type = "ms-wordx" Then
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.docx")
                    Try
                        Dim FilePath As String = MapPath("..\eqimages\" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex As Exception
                        Try
                            Dim FilePath As String = MapPath("../eqimages/" & file)
                            Response.WriteFile(FilePath)
                            Response.End()
                        Catch ex1 As Exception
                            Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                            Response.End()
                        End Try
                    End Try
                ElseIf type = "msexcel" Or type = "ms-excel" Then
                    Response.ContentType = "Application/x-msexcel"
                    Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.xls")
                    Try
                        Dim FilePath As String = MapPath("..\eqimages\" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex As Exception
                        Try
                            Dim FilePath As String = MapPath("../eqimages/" & file)
                            Response.WriteFile(FilePath)
                            Response.End()
                        Catch ex1 As Exception
                            Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                            Response.End()
                        End Try
                    End Try
                ElseIf type = "msexcelx" Or type = "ms-excelx" Then
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    Response.AddHeader("Content-Disposition", "inline;filename=ClientCopy.xlsx")
                    Try
                        Dim FilePath As String = MapPath("..\eqimages\" & file)
                        Response.WriteFile(FilePath)
                        Response.End()
                    Catch ex As Exception
                        Try
                            Dim FilePath As String = MapPath("../eqimages/" & file)
                            Response.WriteFile(FilePath)
                            Response.End()
                        Catch ex1 As Exception
                            Response.WriteFile(Server.MapPath("\") & "/eqimages/" & file)
                            Response.End()
                        End Try
                    End Try
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr389", "doc.aspx.vb") & " " & type & ", " & file
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub errcatch()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<html><head>")
        sb.Append("<LINK href=""../styles/pmcssa1.css"" type=""text/css"" rel=""stylesheet"">")
        sb.Append("</head><body>")
        sb.Append("<table width=""100%"" height=""100%"" border=""0""><tr><td width=""100%"" height=""100%"" class=""bluelabel"" align=""center"" valign=""center"">" & tmod.getlbl("cdlbl115", "doctpm.aspx.vb") & "<br><br>Please Verify That File Was Uploaded At This Location</td></tr></table>")
        sb.Append("</body></html>")
        Response.Clear()
        Response.ContentType = "text/html"
        Response.AddHeader("Contenet-Disposition", "inline;filename=ClientCopy.html")
        Response.Write(sb.ToString)
        'End Try
    End Sub
End Class
