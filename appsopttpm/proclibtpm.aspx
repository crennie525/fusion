<%@ Page Language="vb" AutoEventWireup="false" Codebehind="proclibtpm.aspx.vb" Inherits="lucy_r12.proclibtpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>proclib</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/proclibtpmaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 4px; POSITION: absolute; TOP: 4px" cellSpacing="1" cellPadding="1"
				width="400">
				<tr id="trsrch" runat="server">
					<td class="bluelabel"><asp:Label id="lang1072" runat="server">Search By Document Title</asp:Label></td>
					<td><asp:textbox id="txtsrch" runat="server" CssClass="plainlabel" Width="190px"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
				</tr>
				<tr id="tr1" runat="server">
					<td colSpan="3">
						<div id="supdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; HEIGHT: 200px"
							runat="server"></div>
					</td>
				</tr>
			</table>
			<input id="lblpost" type="hidden" runat="server" NAME="lblpost"><input id="lbltyp" type="hidden" runat="server" NAME="lbltyp">
			<input id="lblskillid" type="hidden" runat="server" NAME="lblskillid"><input id="lblsupid" type="hidden" runat="server" NAME="lblsupid">
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
