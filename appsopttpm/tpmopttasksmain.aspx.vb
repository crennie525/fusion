

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class tpmopttasksmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang1116 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1115 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1114 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1113 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim login As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim main As New Utilities

    Dim tasklev, sid, did, clid, eqid, fuid, coid, chk, tli, cid, jump, typ, lid, start, ro As String
    Protected WithEvents imgnav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifimg As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imghide As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdqs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents retqs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsavetasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tbopt As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents imgopt As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iftaskdet As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifpmdoc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnNext As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetArchLabel()
        'Put user code to initialize the page here
        'Dim sitst As String = Request.QueryString.ToString
        'siutils.GetAscii(Me, sitst)

        'Dim app As New AppUtils
        'Dim url As String = app.Switch
        'If url <> "ok" Then
        'Response.Redirect(url)
        'End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("PM3OptMain.aspx?jump=no")
        Catch ex As Exception

        End Try
        'sid = Request.Form("lblps")
        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    'pgtitle.InnerHtml = "TPM Optimizer (Read Only)"
                Else
                    'pgtitle.InnerHtml = "TPM Optimizer"
                End If
            Catch ex As Exception

            End Try
            Try
                start = Request.QueryString("start").ToString
                If start = "yes" Then
                    Try
                        Dim qs As String = Request.QueryString.ToString
                        retqs.Value = qs
                        Dim lvl As String = Request.QueryString("lvl").ToString
                        If lvl = "eq" Then
                            tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Equipment Screen</a>"
                        ElseIf lvl = "fu" Then
                            tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Function Screen</a>"
                        ElseIf lvl = "co" Then
                            tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Component Screen</a>"
                        End If
                    Catch ex As Exception

                    End Try
                    Try
                        Dim qs As String = Request.QueryString.ToString
                        Dim jtyp As String = Request.QueryString("jtyp").ToString
                        If jtyp = "opt" Then
                            tdqs.InnerHtml = "<a href='../appsopt/PM3OptMain.aspx?" & qs & "' class='A1'>Return to PM Optimizer</a>"
                        ElseIf jtyp = "dev" Then
                            tdqs.InnerHtml = "<a href='../apps/PMTasks.aspx?" & qs & "' class='A1'>Return to PM Developer</a>"
                        End If
                    Catch ex As Exception

                    End Try

                End If
            Catch ex As Exception

            End Try
            Try
                jump = Request.QueryString("jump").ToString
                If jump = "yes" Then

                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    Try
                        lid = Request.QueryString("lid").ToString
                        lbllid.Value = lid
                    Catch ex As Exception

                    End Try

                    tli = Request.QueryString("tli").ToString
                    lbltasklev.Value = "5" 'tli
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    did = Request.QueryString("did").ToString
                    lbldid.Value = did
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    clid = Request.QueryString("clid").ToString
                    lblclid.Value = clid
                    Try
                        fuid = Request.QueryString("funid").ToString
                        lblfuid.Value = fuid
                        coid = Request.QueryString("comid").ToString
                        lblcoid.Value = coid
                    Catch ex As Exception

                    End Try
                    chk = Request.QueryString("chk").ToString
                    lblchk.Value = chk
                    lblgetarch.Value = "yes"
                    geteq.Attributes.Add("src", "tpmgetopt.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
                End If
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx")
            End Try
        Else
            If Page.Request("lblgetarch") = "eq" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "4"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "tpmgetopt.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
            ElseIf Page.Request("lblgetarch") = "fu" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "5"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = "" 'lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "tpmgetopt.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
            ElseIf Page.Request("lblgetarch") = "co" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "5"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "tpmgetopt.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
            End If
            If Request.Form("lblsubmit") = "optit" Then
                lblsubmit.Value = ""
                main.Open()
                ArchiveEq()
                OptIt()
                main.Dispose()
                cid = lblcid.Value
                tli = "4"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "tpmgetopt.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
            ElseIf Request.Form("lblsubmit") = "archit" Then
                lblsubmit.Value = ""
                main.Open()
                ArchiveEq()
                cid = lblcid.Value
                tli = "4"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                geteq.Attributes.Add("src", "tpmgetopt.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ)
                main.Dispose()
            End If
        End If


    End Sub
    Private Sub ArchiveEq()
        
    End Sub
    Private Sub OptIt()
        Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
        Session("username") = user
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        eqid = lbleqid.Value
        sql = "usp_copyTasksToOpttpm '" & eqid & "', '" & ustr & "'"
        main.Update(sql)
    End Sub
	
    Private Sub GetArchLabel()
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            Img1.Attributes.Add("src", "../images2/eng/archlabel.gif")
        ElseIf lang = "fre" Then
            Img1.Attributes.Add("src", "../images2/fre/archlabel.gif")
        ElseIf lang = "ger" Then
            Img1.Attributes.Add("src", "../images2/ger/archlabel.gif")
        ElseIf lang = "ita" Then
            Img1.Attributes.Add("src", "../images2/ita/archlabel.gif")
        ElseIf lang = "spa" Then
            Img1.Attributes.Add("src", "../images2/spa/archlabel.gif")
        End If
    End Sub








    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1113.Text = axlabs.GetASPXPage("tpmopttasksmain.aspx", "lang1113")
        Catch ex As Exception
        End Try
        Try
            lang1114.Text = axlabs.GetASPXPage("tpmopttasksmain.aspx", "lang1114")
        Catch ex As Exception
        End Try
        Try
            lang1115.Text = axlabs.GetASPXPage("tpmopttasksmain.aspx", "lang1115")
        Catch ex As Exception
        End Try
        Try
            lang1116.Text = axlabs.GetASPXPage("tpmopttasksmain.aspx", "lang1116")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imghide.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasksmain.aspx", "imghide") & "', ABOVE, LEFT)")
            imghide.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgnav.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasksmain.aspx", "imgnav") & "', ABOVE, LEFT)")
            imgnav.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
