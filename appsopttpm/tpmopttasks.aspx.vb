

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tpmopttasks
    Inherits System.Web.UI.Page
    Protected WithEvents sgrid As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid156 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid155 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid154 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid153 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid152 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid151 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid150 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid149 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid148 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid147 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid146 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid145 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid144 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents ovid143 As System.Web.UI.HtmlControls.HtmlTableCell

    Protected WithEvents btnpfint As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang1112 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1111 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1110 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1109 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1108 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1107 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1106 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1105 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1104 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1103 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1102 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1101 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1100 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1099 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1098 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1097 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1096 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1095 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1094 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1093 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1092 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1091 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1090 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1089 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1088 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1087 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1086 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1085 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tl, cid, sid, did, clid, eqid, fuid, coid, sql, val, name, field, ttid, tnum, username, lid, typ, ro, appstr As String
    Dim co, fail, chk, tc, rteid, rbskillid, rbqty, rbfreq, rbrdid As String
    Dim tskcnt As Integer
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities

    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim Tables As String = "pmtaskstpm"
    Dim PK As String = "pmtskid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*, haspm = (select e.hastpm from equipment e where e.eqid = pmtaskstpm.eqid)"
    Dim Filter As String = ""
    Dim CntFilter As String = ""
    Dim Group As String = ""
    Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    Protected WithEvents ddtypeo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    Protected WithEvents txtqtyo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txttro As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtqty As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsvchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblenable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgholder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label20 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Label22 As System.Web.UI.WebControls.Label
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents Label23 As System.Web.UI.WebControls.Label
    Protected WithEvents lblcnt As System.Web.UI.WebControls.Label
    Protected WithEvents btnaddsubtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnsavetask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnStart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnPrev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnNext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnEnd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnlookup As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbltabid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldoctype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcleantasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgototasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddeqstato As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddeqstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label26 As System.Web.UI.WebControls.Label
    Protected WithEvents lblspg As System.Web.UI.WebControls.Label
    Protected WithEvents Label27 As System.Web.UI.WebControls.Label
    Protected WithEvents lblscnt As System.Web.UI.WebControls.Label
    Protected WithEvents txtodesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txtdesc As System.Web.UI.HtmlControls.HtmlTextArea

    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgrat As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgrid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcind As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgflag As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lbofailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbfaillist As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox

    Protected WithEvents ibfromo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibtoo As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibReuse As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibToTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibFromTask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsesscnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtrdt As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsubcount As System.Web.UI.WebControls.Label
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnaddcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblhaspm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents txtordt As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnlookup2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ibCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ggrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddtsk As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnedittask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgdeltask As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents cb2 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Checkbox1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Checkbox2 As System.Web.UI.HtmlControls.HtmlInputCheckBox

    Protected WithEvents txtpfint As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtaskstat As System.Web.UI.WebControls.DropDownList

    Protected WithEvents ocb2o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblnoeq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgcopycomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddnewfail As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromreusedis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnsav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfreqprev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreqprevo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txttr As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbloto As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbcs As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ocbcust As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbcust As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblcbcust As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblocbcust As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb1sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb2sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lcb3sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb1sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb2sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3o As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3mon As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3tue As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3wed As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3thu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3fri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3sat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locb3sun As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfreqo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox

    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreqo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblxstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblghostoff As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltpmhold As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents lblrteid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrbskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrbqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrbfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrbrdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String = "tasknum, subtask asc"

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddcomp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtcQty As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetFSLangs()
        Dim comi As New mmenu_utils_a
        Dim coi As String = comi.COMPI
        lblcomi.Value = coi
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        'Try


        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Page.EnableViewState = True
        If Request.Form("lbldel") = "delr" Then
            lbldel.Value = ""
            DelRevised()
        End If
        Dim login As String
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            'Server.Transfer("/laipm3/NewLogin.aspx")
            lbllog.Value = "no"
            Exit Sub
        End Try
        If lbllog.Value <> "no" Then
            If lblsvchk.Value = "1" Then
                goNext()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "2" Then
                goPrev()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "3" Then
                goSubNext()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "4" Then
                goSubPrev()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "5" Then
                GoFirst()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "6" Then
                GoLast()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "7" Then
                GoNav()
                'lblsave.Value = "no"
            End If
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PopComp()
                'added in case components with tasks were copied on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                coid = lblco.Value
                Try
                    ddcomp.SelectedValue = coid
                    SaveTaskComp()
                Catch ex As Exception

                End Try
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "2" Then
                lblcompchk.Value = "0"
                tasks.Open()
                PopComp()
                'added in case components deleted on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                tasks.Dispose()
                coid = lblco.Value
                Try
                    ddcomp.SelectedValue = coid
                Catch ex As Exception

                End Try
            ElseIf Request.Form("lblcompchk") = "3" Then
                lblcompchk.Value = "0"
                tasksadd.Open()
                SaveTask2()
                tasksadd.Dispose()
            ElseIf Request.Form("lblcompchk") = "4" Then
                lblcompchk.Value = "0"
                tasks.Open()
                GetLists()
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "5" Then
                lblcompchk.Value = "0"
                tasks.Open()
                SubCount(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "6" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'LoadCommon()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "7" Then
                lblcompchk.Value = "0"
                tasks.Open()
                DeleteTask()
                tasks.Dispose()

            End If

            If Request.Form("lblcompfailchk") = "1" Then
                lblcompfailchk.Value = "0"
                AddFail()
            End If
            Dim start As String = Request.QueryString("start").ToString

            If start = "no" Then
                lblpg.Text = "0"
                lblcnt.Text = "0"
                lblspg.Text = "0"
                lblscnt.Text = "0"
            Else
                lblstart.Value = "no"
            End If
            If Request.Form("lblgrid") = "yes" Then
                lblgrid.Value = ""
                val = lblfuid.Value
                field = "funcid"
                lblsb.Value = "0"
                Filter = lblfilt.Value 'field & " = " & val ' & " and subtask = 0"
                lblfilt.Value = Filter
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
                'GoToGrid()
            End If
            If Not IsPostBack Then

                'lblsave.Value = "no"
                If start = "yes" Then
                    appstr = HttpContext.Current.Session("appstr").ToString()
                    CheckApps(appstr)
                    'tc = Request.QueryString("tc").ToString
                    'lblstart.Value = "yes" & tc
                    lblcnt.Text = 0
                    lblsvchk.Value = "0"
                    lblenable.Value = "1"
                    tl = Request.QueryString("tl").ToString
                    lbltasklev.Value = tl
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    did = Request.QueryString("did").ToString
                    lbldid.Value = did
                    clid = Request.QueryString("clid").ToString
                    lblclid.Value = clid
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    chk = Request.QueryString("chk").ToString
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    If chk = "" Then
                        chk = "no"
                        lblchk.Value = chk
                    Else
                        lblchk.Value = chk
                    End If
                    tasks.Open()
                    fuid = Request.QueryString("fuid").ToString
                    lblfuid.Value = fuid
                    Dim pmid, pmstr As String
                    'pmid = Request.QueryString("pmid").ToString
                    'lbldocpmid.Value = pmid
                    'pmstr = Request.QueryString("pmstr").ToString
                    'lbldocpmstr.Value = pmstr
                    'val = fuid
                    'field = "funcid"
                    'name = "Function"
                    'eqid = Request.QueryString("eqid").ToString
                    'lbleqid.Value = eqid

                    'GetLists()
                    'lblsb.Value = "0"
                    'Filter = field & " = " & val & " and subtask = 0"
                    'lblfilt.Value = Filter

                    val = fuid
                    field = "pmtaskstpm.funcid"
                    name = "Function"
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    GetLists()
                    lblsb.Value = "0"
                    Filter = field & " = " & val & " and pmtaskstpm.subtask = 0" ' and tpm = '1'"
                    CntFilter = field & " = " & val & " and pmtaskstpm.subtask = 0" ' and tpm = '1'"
                    'Filter = field & " = " & val ' & " and subtask = 0"
                    'CntFilter = field & " = " & val & " and subtask = 0"
                    lblfilt.Value = Filter
                    lblfiltcnt.Value = CntFilter
                    Dim tasknum As String
                    Try
                        tasknum = Request.QueryString("task").ToString
                        If tasknum = "" Then
                            tasknum = "0"
                        End If
                        PageNumber = tasknum
                    Catch ex As Exception
                        tasknum = "0"
                    End Try

                    LoadPage(PageNumber, Filter)
                    btnaddtsk.Enabled = True
                    Dim lock, lockby As String
                    Dim user As String = lblusername.Value
                    'Read Only
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro

                    'End Read Only
                    lock = CheckLock(eqid)
                    If lock = "1" Then
                        lockby = lbllockedby.Value
                        If lockby = user Then
                            lbllock.Value = "0"
                        Else
                            lblro.Value = "1" '***use instead?
                            ro = "1"
                            Dim strMessage As String = tmod.getmsg("cdstr441", "tpmopttasks.aspx.vb") & " " & lockby & "."
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                    ElseIf lock = "0" Then
                        'LockRecord(user, eq)
                    End If
                    If ro = "1" Then
                        ibToTask.Visible = False
                        ibFromTask.Visible = False
                        ibfromo.Visible = False
                        ibtoo.Visible = False
                        ibReuse.Visible = False
                        todis.Attributes.Add("class", "view")
                        fromdis.Attributes.Add("class", "view")
                        fromreusedis.Attributes.Add("class", "view")
                        todis1.Attributes.Add("class", "view")
                        fromdis1.Attributes.Add("class", "view")
                        btnedittask.Attributes.Add("src", "../images/appbuttons/minibuttons/lilpendis.gif")
                        btnaddtsk.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        btnaddtsk.Enabled = False
                        imgdeltask.Attributes.Add("src", "../images/appbuttons/minibuttons/deldis.gif")
                        imgdeltask.Attributes.Add("onclick", "")
                        ibCancel.Attributes.Add("src", "../images/appbuttons/minibuttons/candisk1dis.gif")
                        ibCancel.Enabled = False
                        btnsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                        btnsav.Attributes.Add("onclick", "")
                    End If
                    tasks.Dispose()

                Else



                    'tdtaskstat.InnerHtml = "No Task Records Selected Yet"
                    'ddtaskstat.SelectedValue = "Select"
                    btnaddtsk.Enabled = True
                    lblpg.Text = "0"
                    lblcnt.Text = "0"
                    lblspg.Text = "0"
                    lblscnt.Text = "0"
                End If

                Dim locked As String = lbllock.Value
                If locked = "1" Then
                    'btnedittask.Attributes.Add("class", "details")
                    'btnaddtsk.Attributes.Add("class", "details")
                    'ibCancel.Attributes.Add("class", "details")
                    'IMG2.Attributes.Add("class", "details")
                    'ggrid.Attributes.Add("class", "details")
                    'btnaddcomp.Attributes.Add("class", "details")
                    'imgrat.Attributes.Add("class", "details")
                End If
                'Disable all fields
                If ro <> "1" Then
                    DisableOptions()
                    ddtaskstat.Enabled = False 'orig
                    ddcomp.Enabled = False
                    ''lbfaillist.Enabled = False
                    ''lbfailmodes.Enabled = False
                    ibToTask.Enabled = False
                    ibFromTask.Enabled = False
                    ibReuse.Enabled = False
                    txtdesc.Disabled = True
                    txtrdt.Enabled = False
                    txtordt.Enabled = False
                    txtodesc.Disabled = True 'orig
                    ddtype.Enabled = False
                    ddtypeo.Enabled = False 'orig

                    txtfreq.Enabled = False

                    txtfreqo.Enabled = False
                    txtpfint.Enabled = False
                    'ddskill.Enabled = False
                    'ddskillo.Enabled = False 'orig
                    txtqty.Enabled = False
                    txtqtyo.Enabled = False 'orig
                    txttr.Enabled = False
                    txttro.Enabled = False 'orig
                    'ddpt.Enabled = False
                    'ddpto.Enabled = False 'orig
                    ddeqstat.Enabled = False
                    ddeqstato.Enabled = False 'orig
                    lblenable.Value = "1"
                    txtcQty.Enabled = False
                    'lbCompFM.Enabled = False
                    ''lbofailmodes.Enabled = False 'orig
                    'buttons
                    btnaddtsk.Enabled = True
                    btnaddsubtask.Enabled = False
                    'btndeltask.Enabled = False
                    ibCancel.Enabled = False
                    btnsavetask.Enabled = False

                    cbcust.Disabled = True
                    ocbcust.Disabled = True

                    cb1o.Disabled = True
                    cb1.Disabled = True
                    cb1mon.Disabled = True
                    cb1tue.Disabled = True
                    cb1wed.Disabled = True
                    cb1thu.Disabled = True
                    cb1fri.Disabled = True
                    cb1sat.Disabled = True
                    cb1sun.Disabled = True

                    cb2o.Disabled = True
                    cb2.Disabled = True
                    cb2mon.Disabled = True
                    cb2tue.Disabled = True
                    cb2wed.Disabled = True
                    cb2thu.Disabled = True
                    cb2fri.Disabled = True
                    cb2sat.Disabled = True
                    cb2sun.Disabled = True

                    cb3o.Disabled = True
                    cb3.Disabled = True
                    cb3mon.Disabled = True
                    cb3tue.Disabled = True
                    cb3wed.Disabled = True
                    cb3thu.Disabled = True
                    cb3fri.Disabled = True
                    cb3sat.Disabled = True
                    cb3sun.Disabled = True

                    ocb1o.Disabled = True
                    ocb1.Disabled = True
                    ocb1mon.Disabled = True
                    ocb1tue.Disabled = True
                    ocb1wed.Disabled = True
                    ocb1thu.Disabled = True
                    ocb1fri.Disabled = True
                    ocb1sat.Disabled = True
                    ocb1sun.Disabled = True

                    ocb2o.Disabled = True
                    ocb2.Disabled = True
                    ocb2mon.Disabled = True
                    ocb2tue.Disabled = True
                    ocb2wed.Disabled = True
                    ocb2thu.Disabled = True
                    ocb2fri.Disabled = True
                    ocb2sat.Disabled = True
                    ocb2sun.Disabled = True

                    ocb3o.Disabled = True
                    ocb3.Disabled = True
                    ocb3mon.Disabled = True
                    ocb3tue.Disabled = True
                    ocb3wed.Disabled = True
                    ocb3thu.Disabled = True
                    ocb3fri.Disabled = True
                    ocb3sat.Disabled = True
                    ocb3sun.Disabled = True
                End If

                'end disable
                btnsavetask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov120", "tpmopttasks.aspx.vb") & "')")
                btnsavetask.Attributes.Add("onmouseout", "return nd()")
                'btndeltask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov121" , "tpmopttasks.aspx.vb") & "')")
                'btndeltask.Attributes.Add("onmouseout", "return nd()")
                btnaddsubtask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov122", "tpmopttasks.aspx.vb") & "')")
                btnaddsubtask.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov123", "tpmopttasks.aspx.vb") & "')")
                btnaddtsk.Attributes.Add("onmouseout", "return nd()")
                btnaddtsk.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
                btnedittask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov124", "tpmopttasks.aspx.vb") & "')")
                btnedittask.Attributes.Add("onmouseout", "return nd()")
                ibToTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov125", "tpmopttasks.aspx.vb") & "')")
                ibToTask.Attributes.Add("onmouseout", "return nd()")
                'ibToTask.Attributes.Add("onclick", "DisableButton(this);")
                ibReuse.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov126", "tpmopttasks.aspx.vb") & "')")
                ibReuse.Attributes.Add("onmouseout", "return nd()")
                'ibReuse.Attributes.Add("onclick", "DisableButton(this);")
                ibCancel.Attributes.Add("onmouseout", "return nd()")
                ibCancel.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov127", "tpmopttasks.aspx.vb") & "')")
                ibFromTask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov128", "tpmopttasks.aspx.vb") & "')")
                ibFromTask.Attributes.Add("onmouseout", "return nd()")
                'ibFromTask.Attributes.Add("onclick", "DisableButton(this);")
                btnPrev.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov129", "tpmopttasks.aspx.vb") & "')")
                btnPrev.Attributes.Add("onmouseout", "return nd()")
                btnPrev.Attributes.Add("onclick", "confirmExit('tb', 'opt');")
                btnStart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov130", "tpmopttasks.aspx.vb") & "')")
                btnStart.Attributes.Add("onmouseout", "return nd()")
                btnStart.Attributes.Add("onclick", "confirmExit('ts', 'opt');")
                btnNext.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov131", "tpmopttasks.aspx.vb") & "')")
                btnNext.Attributes.Add("onmouseout", "return nd()")
                btnNext.Attributes.Add("onclick", "confirmExit('tf', 'opt');")
                btnEnd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov132", "tpmopttasks.aspx.vb") & "')")
                btnEnd.Attributes.Add("onmouseout", "return nd()")
                btnEnd.Attributes.Add("onclick", "confirmExit('te', 'opt');")
                'btnsubprev.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov133" , "tpmopttasks.aspx.vb") & "')")
                'btnsubprev.Attributes.Add("onmouseout", "return nd()")
                'btnsubprev.Attributes.Add("onclick", "confirmExit('sb');")
                'btnsubnext.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov134" , "tpmopttasks.aspx.vb") & "')")
                'btnsubnext.Attributes.Add("onmouseout", "return nd()")
                'btnsubnext.Attributes.Add("onclick", "confirmExit('sf');")
                btnlookup.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov135", "tpmopttasks.aspx.vb") & "')")
                btnlookup.Attributes.Add("onmouseout", "return nd()")
                btnlookup2.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov136", "tpmopttasks.aspx.vb") & "')")
                btnlookup2.Attributes.Add("onmouseout", "return nd()")
                'imgrat.Attributes.Add("onclick", "confirmExit('rat');")
                ibtoo.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov137", "tpmopttasks.aspx.vb") & "')")
                ibtoo.Attributes.Add("onmouseout", "return nd()")
                'ibtoo.Attributes.Add("onclick", "DisableButton(this);")
                ibfromo.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov138", "tpmopttasks.aspx.vb") & "')")
                ibfromo.Attributes.Add("onmouseout", "return nd()")
                'ibfromo.Attributes.Add("onclick", "DisableButton(this);")
                ddcomp.Attributes.Add("onchange", "chngchk();")
                ddtypeo.Attributes.Add("onchange", "GetType('o', this.value);")
                ddtaskstat.Attributes.Add("onchange", "CheckDel(this.value);")
                ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
                'ddpto.Attributes.Add("onchange", "document.getElementById('ddpt').value=this.value;")
                'ddskillo.Attributes.Add("onchange", "document.getElementById('ddskill').value=this.value;")

                ddeqstato.Attributes.Add("onchange", "document.getElementById('ddeqstat').value=this.value;zerodt('o', this.value);")
                txtqtyo.Attributes.Add("onchange", "document.getElementById('txtqty').value=this.value;")
                txttro.Attributes.Add("onkeyup", "filldown('o', this.value);")
                txttr.Attributes.Add("onkeyup", "filldown('r', this.value);")
                ddeqstat.Attributes.Add("onchange", "zerodt('d', this.value);")

                txtordt.Attributes.Add("onkeyup", "document.getElementById('txtrdt').value=this.value;")

                

                cb1o.Attributes.Add("onclick", "checkcb1o('cb1o');")
                cb2o.Attributes.Add("onclick", "checkcb1o('cb2o');")
                cb3o.Attributes.Add("onclick", "checkcb1o('cb3o');")

                ocb1o.Attributes.Add("onclick", "checkocb1o('ocb1o','cb1o');")
                ocb2o.Attributes.Add("onclick", "checkocb1o('ocb2o','cb2o');")
                ocb3o.Attributes.Add("onclick", "checkocb1o('ocb3o','cb3o');")

                cb1.Attributes.Add("onclick", "checkcb1();")
                cb2.Attributes.Add("onclick", "checkcb2();")
                cb3.Attributes.Add("onclick", "checkcb3();")

                ocb1.Attributes.Add("onclick", "checkocb1();")
                ocb2.Attributes.Add("onclick", "checkocb2();")
                ocb3.Attributes.Add("onclick", "checkocb3();")

                cb1mon.Attributes.Add("onclick", "checkday1('cb1mon');")
                cb1tue.Attributes.Add("onclick", "checkday1('cb1tue');")
                cb1wed.Attributes.Add("onclick", "checkday1('cb1wed');")
                cb1thu.Attributes.Add("onclick", "checkday1('cb1thu');")
                cb1fri.Attributes.Add("onclick", "checkday1('cb1fri');")
                cb1sat.Attributes.Add("onclick", "checkday1('cb1sat');")
                cb1sun.Attributes.Add("onclick", "checkday1('cb1sun');")

                cb2mon.Attributes.Add("onclick", "checkday2('cb2mon');")
                cb2tue.Attributes.Add("onclick", "checkday2('cb2tue');")
                cb2wed.Attributes.Add("onclick", "checkday2('cb2wed');")
                cb2thu.Attributes.Add("onclick", "checkday2('cb2thu');")
                cb2fri.Attributes.Add("onclick", "checkday2('cb2fri');")
                cb2sat.Attributes.Add("onclick", "checkday2('cb2sat');")
                cb2sun.Attributes.Add("onclick", "checkday2('cb2sun');")

                cb3mon.Attributes.Add("onclick", "checkday3('cb3mon');")
                cb3tue.Attributes.Add("onclick", "checkday3('cb3tue');")
                cb3wed.Attributes.Add("onclick", "checkday3('cb3wed');")
                cb3thu.Attributes.Add("onclick", "checkday3('cb3thu');")
                cb3fri.Attributes.Add("onclick", "checkday3('cb3fri');")
                cb3sat.Attributes.Add("onclick", "checkday3('cb3sat');")
                cb3sun.Attributes.Add("onclick", "checkday3('cb3sun');")

                ocb1mon.Attributes.Add("onclick", "checkday1o('ocb1mon','cb1mon');")
                ocb1tue.Attributes.Add("onclick", "checkday1o('ocb1tue','cb1tue');")
                ocb1wed.Attributes.Add("onclick", "checkday1o('ocb1wed','cb1wed');")
                ocb1thu.Attributes.Add("onclick", "checkday1o('ocb1thu','cb1thu');")
                ocb1fri.Attributes.Add("onclick", "checkday1o('ocb1fri','cb1fri');")
                ocb1sat.Attributes.Add("onclick", "checkday1o('ocb1sat','cb1sat');")
                ocb1sun.Attributes.Add("onclick", "checkday1o('ocb1sun','cb1sun');")

                ocb2mon.Attributes.Add("onclick", "checkday2o('ocb2mon','cb2mon');")
                ocb2tue.Attributes.Add("onclick", "checkday2o('ocb2tue','cb2tue');")
                ocb2wed.Attributes.Add("onclick", "checkday2o('ocb2wed','cb2wed');")
                ocb2thu.Attributes.Add("onclick", "checkday2o('ocb2thu','cb2thu');")
                ocb2fri.Attributes.Add("onclick", "checkday2o('ocb2fri','cb2fri');")
                ocb2sat.Attributes.Add("onclick", "checkday2o('ocb2sat','cb2sat');")
                ocb2sun.Attributes.Add("onclick", "checkday2o('ocb2sun','cb2sun');")

                ocb3mon.Attributes.Add("onclick", "checkday3o('ocb3mon','cb3mon');")
                ocb3tue.Attributes.Add("onclick", "checkday3o('ocb3tue','cb3tue');")
                ocb3wed.Attributes.Add("onclick", "checkday3o('ocb3wed','cb3wed');")
                ocb3thu.Attributes.Add("onclick", "checkday3o('ocb3thu','cb3thu');")
                ocb3fri.Attributes.Add("onclick", "checkday3o('ocb3fri','cb3fri');")
                ocb3sat.Attributes.Add("onclick", "checkday3o('ocb3sat','cb3sat');")
                ocb3sun.Attributes.Add("onclick", "checkday3o('ocb3sun','cb3sun');")

                '***** CHECK THIS FOR METERS - ANYTHING FREQ RELATED
                txtfreqo.Attributes.Add("onkeyup", "document.getElementById('txtfreq').value=this.value;")
                txtfreq.Attributes.Add("onkeyup", "checkfreq17(this.value);")
                txtfreqo.Attributes.Add("onkeyup", "checkfreq17o(this.value);")

            End If
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim e As String = "0"
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "eq" Then
                    e = "1"
                End If
            Next
            If e <> "1" Then
                lblnoeq.Value = "1"
                btnaddcomp.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                btnaddcomp.Attributes.Add("onclick", "")
                btnaddnewfail.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                btnaddnewfail.Attributes.Add("onclick", "")
                imgcopycomp.Attributes.Add("onclick", "")
            End If
        Else
            lblnoeq.Value = "0"
        End If

    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        Dim lock As String
        sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = tasks.GetRdrData(sql)
            While dr.Read
                lock = dr.Item("locked").ToString
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString
            End While
            dr.Close()
        Catch ex As Exception

        End Try
        Return lock
    End Function
    Private Sub GoToGrid()

        chk = lblchk.Value
        cid = lblcid.Value
        Dim tid As String = lbltaskid.Value
        Dim funid As String = lblfuid.Value
        Dim comid As String = lblco.Value
        clid = lblclid.Value

        Response.Redirect("../appstpm/GTasksFunctpm.aspx?tli=5a&funid=" & funid & "&comid=no&cid=" & cid & "&chk=" & chk & "&date=" & Now() & " Target='_top'")
    End Sub
    Private Sub DisableOptions()
        'btnaddtsk.Enabled = False
        'btnaddsubtask.Enabled = False
        'btndeltask.Enabled = False
        'ibCancel.Enabled = False
        'btnsavetask.Enabled = False
    End Sub
    Private Sub EnableOptions()
        btnaddtsk.Enabled = True
        btnaddsubtask.Enabled = True
        'btndeltask.Enabled = True
        ibCancel.Enabled = True
        btnsavetask.Enabled = True
    End Sub
    Private Function SubCount(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim scnt As Integer
        fuid = lblfuid.Value
        Try
            sql = "select count(*) from pmtaskstpm where funcid = '" & fuid & "' and tasknum = " & PageNumber & " and subtask <> '0'"
            Try
                scnt = tasks.Scalar(sql)
            Catch ex As Exception
                scnt = tasksadd.Scalar(sql)
            End Try
            lblsubcount.Text = scnt
        Catch ex As Exception
            scnt = 0
        End Try

        Return scnt
    End Function
    Private Function SubTaskCnt(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim tcnt As Integer
        sql = "select count(*) from pmtaskstpm where " & Filter & " and tasknum <= " & PageNumber & " and subtask = '0'"
        tcnt = tasks.Scalar(sql)
        Dim scnt As Integer
        sql = "select count(*) from pmtaskstpm where " & Filter & " and tasknum < " & PageNumber & " and subtask <> '0'"
        scnt = tasks.Scalar(sql)
        Dim wcnt = tcnt + scnt
        Return wcnt
    End Function
    Private Sub goSubNext()
        If Len(lblfuid.Value) <> 0 Then
            Dim scnt, cscnt, currcnt As Integer
            cscnt = lblsb.Value
            Filter = lblfilt.Value
            PageNumber = lblpg.Text
            tasks.Open()
            scnt = SubCount(PageNumber, Filter)
            currcnt = SubTaskCnt(PageNumber, Filter)
            lblcurrsb.Value = currcnt
            If scnt > 0 Then
                cscnt = cscnt + 1
                lblcurrcs.Value = cscnt
                If cscnt <= scnt Then
                    lblsb.Value = cscnt
                    PageNumber = currcnt + cscnt
                    LoadPage(PageNumber, Filter)
                    lblspg.Text = cscnt
                    lblscnt.Text = scnt
                Else

                    Dim strMessage As String = tmod.getmsg("cdstr442", "tpmopttasks.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
            lblenable.Value = "1"
            tasks.Dispose()
        End If

    End Sub
    Private Sub goSubPrev()
        If Len(lblfuid.Value) <> 0 Then
            Dim scnt, cscnt, currcnt As Integer
            cscnt = lblsb.Value
            Filter = lblfilt.Value
            PageNumber = lblpg.Text
            tasks.Open()
            scnt = SubCount(PageNumber, Filter)
            currcnt = SubTaskCnt(PageNumber, Filter)
            lblcurrsb.Value = currcnt
            cscnt = cscnt - 1
            lblcurrcs.Value = cscnt
            If cscnt > 0 Then
                lblsb.Value = cscnt
                PageNumber = currcnt + cscnt
            Else
                lblsb.Value = "0"
                PageNumber = lblpgholder.Value
            End If
            lblspg.Text = cscnt
            lblscnt.Text = scnt
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            lblenable.Value = "1"
        End If
    End Sub
    Private Sub GoNav()
        Dim pg As String = pgflag.Value
        If pg <> "0" Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = pg
            tasks.Open()
            'Filter = Filter & " and tasknum = " & PageNumber
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            lblenable.Value = "1"
        End If
    End Sub
    Private Sub GoFirst()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = 1
            'Filter = Filter & " and tasknum = " & PageNumber
            tasks.Open()
            LoadPage(PageNumber, Filter)
            lblenable.Value = "1"
            tasks.Dispose()
        End If
    End Sub
    Private Sub GoLast()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = lblcnt.Text
            'Filter = Filter & " and tasknum = " & PageNumber
            tasks.Open()
            LoadPage(PageNumber, Filter)
            lblenable.Value = "1"
            tasks.Dispose()
        End If
    End Sub
    Private Sub DeleteTask()
        fuid = lblfuid.Value
        tnum = lblt.Value
        ttid = lbltaskid.Value
        'RBAS
        rteid = lblrteid.Value
        If rteid <> "0" And rteid <> "" Then
            rbskillid = lblrbskillid.Value
            rbqty = lblrbqty.Value
            rbfreq = lblrbfreq.Value
            rbrdid = lblrbrdid.Value
            RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
        End If

        sql = "usp_delTPMTask '" & fuid & "', '" & tnum & "', '0'"
        tasks.Update(sql)
        PageNumber = lblpg.Text
        tskcnt = lblcnt.Text
        tskcnt = tskcnt - 1
        lblcnt.Text = tskcnt
        If PageNumber <= tskcnt Then
            PageNumber = PageNumber
        Else
            PageNumber = tskcnt
        End If
        If PageNumber = 0 Then
            PageNumber = 1
        End If
        eqid = lbleqid.Value
        tasks.UpMod(eqid)
        Filter = lblfilt.Value
        

        LoadPage(PageNumber, Filter)
    End Sub
    Private Sub goNext()
        Filter = lblfilt.Value
        If Len(lblfuid.Value) <> 0 Then
            If lblsb.Value = "0" Then
                PageNumber = lblpg.Text
            Else
                PageNumber = lblpgholder.Value
            End If

            lblsb.Value = "0"
            PageNumber = PageNumber + 1
            'Filter = Filter & " and tasknum = " & PageNumber
            If PageNumber <= lblcnt.Text Then
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            Else
                Dim strMessage As String = tmod.getmsg("cdstr443", "tpmopttasks.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            lblenable.Value = "1"
        End If

    End Sub
    Private Sub goPrev()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            If lblsb.Value = "0" Or lblspg.Text = "1" Then
                PageNumber = lblpg.Text
            Else
                PageNumber = lblpgholder.Value
            End If
            lblsb.Value = "0"
            'PageNumber = lblpg.Text

            If PageNumber > 1 Then
                PageNumber = PageNumber - 1
                'Filter = Filter & " and tasknum = " & PageNumber
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf lblspg.Text = "1" Then
                PageNumber = PageNumber
                'Filter = Filter & " and tasknum = " & PageNumber
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            Else

                Dim strMessage As String = tmod.getmsg("cdstr444", "tpmopttasks.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            lblenable.Value = "1"
        End If

    End Sub
    Private Sub PopComp()

        fuid = lblfuid.Value
        sql = "select comid, compnum + ' - ' + isnull(compdesc, '') as compnum " _
          + "from components where func_id = '" & fuid & "' order by compnum desc"
        dr = tasks.GetRdrData(sql)
        ddcomp.DataSource = dr
        ddcomp.DataTextField = "compnum"
        ddcomp.DataValueField = "comid"
        ddcomp.DataBind()
        dr.Close()
        ddcomp.Items.Insert(0, New ListItem("Select"))
        ddcomp.Items(0).Value = 0

    End Sub
    Private Sub AddFail()
        Dim comp As String = lblco.Value
        tasks.Open()
        PopCompFailList(comp)
        PopFailList(comp)
        PopTaskFailModes(comp)
        tasks.Dispose()
    End Sub
    Private Sub GetLists()
        'msglbl.Text = ""
        'cid = lblcid.Value
        cid = "0"
        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where tasktype <> 'Select' and ttid in (2, 3, 5, 6) order by compid"
        dr = tasks.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0
        'orig
        dr = tasks.GetRdrData(sql)
        ddtypeo.DataSource = dr
        ddtypeo.DataBind()
        dr.Close()
        ddtypeo.Items.Insert(0, New ListItem("Select"))
        ddtypeo.Items(0).Value = 0

        sid = HttpContext.Current.Session("dfltps").ToString() 'lblsid.Value
        sid = lblsid.Value





        sql = "select statid, status " _
        + "from pmStatus order by compid"
        dr = tasks.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        ddeqstat.DataBind()
        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0
        'orig
        dr = tasks.GetRdrData(sql)
        ddeqstato.DataSource = dr
        ddeqstato.DataTextField = "status"
        ddeqstato.DataValueField = "statid"
        ddeqstato.DataBind()
        dr.Close()
        ddeqstato.Items.Insert(0, New ListItem("Select"))
        ddeqstato.Items(0).Value = 0

        PopComp()
    End Sub
    Private Sub CleanPage()
        lblt.Value = ""
        lbltaskid.Value = ""
        lblst.Value = ""
        Try
            ddtype.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddtypeo.SelectedIndex = 0
        Catch ex As Exception

        End Try
       

        'ddpt.SelectedIndex = 0
        'ddpto.SelectedIndex = 0
        'ddskill.SelectedIndex = 0
        'ddskillo.SelectedIndex = 0
        Try
            ddeqstat.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddeqstato.SelectedIndex = 0
        Catch ex As Exception

        End Try

        txtdesc.Value = ""
        txtodesc.Value = ""
        txtqty.Text = ""
        txtqtyo.Text = ""
        txttr.Text = ""
        txttro.Text = ""
        txtordt.Text = ""
        txtrdt.Text = ""
        lblcind.Value = ""
        Try
            ddtaskstat.SelectedIndex = 0
        Catch ex As Exception

        End Try

        cbloto.Checked = False
        cbcs.Checked = False
        lblco.Value = ""
        txtcQty.Text = ""
        lbfaillist.Items.Clear()
        lbfailmodes.Items.Clear()
        lbofailmodes.Items.Clear()
        lbCompFM.Items.Clear()
    End Sub
    Private Sub LoadPage(ByVal PageNumber As Integer, ByVal Filter As String)
        '**** Added for PM Manager
        eqid = lbleqid.Value
        'Try
        'lblhaspm.Value = tasks.HasPM(eqid)
        'Catch ex As Exception
        'lblhaspm.Value = tasksadd.HasPM(eqid)
        'End Try
        Dim thold As String
        If lblsb.Value = "0" Then
            Dim scnt As Integer
            scnt = SubCount(PageNumber, Filter)
            lblscnt.Text = scnt
            lblsubcount.Text = scnt
            lblpgholder.Value = PageNumber
            lblspg.Text = "0"
            cbloto.Checked = False
            cbcs.Checked = False
        End If
        Dim tskcnt, optcnt As Integer
        tskcnt = 0
        If tskcnt = 0 Then
            CntFilter = lblfiltcnt.Value
            sql = "select count(*) from pmtaskstpm where " & CntFilter
            Try
                tskcnt = tasks.Scalar(sql)
            Catch ex As Exception
                tskcnt = tasksadd.Scalar(sql)
            End Try
        End If
        lblcnt.Text = tskcnt
        lblpg.Text = PageNumber
        Dim cr, rv As String
        If tskcnt = 0 Then
            lblpg.Text = "0"
            lblcnt.Text = tskcnt
            CleanPage()
            Dim strMessage As String = tmod.getmsg("cdstr445", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Dim JTables, J2Tables, JPK, J2PK As String
            Tables = "pmtaskstpm"
            JTables = "pmtasksdays"
            J2Tables = "pmotasksdays"
            PK = "pmtaskstpm.pmtskid"
            JPK = "pmtasksdays.pmtskid"
            J2PK = "pmotasksdays.pmtskid"
            fuid = lblfuid.Value
            Try
                dr = tasks.GetPage3(Tables, JTables, J2Tables, PK, JPK, J2PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            Catch ex As Exception
                dr = tasksadd.GetPage3(Tables, JTables, J2Tables, PK, JPK, J2PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            End Try
            Dim cust, ocust As String
            While dr.Read
                rteid = dr.Item("rteid").ToString

                txtpfint.Text = dr.Item("pfInterval").ToString
                lblt.Value = dr.Item("tasknum").ToString
                ttid = dr.Item("pmtskid").ToString
                lbltaskid.Value = ttid
                lblst.Value = dr.Item("subtask").ToString
                cust = dr.Item("cust").ToString
                ocust = dr.Item("ocust").ToString
                thold = dr.Item("tpmhold").ToString
                lbltpmhold.Value = thold
                lblcbcust.Value = cust
                lblocbcust.Value = ocust
                If cust = "1" Then
                    cbcust.Checked = True
                Else
                    cbcust.Checked = False
                End If
                If ocust = "1" Then
                    ocbcust.Checked = True
                Else
                    ocbcust.Checked = False
                End If

                Try
                    ddtype.SelectedValue = dr.Item("ttid").ToString
                Catch ex As Exception
                    Try
                        ddtype.SelectedIndex = 0
                    Catch ex1 As Exception

                    End Try

                End Try
                Try
                    ddtypeo.SelectedValue = dr.Item("origttid").ToString 'orig
                Catch ex As Exception
                End Try
                If cust <> "1" Then
                    Try

                        txtfreq.Text = dr.Item("freq").ToString 'was freqid
                        'lblfreq.Value = dr.Item("freq").ToString
                    Catch ex As Exception
                        Try
                            txtfreq.Text = ""
                        Catch ex1 As Exception

                        End Try

                    End Try
                Else
                    Try
                        txtfreq.Text = ""
                    Catch ex As Exception

                    End Try

                End If
                If ocust <> "1" Then
                    Try

                        txtfreqo.Text = dr.Item("origfreq").ToString 'orig ' was origfreqid
                        'lblfreqo.Value = dr.Item("origfreq").ToString
                    Catch ex As Exception
                        Try
                            txtfreqo.Text = ""
                        Catch ex1 As Exception

                        End Try

                    End Try
                Else
                    Try
                        txtfreqo.Text = ""
                    Catch ex As Exception

                    End Try

                End If

                Try
                    ddeqstat.SelectedValue = dr.Item("rdid").ToString
                Catch ex As Exception
                    Try
                        ddeqstat.SelectedIndex = 0
                    Catch ex1 As Exception

                    End Try

                End Try
                Try
                    ddeqstato.SelectedValue = dr.Item("origrdid").ToString 'orig
                Catch ex As Exception
                    Try
                        ddeqstato.SelectedIndex = 0
                    Catch ex1 As Exception

                    End Try

                End Try
                txtdesc.Value = dr.Item("taskdesc").ToString
                txtodesc.Value = dr.Item("otaskdesc").ToString 'orig
                txtqty.Text = dr.Item("qty").ToString
                txtqtyo.Text = dr.Item("origqty").ToString 'orig
                txttr.Text = dr.Item("tTime").ToString
                txttro.Text = dr.Item("origtTime").ToString 'orig
                txtordt.Text = dr.Item("origrdt").ToString
                txtrdt.Text = dr.Item("rdt").ToString
                lblcind.Value = dr.Item("compindex").ToString
                Try
                    ddtaskstat.SelectedValue = dr.Item("taskstatus").ToString
                Catch ex As Exception
                    Try
                        ddtaskstat.SelectedIndex = 0
                    Catch ex1 As Exception

                    End Try

                End Try

                If dr.Item("lotoid").ToString = "1" Then
                    cbloto.Checked = True
                Else
                    cbloto.Checked = False
                End If
                If dr.Item("conid").ToString = "1" Then
                    cbcs.Checked = True
                Else
                    cbcs.Checked = False
                End If
                co = dr.Item("comid").ToString
                If Len(co) = 0 Or co = "" Then
                    co = "0"
                End If
                lblco.Value = co
                txtcQty.Text = dr.Item("cqty").ToString
                If txtcQty.Text = "" Then
                    txtcQty.Text = "1"
                End If

                If dr.Item("cb1o").ToString = "1" Then
                    lcb1o.Value = "1"
                    cb1o.Checked = True
                Else
                    lcb1o.Value = "0"
                    cb1o.Checked = False
                End If
                If dr.Item("cb1").ToString = "1" Then
                    cb1.Checked = True
                    cb1mon.Checked = True
                    cb1tue.Checked = True
                    cb1wed.Checked = True
                    cb1thu.Checked = True
                    cb1fri.Checked = True
                    cb1sat.Checked = True
                    cb1sun.Checked = True

                    lcb1.Value = "1"
                    lcb1mon.Value = "1"
                    lcb1tue.Value = "1"
                    lcb1wed.Value = "1"
                    lcb1thu.Value = "1"
                    lcb1fri.Value = "1"
                    lcb1sat.Value = "1"
                    lcb1sun.Value = "1"
                Else
                    lcb1.Value = "0"
                    cb1.Checked = False
                    If dr.Item("cb1mon").ToString = "1" Then
                        lcb1mon.Value = "1"
                        cb1mon.Checked = True
                    Else
                        lcb1mon.Value = "0"
                        cb1mon.Checked = False
                    End If
                    If dr.Item("cb1tue").ToString = "1" Then
                        lcb1tue.Value = "1"
                        cb1tue.Checked = True
                    Else
                        lcb1tue.Value = "0"
                        cb1tue.Checked = False
                    End If
                    If dr.Item("cb1wed").ToString = "1" Then
                        lcb1wed.Value = "1"
                        cb1wed.Checked = True
                    Else
                        lcb1wed.Value = "0"
                        cb1wed.Checked = False
                    End If
                    If dr.Item("cb1thu").ToString = "1" Then
                        lcb1thu.Value = "1"
                        cb1thu.Checked = True
                    Else
                        lcb1thu.Value = "0"
                        cb1thu.Checked = False
                    End If
                    If dr.Item("cb1fri").ToString = "1" Then
                        lcb1fri.Value = "1"
                        cb1fri.Checked = True
                    Else
                        lcb1fri.Value = "0"
                        cb1fri.Checked = False
                    End If
                    If dr.Item("cb1sat").ToString = "1" Then
                        lcb1sat.Value = "1"
                        cb1sat.Checked = True
                    Else
                        lcb1sat.Value = "0"
                        cb1sat.Checked = False
                    End If
                    If dr.Item("cb1sun").ToString = "1" Then
                        lcb1sun.Value = "1"
                        cb1sun.Checked = True
                    Else
                        lcb1sun.Value = "0"
                        cb1sun.Checked = False
                    End If
                End If
                'End If

                If dr.Item("cb2o").ToString = "1" Then
                    lcb2o.Value = "1"
                    cb2o.Checked = True
                Else
                    lcb2o.Value = "0"
                    cb2o.Checked = False
                End If
                If dr.Item("cb2").ToString = "1" Then
                    cb2.Checked = True
                    cb2mon.Checked = True
                    cb2tue.Checked = True
                    cb2wed.Checked = True
                    cb2thu.Checked = True
                    cb2fri.Checked = True
                    cb2sat.Checked = True
                    cb2sun.Checked = True

                    lcb2.Value = "1"
                    lcb2mon.Value = "1"
                    lcb2tue.Value = "1"
                    lcb2wed.Value = "1"
                    lcb2thu.Value = "1"
                    lcb2fri.Value = "1"
                    lcb2sat.Value = "1"
                    lcb2sun.Value = "1"
                Else
                    lcb2.Value = "0"
                    cb2.Checked = False
                    If dr.Item("cb2mon").ToString = "1" Then
                        lcb2mon.Value = "1"
                        cb2mon.Checked = True
                    Else
                        lcb2mon.Value = "0"
                        cb2mon.Checked = False
                    End If
                    If dr.Item("cb2tue").ToString = "1" Then
                        lcb2tue.Value = "1"
                        cb2tue.Checked = True
                    Else
                        lcb2tue.Value = "0"
                        cb2tue.Checked = False
                    End If
                    If dr.Item("cb2wed").ToString = "1" Then
                        lcb2wed.Value = "1"
                        cb2wed.Checked = True
                    Else
                        lcb2wed.Value = "0"
                        cb2wed.Checked = False
                    End If
                    If dr.Item("cb2thu").ToString = "1" Then
                        lcb2thu.Value = "1"
                        cb2thu.Checked = True
                    Else
                        lcb2thu.Value = "0"
                        cb2thu.Checked = False
                    End If
                    If dr.Item("cb2fri").ToString = "1" Then
                        lcb2fri.Value = "1"
                        cb2fri.Checked = True
                    Else
                        lcb2fri.Value = "0"
                        cb2fri.Checked = False
                    End If
                    If dr.Item("cb2sat").ToString = "1" Then
                        lcb2sat.Value = "1"
                        cb2sat.Checked = True
                    Else
                        lcb2sat.Value = "0"
                        cb2sat.Checked = False
                    End If
                    If dr.Item("cb2sun").ToString = "1" Then
                        lcb2sun.Value = "1"
                        cb2sun.Checked = True
                    Else
                        lcb2sun.Value = "0"
                        cb2sun.Checked = False
                    End If
                End If
                'End If
                If dr.Item("cb3o").ToString = "1" Then
                    lcb3o.Value = "1"
                    cb3o.Checked = True
                Else
                    lcb3o.Value = "0"
                    cb3o.Checked = False
                End If
                If dr.Item("cb3").ToString = "1" Then
                    cb3.Checked = True
                    cb3mon.Checked = True
                    cb3tue.Checked = True
                    cb3wed.Checked = True
                    cb3thu.Checked = True
                    cb3fri.Checked = True
                    cb3sat.Checked = True
                    cb3sun.Checked = True

                    lcb3.Value = "1"
                    lcb3mon.Value = "1"
                    lcb3tue.Value = "1"
                    lcb3wed.Value = "1"
                    lcb3thu.Value = "1"
                    lcb3fri.Value = "1"
                    lcb3sat.Value = "1"
                    lcb3sun.Value = "1"
                Else
                    lcb3.Value = "0"
                    cb3.Checked = False
                    If dr.Item("cb3mon").ToString = "1" Then
                        lcb3mon.Value = "1"
                        cb3mon.Checked = True
                    Else
                        lcb3mon.Value = "0"
                        cb3mon.Checked = False
                    End If
                    If dr.Item("cb3tue").ToString = "1" Then
                        lcb3tue.Value = "1"
                        cb3tue.Checked = True
                    Else
                        lcb3tue.Value = "0"
                        cb3tue.Checked = False
                    End If
                    If dr.Item("cb3wed").ToString = "1" Then
                        lcb3wed.Value = "1"
                        cb3wed.Checked = True
                    Else
                        lcb3wed.Value = "0"
                        cb3wed.Checked = False
                    End If
                    If dr.Item("cb3thu").ToString = "1" Then
                        lcb3thu.Value = "1"
                        cb3thu.Checked = True
                    Else
                        lcb3thu.Value = "0"
                        cb3thu.Checked = False
                    End If
                    If dr.Item("cb3fri").ToString = "1" Then
                        lcb3fri.Value = "1"
                        cb3fri.Checked = True
                    Else
                        lcb3fri.Value = "0"
                        cb3fri.Checked = False
                    End If
                    If dr.Item("cb3sat").ToString = "1" Then
                        lcb3sat.Value = "1"
                        cb3sat.Checked = True
                    Else
                        lcb3sat.Value = "0"
                        cb3sat.Checked = False
                    End If
                    If dr.Item("cb3sun").ToString = "1" Then
                        lcb3sun.Value = "1"
                        cb3sun.Checked = True
                    Else
                        lcb3sun.Value = "0"
                        cb3sun.Checked = False
                    End If
                End If
                'End If
                'Orig
                If dr.Item("ocb1o").ToString = "1" Then
                    locb1o.Value = "1"
                    ocb1o.Checked = True
                Else
                    locb1o.Value = "0"
                    ocb1o.Checked = False
                End If
                If dr.Item("ocb1").ToString = "1" Then
                    ocb1.Checked = True
                    ocb1mon.Checked = True
                    ocb1tue.Checked = True
                    ocb1wed.Checked = True
                    ocb1thu.Checked = True
                    ocb1fri.Checked = True
                    ocb1sat.Checked = True
                    ocb1sun.Checked = True

                    locb1.Value = "1"
                    locb1mon.Value = "1"
                    locb1tue.Value = "1"
                    locb1wed.Value = "1"
                    locb1thu.Value = "1"
                    locb1fri.Value = "1"
                    locb1sat.Value = "1"
                    locb1sun.Value = "1"
                Else
                    locb1.Value = "0"
                    ocb1.Checked = False
                    If dr.Item("ocb1mon").ToString = "1" Then
                        locb1mon.Value = "1"
                        ocb1mon.Checked = True
                    Else
                        locb1mon.Value = "0"
                        ocb1mon.Checked = False
                    End If
                    If dr.Item("ocb1tue").ToString = "1" Then
                        locb1tue.Value = "1"
                        ocb1tue.Checked = True
                    Else
                        locb1tue.Value = "0"
                        ocb1tue.Checked = False
                    End If
                    If dr.Item("ocb1wed").ToString = "1" Then
                        locb1wed.Value = "1"
                        ocb1wed.Checked = True
                    Else
                        locb1wed.Value = "0"
                        ocb1wed.Checked = False
                    End If
                    If dr.Item("ocb1thu").ToString = "1" Then
                        locb1thu.Value = "1"
                        ocb1thu.Checked = True
                    Else
                        locb1thu.Value = "0"
                        ocb1thu.Checked = False
                    End If
                    If dr.Item("ocb1fri").ToString = "1" Then
                        locb1fri.Value = "1"
                        ocb1fri.Checked = True
                    Else
                        locb1fri.Value = "0"
                        ocb1fri.Checked = False
                    End If
                    If dr.Item("ocb1sat").ToString = "1" Then
                        locb1sat.Value = "1"
                        ocb1sat.Checked = True
                    Else
                        locb1sat.Value = "0"
                        ocb1sat.Checked = False
                    End If
                    If dr.Item("ocb1sun").ToString = "1" Then
                        locb1sun.Value = "1"
                        ocb1sun.Checked = True
                    Else
                        locb1sun.Value = "0"
                        ocb1sun.Checked = False
                    End If
                End If
                'End If

                If dr.Item("ocb2o").ToString = "1" Then
                    locb2o.Value = "1"
                    ocb2o.Checked = True
                Else
                    locb2o.Value = "0"
                    ocb2o.Checked = False
                End If
                If dr.Item("ocb2").ToString = "1" Then
                    ocb2.Checked = True
                    ocb2mon.Checked = True
                    ocb2tue.Checked = True
                    ocb2wed.Checked = True
                    ocb2thu.Checked = True
                    ocb2fri.Checked = True
                    ocb2sat.Checked = True
                    ocb2sun.Checked = True

                    locb2.Value = "1"
                    locb2mon.Value = "1"
                    locb2tue.Value = "1"
                    locb2wed.Value = "1"
                    locb2thu.Value = "1"
                    locb2fri.Value = "1"
                    locb2sat.Value = "1"
                    locb2sun.Value = "1"
                Else
                    locb2.Value = "0"
                    ocb2.Checked = False
                    If dr.Item("ocb2mon").ToString = "1" Then
                        locb2mon.Value = "1"
                        ocb2mon.Checked = True
                    Else
                        locb2mon.Value = "0"
                        ocb2mon.Checked = False
                    End If
                    If dr.Item("ocb2tue").ToString = "1" Then
                        locb2tue.Value = "1"
                        ocb2tue.Checked = True
                    Else
                        locb2tue.Value = "0"
                        ocb2tue.Checked = False
                    End If
                    If dr.Item("ocb2wed").ToString = "1" Then
                        locb2wed.Value = "1"
                        ocb2wed.Checked = True
                    Else
                        locb2wed.Value = "0"
                        ocb2wed.Checked = False
                    End If
                    If dr.Item("ocb2thu").ToString = "1" Then
                        locb2thu.Value = "1"
                        ocb2thu.Checked = True
                    Else
                        locb2thu.Value = "0"
                        ocb2thu.Checked = False
                    End If
                    If dr.Item("ocb2fri").ToString = "1" Then
                        locb2fri.Value = "1"
                        ocb2fri.Checked = True
                    Else
                        locb2fri.Value = "0"
                        ocb2fri.Checked = False
                    End If
                    If dr.Item("ocb2sat").ToString = "1" Then
                        locb2sat.Value = "1"
                        ocb2sat.Checked = True
                    Else
                        locb2sat.Value = "0"
                        ocb2sat.Checked = False
                    End If
                    If dr.Item("ocb2sun").ToString = "1" Then
                        locb2sun.Value = "1"
                        ocb2sun.Checked = True
                    Else
                        locb2sun.Value = "0"
                        ocb2sun.Checked = False
                    End If
                End If
                'End If
                If dr.Item("ocb3o").ToString = "1" Then
                    locb3o.Value = "1"
                    ocb3o.Checked = True
                Else
                    locb3o.Value = "0"
                    ocb3o.Checked = False
                End If
                If dr.Item("ocb3").ToString = "1" Then
                    ocb3.Checked = True
                    ocb3mon.Checked = True
                    ocb3tue.Checked = True
                    ocb3wed.Checked = True
                    ocb3thu.Checked = True
                    ocb3fri.Checked = True
                    ocb3sat.Checked = True
                    ocb3sun.Checked = True

                    locb3.Value = "1"
                    locb3mon.Value = "1"
                    locb3tue.Value = "1"
                    locb3wed.Value = "1"
                    locb3thu.Value = "1"
                    locb3fri.Value = "1"
                    locb3sat.Value = "1"
                    locb3sun.Value = "1"
                Else
                    locb3.Value = "0"
                    ocb3.Checked = False
                    If dr.Item("ocb3mon").ToString = "1" Then
                        locb3mon.Value = "1"
                        ocb3mon.Checked = True
                    Else
                        locb3mon.Value = "0"
                        ocb3mon.Checked = False
                    End If
                    If dr.Item("ocb3tue").ToString = "1" Then
                        locb3tue.Value = "1"
                        ocb3tue.Checked = True
                    Else
                        locb3tue.Value = "0"
                        ocb3tue.Checked = False
                    End If
                    If dr.Item("ocb3wed").ToString = "1" Then
                        locb3wed.Value = "1"
                        ocb3wed.Checked = True
                    Else
                        locb3wed.Value = "0"
                        ocb3wed.Checked = False
                    End If
                    If dr.Item("ocb3thu").ToString = "1" Then
                        locb3thu.Value = "1"
                        ocb3thu.Checked = True
                    Else
                        locb3thu.Value = "0"
                        ocb3thu.Checked = False
                    End If
                    If dr.Item("ocb3fri").ToString = "1" Then
                        locb3fri.Value = "1"
                        ocb3fri.Checked = True
                    Else
                        locb3fri.Value = "0"
                        ocb3fri.Checked = False
                    End If
                    If dr.Item("ocb3sat").ToString = "1" Then
                        locb3sat.Value = "1"
                        ocb3sat.Checked = True
                    Else
                        locb3sat.Value = "0"
                        ocb3sat.Checked = False
                    End If
                    If dr.Item("ocb3sun").ToString = "1" Then
                        locb3sun.Value = "1"
                        ocb3sun.Checked = True
                    Else
                        locb3sun.Value = "0"
                        ocb3sun.Checked = False
                    End If
                End If
                'End If
                cr = dr.Item("created").ToString
                rv = dr.Item("revised").ToString
                lblhaspm.Value = dr.Item("haspm").ToString
            End While
            dr.Close()
            'RBAS
            lblrteid.Value = rteid
            If rteid <> "0" And rteid <> "" Then
                sql = "select skillid, qty, freq, rdid from pmroutes where rid = '" & rteid & "'"
                Try
                    dr = tasks.GetRdrData(sql)
                    While dr.Read
                        rbskillid = dr.Item("skillid").ToString
                        rbqty = dr.Item("qty").ToString
                        rbfreq = dr.Item("freq").ToString
                        rbrdid = dr.Item("rdid").ToString
                    End While
                    dr.Close()
                Catch ex As Exception
                    dr = tasksadd.GetRdrData(sql)
                    While dr.Read
                        rbskillid = dr.Item("skillid").ToString
                        rbqty = dr.Item("qty").ToString
                        rbfreq = dr.Item("freq").ToString
                        rbrdid = dr.Item("rdid").ToString
                    End While
                    dr.Close()
                End Try

                lblrbskillid.Value = rbskillid
                lblrbqty.Value = rbqty
                lblrbfreq.Value = rbfreq
                lblrbrdid.Value = rbrdid
            End If

            Dim cind As String = lblcind.Value
            If co <> "0" Then
                lblco.Value = co
                Try
                    ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                PopCompFailList(co)
                PopFailList(co)
                PopTaskFailModes(co)
                PopoTaskFailModes(co)
                'UpdateFailStats(co) ? This procedure fills no field?
                lblco.Value = co
                chk = "comp"

            Else
                Try
                    ddcomp.SelectedValue = co
                Catch ex As Exception

                End Try
                lbfaillist.Items.Clear()
                lbfailmodes.Items.Clear()
                lbofailmodes.Items.Clear()
                lbCompFM.Items.Clear()
                lblco.Value = co
                chk = "comp"
            End If
            lblpar.Value = "comp"
            'UpDateRevs(ttid)
            'fuid = lblfuid.Value
            'If lblsb.Value = "0" Then
            'lblpg.Text = PageNumber
            'If Len(Filter) = 0 Then
            'Filter = " subtask = 0"
            'Else
            '    Filter = "funcid = " & fuid & " and subtask = 0"
            'End If
            'sql = "select count(*) from pmtasks where " & Filter
            'Try
            'tskcnt = tasks.Scalar(sql)
            'Catch ex As Exception
            'tskcnt = tasksadd.Scalar(sql)
            'End Try
            'Else
            'lblpg.Text = lblpgholder.Value
            'PageNumber = lblpgholder.Value
            'If Len(Filter) = 0 Then
            'Filter = " subtask = 0"
            'Else
            '   Filter = "funcid = " & fuid & " and subtask = 0"
            'End If
            'sql = "select count(*) from pmtasks where " & Filter
            'Try
            'tskcnt = tasks.Scalar(sql)
            'Catch ex As Exception
            'tskcnt = tasksadd.Scalar(sql)
            'End Try
            'End If
            'lblcnt.Text = tskcnt
            'CheckPgNav(tskcnt, PageNumber)
            btnaddsubtask.Enabled = True
        End If
        'tasks.Dispose()
    End Sub
    Private Sub PopFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
         + "select failid from pmtaskfailmodes where comid = '" & comp & "')" ' and taskid = '" & ttid & "')"
        sql = "usp_getcfall_tskna_tpm '" & comp & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        lbfaillist.DataBind()
        dr.Close()
    End Sub

    Private Sub PopCompFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        'sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
        sql = "select * from pmtaskfailmodes where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsk_tpm '" & comp & "','" & ttid & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub PopoTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        sql = "select * from pmotaskfailmodes where taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsko_tpm '" & ttid & "','" & lang & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try


        lbofailmodes.DataSource = dr
        lbofailmodes.DataTextField = "failuremode"
        lbofailmodes.DataValueField = "failid"
        lbofailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub UpdateFailStats(ByVal comp As String)
        Dim fmcnt, fmcnta As Integer
        sql = "select count(*) from ComponentFailModes where comid = '" & comp & "'"
        Try
            fmcnt = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnt = tasksadd.Scalar(sql)
        End Try

        sql = "select count(distinct failid) from pmTaskFailModestpm where comid = '" & comp & "'"
        Try
            fmcnta = tasks.Scalar(sql)
        Catch ex As Exception
            fmcnta = tasksadd.Scalar(sql)
        End Try

        fmcnta = fmcnt - fmcnta
    End Sub
    Private Sub UpDateRevs(ByVal Filter As String)
        sql = "select created, revised from pmtaskstpm where pmtskid = '" & Filter & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        Dim cr, rv As String
        While dr.Read
            cr = dr.Item("created").ToString
            rv = dr.Item("revised").ToString
        End While
        dr.Close()
        If Len(rv) = 0 Then
            rv = "NA"
        End If
        'imgClock.Attributes.Add("onmouseover", "return overlib('Task Created: " & cr & "<br> Task Revised: " & rv & "')")
        'imgClock.Attributes.Add("onmouseout", "return nd()")
    End Sub

    Private Sub ibReuse_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibReuse.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0

        ttid = lbltaskid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            'SaveTask()
            'lblenable.Value = "0"
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try

        tasks.Dispose()

    End Sub
    Private Sub GetItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "' and  (oaid is null or oaid = '0')"
        End If
        'sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "'"
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr446", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr447", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "usp_addTaskFailureModetpm " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'dev','" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim fm As String
                Dim ipar As Integer = 0
                For Each Item In lbfailmodes.Items
                    f = Item.Text.ToString
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                    If Len(fm) = 0 Then
                        fm = f & "(___)"
                    Else
                        fm += " " & f & "(___)"
                    End If
                Next
                fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
                'sql = "update pmtaskstpm set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
                'tasks.Update(sql)

            Catch ex As Exception

            End Try
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        End If
    End Sub
    Private Sub RemItems(ByVal f As String, ByVal fi As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "usp_delTaskFailureModetpm '" & ttid & "', '" & fi & "','" & oaid & "'"
            tasks.Update(sql)
            Dim fm As String
            Dim ipar As Integer = 0
            Dim Item As ListItem
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
            sql = "update pmtaskstpm set fm1 = '" & fm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetoItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Dim fcnt, fcnt2 As Integer
        If oaid <> "0" Then
            sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid = '" & oaid & "'"
        Else
            sql = "select count(*) from pmTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "' and oaid is null"
        End If
        'sql = "select count(*) from pmoTaskFailModestpm where taskid = '" & ttid & "' and failid = '" & fi & "'"
        fcnt = tasks.Scalar(sql)
        sql = "select count(*) from pmoTaskFailModestpm where taskid = '" & ttid & "'"
        fcnt2 = tasks.Scalar(sql)
        If fcnt2 >= 5 Then
            Dim strMessage As String = tmod.getmsg("cdstr448", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf fcnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr449", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Try
                sql = "usp_addTaskFailureModetpm " & ttid & ", " & fi & ", '" & f & "', '" & comp & "', 'opt', '" & oaid & "'"
                tasks.Update(sql)
                Dim Item As ListItem
                Dim ofm, ofs, ofi As String
                For Each Item In lbofailmodes.Items
                    ofs = Item.ToString
                    If Len(ofm) = 0 Then
                        ofm = ofs & "(___)"
                    Else
                        ofm += " " & ofs & "(___)"
                    End If
                Next
                ofm = Replace(ofm, "'", Chr(180), , , vbTextCompare)
                sql = "update pmtaskstpm set ofm1 = '" & ofm & "' where pmtskid = '" & ttid & "'"
                tasks.Update(sql)
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub RemoItems(ByVal f As String, ByVal fi As String, ByVal comp As String, ByVal oaid As String)
        ttid = lbltaskid.Value
        Try
            sql = "usp_deloTaskFailureModetpm " & ttid & ", " & fi & ", '" & f & "','" & oaid & "'"
            tasks.Update(sql)
            Dim Item As ListItem
            Dim ofm, ofs, ofi As String
            For Each Item In lbofailmodes.Items
                ofs = Item.ToString
                If Len(ofm) = 0 Then
                    ofm = ofs & "(___)"
                Else
                    ofm += " " & ofs & "(___)"
                End If
            Next
            ofm = Replace(ofm, "'", Chr(180), , , vbTextCompare)
            sql = "update pmtaskstpm set ofm1 = '" & ofm & "' where pmtskid = '" & ttid & "'"
            tasks.Update(sql)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ibfromo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibfromo.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In lbofailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemoItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopoTaskFailModes(comp)
            UpdateFailStats(comp)
            lbltaskid.Value = ttid
            'SaveTask()
            'lblenable.Value = "0"
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub SaveTaskComp()
        Dim comp As String = ddcomp.SelectedValue.ToString
        Dim compd As String = ddcomp.SelectedItem.ToString
        If ddcomp.SelectedIndex <> 0 Then
            lblco.Value = comp
            chk = comp
            ttid = lbltaskid.Value
            fuid = lblfuid.Value
            Try
                tasks.Open()
                sql = "usp_inserttpmComp '" & comp & "', '" & compd & "', '" & ttid & "', '" & fuid & "'"
                tasks.Update(sql)
                PopCompFailList(comp)
                PopFailList(comp)
                PopTaskFailModes(comp)
                UpdateFailStats(comp)
                eqid = lbleqid.Value
                tasks.UpMod(eqid)
            Catch ex As Exception

            End Try
            Try
                tasks.Dispose()
            Catch ex As Exception

            End Try
        Else
            Try
                ddcomp.SelectedValue = lblco.Value
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub ddcomp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcomp.SelectedIndexChanged
        If ddcomp.SelectedIndex <> 0 Then
            Dim comp As String = ddcomp.SelectedValue.ToString
            Dim compd As String = ddcomp.SelectedItem.ToString
            lblco.Value = comp
            chk = comp
            ttid = lbltaskid.Value
            fuid = lblfuid.Value
            Try
                tasks.Open()
                sql = "usp_inserttpmComp '" & comp & "', '" & compd & "', '" & ttid & "', '" & fuid & "'"
                tasks.Update(sql)
                PopCompFailList(comp)
                PopFailList(comp)
                PopTaskFailModes(comp)
                PopoTaskFailModes(comp)
                UpdateFailStats(comp)
                lbltaskid.Value = ttid
                PageNumber = lblpg.Text
                Filter = lblfilt.Value
                eqid = lbleqid.Value
                tasks.UpMod(eqid)
                LoadPage(PageNumber, Filter)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr450", "tpmopttasks.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
            Try
                tasks.Dispose()
            Catch ex As Exception

            End Try
        Else
            Try
                ddcomp.SelectedValue = lblco.Value
            Catch ex As Exception

            End Try
        End If

    End Sub
    Private Sub ibtoo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtoo.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()
        lblenable.Value = "0"
        For Each Item In lbCompFM.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetoItems(f, fi, comp, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            PopoTaskFailModes(comp)
            UpdateFailStats(comp)
            'LoadPage(PageNumber, Filter) 'test

            'SaveTask()
            'lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub
    Private Sub ToDisClick()

    End Sub
    Private Sub ibToTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibToTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()
        Dim tst As String
        For Each Item In lbfaillist.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                GetItems(f, fi, comp, oa)
            End If
        Next

        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            'LoadPage(PageNumber, Filter) 'test
            'SaveTask()
            'lblenable.Value = "0"
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub UpdateFM()
        Dim ttid As String = lbltaskid.Value
        sql = "usp_UpdateFM1tpm '" & ttid & "'"
        tasks.Update(sql)
    End Sub

    Private Sub ibFromTask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibFromTask.Click
        Dim Item As ListItem
        Dim f, fi, oa As String
        Dim ipar As Integer = 0
        ttid = lbltaskid.Value
        Dim comp As String = ddcomp.SelectedValue.ToString
        tasks.Open()

        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                Dim fiarr() As String = fi.Split("-")
                fi = fiarr(0)
                oa = fiarr(1)
                If oa <> "0" Then
                    ipar = f.LastIndexOf("(")
                    If ipar <> -1 Then
                        f = Mid(f, 1, ipar)
                    End If
                End If
                RemItems(f, fi, oa)
            End If
        Next
        Try
            PopFailList(comp)
            PopTaskFailModes(comp)
            UpdateFailStats(comp)
            UpdateFM()
            'LoadPage(PageNumber, Filter) 'test
            'SaveTask()
            'lblenable.Value = "0"
            lbltaskid.Value = ttid
            PageNumber = lblpg.Text
            'PopComp()
            Filter = lblfilt.Value

            'LoadPage(PageNumber, Filter)
        Catch ex As Exception
        End Try
        tasks.Dispose()
    End Sub

    Private Sub btnaddtsk_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddtsk.Click
        If Len(lblfuid.Value) <> 0 Then
            lblenable.Value = "0"
            tl = lbltasklev.Value
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eqid = lbleqid.Value
            fuid = lblfuid.Value

            tl = 5
            tasksadd.Open()
            Dim usr As String = HttpContext.Current.Session("username").ToString()
            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            sql = "usp_AddTaskTPM '" & tl & "', '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & fuid & "', '" & ustr & "'"
            tasksadd.Scalar(sql)
            Filter = lblfilt.Value

            PageNumber = lblcnt.Text
            PageNumber = PageNumber + 1
            eqid = lbleqid.Value
            LoadPage(PageNumber, Filter)
            tasksadd.Dispose()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr451", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub

    Private Sub btndeltask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Len(lblfuid.Value) <> 0 Then
            PageNumber = lblpg.Text
            Filter = lblfilt.Value
            fuid = lblfuid.Value
            tnum = lblt.Value
            sql = "usp_delTPMTask '" & fuid & "', '" & tnum & "'"
            tasks.Open()
            tasks.Update(sql)
            PageNumber = PageNumber - 1
            eqid = lbleqid.Value
            tasks.UpMod(eqid)
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr452", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub

    Private Sub ibCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Len(lblfuid.Value) <> 0 Then
            tasks.Open()
            GetLists()
            Filter = lblfilt.Value
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr453", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub

    Private Sub btnsavetask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsavetask.Click
        tasks.Open()
        SaveTask2()
        tasks.Dispose()
    End Sub
    Private Sub SaveTask2()
        Dim freor, frestror, fre, frestr, cust, ocust As String
        cust = lblcbcust.Value
        ocust = lblocbcust.Value
        If cust <> "1" Then
           
            Try
                fre = "" 'txtfreq.Text
                frestr = txtfreq.Text
            Catch ex As Exception
                fre = "" 'txtfreq.Text
                frestr = lblfreq.Value
            End Try
            If frestr = "" Then
                frestr = lblfreq.Value
            End If
        Else
            fre = "0"
            frestr = ""
        End If

        If ocust <> "1" Then
            
            Try
                freor = "" 'txtfreqo.Text
                frestror = txtfreqo.Text
            Catch ex As Exception
                freor = "" 'txtfreqo.Text
                frestror = lblfreqo.Value
            End Try
            If frestror = "" Then
                frestror = lblfreqo.Value
            End If
        Else
        freor = "0"
        frestror = ""
        End If


        Dim icb1o, icb1, icb1mon, icb1tue, icb1wed, icb1thu, icb1fri, icb1sat, icb1sun As Integer
        icb1o = 0
        icb1 = 0
        icb1mon = 0
        icb1tue = 0
        icb1wed = 0
        icb1thu = 0
        icb1fri = 0
        icb1sat = 0
        icb1sun = 0
        Dim firststr, shiftonly1 As String
        Dim icb2o, icb2, icb2mon, icb2tue, icb2wed, icb2thu, icb2fri, icb2sat, icb2sun As Integer
        Dim secondstr, shiftonly2 As String
        icb2o = 0
        icb2 = 0
        icb2mon = 0
        icb2tue = 0
        icb2wed = 0
        icb2thu = 0
        icb2fri = 0
        icb2sat = 0
        icb2sun = 0
        Dim icb3o, icb3, icb3mon, icb3tue, icb3wed, icb3thu, icb3fri, icb3sat, icb3sun As Integer
        Dim thirdstr, shiftonly3 As String
        icb3o = 0
        icb3 = 0
        icb3mon = 0
        icb3tue = 0
        icb3wed = 0
        icb3thu = 0
        icb3fri = 0
        icb3sat = 0
        icb3sun = 0
        Dim c1cnt, c2cnt, c3cnt As Integer
        c1cnt = 0
        c2cnt = 0
        c3cnt = 0
        If fre <> "0" Or cust = "1" Then
            If lcb1o.Value = "1" Then
                icb1o = 1
                'icb1 = 0
                'icb1mon = 0
                'icb1tue = 0
                'icb1wed = 0
                'icb1thu = 0
                'icb1fri = 0
                'icb1sat = 0
                'icb1sun = 0
                shiftonly1 = "1st shift"
            Else
                icb1o = 0
            End If
            If lcb1.Value = "1" Then
                icb1 = 1
                icb1mon = 1
                icb1tue = 1
                icb1wed = 1
                icb1thu = 1
                icb1fri = 1
                icb1sat = 1
                icb1sun = 1
                'firststr = "M,Tu,W,Th,F,Sa,Su"
                c1cnt = 7
            Else
                icb1 = 0

                If lcb1mon.Value = "1" Then
                    icb1mon = 1
                    firststr = "M"
                    c1cnt += 1
                Else
                    icb1mon = 0
                End If
                If lcb1tue.Value = "1" Then
                    icb1tue = 1
                    If firststr = "" Then
                        firststr = "Tu"
                    Else
                        firststr += ",Tu"
                    End If
                    c1cnt += 1
                Else
                    icb1tue = 0
                End If
                If lcb1wed.Value = "1" Then
                    icb1wed = 1
                    If firststr = "" Then
                        firststr = "W"
                    Else
                        firststr += ",W"
                    End If
                    c1cnt += 1
                Else
                    icb1wed = 0
                End If
                If lcb1thu.Value = "1" Then
                    icb1thu = 1
                    If firststr = "" Then
                        firststr = "Th"
                    Else
                        firststr += ",Th"
                    End If
                    c1cnt += 1
                Else
                    icb1thu = 0
                End If
                If lcb1fri.Value = "1" Then
                    icb1fri = 1
                    If firststr = "" Then
                        firststr = "F"
                    Else
                        firststr += ",F"
                    End If
                    c1cnt += 1
                Else
                    icb1fri = 0
                End If
                If lcb1sat.Value = "1" Then
                    icb1sat = 1
                    If firststr = "" Then
                        firststr = "Sa"
                    Else
                        firststr += ",Sa"
                    End If
                    c1cnt += 1
                Else
                    icb1sat = 0
                End If
                If lcb1sun.Value = "1" Then
                    icb1sun = 1
                    If firststr = "" Then
                        firststr = "Su"
                    Else
                        firststr += ",Su"
                    End If
                    c1cnt += 1
                Else
                    icb1sun = 0
                End If
            End If
            'End If

            If lcb2o.Value = "1" Then
                icb2o = 1
                'icb2 = 0
                'icb2mon = 0
                'icb2tue = 0
                'icb2wed = 0
                'icb2thu = 0
                'icb2fri = 0
                'icb2sat = 0
                'icb2sun = 0
                shiftonly2 = "2nd shift"
            Else
                icb2o = 0
            End If
            If lcb2.Value = "1" Then
                icb2 = 1
                icb2mon = 1
                icb2tue = 1
                icb2wed = 1
                icb2thu = 1
                icb2fri = 1
                icb2sat = 1
                icb2sun = 1
                'secondstr = "M,Tu,W,Th,F,Sa,Su"
                c2cnt = 7
            Else
                icb2 = 0
                If lcb2mon.Value = "1" Then
                    icb2mon = 1

                    secondstr = "M"

                    c2cnt += 1
                Else
                    icb2mon = 0
                End If
                If lcb2tue.Value = "1" Then
                    icb2tue = 1
                    If secondstr = "" Then
                        secondstr = "Tu"
                    Else
                        secondstr += ",Tu"
                    End If
                    c2cnt += 1
                Else
                    icb2tue = 0
                End If
                If lcb2wed.Value = "1" Then
                    icb2wed = 1
                    If secondstr = "" Then
                        secondstr = "W"
                    Else
                        secondstr += ",W"
                    End If
                    c2cnt += 1
                Else
                    icb2wed = 0
                End If
                If lcb2thu.Value = "1" Then
                    icb2thu = 1
                    If secondstr = "" Then
                        secondstr = "Th"
                    Else
                        secondstr += ",Th"
                    End If
                    c2cnt += 1
                Else
                    icb2thu = 0
                End If
                If lcb2fri.Value = "1" Then
                    icb2fri = 1
                    If secondstr = "" Then
                        secondstr = "F"
                    Else
                        secondstr += ",F"
                    End If
                    c2cnt += 1
                Else
                    icb2fri = 0
                End If
                If lcb2sat.Value = "1" Then
                    icb2sat = 1
                    If secondstr = "" Then
                        secondstr = "Sa"
                    Else
                        secondstr += ",Sa"
                    End If
                    c2cnt += 1
                Else
                    icb2sat = 0
                End If
                If lcb2sun.Value = "1" Then
                    icb2sun = 1
                    If secondstr = "" Then
                        secondstr = "Su"
                    Else
                        secondstr += ",Su"
                    End If
                    c2cnt += 1
                Else
                    icb2sun = 0
                End If
            End If
            'End If

            If lcb3o.Value = "1" Then
                icb3o = 1
                'icb3 = 0
                'icb3mon = 0
                'icb3tue = 0
                'icb3wed = 0
                'icb3thu = 0
                'icb3fri = 0
                'icb3sat = 0
                'icb3sun = 0
                shiftonly3 = "3rd shift"
            Else
                icb3o = 0
            End If
            If lcb3.Value = "1" Then
                icb3 = 1
                icb3mon = 1
                icb3tue = 1
                icb3wed = 1
                icb3thu = 1
                icb3fri = 1
                icb3sat = 1
                icb3sun = 1
                'thirdstr = "M,Tu,W,Th,F,Sa,Su"
                c3cnt = 7
            Else
                icb3 = 0
                If lcb3mon.Value = "1" Then
                    icb3mon = 1

                    thirdstr = "M"

                    c3cnt += 1
                Else
                    icb3mon = 0
                End If
                If lcb3tue.Value = "1" Then
                    icb3tue = 1
                    If thirdstr = "" Then
                        thirdstr = "T"
                    Else
                        thirdstr += ",T"
                    End If
                    c3cnt += 1
                Else
                    icb3tue = 0
                End If
                If lcb3wed.Value = "1" Then
                    icb3wed = 1
                    If thirdstr = "" Then
                        thirdstr = "W"
                    Else
                        thirdstr += ",W"
                    End If
                    c3cnt += 1
                Else
                    icb3wed = 0
                End If
                If lcb3thu.Value = "1" Then
                    icb3thu = 1
                    If thirdstr = "" Then
                        thirdstr = "Th"
                    Else
                        thirdstr += ",Th"
                    End If
                    c3cnt += 1
                Else
                    icb3thu = 0
                End If
                If lcb3fri.Value = "1" Then
                    icb3fri = 1
                    If thirdstr = "" Then
                        thirdstr = "F"
                    Else
                        thirdstr += ",F"
                    End If
                    c3cnt += 1
                Else
                    icb3fri = 0
                End If
                If lcb3sat.Value = "1" Then
                    icb3sat = 1
                    If thirdstr = "" Then
                        thirdstr = "Sa"
                    Else
                        thirdstr += ",Sa"
                    End If
                    c3cnt += 1
                Else
                    icb3sat = 0
                End If
                If lcb3sun.Value = "1" Then
                    icb3sun = 1
                    If thirdstr = "" Then
                        thirdstr = "Su"
                    Else
                        thirdstr += ",Su"
                    End If
                    c3cnt += 1
                Else
                    icb3sun = 0
                End If
            End If
            'End If


        End If
        Dim ms As Integer = 0
        If firststr <> "" Then
            ms += 1
        End If
        If secondstr <> "" Then
            ms += 1
        End If
        If thirdstr <> "" Then
            ms += 1
        End If
        If ms < 2 Then
            ms = 0
            If shiftonly1 <> "" Then
                ms += 1
            End If
            If shiftonly2 <> "" Then
                ms += 1
            End If
            If shiftonly3 <> "" Then
                ms += 1
            End If
            If ms > 1 Then
                ms = 1
            Else
                ms = 0
            End If
        Else
            ms = 1
        End If

        Dim oicb1o, oicb1, oicb1mon, oicb1tue, oicb1wed, oicb1thu, oicb1fri, oicb1sat, oicb1sun As Integer
        oicb1o = 0
        oicb1 = 0
        oicb1mon = 0
        oicb1tue = 0
        oicb1wed = 0
        oicb1thu = 0
        oicb1fri = 0
        oicb1sat = 0
        oicb1sun = 0
        Dim oicb2o, oicb2, oicb2mon, oicb2tue, oicb2wed, oicb2thu, oicb2fri, oicb2sat, oicb2sun As Integer
        oicb2o = 0
        oicb2 = 0
        oicb2mon = 0
        oicb2tue = 0
        oicb2wed = 0
        oicb2thu = 0
        oicb2fri = 0
        oicb2sat = 0
        oicb2sun = 0
        Dim oicb3o, oicb3, oicb3mon, oicb3tue, oicb3wed, oicb3thu, oicb3fri, oicb3sat, oicb3sun As Integer
        oicb3o = 0
        oicb3 = 0
        oicb3mon = 0
        oicb3tue = 0
        oicb3wed = 0
        oicb3thu = 0
        oicb3fri = 0
        oicb3sat = 0
        oicb3sun = 0

        Dim oc1cnt, oc2cnt, oc3cnt As Integer
        oc1cnt = 0
        oc2cnt = 0
        oc3cnt = 0
        Dim ofirststr, oshiftonly1 As String
        Dim osecondstr, oshiftonly2 As String
        Dim othirdstr, oshiftonly3 As String
        If freor <> "0" Or ocust = "1" Then
            If locb1o.Value = "1" Then
                oicb1o = 1
                'oicb1 = 0
                'oicb1mon = 0
                'oicb1tue = 0
                'oicb1wed = 0
                'oicb1thu = 0
                'oicb1fri = 0
                'oicb1sat = 0
                'oicb1sun = 0
                oshiftonly1 = "1st shift"
            Else
                oicb1o = 0
            End If
            If lcb1.Value = "1" Then
                oicb1 = 1
                oicb1mon = 1
                oicb1tue = 1
                oicb1wed = 1
                oicb1thu = 1
                oicb1fri = 1
                oicb1sat = 1
                oicb1sun = 1
                oc1cnt = 7
            Else
                oicb1 = 0
                If locb1mon.Value = "1" Then
                    oicb1mon = 1
                    ofirststr = "M"
                    oc1cnt += 1
                Else
                    oicb1mon = 0
                End If
                If locb1tue.Value = "1" Then
                    oicb1tue = 1
                    If ofirststr = "" Then
                        ofirststr = "Tu"
                    Else
                        ofirststr += ",Tu"
                    End If
                    oc1cnt += 1
                Else
                    oicb1tue = 0
                End If
                If locb1wed.Value = "1" Then
                    oicb1wed = 1
                    If ofirststr = "" Then
                        ofirststr = "W"
                    Else
                        ofirststr += ",W"
                    End If
                    oc1cnt += 1
                Else
                    oicb1wed = 0
                End If
                If locb1thu.Value = "1" Then
                    oicb1thu = 1
                    If ofirststr = "" Then
                        ofirststr = "Th"
                    Else
                        ofirststr += ",Th"
                    End If
                    oc1cnt += 1
                Else
                    oicb1thu = 0
                End If
                If locb1fri.Value = "1" Then
                    oicb1fri = 1
                    If ofirststr = "" Then
                        ofirststr = "F"
                    Else
                        ofirststr += ",F"
                    End If
                    oc1cnt += 1
                Else
                    oicb1fri = 0
                End If
                If locb1sat.Value = "1" Then
                    oicb1sat = 1
                    If ofirststr = "" Then
                        ofirststr = "Sa"
                    Else
                        ofirststr += ",Sa"
                    End If
                    oc1cnt += 1
                Else
                    oicb1sat = 0
                End If
                If locb1sun.Value = "1" Then
                    oicb1sun = 1
                    If ofirststr = "" Then
                        ofirststr = "Su"
                    Else
                        ofirststr += ",Su"
                    End If
                    oc1cnt += 1
                Else
                    oicb1sun = 0
                End If
            End If
            'End If

            If locb2o.Value = "1" Then
                oicb2o = 1
                'oicb2 = 0
                oicb2mon = 0
                'oicb2tue = 0
                'oicb2wed = 0
                'oicb2thu = 0
                'oicb2fri = 0
                'oicb2sat = 0
                'oicb2sun = 0
                oshiftonly2 = "2nd Shift"
            Else
                oicb2o = 0
            End If
            If locb2.Value = "1" Then
                oicb2 = 1
                oicb2mon = 1
                oicb2tue = 1
                oicb2wed = 1
                oicb2thu = 1
                oicb2fri = 1
                oicb2sat = 1
                oicb2sun = 1
                oc2cnt = 7
            Else
                oicb2 = 0
                If locb2mon.Value = "1" Then
                    oicb2mon = 1
                    osecondstr = "M"
                    oc2cnt += 1
                Else
                    oicb2mon = 0
                End If
                If locb2tue.Value = "1" Then
                    oicb2tue = 1
                    If osecondstr = "" Then
                        osecondstr = "Tu"
                    Else
                        osecondstr += ",Tu"
                    End If
                    oc2cnt += 1
                Else
                    oicb2tue = 0
                End If
                If locb2wed.Value = "1" Then
                    oicb2wed = 1
                    If osecondstr = "" Then
                        osecondstr = "W"
                    Else
                        osecondstr += ",W"
                    End If
                    oc2cnt += 1
                Else
                    oicb2wed = 0
                End If
                If locb2thu.Value = "1" Then
                    oicb2thu = 1
                    If osecondstr = "" Then
                        osecondstr = "Th"
                    Else
                        osecondstr += ",Th"
                    End If
                    oc2cnt += 1
                Else
                    oicb2thu = 0
                End If
                If locb2fri.Value = "1" Then
                    oicb2fri = 1
                    If osecondstr = "" Then
                        osecondstr = "F"
                    Else
                        osecondstr += ",F"
                    End If
                    oc2cnt += 1
                Else
                    oicb2fri = 0
                End If
                If locb2sat.Value = "1" Then
                    oicb2sat = 1
                    If osecondstr = "" Then
                        osecondstr = "Sa"
                    Else
                        osecondstr += ",Sa"
                    End If
                    oc2cnt += 1
                Else
                    oicb2sat = 0
                End If
                If locb2sun.Value = "1" Then
                    oicb2sun = 1
                    If osecondstr = "" Then
                        osecondstr = "Su"
                    Else
                        osecondstr += ",Su"
                    End If
                    oc2cnt += 1
                Else
                    oicb2sun = 0
                End If
            End If
            'End If

            If locb3o.Value = "1" Then
                oicb3o = 1
                'oicb3 = 0
                'oicb3mon = 0
                'oicb3tue = 0
                'oicb3wed = 0
                'oicb3thu = 0
                'oicb3fri = 0
                'oicb3sat = 0
                'oicb3sun = 0
                oshiftonly3 = "3rd Shift"
            Else
                oicb3o = 0
            End If
            If locb3.Value = "1" Then
                oicb3 = 1
                oicb3mon = 1
                oicb3tue = 1
                oicb3wed = 1
                oicb3thu = 1
                oicb3fri = 1
                oicb3sat = 1
                oicb3sun = 1
                oc3cnt = 7
            Else
                oicb3 = 0
                If locb3mon.Value = "1" Then
                    oicb3mon = 1
                    othirdstr = "M"
                    oc3cnt += 1
                Else
                    oicb3mon = 0
                End If
                If locb3tue.Value = "1" Then
                    oicb3tue = 1
                    If othirdstr = "" Then
                        othirdstr = "Tu"
                    Else
                        othirdstr += ",Tu"
                    End If
                    oc3cnt += 1
                Else
                    oicb3tue = 0
                End If
                If locb3wed.Value = "1" Then
                    oicb3wed = 1
                    If othirdstr = "" Then
                        othirdstr = "W"
                    Else
                        othirdstr += ",W"
                    End If
                    oc3cnt += 1
                Else
                    oicb3wed = 0
                End If
                If locb3thu.Value = "1" Then
                    oicb3thu = 1
                    If othirdstr = "" Then
                        othirdstr = "Th"
                    Else
                        othirdstr += ",Th"
                    End If
                    oc3cnt += 1
                Else
                    oicb3thu = 0
                End If
                If locb3fri.Value = "1" Then
                    oicb3fri = 1
                    If othirdstr = "" Then
                        othirdstr = "F"
                    Else
                        othirdstr += ",F"
                    End If
                    oc3cnt += 1
                Else
                    oicb3fri = 0
                End If
                If locb3sat.Value = "1" Then
                    oicb3sat = 1
                    If othirdstr = "" Then
                        othirdstr = "Sa"
                    Else
                        othirdstr += ",Sa"
                    End If
                    oc3cnt += 1
                Else
                    oicb3sat = 0
                End If
                If locb3sun.Value = "1" Then
                    oicb3sun = 1
                    If othirdstr = "" Then
                        othirdstr = "Su"
                    Else
                        othirdstr += ",Su"
                    End If
                    oc3cnt += 1
                Else
                    oicb3sun = 0
                End If

            End If
            'End If
        End If


        Dim t, st, tid, typ, des, odes, rmt, pt, ski, qty, tr, eqs, lot, cs, ci, cn, cin As String
        Dim typor, typstror, ptor, ptstror As String
        Dim skior, skistror, qtyor, tror, eqsor, eqsstror As String
        Dim typstr, rmtstr, ptstr, skistr, eqsstr, cqty, taskstat, taskstatid As String
        If Len(lblfuid.Value) <> 0 Then
            t = lblt.Value

            st = lblsb.Value
            If ddtaskstat.SelectedIndex <> 0 Then
                taskstatid = ddtaskstat.SelectedValue
                taskstat = ddtaskstat.SelectedItem.ToString
                taskstat = Replace(taskstat, "'", Chr(180), , , vbTextCompare)
            Else
                taskstatid = "0"
            End If

            If ddtype.SelectedIndex <> 0 Then
                typ = ddtype.SelectedValue
                typstr = ddtype.SelectedItem.ToString
                typstr = Replace(typstr, "'", Chr(180), , , vbTextCompare)
            Else
                typ = "0"
            End If
            If ddtypeo.SelectedIndex <> 0 Then
                typor = ddtypeo.SelectedValue 'orig +
                typstror = ddtypeo.SelectedItem.ToString
                typstror = Replace(typstror, "'", Chr(180), , , vbTextCompare)
            Else
                typor = "0"
            End If


            qty = txtqty.Text

            If Len(qty) = 0 Then
                qty = "1"
            End If
            qtyor = txtqtyo.Text 'orig 
            If Len(qtyor) = 0 Then
                qtyor = "1"
            End If
            tr = txttr.Text
            If Len(tr) = 0 Then
                tr = "0"
            End If
            tror = txttro.Text 'orig
            If Len(tror) = 0 Then
                tror = "0"
            End If
            If ddeqstat.SelectedIndex <> 0 Then
                eqs = ddeqstat.SelectedValue
                eqsstr = ddeqstat.SelectedItem.ToString
                eqsstr = Replace(eqsstr, "'", Chr(180), , , vbTextCompare)
            Else
                eqs = "0"
            End If
            If ddeqstato.SelectedIndex <> 0 Then
                eqsor = ddeqstato.SelectedValue 'orig +
                eqsstror = ddeqstato.SelectedItem.ToString
                eqsstror = Replace(eqsstror, "'", Chr(180), , , vbTextCompare)
            Else
                eqsor = "0"
            End If

            Dim ordt, rdt As String
            ordt = txtordt.Text
            rdt = txtrdt.Text
            If ordt = "" Then
                ordt = "0"
            End If
            If rdt = "" Then
                rdt = "0"
            End If

            des = txtdesc.Value
            des = Replace(des, "'", Chr(180), , , vbTextCompare)
            des = Replace(des, "--", "-", , , vbTextCompare)
            des = Replace(des, ";", ":", , , vbTextCompare)
            odes = txtodesc.Value 'orig
            odes = Replace(odes, "'", Chr(180), , , vbTextCompare)
            odes = Replace(odes, "--", "-", , , vbTextCompare)
            odes = Replace(odes, ";", ":", , , vbTextCompare)
            If cbloto.Checked = True Then
                lot = "1"
            Else
                lot = "0"
            End If
            If cbcs.Checked = True Then
                cs = "1"
            Else
                cs = "0"
            End If
            tid = lbltaskid.Value
            Filter = lblfilt.Value
            If ddcomp.SelectedIndex <> 0 Then
                ci = ddcomp.SelectedValue.ToString
            Else
                ci = "0"
            End If

            fuid = lblfuid.Value
            cqty = txtcQty.Text

            cn = ddcomp.SelectedItem.ToString
            cn = Replace(cn, "'", Chr(180), , , vbTextCompare)
            Dim fm As String
            Dim Item As ListItem
            Dim f, fi As String
            Dim ipar As Integer = 0
            For Each Item In lbfailmodes.Items
                f = Item.Text.ToString
                ipar = f.LastIndexOf("(")
                If ipar <> -1 Then
                    f = Mid(f, 1, ipar)
                End If
                If Len(fm) = 0 Then
                    fm = f & "(___)"
                Else
                    fm += " " & f & "(___)"
                End If
            Next
            fm = Replace(fm, "'", Chr(180), , , vbTextCompare)
            Dim ofm, ofs, ofi As String
            For Each Item In lbofailmodes.Items
                ofs = Item.ToString
                ipar = ofs.LastIndexOf("(")
                If ipar <> -1 Then
                    ofs = Mid(ofs, 1, ipar)
                End If

                If Len(ofm) = 0 Then
                    ofm = ofs & "(___)"
                Else
                    ofm += " " & ofs & "(___)"
                End If
            Next
            ofm = Replace(ofm, "'", Chr(180), , , vbTextCompare)
            Dim ttid As String = lbltaskid.Value
            Dim usr As String = HttpContext.Current.Session("username").ToString()
            Dim ustr As String = Replace(usr, "'", Chr(180), , , vbTextCompare)
            Dim pf As String = txtpfint.Text
            eqid = lbleqid.Value
            Dim hpm As String = lblhaspm.Value
            sid = lblsid.Value
            If Len(ttid) <> 0 Then
                sql = "exec usp_updatepmtaskstpmopt '" & ttid & "','" & des & "','" & qty & "','" & tr & "','" & eqs & "','" & eqsstr & ", " _
                + "'" & fre & "','" & frestr & "','" & lot & "','" & cs & "','" & typ & "','" & typstr & "','" & ci & "', " _
                + "'" & cn & "','" & fm & "','" & cqty & "','" & pf & "','" & rdt & "','" & Filter & "', " _
                + "'" & PageNumber & "','" & fuid & "', " _
                + "'" & icb1o & "','" & icb1 & "','" & icb1mon & "','" & icb1tue & "','" & icb1wed & "','" & icb1thu & "','" & icb1fri & "','" & icb1sat & "', " _
                + "'" & icb1sun & "','" & icb2o & "','" & icb2 & "','" & icb2mon & "','" & icb2tue & "','" & icb2wed & "','" & icb2thu & "','" & icb2fri & "','" & icb2sat & "', " _
                + "'" & icb2sun & "','" & icb3o & "','" & icb3 & "','" & icb3mon & "','" & icb3tue & "','" & icb3wed & "','" & icb3thu & "','" & icb3fri & "','" & icb3sat & "', " _
                + "'" & icb3sun & "', " _
                + "'" & ustr & "','" & eqid & "','" & sid & "', " _
                + "'" & oicb1o & "','" & oicb1 & "','" & oicb1mon & "','" & oicb1tue & "','" & oicb1wed & "','" & oicb1thu & "','" & oicb1fri & "','" & oicb1sat & "', " _
                + "'" & oicb1sun & "','" & oicb2o & "','" & oicb2 & "','" & oicb2mon & "','" & oicb2tue & "','" & oicb2wed & "','" & oicb2thu & "','" & oicb2fri & "','" & oicb2sat & "', " _
                + "'" & oicb2sun & "','" & oicb3o & "','" & oicb3 & "','" & oicb3mon & "','" & oicb3tue & "','" & oicb3wed & "','" & oicb3thu & "','" & oicb3fri & "','" & oicb3sat & "', " _
                + "'" & oicb3sun & "', " _
                + "'" & typor & "','" & typstror & "','" & freor & "','" & frestror & "','" & qtyor & "','" & tror & "','" & eqsor & "','" & eqsstror & "','" & taskstat & "','" & ordt & "', " _
                + "'" & hpm & "', @odes"

                'Dim typor, typstror, freor, frestror, ptor, ptstror As String
                'Dim skior, skistror, qtyor, tror, eqsor, eqsstror As String
                'usp_updatepmtaskstpm](@pmtskid int, @des varchar(500), @qty int, @ttime decimal(10,2), @rdid int, @rd varchar(50),
                '@fre int, @freq varchar(50), @lotoid int, @conid int, @ttid int, @tasktype varchar(50), @comid int, 
                '@compnum varchar(200), @fm1 varchar(350), @cqty int, @pfinterval int, @rdt decimal(10,2), @filter varchar(2000),
                '@pagenumber int, @fuid int,
                '@icb1o int, @icb1 int, @icb1mon int, @icb1tue int, @icb1wed int, @icb1thu int, @icb1fri int, @icb1sat int, 
                '@icb1sun int, @icb2o int, @icb2 int, @icb2mon int, @icb2tue int, @icb2wed int, @icb2thu int, @icb2fri int, @icb2sat int, 
                '@icb2sun int, @icb3o int, @icb3 int, @icb3mon int, @icb3tue int, @icb3wed int, @icb3thu int, @icb3fri int, @icb3sat int, 
                '@icb3sun int,
                '@ustr varchar(50), @eqid int, @sid int, @hpm int,
                '@firststr varchar(50), @secondstr varchar(50), @thirdstr varchar(50),
                '@shift1 varchar(50), @shift2 varchar(50), @shift3 varchar(50), @ms int, @cust varchar(10) = null
                Dim cmd As New SqlCommand
                cmd.CommandText = "exec usp_updatepmtaskstpmopt @pmtskid, @des, @qty, @ttime, @rdid, @rd, " _
                + "@fre, @freq, @lotoid, @conid, @ttid, @tasktype, @comid, " _
                + "@compnum, @fm1, @ofm1, @cqty, @pfinterval, @rdt, @filter, " _
                + "@pagenumber, @fuid, " _
                + "@icb1o, @icb1, @icb1mon, @icb1tue, @icb1wed, @icb1thu, @icb1fri, @icb1sat, " _
                + "@icb1sun, @icb2o, @icb2, @icb2mon, @icb2tue, @icb2wed, @icb2thu, @icb2fri, @icb2sat, " _
                + "@icb2sun, @icb3o, @icb3, @icb3mon, @icb3tue, @icb3wed, @icb3thu, @icb3fri, @icb3sat, " _
                + "@icb3sun, " _
                + "@ustr, @eqid, @sid, " _
                + "@oicb1o, @oicb1, @oicb1mon, @oicb1tue, @oicb1wed, @oicb1thu, @oicb1fri, @oicb1sat, " _
                + "@oicb1sun, @oicb2o, @oicb2, @oicb2mon, @oicb2tue, @oicb2wed, @oicb2thu, @oicb2fri, @oicb2sat, " _
                + "@oicb2sun, @oicb3o, @oicb3, @oicb3mon, @oicb3tue, @oicb3wed, @oicb3thu, @oicb3fri, @oicb3sat, " _
                + "@oicb3sun, " _
                + "@ottid, @otasktype, @ofre, @ofreq, @oqty, @ottime, @ordid, @ord, @taskstat, @ordt, @hpm, @odes, " _
                + "@first, @second, @third, @shift1, @shift2, @shift3, @ms, @cust, @ocust, " _
                + "@c1cnt, @c2cnt, @c3cnt, @oc1cnt, @oc2cnt, @oc3cnt, " _
                + "@ofirst, @osecond, @othird, @oshift1, @oshift2, @oshift3"

                Dim param = New SqlParameter("@pmtskid", SqlDbType.Int)
                param.Value = ttid
                cmd.Parameters.Add(param)
                Dim param01 = New SqlParameter("@des", SqlDbType.VarChar)
                If des = "" Then
                    param01.Value = System.DBNull.Value
                Else
                    param01.Value = des
                End If
                cmd.Parameters.Add(param01)
                Dim param02 = New SqlParameter("@qty", SqlDbType.Int)
                If qty = "" Then
                    param02.Value = "1"
                Else
                    param02.Value = qty
                End If
                cmd.Parameters.Add(param02)
                Dim param03 = New SqlParameter("@ttime", SqlDbType.Decimal)
                If tr = "" Then
                    param03.Value = "0"
                Else
                    param03.Value = tr
                End If
                cmd.Parameters.Add(param03)
                Dim param04 = New SqlParameter("@rdid", SqlDbType.Int)
                If eqs = "" Then
                    param04.Value = "0"
                Else
                    param04.Value = eqs
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@rd", SqlDbType.VarChar)
                If eqsstr = "" Or eqsstr = "Select" Then
                    param05.Value = System.DBNull.Value
                Else
                    param05.Value = eqsstr
                End If
                cmd.Parameters.Add(param05)
                Dim param06 = New SqlParameter("@fre", SqlDbType.Int)
                If fre = "" Then
                    param06.Value = "0"
                Else
                    param06.Value = fre
                End If
                cmd.Parameters.Add(param06)

                Dim param07 = New SqlParameter("@freq", SqlDbType.VarChar)
                If frestr = "" Or frestr = "Select" Then
                    param07.Value = System.DBNull.Value
                Else
                    param07.Value = frestr
                End If
                cmd.Parameters.Add(param07)
                Dim param08 = New SqlParameter("@lotoid", SqlDbType.Int)
                If lot = "" Then
                    param08.Value = "0"
                Else
                    param08.Value = lot
                End If
                cmd.Parameters.Add(param08)
                Dim param09 = New SqlParameter("@conid", SqlDbType.Int)
                If cs = "" Then
                    param09.Value = "0"
                Else
                    param09.Value = cs
                End If
                cmd.Parameters.Add(param09)
                Dim param10 = New SqlParameter("@ttid", SqlDbType.Int)
                If typ = "" Then
                    param10.Value = "0"
                Else
                    param10.Value = typ
                End If
                cmd.Parameters.Add(param10)
                Dim param11 = New SqlParameter("@tasktype", SqlDbType.VarChar)
                If typstr = "" Or typstr = "Select" Then
                    param11.Value = System.DBNull.Value
                Else
                    param11.Value = typstr
                End If
                cmd.Parameters.Add(param11)
                Dim param12 = New SqlParameter("@comid", SqlDbType.Int)
                If ci = "" Then
                    param12.Value = "0"
                Else
                    param12.Value = ci
                End If
                cmd.Parameters.Add(param12)
                Dim param13 = New SqlParameter("@compnum", SqlDbType.VarChar)
                If cn = "" Or cn = "Select" Then
                    param13.Value = System.DBNull.Value
                Else
                    param13.Value = cn
                End If
                cmd.Parameters.Add(param13)
                Dim param14 = New SqlParameter("@fm1", SqlDbType.VarChar)
                If fm = "" Then
                    param14.Value = System.DBNull.Value
                Else
                    param14.Value = fm
                End If
                cmd.Parameters.Add(param14)
                Dim param140 = New SqlParameter("@ofm1", SqlDbType.VarChar)
                If ofm = "" Then
                    param140.Value = System.DBNull.Value
                Else
                    param140.Value = ofm
                End If
                cmd.Parameters.Add(param140)
                Dim param15 = New SqlParameter("@cqty", SqlDbType.Int)
                If cqty = "" Then
                    param15.Value = "1"
                Else
                    param15.Value = cqty
                End If
                cmd.Parameters.Add(param15)
                Dim param16 = New SqlParameter("@pfinterval", SqlDbType.Int)
                If pf = "" Then
                    param16.Value = System.DBNull.Value
                Else
                    param16.Value = pf
                End If
                cmd.Parameters.Add(param16)
                Dim param17 = New SqlParameter("@rdt", SqlDbType.Decimal)
                If rdt = "" Then
                    param17.Value = "0"
                Else
                    param17.Value = rdt
                End If
                cmd.Parameters.Add(param17)
                Dim param18 = New SqlParameter("@filter", SqlDbType.VarChar)
                If Filter = "" Then
                    param18.Value = System.DBNull.Value
                Else
                    param18.Value = Filter
                End If
                cmd.Parameters.Add(param18)
                Dim param19 = New SqlParameter("@pagenumber", SqlDbType.Int)
                param19.Value = lblt.Value
                cmd.Parameters.Add(param19)
                Dim param20 = New SqlParameter("@fuid", SqlDbType.Int)
                If fuid = "" Then
                    param20.Value = "0"
                Else
                    param20.Value = fuid
                End If
                cmd.Parameters.Add(param20)
                Dim param21 = New SqlParameter("@icb1o", SqlDbType.Int)
                param21.Value = icb1o
                cmd.Parameters.Add(param21)
                Dim param22 = New SqlParameter("@icb1", SqlDbType.Int)
                param22.Value = icb1
                cmd.Parameters.Add(param22)
                Dim param23 = New SqlParameter("@icb1mon", SqlDbType.Int)
                param23.Value = icb1mon
                cmd.Parameters.Add(param23)
                Dim param24 = New SqlParameter("@icb1tue", SqlDbType.Int)
                param24.Value = icb1tue
                cmd.Parameters.Add(param24)
                Dim param25 = New SqlParameter("@icb1wed", SqlDbType.Int)
                param25.Value = icb1wed
                cmd.Parameters.Add(param25)
                Dim param26 = New SqlParameter("@icb1thu", SqlDbType.Int)
                param26.Value = icb1thu
                cmd.Parameters.Add(param26)
                Dim param27 = New SqlParameter("@icb1fri", SqlDbType.Int)
                param27.Value = icb1fri
                cmd.Parameters.Add(param27)
                Dim param28 = New SqlParameter("@icb1sat", SqlDbType.Int)
                param28.Value = icb1sat
                cmd.Parameters.Add(param28)
                Dim param29 = New SqlParameter("@icb1sun", SqlDbType.Int)
                param29.Value = icb1sun
                cmd.Parameters.Add(param29)

                Dim param30 = New SqlParameter("@icb2o", SqlDbType.Int)
                param30.Value = icb2o
                cmd.Parameters.Add(param30)
                Dim param31 = New SqlParameter("@icb2", SqlDbType.Int)
                param31.Value = icb2
                cmd.Parameters.Add(param31)
                Dim param32 = New SqlParameter("@icb2mon", SqlDbType.Int)
                param32.Value = icb2mon
                cmd.Parameters.Add(param32)
                Dim param33 = New SqlParameter("@icb2tue", SqlDbType.Int)
                param33.Value = icb2tue
                cmd.Parameters.Add(param33)
                Dim param34 = New SqlParameter("@icb2wed", SqlDbType.Int)
                param34.Value = icb2wed
                cmd.Parameters.Add(param34)
                Dim param35 = New SqlParameter("@icb2thu", SqlDbType.Int)
                param35.Value = icb2thu
                cmd.Parameters.Add(param35)
                Dim param36 = New SqlParameter("@icb2fri", SqlDbType.Int)
                param36.Value = icb2fri
                cmd.Parameters.Add(param36)
                Dim param37 = New SqlParameter("@icb2sat", SqlDbType.Int)
                param37.Value = icb2sat
                cmd.Parameters.Add(param37)
                Dim param38 = New SqlParameter("@icb2sun", SqlDbType.Int)
                param38.Value = icb2sun
                cmd.Parameters.Add(param38)

                Dim param39 = New SqlParameter("@icb3o", SqlDbType.Int)
                param39.Value = icb3o
                cmd.Parameters.Add(param39)
                Dim param40 = New SqlParameter("@icb3", SqlDbType.Int)
                param40.Value = icb3
                cmd.Parameters.Add(param40)
                Dim param41 = New SqlParameter("@icb3mon", SqlDbType.Int)
                param41.Value = icb3mon
                cmd.Parameters.Add(param41)
                Dim param42 = New SqlParameter("@icb3tue", SqlDbType.Int)
                param42.Value = icb3tue
                cmd.Parameters.Add(param42)
                Dim param43 = New SqlParameter("@icb3wed", SqlDbType.Int)
                param43.Value = icb3wed
                cmd.Parameters.Add(param43)
                Dim param44 = New SqlParameter("@icb3thu", SqlDbType.Int)
                param44.Value = icb3thu
                cmd.Parameters.Add(param44)
                Dim param45 = New SqlParameter("@icb3fri", SqlDbType.Int)
                param45.Value = icb3fri
                cmd.Parameters.Add(param45)
                Dim param46 = New SqlParameter("@icb3sat", SqlDbType.Int)
                param46.Value = icb3sat
                cmd.Parameters.Add(param46)
                Dim param47 = New SqlParameter("@icb3sun", SqlDbType.Int)
                param47.Value = icb3sun
                cmd.Parameters.Add(param47)

                Dim param48 = New SqlParameter("@ustr", SqlDbType.VarChar)
                If ustr = "" Then
                    param48.Value = System.DBNull.Value
                Else
                    param48.Value = ustr
                End If
                cmd.Parameters.Add(param48)
                Dim param49 = New SqlParameter("@eqid", SqlDbType.Int)
                param49.Value = eqid
                cmd.Parameters.Add(param49)
                Dim param50 = New SqlParameter("@sid", SqlDbType.Int)
                param50.Value = sid
                cmd.Parameters.Add(param50)


                Dim param51 = New SqlParameter("@oicb1o", SqlDbType.Int)
                param51.Value = oicb1o
                cmd.Parameters.Add(param51)
                Dim param52 = New SqlParameter("@oicb1", SqlDbType.Int)
                param52.Value = oicb1
                cmd.Parameters.Add(param52)
                Dim param53 = New SqlParameter("@oicb1mon", SqlDbType.Int)
                param53.Value = oicb1mon
                cmd.Parameters.Add(param53)
                Dim param54 = New SqlParameter("@oicb1tue", SqlDbType.Int)
                param54.Value = oicb1tue
                cmd.Parameters.Add(param54)
                Dim param55 = New SqlParameter("@oicb1wed", SqlDbType.Int)
                param55.Value = oicb1wed
                cmd.Parameters.Add(param55)
                Dim param56 = New SqlParameter("@oicb1thu", SqlDbType.Int)
                param56.Value = oicb1thu
                cmd.Parameters.Add(param56)
                Dim param57 = New SqlParameter("@oicb1fri", SqlDbType.Int)
                param57.Value = oicb1fri
                cmd.Parameters.Add(param57)
                Dim param58 = New SqlParameter("@oicb1sat", SqlDbType.Int)
                param58.Value = oicb1sat
                cmd.Parameters.Add(param58)
                Dim param59 = New SqlParameter("@oicb1sun", SqlDbType.Int)
                param59.Value = oicb1sun
                cmd.Parameters.Add(param59)

                Dim param60 = New SqlParameter("@oicb2o", SqlDbType.Int)
                param60.Value = oicb2o
                cmd.Parameters.Add(param60)
                Dim param61 = New SqlParameter("@oicb2", SqlDbType.Int)
                param61.Value = oicb2
                cmd.Parameters.Add(param61)
                Dim param62 = New SqlParameter("@oicb2mon", SqlDbType.Int)
                param62.Value = oicb2mon
                cmd.Parameters.Add(param62)
                Dim param63 = New SqlParameter("@oicb2tue", SqlDbType.Int)
                param63.Value = oicb2tue
                cmd.Parameters.Add(param63)
                Dim param64 = New SqlParameter("@oicb2wed", SqlDbType.Int)
                param64.Value = oicb2wed
                cmd.Parameters.Add(param64)
                Dim param65 = New SqlParameter("@oicb2thu", SqlDbType.Int)
                param65.Value = oicb2thu
                cmd.Parameters.Add(param65)
                Dim param66 = New SqlParameter("@oicb2fri", SqlDbType.Int)
                param66.Value = oicb2fri
                cmd.Parameters.Add(param66)
                Dim param67 = New SqlParameter("@oicb2sat", SqlDbType.Int)
                param67.Value = oicb2sat
                cmd.Parameters.Add(param67)
                Dim param68 = New SqlParameter("@oicb2sun", SqlDbType.Int)
                param68.Value = oicb2sun
                cmd.Parameters.Add(param68)

                Dim param69 = New SqlParameter("@oicb3o", SqlDbType.Int)
                param69.Value = oicb3o
                cmd.Parameters.Add(param69)
                Dim param70 = New SqlParameter("@oicb3", SqlDbType.Int)
                param70.Value = oicb3
                cmd.Parameters.Add(param70)
                Dim param71 = New SqlParameter("@oicb3mon", SqlDbType.Int)
                param71.Value = oicb3mon
                cmd.Parameters.Add(param71)
                Dim param72 = New SqlParameter("@oicb3tue", SqlDbType.Int)
                param72.Value = oicb3tue
                cmd.Parameters.Add(param72)
                Dim param73 = New SqlParameter("@oicb3wed", SqlDbType.Int)
                param73.Value = oicb3wed
                cmd.Parameters.Add(param73)
                Dim param74 = New SqlParameter("@oicb3thu", SqlDbType.Int)
                param74.Value = oicb3thu
                cmd.Parameters.Add(param74)
                Dim param75 = New SqlParameter("@oicb3fri", SqlDbType.Int)
                param75.Value = oicb3fri
                cmd.Parameters.Add(param75)
                Dim param76 = New SqlParameter("@oicb3sat", SqlDbType.Int)
                param76.Value = oicb3sat
                cmd.Parameters.Add(param76)
                Dim param77 = New SqlParameter("@oicb3sun", SqlDbType.Int)
                param77.Value = oicb3sun
                cmd.Parameters.Add(param77)
                Dim param78 = New SqlParameter("@ottid", SqlDbType.Int)
                If typor = "" Then
                    param78.Value = "0"
                Else
                    param78.Value = typor
                End If
                cmd.Parameters.Add(param78)

                Dim param79 = New SqlParameter("@otasktype", SqlDbType.VarChar)
                If typstror = "" Or typstror = "Select" Then
                    param79.Value = System.DBNull.Value
                Else
                    param79.Value = typstror
                End If
                cmd.Parameters.Add(param79)

                Dim param80 = New SqlParameter("@ofre", SqlDbType.Int)
                If freor = "" Then
                    param80.Value = "0"
                Else
                    param80.Value = freor
                End If
                cmd.Parameters.Add(param80)

                Dim param81 = New SqlParameter("@ofreq", SqlDbType.VarChar)
                If frestror = "" Or frestror = "Select" Then
                    param81.Value = System.DBNull.Value
                Else
                    param81.Value = frestror
                End If
                cmd.Parameters.Add(param81)

                Dim param82 = New SqlParameter("@oqty", SqlDbType.Int)
                If qtyor = "" Then
                    param82.Value = "1"
                Else
                    param82.Value = qtyor
                End If
                cmd.Parameters.Add(param82)
                Dim param83 = New SqlParameter("@ottime", SqlDbType.Decimal)
                If tror = "" Then
                    param83.Value = "0"
                Else
                    param83.Value = tror
                End If
                cmd.Parameters.Add(param83)

                Dim param84 = New SqlParameter("@ordid", SqlDbType.Int)
                If eqsor = "" Then
                    param84.Value = "0"
                Else
                    param84.Value = eqsor
                End If
                cmd.Parameters.Add(param84)
                Dim param85 = New SqlParameter("@ord", SqlDbType.VarChar)
                If eqsstror = "" Or eqsstror = "Select" Then
                    param85.Value = System.DBNull.Value
                Else
                    param85.Value = eqsstror
                End If
                cmd.Parameters.Add(param85)


                Dim param86 = New SqlParameter("@taskstat", SqlDbType.VarChar)
                If taskstat = "" Or taskstat = "Select" Then
                    param86.Value = System.DBNull.Value
                Else
                    param86.Value = taskstat
                End If
                cmd.Parameters.Add(param86)
                'Dim param87 = New SqlParameter("@taskstatid", SqlDbType.Int)
                'If taskstatid = "" Then
                'param87.Value = "0"
                'Else
                'param87.Value = taskstatid
                'End If
                'cmd.Parameters.Add(param87)

                Dim param88 = New SqlParameter("@ordt", SqlDbType.Decimal)
                If ordt = "" Then
                    param88.Value = "0"
                Else
                    param88.Value = ordt
                End If
                cmd.Parameters.Add(param88)

                Dim param89 = New SqlParameter("@hpm", SqlDbType.Int)
                If hpm = "" Then
                    param89.Value = "0"
                Else
                    param89.Value = hpm
                End If
                cmd.Parameters.Add(param89)

                Dim param90 = New SqlParameter("@odes", SqlDbType.VarChar)
                If odes = "" Then
                    param90.Value = System.DBNull.Value
                Else
                    param90.Value = odes
                End If
                cmd.Parameters.Add(param90)

                Dim param91 = New SqlParameter("@first", SqlDbType.VarChar)
                If firststr = "" Then
                    param91.Value = System.DBNull.Value
                Else
                    param91.Value = firststr
                End If
                cmd.Parameters.Add(param91)

                Dim param92 = New SqlParameter("@second", SqlDbType.VarChar)
                If secondstr = "" Then
                    param92.Value = System.DBNull.Value
                Else
                    param92.Value = secondstr
                End If
                cmd.Parameters.Add(param92)

                Dim param93 = New SqlParameter("@third", SqlDbType.VarChar)
                If thirdstr = "" Then
                    param93.Value = System.DBNull.Value
                Else
                    param93.Value = thirdstr
                End If
                cmd.Parameters.Add(param93)

                Dim param94 = New SqlParameter("@shift1", SqlDbType.VarChar)
                If shiftonly1 = "" Then
                    param94.Value = System.DBNull.Value
                Else
                    param94.Value = shiftonly1
                End If
                cmd.Parameters.Add(param94)

                Dim param95 = New SqlParameter("@shift2", SqlDbType.VarChar)
                If shiftonly2 = "" Then
                    param95.Value = System.DBNull.Value
                Else
                    param95.Value = shiftonly2
                End If
                cmd.Parameters.Add(param95)

                Dim param96 = New SqlParameter("@shift3", SqlDbType.VarChar)
                If shiftonly3 = "" Then
                    param96.Value = System.DBNull.Value
                Else
                    param96.Value = shiftonly3
                End If
                cmd.Parameters.Add(param96)

                Dim param97 = New SqlParameter("@ms", SqlDbType.VarChar)
                param97.Value = ms
                cmd.Parameters.Add(param97)

                Dim param98 = New SqlParameter("@cust", SqlDbType.VarChar)
                If cust = "" Then
                    param98.Value = "0"
                Else
                    param98.Value = cust
                End If
                cmd.Parameters.Add(param98)

                Dim param99 = New SqlParameter("@ocust", SqlDbType.VarChar)
                If ocust = "" Then
                    param99.Value = "0"
                Else
                    param99.Value = ocust
                End If
                cmd.Parameters.Add(param99)

                Dim param100 = New SqlParameter("@c1cnt", SqlDbType.Int)
                If IsDBNull(c1cnt) Then
                    param100.Value = 0
                Else
                    param100.Value = c1cnt
                End If
                cmd.Parameters.Add(param100)

                Dim param101 = New SqlParameter("@c2cnt", SqlDbType.Int)
                If IsDBNull(c2cnt) Then
                    param101.Value = 0
                Else
                    param101.Value = c2cnt
                End If
                cmd.Parameters.Add(param101)

                Dim param102 = New SqlParameter("@c3cnt", SqlDbType.Int)
                If IsDBNull(c3cnt) Then
                    param102.Value = 0
                Else
                    param102.Value = c3cnt
                End If
                cmd.Parameters.Add(param102)

                Dim param104 = New SqlParameter("@oc1cnt", SqlDbType.Int)
                If IsDBNull(oc1cnt) Then
                    param104.Value = 0
                Else
                    param104.Value = oc1cnt
                End If
                cmd.Parameters.Add(param104)

                Dim param105 = New SqlParameter("@oc2cnt", SqlDbType.Int)
                If IsDBNull(oc2cnt) Then
                    param105.Value = 0
                Else
                    param105.Value = oc2cnt
                End If
                cmd.Parameters.Add(param105)

                Dim param106 = New SqlParameter("@oc3cnt", SqlDbType.Int)
                If IsDBNull(oc3cnt) Then
                    param106.Value = 0
                Else
                    param106.Value = oc3cnt
                End If
                cmd.Parameters.Add(param106)

                Dim param107 = New SqlParameter("@ofirst", SqlDbType.VarChar)
                If ofirststr = "" Then
                    param107.Value = System.DBNull.Value
                Else
                    param107.Value = ofirststr
                End If
                cmd.Parameters.Add(param107)

                Dim param108 = New SqlParameter("@osecond", SqlDbType.VarChar)
                If osecondstr = "" Then
                    param108.Value = System.DBNull.Value
                Else
                    param108.Value = osecondstr
                End If
                cmd.Parameters.Add(param108)

                Dim param109 = New SqlParameter("@othird", SqlDbType.VarChar)
                If othirdstr = "" Then
                    param109.Value = System.DBNull.Value
                Else
                    param109.Value = othirdstr
                End If
                cmd.Parameters.Add(param109)

                Dim param110 = New SqlParameter("@oshift1", SqlDbType.VarChar)
                If oshiftonly1 = "" Then
                    param110.Value = System.DBNull.Value
                Else
                    param110.Value = oshiftonly1
                End If
                cmd.Parameters.Add(param110)

                Dim param111 = New SqlParameter("@oshift2", SqlDbType.VarChar)
                If oshiftonly2 = "" Then
                    param111.Value = System.DBNull.Value
                Else
                    param111.Value = oshiftonly2
                End If
                cmd.Parameters.Add(param111)

                Dim param112 = New SqlParameter("@oshift3", SqlDbType.VarChar)
                If oshiftonly3 = "" Then
                    param112.Value = System.DBNull.Value
                Else
                    param112.Value = oshiftonly3
                End If
                cmd.Parameters.Add(param112)



                Try
                    tasks.UpdateHack(cmd)
                Catch ex As Exception
                    tasksadd.UpdateHack(cmd)
                End Try

                'RBAS
                rteid = lblrteid.Value
                If rteid <> "0" And rteid <> "" Then
                    rbskillid = lblrbskillid.Value
                    rbqty = lblrbqty.Value
                    rbfreq = lblrbfreq.Value
                    rbrdid = lblrbrdid.Value
                    If rbskillid <> ski Then
                        RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
                    ElseIf rbqty <> qty Then
                        RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
                    ElseIf rbfreq <> frestr Then
                        RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
                    ElseIf rbrdid <> eqs Then
                        RBASRem(ttid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
                    End If
                End If

                Dim currcnt As String = lblcurrsb.Value
                Dim cscnt As String = lblcurrcs.Value
                If st = 0 Then
                    PageNumber = lblpg.Text
                Else
                    PageNumber = currcnt + cscnt
                End If
                field = "funcid"
                val = fuid
                Filter = lblfilt.Value
                Try
                    LoadPage(PageNumber, Filter)
                Catch ex As Exception
                    LoadPage(PageNumber, Filter)
                End Try

                lblenable.Value = "1"
                lblsave.Value = "yes"
                eqid = lbleqid.Value
                Try
                    tasks.UpMod(eqid)
                Catch ex As Exception
                    tasksadd.UpMod(eqid)
                End Try
            Else
                Dim strMessage As String = tmod.getmsg("cdstr454", "tpmopttasks.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            Dim strMessage As String = tmod.getmsg("cdstr455", "tpmopttasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub RBASRem(ByVal ttid As String, ByVal rteid As String, ByVal rbskillid As String, ByVal rbqty As String, ByVal rbfreq As String, ByVal rbrdid As String)
        'sql = "update pmtasks set rteid = null, rte = null, rteno = null, rteseq = null, rteseqold = null where pmtskid = '" & ttid & "'"
        'Try
        'tasks.Update(sql)
        'Catch ex As Exception
        'tasksadd.Update(sql)
        'End Try

        'sql = "update pmtasks set rteid = NULL, rtechng = 1 where rteid = '" & rteid & "' and rteno = 0"
        'Try
        'tasks.Update(sql)
        'Catch ex As Exception
        'tasksadd.Update(sql)
        'End Try

        'sql = "usp_rbas_dflt_seq '" & rteid & "','" & rbskillid & "','" & rbqty & "','" & rbfreq & "','" & rbrdid & "'"
        sql = "usp_rem_rbas_seqT '" & rteid & "','" & ttid & "'"
        Try
            tasks.Update(sql)
        Catch ex As Exception
            tasksadd.Update(sql)
        End Try

    End Sub
    Private Sub DelRevised()
        Dim tid As String = lbltaskid.Value
        Dim tnum As String = lblpg.Text
        sid = lblsid.Value
        fuid = lblfuid.Value
        tasks.Open()
        'SaveTask2()
        'RBAS
        rteid = lblrteid.Value
        If rteid <> "0" And rteid <> "" Then
            rbskillid = lblrbskillid.Value
            rbqty = lblrbqty.Value
            rbfreq = lblrbfreq.Value
            rbrdid = lblrbrdid.Value
            RBASRem(tid, rteid, rbskillid, rbqty, rbfreq, rbrdid)
        End If
        sql = "usp_deltpmrevised '" & tid & "','" & tnum & "','" & fuid & "','" & sid & "'"
        tasks.Update(sql)
        PageNumber = lblpg.Text
        Filter = lblfilt.Value
        eqid = lbleqid.Value
        tasks.UpMod(eqid)
        LoadPage(PageNumber, Filter)
        lblenable.Value = "1"
        
        tasks.Dispose()
        'tasks.Dispose()
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label22.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "Label22")
        Catch ex As Exception
        End Try
        Try
            Label23.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "Label23")
        Catch ex As Exception
        End Try
        Try
            Label26.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "Label26")
        Catch ex As Exception
        End Try
        Try
            Label27.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "Label27")
        Catch ex As Exception
        End Try
        Try
            lang1085.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1085")
        Catch ex As Exception
        End Try
        Try
            lang1086.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1086")
        Catch ex As Exception
        End Try
        Try
            lang1087.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1087")
        Catch ex As Exception
        End Try
        Try
            lang1088.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1088")
        Catch ex As Exception
        End Try
        Try
            lang1089.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1089")
        Catch ex As Exception
        End Try
        Try
            lang1090.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1090")
        Catch ex As Exception
        End Try
        Try
            lang1091.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1091")
        Catch ex As Exception
        End Try
        Try
            lang1092.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1092")
        Catch ex As Exception
        End Try
        Try
            lang1093.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1093")
        Catch ex As Exception
        End Try
        Try
            lang1094.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1094")
        Catch ex As Exception
        End Try
        Try
            lang1095.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1095")
        Catch ex As Exception
        End Try
        Try
            lang1096.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1096")
        Catch ex As Exception
        End Try
        Try
            lang1097.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1097")
        Catch ex As Exception
        End Try
        Try
            lang1098.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1098")
        Catch ex As Exception
        End Try
        Try
            lang1099.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1099")
        Catch ex As Exception
        End Try
        Try
            lang1100.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1100")
        Catch ex As Exception
        End Try
        Try
            lang1101.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1101")
        Catch ex As Exception
        End Try
        Try
            lang1102.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1102")
        Catch ex As Exception
        End Try
        Try
            lang1103.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1103")
        Catch ex As Exception
        End Try
        Try
            lang1104.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1104")
        Catch ex As Exception
        End Try
        Try
            lang1105.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1105")
        Catch ex As Exception
        End Try
        Try
            lang1106.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1106")
        Catch ex As Exception
        End Try
        Try
            lang1107.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1107")
        Catch ex As Exception
        End Try
        Try
            lang1108.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1108")
        Catch ex As Exception
        End Try
        Try
            lang1109.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1109")
        Catch ex As Exception
        End Try
        Try
            lang1110.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1110")
        Catch ex As Exception
        End Try
        Try
            lang1111.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1111")
        Catch ex As Exception
        End Try
        Try
            lang1112.Text = axlabs.GetASPXPage("tpmopttasks.aspx", "lang1112")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddcomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "btnaddcomp") & "', ABOVE, LEFT)")
            btnaddcomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btnaddnewfail.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "btnaddnewfail") & "')")
            btnaddnewfail.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            btnpfint.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "btnpfint") & "')")
            btnpfint.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ggrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ggrid") & "')")
            ggrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "Img3") & "')")
            Img3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img4.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "Img4") & "', ABOVE, LEFT)")
            Img4.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img5.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "Img5") & "', ABOVE, LEFT)")
            Img5.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgcopycomp.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "imgcopycomp") & "', ABOVE, LEFT)")
            imgcopycomp.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdeltask.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "imgdeltask") & "', ABOVE, LEFT)")
            imgdeltask.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgrat.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "imgrat") & "')")
            imgrat.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid143.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid143") & "', ABOVE, LEFT)")
            ovid143.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid144.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid144") & "')")
            ovid144.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid145.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid145") & "')")
            ovid145.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid146.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid146") & "')")
            ovid146.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid147.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid147") & "')")
            ovid147.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid148.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid148") & "')")
            ovid148.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid149.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid149") & "')")
            ovid149.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid150.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid150") & "')")
            ovid150.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid151.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid151") & "')")
            ovid151.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid152.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid152") & "')")
            ovid152.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid153.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid153") & "')")
            ovid153.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid154.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid154") & "')")
            ovid154.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid155.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid155") & "')")
            ovid155.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid156.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "ovid156") & "')")
            ovid156.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            sgrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("tpmopttasks.aspx", "sgrid") & "')")
            sgrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
