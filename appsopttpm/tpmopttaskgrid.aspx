<%@ Page Language="vb" AutoEventWireup="false" Codebehind="tpmopttaskgrid.aspx.vb" Inherits="lucy_r12.tpmopttaskgrid" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMOptTasksGrid</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/tpmopttaskgridaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
		<script language="javascript" type="text/javascript">
		function addtask() {
		document.getElementById("lblsubmit").value="addnew";
		document.getElementById("form1").submit();
		}
		function addtask2() {
		document.getElementById("lblsubmit").value="addnewcomp";
		document.getElementById("form1").submit();
		}
		</script>
	</HEAD>
	<body class="tbg"  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<div style="Z-INDEX: 100;POSITION: absolute;HEIGHT: 390px;OVERFLOW: auto;TOP: 0px;LEFT: 0px">
				<table width="722" cellspacing="0" cellpadding="0">
					<TBODY>
						<tr>
							<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/compresstpm.gif" border="0"></td>
							<td colSpan="2" class="thdrsingrt label" width="696"><asp:Label id="lang1080" runat="server">Current Tasks</asp:Label></td>
						</tr>
						<tr>
							<td colspan="3" id="tdaddtask" runat="server">
								<table>
									<tr>
										<td width="20"><IMG src="../images/appbuttons/minibuttons/addnew.gif" border="0" onclick="addtask();"></td>
										<td class="bluelabel" width="80"><asp:Label id="lang1042" runat="server">Add Task</asp:Label></td>
										<td width="20"><IMG id="imgaddcomp" class="details" onclick="addtask2();" border="0" src="../images/appbuttons/minibuttons/addnew.gif"
												runat="server"></td>
										<td class="bluelabel" width="382"><asp:label id="lbladdcomp" runat="server" Visible="False">Add Task to Component</asp:label></td>
										<td id="tdsort" class="plainlabelblue" width="200" align="right" runat="server"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colSpan="3"><asp:repeater id="rptrtasks" runat="server">
									<HeaderTemplate>
										<table cellspacing="2">
											<tr class="tbg" width="722" height="26">
												<td class="thdrsingg plainlabel" width="80"><asp:LinkButton OnClick="SortTasks" id="lang1081" runat="server">Task#</asp:LinkButton></td>
												<td class="thdrsingg plainlabel" width="180"><asp:LinkButton OnClick="SortCompTasks" id="lang1082" runat="server">Component Addressed</asp:LinkButton></td>
												<td class="thdrsingg plainlabel" width="40"><asp:Label id="lang1083" runat="server">Type</asp:Label></td>
												<td class="thdrsingg plainlabel" width="432"><asp:Label id="lang1084" runat="server">Task Description</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="transrowblue" height="20">
											<td class="plainlabel transrowblue">&nbsp;
												<asp:LinkButton ID="lbltn"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' Runat = server>
												</asp:LinkButton>
											</td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
												</asp:Label></td>
											<td class="label transrowblue" id="lblo" runat="server"><%# DataBinder.Eval(Container.DataItem,"o")%></td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"otaskdesc")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>' Runat = server>
												</asp:Label></td>
										</tr>
										<tr class="tbg">
											<td>&nbsp;</td>
											<td></td>
											<td class="label"><%# DataBinder.Eval(Container.DataItem,"r")%></td>
											<td class="plainlabel"><asp:Label ID="Label4" Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<AlternatingItemTemplate>
										<tr class="transrowblue" height="20">
											<td class="plainlabel transrowblue">&nbsp;
												<asp:LinkButton ID="lbltnalt"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' Runat = server>
												</asp:LinkButton>
											</td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
												</asp:Label></td>
											<td class="label transrowblue"><%# DataBinder.Eval(Container.DataItem,"o")%></td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"otaskdesc")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>' Runat = server>
												</asp:Label></td>
										</tr>
										<tr class="tbg">
											<td>&nbsp;</td>
											<td></td>
											<td class="label"><%# DataBinder.Eval(Container.DataItem,"r")%></td>
											<td class="plainlabel"><asp:Label ID="Label5" Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</AlternatingItemTemplate>
									<FooterTemplate>
				</table>
				</FooterTemplate> </asp:repeater></TD></TR></TBODY></TABLE>
			</div>
			<input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblfilt" type="hidden" name="lblfilt" runat="server"> <input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="taskcnt" type="hidden" name="taskcnt" runat="server"><input type="hidden" id="lbltasknum" runat="server">
			<input type="hidden" id="lblcoid" runat="server"><input type="hidden" id="lbltyp" runat="server" NAME="lbltyp">
			<input type="hidden" id="lbllid" runat="server" NAME="lbllid"> <input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="lblfslang" runat="server" NAME="lblfslang"><input type="hidden" id="lblcurrsort" runat="server" NAME="lblcurrsort">
			<input type="hidden" id="lblcurrcompsort" runat="server" NAME="lblcurrcompsort"><input type="hidden" id="lblcompnum" runat="server" NAME="lblcompnum">
		</form>
	</body>
</HTML>
