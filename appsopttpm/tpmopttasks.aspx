<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tpmopttasks.aspx.vb" Inherits="lucy_r12.tpmopttasks" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMOptTasks</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <!--<script language="javascript" src="../scripts/pmtaskdivfunctpm_1016_1.js"></script>-->
    <script language="javascript" type="text/javascript" src="../scripts/pmtaskdivfunctpm_1016a_21.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/tpmopttasksaspx_1.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />   
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function tasklookup() {

            var chken = document.getElementById("lblenable").value;
            var ro = document.getElementById("lblro").value;
            var tpmhold = document.getElementById("lbltpmhold").value;
            if ((chken != "1" || ro == "1") && tpmhold != "1") {
                ////window.parent.setref();
                //var str = document.getElementById("txtdesc").innerHTML;
                var str = document.getElementById("txtdesc").value;
                //alert(str)
                var eReturn = window.showModalDialog("../apps/TaskLookup.aspx?str=" + str, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                //alert(eReturn)
                if (eReturn != "no" && eReturn != "~none~") {
                    //document.getElementById("txtdesc").innerHTML = eReturn;
                    document.getElementById("txtdesc").value = eReturn;
                }
                else if (eReturn != "~none~") {
                    document.getElementById("form1").submit();
                }
            }
        }
        function checkday2(cbn) {
            var custchk = document.getElementById("lblcbcust").value;
            //if(custchk!="1") {
            var cb = document.getElementById(cbn);
            var freq = document.getElementById("txtfreq").value;
            if (freq != "Select" && freq != "1" && cb.checked == true && custchk != "1") {
                document.getElementById("cb2mon").checked = false;
                document.getElementById("cb2tue").checked = false;
                document.getElementById("cb2wed").checked = false;
                document.getElementById("cb2thu").checked = false;
                document.getElementById("cb2fri").checked = false;
                document.getElementById("cb2sat").checked = false;
                document.getElementById("cb2sun").checked = false;
                cb.checked = true;
                document.getElementById("cb2o").checked = true;

                document.getElementById("lcb2mon").value = "0";
                document.getElementById("lcb2tue").value = "0";
                document.getElementById("lcb2wed").value = "0";
                document.getElementById("lcb2thu").value = "0";
                document.getElementById("lcb2fri").value = "0";
                document.getElementById("lcb2sat").value = "0";
                document.getElementById("lcb2sun").value = "0";
                document.getElementById("l" + cbn).value = "1";
                document.getElementById("lcb2o").value = "1";
            }
            else if (freq != "Select" && freq != "1" && cb.checked == false && custchk != "1") {
                cb.checked = false;
                document.getElementById("cb2o").checked = false;

                document.getElementById("l" + cbn).value = "0";
                document.getElementById("lcb2o").value = "0";
            }
            else if (custchk == "1" && cb.checked == true) {
                cb.checked = true;
                document.getElementById("l" + cbn).value = "1";
                document.getElementById("lcb2o").value = "1";
                document.getElementById("cb2o").checked = true;

            }

            else if (custchk == "1" && cb.checked == false) {
                cb.checked = false;
                document.getElementById("l" + cbn).value = "0";
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("cb2" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk == 0) {
                    document.getElementById("lcb2o").value = "0";
                    document.getElementById("cb2o").checked = false;
                }
            }
            else if (((freq != "Select" && freq == "1") || custchk == "1") && cb.checked == true) {
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("cb2" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk != 0) {
                    cb.checked = true;
                    document.getElementById("cb2o").checked = true;

                    document.getElementById("l" + cbn).value = "1";
                    document.getElementById("lcb2o").value = "1";
                }
                else {
                    cb.checked = false;
                    document.getElementById("cb2o").checked = false;

                    document.getElementById("l" + cbn).value = "0";
                    document.getElementById("lcb2o").value = "0";
                }
            }
            else if (((freq != "Select" && freq == "1") || custchk == "1") && cb.checked == false) {
                cb.checked = false;

                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("cb2" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk == 0) {

                    document.getElementById("cb2o").checked = false;
                }
            }
            //}
        }
        function CheckDel(val) {

            if (val == "Delete") {
                var decision = confirm("Are You Sure You Want to Mark this Task as Delete?")
                if (decision == true) {
                    //document.getElementById("submit('ddcomp', '');
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("lbldel").value = "delr"
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }

            }
        }
        function GetType(app, sval) {
            //alert(app + ", " + sval)
            var typlst
            var sstr
            var ssti
            if (app == "d") {
                typlst = document.getElementById("ddtype");
                //ptlst = document.getElementById("ddpt");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtype").value;
                if (ssti == "7") {

                }
                else {

                }
            }
            else if (app == "o") {
                document.getElementById("ddtype").value = sval;
                typlst = document.getElementById("ddtypeo");
                //ptlst = document.getElementById("ddpt");
                //ptolst = document.getElementById("ddpto");
                sstr = typlst.options[typlst.selectedIndex].text;
                ssti = document.getElementById("ddtypeo").value;
                if (ssti == "7") {

                }
                else {
                    //ptlst.value="0";
                    //ptolst.value="0";

                }
            }
        }
        function filldown(id, val) {
            var ghostoff = document.getElementById("lblghostoff").value;
            var xstatus = document.getElementById("lblxstatus").value;
            var xflg = 0;
            if (ghostoff == "yes" && xstatus == "yes") {
                xflg = 1;
            }
            if (id == 'o') {
                if (xflg == 0) {
                    document.getElementById('txttr').value = val;
                }
                var dt = document.getElementById("ddeqstato").options[document.getElementById("ddeqstato").options.selectedIndex].text;
                dt = document.getElementById("ddeqstato").value;
                if (dt == "2") {
                    document.getElementById("txtordt").value = val;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = val;
                    }
                }
            }
            else {
                var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
                dt = document.getElementById("ddeqstat").value;
                if (dt == "2") {
                    document.getElementById("txtrdt").value = val;
                }
            }
        }
        function zerodt(id, val) {
            //var ghostoff = document.getElementById("lblghostoff").value;
            //var xstatus = document.getElementById("lblxstatus").value;
            //var xflg = 0;
            //if (ghostoff == "yes" && xstatus == "yes") {
            //    xflg = 1;
            //}
            if (id == 'o') {
                var dt = document.getElementById("ddeqstato").options[document.getElementById("ddeqstato").options.selectedIndex].text;
                dt = document.getElementById("ddeqstato").value;
                if (dt != "2") {
                    document.getElementById("txtordt").value = 0;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = "0";
                    }
                }
                else {
                    document.getElementById("txtordt").value = document.getElementById("txttro").value;
                    if (xflg == 0) {
                        document.getElementById("txtrdt").value = document.getElementById("txttr").value;
                    }
                }
            }
            else {
                var dt = document.getElementById("ddeqstat").options[document.getElementById("ddeqstat").options.selectedIndex].text;
                dt = document.getElementById("ddeqstat").value;
                if (dt != "2") {
                    document.getElementById("txtrdt").value = "0";
                }
                else {
                    document.getElementById("txtrdt").value = document.getElementById("txttr").value;
                }
            }
        }
        function checkit() {
            //var freq = document.getElementById("lblfreq").value;
            //var freqo = document.getElementById("lblfreqo").value;
            //checkfreq17(freq)
            //checkfreq17o(freqo)
            populateArrays();
            var start = document.getElementById("lblstart").value;
            if (start == "yes1") {
                GetNavGrid();
            }

            var tsk = document.getElementById("lblpg").innerHTML;
            var fuid = document.getElementById("lblfuid").value;
            var ro = document.getElementById("lblro").value;
            if (tsk != "") {
                window.parent.handletask1(tsk, fuid, ro);
            }

            document.getElementById("lblstart").value = "no";

            var grid = document.getElementById("lblgrid").value;
            if (grid == "yes") {
                window.location.reload(false);
                document.getElementById("form1").submit();
            }
            var log = document.getElementById("lbllog").value;
            if (log == "no") setref();
            else window.setTimeout("setref();", 1205000);
        }
        function checkfreq17o(val) {
            if (val != 1 && val != 7) {
                document.getElementById("ocb1").checked = false;
                document.getElementById("ocb2").checked = false;
                document.getElementById("ocb3").checked = false;
                document.getElementById("ocb1").disabled = true;
                document.getElementById("ocb2").disabled = true;
                document.getElementById("ocb3").disabled = true;

                document.getElementById("ocb1mon").checked = false;
                document.getElementById("ocb1tue").checked = false;
                document.getElementById("ocb1wed").checked = false;
                document.getElementById("ocb1thu").checked = false;
                document.getElementById("ocb1fri").checked = false;
                document.getElementById("ocb1sat").checked = false;
                document.getElementById("ocb1sun").checked = false;

                document.getElementById("ocb1mon").disabled = true;
                document.getElementById("ocb1tue").disabled = true;
                document.getElementById("ocb1wed").disabled = true;
                document.getElementById("ocb1thu").disabled = true;
                document.getElementById("ocb1fri").disabled = true;
                document.getElementById("ocb1sat").disabled = true;
                document.getElementById("ocb1sun").disabled = true;

                document.getElementById("locb1mon").value = "0";
                document.getElementById("locb1tue").value = "0";
                document.getElementById("locb1wed").value = "0";
                document.getElementById("locb1thu").value = "0";
                document.getElementById("locb1fri").value = "0";
                document.getElementById("locb1sat").value = "0";
                document.getElementById("locb1sun").value = "0";

                document.getElementById("ocb2mon").checked = false;
                document.getElementById("ocb2tue").checked = false;
                document.getElementById("ocb2wed").checked = false;
                document.getElementById("ocb2thu").checked = false;
                document.getElementById("ocb2fri").checked = false;
                document.getElementById("ocb2sat").checked = false;
                document.getElementById("ocb2sun").checked = false;

                document.getElementById("ocb2mon").disabled = true;
                document.getElementById("ocb2tue").disabled = true;
                document.getElementById("ocb2wed").disabled = true;
                document.getElementById("ocb2thu").disabled = true;
                document.getElementById("ocb2fri").disabled = true;
                document.getElementById("ocb2sat").disabled = true;
                document.getElementById("ocb2sun").disabled = true;

                document.getElementById("locb2mon").value = "0";
                document.getElementById("locb2tue").value = "0";
                document.getElementById("locb2wed").value = "0";
                document.getElementById("locb2thu").value = "0";
                document.getElementById("locb2fri").value = "0";
                document.getElementById("locb2sat").value = "0";
                document.getElementById("locb2sun").value = "0";

                document.getElementById("ocb3mon").checked = false;
                document.getElementById("ocb3tue").checked = false;
                document.getElementById("ocb3wed").checked = false;
                document.getElementById("ocb3thu").checked = false;
                document.getElementById("ocb3fri").checked = false;
                document.getElementById("ocb3sat").checked = false;
                document.getElementById("ocb3sun").checked = false;

                document.getElementById("ocb3mon").disabled = true;
                document.getElementById("ocb3tue").disabled = true;
                document.getElementById("ocb3wed").disabled = true;
                document.getElementById("ocb3thu").disabled = true;
                document.getElementById("ocb3fri").disabled = true;
                document.getElementById("ocb3sat").disabled = true;
                document.getElementById("ocb3sun").disabled = true;

                document.getElementById("locb3mon").value = "0";
                document.getElementById("locb3tue").value = "0";
                document.getElementById("locb3wed").value = "0";
                document.getElementById("locb3thu").value = "0";
                document.getElementById("locb3fri").value = "0";
                document.getElementById("locb3sat").value = "0";
                document.getElementById("locb3sun").value = "0";
            }
            else {
                if (val == 7) {
                    document.getElementById("ocb1").checked = false;
                    document.getElementById("ocb2").checked = false;
                    document.getElementById("ocb3").checked = false;
                    document.getElementById("ocb1").disabled = true;
                    document.getElementById("ocb2").disabled = true;
                    document.getElementById("ocb3").disabled = true;
                }
                else {
                    document.getElementById("ocb1").checked = false;
                    document.getElementById("ocb2").checked = false;
                    document.getElementById("ocb3").checked = false;
                    document.getElementById("ocb1").disabled = false;
                    document.getElementById("ocb2").disabled = false;
                    document.getElementById("ocb3").disabled = false;
                }

                document.getElementById("ocb1mon").checked = false;
                document.getElementById("ocb1tue").checked = false;
                document.getElementById("ocb1wed").checked = false;
                document.getElementById("ocb1thu").checked = false;
                document.getElementById("ocb1fri").checked = false;
                document.getElementById("ocb1sat").checked = false;
                document.getElementById("ocb1sun").checked = false;

                document.getElementById("ocb1mon").disabled = false;
                document.getElementById("ocb1tue").disabled = false;
                document.getElementById("ocb1wed").disabled = false;
                document.getElementById("ocb1thu").disabled = false;
                document.getElementById("ocb1fri").disabled = false;
                document.getElementById("ocb1sat").disabled = false;
                document.getElementById("ocb1sun").disabled = false;

                document.getElementById("locb1mon").value = "0";
                document.getElementById("locb1tue").value = "0";
                document.getElementById("locb1wed").value = "0";
                document.getElementById("locb1thu").value = "0";
                document.getElementById("locb1fri").value = "0";
                document.getElementById("locb1sat").value = "0";
                document.getElementById("locb1sun").value = "0";

                document.getElementById("ocb2mon").checked = false;
                document.getElementById("ocb2tue").checked = false;
                document.getElementById("ocb2wed").checked = false;
                document.getElementById("ocb2thu").checked = false;
                document.getElementById("ocb2fri").checked = false;
                document.getElementById("ocb2sat").checked = false;
                document.getElementById("ocb2sun").checked = false;

                document.getElementById("ocb2mon").disabled = false;
                document.getElementById("ocb2tue").disabled = false;
                document.getElementById("ocb2wed").disabled = false;
                document.getElementById("ocb2thu").disabled = false;
                document.getElementById("ocb2fri").disabled = false;
                document.getElementById("ocb2sat").disabled = false;
                document.getElementById("ocb2sun").disabled = false;

                document.getElementById("locb2mon").value = "0";
                document.getElementById("locb2tue").value = "0";
                document.getElementById("locb2wed").value = "0";
                document.getElementById("locb2thu").value = "0";
                document.getElementById("locb2fri").value = "0";
                document.getElementById("locb2sat").value = "0";
                document.getElementById("locb2sun").value = "0";

                document.getElementById("ocb3mon").checked = false;
                document.getElementById("ocb3tue").checked = false;
                document.getElementById("ocb3wed").checked = false;
                document.getElementById("ocb3thu").checked = false;
                document.getElementById("ocb3fri").checked = false;
                document.getElementById("ocb3sat").checked = false;
                document.getElementById("ocb3sun").checked = false;

                document.getElementById("ocb3mon").disabled = false;
                document.getElementById("ocb3tue").disabled = false;
                document.getElementById("ocb3wed").disabled = false;
                document.getElementById("ocb3thu").disabled = false;
                document.getElementById("ocb3fri").disabled = false;
                document.getElementById("ocb3sat").disabled = false;
                document.getElementById("ocb3sun").disabled = false;

                document.getElementById("locb3mon").value = "0";
                document.getElementById("locb3tue").value = "0";
                document.getElementById("locb3wed").value = "0";
                document.getElementById("locb3thu").value = "0";
                document.getElementById("locb3fri").value = "0";
                document.getElementById("locb3sat").value = "0";
                document.getElementById("locb3sun").value = "0";
            }
            checkfreq17(val);
            document.getElementById("txtfreq").value = val;
        }
        function checkfreq17(val) {
            if (val != 1 && val != 7) {
                document.getElementById("cb1").checked = false;
                document.getElementById("cb2").checked = false;
                document.getElementById("cb3").checked = false;
                document.getElementById("cb1").disabled = true;
                document.getElementById("cb2").disabled = true;
                document.getElementById("cb3").disabled = true;

                document.getElementById("cb1mon").checked = false;
                document.getElementById("cb1tue").checked = false;
                document.getElementById("cb1wed").checked = false;
                document.getElementById("cb1thu").checked = false;
                document.getElementById("cb1fri").checked = false;
                document.getElementById("cb1sat").checked = false;
                document.getElementById("cb1sun").checked = false;

                document.getElementById("cb1mon").disabled = true;
                document.getElementById("cb1tue").disabled = true;
                document.getElementById("cb1wed").disabled = true;
                document.getElementById("cb1thu").disabled = true;
                document.getElementById("cb1fri").disabled = true;
                document.getElementById("cb1sat").disabled = true;
                document.getElementById("cb1sun").disabled = true;

                document.getElementById("lcb1mon").value = "0";
                document.getElementById("lcb1tue").value = "0";
                document.getElementById("lcb1wed").value = "0";
                document.getElementById("lcb1thu").value = "0";
                document.getElementById("lcb1fri").value = "0";
                document.getElementById("lcb1sat").value = "0";
                document.getElementById("lcb1sun").value = "0";

                document.getElementById("cb2mon").checked = false;
                document.getElementById("cb2tue").checked = false;
                document.getElementById("cb2wed").checked = false;
                document.getElementById("cb2thu").checked = false;
                document.getElementById("cb2fri").checked = false;
                document.getElementById("cb2sat").checked = false;
                document.getElementById("cb2sun").checked = false;

                document.getElementById("cb2mon").disabled = true;
                document.getElementById("cb2tue").disabled = true;
                document.getElementById("cb2wed").disabled = true;
                document.getElementById("cb2thu").disabled = true;
                document.getElementById("cb2fri").disabled = true;
                document.getElementById("cb2sat").disabled = true;
                document.getElementById("cb2sun").disabled = true;

                document.getElementById("lcb2mon").value = "0";
                document.getElementById("lcb2tue").value = "0";
                document.getElementById("lcb2wed").value = "0";
                document.getElementById("lcb2thu").value = "0";
                document.getElementById("lcb2fri").value = "0";
                document.getElementById("lcb2sat").value = "0";
                document.getElementById("lcb2sun").value = "0";

                document.getElementById("cb3mon").checked = false;
                document.getElementById("cb3tue").checked = false;
                document.getElementById("cb3wed").checked = false;
                document.getElementById("cb3thu").checked = false;
                document.getElementById("cb3fri").checked = false;
                document.getElementById("cb3sat").checked = false;
                document.getElementById("cb3sun").checked = false;

                document.getElementById("cb3mon").disabled = true;
                document.getElementById("cb3tue").disabled = true;
                document.getElementById("cb3wed").disabled = true;
                document.getElementById("cb3thu").disabled = true;
                document.getElementById("cb3fri").disabled = true;
                document.getElementById("cb3sat").disabled = true;
                document.getElementById("cb3sun").disabled = true;

                document.getElementById("lcb3mon").value = "0";
                document.getElementById("lcb3tue").value = "0";
                document.getElementById("lcb3wed").value = "0";
                document.getElementById("lcb3thu").value = "0";
                document.getElementById("lcb3fri").value = "0";
                document.getElementById("lcb3sat").value = "0";
                document.getElementById("lcb3sun").value = "0";
            }
            else {
                if (val == 7) {
                    document.getElementById("cb1").checked = false;
                    document.getElementById("cb2").checked = false;
                    document.getElementById("cb3").checked = false;
                    document.getElementById("cb1").disabled = true;
                    document.getElementById("cb2").disabled = true;
                    document.getElementById("cb3").disabled = true;
                }
                else {
                    document.getElementById("cb1").checked = false;
                    document.getElementById("cb2").checked = false;
                    document.getElementById("cb3").checked = false;
                    document.getElementById("cb1").disabled = false;
                    document.getElementById("cb2").disabled = false;
                    document.getElementById("cb3").disabled = false;
                }


                document.getElementById("cb1mon").checked = false;
                document.getElementById("cb1tue").checked = false;
                document.getElementById("cb1wed").checked = false;
                document.getElementById("cb1thu").checked = false;
                document.getElementById("cb1fri").checked = false;
                document.getElementById("cb1sat").checked = false;
                document.getElementById("cb1sun").checked = false;

                document.getElementById("cb1mon").disabled = false;
                document.getElementById("cb1tue").disabled = false;
                document.getElementById("cb1wed").disabled = false;
                document.getElementById("cb1thu").disabled = false;
                document.getElementById("cb1fri").disabled = false;
                document.getElementById("cb1sat").disabled = false;
                document.getElementById("cb1sun").disabled = false;

                document.getElementById("lcb1mon").value = "0";
                document.getElementById("lcb1tue").value = "0";
                document.getElementById("lcb1wed").value = "0";
                document.getElementById("lcb1thu").value = "0";
                document.getElementById("lcb1fri").value = "0";
                document.getElementById("lcb1sat").value = "0";
                document.getElementById("lcb1sun").value = "0";

                document.getElementById("cb2mon").checked = false;
                document.getElementById("cb2tue").checked = false;
                document.getElementById("cb2wed").checked = false;
                document.getElementById("cb2thu").checked = false;
                document.getElementById("cb2fri").checked = false;
                document.getElementById("cb2sat").checked = false;
                document.getElementById("cb2sun").checked = false;

                document.getElementById("cb2mon").disabled = false;
                document.getElementById("cb2tue").disabled = false;
                document.getElementById("cb2wed").disabled = false;
                document.getElementById("cb2thu").disabled = false;
                document.getElementById("cb2fri").disabled = false;
                document.getElementById("cb2sat").disabled = false;
                document.getElementById("cb2sun").disabled = false;

                document.getElementById("lcb2mon").value = "0";
                document.getElementById("lcb2tue").value = "0";
                document.getElementById("lcb2wed").value = "0";
                document.getElementById("lcb2thu").value = "0";
                document.getElementById("lcb2fri").value = "0";
                document.getElementById("lcb2sat").value = "0";
                document.getElementById("lcb2sun").value = "0";

                document.getElementById("cb3mon").checked = false;
                document.getElementById("cb3tue").checked = false;
                document.getElementById("cb3wed").checked = false;
                document.getElementById("cb3thu").checked = false;
                document.getElementById("cb3fri").checked = false;
                document.getElementById("cb3sat").checked = false;
                document.getElementById("cb3sun").checked = false;

                document.getElementById("cb3mon").disabled = false;
                document.getElementById("cb3tue").disabled = false;
                document.getElementById("cb3wed").disabled = false;
                document.getElementById("cb3thu").disabled = false;
                document.getElementById("cb3fri").disabled = false;
                document.getElementById("cb3sat").disabled = false;
                document.getElementById("cb3sun").disabled = false;

                document.getElementById("lcb3mon").value = "0";
                document.getElementById("lcb3tue").value = "0";
                document.getElementById("lcb3wed").value = "0";
                document.getElementById("lcb3thu").value = "0";
                document.getElementById("lcb3fri").value = "0";
                document.getElementById("lcb3sat").value = "0";
                document.getElementById("lcb3sun").value = "0";
            }
        }
        function checkcb1o(cbn) {
            var freq = document.getElementById("txtfreq").value;
            var custchk = document.getElementById("lblcbcust").value;
            if (custchk != "1") {
                var cb = document.getElementById(cbn);
                //alert(cb.checked)
                if (cb.checked == true) {
                    if (freq == "1") {
                        document.getElementById("cb1").checked = false;
                        document.getElementById("cb1").disabled = false;

                        document.getElementById("cb2").checked = false;
                        document.getElementById("cb2").disabled = false;

                        document.getElementById("cb3").checked = false;
                        document.getElementById("cb3").disabled = false;

                        document.getElementById("lcb1").value = "0";
                        document.getElementById("lcb2").value = "0";
                        document.getElementById("lcb3").value = "0";
                    }
                    else {
                        document.getElementById("cb1").checked = false;
                        document.getElementById("cb1").disabled = true;

                        document.getElementById("cb2").checked = false;
                        document.getElementById("cb2").disabled = true;

                        document.getElementById("cb3").checked = false;
                        document.getElementById("cb3").disabled = true;

                        document.getElementById("lcb1").value = "0";
                        document.getElementById("lcb2").value = "0";
                        document.getElementById("lcb3").value = "0";
                    }
                    

                    document.getElementById(cbn).checked = true;
                }
                else {
                    
                    if (freq == "Select") {
                        checkselect();
                    }
                    else if (freq == "1") {
                    //alert()
                        document.getElementById("cb1").disabled = false;
                        document.getElementById("cb2").disabled = false;
                        document.getElementById("cb3").disabled = false;
                        activatedays();
                    }
                    else {
                        document.getElementById("cb1").disabled = true;
                        document.getElementById("cb2").disabled = true;
                        document.getElementById("cb3").disabled = true;
                        var wk = parseInt(freq) / 7;
                        //alert(wk)
                        wk += '';
                        var dchk = wk.indexOf(".");
                        if (freq == "7") {
                            activatedays();
                        }
                    }

                }
            }
        }
        function checkocb1o(cbn1, cbn2) {
            var custchk = document.getElementById("lblocbcust").value;
            var freq = document.getElementById("txtfreqo").value;
            if (custchk != "1") {
                var fo = document.getElementById("txtfreqo").value;
                document.getElementById("txtfreq").value = fo;
                //checkfreq();
                var cb = document.getElementById(cbn1);
                if (cb.checked == true) {
                    document.getElementById(cbn2).checked = true;
                    if (freq == "1") {
                        document.getElementById("ocb1").checked = false;
                        document.getElementById("ocb1").disabled = false;

                        document.getElementById("ocb2").checked = false;
                        document.getElementById("ocb2").disabled = false;

                        document.getElementById("ocb3").checked = false;
                        document.getElementById("ocb3").disabled = false;

                        document.getElementById("locb1").value = "0";
                        document.getElementById("locb2").value = "0";
                        document.getElementById("locb3").value = "0";
                    }
                    else {
                        document.getElementById("ocb1").checked = false;
                        document.getElementById("ocb1").disabled = true;

                        document.getElementById("ocb2").checked = false;
                        document.getElementById("ocb2").disabled = true;

                        document.getElementById("ocb3").checked = false;
                        document.getElementById("ocb3").disabled = true;

                        document.getElementById("locb1").value = "0";
                        document.getElementById("locb2").value = "0";
                        document.getElementById("locb3").value = "0";
                    }
                    

                    document.getElementById(cbn2).checked = true;
                    checkcb1o(cbn2);
                }
                else {
                    document.getElementById(cbn2).checked = false;
                    if (freq == "Select") {
                        checkoselect();
                    }
                    else if (freq == "1") {
                        document.getElementById("ocb1").disabled = false;
                        document.getElementById("ocb2").disabled = false;
                        document.getElementById("ocb3").disabled = false;
                        activateodays();
                    }
                    else {
                        document.getElementById("ocb1").disabled = true;
                        document.getElementById("ocb2").disabled = true;
                        document.getElementById("ocb3").disabled = true;
                        var wk = parseInt(freq) / 7;
                        wk += '';
                        var dchk = wk.indexOf(".");
                        if (dchk == -1) {
                            activateodays();
                        }
                    }
                    checkcb1o(cbn2);
                }
            }
        }
        function checkday1o(cbn, cbn2) {
        //alert(cbn + "," + cbn2)
            var custchk = document.getElementById("lblocbcust").value;
            //if(custchk!="1") {
            var fro = document.getElementById("txtfreqo").value;
            document.getElementById("txtfreq").value = fro;
            //checkfreq();
            var cb = document.getElementById(cbn);
            var cb2 = document.getElementById(cbn2);
            var freq = document.getElementById("txtfreqo").value;
            //alert(freq + ", " + cb.checked + ", " + custchk + ", " + cbn + ", " + cbn2)
            if (freq != "Select" && freq != "1" && cb.checked == true && custchk != "1") {
                document.getElementById("ocb1mon").checked = false;
                document.getElementById("ocb1tue").checked = false;
                document.getElementById("ocb1wed").checked = false;
                document.getElementById("ocb1thu").checked = false;
                document.getElementById("ocb1fri").checked = false;
                document.getElementById("ocb1sat").checked = false;
                document.getElementById("ocb1sun").checked = false;
                cb.checked = true;
                document.getElementById("ocb1o").checked = true;
                document.getElementById("cb1mon").checked = false;
                document.getElementById("cb1tue").checked = false;
                document.getElementById("cb1wed").checked = false;
                document.getElementById("cb1thu").checked = false;
                document.getElementById("cb1fri").checked = false;
                document.getElementById("cb1sat").checked = false;
                document.getElementById("cb1sun").checked = false;
                cb2.checked = true;
                document.getElementById("cb1o").checked = true;

                document.getElementById("lcb1mon").value = "0";
                document.getElementById("lcb1tue").value = "0";
                document.getElementById("lcb1wed").value = "0";
                document.getElementById("lcb1thu").value = "0";
                document.getElementById("lcb1fri").value = "0";
                document.getElementById("lcb1sat").value = "0";
                document.getElementById("lcb1sun").value = "0";
                document.getElementById("l" + cbn2).value = "1";
                //alert("l" + cbn)
                document.getElementById("lcb1o").value = "1";

                document.getElementById("locb1mon").value = "0";
                document.getElementById("locb1tue").value = "0";
                document.getElementById("locb1wed").value = "0";
                document.getElementById("locb1thu").value = "0";
                document.getElementById("locb1fri").value = "0";
                document.getElementById("locb1sat").value = "0";
                document.getElementById("locb1sun").value = "0";
                document.getElementById("l" + cbn).value = "1";
                //alert("l" + cbn)
                document.getElementById("locb1o").value = "1";
            }
            else if (freq != "Select" && freq != "1" && cb.checked == false && custchk != "1") {
                cb.checked = false;
                document.getElementById("cb1o").checked = false;
                cb2.checked = false;
                document.getElementById("ocb1o").checked = false;

                document.getElementById("l" + cbn2).value = "0";
                document.getElementById("lcb1o").value = "0";
                document.getElementById("l" + cbn).value = "0";
                document.getElementById("locb1o").value = "0";
            }
            else if (custchk == "1" && cb.checked == true) {
                cb.checked = true;
                cb2.checked = true;
                document.getElementById("l" + cbn).value = "1";
                document.getElementById("l" + cbn2).value = "1";
                document.getElementById("lcb1o").value = "1";
                document.getElementById("locb1o").value = "1";
                document.getElementById("ocb1o").checked = true;
                document.getElementById("cb1o").checked = true;
            }

            else if (custchk == "1" && cb.checked == false) {
                cb.checked = false;
                cb2.checked = false;
                document.getElementById("l" + cbn).value = "0";
                document.getElementById("l" + cbn2).value = "0";
                
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb1" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk == 0) {
                    document.getElementById("lcb1o").value = "0";
                    document.getElementById("locb1o").value = "0";
                    document.getElementById("ocb1o").checked = false;
                    document.getElementById("cb1o").checked = false;
                }
                
            }
            else if (((freq != "Select" && freq == "1") || custchk == "1") && cb.checked == true) {
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb1" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk != 0) {
                    cb.checked = true;
                    document.getElementById("ocb1o").checked = true;
                    cb2.checked = true;
                    document.getElementById("cb1o").checked = true;

                    document.getElementById("l" + cbn2).value = "1";
                    document.getElementById("lcb1o").value = "1";
                    document.getElementById("l" + cbn).value = "1";
                    document.getElementById("locb1o").value = "1";
                }
                else {
                    cb.checked = false;
                    document.getElementById("ocb1o").checked = false;
                    cb2.checked = false;
                    document.getElementById("cb1o").checked = false;

                    document.getElementById("l" + cbn2).value = "0";
                    document.getElementById("lcb1o").value = "0";
                    document.getElementById("l" + cbn).value = "0";
                    document.getElementById("locb1o").value = "0";
                }
            }
            else if (((freq != "Select" && freq == "1") || custchk == "1") && cb.checked == false) {
                cb.checked = false;
                cb2.checked = false;
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb1" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk == 0) {
                    document.getElementById("ocb1o").checked = false;
                    document.getElementById("cb1o").checked = false;
                }
            }
            //}
        }
        function checkday2o(cbn, cbn2) {
            //alert(cbn + "," + cbn2)
            var custchk = document.getElementById("lblocbcust").value;
            //if(custchk!="1") {
            var fro = document.getElementById("txtfreqo").value;
            document.getElementById("txtfreq").value = fro;
            //checkfreq();
            var cb = document.getElementById(cbn);
            var cb2 = document.getElementById(cbn2);
            var freq = document.getElementById("txtfreqo").value;
            //alert(freq + ", " + cb.checked + ", " + custchk + ", " + cbn + ", " + cbn2)
            if (freq != "Select" && freq != "1" && cb.checked == true && custchk != "1") {
                document.getElementById("ocb2mon").checked = false;
                document.getElementById("ocb2tue").checked = false;
                document.getElementById("ocb2wed").checked = false;
                document.getElementById("ocb2thu").checked = false;
                document.getElementById("ocb2fri").checked = false;
                document.getElementById("ocb2sat").checked = false;
                document.getElementById("ocb2sun").checked = false;
                cb.checked = true;
                document.getElementById("ocb2o").checked = true;
                document.getElementById("cb2mon").checked = false;
                document.getElementById("cb2tue").checked = false;
                document.getElementById("cb2wed").checked = false;
                document.getElementById("cb2thu").checked = false;
                document.getElementById("cb2fri").checked = false;
                document.getElementById("cb2sat").checked = false;
                document.getElementById("cb2sun").checked = false;
                cb2.checked = true;
                document.getElementById("cb2o").checked = true;

                document.getElementById("lcb2mon").value = "0";
                document.getElementById("lcb2tue").value = "0";
                document.getElementById("lcb2wed").value = "0";
                document.getElementById("lcb2thu").value = "0";
                document.getElementById("lcb2fri").value = "0";
                document.getElementById("lcb2sat").value = "0";
                document.getElementById("lcb2sun").value = "0";
                document.getElementById("l" + cbn2).value = "1";
                document.getElementById("lcb2o").value = "1";

                document.getElementById("locb2mon").value = "0";
                document.getElementById("locb2tue").value = "0";
                document.getElementById("locb2wed").value = "0";
                document.getElementById("locb2thu").value = "0";
                document.getElementById("locb2fri").value = "0";
                document.getElementById("locb2sat").value = "0";
                document.getElementById("locb2sun").value = "0";
                document.getElementById("l" + cbn).value = "1";
                document.getElementById("locb2o").value = "1";
            }
            else if (freq != "Select" && freq != "1" && cb.checked == false && custchk != "1") {
                cb.checked = false;
                document.getElementById("cb2o").checked = false;
                cb2.checked = false;
                document.getElementById("ocb2o").checked = false;

                document.getElementById("l" + cbn2).value = "0";
                document.getElementById("lcb2o").value = "0";
                document.getElementById("l" + cbn).value = "0";
                document.getElementById("locb2o").value = "0";

            }
            else if (custchk == "1" && cb.checked == true) {
                cb.checked = true;
                cb2.checked = true;
                document.getElementById("l" + cbn).value = "1";
                document.getElementById("l" + cbn2).value = "1";
                document.getElementById("lcb2o").value = "1";
                document.getElementById("locb2o").value = "1";
                document.getElementById("ocb2o").checked = true;
                document.getElementById("cb2o").checked = true;
            }

            else if (custchk == "1" && cb.checked == false) {
                cb.checked = false;
                cb2.checked = false;
                document.getElementById("l" + cbn).value = "0";
                document.getElementById("l" + cbn2).value = "0";
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb2" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk == 0) {
                    document.getElementById("lcb2o").value = "0";
                    document.getElementById("locb2o").value = "0";
                    document.getElementById("ocb2o").checked = false;
                    document.getElementById("cb2o").checked = false;
                }
            }
            else if (((freq != "Select" && freq == "1") || custchk == "1") && cb.checked == true) {
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb2" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk != 0) {
                    cb.checked = true;
                    document.getElementById("ocb2o").checked = true;
                    cb2.checked = true;
                    document.getElementById("cb2o").checked = true;

                    document.getElementById("l" + cbn2).value = "1";
                    document.getElementById("lcb2o").value = "1";
                    document.getElementById("l" + cbn).value = "1";
                    document.getElementById("locb2o").value = "1";
                }
                else {
                    cb.checked = false;
                    document.getElementById("ocb2o").checked = false;
                    cb2.checked = false;
                    document.getElementById("cb2o").checked = false;

                    document.getElementById("l" + cbn2).value = "0";
                    document.getElementById("lcb2o").value = "0";
                    document.getElementById("l" + cbn).value = "0";
                    document.getElementById("locb2o").value = "0";
                }
            }
            else if (((freq != "Select" && freq == "1") || custchk == "1") && cb.checked == false) {
                cb.checked = false;
                cb2.checked = false;
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb2" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk == 0) {
                    document.getElementById("ocb2o").checked = false;
                    document.getElementById("cb2o").checked = false;
                }
            }
            //}
        }

        function checkday3o(cbn, cbn2) {
            var custchk = document.getElementById("lblocbcust").value;
            //if(custchk!="1") {

            var cb = document.getElementById(cbn);
            var cb2 = document.getElementById(cbn2);
            var freq = document.getElementById("txtfreqo").value;
            //alert(freq + ", " + cb.checked + ", " + custchk + ", " + cbn + ", " + cbn2)
            if (freq != "Select" && freq != "1" && cb.checked == true && custchk != "1") {
                document.getElementById("ocb3mon").checked = false;
                document.getElementById("ocb3tue").checked = false;
                document.getElementById("ocb3wed").checked = false;
                document.getElementById("ocb3thu").checked = false;
                document.getElementById("ocb3fri").checked = false;
                document.getElementById("ocb3sat").checked = false;
                document.getElementById("ocb3sun").checked = false;
                cb.checked = true;
                document.getElementById("ocb3o").checked = true;
                document.getElementById("cb3mon").checked = false;
                document.getElementById("cb3tue").checked = false;
                document.getElementById("cb3wed").checked = false;
                document.getElementById("cb3thu").checked = false;
                document.getElementById("cb3fri").checked = false;
                document.getElementById("cb3sat").checked = false;
                document.getElementById("cb3sun").checked = false;
                cb2.checked = true;
                //alert(cb2.checked + ", " + cb2)
                document.getElementById("cb3o").checked = true;

                document.getElementById("lcb3mon").value = "0";
                document.getElementById("lcb3tue").value = "0";
                document.getElementById("lcb3wed").value = "0";
                document.getElementById("lcb3thu").value = "0";
                document.getElementById("lcb3fri").value = "0";
                document.getElementById("lcb3sat").value = "0";
                document.getElementById("lcb3sun").value = "0";
                document.getElementById("l" + cbn2).value = "1";
                document.getElementById("lcb3o").value = "1";

                document.getElementById("locb3mon").value = "0";
                document.getElementById("locb3tue").value = "0";
                document.getElementById("locb3wed").value = "0";
                document.getElementById("locb3thu").value = "0";
                document.getElementById("locb3fri").value = "0";
                document.getElementById("locb3sat").value = "0";
                document.getElementById("locb3sun").value = "0";
                document.getElementById("l" + cbn).value = "1";
                document.getElementById("locb3o").value = "1";
            }
            else if (freq != "Select" && freq != "1" && cb.checked == false && custchk != "1") {
                cb.checked = false;
                document.getElementById("cb3o").checked = false;
                cb2.checked = false;
                document.getElementById("ocb3o").checked = false;

                document.getElementById("l" + cbn2).value = "0";
                document.getElementById("lcb3o").value = "0";
                document.getElementById("l" + cbn).value = "0";
                document.getElementById("locb3o").value = "0";
            }
            else if (custchk == "1" && cb.checked == true) {
                cb.checked = true;
                cb2.checked = true;
                document.getElementById("l" + cbn).value = "1";
                document.getElementById("l" + cbn2).value = "1";
                document.getElementById("lcb3o").value = "1";
                document.getElementById("locb3o").value = "1";
                document.getElementById("ocb3o").checked = true;
                document.getElementById("cb3o").checked = true;
            }

            else if (custchk == "1" && cb.checked == false) {
                cb.checked = false;
                cb2.checked = false;
                document.getElementById("l" + cbn).value = "0";
                document.getElementById("l" + cbn2).value = "0";
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb3" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk == 0) {
                    document.getElementById("lcb3o").value = "0";
                    document.getElementById("locb3o").value = "0";
                    document.getElementById("ocb3o").checked = false;
                    document.getElementById("cb3o").checked = false;
                }
            }
            else if (((freq != "Select" && freq == "1") || custchk == "1") && cb.checked == true) {

                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb3" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                //alert(chk)
                if (chk != 0) {
                    cb.checked = true;
                    document.getElementById("ocb3o").checked = true;
                    cb2.checked = true;
                    document.getElementById("cb3o").checked = true;

                    document.getElementById("l" + cbn2).value = "1";
                    document.getElementById("lcb3o").value = "1";
                    document.getElementById("l" + cbn).value = "1";
                    document.getElementById("locb3o").value = "1";
                }
                else {
                    cb.checked = false;
                    document.getElementById("ocb3o").checked = false;
                    cb2.checked = false;
                    document.getElementById("cb3o").checked = false;

                    document.getElementById("l" + cbn2).value = "0";
                    document.getElementById("lcb3o").value = "0";
                    document.getElementById("l" + cbn).value = "0";
                    document.getElementById("locb3o").value = "0";
                }
            }
            else if (((freq != "Select" && freq == "1") || custchk == "1") && cb.checked == false) {
                cb.checked = false;
                cb2.checked = false;
                var chkstr = "mon,tue,wed,thu,fri,sat,sun"
                var chkarr = chkstr.split(",");
                var chk = 0;
                for (var i = 0; i <= chkarr.length - 1; i++) {
                    var c = document.getElementById("ocb3" + chkarr[i]).checked;
                    if (c == true) {
                        chk += 1;
                        break;
                    }
                }
                if (chk == 0) {
                    document.getElementById("ocb3o").checked = false;
                    document.getElementById("cb3o").checked = false;
                }
            }
            //}
        }
        function verifyofreq(typ) {
            var chk = 0;
            var ochk = 0;
            var rchk = 0;
            var oc1, oc2, oc3;
            var c1, c2, c3;
            var chkstr = "mon,tue,wed,thu,fri,sat,sun"
            var chkarr = chkstr.split(",");

            var ofreq = document.getElementById("txtfreqo").value;
            //alert(ofreq)
            if (ofreq == "1") {
                for (var i = 0; i < chkarr.length; i++) {
                    for (var j = 1; j < 4; j++) {
                        var c = document.getElementById("ocb" + j + chkarr[i]).checked;
                        if (c == true) {
                            ochk += 1;
                            break;
                        }
                    }
                }
                //alert(ochk)
                if (ochk == 7 || ochk == 0) {

                    var freq = document.getElementById("txtfreq").value;
                    if (freq == "1") {
                        for (var i = 0; i < chkarr.length; i++) {
                            for (var j = 1; j < 4; j++) {
                                var c = document.getElementById("cb" + j + chkarr[i]).checked;
                                if (c == true) {
                                    chk += 1;
                                    break;
                                }
                            }
                        }
                    }
                    //alert(chk)
                    if (chk == 7 || chk == 0) {
                        document.getElementById("lblcompchk").value = "3";
                        FreezeScreen('Your Data Is Being Processed...');
                        document.getElementById("form1").submit();
                    }
                    else {
                        alert("Revised Day Selection Incomplete(1)")
                    }
                }
                else {
                    alert("Original Day Selection Incomplete---")
                }
            }
            else {
                if (parseInt(ofreq) > 7) {
                    document.getElementById("ocb1mon").checked = false;
                    document.getElementById("ocb1tue").checked = false;
                    document.getElementById("ocb1wed").checked = false;
                    document.getElementById("ocb1thu").checked = false;
                    document.getElementById("ocb1fri").checked = false;
                    document.getElementById("ocb1sat").checked = false;
                    document.getElementById("ocb1sun").checked = false;

                    document.getElementById("locb1mon").value = "0";
                    document.getElementById("locb1tue").value = "0";
                    document.getElementById("locb1wed").value = "0";
                    document.getElementById("locb1thu").value = "0";
                    document.getElementById("locb1fri").value = "0";
                    document.getElementById("locb1sat").value = "0";
                    document.getElementById("locb1sun").value = "0";

                    document.getElementById("ocb2mon").checked = false;
                    document.getElementById("ocb2tue").checked = false;
                    document.getElementById("ocb2wed").checked = false;
                    document.getElementById("ocb2thu").checked = false;
                    document.getElementById("ocb2fri").checked = false;
                    document.getElementById("ocb2sat").checked = false;
                    document.getElementById("ocb2sun").checked = false;

                    document.getElementById("locb2mon").value = "0";
                    document.getElementById("locb2tue").value = "0";
                    document.getElementById("locb2wed").value = "0";
                    document.getElementById("locb2thu").value = "0";
                    document.getElementById("locb2fri").value = "0";
                    document.getElementById("locb2sat").value = "0";
                    document.getElementById("locb2sun").value = "0";

                    document.getElementById("ocb3mon").checked = false;
                    document.getElementById("ocb3tue").checked = false;
                    document.getElementById("ocb3wed").checked = false;
                    document.getElementById("ocb3thu").checked = false;
                    document.getElementById("ocb3fri").checked = false;
                    document.getElementById("ocb3sat").checked = false;
                    document.getElementById("ocb3sun").checked = false;

                    document.getElementById("locb3mon").value = "0";
                    document.getElementById("locb3tue").value = "0";
                    document.getElementById("locb3wed").value = "0";
                    document.getElementById("locb3thu").value = "0";
                    document.getElementById("locb3fri").value = "0";
                    document.getElementById("locb3sat").value = "0";
                    document.getElementById("locb3sun").value = "0";
                }
                verifyfreq('o');
            }
        }
        function verifyfreq(typ) {
            var chk = 0;
            var ochk = 0;
            var rchk = 0;
            var oc1, oc2, oc3;
            var c1, c2, c3;
            var chkstr = "mon,tue,wed,thu,fri,sat,sun"
            var chkarr = chkstr.split(",");

            var freq = document.getElementById("txtfreq").value;
            if (freq == "1") {

                var freq = document.getElementById("txtfreq").value;
                if (freq == "1") {
                    for (var i = 0; i < chkarr.length; i++) {
                        for (var j = 1; j < 4; j++) {
                            var c = document.getElementById("cb" + j + chkarr[i]).checked;
                            if (c == true) {
                                chk += 1;
                                break;
                            }
                        }
                    }
                }
                //alert(chk)
                if (chk == 7 || chk == 0) {
                    document.getElementById("lblcompchk").value = "3";
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }
                else {
                    if (typ == "o") {
                        alert("Revised Day Selection Incomplete")
                    }
                    else {
                        alert("Day Selection Incomplete")
                    }
                }
            }
            else {
                //
                if (parseInt(freq) != 7) {
                    document.getElementById("cb1mon").checked = false;
                    document.getElementById("cb1tue").checked = false;
                    document.getElementById("cb1wed").checked = false;
                    document.getElementById("cb1thu").checked = false;
                    document.getElementById("cb1fri").checked = false;
                    document.getElementById("cb1sat").checked = false;
                    document.getElementById("cb1sun").checked = false;

                    document.getElementById("lcb1mon").value = "0";
                    document.getElementById("lcb1tue").value = "0";
                    document.getElementById("lcb1wed").value = "0";
                    document.getElementById("lcb1thu").value = "0";
                    document.getElementById("lcb1fri").value = "0";
                    document.getElementById("lcb1sat").value = "0";
                    document.getElementById("lcb1sun").value = "0";

                    document.getElementById("cb2mon").checked = false;
                    document.getElementById("cb2tue").checked = false;
                    document.getElementById("cb2wed").checked = false;
                    document.getElementById("cb2thu").checked = false;
                    document.getElementById("cb2fri").checked = false;
                    document.getElementById("cb2sat").checked = false;
                    document.getElementById("cb2sun").checked = false;

                    document.getElementById("lcb2mon").value = "0";
                    document.getElementById("lcb2tue").value = "0";
                    document.getElementById("lcb2wed").value = "0";
                    document.getElementById("lcb2thu").value = "0";
                    document.getElementById("lcb2fri").value = "0";
                    document.getElementById("lcb2sat").value = "0";
                    document.getElementById("lcb2sun").value = "0";

                    document.getElementById("cb3mon").checked = false;
                    document.getElementById("cb3tue").checked = false;
                    document.getElementById("cb3wed").checked = false;
                    document.getElementById("cb3thu").checked = false;
                    document.getElementById("cb3fri").checked = false;
                    document.getElementById("cb3sat").checked = false;
                    document.getElementById("cb3sun").checked = false;

                    document.getElementById("lcb3mon").value = "0";
                    document.getElementById("lcb3tue").value = "0";
                    document.getElementById("lcb3wed").value = "0";
                    document.getElementById("lcb3thu").value = "0";
                    document.getElementById("lcb3fri").value = "0";
                    document.getElementById("lcb3sat").value = "0";
                    document.getElementById("lcb3sun").value = "0";
                }
                document.getElementById("lblcompchk").value = "3";
                FreezeScreen('Your Data Is Being Processed...');
                document.getElementById("form1").submit();
            }
        }
        function valpgnums() {
            if (document.getElementById("lblenable").value != "1") {
                var desc = document.getElementById("txtdesc").innerHTML;
                var odesc = document.getElementById("txtodesc").innerHTML;
                if (desc.length > 500) {
                    alert("Maximum Lenth for Task Description is 500 Characters")
                }
                else if (odesc.length > 500) {
                    alert("Maximum Lenth for Task Description is 500 Characters")
                }
                else if (isNaN(document.getElementById("txtqtyo").value)) {
                    alert("Original Skill Qty is Not a Number")
                }
                else if (isNaN(document.getElementById("txtqty").value)) {
                    alert("Revised Skill Qty is Not a Number")
                }
                else if (isNaN(document.getElementById("txttro").value)) {
                    alert("Original Skill Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txttr").value)) {
                    alert("Revised Skill Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txtordt").value)) {
                    alert("Original Running/Down Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txtrdt").value)) {
                    alert("Revised Running/Down Time is Not a Number")
                }
                else if (isNaN(document.getElementById("txtfreqo").value)) {
                    alert("Original Frequency is Not a Number")
                }
                else if (isNaN(document.getElementById("txtfreq").value)) {
                    alert("Revised Frequency is Not a Number")
                }
                else if (document.getElementById("ddtaskstat").value == 'Delete') {
                    CheckChanges();
                }
                else if (document.getElementById("txtfreqo").value != 'Select' && document.getElementById("txtfreqo").value != 0) {
                    
                    verifyofreq();
                }
                else if (document.getElementById("txtfreq").value != 'Select' && document.getElementById("txtfreq").value != 0) {
                    //alert("verifyfreq")
                    verifyfreq("o");
                }
                else {
                    var cbchk = document.getElementById("lblcbcust").value;
                    if (cbchk == "1") {
                        document.getElementById("cb1o").disabled = false;
                        document.getElementById("cb2o").disabled = false;
                        document.getElementById("cb3o").disabled = false;
                    }
                    var ocbchk = document.getElementById("lblocbcust").value;
                    if (ocbchk == "1") {
                        document.getElementById("ocb1o").disabled = false;
                        document.getElementById("ocb2o").disabled = false;
                        document.getElementById("ocb3o").disabled = false;
                    }
                    document.getElementById("lblcompchk").value = "3";
                    FreezeScreen('Your Data Is Being Processed...');
                    document.getElementById("form1").submit();
                }

            }
        }
        function todis() {
            document.getElementById("lblcompchk").value = "todis";
            FreezeScreen('Your Data Is Being Processed...');
            document.getElementById("form1").submit();
        }
        function jumpto() {
            var chken = document.getElementById("lblenable").value;
            var ro = document.getElementById("lblro").value;
            if (chken != "1" || ro == "1") {
                var typ = document.getElementById("ddtype").value;
                var pdm = ""; // document.getElementById("ddpt").value;
                var tid = document.getElementById("lbltaskid").value;
                var sid = document.getElementById("lblsid").value;
                var ro = document.getElementById("lblro").value;
                var eReturn = window.showModalDialog("../appstpm/commontasksdialogtpm.aspx?typ=" + typ + "&pdm=" + pdm + "&tid=" + tid + "&sid=" + sid + "&ro=" + ro, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    if (eReturn == "task") {
                        document.getElementById("lblcompchk").value = "6";
                        //document.getElementById("lbltaskholdid").value = eReturn;
                        document.getElementById("form1").submit();
                    }
                }
            }
        }
        //-->
    </script>
</head>
<body class="tbg" onload="page_maint('o');checkit();" >
    <div id="overDiv" style="z-index: 1000; visibility: hidden; position: absolute">
    </div>
    <form id="form1" method="post" runat="server">
    <div class="FreezePaneOff" id="FreezePane" align="center">
        <div class="InnerFreezePane" id="InnerFreezePane">
        </div>
    </div>
    <div style="z-index: 100; left: 0px; position: absolute; top: 0px">
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/compresstpm.gif" border="0">
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('Against what is the TPM intended to protect?', ABOVE, LEFT)"
                    onmouseout="return nd()" width="696">
                    <asp:Label ID="lang1085" runat="server">Failure Modes, Causes And/Or Effects</asp:Label>
                </td>
            </tr>
        </table>
        <table id="tbeq" cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td class="label" width="160" height="20">
                    <asp:Label ID="lang1086" runat="server">Component Addressed:</asp:Label>
                </td>
                <td class="label" width="242">
                    <asp:DropDownList ID="ddcomp" runat="server" Width="240px" CssClass="plainlabel">
                    </asp:DropDownList>
                </td>
                <td class="label" width="48">
                    <img id="btnaddcomp" onmouseover="return overlib('Add/Edit Components for Selected Function', ABOVE, LEFT)"
                        onclick="GetCompDiv();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                        runat="server"><img id="imgcopycomp" onmouseover="return overlib('Copy Components for Selected Function', ABOVE, LEFT)"
                            onclick="GetCompCopy();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                            runat="server" />
                </td>
                <td class="label" width="30">
                    <asp:Label ID="lang1087" runat="server">Qty:</asp:Label>
                </td>
                <td class="label" width="262">
                    <asp:TextBox ID="txtcQty" runat="server" Width="40px" CssClass="plainlabel"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table id="tbeq2" cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td width="395">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="20">
                            </td>
                            <td width="170">
                            </td>
                            <td width="23">
                            </td>
                            <td width="5">
                            </td>
                            <td width="195">
                            </td>
                            <td width="20">
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray">
                                &nbsp;
                            </td>
                            <td class="label transrowgray" align="center" colspan="1">
                                <asp:Label ID="lang1088" runat="server">Original Failure Modes</asp:Label>
                            </td>
                            <td class="transrowgray">
                            </td>
                            <td class="tbg">
                                &nbsp;
                            </td>
                            <td class="bluelabel transrowdrk" align="center" colspan="1">
                                <asp:Label ID="lang1089" runat="server">Revised Failure Modes</asp:Label>
                            </td>
                            <td class="transrowdrk">
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray">
                                &nbsp;
                            </td>
                            <td class="transrowgray" align="center">
                                <asp:ListBox ID="lbofailmodes" runat="server" CssClass="plainlabel" Width="150px"
                                    SelectionMode="Multiple" Height="50px" ForeColor="Blue"></asp:ListBox>
                            </td>
                            <td class="transrowgray" valign="middle" align="center">
                                <img class="details" id="todis1" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    width="20" runat="server" />
                                <img class="details" id="fromdis1" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20" runat="server" />
                                <asp:ImageButton ID="ibfromo" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardbg.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="ibtoo" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbgdrk.gif">
                                </asp:ImageButton>
                            </td>
                            <td class="tbg">
                                &nbsp;
                            </td>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbCompFM" runat="server" CssClass="plainlabel" Width="150px" SelectionMode="Multiple"
                                    Height="50px"></asp:ListBox>
                            </td>
                            <td class="transrowdrk" valign="middle" align="center">
                                <img id="btnaddnewfail" onmouseover="return overlib('Add a New Failure Mode to the Component Selected Above')"
                                    onclick="GetFailDiv();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/addmod.gif"
                                    runat="server" /><br>
                                <asp:ImageButton ID="ibReuse" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardblu.gif">
                                </asp:ImageButton><img class="details" id="fromreusedis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="338">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="150">
                            </td>
                            <td width="20">
                            </td>
                            <td width="150">
                            </td>
                            <td width="47">
                            </td>
                            <td width="10">
                            </td>
                        </tr>
                        <tr>
                            <td class="redlabel transrowdrk" align="center" bgcolor="#bad8fc">
                                <asp:Label ID="lang1090" runat="server">Not Addressed</asp:Label>
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                            <td class="bluelabel transrowdrk" align="center" bgcolor="#bad8fc">
                                <asp:Label ID="lang1091" runat="server">Selected</asp:Label>
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbfaillist" runat="server" CssClass="plainlabel" Width="150px" SelectionMode="Multiple"
                                    Height="50px" ForeColor="Red"></asp:ListBox>
                            </td>
                            <td class="transrowdrk" valign="middle" align="center">
                                <img class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    width="20" runat="server" />
                                <img class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    width="20" runat="server" />
                                <asp:ImageButton ID="ibToTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardblu.gif">
                                </asp:ImageButton><br />
                                <asp:ImageButton ID="ibFromTask" runat="server" ImageUrl="../images/appbuttons/minibuttons/backblu.gif">
                                </asp:ImageButton>
                            </td>
                            <td class="transrowdrk" align="center">
                                <asp:ListBox ID="lbfailmodes" runat="server" CssClass="plainlabel" Width="150px"
                                    SelectionMode="Multiple" Height="50px" ForeColor="Blue"></asp:ListBox>
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                            <td class="transrowdrk">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/compresstpm.gif" border="0" />
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
                    onmouseout="return nd()" width="684" colspan="3">
                    <asp:Label ID="lang1092" runat="server">Task Activity Details</asp:Label>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="372">
                        <tr>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="2">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray" colspan="4">
                                <asp:Label ID="lang1093" runat="server">Original Task Description</asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowgray" colspan="4">
                                <textarea id="txtodesc" onkeyup="copydesc(this.value);" style="font-size: 12px; width: 296px;
                                    font-family: Arial; height: 40px" name="txtdesc" rows="2" cols="34" onchange="copydesc(this.value);"
                                    runat="server"></textarea>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowgray">
                                <asp:Label ID="lang1094" runat="server">Type</asp:Label>
                            </td>
                            <td class="transrowgray">
                                <asp:DropDownList ID="ddtypeo" runat="server" Width="140px" CssClass="plainlabel"
                                    DataValueField="ttid" DataTextField="tasktype" Rows="1">
                                </asp:DropDownList>
                            </td>
                            <td class="label transrowgray">
                            </td>
                            <td class="transrowgray">
                            </td>
                            <td class="tbg">
                                <img src="../images/appbuttons/minibuttons/2PX.gif">
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table cellspacing="0" cellpadding="0" width="370">
                        <tr>
                            <td width="30">
                            </td>
                            <td width="140">
                            </td>
                            <td width="30">
                            </td>
                            <td width="90">
                            </td>
                            <td width="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk" colspan="5">
                                <asp:Label ID="lang1095" runat="server">Revised Task Description</asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="transrowdrk" colspan="4">
                                <textarea class="plainlabel" id="txtdesc" style="font-size: 12px; width: 290px; font-family: Arial;
                                    height: 40px" name="txtdesc" rows="2" cols="32" runat="server"></textarea>
                            </td>
                            <td class="transrowdrk">
                                <img id="btnlookup" onclick="tasklookup();" src="../images/appbuttons/minibuttons/lookupblu.gif"
                                    runat="server">
                                <img id="btnlookup2" onclick="jumpto();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                    runat="server">
                                <br>
                                <img onmouseover="return overlib('Add/Edit Measurements for this Task')" onclick="getMeasDiv();"
                                    onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/measblu.gif"
                                    width="27">
                            </td>
                        </tr>
                        <tr>
                            <td class="label transrowdrk">
                                <asp:Label ID="lang1096" runat="server">Type</asp:Label>
                            </td>
                            <td class="transrowdrk">
                                <asp:DropDownList ID="ddtype" runat="server" Width="140px" CssClass="plainlabel"
                                    DataValueField="ttid" DataTextField="tasktype" Rows="1">
                                </asp:DropDownList>
                            </td>
                            <td class="label transrowdrk">
                            </td>
                            <td class="label transrowdrk" colspan="2">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td class="thdrsinglft" align="left" width="26">
                    <img src="../images/appbuttons/minibuttons/compresstpm.gif" border="0" />
                </td>
                <td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
                    onmouseout="return nd()" width="554" colspan="3">
                    <asp:Label ID="lang1097" runat="server">Planning &amp; Scheduling Details</asp:Label>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742">
            <tr>
                <td class="label transrowgray" valign="top">
                    <table cellspacing="0" cellpadding="0" width="372">
                        <tr>
                            <td class="label">
                                Qty
                            </td>
                            <td>
                                <asp:TextBox ID="txtqtyo" runat="server" Width="24PX" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td class="label ">
                                <asp:Label ID="lang1098" runat="server">Min Ea</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txttro" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1099" runat="server">Status</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddeqstato" runat="server" Width="90px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                            <td class="label">
                                DT
                            </td>
                            <td>
                                <asp:TextBox ID="txtordt" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="label">
                                            <input id="ocbcust" onclick="checkcust('o');" type="checkbox" runat="server" /><asp:Label
                                                ID="lang1100" runat="server">Use Custom?</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang1101" runat="server">Frequency</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:textbox id="txtfreqo" runat="server" CssClass="plainlabel" Width="55px"></asp:textbox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="5">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="label" onmouseover="return overlib('Use to indicate shift - Auto Filled for Day Enries')"
                                            onmouseout="return nd()" align="center">
                                            <asp:Label ID="lang1102" runat="server">Shift</asp:Label>
                                        </td>
                                        <td class="label" onmouseover="return overlib('Selects All Days for Selected Shift')"
                                            onmouseout="return nd()" align="center">
                                            <asp:Label ID="lang1103" runat="server">All</asp:Label>
                                        </td>
                                        <td class="label">
                                        </td>
                                        <td class="label" align="center" width="23">
                                            M
                                        </td>
                                        <td class="label" align="center" width="23">
                                            Tu
                                        </td>
                                        <td class="label" align="center" width="23">
                                            W
                                        </td>
                                        <td class="label" align="center" width="23">
                                            Th
                                        </td>
                                        <td class="label" align="center" width="23">
                                            F
                                        </td>
                                        <td class="label" align="center" width="23">
                                            Sa
                                        </td>
                                        <td class="label" align="center" width="23">
                                            Su
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <input id="ocb1o" type="checkbox" name="cb1o" runat="server" />
                                        </td>
                                        <td>
                                            <input id="ocb1" type="checkbox" name="cb1" runat="server" />
                                        </td>
                                        <td class="label">
                                            1st
                                        </td>
                                        <td align="center">
                                            <input id="ocb1mon" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb1tue" type="checkbox" name="Checkbox2" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb1wed" type="checkbox" name="Checkbox3" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb1thu" type="checkbox" name="Checkbox4" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb1fri" type="checkbox" name="Checkbox5" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb1sat" type="checkbox" name="Checkbox6" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb1sun" type="checkbox" name="Checkbox7" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <input id="ocb2o" type="checkbox" name="cb1o" runat="server" />
                                        </td>
                                        <td>
                                            <input id="ocb2" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td class="label">
                                            2nd
                                        </td>
                                        <td align="center">
                                            <input id="ocb2mon" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb2tue" type="checkbox" name="Checkbox2" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb2wed" type="checkbox" name="Checkbox3" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb2thu" type="checkbox" name="Checkbox4" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb2fri" type="checkbox" name="Checkbox5" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb2sat" type="checkbox" name="Checkbox6" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb2sun" type="checkbox" name="Checkbox7" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <input id="ocb3o" type="checkbox" name="cb1o" runat="server" />
                                        </td>
                                        <td>
                                            <input id="ocb3" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td class="label">
                                            3rd
                                        </td>
                                        <td align="center">
                                            <input id="ocb3mon" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb3tue" type="checkbox" name="Checkbox2" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb3wed" type="checkbox" name="Checkbox3" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb3thu" type="checkbox" name="Checkbox4" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb3fri" type="checkbox" name="Checkbox5" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb3sat" type="checkbox" name="Checkbox6" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="ocb3sun" type="checkbox" name="Checkbox7" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="label transrowdrk" valign="top">
                    <table cellspacing="0" cellpadding="0" width="370">
                        <tr>
                            <td class="label">
                                Qty
                            </td>
                            <td class="label">
                                <asp:TextBox ID="txtqty" runat="server" Width="30px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1104" runat="server">Min Ea</asp:Label>
                            </td>
                            <td class="label">
                                <asp:TextBox ID="txttr" runat="server" CssClass="plainlabel" Width="35"></asp:TextBox>
                            </td>
                            <td class="label">
                                <asp:Label ID="lang1105" runat="server">Status</asp:Label>
                            </td>
                            <td class="label">
                                <asp:DropDownList ID="ddeqstat" runat="server" Width="80px" CssClass="plainlabel">
                                </asp:DropDownList>
                            </td>
                            <td class="label" style="width: 19px">
                                DT
                            </td>
                            <td class="label">
                                <asp:TextBox ID="txtrdt" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="label">
                                            <input id="cbcust" onclick="checkcust('r');" type="checkbox" name="Checkbox1" runat="server" /><asp:Label
                                                ID="lang1106" runat="server">Use Custom?</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" colspan="2">
                                            <asp:Label ID="lang1107" runat="server">Frequency</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:textbox id="txtfreq" runat="server" CssClass="plainlabel" Width="55px"></asp:textbox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" colspan="2">
                                            <asp:Label ID="lang1108" runat="server">P-F Interval</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtpfint" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                        <td>
                                            <img id="btnpfint" onmouseover="return overlib('Estimate Frequency using the PF Interval')"
                                                onclick="getPFDiv();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/lilcalcblu.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="5">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="label" onmouseover="return overlib('Use to indicate shift - Auto Filled for Day Enries')"
                                            onmouseout="return nd()" align="center">
                                            <asp:Label ID="lang1109" runat="server">Shift</asp:Label>
                                        </td>
                                        <td class="label" onmouseover="return overlib('Selects All Days for Selected Shift')"
                                            onmouseout="return nd()" align="center">
                                            <asp:Label ID="lang1110" runat="server">All</asp:Label>
                                        </td>
                                        <td class="label">
                                        </td>
                                        <td class="label" align="center" width="23">
                                            M
                                        </td>
                                        <td class="label" align="center" width="23">
                                            Tu
                                        </td>
                                        <td class="label" align="center" width="23">
                                            W
                                        </td>
                                        <td class="label" align="center" width="23">
                                            Th
                                        </td>
                                        <td class="label" align="center" width="23">
                                            F
                                        </td>
                                        <td class="label" align="center" width="23">
                                            Sa
                                        </td>
                                        <td class="label" align="center" width="23">
                                            Su
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <input id="cb1o" type="checkbox" name="cb1o" runat="server" />
                                        </td>
                                        <td>
                                            <input id="cb1" type="checkbox" name="cb1" runat="server" />
                                        </td>
                                        <td class="label">
                                            1st
                                        </td>
                                        <td align="center">
                                            <input id="cb1mon" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb1tue" type="checkbox" name="Checkbox2" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb1wed" type="checkbox" name="Checkbox3" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb1thu" type="checkbox" name="Checkbox4" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb1fri" type="checkbox" name="Checkbox5" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb1sat" type="checkbox" name="Checkbox6" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb1sun" type="checkbox" name="Checkbox7" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <input id="cb2o" type="checkbox" name="cb2o" runat="server" />
                                        </td>
                                        <td>
                                            <input id="cb2" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td class="label">
                                            2nd
                                        </td>
                                        <td align="center">
                                            <input id="cb2mon" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb2tue" type="checkbox" name="Checkbox2" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb2wed" type="checkbox" name="Checkbox3" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb2thu" type="checkbox" name="Checkbox4" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb2fri" type="checkbox" name="Checkbox5" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb2sat" type="checkbox" name="Checkbox6" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb2sun" type="checkbox" name="Checkbox7" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <input id="cb3o" type="checkbox" name="cb3o" runat="server" />
                                        </td>
                                        <td>
                                            <input id="cb3" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td class="label">
                                            3rd
                                        </td>
                                        <td align="center">
                                            <input id="cb3mon" type="checkbox" name="Checkbox1" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb3tue" type="checkbox" name="Checkbox2" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb3wed" type="checkbox" name="Checkbox3" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb3thu" type="checkbox" name="Checkbox4" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb3fri" type="checkbox" name="Checkbox5" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb3sat" type="checkbox" name="Checkbox6" runat="server" />
                                        </td>
                                        <td align="center">
                                            <input id="cb3sun" type="checkbox" name="Checkbox7" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="tbg">
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="bluelabel" width="80">
                                <asp:Label ID="lang1111" runat="server">Task Status</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddtaskstat" runat="server" CssClass="plainlabel" AutoPostBack="False">
                                    <asp:ListItem Value="Select">Select</asp:ListItem>
                                    <asp:ListItem Value="UnChanged">UnChanged</asp:ListItem>
                                    <asp:ListItem Value="Add">Add</asp:ListItem>
                                    <asp:ListItem Value="Revised">Revised</asp:ListItem>
                                    <asp:ListItem Value="Delete">Delete</asp:ListItem>
                                    <asp:ListItem Value="Detailed Steps">Detailed Steps</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="370">
                            <tr>
                                <td class="labelsm">
                                    <asp:CheckBox ID="cbloto" runat="server"></asp:CheckBox><asp:Label ID="lang1112"
                                        runat="server">LOTO</asp:Label><asp:CheckBox ID="cbcs" runat="server"></asp:CheckBox>CS
                                </td>
                                <td class="bluelabel" align="right">
                                    <img onmouseover="return overlib('Choose Equipment Spare Parts for this Task')" onclick="GetSpareDiv();"
                                        onmouseout="return nd()" src="../images/appbuttons/minibuttons/spareparts.gif" /><img
                                            id="Img3" onmouseover="return overlib('Add/Edit Parts for this Task')" onclick="GetPartDiv();"
                                            onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/parttrans.gif"
                                            width="23" runat="server" /><img onmouseover="return overlib('Add/Edit Tools for this Task')"
                                                onclick="GetToolDiv();" onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif"
                                                width="23" /><img onmouseover="return overlib('Add/Edit Lubricants for this Task')"
                                                    onclick="GetLubeDiv();" onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif"
                                                    width="23" /><img onmouseover="return overlib('Add/Edit Notes for this Task')" onclick="GetNoteDiv();"
                                                        onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/notessm.gif"
                                                        width="25" />
                                    <img id="Img5" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - This Task', ABOVE, LEFT)"
                                        onclick="editadj('0');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwd.gif"
                                        border="0" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="742" border="0">
            <tr id="Tr1" runat="server" name="Tr1">
                <td style="border-top: #7ba4e0 thin solid" align="center">
                    <img id="btnStart" height="20" src="../images/appbuttons/minibuttons/tostartbg.gif"
                        width="20" runat="server" /><img id="btnPrev" height="20" src="../images/appbuttons/minibuttons/prevarrowbg.gif"
                            width="20" runat="server" />
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center" height="30">
                    <asp:Label ID="Label22" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small"
                        Font-Names="Arial" Font-Bold="True">Task# </asp:Label><asp:Label ID="lblpg" runat="server"
                            CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
                            Font-Bold="True"></asp:Label><asp:Label ID="Label23" runat="server" CssClass="bluelabel"
                                ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"> of </asp:Label><asp:Label
                                    ID="lblcnt" runat="server" CssClass="bluelabel" ForeColor="Blue" Font-Size="X-Small"
                                    Font-Names="Arial" Font-Bold="True"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center">
                    <img id="btnNext" height="20" src="../images/appbuttons/minibuttons/nextarrowbg.gif"
                        width="20" runat="server" /><img id="btnEnd" height="20" src="../images/appbuttons/minibuttons/tolastbg.gif"
                            width="20" runat="server" />
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="center">
                    <asp:Label ID="Label26" runat="server" CssClass="greenlabel" Font-Size="X-Small"
                        Font-Names="Arial" Font-Bold="True">Sub Tasks: </asp:Label><asp:Label ID="lblsubcount"
                            runat="server" CssClass="greenlabel" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"></asp:Label><asp:Label
                                ID="lblspg" runat="server" CssClass="details" ForeColor="Green" Font-Size="X-Small"
                                Font-Names="Arial" Font-Bold="True"></asp:Label><asp:Label ID="Label27" runat="server"
                                    CssClass="details" ForeColor="Green" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"> of </asp:Label><asp:Label
                                        ID="lblscnt" runat="server" CssClass="details" ForeColor="Green" Font-Size="X-Small"
                                        Font-Names="Arial" Font-Bold="True"></asp:Label>
                </td>
                <td style="border-top: #7ba4e0 thin solid" align="right">
                <a href="#" onclick="retpm();" id="aretpm" runat="server" class="details">Return Task To PM</a>&nbsp;
                    <img onclick="GetNavGrid();" height="20" alt="" src="../images/appbuttons/minibuttons/navgrid.gif"
                        width="20" /><img id="btnedittask" onclick="doEnable();" height="19" alt="" src="../images/appbuttons/minibuttons/lilpentrans.gif"
                            width="19" border="0" runat="server"><asp:ImageButton ID="btnaddtsk" runat="server"
                                ImageUrl="../images/appbuttons/minibuttons/addnewbg1.gif"></asp:ImageButton>
                    <asp:ImageButton ID="btnaddsubtask" runat="server" CssClass="details" ImageUrl="../images/appbuttons/minibuttons/subtask.gif">
                    </asp:ImageButton><asp:ImageButton ID="ibCancel" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif">
                    </asp:ImageButton><asp:ImageButton ID="btnsavetask" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                        Visible="False"></asp:ImageButton><img id="btnsav" onclick="valpgnums();" height="20"
                            alt="" src="../images/appbuttons/minibuttons/savedisk1.gif" width="20" runat="server"><img
                                id="imgrat" onmouseover="return overlib('Add/Edit Rationale for Task Changes')"
                                onclick="GetRationale();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/rationale.gif"
                                width="20" runat="server" /><img onmouseover="return overlib('View Reports')" onclick="getValueAnalysis();"
                                    onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/report.gif" /><img
                                        class="details" id="ggrid" onmouseover="return overlib('Review Changes in Grid View')"
                                        onclick="GetGrid();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/grid1.gif"
                                        width="20" runat="server" /><img class="details" onmouseover="return overlib('Go to the Centralized PM Library', LEFT, WIDTH, 270)"
                                            onclick="GoToPMLib();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/gotolibsm.gif"
                                            width="23" /><img id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')" onclick="getsgrid();"
                                                onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif"
                                                width="20" />
                    <img id="Img4" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - All Tasks', ABOVE, LEFT)"
                        onclick="editadj('pre');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwall.gif"
                        border="0" runat="server" /><img id="imgdeltask" onmouseover="return overlib('Delete This Task', ABOVE, LEFT)"
                            onclick="checkdeltask();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/del.gif"
                            border="0" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td width="45">
                </td>
                <td width="120">
                </td>
                <td width="45">
                </td>
                <td width="90">
                </td>
                <td width="367">
                </td>
            </tr>
        </table>
    </div>
    <input id="lblsvchk" type="hidden" runat="server" />
    <input id="lblcompchk" type="hidden" name="lblcompchk" runat="server" />
    <input id="lblsb" type="hidden" name="lblsb" runat="server" />
    <input id="lblfilt" type="hidden" name="lblfilt" runat="server" />
    <input id="lblenable" type="hidden" name="lblenable" runat="server" />
    <input id="lblpgholder" type="hidden" name="lblpgholder" runat="server" />
    <input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server" />
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server" />
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server" />
    <input id="lblcid" type="hidden" name="lblcid" runat="server" />
    <input id="lblchk" type="hidden" name="lblchk" runat="server" />
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server" />
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server" />
    <input id="lblstart" type="hidden" name="lblpmtype" runat="server" />
    <input id="lblt" type="hidden" name="lblt" runat="server" />
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server" />
    <input id="lblst" type="hidden" name="lblst" runat="server" />
    <input id="lblco" type="hidden" name="lblco" runat="server" />
    <input id="lblpar" type="hidden" name="lblpar" runat="server" />
    <input id="lbltabid" type="hidden" name="lbltabid" runat="server" />
    <input id="lblpmstr" type="hidden" name="lblpmstr" runat="server" />
    <input id="pgflag" type="hidden" name="pgflag" runat="server" />
    <input id="lblpmid" type="hidden" name="lblpmid" runat="server" />
    <input id="lbldid" type="hidden" runat="server" />
    <input id="lblclid" type="hidden" runat="server" /><input id="lblsid" type="hidden"
        runat="server" />
    <input id="lbldel" type="hidden" runat="server" /><input id="lblsave" type="hidden"
        runat="server" />
    <input id="lblgrid" type="hidden" runat="server" /><input id="lblcind" type="hidden"
        runat="server" />
    <input id="lblcurrsb" type="hidden" runat="server" /><input id="lblcurrcs" type="hidden"
        runat="server" />
    <input id="lbldocpmid" type="hidden" runat="server" /><input id="lbldocpmstr" type="hidden"
        runat="server" />
    <input id="lblsesscnt" type="hidden" runat="server" /><input id="appchk" type="hidden"
        name="appchk" runat="server" />
    <input id="lbllog" type="hidden" name="lbllog" runat="server" /><input id="lblfiltcnt"
        type="hidden" runat="server" />
    <input id="lblusername" type="hidden" runat="server" />
    <input id="lbllock" type="hidden" name="lbllock" runat="server" />
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server" />
    <input id="lblhaspm" type="hidden" runat="server" />
    <input id="lbltyp" type="hidden" runat="server" />
    <input id="lbllid" type="hidden" runat="server" />
    <input id="lblnoeq" type="hidden" name="lblnoeq" runat="server" />
    <input id="lblro" type="hidden" name="lblro" runat="server" />
    <input id="lblfreqprev" type="hidden" runat="server" /><input id="lblfreqprevo" type="hidden"
        runat="server" />
    <input id="lblcbcust" type="hidden" runat="server" /><input id="lblocbcust" type="hidden"
        runat="server" />
    <input id="lcb1o" type="hidden" runat="server" name="lcb1o" />
    <input id="lcb1" type="hidden" runat="server" name="lcb1" />
    <input id="lcb1mon" type="hidden" runat="server" name="lcb1mon" />
    <input id="lcb1tue" type="hidden" runat="server" name="lcb1tue" />
    <input id="lcb1wed" type="hidden" runat="server" name="lcb1wed" />
    <input id="lcb1thu" type="hidden" runat="server" name="lcb1thu" />
    <input id="lcb1fri" type="hidden" runat="server" name="lcb1fri" />
    <input id="lcb1sat" type="hidden" runat="server" name="lcb1sat" />
    <input id="lcb1sun" type="hidden" runat="server" name="lcb1sun" />
    <input id="lcb2o" type="hidden" runat="server" name="lcb2o" />
    <input id="lcb2" type="hidden" runat="server" name="lcb2" />
    <input id="lcb2mon" type="hidden" runat="server" name="lcb2mon" />
    <input id="lcb2tue" type="hidden" runat="server" name="lcb2tue" />
    <input id="lcb2wed" type="hidden" runat="server" name="lcb2wed" />
    <input id="lcb2thu" type="hidden" runat="server" name="lcb2thu" />
    <input id="lcb2fri" type="hidden" runat="server" name="lcb2fri" />
    <input id="lcb2sat" type="hidden" runat="server" name="lcb2sat" />
    <input id="lcb2sun" type="hidden" runat="server" name="lcb2sun" />
    <input id="lcb3o" type="hidden" runat="server" name="lcb3o" />
    <input id="lcb3" type="hidden" runat="server" name="lcb3" />
    <input id="lcb3mon" type="hidden" runat="server" name="lcb3mon" />
    <input id="lcb3tue" type="hidden" runat="server" name="lcb3tue" />
    <input id="lcb3wed" type="hidden" runat="server" name="lcb3wed" />
    <input id="lcb3thu" type="hidden" runat="server" name="lcb3thu" />
    <input id="lcb3fri" type="hidden" runat="server" name="lcb3fri" />
    <input id="lcb3sat" type="hidden" runat="server" name="lcb3sat" />
    <input id="lcb3sun" type="hidden" runat="server" name="lcb3sun" />
    <input id="locb1o" type="hidden" runat="server" name="locb1o" />
    <input id="locb1" type="hidden" runat="server" name="locb1" />
    <input id="locb1mon" type="hidden" runat="server" name="locb1mon" />
    <input id="locb1tue" type="hidden" runat="server" name="locb1tue" />
    <input id="locb1wed" type="hidden" runat="server" name="locb1wed" />
    <input id="locb1thu" type="hidden" runat="server" name="locb1thu" />
    <input id="locb1fri" type="hidden" runat="server" name="locb1fri" />
    <input id="locb1sat" type="hidden" runat="server" name="locb1sat" />
    <input id="locb1sun" type="hidden" runat="server" name="locb1sun" />
    <input id="locb2o" type="hidden" runat="server" name="locb2o" />
    <input id="locb2" type="hidden" runat="server" name="locb2" />
    <input id="locb2mon" type="hidden" runat="server" name="locb2mon" />
    <input id="locb2tue" type="hidden" runat="server" name="locb2tue" />
    <input id="locb2wed" type="hidden" runat="server" name="locb2wed" />
    <input id="locb2thu" type="hidden" runat="server" name="locb2thu" />
    <input id="locb2fri" type="hidden" runat="server" name="locb2fri" />
    <input id="locb2sat" type="hidden" runat="server" name="locb2sat" />
    <input id="locb2sun" type="hidden" runat="server" name="locb2sun" />
    <input id="locb3o" type="hidden" runat="server" name="locb3o" />
    <input id="locb3" type="hidden" runat="server" name="locb3" />
    <input id="locb3mon" type="hidden" runat="server" name="locb3mon" />
    <input id="locb3tue" type="hidden" runat="server" name="locb3tue" />
    <input id="locb3wed" type="hidden" runat="server" name="locb3wed" />
    <input id="locb3thu" type="hidden" runat="server" name="locb3thu" />
    <input id="locb3fri" type="hidden" runat="server" name="locb3fri" />
    <input id="locb3sat" type="hidden" runat="server" name="locb3sat" />
    <input id="locb3sun" type="hidden" runat="server" name="locb3sun" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lblfreqo" runat="server" />
    <input type="hidden" id="lblxstatus" runat="server" />
    <input type="hidden" id="lblghostoff" runat="server" />
    <input type="hidden" id="lbltpmhold" runat="server" />
    <input type="hidden" id="lblrteid" runat="server" />
    <input type="hidden" id="lblrbskillid" runat="server" />
    <input type="hidden" id="lblrbqty" runat="server" />
    <input type="hidden" id="lblrbfreq" runat="server" />
    <input type="hidden" id="lblrbrdid" runat="server" />
    <input type="hidden" id="lblcomi" runat="server" />
    </form>
</body>
</html>
