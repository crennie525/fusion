<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tpmopttasksmain.aspx.vb"
    Inherits="lucy_r12.tpmopttasksmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title runat="server" id="pgtitle">PM3OptMain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" src="../scripts/taskgridtpm.js"></script>
    <script language="JavaScript" src="../scripts1/tpmopttasksmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function tpmjump(eqid, funid, jtyp) {

            cid = document.getElementById("lblcid").value
            tli = "5"
            sid = document.getElementById("lblsid").value
            did = document.getElementById("lbldid").value
            clid = document.getElementById("lblclid").value
            eqid = document.getElementById("lbleqid").value
            chk = document.getElementById("lblchk").value
            typ = document.getElementById("lbltyp").value;
            lid = document.getElementById("lbllid").value;
            comid = ""
            lvl = "5"
            if (jtyp == "r") {
                jtyp = "opt"
                //window.location = "PM3OptMain.aspx?jump=yes&cid=0&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eid + "&clid=" + clid + "&funid=" + fid + "&comid=" + comid + "&chk=" + cell + "&app=pmopt&lid=" + lid + "&typ=" + typ + "&ustr=" + ustr; 
                window.location = "../appsopt/PM3OptMain.aspx?jump=yes&start=yes&lvl=" + lvl + "&cid=" + cid + "&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + funid + "&comid=" + comid + "&chk=" + chk + "&lid=" + lid + "&typ=" + typ + "&jtyp=" + jtyp;
            }
            else if (jtyp == "o") {
                jtyp = "opt"
                window.location = "../apps/PMTasks.aspx?jump=yes&start=yes&lvl=" + lvl + "&cid=" + cid + "&tli=" + tli + "&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + funid + "&comid=" + comid + "&chk=" + chk + "&lid=" + lid + "&typ=" + typ + "&jtyp=" + jtyp;
            }
        }
        var procflag = 0
        var navflag = 0
        function opennav() {
            if (procflag == 0) {
                if (navflag == 0) {
                    navflag = 1;
                    //procflag=1;
                    document.getElementById("tdnav").className = "viewcell";
                    document.getElementById("tdnavtop").className = "thdrsinglft viewcell";
                    document.getElementById("tdnavtoprt").className = "thdrsingrt label viewcell";
                    //document.getElementById("tdnavtop").style.width="305px";
                    document.getElementById("tdproc").className = "details";
                    document.getElementById("tdproctop").className = "details";
                    document.getElementById("tdproctoplft").className = "details";
                    document.getElementById("tdproctoplft1").className = "details";
                    document.getElementById("tdproctop").style.width = "0px";
                    document.getElementById("tblopt").style.width = "915px";
                    document.getElementById("imgnav").src = "../images/appbuttons/minibuttons/close1.gif";
                }
                else {
                    navflag = 0;
                    //procflag=0;
                    document.getElementById("tdnav").className = "details";
                    document.getElementById("tdnavtop").className = "details";
                    document.getElementById("tdnavtoprt").className = "details";
                    document.getElementById("tdnavtop").style.width = "0px";
                    document.getElementById("tdproc").className = "viewcell";
                    document.getElementById("tdproctoplft").className = "thdrsingtbo viewcell";
                    document.getElementById("tdproctop").className = "thdrsingrt label viewcell";
                    document.getElementById("tdproctoplft1").className = "thdrsingtbo viewcell";
                    document.getElementById("tdproctop").style.width = "580px";
                    document.getElementById("tblopt").style.width = "1190px";
                    document.getElementById("imgnav").src = "../images/appbuttons/minibuttons/open.gif";
                }
            }
        }
    </script>
</head>
<body onload="checkarch();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table id="tblopt" style="z-index: 1; left: 2px; position: absolute; top: 76px" cellspacing="0"
        cellpadding="0" width="1316">
        <tr>
            <td id="tdqs" runat="server" colspan="2">
            </td>
        </tr>
        <tr>
            <td class="thdrsinglft" align="left" width="26">
                <img src="../images/appbuttons/minibuttons/compresstpm.gif" border="0" id="imgopt"
                    runat="server">
            </td>
            <td class="thdrsingrt label" align="left" width="720" id="tdloctop">
                <asp:Label ID="lang1113" runat="server">Location/Equipment/Function Details</asp:Label>
            </td>
            <td class="thdrsingtbo" valign="top" align="left" width="22">
                <img onmouseover="return overlib('Show/Hide Asset Information', ABOVE, LEFT)" onmouseout="return nd()"
                    id="imgnav" onclick="opennav();" src="../images/appbuttons/minibuttons/open.gif"
                    runat="server">
            </td>
            <td class="thdrsinglft details" id="tdnavtop" width="22">
                <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
            </td>
            <td class="thdrsingrt label details" id="tdnavtoprt" align="left" width="268">
                <font class="label">
                    <asp:Label ID="lang1114" runat="server">Asset Hierarchy</asp:Label></font>
            </td>
            <td class="thdrsingtbo" valign="top" align="left" width="22" id="tdproctoplft1">
                <img onmouseover="return overlib('Expand/Shrink Procedure to Optimize', ABOVE, LEFT)"
                    onmouseout="return nd()" id="imghide" onclick="hiderev();" src="../images/appbuttons/minibuttons/close1.gif"
                    runat="server">
            </td>
            <td class="thdrsingtbo" align="left" width="26" id="tdproctoplft">
                <img src="../images/appbuttons/minibuttons/compresstpm.gif" border="0">
            </td>
            <td class="thdrsingrt label" id="tdproctop" width="552">
                <font class="label">
                    <asp:Label ID="lang1115" runat="server">TPM Procedure to Optimize</asp:Label></font>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <table cellspacing="0" cellpadding="0" id="tbopt" runat="server">
                    <tr>
                        <td>
                            <iframe id="geteq" style="border-top-style: none; border-right-style: none; border-left-style: none;
                                background-color: transparent; border-bottom-style: none" src="tpmgetopt.aspx?tli=5&amp;jump=no"
                                frameborder="no" width="746" scrolling="no" height="114" runat="server" allowtransparency>
                            </iframe>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <iframe id="iftaskdet" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                border-left-style: none; background-color: transparent; border-bottom-style: none"
                                src="tpmopttaskgrid.aspx?start=no" frameborder="no" width="746" scrolling="no"
                                height="454" runat="server" allowtransparency></iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" align="center" width="22" class="viewcell">
                <img id="Img1" onclick="opennav();" src="../images/appbuttons/minibuttons/archlabel.gif"
                    runat="server">
            </td>
            <td class="details" id="tdnav" colspan="2" valign="top">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="2">
                            <iframe id="ifarch" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                border-left-style: none; background-color: transparent; border-bottom-style: none"
                                src="OptDevArchtpm.aspx?start=no" frameborder="no" width="270" scrolling="no"
                                height="180" runat="server" allowtransparency></iframe>
                        </td>
                    </tr>
                    <tr>
                        <td class="thdrsinglft" width="22">
                            <img src="../images/appbuttons/minibuttons/eqarch.gif" border="0">
                        </td>
                        <td class="thdrsingrt label" width="258">
                            <asp:Label ID="lang1116" runat="server">Task Images</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <iframe id="ifimg" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                margin: 0px; border-top-style: none; padding-top: 0px; border-right-style: none;
                                border-left-style: none; background-color: transparent; border-bottom-style: none"
                                src="../tpmpics/taskimagetpm.aspx?eqid=0" frameborder="no" width="270" scrolling="no"
                                height="280" runat="server" allowtransparency></iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td id="tdproc" valign="top" colspan="3" class="viewcell">
                <iframe id="ifpmdoc" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                    margin: 0px; padding-top: 0px; background-color: transparent" src="OptHoldertpm.aspx"
                    frameborder="yes" width="600" scrolling="no" height="480" runat="server" allowtransparency>
                </iframe>
            </td>
        </tr>
    </table>
    <input id="lbltab" type="hidden" name="lbltab" runat="server">
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
    <input type="hidden" id="lbltyp" runat="server" name="lbltyp">
    <input type="hidden" id="lbllid" runat="server" name="lbllid">
    <input id="lblret" type="hidden" name="lblret" runat="server">
    <input id="lbldchk" type="hidden" name="lbldchk" runat="server">
    <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
    <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
    <input id="tasknum" type="hidden" name="tasknum" runat="server">
    <input id="taskcnt" type="hidden" name="taskcnt" runat="server">
    <input type="hidden" id="retqs" runat="server">
    <input type="hidden" id="lblsave" runat="server">
    <input type="hidden" id="lblsavetasks" runat="server"><input type="hidden" id="lblro"
        runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
