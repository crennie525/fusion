

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class OptAssetImagetpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim pi As New Utilities
    Dim dr As SqlDataReader
    Dim eqid, Login As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents pr As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ne As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents po As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcur As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpareqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblurl As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetImgEq()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                If Request.QueryString("eqid") <> "0" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                    pi.Open()
                    LoadImages()
                    pi.Dispose()
                End If
            Catch ex As Exception
                tdcnt.InnerHtml = "Image 0 of 0"
                lblurl.Value = System.Configuration.ConfigurationManager.AppSettings("CustAppUrl").ToString
                lblurl.Value += "/" & System.Configuration.ConfigurationManager.AppSettings("CustAppName").ToString + "/"
            End Try
        Else

        End If

    End Sub
    Private Sub LoadImages()
        eqid = lbleqid.Value
        Dim pic As String
        sql = "select top 1 picurltm from pmPictures where funcid is null and comid is null and eqid = '" & eqid & "' " _
        + "order by pic_id"
        pic = pi.strScalar(sql)
        lblpic.Value = pic
        Dim piccnt As Integer
        sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        piccnt = pi.Scalar(sql)
        'lblcnt.Text = piccnt
        If Len(pic) <> 0 Then
            sql = "select origpareqid from equipment where eqid = '" & eqid & "'"
            dr = pi.GetRdrData(sql)
            While dr.Read
                lblpareqid.Value = dr.Item("origpareqid").ToString
            End While
            dr.Close()
            imgeq.Src = pic
            tdcnt.InnerHtml = "Image 1 of " & piccnt
            lblcur.Value = "1"
            lblcnt.Value = piccnt
        Else
            tdcnt.InnerHtml = "Image 0 of 0"
        End If

    End Sub
    Private Sub GetImgEq()
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/eqimg1.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/eqimg1.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/eqimg1.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/eqimg1.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/eqimg1.gif")
        End If
    End Sub
End Class
