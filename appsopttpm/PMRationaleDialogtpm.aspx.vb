

'********************************************************
'*
'********************************************************



Public Class PMRationaleDialogtpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim tnum, tid, deptid, cellid, eqid, comid, funid, sql, Login, ro As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            Try
                tid = Request.QueryString("tid").ToString
                lbltaskid.Value = tid
                If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                    tnum = Request.QueryString("tnum").ToString
                    deptid = Request.QueryString("did").ToString
                    cellid = Request.QueryString("clid").ToString
                    eqid = Request.QueryString("eqid").ToString
                    funid = Request.QueryString("fuid").ToString
                    comid = Request.QueryString("coid").ToString
                    'ro = Request.QueryString("ro").ToString
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblpg.Value = tnum
                    lbldid.Value = deptid
                    lblclid.Value = cellid
                    lbleqid.Value = eqid
                    lblcoid.Value = comid
                    lblfuid.Value = funid
                    lblro.Value = ro
                    If ro = "1" Then
                        'pgtitle.InnerHtml = "TPM Optimization Rationale (Read Only)"
                    Else
                        'pgtitle.InnerHtml = "TPM Optimization Rationale"
                    End If
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr432" , "PMRationaleDialogtpm.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "nodeptid"
                End If
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr433" , "PMRationaleDialogtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "nodeptid"
            End Try
        End If

    End Sub

End Class
