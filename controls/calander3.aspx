﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="calander3.aspx.vb" Inherits="lucy_r12.calander3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="JavaScript" type="text/javascript" src="../scripts1/calendaraspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <style type="text/css">
    
	.calTitle   {font-weight: bold;
	             font-size: 11;
	             background-color:#cccccc;
	             color: black;
	             width: 90px;
	}



	.calBody    {font-size: 11;
	             border-width: 10px;
	}
</style>

</head>
<body onload="checkit();">
        <form id="form1" runat="server">
        <table cellspacing="0">
        <tr>
                <td align="left" bgcolor="#cccccc">
                    <asp:DropDownList id="drpCalMonth" Runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" cssClass="calTitle" Width="120px"></asp:DropDownList>
                </td>
                <td align="right" bgcolor="#cccccc">
                    <asp:DropDownList id="drpCalYear" Runat="Server" AutoPostBack="True" OnSelectedIndexChanged="Set_Calendar" cssClass="calTitle"></asp:DropDownList>
                </td>
            </tr>



            <tr>
                <td colspan="2">
                    <asp:Calendar ID="Calendar2" runat="server" Height="206px" BorderStyle="Solid" BackColor="transparent"
                        BorderColor="Blue" Font-Size="X-Small" Font-Names="Arial" ForeColor="Black" Font-Bold="True"
                        Width="254px" FirstDayOfWeek="Sunday" DayHeaderStyle-BackColor="#eeeeee">
                        
                        <DayStyle BorderWidth="1px" BorderStyle="None" BorderColor="Blue"></DayStyle>
                        
                        <SelectedDayStyle Font-Size="X-Small" Font-Names="Arial" Font-Bold="True" BorderWidth="1px"
                            ForeColor="Blue" BorderStyle="Solid" BorderColor="Blue" BackColor="transparent">
                        </SelectedDayStyle>
                        
                        <OtherMonthDayStyle BackColor="#80FFFF"></OtherMonthDayStyle>
                    </asp:Calendar>
                </td>
            </tr>
        </table>
        <input id="lblwho" type="hidden" name="lblwho" runat="server" />
        <input id="lbldate" type="hidden" name="lbldate" runat="server" />
        <input id="lblch" type="hidden" name="lblch" runat="server" />
        <input type="hidden" id="lblcalday" runat="server" />
        <input type="hidden" id="lbldays" runat="server" />
        <input type="hidden" id="lblretday" runat="server" />
        <input type="hidden" id="lblfslang" runat="server" />
        <input type="hidden" id="lblsel" runat="server">
        </form>
    </body>
</html>
