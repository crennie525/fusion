<%@ Page Language="vb" AutoEventWireup="false" Codebehind="calandar2.aspx.vb" Inherits="lucy_r12.calandar2"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>calandar2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="javascript" type="text/javascript">
		    function checkit() {
		        var typ = document.getElementById("lbltyp").value;
		        var chk = document.getElementById("lblret").value;
		        if (typ == "wo") {
		            if (chk == "return,yes") {
		                var dt = document.getElementById("lbldate").value;
		                dt = dt + ",yes";
		                window.parent.handledt(dt);
		            }
		            else if (chk == "return,no") {
		                var dt = document.getElementById("lbldate").value;
		                dt = dt + ",no";
		                window.parent.handledt(dt);
		            }
		        }
		        else {
		            if (chk == "return") {
		                var dt = document.getElementById("lbldate").value;
		                window.parent.handledt(dt);
		            }
		        }
		
		}
		</script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bluelabel" width="140">Work Order#</td>
					<td id="tdwonum" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td id="tdschedstart" class="bluelabel" runat="server">Scheduled Start Date</td>
					<td id="tdeststart" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td id="tdschedcomp" class="bluelabel" runat="server">Scheduled Complete Date</td>
					<td id="tdestcomp" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="2"><asp:calendar id="Calendar1" runat="server" CellPadding="0" CssClass="plainlabel" Width="300px"
							BackColor="White" Height="250px">
							<SelectorStyle HorizontalAlign="Center" CssClass="bluelabel" VerticalAlign="Middle"></SelectorStyle>
							<DayStyle BorderWidth="1px" BorderStyle="None" BorderColor="Blue"></DayStyle>
							<NextPrevStyle HorizontalAlign="Center" CssClass="bluelabel" VerticalAlign="Middle"></NextPrevStyle>
							<DayHeaderStyle CssClass="bluelabel" BackColor="White"></DayHeaderStyle>
							<TitleStyle HorizontalAlign="Center" Height="22px" CssClass="label"></TitleStyle>
							<OtherMonthDayStyle BackColor="#E7F1FD"></OtherMonthDayStyle>
						</asp:calendar></td>
				</tr>
			</table>
			<input id="lbldate" type="hidden" name="lbldate" runat="server"> <input id="lblch" type="hidden" name="lblch" runat="server">
			<input id="lblwho" type="hidden" name="lblwho" runat="server"> <input id="lblwo" type="hidden" name="lblwo" runat="server">
			<input id="lblpr" type="hidden" name="lblpr" runat="server"> <input id="lbltb" type="hidden" name="lbltb" runat="server">
			<input id="lbleq" type="hidden" name="lbleq" runat="server"> <input id="lbltyp" type="hidden" runat="server">
			<input id="lbldays" type="hidden" runat="server"> <input id="lblcalday" type="hidden" runat="server">
			<input id="lblretday" type="hidden" runat="server"> <input id="lblchk" type="hidden" runat="server">
			<input id="lblret" type="hidden" runat="server"> <input type="hidden" id="lblodt" runat="server">
			<input type="hidden" id="lblsdt" runat="server">
		</form>
	</body>
</HTML>
