Imports System.Data.SqlClient
Public Class calandar2
    Inherits System.Web.UI.Page
    Dim dt, days, typ, wo, who, chk, cdt, sdt, dtyp As String
    Dim calday As Integer
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldays As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcalday As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretday As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblodt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdwonum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdschedstart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeststart As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdschedcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdestcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsdt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim cal As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Calendar1 As System.Web.UI.WebControls.Calendar
    Protected WithEvents lbldate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
                Try
                    dt = Request.QueryString("dt").ToString
                    Try
                        calday = Request.QueryString("calday").ToString
                        lblcalday.Value = calday
                    Catch ex As Exception
                        lblcalday.Value = -1
                    End Try
                    Try
                        days = Request.QueryString("days").ToString
                        lbldays.Value = days
                    Catch ex As Exception
                        lbldays.Value = "no"
                    End Try
                    If days = "" Then
                        lbldays.Value = "no"
                    End If
                    If dt <> "0" Then
                        Dim sel As Date
                        sel = CDate(dt)
                        Calendar1.VisibleDate = sel
                        Calendar1.SelectedDate = sel

                    End If
                Catch ex As Exception

                End Try
            Catch ex As Exception
                lblwho.Value = "other"
            End Try

            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            If typ = "wo" Then
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                cdt = Request.QueryString("cdt").ToString
                lblodt.Value = cdt
                sdt = Request.QueryString("sdt").ToString
                lblsdt.Value = sdt
                wo = Request.QueryString("wo").ToString
                lblwo.Value = wo
                tdwonum.InnerHtml = wo
                tdeststart.InnerHtml = sdt
                'lblstart.Value = sdt
                tdestcomp.InnerHtml = cdt
                'lblcomp.Value = tend
                Try
                    dtyp = Request.QueryString("dtyp").ToString
                Catch ex As Exception
                    dtyp = "0"
                End Try
                If dtyp = "t" Then
                    tdschedstart.InnerHtml = "Target Start Date"
                    tdschedcomp.InnerHtml = "Target Complete Date"
                ElseIf dtyp = "s" Then
                    tdschedstart.InnerHtml = "Scheduled Start Date"
                    tdschedcomp.InnerHtml = "Scheduled Complete Date"
                ElseIf dtyp = "a" Then
                    tdschedstart.InnerHtml = "Actual Start Date"
                    tdschedcomp.InnerHtml = "Actual Complete Date"

                End If
            End If
        End If
    End Sub
    Private Sub CheckWoAssign(ByVal sdate As Date, ByVal edate As Date)
        wo = lblwo.Value
        sql = "usp_checkwoassign '" & sdate & "','" & edate & "','" & wo & "'"
        cal.Update(sql)
    End Sub
    Private Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        lbldate.Value = Calendar1.SelectedDate
        lblretday.Value = Calendar1.SelectedDate.DayOfWeek
        lblch.Value = "1"
        Dim rdate As String = Calendar1.SelectedDate
        typ = lbltyp.Value
        who = lblwho.Value
        chk = lblchk.Value
        wo = lblwo.Value
        cdt = lblodt.Value
        sdt = lblsdt.Value

        If who = "estart" Then
            who = "sstart"
        End If
        If who = "ecomp" Then
            who = "scomp"
        End If
        If cdt = "" Then
            cdt = "0"
        End If
        If sdt = "" Then
            sdt = "0"
        End If
        Dim csel As Date
        Dim ssel As Date

        Dim sel As Date
        sel = CDate(rdate)

        Dim dtst As Date

        If cdt <> "0" And chk = "no" Then
            csel = CDate(cdt)
            If csel < sel Then
                chk = "yes"
            Else
                chk = "no"
            End If
        End If

        If who = "tcomp" Or who = "scomp" Or who = "acomp" Then
            If sdt <> "0" Then
                ssel = CDate(sdt)
                If ssel > sel Then
                    chk = "yes"
                Else
                    chk = "no"
                End If
            Else
                chk = "yes"
            End If

        End If

        If typ = "wo" Then
            cal.Open()
            If who = "tstart" Then
                If chk = "yes" Then
                    sql = "update workorder set targstartdate = '" & rdate & "', targcompdate = '" & rdate & "' where wonum = '" & wo & "'"
                Else
                    sql = "update workorder set targstartdate = '" & rdate & "' where wonum = '" & wo & "'"
                End If
            ElseIf who = "tcomp" Then
                If chk = "yes" Then
                    sql = "update workorder set targstartdate = '" & rdate & "', targcompdate = '" & rdate & "' where wonum = '" & wo & "'"
                Else
                    sql = "update workorder set targcompdate = '" & rdate & "' where wonum = '" & wo & "'"
                End If
            ElseIf who = "sstart" Then
                If chk = "yes" Then
                    CheckWoAssign(rdate, rdate)
                    sql = "update workorder set schedstart = '" & rdate & "', schedfinish = '" & rdate & "' where wonum = '" & wo & "'"
                Else
                    CheckWoAssign(rdate, csel)
                    sql = "update workorder set schedstart = '" & rdate & "' where wonum = '" & wo & "'"
                End If
            ElseIf who = "scomp" Then
                If chk = "yes" Then
                    CheckWoAssign(rdate, rdate)
                    sql = "update workorder set schedstart = '" & rdate & "', schedfinish = '" & rdate & "' where wonum = '" & wo & "'"
                Else
                    CheckWoAssign(ssel, rdate)
                    sql = "update workorder set schedfinish = '" & rdate & "' where wonum = '" & wo & "'"
                End If
            ElseIf who = "astart" Then
                If chk = "yes" Then
                    sql = "update workorder set actstart = '" & rdate & "', actfinish = '" & rdate & "' where wonum = '" & wo & "'"
                Else
                    sql = "update workorder set actstart = '" & rdate & "' where wonum = '" & wo & "'"
                End If
            ElseIf who = "acomp" Then
                If chk = "yes" Then
                    sql = "update workorder set actstart = '" & rdate & "', actfinish = '" & rdate & "' where wonum = '" & wo & "'"
                Else
                    sql = "update workorder set actfinish = '" & rdate & "' where wonum = '" & wo & "'"
                End If
            End If

            cal.Update(sql)
            cal.Dispose()
            lblret.Value = "return," & chk
        End If

    End Sub
    Private Sub Calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        days = lbldays.Value
        If days <> "no" Then
            e.Day.IsSelectable = False
            Dim daysarr() As String = days.Split(",")
            Dim cday As String
            Dim nday As Integer
            Dim i As Integer
            For i = 0 To daysarr.Length - 1
                cday = daysarr(i)
                Select Case cday
                    Case "M"
                        nday = 1
                    Case "Tu"
                        nday = 2
                    Case "T"
                        nday = 2
                    Case "W"
                        nday = 3
                    Case "Th"
                        nday = 4
                    Case "F"
                        nday = 5
                    Case "Sa"
                        nday = 6
                    Case "Su"
                        nday = 0
                    Case Else
                        nday = -1
                End Select
                If nday <> -1 Then
                    If e.Day.Date.DayOfWeek = nday Then
                        e.Day.IsSelectable = True
                    Else

                    End If
                End If
            Next
        End If
    End Sub


End Class
