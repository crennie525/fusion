

'********************************************************
'*
'********************************************************



Public Class calendar
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dt, days As String
    Dim calday As Integer
    Protected WithEvents lbldays As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretday As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcalday As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Calendar2 As System.Web.UI.WebControls.Calendar
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldate As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblch As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
Try
            lblwho.Value = Request.QueryString("who").ToString
            Try
                dt = Request.QueryString("dt").ToString
                Try
                    calday = Request.QueryString("calday").ToString
                    lblcalday.Value = calday
                Catch ex As Exception
                    lblcalday.Value = -1
                End Try
                Try
                    days = Request.QueryString("days").ToString
                    lbldays.Value = days
                Catch ex As Exception
                    lbldays.Value = "no"
                End Try
                If days = "" Then
                    lbldays.Value = "no"
                End If
                If dt <> "0" Then
                    Dim sel As Date
                    sel = CDate(dt)
                    Calendar2.VisibleDate = sel
                    Calendar2.SelectedDate = sel

                End If
            Catch ex As Exception

            End Try
        Catch ex As Exception
            lblwho.Value = "other"
        End Try

    End Sub

    Private Sub Calendar2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar2.SelectionChanged
        lbldate.Value = Calendar2.SelectedDate
        lblretday.Value = Calendar2.SelectedDate.DayOfWeek
        lblch.Value = "1"
    End Sub

    Private Sub Calendar2_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar2.DayRender

        days = lbldays.Value
        If days <> "no" Then
            e.Day.IsSelectable = False
            Dim daysarr() As String = days.Split(",")
            Dim cday As String
            Dim nday As Integer
            Dim i As Integer
            For i = 0 To daysarr.Length - 1
                cday = daysarr(i)
                Select Case cday
                    Case "M"
                        nday = 1
                    Case "Tu"
                        nday = 2
                    Case "T"
                        nday = 2
                    Case "W"
                        nday = 3
                    Case "Th"
                        nday = 4
                    Case "F"
                        nday = 5
                    Case "Sa"
                        nday = 6
                    Case "Su"
                        nday = 0
                    Case Else
                        nday = -1
                End Select
                If nday <> -1 Then
                    If e.Day.Date.DayOfWeek = nday Then
                        e.Day.IsSelectable = True
                    Else

                    End If
                End If
            Next
        End If

    End Sub
End Class
