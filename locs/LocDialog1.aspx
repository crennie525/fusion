<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LocDialog1.aspx.vb" Inherits="lucy_r12.LocDialog1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>LocDialog1</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" src="../scripts/sessrefdialog.js"></script>
        <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function handleentry() {
			var log = document.getElementById("lbllog").value;
			if(log=="no") {
				window.returnValue = "log";
				window.close();
			}
			else if(log=="noeqid") {
				window.returnValue = "no";
				window.close();
			}
		}
		function handleexit() {
			window.returnValue = "1";
			window.close();
		}
		
    </script>
</head>
<body  bgcolor="white" onload="handleentry();" onunload="handleexit();">
    <form id="form1" method="post" runat="server">
    <iframe id="iff" src="" width="680" height="450" frameborder="no" runat="server">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="no" runat="server" style="z-index: 0">
    </iframe>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
