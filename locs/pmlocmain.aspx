<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmlocmain.aspx.vb" Inherits="lucy_r12.pmlocmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title id="pgtitle">pmlocmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function gotoeq(href) {
            window.location = href;
        }
        function jumpto(id) {
            //alert(id & "," & "jump")
            var usrname = document.getElementById("lbluser").value;
            window.location = "pmlocmain.aspx?jump=yes&lhid=" + id + "&usrname=" + usrname;
        }
        function handleloc(lhid, lid) {
            document.getElementById("lbllhid").value = lhid;
            document.getElementById("lbllid").value = lid;
            document.getElementById("lbltyp").value = "sys";
            document.getElementById("lblsubmit").value = "srch";
            document.getElementById("form1").submit();
        }



        function delloc() {
            var lid = document.getElementById("lbllid").value;
            var childcnt = document.getElementById("lblchildcnt").value;

            if (lid != "") {
                if (childcnt != "0") {
                    alert("This Location has Child Locations and Cannot be Deleted")
                }
                else {
                    var deltask = confirm("Are you sure you want to Delete this Location?");
                    if (deltask == true) {
                        var ddeltask = confirm("If Delete Operation is successful you will be responsible\nfor Re-Assigning or Deleting any Child Locations\n\nDo you want to Continue?");
                        if (ddeltask == true) {
                            document.getElementById("lblsubmit").value = "delloc";
                            document.getElementById("form1").submit();
                        }
                        else {
                            alert("Action Cancelled")
                        }
                    }
                    else {
                        alert("Action Cancelled")
                    }
                }

            }
            else {
                alert("No Location Selected")
            }
        }

        function delpar(loc, lhid) {
            var deltask = confirm("Are you sure you want to Delete this as a Parent Location?");
            if (deltask == true) {
                //alert(loc)
                document.getElementById("dellhid").value = lhid;
                document.getElementById("lbldelpar").value = loc;
                document.getElementById("lblsubmit").value = "delparloc";
                document.getElementById("form1").submit();
            }
            else {
                alert("Action Cancelled")
            }
        }

        function delchild(lhid, loc) {
            var deltask = confirm("Are you sure you want to Delete this as a Child Location?");
            if (deltask == true) {

                document.getElementById("lbldelchild").value = loc;
                document.getElementById("lbldellhid").value = lhid;
                document.getElementById("lblsubmit").value = "delchild";
                document.getElementById("form1").submit();
            }
            else {
                alert("Action Cancelled")
            }
        }

        function checkstat() {
            var chk = document.getElementById("lblstat").value;
            if (chk == "active") {
                var cur = document.getElementById("lblsys").value;
                var pic = document.getElementById("ddsys").value;

            }
        }
        /*
        function lookup(id) {
        alert(id)
        var eReturn = window.showModalDialog("LocDialog.aspx?typ=par", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
        if (eReturn) {
        alert(eReturn)
        var ret = eReturn.split(",");
        document.getElementById("lbllid").value = ret[0];
        document.getElementById("lbltyp").value = ret[1];
        document.getElementById("tdloc" + id).innerHTML = ret[2];
        document.getElementById("tddesc" + id).innerHTML = ret[3];
        document.getElementById("tdtype" + id).innerHTML = ret[4];
			
        document.getElementById("lblrelloc").value = ret[2];
        ro = document.getElementById("lblro").value;
        if(ro!="1") {
        document.getElementById("lblret").value = "par"
        if(document.getElementById("lblref").value == "go") {
        document.getElementById("lblsubmit").value = "addpar";
        document.getElementById("form1").submit();	
        }
        }
        }
        }
        function blookup(id) {
        alert(id)
        var eReturn = window.showModalDialog("LocDialog.aspx?typ=child", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
        if (eReturn) {
        var ret = eReturn.split(",");
        document.getElementById("lbllid").value = ret[0];
        document.getElementById("lbltyp").value = ret[1];
        document.getElementById("tdbloc" + id).innerHTML = ret[2];
        document.getElementById("tdbdesc" + id).innerHTML = ret[3];
        document.getElementById("tdbtype" + id).innerHTML = ret[4];
			
        document.getElementById("lblrelloc").value = ret[2];
        ro = document.getElementById("lblro").value;
        if(ro!="1") {
        document.getElementById("lblret").value = "child"
        if(document.getElementById("lblref").value = "go") {
        document.getElementById("lblsubmit").value = "addchild";
        document.getElementById("form1").submit();	
        }
        }
        }
        }
        */
        function gettab(name) {
            closeall();
            if (name == "tdloc") {
                document.getElementById("tdloc").className = "thdrhov plainlabel";
                document.getElementById("dloc").className = 'view';
                //document.getElementById("ifmain").src="PMMainMan.aspx?date=" + Date();
            }
            else if (name == "tdeq") {
                document.getElementById("tdeq").className = "thdrhov plainlabel";
                document.getElementById("deq").className = 'view';
                lid = document.getElementById("lbllid").value;
                ro = document.getElementById("lblro").value;
                var ustr = document.getElementById("lbluser").value;
                var sid = document.getElementById("lblsid").value;
                //alert(lid)
                if (lid != "") {
                    document.getElementById("ifeq").src = "LocEq.aspx?lid=" + lid + "&ro=" + ro + "&ustr=" + ustr + "&sid=" + sid + "&date=" + Date();
                }

            }
        }
        function closeall() {
            document.getElementById("tdloc").className = "thdr plainlabel";
            document.getElementById("tdeq").className = "thdr plainlabel";
            document.getElementById("dloc").className = 'details';
            document.getElementById("deq").className = 'details';
        }

        function addloc() {
            //alert("Add Locations Disabled in Demo Mode")
            var usrname = document.getElementById("lbluser").value;
            if (document.getElementById("lblref").value == "go") {
                window.location = "pmlocmain.aspx?usrname=" + usrname
            }
            else {
                var loc = document.getElementById("txtloc").value;
                var locdesc = document.getElementById("txtlocdesc").value;
                var sys = document.getElementById("ddsys").value;
                var typ = document.getElementById("ddtype").value;
                if (loc == "") {
                    alert("Location Required")
                }
                else if (sys == "Select System") {
                    alert("System Required")
                }
                else if (typ == "Select Type") {
                    alert("Type Required")
                }
                else {
                    document.getElementById("lblsubmit").value = "add";
                    document.getElementById("form1").submit();
                }
            }
        }
        function savloc() {
            var typ = document.getElementById("ddtype").value;
            var eqid = document.getElementById("lbleqid").value;
            if (typ == "EQUIPMENT" && eqid == "") {
                alert("Cannot Save Location as Equipment")
            }
            else {
                var lid = document.getElementById("lbllid").value;
                var loc = document.getElementById("txtloc").value;
                if (lid != "" && loc != "") {
                    document.getElementById("lblsubmit").value = "savloc";
                    document.getElementById("form1").submit();
                }
                else {
                    if (lid == "") {
                        alert("No Location Selected")
                    }
                    else if (loc == "") {
                        alert("Location Name Required")
                    }

                }
            }
        }
        function srchloc() {
            /*
            var eReturn = window.showModalDialog("LocDialog.aspx?typ=loc", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
            var ret = eReturn.split(",");
            document.getElementById("lbllhid").value = ret[0];
            document.getElementById("lbltyp").value = ret[1];
            document.getElementById("lblsubmit").value = "srch";
            document.getElementById("form1").submit();
				
            }
            */
            //alert("Temporarily Disabled")

            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lu"
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lu&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                var ret = eReturn.split("~");
                var eqret = ret[3];

                //if (eqret != "") {
                //document.getElementById("lbllid").value = eqret;
                //}
                //else {
                document.getElementById("lbllid").value = ret[0];
                //}
                //alert(eqret + ", eqret")
                document.getElementById("lblsubmit").value = "srch";
                document.getElementById("form1").submit();
            }

        }
        function lookup(id) {
            //alert(id)
            var cbt = document.getElementById("cbtop");
            var eqid = document.getElementById("lbleqid").value;
            var eqparcnt = document.getElementById("lbleqparcnt").value;
            var eqparloccnt = document.getElementById("lbleqparloccnt").value;
            //alert(eqparcnt + "," + eqparloccnt)
            if (cbt.checked == false) {
                var sid = document.getElementById("lblsid").value;
                var wo = "";
                var typ = "lu"
                var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lul&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
                if (eReturn) {
                    //alert(eReturn)
                    var ret = eReturn.split("~");
                    //document.getElementById("lbllid").value = ret[0];
                    //document.getElementById("lbltyp").value = ret[1];
                    //document.getElementById("tdloc" + id).innerHTML = ret[2];
                    //document.getElementById("tddesc" + id).innerHTML = ret[3];
                    //document.getElementById("tdtype" + id).innerHTML = document.getElementById("lbltyp").value;
                    var eqret = ret[3];
                    document.getElementById("lblrelloc").value = ret[1];
                    //alert(ret[1])
                    if (eqid != "" && eqparcnt != "0" && eqret != "") {
                        //need confirm for switch for eq parent
                        //DISABLE --limit to equipment tab
                        //alert("eq has eq parent")
                    }
                    else if (eqid != "" && eqparloccnt != "0" && eqret == "") {
                        //need confirm for switch for location parent
                        //alert("eq has loc parent")
                        var switchloc = confirm("Change Location for this Equipment Record?");
                        if (switchloc == true) {
                            document.getElementById("lblsubmit").value = "switchloc";
                            document.getElementById("form1").submit();
                        }
                        else {
                            document.getElementById("lblrelloc").value = "";
                            alert("Action Cancelled")
                        }
                    }
                    else {
                        if (eqid == "" && eqret != "") {
                            document.getElementById("lblrelloc").value = "";
                            alert("Cannot Add a Location to an Equipment Record")
                        }
                        else {
                            ro = document.getElementById("lblro").value;
                            if (ro != "1") {
                                document.getElementById("lblret").value = "par"
                                if (document.getElementById("lblref").value == "go") {
                                    document.getElementById("lblsubmit").value = "addpar";
                                    document.getElementById("form1").submit();
                                }
                            }
                        }
                    }  
                }
            }
            else {
                alert("Cannot Add Parent to a Top Level Location")
            }
        }
        function blookup(id) {
            var chk = document.getElementById("lbllhid").value;
            var istop = document.getElementById("lbltop").value;
            if (chk != "" && istop == "A") {
                alert("Cannot Add a Parent to a Top Level Location")
            }
            else {
                var eqid = document.getElementById("lbleqid").value;
                var sid = document.getElementById("lblsid").value;
                var wo = "";
                var typ = "lu"
                var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lul&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn.split("~");
                    //document.getElementById("lbllid").value = ret[0];
                    //document.getElementById("lbltyp").value = ret[1];
                    //document.getElementById("tdbloc" + id).innerHTML = ret[2];
                    //document.getElementById("tdbdesc" + id).innerHTML = ret[3];
                    //document.getElementById("tdbtype" + id).innerHTML = ret[4];
                    var eqret = ret[3];
                    document.getElementById("lblrelloc").value = ret[1];
                    ro = document.getElementById("lblro").value;
                    if (ro != "1") {
                        if (eqid != "" && eqret == "") {
                            alert("Cannot Add a Location to an Equipment Record")
                        }
                        else {
                            document.getElementById("lblret").value = "child"
                            if (document.getElementById("lblref").value = "go") {
                                document.getElementById("lblsubmit").value = "addchild";
                                document.getElementById("form1").submit();
                            }
                        }
                    }
                }
            }
        }
        function checktop() {
            var chk = document.getElementById("lbllhid").value;
            var cb = document.getElementById("cbtop");
            if (chk != "") {
                if (cb.checked == true) {
                    document.getElementById("lbltop").value = "Y";
                }
                else {
                    document.getElementById("lbltop").value = "";
                }
            }
            else {
                alert("No Location Selected")
            }
        }
        function resetaloc() {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("../locs/locresdialog.aspx?sid=" + sid, "", "dialogHeight:550px; dialogWidth:520px; resizable=yes");
            if (eReturn) {
                //var ret = eReturn.split("~");
                document.getElementById("lbllhid").value = eReturn;
                document.getElementById("lblsubmit").value = "srch1";
                document.getElementById("form1").submit();
            }
        }
        function checktype(typ) {
            //alert(typ)
            if (typ == "EQUIPMENT") {
                document.getElementById("imgadd").disabled = true;
                document.getElementById("imgadd").src = "../images/appbuttons/minibuttons/addnewdis.gif";
                document.getElementById("Img2").disabled = true;
                document.getElementById("Img2").src = "../images/appbuttons/minibuttons/deldis.gif";

            }
            else {
                document.getElementById("imgadd").disabled = false;
                document.getElementById("imgadd").src = "../images/appbuttons/minibuttons/addnew.gif";
                document.getElementById("Img2").disabled = false;
                document.getElementById("Img2").src = "../images/appbuttons/minibuttons/del.gif";
            }
        }
        //style="z-index: 1; position: absolute; top: 81px; left: 750px"
    </script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table cellspacing="0" cellpadding="0" width="263" class="details">
        <tr>
            <td class="thdrsinglft" align="left" width="26">
                <img src="../images/appbuttons/minibuttons/lodhdr.gif" border="0">
            </td>
            <td class="thdrsingrt label" width="237">
                <asp:Label ID="lang3057" runat="server">Location Hierarchy</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <iframe id="ifarch" style="border-bottom-style: none; padding-bottom: 0px; border-right-style: none;
                    margin: 0px; padding-left: 0px; padding-right: 0px; border-top-style: none; border-left-style: none;
                    padding-top: 0px" src="LocArch.aspx?start=no&amp;sys=PRIMARY" frameborder="no"
                    width="263" scrolling="yes" height="480" runat="server" allowtransparency></iframe>
            </td>
        </tr>
    </table>
    <table style="z-index: 1; position: absolute; top: 81px; left: 7px" cellspacing="0"
        cellpadding="0" width="680">
        <tr>
            <td class="thdrsinglft" align="left" width="26" height="20">
                <img src="../images/appbuttons/minibuttons/lodhdr.gif" border="0" height="19" width="19">
            </td>
            <td class="thdrsingrt label" width="654">
                <asp:Label ID="lang3058" runat="server">Add/Edit Locations</asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <table cellpadding="0" width="680" border="0">
                    <tr id="deptdiv" runat="server">
                        <td class="label" width="92">
                            <asp:Label ID="lang3059" runat="server">Location</asp:Label>
                        </td>
                        <td width="180">
                            <asp:TextBox ID="txtloc" runat="server" Width="180px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td width="22">
                            <img id="imgadd" onclick="addloc();" src="../images/appbuttons/minibuttons/addnew.gif"
                                runat="server">
                        </td>
                        <td width="22">
                            <img id="imgsrch" onclick="srchloc();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                runat="server">
                        </td>
                        <td width="22">
                            <img id="Img1" onclick="savloc();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                runat="server">
                        </td>
                        <td width="22">
                            <img id="Img2" onclick="delloc();" src="../images/appbuttons/minibuttons/del.gif"
                                runat="server">
                        </td>
                        <td width="320" class="bluelabel">
                            <input type="checkbox" id="cbtop" runat="server" onclick="checktop();" />Location
                            is Top Level
                        </td>
                    </tr>
                    <tr id="Tr2" runat="server">
                        <td class="label" width="92">
                            <asp:Label ID="lang3060" runat="server">Description</asp:Label>
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="txtlocdesc" runat="server" Width="260px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <a href="#" class="A1" onclick="resetaloc();">Reset a Location</a>
                        </td>
                    </tr>
                    <tr id="celldiv" runat="server">
                        <td class="label">
                            <asp:Label ID="lang3061" runat="server">System</asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddsys" runat="server" Width="180px" CssClass="plainlabel">
                            </asp:DropDownList>
                        </td>
                        <td colspan="5">
                            &nbsp;<asp:Label ID="lblsysdesc" runat="server" CssClass="label" Font-Bold="True"
                                Font-Names="Arial" Font-Size="X-Small"></asp:Label>
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server">
                        <td class="label">
                            <asp:Label ID="lang3062" runat="server">Type</asp:Label>
                        </td>
                        <td colspan="6">
                            <asp:DropDownList ID="ddtype" runat="server" Width="180px" CssClass="plainlabel">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <table>
                                <tr>
                                    <td class="thdrhov plainlabel" id="tdloc" onclick="gettab('tdloc');" width="100">
                                        <asp:Label ID="lang3063" runat="server">Location Details</asp:Label>
                                    </td>
                                    <td class="thdr plainlabel" id="tdeq" onclick="gettab('tdeq');" width="100">
                                        <asp:Label ID="lang3064" runat="server">Equipment</asp:Label>
                                    </td>
                                    <td class="details" id="tdrt" onclick="gettab('tdrt');" width="100">
                                        <asp:Label ID="lang3065" runat="server">Routes</asp:Label>
                                    </td>
                                    <td width="380">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" valign="top">
                            <div id="dloc" style="border-bottom: 2px groove; border-left: 1px groove; width: 680px;
                                height: 380px; border-top: 1px groove; border-right: 2px groove;">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <img src="../images/appbuttons/minibuttons/4PX.gif">
                                        </td>
                                    </tr>
                                    <tr height="20">
                                        <td class="bluelabel">
                                            <asp:Label ID="lang3066" runat="server">Belongs To:</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdbelongs" runat="server">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabel">
                                            <asp:Label ID="lang3067" runat="server">Child Locations:</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdchildren" runat="server">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="plainlabelred" align="center">
                                            <asp:Label ID="lang3068" runat="server">The PM Location Feature is included as a convenience for PM data transfer to certain CMMS systems.</asp:Label><br>
                                            <asp:Label ID="lang3069" runat="server">Locations Are Not Required for standard Fusion Operations.</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="details" id="deq" style="border-bottom: 1px groove; border-left: 1px groove;
                                border-top: 1px groove; border-right: 1px groove;">
                                <iframe id="ifeq" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                                    border-top-style: none; border-left-style: none" src="#" frameborder="no" width="680"
                                    height="380" runat="server" allowtransparency></iframe>
                            </div>
                            <div class="details" id="drt" style="border-bottom: 1px groove; border-left: 1px groove;
                                border-top: 1px groove; top: BORDER-RIGHT: 1px groove;">
                                <iframe id="ifrt" style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                                    border-top-style: none; border-left-style: none" src="#" frameborder="no" width="680"
                                    height="380" runat="server" allowtransparency></iframe>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" runat="server"><input id="lblsubmit" type="hidden"
        runat="server">
    <input id="lblsid" type="hidden" runat="server"><input id="lblrelloc" type="hidden"
        runat="server">
    <input id="lbltyp" type="hidden" runat="server"><input id="lbllid" type="hidden"
        runat="server">
    <input id="lblret" type="hidden" runat="server"><input id="lblref" type="hidden"
        runat="server">
    <input type="hidden" id="lblstat" runat="server"><input type="hidden" id="lblsys"
        runat="server">
    <input type="hidden" id="lblro" runat="server"><input type="hidden" id="lbllhid"
        runat="server">
    <input type="hidden" id="lbllocsav" runat="server"><input type="hidden" id="lbldelpar"
        runat="server">
    <input type="hidden" id="lbldelchild" runat="server">
    <input type="hidden" id="lbldellhid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbltop" runat="server" />
    <input type="hidden" id="lbluser" runat="server" />
    <input type="hidden" id="lblchildcnt" runat="server" />
    <input type="hidden" id="dellhid" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleqparcnt" runat="server" />
    <input type="hidden" id="lbleqparloccnt" runat="server" />
    <input type="hidden" id="lblswitchloc" runat="server" />
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
