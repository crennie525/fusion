<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LocArch.aspx.vb" Inherits="lucy_r12.LocArch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LocArch</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/LocArchaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 100; POSITION: absolute; TOP: 0px" cellSpacing="0" cellPadding="0">
				<tr class="details" id="tdro" runat="server" height="20">
					<td class="plainlabelblue" align="center"><asp:Label id="lang3038" runat="server">Reference Only</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3039" runat="server">System:</asp:Label><asp:DropDownList id="ddsys" runat="server" CssClass="plainlabel" AutoPostBack="True"></asp:DropDownList></td>
				</tr>
				<tr>
					<td><hr size="1" color="gray">
					</td>
				</tr>
				<tr>
					<td id="tdarch" runat="server" class="plainlabelblue"></td>
				</tr>
			</table>
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"> <input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
