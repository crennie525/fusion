

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class LocArch
    Inherits System.Web.UI.Page
	Protected WithEvents lang3039 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3038 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim main As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ddsys As System.Web.UI.WebControls.DropDownList
    Dim sid, cid, sys, typ, ro, appstr, jsys, start As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdro As System.Web.UI.HtmlControls.HtmlTableRow
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            'If Request.QueryString("start").ToString = "yes" Then
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            cid = HttpContext.Current.Session("comp").ToString
           
            appstr = HttpContext.Current.Session("appstr").ToString()
            Dim apparr() As String = appstr.Split(",")
            'eq,dev,opt,inv
            If appstr <> "all" Then
                Dim i, j As Integer
                j = 0
                For i = 0 To apparr.Length - 1
                    If apparr(i) = "loc" Then
                        j = 1
                    End If
                Next
                If j = 0 Then
                    ro = "1"
                    tdro.Attributes.Add("class", "view")
                End If
            Else
                ro = "0"
            End If
            lblro.Value = ro
            main.Open()
            PopSys(cid)
            jsys = Request.QueryString("sys").ToString
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                ddsys.SelectedValue = jsys '"PRIMARY"
                typ = jsys '"PRIMARY"
                sys = "sys"
                GetArch(sys, typ, ro)
            Else
                tdarch.InnerHtml = "Waiting for location..."
            End If
            main.Dispose()
        End If
    End Sub
    Private Sub PopSys(ByVal cid As String)
        sql = "select * from pmlocsys where compid = '" & cid & "'"
        dr = main.GetRdrData(sql)
        ddsys.DataSource = dr
        ddsys.DataTextField = "systemid"
        ddsys.DataValueField = "systemid"
        ddsys.DataBind()
        dr.Close()
        'ddsys.Items.Insert(0, "Select System")
    End Sub
    Private Sub GetArch(ByVal typ As String, ByVal sys As String, ByVal ro As String)
        Dim sb As StringBuilder = New StringBuilder
        sid = lblsid.Value
        Dim eqnum, eqdesc As String
        sb.Append("<table cellspacing=""0"" border=""0"" bgcolor=""transparent""><tr>")
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""5610""></tr>" & vbCrLf)



        Dim lid, lhid, hold, stat, loc1, loc2, loc3, loc4, loc5, loc6, loc7, desc, chi, chin, ret, loctype As String

        If typ = "sys" Then
            sql = "usp_getlocheir1 '" & sys & "'"
            dr = main.GetRdrData(sql)
            While dr.Read
                lhid = dr.Item("lhid").ToString
                lid = dr.Item("lid").ToString
                hold = dr.Item("hold").ToString
                stat = dr.Item("stat").ToString
                chi = dr.Item("haschild").ToString
                chin = dr.Item("childnum").ToString
                loc1 = dr.Item("location1").ToString
                loc2 = dr.Item("location2").ToString
                loc3 = dr.Item("location3").ToString
                loc4 = dr.Item("location4").ToString
                loc5 = dr.Item("location5").ToString
                loc6 = dr.Item("location6").ToString
                loc7 = dr.Item("location7").ToString
                desc = dr.Item("description").ToString
                loctype = dr.Item("loctype").ToString

                Select Case stat
                    Case "start"
                        If ro = "1" Then
                            sb.Append("<tr><td colspan=""8""><a href=""#"" class=""bluelabelu"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<tr><td colspan=""8""><a href=""#"" class=""bluelabelu"" onclick=""gotoloc('" & lhid & "','" & lid & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "main"
                        sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        'If ret = "loc" Then
                        'sb.Append("<td colspan=""7""><a href=""#"" class=""linklabel"" onclick=""gotoloc('" & lhid & "', '" & loctype & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        'Else
                        If ro = "1" Then
                            sb.Append("<td colspan=""7""><a href=""#"" class=""bluelabelu"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""7""><a href=""#"" class=""linklabel"" onclick=""gotoloc('" & lhid & "','" & lid & "','" & loc1 & "', '" & desc & "', '" & loctype & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                        'End If


                    Case "level1"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        'If ret = "loc" Then
                        'sb.Append("<td colspan=""6""><a href=""#"" class=""linklabelblk"" onclick=""gotoloc('" & lhid & "');"">" & loc2 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        'Else
                        If ro = "1" Then
                            sb.Append("<td colspan=""6""><a href=""#"" class=""linklabelblk"">" & loc2 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""6""><a href=""#"" class=""linklabelblk"" onclick=""gotoloc('" & lhid & "','" & lid & "','" & loc2 & "', '" & desc & "', '" & loctype & "');"">" & loc2 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                        'End If


                    Case "level2"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        'If ret = "loc" Then
                        'sb.Append("<td colspan=""5""><a href=""#"" class=""linklabel"" onclick=""gotoloc('" & lhid & "');"">" & loc3 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        'Else
                        If ro = "1" Then
                            sb.Append("<td colspan=""5""><a href=""#"" class=""linklabel"" >" & loc3 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""5""><a href=""#"" class=""linklabel"" onclick=""gotoloc('" & lhid & "','" & lid & "','" & loc3 & "', '" & desc & "', '" & loctype & "');"">" & loc3 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                        'End If

                    Case "level3"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        'If ret = "loc" Then
                        'sb.Append("<td colspan=""4""><a href=""#"" class=""linklabelblk"" onclick=""gotoloc('" & lhid & "');"">" & loc4 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        'Else
                        If ro = "1" Then
                            sb.Append("<td colspan=""4""><a href=""#"" class=""linklabelblk"">" & loc4 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""4""><a href=""#"" class=""linklabelblk"" onclick=""gotoloc('" & lhid & "','" & lid & "','" & loc4 & "', '" & desc & "', '" & loctype & "');"">" & loc4 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                        'End If

                    Case "level4"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        'If ret = "loc" Then
                        'sb.Append("<td colspan=""3""><a href=""#"" class=""linklabel"" onclick=""gotoloc('" & lhid & "');"">" & loc5 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        'Else
                        If ro = "1" Then
                            sb.Append("<td colspan=""3""><a href=""#"" class=""linklabel"">" & loc5 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""3""><a href=""#"" class=""linklabel"" onclick=""gotoloc('" & lhid & "','" & lid & "','" & loc5 & "', '" & desc & "', '" & loctype & "');"">" & loc5 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                        'End If

                    Case "level5"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        'If ret = "loc" Then
                        'sb.Append("<td colspan=""2""><a href=""#"" class=""linklabelblk"" onclick=""gotoloc('" & lhid & "');"">" & loc6 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        'Else
                        If ro = "1" Then
                            sb.Append("<td colspan=""2""><a href=""#"" class=""linklabelblk"">" & loc6 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""2""><a href=""#"" class=""linklabelblk"" onclick=""gotoloc('" & lhid & "','" & lid & "','" & loc6 & "', '" & desc & "', '" & loctype & "');"">" & loc6 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                        'End If

                    Case "level6"
                        'If ret = "loc" Then
                        'sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td><img src=""../images/appbuttons/bgbuttons/minus.gif""></td><td colspan=""1""><a href=""#"" class=""linklabel"" onclick=""gotoloc('" & lhid & "');"">" & loc7 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        'Else
                        If ro = "1" Then
                            sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td><img src=""../images/appbuttons/bgbuttons/minus.gif""></td><td colspan=""1""><a href=""#"" class=""linklabel"" >" & loc7 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td><img src=""../images/appbuttons/bgbuttons/minus.gif""></td><td colspan=""1""><a href=""#"" class=""linklabel"" onclick=""gotoloc('" & lhid & "', '" & loc7 & "', '" & desc & "', '" & loctype & "');"">" & loc7 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                        'End If

                End Select
            End While
            dr.Close()

        Else
            dr = main.GetRdrData(sql)
            Dim locid As String
            While dr.Read
                locid = dr.Item("locid").ToString
                loc1 = dr.Item("location").ToString
                desc = dr.Item("description").ToString
                If ret = "loc" Then
                    sb.Append("<tr><td colspan=""8""><a href=""#"" class=""bluelabelu"" onclick=""gotoloc('" & locid & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                Else
                    sb.Append("<tr><td colspan=""8""><a href=""#"" class=""bluelabelu"" onclick=""gotoloc('" & locid & "', '" & loc1 & "', '" & desc & "', '" & loctype & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                End If

            End While
            dr.Close()
        End If



        sb.Append("</table></div>" & vbCrLf)
        tdarch.InnerHtml = sb.ToString

    End Sub

    Private Sub ddsys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsys.SelectedIndexChanged
        Dim sys As String = ddsys.SelectedValue
        main.Open()
        ro = lblro.Value
        GetArch("sys", sys, ro)
        main.Dispose()

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3038.Text = axlabs.GetASPXPage("LocArch.aspx", "lang3038")
        Catch ex As Exception
        End Try
        Try
            lang3039.Text = axlabs.GetASPXPage("LocArch.aspx", "lang3039")
        Catch ex As Exception
        End Try

    End Sub

End Class
