

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class LocDets
    Inherits System.Web.UI.Page
	Protected WithEvents lang3043 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3042 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3041 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3040 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim loc As String
    Dim sql As String
    Dim lcon As New Utilities
    Protected WithEvents tdbelongs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdchildren As System.Web.UI.HtmlControls.HtmlTableCell
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            loc = "2" 'Request.QueryString("loc").ToString
            lblloc.Value = loc
            lcon.Open()
            GetBelongs(loc)
            GetChildren(loc)
            lcon.Dispose()
        End If
        

    End Sub
    Private Sub GetBelongs(ByVal loc As String)
        Dim locstr, desc, typ, locid As String
        Dim img As String = "../images/appbuttons/minibuttons/magnifier.gif"
        Dim img1 As String = "../images/appbuttons/minibuttons/2PX.gif"
        Dim msg As String = "onmouseover = ""return overlib('" & tmod.getov("cov299" , "LocDets.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim msg1 As String = "onmouseover = ""return overlib('" & tmod.getov("cov300" , "LocDets.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim simg As String = "<img src=""" & img & """ onclick=""lookup();""" & msg & ">"
        Dim spimg As String = "<img src=""" & img & """ onclick=""jumpto('" & locid & "');""" & msg1 & ">" 'locstr

        Dim sb As New StringBuilder
        sb.Append("<table width=""670"" cellpadding=""0"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""150"">" & tmod.getlbl("cdlbl583" , "LocDets.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=""../images/appbuttons/minibuttons/magnifier.gif""></td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""330"">" & tmod.getlbl("cdlbl584" , "LocDets.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""150"">" & tmod.getlbl("cdlbl585" , "LocDets.aspx.vb") & "</td>")
        sb.Append("<td width=""20"">&nbsp;</td></tr>")

        sb.Append("<tr><td colspan=""5"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 662px;  HEIGHT: 110px"">")
        sb.Append("<table ><tr><td width=""148""><td width=""22""><td width=""328""><td width=""148""></tr>")

        sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
        sb.Append("<td class=""plainlabel"">" & simg & "</td>")
        sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")

        sql = "select h.*, l.locid from pmlocheir h left join pmlocations l on l.location = h.location where h.location = '" & loc & "' and h.parent is not null"
        dr = lcon.GetRdrData(sql)
        While dr.Read
            locstr = dr.Item("location").ToString
            desc = dr.Item("description").ToString
            typ = dr.Item("type").ToString
            sb.Append("<tr><td class=""plainlabel grayborder"">" & locstr & "</td>")
            sb.Append("<td class=""plainlabel"">" & spimg & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & desc & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & typ & "</td></tr>")
        End While

        dr.Close()

        Dim i As Integer
        For i = 0 To 5
            sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel"">" & simg & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
        Next
        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdbelongs.InnerHtml = sb.ToString

    End Sub
    Private Sub GetChildren(ByVal loc As String)
        Dim locstr, desc, typ, locid As String
        Dim img As String = "../images/appbuttons/minibuttons/magnifier.gif"
        Dim img1 As String = "../images/appbuttons/minibuttons/2PX.gif"
        Dim msg As String = "onmouseover = ""return overlib('" & tmod.getov("cov301" , "LocDets.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim msg1 As String = "onmouseover = ""return overlib('" & tmod.getov("cov302" , "LocDets.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim simg As String = "<img src=""" & img & """ onclick=""lookup();""" & msg & ">"
        Dim spimg As String = "<img src=""" & img & """ onclick=""jumpto('" & locid & "');""" & msg1 & ">" 'locstr

        Dim sb As New StringBuilder
        sb.Append("<table width=""670"" cellpadding=""0"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""150"">" & tmod.getlbl("cdlbl586" , "LocDets.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=""../images/appbuttons/minibuttons/magnifier.gif""></td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""330"">" & tmod.getlbl("cdlbl587" , "LocDets.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""150"">" & tmod.getlbl("cdlbl588" , "LocDets.aspx.vb") & "</td>")
        sb.Append("<td width=""20"">&nbsp;</td></tr>")

        sb.Append("<tr><td colspan=""5"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 662px;  HEIGHT: 110px"">")
        sb.Append("<table ><tr><td width=""148""><td width=""22""><td width=""328""><td width=""148""></tr>")

        sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
        sb.Append("<td class=""plainlabel"">" & simg & "</td>")
        sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
        sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")

        sql = "select h.*, l.locid from pmlocheir h left join pmlocations l on l.loction = h.location where h.parent = '" & loc & "'"
        dr = lcon.GetRdrData(sql)
        While dr.Read
            locstr = dr.Item("location").ToString
            desc = dr.Item("description").ToString
            typ = dr.Item("type").ToString
            sb.Append("<tr><td class=""plainlabel grayborder"">" & locstr & "</td>")
            sb.Append("<td class=""plainlabel"">" & spimg & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & desc & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & typ & "</td></tr>")
        End While

        dr.Close()

        Dim i As Integer
        For i = 0 To 5
            sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel"">" & simg & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
        Next
        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdchildren.InnerHtml = sb.ToString

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3040.Text = axlabs.GetASPXPage("LocDets.aspx", "lang3040")
        Catch ex As Exception
        End Try
        Try
            lang3041.Text = axlabs.GetASPXPage("LocDets.aspx", "lang3041")
        Catch ex As Exception
        End Try
        Try
            lang3042.Text = axlabs.GetASPXPage("LocDets.aspx", "lang3042")
        Catch ex As Exception
        End Try
        Try
            lang3043.Text = axlabs.GetASPXPage("LocDets.aspx", "lang3043")
        Catch ex As Exception
        End Try

    End Sub

End Class
