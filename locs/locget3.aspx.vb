Imports System.Data.SqlClient
Imports System.Text
Public Class locget3
    Inherits System.Web.UI.Page
    Dim ec As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim sid, did, dept, cid, cell, eqid, eq, fuid, fu, comid, comp, lid, loc, ncid, nc, sys, lev, lidi, lidid As String
    Dim filt, dt, val, Filter, wonum, typ, who, jpid, eqdesc, fpc As String
    Dim rlid As String
    Dim retloc As String
    Protected WithEvents ddsys As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbl1 As System.Web.UI.WebControls.Label
    Protected WithEvents divfu As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliseq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmisc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents divco As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents divmisc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblhold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblholdi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloctyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocsys As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappver As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrchtyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqdesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqchild As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdtab As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec7 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec8 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec9 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec10 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec11 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec12 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec13 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec14 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec15 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec16 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec17 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec18 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec19 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec20 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec21 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec22 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec23 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec24 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdec25 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd9 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd18 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd10 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd19 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd11 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd20 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd12 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd21 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd13 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd22 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd14 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd23 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd15 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd24 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd7 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd16 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd25 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd8 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd17 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trsave As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsys As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltabs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid4 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc4 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid5 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc5 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid6 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc6 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid7 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc7 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid8 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc8 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid9 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc9 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid10 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc10 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid11 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc11 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid12 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc12 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid13 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc13 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid14 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc14 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid15 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc15 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid16 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc16 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid17 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc17 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid18 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc18 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid19 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc19 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid20 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc20 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid21 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc21 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid22 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc22 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid23 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc23 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid24 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc24 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid25 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc25 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllevel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretlid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretlidi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi4 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi5 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi6 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi7 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi8 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi9 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi10 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi11 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi12 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi13 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi14 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi15 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi16 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi17 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi18 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi19 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi20 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi21 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi22 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi23 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi24 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllidi25 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfpc As System.Web.UI.HtmlControls.HtmlInputHidden
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            typ = Request.QueryString("typ").ToString '"locs" '
            If typ = "wo" Or typ = "woret" Then
                wonum = Request.QueryString("wo").ToString
            ElseIf typ = "jp" Or typ = "jpret" Then
                jpid = Request.QueryString("jpid").ToString
            End If
            lblsid.Value = sid
            lblwonum.Value = wonum
            lbljpid.Value = jpid
            If typ = "woret" Then
                lbltyp.Value = "wo"
            ElseIf typ = "jpret" Then
                lbltyp.Value = "jp"
            Else
                lbltyp.Value = typ
            End If

            If typ = "lup" Then
                'tdmsg.Attributes.Add("class", "view")
                tdmsg.InnerHtml = "Equipment# Only Required for Return"
            ElseIf typ = "lul" Then
                'tdmsg.Attributes.Add("class", "view")
                tdmsg.InnerHtml = "Location# Only Required for Return"
            End If
            ec.Open()
            'need dup check here
            dup_chk(sid)
            PopSys()
            PopTypes()
            lbllocsys.Value = "PRIMARY"
            ddsys.SelectedValue = "PRIMARY"
            Dim mm1 As New mmenu_utils_a
            Dim appver As String = mm1.AppVer
            If appver = "2" Then
                lblloctyp.Value = "POSITION"
                ddtype.SelectedValue = "POSITION"
            End If

            GetL1()
           

            If typ = "retloc" Or typ = "woret" Or typ = "jpret" Then

                rlid = Request.QueryString("rlid").ToString
                lblretlidi.Value = rlid
                sql = "usp_lookup_locs_ret '" & rlid & "'"
                Try
                    retloc = ec.strScalar(sql)
                    Dim retlocarr() As String = retloc.Split(",")
                    Dim retlen As Integer = retlocarr.Length - 1
                    Dim retlev As String
                    Dim retcnt As Integer = 1
                    lbllevel.Value = retcnt
                    For i = retlen To 0 Step -1
                        retlev = retlocarr(i)

                        Select Case retcnt
                            Case "1"
                                lbllid.Value = retlev
                            Case "2"
                                lbllid1.Value = retlev
                            Case "3"
                                lbllid2.Value = retlev
                            Case "4"
                                lbllid3.Value = retlev
                            Case "5"
                                lbllid4.Value = retlev
                            Case "6"
                                lbllid4.Value = retlev

                        End Select
                        GetL2(retcnt)
                        retcnt += 1
                        lbllevel.Value = retcnt
                    Next
                    lblretlid.Value = retlev
                    'Dim retlid As String
                    'sql = "select locid from pmlocations where location = '" & retlev & "'"
                    'retlid = ec.strScalar(sql)
                    'lblretlid.Value = retlid
                    eqid = Request.QueryString("eqid").ToString
                    If eqid <> "" Then
                        Dim eqnum As String
                        sql = "select eqnum, eqdesc, fpc from equipment where eqid = '" & eqid & "'"
                        dr = ec.GetRdrData(sql)
                        While dr.Read
                            eqnum = dr.Item("eqnum").ToString
                            eqdesc = dr.Item("eqdesc").ToString
                            fpc = dr.Item("fpc").ToString
                        End While
                        dr.Close()
                        lbleqid.Value = eqid
                        lbleq.Value = eqnum
                        lbleqdesc.Value = eqdesc
                        lblfpc.Value = fpc
                        GetFunctions()
                        FillLevels(1)
                        lbllevel.Value = retcnt
                    End If
                    fuid = Request.QueryString("fuid").ToString
                    If fuid <> "" Then
                        Dim func As String
                        sql = "select func from functions where func_id = '" & fuid & "'"
                        func = ec.strScalar(sql)
                        lblfuid.Value = fuid
                        lblfunc.Value = func
                        GetComponents()
                        FillLevels(1)
                        lbllevel.Value = retcnt
                    End If
                    comid = Request.QueryString("coid").ToString
                    If comid <> "" Then
                        Dim comp As String
                        sql = "select compnum from components where comid = '" & comid & "'"
                        comp = ec.strScalar(sql)
                        lblcoid.Value = comid
                        lblcomp.Value = comp

                        FillLevels(1)
                        lbllevel.Value = retcnt
                    End If

                    tdeq.InnerHtml = lbleq.Value
                    tdfu.InnerHtml = lblfunc.Value
                    tdco.InnerHtml = lblcomp.Value
                    tdmisc.InnerHtml = lblncnum.Value
                Catch ex As Exception

                End Try
                

            End If
            If typ = "ret" Then
                who = Request.QueryString("who").ToString
                eqid = Request.QueryString("eqid").ToString
                eq = Request.QueryString("eq").ToString
                fuid = Request.QueryString("fuid").ToString
                fu = Request.QueryString("fu").ToString
                comid = Request.QueryString("coid").ToString
                comp = Request.QueryString("comp").ToString
                lid = Request.QueryString("lid").ToString
                loc = Request.QueryString("loc").ToString
                lev = Request.QueryString("lev").ToString
                lblsid.Value = sid
                lbleqid.Value = eqid
                lbleq.Value = eq
                lblfuid.Value = fuid
                lblfunc.Value = fu
                lblcoid.Value = comid
                lblcomp.Value = comp
                lbllid.Value = lid
                lblloc.Value = loc
                lbllevel.Value = lev
                If who = "checkl1" Then
                    lblsubmit.Value = ""
                    lev = lbllevel.Value
                    GetL2(lev)
                ElseIf who = "getfunc" Then
                    lblsubmit.Value = ""
                    GetFunctions()
                    FillLevels(1)
                ElseIf who = "getco" Then
                    lblsubmit.Value = ""
                    GetComponents()
                    FillLevels(1)
                End If
            End If
            ec.Dispose()
        Else
            If Request.Form("lblsubmit") = "checkl1" Then
                lblsubmit.Value = ""
                lev = lbllevel.Value
                ec.Open()
                GetL2(lev)
                PopNC()
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "savewo" Then
                lblsubmit.Value = ""
                ec.Open()
                savewo()
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "savejp" Then
                lblsubmit.Value = ""
                ec.Open()
                savejp()
                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "getfunc" Or Request.Form("lblsubmit") = "getfuncmod" Then
                lblsubmit.Value = ""
                lev = lbllevel.Value
                ec.Open()

                If lblsrchtyp.Value <> "eq" Then
                    If Request.Form("lblsubmit") = "getfuncmod" Then
                        GetL2(lev)
                        lbleqchild.Value = "yes"
                    End If
                    GetFunctions()
                    FillLevels(1)
                    If Request.Form("lblsubmit") = "getfuncmod" Then
                        lbleqchild.Value = "no"
                    End If

                Else
                    GetL2(lev)

                    eqid = lbleqid.Value
                    eq = lbleq.Value

                    GetL1()

                    tdecd1.InnerHtml = ""
                    tdecd2.InnerHtml = ""
                    tdecd3.InnerHtml = ""
                    tdecd4.InnerHtml = ""
                    tdecd5.InnerHtml = ""
                    tdecd6.InnerHtml = ""
                    tdecd7.InnerHtml = ""
                    tdecd8.InnerHtml = ""
                    tdecd9.InnerHtml = ""
                    tdecd11.InnerHtml = ""
                    tdecd11.InnerHtml = ""
                    tdecd12.InnerHtml = ""
                    tdecd13.InnerHtml = ""
                    tdecd14.InnerHtml = ""
                    tdecd15.InnerHtml = ""
                    tdecd16.InnerHtml = ""
                    tdecd17.InnerHtml = ""
                    tdecd18.InnerHtml = ""
                    tdecd19.InnerHtml = ""
                    tdecd20.InnerHtml = ""
                    tdecd21.InnerHtml = ""
                    tdecd22.InnerHtml = ""
                    tdecd23.InnerHtml = ""
                    tdecd24.InnerHtml = ""
                    tdecd25.InnerHtml = ""


                    rlid = lblretlidi.Value
                    If rlid = "" Then
                        rlid = "0"
                    End If
                    sql = "usp_lookup_locs_ret '" & rlid & "'"
                    retloc = ec.strScalar(sql)
                    Dim retlocarr() As String = retloc.Split(",")
                    Dim retlen As Integer = retlocarr.Length - 1
                    Dim retlev As String
                    Dim retcnt As Integer = 1
                    lbllevel.Value = retcnt
                    For i = retlen To 0 Step -1
                        retlev = retlocarr(i)

                        Select Case retcnt
                            Case "1"
                                lbllid.Value = retlev
                            Case "2"
                                lbllid1.Value = retlev
                            Case "3"
                                lbllid2.Value = retlev
                            Case "4"
                                lbllid3.Value = retlev
                            Case "5"
                                lbllid4.Value = retlev
                            Case "6"
                                lbllid4.Value = retlev

                        End Select
                        GetL2(retcnt)
                        retcnt += 1
                        lbllevel.Value = retcnt
                    Next
                    lblretlid.Value = retlev
                    lbleqid.Value = eqid
                    lbleq.Value = eq
                    GetFunctions()
                    FillLevels(1)
                    lbllevel.Value = retcnt


                End If

                ec.Dispose()
            ElseIf Request.Form("lblsubmit") = "getco" Then
                lblsubmit.Value = ""
                ec.Open()
                GetComponents()
                FillLevels(1)
                ec.Dispose()
                lblcoid.Value = ""
                lblcomp.Value = ""
                tdco.InnerHtml = ""
            End If
            typ = lbltyp.Value
            If typ = "lup" Then
                'tdmsg.Attributes.Add("class", "view")
                tdmsg.InnerHtml = "Equipment# Only Required for Return"
            ElseIf typ = "lul" Then
                'tdmsg.Attributes.Add("class", "view")
                tdmsg.InnerHtml = "Location# Only Required for Return"
            End If

            tdeq.InnerHtml = lbleq.Value
            tdfu.InnerHtml = lblfunc.Value
            tdco.InnerHtml = lblcomp.Value
            tdmisc.InnerHtml = lblncnum.Value
        End If
    End Sub
    Private Sub dup_chk(ByVal sid As String)
        Dim duploc, dupsys, duplhid, duplocid, replocid, repeqid As String
        Dim dupcnt, retcnt, dupchildcnt, duploccnt As Integer
        retcnt = 0

        sql = "select location, systemid, count(*) as 'dup' from pmlocheir where siteid = '" & sid & "' and parent is null group by location, systemid having count(*) > 1"
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds = ec.GetDSData(sql)
        dt = New DataTable
        dt = ds.Tables(0)
        Dim row As DataRow
        For Each row In dt.Rows
            dupchildcnt = 0
            dupcnt = row("dup").ToString
            duploc = row("location").ToString
            dupsys = row("systemid").ToString
            retcnt += 1
            'check for child locs
            sql = "select count(*) from pmlocheir where location = '" & duploc & "' and systemid = '" & dupsys & "' and haschildren = 'Y' and siteid = '" & sid & "'"
            dupchildcnt = ec.Scalar(sql)
            If dupchildcnt > 0 Then
                'perform reg dup chk - client run
            Else
                While dupcnt > 1
                    sql = "select top 1 lhid from  pmlocheir where location = '" & duploc & "' and systemid = '" & dupsys & "' and siteid = '" & sid & "'"
                    duplhid = ec.strScalar(sql)
                    sql = "delete from pmlocheir where lhid = '" & duplhid & "'"
                    ec.Update(sql)
                    sql = "select count(*) from pmlocheir where location = '" & duploc & "' and systemid = '" & dupsys & "' and siteid = '" & sid & "'"
                    dupcnt = ec.Scalar(sql)
                End While
                'check for dups in pmlocations
                sql = "select count(*) from pmlocations where location = '" & duploc & "' and siteid = '" & sid & "' and eqid is null"
                duploccnt = ec.Scalar(sql)
                If duploccnt > 1 Then
                    While duploccnt > 1
                        sql = "select top 1 locid from pmlocations where location = '" & duploc & "' and siteid = '" & sid & "' and eqid is null"
                        duplocid = ec.strScalar(sql)
                        sql = "delete from pmlocations where locid = '" & duplocid & "'"
                        ec.Update(sql)
                        sql = "select count(*) from pmlocations where location = '" & duploc & "' and siteid = '" & sid & "' and eqid is null"
                        duploccnt = ec.Scalar(sql)
                    End While
                End If
                'equipment requires additional update
                sql = "select count(*) from pmlocations where location = '" & duploc & "' and siteid = '" & sid & "' and eqid is not null"
                duploccnt = ec.Scalar(sql)
                If duploccnt > 1 Then
                    If duploccnt > 1 Then
                        While duploccnt > 1
                            sql = "select top 1 locid from pmlocations where location = '" & duploc & "' and siteid = '" & sid & "' and eqid is not null"
                            duplocid = ec.strScalar(sql)
                            sql = "delete from pmlocations where locid = '" & duplocid & "'"
                            ec.Update(sql)
                            sql = "select count(*) from pmlocations where location = '" & duploc & "' and siteid = '" & sid & "' and eqid is not null"
                            duploccnt = ec.Scalar(sql)
                        End While
                        sql = "select top 1 locid, eqid from pmlocations where location = '" & duploc & "' and siteid = '" & sid & "' and eqid is not null"
                        dr = ec.GetRdrData(sql)
                        While dr.Read
                            replocid = dr.Item("locid").ToString
                            repeqid = dr.Item("eqid").ToString
                        End While
                        dr.Close()
                        sql = "update equipment set locid = '" & replocid & "' where eqid = '" & repeqid & "'"
                        ec.Update(sql)
                    End If
                End If
            End If
        Next
        

       

    End Sub
    Private Sub ddsys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsys.SelectedIndexChanged
        If ddsys.SelectedIndex <> 0 And ddsys.SelectedIndex <> -1 Then
            lbllocsys.Value = ddsys.SelectedValue
            ec.Open()
            GetL1()
            ec.Dispose()
        Else
            lbllocsys.Value = "PRIMARY"
            ddsys.SelectedValue = "PRIMARY"
            GetL1()
        End If
    End Sub
    Private Sub ddtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtype.SelectedIndexChanged
        If ddtype.SelectedIndex <> 0 And ddtype.SelectedIndex <> -1 Then
            lblloctyp.Value = ddtype.SelectedValue
        End If
        ec.Open()
        GetL1()
        ec.Dispose()
    End Sub
    Private Sub savewo()
        lid = lblretlid.Value
        loc = lblretloc.Value
        lidi = lblretlidi.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        fu = lblfunc.Value
        comid = lblcoid.Value
        comp = lblcomp.Value
        wonum = lblwonum.Value
        sid = lblsid.Value
        eq = lbleq.Value
        ncid = lblncid.Value
        nc = lblncnum.Value
        sql = "delete from wofailmodes1 where wonum = '" & wonum & "'"
        ec.Update(sql)
        Dim cmd As New SqlCommand
        cmd.CommandText = "update workorder set siteid = @sid, " _
        + "eqid = @eqid, eqnum = @eq, funcid = @fuid, comid = @comid, " _
        + "locid = @lidi, location = @lid, ncid = @ncid, ncnum = @nc, ld = 'L' where wonum = @wonum"
        Dim param = New SqlParameter("@wonum", SqlDbType.Int)
        param.Value = wonum
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@sid", SqlDbType.VarChar)
        If sid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sid
        End If
        cmd.Parameters.Add(param01)
        Dim param04 = New SqlParameter("@eqid", SqlDbType.VarChar)
        If eqid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = eqid
        End If
        cmd.Parameters.Add(param04)
        Dim param04a = New SqlParameter("@fuid", SqlDbType.VarChar)
        If fuid = "" Then
            param04a.Value = System.DBNull.Value
        Else
            param04a.Value = fuid
        End If
        cmd.Parameters.Add(param04a)
        Dim param04b = New SqlParameter("@comid", SqlDbType.VarChar)
        If comid = "" Then
            param04b.Value = System.DBNull.Value
        Else
            param04b.Value = comid
        End If
        cmd.Parameters.Add(param04b)
        Dim param05 = New SqlParameter("@eq", SqlDbType.VarChar)
        If eq = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = eq
        End If
        cmd.Parameters.Add(param05)

        Dim param10 = New SqlParameter("@lidi", SqlDbType.VarChar)
        If lidi = "" Then
            param10.Value = System.DBNull.Value
        Else
            param10.Value = lidi
        End If
        cmd.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@lid", SqlDbType.VarChar)
        If lid = "" Then
            param11.Value = System.DBNull.Value
        Else
            param11.Value = lid
        End If
        cmd.Parameters.Add(param11)
        Dim param12 = New SqlParameter("@ncid", SqlDbType.VarChar)
        If ncid = "0" Then
            param12.Value = System.DBNull.Value
        Else
            param12.Value = ncid
        End If
        cmd.Parameters.Add(param12)
        Dim param13 = New SqlParameter("@nc", SqlDbType.VarChar)
        If nc = "" Then
            param13.Value = System.DBNull.Value
        Else
            param13.Value = nc
        End If
        cmd.Parameters.Add(param13)

        ec.UpdateHack(cmd)
        lblsubmit.Value = "return"
    End Sub
    Private Sub savejp()
        lid = lblretlid.Value
        loc = lblretloc.Value
        lidi = lblretlidi.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        fu = lblfunc.Value
        comid = lblcoid.Value
        comp = lblcomp.Value
        jpid = lbljpid.Value
        sid = lblsid.Value
        eq = lbleq.Value
        ncid = lblncid.Value
        nc = lblncnum.Value
        Dim cmd As New SqlCommand
        cmd.CommandText = "update pmjobplans set siteid = @sid, " _
        + "eqid = @eqid, funcid = @fuid, comid = @comid, " _
        + "locid = @lidi, ncid = @ncid, ld = 'L' where jpid = @jpid"
        Dim param = New SqlParameter("@jpid", SqlDbType.Int)
        param.Value = jpid
        cmd.Parameters.Add(param)
        Dim param01 = New SqlParameter("@sid", SqlDbType.Int)
        If sid = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = sid
        End If
        cmd.Parameters.Add(param01)
        Dim param04 = New SqlParameter("@eqid", SqlDbType.Int)
        If eqid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = eqid
        End If
        cmd.Parameters.Add(param04)
        Dim param04a = New SqlParameter("@fuid", SqlDbType.Int)
        If fuid = "" Then
            param04a.Value = System.DBNull.Value
        Else
            param04a.Value = fuid
        End If
        cmd.Parameters.Add(param04a)
        Dim param04b = New SqlParameter("@comid", SqlDbType.Int)
        If comid = "" Then
            param04b.Value = System.DBNull.Value
        Else
            param04b.Value = comid
        End If
        cmd.Parameters.Add(param04b)
        

        Dim param10 = New SqlParameter("@lidi", SqlDbType.Int)
        If lidi = "" Then
            param10.Value = System.DBNull.Value
        Else
            param10.Value = lidi
        End If
        cmd.Parameters.Add(param10)

        Dim param11 = New SqlParameter("@ncid", SqlDbType.Int)
        If ncid = "0" Or ncid = "" Then
            param11.Value = System.DBNull.Value
        Else
            param11.Value = ncid
        End If
        cmd.Parameters.Add(param11)
        
        ec.UpdateHack(cmd)
        lblsubmit.Value = "return"
    End Sub
    Private Sub GetFunctions()
        eqid = lbleqid.Value
        Dim fi, fu As String
        Dim sb As New StringBuilder
        'srchtyp = lblsrchtyp.Value

        sql = "select func_id, func from functions where eqid = '" & eqid & "' order by routing"
        dr = ec.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            fi = dr.Item("func_id").ToString
            fu = dr.Item("func").ToString
            sb.Append("<tr><td>")
            'If srchtyp = "nofu" Then
            'sb.Append(fu)
            'sb.Append("<a class=""A1"" href=""#"" onclick=""getdfu('" & fi & "','" & fu & "','down')"">")
            'sb.Append(fu)
            'Else
                sb.Append("<a class=""A1"" href=""#"" onclick=""getdfu('" & fi & "','" & fu & "','down')"">")
                sb.Append(fu)
                sb.Append("</td></tr>")
            'End If
        End While
        dr.Close()
        sb.Append("</table>")
        divfu.InnerHtml = sb.ToString
        lblfuid.Value = ""
        lblfunc.Value = ""
        tdfu.InnerHtml = ""
        divco.InnerHtml = ""
        lblcoid.Value = ""
        lblcomp.Value = ""
        tdco.InnerHtml = ""
    End Sub
    Private Sub GetComponents()

        fuid = lblfuid.Value
        Dim ci, co, cd As String
        Dim sb As New StringBuilder
        'srchtyp = lblsrchtyp.Value

        sql = "select comid, compnum, compdesc from components where func_id = '" & fuid & "' order by crouting"
        dr = ec.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            ci = dr.Item("comid").ToString
            co = dr.Item("compnum").ToString
            cd = dr.Item("compdesc").ToString
            sb.Append("<tr><td class=""plainlabel"">")
            'If srchtyp = "noco" Or srchtyp = "nofu" Then
            'sb.Append(co)
            'Else
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdco('" & ci & "','" & co & "')"">")
            If cd <> "" Then
                sb.Append(co & " - " & cd & "</a>")
            Else
                sb.Append(co & "</a>")
            End If

            'End If

            sb.Append("</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divco.InnerHtml = sb.ToString
        Dim func As String = lblfunc.Value
        tdfu.InnerHtml = func
        Dim eq As String = lbleq.Value
        tdeq.InnerHtml = eq
    End Sub
    Private Sub PopNC()
        Dim lidinc As String
        lidinc = lblretlidi.Value
        sql = "select count(*) from noncritical where locid = '" & lidinc & "'"
        filt = " where locid = '" & lidinc & "'"
        Dim eqcnt As Integer
        eqcnt = ec.Scalar(sql)
        If eqcnt > 0 Then
            sql = "select ncid, ncnum, locid from noncritical " & filt
            dr = ec.GetRdrData(sql)
            Dim sb As New StringBuilder
            sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
            sb.Append("<table width=""230"" cellpadding=""1"">")
            sb.Append("<tr>")
            sb.Append("<td width=""230""></td>")
            sb.Append("</tr>")
            While dr.Read
                ncid = dr.Item("ncid").ToString
                nc = dr.Item("ncnum").ToString
                lid = dr.Item("locid").ToString
                lbllid.Value = lid
                sb.Append("<tr>")
                sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""getnc('" & ncid & "','" & nc & "','yes');"">" & nc & "</td>")
                sb.Append("</tr>")
            End While
            dr.Close()
            sb.Append("</table>")
            divmisc.InnerHtml = sb.ToString
        End If
    End Sub
    Private Sub PopSys()
        sql = "select distinct systemid from pmlocsys"
        dr = ec.GetRdrData(sql)
        ddsys.DataSource = dr
        ddsys.DataTextField = "systemid"
        ddsys.DataValueField = "systemid"
        ddsys.DataBind()
        dr.Close()
        ddsys.Items.Insert(0, "Select System")
    End Sub
    Private Sub PopTypes()
        sql = "select * from pmloctypes where loctype <> 'EQUIPMENT'"
        dr = ec.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataTextField = "loctype"
        ddtype.DataValueField = "loctype"
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, "Select Type")
    End Sub
    Private Sub GetL2(ByVal level As String)
        'sys = "PRIMARY"
        sys = ddsys.SelectedValue
        Dim loctyp As String = ""
        If ddtype.SelectedIndex <> 0 And ddtype.SelectedIndex <> -1 Then
            loctyp = ddtype.SelectedValue
        End If
        Dim appver As String = lblappver.Value
        Dim atyp As String
        atyp = lbltyp.Value
        Select Case level
            Case "1"
                lid = lbllid.Value
                lidid = lblretlidi.Value
            Case "2"
                lid = lbllid1.Value
                lidid = lbllidi1.Value
            Case "3"
                lid = lbllid2.Value
                lidid = lbllidi2.Value
            Case "4"
                lid = lbllid3.Value
                lidid = lbllidi3.Value
            Case "5"
                lid = lbllid4.Value
                lidid = lbllidi4.Value
            Case "6"
                lid = lbllid5.Value
                lidid = lbllidi5.Value
            Case "7"
                lid = lbllid6.Value
                lidid = lbllidi6.Value
            Case "8"
                lid = lbllid7.Value
                lidid = lbllidi7.Value
            Case "9"
                lid = lbllid8.Value
                lidid = lbllidi8.Value
            Case "10"
                lid = lbllid9.Value
                lidid = lbllidi9.Value
            Case "11"
                lid = lbllid10.Value
                lidid = lbllidi10.Value
            Case "12"
                lid = lbllid11.Value
                lidid = lbllidi11.Value
            Case "13"
                lid = lbllid12.Value
                lidid = lbllidi12.Value
            Case "14"
                lid = lbllid13.Value
                lidid = lbllidi13.Value
            Case "15"
                lid = lbllid14.Value
                lidid = lbllidi14.Value
            Case "16"
                lid = lbllid15.Value
                lidid = lbllidi15.Value
            Case "17"
                lid = lbllid16.Value
                lidid = lbllidi16.Value
            Case "18"
                lid = lbllid17.Value
                lidid = lbllidi17.Value
            Case "19"
                lid = lbllid18.Value
                lidid = lbllidi18.Value
            Case "20"
                lid = lbllid19.Value
                lidid = lbllidi19.Value
            Case "21"
                lid = lbllid20.Value
                lidid = lbllidi20.Value
            Case "22"
                lid = lbllid21.Value
                lidid = lbllidi21.Value
            Case "23"
                lid = lbllid22.Value
                lidid = lbllidi22.Value
            Case "24"
                lid = lbllid23.Value
                lidid = lbllidi23.Value
            Case "25"
                lid = lbllid24.Value
                lidid = lbllidi24.Value
        End Select
        Dim ccnt As Integer
        Dim eqid, typ, eqnum, eqdesc, fpc As String
        Dim getlev, tdlev, geteq As String
        getlev = "getecd" & level
        tdlev = "tdec" & level
        'sql = "select count(*) from pmlocations where locid in (select locid from pmlocations where location in " _
        '+ "(select location from pmlocheir where parent = '" & lid & "')) " _
        '+ "and type = 'EQUIPMENT'"
        sid = lblsid.Value
        sql = "select count(*) " _
        + "from pmlocations l " _
        + "left join equipment e on e.locid = l.locid " _
        + "left join pmlocheir h on h.location = l.location " _
        + "where h.systemid = '" & sys & "' and h.parent = '" & lid & "' and l.siteid = '" & sid & "'" ' and l.type <> 'EQUIPMENT'"
        Dim lcnt As Integer
        Dim lidhold, lididhold As String
        lidhold = lblhold.Value
        lididhold = lblholdi.Value
        lblhold.Value = lid
        lblholdi.Value = lidid
        lcnt = ec.Scalar(sql)
        If lcnt > 0 Then
            sql = "select h.location, l.description, isnull(e.eqid, 0) as eqid, e.eqnum, e.eqdesc, l.type, l.locid, e.fpc, " _
            + "ccnt = (select count(*) from pmlocheir h1 where h1.parent = l.location and h1.systemid = '" & sys & "' and h1.siteid = '" & sid & "'), h.parent " _
            + "from pmlocations l " _
            + "left join equipment e on e.locid = l.locid " _
            + "left join pmlocheir h on h.location = l.location " _
            + "where h.systemid = '" & sys & "' and h.parent = '" & lid & "' and l.siteid = '" & sid & "' and h.siteid = '" & sid & "'"
            'appver = "2" And 
            If atyp = "lul" Then
                sql += " and l.type <> 'EQUIPMENT' order by h.location, e.eqnum"
            Else
                If loctyp <> "" Then
                    sql += " and l.type = '" & loctyp & "' order by h.location, e.eqnum"
                Else
                    sql += " order by h.location, e.eqnum"
                End If
            End If
            
        Else
            'l.type
            If atyp <> "lul" Then
                sql = "select distinct h.location, l.description, isnull(e.eqid, 0) as eqid, e.eqnum, e.eqdesc, e.fpc, l.type, l.locid, " _
                + "ccnt = (select count(*) from pmlocheir h1 where h1.parent = l.location and h1.systemid = '" & sys & "'), h.parent " _
                + "from pmlocations l " _
                + "left join equipment e on e.locid = l.locid " _
                + "left join pmlocheir h on h.location = l.location " _
                + "where h.systemid = '" & sys & "' and h.parent = '" & lid & "' and l.siteid = '" & sid & "' order by h.location, e.eqnum"
            Else
                FillLevels()
                Dim strMessage As String
                strMessage = "No Locations Beyond This Level"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
           
        End If
        dr = ec.GetRdrData(sql)

        Dim parent As String

        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 530px;  HEIGHT: 270px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""500"" cellpadding=""1"">")
        sb.Append("<tr>")
        sb.Append("<td width=""150""></td>")
        sb.Append("<td width=""280""></td>")
        sb.Append("<td width=""70""></td>")
        sb.Append("</tr>")
        Dim ltst As String
        While dr.Read
            parent = dr.Item("parent").ToString
            fpc = dr.Item("fpc").ToString
            lid = dr.Item("location").ToString
            If lid = "P405011" Then
                ltst = lid
            End If
            loc = dr.Item("description").ToString
            ccnt = dr.Item("ccnt").ToString
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            If eqid <> "0" Then
                Dim echk As String = eqnum
            End If

            typ = dr.Item("type").ToString
            lidi = dr.Item("locid").ToString
            sb.Append("<tr>")
            If ccnt = 0 And ccnt = 0 Then
                If eqid = "0" Then
                    sb.Append("<td class=""label""><a href=""#"" class=""A1R""  onclick=""" & getlev & "('" & lidi & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','" & fpc & "','" & eqdesc & "','" & parent & "','no','no');"">" & lid & "</td>")
                    sb.Append("<td class=""plainlabelblue"">" & loc & "</td>")
                    sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                Else
                    If lcnt > 0 Then
                        sb.Append("<td class=""label""><a href=""#"" class=""A1G""  onclick=""" & getlev & "('" & lidi & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','" & fpc & "','" & eqdesc & "','" & parent & "','no','yes');"">" & eqnum & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & eqdesc & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                    Else
                        sb.Append("<td class=""label""><a href=""#"" class=""A1G""  onclick=""" & getlev & "('" & lidi & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','" & fpc & "','" & eqdesc & "','" & parent & "','no','yes');"">" & eqnum & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & eqdesc & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                    End If

                End If
            Else
                'Dim dummy As String = "ok"
                If eqid = "0" Then
                    sb.Append("<td class=""label""><a href=""#"" onclick=""" & getlev & "('" & lidi & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','" & fpc & "','" & eqdesc & "','" & parent & "','yes','no');"">" & lid & "</td>")
                    sb.Append("<td class=""plainlabelblue"">" & loc & "</td>")
                    sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                Else
                    If ccnt > 0 Then 'was lcnt
                        sb.Append("<td class=""label""><a href=""#"" class=""A1G""  onclick=""" & getlev & "('" & lidi & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','" & fpc & "','" & eqdesc & "','" & parent & "','yes','yes');"">" & eqnum & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & eqdesc & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                    Else
                        sb.Append("<td class=""label""><a href=""#"" class=""A1G""  onclick=""" & getlev & "('" & lidi & "','" & lid & "','" & loc & "','" & eqid & "','" & eqnum & "','" & fpc & "','" & eqdesc & "','" & parent & "','no','yes');"">" & eqnum & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & eqdesc & "</td>")
                        sb.Append("<td class=""plainlabelblue"">" & typ & "</td>")
                    End If
                End If

            End If
            sb.Append("</tr>")
        End While
        dr.Close()

        sb.Append("</table>")

        tdec.Attributes.Add("class", "details")
        HideAll()
        Select Case level
            Case "1"
                lbltabs.Value = "ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec1.InnerHtml = sb.ToString
                tdec1.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 1"
            Case "2"
                lbltabs.Value = "ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec2.InnerHtml = sb.ToString
                tdec2.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 2"
            Case "3"
                lbltabs.Value = "ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec3.InnerHtml = sb.ToString
                tdec3.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 3"
            Case "4"
                lbltabs.Value = "ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec4.InnerHtml = sb.ToString
                tdec4.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 4"
            Case "5"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec5.InnerHtml = sb.ToString
                tdec5.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 5"
            Case "6"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec6.InnerHtml = sb.ToString
                tdec6.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 6"
            Case "7"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec7.InnerHtml = sb.ToString
                tdec7.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 7"
            Case "8"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec8.InnerHtml = sb.ToString
                tdec8.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 8"
            Case "9"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec9.InnerHtml = sb.ToString
                tdec9.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 9"
            Case "10"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec10.InnerHtml = sb.ToString
                tdec10.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 10"
            Case "11"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec11.InnerHtml = sb.ToString
                tdec11.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 11"
            Case "12"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec12.InnerHtml = sb.ToString
                tdec12.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 12"
            Case "13"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no,no"
                tdec13.InnerHtml = sb.ToString
                tdec13.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 13"
            Case "14"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no,no"
                tdec14.InnerHtml = sb.ToString
                tdec14.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 14"
            Case "15"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no,no"
                tdec15.InnerHtml = sb.ToString
                tdec15.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 15"
            Case "16"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no,no"
                tdec16.InnerHtml = sb.ToString
                tdec16.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 16"
            Case "17"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no,no"
                tdec17.InnerHtml = sb.ToString
                tdec17.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 17"
            Case "18"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no,no"
                tdec18.InnerHtml = sb.ToString
                tdec18.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 18"
            Case "19"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no,no"
                tdec19.InnerHtml = sb.ToString
                tdec19.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 19"
            Case "20"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no,no"
                tdec20.InnerHtml = sb.ToString
                tdec20.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 20"
            Case "21"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no,no"
                tdec21.InnerHtml = sb.ToString
                tdec21.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 21"
            Case "22"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no,no"
                tdec22.InnerHtml = sb.ToString
                tdec22.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 22"
            Case "23"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no,no"
                tdec23.InnerHtml = sb.ToString
                tdec23.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 23"
            Case "24"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,no"
                tdec24.InnerHtml = sb.ToString
                tdec24.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 24"
            Case "25"
                lbltabs.Value = "ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok,ok"
                tdec25.InnerHtml = sb.ToString
                tdec25.Attributes.Add("class", "view")
                tdtab.InnerHtml = "Level 25"
        End Select
        FillLevels()
        

    End Sub
    Private Sub UnDoEq()
        lbleqid.Value = ""
        lbleq.Value = ""
        lbleqdesc.Value = ""
        lblfpc.Value = ""
        lblfuid.Value = ""
        lblcoid.Value = ""
        lblfunc.Value = ""
        lblcomp.Value = ""
        divfu.InnerHtml = ""
        divco.InnerHtml = ""
    End Sub
    Private Sub FillLevels(Optional ByVal iseq As Integer = 0)
        lev = lbllevel.Value
        Dim ischild As String = lbleqchild.Value
        If iseq = 1 And ischild <> "yes" Then
            Dim levi As Integer
            Try
                levi = CType(lev, Integer)
                lev = levi - 1
            Catch ex As Exception

            End Try
        End If
        Select Case lev
            Case "0"
                lbllid.Value = ""
                lblloc.Value = ""

                lbllid1.Value = ""
                lblloc1.Value = ""
                lbllid2.Value = ""
                lblloc2.Value = ""
                lbllid3.Value = ""
                lblloc3.Value = ""
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd.InnerHtml = lbleq.Value
                    tdecd.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "1"
                lbllid1.Value = ""
                lblloc1.Value = ""
                lbllid2.Value = ""
                lblloc2.Value = ""
                lbllid3.Value = ""
                lblloc3.Value = ""
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd1.InnerHtml = lbleq.Value
                    tdecd1.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "2"
                lbllid2.Value = ""
                lblloc2.Value = ""
                lbllid3.Value = ""
                lblloc3.Value = ""
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd2.InnerHtml = lbleq.Value
                    tdecd2.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "3"
                lbllid3.Value = ""
                lblloc3.Value = ""
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd3.InnerHtml = lbleq.Value
                    tdecd3.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "4"
                lbllid4.Value = ""
                lblloc4.Value = ""
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd4.InnerHtml = lbleq.Value
                    tdecd4.Attributes.Add("class", "greenlabel")
                Else
                    ''UnDoEq()
                End If
            Case "5"
                lbllid5.Value = ""
                lblloc5.Value = ""
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd5.InnerHtml = lbleq.Value
                    tdecd5.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "6"
                lbllid6.Value = ""
                lblloc6.Value = ""
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd6.InnerHtml = lbleq.Value
                    tdecd6.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "7"
                lbllid7.Value = ""
                lblloc7.Value = ""
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd7.InnerHtml = lbleq.Value
                    tdecd7.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "8"
                lbllid8.Value = ""
                lblloc8.Value = ""
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd8.InnerHtml = lbleq.Value
                    tdecd8.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "9"
                lbllid9.Value = ""
                lblloc9.Value = ""
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd9.InnerHtml = lbleq.Value
                    tdecd9.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "10"
                lbllid10.Value = ""
                lblloc10.Value = ""

                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd10.InnerHtml = lbleq.Value
                    tdecd10.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "11"
                lbllid11.Value = ""
                lblloc11.Value = ""
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd11.InnerHtml = lbleq.Value
                    tdecd11.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "12"
                lbllid12.Value = ""
                lblloc12.Value = ""
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd12.InnerHtml = lbleq.Value
                    tdecd12.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "13"
                lbllid13.Value = ""
                lblloc13.Value = ""
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd13.InnerHtml = lbleq.Value
                    tdecd13.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "14"
                lbllid14.Value = ""
                lblloc14.Value = ""
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd14.InnerHtml = lbleq.Value
                    tdecd14.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "15"
                lbllid15.Value = ""
                lblloc15.Value = ""
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd15.InnerHtml = lbleq.Value
                    tdecd15.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "16"
                lbllid16.Value = ""
                lblloc16.Value = ""
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd16.InnerHtml = lbleq.Value
                    tdecd16.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "17"
                lbllid17.Value = ""
                lblloc17.Value = ""
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd17.InnerHtml = lbleq.Value
                    tdecd17.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "18"
                lbllid18.Value = ""
                lblloc18.Value = ""
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd18.InnerHtml = lbleq.Value
                    tdecd18.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "19"
                lbllid19.Value = ""
                lblloc19.Value = ""
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd19.InnerHtml = lbleq.Value
                    tdecd19.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "20"
                lbllid20.Value = ""
                lblloc20.Value = ""

                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd20.InnerHtml = lbleq.Value
                    tdecd20.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "21"
                lbllid21.Value = ""
                lblloc21.Value = ""
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd21.InnerHtml = lbleq.Value
                    tdecd21.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "22"
                lbllid22.Value = ""
                lblloc22.Value = ""
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd22.InnerHtml = lbleq.Value
                    tdecd22.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "23"
                lbllid23.Value = ""
                lblloc23.Value = ""
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd23.InnerHtml = lbleq.Value
                    tdecd23.Attributes.Add("class", "greenlabel")
                End If
            Case "24"
                lbllid24.Value = ""
                lblloc24.Value = ""
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd24.InnerHtml = lbleq.Value
                    tdecd24.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
            Case "25"
                lbllid25.Value = ""
                lblloc25.Value = ""
                FillAll()
                If iseq = 1 Then
                    tdecd25.InnerHtml = lbleq.Value
                    tdecd25.Attributes.Add("class", "greenlabel")
                Else
                    'UnDoEq()
                End If
        End Select

    End Sub
    Private Sub FillAll()
        tdecd.InnerHtml = lbllid.Value
        tdecd1.InnerHtml = lbllid1.Value
        tdecd2.InnerHtml = lbllid2.Value
        tdecd3.InnerHtml = lbllid3.Value
        tdecd4.InnerHtml = lbllid4.Value
        tdecd5.InnerHtml = lbllid5.Value
        tdecd6.InnerHtml = lbllid6.Value
        tdecd7.InnerHtml = lbllid7.Value
        tdecd8.InnerHtml = lbllid8.Value
        tdecd9.InnerHtml = lbllid9.Value
        tdecd10.InnerHtml = lbllid10.Value
        tdecd11.InnerHtml = lbllid11.Value
        tdecd12.InnerHtml = lbllid12.Value
        tdecd13.InnerHtml = lbllid13.Value
        tdecd14.InnerHtml = lbllid14.Value
        tdecd15.InnerHtml = lbllid15.Value
        tdecd16.InnerHtml = lbllid16.Value
        tdecd17.InnerHtml = lbllid17.Value
        tdecd18.InnerHtml = lbllid18.Value
        tdecd19.InnerHtml = lbllid19.Value
        tdecd20.InnerHtml = lbllid20.Value
        tdecd21.InnerHtml = lbllid21.Value
        tdecd22.InnerHtml = lbllid22.Value
        tdecd23.InnerHtml = lbllid23.Value
        tdecd24.InnerHtml = lbllid24.Value
        tdecd25.InnerHtml = lbllid25.Value

        tdecd.Attributes.Add("class", "plainlabel")
        tdecd1.Attributes.Add("class", "plainlabel")
        tdecd2.Attributes.Add("class", "plainlabel")
        tdecd3.Attributes.Add("class", "plainlabel")
        tdecd4.Attributes.Add("class", "plainlabel")
        tdecd5.Attributes.Add("class", "plainlabel")
        tdecd6.Attributes.Add("class", "plainlabel")
        tdecd7.Attributes.Add("class", "plainlabel")
        tdecd8.Attributes.Add("class", "plainlabel")
        tdecd9.Attributes.Add("class", "plainlabel")
        tdecd10.Attributes.Add("class", "plainlabel")
        tdecd11.Attributes.Add("class", "plainlabel")
        tdecd12.Attributes.Add("class", "plainlabel")
        tdecd13.Attributes.Add("class", "plainlabel")
        tdecd14.Attributes.Add("class", "plainlabel")
        tdecd15.Attributes.Add("class", "plainlabel")
        tdecd16.Attributes.Add("class", "plainlabel")
        tdecd17.Attributes.Add("class", "plainlabel")
        tdecd18.Attributes.Add("class", "plainlabel")
        tdecd19.Attributes.Add("class", "plainlabel")
        tdecd20.Attributes.Add("class", "plainlabel")
        tdecd21.Attributes.Add("class", "plainlabel")
        tdecd22.Attributes.Add("class", "plainlabel")
        tdecd23.Attributes.Add("class", "plainlabel")
        tdecd24.Attributes.Add("class", "plainlabel")
        tdecd25.Attributes.Add("class", "plainlabel")
    End Sub
    Private Sub HideAll()
        tdec1.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdec5.Attributes.Add("class", "details")
        tdec6.Attributes.Add("class", "details")
        tdec7.Attributes.Add("class", "details")
        tdec8.Attributes.Add("class", "details")
        tdec9.Attributes.Add("class", "details")
        tdec10.Attributes.Add("class", "details")
        tdec11.Attributes.Add("class", "details")
        tdec12.Attributes.Add("class", "details")
        tdec13.Attributes.Add("class", "details")
        tdec14.Attributes.Add("class", "details")
        tdec15.Attributes.Add("class", "details")
        tdec16.Attributes.Add("class", "details")
        tdec17.Attributes.Add("class", "details")
        tdec18.Attributes.Add("class", "details")
        tdec19.Attributes.Add("class", "details")
        tdec20.Attributes.Add("class", "details")
        tdec21.Attributes.Add("class", "details")
        tdec22.Attributes.Add("class", "details")
        tdec23.Attributes.Add("class", "details")
        tdec24.Attributes.Add("class", "details")
        tdec25.Attributes.Add("class", "details")
    End Sub
    Private Sub GetL1()
        sid = lblsid.Value
        'sys = "PRIMARY"
        sys = ddsys.SelectedValue
        sql = "select l.location, l.description, l.locid from pmlocations l " _
        + "left join pmlocheir h on l.location = h.location " _
        + "where h.systemid = '" & sys & "' and h.parent is null and l.siteid = '" & sid & "' and l.type <> 'EQUIPMENT'"



        dr = ec.GetRdrData(sql)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 520px;  HEIGHT: 250px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table width=""500"" cellpadding=""1"">")
        sb.Append("<tr>")
        sb.Append("<td width=""150""></td>")
        sb.Append("<td width=""280""></td>")
        sb.Append("<td width=""70""></td>")
        sb.Append("</tr>")
        While dr.Read
            lid = dr.Item("location").ToString
            loc = dr.Item("description").ToString
            lidi = dr.Item("locid").ToString
            sb.Append("<tr>")
            sb.Append("<td class=""label""><a href=""#"" onclick=""getecd0('" & lidi & "','" & lid & "','" & loc & "','yes');"">" & lid & "</td>")
            sb.Append("<td class=""plainlabelblue"">" & loc & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        lbltabs.Value = "ok,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no,no"
        sb.Append("</table>")
        tdec.InnerHtml = sb.ToString

        tdec.Attributes.Add("class", "view")
        tdec1.Attributes.Add("class", "details")
        tdec2.Attributes.Add("class", "details")
        tdec3.Attributes.Add("class", "details")
        tdec4.Attributes.Add("class", "details")
        tdec5.Attributes.Add("class", "details")
        tdec6.Attributes.Add("class", "details")
        tdec7.Attributes.Add("class", "details")
        tdec8.Attributes.Add("class", "details")
        tdec9.Attributes.Add("class", "details")
        tdec10.Attributes.Add("class", "details")
        tdec11.Attributes.Add("class", "details")
        tdec12.Attributes.Add("class", "details")
        tdec13.Attributes.Add("class", "details")
        tdec14.Attributes.Add("class", "details")
        tdec15.Attributes.Add("class", "details")
        tdec16.Attributes.Add("class", "details")
        tdec17.Attributes.Add("class", "details")
        tdec18.Attributes.Add("class", "details")
        tdec19.Attributes.Add("class", "details")
        tdec20.Attributes.Add("class", "details")
        tdec21.Attributes.Add("class", "details")
        tdec22.Attributes.Add("class", "details")
        tdec23.Attributes.Add("class", "details")
        tdec24.Attributes.Add("class", "details")
        tdec25.Attributes.Add("class", "details")

        tdtab.InnerHtml = "Level 0"
        lbllevel.Value = "0"
        HideAll()
        Dim srchtyp As String = lblsrchtyp.Value
        If srchtyp <> "eq" Then
            FillLevels()
        End If

    End Sub

   
    
End Class

