

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class pmlocmain
    Inherits System.Web.UI.Page
    Protected WithEvents lang3069 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3068 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3067 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3066 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3065 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3064 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3063 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3062 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3061 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3060 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3059 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3058 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3057 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim chk, sid, did, clid, cid, ret, app, srch, jump, lid, lhid, ro, ustr As String
    Dim filt As String
    Dim sql As String
    Dim main As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents Tr2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtlocdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdbelongs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdchildren As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblrelloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsys As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifeq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifrt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocsav As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbldelpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldelchild As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldellhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltop As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbtop As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblchildcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dellhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqparcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqparloccnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String
    Dim childcnt As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents celldiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ddsys As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblsysdesc As System.Web.UI.WebControls.Label
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Dim appc As New AppUtils
        'Dim url As String = appc.Switch
        'If url <> "ok" Then
        'Response.Redirect(url)
        'End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        If Not IsPostBack Then
            ddtype.Attributes.Add("onchange", "checktype(this.value)")
            ustr = Request.QueryString("usrname").ToString
            lbluser.Value = ustr
            cid = HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            'Read Only
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                'pgtitle.InnerHtml = "PM Locations (Read Only)"
            Else
                'pgtitle.InnerHtml = "PM Locations"
            End If

            main.Open()
            'GetArch()
            PopSys(cid)
            PopTypes(cid)
            GetBelongs("0")
            GetChildren("0")
            Try
                jump = Request.QueryString("jump").ToString
                If jump = "yes" Then
                    lhid = Request.QueryString("lhid").ToString
                    lbllhid.Value = lhid
                    lbltyp.Value = "sys"
                    SrchLoc()
                End If
            Catch ex As Exception
                If ro = "1" Then
                    imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    imgadd.Attributes.Add("onclick", "")
                End If
            End Try
            main.Dispose()
            lblstat.Value = "wait"
        Else
            ret = Request.Form("lblsubmit")
            If ret = "add" Then
                lblsubmit.Value = ""
                main.Open()
                AddLoc()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "addpar" Then
                lblsubmit.Value = ""
                main.Open()
                AddPar()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "addchild" Then
                lblsubmit.Value = ""
                main.Open()
                AddChild()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "srch" Then
                lblsubmit.Value = ""
                main.Open()
                lbllocsav.Value = ""
                SrchLoc()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "srch1" Then
                lblsubmit.Value = ""
                main.Open()
                lbllocsav.Value = ""
                SrchLoc1()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "savloc" Then
                lblsubmit.Value = ""
                main.Open()
                SavLoc()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "delloc" Then
                lblsubmit.Value = ""
                main.Open()
                'lbllocsav.Value = ""
                DelLoc()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "delparloc" Then
                lblsubmit.Value = ""
                main.Open()
                DelParLoc()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "delchild" Then
                lblsubmit.Value = ""
                main.Open()
                DelChild()
                main.Dispose()
                lblref.Value = "go"
            ElseIf ret = "switchloc" Then
                lblsubmit.Value = ""
                main.Open()
                SwitchLoc()
                main.Dispose()
                lblref.Value = "go"
            End If
        End If
        'ddsys.Attributes.Add("onchange", "checksys();")

    End Sub
    Private Sub SwitchLoc()
        Dim strMessage As String
        Dim loc, rel, lid, lhid, sys, psys, istop, eqid, releq As String
        eqid = lbleqid.Value
        loc = txtloc.Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)
        rel = lblrelloc.Value
        sql = "update pmlocheir set parent = '" & rel & "' where location = '" & loc & "'"
        main.Update(sql)
        GetBelongs(loc)
        lblstat.Value = "active"
        lblsys.Value = sys
        ddsys.Enabled = False
        GetChildren(loc)
        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")

        
    End Sub
    Private Sub AddPar()
        Dim strMessage As String
        Dim loc, rel, lid, lhid, sys, psys, istop, eqid, releq As String
        eqid = lbleqid.Value
        loc = txtloc.Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)
        rel = lblrelloc.Value
        sql = "select isnull(eqid, 0) from pmlocations where location = '" & rel & "'"
        releq = main.strScalar(sql)
        If eqid = "" And releq <> "0" Then
            strMessage = "Cannot Add a Location to an Equipment Record"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        lhid = lbllhid.Value
        istop = lbltop.Value
        Dim relcnt, relcnt1, childcnt As Integer
        Dim childval As String
        cid = lblcid.Value
        sid = lblsid.Value
        Dim locid As Integer
        If rel <> loc Then
            If ddsys.SelectedIndex = 0 Then
                strMessage = "Location System Required!"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                sys = ddsys.SelectedValue.ToString
            End If
            If istop = "Y" Then
                strMessage = "Cannot Add a Parent to a Top Level Location!"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            sql = "usp_cntpar '" & sys & "', '" & lhid & "'"
            Dim lvl As Integer = main.Scalar(sql)
            'sql = "select count(*) from pmlocheir where lhid = '" & lhid & "' and parent is null or len(parent) = 0"
            sql = "select count(*) from pmlocheir where location = '" & loc & "' and (parent is null or len(parent) = 0)"
            relcnt = main.Scalar(sql)
            sql = "select count(*) from pmlocheir where parent = '" & loc & "'"
            childcnt = main.Scalar(sql)
            If childcnt > 0 Then
                childval = "Y"
            Else
                childval = "N"
            End If
            sql = "select systemid from pmlocheir where location = '" & rel & "'"
            psys = main.strScalar(sql)
            If sys = psys Then
                If relcnt = 1 Then
                    If lvl < 24 Then
                        sql = "update pmlocheir set parent = '" & rel & "' where location = '" & loc & "'"
                        main.Update(sql)
                        GetBelongs(loc)
                        lblstat.Value = "active"
                        lblsys.Value = sys
                        ddsys.Enabled = False
                        'GetArch()
                        GetChildren(loc)
                        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
                    Else
                        Dim strMessage0 As String = tmod.getmsg("cdstr1541", "pmlocmain.aspx.vb") & " " + "cannot have a Child Location."
                        Utilities.CreateMessageAlert(Me, strMessage0, "strKey1")
                    End If
                Else
                    sql = "insert into pmlocheir (compid, siteid, location, systemid, haschildren, parent) values " _
                        + "('" & cid & "','" & sid & "','" & loc & "','" & psys & "','" & childval & "','" & rel & "') select @@identity"
                    locid = main.Scalar(sql)
                    lbllhid.Value = locid
                    GetBelongs(loc)
                    lblstat.Value = "active"
                    lblsys.Value = sys
                    ddsys.Enabled = False
                    'GetArch()
                    GetChildren(loc)
                    imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
                End If
            Else
                If relcnt = 1 Then
                    If lvl < 24 Then
                        sql = "update pmlocheir set parent = '" & rel & "', systemid = '" & psys & "' where location = '" & loc & "'"
                        main.Update(sql)
                        GetBelongs(loc)
                        lblstat.Value = "active"
                        lblsys.Value = sys
                        ddsys.Enabled = False
                        'GetArch()
                        GetChildren(loc)
                        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
                    Else
                        Dim strMessage0 As String = tmod.getmsg("cdstr1541", "pmlocmain.aspx.vb") & " " + "cannot have a Child Location."
                        Utilities.CreateMessageAlert(Me, strMessage0, "strKey1")
                    End If
                Else
                    sql = "insert into pmlocheir (compid, siteid, location, systemid, haschildren) values " _
                        + "('" & cid & "','" & sid & "','" & loc & "','" & psys & "'," & childval & "') select @@identity"
                    locid = main.Scalar(sql)
                    lbllhid.Value = locid
                    GetBelongs(loc)
                    lblstat.Value = "active"
                    lblsys.Value = sys
                    ddsys.Enabled = False
                    'GetArch()
                    GetChildren(loc)
                    imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
                End If
            End If
            
        Else
            strMessage = "Cannot Add a Location as a Parent of Itself!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

        End If


    End Sub
    Private Sub AddChild()
        Dim strMessage As String
        Dim loc, rel, lid, lhid, sys, istop, eqid, releq As String
        eqid = lbleqid.Value
        loc = txtloc.Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)
        rel = lblrelloc.Value
        sql = "select isnull(eqid, 0) from pmlocations where location = '" & rel & "'"
        releq = main.strScalar(sql)
        If eqid <> "" And releq = "0" Then
            strMessage = "Cannot Add a Location to an Equipment Record"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        lhid = lbllid.Value 'lbllhid.Value
        If rel <> loc Then
            If ddsys.SelectedIndex = 0 Then
                strMessage = "Location System Required!"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                sys = ddsys.SelectedValue.ToString
            End If
            sql = "usp_cntpar_temp '" & sys & "', '" & lhid & "'"
            Dim lvl As Integer = main.Scalar(sql)
            If lvl <> 24 Then
                sql = "select istop from pmlocheir where location = '" & rel & "'"
                Try
                    istop = main.strScalar(sql)
                Catch ex As Exception
                    istop = "N"
                End Try

                If istop = "Y" Then
                    strMessage = "Cannot Add a Top Level Location as a Child of Another Location!"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    sql = "update pmlocheir set parent = '" & loc & "' where location = '" & rel & "'"
                    main.Update(sql)
                    GetBelongs(loc)
                    lblstat.Value = "active"
                    lblsys.Value = sys
                    ddsys.Enabled = False
                    'GetArch()
                    GetChildren(loc)
                    imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
                End If
            Else
                Dim strMessage0 As String = "The parent Location is Level 24 and cannot have a Child Location" 'tmod.getmsg("cdstr1542", "pmlocmain.aspx.vb") _
                '+ "cannot have a Child Location."
                Utilities.CreateMessageAlert(Me, strMessage0, "strKey1")
            End If
        Else
            strMessage = "Cannot Add a Location as a Child of Itself!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub DelChild()
        lhid = lbldellhid.Value
        Dim par As String = lbldelchild.Value
        sql = "update pmlocheir set parent = null where lhid = '" & lhid & "' and parent = '" & par & "'"
        main.Update(sql)
        Dim loc As String = lbllocsav.Value
        lblstat.Value = "active"
        ddsys.Enabled = False
        'GetArch()
        GetBelongs(loc)
        GetChildren(loc)
        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
    End Sub
    Private Sub DelLoc()
        lid = lbllid.Value
        lhid = lbllhid.Value
        ustr = lbluser.Value
        Dim lcnt As Integer
        Dim strMessage As String
        Dim locsav As String = lbllocsav.Value
        sql = "select count(*) from equipment where locid = '" & lid & "' and dept_id is null"
        lcnt = main.Scalar(sql)
        If lcnt = 0 Then
            sql = "delete from pmlocations where locid = '" & lid & "'; " _
            + "delete from pmlocheir where lhid = '" & lhid & "'; " _
            + "update pmlocheir set parent = null where parent = '" & locsav & "'"
            main.Update(sql)
            Response.Redirect("pmlocmain.aspx?usrname=" & ustr)
        Else
            strMessage = "One or More Equipment Records are Assigned to this Location and it Cannot be Deleted"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
    End Sub
    Private Sub DelParLoc()
        Dim currlhid As String = lbllhid.Value 'dellhid.Value
        lid = lbllid.Value
        lhid = lbllhid.Value
        ustr = lbluser.Value
        Dim strMessage As String
        Dim locdel As String = lbldelpar.Value
        Dim locsav As String = lbllocsav.Value
        Dim sys As String = ddsys.SelectedValue.ToString
        sql = "update pmlocheir set parent = null where location = '" & locsav & "' " _
        + "and parent = '" & locdel & "'"
        main.Update(sql)
        Dim rcnt, rcnt1 As Integer
        sql = "select count(*) from pmlocheir where location = '" & locsav & "' and parent is null and systemid = '" & sys & "'"
        rcnt = main.Scalar(sql)
        sql = "select count(*) from pmlocheir where location = '" & locsav & "' and parent is not null and systemid = '" & sys & "'"
        rcnt1 = main.Scalar(sql)
        If rcnt = 1 And rcnt1 <> 0 Then
            sql = "delete from pmlocheir where location = '" & locsav & "' and parent is null and systemid = '" & sys & "'"
            main.Update(sql)
            GetBelongs(locsav)
            GetChildren(locsav)
            imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
            'Response.Redirect("pmlocmain.aspx?usrname=" & ustr)
        Else
            Dim loc As String = lbllocsav.Value
            lblstat.Value = "active"
            ddsys.Enabled = False
            'GetArch()
            GetBelongs(loc)
            GetChildren(loc)
            imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
        End If



    End Sub
    Private Sub SavLoc()
        Dim strMessage As String
        Dim loc, desc, sys, typ, typid, rel, locchk, istop, eqid As String
        eqid = lbleqid.Value
        If cbtop.Checked = True Then
            istop = "Y"
        End If
        If ddtype.SelectedIndex = 0 Then
            strMessage = "Location Type Required!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else
            typ = ddtype.SelectedValue.ToString
        End If
        If eqid <> "" Then
            strMessage = "Cannot Save Location as Equipment"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If ddsys.SelectedIndex = 0 Then
            strMessage = "Location System Required!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else
            sys = ddsys.SelectedValue.ToString
        End If

        loc = txtloc.Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)

        locchk = lbllocsav.Value
        locchk = Replace(locchk, "'", Chr(180), , , vbTextCompare)
        locchk = Replace(locchk, "--", "-", , , vbTextCompare)
        locchk = Replace(locchk, ";", ":", , , vbTextCompare)

        desc = txtlocdesc.Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        cid = lblcid.Value
        sid = lblsid.Value
        Dim cnt, cntsys, cntchild As Integer
        If loc <> "0" Then
            If loc <> locchk Then
                sql = "select count(*) from pmlocations where location = '" & loc & "' and siteid = '" & sid & "'"
                cnt = main.Scalar(sql)
            Else
                cnt = 0
            End If

            If cnt = 0 Then
                lhid = lbllhid.Value
                lid = lbllid.Value
                If istop = "Y" Then
                    sql = "update pmlocheir set " _
               + "location = '" & loc & "', systemid = '" & sys & "', istop = 'Y' " _
               + "where lhid = '" & lhid & "'"
                Else
                    sql = "update pmlocheir set " _
               + "location = '" & loc & "', systemid = '" & sys & "', istop = NULL " _
               + "where lhid = '" & lhid & "'"
                End If

                'this needs to change
                main.Update(sql)
                sql = "update pmlocheir set parent = '" & loc & "' where parent = '" & locchk & "'"
                main.Update(sql)
                sql = "update pmlocations set location = '" & loc & "', description = '" & desc & "', " _
                + "type = '" & typ & "' where locid = '" & lid & "'"
                main.Update(sql)
                imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
            Else 'cnt = 0
                strMessage = "This Site Already Has A Location With This Name"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else 'loc = 0
            strMessage = "Location ""0"" is Reserved"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub AddLoc()
        Dim strMessage As String
        Dim loc, desc, sys, typ, typid, rel, istop As String
        If cbtop.Checked = True Then
            istop = "Y"
        End If
        If ddtype.SelectedIndex = 0 Then
            strMessage = "Location Type Required!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else
            typ = ddtype.SelectedValue.ToString
            If typ = "EQUIPMENT" Then
                strMessage = "Cannot add Equipment as a Location in the Location Manager"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If
        If ddsys.SelectedIndex = 0 Then
            strMessage = "Location System Required!"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else
            sys = ddsys.SelectedValue.ToString
        End If

        loc = txtloc.Text
        loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
        loc = Replace(loc, "--", "-", , , vbTextCompare)
        loc = Replace(loc, ";", ":", , , vbTextCompare)
        desc = txtlocdesc.Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        cid = lblcid.Value
        sid = lblsid.Value
        Dim cnt, cntsys, cntchild As Integer
        If loc <> "0" Then
            sql = "select count(*) from pmlocations where location = '" & loc & "' and siteid = '" & sid & "'"
            cnt = main.Scalar(sql)
            If cnt = 0 Then

                Dim locid As String
                Dim loci As String
                rel = lblrelloc.Value
                If rel = "" Then
                    If istop = "Y" Then
                        sql = "insert into pmlocheir (compid, siteid, location, systemid, istop) values " _
                    + "('" & cid & "','" & sid & "','" & loc & "','" & sys & "', 'Y') select @@identity"
                    Else
                        sql = "insert into pmlocheir (compid, siteid, location, systemid) values " _
                    + "('" & cid & "','" & sid & "','" & loc & "','" & sys & "') select @@identity"
                    End If

                    locid = main.Scalar(sql)
                    lbllhid.Value = locid
                    sql = "insert into pmlocations (location, description, type, compid, siteid) values " _
                    + "('" & loc & "','" & desc & "','" & typ & "','" & cid & "','" & sid & "') select @@identity"
                    loci = main.Scalar(sql)
                    lbllid.Value = loci
                    lbllocsav.Value = loc
                    GetBelongs(loc)
                    lblstat.Value = "active"
                    lblsys.Value = sys
                    ddsys.Enabled = False
                    'GetArch()
                    GetChildren(loc)
                    imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
                Else 'rel = ''
                    'check par
                    If lblret.Value = "par" Then
                        Dim lid, lhid, rtyp, rsys As String
                        rtyp = lbltyp.Value
                        lhid = lbllhid.Value
                        If lhid <> "" Then
                            lbltyp.Value = ""
                            lbllhid.Value = ""
                            'determine level
                            sql = "usp_cntpar '" & sys & "', '" & lhid & "'"
                            Dim lvl As Integer = main.Scalar(sql)
                            If lvl <> 7 Then
                                sql = "insert into pmlocheir (compid, siteid, location, parent, systemid) values " _
                                + "('" & cid & "','" & sid & "','" & loc & "','" & rel & "','" & sys & "')"
                                main.Update(sql)
                                sql = "select count(parent) from pmlocheir where location = '" & loc & "'"
                                cntchild = main.Scalar(sql)
                                If cntchild > 1 Then
                                    sql = "update pmlocsys set network = 'Y' where systemid = '" & sys & "'"
                                    main.Update(sql)
                                End If
                                typ = ddtype.SelectedItem.ToString
                                typid = ddtype.SelectedValue.ToString
                                sql = "insert into pmlocations (location, description, type, compid, siteid) values " _
                                + "('" & loc & "','" & desc & "','" & typ & "','" & cid & "','" & sid & "') select @@identity"
                                loci = main.Scalar(sql)
                                lbllid.Value = loci
                                lbllocsav.Value = loc
                                lblstat.Value = "active"
                                lblsys.Value = sys
                                ddsys.Enabled = False
                                'GetArch()
                                GetBelongs(loc)
                                GetChildren(loc)
                                imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
                            Else
                                Dim strMessage0 As String = tmod.getmsg("cdstr1543", "pmlocmain.aspx.vb") _
                                + "cannot have a Child Location."
                                Utilities.CreateMessageAlert(Me, strMessage0, "strKey1")
                            End If
                        Else ' lid = ""
                            'this shouldn't happen
                            strMessage = "No parent Location Selected"
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                    Else 'lblret.Value = "par"
                        'this shouldn't happen?

                    End If

                End If
            Else 'cnt = 0
                strMessage = "This Site Already Has A Location With This Name"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else 'loc = 0
            strMessage = "Location ""0"" is Reserved"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub SrchLoc1()
        Dim lid, lhid, typ, ltyp, sys, par, istop As String
        lhid = lbllhid.Value
        Dim loc, eqid As String
        sql = "select isnull(h.parent, '0') as parent, l.locid, h.lhid, h.location, l.description, h.systemid, h.istop, l.eqid, l.type from pmLocHeir h " _
        + "left join pmlocations l on l.location = h.location where h.lhid = '" & lhid & "'"
        dr = main.GetRdrData(sql)
        While dr.Read
            eqid = dr.Item("eqid").ToString
            lbleqid.Value = eqid
            lid = dr.Item("locid").ToString
            lbllid.Value = lid
            istop = dr.Item("istop").ToString
            lbltop.Value = istop
            lhid = dr.Item("lhid").ToString
            lbllhid.Value = lhid
            loc = dr.Item("location").ToString
            txtloc.Text = loc
            lbllocsav.Value = loc
            txtlocdesc.Text = dr.Item("description").ToString
            ltyp = dr.Item("type").ToString
            lbltyp.Value = ltyp
            sys = dr.Item("systemid").ToString
        End While
        dr.Close()

        sql = "select count(*) from pmLocHeir where parent = '" & loc & "'"
        childcnt = main.Scalar(sql)
        lblchildcnt.Value = childcnt

        Dim eqparcnt As Integer
        Dim eqparloccnt As Integer
        If eqid <> "" Then
            sql = "select count(*) from pmlocheir where location = '" & loc & "' and parent in (select location from pmlocations where eqid is not null and eqid <> 0 and type = 'EQUIPMENT')"
            eqparcnt = main.Scalar(sql)
            lbleqparcnt.Value = eqparcnt
            sql = "select count(*) from pmlocheir where location = '" & loc & "' and parent in (select location from pmlocations where (eqid is null or eqid = 0) and type <> 'EQUIPMENT')"
            eqparloccnt = main.Scalar(sql)
            lbleqparloccnt.Value = eqparloccnt
        End If

        If istop = "Y" Then
            cbtop.Checked = True
        Else
            cbtop.Checked = False
        End If
        ddsys.SelectedValue = sys
        lblsys.Value = sys
        ddsys.Enabled = False
        Try
            ddtype.SelectedValue = ltyp
            If ltyp = "EQUIPMENT" Then
                Img2.Disabled = True
                Img2.Src = "../images/appbuttons/minibuttons/deldis.gif"
                Img1.Disabled = True
                Img1.Src = "../images/appbuttons/minibuttons/savedisk1dis.gif"
                imgadd.Disabled = True
                imgadd.Src = "../images/appbuttons/minibuttons/addnewdis.gif"
                ddtype.Enabled = False
            Else
                Img2.Disabled = False
                Img2.Src = "../images/appbuttons/minibuttons/del.gif"
                Img1.Disabled = False
                Img1.Src = "../images/appbuttons/minibuttons/savedisk1.gif"
                imgadd.Disabled = False
                imgadd.Src = "../images/appbuttons/minibuttons/addnew.gif"
                ddtype.Enabled = True
            End If
        Catch ex As Exception

        End Try
        'If typ = "sys" Then
        'sql = "select h.location, l.description, h.systemid, l.type from pmLocHeir h " _
        '+ "left join pmlocations l on l.location = h.location where h.lhid = '" & lhid & "'"
        'Else
        'sql = "select location, description, type from pmlocations where locid = '" & lid & "'"
        'dr = main.GetRdrData(sql)
        'While dr.Read
        'loc = dr.Item("location").ToString
        'txtloc.Text = loc
        'lbllocsav.Value = loc
        'txtlocdesc.Text = dr.Item("description").ToString
        'ltyp = dr.Item("type").ToString
        'End While
        'dr.Close()
        'End If

        lblstat.Value = "active"
        GetBelongs(loc)
        'If typ = "sys" Then
        GetChildren(loc)
        'Else
        'GetChildren(0)
        'End If

        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
        lblref.Value = "go"
        'GetArch()
    End Sub
    Private Sub SrchLoc()
        Dim lid, lhid, typ, ltyp, sys, par, istop As String
        typ = lbltyp.Value
        lhid = lbllhid.Value
        lid = lbllid.Value
        Dim loc, eqid As String
        sql = "select isnull(h.parent, '0') as parent, h.lhid, h.location, l.description, h.systemid, h.istop, l.type, l.eqid from pmLocHeir h " _
        + "left join pmlocations l on l.location = h.location where l.locid = '" & lid & "'"
        dr = main.GetRdrData(sql)
        While dr.Read
            eqid = dr.Item("eqid").ToString
            lbleqid.Value = eqid
            istop = dr.Item("istop").ToString
            lbltop.Value = istop
            lhid = dr.Item("lhid").ToString
            lbllhid.Value = lhid
            loc = dr.Item("location").ToString
            txtloc.Text = loc
            lbllocsav.Value = loc
            txtlocdesc.Text = dr.Item("description").ToString
            ltyp = dr.Item("type").ToString
            sys = dr.Item("systemid").ToString
        End While
        dr.Close()

        sql = "select count(*) from pmLocHeir where parent = '" & loc & "'"
        childcnt = main.Scalar(sql)
        lblchildcnt.Value = childcnt

        Dim eqparcnt As Integer
        Dim eqparloccnt As Integer
        If eqid <> "" Then
            sql = "select count(*) from pmlocheir where location = '" & loc & "' and parent in (select location from pmlocations where eqid is not null and eqid <> 0 and type = 'EQUIPMENT')"
            eqparcnt = main.Scalar(sql)
            lbleqparcnt.Value = eqparcnt
            sql = "select count(*) from pmlocheir where location = '" & loc & "' and parent in (select location from pmlocations where (eqid is null or eqid = 0) and type <> 'EQUIPMENT')"
            eqparloccnt = main.Scalar(sql)
            lbleqparloccnt.Value = eqparloccnt
        End If

        If istop = "Y" Then
            cbtop.Checked = True
        Else
            cbtop.Checked = False
        End If
        ddsys.SelectedValue = sys
        lblsys.Value = sys
        ddsys.Enabled = False
        Try
            ddtype.SelectedValue = ltyp
            If ltyp = "EQUIPMENT" Then
                Img2.Disabled = True
                Img2.Src = "../images/appbuttons/minibuttons/deldis.gif"
                Img1.Disabled = True
                Img1.Src = "../images/appbuttons/minibuttons/savedisk1dis.gif"
                imgadd.Disabled = True
                imgadd.Src = "../images/appbuttons/minibuttons/addnewdis.gif"
                ddtype.Enabled = False
            Else
                Img2.Disabled = False
                Img2.Src = "../images/appbuttons/minibuttons/del.gif"
                Img1.Disabled = False
                Img1.Src = "../images/appbuttons/minibuttons/savedisk1.gif"
                imgadd.Disabled = False
                imgadd.Src = "../images/appbuttons/minibuttons/addnew.gif"
                ddtype.Enabled = True
            End If
        Catch ex As Exception

        End Try
        'If typ = "sys" Then
        'sql = "select h.location, l.description, h.systemid, l.type from pmLocHeir h " _
        '+ "left join pmlocations l on l.location = h.location where h.lhid = '" & lhid & "'"
        'Else
        'sql = "select location, description, type from pmlocations where locid = '" & lid & "'"
        'dr = main.GetRdrData(sql)
        'While dr.Read
        'loc = dr.Item("location").ToString
        'txtloc.Text = loc
        'lbllocsav.Value = loc
        'txtlocdesc.Text = dr.Item("description").ToString
        'ltyp = dr.Item("type").ToString
        'End While
        'dr.Close()
        'End If

        lblstat.Value = "active"
        GetBelongs(loc)
        'If typ = "sys" Then
        GetChildren(loc)
        'Else
        'GetChildren(0)
        'End If

        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
        lblref.Value = "go"
        'GetArch()
    End Sub
    Private Sub GetArch()
        Dim sys As String
        If ddsys.SelectedIndex <> 0 And ddsys.SelectedIndex <> -1 Then
            sys = ddsys.SelectedValue.ToString
        Else
            sys = "PRIMARY"
        End If

        ifarch.Attributes.Add("src", "LocArch.aspx?start=no&sys=" & sys)

    End Sub
    Private Sub GetBelongs(ByVal loc As String)
        Dim locstr, desc, typ, locid As String
        Dim img As String = "../images/appbuttons/minibuttons/magnifier.gif"
        Dim img1 As String = "../images/appbuttons/minibuttons/2PX.gif"
        Dim img3 As String = "../images/appbuttons/minibuttons/magnifierdis.gif"
        Dim nimg As String = "<img src=""" & img3 & """>"
        Dim msg As String = "onmouseover = ""return overlib('" & tmod.getov("cov303", "pmlocmain.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim msg1 As String = "onmouseover = ""return overlib('" & tmod.getov("cov304", "pmlocmain.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim simg As String = "<img src=""" & img & """ onclick=""lookup("
        Dim simg1 As String = ");""" & msg & ">"
        Dim spimg As String = "<img src=""" & img & """ onclick=""jumpto('" & locid & "');""" & msg1 & ">"

        Dim sb As New StringBuilder
        sb.Append("<table width=""692"" cellpadding=""0"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""150"">" & tmod.getlbl("cdlbl589", "pmlocmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=""../images/appbuttons/minibuttons/magnifier.gif""></td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""330"">" & tmod.getlbl("cdlbl590", "pmlocmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""150"">" & tmod.getlbl("cdlbl591", "pmlocmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""22""><img src=""../images/appbuttons/minibuttons/del.gif""></td>")
        sb.Append("<td width=""20"">&nbsp;</td></tr>")

        sb.Append("<tr><td colspan=""6"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 684px;  HEIGHT: 110px"">")
        sb.Append("<table ><tr><td width=""148""></td><td width=""22""></td><td width=""328""></td><td width=""148""></td><td width=""22""></td></tr>")

        Dim sys As String
        sys = ddsys.SelectedValue.ToString

        Dim start As Integer = 0
        Dim lineid As Integer = 0
        Dim lhid, delpar As String
        Dim ccnt As Integer
        sql = "select *, l.locid, l.description, ccnt = (select count(*) from pmlocheir h1 where h1.parent = '" & loc & "') from pmlocheir h left join pmlocations l on l.location = h.location " _
            + "where h.location in (select parent from pmlocheir where location = '" & loc & "' and systemid = '" & sys & "')" ' and l.type <> 'EQUIPMENT'" ' and h.parent is not null"

        dr = main.GetRdrData(sql)
        While dr.Read
            lineid += 1
            If start = 0 Then
                start = 1
                sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdloc1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & simg & lineid & simg1 & "</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tddesc1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdtype1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</tr>")
            End If
            locstr = dr.Item("location").ToString
            lhid = dr.Item("lhid").ToString
            'delpar = dr.Item("delpar").ToString
            desc = dr.Item("description").ToString
            typ = dr.Item("type").ToString
            locid = dr.Item("locid").ToString
            ccnt = dr.Item("ccnt").ToString
            sb.Append("<tr><td class=""plainlabel grayborder"">" & locstr & "</td>")
            sb.Append("<td class=""plainlabel""><img src=""" & img & """ onclick=""jumpto('" & locid & "');""" & msg1 & "></td>")
            sb.Append("<td class=""plainlabel grayborder"">" & desc & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & typ & "</td>")
            If typ <> "EQUIPMENT" Then 'And ccnt = 0
                sb.Append("<td class=""plainlabel grayborder""><img src=""../images/appbuttons/minibuttons/del.gif"" onclick=""delpar('" & locstr & "','" & lhid & "');""></td></tr>")
            Else
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            End If



        End While

        dr.Close()

        Dim i As Integer
        If start = 0 Then
            lineid += 1
            sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdloc1"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel"">" & simg & lineid & simg1 & "</td>")
            sb.Append("<td class=""plainlabel grayborder"" id=""tddesc1"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel grayborder"" id=""tdtype1"">&nbsp;</td>")
            sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            For i = 0 To 5
                sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & nimg & "</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        Else
            For i = 0 To 5
                lineid += 1
                sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdloc" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & simg & lineid & simg1 & "</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tddesc" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdtype" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        End If

        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdbelongs.InnerHtml = sb.ToString

    End Sub
    Private Sub GetChildren(ByVal loc As String)
        Dim locstr, desc, typ, locid, plocstr As String
        Dim img As String = "../images/appbuttons/minibuttons/magnifier.gif"
        Dim img1 As String = "../images/appbuttons/minibuttons/2PX.gif"
        Dim img3 As String = "../images/appbuttons/minibuttons/magnifierdis.gif"
        Dim nimg As String = "<img src=""" & img3 & """>"
        Dim msg As String = "onmouseover = ""return overlib('" & tmod.getov("cov305", "pmlocmain.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim msg1 As String = "onmouseover = ""return overlib('" & tmod.getov("cov306", "pmlocmain.aspx.vb") & "', ABOVE, LEFT)"" onmouseout = ""return nd()"""
        Dim simg As String = "<img src=""" & img & """ onclick=""blookup("
        Dim simg1 As String = ");""" & msg & ">"
        Dim spimg As String = "<img src=""" & img & """ onclick=""jumpto('" & locid & "');""" & msg1 & ">"

        Dim sb As New StringBuilder
        sb.Append("<table width=""692"" cellpadding=""0"">")
        sb.Append("<tr><td class=""btmmenu plainlabel"" width=""150"">" & tmod.getlbl("cdlbl592", "pmlocmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""20""><img src=""../images/appbuttons/minibuttons/magnifier.gif""></td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""330"">" & tmod.getlbl("cdlbl593", "pmlocmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""150"">" & tmod.getlbl("cdlbl594", "pmlocmain.aspx.vb") & "</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""22""><img src=""../images/appbuttons/minibuttons/del.gif""></td>")
        sb.Append("<td width=""20"">&nbsp;</td></tr>")

        sb.Append("<tr><td colspan=""6"" valign=""top""><div style=""OVERFLOW: auto; WIDTH: 684px;  HEIGHT: 110px"">")
        sb.Append("<table ><tr><td width=""148""></td><td width=""22""></td><td width=""328""></td><td width=""148""></td><td width=""22""></td></tr>")

        sid = lblsid.Value
        Dim start As Integer = 0
        Dim lineid As Integer = 0
        sql = "select *, l.locid, l.description from pmlocheir h left join pmlocations l on l.location = h.location where h.parent = '" & loc & "' and h.siteid = '" & sid & "'"

        sql = "select l.locid, h.location, l.description, l.type, h.* from pmlocations l " _
            + "left join pmlocheir h on h.location = l.location " _
            + "where h.parent = '" & loc & "'  and l.siteid = '" & sid & "'" 'and l.type <> 'EQUIPMENT'
        dr = main.GetRdrData(sql)
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdbloc1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & simg & lineid & simg1 & "</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbdesc1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</tr>")
            End If
            locstr = dr.Item("location").ToString
            plocstr = dr.Item("parent").ToString
            desc = dr.Item("description").ToString
            typ = dr.Item("type").ToString
            locid = dr.Item("lhid").ToString
            sb.Append("<tr><td class=""plainlabel grayborder"">" & locstr & "</td>")
            sb.Append("<td class=""plainlabel""><img src=""" & img & """ onclick=""jumpto('" & locid & "');""" & msg1 & "></td>")
            sb.Append("<td class=""plainlabel grayborder"">" & desc & "</td>")
            sb.Append("<td class=""plainlabel grayborder"">" & typ & "</td>")
            If typ <> "EQUIPMENT" Then
                sb.Append("<td class=""plainlabel grayborder""><img src=""../images/appbuttons/minibuttons/del.gif"" onclick=""delchild('" & locid & "','" & plocstr & "');""></td></tr>")
            Else
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            End If



        End While

        dr.Close()

        Dim i As Integer
        If start = 0 Then
            If loc <> "0" Then
                lineid += 1
                sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdbloc1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & simg & lineid & simg1 & "</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbdesc1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype1"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            End If
            For i = 0 To 6
                sb.Append("<tr><td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & nimg & "</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        Else
            For i = 0 To 5
                lineid += 1
                sb.Append("<tr><td class=""plainlabel grayborder"" id=""tdbloc" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel"">" & simg & lineid & simg1 & "</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbdesc" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"" id=""tdbtype" & lineid & """>&nbsp;</td>")
                sb.Append("<td class=""plainlabel grayborder"">&nbsp;</td></tr>")
            Next
        End If

        sb.Append("</table></div></td></tr>")
        sb.Append("</table>")
        tdchildren.InnerHtml = sb.ToString

    End Sub
    Private Sub PopSys(ByVal cid As String)
        sql = "select * from pmlocsys where compid = '" & cid & "'"
        dr = main.GetRdrData(sql)
        ddsys.DataSource = dr
        ddsys.DataTextField = "systemid"
        ddsys.DataValueField = "systemid"
        ddsys.DataBind()
        dr.Close()
        ddsys.Items.Insert(0, "Select System")
    End Sub
    Private Sub PopTypes(ByVal cid As String)
        sql = "select * from pmloctypes where compid = '" & cid & "'" ' and loctype <> 'EQUIPMENT'"
        dr = main.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataTextField = "loctype"
        ddtype.DataValueField = "loctype"
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, "Select Type")
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3057.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3057")
        Catch ex As Exception
        End Try
        Try
            lang3058.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3058")
        Catch ex As Exception
        End Try
        Try
            lang3059.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3059")
        Catch ex As Exception
        End Try
        Try
            lang3060.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3060")
        Catch ex As Exception
        End Try
        Try
            lang3061.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3061")
        Catch ex As Exception
        End Try
        Try
            lang3062.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3062")
        Catch ex As Exception
        End Try
        Try
            lang3063.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3063")
        Catch ex As Exception
        End Try
        Try
            lang3064.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3064")
        Catch ex As Exception
        End Try
        Try
            lang3065.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3065")
        Catch ex As Exception
        End Try
        Try
            lang3066.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3066")
        Catch ex As Exception
        End Try
        Try
            lang3067.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3067")
        Catch ex As Exception
        End Try
        Try
            lang3068.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3068")
        Catch ex As Exception
        End Try
        Try
            lang3069.Text = axlabs.GetASPXPage("pmlocmain.aspx", "lang3069")
        Catch ex As Exception
        End Try

    End Sub

End Class
