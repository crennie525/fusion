

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class LocLook1
    Inherits System.Web.UI.Page
	Protected WithEvents lang3056 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3055 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3054 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3053 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3052 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim look As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim cid, sid, typ As String
    Dim list, retid, retid1, retid2, retid3, retid4, retid5, retid6, eq, pm, jp, did, clid, locid, nc As String
    Protected WithEvents rb2 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltypreturn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllist As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid4 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid5 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid6 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchng As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddsys As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdlist As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
If Not IsPostBack Then
            look.Open()
            typ = Request.QueryString("typ").ToString
            lbltypreturn.Value = typ
            cid = "0"
            lblcid.Value = cid
            Try
                sid = HttpContext.Current.Session("dfltps").ToString
            Catch ex As Exception
                sid = "0"
            End Try

            lblsid.Value = sid
            GetSys()
            ddsys.SelectedValue = "PRIMARY"
            GetTyp()
            GetLocs("sys")
            look.Dispose()
            lbltyp.Value = "sys"
            If typ = "rt" Then
                list = Request.QueryString("list").ToString
                lbllist.Value = list
                retid = Request.QueryString("id").ToString
                lblid.Value = retid
                eq = Request.QueryString("eq").ToString
                lbleq.Value = eq
                retid2 = Request.QueryString("id2").ToString
                lblid2.Value = retid2
                pm = Request.QueryString("pm").ToString
                lblpm.Value = pm
                retid3 = Request.QueryString("id3").ToString
                lblid3.Value = retid3
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                retid4 = Request.QueryString("id4").ToString
                lblid4.Value = retid4
                locid = Request.QueryString("locid").ToString
                lbllocid.Value = locid
                retid5 = Request.QueryString("id5").ToString
                lblid5.Value = retid5
                jp = Request.QueryString("jp").ToString
                lbljp.Value = jp
                retid6 = Request.QueryString("id6").ToString
                lblid6.Value = retid6
                nc = Request.QueryString("nc").ToString
                lblnc.Value = nc
            End If
        Else
            If Request.Form("lblchng") = "sys" Then
                lblchng.value = ""
                look.Open()
                GetLocs("sys")
                look.Dispose()
            ElseIf Request.Form("lblchng") = "typ" Then
                lblchng.Value = ""
                look.Open()
                GetLocs("typ")
                look.Dispose()
                rb2.Checked = True
            ElseIf Request.Form("lblchng") = "rt" Then
                lblchng.Value = ""
                look.Open()
                GetRTDets()
                look.Dispose()
            End If
        End If
    End Sub
    Private Sub GetRTDets()
        Dim sb As StringBuilder = New StringBuilder
        Dim bc, pmid, pmd, pm, eq, eqid, did, dept, clid, cell, loc, jp, jpid, nc, ncid, desc As String
        Dim Filer As String
        sid = lblsid.Value
        locid = lbllocid.Value
        Dim Filter As String = "e.siteid = '" & sid & "' and l.locid = '" & locid & "' "
        did = lbldid.Value
        If did = "" Then
            'Filter += "and d.dept_id is null "
        Else
            Filter += "and d.dept_id = '" & did & "' "
        End If
        clid = lblclid.Value
        If clid = "" Then
            'Filter += "and c.cellid is null "
        Else
            Filter += "and c.cellid = '" & clid & "' "
        End If
        pmid = lblpm.Value
        If pmid = "" Then
            'Filter += "and p.pmid is null "
        Else
            Filter += "and p.pmid = '" & pmid & "' "
        End If
        eqid = lbleq.Value
        If eqid = "" Then
            'Filter += "and p.eqid is null "
        Else
            Filter += "and p.eqid = '" & eqid & "' and l.siteid = '" & sid & "'"
        End If
        ncid = lblnc.Value
        If ncid = "0" Then
            'Filter += "and n.ncid is null "
        Else
            Filter += "and n.ncid = '" & ncid & "' and l.siteid = '" & sid & "'"
        End If
        Dim ccnt As Integer
        sql = "select count(*) " _
        + "from pmlocations l " _
        + "left join equipment e on l.locid = e.locid " _
        + "left join noncritical n on l.locid = n.locid " _
        + "left join pm p on p.eqid = e.eqid " _
        + "left join dept d on e.dept_id = d.dept_id " _
        + "left join cells c on e.cellid = c.cellid " _
        + "where " & Filter
        ccnt = look.Scalar(sql)
        If ccnt > 0 Then
            lblchng.Value = "gort"
        Else
            lblpm.Value = ""
            lbleq.Value = ""
            lblnc.Value = ""
            lbldid.Value = ""
            lblclid.Value = ""
            lblchng.Value = "gort"
        End If
       
       
    End Sub
    Private Sub GetSys()
        cid = lblcid.Value
        sql = "select sysid, systemid from pmlocsys where compid = '" & cid & "'"
        dr = look.GetRdrData(sql)
        ddsys.DataSource = dr
        ddsys.DataValueField = "systemid"
        ddsys.DataTextField = "systemid"
        ddsys.DataBind()
        dr.Close()
        ddsys.Items.Insert(0, "Select System")
        ddsys.Attributes.Add("onchange", "handlesys();")

    End Sub
    Private Sub GetTyp()
        cid = lblcid.Value
        sql = "select typid, loctype from pmloctypes where compid = '" & cid & "'"
        dr = look.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataValueField = "loctype"
        ddtype.DataTextField = "loctype"
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, "Select Type")
        ddtype.Attributes.Add("onchange", "handletyp();")

    End Sub
    Private Sub GetLocs(ByVal typ As String)
        cid = lblcid.Value
        sid = lblsid.Value
        If typ = "sys" Then
            Dim sys As String
            Try
                sys = ddsys.SelectedItem.ToString
            Catch ex As Exception
                sys = "PRIMARY"
            End Try
            sql = "usp_getlocheir1_temp '" & sys & "'"
        Else
            typ = ddtype.SelectedValue.ToString
            sql = "select locid, location, description from pmlocations where compid = '" & cid & "' and siteid = '" & sid & "' and type = '" & typ & "'"
            'sql = "usp_getlocheir2 '" & typ & "'"
            ' Dim ltyp As String = ddtype.SelectedItem.ToString
            'sql = "select l.location,h.parent,h.systemid, l.description from pmlocheir h " _
            '  + "right join pmlocations l on l.location = h.location where l.type = '" & ltyp & "' " _
            ' + "and l.compid = '" & cid & "' and l.siteid = '" & sid & "' order by l.parent, l.location"
        End If
        Dim sb As New StringBuilder
        sb.Append("<div style=""BORDER-RIGHT: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; overflow: scroll; width: 620px; height: 320px;"">")
        sb.Append("<table width=""600"" border=""0""><tr>" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""355""></tr>" & vbCrLf)

        Dim lhid, hold, stat, loc1, loc2, loc3, loc4, loc5, loc6, loc7, desc, chi, chin, ret, loctype, lid As String
        Dim loc8, loc9, loc10, loc11, loc12, loc13, loc14, loc15, loc16, loc17, loc18, loc19, loc20, loc21, loc22, loc23, loc24
        ret = lbltypreturn.Value
        If typ = "sys" Then
            dr = look.GetRdrData(sql)
            While dr.Read
                lhid = dr.Item("lhid").ToString
                hold = dr.Item("hold").ToString
                stat = dr.Item("stat").ToString
                chi = dr.Item("haschild").ToString
                chin = dr.Item("childnum").ToString
                loc1 = dr.Item("location1").ToString
                loc2 = dr.Item("location2").ToString
                loc3 = dr.Item("location3").ToString
                loc4 = dr.Item("location4").ToString
                loc5 = dr.Item("location5").ToString
                loc6 = dr.Item("location6").ToString
                loc7 = dr.Item("location7").ToString
                loc8 = dr.Item("location8").ToString
                loc9 = dr.Item("location9").ToString
                loc10 = dr.Item("location10").ToString
                loc11 = dr.Item("location11").ToString
                loc12 = dr.Item("location12").ToString
                loc13 = dr.Item("location13").ToString
                loc14 = dr.Item("location14").ToString
                loc15 = dr.Item("location15").ToString
                loc16 = dr.Item("location16").ToString
                loc17 = dr.Item("location17").ToString
                loc18 = dr.Item("location18").ToString
                loc19 = dr.Item("location19").ToString
                loc20 = dr.Item("location20").ToString
                loc21 = dr.Item("location21").ToString
                loc22 = dr.Item("location22").ToString
                loc23 = dr.Item("location23").ToString
                loc24 = dr.Item("location24").ToString
                desc = dr.Item("description").ToString
                loctype = dr.Item("loctype").ToString
                lid = dr.Item("lid").ToString

                Select Case stat
                    Case "start"
                        If ret = "loc" Then
                            sb.Append("<tr><td colspan=""25""><a href=""#"" class=""bluelabelu"" onclick=""gotoloc('" & lhid & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<tr><td colspan=""25""><a href=""#"" class=""bluelabelu"" onclick=""gotoloc('" & lhid & "', '" & loc1 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "main"
                        sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""24""><a href=""#"" class=""A2"" onclick=""gotoloc('" & lhid & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""24""><a href=""#"" class=""A2"" onclick=""gotoloc('" & lhid & "', '" & loc1 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If


                    Case "level1"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""23""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc2 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""23""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc2 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc2 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If


                    Case "level2"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""22""><a href=""#"" class=""A2"" onclick=""gotoloc('" & lhid & "');"">" & loc3 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""22""><a href=""#"" class=""A2"" onclick=""gotoloc('" & lhid & "', '" & loc3 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc3 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level3"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""21""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc4 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""21""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc4 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc4 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level4"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""20""><a href=""#"" class=""A2"" onclick=""gotoloc('" & lhid & "');"">" & loc5 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""20""><a href=""#"" class=""A2"" onclick=""gotoloc('" & lhid & "', '" & loc5 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc5 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level5"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""19""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc6 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""19""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc6 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc6 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level6"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""18""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc7 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""18""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc7 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc7 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level7"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""17""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc8 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""17""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc8 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc8 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level8"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""16""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc9 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""16""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc9 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc9 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level9"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""15""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc10 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""15""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc10 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc10 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level10"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""14""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc11 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""14""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc11 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc11 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level11"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""13""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc12 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""13""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc12 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc12 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level12"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""12""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc13 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""12""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc13 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc13 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level13"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""11""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc14 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""11""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc14 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc14 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level14"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""10""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc15 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""10""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc15 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc15 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level15"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""9""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc16 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""9""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc16 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc16 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level16"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""8""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc17 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""8""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc17 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc17 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level17"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""7""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc18 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""7""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc18 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc18 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level18"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""6""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc19 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""6""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc19 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc19 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level19"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""5""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc20 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""5""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc20 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc20 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level20"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""4""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc21 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""4""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc21 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc21 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level21"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""3""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc22 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""3""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc22 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc22 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level22"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""2""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc23 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""2""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc23 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc23 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level23"
                        sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi <> "N" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "', '" + chi + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        If ret = "loc" Then
                            sb.Append("<td colspan=""1""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "');"">" & loc24 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<td colspan=""1""><a href=""#"" class=""A1"" onclick=""gotoloc('" & lhid & "', '" & loc24 & "', '" & desc & "', '" & loctype & "', '" & lid & "');"">" & loc24 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                    Case "level23no"
                        If ret = "loc" Then
                            sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td><img src=""../images/appbuttons/bgbuttons/minus.gif""></td><td colspan=""1""><a href=""#"" class=""A2"" onclick=""gotoloc('" & lhid & "');"">" & loc7 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        Else
                            sb.Append("<tr class=""details"" id='t" + hold + "-" + chin + "'><td></td><td></td><td></td><td></td><td></td><td></td><td><img src=""../images/appbuttons/bgbuttons/minus.gif""></td><td colspan=""1""><a href=""#"" class=""A2"" onclick=""gotoloc('" & lhid & "', '" & loc7 & "', '" & desc & "', '" & loctype & "');"">" & loc7 & " - " & desc & "</a></td></tr>" & vbCrLf)
                        End If

                End Select
            End While
            dr.Close()

        Else
            dr = look.GetRdrData(sql)
            Dim locid As String
            While dr.Read
                locid = dr.Item("locid").ToString
                loc1 = dr.Item("location").ToString
                desc = dr.Item("description").ToString
                If ret = "loc" Then
                    sb.Append("<tr><td colspan=""21""><a href=""#"" class=""bluelabelu"" onclick=""gotoloc('" & locid & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                Else
                    sb.Append("<tr><td colspan=""21""><a href=""#"" class=""bluelabelu"" onclick=""gotoloc('" & locid & "', '" & loc1 & "', '" & desc & "', '" & loctype & "');"">" & loc1 & " - " & desc & "</a></td></tr>" & vbCrLf)
                End If

            End While
            dr.Close()
        End If



        sb.Append("</table></div>" & vbCrLf)
        tdlist.InnerHtml = sb.ToString


    End Sub

    Private Sub ddsys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsys.SelectedIndexChanged

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3052.Text = axlabs.GetASPXPage("LocLook1.aspx", "lang3052")
        Catch ex As Exception
        End Try
        Try
            lang3053.Text = axlabs.GetASPXPage("LocLook1.aspx", "lang3053")
        Catch ex As Exception
        End Try
        Try
            lang3054.Text = axlabs.GetASPXPage("LocLook1.aspx", "lang3054")
        Catch ex As Exception
        End Try
        Try
            lang3055.Text = axlabs.GetASPXPage("LocLook1.aspx", "lang3055")
        Catch ex As Exception
        End Try
        Try
            lang3056.Text = axlabs.GetASPXPage("LocLook1.aspx", "lang3056")
        Catch ex As Exception
        End Try

    End Sub

End Class
