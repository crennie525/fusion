<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LocDialog.aspx.vb" Inherits="lucy_r12.LocDialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Location Lookup Dialog</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<script language="JavaScript" src="../scripts/sessrefdialog.js"></script>
		<script language="JavaScript" src="../scripts1/LocDialogaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="resetsess();pageScroll();">
		<form id="form1" method="post" runat="server">
			<iframe id="ifloc" src="" frameBorder="no" width="100%" height="100%" runat="server"></iframe>
			<iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input type="hidden" id="lbltyp" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
