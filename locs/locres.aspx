﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="locres.aspx.vb" Inherits="lucy_r12.locres" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" src="../scripts/gridnav.js"></script>
    <script language="javascript" type="text/javascript">
        function getloc(lid, lhid) {
            var decision = confirm("Do You Want to Reset the Selected Location?")
            if (decision == true) {
                document.getElementById("lbllhid").value = lhid;
                document.getElementById("lblsubmit").value = "save";
                document.getElementById("form1").submit();
            }
            else {
                alert("Action Cancelled")
            }
        }
        function checkret() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "go") {
                var lhid = document.getElementById("lbllhid").value;
                window.parent.handlereturn(lhid);
            }
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <table>
        <tr>
            <td class="label">
                Search Location
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="160px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="border-bottom: blue 1px solid; border-left: blue 1px solid; width: 300px;
                    height: 300px; overflow: auto; border-top: blue 1px solid; border-right: blue 1px solid"
                    id="divloc" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lbllhid" runat="server" />
    <input type="hidden" id="txtpg" runat="server" /><input type="hidden" id="txtpgcnt"
        runat="server" />
    </form>
</body>
</html>
