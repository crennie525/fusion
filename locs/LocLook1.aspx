<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LocLook1.aspx.vb" Inherits="lucy_r12.LocLook1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LocLook1</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/LocLook1aspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkret();">
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="600">
				<tr>
					<td class="plainlabel" align="center" colSpan="4"><input id="rb1" onclick="handlesysrb();" type="radio" CHECKED name="rbsel"><asp:Label id="lang3052" runat="server">Use System</asp:Label><input id="rb2" onclick="handletyprb();" type="radio" name="rbsel" runat="server"><asp:Label id="lang3053" runat="server">Use Type</asp:Label></td>
				</tr>
				<tr>
					<td><IMG src="../images/appbuttons/minibuttons/6PX.gif"></td>
				</tr>
				<tr>
					<td class="label" width="60"><asp:Label id="lang3054" runat="server">System</asp:Label></td>
					<td width="170"><asp:dropdownlist id="ddsys" runat="server" CssClass="plainlabel" Width="160px"></asp:dropdownlist></td>
					<td class="label" width="60"><asp:Label id="lang3055" runat="server">Type</asp:Label></td>
					<td width="170"><asp:dropdownlist id="ddtype" runat="server" CssClass="plainlabel" Width="160px"></asp:dropdownlist></td>
					<td width="150"></td>
				</tr>
				<tr>
					<td><IMG src="../images/appbuttons/minibuttons/6PX.gif"></td>
				</tr>
				<tr>
					<td class="thdrsing label" colSpan="5"><asp:Label id="lang3056" runat="server">Locations</asp:Label></td>
				</tr>
				<tr>
					<td id="tdlist" vAlign="top" colSpan="5" runat="server"></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lblchng" type="hidden" runat="server"><input id="lbltyp" type="hidden" runat="server">
			<input type="hidden" id="lbltypreturn" runat="server"> <input id="lbllist" type="hidden" runat="server" NAME="lbllist"><input id="lblid" type="hidden" runat="server" NAME="lblid">
			<input id="lblid1" type="hidden" runat="server" NAME="lblid1"><input id="lblid2" type="hidden" runat="server" NAME="lblid2">
			<input id="lbleq" type="hidden" runat="server" NAME="lbleq"> <input id="lblpm" type="hidden" runat="server" NAME="lblpm">
			<input type="hidden" id="lblid3" runat="server" NAME="lblid3"> <input type="hidden" id="lblid4" runat="server" NAME="lblid4">
			<input type="hidden" id="lbldid" runat="server" NAME="lbldid"> <input type="hidden" id="lblclid" runat="server" NAME="lblclid">
			<input type="hidden" id="lbllocid" runat="server" NAME="lbllocid"> <input type="hidden" id="lblid5" runat="server" NAME="lblid5">
			<input type="hidden" id="lbljp" runat="server" NAME="lbljp"> <input type="hidden" id="lblnc" runat="server" NAME="lblnc">
			<input type="hidden" id="lblid6" runat="server" NAME="lblid6"><input type="hidden" id="lblloc" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
