﻿Imports System.Data.SqlClient
Public Class locgetbyeq
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim sid As String
    Dim esrch As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            txtpg.Value = "1"
        Else
            If Request.Form("lblsubmit") = "srch" Then
                esrch.Open()
                eqsrch()
                esrch.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "next" Then
                esrch.Open()
                GetNext()
                esrch.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                esrch.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
               eqsrch()
                esrch.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                esrch.Open()
                GetPrev()
                esrch.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                esrch.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
               eqsrch()
                esrch.Dispose()
                lblsubmit.Value = ""
            End If

        End If
    End Sub
    Private Sub eqsrch()
        Dim sb As New StringBuilder
        sb.Append("<table>")
        sid = lblsid.Value
        Dim srch, ret As String
        Dim eqid, eqnum, eqdesc, loc, parid As String
        srch = txteqnum.Text
        srch = ModString(srch)
        sql = "select count(*) " _
           + "from pmlocations l " _
           + "left join equipment e on e.eqid = l.eqid " _
           + " " _
           + "where l.eqid is not null and l.location like '%" & srch & "%' and l.siteid = '" & sid & "'"
        Dim dc As Integer = esrch.PageCount(sql, PageSize)
        If dc = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & dc
        End If
        txtpgcnt.Value = dc
        sql = "select l.locid as eqlocid, l.eqid, l.location as eqnum, e.eqdesc, location = " _
            + "(select h.parent from pmlocheir h where h.location = l.location), " _
            + "parid = (select l1.locid from pmlocations l1 where l1.location = " _
            + "(select h.parent from pmlocheir h where h.location = l.location)) " _
            + "from pmlocations l " _
            + "left join equipment e on e.eqid = l.eqid " _
            + " " _
            + "where l.eqid is not null and l.location like '%" & srch & "%' and l.siteid = '" & sid & "'"
        sql = "usp_getAllEqPg_loc '" & PageNumber & "', '" & PageSize & "', '" & srch & "','" & sid & "'"
        dr = esrch.GetRdrData(sql)
        While dr.Read
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqnum = Replace(eqnum, "/", "//", , , vbTextCompare)
            eqnum = Replace(eqnum, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqnum = ModString(eqnum)
            eqdesc = dr.Item("eqdesc").ToString
            eqdesc = Replace(eqdesc, "/", "//", , , vbTextCompare)
            eqdesc = Replace(eqdesc, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqdesc = ModString(eqdesc)
            loc = dr.Item("location").ToString
            parid = dr.Item("parid").ToString
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""retsrch('" & eqid & "','" & eqnum & "','" & eqdesc & "','" & loc & "','" & parid & "')"">")
            sb.Append(eqnum & " - " & eqdesc)
            sb.Append("</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        diveq.InnerHtml = sb.ToString
    End Sub
    Private Function ModString(ByVal str As String)
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        Return str
    End Function
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            eqsrch()
        Catch ex As Exception
            esrch.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1632", "reports2.aspx.vb")

            esrch.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            eqsrch()
        Catch ex As Exception
            esrch.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1633", "reports2.aspx.vb")

            esrch.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
End Class