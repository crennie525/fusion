﻿Imports System.Data.SqlClient
Imports System.Text
Public Class locres
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim res As New Utilities
    Dim dr As SqlDataReader
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Dim intPgNav As Integer
    Dim tmod As New transmod
    Dim sloc, lid, lhid, slocs, srch, sid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            res.Open()
            getlocs(PageNumber)
            res.Dispose()
        Else
            If Request.Form("lblsubmit") = "save" Then
                lblsubmit.Value = ""
                res.Open()
                uploc()
                res.Dispose()
                lblsubmit.Value = "go"
            End If
            If Request.Form("lblret") = "next" Then
                res.Open()
                GetNext()
                res.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                res.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                getlocs(PageNumber)
                res.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                res.Open()
                GetPrev()
                res.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                res.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                getlocs(PageNumber)
                res.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            getlocs(PageNumber)
        Catch ex As Exception
            res.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr26", "AppSetDeptTab.aspx.vb")

            res.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getlocs(PageNumber)
        Catch ex As Exception
            res.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr27", "AppSetDeptTab.aspx.vb")

            res.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub uploc()
        lhid = lbllhid.Value
        sql = "update pmlocheir set parent = NULL where lhid = '" & lhid & "'"
        res.Update(sql)

    End Sub
    Private Sub getlocs(ByVal PageNumber As String)
        srch = txtsrch.Text
        srch = res.ModString1(srch)
        sid = lblsid.Value
        Filter = "siteid = '" & sid & "' and type <> 'EQUIPMENT'"
        'Filter = "siteid = ''" & sid & "''"
        If srch <> "" Then
            Filter += " and location like '%" & srch & "%' or description like '%" & srch & "%', and siteid = '" & sid & "'"
            'Filter += " and location like ''%" & srch & "%'' or description like ''%" & srch & "%''"
        End If
        sql = "select Count(*) from pmlocations " _
        + "where " & Filter
        txtpg.Value = PageNumber
        intPgNav = res.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        PK = "locid"
        Tables = "pmlocations"
        Fields = "*, lhid = (select top 1 h.lhid from pmlocheir h where h.location = pmlocations.location)"
        Sort = "location asc"
        Dim sb As New StringBuilder
        sb.Append("<table width=""300""><tr>")
        sb.Append("<td width=""150"" class=""plainlabel""></td>")
        sb.Append("<td width=""150"" class=""plainlabel""></td></tr>")
        dr = res.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        While dr.Read
            sloc = dr.Item("location").ToString
            slocs = dr.Item("description").ToString
            lid = dr.Item("locid").ToString
            lhid = dr.Item("lhid").ToString
            sb.Append("<tr><td><a href=""#"" class=""A1"" onclick=""getloc('" & lid & "','" & lhid & "')"">" & sloc & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & slocs & "</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divloc.InnerHtml = sb.ToString

    End Sub

    Protected Sub ibtnsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        res.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        getlocs(PageNumber)
        res.Dispose()
    End Sub
End Class