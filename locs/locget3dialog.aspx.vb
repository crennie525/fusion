Public Class locget3dialog
    Inherits System.Web.UI.Page
    Dim sid, wonum, typ As String
    Dim who, eqid, eq, fuid, fu, coid, comp, lid, loc, lev, jpid, rlid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iftd As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            typ = Request.QueryString("typ").ToString
            If typ = "wo" Or typ = "woret" Then
                wonum = Request.QueryString("wo").ToString
            ElseIf typ = "jp" Or typ = "jpret" Then
                jpid = Request.QueryString("jpid").ToString
            End If
            Try
                rlid = Request.QueryString("rlid").ToString
                eqid = Request.QueryString("eqid").ToString
                fuid = Request.QueryString("fuid").ToString
                coid = Request.QueryString("coid").ToString
            Catch ex As Exception

            End Try
            Try
                who = Request.QueryString("who").ToString
            Catch ex1 As Exception
                who = ""
            End Try
            Try
                'who = Request.QueryString("who").ToString
                'eqid = Request.QueryString("eqid").ToString
                eq = Request.QueryString("eq").ToString
                'fuid = Request.QueryString("fuid").ToString
                fu = Request.QueryString("fu").ToString
                'coid = Request.QueryString("coid").ToString
                comp = Request.QueryString("comp").ToString
                lid = Request.QueryString("lid").ToString
                loc = Request.QueryString("loc").ToString
                lev = Request.QueryString("lev").ToString
            Catch ex2 As Exception
                'who = ""
                'eqid = ""
                eq = ""
                'fuid = ""
                fu = ""
                'coid = ""
                comp = ""
                lid = ""
                loc = ""
                lev = ""
            End Try
            If typ = "jp" Or typ = "jpret" Then
                iftd.Attributes.Add("src", "locget3.aspx?typ=" + typ + "&sid=" + sid + "&jpid=" + jpid + "&eqid=" + eqid + "&eq=" + eq + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who + "&lev=" + lev + "&rlid=" + rlid + "&date=" + Now)
            Else
                iftd.Attributes.Add("src", "locget3.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wonum + "&eqid=" + eqid + "&eq=" + eq + "&fuid=" + fuid + "&fu=" + fu + "&coid=" + coid + "&comp=" + comp + "&lid=" + lid + "&loc=" + loc + "&who=" + who + "&lev=" + lev + "&rlid=" + rlid + "&date=" + Now)
            End If


        End If
    End Sub

End Class
