﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="locgetbyeq.aspx.vb" Inherits="lucy_r12.locgetbyeq" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" src="../scripts/reportnav.js"  type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function srch() {
            var chk = document.getElementById("txteqnum").value;
            if (chk != "") {
                document.getElementById("lblsubmit").value = "srch";
                document.getElementById("form1").submit();
            }
        }
        function retsrch(eqid, eqnum, eqdesc, loc, parid) {
            //eqid & "','" & eqnum & "','" & eqdesc & "','" & loc & "','" & parid
            var ret = eqid + "~" + eqnum + "~" + eqdesc + "~" + loc + "~" + parid
            window.parent.handlereturn(ret);
        }
        function getnext() {
            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            //alert(cnt + ", " + pg)
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblsubmit").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblsubmit").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblsubmit").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblsubmit").value = "first"
                document.getElementById("form1").submit();
            }
        }
        //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang3359" runat="server">Equipment#</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txteqnum" runat="server" CssClass="plainlabel wd160" Width="160px"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="srch();" src="../images/appbuttons/minibuttons/srchsm.gif" alt="" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="border-bottom: black 1px solid; border-left: black 1px solid; width: 350px;
                        height: 257px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                        id="diveq" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" width="190" align="center">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input id="txtpgcnt" type="hidden" runat="server" />
    <input id="txtpg" type="hidden" runat="server" />
    </form>
</body>
</html>
