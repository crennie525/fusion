﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class locres

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''txtsrch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtsrch As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ibtnsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ibtnsearch As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''divloc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divloc As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ifirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifirst As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''iprev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iprev As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''inext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents inext As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''ilast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ilast As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblsid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblret control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblret As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsubmit As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbllhid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbllhid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpg As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtpgcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpgcnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
