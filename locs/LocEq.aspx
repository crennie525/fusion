<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LocEq.aspx.vb" Inherits="lucy_r12.LocEq" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LocEq</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/LocEqaspx1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
     <!--
         function getlocs1() {
             var sid = document.getElementById("lblsid").value;
             var lid = document.getElementById("lbllid").value;
             var typ = "lu"
             var eqid = "";
             var fuid = "";
             var coid = "";
             //alert(fuid)
             var wo = "";
             var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
             if (eReturn) {
                 //clearall();
                 var ret = eReturn.split("~");
                 document.getElementById("lbleq").value = ret[4];
                 if (ret[4] != "") {
                     document.getElementById("lblsubmit").value = "addeq";
                     document.getElementById("form1").submit();
                 }
                 else {
                    alert("No New Equipment Record Selected!")
                 }
                 /*
                 var ret = eReturn.split("~");
                 document.getElementById("lbllid").value = ret[0];
                 document.getElementById("lblloc").value = ret[1];
                 document.getElementById("tdloc3").innerHTML = ret[1];

                 document.getElementById("lbleq").value = ret[4];
                 document.getElementById("tdeq3").innerHTML = ret[4];
                 document.getElementById("lbleqid").value = ret[3];

                 document.getElementById("lblfuid").value = ret[5];
                 document.getElementById("lblfu").value = ret[6];
                 document.getElementById("tdfu3").innerHTML = ret[6];

                 document.getElementById("lblcoid").value = ret[7];
                 document.getElementById("lblcomp").value = ret[8];

                 document.getElementById("lbllevel").value = ret[9];

                 document.getElementById("trlocs").className = "view";
                 document.getElementById("lblrettyp").value = "locs";
                 var who = document.getElementById("lblwho").value;
                 var did = "";
                 var eqid = ret[3];
                 var clid = "";
                 var fuid = ret[5];
                 var coid = ret[7];
                 var lid = ret[0];

                 var eqnum = ret[4];
                 var dept = "";
                 var cell = "";
                 var loc = ret[1];

                 var typ;
                 typ = "loc";
                 var task = "";
                 //alert(coid)
                 try {
                     window.parent.handlearch(did, clid, lid, sid, typ, eqid, fuid, coid, eqnum, dept, cell, loc)
                 }
                 catch (err) {

                 }
                 window.location = "pmgetopt.aspx?who=" + who + "&jump=yes&cid=0&tli=5&sid=" + sid + "&did=" + did + "&eqid=" + eqid + "&clid=" + clid + "&funid=" + fuid + "&comid=" + coid + "&lid=" + lid + "&typ=" + typ + "&task=" + task;
                 */
             }
         }
         //-->
     </script>
	</HEAD>
	<body class="tbg"  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px" width="680">
				<TBODY>
					<tr>
						<td colspan="3">
							<table>
								<tr height="20">
									<td class="label" width="220" align="left" bgColor="white"><asp:Label id="lang3044" runat="server">Transfer Equipment to this Location</asp:Label></td>
									<td width="60" bgColor="white"><img src="../images/appbuttons/minibuttons/addnew.gif" onclick="getlocs1();"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3"><asp:repeater id="rptreq" runat="server">
								<HeaderTemplate>
									<table cellspacing="0" width="680">
										<tr bgcolor="white" height="20">
											<td class="btmmenu plainlabel" width="130"><asp:Label id="lang3045" runat="server">Equipment#</asp:Label></td>
											<td class="btmmenu plainlabel" width="275"><asp:Label id="lang3046" runat="server">Description</asp:Label></td>
											<td class="btmmenu plainlabel" width="275"><asp:Label id="lang3047" runat="server">Special Identifier</asp:Label></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr class="tbg" height="20">
										<td class="plainlabel">&nbsp;
											<asp:LinkButton ID="Label4"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' Runat = server>
											</asp:LinkButton></td>
										<td class="plainlabel">
											<asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel">
											<asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbllocid" Text='<%# DataBinder.Eval(Container.DataItem,"locid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbldept" Text='<%# DataBinder.Eval(Container.DataItem,"dept_id")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblcell" Text='<%# DataBinder.Eval(Container.DataItem,"cellid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblsite" Text='<%# DataBinder.Eval(Container.DataItem,"siteid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbleqloctypei" Text='<%# DataBinder.Eval(Container.DataItem,"loctype")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</ItemTemplate>
								<AlternatingItemTemplate>
									<tr height="20">
										<td class="plainlabel transrowblue">&nbsp;
											<asp:LinkButton ID="Linkbutton1"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' Runat = server>
											</asp:LinkButton></td>
										<td class="plainlabel transrowblue">
											<asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel transrowblue">
											<asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbllocida" Text='<%# DataBinder.Eval(Container.DataItem,"locid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbldepta" Text='<%# DataBinder.Eval(Container.DataItem,"dept_id")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblcella" Text='<%# DataBinder.Eval(Container.DataItem,"cellid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblsitea" Text='<%# DataBinder.Eval(Container.DataItem,"siteid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
										<td class="details">
											<asp:Label ID="lbleqloctypea" Text='<%# DataBinder.Eval(Container.DataItem,"loctype")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</AlternatingItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></TD></TR></TBODY></TABLE> <input type="hidden" id="lblrd" runat="server" NAME="lblrd">
			<input type="hidden" id="lbleqid" runat="server" NAME="lbleqid"> <input type="hidden" id="lblchk" runat="server" NAME="lblchk">
			<input type="hidden" id="lbldchk" runat="server" NAME="lbldchk"> <input type="hidden" id="lblsid" runat="server" NAME="lblsid">
			<input type="hidden" id="lblcid" runat="server" NAME="lblcid"> <input type="hidden" id="lbldid" runat="server" NAME="lbldid">
			<input type="hidden" id="lblclid" runat="server" NAME="lblclid"><input type="hidden" id="lbluser" runat="server" NAME="lbluser">
			<input type="hidden" id="lbladdchk" runat="server" NAME="lbladdchk"><input type="hidden" id="appchk" runat="server" NAME="appchk">
			<input type="hidden" id="lbllid" runat="server" NAME="lbllid"><input type="hidden" id="lbltyp" runat="server" NAME="lbltyp">
			<input type="hidden" id="lblsubmit" runat="server"><input type="hidden" id="lblro" runat="server">
            <input type="hidden" id="lbleq" runat="server" />
            <input type="hidden" id="lblparloc" runat="server" />
           
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
