<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LocLook.aspx.vb" Inherits="lucy_r12.LocLook" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LocLook</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/LocLookaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="plainlabel" align="center" colSpan="4"><input id="rb1" type="radio" CHECKED name="rbsel"><asp:Label id="lang3048" runat="server">Use System</asp:Label><input id="rb2" type="radio" name="rbsel"><asp:Label id="lang3049" runat="server">Use Type</asp:Label></td>
				</tr>
				<tr>
					<td class="label" width="60"><asp:Label id="lang3050" runat="server">System</asp:Label></td>
					<td width="170"><asp:dropdownlist id="ddsys" runat="server" Width="160px" CssClass="plainlabel"></asp:dropdownlist></td>
					<td class="label" width="60"><asp:Label id="lang3051" runat="server">Type</asp:Label></td>
					<td width="170"><asp:dropdownlist id="ddtype" runat="server" Width="160px" CssClass="plainlabel"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td id="tdlist" colSpan="4" runat="server"></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" runat="server"> <input id="lblsid" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
