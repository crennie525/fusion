

'********************************************************
'*
'********************************************************



Public Class LocDialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim typ As String
    Dim list, sid, retid, retid1, retid2, retid3, retid4, retid5, retid6, eq, pm, jp, did, clid, locid, nc As String
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifloc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        typ = Request.QueryString("typ").ToString
        lbltyp.Value = typ
        If typ = "rt" Then
            list = Request.QueryString("list").ToString
            retid = Request.QueryString("id").ToString
            eq = Request.QueryString("eq").ToString
            retid2 = Request.QueryString("id2").ToString
            pm = Request.QueryString("pm").ToString
            retid3 = Request.QueryString("id3").ToString
            did = Request.QueryString("did").ToString
            clid = Request.QueryString("clid").ToString
            retid4 = Request.QueryString("id4").ToString
            locid = Request.QueryString("locid").ToString
            retid5 = Request.QueryString("id5").ToString
            jp = Request.QueryString("jp").ToString
            retid6 = Request.QueryString("id6").ToString
            nc = Request.QueryString("nc").ToString
            ifloc.Attributes.Add("src", "LocLook1.aspx?typ=" & typ & "&list=" & list & "&id=" & retid & "&eq=" & eq & "&id2=" & retid2 & "&pm=" & pm & "&id3=" & retid3 & "&did=" & did & "&clid=" & clid & "&id4=" & retid4 & "&locid=" & locid & "&id5=" & retid5 & "&jp=" & jp & "&id6=" & retid6 & "&nc=" & nc)
        Else
            ifloc.Attributes.Add("src", "LocLook1.aspx?typ=" & typ)
        End If


    End Sub

End Class
