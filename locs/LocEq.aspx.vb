

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class LocEq
    Inherits System.Web.UI.Page
	Protected WithEvents lang3047 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3046 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3045 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3044 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ds As DataSet
    Dim sql, sqlcnt As String
    Dim eqg As New Utilities
    Dim dept, cell, which As String
    Dim chk, dchk, sid, cid As String
    Dim did, clid, eqid, filt, loc, lid, typ, ro, ustr As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblparloc As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnAddEq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If

        If Not IsPostBack Then
            Dim user As String = Request.QueryString("ustr").ToString
            lbluser.Value = user
            'Try
            lid = Request.QueryString("lid").ToString
            lbllid.Value = lid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            eqg.Open()
            LoadEq(lid)
            eqg.Dispose()
            'Catch ex As Exception

            'End Try
        Else
            If Request.Form("lblsubmit") = "ret" Then
                lblsubmit.Value = ""
                lid = lbllid.Value
                eqg.Open()
                LoadEq(lid)
                eqg.Dispose()
            ElseIf Request.Form("lblsubmit") = "addeq" Then
                lblsubmit.Value = ""
                lid = lbllid.Value
                eqg.Open()
                addeq()
                LoadEq(lid)
                eqg.Dispose()
                lbleq.Value = ""
            End If
        End If
    End Sub
    Private Sub addeq()
        Dim parloc As String = lblparloc.Value
        Dim neweq As String = lbleq.Value
        sql = "update pmlocheir set parent = '" & parloc & "' where location = '" & neweq & "'"
        eqg.Update(sql)
    End Sub
    Private Sub LoadEq(ByVal loc As String)
        Dim cnt As Integer '= 0
        sid = lblsid.Value
        sql = "select e.* from equipment e " _
        + "left join pmlocations l on l.locid = e.locid " _
        + "left join pmlocheir h on h.location = l.location " _
        + "where h.lhid = '" & loc & "' and e.locid is not null"
        sqlcnt = "select count(*) from equipment e " _
         + "left join pmlocations l on l.locid = e.locid " _
        + "left join pmlocheir h on h.location = l.location " _
        + "where h.lhid = '" & loc & "' and e.locid is not null"

        Dim locid As String
        sql = "select location from pmlocations where locid = '" & loc & "' and siteid = '" & sid & "'"
        'locid = eqg.strScalar(sql)

        'l.locid
        sql = "select e.*, '" & locid & "', h.location, l.description, l.type from pmlocations l " _
            + "left join pmlocheir h on h.location = l.location " _
            + "left join equipment e on e.eqid = l.eqid " _
            + "where h.parent = '" & locid & "' and l.type = 'EQUIPMENT' and l.siteid = '" & sid & "'"
        sqlcnt = "select count(*) from pmlocations l " _
                + "left join pmlocheir h on h.location = l.location " _
                + "where h.parent = '" & locid & "' and l.type = 'EQUIPMENT' and l.siteid = '" & sid & "'"

        sql = "select location from pmlocations where locid = '" & lid & "'"
        Dim parloc As String
        parloc = eqg.strScalar(sql)
        lblparloc.Value = parloc
        sql = "select * from equipment e " _
        + "left join pmlocheir h on h.location = e.eqnum where h.parent = '" & parloc & "' order by e.eqnum"
        sqlcnt = "select count(*) from equipment e " _
        + "left join pmlocheir h on h.location = e.eqnum where h.parent = '" & parloc & "'"

        cnt = eqg.Scalar(sqlcnt)
        ds = eqg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptreq.DataSource = dt
        Dim i, eint As Integer
        eint = 15
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptreq.DataBind()

    End Sub

    Private Sub rptreq_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptreq.ItemCommand
        If e.CommandName = "Select" Then
            'eid = dr.Item("eqid").ToString
            'sid = dr.Item("siteid").ToString
            'did = dr.Item("dept_id").ToString
            'clid = dr.Item("cellid").ToString
            'lid = dr.Item("locid").ToString

            Dim chk As String
            If e.Item.ItemType = ListItemType.Item Then
                eqid = CType(e.Item.FindControl("lbleqiditem"), Label).Text
                clid = CType(e.Item.FindControl("lblcell"), Label).Text
                did = CType(e.Item.FindControl("lbldept"), Label).Text
                sid = CType(e.Item.FindControl("lblsite"), Label).Text
                lid = CType(e.Item.FindControl("lbllocid"), Label).Text
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                eqid = CType(e.Item.FindControl("lbleqidalt"), Label).Text
                clid = CType(e.Item.FindControl("lblcella"), Label).Text
                did = CType(e.Item.FindControl("lbldepta"), Label).Text
                sid = CType(e.Item.FindControl("lblsitea"), Label).Text
                lid = CType(e.Item.FindControl("lbllocida"), Label).Text
            End If
            If clid <> "" Then
                chk = "yes"
            Else
                chk = "no"
            End If
            lbleqid.Value = eqid
            lblclid.Value = clid
            lbldid.Value = did
            lblsid.Value = sid
            'lbllid.Value = lid
            lblchk.Value = chk
            lbldchk.Value = "go"
        End If

    End Sub
	



    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang3044 As Label
                lang3044 = CType(e.Item.FindControl("lang3044"), Label)
                lang3044.Text = axlabs.GetASPXPage("LocEq.aspx", "lang3044")
            Catch ex As Exception
            End Try
            Try
                Dim lang3045 As Label
                lang3045 = CType(e.Item.FindControl("lang3045"), Label)
                lang3045.Text = axlabs.GetASPXPage("LocEq.aspx", "lang3045")
            Catch ex As Exception
            End Try
            Try
                Dim lang3046 As Label
                lang3046 = CType(e.Item.FindControl("lang3046"), Label)
                lang3046.Text = axlabs.GetASPXPage("LocEq.aspx", "lang3046")
            Catch ex As Exception
            End Try
            Try
                Dim lang3047 As Label
                lang3047 = CType(e.Item.FindControl("lang3047"), Label)
                lang3047.Text = axlabs.GetASPXPage("LocEq.aspx", "lang3047")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3044.Text = axlabs.GetASPXPage("LocEq.aspx", "lang3044")
        Catch ex As Exception
        End Try
        Try
            lang3045.Text = axlabs.GetASPXPage("LocEq.aspx", "lang3045")
        Catch ex As Exception
        End Try
        Try
            lang3046.Text = axlabs.GetASPXPage("LocEq.aspx", "lang3046")
        Catch ex As Exception
        End Try
        Try
            lang3047.Text = axlabs.GetASPXPage("LocEq.aspx", "lang3047")
        Catch ex As Exception
        End Try

    End Sub

End Class
