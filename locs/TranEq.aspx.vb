

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class TranEq
    Inherits System.Web.UI.Page
	Protected WithEvents lang3076 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3075 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3074 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3073 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3072 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3071 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3070 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim fail As New Utilities
    Dim cid, eqid, fuid, co, cod, coid, cvalu, app, sid, Login, lid, lhid, ro As String
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbassigned As System.Web.UI.WebControls.ListBox
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbunassigned As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblocations As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbchecked As System.Web.UI.WebControls.ListBox
    Protected WithEvents todisa As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsys As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()

        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        Page.EnableViewState = True
        If Not IsPostBack Then
            'Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btntocomp.Visible = False
                btnfromcomp.Visible = False
                Imagebutton1.Visible = False
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")
                todisa.Attributes.Add("class", "view")
                cbopts.Enabled = False
            End If
            lhid = Request.QueryString("lhid").ToString
            lbllhid.Value = lhid
            If Len(lhid) <> 0 AndAlso lhid <> "" AndAlso lhid <> "0" Then
                cid = HttpContext.Current.Session("comp").ToString
                lblcid.Value = cid
                lblsid.Value = HttpContext.Current.Session("dfltps").ToString
                lblopt.Value = "0"
                lbassigned.AutoPostBack = False
                lbunassigned.AutoPostBack = False
                lblocations.AutoPostBack = False
                fail.Open()
                GetLocID()
                PopAssigned()
                PopUnAssigned()
                PopLocations()
                fail.Dispose()
            Else
                Dim strMessage As String = tmod.getmsg("cdstr1544" , "TranEq.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End If
            'Catch ex As Exception
            'Dim strMessage As String =  tmod.getmsg("cdstr1545" , "TranEq.aspx.vb")
 
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'lbllog.Value = "noeqid"
            'End Try

        End If
        'btnaddfail.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnaddfail.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")

    End Sub
    Private Sub GetLocID()
        lhid = lbllhid.Value
        Dim loc, sys As String
        sql = "select l.locid, l.location, h.systemid from pmLocHeir h " _
           + "left join pmlocations l on l.location = h.location where h.lhid = '" & lhid & "'"
        dr = fail.GetRdrData(sql)
        While dr.Read
            lid = dr.Item("locid").ToString
            loc = dr.Item("location").ToString
            sys = dr.Item("systemid").ToString
            tdcomp.InnerHtml = dr.Item("location").ToString
        End While
        dr.Close()
        lbllid.Value = lid
        lblloc.Value = loc
    End Sub
    Private Sub PopAssigned()
        lid = lbllid.Value
        sql = "select cast(e.eqid as varchar(50)) + ',' + cast(l.locid as varchar(50)) as eqid, " _
        + "e.eqnum + ' - ' + l.location as eqnum " _
        + "from equipment e " _
        + "left join pmlocations l on l.locid = e.locid " _
        + "where e.locid <> 0 and e.locid is not null and " _
        + "e.eqid not in (select e.eqid from equipment e where locid = '" & lid & "') order by eqnum"
        Dim parloc As String
        sql = "select location from pmlocations where locid = '" & lid & "'"
        parloc = fail.strScalar(sql)

        sql = "select l.eqid, l.location + ' - ' + h.parent as 'eqnum' from pmlocations l left join pmlocheir h on h.location = l.location " _
            + "where h.parent = '" & parloc & "' and h.siteid = '" & sid & "' order by l.location"
        dr = fail.GetRdrData(sql)
        lbassigned.DataSource = dr
        lbassigned.DataValueField = "eqid"
        lbassigned.DataTextField = "eqnum"
        lbassigned.DataBind()
        dr.Close()
        Try
            lbassigned.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopLocations()
        lid = lbllid.Value
        Dim parloc As String
        sql = "select location from pmlocations where locid = '" & lid & "'"
        parloc = fail.strScalar(sql)
        sql = "select e.eqid, e.eqnum + ' - ' + h.parent as eqnum from equipment e " _
        + "left join pmlocheir h on h.location = e.eqnum where h.parent = '" & parloc & "' order by e.eqnum"
        dr = fail.GetRdrData(sql)
        lblocations.DataSource = dr
        lblocations.DataValueField = "eqid"
        lblocations.DataTextField = "eqnum"
        lblocations.DataBind()
        dr.Close()
        Try
            lblocations.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub PopUnAssigned()
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        lid = lbllid.Value
        Dim dt, val, filt As String
        dt = "equipment"
        val = "eqid, eqnum"
        filt = " where siteid = '" & sid & "' and (locid = 0 or locid is null)"
        Dim parloc As String
        sql = "select location from pmlocations where locid = '" & lid & "'"
        parloc = fail.strScalar(sql)
        sql = "select e.eqid, e.eqnum + ' - ' + h.parent as eqnum from equipment e " _
        + "left join pmlocheir h on h.location = e.eqnum where h.parent <> '" & parloc & "' or (e.locid is null or e.locid = 0) " _
        + "and e.siteid = '" & sid & "' order by e.eqnum"
        'dr = fail.GetList(dt, val, filt)
        dr = fail.GetRdrData(sql)
        lbunassigned.DataSource = dr
        lbunassigned.DataTextField = "eqnum"
        lbunassigned.DataValueField = "eqid"
        lbunassigned.DataBind()
        dr.Close()
    End Sub
    Private Sub ToLoc()
        'fail.Dispose()
        cid = lblcid.Value
        Dim Item As ListItem
        Dim f, fi As String
        fail.Open()
        For Each Item In lbunassigned.Items
            If Item.Selected Then
                fi = Item.Value.ToString
                f = Item.Text.ToString
                GetItems(fi, f)
            End If
        Next
        PopLocations()
        PopUnAssigned()
        fail.Dispose()
    End Sub
    Private Sub TransLoc()
        'fail.Dispose()
        cid = lblcid.Value
        Dim Item As ListItem
        Dim f, fi, par As String
        fail.Open()
        For Each Item In lbassigned.Items
            If Item.Selected Then
                fi = Item.Value.ToString
                f = Item.Text.ToString
                'Dim fia() As String = fi.Split(",")
                'fi = fia(0)
                'f = fia(1)
                sql = "select parent from pmlocheir where location = '" & f & "'"
                par = fail.strScalar(sql)
                Dim citem As ListItem = New ListItem(f, par)
                If Not lbchecked.Items.Contains(citem) Then
                    lbchecked.Items.Add(citem)
                End If
                GetItems(fi, f)
            End If
        Next
        PopLocations()
        PopAssigned()
        fail.Dispose()
    End Sub
    Private Sub GetItems(ByVal eqid As String, ByVal eqnum As String)
        cid = lblcid.Value
        lid = lbllid.Value
        sid = lblsid.Value
        Dim loc As String = lblloc.Value
        Dim sys As String = lblsys.Value
        Dim fcnt As Integer
        'sql = "update equipment set locid = '" & lid & "' where eqid = '" & eqid & "'"
        Dim elid, edid, eclid As String
        Dim ecnt As Integer
        sql = "select count(*) from pmlocations where location = '" & eqnum & "'" 'and siteid = '" & sid & "'"
        ecnt = fail.Scalar(sql)
        If ecnt <> 0 Then
            sql = "update pmlocheir set parent = loc where location = '" & loc & "' and siteid = '" & sid & "'"
            fail.Update(sql)
        Else
            sql = "insert into pmlocations (location, type, siteid, eqid, compid) " _
            + "values('" & eqnum & "', 'EQUIPMENT', '" & sid & "', '" & eqid & "', '0') " _
            + "declare @newloc int " _
            + "select @newloc = @@identity " _
            + "update equipment set locid = @newloc where eqid = '" & eqid & "' " _
            + "insert into pmlocheir(compid, siteid, location, parent, systemid, haschildren) " _
            + "values('0', '" & sid & "', '" & eqnum & "', '" & loc & "', '" & sys & "', 'N')"
        End If


    End Sub
    Private Sub FromLoc()
        cid = lblcid.Value
        Dim Item As ListItem
        Dim f, fi, ci, ei, li As String
        Dim tflag As Integer = 0
        fail.Open()
        For Each Item In lblocations.Items
            If Item.Selected Then
                fi = Item.Value.ToString
                Dim citem As ListItem
                For Each citem In lbchecked.Items
                    ci = citem.Value.ToString
                    If ci = fi Then
                        ei = citem.Value.ToString
                        li = citem.ToString
                        'RemItems(ei, li)
                    End If
                Next
                If ci = "" Then
                    RemItems(fi)
                Else
                    Dim ritem As ListItem = New ListItem(li, ei)
                    If lbchecked.Items.Contains(ritem) Then
                        lbchecked.Items.Remove(ritem)
                    End If
                End If
            End If
        Next
        PopLocations()
        PopUnAssigned()
        PopAssigned()
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal eqid As String, Optional ByVal tlid As String = "0")
        cid = lblcid.Value
        lid = lbllid.Value

        Dim fcnt As Integer
        If tlid = "0" Then
            sql = "update equipment set locid = NULL where eqid = '" & eqid & "'"
        Else
            sql = "update equipment set locid = '" & tlid & "' where eqid = '" & eqid & "'"
        End If

        fail.Update(sql)
    End Sub
    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToLoc()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromLoc()
    End Sub

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        TransLoc()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3070.Text = axlabs.GetASPXPage("TranEq.aspx", "lang3070")
        Catch ex As Exception
        End Try
        Try
            lang3071.Text = axlabs.GetASPXPage("TranEq.aspx", "lang3071")
        Catch ex As Exception
        End Try
        Try
            lang3072.Text = axlabs.GetASPXPage("TranEq.aspx", "lang3072")
        Catch ex As Exception
        End Try
        Try
            lang3073.Text = axlabs.GetASPXPage("TranEq.aspx", "lang3073")
        Catch ex As Exception
        End Try
        Try
            lang3074.Text = axlabs.GetASPXPage("TranEq.aspx", "lang3074")
        Catch ex As Exception
        End Try
        Try
            lang3075.Text = axlabs.GetASPXPage("TranEq.aspx", "lang3075")
        Catch ex As Exception
        End Try
        Try
            lang3076.Text = axlabs.GetASPXPage("TranEq.aspx", "lang3076")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
