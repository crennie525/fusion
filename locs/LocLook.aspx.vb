

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class LocLook
    Inherits System.Web.UI.Page
	Protected WithEvents lang3051 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3050 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3049 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3048 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim look As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim cid, sid As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdlist As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddsys As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            look.Open()
            cid = "0"
            lblcid.Value = cid
            sid = HttpContext.Current.Session("dfltps").ToString() '"12"
            lblsid.Value = sid
            GetLocs("sys")
            look.Dispose()
        End If
    End Sub
    Private Sub GetLocs(ByVal typ As String)
        cid = lblcid.Value
        sid = lblsid.Value
        If typ = "sys" Then
            Dim sys As String
            Try
                sys = ddsys.SelectedItem.ToString
            Catch ex As Exception
                sys = "PRIMARY"
            End Try
            sql = "usp_getlocheir '" & sys & "'"
            'sql = "select h.location,h.parent,h.systemid, l.description from pmlocheir h " _
            '+ "left join pmlocations l on l.location = h.location where h.systemid = '" & sys & "' " _
            '+ "and h.compid = '" & cid & "' and h.siteid = '" & sid & "' order by h.parent, h.location"
        Else
            Dim ltyp As String = ddtype.SelectedItem.ToString
            sql = "select l.location,h.parent,h.systemid, l.description from pmlocheir h " _
             + "right join pmlocations l on l.location = h.location where l.type = '" & ltyp & "' " _
             + "and l.compid = '" & cid & "' and l.siteid = '" & sid & "' order by l.parent, l.location"
        End If
        Dim sb As New StringBuilder
        sb.Append("<table width=""500"" border=""1""><tr>" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""15"">" & vbCrLf)
        sb.Append("<td width=""410""></tr>" & vbCrLf)

        Dim lhid, loc, desc, par, chi, stat, lhidhold, lhidholdsav As String
        Dim level, cnt, cnthold, lvlcnt, lvlhold, lvlflag, cnt1hold As Integer
        Dim start As Integer = 0
        lvlflag = 0
        dr = look.GetRdrData(sql)
        While dr.Read
            lhid = dr.Item("lhid").ToString
            loc = dr.Item("location").ToString
            desc = dr.Item("description").ToString
            par = dr.Item("parent").ToString
            chi = dr.Item("haschildren").ToString
            level = dr.Item("lvl").ToString
            cnt = dr.Item("cnt").ToString

            lhidhold = dr.Item("lhidhold").ToString
            stat = dr.Item("stat").ToString

            If level <> 0 And stat = "main" Then
                'lvlcnt = level
                'lvlhold = level

                cnthold = cnt
                lvlflag = 1
            End If
            If stat = "1" Then
                lhidholdsav = lhidhold
            End If

            If typ = "sys" Then
                If start = 0 Then
                    start = 1
                    sb.Append("<tr><td colspan=""8""><a href=""#"" class=""A1"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                Else
                    If stat = "main" Then
                        sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                        If chi = "Y" Then
                            sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "');""" & vbCrLf)
                            sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                        Else
                            sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                        End If
                        sb.Append("<td colspan=""8""><a href=""#""  class=""A2"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                        If lvlflag = 1 And chi = "Y" Then
                            cnthold = cnt
                            sb.Append("<tr><td></td><td colspan=""7""><table class=""details"" id='t" + lhid + "'>" & vbCrLf)
                        End If
                    Else
                        Select Case stat
                            Case 1
                                'If lhidholdsav = lhidhold Then
                                'sb.Append("</table></td></tr>")
                                'sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                                'Else

                                'End If
                                sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "');""" & vbCrLf)
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                                Else
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                                End If
                                'cnthold -= 1
                                'If cnthold <> 0 Then
                                sb.Append("<td colspan=""7""><a href=""#""  class=""A2"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                                'Else
                                '    sb.Append("<td colspan=""8""><a href=""#""  class=""A2"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                                'End If

                                If chi = "Y" Then
                                    sb.Append("<tr><td></td><td></td><td colspan=""6""><table class=""details"" id='t" + lhid + "'>" & vbCrLf)
                                End If

                            Case 2
                                sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "');""" & vbCrLf)
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                                Else
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                                End If
                                sb.Append("<td colspan=""6""><a href=""#""  class=""A2"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("<tr><td></td><td></td><td colspan=""5""><table class=""details"" id='t" + lhid + "'>" & vbCrLf)
                                End If
                            Case 3
                                sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "');""" & vbCrLf)
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                                Else
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                                End If
                                sb.Append("<td colspan=""5""><a href=""#""  class=""A2"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("<tr><td></td><td></td><td colspan=""4""><table class=""details"" id='t" + lhid + "'>" & vbCrLf)
                                End If
                            Case 4
                                sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "');""" & vbCrLf)
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                                Else
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                                End If
                                sb.Append("<td colspan=""4""><a href=""#""  class=""A2"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("<tr><td></td><td></td><td colspan=""3""><table class=""details"" id='t" + lhid + "'>" & vbCrLf)
                                End If
                            Case 5
                                sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "');""" & vbCrLf)
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                                Else
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                                End If
                                sb.Append("<td colspan=""3""><a href=""#""  class=""A2"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("<tr><td></td><td></td><td colspan=""2""><table class=""details"" id='t" + lhid + "'>" & vbCrLf)
                                End If
                            Case 6
                                sb.Append("<tr><td><img id='i" + lhid + "' " & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("onclick=""fclose('t" + lhid + "', 'i" + lhid + "');""" & vbCrLf)
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                                Else
                                    sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>" & vbCrLf)
                                End If
                                sb.Append("<td colspan=""2""><a href=""#""  class=""A2"" onlick=""gotoloc('" & loc & "');"">" & loc & " - " & desc & "</a></td></tr>" & vbCrLf)
                                If chi = "Y" Then
                                    sb.Append("<tr><td></td><td></td><td colspan=""1""><table class=""details"" id='t" + lhid + "'>" & vbCrLf)
                                End If
                        End Select
                        If lvlflag = 1 Then
                            If level = 0 Then
                                Dim i As Integer
                                'Dim ito As Integer
                                'If Convert.ToInt32(stat) > 2 Then
                                'ito = Convert.ToInt32(stat)
                                'Else
                                'ito = Convert.ToInt32(stat)
                                'End If
                                If stat > 1 Then
                                    For i = 1 To Convert.ToInt32(stat) - cnthold
                                        sb.Append("</table></td></tr>")
                                    Next

                                Else
                                    For i = 1 To Convert.ToInt32(stat)
                                        sb.Append("</table></td></tr>")
                                    Next

                                End If
                                cnthold -= 1

                                'If cnthold = 0 Then
                                'cnthold = 1
                                'End If
                                'lvlflag = 0
                                'Dim chk As String = cnthold
                            End If
                            Else
                        If chi = "N" Then
                            sb.Append("</table></td></tr>")
                        End If
                        End If
                    End If
                End If
            End If

        End While
        dr.Close()
        sb.Append("</table>" & vbCrLf)
        tdlist.InnerHtml = sb.ToString


    End Sub

    Private Sub ddsys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsys.SelectedIndexChanged

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3048.Text = axlabs.GetASPXPage("LocLook.aspx","lang3048")
		Catch ex As Exception
		End Try
		Try
			lang3049.Text = axlabs.GetASPXPage("LocLook.aspx","lang3049")
		Catch ex As Exception
		End Try
		Try
			lang3050.Text = axlabs.GetASPXPage("LocLook.aspx","lang3050")
		Catch ex As Exception
		End Try
		Try
			lang3051.Text = axlabs.GetASPXPage("LocLook.aspx","lang3051")
		Catch ex As Exception
		End Try

	End Sub

End Class
