<%@ Page Language="vb" AutoEventWireup="false" Codebehind="LocDets.aspx.vb" Inherits="lucy_r12.LocDets" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LocDets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/LocDetsaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" style="LEFT: 8px; POSITION: absolute">
			<tr><td><img src="../images/appbuttons/minibuttons/4PX.gif"></td></tr>
				<tr height="20">
					<td class="bluelabel"><asp:Label id="lang3040" runat="server">Belongs To:</asp:Label></td>
				</tr>
				<tr>
					<td id="tdbelongs" runat="server"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr height="20">
				
					<td class="bluelabel"><asp:Label id="lang3041" runat="server">Child Locations:</asp:Label></td>
				</tr>
				<tr>
					<td id="tdchildren" runat="server"></td>
				</tr>
				<tr>
				<td class="plainlabelred" align="center"><asp:Label id="lang3042" runat="server">The PM Location Feature is included as a convenience for PM data transfer to certain CMMS systems.</asp:Label><br><asp:Label id="lang3043" runat="server">Locations Are Not Required for standard Fusion Operations.</asp:Label></td>
				</tr>
			</table>
			<input type="hidden" id="lblloc" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
