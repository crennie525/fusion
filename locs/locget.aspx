<%@ Page Language="vb" AutoEventWireup="false" Codebehind="locget.aspx.vb" Inherits="lucy_r12.locget" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>locget</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="javascript" type="text/javascript">
		function getecd0(lidi, lid, loc, ok) {
			document.getElementById("lbllid").value=lid;
			document.getElementById("lblloc").value=loc;
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			
			if(ok=="yes") {
			document.getElementById("lbllevel").value="1";
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			
		}
		function getecd1(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllidi1").value=lidi;
			document.getElementById("lbllid1").value=lid;
			document.getElementById("lblloc1").value=loc;
			document.getElementById("tdecd1").innerHTML=lid;
			document.getElementById("lbllevel").value="2";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd1").className="greenlabel";
			}
		}
		function getecd2(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid2").value=lid;
			document.getElementById("lbllidi2").value=lidi;
			document.getElementById("lblloc2").value=loc;
			document.getElementById("tdecd2").innerHTML=lid;
			document.getElementById("lbllevel").value="3";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd2").className="greenlabel";
			}
		}
		function getecd3(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid3").value=lid;
			document.getElementById("lbllidi3").value=lidi;
			document.getElementById("lblloc3").value=loc;
			document.getElementById("tdecd3").innerHTML=lid;
			document.getElementById("lbllevel").value="4";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd3").className="greenlabel";
			}
		}
		function getecd4(lidi, lid, loc, eq, ok) {
		
			document.getElementById("lbllid4").value=lid;
			document.getElementById("lbllidi4").value=lidi;
			document.getElementById("lblloc4").value=loc;
			document.getElementById("tdecd4").innerHTML=lid;
			document.getElementById("lbllevel").value="5";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd4").className="greenlabel";
			}
		}
		function getecd5(lidi, lid, loc, eq, ok) {
		//alert(eq)
			document.getElementById("lbllid5").value=lid;
			document.getElementById("lbllidi5").value=lidi;
			document.getElementById("lblloc5").value=loc;
			document.getElementById("tdecd5").innerHTML=lid;
			document.getElementById("lbllevel").value="6";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd5").className="greenlabel";
			}
		}
		function getecd6(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid6").value=lid;
			document.getElementById("lbllidi6").value=lidi;
			document.getElementById("lblloc6").value=loc;
			document.getElementById("tdecd6").innerHTML=lid;
			document.getElementById("lbllevel").value="7";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd6").className="greenlabel";
			}
		}
		function getecd7(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid7").value=lid;
			document.getElementById("lbllidi7").value=lidi;
			document.getElementById("lblloc7").value=loc;
			document.getElementById("tdecd7").innerHTML=lid;
			document.getElementById("lbllevel").value="8";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd7").className="greenlabel";
			}
		}
		function getecd8(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid8").value=lid;
			document.getElementById("lbllidi8").value=lidi;
			document.getElementById("lblloc8").value=loc;
			document.getElementById("tdecd8").innerHTML=lid;
			document.getElementById("lbllevel").value="9";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd8").className="greenlabel";
			}
		}
		function getecd9(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid9").value=lid;
			document.getElementById("lbllidi9").value=lidi;
			document.getElementById("lblloc9").value=loc;
			document.getElementById("tdecd9").innerHTML=lid;
			document.getElementById("lbllevel").value="10";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd9").className="greenlabel";
			}
		}
		function getecd10(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid10").value=lid;
			document.getElementById("lbllidi10").value=lidi;
			document.getElementById("lblloc10").value=loc;
			document.getElementById("tdecd10").innerHTML=lid;
			document.getElementById("lbllevel").value="11";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd10").className="greenlabel";
			}
		}
		function getecd11(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid11").value=lid;
			document.getElementById("lbllidi11").value=lidi;
			document.getElementById("lblloc11").value=loc;
			document.getElementById("tdecd11").innerHTML=lid;
			document.getElementById("lbllevel").value="12";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd11").className="greenlabel";
			}
		}
		function getecd12(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid12").value=lid;
			document.getElementById("lbllidi12").value=lidi;
			document.getElementById("lblloc12").value=loc;
			document.getElementById("tdecd12").innerHTML=lid;
			document.getElementById("lbllevel").value="13";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd12").className="greenlabel";
			}
		}
		function getecd13(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid13").value=lid;
			document.getElementById("lblloc13").value=loc;
			document.getElementById("lbllidi13").value=lidi;
			document.getElementById("tdecd13").innerHTML=lid;
			document.getElementById("lbllevel").value="14";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd13").className="greenlabel";
			}
		}
		function getecd14(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid14").value=lid;
			document.getElementById("lbllidi14").value=lidi;
			document.getElementById("lblloc14").value=loc;
			document.getElementById("tdecd14").innerHTML=lid;
			document.getElementById("lbllevel").value="15";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd14").className="greenlabel";
			}
		}
		function getecd15(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid15").value=lid;
			document.getElementById("lbllidi15").value=lidi;
			document.getElementById("lblloc15").value=loc;
			document.getElementById("tdecd15").innerHTML=lid;
			document.getElementById("lbllevel").value="16";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd15").className="greenlabel";
			}
		}
		function getecd16(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid16").value=lid;
			document.getElementById("lbllidi16").value=lidi;
			document.getElementById("lblloc16").value=loc;
			document.getElementById("tdecd16").innerHTML=lid;
			document.getElementById("lbllevel").value="17";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd16").className="greenlabel";
			}
		}
		function getecd17(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid17").value=lid;
			document.getElementById("lblloc17").value=loc;
			document.getElementById("lbllidi17").value=lidi;
			document.getElementById("tdecd17").innerHTML=lid;
			document.getElementById("lbllevel").value="18";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd17").className="greenlabel";
			}
		}
		function getecd18(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid18").value=lid;
			document.getElementById("lblloc18").value=loc;
			document.getElementById("lbllidi18").value=lidi;
			document.getElementById("tdecd18").innerHTML=lid;
			document.getElementById("lbllevel").value="19";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd18").className="greenlabel";
			}
		}
		function getecd19(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid19").value=lid;
			document.getElementById("lbllidi19").value=lidi;
			document.getElementById("lblloc19").value=loc;
			document.getElementById("tdecd19").innerHTML=lid;
			document.getElementById("lbllevel").value="20";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd19").className="greenlabel";
			}
		}
		function getecd20(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid20").value=lid;
			document.getElementById("lbllidi20").value=lidi;
			document.getElementById("lblloc20").value=loc;
			document.getElementById("tdecd20").innerHTML=lid;
			document.getElementById("lbllevel").value="21";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd20").className="greenlabel";
			}
		}
		function getecd21(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid21").value=lid;
			document.getElementById("lbllidi21").value=lidi;
			document.getElementById("lblloc21").value=loc;
			document.getElementById("tdecd21").innerHTML=lid;
			document.getElementById("lbllevel").value="22";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd21").className="greenlabel";
			}
		}
		function getecd22(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid22").value=lid;
			document.getElementById("lbllidi22").value=lidi;
			document.getElementById("lblloc22").value=loc;
			document.getElementById("tdecd22").innerHTML=lid;
			document.getElementById("lbllevel").value="23";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd22").className="greenlabel";
			}
		}
		function getecd23(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid23").value=lid;
			document.getElementById("lbllidi23").value=lidi;
			document.getElementById("lblloc23").value=loc;
			document.getElementById("tdecd23").innerHTML=lid;
			document.getElementById("lbllevel").value="24";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd123").className="greenlabel";
			}
		}
		function getecd24(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid24").value=lid;
			document.getElementById("lbllidi24").value=lidi;
			document.getElementById("lblloc24").value=loc;
			document.getElementById("tdecd24").innerHTML=lid;
			document.getElementById("lbllevel").value="25";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			
			document.getElementById("lblsubmit").value="checkl1";
			document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd24").className="greenlabel";
			}
		}
		function getecd25(lidi, lid, loc, eq, ok) {
			document.getElementById("lbllid25").value=lid;
			document.getElementById("lbllidi25").value=lidi;
			document.getElementById("lblloc25").value=loc;
			document.getElementById("tdecd25").innerHTML=lid;
			document.getElementById("lbllevel").value="26";
			document.getElementById("lblretlid").value=lid;
			document.getElementById("lblretlidi").value=lidi;
			document.getElementById("lblretloc").value=loc;
			if(ok=="yes") {
			//document.getElementById("lbllevel").value="25";
			//document.getElementById("lblsubmit").value="checkl1";
			//document.getElementById("form1").submit();
			}
			if(eq!="") {
			document.getElementById("lbleqid").value=eq;
			document.getElementById("tdecd25").className="greenlabel";
			}
		}
		
		
		function refit() {
		var sid = document.getElementById("lblsid").value;
		var wo = document.getElementById("lblwonum").value;
		var typ = document.getElementById("lbltyp").value;
		window.location = "locget.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&date=" + Date();
		//window.location = "http://www.laiproducts.com/canton_locs/locs/locget.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo + "&date=" + Date();
		}
		
		function retit() {
		var typ = document.getElementById("lbltyp").value;
		var eq = document.getElementById("lbleqid").value;
		var lid = document.getElementById("lblretlid").value;
        var loc = document.getElementById("lblretloc").value;
        if(lid==""&&loc=="") {
        alert("No Level Selected")
        }
        else {
		if(typ=="wo") {
			if(eq=="") {
				var decision = confirm("No Equipment Record Selected\nAre you sure you want to Save Changes and Return?")
				if(decision==true) {
					document.getElementById("lblsubmit").value="savewo";
					document.getElementById("form1").submit();
				}
				else {
					alert("Action Cancelled")
				}
			}
			else {
				document.getElementById("lblsubmit").value="savewo";
				document.getElementById("form1").submit();
			}
		}
		else {
			goback();
		}
		}
		}
		
		function goback() {
        var lidi = document.getElementById("lblretlidi").value;
        var lid = document.getElementById("lblretlid").value;
        var loc = document.getElementById("lblretloc").value;
        var eq = document.getElementById("lbleqid").value;
		var ret;
		ret = lidi + "~" + lid + "~" + loc + "~" + eq;
		window.parent.handlereturn(ret);
		//alert("Return Values would be " + lid + " and " + loc)
		}
		
		function checkit() {
		var chk = document.getElementById("lblsubmit").value;
		if(chk=="return") {
		goback();
		}
		}
		
		function gettab(who) {
		var chk = document.getElementById("lbltabs").value;
		var chkarr = chk.split(",");
		var aflg = 0;
		if(who=="l0") {
		aflg = 1;
		closeall();
		document.getElementById("tdec").className="view";
		document.getElementById("tdtab").innerHTML="Level 0";
		}
		else if(who=="l1") {
			if(chkarr[1]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec1").className="view";
			document.getElementById("tdtab").innerHTML="Level 1";
			}
		}
		else if(who=="l2") {
			if(chkarr[2]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec2").className="view";
			document.getElementById("tdtab").innerHTML="Level 2";
			}
		}
		else if(who=="l3") {
			if(chkarr[3]=="ok") 
			aflg = 1;{
			closeall();
			document.getElementById("tdec3").className="view";
			document.getElementById("tdtab").innerHTML="Level 3";
			}
		}
		else if(who=="l4") {
			if(chkarr[4]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec4").className="view";
			document.getElementById("tdtab").innerHTML="Level 4";
			}
		}
		else if(who=="l5") {
			if(chkarr[5]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec5").className="view";
			document.getElementById("tdtab").innerHTML="Level 5";
			}
		}
		else if(who=="l6") {
			if(chkarr[6]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec6").className="view";
			document.getElementById("tdtab").innerHTML="Level 6";
			}
		}
		else if(who=="l7") {
			if(chkarr[7]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec7").className="view";
			document.getElementById("tdtab").innerHTML="Level 7";
			}
		}
		else if(who=="l8") {
			if(chkarr[8]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec8").className="view";
			document.getElementById("tdtab").innerHTML="Level 8";
			}
		}
		else if(who=="l9") {
			if(chkarr[9]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec9").className="view";
			document.getElementById("tdtab").innerHTML="Level 9";
			}
		}
		else if(who=="l10") {
			if(chkarr[10]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec10").className="view";
			document.getElementById("tdtab").innerHTML="Level 10";
			}
		}
		
		else if(who=="l11") {
			if(chkarr[11]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec11").className="view";
			document.getElementById("tdtab").innerHTML="Level 11";
			}
		}
		else if(who=="l12") {
			if(chkarr[12]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec12").className="view";
			document.getElementById("tdtab").innerHTML="Level 12";
			}
		}
		else if(who=="l13") {
			if(chkarr[13]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec13").className="view";
			document.getElementById("tdtab").innerHTML="Level 13";
			}
		}
		else if(who=="l14") {
			if(chkarr[14]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec14").className="view";
			document.getElementById("tdtab").innerHTML="Level 14";
			}
		}
		else if(who=="l15") {
			if(chkarr[15]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec15").className="view";
			document.getElementById("tdtab").innerHTML="Level 15";
			}
		}
		else if(who=="l16") {
			if(chkarr[16]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec16").className="view";
			document.getElementById("tdtab").innerHTML="Level 16";
			}
		}
		else if(who=="l17") {
			if(chkarr[17]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec17").className="view";
			document.getElementById("tdtab").innerHTML="Level 17";
			}
		}
		else if(who=="l18") {
			if(chkarr[18]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec18").className="view";
			document.getElementById("tdtab").innerHTML="Level 18";
			}
		}
		else if(who=="l19") {
			if(chkarr[19]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec19").className="view";
			document.getElementById("tdtab").innerHTML="Level 19";
			}
		}
		else if(who=="l20") {
			if(chkarr[120]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec20").className="view";
			document.getElementById("tdtab").innerHTML="Level 20";
			}
		}
		else if(who=="l21") {
			if(chkarr[121]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec21").className="view";
			document.getElementById("tdtab").innerHTML="Level 21";
			}
		}
		else if(who=="l22") {
			if(chkarr[122]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec22").className="view";
			document.getElementById("tdtab").innerHTML="Level 22";
			}
		}
		else if(who=="l23") {
			if(chkarr[123]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec23").className="view";
			document.getElementById("tdtab").innerHTML="Level 23";
			}
		}
		else if(who=="l24") {
			if(chkarr[124]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec24").className="view";
			document.getElementById("tdtab").innerHTML="Level 24";
			}
		}
		else if(who=="l25") {
			if(chkarr[125]=="ok") {
			aflg = 1;
			closeall();
			document.getElementById("tdec25").className="view";
			document.getElementById("tdtab").innerHTML="Level 25";
			}
		}
		if(aflg==0) {
		alert("No Data Retrieved for the Level Yet")
		}
		}
		
		function closeall() {
		document.getElementById("tdec").className="details";
		document.getElementById("tdec1").className="details";
		document.getElementById("tdec2").className="details";
		document.getElementById("tdec3").className="details";
		document.getElementById("tdec4").className="details";
		document.getElementById("tdec5").className="details";
		document.getElementById("tdec6").className="details";
		document.getElementById("tdec7").className="details";
		document.getElementById("tdec8").className="details";
		document.getElementById("tdec9").className="details";
		document.getElementById("tdec10").className="details";
		document.getElementById("tdec11").className="details";
		document.getElementById("tdec12").className="details";
		document.getElementById("tdec13").className="details";
		document.getElementById("tdec14").className="details";
		document.getElementById("tdec15").className="details";
		document.getElementById("tdec16").className="details";
		document.getElementById("tdec17").className="details";
		document.getElementById("tdec18").className="details";
		document.getElementById("tdec19").className="details";
		document.getElementById("tdec20").className="details";
		document.getElementById("tdec21").className="details";
		document.getElementById("tdec22").className="details";
		document.getElementById("tdec23").className="details";
		document.getElementById("tdec24").className="details";
		document.getElementById("tdec25").className="details";
		}
		</script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td align="center">
						<table>
							<tr>
								<td id="tdtab" class="label" runat="server">Level 0</td>
							</tr>
							<tr>
								<td id="tdec" runat="server"></td>
								<td id="tdec1" class="details" runat="server"></td>
								<td id="tdec2" class="details" runat="server"></td>
								<td id="tdec3" class="details" runat="server"></td>
								<td id="tdec4" class="details" runat="server"></td>
								<td id="tdec5" class="details" runat="server"></td>
								<td id="tdec6" class="details" runat="server"></td>
								<td id="tdec7" class="details" runat="server"></td>
								<td id="tdec8" class="details" runat="server"></td>
								<td id="tdec9" class="details" runat="server"></td>
								<td id="tdec10" class="details" runat="server"></td>
								<td id="tdec11" class="details" runat="server"></td>
								<td id="tdec12" class="details" runat="server"></td>
								<td id="tdec13" class="details" runat="server"></td>
								<td id="tdec14" class="details" runat="server"></td>
								<td id="tdec15" class="details" runat="server"></td>
								<td id="tdec16" class="details" runat="server"></td>
								<td id="tdec17" class="details" runat="server"></td>
								<td id="tdec18" class="details" runat="server"></td>
								<td id="tdec19" class="details" runat="server"></td>
								<td id="tdec20" class="details" runat="server"></td>
								<td id="tdec21" class="details" runat="server"></td>
								<td id="tdec22" class="details" runat="server"></td>
								<td id="tdec23" class="details" runat="server"></td>
								<td id="tdec24" class="details" runat="server"></td>
								<td id="tdec25" class="details" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" class="plainlabelred">
						When Link above is Blue this Level has Child Locations<br>
						When Link above is Red this Level has No Child Locations<br>
						When Link above is Green this Level represents an Equipment Record<br>
						<br>
						Clicking on a Previous Level Label below will allow you to review that Level<br>
					</td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel" height="20" width="60"><A onclick="gettab('l0');" href="#">Level 
										0</A></td>
								<td id="tdecd" class="plainlabel" width="120" runat="server"></td>
								<td class="bluelabel" height="20" width="60"><A onclick="gettab('l9');" href="#">Level 
										9</A></td>
								<td id="tdecd9" class="plainlabel" width="120" runat="server"></td>
								<td class="bluelabel" height="20" width="60"><A onclick="gettab('l18');" href="#">Level 
										18</A></td>
								<td id="tdecd18" class="plainlabel" width="120" runat="server"></td>
								<td width="20"><IMG onclick="refit();" src="../images/appbuttons/minibuttons/refreshit.gif"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="20"><A onclick="gettab('l1');" href="#">Level 1</A></td>
								<td id="tdecd1" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l10');" href="#">Level 10</A></td>
								<td id="tdecd10" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l19');" href="#">Level 19</A></td>
								<td id="tdecd19" class="plainlabel" runat="server"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="20"><A onclick="gettab('l2');" href="#">Level 2</A></td>
								<td id="tdecd2" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l11');" href="#">Level 11</A></td>
								<td id="tdecd11" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l20');" href="#">Level 20</A></td>
								<td id="tdecd20" class="plainlabel" runat="server"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="20"><A onclick="gettab('l3');" href="#">Level 3</A></td>
								<td id="tdecd3" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l12');" href="#">Level 12</A></td>
								<td id="tdecd12" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l21');" href="#">Level 21</A></td>
								<td id="tdecd21" class="plainlabel" runat="server"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="20"><A onclick="gettab('l4');" href="#">Level 4</A></td>
								<td id="tdecd4" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20" width="60"><A onclick="gettab('l13');" href="#">Level 
										13</A></td>
								<td id="tdecd13" class="plainlabel" width="120" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l22');" href="#">Level 22</A></td>
								<td id="tdecd22" class="plainlabel" runat="server"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="20"><A onclick="gettab('l5');" href="#">Level 5</A></td>
								<td id="tdecd5" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l14');" href="#">Level 14</A></td>
								<td id="tdecd14" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l23');" href="#">Level 23</A></td>
								<td id="tdecd23" class="plainlabel" runat="server"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="20"><A onclick="gettab('l6');" href="#">Level 6</A></td>
								<td id="tdecd6" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l15');" href="#">Level 15</A></td>
								<td id="tdecd15" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l24');" href="#">Level 24</A></td>
								<td id="tdecd24" class="plainlabel" runat="server"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="20"><A onclick="gettab('l7');" href="#">Level 7</A></td>
								<td id="tdecd7" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l16');" href="#">Level 16</A></td>
								<td id="tdecd16" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l25');" href="#">Level 25</A></td>
								<td id="tdecd25" class="plainlabel" runat="server"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="20"><A onclick="gettab('l8');" href="#">Level 8</A></td>
								<td id="tdecd8" class="plainlabel" runat="server"></td>
								<td class="bluelabel" height="20"><A onclick="gettab('l17');" href="#">Level 17</A></td>
								<td id="tdecd17" class="plainlabel" runat="server"></td>
								<td></td>
								<td></td>
								<td><IMG onclick="retit();" src="../images/appbuttons/minibuttons/savedisk1.gif"></td>
							</tr>
							<tr id="trsave" runat="server" class="details">
								<td colSpan="7" align="right"><IMG onclick="retit();" src="../images/appbuttons/minibuttons/savedisk1.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"> <input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbltyp" type="hidden" name="lbltyp" runat="server"> <input id="lblsys" type="hidden" name="lblsys" runat="server">
			<input id="lblltyp" type="hidden" name="lblltyp" runat="server"> <input id="lblwonum" type="hidden" name="lblwonum" runat="server">
			<input id="lbltabs" type="hidden" name="lbltabs" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lbleq" type="hidden" name="lbleq" runat="server"> <input id="lbllid" type="hidden" name="lbllid" runat="server">
			<input id="lblloc" type="hidden" name="lblloc" runat="server"> <input id="lbllid1" type="hidden" name="lbllid1" runat="server">
			<input id="lblloc1" type="hidden" name="lblloc1" runat="server"> <input id="lbllid2" type="hidden" name="lbllid2" runat="server">
			<input id="lblloc2" type="hidden" name="lblloc2" runat="server"> <input id="lbllid3" type="hidden" name="lbllid3" runat="server">
			<input id="lblloc3" type="hidden" name="lblloc3" runat="server"> <input id="lbllid4" type="hidden" name="lbllid4" runat="server">
			<input id="lblloc4" type="hidden" name="lblloc4" runat="server"> <input id="lbllid5" type="hidden" name="lbllid5" runat="server">
			<input id="lblloc5" type="hidden" name="lblloc5" runat="server"> <input id="lbllid6" type="hidden" name="lbllid6" runat="server">
			<input id="lblloc6" type="hidden" name="lblloc6" runat="server"> <input id="lbllid7" type="hidden" name="lbllid7" runat="server">
			<input id="lblloc7" type="hidden" name="lblloc7" runat="server"> <input id="lbllid8" type="hidden" name="lbllid8" runat="server">
			<input id="lblloc8" type="hidden" name="lblloc8" runat="server"> <input id="lbllid9" type="hidden" name="lbllid9" runat="server">
			<input id="lblloc9" type="hidden" name="lblloc9" runat="server"> <input id="lbllid10" type="hidden" name="lbllid10" runat="server">
			<input id="lblloc10" type="hidden" name="lblloc10" runat="server"> <input id="lbllid11" type="hidden" name="lbllid11" runat="server">
			<input id="lblloc11" type="hidden" name="lblloc11" runat="server"> <input id="lbllid12" type="hidden" name="lbllid12" runat="server">
			<input id="lblloc12" type="hidden" name="lblloc12" runat="server"> <input id="lbllid13" type="hidden" name="lbllid13" runat="server">
			<input id="lblloc13" type="hidden" name="lblloc13" runat="server"> <input id="lbllid14" type="hidden" name="lbllid14" runat="server">
			<input id="lblloc14" type="hidden" name="lblloc14" runat="server"> <input id="lbllid15" type="hidden" name="lbllid15" runat="server">
			<input id="lblloc15" type="hidden" name="lblloc15" runat="server"> <input id="lbllid16" type="hidden" name="lbllid16" runat="server">
			<input id="lblloc16" type="hidden" name="lblloc16" runat="server"> <input id="lbllid17" type="hidden" name="lbllid17" runat="server">
			<input id="lblloc17" type="hidden" name="lblloc17" runat="server"> <input id="lbllid18" type="hidden" name="lbllid18" runat="server">
			<input id="lblloc18" type="hidden" name="lblloc18" runat="server"> <input id="lbllid19" type="hidden" name="lbllid19" runat="server">
			<input id="lblloc19" type="hidden" name="lblloc19" runat="server"> <input id="lbllid20" type="hidden" name="lbllid20" runat="server">
			<input id="lblloc20" type="hidden" name="lblloc20" runat="server"> <input id="lbllid21" type="hidden" name="lbllid21" runat="server">
			<input id="lblloc21" type="hidden" name="lblloc21" runat="server"> <input id="lbllid22" type="hidden" name="lbllid22" runat="server">
			<input id="lblloc22" type="hidden" name="lblloc22" runat="server"> <input id="lbllid23" type="hidden" name="lbllid23" runat="server">
			<input id="lblloc23" type="hidden" name="lblloc23" runat="server"> <input id="lbllid24" type="hidden" name="lbllid24" runat="server">
			<input id="lblloc24" type="hidden" name="lblloc24" runat="server"> <input id="lbllid25" type="hidden" name="lbllid25" runat="server">
			<input id="lblloc25" type="hidden" name="lblloc25" runat="server"> <input id="lbllevel" type="hidden" runat="server">
			<input id="lblretlid" type="hidden" runat="server"> <input id="lblretloc" type="hidden" runat="server">
			<input id="lblretlidi" type="hidden" runat="server"> <input id="lbllidi1" type="hidden" name="lbllidi1" runat="server">
			<input id="lbllidi2" type="hidden" name="lbllidi2" runat="server"> <input id="lbllidi3" type="hidden" name="lbllidi3" runat="server">
			<input id="lbllidi4" type="hidden" name="lbllidi4" runat="server"> <input id="lbllidi5" type="hidden" name="lbllidi5" runat="server">
			<input id="lbllidi6" type="hidden" name="lbllidi6" runat="server"> <input id="lbllidi7" type="hidden" name="lbllidi7" runat="server">
			<input id="lbllidi8" type="hidden" name="lbllidi8" runat="server"> <input id="lbllidi9" type="hidden" name="lbllidi9" runat="server">
			<input id="lbllidi10" type="hidden" name="lbllidi10" runat="server"> <input id="lbllidi11" type="hidden" name="lbllidi11" runat="server">
			<input id="lbllidi12" type="hidden" name="lbllidi12" runat="server"> <input id="lbllidi13" type="hidden" name="lbllidi13" runat="server">
			<input id="lbllidi14" type="hidden" name="lbllidi14" runat="server"> <input id="lbllidi15" type="hidden" name="lbllidi15" runat="server">
			<input id="lbllidi16" type="hidden" name="lbllidi16" runat="server"> <input id="lbllidi17" type="hidden" name="lbllidi17" runat="server">
			<input id="lbllidi18" type="hidden" name="lbllidi18" runat="server"> <input id="lbllidi19" type="hidden" name="lbllidi19" runat="server">
			<input id="lbllidi20" type="hidden" name="lbllidi20" runat="server"> <input id="lbllidi21" type="hidden" name="lbllidi21" runat="server">
			<input id="lbllidi22" type="hidden" name="lbllidi22" runat="server"> <input id="lbllidi23" type="hidden" name="lbllidi23" runat="server">
			<input id="lbllidi24" type="hidden" name="lbllidi24" runat="server"> <input id="lbllidi25" type="hidden" name="lbllidi25" runat="server">
		</form>
	</body>
</HTML>
