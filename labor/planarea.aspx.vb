Imports System.Data.SqlClient
Public Class planarea
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim wa As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Protected WithEvents lblwret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaidret As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lang2969a As System.Web.UI.WebControls.Label
    Protected WithEvents lblskill As System.Web.UI.WebControls.Label
    Protected WithEvents lang113 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgskill As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocreturn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocadd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            lblsid.Value = "12" 'HttpContext.Current.Session("dfltps").ToString
            wa.Open()
            LoadWAS(PageNumber)
            wa.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                wa.Open()
                GetNext()
                wa.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                wa.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadWAS(PageNumber)
                wa.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                wa.Open()
                GetPrev()
                wa.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                wa.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadWAS(PageNumber)
                wa.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadWAS(PageNumber)
        Catch ex As Exception
            wa.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr102", "AppSetTaskTabST.aspx.vb")

            wa.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadWAS(PageNumber)
        Catch ex As Exception
            wa.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr103", "AppSetTaskTabST.aspx.vb")

            wa.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub LoadWAS(ByVal PageNumber As Integer)
        Dim srch, sid As String
        srch = txtsrch.Text
        srch = wa.ModString1(srch)
        sid = lblsid.Value
        If Len(srch) > 0 Then
            Filter = "(planarea like ''%" & srch & "%'' or workarea like ''%" & srch & "%'' or wadesc like ''%" & srch & "%'') and siteid = ''" & sid & "''"
            FilterCnt = "(planarea like '%" & srch & "%' or workarea like '%" & srch & "%' or wadesc like '%" & srch & "%') and siteid = '" & sid & "'"
        Else
            Filter = "siteid = ''" & sid & "''"
            FilterCnt = "siteid = '" & sid & "'"
        End If
        sql = "select count(*) " _
                + "from workplngareas where " & FilterCnt 'compid = '" & cid & "'"
        dgskill.VirtualItemCount = wa.Scalar(sql)
        If dgskill.VirtualItemCount = 0 Then
            lblskill.Text = "No Planning Area Records Found"
            Tables = "workplngareas"
            PK = "wpaid"
            PageSize = "200"
            dr = wa.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgskill.DataSource = dr
            dgskill.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgskill.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgskill.PageCount
        Else

            Tables = "workplngareas"
            PK = "wpaid"
            PageSize = "200"
            dr = wa.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgskill.DataSource = dr
            dgskill.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgskill.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgskill.PageCount
        End If
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub dgskill_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.ItemCommand
        If e.CommandName = "Add" Then
            wa.Open()
            Dim pname, lname, ldesc, lloc, sid, locadd, id2 As String
            pname = CType(e.Item.FindControl("txtnewplanarea"), TextBox).Text
            pname = wa.ModString1(pname)

            lname = CType(e.Item.FindControl("txtnewworkarea"), Label).Text
            lname = wa.ModString1(lname)

            ldesc = CType(e.Item.FindControl("txtnewwadesc"), Label).Text
            ldesc = wa.ModString1(ldesc)

            If lblwret.Value = "yes" Then
                id2 = lblwaidret.Value
                lblwret.Value = "no"
                sql = "select workarea, wadesc from workareas where waid = '" & id2 & "'"
                dr = wa.GetRdrData(sql)
                While dr.Read
                    lname = dr.Item("workarea").ToString
                    ldesc = dr.Item("wadesc").ToString
                End While
                dr.Close()
                If lname = "" Then
                    Dim strMessage As String = "Work Area Required"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    wa.Dispose()
                    Exit Sub
                End If
            End If

            sid = lblsid.Value
            If Len(pname) > 50 Then
                Dim strMessage As String = "Planning Area Limited To 50 Characters"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                wa.Dispose()
                Exit Sub
            End If

            sql = "select count(*) from workplngareas where lower(workarea) = '" & lname & "' and lower(planarea) = '" & pname & "' and siteid = '" & sid & "'"
            Dim strchk As Integer
            strchk = wa.Scalar(sql)
            If strchk = 0 Then
                If Len(pname) > 0 Then
                    sql = "insert into workplngareas (planarea, workarea, wadesc, siteid, waid) values ('" & pname & "','" & lname & "','" & ldesc & "','" & sid & "','" & id2 & "')"

                    'wa.Update(sql)
                    txtsrch.Text = ""
                    PageNumber = txtpg.Value
                    LoadWAS(PageNumber)

                Else
                    Dim strMessage As String = "Planning Area Required"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    wa.Dispose()
                    Exit Sub
                End If
            Else
                Dim strMessage As String = "Cannot Enter Duplicate Planning Area \ Work Area Combination"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

            End If
            wa.Dispose()
        End If
    End Sub

    Private Sub dgskill_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("lblplanarea"), Label).Text
        dgskill.EditItemIndex = e.Item.ItemIndex
        wa.Open()
        LoadWAS(PageNumber)
        wa.Dispose()
    End Sub

    Private Sub dgskill_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.UpdateCommand
        wa.Open()
        Dim id, id2, desc, workarea, sid, loc, locadd, planarea As String
        id = CType(e.Item.FindControl("lblwpaid"), Label).Text
        id2 = CType(e.Item.FindControl("lblwaid"), Label).Text
        planarea = CType(e.Item.FindControl("txtplanarea"), TextBox).Text
        planarea = wa.ModString1(planarea)
        workarea = CType(e.Item.FindControl("txtworkarea"), Label).Text
        workarea = wa.ModString1(workarea)
        desc = CType(e.Item.FindControl("txtwadesc"), Label).Text
        desc = wa.ModString1(desc)
        sql = "select workarea, wadesc from workareas where waid = '" & id2 & "'"
        dr = wa.GetRdrData(sql)
        While dr.Read
            workarea = dr.Item("workarea").ToString
            desc = dr.Item("wadesc").ToString
        End While
        dr.Close()
        If Len(planarea) > 50 Then
            Dim strMessage As String = "Planning Area Limited To 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            wa.Dispose()
            Exit Sub
        End If

        sid = lblsid.Value
        Dim old As String = lblold.Value
        If old <> workarea Then
            Dim faill As String = workarea.ToLower
            Dim failll As String = planarea.ToLower
            sql = "select count(*) from workplngareas where lower(workarea) = '" & faill & "' and lower(planarea) = '" & failll & "' and siteid = '" & sid & "'"
            Dim strchk As Integer
            strchk = wa.Scalar(sql)
            If strchk = 0 Then
                Try
                    sql = "update workareas set planarea = '" & planarea & "', waid = '" & id2 & "', workarea = " _
                    + "'" & workarea & "', wadesc = '" & desc & "' " _
                    + "where wpaid = '" & id & "'"
                    'wa.Update(sql)
                    dgskill.EditItemIndex = -1
                    PageNumber = txtpg.Value
                    LoadWAS(PageNumber)
                Catch ex As Exception
                    Dim strMessage As String = "Problem Saving Data"

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
            Else
                Dim strMessage As String = "Cannot Enter Duplicate Planning Area \ Work Area Combination"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                wa.Dispose()
            End If
        ElseIf old = planarea Then
            Try
                sql = "update workareas set planarea = '" & planarea & "', waid = '" & id2 & "', workarea = " _
                     + "'" & workarea & "', wadesc = '" & desc & "' " _
                     + "where wpaid = '" & id & "'"
                'wa.Update(sql)
                dgskill.EditItemIndex = -1
                PageNumber = txtpg.Value
                LoadWAS(PageNumber)
            Catch ex As Exception
                Dim strMessage As String = "Problem Saving Data"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        Else
            dgskill.EditItemIndex = -1
            PageNumber = txtpg.Value
            LoadWAS(PageNumber)
        End If

        wa.Dispose()
    End Sub

    Private Sub dgskill_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.CancelCommand
        dgskill.EditItemIndex = -1
        wa.Open()
        PageNumber = txtpg.Value
        LoadWAS(PageNumber)
        wa.Dispose()
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        wa.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadWAS(PageNumber)
        wa.Dispose()
    End Sub

    Private Sub dgskill_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgskill.ItemDataBound
        Dim sid As String = lblsid.Value
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim lblwa As Label = CType(e.Item.FindControl("txtworkarea"), Label)
            Dim lblwadesc As Label = CType(e.Item.FindControl("txtwadesc"), Label)
            Dim lblwaid As Label = CType(e.Item.FindControl("lblwaid"), Label)
            Dim wa As String
            wa = lblwa.ClientID.ToString
            Dim wadesc As String
            wadesc = lblwadesc.ClientID.ToString
            Dim waid As String
            waid = lblwaid.ClientID.ToString
            Dim imgwa As HtmlImage = CType(e.Item.FindControl("imgwa"), HtmlImage)
            imgwa.Attributes.Add("onclick", "getwa('" & wa & "','" & wadesc & "','" & waid & "');")
        ElseIf e.Item.ItemType = ListItemType.Footer Then
            Dim lblwa As Label = CType(e.Item.FindControl("txtnewworkarea"), Label)
            Dim lblwadesc As Label = CType(e.Item.FindControl("txtnewwadesc"), Label)
            Dim wa As String
            wa = lblwa.ClientID.ToString
            Dim wadesc As String
            wadesc = lblwadesc.ClientID.ToString
            Dim imgwa As HtmlImage = CType(e.Item.FindControl("imgfwa"), HtmlImage)
            imgwa.Attributes.Add("onclick", "getwa('" & wa & "','" & wadesc & "');")
        End If
    End Sub

    Private Sub dgskill_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.DeleteCommand
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lblwpaid"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblwpaidi"), Label).Text
        End Try
        dgskill.EditItemIndex = -1
        wa.Open()
        sql = "delete from workplngareas where wpaid = '" & id & "'"
        wa.Update(sql)
        PageNumber = txtpg.Value
        LoadWAS(PageNumber)
        wa.Dispose()


    End Sub
End Class
