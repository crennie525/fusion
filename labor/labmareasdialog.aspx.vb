Public Class labmareasdialog
    Inherits System.Web.UI.Page
    Dim userid, skid, skil, muser As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifss As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            userid = Request.QueryString("userid").ToString
            muser = Request.QueryString("muser").ToString
            skid = Request.QueryString("skid").ToString
            skil = Request.QueryString("skil").ToString
            ifss.Attributes.Add("src", "labmareas.aspx?userid=" & userid & "&muser=" & muser & "&skid=" & skid & "&skil=" & skil & "&date=" & Now)
        End If
    End Sub

End Class
