

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMSuperSelect1
    Inherits System.Web.UI.Page
    Protected WithEvents lang2985 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2984 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, supid, name, srch, cid, typ, skill, lsupid, sid, ro, rostr, wo, pmid As String
    Dim dr As SqlDataReader
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trskill As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents trsrch As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tr2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents supdiv2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsavewo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsavemax As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sup As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents supdiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            '
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try

            lblro.Value = ro
            If ro = "1" Then
                imgadd.Attributes.Add("class", "details")
            End If
            If ro <> "1" Then
                Try
                    rostr = HttpContext.Current.Session("rostr").ToString
                Catch ex As Exception
                    rostr = "0"
                End Try

                If Len(rostr) <> 0 Then
                    ro = sup.CheckROS(rostr, "man")
                    If ro <> "1" Then
                        ro = sup.CheckROS(rostr, "wo")
                        If ro <> "1" Then
                            ro = sup.CheckROS(rostr, "wr")
                        End If
                    End If
                End If
                If ro = "1" Then
                    lblro.Value = ro
                    imgadd.Attributes.Add("class", "details")
                End If
            End If
            typ = Request.QueryString("typ").ToString
            If typ = "sup" Then
                Try
                    wo = Request.QueryString("wo").ToString
                    lblwonum.Value = wo
                    pmid = Request.QueryString("pmid").ToString
                    lblpmid.Value = pmid
                    lblsavemax.Value = "yes"
                Catch ex As Exception

                End Try
            End If
            Try
                wo = Request.QueryString("wo").ToString
                lblwonum.Value = wo
                If pmid = "" Then
                    lblsavewo.Value = "yes"
                End If

            Catch ex As Exception
                wo = ""
            End Try
            
            skill = Request.QueryString("skill").ToString
            lbltyp.Value = typ
            lblskillid.Value = skill
            lblsid.Value = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString
            sup.Open()
            If typ = "lead" Then
                LoadSkills()
                LoadLead()
                trskill.Attributes.Add("class", "view")
                tr1.Attributes.Add("class", "view")
                tr2.Attributes.Add("class", "details")
                'imgadd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov296" , "PMSuperSelect.aspx.vb") & "')")
                'imgadd.Attributes.Add("onmouseout", "return nd()")
                'imgadd.Attributes.Add("onclick", "GetSupDiv('lead')")
                imgadd.Attributes.Add("class", "details")
                Try
                    ddskill.SelectedValue = skill
                Catch ex As Exception

                End Try
            ElseIf typ = "slead" Then
                LoadSkills()
                trskill.Attributes.Add("class", "view")
                tr1.Attributes.Add("class", "details")
                tr2.Attributes.Add("class", "view")
                lsupid = Request.QueryString("supid").ToString
                lblsupid.Value = lsupid
                lblsid.Value = Request.QueryString("sid").ToString
                trsrch.Attributes.Add("class", "details")
                'supdiv.Attributes.Add("Height", "96")
                LoadSLead()

            ElseIf typ = "wait" Then
                tr1.Attributes.Add("class", "details")
                tr2.Attributes.Add("class", "view")
                ddskill.Items.Insert(0, New ListItem("All Skills"))
                ddskill.Items(0).Value = 0
                trskill.Attributes.Add("class", "view")
                trsrch.Attributes.Add("class", "details")
                'supdiv.Attributes.Add("Height", "94")
            ElseIf typ = "sup" Then
                LoadSuper()
                tr1.Attributes.Add("class", "view")
                tr2.Attributes.Add("class", "details")
                trskill.Attributes.Add("class", "details")
                imgadd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov297", "PMSuperSelect.aspx.vb") & "')")
                imgadd.Attributes.Add("onmouseout", "return nd()")
                imgadd.Attributes.Add("onclick", "GetSupDiv('sup')")
            ElseIf typ = "plan" Or typ = "wplan" Then
                LoadPlanner()
                tr1.Attributes.Add("class", "view")
                tr2.Attributes.Add("class", "details")
                trskill.Attributes.Add("class", "details")
                imgadd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov298", "PMSuperSelect.aspx.vb") & "')")
                imgadd.Attributes.Add("onmouseout", "return nd()")
                imgadd.Attributes.Add("onclick", "GetSupDiv('plan')")
            End If
            sup.Dispose()
        Else
            If Request.Form("lblpost") = "ret" Then
                sup.Open()
                typ = lbltyp.Value
                If typ = "lead" Then
                    LoadLead()
                    trskill.Attributes.Add("class", "view")
                Else
                    LoadSuper()
                    trskill.Attributes.Add("class", "details")
                End If
                sup.Dispose()
            End If
            If Request.Form("lblsubmit") = "savewo" Then
                lblsubmit.Value = ""
                sup.Open()
                savewo()
                sup.Dispose()
            ElseIf Request.Form("lblsubmit") = "savemax" Then
                lblsubmit.Value = ""
                sup.Open()
                savewo()
                savepmmax()
                sup.Dispose()
            End If
        End If
    End Sub
    Private Sub savepmmax()
        pmid = lblpmid.Value
        lsupid = lblid.Value
        Dim supr As String = lblval.Value
        sql = "update pmmax set superid = '" & lsupid & "', supervisor = '" & supr & "' where pmid = '" & pmid & "'"
        sup.update(sql)
    End Sub
    Private Sub LoadSLead()
        lsupid = lblsupid.Value
        sid = lblsid.Value
        If ddskill.SelectedIndex = 0 Or ddskill.SelectedIndex = -1 Then
            sql = "select * from pmsysusers where superid = '" & lsupid & "' and dfltps = '" & sid & "' and status = 'active'"
        Else
            skill = ddskill.SelectedValue.ToString
            If skill = "0" Or ddskill.SelectedIndex = -1 Then
                sql = "select * from pmsysusers where superid = '" & lsupid & "' and dfltps = '" & sid & "' and status = 'active'"
            Else
                sql = "select * from pmsysusers where skillid = '" & skill & "' and superid = '" & lsupid & "' and dfltps = '" & sid & "' and status = 'active'"
            End If

        End If
        Dim labid, super, email, skillid, shift As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        dr = sup.GetRdrData(sql)
        While dr.Read
            labid = dr.Item("userid").ToString
            name = dr.Item("username").ToString
            skill = dr.Item("skill").ToString
            skillid = dr.Item("skillid").ToString
            supid = dr.Item("superid").ToString
            super = dr.Item("super").ToString
            shift = dr.Item("shift").ToString
            If shift = "0" Then
                shift = "Any Shift"
            ElseIf shift = "1" Then
                shift = "1st Shift"
            ElseIf shift = "2" Then
                shift = "2nd Shift"
            ElseIf shift = "3" Then
                shift = "3rd Shift"
            Else
                shift = "Any Shift"
            End If
            sb.Append("<tr><td class=""plainlabelblue"">" & name & "</td>")
            sb.Append("<td class=""plainlabel"">" & skill & "</td>")
            sb.Append("<td class=""plainlabel"">" & shift & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv2.InnerHtml = sb.ToString
    End Sub
    Private Sub savewo()
        Dim wonum As String
        wonum = lblwonum.Value
        sid = lblsid.Value
        Dim err1, err2, err3, err4, jpref As String
        err1 = lblid.Value
        err2 = lblval.Value
        err3 = lblid1.Value
        err4 = lblval1.Value
        typ = lbltyp.Value
        If wonum <> "" Then
            If typ = "lead" Then
                Dim cmd As New SqlCommand
                cmd.CommandText = "update workorder set leadcraft = @lead, supervisor = @sup, " _
                + "leadcraftid = @leadi, superid = @supi " _
                + "where wonum = @wonum"
                Dim param = New SqlParameter("@wonum", SqlDbType.VarChar)
                param.Value = wonum
                cmd.Parameters.Add(param)
                Dim param04 = New SqlParameter("@lead", SqlDbType.VarChar)
                If err2 = "" Then
                    param04.Value = System.DBNull.Value
                Else
                    param04.Value = err2
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@sup", SqlDbType.VarChar)
                If err4 = "" Then
                    param05.Value = System.DBNull.Value
                Else
                    param05.Value = err4
                End If
                cmd.Parameters.Add(param05)
                Dim param10 = New SqlParameter("@leadi", SqlDbType.VarChar)
                If err1 = "" Then
                    param10.Value = System.DBNull.Value
                Else
                    param10.Value = err1
                End If
                cmd.Parameters.Add(param10)
                Dim param11 = New SqlParameter("@supi", SqlDbType.VarChar)
                If err3 = "" Then
                    param11.Value = System.DBNull.Value
                Else
                    param11.Value = err3
                End If
                cmd.Parameters.Add(param11)

                sup.UpdateHack(cmd)
            ElseIf typ = "sup" Then
                Dim cmd1 As New SqlCommand
                cmd1.CommandText = "update workorder set supervisor = @sup, " _
                + "superid = @supi " _
                + "where wonum = @wonum"
                Dim param1 = New SqlParameter("@wonum", SqlDbType.VarChar)
                param1.Value = wonum
                cmd1.Parameters.Add(param1)
                Dim param051 = New SqlParameter("@sup", SqlDbType.VarChar)
                If err2 = "" Then
                    param051.Value = System.DBNull.Value
                Else
                    param051.Value = err2
                End If
                cmd1.Parameters.Add(param051)
                Dim param111 = New SqlParameter("@supi", SqlDbType.VarChar)
                If err1 = "" Then
                    param111.Value = System.DBNull.Value
                Else
                    param111.Value = err1
                End If
                cmd1.Parameters.Add(param111)

                sup.UpdateHack(cmd1)
            ElseIf typ = "plan" Then
                sql = "select jpref from workorder where wonum = '" & wonum & "'"
                dr = sup.GetRdrData(sql)
                While dr.Read
                    jpref = dr.Item("jpref").ToString
                End While
                dr.Close()
                Dim cmd11 As New SqlCommand
                'plnrid = @plnrid,
                cmd11.CommandText = "update workorder set  " _
                + "jpref = @jpref " _
                + "where wonum = @wonum"
                Dim param11 = New SqlParameter("@wonum", SqlDbType.VarChar)
                param11.Value = wonum
                cmd11.Parameters.Add(param11)
                'Dim param0511 = New SqlParameter("@plnrid", SqlDbType.VarChar)
                'If err2 = "" Then
                'param0511.Value = System.DBNull.Value
                'Else
                'param0511.Value = err1
                'End If
                'cmd11.Parameters.Add(param0511)
                Dim param1111 = New SqlParameter("@jpref", SqlDbType.VarChar)
                If jpref = "" Then
                    param1111.Value = System.DBNull.Value
                Else
                    param1111.Value = jpref
                End If
                cmd11.Parameters.Add(param1111)

                sup.UpdateHack(cmd11)
            ElseIf typ = "wplan" Then
                
                Dim cmd12 As New SqlCommand
                'plnrid = @plnrid,
                cmd12.CommandText = "update workorder set  " _
                + "plannerid = @supi, planner = @sup " _
                + "where wonum = @wonum"
                Dim param11 = New SqlParameter("@wonum", SqlDbType.VarChar)
                param11.Value = wonum
                cmd12.Parameters.Add(param11)
                Dim param051 = New SqlParameter("@sup", SqlDbType.VarChar)
                If err2 = "" Then
                    param051.Value = System.DBNull.Value
                Else
                    param051.Value = err2
                End If
                cmd12.Parameters.Add(param051)
                Dim param111 = New SqlParameter("@supi", SqlDbType.VarChar)
                If err1 = "" Then
                    param111.Value = System.DBNull.Value
                Else
                    param111.Value = err1
                End If
                cmd12.Parameters.Add(param111)

                sup.UpdateHack(cmd12)
            End If
        End If
        

        lblsubmit.Value = "return"
    End Sub
    Private Sub LoadLead()
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        sid = lblsid.Value
        If Len(srch) > 0 Then
            srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
            srch = Replace(srch, "--", "-", , , vbTextCompare)
            srch = Replace(srch, ";", ":", , , vbTextCompare)
            sql = "select * from pmsysusers where username like '%" & srch & "%' and islabor = 1 and dfltps = '" & sid & "' and status = 'active'"
            Try
                ddskill.SelectedIndex = 0
            Catch ex As Exception

            End Try

        Else
            If ddskill.SelectedIndex = 0 Then
                sql = "select * from pmsysusers where islabor = 1"
            Else
                skill = ddskill.SelectedValue.ToString
                If skill = "0" Then
                    sql = "select * from pmsysusers where islabor = 1 and status = 'active'"
                Else
                    sql = "select * from pmsysusers where skillid = '" & skill & "' and islabor = 1 and dfltps = '" & sid & "' and status = 'active'"
                End If
                txtsrch.Text = ""
            End If

        End If
        Dim labid, super, email, skillid As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        dr = sup.GetRdrData(sql)
        While dr.Read
            labid = dr.Item("userid").ToString
            name = dr.Item("username").ToString
            skill = dr.Item("skill").ToString
            skillid = dr.Item("skillid").ToString
            supid = dr.Item("superid").ToString
            super = dr.Item("super").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup('" & labid & "', '" & name & "', '" & supid & "', '" & super & "');"">" & name & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & skill & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadSuper()
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        sid = lblsid.Value
        If Len(srch) > 0 Then
            srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
            sql = "select * from pmsysusers where username like '%" & srch & "%' and issuper = 1 and dfltps = '" & sid & "' and status = 'active'"
        Else
            sql = "select * from pmsysusers where issuper = 1 and dfltps = '" & sid & "'"
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        dr = sup.GetRdrData(sql)
        While dr.Read
            supid = dr.Item("userid").ToString
            name = dr.Item("username").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup('" & supid & "', '" & name & "');"">" & name & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadPlanner()
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        sid = lblsid.Value
        If Len(srch) > 0 Then
            srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
            sql = "select * from pmsysusers where isplanner = 1 and username like '%" & srch & "%' and status = 'active'" ' and siteid = '" & sid & "'"
        Else
            sql = "select * from pmsysusers where isplanner = 1 and status = 'active'" ' where siteid = '" & sid & "'"
        End If

        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        dr = sup.GetRdrData(sql)
        While dr.Read
            supid = dr.Item("userid").ToString
            name = dr.Item("username").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup('" & supid & "', '" & name & "');"">" & name & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        typ = lbltyp.Value
        sup.Open()
        If typ = "sup" Then
            LoadSuper()
        ElseIf typ = "plan" Then
            LoadPlanner()
        Else
            LoadLead()
        End If
        sup.Dispose()
    End Sub
    Private Sub LoadSkills()
        cid = "0"
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = sup.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("All Skills"))
        ddskill.Items(0).Value = 0

    End Sub

    Private Sub ddskill_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddskill.SelectedIndexChanged
        typ = lbltyp.Value
        sup.Open()
        If typ = "lead" Then
            LoadLead()
        ElseIf typ = "slead" Then
            LoadSLead()
        End If
        sup.Dispose()
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2984.Text = axlabs.GetASPXPage("PMSuperSelect.aspx", "lang2984")
        Catch ex As Exception
        End Try
        Try
            lang2985.Text = axlabs.GetASPXPage("PMSuperSelect.aspx", "lang2985")
        Catch ex As Exception
        End Try

    End Sub

End Class
