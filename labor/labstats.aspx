<%@ Page Language="vb" AutoEventWireup="false" Codebehind="labstats.aspx.vb" Inherits="lucy_r12.labstats" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>labstats</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="javascript" type="text/javascript">
		function getval(val) {
			window.returnValue = val;
			window.close();
		}
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<div style="WIDTH: 130px; HEIGHT: 130px; OVERFLOW: auto; border-top: blue 1px solid; border-bottom: blue 1px solid; border-right: blue 1px solid; border-left: blue 1px solid;">
				<table>
					<tr>
						<td class="plainlabel" width="30"><a href="#" onclick="getval('R');">R</a></td>
						<td class="plainlabel" width="100">Regular Day</td>
					</tr>
					<tr>
						<td class="plainlabel" width="30"><a href="#" onclick="getval('P');">P</a></td>
						<td class="plainlabel" width="100">Partial Day</td>
					</tr>
					<tr>
						<td class="plainlabel" width="30"><a href="#" onclick="getval('O');">O</a></td>
						<td class="plainlabel" width="100">Off</td>
					</tr>
					<tr>
						<td class="plainlabel" width="30"><a href="#" onclick="getval('V');">V</a></td>
						<td class="plainlabel" width="100">Vacation\Personal</td>
					</tr>
					<tr>
						<td class="plainlabel" width="30"><a href="#" onclick="getval('T');">T</a></td>
						<td class="plainlabel" width="100">Training</td>
					</tr>
					<tr>
						<td class="plainlabel" width="30"><a href="#" onclick="getval('S');">S</a></td>
						<td class="plainlabel" width="100">Sick</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</HTML>
