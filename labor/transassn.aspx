<%@ Page Language="vb" AutoEventWireup="false" Codebehind="transassn.aspx.vb" Inherits="lucy_r12.transassn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>transassn</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/transassnaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body bgColor="white" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table width="644">
				<tr height="26">
					<td class="thdrsing label" colSpan="2"><asp:Label id="lang2990" runat="server">Supervisor Assignment Dialog</asp:Label></td>
				</tr>
				<tr>
					<td class="label" width="124" height="26"><asp:Label id="lang2991" runat="server">Current Supervisor:</asp:Label></td>
					<td class="labellt" id="tdcomp" width="520" height="26" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="2">
						<hr style="BORDER-RIGHT: #0000ff 1px solid; BORDER-TOP: #0000ff 1px solid; BORDER-LEFT: #0000ff 1px solid; BORDER-BOTTOM: #0000ff 1px solid">
					</td>
				</tr>
			</table>
			<table width="644" cellpadding="0">
				<tr>
					<td class="label" align="center"><asp:Label id="lang2992" runat="server">Assigned to Others</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang2993" runat="server">Available</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang2994" runat="server">Assigned to Supervisor</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="200"><asp:listbox id="lbassigned" runat="server" Height="180px" SelectionMode="Multiple" Width="200px"
							CssClass="plainlabel"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22"><IMG class="details" id="todisa" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
							width="20">
						<asp:imagebutton id="ifromassn" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton></td>
					<td align="center" width="200"><asp:listbox id="lbunassigned" runat="server" Height="180px" SelectionMode="Multiple" Width="200px"
							CssClass="plainlabel"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22"><IMG class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
							width="20"> <IMG class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
							width="20">
						<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
					<td align="center" width="200"><asp:listbox id="lblocations" runat="server" Height="180px" SelectionMode="Multiple" Width="200px"
							CssClass="plainlabel"></asp:listbox></td>
				</tr>
				<tr>
					<td align="right" colSpan="5"><IMG id="ibtnret" onclick="handleexit();" height="19" alt="" src="../images/appbuttons/bgbuttons/return.gif"
							width="69" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" colSpan="5"><asp:Label id="lang2995" runat="server">List Box Options</asp:Label></td>
				</tr>
				<tr>
					<td class="label" colSpan="5"><asp:radiobuttonlist id="cbopts" runat="server" Width="400px" CssClass="labellt" AutoPostBack="True">
							<asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
							<asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="note" colSpan="5"><asp:Label id="lang2996" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Selected Equipment list if only one item is present.</asp:Label></td>
				</tr>
			</table>
			<table class="details">
				<tr>
					<td><asp:listbox id="lbchecked" runat="server"></asp:listbox></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" runat="server" NAME="lblcid"> <input id="lbleqid" type="hidden" runat="server" NAME="lbleqid">
			<input id="lbllhid" type="hidden" runat="server" NAME="lbllhid"> <input id="lbllid" type="hidden" runat="server" NAME="lbllid">
			<input id="lblapp" type="hidden" runat="server" NAME="lblapp"> <input type="hidden" id="lblopt" runat="server" NAME="lblopt"><input type="hidden" id="lblfailchk" runat="server" NAME="lblfailchk">
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"><input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblsupid" runat="server"><input type="hidden" id="lbltyp" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
