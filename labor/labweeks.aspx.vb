Imports System.Data.SqlClient
Imports System.Text
Public Class labweeks
    Inherits System.Web.UI.Page
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lang585 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsiteid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim wks As New Utilities
    Dim intPgNav As Integer
    Protected WithEvents tdemps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblweek As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Dim sid As String
    Protected WithEvents lblsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tmod As New transmod

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            txtpg.Value = "1"
            sid = HttpContext.Current.Session("dfltps").ToString '"12"
            lblsid.Value = sid
            lblsort.Value = "reg"
            wks.Open()
            CheckEmps()
            CheckWeeks(sid)
            LoadWeeks()
            wks.Dispose()
        Else
            If Request.Form("lblret") = "sched" Then
                wks.Open()
                CheckEmps()
                SchedWeek()
                LoadWeeks()
                wks.Dispose()
            ElseIf Request.Form("lblret") = "sort" Then
                wks.Open()
                CheckEmps()
                LoadWeeks()
                wks.Dispose()
            ElseIf Request.Form("lblret") = "next" Then
                wks.Open()
                GetNext()
                wks.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                wks.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadWeeks()
                wks.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                wks.Open()
                GetPrev()
                wks.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                wks.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                CheckEmps()
                LoadWeeks()
                wks.Dispose()
                lblret.Value = ""
            End If
            lblret.Value = ""
        End If
    End Sub
    Private Sub CheckWeeks(ByVal sid As String)
        Dim mutils As New mmenu_utils_a
        Dim dfirst As Integer = mutils.FDAY
        sql = "exec usp_checkweeks '" & sid & "', '" & dfirst & "'"
        wks.Update(sql)
    End Sub
    Private Sub CheckEmps()
        sid = lblsid.Value
        sql = "select count(*) from labregsched l left join pmsysusers s on s.userid = l.userid " _
        + "where (s.shift is not null or len(s.shift) > 0) " _
        + "and (l.hrs is not null or len(l.hrs) > 0) and (l.daycnt is not null or len(l.daycnt) > 0) and s.dfltps = '" & sid & "'"
        Dim pemps As Integer = 0
        pemps = wks.Scalar(sql)
        sql = "select count(*) from pmsysusers where islabor = 1 and dfltps = '" & sid & "'"
        Dim emps As Integer = 0
        emps = wks.Scalar(sql)
        tdnum.InnerHtml = emps
        tdemps.InnerHtml = pemps
    End Sub
    Private Sub LoadWeeks()
        PageNumber = txtpg.Value

        sid = lblsid.Value
        sql = "select count(*) from labweeks where siteid = '" & sid & "'"
        intPgNav = wks.PageCount(sql, PageSize)
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        intPgNav = txtpgcnt.Value
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<div id=""spdiv"" style=""OVERFLOW: auto; WIDTH: 450px;  HEIGHT: 280px"" onscroll=""SetsDivPosition();"">")
        sb.Append("<table width=""430"" cellpadding=""1"">")
        sb.Append("<tr><td class=""btmmenu label"" width=""90"" align=""center"" height=""22"">Labor Year</td>")
        sb.Append("<td class=""btmmenu label"" width=""90"" align=""center"">Labor Week</td>")
        sb.Append("<td class=""btmmenu label"" width=""180"" align=""center""><a href=""#"" onclick=""changesort();"">Week of</a></td>")
        sb.Append("<td class=""btmmenu label"" width=""70"" align=""center"">Scheduled</td>")
        sb.Append("<td class=""btmmenu label"" width=""70"" align=""center"">Schedule</td></tr>")

        Dim labyr, labwk, wkof, sched, wkid, fnt As String
        sql = "exec usp_getAllWksPg '" & sid & "','" & PageNumber & "','" & PageSize & "'"
        Dim ds As New DataSet
        ds = wks.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        Dim bgr As Integer = 0
        Dim row As DataRow
        Dim tsort As String = lblsort.Value
        If tsort = "reg" Then
            For Each row In dt.Rows
                labyr = row("labyr").ToString
                labwk = row("yrweek").ToString
                wkof = row("weekof").ToString
                sched = row("sched").ToString
                wkid = row("wkid").ToString
                'btmmenuy
                If bgr = 0 Then
                    bgr = 1
                Else
                    bgr = 0
                End If
                If sched = "0" Then
                    If bgr = 0 Then
                        fnt = "btmmenuy plainlabelred"
                    Else
                        fnt = "plainlabelred"
                    End If

                Else
                    If bgr = 0 Then
                        fnt = "btmmenuy plainlabelgreen"
                    Else
                        fnt = "plainlabelgreen"
                    End If

                End If
                sb.Append("<tr><td class=""" & fnt & """ align=""center"">" & labyr & "</td>")
                sb.Append("<td class=""" & fnt & """ align=""center"">" & labwk & "</td>")
                'If sched = "0" Then
                sb.Append("<td class=""" & fnt & """ align=""center"">" & wkof & "</td>")
                'Else
                '    sb.Append("<td class=""plainlabel"" align=""center""><a href=""#"" onclick=""getweek('" & wkid & "')"">" & wkof & "</a></td>")
                'End If
                If sched = "0" Then
                    sb.Append("<td class=""" & fnt & """ align=""center"">NO</td>")
                Else
                    sb.Append("<td class=""" & fnt & """ align=""center"">YES</td>")
                End If
                sb.Append("<td class=""" & fnt & """ align=""center""><img src=""../images/appbuttons/minibuttons/planb1.gif"" onclick=""schedweek('" + wkid + "')"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""lookweek('" + wkid + "','" + wkof + "','" + sid + "')""></td></tr>")

            Next
        Else
            Dim i As Integer
            For i = dt.Rows.Count - 1 To 0 Step -1
                row = dt.Rows(i)
                labyr = row("labyr").ToString
                labwk = row("yrweek").ToString
                wkof = row("weekof").ToString
                sched = row("sched").ToString
                wkid = row("wkid").ToString
                'btmmenuy
                If bgr = 0 Then
                    bgr = 1
                Else
                    bgr = 0
                End If
                If sched = "0" Then
                    If bgr = 0 Then
                        fnt = "btmmenuy plainlabelred"
                    Else
                        fnt = "plainlabelred"
                    End If

                Else
                    If bgr = 0 Then
                        fnt = "btmmenuy plainlabelgreen"
                    Else
                        fnt = "plainlabelgreen"
                    End If

                End If
                sb.Append("<tr><td class=""" & fnt & """ align=""center"">" & labyr & "</td>")
                sb.Append("<td class=""" & fnt & """ align=""center"">" & labwk & "</td>")
                'If sched = "0" Then
                sb.Append("<td class=""" & fnt & """ align=""center"">" & wkof & "</td>")
                'Else
                '    sb.Append("<td class=""plainlabel"" align=""center""><a href=""#"" onclick=""getweek('" & wkid & "')"">" & wkof & "</a></td>")
                'End If
                If sched = "0" Then
                    sb.Append("<td class=""" & fnt & """ align=""center"">NO</td>")
                Else
                    sb.Append("<td class=""" & fnt & """ align=""center"">YES</td>")
                End If
                sb.Append("<td class=""" & fnt & """ align=""center""><img src=""../images/appbuttons/minibuttons/planb1.gif"" onclick=""schedweek('" + wkid + "')"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/magnifier.gif"" onclick=""lookweek('" + wkid + "','" + wkof + "','" + sid + "')""></td></tr>")

            Next
        End If

        sb.Append("</table></div>")
        tdarch.InnerHtml = sb.ToString
    End Sub
    Private Sub SchedWeek()
        Dim wkid As String = lblweek.Value
        Dim sid As String = lblsid.Value
        sql = "usp_schedweek '" & wkid & "','" & sid & "'"
        wks.Update(sql)
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadWeeks()
        Catch ex As Exception
            wks.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr328", "PMArchMan.aspx.vb")

            wks.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadWeeks()
        Catch ex As Exception
            wks.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr329", "PMArchMan.aspx.vb")

            wks.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
End Class
