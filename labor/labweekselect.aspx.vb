Imports System.Data.SqlClient
Imports System.Text

Public Class labweekselect
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim lab As New Utilities
    Dim sid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdwks As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents wksdiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lab.Open()
            LoadWeeks(sid)
            lab.Dispose()
        End If
    End Sub
    Private Sub LoadWeeks(ByVal sid As String)
        Dim sb As New System.Text.StringBuilder
        Dim fd, ld, efd, eld, wkid, wkid2, wkid3, wkid4 As String
        Dim fd2, ld2, fd3, ld3 As String
        sb.Append("<table>")
        sql = "select wkid, Convert(char(10),fd,101) as fd, Convert(char(10),ld,101) as ld, " _
        + "Convert(char(10),dateadd(dd,7,fd),101) as fd2, Convert(char(10),dateadd(dd,13,fd),101) as ld2, " _
        + "Convert(char(10),dateadd(dd,14,fd),101) as fd3, Convert(char(10),dateadd(dd,20,fd),101) as ld3, " _
        + "Convert(char(10),dateadd(dd,21,fd),101) as efd, Convert(char(10),dateadd(dd,27,fd),101) as eld, " _
        + "wkid2 = (select w.wkid from labweeks w where w.fd = Convert(char(10),dateadd(dd,7,labweeks.fd),101) and w.siteid = '" & sid & "'), " _
        + "wkid3 = (select w.wkid from labweeks w where w.fd = Convert(char(10),dateadd(dd,14,labweeks.fd),101) and w.siteid = '" & sid & "'), " _
        + "wkid4 = (select w.wkid from labweeks w where w.fd = Convert(char(10),dateadd(dd,21,labweeks.fd),101) and w.siteid = '" & sid & "') " _
        + "from labweeks where siteid = '" & sid & "' " _
        + "and fd >= Convert(char(10),dateadd(dd,-56,fd),101) and " _
        + "fd <= Convert(char(10),dateadd(dd,308,fd),101) order by cast(fd as datetime) asc"
        dr = lab.GetRdrData(sql)
        While dr.Read
            fd = dr.Item("fd").ToString
            ld = dr.Item("ld").ToString
            fd2 = dr.Item("fd2").ToString
            ld2 = dr.Item("ld2").ToString
            fd3 = dr.Item("fd3").ToString
            ld3 = dr.Item("ld3").ToString
            efd = dr.Item("efd").ToString
            eld = dr.Item("eld").ToString
            wkid = dr.Item("wkid").ToString
            wkid2 = dr.Item("wkid2").ToString
            wkid3 = dr.Item("wkid3").ToString
            wkid4 = dr.Item("wkid4").ToString

            sb.Append("<tr><td class=""plainlabel""><a href=""#"" ")
            sb.Append("onclick=""getweek('" & fd & "','" & ld & "','" & efd & "','" & eld & "','" & fd2 & "','" & ld2 & "', " _
            + "'" & fd3 & "','" & ld3 & "','" & wkid & "','" & wkid2 & "','" & wkid3 & "','" & wkid4 & "')"">")
            sb.Append(fd & " - " & ld & "</a></td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        wksdiv.InnerHtml = sb.ToString
    End Sub
End Class
