

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class wrassign2
    Inherits System.Web.UI.Page
	Protected WithEvents lang3023 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3022 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3021 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3020 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3019 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3018 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3017 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3016 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3015 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3014 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3013 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3012 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3011 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sup As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim sid, supid, ro, dsupid As String
    Protected WithEvents getlab As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents getdept As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents getskill As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents getloc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblhide As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddsup2 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdsite As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddsup As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdshift As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdemail As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            sup.Open()
            PopSuper(sid)
            GetDef()
            sup.Dispose()
            lblsel.Value = "rev"
            lblhide.Value = "yes"
        End If
    End Sub
    Private Sub GetDef()
        sid = lblsid.Value
        sql = "select userid from pmsysusers where isdefault = 1 and dfltps = '" & sid & "'"
        ' uid in (select uid from " _
        '+ "pmusersites where siteid = '" & sid & "')"
        Try
            dsupid = sup.strScalar(sql)
            If dsupid <> "" Then
                ddsup2.SelectedValue = dsupid
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub PopSuper(ByVal sid As String)
        sql = "select userid, username from pmsysusers where issuper = 1 and uid in (select uid from " _
        + "pmusersites where siteid = '" & sid & "')"
        dr = sup.GetRdrData(sql)
        ddsup.DataSource = dr
        ddsup.DataTextField = "username"
        ddsup.DataValueField = "userid"
        ddsup.DataBind()
        dr.Close()
        ddsup.Items.Insert(0, "Select Supervisor")



        Dim sname As String
        sql = "select sitename from sites where siteid = '" & sid & "'"
        sname = sup.strScalar(sql)
        If sname = "0" Then
            sname = "Any"
        End If
        tdsite.InnerHtml = sname

        sql = "select userid, username from pmsysusers where issuper = 1 and dfltps = '" & sid & "'"
        'uid in (select uid from " _
        '+ "pmusersites where siteid = '" & sid & "')"
        dr = sup.GetRdrData(sql)
        ddsup2.DataSource = dr
        ddsup2.DataTextField = "username"
        ddsup2.DataValueField = "userid"
        ddsup2.DataBind()
        dr.Close()
        ddsup2.Items.Insert(0, "Select Supervisor")

    End Sub

    Private Sub ddsup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsup.SelectedIndexChanged
        If ddsup.SelectedIndex <> 0 Then
            supid = ddsup.SelectedValue
            lblsupid.Value = supid
            sql = "select shift, phonenum, email from pmsysusers where userid = '" & supid & "'"
            sup.Open()
            dr = sup.GetRdrData(sql)
            While dr.Read
                tdshift.InnerHtml = dr.Item("shift").ToString
                tdphone.InnerHtml = dr.Item("phonenum").ToString
                tdemail.InnerHtml = dr.Item("email").ToString
            End While
            dr.Close()
            lblsel.Value = "go"

            sup.Dispose()

        End If
    End Sub

    Private Sub ddsup2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsup2.SelectedIndexChanged
        If ddsup2.SelectedIndex <> 0 Then
            supid = ddsup2.SelectedValue
            sid = lblsid.Value
            sup.Open()

            sql = "update pmsysusers set isdefault = NULL where dfltps = '" & sid & "'"
            sup.Update(sql)

            sql = "update pmsysusers set isdefault = 1 where userid = '" & supid & "'"
            sup.Update(sql)
            GetDef()
            sup.Dispose()

        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3011.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3011")
        Catch ex As Exception
        End Try
        Try
            lang3012.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3012")
        Catch ex As Exception
        End Try
        Try
            lang3013.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3013")
        Catch ex As Exception
        End Try
        Try
            lang3014.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3014")
        Catch ex As Exception
        End Try
        Try
            lang3015.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3015")
        Catch ex As Exception
        End Try
        Try
            lang3016.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3016")
        Catch ex As Exception
        End Try
        Try
            lang3017.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3017")
        Catch ex As Exception
        End Try
        Try
            lang3018.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3018")
        Catch ex As Exception
        End Try
        Try
            lang3019.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3019")
        Catch ex As Exception
        End Try
        Try
            lang3020.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3020")
        Catch ex As Exception
        End Try
        Try
            lang3021.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3021")
        Catch ex As Exception
        End Try
        Try
            lang3022.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3022")
        Catch ex As Exception
        End Try
        Try
            lang3023.Text = axlabs.GetASPXPage("wrassign2.aspx", "lang3023")
        Catch ex As Exception
        End Try

    End Sub

End Class
