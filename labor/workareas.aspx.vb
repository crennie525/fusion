

'********************************************************
'*
'********************************************************

Imports System.Data.SqlClient


Public Class workareas
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim wa As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocreturn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocadd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang2969a As System.Web.UI.WebControls.Label
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblskill As System.Web.UI.WebControls.Label
    Protected WithEvents lang113 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgskill As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            lblsid.Value = HttpContext.Current.Session("dfltps").ToString
            wa.Open()
            LoadWAS(PageNumber)
            wa.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                wa.Open()
                GetNext()
                wa.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                wa.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadWAS(PageNumber)
                wa.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                wa.Open()
                GetPrev()
                wa.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                wa.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadWAS(PageNumber)
                wa.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadWAS(PageNumber)
        Catch ex As Exception
            wa.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr102", "AppSetTaskTabST.aspx.vb")

            wa.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadWAS(PageNumber)
        Catch ex As Exception
            wa.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr103", "AppSetTaskTabST.aspx.vb")

            wa.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub LoadWAS(ByVal PageNumber As Integer)
        Dim srch, sid As String
        srch = txtsrch.Text
        srch = wa.ModString1(srch)
        sid = lblsid.Value
        If Len(srch) > 0 Then
            Filter = "(workarea like ''%" & srch & "%'' or wadesc like ''%" & srch & "%'') and siteid = ''" & sid & "''"
            FilterCnt = "(workarea like '%" & srch & "%' or wadesc like '%" & srch & "%') and siteid = '" & sid & "'"
        Else
            Filter = "siteid = ''" & sid & "''"
            FilterCnt = "siteid = '" & sid & "'"
        End If
        sql = "select count(*) " _
                + "from workareas where " & FilterCnt 'compid = '" & cid & "'"
        dgskill.VirtualItemCount = wa.Scalar(sql)
        If dgskill.VirtualItemCount = 0 Then
            lblskill.Text = "No Work Area Records Found"
            Tables = "workareas"
            PK = "waid"
            PageSize = "10"
            dr = wa.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgskill.DataSource = dr
            dgskill.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgskill.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgskill.PageCount
        Else

            Tables = "workareas"
            PK = "waid"
            PageSize = "10"
            dr = wa.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgskill.DataSource = dr
            dgskill.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgskill.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgskill.PageCount
        End If
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub dgskill_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.ItemCommand
        If e.CommandName = "Add" Then
            Dim lname, ldesc, lloc, sid, locadd As String
            lname = CType(e.Item.FindControl("txtnewworkarea"), TextBox).Text
            lname = wa.ModString1(lname)
            lloc = CType(e.Item.FindControl("txtnewwaloc"), Label).Text
            locadd = lbllocadd.Value
            If locadd = "yes" Then
                lbllocadd.Value = "no"
                lloc = lbllocreturn.Value

            End If

            ldesc = CType(e.Item.FindControl("txtnewwadesc"), TextBox).Text
            ldesc = wa.ModString1(ldesc)
            sid = lblsid.Value
            If Len(lname) > 50 Then
                Dim strMessage As String = "Work Area Limited To 50 Characters"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            If Len(ldesc) > 100 Then
                Dim strMessage As String = "Work Area Description Limited To 100 Characters"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            If Len(lname) > 0 Then
                sql = "insert into workareas (workarea, wadesc, waloc, siteid) values ('" & lname & "','" & ldesc & "','" & lloc & "','" & sid & "')"
                wa.Open()
                wa.Update(sql)
                txtsrch.Text = ""
                PageNumber = txtpg.Value
                LoadWAS(PageNumber)
                wa.Dispose()
            Else
                Dim strMessage As String = "Work Area Required"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If
    End Sub

    Private Sub dgskill_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgskill.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim lblfreq As Label = CType(e.Item.FindControl("txtwaloc"), Label)
            Dim freqid As String
            freqid = lblfreq.ClientID.ToString
            Dim imgfreq As HtmlImage = CType(e.Item.FindControl("imgloc"), HtmlImage)
            imgfreq.Attributes.Add("onclick", "getloc('" & freqid & "');")
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            Dim lblfreq As Label = CType(e.Item.FindControl("txtnewwaloc"), Label)
            Dim freqid As String
            freqid = lblfreq.ClientID.ToString
            Dim imgfreq As HtmlImage = CType(e.Item.FindControl("imgnewloc"), HtmlImage)
            imgfreq.Attributes.Add("onclick", "getloc('" & freqid & "');")
        End If
    End Sub

    Private Sub dgskill_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.EditCommand
        PageNumber = txtpg.Value
        lbllocadd.Value = "no"
        lblold.Value = CType(e.Item.FindControl("lblworkarea"), Label).Text
        dgskill.EditItemIndex = e.Item.ItemIndex
        wa.Open()
        LoadWAS(PageNumber)
        wa.Dispose()
    End Sub

    Private Sub dgskill_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.UpdateCommand
        wa.Open()
        Dim id, desc, workarea, sid, loc, locadd As String
        id = CType(e.Item.FindControl("lblwaid"), Label).Text
        workarea = CType(e.Item.FindControl("txtworkarea"), TextBox).Text
        workarea = wa.ModString1(workarea)
        desc = CType(e.Item.FindControl("txtwadesc"), TextBox).Text
        desc = wa.ModString1(desc)

        If Len(workarea) > 50 Then
            Dim strMessage As String = "Work Area Limited To 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(desc) > 100 Then
            Dim strMessage As String = "Work Area Description Limited To 100 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        loc = CType(e.Item.FindControl("txtwaloc"), Label).Text
        locadd = lbllocadd.Value
        If locadd = "yes" Then
            lbllocadd.Value = "no"
            loc = lbllocreturn.Value

        End If

        sid = lblsid.Value
        Dim old As String = lblold.Value
        If old <> workarea Then
            Dim faill As String = workarea.ToLower
            sql = "select count(*) from workareas where lower(workarea) = '" & faill & "' and siteid = '" & sid & "'"
            Dim strchk As Integer
            strchk = wa.Scalar(sql)
            If strchk = 0 Then
                Try
                    sql = "update workareas set workarea = " _
                    + "'" & workarea & "', wadesc = '" & desc & "', waloc = '" & loc & "' " _
                    + "where waid = '" & id & "'"
                    wa.Update(sql)
                    dgskill.EditItemIndex = -1
                    PageNumber = txtpg.Value
                    LoadWAS(PageNumber)
                Catch ex As Exception
                    Dim strMessage As String = "Problem Saving Data"

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
            Else
                Dim strMessage As String = "Cannot Enter Duplicate Work Area"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                wa.Dispose()
            End If
        ElseIf old = workarea Then
            Try
                sql = "update workareas set workarea = " _
                   + "'" & workarea & "', wadesc = '" & desc & "', waloc = '" & loc & "' " _
                   + "where waid = '" & id & "'"
                wa.Update(sql)
                dgskill.EditItemIndex = -1
                PageNumber = txtpg.Value
                LoadWAS(PageNumber)
            Catch ex As Exception
                Dim strMessage As String = "Problem Saving Data"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        Else
            dgskill.EditItemIndex = -1
            PageNumber = txtpg.Value
            LoadWAS(PageNumber)
        End If

        wa.Dispose()
    End Sub

    Private Sub dgskill_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.DeleteCommand
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lblwaid"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblwaidi"), Label).Text
        End Try

    End Sub

    Private Sub dgskill_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.CancelCommand
        dgskill.EditItemIndex = -1
        wa.Open()
        PageNumber = txtpg.Value
        LoadWAS(PageNumber)
        wa.Dispose()
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        wa.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadWAS(PageNumber)
        wa.Dispose()
    End Sub
End Class
