

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class transassign
    Inherits System.Web.UI.Page
	Protected WithEvents ovid277 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2989 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2988 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2987 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2986 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim assn As New Utilities
    Dim supid, cid, typ, sid, ro, skill As String
    Dim Filter As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ddshift As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblup As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbunassigned As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblocations As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbassigned As System.Web.UI.WebControls.ListBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            'Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                'cbopts.Enabled = False
                btntocomp.Visible = False
                btnfromcomp.Visible = False
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")
            End If
            supid = Request.QueryString("supid").ToString
            lblsupid.Value = supid
            lblsid.Value = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            If typ = "wait" Then
                skill = Request.QueryString("skill").ToString
                lblskill.Value = skill
                assn.Open()
                PopWait(skill)
                assn.Dispose()
            Else

                cid = HttpContext.Current.Session("comp").ToString
                lblcid.Value = cid

                lblopt.Value = "0"
                lbassigned.AutoPostBack = False
                lbunassigned.AutoPostBack = False
                lblocations.AutoPostBack = False
                assn.Open()
                PopAssigned()
                PopUnAssigned()
                PopSuperAssigned()
                assn.Dispose()
            End If
        Else
            If Request.Form("lblup") = "wrskill" Then
                assn.Open()
                PopAssigned()
                PopUnAssigned()
                PopSuperAssigned()
                assn.Dispose()
            End If
        End If
        'btnaddfail.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnaddfail.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")
    End Sub
    Private Sub PopWait(ByVal skill As String)
        sid = lblsid.Value
        Filter = lblfilt.Value
        'dept
        If skill = "dept" Then
            sql = "select d.dept_id, d.dept_line + ' - ' + s.username + ' - ' + case shift when 1 then '1st Shift' when 2 then '3rd shift' else 'Any Shift' end as 'dept' from dept d " _
                       + "left join pmsuperlocs sl on sl.dept_id = d.dept_id " _
                       + "left join pmsysusers s on s.userid = sl.superid where d.dept_id in " _
                       + "(select sl.dept_id from pmsuperlocs sl where sl.siteid = '" & sid & "')" & Filter
            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "dept_id"
            lbassigned.DataTextField = "dept"
            lbassigned.DataBind()
            dr.Close()
        End If

        'loc
        If skill = "loc" Then
            sql = "select l.locid, l.location + ' - ' + s.username + ' - ' + case shift when 1 then '1st Shift' when 2 then '3rd shift' else 'Any Shift' end as 'location' from pmlocations l " _
           + "left join pmsuperlocs sl on sl.locid = l.locid " _
           + "left join pmsysusers s on s.userid = sl.superid where l.locid in " _
           + "(select sl.locid from pmsuperlocs sl where sl.siteid = '" & sid & "')" & Filter
            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "locid"
            lbassigned.DataTextField = "location"
            lbassigned.DataBind()
            dr.Close()
        End If


        'skill
        If skill = "skill" Then
            sql = "select ss.skillid, ss.skill + ' - ' + s.username + ' - ' + case s.shift when 1 then '1st Shift' when 2 then '3rd shift' else 'Any Shift' end as 'skill' from pmskills ss " _
                     + "left join pmsupercrafts sl on sl.skillid = ss.skillid " _
                     + "left join pmsysusers s on s.userid = sl.superid where sl.superid in " _
                     + "(select sl.superid from pmsupercrafts sl where sl.siteid = '" & sid & "')" & Filter
            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "skillid"
            lbassigned.DataTextField = "skill"
            lbassigned.DataBind()
            dr.Close()
        End If

      
    End Sub
    Private Sub PopAssigned()
        supid = lblsupid.Value
        sid = lblsid.Value
        typ = lbltyp.Value
        Filter = lblfilt.Value
        If typ = "skill" Then
            'sql = "select skillid, skill from pmskills where skillid in " _
            '     + "(select s.skillid from pmsupercrafts s where s.siteid = '" & sid & "')"

            sql = "select ss.skillid, ss.skill + ' - ' + s.username + ' - ' + case s.shift when 1 then '1st Shift' when 2 then '3rd shift' else 'Any Shift' end as 'skill' from pmskills ss " _
           + "left join pmsupercrafts sl on sl.skillid = ss.skillid " _
           + "left join pmsysusers s on s.userid = sl.superid where sl.superid in " _
           + "(select sl.superid from pmsupercrafts sl where sl.siteid = '" & sid & "' and sl.superid <> '" & supid & "')" & Filter

            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "skillid"
            lbassigned.DataTextField = "skill"
            lbassigned.DataBind()
            dr.Close()
        End If
        Try
            lbassigned.SelectedIndex = 0
        Catch ex As Exception

        End Try

        If typ = "dept" Then
            sql = "select d.dept_id, d.dept_line + ' - ' + s.username + ' - ' + case shift when 1 then '1st Shift' when 2 then '3rd shift' else 'Any Shift' end as 'dept' from dept d " _
            + "left join pmsuperlocs sl on sl.dept_id = d.dept_id " _
            + "left join pmsysusers s on s.userid = sl.superid where d.dept_id in " _
            + "(select sl.dept_id from pmsuperlocs sl where sl.siteid = '" & sid & "' and sl.superid <> '" & supid & "')" & Filter
            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "dept_id"
            lbassigned.DataTextField = "dept"
            lbassigned.DataBind()
            dr.Close()
        End If
        If typ = "loc" Then
            sql = "select l.locid, l.location + ' - ' + s.username + ' - ' + case shift when 1 then '1st Shift' when 2 then '3rd shift' else 'Any Shift' end as 'location' from pmlocations l " _
            + "left join pmsuperlocs sl on sl.locid = l.locid " _
            + "left join pmsysusers s on s.userid = sl.superid where l.locid in " _
            + "(select sl.locid from pmsuperlocs sl where sl.siteid = '" & sid & "' and sl.superid <> '" & supid & "')" & Filter
            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "locid"
            lbassigned.DataTextField = "location"
            lbassigned.DataBind()
            dr.Close()
        End If
        If typ = "eq" Then
            sql = "select e.eqid, e.eqnum + ' - ' + s.username + ' - ' + case shift when 1 then '1st Shift' when 2 then '3rd shift' else 'Any Shift' end as 'eqnum' from equipment e " _
             + "left join pmsupereq sl on sl.eqid = e.eqid " _
            + "left join pmsysusers s on s.userid = sl.superid where e.eqid in " _
            + "(select sl.eqid from pmsupereq sl where sl.siteid = '" & sid & "' and sl.superid <> '" & supid & "')" 'sl.siteid = '" & sid & "' and
            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "eqid"
            lbassigned.DataTextField = "eqnum"
            lbassigned.DataBind()
            dr.Close()
        End If
    End Sub
    Private Sub PopUnAssigned()
        sid = lblsid.Value
        supid = lblsupid.Value
        typ = lbltyp.Value
        Filter = lblfilt.Value
        If typ = "skill" Then
            sql = "select ss.skillid, ss.skill from pmskills ss where ss.skillid <> '0' and ss.skillid not in " _
            + "(select sc.skillid from pmsupercrafts sc " _
            + "left join pmsysusers s on s.userid = sc.superid " _
            + "where sc.siteid = '" & sid & "' and sc.superid = '" & supid & "' and sc.skillid is not null" & Filter & ")"
            dr = assn.GetRdrData(sql)
            lbunassigned.DataSource = dr
            lbunassigned.DataTextField = "skill"
            lbunassigned.DataValueField = "skillid"
            lbunassigned.DataBind()
            dr.Close()
        End If
        If typ = "dept" Then
            sql = "select dept_id, dept_line from dept where siteid = '" & sid & "' and dept_id not in " _
            + "(select sl.dept_id from pmsuperlocs sl " _
            + "left join pmsysusers s on s.userid = sl.superid " _
            + "where sl.siteid = '" & sid & "' and sl.superid = '" & supid & "' and sl.dept_id is not null" & Filter & ")"
           
            dr = assn.GetRdrData(sql)
            lbunassigned.DataSource = dr
            lbunassigned.DataTextField = "dept_line"
            lbunassigned.DataValueField = "dept_id"
            lbunassigned.DataBind()
            dr.Close()
        End If
        If typ = "loc" Then
            sql = "select locid, location from pmlocations where siteid = '" & sid & "' and locid not in " _
            + "(select sl.locid from pmsuperlocs sl " _
            + "left join pmsysusers s on s.userid = sl.superid " _
            + "where s.siteid = '" & sid & "' and s.superid = '" & supid & "' and s.locid is not null" & Filter & ")"
            dr = assn.GetRdrData(sql)
            lbunassigned.DataSource = dr
            lbunassigned.DataTextField = "location"
            lbunassigned.DataValueField = "locid"
            lbunassigned.DataBind()
            dr.Close()
        End If
        If typ = "eq" Then
            sql = "select eqid, eqnum from equipment where siteid = '" & sid & "' and eqid not in " _
                   + "(select s.eqid from pmsupereq s where s.siteid = '" & sid & "' and s.superid = '" & supid & "' and s.eqid is not null)"
            dr = assn.GetRdrData(sql)
            lbunassigned.DataSource = dr
            lbunassigned.DataTextField = "eqnum"
            lbunassigned.DataValueField = "eqid"
            lbunassigned.DataBind()
            dr.Close()
        End If
    End Sub
    Private Sub PopSuperAssigned()

        sid = lblsid.Value
        supid = lblsupid.Value
        typ = lbltyp.Value
        Filter = lblfilt.Value
        If typ = "skill" Then
            sql = "select skillid, skill from pmskills where skillid in " _
            + "(select s.skillid from pmsupercrafts s where s.siteid = '" & sid & "' and s.superid = '" & supid & "')"
            dr = assn.GetRdrData(sql)
            lblocations.DataSource = dr
            lblocations.DataTextField = "skill"
            lblocations.DataValueField = "skillid"
            lblocations.DataBind()
            dr.Close()
        End If
        If typ = "dept" Then
            sql = "select dept_id, dept_line from dept where dept_id in " _
            + "(select s.dept_id from pmsuperlocs s where s.siteid = '" & sid & "' and s.superid = '" & supid & "')"
            dr = assn.GetRdrData(sql)
            lblocations.DataSource = dr
            lblocations.DataTextField = "dept_line"
            lblocations.DataValueField = "dept_id"
            lblocations.DataBind()
            dr.Close()
        End If
        If typ = "loc" Then
            sql = "select locid, location from pmlocations where locid in " _
            + "(select s.locid from pmsuperlocs s where s.siteid = '" & sid & "' and s.superid = '" & supid & "')" 's.siteid = '" & sid & "' and
            dr = assn.GetRdrData(sql)
            lblocations.DataSource = dr
            lblocations.DataTextField = "location"
            lblocations.DataValueField = "locid"
            lblocations.DataBind()
            dr.Close()
        End If
        If typ = "eq" Then
            sql = "select eqid, eqnum from equipment where eqid in " _
            + "(select s.eqid from pmsupereq s where s.siteid = '" & sid & "' and s.superid = '" & supid & "')" 's.siteid = '" & sid & "' and
            dr = assn.GetRdrData(sql)
            lblocations.DataSource = dr
            lblocations.DataTextField = "eqnum"
            lblocations.DataValueField = "eqid"
            lblocations.DataBind()
            dr.Close()
        End If

    End Sub
    Private Sub ToLoc()
        cid = lblcid.Value
        Dim Item As ListItem
        Dim f, fi As String
        assn.Open()
        For Each Item In lbunassigned.Items
            If Item.Selected Then
                fi = Item.Value.ToString
                GetItems(fi)
            End If
        Next
        PopSuperAssigned()
        PopUnAssigned()
        assn.Dispose()
    End Sub
    Private Sub GetItems(ByVal skillid As String)
        supid = lblsupid.Value
        sid = lblsid.Value
        typ = lbltyp.Value
        Dim fcnt As Integer
        If typ = "skill" Then
            sql = "insert into pmsupercrafts (siteid, skillid, superid) values('" & sid & "','" & skillid & "','" & supid & "')"
        End If
        If typ = "dept" Then
            sql = "insert into pmsuperlocs (siteid, dept_id, superid) values('" & sid & "','" & skillid & "','" & supid & "')"
        End If
        If typ = "loc" Then
            sql = "insert into pmsuperlocs (siteid, locid, superid) values('" & sid & "','" & skillid & "','" & supid & "')"
        End If
        If typ = "eq" Then
            sql = "insert into pmsupereq (siteid, locid, dept_id, eqid, superid) select siteid, locid, dept_id, eqid, " & supid & " " _
            + "from equipment where eqid = '" & skillid & "'"
        End If
        assn.Update(sql)
    End Sub
    Private Sub FromLoc()
        cid = lblcid.Value
        Dim Item As ListItem
        Dim f, fi, ci, ei, li As String
        assn.Open()
        For Each Item In lblocations.Items
            If Item.Selected Then
                fi = Item.Value.ToString
                RemItems(fi)
            End If
        Next
        PopSuperAssigned()
        PopUnAssigned()
        PopAssigned()
        assn.Dispose()
    End Sub
    Private Sub RemItems(ByVal skillid As String, Optional ByVal tlid As String = "0")
        supid = lblsupid.Value
        typ = lbltyp.Value
        If typ = "skill" Then
            sql = "delete from pmsupercrafts where siteid = '" & sid & "' and superid = '" & supid & "' and skillid = '" & skillid & "'"
        End If
        If typ = "dept" Then
            sql = "delete from pmsuperlocs where superid = '" & supid & "' and dept_id = '" & skillid & "'"
        End If
        If typ = "loc" Then
            sql = "delete from pmsuperlocs where superid = '" & supid & "' and locid = '" & skillid & "'"
        End If
        If typ = "eq" Then
            sql = "delete from pmsupereq where superid = '" & supid & "' and eqid = '" & skillid & "'"
        End If

        assn.Update(sql)
    End Sub
    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToLoc()
        lblup.Value = "up"
    End Sub
    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromLoc()
        lblup.Value = "up"
    End Sub

    Private Sub ddshift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddshift.SelectedIndexChanged
        typ = lbltyp.Value
        skill = lblskill.Value
        Dim shift As String = ddshift.SelectedValue
        If shift = "1st Shift" Then
            shift = 1
            lblfilt.Value = " and s.shift = '1'"
        ElseIf shift = "2nd Shift" Then
            shift = 2
            lblfilt.Value = " and s.shift = '2'"
        ElseIf shift = "3rd Shift" Then
            shift = 3
            lblfilt.Value = " and s.shift = '3'"
        Else
            lblfilt.Value = ""
        End If
       
        If typ = "wait" Then
            assn.Open()
            PopWait(skill)
            assn.Dispose()
        Else
            assn.Open()
            PopAssigned()
            PopUnAssigned()
            PopSuperAssigned()
            assn.Dispose()
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2986.Text = axlabs.GetASPXPage("transassign.aspx", "lang2986")
        Catch ex As Exception
        End Try
        Try
            lang2987.Text = axlabs.GetASPXPage("transassign.aspx", "lang2987")
        Catch ex As Exception
        End Try
        Try
            lang2988.Text = axlabs.GetASPXPage("transassign.aspx", "lang2988")
        Catch ex As Exception
        End Try
        Try
            lang2989.Text = axlabs.GetASPXPage("transassign.aspx", "lang2989")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid277.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("transassign.aspx", "ovid277") & "')")
            ovid277.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class

