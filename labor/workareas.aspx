<%@ Page Language="vb" AutoEventWireup="false" Codebehind="workareas.aspx.vb" Inherits="lucy_r12.workareas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>workareas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="javascript" type="text/javascript">
		function getloc(id) {
		var eReturn = window.showModalDialog("lablocsdialog.aspx", "", "dialogHeight:220px; dialogWidth:320px; resizable=yes");
			if (eReturn) {
			var ret = eReturn;
			if(ret!="") {
			document.getElementById(id).innerHTML = ret;
			document.getElementById("lbllocreturn").value = ret;
			document.getElementById("lbllocadd").value = "yes";
			}	
			}
		}
		function SetiDivPosition(){
        var intY = document.getElementById("ispdiv").scrollTop;
        document.getElementById("ispdivy").value = "yPos=!~" + intY + "~!";
        }
        function GetiScroll() {
		var strY = document.getElementById("ispdivy").value;
        if(strY.indexOf("!~")!=0){
		try {
          var intS = strY.indexOf("!~");
          var intE = strY.indexOf("~!");
          var strPos = strY.substring(intS+2,intE);
          document.getElementById("ispdiv").scrollTop = strPos;
        }
        catch(err) {
        
        }
        }
		}

		</script>
	</HEAD>
	<body onload="GetiScroll();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: -5px; LEFT: 0px">
				<tr>
					<td width="130">&nbsp;</td>
					<td width="180"></td>
					<td width="20"></td>
					<td width="550"></td>
				</tr>
				<tr>
					<td class="thdrsing label" colSpan="4"><asp:label id="lang2969a" runat="server">Add\Edit Work Areas</asp:label></td>
				</tr>
				<tr>
					<td style="HEIGHT: 16px" colSpan="3"><asp:label id="lblskill" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
							Width="290px" ForeColor="Red"></asp:label></td>
				</tr>
				<tr>
					<td class="label"><asp:label id="lang113" runat="server">Search Work Areas</asp:label></td>
					<td><asp:textbox id="txtsrch" runat="server" Width="170px"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
							CssClass="imgbutton"></asp:imagebutton></td>
				</tr>
				<tr>
					<td vAlign="top" colSpan="4">
						<div style="HEIGHT: 500px; OVERFLOW: auto" onscroll="SetiDivPosition();" id="ispdiv"><asp:datagrid id="dgskill" runat="server" BackColor="transparent" CellSpacing="1" AutoGenerateColumns="False"
								AllowCustomPaging="True" AllowPaging="True" GridLines="None" CellPadding="0" ShowFooter="True">
								<FooterStyle BackColor="transparent"></FooterStyle>
								<AlternatingItemStyle CssClass="ptransrowblue" Height="20px"></AlternatingItemStyle>
								<ItemStyle CssClass="ptransrow" Height="20px"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit">
										<HeaderStyle Height="20px" Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											&nbsp;
											<asp:ImageButton id="imgedit" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												CommandName="Edit"></asp:ImageButton>
										</ItemTemplate>
										<FooterTemplate>
											&nbsp;
											<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
												CommandName="Add"></asp:ImageButton>
										</FooterTemplate>
										<EditItemTemplate>
											&nbsp;
											<asp:ImageButton id="Imagebutton31" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												CommandName="Update"></asp:ImageButton>
											<asp:ImageButton id="Imagebutton32" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												CommandName="Cancel"></asp:ImageButton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label id="lblwaidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.waid") %>' NAME="Label21">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label id="lblwaid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.waid") %>' NAME="Label21">
											</asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Work Area">
										<HeaderStyle Width="170px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											&nbsp;
											<asp:Label id="lblworkarea" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.workarea") %>'>
											</asp:Label>
										</ItemTemplate>
										<FooterTemplate>
											<asp:TextBox id="txtnewworkarea" runat="server" Width="170px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.workarea") %>'>
											</asp:TextBox>
										</FooterTemplate>
										<EditItemTemplate>
											&nbsp;
											<asp:TextBox id=txtworkarea runat="server" Width="170px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.workarea") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=lblwadesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wadesc") %>'>
											</asp:Label>
										</ItemTemplate>
										<FooterTemplate>
											<asp:TextBox id=txtnewwadesc runat="server" Width="250px" Text='<%# DataBinder.Eval(Container, "DataItem.wadesc") %>'>
											</asp:TextBox>
										</FooterTemplate>
										<EditItemTemplate>
											&nbsp;
											<asp:TextBox id=txtwadesc runat="server" Width="250px" Text='<%# DataBinder.Eval(Container, "DataItem.wadesc") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Location">
										<HeaderStyle Width="300px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="lblwaloci" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.waloc") %>'>
											</asp:Label>
										</ItemTemplate>
										<FooterTemplate>
											<asp:Label id="txtnewwaloc" runat="server" CssClass="plainlabel" Width="270px" Text='<%# DataBinder.Eval(Container, "DataItem.waloc") %>'>
											</asp:Label><img id="imgnewloc" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
										</FooterTemplate>
										<EditItemTemplate>
											&nbsp;
											<asp:Label id="txtwaloc" runat="server" CssClass="plainlabel" Width="270px" Text='<%# DataBinder.Eval(Container, "DataItem.waloc") %>'>
											</asp:Label><img id="imgloc" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remove" Visible="False">
										<HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<asp:ImageButton id="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle Visible="False"></PagerStyle>
							</asp:datagrid></div>
					</td>
				</tr>
				<tr>
					<td colSpan="4" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input id="lblsid" type="hidden" runat="server"> <input id="lblold" type="hidden" runat="server">
			<input id="lbllocreturn" type="hidden" runat="server"> <input id="lbllocadd" type="hidden" runat="server">
			<input type="hidden" id="lblret" runat="server"><input type="hidden" id="ispdivy" runat="server" NAME="ispdivy">
		</form>
	</body>
</HTML>
