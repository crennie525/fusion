

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMLeadMan
    Inherits System.Web.UI.Page
	Protected WithEvents lang2981 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2980 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2979 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim cid, sql, sid, srch, ro, rostr As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*, ps = (select s.sitename from sites s where s.siteid = pmsysusers.dfltps), shiftstr = case shift when 0 then ''Any'' when 1 then ''First'' when 2 then ''Second'' when 3 then ''Third'' else '''' end"
    '
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Dim dslev As DataSet
    Dim ds As DataSet
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ddfilt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sup As New Utilities
    Dim pmadmin, cadm, xlbl As String
    Protected WithEvents lblskid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskid2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblr1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblr2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaidret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshiftret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuperret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuperidret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblx As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imggrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpgtop As System.Web.UI.WebControls.Label
    Protected WithEvents tdnavtop As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tblgrid As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblddfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsret As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblps As System.Web.UI.WebControls.Label
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblchng As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                xlbl = Request.QueryString("x").ToString
                lblx.Value = xlbl
                If xlbl = "yes" Then
                    imggrid.Attributes.Add("src", "../images/appbuttons/bgbuttons/return.gif")
                    imggrid.Attributes.Add("onmouseover", "return overlib('Return to Application Setup', BELOW, RIGHT)")
                    imggrid.Attributes.Add("onmouseout", "return nd()")
                    tdnavtop.Attributes.Add("class", "view")
                    tblgrid.Attributes.Add("style", "POSITION: absolute; TOP: 60px; LEFT: 5px")
                Else
                    imggrid.Attributes.Add("src", "../images/appbuttons/minibuttons/grid1.gif")
                    imggrid.Attributes.Add("onmouseover", "return overlib('Review Changes in Grid View', BELOW, RIGHT)")
                    imggrid.Attributes.Add("onmouseout", "return nd()")
                    tdnavtop.Attributes.Add("class", "details")
                    tblgrid.Attributes.Add("style", "POSITION: absolute; TOP: 35px; LEFT: 5px")
                End If
            Catch ex As Exception
                lblx.Value = "no"
                imggrid.Attributes.Add("src", "../images/appbuttons/minibuttons/grid1.gif")
                imggrid.Attributes.Add("onmouseover", "return overlib('Review Changes in Grid View', BELOW, RIGHT)")
                imggrid.Attributes.Add("onmouseout", "return nd()")
                tdnavtop.Attributes.Add("class", "details")
                tblgrid.Attributes.Add("style", "POSITION: absolute; TOP: 35px; LEFT: 5px")
            End Try

            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgdepts.Columns(0).Visible = False
                dgdepts.Columns(3).Visible = False
                dgdepts.Columns(4).Visible = False
            End If
            '''Temp 


            Try
                pmadmin = HttpContext.Current.Session("pmadmin").ToString()
                cadm = HttpContext.Current.Session("cadm").ToString()
                'lblpmadmin.Value = pmadmin
                'lblcadm.Value = cadm
            Catch ex As Exception
                pmadmin = "no"
                cadm = "no"
                'lblpmadmin.Value = ""
                'lblcadm.Value = ""
            End Try
            If pmadmin = "no" Then
                If cadm = "no" Then
                    dgdepts.Columns(0).Visible = False
                    dgdepts.Columns(3).Visible = False
                    dgdepts.Columns(4).Visible = False
                Else
                    dgdepts.Columns(0).Visible = True
                    dgdepts.Columns(3).Visible = True
                    dgdepts.Columns(4).Visible = True
                End If
            Else
                dgdepts.Columns(0).Visible = True
                dgdepts.Columns(3).Visible = True
                dgdepts.Columns(4).Visible = True
            End If



            sid = HttpContext.Current.Session("dfltps").ToString '"12"
            lblsid.Value = sid
            cid = "0"
            lblcid.Value = cid
            sup.Open()
            ddfilt.DataSource = PopulateSkills()
            ddfilt.DataTextField = "skill"
            ddfilt.DataValueField = "skillid"

            ddfilt.DataBind()
            ddfilt.Items.Insert(0, New ListItem("Select"))
            ddfilt.Items(0).Value = 0
            LoadSuper(PageNumber)
            sup.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                sup.Open()
                GetNext()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                sup.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadSuper(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                sup.Open()
                GetPrev()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                sup.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadSuper(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "big" Then
                lblret.Value = ""
                GetBig()
            End If
        End If
    End Sub
    Private Sub GetBig()
        sid = lblsid.Value
        Response.Redirect("../admin/Appset.aspx?rettab=yes&sid=" & sid)
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        Dim srch As String = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim filt, filtd As String
        cid = lblcid.Value
        If Len(srch) <> 0 Then
            filt = "compid = " & cid & " and username like '%" & srch & "%' and islabor = 1"
            filtd = "compid = " & cid & " and username like ''%" & srch & "%'' and islabor = 1"
            lblfilt.Value = filt
            lblfiltd.Value = filtd
            sup.Open()
            LoadSuper(1)
            sup.Dispose()
        Else
            sup.Open()
            LoadSuper(1)
            sup.Dispose()
        End If
    End Sub
    Private Sub ddfilt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddfilt.SelectedIndexChanged
        Dim srch As String = txtsrch.Text
        Dim sel As String = ddfilt.SelectedValue
        Dim filt, filtd As String
        If ddfilt.SelectedIndex <> 0 Then
            cid = lblcid.Value
            lblddfilt.Value = "yes"
            If Len(srch) <> 0 Then
                filt = "compid = " & cid & " and username like '%" & srch & "%' and skillid = '" & sel & "'"
                filtd = "compid = " & cid & " and username like ''%" & srch & "%'' and skillid = ''" & sel & "''"
            Else
                filt = "compid = " & cid & " and skillid = '" & sel & "'"
                filtd = "compid = " & cid & " and skillid = ''" & sel & "''"
            End If
            lblfilt.Value = filt
            lblfiltd.Value = filtd
            sup.Open()
            LoadSuper(1)
            sup.Dispose()
        End If
    End Sub
    Private Sub LoadSuper(ByVal PageNumber As Integer)
        srch = txtsrch.Text
        Dim sel As String = ddfilt.SelectedValue
        Dim filt, filtd As String
        'Try
        cid = "0" 'lblcid.Value
        If ddfilt.SelectedIndex <> 0 Then
            If Len(srch) <> 0 Then
                FilterCnt = "compid = " & cid & " and username like '%" & srch & "%' and skillid = '" & sel & "'"
                Filter = "compid = " & cid & " and username like ''%" & srch & "%'' and skillid = ''" & sel & "''"
            Else
                FilterCnt = "compid = " & cid & " and skillid = '" & sel & "'"
                Filter = "compid = " & cid & " and skillid = ''" & sel & "''"
            End If
        Else
            If Len(txtsrch.Text) <> 0 Then
                FilterCnt = lblfilt.Value
                Filter = lblfiltd.Value
            Else
                Filter = "compid = " & cid & " and islabor = 1"
                FilterCnt = "compid = " & cid & " and islabor = 1"
            End If
        End If
        sid = lblsid.Value
        If Len(Filter) = 0 Then
            Filter = "dfltps = " & sid
            FilterCnt = "dfltps = " & sid
        Else
            Filter += " and dfltps = " & sid
            FilterCnt += " and dfltps = " & sid
        End If
        sql = "select Count(*) from pmsysusers where " & FilterCnt
        Dim dc As Integer = sup.Scalar(sql)

        dgdepts.VirtualItemCount = dc
        If dc = 0 Then
            lblps.Visible = True
            lblps.Text = "No Labor Records Found"
            dgdepts.Visible = True
            Tables = "pmsysusers"
            PK = "userid"
            xlbl = lblx.Value
            If xlbl = "no" Then
                PageSize = "10"
                dgdepts.PageSize = 10
            Else
                PageSize = "100"
                dgdepts.PageSize = 100
            End If

            'Fields = "*"
            'If Len(txtsrch.Text) <> 0 Then
            'filt = lblfiltd.Value
            'Filter = filt
            'Else
            'Filter = "compid = " & cid & " and islabor = 1"
            'End If
            ds = sup.GetDSPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            Dim dv As DataView
            dv = ds.Tables(0).DefaultView
            Try
                dgdepts.DataSource = dv
                dgdepts.DataBind()
            Catch ex As Exception
                dgdepts.CurrentPageIndex = 0
                dgdepts.DataBind()
            End Try
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            If dgdepts.PageCount = 0 Then
                lblpg.Text = "Page 0 of 0"
                lblpgtop.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
                lblpgtop.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            End If
        Else
            dgdepts.Visible = True
            lblps.Visible = False
            Tables = "pmsysusers"
            PK = "userid"
            xlbl = lblx.Value
            If xlbl = "no" Then
                PageSize = "10"
                dgdepts.PageSize = 10
            Else
                PageSize = "100"
                dgdepts.PageSize = 100
            End If
            'Fields = "*"
            'If Len(txtsrch.Text) <> 0 Then
            'filt = lblfiltd.Value
            'Filter = filt
            'Else
            '   Filter = "compid = " & cid & " and islabor = 1"
            'End If
            ds = sup.GetDSPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            Dim dv As DataView
            dv = ds.Tables(0).DefaultView
            Try
                dgdepts.DataSource = dv
                dgdepts.DataBind()
            Catch ex As Exception
                dgdepts.CurrentPageIndex = 0
                dgdepts.DataBind()
            End Try
            lblps.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            If dgdepts.PageCount = 0 Then
                lblpg.Text = "Page 0 of 0"
                lblpgtop.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
                lblpgtop.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            End If
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadSuper(PageNumber)
        Catch ex As Exception
            sup.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1521", "PMLeadMan.aspx.vb")

            sup.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadSuper(PageNumber)
        Catch ex As Exception
            sup.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1522", "PMLeadMan.aspx.vb")

            sup.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Public Function PopulateSkills() As DataSet
        cid = lblcid.Value
        sid = lblsid.Value
        sql = "select skillid, skill, skillindex from pmSkills where compid = '" & cid & "' order by skillindex" 'and siteid = '" & sid & "') or skillindex = '0'
        dslev = sup.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Private Sub dgdepts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.UpdateCommand
        sup.Open()
        'cid = lblcid.Value
        Dim name, phone, email, supid, skill, skillid, skillindx, rate, orate, wa, waid, shift, superid, super, empno As String
        supid = CType(e.Item.FindControl("lblsupid"), Label).Text
        skillid = CType(e.Item.FindControl("lblskid"), Label).Text
        skill = CType(e.Item.FindControl("lblskill"), Label).Text
        'rate = CType(e.Item.FindControl("txtlr"), TextBox).Text
        'orate = CType(e.Item.FindControl("txtor"), TextBox).Text
        wa = CType(e.Item.FindControl("lblwa"), Label).Text
        waid = CType(e.Item.FindControl("lblwaid"), Label).Text
        shift = CType(e.Item.FindControl("lblshift"), Label).Text
        superid = CType(e.Item.FindControl("lblsuperid"), Label).Text
        super = CType(e.Item.FindControl("lblsuper"), Label).Text
        empno = CType(e.Item.FindControl("txtempno"), TextBox).Text
        If Len(empno) > 50 Then
            Dim strMessage As String = "Employee Number Limited to 50 Characters"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        'If rate = "" Then
        'rate = "0"
        'End If
        'If orate = "" Then
        'orate = "0"
        'End If
        Dim old As String = lblold.Value
        'Dim ratechk As Long
        'Try
        'ratechk = System.Convert.ToDecimal(rate)
        'Catch ex As Exception
        '    Dim strMessage As String = tmod.getmsg("cdstr1523", "PMLeadMan.aspx.vb")

        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End Try

        'Dim ochk As Long
        'Try
        'ochk = System.Convert.ToDecimal(orate)
        'Catch ex As Exception
        'Dim strMessage As String = tmod.getmsg("cdstr1523", "PMLeadMan.aspx.vb")

        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End Try

        Dim cnt As Integer = 0
        If lblsret.Value = "yes" Then
            skillid = lblskid1.Value
            skill = lblskid2.Value
            lblsret.Value = ""
        End If
        If lblwret.Value = "yes" Then
            waid = lblwaidret.Value
            wa = lblwaret.Value
            lblwret.Value = ""
        End If
        If lblsuret.Value = "yes" Then
            superid = lblsuperidret.Value
            super = lblsuperret.Value
            lblsuret.Value = ""
        End If
        If lblshret.Value = "yes" Then
            shift = lblshiftret.Value
            lblshret.Value = ""
        End If
        If shift <> "" Then
            If shift = "Any" Then
                shift = "0"
            ElseIf shift = "First" Then
                shift = "1"
            ElseIf shift = "Second" Then
                shift = "2"
            ElseIf shift = "Third" Then
                shift = "3"
            Else
                shift = "0"
            End If
        Else
            shift = "0"
        End If
        If cnt = 0 Then
            sql = "select rate, otrate from pmskills where skillid = '" & skillid & "'"

            Dim srate, sot As String
            srate = "0"
            sot = "0"
            Try
                dr = sup.GetRdrData(sql)
                While dr.Read
                    srate = dr.Item("rate").ToString
                    sot = dr.Item("otrate").ToString
                End While
                dr.Close()
            Catch ex As Exception

            End Try
           
            sql = "update pmsysusers set skill = '" & skill & "', skillid = '" & skillid & "', " _
            + "waid = '" & waid & "', workarea = '" & wa & "', " _
            + "shift = '" & shift & "', super = '" & super & "', superid = '" & superid & "' " _
            + "where userid = '" & supid & "'"
            sup.Update(sql)

            Dim cmd As New SqlCommand("update pmsysusers set laborrate = @srate, otrate = @sot, empno = @empno where userid = @supid")
            Dim param01 = New SqlParameter("@supid", SqlDbType.VarChar)
            If Len(supid) > 0 Then
                param01.Value = supid
            Else
                param01.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@srate", SqlDbType.VarChar)
            If Len(srate) > 0 Then
                param02.Value = srate
            Else
                param02.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@sot", SqlDbType.VarChar)
            If Len(sot) > 0 Then
                param03.Value = sot
            Else
                param03.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@empno", SqlDbType.VarChar)
            If Len(empno) > 0 Then
                param04.Value = empno
            Else
                param04.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param04)
            sup.UpdateHack(cmd)

            If shift <> "0" Then
                sql = "update labregsched set shift = '" & shift & "' where userid = '" & supid & "'"
                sup.Update(sql)
            End If
            If skillid <> "" Then
                sql = "update labregsched set skillid = '" & skillid & "' where userid = '" & supid & "'"
                sup.Update(sql)
            End If
            PageNumber = txtpg.Value
            dgdepts.EditItemIndex = -1
            LoadSuper(PageNumber)
            lblchng.Value = "yes"
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1525", "PMLeadMan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

        sup.Dispose()

        'End Try
    End Sub

    Private Sub dgdepts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.ItemCommand
        Dim name, phone, email, supid, skill, skillid, skillindx, rate As String
        Dim namei, phonei, emaili As TextBox

        If e.CommandName = "Add" Then

            namei = CType(e.Item.FindControl("txtnewname"), TextBox)
            name = namei.Text
            name = Replace(name, "'", Chr(180), , , vbTextCompare)
            name = Replace(name, "--", "-", , , vbTextCompare)
            name = Replace(name, ";", ":", , , vbTextCompare)
            phonei = CType(e.Item.FindControl("txtnewphone"), TextBox)
            phone = phonei.Text
            phone = Replace(phone, "'", Chr(180), , , vbTextCompare)
            phone = Replace(phone, "--", "-", , , vbTextCompare)
            phone = Replace(phone, ";", ":", , , vbTextCompare)
            emaili = CType(e.Item.FindControl("txtnewemail"), TextBox)
            email = emaili.Text
            email = Replace(email, "'", Chr(180), , , vbTextCompare)
            email = Replace(email, "--", "-", , , vbTextCompare)
            email = Replace(email, ";", ":", , , vbTextCompare)

            'supid = CType(e.Item.FindControl("lblsupida"), Label).Text
            skill = CType(e.Item.FindControl("ddskilla"), DropDownList).SelectedItem.ToString
            skillid = CType(e.Item.FindControl("ddskilla"), DropDownList).SelectedValue
            skillindx = CType(e.Item.FindControl("ddskilla"), DropDownList).SelectedIndex
            rate = CType(e.Item.FindControl("txtratea"), TextBox).Text

            If namei.Text <> "" Then
                If Len(name) > 50 Then
                    Dim strMessage As String = tmod.getmsg("cdstr1526", "PMLeadMan.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    sup.Open()
                    Dim cnt As Integer
                    sql = "select count(*) from pmlabor where name = '" & name & "'"
                    cnt = sup.Scalar(sql)
                    If cnt = 0 Then
                        cid = "0" 'lblcid.Value
                        'sid = lblsid.Value
                        sql = "insert into pmlabor (compid, name, phone, email, skillid, skillindex, skill, rate) " _
                        + "values ('" & cid & "', '" & name & "', '" & phone & "', '" & email & "', '" & skillid & "', " _
                        + "'" & skillindx & "', '" & skill & "', '" & rate & "')"
                        sup.Update(sql)
                        Dim deptcnt As Integer = dgdepts.VirtualItemCount + 1
                        If IsDBNull(dgdepts.VirtualItemCount) Or dgdepts.VirtualItemCount = 0 Then
                            deptcnt = 1
                        End If
                        namei.Text = ""
                        phonei.Text = ""
                        emaili.Text = ""
                        sql = "select Count(*) from pmlabor"
                        PageNumber = sup.PageCount(sql, PageSize)
                        LoadSuper(PageNumber)
                        lblchng.Value = "yes"
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1527", "PMLeadMan.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                    sup.Dispose()
                End If
            End If
        End If
    End Sub




    Private Sub dgdepts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.EditCommand
        PageNumber = txtpg.Value 'dgdepts.CurrentPageIndex + 1
        lblold.Value = CType(e.Item.FindControl("Label4"), Label).Text
        dgdepts.EditItemIndex = e.Item.ItemIndex
        sup.Open()
        LoadSuper(PageNumber)
        sup.Dispose()
    End Sub

    Private Sub dgdepts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.CancelCommand
        lblsret.Value = ""
        lblwret.Value = ""
        lblshret.Value = ""
        lblsuret.Value = ""
        dgdepts.EditItemIndex = -1
        sup.Open()
        LoadSuper(PageNumber)
        sup.Dispose()
    End Sub




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("PMLeadMan.aspx", "dgdepts", "0")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(1).HeaderText = dlabs.GetDGPage("PMLeadMan.aspx", "dgdepts", "1")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(7).HeaderText = dlabs.GetDGPage("PMLeadMan.aspx", "dgdepts", "2")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(10).HeaderText = dlabs.GetDGPage("PMLeadMan.aspx", "dgdepts", "3")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(11).HeaderText = dlabs.GetDGPage("PMLeadMan.aspx", "dgdepts", "4")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2979.Text = axlabs.GetASPXPage("PMLeadMan.aspx", "lang2979")
        Catch ex As Exception
        End Try
        Try
            lang2980.Text = axlabs.GetASPXPage("PMLeadMan.aspx", "lang2980")
        Catch ex As Exception
        End Try
        Try
            lang2981.Text = axlabs.GetASPXPage("PMLeadMan.aspx", "lang2981")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub dgdepts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdepts.ItemDataBound
        sid = lblsid.Value
        If e.Item.ItemType = ListItemType.EditItem Then

            Dim lblskill As Label = CType(e.Item.FindControl("lblskill"), Label)
            Dim lblskill1 As Label = CType(e.Item.FindControl("lblskid"), Label)
            Dim lblr1 As Label = CType(e.Item.FindControl("txtlr"), Label)
            Dim lblr2 As Label = CType(e.Item.FindControl("txtor"), Label)
            Dim skillid As String
            skillid = lblskill.ClientID.ToString
            Dim skillid1 As String
            skillid1 = lblskill1.ClientID.ToString
            Dim r1 As String
            r1 = lblr1.ClientID.ToString
            Dim r2 As String
            r2 = lblr2.ClientID.ToString
            Dim imgskill As HtmlImage = CType(e.Item.FindControl("imgskill"), HtmlImage)
            Dim pmtskid As String = "0"
            imgskill.Attributes.Add("onclick", "getskill('" & skillid & "','" & skillid1 & "','" & r1 & "','" & r2 & "');")

            Dim skid As String = DataBinder.Eval(e.Item.DataItem, "skillid").ToString
            Dim skil As String = DataBinder.Eval(e.Item.DataItem, "skill").ToString
            Dim userid As String = DataBinder.Eval(e.Item.DataItem, "userid").ToString
            Dim muser As String = DataBinder.Eval(e.Item.DataItem, "username").ToString
            Dim imgmskill As HtmlImage = CType(e.Item.FindControl("imgmskill"), HtmlImage)
            If skid <> "" Then
                imgmskill.Attributes.Add("onclick", "getmskill('" & skid & "','" & skil & "','" & userid & "','" & muser & "');")
            Else
                imgmskill.Attributes.Add("class", "details")
            End If


            Dim lblwa As Label = CType(e.Item.FindControl("lblwa"), Label)
            Dim lblwaid As Label = CType(e.Item.FindControl("lblwaid"), Label)
            Dim wa As String
            wa = lblwa.ClientID.ToString
            Dim waid As String
            waid = lblwaid.ClientID.ToString
            Dim imgwa As HtmlImage = CType(e.Item.FindControl("imgwa"), HtmlImage)
            imgwa.Attributes.Add("onclick", "getwa('" & wa & "','" & waid & "');")

            Dim waid1 As String = DataBinder.Eval(e.Item.DataItem, "waid").ToString
            Dim wa1 As String = DataBinder.Eval(e.Item.DataItem, "workarea").ToString

            Dim imgmwa As HtmlImage = CType(e.Item.FindControl("imgmwa"), HtmlImage)
            If waid1 <> "" Then
                imgmwa.Attributes.Add("onclick", "getmwa('" & waid1 & "','" & wa1 & "','" & userid & "','" & muser & "');")
            Else
                imgmwa.Attributes.Add("class", "details")
            End If

            Dim lblshift As Label = CType(e.Item.FindControl("lblshift"), Label)
            Dim shift As String = lblshift.ClientID.ToString
            Dim imgshift As HtmlImage = CType(e.Item.FindControl("imgshift"), HtmlImage)
            imgshift.Attributes.Add("onclick", "getshift('" & shift & "');")

            Dim lblsuper As Label = CType(e.Item.FindControl("lblsuper"), Label)
            Dim super As String = lblsuper.ClientID.ToString
            Dim imgsuper As HtmlImage = CType(e.Item.FindControl("imgsuper"), HtmlImage)
            imgsuper.Attributes.Add("onclick", "getsuper('" & super & "');")

            Dim shift1 As String = DataBinder.Eval(e.Item.DataItem, "shift").ToString
            Dim imgesched As HtmlImage = CType(e.Item.FindControl("imgesched"), HtmlImage)
            imgesched.Attributes.Add("onclick", "getsched('" & userid & "','" & muser & "','" & shift1 & "','" & skid & "','" & skil & "','" & sid & "');")

        End If

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim userid As String = DataBinder.Eval(e.Item.DataItem, "userid").ToString
            Dim muser As String = DataBinder.Eval(e.Item.DataItem, "username").ToString
            Dim imgisched As HtmlImage = CType(e.Item.FindControl("imgisched"), HtmlImage)
            'Dim lblshift As Label = CType(e.Item.FindControl("lblshift"), Label)
            Dim shift As String = DataBinder.Eval(e.Item.DataItem, "shift").ToString
            Dim skid As String = DataBinder.Eval(e.Item.DataItem, "skillid").ToString
            Dim skil As String = DataBinder.Eval(e.Item.DataItem, "skill").ToString
            imgisched.Attributes.Add("onclick", "getsched('" & userid & "','" & muser & "','" & shift & "','" & skid & "','" & skil & "','" & sid & "');")
        End If

    End Sub

End Class
