

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrskills
    Inherits System.Web.UI.Page
	Protected WithEvents lang3037 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3036 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3035 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3034 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3033 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim skill As New Utilities
    Dim cid, sid, sname, app, Login, ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbskillmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbsiteskills As System.Web.UI.WebControls.ListBox
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        Page.EnableViewState = True
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btntocomp.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                btntocomp.Enabled = False
                btnfromcomp.ImageUrl = "../images/appbuttons/minibuttons/backgraybg.gif"
                btnfromcomp.Enabled = False
                cbopts.Enabled = False
            End If
            Try
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                If Len(sid) <> 0 AndAlso sid <> "" AndAlso sid <> "0" Then
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    Session("comp") = cid
                    skill.Open()
                    PopSkills(cid)
                    PopSiteSkills(cid, sid)
                    skill.Dispose()
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr1536" , "wrskills.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "nodeptid"
                End If
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1537" , "wrskills.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "nodeptid"
            End Try

        End If
        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
  
    Private Sub PopSkills(ByVal cid As String)
        sid = lblsid.Value
        sql = "select * from pmSkills where skillid not in " _
        + "(select w.skillid from wrskills w where w.siteid = '" & sid & "')"
        dr = skill.GetRdrData(sql)
        lbskillmaster.DataSource = dr
        lbskillmaster.DataValueField = "skillid"
        lbskillmaster.DataTextField = "skill"
        lbskillmaster.DataBind()
        dr.Close()
        Try
            lbskillmaster.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopSiteSkills(ByVal cid As String, ByVal sid As String)
        sql = "select * from wrskills where compid = '" & cid & "' and siteid = '" & sid & "'"
        dr = skill.GetRdrData(sql)
        lbsiteskills.DataSource = dr
        lbsiteskills.DataValueField = "skillid"
        lbsiteskills.DataTextField = "skill"
        lbsiteskills.DataBind()
        dr.Close()
        Try
            lbsiteskills.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetItems(ByVal skillid As String, ByVal skillstr As String)
        cid = lblcid.Value
        sid = lblsid.Value
        app = lblapp.Value
        Dim fcnt As Integer
        sql = "select count(*) from wrskills where compid = '" & cid & "' and " _
        + "siteid = '" & sid & "' and skillid = '" & skillid & "'"
        fcnt = skill.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into wrskills (skillid, skill, siteid, compid) values ('" & skillid & "', " _
            + "'" & skillstr & "', '" & sid & "', '" & cid & "')"
            skill.Update(sql)
        End If
    End Sub
    Private Sub ToComp()
        cid = lblcid.Value
        sid = lblsid.Value
        Dim Item As ListItem
        Dim s, ss As String
        skill.Open()
        For Each Item In lbskillmaster.Items
            If Item.Selected Then
                ss = Item.ToString
                s = Item.Value.ToString
                GetItems(s, ss)
            End If
        Next
        PopSiteSkills(cid, sid)
        PopSkills(cid)
        skill.Dispose()
    End Sub
    Private Sub FromComp()
        cid = lblcid.Value
        sid = lblsid.Value
        Dim Item As ListItem
        Dim s As String
        skill.Open()
        For Each Item In lbsiteskills.Items
            If Item.Selected Then
                s = Item.Value.ToString
                RemItems(s)
            End If
        Next
        PopSiteSkills(cid, sid)
        PopSkills(cid)
        skill.Dispose()
    End Sub
    Private Sub RemItems(ByVal skillid As String)
        sid = lblsid.Value
        sql = "delete from wrskills where skillid = '" & skillid & "' and siteid = '" & sid & "'"
        skill.Update(sql)
    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbskillmaster.AutoPostBack = False
            lbsiteskills.AutoPostBack = False
        Else
            lbskillmaster.AutoPostBack = True
            lbsiteskills.AutoPostBack = True
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3033.Text = axlabs.GetASPXPage("wrskills.aspx", "lang3033")
        Catch ex As Exception
        End Try
        Try
            lang3034.Text = axlabs.GetASPXPage("wrskills.aspx", "lang3034")
        Catch ex As Exception
        End Try
        Try
            lang3035.Text = axlabs.GetASPXPage("wrskills.aspx", "lang3035")
        Catch ex As Exception
        End Try
        Try
            lang3036.Text = axlabs.GetASPXPage("wrskills.aspx", "lang3036")
        Catch ex As Exception
        End Try
        Try
            lang3037.Text = axlabs.GetASPXPage("wrskills.aspx", "lang3037")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click

    End Sub
End Class
