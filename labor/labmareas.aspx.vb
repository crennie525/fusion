'********************************************************
'*
'********************************************************
Imports System.Data.SqlClient
Public Class labmareas
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql As String
    Dim skill As New Utilities
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim intPgCnt, intPgNav As Integer
    Dim vid, vind, val, pmtskid, fld, sid, desc, loc, userid, waid, wa, skid, skil, muser, ro As String
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lang3033 As System.Web.UI.WebControls.Label
    Protected WithEvents lang3034 As System.Web.UI.WebControls.Label
    Protected WithEvents lang3035 As System.Web.UI.WebControls.Label
    Protected WithEvents lbskillmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbsiteskills As System.Web.UI.WebControls.ListBox
    Protected WithEvents lang3036 As System.Web.UI.WebControls.Label
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lang3037 As System.Web.UI.WebControls.Label
    Protected WithEvents tdcur As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btntocomp.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                btntocomp.Enabled = False
                btnfromcomp.ImageUrl = "../images/appbuttons/minibuttons/backgraybg.gif"
                btnfromcomp.Enabled = False
                cbopts.Enabled = False
            End If
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            userid = Request.QueryString("userid").ToString
            lbluserid.Value = userid
            skid = Request.QueryString("skid").ToString
            lblskillid.Value = skid
            skil = Request.QueryString("skil").ToString
            tdskill.InnerHtml = skil
            muser = Request.QueryString("muser").ToString
            tdcur.InnerHtml = muser
            txtpg.Value = "1"
            skill.Open()
            PopSkills(PageNumber)
            skill.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                skill.Open()
                GetNext()
                skill.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                skill.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopSkills(PageNumber)
                skill.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                skill.Open()
                GetPrev()
                skill.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                skill.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopSkills(PageNumber)
                skill.Dispose()
                lblret.Value = ""
            End If

        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopSkills(PageNumber)
        Catch ex As Exception
            skill.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr102", "AppSetTaskTabST.aspx.vb")

            skill.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopSkills(PageNumber)
        Catch ex As Exception
            skill.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr103", "AppSetTaskTabST.aspx.vb")

            skill.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopSkills(ByVal PageNumber As Integer)
        Dim srch As String
        srch = "" 'txtsrch.Text
        srch = skill.ModString1(srch)
        sid = lblsid.Value
        userid = lbluserid.Value
        skid = lblskillid.Value
        If Len(srch) > 0 Then
            Filter = "(workarea like ''%" & srch & "%'' or wadesc like ''%" & srch & "%'') and siteid = ''" & sid & "''"
            FilterCnt = "(workarea like '%" & srch & "%' or wadesc like '%" & srch & "%') and siteid = '" & sid & "'"
        Else
            Filter = "siteid = ''" & sid & "''"
            FilterCnt = "siteid = '" & sid & "'"
        End If
        Filter += " and waid not in (select waid from womultiareas where userid = ''" & userid & "'') and waid <> ''" & skid & "''"
        FilterCnt += " and waid not in (select waid from womultiareas where userid = '" & userid & "') and waid <> '" & skid & "'"
        sql = "select count(*) " _
        + "from workareas where " & FilterCnt
        PageNumber = txtpg.Value
        intPgCnt = skill.Scalar(sql)
        intPgNav = skill.PageCount(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        'Dim sb As New StringBuilder
        'sql = "select waid, workarea, wadesc, waloc, siteid " _
        '    + "from workareas where siteid = '" & sid & "'"
        'dr = skill.GetRdrData(sql)
        Tables = "workareas"
        PK = "waid"
        dr = skill.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        lbskillmaster.DataSource = dr
        lbskillmaster.DataValueField = "waid"
        lbskillmaster.DataTextField = "workarea"
        lbskillmaster.DataBind()
        dr.Close()
        Try
            lbskillmaster.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopSiteSkills()
        sid = lblsid.Value
        userid = lbluserid.Value
        sql = "select m.waid, s.workarea from womultiareas m left join workareas s on s.waid = m.waid " _
        + "where m.userid = '" & userid & "'"
        dr = skill.GetRdrData(sql)
        lbsiteskills.DataSource = dr
        lbsiteskills.DataValueField = "waid"
        lbsiteskills.DataTextField = "workarea"
        lbsiteskills.DataBind()
        dr.Close()
        Try
            lbsiteskills.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        sid = lblsid.Value
        Dim Item As ListItem
        Dim s, ss As String
        skill.Open()
        For Each Item In lbskillmaster.Items
            If Item.Selected Then
                ss = Item.ToString
                s = Item.Value.ToString
                GetItems(s, ss)
            End If
        Next
        PopSiteSkills()
        PageNumber = txtpg.Value
        PopSkills(PageNumber)
        skill.Dispose()
    End Sub
    Private Sub GetItems(ByVal skillid As String, ByVal skillstr As String)
        sid = lblsid.Value
        userid = lbluserid.Value
        Dim fcnt As Integer
        sql = "select count(*) from womultiareas where userid = '" & userid & "' and waid = '" & skillid & "'"
        fcnt = skill.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into womultiareas (waid, userid) values ('" & skillid & "', " _
            + "'" & userid & "')"
            skill.Update(sql)
        End If
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        sid = lblsid.Value
        Dim Item As ListItem
        Dim s As String
        skill.Open()
        For Each Item In lbsiteskills.Items
            If Item.Selected Then
                s = Item.Value.ToString
                RemItems(s)
            End If
        Next
        PopSiteSkills()
        PageNumber = txtpg.Value
        PopSkills(PageNumber)
        skill.Dispose()
    End Sub
    Private Sub RemItems(ByVal skillid As String)
        sid = lblsid.Value
        userid = lbluserid.Value
        sql = "delete from womultiareas where waid = '" & skillid & "' and userid = '" & userid & "'"
        skill.Update(sql)
    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbskillmaster.AutoPostBack = False
            lbsiteskills.AutoPostBack = False
        Else
            lbskillmaster.AutoPostBack = True
            lbsiteskills.AutoPostBack = True
        End If
    End Sub
End Class
