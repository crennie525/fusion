<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMSuperMan.aspx.vb" Inherits="lucy_r12.PMSuperMan" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMSuperMan</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/PMSuperManaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
		<script language="javascript" type="text/javascript">
		function getpa(skid1, skid2) {
		var sid = document.getElementById("lblsid").value;
		var eReturn = window.showModalDialog("planareaselectdialog.aspx?typ=lab&sid=" + sid, "", "dialogHeight:500px; dialogWidth:820px; resizable=yes");
		if (eReturn) {
		if(eReturn!="") {
			var ret = eReturn.split("~~")

			document.getElementById(skid1).innerHTML = ret[1];
			document.getElementById("lblwaret").value = ret[1];
			document.getElementById("lblwpaidret").value = ret[0];
			
			document.getElementById("lblwpret").value = "yes";	
		}
		}
		}
		function getwa(skid1, skid2) {
		    var sid = document.getElementById("lblsid").value;
		    var eReturn = window.showModalDialog("workareaselectdialog.aspx?typ=lab&sid=" + sid, "", "dialogHeight:500px; dialogWidth:820px; resizable=yes");
		    if (eReturn) {
		        if (eReturn != "") {
		            var ret = eReturn.split("~~")

		            document.getElementById(skid1).innerHTML = ret[1];
		            document.getElementById("lblwaret").value = ret[1];
		            document.getElementById("lblwaidret").value = ret[0];

		            document.getElementById("lblwret").value = "yes";
		        }
		    }
		}
		</script>
	</HEAD>
	<body onload="checkchng();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 5px" cellSpacing="0" cellPadding="0"
				width="780">
				<tr>
					<td colSpan="3">
						<table>
							<tr>
								<td class="label" id="tdsrch" width="100" runat="server"><asp:Label id="lang2982" runat="server">Search Super</asp:Label></td>
								<td><asp:textbox id="txtsrch" runat="server"></asp:textbox></td>
								<td width="30"><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colSpan="2"><asp:label id="lblps" runat="server" ForeColor="Red" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
							Width="376px"></asp:label></td>
				</tr>
				<tr>
					<td width="20">&nbsp;</td>
					<td width="5"></td>
					<td class="bluelabel" id="tdps" width="755" runat="server"></td>
				</tr>
				<tr>
					<td width="20">&nbsp;</td>
					<TD colSpan="2"><asp:datagrid id="dgdepts" runat="server" CellSpacing="1" AutoGenerateColumns="False" AllowCustomPaging="True"
							AllowPaging="True" GridLines="None" CellPadding="0" ShowFooter="False">
							<FooterStyle BackColor="White"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight"></ItemStyle>
							<HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<FooterTemplate>
										<asp:ImageButton id="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
											CommandName="Add"></asp:ImageButton>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewname runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id=txtname runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="workarea desc" HeaderText="Work Area">
									<HeaderStyle Width="170px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.workarea") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblwa" width="140px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.workarea") %>'>
										</asp:Label><img id="imgwa" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
										<img id="imgmwa" runat="server" src="../images/appbuttons/minibuttons/addnew.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="planarea desc" HeaderText="Planning Area">
									<HeaderStyle Width="170px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.planarea") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblplanarea" width="140px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.planarea") %>'>
										</asp:Label><img id="imgpa" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="shift desc" HeaderText="Shift">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shiftstr") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblshift" width="40px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shiftstr") %>'>
										</asp:Label><img id="imgshift" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Phone">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.phonenum") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="txtnewphone" runat="server" Width="120px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.phonenum") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="txtphone" runat="server" Width="120px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.phonenum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Email Address">
									<HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<a href='mailto:<%# DataBinder.Eval(Container, "DataItem.email") %>'>
											<%# DataBinder.Eval(Container, "DataItem.email") %>
										</a>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewemail runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id=txtemail runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label1 runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.userid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewllabel runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.userid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="lblsupid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.userid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Assignments" Visible="False">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:imagebutton id="i23" runat="server" ImageUrl="../images/appbuttons/minibuttons/people2.gif"
											ToolTip="Skills/Crafts"></asp:imagebutton>
										<asp:imagebutton id="i23a" runat="server" ImageUrl="../images/appbuttons/minibuttons/dept01.gif"
											ToolTip="Departments"></asp:imagebutton>
										<asp:imagebutton id="i23b" runat="server" ImageUrl="../images/appbuttons/minibuttons/loc01.gif" ToolTip="Locations"></asp:imagebutton>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label8" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.waid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="Textbox4" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.waid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="lblwaid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.waid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label11" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.wpaid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="Textbox2" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.wpaid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="lblwpaid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.wpaid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label6" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.wpaid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="Textbox1" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.wpaid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="Label10" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.wpaid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="middle" class="plainlabelred" colspan="3"><asp:Label id="lang2983" runat="server">Supervisor and Planner Records are controlled in the User Administration Module.</asp:Label></td>
				</tr>
			</table>
			<input id="lblchng" type="hidden" runat="server"> <input id="lblsid" type="hidden" runat="server">
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblcid" type="hidden" name="lblcid" runat="server">
			<input id="lblfilt" type="hidden" name="lblfilt" runat="server"> <input id="lblfiltd" type="hidden" name="lblfiltd" runat="server">
			<input id="lblold" type="hidden" name="lblold" runat="server"> <input id="lblwho" type="hidden" runat="server">
			<input type="hidden" id="lblwret" runat="server" NAME="lblwret"> <input type="hidden" id="lblwaret" runat="server" NAME="lblwaret">
			<input type="hidden" id="lblwaidret" runat="server" NAME="lblwaidret"> <input type="hidden" id="lblfslang" runat="server" />
			<input type="hidden" id="lblshret" runat="server" NAME="lblshret"> <input type="hidden" id="lblshiftret" runat="server" NAME="lblshiftret">
			<input type="hidden" id="lblwpaidret" runat="server"> <input type="hidden" id="lblwpret" runat="server">
		</form>
	</body>
</HTML>
