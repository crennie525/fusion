<%@ Page Language="vb" AutoEventWireup="false" Codebehind="wrassign2.aspx.vb" Inherits="lucy_r12.wrassign2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>wrassign2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/wrassign2aspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checksel();">
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; POSITION: absolute; TOP: 2px; LEFT: 2px" cellSpacing="0" cellPadding="2"
				width="1040">
				<tr>
					<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/woreq.gif" border="0"></td>
					<td class="thdrsingrt label" width="1014"><asp:Label id="lang3011" runat="server">Work Request Assignment Administration</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td class="label" width="80"><asp:Label id="lang3012" runat="server">Supervisor</asp:Label></td>
								<td width="180"><asp:dropdownlist id="ddsup" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
								<td class="label" width="80"><asp:Label id="lang3013" runat="server">Plant Site</asp:Label></td>
								<td width="180" class="plainlabel" id="tdsite" runat="server"></td>
								<td class="label" width="60"><asp:Label id="lang3014" runat="server">Shift</asp:Label></td>
								<td class="plainlabel" id="tdshift" width="60" runat="server"></td>
								<td class="label" width="60"><asp:Label id="lang3015" runat="server">Phone</asp:Label></td>
								<td class="plainlabel" id="tdphone" width="80" runat="server"></td>
								<td class="label" width="60"><asp:Label id="lang3016" runat="server">Email</asp:Label></td>
								<td class="plainlabel" id="tdemail" width="200" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table width="1040">
							<tr>
								<td class="thdrsingg label" width="300"><asp:Label id="lang3017" runat="server">Labor Assignments (Reference)</asp:Label></td>
								<td class="thdrsingg label" width="740"><asp:Label id="lang3018" runat="server">Department Assignments</asp:Label></td>
							</tr>
							<tr>
								<td rowSpan="3" valign="top">
									<table cellSpacing="0" cellPadding="3">
										<tr>
											<td><iframe id="getlab" style="BACKGROUND-COLOR: transparent" src="wrlabor.aspx?supid=&amp;wr=&amp;ro="
													frameBorder="no" width="300" scrolling="no" height="345" allowTransparency runat="server"></iframe>
											</td>
										</tr>
										<tr>
											<td class="thdrsingg label" width="300"><asp:Label id="lang3019" runat="server">Plant Site Default</asp:Label></td>
										</tr>
										<tr>
											<td>
												<table>
                                                <tr>
                                                <td class="plainlabelblue" colspan="2">Based on Supervisor's Default Plant Site</td>
                                                </tr>
													<tr>
														<td class="label" width="80"><asp:Label id="lang3020" runat="server">Supervisor</asp:Label></td>
														<td width="180"><asp:dropdownlist id="ddsup2" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table cellSpacing="0" cellPadding="3">
										<tr>
											<td><iframe id="getdept" style="BACKGROUND-COLOR: transparent" src="transassign.aspx?typ=&amp;supid=&amp;wr=&amp;ro=&amp;sid="
													frameBorder="no" width="740" scrolling="no" height="165" allowTransparency runat="server"></iframe>
											</td>
										</tr>
										<tr id="trloc1">
											<td class="thdrsingg label" width="740"><asp:Label id="lang3021" runat="server">Location Assignments</asp:Label><a href="#" onclick="hidelocs();" id="hloc"><asp:Label id="lang3022" runat="server">Show Locations</asp:Label></a></td>
										</tr>
										<tr id="trloc2" class="details">
											<td><iframe id="getloc" style="BACKGROUND-COLOR: transparent" src="transassign.aspx?typ=&amp;supid=&amp;wr=&amp;ro=&amp;sid="
													frameBorder="no" width="740" scrolling="no" height="165" allowTransparency runat="server"></iframe>
											</td>
										</tr>
										<tr>
											<td class="thdrsingg label" width="740"><asp:Label id="lang3023" runat="server">Skill Assignments</asp:Label></td>
										</tr>
										<tr>
											<td><iframe id="getskill" style="BACKGROUND-COLOR: transparent" src="transassign.aspx?typ=&amp;supid=&amp;wr=&amp;ro=&amp;sid="
													frameBorder="no" width="740" scrolling="no" height="165" allowTransparency runat="server"></iframe>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblsupid" type="hidden" runat="server" NAME="lblsupid"><input id="lblsel" type="hidden" runat="server" NAME="lblsel">
			<input id="lblsup" type="hidden" runat="server" NAME="lblsup"><input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="lblro" runat="server" NAME="lblro"><input type="hidden" id="lblsupret" runat="server" NAME="lblsupret">
			<input type="hidden" id="lblsid" runat="server"><input type="hidden" id="lblhide" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
