<%@ Page Language="vb" AutoEventWireup="false" Codebehind="wrselect.aspx.vb" Inherits="lucy_r12.wrselect" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>wrselect</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="JavaScript" src="../scripts1/wrselectaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 4px; LEFT: 4px" cellSpacing="1" cellPadding="1"
				width="600">
				<tr>
					<td class="bluelabel"><asp:Label id="lang3025" runat="server">Current Supervisor</asp:Label></td>
					<td class="plainlabel" colspan="3" id="tdsup" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang3026" runat="server">Search By Name</asp:Label></td>
					<td><asp:textbox id="txtsrch" runat="server" CssClass="plainlabel"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang3027" runat="server">Filter By Skill</asp:Label></td>
					<td colSpan="3"><asp:dropdownlist id="ddskill" runat="server" CssClass="plainlabel" AutoPostBack="True"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang3028" runat="server">Filter By Shift</asp:Label></td>
					<td colSpan="3"><asp:dropdownlist id="ddshift" runat="server" CssClass="plainlabel" AutoPostBack="True">
							<asp:ListItem Value="All">All</asp:ListItem>
							<asp:ListItem Value="1st Shift">1st Shift</asp:ListItem>
							<asp:ListItem Value="2nd Shift">2nd Shift</asp:ListItem>
							<asp:ListItem Value="3rd Shift">3rd Shift</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td colspan="4" class="label" align="center" width="300"><asp:Label id="lang3029" runat="server">Available Labor</asp:Label></td>
					<td colspan="4" class="label" align="center" width="300"><asp:Label id="lang3030" runat="server">Assigned to Supervisor</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="4">
						<div id="supdiv" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; HEIGHT: 300px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
					<td colSpan="4">
						<div id="supdiv2" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; HEIGHT: 300px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
				<tr>
					<td colspan="4" align="center" class="plainlabelred"><asp:Label id="lang3031" runat="server">Click User Link to Add To Supervisor</asp:Label></td>
					<td colspan="4" align="center" class="plainlabelred"><asp:Label id="lang3032" runat="server">Click User Link to Remove From Supervisor</asp:Label></td>
				</tr>
				<tr>
					<td colspan="4" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblpost" type="hidden" runat="server" NAME="lblpost"><input id="lbltyp" type="hidden" runat="server" NAME="lbltyp">
			<input id="lblskillid" type="hidden" runat="server" NAME="lblskillid"><input id="lblsupid" type="hidden" runat="server" NAME="lblsupid">
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"><input type="hidden" id="lblro" runat="server" NAME="lblro">
			<input type="hidden" id="lblfilt" runat="server"> <input type="hidden" id="lblret" runat="server">
			<input type="hidden" id="txtpg" runat="server" NAME="txtpg"> <input type="hidden" id="txtpgcnt" runat="server" NAME="txtpgcnt">
			<input type="hidden" id="lblsubmit" runat="server"><input type="hidden" id="lblsuper" runat="server">
			<input type="hidden" id="lbllabid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
