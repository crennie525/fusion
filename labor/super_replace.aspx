﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="super_replace.aspx.vb"
    Inherits="lucy_r12.super_replace" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="javascript" type="text/javascript">
        function getsup2(supid2, sup2) {
            document.getElementById("lblsupid2").value = supid2;
            document.getElementById("lblsup2").value = sup2;
            document.getElementById("tdsup2").innerHTML = sup2;
        }
        function getsup3(supid3, sup3) {
            document.getElementById("lblsupid3").value = supid3;
            document.getElementById("lblsup3").value = sup3;
            document.getElementById("tdsup3").innerHTML = sup3;
        }
        function checkrep() {
            var supid2 = document.getElementById("lblsupid2").value;
            var supid3 = document.getElementById("lblsupid3").value;
            if ((supid2 != "" && supid3 != "") && (supid2 != supid3)) {
                document.getElementById("lblret").value = "checksup";
                document.getElementById("form1").submit();
            }
            else if (supid2 == "") {
                alert("No Current Supervisor Selected")
            }
            else if (supid3 == "") {
                alert("No Replacement Supervisor Selected")
            }
            else if (supid2 == "" && supid3 == "") {
                alert("No Current or Replacement Supervisor Selected")
            }
        }
        function checkit() {

        }
    </script>
</head>
<body onload="checkit();">
    <form id="form1" runat="server">
    <table style="position: absolute; top: 4px; left: 4px" cellspacing="1" cellpadding="1"
        width="950">
        <tr>
            <td class="label">
                Current Supervisor
            </td>
            <td class="label">
                Replacement Supervisor
            </td>
        </tr>
        <tr id="tr2" runat="server">
            <td width="300">
                <div id="supdiv2" style="border-bottom: black 1px solid; border-left: black 1px solid;
                    width: 280px; height: 194px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                    runat="server">
                </div>
            </td>
            <td width="300">
                <div id="supdiv3" style="border-bottom: black 1px solid; border-left: black 1px solid;
                    width: 280px; height: 194px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                    runat="server">
                </div>
            </td>
            <td width="350" class="plainlabelblue">This function will replace the selected Current Supervisor with the selected
            Replacement Supervisor for all PM records, TPM records, and existing open Work Orders.<br /><br />
            Please Note that the Current Supervisor must be designated as a Supervisor in User Administration to appear in the Current Supervisor list.</td>
        </tr>
        <tr>
            <td class="plainlabel" id="tdsup2" runat="server" height="30">
            </td>
            <td class="plainlabel" id="tdsup3" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <input id="btnsuprep" type="button" value="Replace" onclick="checkrep();" />
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblsup2" runat="server" />
    <input type="hidden" id="lblsup3" runat="server" />
    <input type="hidden" id="lblsupid2" runat="server" />
    <input type="hidden" id="lblsupid3" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblcheckit" runat="server" />
    </form>
</body>
</html>
