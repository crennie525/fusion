<%@ Page Language="vb" AutoEventWireup="false" Codebehind="labweeksched.aspx.vb" Inherits="lucy_r12.labweeksched" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>labweeksched</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="javascript" src="../scripts/smartscroll4.js"></script>
		<script language="javascript" type="text/javascript">
		    function getnext() {

		        var cnt = document.getElementById("txtpgcnt").value;
		        var pg = document.getElementById("txtpg").value;
		        pg = parseInt(pg);
		        cnt = parseInt(cnt)
                alert(pg + "," + cnt)
		        if (pg < cnt) {
		            document.getElementById("lblret").value = "next"
		            document.getElementById("form1").submit();
		        }
		    }
		    function getlast() {

		        var cnt = document.getElementById("txtpgcnt").value;
		        var pg = document.getElementById("txtpg").value;
		        pg = parseInt(pg);
		        cnt = parseInt(cnt)
		        if (pg < cnt) {
		            document.getElementById("lblret").value = "last"
		            document.getElementById("form1").submit();
		        }
		    }
		    function getprev() {

		        var cnt = document.getElementById("txtpgcnt").value;
		        var pg = document.getElementById("txtpg").value;
		        pg = parseInt(pg);
		        cnt = parseInt(cnt)
		        if (pg > 1) {
		            document.getElementById("lblret").value = "prev"
		            document.getElementById("form1").submit();
		        }
		    }
		    function getfirst() {

		        var cnt = document.getElementById("txtpgcnt").value;
		        var pg = document.getElementById("txtpg").value;
		        pg = parseInt(pg);
		        cnt = parseInt(cnt)
		        if (pg > 1) {
		            document.getElementById("lblret").value = "first"
		            document.getElementById("form1").submit();
		        }
		    }
		function getskill() {
		var sid = document.getElementById("lblsid").value;
		var eReturn = window.showModalDialog("labskillsdialog.aspx?who=lw&sid=" + sid, "", "dialogHeight:500px; dialogWidth:360px; resizable=yes");
		if (eReturn) {
		if(eReturn!="") {
			var ret = eReturn.split("~")
			
			//document.getElementById(who).value = "yes";
			//document.getElementById(r1).innerHTML = ret[1];
			//document.getElementById(skid1).value = ret[1];
			//document.getElementById(skid2).value = ret[0];
			document.getElementById("lblskillid").value = ret[0];
			document.getElementById("lblskill").value = ret[1];
			document.getElementById("tdskill").innerHTML = ret[1];
			document.getElementById("lblsubmit").value = "getweeks";
			document.getElementById("form1").submit();	
		}	
		}
		}
		function getwa() {
		var sid = document.getElementById("lblsid").value;
		var eReturn = window.showModalDialog("workareaselectdialog.aspx?typ=lab&sid=" + sid, "", "dialogHeight:500px; dialogWidth:820px; resizable=yes");
		if (eReturn) {
		if(eReturn!="") {
			var ret = eReturn.split("~~")

			//document.getElementById(skid1).innerHTML = ret[1];
			//document.getElementById("lblwaret").value = ret[1];
			//document.getElementById("lblwaidret").value = ret[0];
			//document.getElementById("lblwret").value = "yes";	
			document.getElementById("lblwaid").value = ret[0];
			document.getElementById("lblworkarea").value = ret[1];
			document.getElementById("tdworkarea").innerHTML = ret[1];
			document.getElementById("lblsubmit").value = "getweeks";
			document.getElementById("form1").submit();	
		}
		}
		}
		function pageScroll() {
		window.scrollTo(0,top);
		//scrolldelay = setTimeout('pageScroll()', 200); 
		} 
		function getref(val) {
		if(val=="all") {
		document.getElementById("lblskillid").value = "";
		document.getElementById("lblskill").value = "";
		
		document.getElementById("lblwaid").value = "";
		document.getElementById("lblworkarea").value = "";
		}
		else if(val=="skill") {
		document.getElementById("lblskillid").value = "";
		document.getElementById("lblskill").value = "";
		}
		else if(val=="wa") {
		document.getElementById("lblwaid").value = "";
		document.getElementById("lblworkarea").value = "";
		}
		document.getElementById("lblsubmit").value = "getweeks";
		document.getElementById("form1").submit();	
		}
		</script>
	</HEAD>
	<body  onload="GetsScroll();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel">Selected Week</td>
								<td id="tdwo" runat="server" class="plainlabel" colspan="2"></td>
								<td><img onclick="getref('all');" src="../images/appbuttons/minibuttons/refreshit.gif"></td>
							</tr>
							<tr>
								<td class="bluelabel" width="140">Filter by Skill</td>
								<td class="plainlabel" id="tdskill" runat="server" width="180"></td>
								<td width="20"><img onclick="getskill();" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								<td width="110"><img onclick="getref('skill');" src="../images/appbuttons/minibuttons/refreshit.gif"></td>
							</tr>
							<tr>
								<td class="bluelabel">Filter by Work Area</td>
								<td class="plainlabel" id="tdworkarea" runat="server"></td>
								<td><img onclick="getwa();" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
								<td><img onclick="getref('wa');" src="../images/appbuttons/minibuttons/refreshit.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst2.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev2.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="140"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext2.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast2.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdarch" runat="server"></td>
				</tr>
			</table>
			<input type="hidden" id="lblwkid" runat="server" NAME="lblwkid"> <input type="hidden" id="lblskillid" runat="server" NAME="lblskillid">
			<input type="hidden" id="lblworkarea" runat="server" NAME="lblworkarea"> <input type="hidden" id="lblsid" runat="server">
			<input type="hidden" id="lblsubmit" runat="server"> <input type="hidden" id="lblskill" runat="server">
			<input type="hidden" id="lblwaid" runat="server"> <input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"><input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden" runat="server" /> <input type="hidden" id="lblweekof" runat="server">
			<input type="hidden" id="lblnewweekof" runat="server"> <input type="hidden" id="lblnewwkid" runat="server">
		</form>
	</body>
</HTML>
