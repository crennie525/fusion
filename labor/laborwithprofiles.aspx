<%@ Page Language="vb" AutoEventWireup="false" Codebehind="laborwithprofiles.aspx.vb" Inherits="lucy_r12.laborwithprofiles" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Employees with Scheduling Profiles</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/PMSuperSelectaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function getsup(id, val, id1, val1) {

                var chk = document.getElementById("lblsavewo").value;
                //alert(chk)
                if (chk == "yes") {
                    document.getElementById("lblid").value = id;
                    document.getElementById("lblval").value = val;
                    document.getElementById("lblid1").value = id1;
                    document.getElementById("lblval1").value = val1;
                    document.getElementById("lblsubmit").value = "savewo";
                    document.getElementById("form1").submit();
                }
                else {
                    window.parent.handlesup(id, val, id1, val1);
                }

            }
        </script>
	</HEAD>
	<body class="tbg" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 4px; LEFT: 4px" cellSpacing="1" cellPadding="1"
				width="320">
				<tr>
					<td class="plainlabelred" align="center" colspan="4">This dialog lists users with 
						established Scheduling Profiles Only for the current Plant Site
					</td>
				</tr>
				<tr id="trsrch" runat="server">
					<td class="bluelabel"><asp:label id="lang2984" runat="server">Search By Name</asp:label></td>
					<td><asp:textbox id="txtsrch" runat="server" CssClass="plainlabel"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td></td>
				</tr>
				<tr id="trskill" class="details" runat="server">
					<td class="bluelabel"><asp:label id="lang2985" runat="server">Filter By Skill</asp:label></td>
					<td colSpan="3"><asp:dropdownlist id="ddskill" runat="server" CssClass="plainlabel" AutoPostBack="True"></asp:dropdownlist></td>
				</tr>
				<tr id="trwa" class="details" runat="server">
					<td class="bluelabel"><asp:label id="Label1" runat="server">Filter By Work Area</asp:label></td>
					<td colSpan="3"><asp:dropdownlist id="ddwa" runat="server" CssClass="plainlabel" AutoPostBack="True"></asp:dropdownlist></td>
				</tr>
				<tr id="tr1" runat="server">
					<td colSpan="4">
						<div style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; HEIGHT: 300px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							id="supdiv" runat="server"></div>
					</td>
				</tr>
				<tr id="tr2" runat="server">
					<td colSpan="4">
						<div style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 280px; HEIGHT: 94px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							id="supdiv2" runat="server"></div>
					</td>
				</tr>
			</table>
			<input id="lblpost" type="hidden" name="lblpost" runat="server"><input id="lbltyp" type="hidden" name="lbltyp" runat="server">
			<input id="lblskillid" type="hidden" name="lblskillid" runat="server"><input id="lblsupid" type="hidden" name="lblsupid" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lblro" type="hidden" name="lblro" runat="server">
			<input id="lblfslang" type="hidden" name="lblfslang" runat="server"> <input type="hidden" id="lblfilter" runat="server">
			<input id="Hidden1" type="hidden" runat="server" NAME="Hidden1"><input id="Hidden2" type="hidden" runat="server" NAME="Hidden2">
			<input id="Hidden3" type="hidden" runat="server" NAME="Hidden3"><input id="Hidden4" type="hidden" runat="server" NAME="Hidden4">
			<input type="hidden" id="Hidden5" runat="server" NAME="Hidden5"><input type="hidden" id="Hidden6" runat="server" NAME="Hidden6">
			<input type="hidden" id="Hidden7" runat="server" NAME="Hidden7"> <input type="hidden" id="lblwonum" runat="server" NAME="lblwonum">
			<input type="hidden" id="lblsavewo" runat="server" NAME="lblsavewo"><input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="lblid" runat="server" NAME="lblid"> <input type="hidden" id="lblval" runat="server" NAME="lblval">
			<input type="hidden" id="lblid1" runat="server" NAME="lblid1"> <input type="hidden" id="lblval1" runat="server" NAME="lblval1">
		</form>
	</body>
</HTML>
