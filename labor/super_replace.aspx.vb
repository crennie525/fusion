﻿Imports System.Data.SqlClient
Public Class super_replace
    Inherits System.Web.UI.Page
    Dim sid, supid2, supid3, sup2, sup3 As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sup As New Utilities
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            sup.Open()
            popsup2()
            popsup3()
            sup.Dispose()
        Else
            If Request.Form("lblret") = "checksup" Then
                sup.Open()
                checksup()
                sup.Dispose()
            End If
        End If
    End Sub
    Private Sub checksup()
        supid2 = lblsupid2.Value
        sup2 = lblsup2.Value
        supid3 = lblsupid3.Value
        sup3 = lblsup3.Value
        Dim msg As String
        If supid2 <> supid3 And supid2 <> "" And supid3 <> "" Then
            Try
                sql = "update pm set superid = '" & supid3 & "', super = '" & sup3 & "' where superid = '" & supid2 & "'"
                sup.Update(sql)
               
            Catch ex As Exception
                msg = "Problem Updating PM Records"

            End Try
            Try
                sql = "update tpm set superid = '" & supid3 & "', super = '" & sup3 & "' where superid = '" & supid2 & "'"
                sup.Update(sql)
                
            Catch ex As Exception
                If msg = "" Then
                    msg = "Problem Updating TPM Records"
                Else
                    msg += "\nProblem Updating TPM Records"
                End If
            End Try
            Try
                sql = "update pmmax set superid = '" & supid3 & "', supervisor = '" & sup3 & "' where superid = '" & supid2 & "'"
                sup.Update(sql)

            Catch ex As Exception
                If msg = "" Then
                    msg = "Problem Updating PM Job Plan Records"
                Else
                    msg += "\nProblem Updating PM Job Plan Records"
                End If
            End Try
            Try
                sql = "update workorder set superid = '" & supid3 & "', supervisor = '" & sup3 & "' where superid = '" & supid2 & "' and status not in ('CAN', 'COMP', 'CANCEL')"
                sup.Update(sql)

            Catch ex As Exception
                If msg = "" Then
                    msg = "Problem Updating Work Order Records"
                Else
                    msg += "\nProblem Updating Work Order Records"
                End If
            End Try
            Dim strMessage As String
            If msg = "" Then
                strMessage = sup2 & "Records Have Been Replaced with " & sup3
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                strMessage = msg
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

            
        Else
            Dim strMessage As String = "Not Enough Data to Process Request"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub popsup2()
        sid = lblsid.Value
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from pmsysusers where issuper = 1 and dfltps = '" & sid & "'"
        dr = sup.GetRdrData(sql)
        While dr.Read
            supid2 = dr.Item("userid").ToString
            sup2 = dr.Item("username").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup2('" & supid2 & "', '" & sup2 & "');"">" & sup2 & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv2.InnerHtml = sb.ToString
    End Sub
    Private Sub popsup3()
        sid = lblsid.Value
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from pmsysusers where issuper = 1 and dfltps = '" & sid & "'"
        dr = sup.GetRdrData(sql)
        While dr.Read
            supid3 = dr.Item("userid").ToString
            sup3 = dr.Item("username").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup3('" & supid3 & "', '" & sup3 & "');"">" & sup3 & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv3.InnerHtml = sb.ToString
    End Sub
End Class