<%@ Page Language="vb" AutoEventWireup="false" Codebehind="labweeks.aspx.vb" Inherits="lucy_r12.labweeks" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>labweeks</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="javascript" src="../scripts/smartscroll4.js"></script>
		<script language="javascript" type="text/javascript">
		function lookweek(wkid, wkof, sid) {
		var eReturn = window.showModalDialog("labweekscheddialog.aspx?wkid=" + wkid + "&wkof=" + wkof + "&sid=" + sid, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
		if (eReturn) {
		
		}
		}
		function getweek(val) {
			window.parent.handleweek();
		}
		function schedweek(val) {
		//alert(val)
			document.getElementById("lblweek").value = val;
			document.getElementById("lblret").value = "sched";
			document.getElementById("form1").submit();
		}
		function changesort() {
		var chk = document.getElementById("lblsort").value;
		if(chk=="reg") {
		document.getElementById("lblsort").value="rev";
		}
		else {
		document.getElementById("lblsort").value="reg";
		}
		document.getElementById("lblret").value = "sort";
		document.getElementById("form1").submit();
		}
		function chkscroll() {
		window.parent.chkscroll();
		}
		</script>
	</HEAD>
	<body  onload="GetsScroll();chkscroll();">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; BACKGROUND-COLOR: transparent; TOP: 0px" cellSpacing="0"
				cellPadding="0" width="920">
				<tr>
					<td width="460">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<table>
										<tr>
											<td class="label" height="26">Number of employees (this Plant Site):</td>
											<td class="plainlabelred" id="tdnum" runat="server"></td>
										</tr>
										<tr>
											<td class="label" height="26">Number of employees with profiles (this Plant 
												Site):&nbsp;&nbsp;</td>
											<td class="plainlabelred" id="tdemps" runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst2.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev2.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="140"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext2.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast2.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td id="tdarch" runat="server"></td>
							</tr>
						</table>
					</td>
					<td class="plainlabelblue" align="center" valign="middle" width="460">
						"Week of" sorting applies to the Current Page View Only.<br>
						<br>
						Clicking the "Schedule Week" button on the right side of the grid populates a 
						"Regular" Schedule for all employees with Scheduling Profiles<br>
						<br>
						Clicking the "Schedule Week" button on the right side of the grid after a week 
						is initially scheduled will only populate schedules for employees who did not 
						have a Scheduling Profile initially<br>
						<br>
						Having a "Scheduling Profile" is defined as an employee with an assigned Plant 
						Site, Shift, Daily Hours, and Regular Work Days assigned<br>
						<br>
						The system attempts to maintain 52 weeks beyond the current week of the year 
						available to be scheduled<br>
						<br>
						If you are opening this tab or page for the first time the system also 
						attempted to create 4 scheduling weeks previous to the current week of the 
						current year IF those previous weeks fell into the current year<br>
						<br>
					</td>
				</tr>
			</table>
			<input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"> <input type="hidden" id="lblsiteid" runat="server" NAME="lblsiteid">
			<input type="hidden" id="lblcomp" runat="server" NAME="lblcomp"> <input type="hidden" id="lblsid" runat="server" NAME="lblsid">
			<input type="hidden" id="lblfslang" runat="server" NAME="lblfslang"> <input type="hidden" id="lblweek" runat="server">
			<input type="hidden" id="lblsort" runat="server"><input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden" runat="server" />
		</form>
	</body>
</HTML>
