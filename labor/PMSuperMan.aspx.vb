

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMSuperMan
    Inherits System.Web.UI.Page
    Protected WithEvents lang2983 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2982 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim cid, sql, sid, who, ro, rostr, pmadmin, cadm As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*, shiftstr = case shift when 0 then ''Any'' when 1 then ''First'' when 2 then ''Second'' when 3 then ''Third'' else '''' end"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblps As System.Web.UI.WebControls.Label
    Protected WithEvents tdps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblchng As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdsrch As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaidret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshiftret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwpaidret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwpret As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sup As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblc As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lbcc As System.Web.UI.WebControls.LinkButton
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents selrow As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdlabor As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcraft As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents supdiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            ro = "0" 'HttpContext.Current.Session("ro").ToString
            If ro = "1" Then
                dgdepts.Columns(0).Visible = False
            End If

            Try
                pmadmin = HttpContext.Current.Session("pmadmin").ToString()
                cadm = HttpContext.Current.Session("cadm").ToString()
                'lblpmadmin.Value = pmadmin
                'lblcadm.Value = cadm
            Catch ex As Exception
                pmadmin = "no"
                cadm = "no"
                'lblpmadmin.Value = ""
                'lblcadm.Value = ""
            End Try
            If pmadmin = "no" Then
                If cadm = "no" Then
                    dgdepts.Columns(0).Visible = False
                    dgdepts.Columns(3).Visible = False
                    dgdepts.Columns(4).Visible = False
                Else
                    dgdepts.Columns(0).Visible = True
                    dgdepts.Columns(3).Visible = True
                    dgdepts.Columns(4).Visible = True
                End If
            Else
                dgdepts.Columns(0).Visible = True
                dgdepts.Columns(3).Visible = True
                dgdepts.Columns(4).Visible = True
            End If
            'Temp
            'dgdepts.Columns(0).Visible = False


            sup.Open()
            Try
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
            Catch ex As Exception

            End Try
            lblcid.Value = "0"
            who = Request.QueryString("who").ToString
            lblwho.Value = who
            If who = "super" Then
                tdsrch.InnerHtml = "Search Super"
            Else
                tdsrch.InnerHtml = "Search Planner"
            End If
            LoadSuper(PageNumber)
            sup.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                sup.Open()
                GetNext()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                sup.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadSuper(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                sup.Open()
                GetPrev()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                sup.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadSuper(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub LoadSuper(ByVal PageNumber As Integer)
        sid = lblsid.Value
        'Try
        cid = lblcid.Value
        'sid = lblsid.Value
        who = lblwho.Value
        Dim srch As String
        srch = txtsrch.Text
        Dim filt, filtd As String
        'Try
        cid = "0" 'lblcid.Value
        If Len(txtsrch.Text) <> 0 Then
            filt = lblfilt.Value
            Filter = lblfiltd.Value 'filt
        Else
            If who = "super" Then
                filt = "issuper = 1 "
                Filter = "issuper = 1 "
            Else
                filt = "isplanner = 1 "
                Filter = "isplanner = 1 "
            End If

        End If
        If who = "super" Then
            If Filter <> "" Then
                sql = "select Count(*) from pmsysusers where " & filt
            Else
                sql = "select Count(*) from pmsysusers where issuper = 1"
            End If
        ElseIf who = "planner" Then
            If Filter <> "" Then
                sql = "select Count(*) from pmsysusers where " & filt
            Else
                sql = "select Count(*) from pmsysusers where isplanner = 1"
            End If
        End If

        Dim dc As Integer = sup.Scalar(sql)

        dgdepts.VirtualItemCount = dc
        If dc = 0 Then
            lblps.Visible = True
            If who = "super" Then
                lblps.Text = "No Supervisor Records Found"
            Else
                lblps.Text = "No Planner Records Found"
            End If

            dgdepts.Visible = True
            Tables = "pmsysusers"
            PK = "userid"


            PageSize = "10"
            'Fields = "*"
            'If Len(txtsrch.Text) <> 0 Then
            'filt = lblfiltd.Value
            'Filter = filt
            'Else
            'Filter = "compid = " & cid
            'End If
            'If Len(txtsrch.Text) <> 0 Then
            'filt = lblfiltd.Value
            'Filter = filt
            'Else
            'Filter = "issuper = 1 "
            'End If
            dr = sup.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgdepts.DataSource = dr
            'Try
            dgdepts.DataBind()
            'Catch ex As Exception
            'dgdepts.CurrentPageIndex = 0
            'dgdepts.DataBind()
            'End Try
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            If dgdepts.PageCount = 0 Then
                lblpg.Text = "Page 0 of 0"
                'lblpgtop.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
                'lblpgtop.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            End If
        Else
            dgdepts.Visible = True
            lblps.Visible = False
            If who = "super" Then
                Tables = "pmsysusers"
                PK = "userid"
                'Fields = "*"
            Else
                Tables = "pmsysusers"
                PK = "userid"
                'Fields = "*"
                'Fields = "name, phone, email, plnrid as superid"
            End If
            PageSize = "10"
            If Len(txtsrch.Text) <> 0 Then
                filt = lblfilt.Value
                Filter = lblfiltd.Value 'filt
            Else
                If who = "super" Then
                    filt = "issuper = 1 "
                    Filter = "issuper = 1 "
                Else
                    filt = "isplanner = 1 "
                    Filter = "isplanner = 1 "
                End If
            End If

            dr = sup.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgdepts.DataSource = dr
            'Try
            dgdepts.DataBind()
            'Catch ex As Exception
            'dgdepts.CurrentPageIndex = 0
            'dgdepts.DataBind()
            'End Try
            dr.Close()
            lblps.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            If dgdepts.PageCount = 0 Then
                lblpg.Text = "Page 0 of 0"
                'lblpgtop.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
                'lblpgtop.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            End If

        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadSuper(PageNumber)
        Catch ex As Exception
            sup.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1528", "PMSuperMan.aspx.vb")

            sup.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadSuper(PageNumber)
        Catch ex As Exception
            sup.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1529", "PMSuperMan.aspx.vb")

            sup.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgdepts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.CancelCommand
        lblwret.Value = ""
        lblshret.Value = ""
        dgdepts.EditItemIndex = -1
        sup.Open()
        LoadSuper(PageNumber)
        sup.Dispose()
    End Sub

    Private Sub dgdepts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.UpdateCommand
        sup.Open()
        'cid = lblcid.Value
        who = lblwho.Value
        Dim name, phone, email, supid, wa, waid, shift, wpaid As String
        'name = CType(e.Item.Cells(1).Controls(1), TextBox).Text
        'name = Replace(name, "'", Chr(180), , , vbTextCompare)
        'name = Replace(name, "--", "-", , , vbTextCompare)
        'name = Replace(name, ";", ":", , , vbTextCompare)
        'phone = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        'phone = Replace(phone, "'", Chr(180), , , vbTextCompare)
        'phone = Replace(phone, "--", "-", , , vbTextCompare)
        'phone = Replace(phone, ";", ":", , , vbTextCompare)
        'email = CType(e.Item.Cells(3).Controls(1), TextBox).Text
        'email = Replace(email, "'", Chr(180), , , vbTextCompare)
        'email = Replace(email, "--", "-", , , vbTextCompare)
        'email = Replace(email, ";", ":", , , vbTextCompare)
        supid = CType(e.Item.FindControl("lblsupid"), Label).Text
        wa = CType(e.Item.FindControl("lblwa"), Label).Text
        waid = CType(e.Item.FindControl("lblwaid"), Label).Text
        shift = CType(e.Item.FindControl("lblshift"), Label).Text
        Dim old As String = lblold.Value
        Dim cnt As Integer
        If Len(name) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1530", "PMSuperMan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            sup.Dispose()
            Exit Sub
        End If
        If lblwret.Value = "yes" Then
            waid = lblwaidret.Value
            wa = lblwaret.Value
            lblwret.Value = ""
        End If
        If lblshret.Value = "yes" Then
            shift = lblshiftret.Value
            lblshret.Value = ""
        End If
        If shift <> "" Then
            If shift = "Any" Then
                shift = "1"
            ElseIf shift = "First" Then
                shift = "1"
            ElseIf shift = "Second" Then
                shift = "2"
            ElseIf shift = "Third" Then
                shift = "3"
            Else
                shift = "0"
            End If
        Else
            shift = "0"
        End If
        'If old <> name Then
        'If who = "super" Then
        'sql = "select count(*) from pmsuper where name = '" & name & "'"
        'Else
        '    sql = "select count(*) from pmplanner where name = '" & name & "'"
        'End If

        'cnt = sup.Scalar(sql)
        'Else
        cnt = 0
        'End If
        If cnt = 0 Then
            If wa <> "" Then
                If who = "super" Then
                    sql = "update pmsysusers set waid = " _
                    + "'" & waid & "', workarea = '" & wa & "', shift = '" & shift & "' where userid = '" & supid & "'"
                Else
                    sql = "update pmsysusers set waid = " _
                    + "'" & waid & "', workarea = '" & wa & "', shift = '" & shift & "' where userid = '" & supid & "'"
                End If
            Else
                If who = "super" Then
                    sql = "update pmsysusers set shift = '" & shift & "' where userid = '" & supid & "'"
                Else
                    sql = "update pmsysusers set shift = '" & shift & "' where userid = '" & supid & "'"
                End If
            End If


            sup.Update(sql)
            dgdepts.EditItemIndex = -1
            LoadSuper(PageNumber)
            lblchng.Value = "yes"
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1531", "PMSuperMan.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

        sup.Dispose()
        'End Try
    End Sub

    Private Sub dgdepts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.ItemCommand
        Dim name, phone, email, supid As String
        Dim namei, phonei, emaili As TextBox
        who = lblwho.Value
        If e.CommandName = "Add" Then

            namei = CType(e.Item.FindControl("txtnewname"), TextBox)
            name = namei.Text
            name = Replace(name, "'", Chr(180), , , vbTextCompare)
            name = Replace(name, "--", "-", , , vbTextCompare)
            name = Replace(name, ";", ":", , , vbTextCompare)
            phonei = CType(e.Item.FindControl("txtnewphone"), TextBox)
            phone = phonei.Text
            phone = Replace(phone, "'", Chr(180), , , vbTextCompare)
            phone = Replace(phone, "--", "-", , , vbTextCompare)
            phone = Replace(phone, ";", ":", , , vbTextCompare)
            emaili = CType(e.Item.FindControl("txtnewemail"), TextBox)
            email = emaili.Text
            email = Replace(email, "'", Chr(180), , , vbTextCompare)
            email = Replace(email, "--", "-", , , vbTextCompare)
            email = Replace(email, ";", ":", , , vbTextCompare)

            If namei.Text <> "" Then
                If Len(name) > 50 Then
                    Dim strMessage As String = tmod.getmsg("cdstr1532", "PMSuperMan.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    Dim cnt As Integer
                    sup.Open()
                    If who = "super" Then
                        sql = "select count(*) from pmsuper where name = '" & name & "'"
                    Else
                        sql = "select count(*) from pmplanner where name = '" & name & "'"
                    End If

                    cnt = sup.Scalar(sql)
                    If cnt = 0 Then
                        cid = "0" 'lblcid.Value
                        sid = lblsid.Value
                        If who = "super" Then

                        End If
                        If who = "super" Then
                            sql = "insert into pmsuper (name, phone, email, siteid) " _
                            + "values ('" & name & "', '" & phone & "', '" & email & "', '" & sid & "')"
                        Else
                            sql = "insert into pmplanner (name, phone, email, siteid) " _
                            + "values ('" & name & "', '" & phone & "', '" & email & "', '" & sid & "')"
                        End If

                        sup.Update(sql)
                        Dim deptcnt As Integer = dgdepts.VirtualItemCount + 1
                        If IsDBNull(dgdepts.VirtualItemCount) Or dgdepts.VirtualItemCount = 0 Then
                            deptcnt = 1
                        End If
                        namei.Text = ""
                        phonei.Text = ""
                        emaili.Text = ""
                        If who = "super" Then
                            sql = "select Count(*) from pmSuper"
                        Else
                            sql = "select Count(*) from pmplanner"
                        End If

                        PageNumber = sup.PageCount(sql, PageSize)

                        LoadSuper(PageNumber)


                        lblchng.Value = "yes"
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1533", "PMSuperMan.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                    sup.Dispose()
                End If
            End If
        End If
    End Sub

    Private Sub dgdepts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdepts.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                                 e.Item.ItemType <> ListItemType.Footer Then

            Try
                Dim id As String = DataBinder.Eval(e.Item.DataItem, "superid").ToString
                Dim img1 As HtmlImage = CType(e.Item.FindControl("i23"), HtmlImage)
                img1.Attributes("onclick") = "getassn('" & id & "','skill');"
                Dim img2 As HtmlImage = CType(e.Item.FindControl("i23a"), HtmlImage)
                img2.Attributes("onclick") = "getassn('" & id & "','dept');"
                Dim img3 As HtmlImage = CType(e.Item.FindControl("i23b"), HtmlImage)
                img3.Attributes("onclick") = "getassn('" & id & "','loc');"


            Catch ex As Exception

            End Try
        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim lblwa As Label = CType(e.Item.FindControl("lblwa"), Label)
            Dim lblwaid As Label = CType(e.Item.FindControl("lblwaid"), Label)
            Dim wa As String
            wa = lblwa.ClientID.ToString
            Dim waid As String
            waid = lblwaid.ClientID.ToString
            Dim imgwa As HtmlImage = CType(e.Item.FindControl("imgwa"), HtmlImage)
            imgwa.Attributes.Add("onclick", "getwa('" & wa & "','" & waid & "');")

            Dim userid As String = DataBinder.Eval(e.Item.DataItem, "userid").ToString
            Dim muser As String = DataBinder.Eval(e.Item.DataItem, "username").ToString
            Dim waid1 As String = DataBinder.Eval(e.Item.DataItem, "waid").ToString
            Dim wa1 As String = DataBinder.Eval(e.Item.DataItem, "workarea").ToString

            Dim imgmwa As HtmlImage = CType(e.Item.FindControl("imgmwa"), HtmlImage)
            If waid1 <> "" Then
                imgmwa.Attributes.Add("onclick", "getmwa('" & waid1 & "','" & wa1 & "','" & userid & "','" & muser & "');")
            Else
                imgmwa.Attributes.Add("class", "details")
            End If

            Dim lblshift As Label = CType(e.Item.FindControl("lblshift"), Label)
            Dim shift As String = lblshift.ClientID.ToString
            Dim imgshift As HtmlImage = CType(e.Item.FindControl("imgshift"), HtmlImage)
            imgshift.Attributes.Add("onclick", "getshift('" & shift & "');")

            Dim imgpa As HtmlImage = CType(e.Item.FindControl("imgpa"), HtmlImage)
            Dim lblwpaid As Label = CType(e.Item.FindControl("lblwpaid"), Label)
            Dim lblplanarea As Label = CType(e.Item.FindControl("lblplanarea"), Label)
            Dim wpaid As String
            wpaid = lblwpaid.ClientID.ToString
            Dim wplanarea As String
            wplanarea = lblplanarea.ClientID.ToString
            imgpa.Attributes.Add("onclick", "getpa('" & wpaid & "','" & wplanarea & "');")

        End If
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        Dim srch As String = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim filt, filtd As String
        cid = lblcid.Value
        If Len(srch) <> 0 Then
            filt = "username like '%" & srch & "%' and issuper = 1"
            filtd = "username like ''%" & srch & "%'' and issuper = 1"
            lblfilt.Value = filt
            lblfiltd.Value = filtd
            sup.Open()
            LoadSuper(1)
            sup.Dispose()
        Else
            sup.Open()
            LoadSuper(1)
            sup.Dispose()
        End If
    End Sub

    Private Sub dgdepts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.EditCommand
        PageNumber = dgdepts.CurrentPageIndex + 1
        lblold.Value = CType(e.Item.FindControl("Label4"), Label).Text
        dgdepts.EditItemIndex = e.Item.ItemIndex
        sup.Open()
        LoadSuper(PageNumber)
        sup.Dispose()
    End Sub




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("PMSuperMan.aspx", "dgdepts", "0")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(1).HeaderText = dlabs.GetDGPage("PMSuperMan.aspx", "dgdepts", "1")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(5).HeaderText = dlabs.GetDGPage("PMSuperMan.aspx", "dgdepts", "2")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(6).HeaderText = dlabs.GetDGPage("PMSuperMan.aspx", "dgdepts", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2982.Text = axlabs.GetASPXPage("PMSuperMan.aspx", "lang2982")
        Catch ex As Exception
        End Try
        Try
            lang2983.Text = axlabs.GetASPXPage("PMSuperMan.aspx", "lang2983")
        Catch ex As Exception
        End Try

    End Sub

End Class
