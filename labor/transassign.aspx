<%@ Page Language="vb" AutoEventWireup="false" Codebehind="transassign.aspx.vb" Inherits="lucy_r12.transassign" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>transassign</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/transassignaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 0px" width="740">
				<tr>
					<td height="20" class="label" colspan="4"><asp:Label id="lang2986" runat="server">Filter by Shift</asp:Label><asp:dropdownlist id="ddshift" runat="server" CssClass="plainlabel" AutoPostBack="True">
							<asp:ListItem Value="All" Selected="True">All</asp:ListItem>
							<asp:ListItem Value="1st Shift">1st Shift</asp:ListItem>
							<asp:ListItem Value="2nd Shift">2nd Shift</asp:ListItem>
							<asp:ListItem Value="3rd Shift">3rd Shift</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr height="20">
					<td class="label" align="center"><asp:Label id="lang2987" runat="server">Assigned to Others-</asp:Label></td>
					<td class="label" align="center"><asp:Label id="lang2988" runat="server">Available</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang2989" runat="server">Assigned to Supervisor</asp:Label></td>
				</tr>
				<tr>
					<td width="240" align="center"><asp:listbox id="lbassigned" runat="server" CssClass="plainlabel" Width="300px" SelectionMode="Multiple"
							Height="120px"></asp:listbox></td>
					<td width="180" align="center"><asp:listbox id="lbunassigned" runat="server" CssClass="plainlabel" Width="190px" SelectionMode="Multiple"
							Height="120px"></asp:listbox></td>
					<td vAlign="middle" width="22" align="center"><IMG id="todis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
							width="20" height="20" runat="server"> <IMG id="fromdis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
							width="20" height="20" runat="server"><IMG class="details" onmouseover="return overlib('Choose Work Request Skills')" onmouseout="return nd()"
							onclick="getss();" src="../images/appbuttons/minibuttons/plusminus.gif" width="20" height="20">
						<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
					<td width="180" align="center"><asp:listbox id="lblocations" runat="server" CssClass="plainlabel" Width="190px" SelectionMode="Multiple"
							Height="120px"></asp:listbox></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lbllhid" type="hidden" name="lbllhid" runat="server"> <input id="lbllid" type="hidden" name="lbllid" runat="server">
			<input id="lblapp" type="hidden" name="lblapp" runat="server"> <input id="lblopt" type="hidden" name="lblopt" runat="server"><input id="lblfailchk" type="hidden" name="lblfailchk" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lblsupid" type="hidden" name="lblsupid" runat="server"><input id="lbltyp" type="hidden" name="lbltyp" runat="server">
			<input id="lblup" type="hidden" runat="server"><input id="lblro" type="hidden" runat="server">
			<input type="hidden" id="lblskill" runat="server"> <input type="hidden" id="lblfilt" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
