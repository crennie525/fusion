

'********************************************************
'*
'********************************************************


Imports System.Data.SqlClient

Public Class labsched
    Inherits System.Web.UI.Page
    Dim lab As New Utilities
    Dim tmod As New transmod
    Dim userid, muser, skill, skillid, shift, sid As String
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents tdemp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtreg As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdshift As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblshift As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1st As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2nd As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3rd As System.Web.UI.HtmlControls.HtmlInputCheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            userid = Request.QueryString("userid").ToString
            lbluserid.Value = userid

            shift = Request.QueryString("shift").ToString
            If shift <> "0" Then
                lblshift.Value = shift
                tdshift.InnerHtml = shift
            Else
                lblshift.Value = shift
                tdshift.InnerHtml = "ANY"
            End If
            skill = Request.QueryString("skill").ToString
            tdskill.InnerHtml = skill
            skillid = Request.QueryString("skillid").ToString
            lblskillid.Value = skillid

            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid

            muser = Request.QueryString("muser").ToString
            tdemp.InnerHtml = muser
            lab.Open()
            GetReg(userid)
            lab.Dispose()
        Else
            If Request.Form("lblsubmit") = "save" Then
                lab.Open()
                SaveReg()
                lab.Dispose()
                lblsubmit.Value = "exit"
            End If
        End If
    End Sub
    Private Sub SaveReg()
        Dim hrs, m, t, w, th, f, s, su As String
        Dim d As String = lbld.Value
        Dim dcnt As Integer = 0
        hrs = txtreg.Text
        userid = lbluserid.Value
        Dim ochk As Long
        Try
            ochk = System.Convert.ToDecimal(hrs)
        Catch ex As Exception
            Dim strMessage As String = "Regular Hours Must Be A Numeric Value"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If cb1mon.Checked = True Then
            m = "1"
            dcnt += 1
        Else
            m = "0"
        End If
        If cb1tue.Checked = True Then
            t = "1"
            dcnt += 1
        Else
            t = "0"
        End If
        If cb1wed.Checked = True Then
            w = "1"
            dcnt += 1
        Else
            w = "0"
        End If
        If cb1thu.Checked = True Then
            th = "1"
            dcnt += 1
        Else
            th = "0"
        End If
        If cb1fri.Checked = True Then
            f = "1"
            dcnt += 1
        Else
            f = "0"
        End If
        If cb1sat.Checked = True Then
            s = "1"
            dcnt += 1
        Else
            s = "0"
        End If
        If cb1sun.Checked = True Then
            su = "1"
            dcnt += 1
        Else
            su = "0"
        End If
        If d = "1" Then
            sql = "update labregsched set hrs = '" & hrs & "', mon = '" & m & "', tue = '" & t & "', " _
            + "wed = '" & w & "', thu = '" & th & "', fri = '" & f & "', sat = '" & s & "', sun = '" & su & "', " _
            + "daycnt = '" & dcnt & "' " _
            + "where userid = '" & userid & "'"
            lab.Update(sql)
        Else
            sql = "insert into labregsched (userid, hrs, mon, tue, wed, thu, fri, sat, sun, daycnt) values " _
            + "('" & userid & "','" & hrs & "','" & m & "','" & t & "','" & w & "','" & th & "','" & f & "','" & s & "','" & su & "','" & dcnt & "')"
            lab.Update(sql)
        End If
        shift = lblshift.Value
        skillid = lblskillid.Value
        sid = lblsid.Value
        If shift <> "" And shift <> "0" Then
            sql = "update labregsched set shift = '" & shift & "' where userid = '" & userid & "'"
            lab.Update(sql)
        End If
        If skillid <> "" Then
            sql = "update labregsched set skillid = '" & skillid & "' where userid = '" & userid & "'"
            lab.Update(sql)
        End If
        If sid <> "" Then
            sql = "update labregsched set siteid = '" & sid & "' where userid = '" & userid & "'"
            lab.Update(sql)
        End If

    End Sub
    Private Sub GetReg(ByVal userid As String)
        Dim hrs, m, t, w, th, f, s, su As String
        Dim d As Integer = 0
        sql = "select l.hrs, l.mon, l.tue, l.wed, l.thu, l.fri, l.sat, l.sun " _
        + "from labregsched l where l.userid = '" & userid & "'"
        dr = lab.GetRdrData(sql)
        While dr.Read
            d = 1
            hrs = dr.Item("hrs").ToString
            m = dr.Item("mon").ToString
            t = dr.Item("tue").ToString
            w = dr.Item("wed").ToString
            th = dr.Item("thu").ToString
            f = dr.Item("fri").ToString
            s = dr.Item("sat").ToString
            su = dr.Item("sun").ToString
        End While
        dr.Close()
        If d = 1 Then
            txtreg.Text = hrs
            If m = "1" Then
                cb1mon.Checked = True
            Else
                cb1mon.Checked = False
            End If
            If t = "1" Then
                cb1tue.Checked = True
            Else
                cb1tue.Checked = False
            End If
            If w = "1" Then
                cb1wed.Checked = True
            Else
                cb1wed.Checked = False
            End If
            If th = "1" Then
                cb1thu.Checked = True
            Else
                cb1thu.Checked = False
            End If
            If f = "1" Then
                cb1fri.Checked = True
            Else
                cb1fri.Checked = False
            End If
            If s = "1" Then
                cb1sat.Checked = True
            Else
                cb1sat.Checked = False
            End If
            If su = "1" Then
                cb1sun.Checked = True
            Else
                cb1sun.Checked = False
            End If
        Else
            tdmsg.InnerHtml = "No Data for this Employee"

        End If
        lbld.Value = d
    End Sub
End Class
