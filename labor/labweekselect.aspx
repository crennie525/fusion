<%@ Page Language="vb" AutoEventWireup="false" Codebehind="labweekselect.aspx.vb" Inherits="lucy_r12.labweekselect"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>labweekselect</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="javascript" type="text/javascript">
		function getweek(fd,ld,efd,eld, fd2, ld2, fd3, ld3, wkid, wkid2, wkid3, wkid4) {
		var ret = fd + "~" + ld + "~" + efd + "~" + eld + "~" + fd2 + "~" + ld2 + "~" + fd3 + "~" + ld3 + "~" + wkid + "~" + wkid2 + "~" + wkid3 + "~" + wkid4;
		//alert(ret)
		window.parent.handlereturn(ret);
		}
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bluelabel">Select Start Week</td>
				</tr>
				<tr>
					<td id="tdwks" runat="server" width="200">
						<div style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; HEIGHT: 300px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							id="wksdiv" runat="server"></div>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
