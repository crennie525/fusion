<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMSuperSelect.aspx.vb" Inherits="lucy_r12.PMSuperSelect1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Super Select</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/PMSuperSelectaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function getsup(id, val, id1, val1) {
            //alert(id + "," + val + "," + id1 + "," + val1)
                var chk = document.getElementById("lblsavewo").value;
                //alert(chk)
                if (chk == "yes") {
                    document.getElementById("lblid").value = id;
                    document.getElementById("lblval").value = val;
                    document.getElementById("lblid1").value = id1;
                    document.getElementById("lblval1").value = val1;
                    document.getElementById("lblsubmit").value = "savewo";
                    document.getElementById("form1").submit();
                }
                var chk2 = document.getElementById("lblsavemax").value;
                //alert(chk)
                if (chk2 == "yes") {
                    document.getElementById("lblid").value = id;
                    document.getElementById("lblval").value = val;
                    document.getElementById("lblid1").value = id1;
                    document.getElementById("lblval1").value = val1;
                    document.getElementById("lblsubmit").value = "savemax";
                    document.getElementById("form1").submit();
                }
                else {
                    window.parent.handlesup(id, val, id1, val1);
                }

            }
            function checkit() {
                var chk = document.getElementById("lblsubmit").value;
                if (chk == "return") {
                    id = document.getElementById("lblid").value;
                    val = document.getElementById("lblval").value;
                    id1 = document.getElementById("lblid1").value;
                    val1 = document.getElementById("lblval1").value;
                    window.parent.handlesup(id, val, id1, val1);
                }
            }
        </script>
	</HEAD>
	<body  class="tbg" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 4px; LEFT: 4px" cellSpacing="1" cellPadding="1"
				width="300">
				<tr id="trsrch" runat="server">
					<td class="bluelabel"><asp:Label id="lang2984" runat="server">Search By Name</asp:Label></td>
					<td><asp:textbox id="txtsrch" runat="server" CssClass="plainlabel"></asp:textbox></td>
					<td><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td><IMG class="details" id="imgadd" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
							width="20" runat="server"></td>
				</tr>
				<tr class="details" id="trskill" runat="server">
					<td class="bluelabel"><asp:Label id="lang2985" runat="server">Filter By Skill</asp:Label></td>
					<td colSpan="3"><asp:dropdownlist id="ddskill" runat="server" CssClass="plainlabel" AutoPostBack="True"></asp:dropdownlist></td>
				</tr>
				<tr id="tr1" runat="server">
					<td colSpan="4">
						<div id="supdiv" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; HEIGHT: 300px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
				<tr id="tr2" runat="server">
					<td colSpan="4">
						<div id="supdiv2" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 280px; HEIGHT: 94px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
			</table>
			<input id="lblpost" type="hidden" runat="server"><input id="lbltyp" type="hidden" runat="server">
			<input id="lblskillid" type="hidden" runat="server"><input id="lblsupid" type="hidden" runat="server">
			<input type="hidden" id="lblsid" runat="server"><input type="hidden" id="lblro" runat="server">
			<input type="hidden" id="lblfslang" runat="server" /> <input type="hidden" id="lblwonum" runat="server">
			<input type="hidden" id="lblsavewo" runat="server"><input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lblid" runat="server"> <input type="hidden" id="lblval" runat="server">
			<input type="hidden" id="lblid1" runat="server"> <input type="hidden" id="lblval1" runat="server">
            <input type="hidden" id="lblsavemax" runat="server" /><input type="hidden" id="lblpmid" runat="server" />
		</form>
	</body>
</HTML>
