

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class conflicts
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim con As New Utilities
    Dim sql As String
    Dim sid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdconflict As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString
            sql = "usp_getconflicts '" & sid & "'"
            Dim sb As New StringBuilder
            Dim shold As String = "0"
            Dim dhold As String = "0"
            Dim sphold As String = "0"
            Dim hhold As String = "0"
            Dim lcnt As Integer = 0
            Dim skillid, skill, dept, super, eqid, eqnum, shift As String
            sb.Append("<table cellspacing=""2"" width=""290""><tr height=""20"">" & vbCrLf)
            sb.Append("<td width=""155""></td>" & vbCrLf)
            sb.Append("<td width=""135""></td></tr>" & vbCrLf)
            'sb.Append("<td width=""100""></td></tr>" & vbCrLf)

            'sb.Append("<td class=""label""><u>Supervisor</u></td></tr>" & vbCrLf)
            con.Open()
            dr = con.GetRdrData(sql)
            While dr.Read
                If hhold = "0" Then
                    hhold = "1"
                    sb.Append("<tr><td class=""label""><u>Department</u></td>" & vbCrLf)
                    sb.Append("<td class=""label""><u>Skill</u></td></tr>" & vbCrLf)
                End If
                lcnt += 1
                skillid = dr.Item("skillid").ToString
                skill = dr.Item("skill").ToString
                dept = dr.Item("dept_line").ToString
                super = dr.Item("username").ToString
                If shold = "0" Then
                    shold = skillid
                End If
                If shold <> skillid Then
                    shold = skillid
                    sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>" & vbCrLf)

                    sb.Append("<tr><td class=""plainlabel"">" & dept & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"">" & skill & "</td></tr>" & vbCrLf)
                    sb.Append("<tr><td class=""plainlabelred"" colspan=""2"">Supervisor:&nbsp;&nbsp;" & super & "</td></tr>" & vbCrLf)
                Else
                    If dhold <> dept Then
                        dhold = dept
                        sb.Append("<tr><td class=""plainlabel"">" & dept & "</td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"">" & skill & "</td></tr>" & vbCrLf)
                    End If
                    sb.Append("<tr><td class=""plainlabelred"" colspan=""2"">Supervisor:&nbsp;&nbsp;" & super & "</td></tr>" & vbCrLf)
                End If
            End While
            dr.Close()
            shold = "0"
            dhold = "0"
            hhold = "0"

            sql = "usp_getconflictsl '" & sid & "'"
            dr = con.GetRdrData(sql)
            While dr.Read
                If hhold = "0" Then
                    hhold = "1"
                    sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>" & vbCrLf)
                    sb.Append("<tr><td class=""label""><u>Location</u></td>" & vbCrLf)
                    sb.Append("<td class=""label""><u>Skill</u></td></tr>" & vbCrLf)
                End If
                lcnt += 1
                skillid = dr.Item("skillid").ToString
                skill = dr.Item("skill").ToString
                dept = dr.Item("location").ToString
                super = dr.Item("username").ToString
                If shold = "0" Then
                    shold = skillid
                End If
                If shold <> skillid Then
                    shold = skillid
                    sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>" & vbCrLf)

                    sb.Append("<tr><td class=""plainlabel"">" & dept & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"">" & skill & "</td></tr>" & vbCrLf)
                    sb.Append("<tr><td class=""plainlabelred"" colspan=""2"">Supervisor:&nbsp;&nbsp;" & super & "</td></tr>" & vbCrLf)
                Else
                    If dhold <> dept Then
                        dhold = dept
                        sb.Append("<tr><td class=""plainlabel"">" & dept & "</td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"">" & skill & "</td></tr>" & vbCrLf)
                    End If
                    sb.Append("<tr><td class=""plainlabelred"" colspan=""2"">Supervisor:&nbsp;&nbsp;" & super & "</td></tr>" & vbCrLf)
                End If
            End While
            dr.Close()
            shold = "0"
            dhold = "0"
            hhold = "0"



            sql = "usp_getconflictse '" & sid & "'"
            dr = con.GetRdrData(sql)
            While dr.Read
                If hhold = "0" Then
                    hhold = "1"
                    sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>" & vbCrLf)
                    sb.Append("<tr><td class=""label""><u>Equipment</u></td>" & vbCrLf)
                    sb.Append("<td class=""label""><u>Supervisor</u></td><td class=""label""><u>Shift</u></td></tr>" & vbCrLf)
                End If
                lcnt += 1
                eqid = dr.Item("eqid").ToString
                eqnum = dr.Item("eqnum").ToString
                shift = dr.Item("shift").ToString
                super = dr.Item("username").ToString
                If shift = "0" Then
                    shift = "Any Shift"
                ElseIf shift = "1" Then
                    shift = "1st Shift"
                ElseIf shift = "2" Then
                    shift = "2nd Shift"
                ElseIf shift = "3" Then
                    shift = "3rd Shift"
                End If

                sb.Append("<tr><td colspan=""3"">&nbsp;</td></tr>" & vbCrLf)

                sb.Append("<tr><td class=""plainlabel"">" & eqnum & "</td>" & vbCrLf)
                sb.Append("<td class=""plainlabel"">" & super & "</td></tr>" & vbCrLf)
                sb.Append("<tr><td class=""plainlabelred"" colspan=""2"">" & shift & "</td></tr>" & vbCrLf)

            End While
            dr.Close()

            If lcnt = 0 Then
                sb.Append("<tr><td class=""plainlabelred"" colspan=""2"">" & tmod.getlbl("cdlbl582" , "conflicts.aspx.vb") & "</td></tr>" & vbCrLf)
            End If
            con.Dispose()
            sb.Append("</td></tr></table>")
            tdconflict.InnerHtml = sb.ToString
        Catch ex As Exception

        End Try
       

    End Sub

End Class
