

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrassign
    Inherits System.Web.UI.Page
	Protected WithEvents lang3010 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3009 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3008 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3007 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3006 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3005 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3004 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3003 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3002 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3001 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3000 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2999 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2998 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2997 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sup As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents ddsup As System.Web.UI.WebControls.DropDownList
    Dim sql As String
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdemail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents getlab As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents getskill As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsel As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sid, supid, ro As String
    Protected WithEvents getdept As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tdconflicts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifcon As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tddef As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ddsite As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdshift As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsupret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdsite As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents getloc As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            lblsid.Value = HttpContext.Current.Session("dfltps").ToString
            sup.Open()
            PopSuper()
            GetDef()
            sup.Dispose()
        Else
            If Request.Form("lblsubmit") = "def" Then
                lblsubmit.Value = ""
                sup.Open()
                SaveDef()
                sup.Dispose()
            End If
        End If
        ddsite.Attributes.Add("onchange", "getsite(this.value);")
    End Sub
   
    Private Sub SaveDef()
        supid = lblsupret.Value
        sid = lblsid.Value
        sql = "update sites set wrdefault = '" & supid & "' where siteid = '" & sid & "' "
        sup.Update(sql)
        GetDef()
    End Sub
    Private Sub GetDef()
        sid = lblsid.Value
        Dim super, site As String
        sql = "select u.username, s.sitename from sites s left join pmsysusers u on u.userid = s.wrdefault where s.siteid = '" & sid & "'"
        dr = sup.GetRdrData(sql)
        While dr.Read
            super = dr.Item("username").ToString
            site = dr.Item("sitename").ToString
        End While
        dr.Close()
        'lblsup.Value = supid
        If site = "" Then
            site = "No Site Selected"
        End If
        If super = "" Then
            super = "None Selected"
        End If
        tdsite.InnerHtml = "Default Supervisor for Missed Assignments - " & site & ":"
        tddef.InnerHtml = super
    End Sub
    Private Sub PopSuper()
        sid = lblsid.Value
        sql = "select userid, username from pmsysusers where issuper = 1" 'siteid = '" & sid & "'"
        dr = sup.GetRdrData(sql)
        ddsup.DataSource = dr
        ddsup.DataTextField = "username"
        ddsup.DataValueField = "userid"
        ddsup.DataBind()
        dr.Close()
        ddsup.Items.Insert(0, "Select Supervisor")
    End Sub
    Private Sub PopSites(ByVal supid As String)
        sql = "select u.siteid, s.sitename from pmusersites u left join sites s on s.siteid = u.siteid " _
        + "left join pmsysusers p on p.uid = u.uid where p.userid = '" & supid & "'"
        dr = sup.GetRdrData(sql)
        ddsite.DataSource = dr
        ddsite.DataTextField = "sitename"
        ddsite.DataValueField = "siteid"
        ddsite.DataBind()
        dr.Close()
        ddsite.Items.Insert(0, "Select Site")
    End Sub
    Private Sub ddsup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsup.SelectedIndexChanged
        Dim supid As String
        If ddsup.SelectedIndex <> 0 Or ddsup.SelectedIndex <> -1 Then
            lblsupid.Value = ddsup.SelectedValue.ToString
            supid = ddsup.SelectedValue.ToString
            sup.Open()
            GetSupDets()
            PopSites(supid)
            sup.Dispose()
            lblsel.Value = "rev"
        End If


    End Sub
   
    Private Sub GetSupDets()
        supid = lblsupid.Value
        sql = "select shift, phonenum, email from pmsysusers where userid = '" & supid & "'"
        dr = sup.GetRdrData(sql)
        While dr.Read
            tdshift.InnerHtml = dr.Item("shift").ToString
            tdphone.InnerHtml = dr.Item("phonenum").ToString
            tdemail.InnerHtml = dr.Item("email").ToString
        End While
        dr.Close()
    End Sub


	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2997.Text = axlabs.GetASPXPage("wrassign.aspx","lang2997")
		Catch ex As Exception
		End Try
		Try
			lang2998.Text = axlabs.GetASPXPage("wrassign.aspx","lang2998")
		Catch ex As Exception
		End Try
		Try
			lang2999.Text = axlabs.GetASPXPage("wrassign.aspx","lang2999")
		Catch ex As Exception
		End Try
		Try
			lang3000.Text = axlabs.GetASPXPage("wrassign.aspx","lang3000")
		Catch ex As Exception
		End Try
		Try
			lang3001.Text = axlabs.GetASPXPage("wrassign.aspx","lang3001")
		Catch ex As Exception
		End Try
		Try
			lang3002.Text = axlabs.GetASPXPage("wrassign.aspx","lang3002")
		Catch ex As Exception
		End Try
		Try
			lang3003.Text = axlabs.GetASPXPage("wrassign.aspx","lang3003")
		Catch ex As Exception
		End Try
		Try
			lang3004.Text = axlabs.GetASPXPage("wrassign.aspx","lang3004")
		Catch ex As Exception
		End Try
		Try
			lang3005.Text = axlabs.GetASPXPage("wrassign.aspx","lang3005")
		Catch ex As Exception
		End Try
		Try
			lang3006.Text = axlabs.GetASPXPage("wrassign.aspx","lang3006")
		Catch ex As Exception
		End Try
		Try
			lang3007.Text = axlabs.GetASPXPage("wrassign.aspx","lang3007")
		Catch ex As Exception
		End Try
		Try
			lang3008.Text = axlabs.GetASPXPage("wrassign.aspx","lang3008")
		Catch ex As Exception
		End Try
		Try
			lang3009.Text = axlabs.GetASPXPage("wrassign.aspx","lang3009")
		Catch ex As Exception
		End Try
		Try
			lang3010.Text = axlabs.GetASPXPage("wrassign.aspx","lang3010")
		Catch ex As Exception
		End Try

	End Sub

End Class
