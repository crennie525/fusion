Imports System.Text
Public Class labshifts
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divval As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Dim sb As New StringBuilder
            sb.Append("<table>")
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('Any')"">")
            sb.Append("Any</a>")
            sb.Append("</td></tr>")

            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('First')"">")
            sb.Append("First</a>")
            sb.Append("</td></tr>")

            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('Second')"">")
            sb.Append("Second</a>")
            sb.Append("</td></tr>")

            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('Third')"">")
            sb.Append("Third</a>")
            sb.Append("</td></tr>")
            sb.Append("</table>")

            divval.InnerHtml = sb.ToString
        End If
    End Sub

End Class
