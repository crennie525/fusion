<%@ Page Language="vb" AutoEventWireup="false" Codebehind="labadmin.aspx.vb" Inherits="lucy_r12.labadmin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>labadmin</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bluelabel" height="22">Name</td>
					<td class="plainlabel" id="tdname" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22">Work Area</td>
					<td class="plainlabel" id="tdarea" runat="server"></td>
					<td><img src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getwa();"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22">Primary Skill</td>
					<td class="plainlabel" id="tdskill" runat="server"><asp:dropdownlist id="ddskill" runat="server"></asp:dropdownlist></td>
					<td><img src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getots();"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22">Labor Rate</td>
					<td class="plainlabel" id="tdrate" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22">OT Rate</td>
					<td class="plainlabel" id="tdot" runat="server"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
