﻿Imports System.Data.SqlClient
Public Class super_assign
    Inherits System.Web.UI.Page
    Dim sid, supid2, supid3, sup2, sup3, srch As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim sup As New Utilities
    Dim tmod As New transmod
    Dim PageNumber As Integer
    Dim PageSize As Integer = 50
    Dim intPgNav As Integer
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = "23" 'Request.QueryString("sid").ToString
            txttab.Value = "eq"
            lblsid.Value = sid
            sup.Open()
            popsup2()
            PageNumber = 1
            popeq(PageNumber)
            sup.Dispose()
            txtsrch.Attributes.Add("onkeyup", "versrch();")
        Else
            If Request.Form("lblret") = "next" Then
                sup.Open()
                GetNext()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                sup.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                popeq(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                sup.Open()
                GetPrev()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                sup.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                popeq(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "eq" Then
                sup.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                popeq(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "jp" Then
                sup.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                popjp(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "pm" Then
                sup.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                poppm(PageNumber)
                sup.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            popeq(PageNumber)
        Catch ex As Exception
            sup.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            sup.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            popeq(PageNumber)
        Catch ex As Exception
            sup.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            sup.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub popsup2()
        sid = lblsid.Value
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from pmsysusers where issuper = 1 and dfltps = '" & sid & "'"
        dr = sup.GetRdrData(sql)
        While dr.Read
            supid2 = dr.Item("userid").ToString
            sup2 = dr.Item("username").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup2('" & supid2 & "', '" & sup2 & "');"">" & sup2 & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv2.InnerHtml = sb.ToString
    End Sub
    Private Sub popeq(ByVal PageNumber As String)
        sid = lblsid.Value
        Filter = " siteid = '" & sid & "' "
        FilterCNT = " where siteid = '" & sid & "' "
        srch = txtsrch.Text
        If srch <> "" Then
            srch = sup.ModString2(srch)
            Filter += " and eqnum like '%" & srch & "%' or eqdesc like '%" & srch & "%' "
            FilterCNT += " and eqnum like '%" & srch & "%' or eqdesc like '%" & srch & "%' "
        End If

        sql = "SELECT Count(*) FROM equipment " & FilterCNT
        Dim eqcnt As Integer
        'eqcnt = sup.Scalar(sql)
        Tables = "equipment"
        PK = "eqid"
        Sort = "eqnum asc"
        Fields = "eqid, eqnum + ' - ' + eqdesc as eqnum"
        PageSize = "200"
        dr = sup.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        supdiv3.DataSource = dr
        supdiv3.DataValueField = "eqid"
        supdiv3.DataTextField = "eqnum"
        supdiv3.DataBind()
        dr.Close()
        intPgNav = sup.PageCount(sql, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
    End Sub
    Private Sub popjp(ByVal PageNumber As String)
        sid = lblsid.Value
        Filter = " siteid = '" & sid & "' "
        FilterCNT = " where siteid = '" & sid & "' "
        srch = txtsrch.Text
        If srch <> "" Then
            srch = sup.ModString2(srch)
            Filter += " and jpnum like '%" & srch & "%' or description like '%" & srch & "%' "
            FilterCNT += " and jpnum like '%" & srch & "%' or description like '%" & srch & "%' "
        End If

        sql = "SELECT Count(*) FROM pmjobplans " & FilterCNT
        Dim eqcnt As Integer
        'eqcnt = sup.Scalar(sql)
        Tables = "pmjobplans"
        PK = "jpid"
        Sort = "jpnum asc"
        Fields = "jpid, jpnum + ' - ' + description as eqnum"
        PageSize = "200"
        dr = sup.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        supdiv3.DataSource = dr
        supdiv3.DataValueField = "jpid"
        supdiv3.DataTextField = "eqnum"
        supdiv3.DataBind()
        dr.Close()
        intPgNav = sup.PageCount(sql, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
    End Sub
    Private Sub poppm(ByVal PageNumber As String)
        sid = lblsid.Value
        'Filter = " siteid = '" & sid & "' "
        'FilterCNT = " where siteid = '" & sid & "' "
        srch = txtsrch.Text
        If srch <> "" Then
            srch = sup.ModString2(srch)
            Filter += " and pmnum like '%" & srch & "%' or description like '%" & srch & "%' "
            FilterCNT += " and pmnum like '%" & srch & "%' or description like '%" & srch & "%' "
        End If

        sql = "SELECT Count(*) FROM pm " & FilterCNT
        Dim eqcnt As Integer
        'eqcnt = sup.Scalar(sql)
        Tables = "p.pm, e.equipment"
        PK = "p.pmid int"
        Sort = "e.eqnum asc"
        Fields = "p.pmid, eqnum = (select e.eqnum from equipment e where e.eqid = p.eqid) + ' - ' + case isnull(ptid, 0) when 0 then skill + '/' + cast(freq as varchar(50)) + ' DAYS/' + rd else pretech + '/' + skill + '/' + cast(freq as varchar(50)) + ' DAYS/' + rd end "
        PageSize = "200"
        dr = sup.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        supdiv3.DataSource = dr
        supdiv3.DataValueField = "p.pmid"
        supdiv3.DataTextField = "eqnum"
        supdiv3.DataBind()
        dr.Close()
        intPgNav = sup.PageCount(sql, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
    End Sub
End Class