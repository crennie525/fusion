

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrselect
    Inherits System.Web.UI.Page
	Protected WithEvents lang3032 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3031 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3030 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3029 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3028 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3027 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3026 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3025 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, supid, name, srch, cid, typ, skill, lsupid, sid, ro, rostr, shift, super As String
    Dim dr As SqlDataReader
    Dim sup As New Utilities
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Tables As String = "pmsysusers"
    Dim PK As String = "userid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim intPgCnt, intPgNav As Integer
    Protected WithEvents tdsup As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuper As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllabid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents supdiv2 As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddshift As System.Web.UI.WebControls.DropDownList
    Protected WithEvents supdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            supid = Request.QueryString("supid").ToString
            lblsupid.Value = supid
            lblsid.Value = HttpContext.Current.Session("dfltps").ToString
            txtpg.Value = "1"
            sup.Open()
            LoadSkills()
            LoadLabor()
            LoadSuper()
            GetDetails()
            sup.Dispose()
        Else
            If Request.Form("lblsubmit") = "addlab" Then
                lblsubmit.Value = ""
                sup.Open()
                AddLab()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadLabor()
                LoadSuper()
                lbllabid.Value = ""
                sup.Dispose()
            ElseIf Request.Form("lblsubmit") = "remlab" Then
                lblsubmit.Value = ""
                sup.Open()
                RemLab()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadLabor()
                LoadSuper()
                lbllabid.Value = ""
                sup.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                sup.Open()
                GetNext()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                sup.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadLabor()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                sup.Open()
                GetPrev()
                sup.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                sup.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadLabor()
                sup.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub AddLab()
        Dim labid As String = lbllabid.Value
        supid = lblsupid.Value
        super = lblsuper.Value
        sql = "update pmsysusers set super = '" & super & "', superid = '" & supid & "' where userid = '" & labid & "'"
        sup.Update(sql)

    End Sub
    Private Sub RemLab()
        Dim labid As String = lbllabid.Value
        supid = lblsupid.Value
        super = lblsuper.Value
        sql = "update pmsysusers set super = null, superid = null where userid = '" & labid & "'"
        sup.Update(sql)

    End Sub
    Private Sub GetDetails()
        supid = lblsupid.Value
        Dim super As String
        sql = "select username, shift from pmsysusers where userid = '" & supid & "'"
        dr = sup.GetRdrData(sql)
        While dr.Read
            super = dr.Item("username").ToString
        End While
        dr.Close()
        tdsup.InnerHtml = super
        lblsuper.Value = super
    End Sub
    Private Sub LoadSuper()
        supid = lblsupid.Value
        If supid <> "" Then
            If ddskill.SelectedIndex = 0 Or ddskill.SelectedIndex = -1 Then
                sql = "select * from pmsysusers where superid = '" & supid & "'"
            Else
                skill = ddskill.SelectedValue.ToString
                If skill = "0" Or ddskill.SelectedIndex = -1 Then
                    sql = "select * from pmsysusers where superid = '" & supid & "'"
                Else
                    sql = "select * from pmsysusers where skillid = '" & skill & "' and superid = '" & supid & "'"
                End If

            End If
            Dim labid, super, email, skillid, shift As String
            Dim sb As New System.Text.StringBuilder
            sb.Append("<table>")
            dr = sup.GetRdrData(sql)
            While dr.Read
                labid = dr.Item("userid").ToString
                name = dr.Item("username").ToString
                skill = dr.Item("skill").ToString
                skillid = dr.Item("skillid").ToString
                supid = dr.Item("superid").ToString
                super = dr.Item("super").ToString
                shift = dr.Item("shift").ToString
                If shift = "0" Then
                    shift = "Any Shift"
                ElseIf shift = "1" Then
                    shift = "1st Shift"
                ElseIf shift = "2" Then
                    shift = "2nd Shift"
                ElseIf shift = "3" Then
                    shift = "3rd Shift"
                Else
                    shift = "Any Shift"
                End If
                sb.Append("<tr><td class=""plainlabelblue""><a href=""#"" onclick=""getlab('" & labid & "');"">" & name & "</a></td>")
                sb.Append("<td class=""plainlabel"">" & skill & "</td>")
                sb.Append("<td class=""plainlabel"">" & shift & "</td></tr>")
            End While
            dr.Close()
            sb.Append("</table>")
            supdiv2.InnerHtml = sb.ToString
        End If
    End Sub

    Private Sub LoadLabor()
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        sid = lblsid.Value
        Dim dflg As Integer = 0
        If Len(srch) > 0 Then
            srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
            srch = Replace(srch, "--", "-", , , vbTextCompare)
            srch = Replace(srch, ";", ":", , , vbTextCompare)
            srch = "%" & srch & "%"
            Filter = "username like ''%" & srch & "%'' and islabor = 1 and dfltps = ''" & sid & "''"
            FilterCnt = "username like '%" & srch & "%' and islabor = 1 and dfltps = '" & sid & "'"
        Else

            If ddskill.SelectedIndex <> 0 Then
                dflg = 1
                skill = ddskill.SelectedValue
                Filter = "skill = ''" & skill & "'' and islabor = 1 and superid is null"
                FilterCnt = "skill = '" & skill & "' and islabor = 1 and superid is null"
            End If
            If ddshift.SelectedIndex <> 0 Then
                shift = ddshift.SelectedValue
                If shift = "1st Shift" Then
                    shift = "1"
                ElseIf shift = "2nd Shift" Then
                    shift = "2"
                ElseIf shift = "3rd Shift" Then
                    shift = "3"
                End If
                If dflg = 1 Then
                    Filter += " and shift = ''" & shift & "''"
                    FilterCnt += " and shift = '" & shift & "'"
                Else
                    Filter = "shift = ''" & shift & "'' and islabor = 1 and superid is null"
                    FilterCnt = "shift = '" & shift & "' and islabor = 1 and superid is null"
                End If

            End If

        End If
        If Filter = "" Then
            Filter = "islabor = 1 and superid is null"
            FilterCnt = "islabor = 1 and superid is null"
        End If
        If Len(srch) = 0 Then
            Filter += " or superid <> ''" & supid & "''"
            FilterCnt += " or superid <> '" & supid & "'"
        End If

        sql = "select count(*) from pmsysusers where " & FilterCnt
        PageNumber = txtpg.Value
        intPgNav = sup.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        Dim labid, super, email, skillid As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        dr = sup.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        While dr.Read
            labid = dr.Item("userid").ToString
            name = dr.Item("username").ToString
            skill = dr.Item("skill").ToString
            skillid = dr.Item("skillid").ToString
            supid = dr.Item("superid").ToString
            super = dr.Item("super").ToString
            If Len(srch) = 0 Then
                sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup('" & labid & "');"">" & name & "</a></td>")
                sb.Append("<td class=""plainlabel"">" & skill & "</td></tr>")
            Else
                If supid = "" Then
                    sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup('" & labid & "');"">" & name & "</a></td>")
                    sb.Append("<td class=""plainlabel"">" & skill & "</td><td></td></tr>")
                Else
                    sb.Append("<tr><td class=""plainlabelred"">" & name & "</td>")
                    sb.Append("<td class=""plainlabelred"">" & skill & "</td><td class=""plainlabelred"">" & super & "'</td></tr>")
                End If
            End If

        End While
        dr.Close()
        sb.Append("</table>")
        supdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadSkills()
        cid = "0"
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = sup.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("All Skills"))
        ddskill.Items(0).Value = 0

    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadLabor()
        Catch ex As Exception
            sup.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1534" , "wrselect.aspx.vb")
 
            sup.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadLabor()
        Catch ex As Exception
            sup.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1535" , "wrselect.aspx.vb")
 
            sup.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3025.Text = axlabs.GetASPXPage("wrselect.aspx", "lang3025")
        Catch ex As Exception
        End Try
        Try
            lang3026.Text = axlabs.GetASPXPage("wrselect.aspx", "lang3026")
        Catch ex As Exception
        End Try
        Try
            lang3027.Text = axlabs.GetASPXPage("wrselect.aspx", "lang3027")
        Catch ex As Exception
        End Try
        Try
            lang3028.Text = axlabs.GetASPXPage("wrselect.aspx", "lang3028")
        Catch ex As Exception
        End Try
        Try
            lang3029.Text = axlabs.GetASPXPage("wrselect.aspx", "lang3029")
        Catch ex As Exception
        End Try
        Try
            lang3030.Text = axlabs.GetASPXPage("wrselect.aspx", "lang3030")
        Catch ex As Exception
        End Try
        Try
            lang3031.Text = axlabs.GetASPXPage("wrselect.aspx", "lang3031")
        Catch ex As Exception
        End Try
        Try
            lang3032.Text = axlabs.GetASPXPage("wrselect.aspx", "lang3032")
        Catch ex As Exception
        End Try

    End Sub

End Class
