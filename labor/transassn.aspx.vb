

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class transassn
    Inherits System.Web.UI.Page
	Protected WithEvents lang2996 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2995 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2994 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2993 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2992 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2991 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2990 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim assn As New Utilities
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim supid, cid, typ, sid As String
    Protected WithEvents ifromassn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbassigned As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbunassigned As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblocations As System.Web.UI.WebControls.ListBox
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lbchecked As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllhid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            'Try
            supid = "0" 'Request.QueryString("supid").ToString
            lblsupid.Value = supid
            typ = "dept" 'Request.QueryString("typ").ToString
            lbltyp.Value = typ
            If typ = "skill" Then
                ifromassn.Attributes.Add("src", "../images/appbuttons/minibuttons/forwardgraybg.gif")
                ifromassn.Enabled = False
            End If
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            lblsid.Value = "12" 'HttpContext.Current.Session("dfltps").ToString
            lblopt.Value = "0"
            lbassigned.AutoPostBack = False
            lbunassigned.AutoPostBack = False
            lblocations.AutoPostBack = False
            assn.Open()
            GetSuper(supid)
            PopAssigned()
            PopUnAssigned()
            PopSuperAssigned()
            assn.Dispose()
        End If
        'btnaddfail.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnaddfail.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")
    End Sub
    Private Sub GetSuper(ByVal supid As String)
        supid = lblsupid.Value
        sql = "select name from pmsuper where superid = '" & supid & "'"
        dr = assn.GetRdrData(sql)
        While dr.Read
            tdcomp.InnerHtml = dr.Item("name").ToString
        End While
        dr.Close()

    End Sub
    Private Sub PopAssigned()
        supid = lblsupid.Value
        sid = lblsid.Value
        typ = lbltyp.Value
        If typ = "skill" Then
            sql = "select skillid, skill from pmskills where skillid in " _
                  + "(select s.skillid from pmsupercrafts s where s.siteid = '" & sid & "')"
            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "skillid"
            lbassigned.DataTextField = "skill"
            lbassigned.DataBind()
            dr.Close()
        End If
        Try
            lbassigned.SelectedIndex = 0
        Catch ex As Exception

        End Try

        If typ = "dept" Then
            sql = "select d.dept_id, d.dept_line + ' - ' + s.name as 'dept' from dept d " _
            + "left join pmsuperlocs sl on sl.dept_id = d.dept_id " _
            + "left join pmsuper s on s.superid = sl.superid where d.dept_id in " _
            + "(select sl.dept_id from pmsuperlocs sl where sl.siteid = '" & sid & "' and sl.superid <> '" & supid & "')"
            dr = assn.GetRdrData(sql)
            lbassigned.DataSource = dr
            lbassigned.DataValueField = "dept_id"
            lbassigned.DataTextField = "dept"
            lbassigned.DataBind()
            dr.Close()
        End If
    End Sub
    Private Sub PopUnAssigned()
        sid = lblsid.Value
        supid = lblsupid.Value
        typ = lbltyp.Value
        If typ = "skill" Then
            sql = "select skillid, skill from pmskills where skillid not in " _
                   + "(select s.skillid from pmsupercrafts s where s.siteid = '" & sid & "' and s.superid = '" & supid & "')"
            dr = assn.GetRdrData(sql)
            lbunassigned.DataSource = dr
            lbunassigned.DataTextField = "skill"
            lbunassigned.DataValueField = "skillid"
            lbunassigned.DataBind()
            dr.Close()
        End If
        If typ = "dept" Then
            sql = "select dept_id, dept_line from dept where dept_id not in " _
                   + "(select s.dept_id from pmsuperlocs s where s.siteid = '" & sid & "' and s.superid = '" & supid & "')"
            dr = assn.GetRdrData(sql)
            lbunassigned.DataSource = dr
            lbunassigned.DataTextField = "dept_line"
            lbunassigned.DataValueField = "dept_id"
            lbunassigned.DataBind()
            dr.Close()
        End If
    End Sub
    Private Sub PopSuperAssigned()
        sid = lblsid.Value
        supid = lblsupid.Value
        typ = lbltyp.Value
        If typ = "skill" Then
            sql = "select skillid, skill from pmskills where skillid in " _
                    + "(select s.skillid from pmsupercrafts s where s.siteid = '" & sid & "' and s.superid = '" & supid & "')"
            dr = assn.GetRdrData(sql)
            lblocations.DataSource = dr
            lblocations.DataTextField = "skill"
            lblocations.DataValueField = "skillid"
            lblocations.DataBind()
            dr.Close()
        End If
        If typ = "dept" Then
            sql = "select dept_id, dept_line from dept where dept_id in " _
                                + "(select s.skillid from pmsupercrafts s where s.siteid = '" & sid & "' and s.superid = '" & supid & "')"
            dr = assn.GetRdrData(sql)
            lblocations.DataSource = dr
            lblocations.DataTextField = "dept_line"
            lblocations.DataValueField = "dept_id"
            lblocations.DataBind()
            dr.Close()
        End If
       
    End Sub
    Private Sub ToLoc()
        cid = lblcid.Value
        Dim Item As ListItem
        Dim f, fi As String
        assn.Open()
        For Each Item In lbunassigned.Items
            If Item.Selected Then
                fi = Item.Value.ToString
                GetItems(fi)
            End If
        Next
        PopSuperAssigned()
        PopUnAssigned()
        assn.Dispose()
    End Sub
    Private Sub GetItems(ByVal skillid As String)
        supid = lblsupid.Value
        sid = lblsid.Value
        Dim fcnt As Integer
        sql = "insert into pmsupercrafts (siteid, skillid, superid) values('" & sid & "','" & skillid & "','" & supid & "')"
        assn.Update(sql)
    End Sub
    Private Sub FromLoc()
        cid = lblcid.Value
        Dim Item As ListItem
        Dim f, fi, ci, ei, li As String
        Dim tflag As Integer = 0
        assn.Open()
        For Each Item In lblocations.Items
            If Item.Selected Then
                fi = Item.Value.ToString
                Dim citem As ListItem
                For Each citem In lbchecked.Items
                    ci = citem.Value.ToString
                    If ci = fi Then
                        ei = citem.Value.ToString
                        li = citem.ToString
                        RemItems(ei, li)
                    End If
                Next
                If ci = "" Then
                    RemItems(fi)
                Else
                    Dim ritem As ListItem = New ListItem(li, ei)
                    If lbchecked.Items.Contains(ritem) Then
                        lbchecked.Items.Remove(ritem)
                    End If
                End If
            End If
        Next
        PopSuperAssigned()
        PopUnAssigned()
        PopAssigned()
        assn.Dispose()
    End Sub
    Private Sub RemItems(ByVal skillid As String, Optional ByVal tlid As String = "0")
        supid = lblsupid.Value
        sql = "delete from pmsupercrafts where superid = '" & supid & "' and skillid = '" & skillid & "'"

        assn.Update(sql)
    End Sub
    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToLoc()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromLoc()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2990.Text = axlabs.GetASPXPage("transassn.aspx", "lang2990")
        Catch ex As Exception
        End Try
        Try
            lang2991.Text = axlabs.GetASPXPage("transassn.aspx", "lang2991")
        Catch ex As Exception
        End Try
        Try
            lang2992.Text = axlabs.GetASPXPage("transassn.aspx", "lang2992")
        Catch ex As Exception
        End Try
        Try
            lang2993.Text = axlabs.GetASPXPage("transassn.aspx", "lang2993")
        Catch ex As Exception
        End Try
        Try
            lang2994.Text = axlabs.GetASPXPage("transassn.aspx", "lang2994")
        Catch ex As Exception
        End Try
        Try
            lang2995.Text = axlabs.GetASPXPage("transassn.aspx", "lang2995")
        Catch ex As Exception
        End Try
        Try
            lang2996.Text = axlabs.GetASPXPage("transassn.aspx", "lang2996")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
