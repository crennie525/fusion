<%@ Page Language="vb" AutoEventWireup="false" Codebehind="wrlabor.aspx.vb" Inherits="lucy_r12.wrlabor" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>wrlabor</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/wrlaboraspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 4px; LEFT: 4px" cellSpacing="1" cellPadding="1"
				width="300">
				<tr>
					<td class="label" width="100"><asp:Label id="lang3024" runat="server">Filter By Skill</asp:Label></td>
					<td colSpan="2" width="180"><asp:dropdownlist id="ddskill" runat="server" CssClass="plainlabel" AutoPostBack="True"></asp:dropdownlist></td>
					<td width="20"><IMG <IMG onmouseover="return overlib('Add Labor to Current Supervisor', LEFT)" onclick="addlab();" onmouseout="return nd()" id="imgadd" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif" runat="server"></td>
				</tr>
				<tr id="tr1" runat="server">
					<td colSpan="4">
						<div id="supdiv" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 290px; HEIGHT: 300px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblsupid" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
