<%@ Page Language="vb" AutoEventWireup="false" Codebehind="labsched.aspx.vb" Inherits="lucy_r12.labsched" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>labsched</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="javascript" type="text/javascript">
		function checksave() {
		document.getElementById("lblsubmit").value = "save";
		document.getElementById("form1").submit();
		}
		function checkexit() {
		var chk = document.getElementById("lblsubmit").value;
		if(chk=="exit") {
		window.parent.handleexit();
		}
		}
		</script>
	</HEAD>
	<body  onload="checkexit();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="bluelabel" height="22" width="120">Employee Name</td>
					<td id="tdemp" class="plainlabel" width="220" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22" width="120">Regular Shift</td>
					<td id="tdshift" class="plainlabel" width="220" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22" width="120">Regular Skill</td>
					<td id="tdskill" class="plainlabel" width="220" runat="server"></td>
				</tr>
				<tr>
					<td class="bluelabel">Regular Hours (Per Day)</td>
					<td class="plainlabel"><asp:textbox id="txtreg" runat="server" width="50"></asp:textbox></td>
				</tr>
				<tr>
					<td class="bluelabel" height="22">Regular Work Days:</td>
				</tr>
				<tr>
					<td colSpan="2" align="center">
						<table cellSpacing="1" cellPadding="1">
							<tr>
								<td class="label" width="23" align="center">M</td>
								<td class="label" width="23" align="center">Tu</td>
								<td class="label" width="23" align="center">W</td>
								<td class="label" width="23" align="center">Th</td>
								<td class="label" width="23" align="center">F</td>
								<td class="label" width="23" align="center">Sa</td>
								<td class="label" width="23" align="center">Su</td>
							</tr>
							<tr>
								<td align="center"><INPUT id="cb1mon" type="checkbox" name="Checkbox1" runat="server"></td>
								<td align="center"><INPUT id="cb1tue" type="checkbox" name="Checkbox2" runat="server"></td>
								<td align="center"><INPUT id="cb1wed" type="checkbox" name="Checkbox3" runat="server"></td>
								<td align="center"><INPUT id="cb1thu" type="checkbox" name="Checkbox4" runat="server"></td>
								<td align="center"><INPUT id="cb1fri" type="checkbox" name="Checkbox5" runat="server"></td>
								<td align="center"><INPUT id="cb1sat" type="checkbox" name="Checkbox6" runat="server"></td>
								<td align="center"><INPUT id="cb1sun" type="checkbox" name="Checkbox7" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2" align="right"><IMG src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="checksave();"></td>
				</tr>
				<tr>
					<td colspan="2" align="center" class="plainlabelred" id="tdmsg" runat="server" height="24"></td>
				</tr>
			</table>
			<input id="lblfslang" type="hidden" runat="server"> <input id="lbluserid" type="hidden" runat="server">
			<input type="hidden" id="lblsubmit" runat="server"><input id="lbld" type="hidden" runat="server">
			<input type="hidden" id="lblskillid" runat="server"> <input type="hidden" id="lblshift" runat="server">
			<input type="hidden" id="lblsid" runat="server">
		</form>
	</body>
</HTML>
