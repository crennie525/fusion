<%@ Page Language="vb" AutoEventWireup="false" Codebehind="wrskills.aspx.vb" Inherits="lucy_r12.wrskills" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>wrskills</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		<script language="JavaScript" src="../scripts1/wrskillsaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body bgColor="white" onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table width="422">
				<tr>
					<td class="thdrsing label" colSpan="3"><asp:Label id="lang3033" runat="server">Add/Edit Work Request Skills Mini Dialog</asp:Label></td>
				</tr>
			</table>
			<table width="422">
				<tr>
					<td class="label" align="center"><asp:Label id="lang3034" runat="server">Available Craft/Skills</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang3035" runat="server">Work Request Craft/Skills</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="200"><asp:listbox id="lbskillmaster" runat="server" Width="170px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22"><br>
						<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
					<td align="center" width="200"><asp:listbox id="lbsiteskills" runat="server" Width="170px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><IMG id="btnreturn" runat='server' onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
							width="69" height="19"></td>
				</tr>
				<tr>
					<td class="bluelabel" colSpan="3"><asp:Label id="lang3036" runat="server">List Box Options</asp:Label></td>
				</tr>
				<tr>
					<td class="label" colSpan="3"><asp:radiobuttonlist id="cbopts" runat="server" Width="400px" AutoPostBack="True" CssClass="labellt">
							<asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
							<asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="note" colSpan="3"><asp:Label id="lang3037" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Site Craft/Skills list if only one item is present.</asp:Label></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" runat="server" NAME="lblcid"> <input id="lblsid" type="hidden" runat="server" NAME="lblsid">
			<input id="lblfuid" type="hidden" runat="server" NAME="lblfuid"> <input id="lblcoid" type="hidden" runat="server" NAME="lblcoid">
			<input id="lblco" type="hidden" runat="server" NAME="lblco"><input id="lblapp" type="hidden" runat="server" NAME="lblapp">
			<input type="hidden" id="lblopt" runat="server" NAME="lblopt"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
