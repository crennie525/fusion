<%@ Page Language="vb" AutoEventWireup="false" Codebehind="planareaselect.aspx.vb" Inherits="lucy_r12.planareaselect"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>planareaselect</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="javascript" type="text/javascript">
		function getval(v1, v2, v3, v4, v5) {
		var ret;
		var typ = document.getElementById("lbltyp").value;
		if(typ=="wo") {
		document.getElementById("lblwaid").value=v1;
		document.getElementById("lblwa").value=v2;
		document.getElementById("lblret").value="savewo";
		document.getElementById("form1").submit();
		}
		else {
		ret = v1 + "~~" + v2 + "~~" + v3 + "~~" + v4 + "~~" + v5;
		window.parent.handlereturn(ret);
		}
		}
		function checkit() {
		var chk = document.getElementById("lblsubmit").value;
		if(chk=="return") {
		var v1 = document.getElementById("lblwaid").value;
		var v2 = document.getElementById("lblwa").value;
		ret = v1 + "~~" + v2;
		window.parent.handlereturn(ret);
		}
		}
		</script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table width="250">
				<tr>
					<td>
						<table>
							<tr>
								<td class="label"><asp:label id="lang113" runat="server">Search Planning Areas</asp:label></td>
								<td><asp:textbox id="txtsrch" runat="server" Width="170px"></asp:textbox></td>
								<td><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
										CssClass="imgbutton"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<DIV style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 750px; HEIGHT: 400px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid; runat: "
							id="divval" runat="server"></DIV>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<INPUT id="lblsid" type="hidden" runat="server" NAME="lblsid"> <input type="hidden" id="lblss" runat="server" NAME="lblss">
			<input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit"> <input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input type="hidden" id="lblret" runat="server" NAME="lblret"><input type="hidden" id="lblwonum" runat="server" NAME="lblwonum">
			<input type="hidden" id="lbltyp" runat="server" NAME="lbltyp"> <input type="hidden" id="lblwa" runat="server" NAME="lblwa">
			<input type="hidden" id="lblwaid" runat="server" NAME="lblwaid">
		</form>
	</body>
</HTML>
