<%@ Page Language="vb" AutoEventWireup="false" Codebehind="lablocs.aspx.vb" Inherits="lucy_r12.lablocs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>lablocs</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		<script language="javascript" type="text/javascript">
		function srchloc() {
		var eReturn = window.showModalDialog("../locs/LocDialog.aspx?typ=eq", "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
			if (eReturn) {
			var ret = eReturn.split(",");
			document.getElementById("lbllid").value = ret[5];
			document.getElementById("lbltyp").value = ret[1];
			document.getElementById("lblloc").innerHTML = ret[2];
			var lid = ret[5];
			document.getElementById("lblpchk").value = "loc"
			//document.getElementById("form1").submit();
				
			}
		}
		function undoloc() {
		
		}
		function handleexit() {
		var dlst = document.getElementById("dddepts");
		var dept = dlst.options[dlst.selectedIndex].text
		
		var clst = document.getElementById("ddcells");
		var cell = clst.options[clst.selectedIndex].text
		
		var loc = document.getElementById("lblloc").innerHTML;
		var chk = document.getElementById("lblchk").value;
		var ret;
		if(dept!="Select Department") {
		ret = dept;
			if(cell!="Select Station/Cell") {
			 ret = ret + " ~ " + cell;
			}
		}
		if(loc!="") {
			if(ret=="") {
				ret = loc;
			}
			else {
				ret = ret + " ~ " + loc;
			}
		}
		window.parent.handleexit(ret);
		}
		
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="label"><asp:label id="lang241" runat="server">Department</asp:label></td>
					<td><asp:dropdownlist id="dddepts" runat="server" Width="170px" AutoPostBack="True" CssClass="plainlabel"></asp:dropdownlist></td>
					<td></td>
				</tr>
				<tr id="celldiv" class="details" runat="server">
					<td class="label"><asp:label id="lang242" runat="server">Station/Cell</asp:label></td>
					<td><asp:dropdownlist id="ddcells" runat="server" Width="170px" AutoPostBack="True" CssClass="plainlabel"></asp:dropdownlist></td>
					<td></td>
				</tr>
				<tr>
					<td class="label"><asp:label id="lang245" runat="server">Location</asp:label></td>
					<td><asp:label id="lblloc" runat="server" Width="170px" CssClass="plainlabel" Height="20px" BorderColor="#8080FF"
							BorderWidth="1px" BorderStyle="Solid"></asp:label></td>
					<td><IMG onmouseover="return overlib('Use Locations', LEFT)" onmouseout="return nd()" onclick="srchloc();"
							src="../images/appbuttons/minibuttons/useloc.gif"></td>
				</tr>
				<tr>
					<td colSpan="3" align="right"><IMG id="imgsw" onmouseover="return overlib('Clear Screen and Start Over', LEFT)" onmouseout="return nd()"
							onclick="undoloc();" src="../images/appbuttons/minibuttons/switch.gif" runat="server">
						<IMG id="Img1" onmouseover="return overlib('Save Changes and Return', LEFT)" onmouseout="return nd()"
							onclick="handleexit();" src="../images/appbuttons/minibuttons/savedisk1.gif" runat="server"></td>
				</tr>
			</table>
		</form>
		<input id="lblsid" type="hidden" runat="server"> <input id="lbllid" type="hidden" runat="server">
		<input id="lblpchk" type="hidden" runat="server"> <input id="lbltyp" type="hidden" runat="server">
		<input id="lblcid" type="hidden" runat="server"> <input type="hidden" id="lbldept" runat="server">
		<input type="hidden" id="lblpar" runat="server"> <input type="hidden" id="lblchk" runat="server">
		<input type="hidden" id="lblpar2" runat="server"> <input type="hidden" id="lblclid" runat="server">
	</body>
</HTML>
