<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="labskills.aspx.vb" Inherits="lucy_r12.labskills" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>labskills</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="javascript" type="text/javascript">
        function checkrb(who) {
            document.getElementById("lblsubmit").value = who;
            document.getElementById("form1").submit();
        }
        function getval(v1, v2, v3, v4, v5) {
            var who = document.getElementById("lblwho").value;
            if (who == "wo") {
                document.getElementById("lblskillid").value = v1;
                document.getElementById("lblskill").value = v2;
                document.getElementById("lblsubmit").value = "savewo";
                document.getElementById("form1").submit();
            }
            else if (who == "jp") {
                document.getElementById("lblskillid").value = v1;
                document.getElementById("lblskill").value = v2;
                document.getElementById("lblsubmit").value = "savejp";
                document.getElementById("form1").submit();
            }
            else if (who == "pmmax") {
                document.getElementById("lblskillid").value = v1;
                document.getElementById("lblskill").value = v2;
                document.getElementById("lblsubmit").value = "savepmmax";
                document.getElementById("form1").submit();
            }
            else {
                var ret;
                if (document.getElementById("cbur").checked == true) {
                    //alert(v4 + ", " + v5)
                    v3 = "1"
                    ret = v1 + "~" + v2 + "~" + v3 + "~" + v4 + "~" + v5;
                }
                else {
                    ret = v1 + "~" + v2 + "~" + v3;
                }
                window.parent.handlereturn(ret);
            }
        }
        function checkit() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "return") {
                var v1 = document.getElementById("lblskillid").value;
                var v2 = document.getElementById("lblskill").value;
                var ret;
                ret = v1 + "~" + v2;
                window.parent.handlereturn(ret);
            }
        }
    </script>
</head>
<body  onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table width="250">
        <tr>
            <td class="label" align="right">
                <input id="rball" onclick="checkrb('all');" type="radio" name="rbsel" runat="server">All
                Skills&nbsp;&nbsp;<input id="rbsite" onclick="checkrb('site');" type="radio" name="rbsel"
                    runat="server">Site Skills
            </td>
        </tr>
        <tr>
            <td class="label" align="right">
                <input id="cbur" type="checkbox" runat="server">Return Rates?
            </td>
        </tr>
        <tr>
            <td>
                <div style="border-bottom: black 1px solid; border-left: black 1px solid; width: 250px;
                    height: 400px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid;
                    runat: " id="divval" runat="server">
                </div>
            </td>
        </tr>
    </table>
    <input id="lblsid" type="hidden" runat="server">
    <input type="hidden" id="lblss" runat="server">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lblwho" runat="server">
    <input type="hidden" id="lblwonum" runat="server">
    <input type="hidden" id="lblskillid" runat="server">
    <input type="hidden" id="lblskill" runat="server"><input type="hidden" id="lbljpid"
        runat="server" />
        <input type="hidden" id="lblpmid" runat="server" />
    </form>
</body>
</html>
