

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class wrlabor
    Inherits System.Web.UI.Page
	Protected WithEvents lang3024 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim sup As New Utilities
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim supid, cid, skill, name As String
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents supdiv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            supid = Request.QueryString("supid").ToString
            lblsupid.Value = supid
            sup.Open()
            LoadSkills()
            LoadLabor()
            sup.Dispose()
        Else
            If Request.Form("lblsubmit") = "ret" Then
                lblsubmit.Value = ""
                sup.Open()
                LoadLabor()
                sup.Dispose()
            End If
        End If
    End Sub
    Private Sub LoadLabor()
        supid = lblsupid.Value
        If supid <> "" Then
            If ddskill.SelectedIndex = 0 Or ddskill.SelectedIndex = -1 Then
                sql = "select * from pmsysusers where superid = '" & supid & "'"
            Else
                skill = ddskill.SelectedValue.ToString
                If skill = "0" Or ddskill.SelectedIndex = -1 Then
                    sql = "select * from pmsysusers where superid = '" & supid & "'"
                Else
                    sql = "select * from pmsysusers where skillid = '" & skill & "' and superid = '" & supid & "'"
                End If

            End If
            Dim labid, super, email, skillid, shift As String
            Dim sb As New System.Text.StringBuilder
            sb.Append("<table>")
            dr = sup.GetRdrData(sql)
            While dr.Read
                labid = dr.Item("userid").ToString
                name = dr.Item("username").ToString
                skill = dr.Item("skill").ToString
                skillid = dr.Item("skillid").ToString
                supid = dr.Item("superid").ToString
                super = dr.Item("super").ToString
                shift = dr.Item("shift").ToString
                If shift = "0" Then
                    shift = "Any Shift"
                ElseIf shift = "1" Then
                    shift = "1st Shift"
                ElseIf shift = "2" Then
                    shift = "2nd Shift"
                ElseIf shift = "3" Then
                    shift = "3rd Shift"
                Else
                    shift = "Any Shift"
                End If
                sb.Append("<tr><td class=""plainlabelblue"">" & name & "</td>")
                sb.Append("<td class=""plainlabel"">" & skill & "</td>")
                sb.Append("<td class=""plainlabel"">" & shift & "</td></tr>")
            End While
            dr.Close()
            sb.Append("</table>")
            supdiv.InnerHtml = sb.ToString
        End If
        
    End Sub
    Private Sub LoadSkills()
        cid = "0"
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = sup.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("All Skills"))
        ddskill.Items(0).Value = 0

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3024.Text = axlabs.GetASPXPage("wrlabor.aspx", "lang3024")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("wrlabor.aspx", "imgadd") & "')")
            imgadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
