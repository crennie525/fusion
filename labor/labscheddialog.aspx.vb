Public Class labscheddialog
    Inherits System.Web.UI.Page
    Dim userid, muser, shift, skill, skillid, sid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iffreq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            muser = Request.QueryString("muser").ToString
            userid = Request.QueryString("userid").ToString
            shift = Request.QueryString("shift").ToString
            skill = Request.QueryString("skill").ToString
            skillid = Request.QueryString("skillid").ToString
            sid = Request.QueryString("sid").ToString
            iffreq.Attributes.Add("src", "labsched.aspx?userid=" & userid & "&muser=" & muser & "&shift=" & shift & "&skill=" & skill & "&skillid=" & skillid & "&sid=" & sid & "&date=" & Now)

        End If
    End Sub

End Class
