﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="super_assign.aspx.vb"
    Inherits="lucy_r12.super_assign" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <style type="text/css">
        .details
        {
            background-color: white;
            display: none;
            font-family: Verdana;
            visibility: hidden;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function getsup2(supid2, sup2) {
            document.getElementById("lblsupid2").value = supid2;
            document.getElementById("lblsup2").value = sup2;
            document.getElementById("tdsup2").innerHTML = sup2;
        }
        function getsup3(supid3, sup3) {
            document.getElementById("lblsupid3").value = supid3;
            document.getElementById("lblsup3").value = sup3;
            document.getElementById("tdsup3").innerHTML = sup3;
        }
        function checkrep() {
            var supid2 = document.getElementById("lblsupid2").value;
            var supid3 = document.getElementById("lblsupid3").value;
            if ((supid2 != "" && supid3 != "") && (supid2 != supid3)) {
                document.getElementById("lblret").value = "checksup";
                document.getElementById("form1").submit();
            }
            else if (supid2 == "") {
                alert("No Current Supervisor Selected")
            }
            else if (supid3 == "") {
                alert("No Replacement Supervisor Selected")
            }
            else if (supid2 == "" && supid3 == "") {
                alert("No Current or Replacement Supervisor Selected")
            }
        }
        

        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function nogray() {
            document.getElementById("f1").className = "label";
            document.getElementById("f2").className = "label";
            document.getElementById("f3").className = "label";
            document.getElementById("f4").className = "label";
            document.getElementById("Radio1").checked = false;
            document.getElementById("Radio2").checked = false;
            document.getElementById("Radio3").checked = false;
            document.getElementById("Radio4").checked = false;
        }
        function checkit() {
            //document.getElementById("f1").className = "redlabel";
            var divflag = document.getElementById("txttab").value;
            if (divflag == "eq") {
                nogray()
                document.getElementById("eq").className = "thdrhov plainlabel";
            }
            else if (divflag == "jp") {
                nogray()
                document.getElementById("jp").className = "thdrhov plainlabel";
                document.getElementById("Radio2").disabled = true;
                document.getElementById("f2").className = "graylabel";
                document.getElementById("Radio3").disabled = true;
                document.getElementById("f3").className = "graylabel";
                document.getElementById("Radio1").checked = true;
            }
            else if (divflag == "pm") {
                nogray()
                document.getElementById("pm").className = "thdrhov plainlabel";
                document.getElementById("Radio1").disabled = true;
                document.getElementById("f1").className = "graylabel";
                document.getElementById("Radio3").disabled = true;
                document.getElementById("f3").className = "graylabel";
                document.getElementById("Radio2").checked = true;
            }
            else if (divflag == "tm") {
                nogray()
                document.getElementById("tm").className = "thdrhov plainlabel";
                document.getElementById("Radio1").disabled = true;
                document.getElementById("f1").className = "graylabel";
                document.getElementById("Radio2").disabled = true;
                document.getElementById("f2").className = "graylabel";
                document.getElementById("Radio3").checked = true;
            }
            else if (divflag == "wo") {
                document.getElementById("wo").className = "thdrhov plainlabel";
                document.getElementById("Radio1").disabled = true;
                document.getElementById("f1").className = "graylabel";
                document.getElementById("Radio2").disabled = true;
                document.getElementById("f2").className = "graylabel";
                document.getElementById("Radio3").disabled = true;
                document.getElementById("f3").className = "graylabel";
                document.getElementById("Radio4").checked = true;
            }
            var srch = document.getElementById("txtsrch").value;
            if (srch == "") {
                document.getElementById("rbsql").disabled = true;
                document.getElementById("rbsql").checked = false;
                document.getElementById("f5").className = "graylabel";
            }
        }
        function eqtab() {
            document.getElementById("txttab").value = "eq";
            document.getElementById("eq").className = "thdrhov plainlabel";
            document.getElementById("lblret").value = "eq";
            document.getElementById("form1").submit();
        }
        function jptab() {
            nogray()
            document.getElementById("txttab").value = "jp";
            document.getElementById("jp").className = "thdrhov plainlabel";
            document.getElementById("Radio2").disabled = true;
            document.getElementById("f2").className = "graylabel";
            document.getElementById("Radio3").disabled = true;
            document.getElementById("f3").className = "graylabel";
            document.getElementById("Radio1").checked = true;
            document.getElementById("lblret").value = "jp";
            document.getElementById("form1").submit();
        }
        function pmtab() {
            nogray()
            document.getElementById("txttab").value = "pm";
            document.getElementById("pm").className = "thdrhov plainlabel";
            document.getElementById("Radio1").disabled = true;
            document.getElementById("f1").className = "graylabel";
            document.getElementById("Radio3").disabled = true;
            document.getElementById("f3").className = "graylabel";
            document.getElementById("Radio2").checked = true;
            document.getElementById("lblret").value = "pm";
            document.getElementById("form1").submit();
        }
        function tmtab() {
            nogray()
            document.getElementById("txttab").value = "tm";
            document.getElementById("tm").className = "thdrhov plainlabel";
            document.getElementById("Radio1").disabled = true;
            document.getElementById("f1").className = "graylabel";
            document.getElementById("Radio2").disabled = true;
            document.getElementById("f2").className = "graylabel";
            document.getElementById("Radio3").checked = true;
        }
        function wotab() {
            nogray()
            document.getElementById("txttab").value = "wo";
            document.getElementById("wo").className = "thdrhov plainlabel";
            document.getElementById("Radio1").disabled = true;
            document.getElementById("f1").className = "graylabel";
            document.getElementById("Radio2").disabled = true;
            document.getElementById("f2").className = "graylabel";
            document.getElementById("Radio3").disabled = true;
            document.getElementById("f3").className = "graylabel";
            document.getElementById("Radio4").checked = true;
        }
        function versrch() {
            var srch = document.getElementById("txtsrch").value;
            if (srch.length > 0) {
                //document.getElementById("f5").checked = true;
                document.getElementById("f5").className = "label";
                document.getElementById("rbsql").disabled = false;
            }
            else {

                document.getElementById("f5").className = "graylabel";
                document.getElementById("rbsql").checked = false;
                document.getElementById("rbsql").disabled = true;
            }
        }
        function gettab(id) {
            document.getElementById("txtsrch").value = "";
            document.getElementById("rbsql").checked = false;
            document.getElementById("f5").className = "graylabel";

            document.getElementById("eq").className = "thdr plainlabel";
            document.getElementById("jp").className = "thdr plainlabel";
            document.getElementById("pm").className = "thdr plainlabel";
            document.getElementById("tm").className = "thdr plainlabel";
            document.getElementById("wo").className = "thdr plainlabel";

            if (id == "eq") {
                eqtab();
            }
            else if (id == "jp") {
                jptab();
            }
            else if (id == "pm") {
                pmtab();
            }
            else if (id == "tm") {
                tmtab();
            }
            else if (id == "wo") {
                wotab();
            }
            
        }
        function checktab() {
            var divflg = document.getElementById("txttab").value;
            gettab(divflg);
        }
        function chksrch() {
            var ret = document.getElementById("txttab").value;
            document.getElementById("lblret").value = ret;
            document.getElementById("form1").submit();
        }
    </script>
</head>
<body onload="checkit();">
    <form id="form1" runat="server">
    <table style="position: absolute; top: 4px; left: 4px" cellspacing="1" cellpadding="1"
        width="950">
        <tr>
            <td class="label">
                Supervisor
            </td>
            <td class="label">
                Assignments
            </td>
        </tr>
        <tr id="tr2" runat="server" valign="top">
            <td width="300">
                <table>
                    <tr>
                        <td>
                            <div id="supdiv2" style="border-bottom: black 1px solid; border-left: black 1px solid;
                                width: 280px; height: 364px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                                runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Selected Supervisor:
                        </td>
                    </tr>
                    <tr>
                        <td class="plainlabel" id="tdsup2" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="600" valign="top">
                <table>
                    <tr>
                        <td id="eq" onclick="gettab('eq');" class="thdrhov plainlabel" width="100" height="20">
                            <asp:Label ID="lang80" runat="server">Equipment</asp:Label>
                        </td>
                        <td id="jp" onclick="gettab('jp');" class="thdr plainlabel" width="100" height="20">
                            <asp:Label ID="Label1" runat="server">PM Job Plans</asp:Label>
                        </td>
                        <td id="pm" onclick="gettab('pm');" class="thdr plainlabel" width="100" height="20">
                            <asp:Label ID="Label2" runat="server">PM Records</asp:Label>
                        </td>
                        <td id="tm" onclick="gettab('tm');" class="thdr plainlabel" width="100" height="20">
                            <asp:Label ID="Label3" runat="server">TPM Records</asp:Label>
                        </td>
                        <td id="wo" onclick="gettab('wo');" class="thdr plainlabel" width="100" height="20">
                            <asp:Label ID="Label4" runat="server">Work Orders</asp:Label>
                        </td>
                        <td width="100">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <table>
                                <tr>
                                    <td class="label" width="80">
                                        Search
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtsrch" runat="server" CssClass="plainlabel" Width="130px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <img onclick="chksrch();" src="../images/appbuttons/minibuttons/srchsm1.gif" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:ListBox ID="supdiv3" runat="server" Height="194px" SelectionMode="Multiple"
                                Width="600px"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="border-right: blue 1px solid" width="20">
                                        <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst2.gif"
                                            runat="server">
                                    </td>
                                    <td style="border-right: blue 1px solid" width="20">
                                        <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev2.gif"
                                            runat="server">
                                    </td>
                                    <td style="border-right: blue 1px solid" valign="middle" align="center" width="140">
                                        <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                                    </td>
                                    <td style="border-right: blue 1px solid" width="20">
                                        <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext2.gif"
                                            runat="server">
                                    </td>
                                    <td width="20">
                                        <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast2.gif"
                                            runat="server">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="label">
                            <u>Selection Options</u>
                        </td>
                        <td colspan="3" class="label">
                            <u>Assignment Options (per Selected)</u>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="label" valign="top">
                            <input type="radio" id="rbus" name="rbopt1" />Use Selected<br />
                            <input type="radio" id="rball" name="rbopt1" />Use All on Current Page<br />
                            <input type="radio" id="rbsql" name="rbopt1" /><font class="label" id="f5">Use Search
                                Keyword</font><br />
                        </td>
                        <td colspan="3" class="label">
                            <input type="radio" id="Radio1" /><font class="label" id="f1">All PM Job Plan Records</font><br />
                            <input type="radio" id="Radio2" /><font class="label" id="f2">All PM Records</font><br />
                            <input type="radio" id="Radio3" /><font class="label" id="f3">All TPM Records</font><br />
                            <input type="radio" id="Radio4" /><font class="label" id="f4">All Open Work Orders</font><br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="right">
                            <input type="button" value="Submit" onclick="checkrep();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblsup2" runat="server" />
    <input type="hidden" id="lblsup3" runat="server" />
    <input type="hidden" id="lblsupid2" runat="server" />
    <input type="hidden" id="lblsupid3" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblcheckit" runat="server" />
    <input type="hidden" id="txtpg" runat="server" />
    <input type="hidden" id="txtpgcnt" runat="server" />
    <input type="hidden" id="txttab" runat="server" />
    </form>
</body>
</html>
