<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PMLeadMan.aspx.vb" Inherits="lucy_r12.PMLeadMan" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PMLeadMan</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script language="JavaScript" src="../scripts1/PMLeadManaspx.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
		<script language="javascript" type="text/javascript">
		function getskill(skid1, skid2, r1, r2) {
		var sid = document.getElementById("lblsid").value;
		var eReturn = window.showModalDialog("labskillsdialog.aspx?sid=" + sid, "", "dialogHeight:500px; dialogWidth:360px; resizable=yes");
		if (eReturn) {
		if(eReturn!="") {
			var ret = eReturn.split("~")
			
			document.getElementById("lblsret").value = "yes";
			document.getElementById(skid1).innerHTML = ret[1];
			document.getElementById("lblskid1").value = ret[0];
			document.getElementById("lblskid2").value = ret[1];
			var userate = ret[2];
			if(userate=="1") {
				document.getElementById("lbluret").value = "yes";
				document.getElementById("lblr1").value = ret[3];
				document.getElementById("lblr2").value = ret[4];
				document.getElementById(r1).value = ret[3];
				document.getElementById(r2).value = ret[4];
			}
			else {
				document.getElementById("lbluret").value = "no";
			}
			
		}	
		}
		}
		
		function getwa(skid1, skid2) {
		var sid = document.getElementById("lblsid").value;
		var eReturn = window.showModalDialog("workareaselectdialog.aspx?typ=lab&sid=" + sid, "", "dialogHeight:500px; dialogWidth:820px; resizable=yes");
		if (eReturn) {
		if(eReturn!="") {
			var ret = eReturn.split("~~")

			document.getElementById(skid1).innerHTML = ret[1];
			document.getElementById("lblwaret").value = ret[1];
			document.getElementById("lblwaidret").value = ret[0];
			
			document.getElementById("lblwret").value = "yes";	
		}
		}
		}
		function getmskill(skid, skil, userid, muser) {
		var eReturn = window.showModalDialog("labmskillsdialog.aspx?userid=" + userid + "&muser=" + muser + "&skid=" + skid + "&skil=" + skil, "", "dialogHeight:500px; dialogWidth:550px; resizable=yes");
		if (eReturn) {
		}
		}
		function getmwa(skid, skil, userid, muser) {
		var eReturn = window.showModalDialog("labmareasdialog.aspx?userid=" + userid + "&muser=" + muser + "&skid=" + skid + "&skil=" + skil, "", "dialogHeight:540px; dialogWidth:590px; resizable=yes");
		if (eReturn) {
		}
		}
		function getsched(userid, muser, shift, skid, skil, sid) {
		var eReturn = window.showModalDialog("labscheddialog.aspx?userid=" + userid + "&muser=" + muser + "&shift=" + shift + "&skill=" + skil + "&skillid=" + skid + "&sid=" + sid, "", "dialogHeight:400px; dialogWidth:400px; resizable=yes");
		if (eReturn) {
		}
		}
		function getbig() {
		//window.open("PMLeadMan.aspx?x=yes" + " Target='_top'");
		
		var chk = document.getElementById("lblx").value;
		if(chk=="no") {
		window.parent.getbig();
		}
		else {
		document.getElementById("lblret").value="big";
		document.getElementById("form1").submit();
		}
		}
		function getshift(shift) {
		var eReturn = window.showModalDialog("labshifts.aspx", "", "dialogHeight:140px; dialogWidth:140px; resizable=yes");
		if (eReturn) {
		if(eReturn!="") {
			var ret = eReturn

			document.getElementById(shift).innerHTML = ret;
			document.getElementById("lblshiftret").value = ret;
			
			document.getElementById("lblshret").value = "yes";	
		}
		}
		}
		function getsuper(skid1, skid2) {
		var skill = ""; //document.getElementById("lblskillid").value;
		var sid = document.getElementById("lblsid").value;
		var ro = "0";
		typ = "sup"
		var eReturn = window.showModalDialog("../labor/SuperSelectDialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro + "&sid=" + sid, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
				if (eReturn) {
					if(eReturn!="") {
						var retarr = eReturn.split(",")
						document.getElementById(skid1).innerHTML = retarr[1];
						document.getElementById("lblsuperret").value = retarr[1];
						document.getElementById("lblsuperidret").value = retarr[0];
			
						document.getElementById("lblsuret").value = "yes";	
					}
				}
		}
		</script>
	</HEAD>
	<body onload="scrolltop();checkchng();" >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" style="Z-INDEX: 103; POSITION: absolute; TOP: 0px; LEFT: 5px" bgColor="#dee0f8">
				<tr>
					<td>
						<table>
							<tr>
								<td class="label" width="100"><asp:label id="lang2979" runat="server">Search Labor</asp:label></td>
								<td width="120"><asp:textbox id="txtsrch" runat="server"></asp:textbox></td>
								<td width="30"><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
								<td class="label" width="60"><asp:label id="lang2980" runat="server">Filter By:</asp:label></td>
								<td><asp:dropdownlist id="ddfilt" runat="server" AutoPostBack="True" Width="160"></asp:dropdownlist></td>
								<td width="200" align="right"><img src="../images/appbuttons/minibuttons/grid1.gif" onclick="getbig();" id="imggrid"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td id="tdnavtop" runat="server" class="details">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" bgcolor="#ffffff">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img1" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img2" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpgtop" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img3" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="Img4" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table style="POSITION: absolute; TOP: 40px; LEFT: 5px" cellSpacing="0" cellPadding="0"
				width="1335" id="tblgrid" runat="server">
				<tr>
					<td colSpan="3">
					</td>
				</tr>
				<tr>
					<td></td>
					<td colSpan="2"><asp:label id="lblps" runat="server" ForeColor="Red" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
							Width="376px"></asp:label></td>
				</tr>
				<tr>
					<td width="5"></td>
					<td id="tdps" class="bluelabel" width="1320" runat="server"></td>
				</tr>
				<tr>
					<td width="20">&nbsp;</td>
					<TD colSpan="2"><asp:datagrid id="dgdepts" runat="server" Width="1400" AutoGenerateColumns="False" AllowCustomPaging="True"
							AllowPaging="True" GridLines="None" cellspacing="2" CellPadding="0" ShowFooter="False">
							<FooterStyle BackColor="White"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight"></ItemStyle>
							<HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<FooterTemplate>
										<asp:ImageButton id="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
											CommandName="Add"></asp:ImageButton>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewname runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id=txtname runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.username") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="empno desc" HeaderText="Employee#" Visible="True">
									<HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lblempno" runat="server" width="70px" Text='<%# DataBinder.Eval(Container, "DataItem.empno") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtempno" runat="server" Width="70px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.empno") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="site desc" HeaderText="Plant Site" Visible="False">
									<HeaderStyle Width="130px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label12" runat="server" width="130px" Text='<%# DataBinder.Eval(Container, "DataItem.ps") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="Label13" width="130px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ps") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="workarea desc" HeaderText="Work Area">
									<HeaderStyle Width="130px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label7" runat="server" width="130px" Text='<%# DataBinder.Eval(Container, "DataItem.workarea") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblwa" width="130px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.workarea") %>'>
										</asp:Label><img id="imgwa" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
										<img id="imgmwa" runat="server" src="../images/appbuttons/minibuttons/addnew.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="shift desc" HeaderText="Shift">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label9" runat="server" width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.shiftstr") %>'>
										</asp:Label><img id="imgisched" runat="server" src="../images/appbuttons/minibuttons/sched.gif">
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblshift" width="100px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.shiftstr") %>'>
										</asp:Label><img id="imgshift" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
										<img id="imgesched" runat="server" src="../images/appbuttons/minibuttons/sched.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="super desc" HeaderText="Supervisor">
									<HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label10" runat="server" width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.super") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
                                    
										<asp:Label id="lblsuper" width="150px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.super") %>'>
										</asp:Label><img id="imgsuper" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="skill desc" HeaderText="Skill Required">
									<HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="lblskilli" runat="server" width="160px" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblskill" width="160px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
										</asp:Label><img id="imgskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
										<img id="imgmskill" runat="server" src="../images/appbuttons/minibuttons/addnew.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Labor Rate">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label3" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.laborrate") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="Textbox1" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.laborrate") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="txtlr" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.laborrate") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="OT Rate">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label6" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.otrate") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="Textbox2" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.otrate") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="txtor" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.otrate") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Phone">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.phonenum") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="txtnewphone" runat="server" Width="120px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.phonenum") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="txtphone" runat="server" Width="120px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.phonenum") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Email Address">
									<HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<a  href='mailto:<%# DataBinder.Eval(Container, "DataItem.email") %>'>
											<%# DataBinder.Eval(Container, "DataItem.email") %>
										</a>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewemail runat="server" Width="220px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id=txtemail runat="server" Width="220px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label1 runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.userid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id=txtnewllabel runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.userid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="lblsupid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.userid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="Textbox3" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="lblskid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label8" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.waid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="Textbox4" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.waid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="lblwaid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.waid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="220px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label11" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.superid") %>'>
										</asp:Label>
									</ItemTemplate>
									<FooterTemplate>
										<asp:TextBox id="Textbox5" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.superid") %>'>
										</asp:TextBox>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:Label id="lblsuperid" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.superid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></TD>
				</tr>
				<tr>
					<td colSpan="2">
						<table width="1020" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="plainlabelred" vAlign="middle" align="center" height="22"><asp:label id="lang2981" runat="server">Labor Records are controlled in the User Administration Module.</asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblchng" type="hidden" name="lblchng" runat="server"> <input id="lblcid" type="hidden" runat="server">
			<input id="lblsid" type="hidden" runat="server"> <input id="lblfilt" type="hidden" runat="server">
			<input id="lblfiltd" type="hidden" runat="server"><input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"><input id="lblold" type="hidden" name="lblold" runat="server">
			<input id="lblfslang" type="hidden" runat="server"> <input id="lblskid1" type="hidden" runat="server">
			<input id="lblskid2" type="hidden" runat="server"> <input id="lblsret" type="hidden" runat="server">
			<input id="lbluret" type="hidden" runat="server"> <input id="lblr1" type="hidden" runat="server">
			<input id="lblr2" type="hidden" runat="server"> <input id="lblwret" type="hidden" runat="server">
			<input id="lblwaret" type="hidden" runat="server"> <input id="lblwaidret" type="hidden" runat="server">
			<input id="lblshret" type="hidden" runat="server"> <input id="lblshiftret" type="hidden" runat="server">
			<input type="hidden" id="lblsuret" runat="server"> <input type="hidden" id="lblsuperret" runat="server">
			<input type="hidden" id="lblsuperidret" runat="server"> <input type="hidden" id="lblx" runat="server">
			<input type="hidden" id="xCoord" runat="server"> <input type="hidden" id="yCoord" runat="server">
			<input type="hidden" id="lblddfilt" runat="server">
		</form>
	</body>
</HTML>
