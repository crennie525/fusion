<%@ Page Language="vb" AutoEventWireup="false" Codebehind="labshedmain.aspx.vb" Inherits="lucy_r12.labshedmain" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>labshedmain</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="javascript" type="text/javascript">
		function getsuper(typ) {
		var skill = ""; //document.getElementById("lblskillid").value;
		var ro = "0";
		var eReturn = window.showModalDialog("../labor/laborwithprofilesdialog.aspx?typ=" + typ + "&skill=" + skill + "&ro=" + ro, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
				if (eReturn) {
					if(eReturn!="") {
						var retarr = eReturn.split(",")
							if (typ=="sup") {
								//document.getElementById("lblsup").value = retarr[0];
								//document.getElementById("txtsup").value = retarr[1];
							}
							else {
								//alert(retarr[0])
								document.getElementById("lbluserid").value = retarr[0];
								document.getElementById("tdname").innerHTML = retarr[1];
								document.getElementById("lblsubmit").value = "newlab";
								document.getElementById("form1").submit();
							}
					}
				}
		}
		function getstat(who, who2, id1, id2) {
		var eReturn = window.showModalDialog("labstats.aspx", "", "dialogHeight:210px; dialogWidth:210px; resizable=yes");
				if (eReturn) {
					if(eReturn!="") {
					 var val = eReturn;
					 var prevwho = document.getElementById(id1).innerHTML;
					 var prevhrs = document.getElementById(id2).value;
					 document.getElementById(who).value = "yes";
					 document.getElementById(who2).value = val;
					 document.getElementById(id1).innerHTML = val;
					 if(val=="O"||val=="V"||val=="T"||val=="S") {
						document.getElementById(id2).value = "0";
					 }
					 else if(val=="P"&&prevwho!="P") {
						alert("Please Adjust Hours")
					 }
					 else if(val=="R"&&prevwho!="R") {
						//alert("Please Adjust Hours")
						var reghrs = document.getElementById("tdreghrs").innerHTML;
						document.getElementById(id2).value = reghrs;
					 }
					}
				}
		}
		function getskill(who, skid1, skid2, r1) {
		var sid = document.getElementById("lblsid").value;
		var eReturn = window.showModalDialog("labskillsdialog.aspx?who=lab&sid=" + sid, "", "dialogHeight:500px; dialogWidth:360px; resizable=yes");
		if (eReturn) {
		if(eReturn!="") {
			var ret = eReturn.split("~")
			
			document.getElementById(who).value = "yes";
			document.getElementById(r1).innerHTML = ret[1];
			document.getElementById(skid1).value = ret[1];
			document.getElementById(skid2).value = ret[0];
				
		}	
		}
		}
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td width="100"></td>
					<td width="150"></td>
					<td width="120"></td>
					<td width="120"></td>
					<td width="160"></td>
					<td width="200"></td>
					<td width="775"></td>
				</tr>
				<tr>
					<td class="thdrsing label" colSpan="8"><asp:label id="lang2969" runat="server">Employee Details</asp:label></td>
				</tr>
				<tr height="22">
					<td class="bluelabel"><asp:label id="lang2970" runat="server">Name</asp:label></td>
					<td id="tdname" class="plainlabel" runat="server"></td>
					<td><IMG onclick="getsuper('lead');" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
				</tr>
				<tr height="22">
					<td class="bluelabel"><asp:label id="lang2971" runat="server">Phone</asp:label></td>
					<td style="WIDTH: 74px" id="tdphone" class="plainlabel" runat="server"></td>
					<td class="bluelabel"><asp:label id="lang2972" runat="server">Email Address</asp:label></td>
					<td id="tdemail" class="plainlabel" colSpan="2" runat="server"></td>
				</tr>
				<tr height="22">
					<td class="bluelabel"><asp:label id="lang2973" runat="server">Supervisor</asp:label></td>
					<td style="WIDTH: 74px" id="tdsup" class="plainlabel" runat="server"></td>
					<td class="bluelabel"><asp:label id="lang2974" runat="server">Regular Skill</asp:label></td>
					<td id="tdskill" class="plainlabel" colSpan="2" runat="server"></td>
				</tr>
				<tr height="22">
					<td class="bluelabel"><asp:label id="lang2975" runat="server">Regular Hours</asp:label></td>
					<td id="tdreghrs" class="plainlabel" runat="server"></td>
					<td class="bluelabel"><asp:label id="lang2976" runat="server">Regular Shift</asp:label></td>
					<td id="tdshift" class="plainlabel" runat="server"></td>
					<td class="bluelabel"><asp:label id="lang2977" runat="server">Regular Work Days</asp:label></td>
					<td rowSpan="2">
						<table cellSpacing="1" cellPadding="1">
							<tr height="18">
								<td class="label" width="23" align="center">M</td>
								<td class="label" width="23" align="center">Tu</td>
								<td class="label" width="23" align="center">W</td>
								<td class="label" width="23" align="center">Th</td>
								<td class="label" width="23" align="center">F</td>
								<td class="label" width="23" align="center">Sa</td>
								<td class="label" width="23" align="center">Su</td>
							</tr>
							<tr height="18">
								<td align="center"><INPUT id="cb1mon" disabled type="checkbox" name="Checkbox1" runat="server"></td>
								<td align="center"><INPUT id="cb1tue" disabled type="checkbox" name="Checkbox2" runat="server"></td>
								<td align="center"><INPUT id="cb1wed" disabled type="checkbox" name="Checkbox3" runat="server"></td>
								<td align="center"><INPUT id="cb1thu" disabled type="checkbox" name="Checkbox4" runat="server"></td>
								<td align="center"><INPUT id="cb1fri" disabled type="checkbox" name="Checkbox5" runat="server"></td>
								<td align="center"><INPUT id="cb1sat" disabled type="checkbox" name="Checkbox6" runat="server"></td>
								<td align="center"><INPUT id="cb1sun" disabled type="checkbox" name="Checkbox7" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td class="thdrsing label" colSpan="7"><asp:label id="lang2978" runat="server">Scheduling</asp:label></td>
				</tr>
				<tr>
					<td colSpan="7"><asp:datagrid id="dgsched" runat="server" ShowFooter="False" CellPadding="0" cellspacing="2" GridLines="None"
							AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False">
							<FooterStyle BackColor="White"></FooterStyle>
							<EditItemStyle Height="15px"></EditItemStyle>
							<AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="ControlLightLight"></ItemStyle>
							<HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:ImageButton id="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
										<asp:ImageButton id="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Week Of">
									<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=Label4 runat="server" Width="150px" Text='<%# DataBinder.Eval(Container, "DataItem.weekof") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id=txtname runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.weekof") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hours Monday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.monhrs") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtmonhrs" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.monhrs") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Monday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label16" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.monstat") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblmonstat" width="30px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.monstat") %>'>
										</asp:Label><img id="imgmonstat" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton11" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											CommandName="hsk" ToolTip="Hide Skill"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											CommandName="ssk" ToolTip="Show Skill"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Assigned Skill" Visible="False">
									<HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.monskill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblmonskill" width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.monskill") %>'>
										</asp:Label><img id="imgmonskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hours Tuesday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tuehrs") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txttuehrs" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.tuehrs") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Tuesday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label17" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tuestat") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lbltuestat" width="30px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tuestat") %>'>
										</asp:Label><img id="imgtuestat" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											CommandName="hsk9" ToolTip="Hide Skill"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											CommandName="ssk9" ToolTip="Show Skill"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Assigned Skill" Visible="False">
									<HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tueskill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lbltueskill" width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tueskill") %>'>
										</asp:Label><img id="imgtueskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hours Wednesday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wedhrs") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtwedhrs" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wedhrs") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Wednesday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wedstat") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblwedstat" width="30px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wedstat") %>'>
										</asp:Label><img id="imgwedstat" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											CommandName="hsk13" ToolTip="Hide Skill"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton7" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											CommandName="ssk13" ToolTip="Show Skill"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Assigned Skill" Visible="False">
									<HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wedskill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblwedskill" width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wedskill") %>'>
										</asp:Label><img id="imgwedskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hours Thursday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.thuhrs") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtthuhrs" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.thuhrs") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Thursday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label19" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.thustat") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblthustat" width="30px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.thustat") %>'>
										</asp:Label><img id="imgthustat" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											CommandName="hsk17" ToolTip="Hide Skill"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											CommandName="ssk17" ToolTip="Show Skill"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Assigned Skill" Visible="False">
									<HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.thuskill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblthuskill" width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.thuskill") %>'>
										</asp:Label><img id="imgthuskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hours Friday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label10" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.frihrs") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtfrihrs" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.frihrs") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Friday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fristat") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblfristat" width="30px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fristat") %>'>
										</asp:Label><img id="imgfristat" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton10" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											CommandName="hsk21" ToolTip="Hide Skill"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton13" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											CommandName="ssk21" ToolTip="Show Skill"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Assigned Skill" Visible="False">
									<HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label11" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.friskill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblfriskill" width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.friskill") %>'>
										</asp:Label><img id="imgfriskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hours Saturday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label12" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sathrs") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtsathrs" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.sathrs") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Saturday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label21" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.satstat") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblsatstat" width="30px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.satstat") %>'>
										</asp:Label><img id="imgsatstat" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton14" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											CommandName="hsk25" ToolTip="Hide Skill"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton15" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											CommandName="ssk25" ToolTip="Show Skill"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Assigned Skill" Visible="False">
									<HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label13" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.satskill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblsatskill" width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.satskill") %>'>
										</asp:Label><img id="imgsatskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Hours Sunday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sunhrs") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtsunhrs" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.sunhrs") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Sunday">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label22" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sunstat") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblsunstat" width="30px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sunstat") %>'>
										</asp:Label><img id="imgsunstat" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="8px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<HeaderTemplate>
										<asp:ImageButton id="Imagebutton16" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
											CommandName="hsk29" ToolTip="Hide Skill"></asp:ImageButton><BR>
										<asp:ImageButton id="Imagebutton17" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
											CommandName="ssk29" ToolTip="Show Skill"></asp:ImageButton>
									</HeaderTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Assigned Skill" Visible="False">
									<HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label15" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sunskill") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblsunskill" width="120px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sunskill") %>'>
										</asp:Label><img id="imgsunskill" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label23" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lsid") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lbllsid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lsid") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
								ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td colSpan="4" align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbluserid" type="hidden" runat="server"> <input id="lblfslang" type="hidden" runat="server">
			<input id="lblsid" type="hidden" runat="server"> <input id="lblsubmit" type="hidden" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"><input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="lblret" type="hidden" runat="server"> <input id="lblretmonstat" type="hidden" name="lblretmonstat" runat="server">
			<input id="lblrettuestat" type="hidden" name="lblrettuestat" runat="server"> <input id="lblretwedstat" type="hidden" name="lblretwedstat" runat="server">
			<input id="lblretthustat" type="hidden" name="lblretthustat" runat="server"> <input id="lblretfristat" type="hidden" name="lblretfristat" runat="server">
			<input id="lblretsatstat" type="hidden" name="lblretsatstat" runat="server"> <input id="lblretsunstat" type="hidden" name="lblretsunstat" runat="server">
			<input id="lblretmonskill" type="hidden" name="lblretmonskill" runat="server"> <input id="lblretmonskillid" type="hidden" name="lblretmonskillid" runat="server">
			<input id="lblrettueskill" type="hidden" name="lblrettueskill" runat="server"> <input id="lblrettueskillid" type="hidden" name="lblrettueskillid" runat="server">
			<input id="lblretwedskill" type="hidden" name="lblretwedskill" runat="server"> <input id="lblretwedskillid" type="hidden" name="lblretwedskillid" runat="server">
			<input id="lblretthuskill" type="hidden" name="lblretthuskill" runat="server"> <input id="lblretthuskillid" type="hidden" name="lblretthuskillid" runat="server">
			<input id="lblretfriskill" type="hidden" name="lblretfriskill" runat="server"> <input id="lblretfriskillid" type="hidden" name="lblretfriskillid" runat="server">
			<input id="lblretsatskill" type="hidden" name="lblretsatskill" runat="server"> <input id="lblretsatskillid" type="hidden" name="lblretsatskillid" runat="server">
			<input id="lblretsunskill" type="hidden" name="lblretsunskill" runat="server"> <input id="lblretsunskillid" type="hidden" name="lblretsunskillid" runat="server">
			<input id="lblmonret" type="hidden" name="lblmonret" runat="server"> <input id="lbltueret" type="hidden" name="lbltueret" runat="server">
			<input id="lblwedret" type="hidden" name="lblwedret" runat="server"> <input id="lblthuret" type="hidden" name="lblthuret" runat="server">
			<input id="lblfriret" type="hidden" name="lblfriret" runat="server"> <input id="lblsatret" type="hidden" name="lblsatret" runat="server">
			<input id="lblsunret" type="hidden" name="lblsunret" runat="server"> <input id="lblmonretstat" type="hidden" name="lblmonretstat" runat="server">
			<input id="lbltueretstat" type="hidden" name="lbltueretstat" runat="server"> <input id="lblwedretstat" type="hidden" name="lblwedretstat" runat="server">
			<input id="lblthuretstat" type="hidden" name="lblthuretstat" runat="server"> <input id="lblfriretstat" type="hidden" name="lblfriretstat" runat="server">
			<input id="lblsatretstat" type="hidden" name="lblsatretstat" runat="server"> <input id="lblsunretstat" type="hidden" name="lblsunretstat" runat="server">
			<input id="lblreghrs" type="hidden" runat="server"> <input id="lblregskill" type="hidden" runat="server">
			<input id="lblregskillid" type="hidden" runat="server">
		</form>
	</body>
</HTML>
