Imports System.Data.SqlClient
Imports System.Text

Public Class labweeksched
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Protected WithEvents lblwkid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblworkarea As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim lw As New Utilities
    Protected WithEvents tdarch As System.Web.UI.HtmlControls.HtmlTableCell
    Dim wkid, sid, skillid, workarea, weekof As String
    Protected WithEvents tdwo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdworkarea As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblweekof As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewweekof As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewwkid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim intPgNav As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            wkid = Request.QueryString("wkid").ToString
            weekof = Request.QueryString("wkof").ToString '"04/09/2010-04/15/2010"
            lblsid.Value = sid
            lblwkid.Value = wkid
            lblweekof.Value = weekof
            tdwo.InnerHtml = weekof
            txtpg.Value = PageNumber
            lw.Open()
            LoadWeek(wkid, sid, skillid, workarea, PageNumber)
            lw.Dispose()
        Else
            If Request.Form("lblsubmit") = "getweeks" Then
                lblsubmit.Value = ""
                sid = lblsid.Value
                wkid = lblwkid.Value
                skillid = lblskillid.Value
                workarea = lblwaid.Value
                PageNumber = txtpg.Value
                lw.Open()
                LoadWeek(wkid, sid, skillid, workarea, PageNumber)
                lw.Dispose()
                tdskill.InnerHtml = lblskill.Value
                tdworkarea.InnerHtml = lblworkarea.Value
                lblweekof.Value = weekof
                tdwo.InnerHtml = weekof
            End If
            If Request.Form("lblret") = "next" Then
                lw.Open()
                GetNext()
                lw.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                lw.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                sid = lblsid.Value
                wkid = lblwkid.Value
                skillid = lblskillid.Value
                workarea = lblwaid.Value
                LoadWeek(wkid, sid, skillid, workarea, PageNumber)
                lw.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                lw.Open()
                GetPrev()
                lw.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                lw.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                sid = lblsid.Value
                wkid = lblwkid.Value
                skillid = lblskillid.Value
                workarea = lblwaid.Value
                LoadWeek(wkid, sid, skillid, workarea, PageNumber)
                lw.Dispose()
                lblret.Value = ""
            End If
            End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            sid = lblsid.Value
            wkid = lblwkid.Value
            skillid = lblskillid.Value
            workarea = lblwaid.Value
            LoadWeek(wkid, sid, skillid, workarea, PageNumber)
        Catch ex As Exception
            lw.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1521", "PMLeadMan.aspx.vb")

            lw.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            sid = lblsid.Value
            wkid = lblwkid.Value
            skillid = lblskillid.Value
            workarea = lblwaid.Value
            LoadWeek(wkid, sid, skillid, workarea, PageNumber)
        Catch ex As Exception
            lw.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1522", "PMLeadMan.aspx.vb")

            lw.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub LoadWeek(ByVal wkid As String, ByVal sid As String, ByVal skillid As String, ByVal workarea As String, ByVal PageNumber As String)

        If skillid = "" And workarea = "" Then
            sql = "select count(*) from labsched ls " _
            + "left join pmsysusers s on s.userid = ls.userid " _
            + "where ls.wkid = '" & wkid & "' and s.dfltps = '" & sid & "'"
            intPgNav = lw.PageCount(sql, PageSize)
        ElseIf skillid <> "" And workarea = "" Then
            sql = "select count(*) from labsched ls " _
            + "left join pmsysusers s on s.userid = ls.userid " _
            + "where ls.wkid = '" & wkid & "' and s.skillid = '" & skillid & "' and ls.mskill = 0 and s.dfltps = '" & sid & "'"
            intPgNav = lw.PageCount(sql, PageSize)
            sql = "select count(*) from labsched ls " _
            + "left join pmsysusers s on s.userid = ls.userid where " _
            + "mskill = 1 and s.skillid <> '" & skillid & "' and ls.wkid = '" & wkid & "' and s.dfltps = '" & sid & "' and " _
            + "((ls.monhrs > 0 and ls.monskill = '" & skillid & "') or " _
            + "(ls.tuehrs > 0 and ls.tueskill = '" & skillid & "') or " _
            + "(ls.wedhrs > 0 and ls.wedskill = '" & skillid & "') or " _
            + "(ls.thuhrs > 0 and ls.thuskill = '" & skillid & "') or " _
            + "(ls.frihrs > 0 and ls.friskill = '" & skillid & "') or " _
            + "(ls.sathrs > 0 and ls.satskill = '" & skillid & "') or " _
            + "(ls.sunhrs > 0 and ls.sunskill = '" & skillid & "'))"
            intPgNav += lw.PageCount(sql, PageSize)
        ElseIf skillid <> "" And workarea <> "" Then
            sql = "select count(*) from labsched ls " _
            + "left join pmsysusers s on s.userid = ls.userid " _
            + "where ls.wkid = '" & wkid & "' and s.skillid = '" & skillid & "' and ls.mskill = 0 and s.waid = '" & workarea & "' and s.dfltps = '" & sid & "'"
            intPgNav = lw.PageCount(sql, PageSize)
            sql = "select count(*) from labsched ls " _
            + "left join pmsysusers s on s.userid = ls.userid where " _
            + "mskill = 1 and s.skillid <> '" & skillid & "' and ls.wkid = '" & wkid & "' and s.waid = '" & workarea & "' and s.dfltps = '" & sid & "' and " _
            + "((ls.monhrs > 0 and ls.monskill = '" & skillid & "') or " _
            + "(ls.tuehrs > 0 and ls.tueskill = '" & skillid & "') or " _
            + "(ls.wedhrs > 0 and ls.wedskill = '" & skillid & "') or " _
            + "(ls.thuhrs > 0 and ls.thuskill = '" & skillid & "') or " _
            + "(ls.frihrs > 0 and ls.friskill = '" & skillid & "') or " _
            + "(ls.sathrs > 0 and ls.satskill = '" & skillid & "') or " _
            + "(ls.sunhrs > 0 and ls.sunskill = '" & skillid & "'))"
            intPgNav += lw.PageCount(sql, PageSize)
        ElseIf skillid = "" And workarea <> "" Then
            sql = "select count(*) from labsched ls " _
            + "left join pmsysusers s on s.userid = ls.userid " _
            + "where ls.wkid = '" & wkid & "' and s.waid = '" & workarea & "' and s.dfltps = '" & sid & "'"
            intPgNav = lw.PageCount(sql, PageSize)
        End If
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        intPgNav = txtpgcnt.Value
        txtpg.Value = PageNumber
        'sql = "usp_getlaborweek '" & wkid & "','" & sid & "','" & skillid & "','" & workarea & "'"

        sql = "usp_getlaborweek_pg '" & wkid & "','" & sid & "','" & skillid & "','" & workarea & "','" & PageNumber & "','" & PageSize & "'"

        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<div id=""spdiv"" style=""OVERFLOW: auto; WIDTH: 450px;  HEIGHT: 280px"" onscroll=""SetsDivPosition();"">")
        sb.Append("<table width=""430"" cellpadding=""1"">")
        sb.Append("<tr><td class=""btmmenu label"" width=""160"" align=""center"" height=""22"">Employee</td>")
        sb.Append("<td class=""btmmenu label"" width=""60"" align=""center"">Shift</td>")
        sb.Append("<td class=""btmmenu label"" width=""40"" align=""center"">M</td>")
        sb.Append("<td class=""btmmenu label"" width=""40"" align=""center"">Tu</td>")
        sb.Append("<td class=""btmmenu label"" width=""40"" align=""center"">W</td>")
        sb.Append("<td class=""btmmenu label"" width=""40"" align=""center"">Th</td>")
        sb.Append("<td class=""btmmenu label"" width=""40"" align=""center"">F</td>")
        sb.Append("<td class=""btmmenu label"" width=""40"" align=""center"">Sa</td>")
        sb.Append("<td class=""btmmenu label"" width=""40"" align=""center"">Su</td></tr>")
        Dim emp, shift, mon, tue, wed, thu, fri, sat, sun As String

        dr = lw.GetRdrData(sql)
        While dr.Read
            emp = dr.Item("username").ToString
            shift = dr.Item("shift").ToString

            mon = dr.Item("monhrs").ToString
            tue = dr.Item("tuehrs").ToString
            wed = dr.Item("wedhrs").ToString
            thu = dr.Item("thuhrs").ToString
            fri = dr.Item("frihrs").ToString
            sat = dr.Item("sathrs").ToString
            sun = dr.Item("sunhrs").ToString

            sb.Append("<tr><td class=""plainlabel"">" & emp & "</td>")
            sb.Append("<td class=""plainlabel"">" & shift & "</td>")

            sb.Append("<td class=""plainlabel"">" & mon & "</td>")
            sb.Append("<td class=""plainlabel"">" & tue & "</td>")
            sb.Append("<td class=""plainlabel"">" & wed & "</td>")
            sb.Append("<td class=""plainlabel"">" & thu & "</td>")
            sb.Append("<td class=""plainlabel"">" & fri & "</td>")
            sb.Append("<td class=""plainlabel"">" & sat & "</td>")
            sb.Append("<td class=""plainlabel"">" & sun & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        tdarch.InnerHtml = sb.ToString



    End Sub
End Class
