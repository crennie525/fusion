

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class labshedmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang2978 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2977 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2976 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2975 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2974 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2973 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2972 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2971 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2970 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2969 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim lab As New Utilities
    Dim sid, cid, userid As String
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdname As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdemail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsup As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdreghrs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdshift As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 20
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim decPgNav As Decimal
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretmonstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettuestat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretwedstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretthustat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretfristat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretsatstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretsunstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretmonskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretmonskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettueskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettueskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretwedskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretwedskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretthuskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretthuskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretfriskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretfriskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretsatskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretsatskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretsunskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretsunskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmonret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltueret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwedret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblthuret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfriret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsatret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsunret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmonretstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltueretstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwedretstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblthuretstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfriretstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsatretstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsunretstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblreghrs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblregskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblregskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim intPgNav As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgsched As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
        'GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            sid = HttpContext.Current.Session("dfltps").ToString '"12"
            lblsid.Value = sid
            txtpg.Value = "1"
        Else
            If Request.Form("lblsubmit") = "newlab" Then
                lab.Open()
                LoadLabor()
                GetLab()
                lab.Dispose()
                lblsubmit.Value = ""
            End If
            If Request.Form("lblret") = "next" Then
                lab.Open()
                GetNext()
                lab.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                lab.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetLab()
                lab.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                lab.Open()
                GetPrev()
                lab.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                lab.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetLab()
                lab.Dispose()
                lblret.Value = ""
            End If
            End If
    End Sub
    Private Sub LoadLabor()
        Dim hrs, m, t, w, th, f, s, su As String
        Dim skill, skillid, shift As String
        userid = lbluserid.Value
        sid = lblsid.Value
        sql = "select l.hrs, l.mon, l.tue, l.wed, l.thu, l.fri, l.sat, l.sun, s.* " _
        + "from pmsysusers s left join labregsched l on l.userid = s.userid where s.userid = '" & userid & "' and s.dfltps = '" & sid & "'"
        dr = lab.GetRdrData(sql)
        While dr.Read
            tdname.InnerHtml = dr.Item("username").ToString
            tdphone.InnerHtml = dr.Item("phonenum").ToString
            tdemail.InnerHtml = dr.Item("email").ToString
            tdsup.InnerHtml = dr.Item("super").ToString
            skill = dr.Item("skill").ToString
            skillid = dr.Item("skillid").ToString
            shift = dr.Item("shift").ToString
            hrs = dr.Item("hrs").ToString
            m = dr.Item("mon").ToString
            t = dr.Item("tue").ToString
            w = dr.Item("wed").ToString
            th = dr.Item("thu").ToString
            f = dr.Item("fri").ToString
            s = dr.Item("sat").ToString
            su = dr.Item("sun").ToString
        End While
        dr.Close()
        tdreghrs.InnerHtml = hrs
        tdshift.InnerHtml = shift
        If shift = "0" Then
            tdshift.InnerHtml = "ANY"
        End If
        tdskill.InnerHtml = skill
        lblregskill.Value = skill
        lblregskillid.Value = skillid
        If m = "1" Then
            cb1mon.Checked = True
        Else
            cb1mon.Checked = False
        End If
        If t = "1" Then
            cb1tue.Checked = True
        Else
            cb1tue.Checked = False
        End If
        If w = "1" Then
            cb1wed.Checked = True
        Else
            cb1wed.Checked = False
        End If
        If th = "1" Then
            cb1thu.Checked = True
        Else
            cb1thu.Checked = False
        End If
        If f = "1" Then
            cb1fri.Checked = True
        Else
            cb1fri.Checked = False
        End If
        If s = "1" Then
            cb1sat.Checked = True
        Else
            cb1sat.Checked = False
        End If
        If su = "1" Then
            cb1sun.Checked = True
        Else
            cb1sun.Checked = False
        End If
    End Sub
    Private Sub GetLab()
        userid = lbluserid.Value
        sid = lblsid.Value
        Dim intPgCnt As Integer
        sql = "select count(*) from labsched where userid = '" & userid & "'"
        intPgCnt = lab.Scalar(sql)
        PageNumber = txtpg.Value
        intPgNav = lab.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        sql = "usp_getUserPg '" & PageNumber & "', '" & PageSize & "', '" & userid & "', '" & sid & "'"

        dr = lab.GetRdrData(sql)
        dgsched.DataSource = dr
        dgsched.DataBind()
        dr.Close()
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetLab()
        Catch ex As Exception
            lab.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr875", "eqcopy2.aspx.vb")

            lab.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetLab()
        Catch ex As Exception
            lab.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr876", "eqcopy2.aspx.vb")

            lab.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgsched.Columns(0).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "0")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(1).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "1")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(2).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "2")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(3).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "3")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(4).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "4")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(5).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "5")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(6).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "6")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(7).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "7")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(8).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "8")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(9).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "9")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(10).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "10")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(11).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "11")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(12).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "12")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(13).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "13")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(14).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "14")
        Catch ex As Exception
        End Try
        Try
            dgsched.Columns(15).HeaderText = dlabs.GetDGPage("labshedmain.aspx", "dgsched", "15")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2969.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2969")
        Catch ex As Exception
        End Try
        Try
            lang2970.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2970")
        Catch ex As Exception
        End Try
        Try
            lang2971.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2971")
        Catch ex As Exception
        End Try
        Try
            lang2972.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2972")
        Catch ex As Exception
        End Try
        Try
            lang2973.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2973")
        Catch ex As Exception
        End Try
        Try
            lang2974.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2974")
        Catch ex As Exception
        End Try
        Try
            lang2975.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2975")
        Catch ex As Exception
        End Try
        Try
            lang2976.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2976")
        Catch ex As Exception
        End Try
        Try
            lang2977.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2977")
        Catch ex As Exception
        End Try
        Try
            lang2978.Text = axlabs.GetASPXPage("labshedmain.aspx", "lang2978")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub dgsched_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsched.ItemCommand
        Dim vc As String = e.CommandName
        Select Case vc
            Case "hsk"
                dgsched.Columns(5).Visible = False
            Case "ssk"
                dgsched.Columns(5).Visible = True
            Case "hsk9"
                dgsched.Columns(9).Visible = False
            Case "ssk9"
                dgsched.Columns(9).Visible = True
            Case "hsk13"
                dgsched.Columns(13).Visible = False
            Case "ssk13"
                dgsched.Columns(13).Visible = True
            Case "hsk17"
                dgsched.Columns(17).Visible = False
            Case "ssk17"
                dgsched.Columns(17).Visible = True
            Case "hsk21"
                dgsched.Columns(21).Visible = False
            Case "ssk21"
                dgsched.Columns(21).Visible = True
            Case "hsk25"
                dgsched.Columns(25).Visible = False
            Case "ssk25"
                dgsched.Columns(25).Visible = True
            Case "hsk29"
                dgsched.Columns(29).Visible = False
            Case "ssk29"
                dgsched.Columns(29).Visible = True
        End Select
    End Sub

    Private Sub dgsched_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsched.EditCommand
        PageNumber = txtpg.Value
        dgsched.EditItemIndex = e.Item.ItemIndex
        lab.Open()
        GetLab()
        lab.Dispose()
    End Sub

    Private Sub dgsched_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgsched.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim lblmonskill1 As Label = CType(e.Item.FindControl("lblmonskill"), Label)
            'Dim lblmonskillid As Label = CType(e.Item.FindControl("lblmonskid"), Label)
            Dim monskillid As String
            monskillid = lblmonskill1.ClientID.ToString
            Dim imgmonskill As HtmlImage = CType(e.Item.FindControl("imgmonskill"), HtmlImage)
            imgmonskill.Attributes.Add("onclick", "getskill('lblmonret','lblretmonskill','lblretmonskillid','" & monskillid & "');")

            Dim lbltueskill1 As Label = CType(e.Item.FindControl("lbltueskill"), Label)
            'Dim lbltueskillid As Label = CType(e.Item.FindControl("lbltueskid"), Label)
            Dim tueskillid As String
            tueskillid = lbltueskill1.ClientID.ToString
            Dim imgtueskill As HtmlImage = CType(e.Item.FindControl("imgtueskill"), HtmlImage)
            imgtueskill.Attributes.Add("onclick", "getskill('lbltueret','lblrettueskill','lblrettueskillid','" & tueskillid & "');")

            Dim lblwedskill1 As Label = CType(e.Item.FindControl("lblwedskill"), Label)
            'Dim lblwedskillid As Label = CType(e.Item.FindControl("lblwedskid"), Label)
            Dim wedskillid As String
            wedskillid = lblwedskill1.ClientID.ToString
            Dim imgwedskill As HtmlImage = CType(e.Item.FindControl("imgwedskill"), HtmlImage)
            imgwedskill.Attributes.Add("onclick", "getskill('lblwedret','lblretwedskill','lblretwedskillid','" & wedskillid & "');")

            Dim lblthuskill1 As Label = CType(e.Item.FindControl("lblthuskill"), Label)
            'Dim lblthuskillid As Label = CType(e.Item.FindControl("lblthuskid"), Label)
            Dim thuskillid As String
            thuskillid = lblthuskill1.ClientID.ToString
            Dim imgthuskill As HtmlImage = CType(e.Item.FindControl("imgthuskill"), HtmlImage)
            imgthuskill.Attributes.Add("onclick", "getskill('lblthuret','lblretthuskill','lblretthuskillid','" & thuskillid & "');")

            Dim lblfriskill1 As Label = CType(e.Item.FindControl("lblfriskill"), Label)
            'Dim lblfriskillid As Label = CType(e.Item.FindControl("lblfriskid"), Label)
            Dim friskillid As String
            friskillid = lblfriskill1.ClientID.ToString
            Dim imgfriskill As HtmlImage = CType(e.Item.FindControl("imgfriskill"), HtmlImage)
            imgfriskill.Attributes.Add("onclick", "getskill('lblfriret','lblretfriskill','lblretfriskillid','" & friskillid & "');")

            Dim lblsatskill1 As Label = CType(e.Item.FindControl("lblsatskill"), Label)
            'Dim lblsatskillid As Label = CType(e.Item.FindControl("lblsatskid"), Label)
            Dim satskillid As String
            satskillid = lblsatskill1.ClientID.ToString
            Dim imgsatskill As HtmlImage = CType(e.Item.FindControl("imgsatskill"), HtmlImage)
            imgsatskill.Attributes.Add("onclick", "getskill('lblsatret','lblretsatskill','lblretsatskillid','" & satskillid & "');")

            Dim lblsunskill1 As Label = CType(e.Item.FindControl("lblsunskill"), Label)
            'Dim lblsunskillid As Label = CType(e.Item.FindControl("lblsunskid"), Label)
            Dim sunskillid As String
            sunskillid = lblsunskill1.ClientID.ToString
            Dim imgsunskill As HtmlImage = CType(e.Item.FindControl("imgsunskill"), HtmlImage)
            imgsunskill.Attributes.Add("onclick", "getskill('lblsunret','lblretsunskill','lblretsunskillid','" & sunskillid & "');")

            '*******************stat***************************

            Dim lblmonstat1 As Label = CType(e.Item.FindControl("lblmonstat"), Label)
            Dim txtmonhrs1 As TextBox = CType(e.Item.FindControl("txtmonhrs"), TextBox)
            Dim monstatid As String
            monstatid = lblmonstat1.ClientID.ToString
            Dim monhrsid As String
            monhrsid = txtmonhrs1.ClientID.ToString
            Dim imgmonstat As HtmlImage = CType(e.Item.FindControl("imgmonstat"), HtmlImage)
            imgmonstat.Attributes.Add("onclick", "getstat('lblmonretstat','lblretmonstat','" & monstatid & "','" & monhrsid & "');")

            Dim lbltuestat1 As Label = CType(e.Item.FindControl("lbltuestat"), Label)
            Dim txttuehrs1 As TextBox = CType(e.Item.FindControl("txttuehrs"), TextBox)
            Dim tuestatid As String
            tuestatid = lbltuestat1.ClientID.ToString
            Dim tuehrsid As String
            tuehrsid = txttuehrs1.ClientID.ToString
            Dim imgtuestat As HtmlImage = CType(e.Item.FindControl("imgtuestat"), HtmlImage)
            imgtuestat.Attributes.Add("onclick", "getstat('lbltueretstat','lblrettuestat','" & tuestatid & "','" & tuehrsid & "');")

            Dim lblwedstat1 As Label = CType(e.Item.FindControl("lblwedstat"), Label)
            Dim txtwedhrs1 As TextBox = CType(e.Item.FindControl("txtwedhrs"), TextBox)
            Dim wedstatid As String
            wedstatid = lblwedstat1.ClientID.ToString
            Dim wedhrsid As String
            wedhrsid = txtwedhrs1.ClientID.ToString
            Dim imgwedstat As HtmlImage = CType(e.Item.FindControl("imgwedstat"), HtmlImage)
            imgwedstat.Attributes.Add("onclick", "getstat('lblwedretstat','lblretwedstat','" & wedstatid & "','" & wedhrsid & "');")

            Dim lblthustat1 As Label = CType(e.Item.FindControl("lblthustat"), Label)
            Dim txtthuhrs1 As TextBox = CType(e.Item.FindControl("txtthuhrs"), TextBox)
            Dim thustatid As String
            thustatid = lblthustat1.ClientID.ToString
            Dim thuhrsid As String
            thuhrsid = txtthuhrs1.ClientID.ToString
            Dim imgthustat As HtmlImage = CType(e.Item.FindControl("imgthustat"), HtmlImage)
            imgthustat.Attributes.Add("onclick", "getstat('lblthuretstat','lblretthustat','" & thustatid & "','" & thuhrsid & "');")

            Dim lblfristat1 As Label = CType(e.Item.FindControl("lblfristat"), Label)
            Dim txtfrihrs1 As TextBox = CType(e.Item.FindControl("txtfrihrs"), TextBox)
            Dim fristatid As String
            fristatid = lblfristat1.ClientID.ToString
            Dim frihrsid As String
            frihrsid = txtfrihrs1.ClientID.ToString
            Dim imgfristat As HtmlImage = CType(e.Item.FindControl("imgfristat"), HtmlImage)
            imgfristat.Attributes.Add("onclick", "getstat('lblfriretstat','lblretfristat','" & fristatid & "','" & frihrsid & "');")

            Dim lblsatstat1 As Label = CType(e.Item.FindControl("lblsatstat"), Label)
            Dim txtsathrs1 As TextBox = CType(e.Item.FindControl("txtsathrs"), TextBox)
            Dim satstatid As String
            satstatid = lblsatstat1.ClientID.ToString
            Dim sathrsid As String
            sathrsid = txtsathrs1.ClientID.ToString
            Dim imgsatstat As HtmlImage = CType(e.Item.FindControl("imgsatstat"), HtmlImage)
            imgsatstat.Attributes.Add("onclick", "getstat('lblsatretstat','lblretsatstat','" & satstatid & "','" & sathrsid & "');")

            Dim lblsunstat1 As Label = CType(e.Item.FindControl("lblsunstat"), Label)
            Dim txtsunhrs1 As TextBox = CType(e.Item.FindControl("txtsunhrs"), TextBox)
            Dim sunstatid As String
            sunstatid = lblsunstat1.ClientID.ToString
            Dim sunhrsid As String
            sunhrsid = txtsunhrs1.ClientID.ToString
            Dim imgsunstat As HtmlImage = CType(e.Item.FindControl("imgsunstat"), HtmlImage)
            imgsunstat.Attributes.Add("onclick", "getstat('lblsunretstat','lblretsunstat','" & sunstatid & "','" & sunhrsid & "');")


        End If

       
    End Sub

    Private Sub dgsched_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsched.UpdateCommand

        Dim lsid As String
        Dim monhrs, tuehrs, wedhrs, thuhrs, frihrs, sathrs, sunhrs As String
        Dim monstat, tuestat, wedstat, thustat, fristat, satstat, sunstat As String
        Dim monskill, tueskill, wedskill, thuskill, friskill, satskill, sunskill As String
        Dim monskillid, tueskillid, wedskillid, thuskillid, friskillid, satskillid, sunskillid As String
        Dim mon, tue, wed, thu, fri, sat, sun As String
        Dim mskill As String = "0"
        Dim skillid As String = lblregskillid.Value

        lsid = CType(e.Item.FindControl("lbllsid"), Label).Text

        If lblmonret.Value = "yes" Then
            monskill = lblretmonskill.Value
            monskillid = lblretmonskillid.Value
        Else
            monskill = lblregskill.Value
            monskillid = lblregskillid.Value
        End If
        If monskillid <> skillid Then
            mskill = "1"
        End If

        If lbltueret.Value = "yes" Then
            tueskill = lblrettueskill.Value
            tueskillid = lblrettueskillid.Value
        Else
            tueskill = lblregskill.Value
            tueskillid = lblregskillid.Value
        End If
        If tueskillid <> skillid Then
            mskill = "1"
        End If

        If lblwedret.Value = "yes" Then
            wedskill = lblretwedskill.Value
            wedskillid = lblretwedskillid.Value
        Else
            wedskill = lblregskill.Value
            wedskillid = lblregskillid.Value
        End If
        If wedskillid <> skillid Then
            mskill = "1"
        End If

        If lblthuret.Value = "yes" Then
            thuskill = lblretthuskill.Value
            thuskillid = lblretthuskillid.Value
        Else
            thuskill = lblregskill.Value
            thuskillid = lblregskillid.Value
        End If
        If thuskillid <> skillid Then
            mskill = "1"
        End If

        If lblfriret.Value = "yes" Then
            friskill = lblretfriskill.Value
            friskillid = lblretfriskillid.Value
        Else
            friskill = lblregskill.Value
            friskillid = lblregskillid.Value
        End If
        If friskillid <> skillid Then
            mskill = "1"
        End If

        If lblsatret.Value = "yes" Then
            satskill = lblretsatskill.Value
            satskillid = lblretsatskillid.Value
        Else
            satskill = lblregskill.Value
            satskillid = lblregskillid.Value
        End If
        If satskillid <> skillid Then
            mskill = "1"
        End If

        If lblsunret.Value = "yes" Then
            sunskill = lblretsunskill.Value
            sunskillid = lblretsunskillid.Value
        Else
            sunskill = lblregskill.Value
            sunskillid = lblregskillid.Value
        End If
        If sunskillid <> skillid Then
            mskill = "1"
        End If

        '***************stat*******************
        monhrs = CType(e.Item.FindControl("txtmonhrs"), TextBox).Text
        tuehrs = CType(e.Item.FindControl("txttuehrs"), TextBox).Text
        wedhrs = CType(e.Item.FindControl("txtwedhrs"), TextBox).Text
        thuhrs = CType(e.Item.FindControl("txtthuhrs"), TextBox).Text
        frihrs = CType(e.Item.FindControl("txtfrihrs"), TextBox).Text
        sathrs = CType(e.Item.FindControl("txtsathrs"), TextBox).Text
        sunhrs = CType(e.Item.FindControl("txtsunhrs"), TextBox).Text

        If lblmonretstat.Value = "yes" Then
            monstat = lblretmonstat.Value
            If monstat = "O" Or monstat = "V" Or monstat = "T" Or monstat = "S" Then
                monhrs = "0"
                mon = "0"
            Else
                Dim monchk As Decimal
                Try
                    monchk = System.Convert.ToDecimal(monhrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Monday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        Else
            monstat = CType(e.Item.FindControl("lblmonstat"), Label).Text
            If monstat = "O" Or monstat = "V" Or monstat = "T" Or monstat = "S" Then
                monhrs = "0"
                mon = "0"
            Else
                Dim monchk As Decimal
                Try
                    monchk = System.Convert.ToDecimal(monhrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Monday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        End If

        If lbltueretstat.Value = "yes" Then
            tuestat = lblrettuestat.Value
            If tuestat = "O" Or tuestat = "V" Or tuestat = "T" Or tuestat = "S" Then
                tuehrs = "0"
                tue = "0"
            Else
                Dim tuechk As Decimal
                Try
                    tuechk = System.Convert.ToDecimal(tuehrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Tuesday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        Else
            tuestat = CType(e.Item.FindControl("lbltuestat"), Label).Text
            If tuestat = "O" Or tuestat = "V" Or tuestat = "T" Or tuestat = "S" Then
                tuehrs = "0"
                tue = "0"
            Else
                Dim tuechk As Decimal
                Try
                    tuechk = System.Convert.ToDecimal(tuehrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Tuesday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        End If

        If lblwedretstat.Value = "yes" Then
            wedstat = lblretwedstat.Value
            If wedstat = "O" Or wedstat = "V" Or wedstat = "T" Or wedstat = "S" Then
                wedhrs = "0"
                wed = "0"
            Else
                Dim wedchk As Decimal
                Try
                    wedchk = System.Convert.ToDecimal(wedhrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Wednesday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        Else
            wedstat = CType(e.Item.FindControl("lblwedstat"), Label).Text
            If wedstat = "O" Or wedstat = "V" Or wedstat = "T" Or wedstat = "S" Then
                wedhrs = "0"
                wed = "0"
            Else
                Dim wedchk As Decimal
                Try
                    wedchk = System.Convert.ToDecimal(wedhrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Wednesday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        End If

        If lblthuretstat.Value = "yes" Then
            thustat = lblretthustat.Value
            If thustat = "O" Or thustat = "V" Or thustat = "T" Or thustat = "S" Then
                thuhrs = "0"
                thu = "0"
            Else
                Dim thuchk As Decimal
                Try
                    thuchk = System.Convert.ToDecimal(thuhrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Thursday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        Else
            thustat = CType(e.Item.FindControl("lblthustat"), Label).Text
            If thustat = "O" Or thustat = "V" Or thustat = "T" Or thustat = "S" Then
                thuhrs = "0"
                thu = "0"
            Else
                Dim thuchk As Decimal
                Try
                    thuchk = System.Convert.ToDecimal(thuhrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Thursday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        End If

        If lblfriretstat.Value = "yes" Then
            fristat = lblretfristat.Value
            If fristat = "O" Or fristat = "V" Or fristat = "T" Or fristat = "S" Then
                frihrs = "0"
                fri = "0"
            Else
                Dim frichk As Decimal
                Try
                    frichk = System.Convert.ToDecimal(frihrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Friday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        Else
            fristat = CType(e.Item.FindControl("lblfristat"), Label).Text
            If fristat = "O" Or fristat = "V" Or fristat = "T" Or fristat = "S" Then
                frihrs = "0"
                fri = "0"
            Else
                Dim frichk As Decimal
                Try
                    frichk = System.Convert.ToDecimal(frihrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Friday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        End If

        If lblsatretstat.Value = "yes" Then
            satstat = lblretsatstat.Value
            If satstat = "O" Or satstat = "V" Or satstat = "T" Or satstat = "S" Then
                sathrs = "0"
                sat = "0"
            Else
                Dim satchk As Decimal
                Try
                    satchk = System.Convert.ToDecimal(sathrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Saturday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        Else
            satstat = CType(e.Item.FindControl("lblsatstat"), Label).Text
            If satstat = "O" Or satstat = "V" Or satstat = "T" Or satstat = "S" Then
                sathrs = "0"
                sat = "0"
            Else
                Dim satchk As Decimal
                Try
                    satchk = System.Convert.ToDecimal(sathrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Monday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        End If

        If lblsunretstat.Value = "yes" Then
            sunstat = lblretsunstat.Value
            If sunstat = "O" Or sunstat = "V" Or sunstat = "T" Or sunstat = "S" Then
                sunhrs = "0"
                sun = "0"
            Else
                Dim sunchk As Decimal
                Try
                    sunchk = System.Convert.ToDecimal(sunhrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Sunday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        Else
            sunstat = CType(e.Item.FindControl("lblsunstat"), Label).Text
            If sunstat = "O" Or sunstat = "V" Or sunstat = "T" Or sunstat = "S" Then
                sunhrs = "0"
                sun = "0"
            Else
                Dim sunchk As Decimal
                Try
                    sunchk = System.Convert.ToDecimal(sunhrs)
                Catch ex As Exception
                    Dim strMessage As String = "Hours Sunday must be a Numeric Value"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
            End If
        End If

        lab.Open()

        sql = "update labsched set mon = '" & mon & "', " _
        + "monhrs = '" & monhrs & "', monskill = '" & monskillid & "', monstat = '" & monstat & "', " _
        + "tue = '" & tue & "', tueskill = '" & tueskillid & "', tuehrs = '" & tuehrs & "', tuestat = '" & tuestat & "', " _
        + "wed = '" & wed & "', wedskill = '" & wedskillid & "', wedhrs = '" & wedhrs & "', wedstat = '" & wedstat & "', " _
        + "thu = '" & thu & "', thuskill = '" & thuskillid & "', thuhrs = '" & thuhrs & "', thustat = '" & thustat & "', " _
        + "fri = '" & fri & "', friskill = '" & friskillid & "', frihrs = '" & frihrs & "', fristat = '" & fristat & "', " _
        + "sat = '" & sat & "', satskill = '" & satskillid & "', sathrs = '" & sathrs & "', satstat = '" & satstat & "', " _
        + "sun = '" & sun & "', sunskill = '" & sunskillid & "', sunhrs = '" & sunhrs & "', sunstat = '" & sunstat & "', " _
        + "mskill = '" & mskill & "' where lsid = '" & lsid & "'"

        lab.Update(sql)
        dgsched.EditItemIndex = -1
        GetLab()
        lab.Dispose()
        lblmonretstat.Value = ""
        lbltueretstat.Value = ""
        lblwedretstat.Value = ""
        lblthuretstat.Value = ""
        lblfriretstat.Value = ""
        lblsatretstat.Value = ""
        lblsunretstat.Value = ""

        lblmonret.Value = ""
        lbltueret.Value = ""
        lblwedret.Value = ""
        lblthuret.Value = ""
        lblfriret.Value = ""
        lblsatret.Value = ""
        lblsunret.Value = ""

        'End Try
    End Sub

    Private Sub dgsched_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgsched.CancelCommand
        lblmonretstat.Value = ""
        lbltueretstat.Value = ""
        lblwedretstat.Value = ""
        lblthuretstat.Value = ""
        lblfriretstat.Value = ""
        lblsatretstat.Value = ""
        lblsunretstat.Value = ""

        lblmonret.Value = ""
        lbltueret.Value = ""
        lblwedret.Value = ""
        lblthuret.Value = ""
        lblfriret.Value = ""
        lblsatret.Value = ""
        lblsunret.Value = ""
        dgsched.EditItemIndex = -1
        lab.Open()
        GetLab()
        lab.Dispose()
    End Sub
End Class
