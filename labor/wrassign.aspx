<%@ Page Language="vb" AutoEventWireup="false" Codebehind="wrassign.aspx.vb" Inherits="lucy_r12.wrassign" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>wrassign</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/wrassignaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checksel();" >
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; LEFT: 2px; POSITION: absolute; TOP: 2px" cellSpacing="0" cellPadding="2"
				width="960">
				<tr>
					<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/woreq.gif" border="0"></td>
					<td class="thdrsingrt label" width="934"><asp:Label id="lang2997" runat="server">Work Request Assignment Administration</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td class="label" width="80"><asp:Label id="lang2998" runat="server">Supervisor</asp:Label></td>
								<td width="180"><asp:dropdownlist id="ddsup" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
								<td class="label" width="80"><asp:Label id="lang2999" runat="server">Plant Site</asp:Label></td>
								<td width="180"><asp:dropdownlist id="ddsite" runat="server"></asp:dropdownlist></td>
								<td class="label" width="60"><asp:Label id="lang3000" runat="server">Shift</asp:Label></td>
								<td class="plainlabel" id="tdshift" width="60" runat="server"></td>
								<td class="label" width="60"><asp:Label id="lang3001" runat="server">Phone</asp:Label></td>
								<td class="plainlabel" id="tdphone" width="80" runat="server"></td>
								<td class="label" width="60"><asp:Label id="lang3002" runat="server">Email</asp:Label></td>
								<td class="plainlabel" id="tdemail" width="200" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table width="960">
							<tr>
								<td class="thdrsingg label" width="300"><asp:Label id="lang3003" runat="server">Labor Assignments (Reference)</asp:Label></td>
								<td class="thdrsingg label" width="660"><asp:Label id="lang3004" runat="server">Skill Assignments</asp:Label></td>
							</tr>
							<tr>
								<td rowSpan="7">
									<table cellSpacing="0" cellPadding="0">
										<TBODY>
											<tr>
												<td><iframe id="getlab" style="BACKGROUND-COLOR: transparent" src="PMSuperSelect.aspx?typ=wait&amp;skill=&amp;wr=&amp;ro="
														frameBorder="no" width="300" scrolling="no" height="125" allowTransparency runat="server"></iframe>
												</td>
								</td>
							</tr>
							<tr height="20">
								<td class="thdrsingg label" width="300"><asp:Label id="lang3005" runat="server">Assignment Conflicts</asp:Label></td>
							</tr>
							<tr>
								<td id="tdconflicts" vAlign="top" rowSpan="6" runat="server"><iframe id="ifcon" style="BACKGROUND-COLOR: transparent" src="conflicts.aspx" frameBorder="no"
										width="300" scrolling="auto" height="280" allowTransparency runat="server"></iframe>
								</td>
							</tr>
						</table>
					<td><iframe id="getskill" style="BACKGROUND-COLOR: transparent" src="transassign.aspx?typ=wait&amp;supid=&amp;wr=&amp;ro="
							frameBorder="no" width="660" scrolling="no" height="85" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="thdrsingg label" width="660"><asp:Label id="lang3006" runat="server">Department Assignments</asp:Label></td>
				</tr>
				<tr>
					<td><iframe id="getdept" style="BACKGROUND-COLOR: transparent" src="transassign.aspx?typ=wait&amp;supid=&amp;wr=&amp;ro="
							frameBorder="no" width="660" scrolling="no" height="85" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="thdrsingg label" width="660"><asp:Label id="lang3007" runat="server">Location Assignments</asp:Label></td>
				</tr>
				<tr>
					<td><iframe id="getloc" style="BACKGROUND-COLOR: transparent" src="transassign.aspx?typ=wait&amp;supid=&amp;wr=&amp;ro="
							frameBorder="no" width="660" scrolling="no" height="85" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="thdrsingg label" width="660"><asp:Label id="lang3008" runat="server">Equipment Assignments</asp:Label></td>
				</tr>
				<tr>
					<td><iframe id="geteq" style="BACKGROUND-COLOR: transparent" src="transassign.aspx?typ=wait&amp;supid=&amp;wr=&amp;ro="
							frameBorder="no" width="660" scrolling="no" height="85" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr height="30">
					<td></td>
					<td>
						<table>
							<tr>
								<td class="redlabel" width="350" id="tdsite" runat="server"><asp:Label id="lang3009" runat="server">Default Supervisor for Missed Assignments:</asp:Label></td>
								<td class="plainlabelred" id="tddef" width="160" runat="server"><asp:Label id="lang3010" runat="server">None Selected</asp:Label></td>
								<td width="20"><IMG onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
										border="0"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE></TABLE></TD></TR></TABLE><input id="lblsid" type="hidden" runat="server">
			<input id="lblsupid" type="hidden" runat="server"><input id="lblsel" type="hidden" runat="server">
			<input id="lblsup" type="hidden" runat="server"><input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lblro" runat="server"><input type="hidden" id="lblsupret" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		
	</body>
</HTML>
