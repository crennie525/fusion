'********************************************************
'*
'********************************************************
Public Class labskillsdialog
    Inherits System.Web.UI.Page
    Dim who, sid, wonum, jpid, pmid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iffreq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
                If who = "wo" Then
                    wonum = Request.QueryString("wo").ToString
                ElseIf who = "jp" Then
                    jpid = Request.QueryString("jpid").ToString
                ElseIf who = "pm" Or who = "pmmax" Then
                    wonum = Request.QueryString("wo").ToString
                    pmid = Request.QueryString("pmid").ToString
                End If
            Catch ex As Exception
                who = "lab"
            End Try
            sid = Request.QueryString("sid").ToString

            iffreq.Attributes.Add("src", "labskills.aspx?who=" & who & "&wo=" & wonum & "&sid=" + sid & "&jpid=" + jpid + "&pmid=" + pmid + "&date=" & Now)

        End If
    End Sub

End Class
