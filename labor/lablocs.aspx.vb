Imports System.Data.SqlClient

Public Class lablocs
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sql, cid, sid, did, clid, locid As String
    Dim Filter, filt, dt, df, tl, val, tli As String
    Dim pmid, type, login As String
    Protected WithEvents celldiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgsw As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang241 As System.Web.UI.WebControls.Label
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang242 As System.Web.UI.WebControls.Label
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lang245 As System.Web.UI.WebControls.Label
    Protected WithEvents lblloc As System.Web.UI.WebControls.Label
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            tasks.Open()
            PopDepts(sid)
            tasks.Dispose()


        End If
    End Sub
    Private Sub PopDepts(ByVal site As String)
        filt = " where siteid = '" & site & "'"
        sql = "select count(*) from Dept where siteid = '" & sid & "'"
        Dim dcnt As Integer = tasks.Scalar(sql)
        If dcnt > 0 Then
            dt = "Dept"
            val = "dept_id , dept_line "
            dr = tasks.GetList(dt, val, filt)
            dddepts.DataSource = dr
            dddepts.DataTextField = "dept_line"
            dddepts.DataValueField = "dept_id"
            dddepts.DataBind()
            dr.Close()
            dddepts.Enabled = True
        Else
            Dim strMessage As String = tmod.getmsg("cdstr219", "PMGetTasksFunc.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            dddepts.Enabled = False
        End If
        dddepts.Items.Insert(0, "Select Department")
    End Sub

    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        Dim dept As String = dddepts.SelectedValue.ToString
        tasks.Open()
        If dddepts.SelectedIndex <> 0 Then
            celldiv.Style.Add("display", "none")
            celldiv.Style.Add("visibility", "hidden")
            CheckTyp(dept)
            Dim deptname As String = dddepts.SelectedItem.ToString
            lbldept.Value = dept
            lblpar.Value = "dept"
            dt = "dept"
            df = "dept_desc"
            filt = " where dept_id = " & dept
            sql = "select count(*) from Cells where dept_id = '" & dept & "'"
            If tl = "2" Or deptname = "No Dept/Line" Then
                'GoToTasks()
            Else
                Filter = dept
                Dim chk As String = CellCheck(Filter)
                If chk = "no" And tl = "3" Then
                    lblchk.Value = "no"
                    lblpar2.Value = "chk"
                    Dim strMessage As String = tmod.getmsg("cdstr220", "PMGetTasksFunc.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                ElseIf chk = "no" AndAlso (tl = "4" Or tl = "5" Or tl = "6" Or tl = "") Then
                    lblclid.Value = "0"
                    lblchk.Value = "no"
                    
                Else
                    lblchk.Value = "yes"
                    lblpar2.Value = "chk"
                    PopCells(dept)
                End If
            End If
        Else
            celldiv.Style.Add("display", "none")
            celldiv.Style.Add("visibility", "hidden")
            
        End If
        tasks.Dispose()

    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = tasks.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = tasks.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
    Private Sub CheckTyp(ByVal dept As String, Optional ByVal loc As String = "N")
        Dim typ As String
        Dim cnt As Integer
        If loc = "N" Then
            sql = "select count(locid) from equipment where dept_id = '" & dept & "' and locid <> 0 and locid is not null"
            cnt = tasks.Scalar(sql)
            If cnt = 0 Then
                lbltyp.Value = "reg"
            Else
                lbltyp.Value = "dloc"
            End If
        Else
            If dept = "" Then
                lbltyp.Value = "loc"
            Else
                sql = "select count(dept_id) from equipment where locid = '" & loc & "' and dept_id <> 0 and dept_id is not null"
                cnt = tasks.Scalar(sql)
                If cnt = 0 Then
                    lbltyp.Value = "loc"
                Else
                    lbltyp.Value = "dloc"
                End If
            End If
        End If

    End Sub
    Private Sub PopCells(ByVal dept As String)
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = tasks.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
        celldiv.Style.Add("display", "block")
        celldiv.Style.Add("visibility", "visible")
    End Sub
End Class
