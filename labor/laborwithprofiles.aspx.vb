'********************************************************
'*
'********************************************************
Imports System.Data.SqlClient
Public Class laborwithprofiles
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql, supid, name, srch, cid, typ, skill, lsupid, sid, ro, rostr As String
    Dim dr As SqlDataReader
    Protected WithEvents lblfilter As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents ddwa As System.Web.UI.WebControls.DropDownList
    Protected WithEvents trwa As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden4 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden5 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden6 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden7 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsavewo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sup As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lang2984 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lang2985 As System.Web.UI.WebControls.Label
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents trsrch As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trskill As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents supdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tr2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents supdiv2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblpost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        If Not IsPostBack Then
            '
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try

            lblro.Value = ro
            If ro = "1" Then
                'imgadd.Attributes.Add("class", "details")
            End If
            If ro <> "1" Then
                Try
                    rostr = HttpContext.Current.Session("rostr").ToString
                Catch ex As Exception
                    rostr = "0"
                End Try

                If Len(rostr) <> 0 Then
                    ro = sup.CheckROS(rostr, "man")
                    If ro <> "1" Then
                        ro = sup.CheckROS(rostr, "wo")
                        If ro <> "1" Then
                            ro = sup.CheckROS(rostr, "wr")
                        End If
                    End If
                End If
                If ro = "1" Then
                    lblro.Value = ro
                    'imgadd.Attributes.Add("class", "details")
                End If
            End If

            typ = Request.QueryString("typ").ToString
            skill = Request.QueryString("skill").ToString
            lbltyp.Value = typ
            lblskillid.Value = skill
            lblsid.Value = HttpContext.Current.Session("dfltps").ToString
            sup.Open()
            If typ = "lead" Then
                LoadSkills()
                LoadLead()
                trskill.Attributes.Add("class", "view")
                tr1.Attributes.Add("class", "view")
                tr2.Attributes.Add("class", "details")
                'imgadd.Attributes.Add("class", "details")
                Try
                    ddskill.SelectedValue = skill
                Catch ex As Exception

                End Try
            End If
            sup.Dispose()
        Else
            If Request.Form("lblpost") = "ret" Then
                sup.Open()
                typ = lbltyp.Value
                If typ = "lead" Then
                    LoadLead()
                    trskill.Attributes.Add("class", "view")
                End If
                sup.Dispose()
            End If
        End If
    End Sub
    Private Sub LoadLead()
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        sid = lblsid.Value
        If Len(srch) > 0 Then
            srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
            srch = Replace(srch, "--", "-", , , vbTextCompare)
            srch = Replace(srch, ";", ":", , , vbTextCompare)
            sql = "select s.* from pmsysusers s left join labregsched l on l.userid = s.userid " _
            + "where s.username like '%" & srch & "%' and s.islabor = 1 and s.dfltps = '" & sid & "' " _
            + "and (s.shift is not null or len(s.shift) > 0) and (l.daycnt is not null or len(l.daycnt) > 0)"
            Try
                ddskill.SelectedIndex = 0
            Catch ex As Exception

            End Try

            
        Else
            If ddskill.SelectedIndex = 0 Then
                sql = "select s.* from pmsysusers s left join labregsched l on l.userid = s.userid " _
                + "where s.dfltps = '" & sid & "' " _
                + "and (s.shift is not null or len(s.shift) > 0) and (l.daycnt is not null or len(l.daycnt) > 0)"
                'sql = "select * from pmsysusers where islabor = 1"
            Else
                skill = ddskill.SelectedValue.ToString
                If skill = "0" Then
                    sql = "select s.* from pmsysusers s left join labregsched l on l.userid = s.userid " _
                    + "where s.dfltps = '" & sid & "' " _
                    + "and (s.shift is not null or len(s.shift) > 0) and (l.daycnt is not null or len(l.daycnt) > 0)"
                    'sql = "select * from pmsysusers where islabor = 1"
                Else
                    sql = "select s.* from pmsysusers s left join labregsched l on l.userid = s.userid " _
                    + "where s.skillid = '" & skill & "' and s.dfltps = '" & sid & "' " _
                    + "and (s.shift is not null or len(s.shift) > 0) and (l.daycnt is not null or len(l.daycnt) > 0)"
                    'sql = "select * from pmsysusers where skillid = '" & skill & "' and islabor = 1 and dfltps = '" & sid & "'"
                End If
                txtsrch.Text = ""
            End If

        End If
        Dim labid, super, email, skillid As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        dr = sup.GetRdrData(sql)
        While dr.Read
            labid = dr.Item("userid").ToString
            name = dr.Item("username").ToString
            skill = dr.Item("skill").ToString
            skillid = dr.Item("skillid").ToString
            supid = dr.Item("superid").ToString
            super = dr.Item("super").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsup('" & labid & "', '" & name & "', '" & supid & "', '" & super & "');"">" & name & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & skill & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        supdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadSkills()
        cid = "0"
        sql = "select skillid, skill " _
        + "from pmSkills where compid = '" & cid & "'"
        dr = sup.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("All Skills"))
        ddskill.Items(0).Value = 0

    End Sub

    Private Sub ddskill_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddskill.SelectedIndexChanged
        typ = lbltyp.Value
        sup.Open()
        If typ = "lead" Then
            LoadLead()
        End If
        sup.Dispose()
    End Sub
    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2984.Text = axlabs.GetASPXPage("PMSuperSelect.aspx", "lang2984")
        Catch ex As Exception
        End Try
        Try
            lang2985.Text = axlabs.GetASPXPage("PMSuperSelect.aspx", "lang2985")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        typ = lbltyp.Value
        sup.Open()
        LoadLead()
        sup.Dispose()
    End Sub

    Private Sub ddwa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddwa.SelectedIndexChanged
        typ = lbltyp.Value
        sup.Open()
        If typ = "lead" Then
            LoadLead()
        End If
        sup.Dispose()
    End Sub
End Class
