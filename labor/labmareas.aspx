<%@ Page Language="vb" AutoEventWireup="false" Codebehind="labmareas.aspx.vb" Inherits="lucy_r12.labmareas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>labmareas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		<script language="JavaScript" src="../scripts/gridnav.js"></script>
		<script language="javascript" type="text/javascript">
		function handleexit() {
		window.parent.handleexit();
		}
		</script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="422">
				<tr>
					<td class="thdrsing label" colSpan="3"><asp:Label id="lang3033" runat="server">Add/Remove Employee Multiple Work Areas</asp:Label></td>
				</tr>
				<tr>
					<td height="22" class="bluelabel">Current Employee</td>
					<td colspan="2" class="plainlabel" id="tdcur" runat="server"></td>
				</tr>
				<tr>
					<td height="22" class="bluelabel">Employee Top Level Work Area</td>
					<td colspan="2" class="plainlabel" id="tdskill" runat="server"></td>
				</tr>
				<tr>
					<td class="label" align="center">
						<asp:Label id="lang3034" runat="server">Available Work Areas</asp:Label>
					</td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang3035" runat="server">Employee Work Areas</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="200">
						<table>
							<tr>
								<td><asp:listbox id="lbskillmaster" runat="server" Width="200px" SelectionMode="Multiple" Height="300px"></asp:listbox></td>
							</tr>
							<tr>
								<td align="center">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td vAlign="top" align="center" width="22"><br>
						<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
					<td vAlign="top" align="center" width="200">
						<table>
							<tr>
								<td><asp:listbox id="lbsiteskills" runat="server" Width="200px" SelectionMode="Multiple" Height="150px"></asp:listbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><IMG id="btnreturn" runat='server' onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
							width="69" height="19"></td>
				</tr>
				<tr>
					<td class="bluelabel" colSpan="3"><asp:Label id="lang3036" runat="server">List Box Options</asp:Label></td>
				</tr>
				<tr>
					<td class="label" colSpan="3"><asp:radiobuttonlist id="cbopts" runat="server" Width="400px" AutoPostBack="True" CssClass="labellt">
							<asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
							<asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td class="note" colSpan="3"><asp:Label id="lang3037" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the right hand list if only one item is present.</asp:Label></td>
				</tr>
			</table>
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"> <input type="hidden" id="lbluserid" runat="server" NAME="lbluserid">
			<input type="hidden" id="lblskillid" runat="server" NAME="lblskillid"> <input type="hidden" id="lblopt" runat="server" NAME="lblopt">
			<input type="hidden" id="txtpg" runat="server"> <input type="hidden" id="txtpgcnt" runat="server">
			<input type="hidden" id="lblret" runat="server">
		</form>
	</body>
</HTML>
