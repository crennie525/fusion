'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class labskills
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql As String
    Dim dval As New Utilities
    Dim dr As SqlDataReader
    Dim vid, vind, val, pmtskid, fld, sid, rt, ort, who, wo, jpid, pmid As String
    Protected WithEvents rball As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbsite As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblss As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbur As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljpid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divval As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString '"12"
            Catch ex As Exception
                sid = "20"
            End Try

            lblsid.Value = sid
            Try
                who = Request.QueryString("who").ToString
            Catch ex As Exception
                who = "lab"
            End Try
            lblwho.Value = who
            If who = "wo" Then
                wo = Request.QueryString("wo").ToString
                lblwonum.Value = wo
            ElseIf who = "jp" Then
                jpid = Request.QueryString("jpid").ToString
                lbljpid.Value = jpid
            ElseIf who = "pm" Or who = "pmmax" Then
                wo = Request.QueryString("wo").ToString
                lblwonum.Value = wo
                pmid = Request.QueryString("pmid").ToString
                lblpmid.Value = pmid
            End If

            If who <> "lab" Then
                cbur.Disabled = True
            End If

            dval.Open()
            GetSkill()
            dval.Dispose()
        Else
            If Request.Form("lblsubmit") = "all" Or Request.Form("lblsubmit") = "site" Then
                dval.Open()
                GetSkill()
                dval.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "savewo" Then
                lblsubmit.Value = ""
                dval.Open()
                savewo()
                dval.Dispose()
            ElseIf Request.Form("lblsubmit") = "savejp" Then
                lblsubmit.Value = ""
                dval.Open()
                savejp()
                dval.Dispose()
            ElseIf Request.Form("lblsubmit") = "savepmmax" Then
                lblsubmit.Value = ""
                dval.Open()
                savewo()
                savepm()
                dval.Dispose()
            End If

        End If
    End Sub
    Private Sub savewo()
        wo = lblwonum.Value
        sid = lblsid.Value
        Dim skillid, skill As String
        skillid = lblskillid.Value
        skill = lblskill.Value
        If wo <> "" Then
            Dim cmd As New SqlCommand
            cmd.CommandText = "update workorder set skillid = @skillid, " _
            + "skill = @skill " _
            + "where wonum = @wonum"
            Dim param = New SqlParameter("@wonum", SqlDbType.Int)
            param.Value = wo
            cmd.Parameters.Add(param)
            Dim param04 = New SqlParameter("@skillid", SqlDbType.VarChar)
            If skillid = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = skillid
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@skill", SqlDbType.VarChar)
            If skill = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = skill
            End If
            cmd.Parameters.Add(param05)


            dval.UpdateHack(cmd)
        End If
       
        lblsubmit.Value = "return"
    End Sub
    Private Sub savepm()
        pmid = lblpmid.Value
        Dim skillid, skill As String
        skillid = lblskillid.Value
        skill = lblskill.Value
        sql = "update pmmax set skillid = '" & skillid & "', skill = '" & skill & "' where pmid = '" & pmid & "'"
        dval.Update(sql)
    End Sub
    Private Sub savejp()
        jpid = lbljpid.Value
        sid = lblsid.Value
        Dim skillid, skill As String
        skillid = lblskillid.Value
        skill = lblskill.Value
        Dim cmd As New SqlCommand
        cmd.CommandText = "update pmjobplans set skillid = @skillid, " _
        + "skill = @skill " _
        + "where jpid = @jpid"
        Dim param = New SqlParameter("@jpid", SqlDbType.Int)
        param.Value = jpid
        cmd.Parameters.Add(param)
        Dim param04 = New SqlParameter("@skillid", SqlDbType.VarChar)
        If skillid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = skillid
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@skill", SqlDbType.VarChar)
        If skill = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = skill
        End If
        cmd.Parameters.Add(param05)


        dval.UpdateHack(cmd)
        lblsubmit.Value = "return"
    End Sub
    Private Sub GetSkill()
        Dim cid As String = "0"
        sid = lblsid.Value
        Dim sb As New StringBuilder
        Dim scnt, scnt2 As Integer
        If lblss.Value = "" Then
            sql = "select count(*) from pmSiteSkills where (compid = '" & cid & "' and siteid = '" & sid & "') and skillindex <> '0'"
            scnt = dval.Scalar(sql)
            lblss.Value = scnt
            If scnt > 1 Then
                rbsite.Disabled = False
                rbsite.Checked = True
            Else
                rbsite.Disabled = True
                rball.Checked = True
            End If
        End If


        scnt = lblss.Value

        If scnt > 1 And rbsite.Checked = True Then

            sql = "select count(*) from pmsiteskills where skill = 'Select' and compid = '" & cid & "'" ' and siteid = '" & sid & "'"

            scnt2 = dval.Scalar(sql)

            sql = "select s.skillid, s.skill, s.skillindex, isnull(s1.rate, 0) as 'rate', isnull(s1.otrate, 0) as orate " _
            + "from pmSiteSkills s " _
            + "left join pmSkills s1 on s1.skillid = s.skillid " _
            + "where (s.compid = '" & cid & "' and s.siteid = '" & sid & "') and s.skillindex <> '0' order by s.skillindex"


            dr = dval.GetRdrData(sql)
            sb.Append("<table>")
            While dr.Read
                vid = dr.Item("skillid").ToString
                val = dr.Item("skill").ToString
                rt = dr.Item("rate").ToString
                ort = dr.Item("orate").ToString
                vind = "0"
                sb.Append("<tr><td class=""plainlabel"">")
                sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "','" & rt & "','" & ort & "')"">")
                sb.Append(val & "</a>")
                sb.Append("</td></tr>")

            End While
            dr.Close()
            sb.Append("</table>")
            divval.InnerHtml = sb.ToString
        Else
            sql = "select skillid, skill, skillindex, isnull(rate, 0) as 'rate', isnull(otrate, 0) as orate " _
            + "from pmSkills where skill <> 'Select' and compid = '" & cid & "' and skillindex <> '0'" ' order by skillindex"
            dr = dval.GetRdrData(sql)
            sb.Append("<table>")
            While dr.Read
                vid = dr.Item("skillid").ToString
                val = dr.Item("skill").ToString
                rt = dr.Item("rate").ToString
                ort = dr.Item("orate").ToString
                vind = "0"
                sb.Append("<tr><td class=""plainlabel"">")
                sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "','" & rt & "','" & ort & "')"">")
                sb.Append(val & "</a>")
                sb.Append("</td></tr>")

            End While
            dr.Close()
            sb.Append("</table>")
            divval.InnerHtml = sb.ToString
        End If
    End Sub
End Class
