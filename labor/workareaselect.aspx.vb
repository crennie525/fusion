'********************************************************
'*
'********************************************************
Imports System.Data.SqlClient
Imports System.Text
Public Class workareaselect
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql As String
    Dim dval As New Utilities
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim intPgCnt, intPgNav As Integer
    Dim vid, vind, val, pmtskid, fld, sid, desc, loc, wo, typ As String
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang113 As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwa As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwaid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divval As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblss As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            sid = Request.QueryString("sid").ToString
            If sid = "" Then
                Try
                    sid = HttpContext.Current.Session("dfltps").ToString '"12"
                Catch ex As Exception

                End Try

            End If
            If typ = "wo" Then
                wo = Request.QueryString("wo").ToString
            Else
                wo = ""
            End If
            lblsid.Value = sid
            lbltyp.Value = typ
            lblwonum.Value = wo
            txtpg.Value = "1"
            dval.Open()
            GetWA(PageNumber)
            dval.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                dval.Open()
                GetNext()
                dval.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                dval.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetWA(PageNumber)
                dval.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                dval.Open()
                GetPrev()
                dval.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                dval.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetWA(PageNumber)
                dval.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "savewo" Then
                dval.Open()
                savewo()
                dval.Dispose()
                lblret.Value = ""
            End If

        End If
    End Sub
    Private Sub savewo()
        Dim wonum, waid, wa As String
        wonum = lblwonum.Value
        sid = lblsid.Value
        Dim err1, err2, err3 As String
        waid = lblwaid.Value
        wa = lblwa.Value
        Dim cmd As New SqlCommand
        cmd.CommandText = "update workorder set waid = @waid " _
        + "where wonum = @wonum"
        Dim param = New SqlParameter("@wonum", SqlDbType.Int)
        param.Value = wonum
        cmd.Parameters.Add(param)
        Dim param04 = New SqlParameter("@waid", SqlDbType.VarChar)
        If waid = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = waid
        End If
        cmd.Parameters.Add(param04)
        

        dval.UpdateHack(cmd)
        lblsubmit.Value = "return"
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetWA(PageNumber)
        Catch ex As Exception
            dval.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr102", "AppSetTaskTabST.aspx.vb")

            dval.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetWA(PageNumber)
        Catch ex As Exception
            dval.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr103", "AppSetTaskTabST.aspx.vb")

            dval.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetWA(ByVal PageNumber As Integer)
        Dim srch As String
        srch = txtsrch.Text
        srch = dval.ModString1(srch)
        sid = lblsid.Value
        If Len(srch) > 0 Then
            Filter = "(workarea like ''%" & srch & "%'' or wadesc like ''%" & srch & "%'') and siteid = ''" & sid & "''"
            FilterCnt = "(workarea like '%" & srch & "%' or wadesc like '%" & srch & "%') and siteid = '" & sid & "'"
        Else
            Filter = "siteid = ''" & sid & "''"
            FilterCnt = "siteid = '" & sid & "'"
        End If
        sql = "select count(*) " _
        + "from workareas where " & FilterCnt
        PageNumber = txtpg.Value
        intPgCnt = dval.Scalar(sql)
        intPgNav = dval.PageCount(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        Dim sb As New StringBuilder
        'sql = "select waid, workarea, wadesc, waloc, siteid " _
        '    + "from workareas where siteid = '" & sid & "'"
        'dr = dval.GetRdrData(sql)
        Tables = "workareas"
        PK = "waid"
        dr = dval.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)

        sb.Append("<table>")
        sb.Append("<tr>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""170"">Work Area</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""250"">Description</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""270"">Location</td>")
        sb.Append("</tr>")
        While dr.Read
            vid = dr.Item("waid").ToString
            val = dr.Item("workarea").ToString
            desc = dr.Item("wadesc").ToString
            loc = dr.Item("waloc").ToString
            vind = "0"
            sb.Append("<tr><td class=""plainlabel"" width=""170"">")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getval('" & vid & "','" & val & "','" & vind & "','" & desc & "','" & loc & "')"">")
            sb.Append(val & "</a>")
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"" width=""250"">" & desc & "</td>")
            sb.Append("<td class=""plainlabel"" width=""270"">" & loc & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divval.InnerHtml = sb.ToString
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        dval.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        GetWA(PageNumber)
        dval.Dispose()
    End Sub
End Class
