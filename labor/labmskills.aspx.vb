'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class labmskills
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim skill As New Utilities
    Dim cid, sid, sname, app, Login, ro, userid, skid, skil, muser As String
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lang3033 As System.Web.UI.WebControls.Label
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcur As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lang3034 As System.Web.UI.WebControls.Label
    Protected WithEvents lang3035 As System.Web.UI.WebControls.Label
    Protected WithEvents lbskillmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbsiteskills As System.Web.UI.WebControls.ListBox
    Protected WithEvents lang3036 As System.Web.UI.WebControls.Label
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lang3037 As System.Web.UI.WebControls.Label
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btntocomp.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                btntocomp.Enabled = False
                btnfromcomp.ImageUrl = "../images/appbuttons/minibuttons/backgraybg.gif"
                btnfromcomp.Enabled = False
                cbopts.Enabled = False
            End If
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            userid = Request.QueryString("userid").ToString
            lbluserid.Value = userid
            skid = Request.QueryString("skid").ToString
            lblskillid.Value = skid
            skil = Request.QueryString("skil").ToString
            tdskill.InnerHtml = skil
            muser = Request.QueryString("muser").ToString
            tdcur.InnerHtml = muser
            skill.Open()
            PopSkills()
            skill.Dispose()

        End If
    End Sub
    Private Sub PopSkills()
        sid = lblsid.Value
        userid = lbluserid.Value
        skid = lblskillid.Value
        sql = "select * from pmSkills where skillid not in " _
        + "(select w.skillid from womultiskills w where w.userid = '" & userid & "') " _
        + "and skillid <> '" & skid & "' and skill <> 'Select'"
        dr = skill.GetRdrData(sql)
        lbskillmaster.DataSource = dr
        lbskillmaster.DataValueField = "skillid"
        lbskillmaster.DataTextField = "skill"
        lbskillmaster.DataBind()
        dr.Close()
        Try
            lbskillmaster.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopSiteSkills()
        sid = lblsid.Value
        userid = lbluserid.Value
        sql = "select m.skillid, s.skill from womultiskills m left join pmskills s on s.skillid = m.skillid " _
        + "where m.userid = '" & userid & "'"
        dr = skill.GetRdrData(sql)
        lbsiteskills.DataSource = dr
        lbsiteskills.DataValueField = "skillid"
        lbsiteskills.DataTextField = "skill"
        lbsiteskills.DataBind()
        dr.Close()
        Try
            lbsiteskills.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        sid = lblsid.Value
        Dim Item As ListItem
        Dim s, ss As String
        skill.Open()
        For Each Item In lbskillmaster.Items
            If Item.Selected Then
                ss = Item.ToString
                s = Item.Value.ToString
                GetItems(s, ss)
            End If
        Next
        PopSiteSkills()
        PopSkills()
        skill.Dispose()
    End Sub
    Private Sub GetItems(ByVal skillid As String, ByVal skillstr As String)
        sid = lblsid.Value
        userid = lbluserid.Value
        Dim fcnt As Integer
        sql = "select count(*) from womultiskills where userid = '" & userid & "' and skillid = '" & skillid & "'"
        fcnt = skill.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into womultiskills (skillid, userid) values ('" & skillid & "', " _
            + "'" & userid & "')"
            skill.Update(sql)
        End If
    End Sub
    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        sid = lblsid.Value
        Dim Item As ListItem
        Dim s As String
        skill.Open()
        For Each Item In lbsiteskills.Items
            If Item.Selected Then
                s = Item.Value.ToString
                RemItems(s)
            End If
        Next
        PopSiteSkills()
        PopSkills()
        skill.Dispose()
    End Sub
    Private Sub RemItems(ByVal skillid As String)
        sid = lblsid.Value
        userid = lbluserid.Value
        sql = "delete from womultiskills where skillid = '" & skillid & "' and userid = '" & userid & "'"
        skill.Update(sql)
    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbskillmaster.AutoPostBack = False
            lbsiteskills.AutoPostBack = False
        Else
            lbskillmaster.AutoPostBack = True
            lbsiteskills.AutoPostBack = True
        End If
    End Sub
End Class
