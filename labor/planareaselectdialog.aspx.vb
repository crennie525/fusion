Public Class planareaselectdialog
    Inherits System.Web.UI.Page
    Dim typ, sid, wo As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iffreq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            sid = Request.QueryString("sid").ToString
            If typ = "wo" Then
                wo = Request.QueryString("wo").ToString
            Else
                wo = ""
            End If


            iffreq.Attributes.Add("src", "planareaselect.aspx?typ=" & typ & "&sid=" & sid & "&wo=" & wo & "&date=" & Now)

        End If
    End Sub

End Class
