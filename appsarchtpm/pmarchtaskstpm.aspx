<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmarchtaskstpm.aspx.vb" Inherits="lucy_r12.pmarchtaskstpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pmarchtaskstpm</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/pmtaskdivfuncarch.js"></script>
		<script language="JavaScript" src="../scripts1/pmarchtaskstpmaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkit();" >
		<div id="overDiv" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute"></div>
		<form id="form1" method="post" runat="server">
			<div class="FreezePaneOff" id="FreezePane" align="center">
				<div class="InnerFreezePane" id="InnerFreezePane"></div>
			</div>
			<div style="Z-INDEX: 100; LEFT: 0px; POSITION: absolute; TOP: 0px">
				<table cellSpacing="0" cellPadding="1" width="742">
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/archtop.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('Against what is the TPM intended to protect?', ABOVE, LEFT)"
							onmouseout="return nd()" width="696"><asp:Label id="lang447" runat="server">Failure Modes, Causes And/Or Effects</asp:Label></td>
					</tr>
				</table>
				<table id="tbeq" cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td class="label" width="160" height="24"><asp:Label id="lang448" runat="server">Component Addressed:</asp:Label></td>
						<td class="plainlabel" id="tdcomp" width="242" runat="server"></td>
						<td class="label" width="30"><asp:Label id="lang449" runat="server">Qty:</asp:Label></td>
						<td class="plainlabel" id="tdcqty" width="282" runat="server"></td>
						<td class="label" width="28"></td>
					</tr>
				</table>
				<table id="tbeq2" cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td width="365">
							<table cellSpacing="0" cellPadding="0">
								<tr>
									<td width="20"></td>
									<td width="170"></td>
									<td width="23"></td>
									<td width="5"></td>
									<td width="195"></td>
									<td width="20"></td>
								</tr>
								<tr>
									<td class="transrowgray">&nbsp;</td>
									<td class="label transrowgray" align="center" colSpan="1"><asp:Label id="lang450" runat="server">Original Failure Modes</asp:Label></td>
									<td class="transrowgray"></td>
									<td>&nbsp;</td>
									<td class="bluelabel transrowdrk" align="center" colSpan="1"><asp:Label id="lang451" runat="server">Revised Failure Modes</asp:Label></td>
									<td class="transrowdrk">&nbsp;</td>
								</tr>
								<tr>
									<td class="transrowgray">&nbsp;</td>
									<td align="center" class="transrowgray"><asp:listbox id="lbofailmodes" runat="server" SelectionMode="Multiple" Height="50px" ForeColor="Blue"
											CssClass="plainlabel" Width="150px"></asp:listbox></td>
									<td vAlign="middle" align="center" class="transrowgray"></td>
									<td>&nbsp;</td>
									<td align="center" class="transrowdrk"><asp:listbox id="lbCompFM" runat="server" SelectionMode="Multiple" Height="50px" CssClass="plainlabel"
											Width="150px"></asp:listbox></td>
									<td vAlign="middle" align="center" class="transrowdrk"></td>
								</tr>
							</table>
						</td>
						<td width="377">
							<table cellSpacing="0" cellPadding="0">
								<tr>
									<td width="150"></td>
									<td width="20"></td>
									<td width="150"></td>
									<td width="47"></td>
									<td width="10"></td>
								</tr>
								<tr>
									<td class="redlabel transrowdrk" align="center"><asp:Label id="lang452" runat="server">Not Addressed</asp:Label></td>
									<td class="transrowdrk">&nbsp;</td>
									<td class="bluelabel transrowdrk" align="center"><asp:Label id="lang453" runat="server">Selected</asp:Label></td>
									<td class="transrowdrk">&nbsp;</td>
									<td class="transrowdrk">&nbsp;</td>
								</tr>
								<tr>
									<td align="center" class="transrowdrk"><asp:listbox id="lbfaillist" runat="server" SelectionMode="Multiple" Height="50px" ForeColor="Red"
											CssClass="plainlabel" Width="150px"></asp:listbox></td>
									<td vAlign="middle" align="center" class="transrowdrk"></td>
									<td align="center" class="transrowdrk"><asp:listbox id="lbfailmodes" runat="server" SelectionMode="Multiple" Height="50px" ForeColor="Blue"
											CssClass="plainlabel" Width="150px"></asp:listbox></td>
									<td class="transrowdrk">&nbsp;</td>
									<td class="transrowdrk">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/archtop.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('What are you going to do?')"
							onmouseout="return nd()" width="684" colSpan="3"><asp:Label id="lang454" runat="server">Task Activity Details</asp:Label></td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td>
							<table cellSpacing="0" cellPadding="1" width="372">
								<tr>
									<td width="30"></td>
									<td width="140"></td>
									<td width="30"></td>
									<td width="140"></td>
									<td width="2"></td>
								</tr>
								<tr>
									<td class="label transrowgray" colSpan="4"><asp:Label id="lang455" runat="server">Original Task Description</asp:Label></td>
									<td></td>
								</tr>
								<tr>
									<td class="transrowgray" colSpan="4"><TEXTAREA id="txtodesc" onkeyup="copydesc(this.value);" style="FONT-SIZE: 12px; WIDTH: 296px; FONT-FAMILY: Arial; HEIGHT: 40px"
											name="txtdesc" rows="2" readOnly cols="34" onchange="copydesc(this.value);" runat="server"></TEXTAREA></td>
									<td></td>
								</tr>
								<tr height="24">
									<td class="label transrowgray"><asp:Label id="lang456" runat="server">Type</asp:Label></td>
									<td class="plainlabel transrowgray" id="tdtypeo" runat="server"></td>
									<td class="label transrowgray"></td>
									<td class="transrowgray"></td>
									<td><IMG src="../images/appbuttons/minibuttons/2PX.gif"></td>
								</tr>
							</table>
						</td>
						<td>
							<table cellSpacing="0" cellPadding="1" width="370">
								<tr>
									<td width="30"></td>
									<td width="140"></td>
									<td width="30"></td>
									<td width="90"></td>
									<td width="50"></td>
								</tr>
								<tr>
									<td class="label transrowdrk" colSpan="5"><asp:Label id="lang457" runat="server">Revised Task Description</asp:Label></td>
									<td class="label transrowdrk"></td>
								</tr>
								<tr>
									<td class="transrowdrk" colSpan="5"><TEXTAREA class="plainlabel" id="txtdesc" style="FONT-SIZE: 12px; WIDTH: 290px; FONT-FAMILY: Arial; HEIGHT: 40px"
											name="txtdesc" rows="2" readOnly cols="32" runat="server"></TEXTAREA></td>
									<td class="transrowdrk"><br>
										<IMG onmouseover="return overlib('Add/Edit Measurements for this Task')" onclick="getMeasDiv();"
											onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/measblu.gif"
											width="27"></td>
								</tr>
								<tr height="24">
									<td class="label transrowdrk"><asp:Label id="lang458" runat="server">Type</asp:Label></td>
									<td class="plainlabel transrowdrk" id="tdtype" runat="server"></td>
									<td class="label transrowdrk"></td>
									<td class="transrowdrk" colSpan="3"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="1" width="742">
					<tr>
						<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/archtop.gif" border="0"></td>
						<td class="thdrsingrt label" onmouseover="return overlib('How are you going to do it?')"
							onmouseout="return nd()" width="554" colSpan="3"><asp:Label id="lang459" runat="server">Planning &amp; Scheduling Details</asp:Label></td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="742">
					<tr>
						<td vAlign="top">
							<table cellSpacing="0" cellPadding="0" width="372">
								<tr height="24">
									<td class="label transrowgray">Qty</td>
									<td class="plainlabel transrowgray" id="tdqtyo" runat="server"></td>
									<td class="label transrowgray"><asp:Label id="lang460" runat="server">Min Ea</asp:Label></td>
									<td class="plainlabel transrowgray" id="tdtro" runat="server"></td>
									<td class="label transrowgray"><asp:Label id="lang461" runat="server">Status</asp:Label></td>
									<td class="plainlabel transrowgray" id="tdeqstato" runat="server"></td>
									<td class="label transrowgray">DT</td>
									<td class="plainlabel transrowgray" id="tdrdto" runat="server"></td>
								</tr>
								<tr>
									<td colSpan="3" class="transrowgray" valign="top">
										<table cellSpacing="0" cellPadding="0">
											<tr height="20">
												<td class="label"><asp:Label id="lang462" runat="server">Frequency</asp:Label></td>
											</tr>
											<tr height="20">
												<td class="plainlabel" id="tdfreqo" runat="server">&nbsp;</td>
											</tr>
										</table>
									</td>
									<td colSpan="5" class="transrowgray">
										<table cellSpacing="0" cellPadding="0">
											<tr>
												<td class="label" onmouseover="return overlib('Use to indicate shift - Clears and Disables any Day Enries')"
													onmouseout="return nd()" align="center">SO</td>
												<td class="label" onmouseover="return overlib('Selects All Days for Selected Shift')"
													onmouseout="return nd()" align="center"><asp:Label id="lang463" runat="server">All</asp:Label></td>
												<td class="label"></td>
												<td class="label" align="center" width="24">Mon</td>
												<td class="label" align="center" width="24">Tue</td>
												<td class="label" align="center" width="24">Wed</td>
												<td class="label" align="center" width="24">Thu</td>
												<td class="label" align="center" width="24">Fri</td>
												<td class="label" align="center" width="24">Sat</td>
												<td class="label" align="center" width="24">Sun</td>
											</tr>
											<tr>
												<td><INPUT id="ocb1o" type="checkbox" name="cb1o" runat="server"></td>
												<td><INPUT id="ocb1" type="checkbox" name="cb1" runat="server"></td>
												<td class="label">1st</td>
												<td align="center"><INPUT id="ocb1mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="ocb1tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="ocb1wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="ocb1thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="ocb1fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="ocb1sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="ocb1sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
											<tr>
												<td><INPUT id="ocb2o" type="checkbox" name="cb1o" runat="server"></td>
												<td><INPUT id="ocb2" type="checkbox" name="Checkbox1" runat="server"></td>
												<td class="label">2nd</td>
												<td align="center"><INPUT id="ocb2mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="ocb2tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="ocb2wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="ocb2thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="ocb2fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="ocb2sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="ocb2sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
											<tr>
												<td><INPUT id="ocb3o" type="checkbox" name="cb1o" runat="server"></td>
												<td><INPUT id="ocb3" type="checkbox" name="Checkbox1" runat="server"></td>
												<td class="label">3rd</td>
												<td align="center"><INPUT id="ocb3mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="ocb3tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="ocb3wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="ocb3thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="ocb3fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="ocb3sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="ocb3sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr class="tbg" height="24">
									<td class="bluelabel" colspan="4"><asp:Label id="lang464" runat="server">Task Status</asp:Label></td>
									<td class="plainlabel" id="tdtaskstat" runat="server" colspan="4"></td>
								</tr>
							</table>
						</td>
						<td vAlign="top">
							<table cellSpacing="0" cellPadding="0" width="370">
								<tr height="24">
									<td class="label transrowdrk">Qty</td>
									<td class="plainlabel transrowdrk" id="tdqty" runat="server"></td>
									<td class="label transrowdrk"><asp:Label id="lang465" runat="server">Min Ea</asp:Label></td>
									<td class="plainlabel transrowdrk" id="tdtr" runat="server"></td>
									<td class="label transrowdrk"><asp:Label id="lang466" runat="server">Status</asp:Label></td>
									<td class="plainlabel transrowdrk" id="tdeqstat" runat="server"></td>
									<td class="label transrowdrk" style="WIDTH: 19px">DT</td>
									<td class="plainlabel transrowdrk" id="tdrdt" runat="server"></td>
								</tr>
								<tr>
									<td colSpan="3" class="transrowdrk" valign="top">
										<table cellSpacing="0" cellPadding="0">
											<tr height="18">
												<td class="label"><asp:Label id="lang467" runat="server">Frequency</asp:Label></td>
											</tr>
											<tr height="18">
												<td class="plainlabel" id="tdfreq" runat="server">&nbsp;</td>
											</tr>
											<tr height="18">
												<td class="label"><asp:Label id="lang468" runat="server">P-F Interval</asp:Label></td>
											</tr>
											<tr height="18">
												<td class="plainlabel" id="tdpfint" runat="server">&nbsp;</td>
											</tr>
										</table>
									</td>
									<td colSpan="5" class="transrowdrk" valign="top">
										<table cellSpacing="0" cellPadding="0">
											<tr>
												<td class="label" onmouseover="return overlib('Use to indicate shift - Clears and Disables any Day Enries')"
													onmouseout="return nd()" align="center">SO</td>
												<td class="label" onmouseover="return overlib('Selects All Days for Selected Shift')"
													onmouseout="return nd()" align="center"><asp:Label id="lang469" runat="server">All</asp:Label></td>
												<td class="label"></td>
												<td class="label" align="center" width="24">Mon</td>
												<td class="label" align="center" width="24">Tue</td>
												<td class="label" align="center" width="24">Wed</td>
												<td class="label" align="center" width="24">Thu</td>
												<td class="label" align="center" width="24">Fri</td>
												<td class="label" align="center" width="24">Sat</td>
												<td class="label" align="center" width="24">Sun</td>
											</tr>
											<tr>
												<td><INPUT id="cb1o" type="checkbox" name="cb1o" runat="server"></td>
												<td><INPUT id="cb1" type="checkbox" name="cb1" runat="server"></td>
												<td class="label">1st</td>
												<td align="center"><INPUT id="cb1mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb1tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb1wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb1thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb1fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb1sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb1sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
											<tr>
												<td><INPUT id="cb2o" type="checkbox" name="cb2o" runat="server"></td>
												<td><INPUT id="cb2" type="checkbox" name="Checkbox1" runat="server"></td>
												<td class="label">2nd</td>
												<td align="center"><INPUT id="cb2mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb2tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb2wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb2thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb2fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb2sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb2sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
											<tr>
												<td><INPUT id="cb3o" type="checkbox" name="cb3o" runat="server"></td>
												<td><INPUT id="cb3" type="checkbox" name="Checkbox1" runat="server"></td>
												<td class="label">3rd</td>
												<td align="center"><INPUT id="cb3mon" type="checkbox" name="Checkbox1" runat="server"></td>
												<td align="center"><INPUT id="cb3tue" type="checkbox" name="Checkbox2" runat="server"></td>
												<td align="center"><INPUT id="cb3wed" type="checkbox" name="Checkbox3" runat="server"></td>
												<td align="center"><INPUT id="cb3thu" type="checkbox" name="Checkbox4" runat="server"></td>
												<td align="center"><INPUT id="cb3fri" type="checkbox" name="Checkbox5" runat="server"></td>
												<td align="center"><INPUT id="cb3sat" type="checkbox" name="Checkbox6" runat="server"></td>
												<td align="center"><INPUT id="cb3sun" type="checkbox" name="Checkbox7" runat="server"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr class="tbg">
									<td class="labelsm" colSpan="4"><asp:checkbox id="cbloto" runat="server"></asp:checkbox><asp:Label id="lang470" runat="server">LOTO</asp:Label><asp:checkbox id="cbcs" runat="server"></asp:checkbox>CS</td>
									<td class="bluelabel" align="right" colSpan="4"><IMG onmouseover="return overlib('Choose Equipment Spare Parts for this Task')" onclick="GetSpareDiv();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/spareparts.gif"><IMG id="Img3" onmouseover="return overlib('Add/Edit Parts for this Task')" onclick="GetPartDiv();"
											onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/parttrans.gif" width="23" runat="server"><IMG onmouseover="return overlib('Add/Edit Tools for this Task')" onclick="GetToolDiv();"
											onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/tooltrans.gif" width="23"><IMG onmouseover="return overlib('Add/Edit Lubricants for this Task')" onclick="GetLubeDiv();"
											onmouseout="return nd()" height="19" alt="" src="../images/appbuttons/minibuttons/lubetrans.gif" width="23"><IMG onmouseover="return overlib('Add/Edit Notes for this Task')" onclick="GetNoteDiv();"
											onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/notessm.gif" width="25">
										<IMG id="Img5" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - This Task', ABOVE, LEFT)"
											onclick="editadj('0');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwd.gif"
											border="0" runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="742" border="0">
					<tr id="Tr1" runat="server" NAME="Tr1">
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="center"><IMG id="btnStart" height="20" src="../images/appbuttons/minibuttons/tostartbg.gif" width="20"
								runat="server"><IMG id="btnPrev" height="20" src="../images/appbuttons/minibuttons/prevarrowbg.gif"
								width="20" runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="center" height="30"><asp:label id="Label22" runat="server" ForeColor="Blue" CssClass="bluelabel" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small">Task# </asp:label><asp:label id="lblpg" runat="server" ForeColor="Blue" CssClass="bluelabel" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label><asp:label id="Label23" runat="server" ForeColor="Blue" CssClass="bluelabel" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"> of </asp:label><asp:label id="lblcnt" runat="server" ForeColor="Blue" CssClass="bluelabel" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="center"><IMG id="btnNext" height="20" src="../images/appbuttons/minibuttons/nextarrowbg.gif"
								width="20" runat="server"><IMG id="btnEnd" height="20" src="../images/appbuttons/minibuttons/tolastbg.gif" width="20"
								runat="server">
						</td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="center"><asp:label id="Label26" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
								Font-Size="X-Small">Sub Tasks: </asp:label><asp:label id="lblsubcount" runat="server" CssClass="greenlabel" Font-Bold="True" Font-Names="Arial"
								Font-Size="X-Small"></asp:label><asp:label id="lblspg" runat="server" ForeColor="Green" CssClass="details" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label><asp:label id="Label27" runat="server" ForeColor="Green" CssClass="details" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"> of </asp:label><asp:label id="lblscnt" runat="server" ForeColor="Green" CssClass="details" Font-Bold="True"
								Font-Names="Arial" Font-Size="X-Small"></asp:label></td>
						<td style="BORDER-TOP: #7ba4e0 thin solid" align="right">&nbsp;<IMG onclick="GetNavGrid();" height="20" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
								width="20">
							<asp:imagebutton id="btnaddsubtask" runat="server" CssClass="details" ImageUrl="../images/appbuttons/minibuttons/subtask.gif"></asp:imagebutton><IMG id="imgrat" onmouseover="return overlib('Add/Edit Rationale for Task Changes')"
								onclick="GetRationale();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/rationale.gif" width="20" runat="server"><IMG id="sgrid" onmouseover="return overlib('Add/Edit Sub Tasks')" onclick="getsgrid();"
								onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/sgrid1.gif" width="20">
							<IMG id="Img4" onmouseover="return overlib('Edit Pass/Fail Adjustment Levels - All Tasks', ABOVE, LEFT)"
								onclick="editadj('pre');" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/screwall.gif"
								border="0" runat="server"></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td width="45"></td>
						<td width="120"></td>
						<td width="45"></td>
						<td width="90"></td>
						<td width="367"></td>
					</tr>
				</table>
			</div>
			<input id="lblsvchk" type="hidden" name="lblsvchk" runat="server"> <input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
			<input id="lblsb" type="hidden" name="lblsb" runat="server"> <input id="lblfilt" type="hidden" name="lblfilt" runat="server">
			<input id="lblenable" type="hidden" name="lblenable" runat="server"> <input id="lblpgholder" type="hidden" name="lblpgholder" runat="server">
			<input id="lblcompfailchk" type="hidden" name="lblcompfailchk" runat="server"> <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
			<input id="lbltasklev" type="hidden" name="lbltasklev" runat="server"> <input id="lblcid" type="hidden" name="lblcid" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblstart" type="hidden" name="lblpmtype" runat="server">
			<input id="lblt" type="hidden" name="lblt" runat="server"> <input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lblst" type="hidden" name="lblst" runat="server"> <input id="lblco" type="hidden" name="lblco" runat="server">
			<input id="lblpar" type="hidden" name="lblpar" runat="server"> <input id="lbltabid" type="hidden" name="lbltabid" runat="server">
			<input id="lblpmstr" type="hidden" name="lblpmstr" runat="server"> <input id="pgflag" type="hidden" name="pgflag" runat="server">
			<input id="lblpmid" type="hidden" name="lblpmid" runat="server"> <input id="lbldid" type="hidden" name="lbldid" runat="server">
			<input id="lblclid" type="hidden" name="lblclid" runat="server"><input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbldel" type="hidden" name="lbldel" runat="server"><input id="lblsave" type="hidden" name="lblsave" runat="server">
			<input id="lblgrid" type="hidden" name="lblgrid" runat="server"><input id="lblcind" type="hidden" name="lblcind" runat="server">
			<input id="lblcurrsb" type="hidden" name="lblcurrsb" runat="server"><input id="lblcurrcs" type="hidden" name="lblcurrcs" runat="server">
			<input id="lbldocpmid" type="hidden" name="lbldocpmid" runat="server"><input id="lbldocpmstr" type="hidden" name="lbldocpmstr" runat="server">
			<input id="lblsesscnt" type="hidden" name="lblsesscnt" runat="server"><input id="appchk" type="hidden" name="appchk" runat="server">
			<input id="lbllog" type="hidden" name="lbllog" runat="server"><input id="lblfiltcnt" type="hidden" name="lblfiltcnt" runat="server">
			<input id="lblusername" type="hidden" name="lblusername" runat="server"> <input id="lbllock" type="hidden" name="lbllock" runat="server">
			<input id="lbllockedby" type="hidden" name="lbllockedby" runat="server"> <input id="lblhaspm" type="hidden" name="lblhaspm" runat="server">
			<input id="lbltyp" type="hidden" name="lbltyp" runat="server"> <input id="lbllid" type="hidden" name="lbllid" runat="server">
			<input id="lblnoeq" type="hidden" name="lblnoeq" runat="server"> <input id="lblro" type="hidden" name="lblro" runat="server">
			<input type="hidden" id="lblrev" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		</TR></TBODY></TABLE></FORM>
	</body>
</HTML>
