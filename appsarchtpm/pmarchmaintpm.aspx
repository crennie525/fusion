<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmarchmaintpm.aspx.vb" Inherits="lucy_r12.pmarchmaintpm" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>TPM Archive (Read Only)</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="javascript" src="../scripts/taskgrid.js"></script>
		<script language="JavaScript" src="../scripts1/pmarchmaintpmaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkarch();"  class="tbg">
		<form id="form1" method="post" runat="server">
			<table id="tblopt" style="Z-INDEX: 1; LEFT: 0px; POSITION: absolute; TOP: 70px" cellSpacing="0"
				cellPadding="0" width="1010">
				<tr>
					<td id="tdqs" runat="server" colspan="2"></td>
				</tr>
				<tr>
					<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/archtop.gif" border="0" id="imgopt" runat="server"></td>
					<td class="thdrsingrt label" align="left" width="716" id="tdloctop"><asp:Label id="lang444" runat="server">Location/Equipment/Function Details</asp:Label></td>
					<td width="3">&nbsp;</td>
					<td class="thdrsinglft" id="tdnavtop" width="22"><IMG src="../images/appbuttons/minibuttons/eqarch.gif" border="0"></td>
					<td class="thdrsingrt label" id="tdnavtoprt" align="left" width="268"><font class="label"><asp:Label id="lang445" runat="server">Asset Hierarchy</asp:Label></font></td>
				</tr>
				<tr>
					<td colSpan="2" valign="top">
						<table cellSpacing="0" cellPadding="0" id="tbopt" runat="server">
							<tr>
								<td><iframe id="geteq" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="pmarchgettpm2.aspx?tli=5&amp;jump=no" frameBorder="no" width="746" scrolling="no"
										height="102" runat="server" allowtransparency></iframe>
								</td>
							</tr>
							<tr>
								<td><iframe id="iftaskdet" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="pmarchgridtpm.aspx?start=no" frameBorder="no" width="746" scrolling="no" height="424"
										runat="server" allowtransparency></iframe>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td class="view" id="tdnav" colSpan="2">
						<table cellSpacing="0">
							<tr>
								<td colSpan="2"><iframe id="ifarch" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="pmarchtpm.aspx?start=no" frameBorder="no" width="270" scrolling="no" height="200" runat="server"
										allowtransparency></iframe>
								</td>
							</tr>
							<tr>
								<td class="thdrsinglft" width="22"><IMG src="../images/appbuttons/minibuttons/eqarch.gif" border="0"></td>
								<td class="thdrsingrt label" width="270" allowtransparency><asp:Label id="lang446" runat="server">Asset Images</asp:Label></td>
							</tr>
							<tr>
								<td colSpan="2"><iframe id="ifimg" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-TOP-STYLE: none; PADDING-TOP: 0px; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-BOTTOM-STYLE: none"
										src="../tpmpics/taskimagetpmarch.aspx?eqid=0" frameBorder="no" width="270" scrolling="no" height="276"
										runat="server"></iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lbltab" type="hidden" name="lbltab" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"> <input id="lbldid" type="hidden" name="lbldid" runat="server">
			<input id="lblclid" type="hidden" name="lblclid" runat="server"> <input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="lblfuid" type="hidden" name="lblfuid" runat="server"> <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lblgetarch" type="hidden" name="lblgetarch" runat="server">
			<input type="hidden" id="lbltyp" runat="server" NAME="lbltyp"> <input type="hidden" id="lbllid" runat="server" NAME="lbllid">

			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="lbldchk" type="hidden" name="lbldchk" runat="server">
			<input id="lbltaskid" type="hidden" name="lbltaskid" runat="server"> <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
			<input id="tasknum" type="hidden" name="tasknum" runat="server"> <input id="taskcnt" type="hidden" name="taskcnt" runat="server">
			<input type="hidden" id="retqs" runat="server" NAME="retqs"> <input type="hidden" id="lblsave" runat="server" NAME="lblsave">
			<input type="hidden" id="lblsavetasks" runat="server" NAME="lblsavetasks"><input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="lblrev" runat="server" NAME="lblrev">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
					<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
