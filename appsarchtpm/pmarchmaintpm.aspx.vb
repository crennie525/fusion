

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmarchmaintpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang446 As System.Web.UI.WebControls.Label

	Protected WithEvents lang445 As System.Web.UI.WebControls.Label

	Protected WithEvents lang444 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim login As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim main As New Utilities

    Dim tasklev, sid, did, clid, eqid, fuid, coid, chk, tli, cid, jump, typ, lid, start, ro, rev As String
    Protected WithEvents tdqs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgopt As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tbopt As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iftaskdet As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifimg As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetarch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents taskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents retqs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsavetasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("PM3OptMain.aspx?jump=no")
        Catch ex As Exception

        End Try
        'sid = Request.Form("lblps")
        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                'If ro = "1" Then
                'pgtitle.InnerHtml = "PM Optimizer (Read Only)"
                'Else
                '    pgtitle.InnerHtml = "PM Optimizer"
                'End If
                start = Request.QueryString("start").ToString
                If start = "yes" Then
                    Dim qs As String = Request.QueryString.ToString
                    retqs.Value = qs
                    Dim lvl As String = Request.QueryString("lvl").ToString
                    If lvl = "eq" Then
                        tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Equipment Screen</a>"
                    ElseIf lvl = "fu" Then
                        tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Function Screen</a>"
                    ElseIf lvl = "co" Then
                        tdqs.InnerHtml = "<a href='../equip/eqmain2.aspx?" & qs & "' class='A1'>Return to Component Screen</a>"
                    End If

                End If
            Catch ex As Exception

            End Try
            'Try
            jump = Request.QueryString("jump").ToString
            If jump = "yes" Then

                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                Try
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                Catch ex As Exception

                End Try

                tli = Request.QueryString("tli").ToString
                lbltasklev.Value = "5" 'tli
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                rev = Request.QueryString("rev").ToString
                lblrev.Value = rev
                Try
                    fuid = Request.QueryString("funid").ToString
                    lblfuid.Value = fuid
                    coid = Request.QueryString("comid").ToString
                    lblcoid.Value = coid
                Catch ex As Exception

                End Try
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                lblgetarch.Value = "yes"
                geteq.Attributes.Add("src", "pmarchgettpm2.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" & lid & "&typ=" & typ & "&rev=" & rev)
            End If
            'Catch ex As Exception
            'Response.Redirect("../NewLogin.aspx")
            'End Try
        Else
            If Page.Request("lblgetarch") = "eq" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "4"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                rev = lblrev.Value
                geteq.Attributes.Add("src", "pmarchgettpm2.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ & "&rev=" & rev)
            ElseIf Page.Request("lblgetarch") = "fu" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "5"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = "" 'lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                rev = lblrev.Value
                geteq.Attributes.Add("src", "pmarchgettpm2.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ & "&rev=" & rev)
            ElseIf Page.Request("lblgetarch") = "co" Then
                lblgetarch.Value = "yes"
                cid = lblcid.Value
                tli = "5"
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                coid = lblcoid.Value
                chk = lblchk.Value
                typ = lbltyp.Value
                lid = lbllid.Value
                rev = lblrev.Value
                geteq.Attributes.Add("src", "pmarchgettpm2.aspx?jump=yes&cid=" & cid & "&tli=" & tli & "&sid=" & sid & "&did=" & did & "&eqid=" & eqid & "&clid=" & clid & "&funid=" & fuid & "&comid=" & coid + "&lid=" + lid + "&typ=" + typ & "&rev=" & rev)
            End If

        End If


    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang444.Text = axlabs.GetASPXPage("pmarchmaintpm.aspx", "lang444")
        Catch ex As Exception
        End Try
        Try
            lang445.Text = axlabs.GetASPXPage("pmarchmaintpm.aspx", "lang445")
        Catch ex As Exception
        End Try
        Try
            lang446.Text = axlabs.GetASPXPage("pmarchmaintpm.aspx", "lang446")
        Catch ex As Exception
        End Try

    End Sub

End Class
