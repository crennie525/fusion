<%@ Page Language="vb" AutoEventWireup="false" Codebehind="archrationaletpm2.aspx.vb" Inherits="lucy_r12.archrationaletpm2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>archrationaletpm2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/archrationaletpm2aspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="handleexit();" >
		<form id="form1" method="post" runat="server">
			<div style="LEFT: 8px; POSITION: absolute; TOP: 8px">
				<table width="800" cellSpacing="1" cellPadding="1">
					<tr>
						<td class="bluelabel" width="80"></td>
						<td width="230" runat="server" ID="Td1" NAME="Td1"></td>
						<td class="bluelabel" width="80"></td>
						<td class="label" width="240"></td>
						<td class="label" width="200"></td>
					</tr>
					<tr height="20">
						<td class="bluelabel"><asp:Label id="lang397" runat="server">Department</asp:Label></td>
						<td class="label" id="tddept" runat="server"></td>
						<td class="bluelabel"><asp:Label id="lang398" runat="server">Station/Cell</asp:Label></td>
						<td class="label" id="tdcell" runat="server"></td>
					</tr>
					<tr height="20">
						<td class="bluelabel"><asp:Label id="lang399" runat="server">Equipment</asp:Label></td>
						<td class="label" id="tdeq" runat="server"></td>
						<td class="bluelabel"><asp:Label id="lang400" runat="server">Function</asp:Label></td>
						<td class="label" id="tdfunc" runat="server"></td>
					</tr>
					<tr height="20">
						<td class="bluelabel"><asp:Label id="lang401" runat="server">Task#</asp:Label></td>
						<td class="label" id="tdtasknum" runat="server"></td>
						<td class="bluelabel"><asp:Label id="lang402" runat="server">Component</asp:Label></td>
						<td class="label" id="tdcomp" runat="server"></td>
						<td class="label" align="right"><asp:imagebutton cssclass="details" id="ibsvrtn" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:imagebutton>
							<img class="details" src="../images/appbuttons/minibuttons/printx.gif" onclick="getreport();"
								onmouseover="return overlib('Print PMO Task Rationale Report', ABOVE, LEFT)" onmouseout="return nd()"></td>
					</tr>
				</table>
				<TABLE cellSpacing="1" cellPadding="1" width="800" class="tbg">
					<tr>
						<td class="thdrsing label" width="120"><asp:Label id="lang403" runat="server">Task Detail</asp:Label></td>
						<td class="thdrsing label" width="220"><asp:Label id="lang404" runat="server">Original</asp:Label></td>
						<td class="thdrsing label" width="220"><asp:Label id="lang405" runat="server">Revised</asp:Label></td>
						<td class="thdrsing label" width="240"><asp:Label id="lang406" runat="server">Rational</asp:Label></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang407" runat="server">Failure Modes</asp:Label></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbofm" runat="server" CssClass="plainlabel" Width="210px" Height="52px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbfm" runat="server" CssClass="plainlabel" Width="210px" Height="52px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr" vAlign="top"><TEXTAREA class="plainlabel" id="txtfm" style="WIDTH: 230px; HEIGHT: 40px" name="txtfm" rows="3"
								cols="20" runat="server"></TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang408" runat="server">Task Description</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdotd" runat="server"><TEXTAREA class="plainlabel" id="txtodesc" style="WIDTH: 210px; HEIGHT: 40px" name="Textarea1"
								rows="3" cols="20" runat="server">						</TEXTAREA></td>
						<td class="labellt cellbrdrcntr" id="tdtd" runat="server"><TEXTAREA class="plainlabel" id="txtdesc" style="WIDTH: 210px; HEIGHT: 40px" name="Textarea1"
								rows="3" cols="20" runat="server">						</TEXTAREA></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txttd" style="WIDTH: 230px; HEIGHT: 40px" name="txttd" rows="3"
								cols="20" runat="server">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang409" runat="server">Task Type</asp:Label></td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdott" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdtt" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txttt" style="WIDTH: 230px; HEIGHT: 40px" name="txttt" rows="3"
								cols="20" runat="server">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang410" runat="server">Labor Minutes Each</asp:Label></td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdolm" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdlm" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txtlm" style="WIDTH: 230px; HEIGHT: 40px" name="txtlm" rows="3"
								cols="20" runat="server">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang411" runat="server">Frequency</asp:Label></td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdofreq" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdfreq" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txtfr" style="WIDTH: 230px; HEIGHT: 40px" name="txtfr" rows="3"
								cols="20" runat="server">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang412" runat="server">EQ Status</asp:Label></td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdoeqs" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdeqs" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txteqs" style="WIDTH: 230px; HEIGHT: 40px" name="txteqs"
								rows="3" cols="20" runat="server">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang413" runat="server">Down Time</asp:Label></td>
						<td class="labellt cellbrdrcntr plainlabel" id="tdodt" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr plainlabel" id="tddt" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txtdt" style="WIDTH: 230px; HEIGHT: 40px" name="txtdt" rows="3"
								cols="20" runat="server">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang414" runat="server">Parts</asp:Label></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lboparts" runat="server" CssClass="plainlabel" Width="210px" Height="52px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbparts" runat="server" CssClass="plainlabel" Width="210px" Height="52px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txtpart" style="WIDTH: 230px; HEIGHT: 40px" name="txtpart"
								rows="3" cols="20" runat="server">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang415" runat="server">Tools</asp:Label></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbotools" runat="server" CssClass="plainlabel" Width="210px" Height="52px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbtools" runat="server" CssClass="plainlabel" Width="210px" Height="52px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txttool" style="WIDTH: 230px; HEIGHT: 40px" name="txttool"
								rows="3" cols="20" runat="server">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang416" runat="server">Lubricants</asp:Label></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbolubes" runat="server" CssClass="plainlabel" Width="210px" Height="52px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lblubes" runat="server" CssClass="plainlabel" Width="210px" Height="52px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA class="plainlabel" id="txtlube" style="WIDTH: 230px; HEIGHT: 40px" name="txtlube"
								rows="3" cols="20" runat="server">						</TEXTAREA></td>
					</tr>
				</TABLE>
			</div>
			<input id="lbltaskid" type="hidden" name="lbltaskid" runat="server"> <input id="lbldid" type="hidden" name="lbldid" runat="server">
			<input id="lblclid" type="hidden" name="lblclid" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblfuid" type="hidden" name="lblfuid" runat="server"> <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
			<input id="txtfin" type="hidden" name="txtfin" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lbllock" type="hidden" name="lbllock" runat="server"> <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
			<input id="lblusername" type="hidden" name="lblusername" runat="server"> <input type="hidden" id="lblrev" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
