

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class archrationaletpm2
    Inherits System.Web.UI.Page
	

	Protected WithEvents lang416 As System.Web.UI.WebControls.Label

	Protected WithEvents lang415 As System.Web.UI.WebControls.Label

	Protected WithEvents lang414 As System.Web.UI.WebControls.Label

	Protected WithEvents lang413 As System.Web.UI.WebControls.Label

	Protected WithEvents lang412 As System.Web.UI.WebControls.Label

	Protected WithEvents lang411 As System.Web.UI.WebControls.Label

	Protected WithEvents lang410 As System.Web.UI.WebControls.Label

	Protected WithEvents lang409 As System.Web.UI.WebControls.Label

	Protected WithEvents lang408 As System.Web.UI.WebControls.Label

	Protected WithEvents lang407 As System.Web.UI.WebControls.Label

	Protected WithEvents lang406 As System.Web.UI.WebControls.Label

	Protected WithEvents lang405 As System.Web.UI.WebControls.Label

	Protected WithEvents lang404 As System.Web.UI.WebControls.Label

	Protected WithEvents lang403 As System.Web.UI.WebControls.Label

	Protected WithEvents lang402 As System.Web.UI.WebControls.Label

	Protected WithEvents lang401 As System.Web.UI.WebControls.Label

	Protected WithEvents lang400 As System.Web.UI.WebControls.Label

	Protected WithEvents lang399 As System.Web.UI.WebControls.Label

	Protected WithEvents lang398 As System.Web.UI.WebControls.Label

	Protected WithEvents lang397 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tnum, tid, deptid, cellid, eqid, comid, funid, sql, login, username, ro, rev As String
    Protected WithEvents ibsvrtn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbofm As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbfm As System.Web.UI.WebControls.ListBox
    Protected WithEvents lboparts As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbparts As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbotools As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbtools As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbolubes As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblubes As System.Web.UI.WebControls.ListBox
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtasknum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtfm As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdotd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtodesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdtd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtdesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txttd As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdott As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txttt As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdolm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtlm As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdofreq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfreq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtfr As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdoeqs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txteqs As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdodt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtdt As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txtpart As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txttool As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txtlube As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rat As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If lbllog.Value <> "no" Then
            If Not IsPostBack Then
                'Response.Write(Request.QueryString.ToString)
                Try
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    If ro = "1" Then
                        ibsvrtn.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                        ibsvrtn.Enabled = False
                    Else
                        'ibsvrtn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
                        'ibsvrtn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
                    End If

                    tid = Request.QueryString("tid").ToString
                    lbltaskid.Value = tid
                    If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                        tnum = Request.QueryString("tnum").ToString
                        deptid = Request.QueryString("did").ToString
                        cellid = Request.QueryString("clid").ToString
                        eqid = Request.QueryString("eqid").ToString
                        funid = Request.QueryString("fuid").ToString
                        comid = Request.QueryString("coid").ToString
                        rev = Request.QueryString("rev").ToString
                        lblrev.value = rev
                        tdtasknum.InnerHtml = tnum
                        lbldid.Value = deptid
                        lblclid.Value = cellid
                        If cellid = "none" Then
                            cellid = "0"
                        End If
                        lbleqid.Value = eqid
                        lblcoid.Value = comid
                        lblfuid.Value = funid
                        txtfin.Value = "0"
                        rat.Open()
                        GetDetails(tid)
                        GetRationale(tid)
                        Dim lock, lockby As String
                        Dim user As String = lblusername.Value
                        lock = "0" 'CheckLock(eqid)
                        If lock = "1" Then
                            lockby = lbllockedby.Value
                            If lockby = user Then
                                lbllock.Value = "0"
                            Else
                                Dim strMessage As String = tmod.getmsg("cdstr280", "archrationaletpm2.aspx.vb") & " " & lockby & "."
                                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                                ibsvrtn.Attributes.Add("class", "details")

                            End If
                        ElseIf lock = "0" Then
                            'LockRecord(user, eq)
                        End If
                        rat.Dispose()
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr281" , "archrationaletpm2.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "nodeptid"
                    End If
                Catch ex As Exception
                    Dim strMessage As String =  tmod.getmsg("cdstr282" , "archrationaletpm2.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "nodeptid"
                End Try
            End If
        End If
        ibsvrtn.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov34" , "archrationaletpm2.aspx.vb") & "')")
        ibsvrtn.Attributes.Add("onmouseout", "return nd()")
    End Sub
    Private Sub GetRationale(ByVal tid As String)
        rev = lblrev.Value
        sql = "usp_getRationalearchtpm '" & tid & "','" & rev & "'"
        dr = rat.GetRdrData(sql)

        While dr.Read
            txtfm.Value = dr.Item("fail").ToString
            txttd.Value = dr.Item("taskdesc").ToString
            txttt.InnerHtml = dr.Item("tasktype").ToString

            txtfr.InnerHtml = dr.Item("freq").ToString
            txteqs.InnerHtml = dr.Item("eqstat").ToString
            txtpart.InnerHtml = dr.Item("part").ToString
            txttool.InnerHtml = dr.Item("tool").ToString
            txtlube.InnerHtml = dr.Item("lube").ToString

            txtlm.InnerHtml = dr.Item("labmin").ToString
            txtdt.InnerHtml = dr.Item("down").ToString
        End While
        dr.Close()
    End Sub
    Private Sub GetDetails(ByVal tid As String)
        rev = lblrev.Value
        sql = "select t.*, d.dept_line, c.cell_name, f.func, e.eqnum, " _
        + "isnull(l.output, 'none') as lubes, " _
        + "isnull(tp.output, 'none') as parts, " _
        + "isnull(tt.output, 'none') as tools, " _
        + "isnull(l.orig_output, 'none') as olubes, " _
        + "isnull(tp.orig_output, 'none') as oparts, " _
        + "isnull(tt.orig_output, 'none') as otools " _
        + "from pmtaskstpmarch t " _
        + "left join pmtasklubesoutarch l on l.pmtskid = t.pmtskid " _
        + "left join pmtaskpartsoutarch tp on tp.pmtskid = t.pmtskid " _
        + "left join pmtasktoolsoutarch tt on tt.pmtskid = t.pmtskid " _
        + "right join dept d on d.dept_id = t.deptid " _
        + "right join cells c on c.cellid = t.cellid " _
        + "right join equipment e on e.eqid = t.eqid " _
        + "right join functions f on t.funcid = f.func_id " _
        + "where t.pmtskid ='" & tid & "' and t.rev = '" & rev & "'"
        'sql = "usp_GetRationaleReportTsk '" & tid & "'"
        Dim part, tool, lube, opart, otool, olube, fm, ofm As String
        dr = rat.GetRdrData(sql)
        While dr.Read
            tddept.InnerHtml = dr.Item("dept_line").ToString
            tdcell.InnerHtml = dr.Item("cell_name").ToString
            tdeq.InnerHtml = dr.Item("eqnum").ToString
            tdfunc.InnerHtml = dr.Item("func").ToString
            tdcomp.InnerHtml = dr.Item("compnum").ToString
            txtodesc.Value = dr.Item("otaskdesc").ToString
            txtdesc.Value = dr.Item("taskdesc").ToString
            tdott.InnerHtml = dr.Item("origtasktype").ToString
            tdtt.InnerHtml = dr.Item("tasktype").ToString

            tdofreq.InnerHtml = dr.Item("origfreq").ToString
            tdfreq.InnerHtml = dr.Item("freq").ToString
            tdoeqs.InnerHtml = dr.Item("origrd").ToString
            tdeqs.InnerHtml = dr.Item("rd").ToString

            tdlm.InnerHtml = dr.Item("tTime").ToString
            tdolm.InnerHtml = dr.Item("origtTime").ToString 'orig
            tdodt.InnerHtml = dr.Item("origrdt").ToString
            tddt.InnerHtml = dr.Item("rdt").ToString

            lube = dr.Item("lubes").ToString
            part = dr.Item("parts").ToString
            tool = dr.Item("tools").ToString
            olube = dr.Item("olubes").ToString
            opart = dr.Item("oparts").ToString
            otool = dr.Item("otools").ToString

            fm = dr.Item("fm1").ToString
            ofm = dr.Item("ofm1").ToString

        End While
        dr.Close()
        Dim i As Integer

        Dim fmarr() As String = fm.Split("(___)")
        Dim fmstr As String
        For i = 0 To fmarr.Length - 1
            fmstr = fmarr(i)
            If fmstr <> "___)" Then
                lbfm.Items.Add(fmstr.Replace("___)", ""))
            End If

        Next
        Dim ofmarr() As String = ofm.Split("(___)")
        Dim ofmstr As String
        For i = 0 To ofmarr.Length - 1
            ofmstr = ofmarr(i)
            If ofmstr <> "___)" Then
                lbofm.Items.Add(ofmstr.Replace("___)", ""))
            End If
        Next

        Dim lubearr() As String = lube.Split(";")
        Dim lubestr As String
        For i = 0 To lubearr.Length - 1
            Dim lint As Integer = lubearr(i).IndexOf(" - located at:")
            If lint = 0 Or lint = -1 Then
                lubestr = lubearr(i)
            Else
                lubestr = lubearr(i).Substring(0, lint)
            End If
            lblubes.Items.Add(lubestr)
        Next
        Dim olubearr() As String = olube.Split(";")
        Dim olubestr As String
        For i = 0 To olubearr.Length - 1
            Dim lint As Integer = olubearr(i).IndexOf(" - located at:")
            If lint = 0 Or lint = -1 Then
                olubestr = olubearr(i)
            Else
                olubestr = olubearr(i).Substring(0, lint)
            End If
            lbolubes.Items.Add(olubestr)
        Next

        Dim partarr() As String = part.Split(";")
        Dim partstr As String
        For i = 0 To partarr.Length - 1
            Dim lint As Integer = partarr(i).IndexOf("located at:")
            If lint = 0 Or lint = -1 Then
                partstr = partarr(i)
            Else
                partstr = partarr(i).Substring(0, lint)
            End If
            lbparts.Items.Add(partstr)
        Next
        Dim opartarr() As String = opart.Split(";")
        Dim opartstr As String
        For i = 0 To opartarr.Length - 1
            Dim lint As Integer = opartarr(i).IndexOf("located at:")
            If lint = 0 Or lint = -1 Then
                opartstr = opartarr(i)
            Else
                opartstr = opartarr(i).Substring(0, lint)
            End If
            lboparts.Items.Add(olubestr)
        Next

        Dim toolarr() As String = tool.Split(";")
        Dim toolstr As String
        For i = 0 To toolarr.Length - 1
            Dim lint As Integer = toolarr(i).IndexOf("located at:")
            If lint = 0 Or lint = -1 Then
                toolstr = toolarr(i)
            Else
                toolstr = toolarr(i).Substring(0, lint)
            End If
            lbtools.Items.Add(toolstr)
        Next
        Dim otoolarr() As String = otool.Split(";")
        Dim otoolstr As String
        For i = 0 To otoolarr.Length - 1
            Dim lint As Integer = otoolarr(i).IndexOf("located at:")
            If lint = 0 Or lint = -1 Then
                otoolstr = otoolarr(i)
            Else
                otoolstr = otoolarr(i).Substring(0, lint)
            End If
            lbotools.Items.Add(otoolstr)
        Next



        If Len(tdott.InnerHtml) = 0 Then
            tdott.InnerHtml = "&nbsp;"
        End If
        If Len(tdtt.InnerHtml) = 0 Then
            tdtt.InnerHtml = "&nbsp;"
        End If

        If Len(tdofreq.InnerHtml) = 0 Then
            tdofreq.InnerHtml = "&nbsp;"
        End If
        If Len(tdfreq.InnerHtml) = 0 Then
            tdfreq.InnerHtml = "&nbsp;"
        End If
        If Len(tdoeqs.InnerHtml) = 0 Then
            tdoeqs.InnerHtml = "&nbsp;"
        End If
        If Len(tdeqs.InnerHtml) = 0 Then
            tdeqs.InnerHtml = "&nbsp;"
        End If
        If Len(tdolm.InnerHtml) = 0 Then
            tdolm.InnerHtml = "&nbsp;"
        End If
        If Len(tdlm.InnerHtml) = 0 Then
            tdlm.InnerHtml = "&nbsp;"
        End If
        If Len(tdodt.InnerHtml) = 0 Then
            tdott.InnerHtml = "&nbsp;"
        End If
        If Len(tddt.InnerHtml) = 0 Then
            tddt.InnerHtml = "&nbsp;"
        End If
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang397.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang397")
		Catch ex As Exception
		End Try
		Try
			lang398.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang398")
		Catch ex As Exception
		End Try
		Try
			lang399.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang399")
		Catch ex As Exception
		End Try
		Try
			lang400.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang400")
		Catch ex As Exception
		End Try
		Try
			lang401.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang401")
		Catch ex As Exception
		End Try
		Try
			lang402.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang402")
		Catch ex As Exception
		End Try
		Try
			lang403.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang403")
		Catch ex As Exception
		End Try
		Try
			lang404.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang404")
		Catch ex As Exception
		End Try
		Try
			lang405.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang405")
		Catch ex As Exception
		End Try
		Try
			lang406.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang406")
		Catch ex As Exception
		End Try
		Try
			lang407.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang407")
		Catch ex As Exception
		End Try
		Try
			lang408.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang408")
		Catch ex As Exception
		End Try
		Try
			lang409.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang409")
		Catch ex As Exception
		End Try
		Try
			lang410.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang410")
		Catch ex As Exception
		End Try
		Try
			lang411.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang411")
		Catch ex As Exception
		End Try
		Try
			lang412.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang412")
		Catch ex As Exception
		End Try
		Try
			lang413.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang413")
		Catch ex As Exception
		End Try
		Try
			lang414.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang414")
		Catch ex As Exception
		End Try
		Try
			lang415.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang415")
		Catch ex As Exception
		End Try
		Try
			lang416.Text = axlabs.GetASPXPage("archrationaletpm2.aspx","lang416")
		Catch ex As Exception
		End Try

	End Sub

	

End Class
