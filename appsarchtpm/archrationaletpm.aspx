<%@ Page Language="vb" AutoEventWireup="false" Codebehind="archrationaletpm.aspx.vb" Inherits="lucy_r12.archrationaletpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>archrationaletpm</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/archrationaletpmaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg"  onload="handleexit();">
		<form id="form1" method="post" runat="server">
			<div style="LEFT: 8px; POSITION: absolute; TOP: 8px">
				<table width="675">
					<tr>
						<td class="bluelabel" width="80"><asp:Label id="lang375" runat="server">Task#</asp:Label></td>
						<td class="label" id="tdtasknum" width="230" runat="server"></td>
						<td class="bluelabel" width="80"></td>
						<td class="label" align="right" width="230"><asp:ImageButton id="ibsvrtn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:ImageButton></td>
					</tr>
					<tr height="20">
						<td class="bluelabel"><asp:Label id="lang376" runat="server">Department</asp:Label></td>
						<td class="label" id="tddept" runat="server"></td>
						<td class="bluelabel"><asp:Label id="lang377" runat="server">Station/Cell</asp:Label></td>
						<td class="label" id="tdcell" runat="server"></td>
					</tr>
					<tr height="20">
						<td class="bluelabel"><asp:Label id="lang378" runat="server">Equipment</asp:Label></td>
						<td class="label" id="tdeq" runat="server"></td>
						<td class="bluelabel"><asp:Label id="lang379" runat="server">Function</asp:Label></td>
						<td class="label" id="tdfunc" runat="server"></td>
					</tr>
					<tr height="20">
						<td class="bluelabel" vAlign="top"><asp:Label id="lang380" runat="server">Component</asp:Label></td>
						<td class="label" id="tdcomp" vAlign="top" runat="server"></td>
						<td class="bluelabel"></td>
						<td class="label" align="right"></td>
					</tr>
				</table>
				<TABLE cellSpacing="1" cellPadding="1" width="675" class="tbg">
					<tr>
						<td width="132" class="thdrsing label"><asp:Label id="lang381" runat="server">Task Detail</asp:Label></td>
						<td width="181" class="thdrsing label"><asp:Label id="lang382" runat="server">Original</asp:Label></td>
						<td width="181" class="thdrsing label"><asp:Label id="lang383" runat="server">Revised</asp:Label></td>
						<td width="181" class="thdrsing label"><asp:Label id="lang384" runat="server">Rational</asp:Label></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang385" runat="server">Failure Modes</asp:Label></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbofm" runat="server" Height="54px" Width="150px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbfm" runat="server" Height="54px" Width="150px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txtfm" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txtfm">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang386" runat="server">Task Description</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdotd" runat="server"><TEXTAREA id="txtodesc" runat="server" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								name="Textarea1" rows="3" cols="20">						</TEXTAREA></td>
						<td class="labellt cellbrdrcntr" id="tdtd" runat="server"><TEXTAREA id="txtdesc" runat="server" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								name="Textarea1" rows="3" cols="20">						</TEXTAREA></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txttd" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txttd">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang387" runat="server">Task Type</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdott" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr" id="tdtt" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txttt" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txttt">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang388" runat="server">PdM Tech</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdopdm" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr" id="tdpdm" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txtpdm" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txtpdm">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang389" runat="server">Skill Required</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdosr" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr" id="tdsr" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txtsr" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txtsr">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang390" runat="server">Labor Minutes Each</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdolm" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr" id="tdlm" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txtlm" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txtlm">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang391" runat="server">Frequency</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdofreq" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr" id="tdfreq" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txtfr" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txtfr">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang392" runat="server">EQ Status</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdoeqs" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr" id="tdeqs" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txteqs" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txteqs">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang393" runat="server">Down Time</asp:Label></td>
						<td class="labellt cellbrdrcntr" id="tdodt" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr" id="tddt" runat="server">N/A</td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txtdt" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txtdt">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang394" runat="server">Parts</asp:Label></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lboparts" runat="server" Height="54px" Width="150px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbparts" runat="server" Height="54px" Width="150px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txtpart" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txtpart">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang395" runat="server">Tools</asp:Label></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbotools" runat="server" Height="54px" Width="150px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbtools" runat="server" Height="54px" Width="150px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txttool" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txttool">						</TEXTAREA></td>
					</tr>
					<tr>
						<td class="label cellbrdrend"><asp:Label id="lang396" runat="server">Lubricants</asp:Label></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lbolubes" runat="server" Height="54px" Width="150px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><asp:listbox id="lblubes" runat="server" Height="54px" Width="150px"></asp:listbox></td>
						<td class="labellt cellbrdrcntr"><TEXTAREA id="txtlube" style="FONT-SIZE: 10pt; WIDTH: 180px; FONT-FAMILY: Arial; HEIGHT: 40px"
								rows="3" cols="20" runat="server" NAME="txtlube">						</TEXTAREA></td>
					</tr>
				</TABLE>
			</div>
			<input id="lbltaskid" type="hidden" runat="server" NAME="lbltaskid"> <input id="lbldid" type="hidden" runat="server" NAME="lbldid">
			<input id="lblclid" type="hidden" runat="server" NAME="lblclid"> <input id="lbleqid" type="hidden" runat="server" NAME="lbleqid">
			<input id="lblfuid" type="hidden" runat="server" NAME="lblfuid"> <input id="lblcoid" type="hidden" runat="server" NAME="lblcoid">
			<input type="hidden" id="txtfin" runat="server" NAME="txtfin"><input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lbllock" runat="server" NAME="lbllock"> <input type="hidden" id="lbllockedby" runat="server" NAME="lbllockedby">
			<input type="hidden" id="lblusername" runat="server" NAME="lblusername"> <input type="hidden" id="lblrev" runat="server" NAME="lblrev">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
