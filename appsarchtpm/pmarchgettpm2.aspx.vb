﻿Imports System.Data.SqlClient
Public Class pmarchgettpm2
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim tasks As New Utilities
    Dim sql, cid, sid, eq, eqid, fu, fuid, did, clid, jump, typ, lid, ro, appstr, task As String
    Dim Filter, filt, dt, df, tl, val, tli, rev, rcnt As String
    Dim pmid, type, login As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblgototasks.Value = "0"
            lblcleantasks.Value = "2"
            lbltaskcnt.Value = "2"
            cid = "0" 'HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            'Read Only
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            'appstr = HttpContext.Current.Session("appstr").ToString()
            'CheckApps(appstr)
            'End Read Only
            tasks.Open()

            Try
                tli = Request.QueryString("tli").ToString
                lbltl.Value = tli
            Catch ex As Exception
                'need to review what the heck this is doing here
            End Try
            jump = Request.QueryString("jump").ToString
            Dim deptcheck As Integer = 0
            Dim fuidcheck As Integer = 0
            If jump = "yes" Then
                Try
                    task = Request.QueryString("task").ToString
                    lbltask.Value = task
                Catch ex As Exception

                End Try
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                Dim eqcheck As Integer = 0
                If typ = "loc" Or typ = "dloc" Then
                    If typ = "loc" Then
                        trdepts.Attributes.Add("class", "details")
                    End If
                    trlocs.Attributes.Add("class", "view")
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    did = Request.QueryString("did").ToString
                    If did = "" Then
                        deptcheck = 1
                    Else
                        Try
                            lbldid.Value = did
                            Filter = did
                        Catch ex As Exception
                            deptcheck = 1
                        End Try
                        If deptcheck <> 1 Then
                            Dim cellchk As Integer = 0
                            Dim chk As String = CellCheck(Filter)
                            If chk = "no" Then
                                lblchk.Value = "no"
                            Else
                                lblchk.Value = "yes"
                                clid = Request.QueryString("clid").ToString
                                Try
                                    lblclid.Value = clid
                                    lblpar.Value = "cell"
                                Catch ex As Exception
                                    cellchk = 1
                                End Try
                            End If
                        End If
                    End If
                    eqid = Request.QueryString("eqid").ToString
                    Try
                        rev = Request.QueryString("rev").ToString
                    Catch ex As Exception
                        rev = ""
                    End Try
                    lblrev.Value = rev
                    Try
                        lbleqid.Value = eqid
                        GetRev(eqid)
                        If rev <> "" Then
                            Try
                                ddrev.SelectedValue = rev
                            Catch ex1 As Exception

                            End Try
                        End If
                    Catch ex As Exception
                        eqcheck = 1
                    End Try
                    If eqcheck <> 1 Then
                        fuid = Request.QueryString("funid").ToString
                        Try
                            lblfuid.Value = fuid
                        Catch ex As Exception
                            fuidcheck = 1
                        End Try
                        If fuid = "" Then
                            fuidcheck = "1"
                        End If
                        If fuidcheck <> 1 Then
                            CheckTasks(fuid)
                            Dim coid As String
                            Try
                                coid = Request.QueryString("comid").ToString
                                lblcoid.Value = coid
                            Catch ex As Exception

                            End Try
                            If lblcoid.Value <> "" AndAlso lblcoid.Value <> "0" Then
                                lbltaskcnt.Value = "1"
                                GoToTask(coid)
                            Else
                                GoToTasks("yes")
                            End If
                        Else
                            lblgototasks.Value = "0"
                        End If
                    End If
                    'Need details here for locs
                    getlocs(lid, eqid, fuid)
                Else
                    trdepts.Attributes.Add("class", "view")
                    did = Request.QueryString("did").ToString
                    Try
                        lbldid.Value = did
                        Filter = did
                    Catch ex As Exception
                        deptcheck = 1
                    End Try
                    If deptcheck <> 1 Then
                        Dim cellchk As Integer = 0
                        Dim chk As String = CellCheck(Filter)
                        If chk = "no" Then
                            lblchk.Value = "no"
                        Else
                            lblchk.Value = "yes"
                            clid = Request.QueryString("clid").ToString
                            Try
                                lblclid.Value = clid
                                lblpar.Value = "cell"
                            Catch ex As Exception
                                cellchk = 1
                            End Try
                        End If
                        If cellchk <> 1 Then
                            eqid = Request.QueryString("eqid").ToString
                            Try
                                rev = Request.QueryString("rev").ToString
                            Catch ex As Exception
                                rev = ""
                            End Try
                            lblrev.Value = rev
                            Try
                                lbleqid.Value = eqid
                                GetRev(eqid)
                                If rev <> "" Then
                                    Try
                                        ddrev.SelectedValue = rev
                                    Catch ex1 As Exception

                                    End Try
                                End If
                            Catch ex As Exception
                                eqcheck = 1
                            End Try
                            If tli <> "4" Then
                                If eqcheck <> 1 Then
                                    Try
                                        fuid = Request.QueryString("funid").ToString
                                        lblfuid.Value = fuid
                                    Catch ex As Exception
                                        fuidcheck = 1
                                    End Try
                                    If fuid = "" Then
                                        fuidcheck = "1"
                                    End If
                                    If fuidcheck <> 1 Then
                                        CheckTasks(fuid)
                                        Dim coid As String
                                        Try
                                            coid = Request.QueryString("comid").ToString
                                            lblcoid.Value = coid
                                        Catch ex As Exception

                                        End Try
                                        If lblcoid.Value <> "" AndAlso lblcoid.Value <> "0" Then
                                            lbltaskcnt.Value = "1"
                                            GoToTask(coid)
                                        Else
                                            GoToTasks("yes")
                                        End If
                                    Else
                                        lblgototasks.Value = "0"
                                    End If
                                Else
                                    GoToTasks()
                                End If
                            Else
                                lbltaskcnt.Value = "4"
                                GoToTasks()
                            End If
                        End If
                    End If
                    'need details for depts here
                    getdepts(did, clid, eqid, fuid)
                End If 'locs or depts

            End If
            If eqid <> "" And rev <> "" Then
                CleanTasks()
            ElseIf eqid <> "" Then
                rcnt = lblrevcnt.Value
                If rcnt <> "0" Then
                    tdrevmsg.InnerHtml = "No Revision Selected"
                End If
            End If
            tasks.Dispose()
        Else
            If Request.Form("lblpchk") = "eq" Then
                tasks.Open()
                eqid = lbleqid.Value
                lblpchk.Value = ""
                lbltaskcnt.Value = "4"
                CleanTasks()
                GoToTasks()
                tasks.Dispose()
            ElseIf Request.Form("lblpchk") = "func" Then
                eqid = lbleqid.Value
                tasks.Open()
                fuid = lblfuid.Value
                If fuid <> "0" AndAlso fuid <> "undefined" Then
                    Try
                        CheckTasks(fuid)
                    Catch ex As Exception
                        lbltaskcnt.Value = "5"
                        lblcleantasks.Value = "0"
                        CleanTasks()
                        GoToTasks()
                    End Try
                Else
                    lbltaskcnt.Value = "5"
                    lblcleantasks.Value = "0"
                    CleanTasks()
                    GoToTasks()
                End If
                lblpchk.Value = ""
                GoToTasks()
                tasks.Dispose()
            ElseIf Request.Form("lblpchk") = "archit" Then
                lblpchk.Value = ""
                tasks.Open()
                'ArchiveEq()
                tasks.Dispose()
            End If
        End If
    End Sub
    Private Sub CleanTasks()
        Dim tskchk As String = lblcleantasks.Value
        If tskchk = "0" Then
            lblcleantasks.Value = "1"
            lblgototasks.Value = "1"
        End If
    End Sub
    Private Sub getlocs(ByVal lid As String, ByVal eqid As String, ByVal fuid As String)
        If fuid = "" Or fuid = "no" Then
            fuid = "0"
        End If
        sql = "declare @loc varchar(50), @eq varchar(50), @fu varchar(50); " _
        + "set @loc = (select location from pmlocations where locid = '" & lid & "'); " _
        + "set @eq = (select eqnum from equipment where eqid = '" & eqid & "'); " _
        + "set @fu = (select func from functions where func_id = '" & fuid & "') " _
        + "select @loc as 'loc', @eq as 'eq', @fu as 'fu'"
        Dim loc, eqnum, func, comp As String
        dr = tasks.GetRdrData(sql)
        While dr.Read
            loc = dr.Item("loc").ToString
            eqnum = dr.Item("eq").ToString
            func = dr.Item("fu").ToString
        End While
        dr.Close()
        tdloc3.InnerHtml = loc
        tdeq3.InnerHtml = eqnum
        tdfu3.InnerHtml = func
        lblloc.Value = loc
        lbleq.Value = eqnum
        lblfu.Value = func
    End Sub
    Private Sub getdepts(ByVal did As String, ByVal clid As String, ByVal eqid As String, ByVal fuid As String)
        If fuid = "" Or fuid = "no" Then
            fuid = "0"
        End If
        sql = "declare @dept varchar(50), @cell varchar(50), @eq varchar(50), @fu varchar(50); " _
        + "set @dept = (select dept_line from dept where dept_id = '" & did & "'); " _
        + "set @cell = (select cell_name from cells where cellid = '" & clid & "'); " _
        + "set @eq = (select eqnum from equipment where eqid = '" & eqid & "'); " _
        + "set @fu = (select func from functions where func_id = '" & fuid & "') " _
        + "select @dept as 'dept', @cell as 'cell', @eq as 'eq', @fu as 'fu'"

        Dim dept, cell, eqnum, func, comp As String
        dr = tasks.GetRdrData(sql)
        While dr.Read
            dept = dr.Item("dept").ToString
            cell = dr.Item("cell").ToString
            eqnum = dr.Item("eq").ToString
            func = dr.Item("fu").ToString
        End While
        dr.Close()
        tddept.InnerHtml = dept
        tdcell.InnerHtml = cell
        tdeq.InnerHtml = eqnum
        tdfu.InnerHtml = func
        lbldept.Value = dept
        lblcell.Value = cell
        lbleq.Value = eqnum
        lblfu.Value = func
    End Sub
    Private Sub GoToTask(ByVal coid As String)
        Dim tn As Integer
        Dim tasknum, chk As String
        lblcoid.Value = coid
        tl = "5" 'lbltasklev.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        chk = lblchk.Value
        fuid = lblfuid.Value
        'sql = "select top(1) tasknum from pmtasks where funcid = '" & fuid & "' and comid = '" & coid & "'"
        'tn = tasks.Scalar(sql)
        'tasknum = tn

        lblgototasks.Value = "1"
        lblcleantasks.Value = "0"
        'Response.Redirect("PMTaskDivFuncGrid.aspx?start=yes&tl=5&chk=" & chk & "&cid=" & _
        'cid + "&fuid=" & fuid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & _
        '"&eqid=" & eqid & "&task=" & tasknum & "&comid=" & coid)

    End Sub
    Private Sub GoToTasks(Optional ByVal jump As String = "no")
        tl = "5" 'lbltl.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        Dim pmstr, doctyp As String
        pmstr = lblpmnum.Value
        doctyp = lbldoctype.Value
        Dim chk As String
        chk = lblchk.Value
        lblgototasks.Value = "1"
        lblcleantasks.Value = "0"
    End Sub
    Private Sub CheckTasks(ByVal fu As String)
        If fu <> "" And fu <> "no" Then
            sql = "select count(*) from pmtasks where funcid = '" & fu & "'"
            Dim cnt As Integer
            cnt = tasks.Scalar(sql)
            If cnt = 0 Then
                lbltaskcnt.Value = "0"
            Else
                lbltaskcnt.Value = "1"
                'CheckTPA(fu)
            End If
        End If

    End Sub
    Private Sub GetRev(ByVal eqid As String)
        eqid = lbleqid.Value
        Dim revcnt As Integer = 0
        sql = "select count(*) from equipmentarch where eqid = '" & eqid & "'"
        revcnt = tasks.Scalar(sql)
        lblrevcnt.Value = revcnt
        If revcnt = 0 Then
            tdrevmsg.InnerHtml = "No Revisions Created for this Record"
        End If
        sql = "select tpmrev from equipmenttpmarch where eqid = '" & eqid & "'"
        'Dim rev As String
        'rev = tasks.strScalar(sql)
        'tdrev.InnerHtml = rev
        'trrev.Attributes.Add("class", "view")
        dr = tasks.GetRdrData(sql)
        ddrev.DataSource = dr
        ddrev.DataValueField = "tpmrev"
        ddrev.DataTextField = "tpmrev"
        ddrev.DataBind()
        dr.Close()
        ddrev.Items.Insert(0, "Select Revision")
        trrev.Style.Add("display", "block")
        trrev.Style.Add("visibility", "visible")

    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = tasks.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = tasks.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function

    Private Sub ddrev_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddrev.SelectedIndexChanged
        Dim rev As String
        rev = ddrev.SelectedValue
        lblrev.Value = rev
        eq = lbleqid.Value
        Try
            If ddrev.SelectedIndex <> 0 Then
                'tasks.Open()
                'PopFunc(eq, rev)
                'tasks.Dispose()
                CleanTasks()
                tdrevmsg.InnerHtml = ""

            Else
                tdrevmsg.InnerHtml = "No Revision Selected"
                lblcleantasks.Value = "0"
                lbltaskcnt.Value = "4"
                CleanTasks()
            End If
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr269", "pmarchget.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        'iftaskdet.Attributes.Add("src", "OptHolder.aspx")
        lblcleantasks.Value = "0"
        CleanTasks()
    End Sub
End Class