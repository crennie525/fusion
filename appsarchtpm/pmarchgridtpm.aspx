<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmarchgridtpm.aspx.vb" Inherits="lucy_r12.pmarchgridtpm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pmarchgridtpm</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/pmarchgridtpmaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg"  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<div style="Z-INDEX: 100;LEFT: 0px;OVERFLOW: auto;POSITION: absolute;TOP: 0px;HEIGHT: 400px">
				<table width="722" cellspacing="0" cellpadding="0">
					<TBODY>
						<tr>
							<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/archtop.gif" border="0"></td>
							<td colSpan="2" class="thdrsingrt label" width="696"><asp:Label id="lang439" runat="server">Current Tasks</asp:Label></td>
						</tr>
						<tr>
							<td colSpan="3"><asp:repeater id="rptrtasks" runat="server">
									<HeaderTemplate>
										<table cellspacing="2">
											<tr class="tbg" width="722" height="26">
												<td class="thdrsingg plainlabel" width="80"><asp:Label id="lang440" runat="server">Task#</asp:Label></td>
												<td class="thdrsingg plainlabel" width="180"><asp:Label id="lang441" runat="server">Component Addressed</asp:Label></td>
												<td class="thdrsingg plainlabel" width="30"><asp:Label id="lang442" runat="server">Type</asp:Label></td>
												<td class="thdrsingg plainlabel" width="402"><asp:Label id="lang443" runat="server">Task Description</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr height="20">
											<td class="plainlabel transrowblue">&nbsp;
												<asp:LinkButton ID="lbltn"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' Runat = server>
												</asp:LinkButton>
											</td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
												</asp:Label></td>
											<td class="label transrowblue" id="lblo" runat="server"><%# DataBinder.Eval(Container.DataItem,"o")%></td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"otaskdesc")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>' Runat = server>
												</asp:Label></td>
										</tr>
										<tr class="tbg">
											<td>&nbsp;</td>
											<td></td>
											<td class="label"><%# DataBinder.Eval(Container.DataItem,"r")%></td>
											<td class="plainlabel"><asp:Label ID="Label4" Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<AlternatingItemTemplate>
										<tr height="20">
											<td class="plainlabel transrowblue">&nbsp;
												<asp:LinkButton ID="lbltnalt"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' Runat = server>
												</asp:LinkButton>
											</td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
												</asp:Label></td>
											<td class="label transrowblue"><%# DataBinder.Eval(Container.DataItem,"o")%></td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"otaskdesc")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>' Runat = server>
												</asp:Label></td>
										</tr>
										<tr class="tbg">
											<td>&nbsp;</td>
											<td></td>
											<td class="label"><%# DataBinder.Eval(Container.DataItem,"r")%></td>
											<td class="plainlabel"><asp:Label ID="Label5" Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</AlternatingItemTemplate>
									<FooterTemplate>
				</table>
				</FooterTemplate> </asp:repeater></TD></TR></TBODY></TABLE>
			</div>
			<input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbltaskid" type="hidden" name="lbltaskid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lbltasklev" type="hidden" name="lbltasklev" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblfilt" type="hidden" name="lblfilt" runat="server"> <input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="taskcnt" type="hidden" name="taskcnt" runat="server"><input type="hidden" id="lbltasknum" runat="server" NAME="lbltasknum">
			<input type="hidden" id="lblcoid" runat="server" NAME="lblcoid"><input type="hidden" id="lbltyp" runat="server" NAME="lbltyp">
			<input type="hidden" id="lbllid" runat="server" NAME="lbllid"><input type="hidden" id="lblrev" runat="server" NAME="lblrev">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
