

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmarchtaskstpm
    Inherits System.Web.UI.Page
	Protected WithEvents sgrid As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid48 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid47 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid46 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid45 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid44 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid43 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid42 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid41 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid40 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid39 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid38 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents ovid37 As System.Web.UI.HtmlControls.HtmlTableCell

	Protected WithEvents lang470 As System.Web.UI.WebControls.Label

	Protected WithEvents lang469 As System.Web.UI.WebControls.Label

	Protected WithEvents lang468 As System.Web.UI.WebControls.Label

	Protected WithEvents lang467 As System.Web.UI.WebControls.Label

	Protected WithEvents lang466 As System.Web.UI.WebControls.Label

	Protected WithEvents lang465 As System.Web.UI.WebControls.Label

	Protected WithEvents lang464 As System.Web.UI.WebControls.Label

	Protected WithEvents lang463 As System.Web.UI.WebControls.Label

	Protected WithEvents lang462 As System.Web.UI.WebControls.Label

	Protected WithEvents lang461 As System.Web.UI.WebControls.Label

	Protected WithEvents lang460 As System.Web.UI.WebControls.Label

	Protected WithEvents lang459 As System.Web.UI.WebControls.Label

	Protected WithEvents lang458 As System.Web.UI.WebControls.Label

	Protected WithEvents lang457 As System.Web.UI.WebControls.Label

	Protected WithEvents lang456 As System.Web.UI.WebControls.Label

	Protected WithEvents lang455 As System.Web.UI.WebControls.Label

	Protected WithEvents lang454 As System.Web.UI.WebControls.Label

	Protected WithEvents lang453 As System.Web.UI.WebControls.Label

	Protected WithEvents lang452 As System.Web.UI.WebControls.Label

	Protected WithEvents lang451 As System.Web.UI.WebControls.Label

	Protected WithEvents lang450 As System.Web.UI.WebControls.Label

	Protected WithEvents lang449 As System.Web.UI.WebControls.Label

	Protected WithEvents lang448 As System.Web.UI.WebControls.Label

	Protected WithEvents lang447 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tl, cid, sid, did, clid, eqid, fuid, coid, sql, val, name, field, ttid, tnum, username, lid, typ, ro, appstr, rev As String
    Dim co, fail, chk, tc As String
    Dim tskcnt As Integer
    Dim tasks As New Utilities
    Dim tasksadd As New Utilities
    Public dr As SqlDataReader
    Dim ds As DataSet
    Dim Tables As String = "pmtaskstpmarch"
    Dim PK As String = "pmtskid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 1
    Dim Fields As String = "*, haspm = (select e.hastpm from equipmenttpmarch e where e.eqid = pmtaskstpmarch.eqid)"
    Dim Filter As String = ""
    Dim CntFilter As String = ""
    Dim Group As String = ""
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img4 As System.Web.UI.HtmlControls.HtmlImage
    Dim Sort As String = "tasknum, subtask asc"
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbofailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbCompFM As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbfaillist As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox
    Protected WithEvents cbloto As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbcs As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Label22 As System.Web.UI.WebControls.Label
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents Label23 As System.Web.UI.WebControls.Label
    Protected WithEvents lblcnt As System.Web.UI.WebControls.Label
    Protected WithEvents Label26 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsubcount As System.Web.UI.WebControls.Label
    Protected WithEvents lblspg As System.Web.UI.WebControls.Label
    Protected WithEvents Label27 As System.Web.UI.WebControls.Label
    Protected WithEvents lblscnt As System.Web.UI.WebControls.Label
    Protected WithEvents btnaddsubtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtodesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txtdesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents ocb1o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb2sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ocb3sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb1sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb2sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3o As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3 As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3mon As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3tue As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3wed As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3thu As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3fri As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cb3sun As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents btnStart As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnPrev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnNext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnEnd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgrat As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblsvchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblenable As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgholder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasklev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblst As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltabid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsave As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgrid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcind As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldocpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsesscnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblhaspm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnoeq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcqty As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtypeo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtype As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdqtyo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtro As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqstato As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrdto As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfreqo As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtaskstat As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdqty As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqstat As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrdt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfreq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpfint As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Page.EnableViewState = True

        Dim login As String
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            'Server.Transfer("/laipm3/NewLogin.aspx")
            lbllog.Value = "no"
            Exit Sub
        End Try
        If lbllog.Value <> "no" Then
            If lblsvchk.Value = "1" Then
                goNext()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "2" Then
                goPrev()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "3" Then
                goSubNext()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "4" Then
                goSubPrev()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "5" Then
                GoFirst()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "6" Then
                GoLast()
                'lblsave.Value = "no"
            ElseIf lblsvchk.Value = "7" Then
                GoNav()
                'lblsave.Value = "no"
            End If
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'PopComp()
                'added in case components with tasks were copied on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                coid = lblco.Value

                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "2" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'PopComp()
                'added in case components deleted on return
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                'end new
                tasks.Dispose()
                coid = lblco.Value

            ElseIf Request.Form("lblcompchk") = "3" Then
                lblcompchk.Value = "0"
                tasksadd.Open()
                'SaveTask2()
                tasksadd.Dispose()
            ElseIf Request.Form("lblcompchk") = "4" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'GetLists()
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "5" Then
                lblcompchk.Value = "0"
                tasks.Open()
                SubCount(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "6" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'LoadCommon()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "7" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'DeleteTask()

                tasks.Dispose()
            ElseIf Request.Form("lblcompchk") = "uptpm" Then
                lblcompchk.Value = "0"
                tasks.Open()
                'UpTPM()
                Filter = lblfilt.Value
                PageNumber = lblpg.Text
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            End If

            If Request.Form("lblcompfailchk") = "1" Then
                lblcompfailchk.Value = "0"
                'AddFail()
            End If
            Dim start As String = Request.QueryString("start").ToString

            If start = "no" Then
                lblpg.Text = "0"
                lblcnt.Text = "0"
                lblspg.Text = "0"
                lblscnt.Text = "0"
            Else
                lblstart.Value = "no"
            End If
            If Request.Form("lblgrid") = "yes" Then
                lblgrid.Value = ""
                val = lblfuid.Value
                field = "funcid"
                lblsb.Value = "0"
                Filter = field & " = " & val ' & " and subtask = 0"
                lblfilt.Value = Filter
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
                'GoToGrid()
            End If
            If Not IsPostBack Then
                appstr = HttpContext.Current.Session("appstr").ToString()
                CheckApps(appstr)
                'lblsave.Value = "no"
                If start = "yes" Then
                    'tc = Request.QueryString("tc").ToString
                    'lblstart.Value = "yes" & tc
                    lblcnt.Text = "0"
                    lblsvchk.Value = "0"
                    lblenable.Value = "1"
                    tl = Request.QueryString("tl").ToString
                    lbltasklev.Value = tl
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    did = Request.QueryString("did").ToString
                    lbldid.Value = did
                    clid = Request.QueryString("clid").ToString
                    lblclid.Value = clid
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    chk = Request.QueryString("chk").ToString
                    typ = Request.QueryString("typ").ToString
                    lbltyp.Value = typ
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    rev = Request.QueryString("rev").ToString
                    lblrev.Value = rev
                    If chk = "" Then
                        chk = "no"
                        lblchk.Value = chk
                    Else
                        lblchk.Value = chk
                    End If
                    tasks.Open()
                    fuid = Request.QueryString("fuid").ToString
                    lblfuid.Value = fuid
                    Dim pmid, pmstr As String
                    'pmid = Request.QueryString("pmid").ToString
                    'lbldocpmid.Value = pmid
                    'pmstr = Request.QueryString("pmstr").ToString
                    'lbldocpmstr.Value = pmstr
                    'val = fuid
                    'field = "funcid"
                    'name = "Function"
                    'eqid = Request.QueryString("eqid").ToString
                    'lbleqid.Value = eqid

                    'GetLists()
                    'lblsb.Value = "0"
                    'Filter = field & " = " & val & " and subtask = 0"
                    'lblfilt.Value = Filter

                    val = fuid
                    field = "funcid"
                    name = "Function"
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    'GetLists()
                    lblsb.Value = "0"
                    Filter = field & " = " & val ' & " and subtask = 0"
                    CntFilter = field & " = " & val & " and subtask = 0 and rev = " & rev
                    lblfilt.Value = Filter
                    lblfiltcnt.Value = CntFilter
                    Dim tasknum As String
                    Try
                        tasknum = Request.QueryString("task").ToString
                        If tasknum = "" Then
                            tasknum = "0"
                        End If
                        Filter = field & " = " & val '& " and tasknum = " & tasknum
                        PageNumber = tasknum
                    Catch ex As Exception
                        tasknum = "0"
                    End Try

                    LoadPage(PageNumber, Filter)

                    Dim lock, lockby As String
                    Dim user As String = lblusername.Value
                    'Read Only
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro

                    'End Read Only
                    'lock = CheckLock(eqid)
                    'If lock = "1" Then
                    'lockby = lbllockedby.Value
                    'If lockby = user Then
                    'lbllock.Value = "0"
                    'Else
                    'lblro.Value = "1" '***use instead?
                    'ro = "1"

                    'End If
                    'ElseIf lock = "0" Then
                    'LockRecord(user, eq)
                    '    End If

                    tasks.Dispose()

                Else



                    'tdtaskstat.InnerHtml = "No Task Records Selected Yet"
                    'ddtaskstat.SelectedValue = "Select"

                    lblpg.Text = "0"
                    lblcnt.Text = "0"
                    lblspg.Text = "0"
                    lblscnt.Text = "0"
                End If

                Dim locked As String = lbllock.Value
                If locked = "1" Then
                    'btnedittask.Attributes.Add("class", "details")
                    'btnaddtsk.Attributes.Add("class", "details")
                    'ibCancel.Attributes.Add("class", "details")
                    'IMG2.Attributes.Add("class", "details")
                    'ggrid.Attributes.Add("class", "details")
                    'btnaddcomp.Attributes.Add("class", "details")
                    'imgrat.Attributes.Add("class", "details")
                End If
                'Disable all fields
                If ro <> "1" Then
                    DisableOptions()

                End If

                'end disable

                btnaddsubtask.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov35" , "pmarchtaskstpm.aspx.vb") & "')")
                btnaddsubtask.Attributes.Add("onmouseout", "return nd()")

                btnPrev.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov36" , "pmarchtaskstpm.aspx.vb") & "')")
                btnPrev.Attributes.Add("onmouseout", "return nd()")
                btnPrev.Attributes.Add("onclick", "confirmExit('tb', 'opt');")
                btnStart.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov37" , "pmarchtaskstpm.aspx.vb") & "')")
                btnStart.Attributes.Add("onmouseout", "return nd()")
                btnStart.Attributes.Add("onclick", "confirmExit('ts', 'opt');")
                btnNext.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov38" , "pmarchtaskstpm.aspx.vb") & "')")
                btnNext.Attributes.Add("onmouseout", "return nd()")
                btnNext.Attributes.Add("onclick", "confirmExit('tf', 'opt');")
                btnEnd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov39" , "pmarchtaskstpm.aspx.vb") & "')")
                btnEnd.Attributes.Add("onmouseout", "return nd()")
                btnEnd.Attributes.Add("onclick", "confirmExit('te', 'opt');")
            End If
        End If

    End Sub

    Private Sub CheckApps(ByVal appstr As String)
        Dim apparr() As String = appstr.Split(",")
        Dim e As String = "0"
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "eq" Then
                    e = "1"
                End If
            Next
            If e <> "1" Then
                lblnoeq.Value = "1"

            End If
        Else
            lblnoeq.Value = "0"
        End If

    End Sub

    Private Sub GoToGrid()

        chk = lblchk.Value
        cid = lblcid.Value
        Dim tid As String = lbltaskid.Value
        Dim funid As String = lblfuid.Value
        Dim comid As String = lblco.Value
        clid = lblclid.Value

        Response.Redirect("../apps/GTasksFunc2.aspx?tli=5a&funid=" & funid & "&comid=no&cid=" & cid & "&chk=" & chk & "&date=" & Now() & " Target='_top'")
    End Sub
    Private Sub DisableOptions()

    End Sub
    Private Sub EnableOptions()

    End Sub
    Private Function SubCount(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim scnt As Integer
        fuid = lblfuid.Value
        Try
            sql = "select count(*) from pmTasks where funcid = '" & fuid & "' and tasknum = " & PageNumber & " and subtask <> '0'"
            Try
                scnt = tasks.Scalar(sql)
            Catch ex As Exception
                scnt = tasksadd.Scalar(sql)
            End Try
            lblsubcount.Text = scnt
        Catch ex As Exception
            scnt = 0
        End Try

        Return scnt
    End Function
    Private Function SubTaskCnt(ByVal PageNumber As Integer, ByVal Filter As String) As Integer
        Dim tcnt As Integer
        sql = "select count(*) from pmTasks where " & Filter & " and tasknum <= " & PageNumber & " and subtask = '0'"
        tcnt = tasks.Scalar(sql)
        Dim scnt As Integer
        sql = "select count(*) from pmTasks where " & Filter & " and tasknum < " & PageNumber & " and subtask <> '0'"
        scnt = tasks.Scalar(sql)
        Dim wcnt = tcnt + scnt
        Return wcnt
    End Function
    Private Sub goSubNext()
        If Len(lblfuid.Value) <> 0 Then
            Dim scnt, cscnt, currcnt As Integer
            cscnt = lblsb.Value
            Filter = lblfilt.Value
            PageNumber = lblpg.Text
            tasks.Open()
            scnt = SubCount(PageNumber, Filter)
            currcnt = SubTaskCnt(PageNumber, Filter)
            lblcurrsb.Value = currcnt
            If scnt > 0 Then
                cscnt = cscnt + 1
                lblcurrcs.Value = cscnt
                If cscnt <= scnt Then
                    lblsb.Value = cscnt
                    PageNumber = currcnt + cscnt
                    LoadPage(PageNumber, Filter)
                    lblspg.Text = cscnt
                    lblscnt.Text = scnt
                Else

                    Dim strMessage As String =  tmod.getmsg("cdstr294" , "pmarchtaskstpm.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
            lblenable.Value = "1"
            tasks.Dispose()
        End If

    End Sub
    Private Sub goSubPrev()
        If Len(lblfuid.Value) <> 0 Then
            Dim scnt, cscnt, currcnt As Integer
            cscnt = lblsb.Value
            Filter = lblfilt.Value
            PageNumber = lblpg.Text
            tasks.Open()
            scnt = SubCount(PageNumber, Filter)
            currcnt = SubTaskCnt(PageNumber, Filter)
            lblcurrsb.Value = currcnt
            cscnt = cscnt - 1
            lblcurrcs.Value = cscnt
            If cscnt > 0 Then
                lblsb.Value = cscnt
                PageNumber = currcnt + cscnt
            Else
                lblsb.Value = "0"
                PageNumber = lblpgholder.Value
            End If
            lblspg.Text = cscnt
            lblscnt.Text = scnt
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            lblenable.Value = "1"
        End If
    End Sub
    Private Sub GoNav()
        Dim pg As String = pgflag.Value
        If pg <> "0" Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = pg
            tasks.Open()
            'Filter = Filter & " and tasknum = " & PageNumber
            LoadPage(PageNumber, Filter)
            tasks.Dispose()
            lblenable.Value = "1"
        End If
    End Sub
    Private Sub GoFirst()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = 1
            'Filter = Filter & " and tasknum = " & PageNumber
            tasks.Open()
            LoadPage(PageNumber, Filter)
            lblenable.Value = "1"
            tasks.Dispose()
        End If
    End Sub
    Private Sub GoLast()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            lblsb.Value = "0"
            PageNumber = lblcnt.Text
            'Filter = Filter & " and tasknum = " & PageNumber
            tasks.Open()
            LoadPage(PageNumber, Filter)
            lblenable.Value = "1"
            tasks.Dispose()
        End If
    End Sub

    Private Sub goNext()
        Filter = lblfilt.Value
        If Len(lblfuid.Value) <> 0 Then
            If lblsb.Value = "0" Then
                PageNumber = lblpg.Text
            Else
                PageNumber = lblpgholder.Value
            End If

            lblsb.Value = "0"
            PageNumber = PageNumber + 1
            'Filter = Filter & " and tasknum = " & PageNumber
            If PageNumber <= lblcnt.Text Then
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr295" , "pmarchtaskstpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            lblenable.Value = "1"
        End If

    End Sub
    Private Sub goPrev()
        If Len(lblfuid.Value) <> 0 Then
            Filter = lblfilt.Value
            If lblsb.Value = "0" Or lblspg.Text = "1" Then
                PageNumber = lblpg.Text
            Else
                PageNumber = lblpgholder.Value
            End If
            lblsb.Value = "0"
            'PageNumber = lblpg.Text

            If PageNumber > 1 Then
                PageNumber = PageNumber - 1
                'Filter = Filter & " and tasknum = " & PageNumber
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            ElseIf lblspg.Text = "1" Then
                PageNumber = PageNumber
                'Filter = Filter & " and tasknum = " & PageNumber
                tasks.Open()
                LoadPage(PageNumber, Filter)
                tasks.Dispose()
            Else

                Dim strMessage As String =  tmod.getmsg("cdstr296" , "pmarchtaskstpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            lblenable.Value = "1"
        End If

    End Sub



    Private Sub CleanPage()
        lblt.Value = ""
        lbltaskid.Value = ""
        lblst.Value = ""
        txtdesc.Value = ""
        txtodesc.Value = ""
        lblcind.Value = ""
        cbloto.Checked = False
        cbcs.Checked = False
        lblco.Value = ""
        lbfaillist.Items.Clear()
        lbfailmodes.Items.Clear()
        lbofailmodes.Items.Clear()
        lbCompFM.Items.Clear()
    End Sub
    Private Sub LoadPage(ByVal PageNumber As Integer, ByVal Filter As String)
        '**** Added for PM Manager
        eqid = lbleqid.Value
        'Try
        'lblhaspm.Value = tasks.HasPM(eqid)
        'Catch ex As Exception
        'lblhaspm.Value = tasksadd.HasPM(eqid)
        'End Try

        If lblsb.Value = "0" Then
            Dim scnt As Integer
            scnt = SubCount(PageNumber, Filter)
            lblscnt.Text = scnt
            lblsubcount.Text = scnt
            lblpgholder.Value = PageNumber
            lblspg.Text = "0"
            cbloto.Checked = False
            cbcs.Checked = False
        End If
        Dim tskcnt, optcnt As Integer
        tskcnt = 0
        If tskcnt = 0 Then
            CntFilter = lblfiltcnt.Value
            sql = "select count(*) from pmtaskstpmarch where " & CntFilter
            Try
                tskcnt = tasks.Scalar(sql)
            Catch ex As Exception
                tskcnt = tasksadd.Scalar(sql)
            End Try
        End If
        lblcnt.Text = tskcnt
        lblpg.Text = PageNumber
        Dim cr, rv As String
        If tskcnt = 0 Then
            lblpg.Text = "0"
            lblcnt.Text = tskcnt
            CleanPage()
            Dim strMessage As String =  tmod.getmsg("cdstr297" , "pmarchtaskstpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Dim JTables, J2Tables, JPK, J2PK As String
            Tables = "pmtaskstpmarch"
            JTables = "pmtasksdaysarch"
            J2Tables = "pmotasksdaysarch"
            PK = "pmtaskstpmarch.pmtskid"
            JPK = "pmtasksdaysarch.pmtskid"
            J2PK = "pmotasksdaysarch.pmtskid"
            fuid = lblfuid.Value
            Fields = "*, haspm = (select e.hastpm from equipmenttpmarch e where e.eqid = pmtaskstpmarch.eqid and rev = " & rev & ")"
            'Try
            dr = tasks.GetPage3(Tables, JTables, J2Tables, PK, JPK, J2PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            'Catch ex As Exception
            'dr = tasksadd.GetPage3(Tables, JTables, J2Tables, PK, JPK, J2PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            'End Try
            While dr.Read
                tdpfint.InnerHtml = dr.Item("pfInterval").ToString
                lblt.Value = dr.Item("tasknum").ToString
                ttid = dr.Item("pmtskid").ToString
                lbltaskid.Value = ttid
                lblst.Value = dr.Item("subtask").ToString
                tdtype.InnerHtml = dr.Item("tasktype").ToString
                tdtypeo.InnerHtml = dr.Item("origtasktype").ToString 'orig
                tdfreq.InnerHtml = dr.Item("freq").ToString
                tdfreqo.InnerHtml = dr.Item("origfreq").ToString 'orig
               
                tdeqstat.InnerHtml = dr.Item("rd").ToString
                tdeqstato.InnerHtml = dr.Item("origrd").ToString 'orig
                txtdesc.Value = dr.Item("taskdesc").ToString
                txtodesc.Value = dr.Item("otaskdesc").ToString 'orig
                tdqty.InnerHtml = dr.Item("qty").ToString
                tdqtyo.InnerHtml = dr.Item("origqty").ToString 'orig
                tdtr.InnerHtml = dr.Item("tTime").ToString
                tdtro.InnerHtml = dr.Item("origtTime").ToString 'orig
                tdrdto.InnerHtml = dr.Item("origrdt").ToString
                tdrdt.InnerHtml = dr.Item("rdt").ToString
                lblcind.Value = dr.Item("compindex").ToString
                tdtaskstat.InnerHtml = dr.Item("taskstatus").ToString

                If dr.Item("lotoid").ToString = "1" Then
                    cbloto.Checked = True
                Else
                    cbloto.Checked = False
                End If
                If dr.Item("conid").ToString = "1" Then
                    cbcs.Checked = True
                Else
                    cbcs.Checked = False
                End If
                co = dr.Item("comid").ToString
                If Len(co) = 0 Or co = "" Then
                    co = "0"
                End If
                lblco.Value = co
                tdcomp.InnerHtml = dr.Item("compnum").ToString
                tdcqty.InnerHtml = dr.Item("cqty").ToString
                lblhaspm.Value = dr.Item("haspm").ToString

                If dr.Item("cb1o").ToString = "1" Then
                    cb1o.Checked = True
                Else
                    cb1o.Checked = False
                    If dr.Item("cb1").ToString = "1" Then
                        cb1.Checked = True
                        cb1mon.Checked = True
                        cb1tue.Checked = True
                        cb1wed.Checked = True
                        cb1thu.Checked = True
                        cb1fri.Checked = True
                        cb1sat.Checked = True
                        cb1sun.Checked = True
                    Else
                        cb1.Checked = False
                        If dr.Item("cb1mon").ToString = "1" Then
                            cb1mon.Checked = True
                        Else
                            cb1mon.Checked = False
                        End If
                        If dr.Item("cb1tue").ToString = "1" Then
                            cb1tue.Checked = True
                        Else
                            cb1tue.Checked = False
                        End If
                        If dr.Item("cb1wed").ToString = "1" Then
                            cb1wed.Checked = True
                        Else
                            cb1wed.Checked = False
                        End If
                        If dr.Item("cb1thu").ToString = "1" Then
                            cb1thu.Checked = True
                        Else
                            cb1thu.Checked = False
                        End If
                        If dr.Item("cb1fri").ToString = "1" Then
                            cb1fri.Checked = True
                        Else
                            cb1fri.Checked = False
                        End If
                        If dr.Item("cb1sat").ToString = "1" Then
                            cb1sat.Checked = True
                        Else
                            cb1sat.Checked = False
                        End If
                        If dr.Item("cb1sun").ToString = "1" Then
                            cb1sun.Checked = True
                        Else
                            cb1sun.Checked = False
                        End If
                    End If
                End If

                If dr.Item("cb2o").ToString = "1" Then
                    cb2o.Checked = True
                Else
                    cb2o.Checked = False
                    If dr.Item("cb2").ToString = "1" Then
                        cb2.Checked = True
                        cb2mon.Checked = True
                        cb2tue.Checked = True
                        cb2wed.Checked = True
                        cb2thu.Checked = True
                        cb2fri.Checked = True
                        cb2sat.Checked = True
                        cb2sun.Checked = True
                    Else
                        cb2.Checked = False
                        If dr.Item("cb2mon").ToString = "1" Then
                            cb2mon.Checked = True
                        Else
                            cb2mon.Checked = False
                        End If
                        If dr.Item("cb2tue").ToString = "1" Then
                            cb2tue.Checked = True
                        Else
                            cb2tue.Checked = False
                        End If
                        If dr.Item("cb2wed").ToString = "1" Then
                            cb2wed.Checked = True
                        Else
                            cb2wed.Checked = False
                        End If
                        If dr.Item("cb2thu").ToString = "1" Then
                            cb2thu.Checked = True
                        Else
                            cb2thu.Checked = False
                        End If
                        If dr.Item("cb2fri").ToString = "1" Then
                            cb2fri.Checked = True
                        Else
                            cb2fri.Checked = False
                        End If
                        If dr.Item("cb2sat").ToString = "1" Then
                            cb2sat.Checked = True
                        Else
                            cb2sat.Checked = False
                        End If
                        If dr.Item("cb2sun").ToString = "1" Then
                            cb2sun.Checked = True
                        Else
                            cb2sun.Checked = False
                        End If
                    End If
                End If
                If dr.Item("cb3o").ToString = "1" Then
                    cb3o.Checked = True
                Else
                    cb3o.Checked = False
                    If dr.Item("cb3").ToString = "1" Then
                        cb3.Checked = True
                        cb3mon.Checked = True
                        cb3tue.Checked = True
                        cb3wed.Checked = True
                        cb3thu.Checked = True
                        cb3fri.Checked = True
                        cb3sat.Checked = True
                        cb3sun.Checked = True
                    Else
                        cb3.Checked = False
                        If dr.Item("cb3mon").ToString = "1" Then
                            cb3mon.Checked = True
                        Else
                            cb3mon.Checked = False
                        End If
                        If dr.Item("cb3tue").ToString = "1" Then
                            cb3tue.Checked = True
                        Else
                            cb3tue.Checked = False
                        End If
                        If dr.Item("cb3wed").ToString = "1" Then
                            cb3wed.Checked = True
                        Else
                            cb3wed.Checked = False
                        End If
                        If dr.Item("cb3thu").ToString = "1" Then
                            cb3thu.Checked = True
                        Else
                            cb3thu.Checked = False
                        End If
                        If dr.Item("cb3fri").ToString = "1" Then
                            cb3fri.Checked = True
                        Else
                            cb3fri.Checked = False
                        End If
                        If dr.Item("cb3sat").ToString = "1" Then
                            cb3sat.Checked = True
                        Else
                            cb3sat.Checked = False
                        End If
                        If dr.Item("cb3sun").ToString = "1" Then
                            cb3sun.Checked = True
                        Else
                            cb3sun.Checked = False
                        End If
                    End If
                End If
                'Orig
                If dr.Item("ocb1o").ToString = "1" Then
                    ocb1o.Checked = True
                Else
                    ocb1o.Checked = False
                    If dr.Item("ocb1").ToString = "1" Then
                        ocb1.Checked = True
                        ocb1mon.Checked = True
                        ocb1tue.Checked = True
                        ocb1wed.Checked = True
                        ocb1thu.Checked = True
                        ocb1fri.Checked = True
                        ocb1sat.Checked = True
                        ocb1sun.Checked = True
                    Else
                        ocb1.Checked = False
                        If dr.Item("ocb1mon").ToString = "1" Then
                            ocb1mon.Checked = True
                        Else
                            ocb1mon.Checked = False
                        End If
                        If dr.Item("ocb1tue").ToString = "1" Then
                            ocb1tue.Checked = True
                        Else
                            ocb1tue.Checked = False
                        End If
                        If dr.Item("ocb1wed").ToString = "1" Then
                            ocb1wed.Checked = True
                        Else
                            ocb1wed.Checked = False
                        End If
                        If dr.Item("ocb1thu").ToString = "1" Then
                            ocb1thu.Checked = True
                        Else
                            ocb1thu.Checked = False
                        End If
                        If dr.Item("ocb1fri").ToString = "1" Then
                            ocb1fri.Checked = True
                        Else
                            ocb1fri.Checked = False
                        End If
                        If dr.Item("ocb1sat").ToString = "1" Then
                            ocb1sat.Checked = True
                        Else
                            ocb1sat.Checked = False
                        End If
                        If dr.Item("ocb1sun").ToString = "1" Then
                            ocb1sun.Checked = True
                        Else
                            ocb1sun.Checked = False
                        End If
                    End If
                End If

                If dr.Item("ocb2o").ToString = "1" Then
                    ocb2o.Checked = True
                Else
                    ocb2o.Checked = False
                    If dr.Item("ocb2").ToString = "1" Then
                        ocb2.Checked = True
                        ocb2mon.Checked = True
                        ocb2tue.Checked = True
                        ocb2wed.Checked = True
                        ocb2thu.Checked = True
                        ocb2fri.Checked = True
                        ocb2sat.Checked = True
                        ocb2sun.Checked = True
                    Else
                        ocb2.Checked = False
                        If dr.Item("ocb2mon").ToString = "1" Then
                            ocb2mon.Checked = True
                        Else
                            ocb2mon.Checked = False
                        End If
                        If dr.Item("ocb2tue").ToString = "1" Then
                            ocb2tue.Checked = True
                        Else
                            ocb2tue.Checked = False
                        End If
                        If dr.Item("ocb2wed").ToString = "1" Then
                            ocb2wed.Checked = True
                        Else
                            ocb2wed.Checked = False
                        End If
                        If dr.Item("ocb2thu").ToString = "1" Then
                            ocb2thu.Checked = True
                        Else
                            ocb2thu.Checked = False
                        End If
                        If dr.Item("ocb2fri").ToString = "1" Then
                            ocb2fri.Checked = True
                        Else
                            ocb2fri.Checked = False
                        End If
                        If dr.Item("ocb2sat").ToString = "1" Then
                            ocb2sat.Checked = True
                        Else
                            ocb2sat.Checked = False
                        End If
                        If dr.Item("ocb2sun").ToString = "1" Then
                            ocb2sun.Checked = True
                        Else
                            ocb2sun.Checked = False
                        End If
                    End If
                End If
                If dr.Item("ocb3o").ToString = "1" Then
                    ocb3o.Checked = True
                Else
                    ocb3o.Checked = False
                    If dr.Item("ocb3").ToString = "1" Then
                        ocb3.Checked = True
                        ocb3mon.Checked = True
                        ocb3tue.Checked = True
                        ocb3wed.Checked = True
                        ocb3thu.Checked = True
                        ocb3fri.Checked = True
                        ocb3sat.Checked = True
                        ocb3sun.Checked = True
                    Else
                        ocb3.Checked = False
                        If dr.Item("ocb3mon").ToString = "1" Then
                            ocb3mon.Checked = True
                        Else
                            ocb3mon.Checked = False
                        End If
                        If dr.Item("ocb3tue").ToString = "1" Then
                            ocb3tue.Checked = True
                        Else
                            ocb3tue.Checked = False
                        End If
                        If dr.Item("ocb3wed").ToString = "1" Then
                            ocb3wed.Checked = True
                        Else
                            ocb3wed.Checked = False
                        End If
                        If dr.Item("ocb3thu").ToString = "1" Then
                            ocb3thu.Checked = True
                        Else
                            ocb3thu.Checked = False
                        End If
                        If dr.Item("ocb3fri").ToString = "1" Then
                            ocb3fri.Checked = True
                        Else
                            ocb3fri.Checked = False
                        End If
                        If dr.Item("ocb3sat").ToString = "1" Then
                            ocb3sat.Checked = True
                        Else
                            ocb3sat.Checked = False
                        End If
                        If dr.Item("ocb3sun").ToString = "1" Then
                            ocb3sun.Checked = True
                        Else
                            ocb3sun.Checked = False
                        End If
                    End If
                End If
                cr = dr.Item("created").ToString
                rv = dr.Item("revised").ToString
                lblhaspm.Value = dr.Item("haspm").ToString
            End While
            dr.Close()
            Dim cind As String = lblcind.Value
            If co <> "0" Then
                lblco.Value = co
                PopCompFailList(co)
                PopFailList(co)
                PopTaskFailModes(co)
                PopoTaskFailModes(co)
                'UpdateFailStats(co)
                'PopDesc(co)
                lblco.Value = co
                chk = "comp"

            Else
                lbfaillist.Items.Clear()
                lbfailmodes.Items.Clear()
                lbofailmodes.Items.Clear()
                lbCompFM.Items.Clear()
                lblco.Value = co
                chk = "comp"
            End If
            lblpar.Value = "comp"
            fuid = lblfuid.Value
            lblcnt.Text = tskcnt
            btnaddsubtask.Enabled = True
        End If
        'tasks.Dispose()
    End Sub
    Private Sub PopFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "' and compfailid not in (" _
         + "select failid from pmtaskfailmodes where comid = '" & comp & "')" ' and taskid = '" & ttid & "')"
        sql = "usp_getcfall_tskna_tpm '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfaillist.DataSource = dr
        lbfaillist.DataTextField = "failuremode"
        lbfaillist.DataValueField = "compfailid"
        lbfaillist.DataBind()
        dr.Close()
    End Sub

    Private Sub PopCompFailList(ByVal comp As String)
        ttid = lbltaskid.Value
        sql = "select compfailid, failuremode " _
         + "from componentfailmodes where comid = '" & comp & "'"
        sql = "usp_getcfall_co '" & comp & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbCompFM.DataSource = dr
        lbCompFM.DataTextField = "failuremode"
        lbCompFM.DataValueField = "compfailid"
        lbCompFM.DataBind()
        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        'sql = "select * from pmtaskfailmodes where taskid = '" & ttid & "'"
        sql = "select * from pmtaskfailmodes where comid = '" & comp & "' and taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsk_tpm '" & comp & "','" & ttid & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try

        lbfailmodes.DataSource = dr
        lbfailmodes.DataTextField = "failuremode"
        lbfailmodes.DataValueField = "failid"
        lbfailmodes.DataBind()
        dr.Close()
    End Sub
    Private Sub PopoTaskFailModes(ByVal comp As String)
        ttid = lbltaskid.Value
        sql = "select * from pmotaskfailmodes where taskid = '" & ttid & "'"
        sql = "usp_getcfall_tsko_tpm '" & ttid & "'"
        Try
            dr = tasks.GetRdrData(sql)
        Catch ex As Exception
            dr = tasksadd.GetRdrData(sql)
        End Try


        lbofailmodes.DataSource = dr
        lbofailmodes.DataTextField = "failuremode"
        lbofailmodes.DataValueField = "failid"
        lbofailmodes.DataBind()
        dr.Close()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label22.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "Label22")
        Catch ex As Exception
        End Try
        Try
            Label23.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "Label23")
        Catch ex As Exception
        End Try
        Try
            Label26.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "Label26")
        Catch ex As Exception
        End Try
        Try
            Label27.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "Label27")
        Catch ex As Exception
        End Try
        Try
            lang447.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang447")
        Catch ex As Exception
        End Try
        Try
            lang448.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang448")
        Catch ex As Exception
        End Try
        Try
            lang449.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang449")
        Catch ex As Exception
        End Try
        Try
            lang450.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang450")
        Catch ex As Exception
        End Try
        Try
            lang451.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang451")
        Catch ex As Exception
        End Try
        Try
            lang452.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang452")
        Catch ex As Exception
        End Try
        Try
            lang453.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang453")
        Catch ex As Exception
        End Try
        Try
            lang454.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang454")
        Catch ex As Exception
        End Try
        Try
            lang455.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang455")
        Catch ex As Exception
        End Try
        Try
            lang456.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang456")
        Catch ex As Exception
        End Try
        Try
            lang457.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang457")
        Catch ex As Exception
        End Try
        Try
            lang458.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang458")
        Catch ex As Exception
        End Try
        Try
            lang459.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang459")
        Catch ex As Exception
        End Try
        Try
            lang460.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang460")
        Catch ex As Exception
        End Try
        Try
            lang461.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang461")
        Catch ex As Exception
        End Try
        Try
            lang462.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang462")
        Catch ex As Exception
        End Try
        Try
            lang463.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang463")
        Catch ex As Exception
        End Try
        Try
            lang464.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang464")
        Catch ex As Exception
        End Try
        Try
            lang465.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang465")
        Catch ex As Exception
        End Try
        Try
            lang466.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang466")
        Catch ex As Exception
        End Try
        Try
            lang467.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang467")
        Catch ex As Exception
        End Try
        Try
            lang468.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang468")
        Catch ex As Exception
        End Try
        Try
            lang469.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang469")
        Catch ex As Exception
        End Try
        Try
            lang470.Text = axlabs.GetASPXPage("pmarchtaskstpm.aspx", "lang470")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            Img3.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "Img3") & "')")
            Img3.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img4.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "Img4") & "', ABOVE, LEFT)")
            Img4.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            Img5.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "Img5") & "', ABOVE, LEFT)")
            Img5.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgrat.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "imgrat") & "')")
            imgrat.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid37.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid37") & "', ABOVE, LEFT)")
            ovid37.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid38.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid38") & "')")
            ovid38.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid39.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid39") & "')")
            ovid39.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid40.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid40") & "')")
            ovid40.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid41.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid41") & "')")
            ovid41.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid42.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid42") & "')")
            ovid42.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid43.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid43") & "')")
            ovid43.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid44.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid44") & "')")
            ovid44.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid45.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid45") & "')")
            ovid45.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid46.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid46") & "')")
            ovid46.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid47.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid47") & "')")
            ovid47.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid48.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "ovid48") & "')")
            ovid48.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            sgrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmarchtaskstpm.aspx", "sgrid") & "')")
            sgrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
