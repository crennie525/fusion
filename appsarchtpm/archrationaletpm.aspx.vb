

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class archrationaletpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang396 As System.Web.UI.WebControls.Label

	Protected WithEvents lang395 As System.Web.UI.WebControls.Label

	Protected WithEvents lang394 As System.Web.UI.WebControls.Label

	Protected WithEvents lang393 As System.Web.UI.WebControls.Label

	Protected WithEvents lang392 As System.Web.UI.WebControls.Label

	Protected WithEvents lang391 As System.Web.UI.WebControls.Label

	Protected WithEvents lang390 As System.Web.UI.WebControls.Label

	Protected WithEvents lang389 As System.Web.UI.WebControls.Label

	Protected WithEvents lang388 As System.Web.UI.WebControls.Label

	Protected WithEvents lang387 As System.Web.UI.WebControls.Label

	Protected WithEvents lang386 As System.Web.UI.WebControls.Label

	Protected WithEvents lang385 As System.Web.UI.WebControls.Label

	Protected WithEvents lang384 As System.Web.UI.WebControls.Label

	Protected WithEvents lang383 As System.Web.UI.WebControls.Label

	Protected WithEvents lang382 As System.Web.UI.WebControls.Label

	Protected WithEvents lang381 As System.Web.UI.WebControls.Label

	Protected WithEvents lang380 As System.Web.UI.WebControls.Label

	Protected WithEvents lang379 As System.Web.UI.WebControls.Label

	Protected WithEvents lang378 As System.Web.UI.WebControls.Label

	Protected WithEvents lang377 As System.Web.UI.WebControls.Label

	Protected WithEvents lang376 As System.Web.UI.WebControls.Label

	Protected WithEvents lang375 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tnum, tid, deptid, cellid, eqid, comid, funid, sql, login, username, ro, rev As String
    Dim dr As SqlDataReader
    Dim rat As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ibsvrtn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbofm As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbfm As System.Web.UI.WebControls.ListBox
    Protected WithEvents lboparts As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbparts As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbotools As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbtools As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbolubes As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblubes As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdtasknum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtfm As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdotd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtodesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdtd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtdesc As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txttd As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdott As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txttt As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdopdm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdpdm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtpdm As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdosr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtsr As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdolm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtlm As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdofreq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfreq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtfr As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdoeqs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txteqs As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents tdodt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtdt As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txtpart As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txttool As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents txtlube As System.Web.UI.HtmlControls.HtmlTextArea
    Protected WithEvents lbltaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
            username = HttpContext.Current.Session("username").ToString()
            lblusername.Value = username
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If lbllog.Value <> "no" Then
            If Not IsPostBack Then
                'Response.Write(Request.QueryString.ToString)
                Try
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    rev = Request.QueryString("rev").ToString
                    lblrev.Value = rev
                    If ro = "1" Then
                        ibsvrtn.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                        ibsvrtn.Enabled = False
                    End If

                    tid = Request.QueryString("tid").ToString
                    lbltaskid.Value = tid
                    If Len(tid) <> 0 AndAlso tid <> "" AndAlso tid <> "0" Then
                        tnum = Request.QueryString("tnum").ToString
                        deptid = Request.QueryString("did").ToString
                        cellid = Request.QueryString("clid").ToString
                        eqid = Request.QueryString("eqid").ToString
                        funid = Request.QueryString("fuid").ToString
                        comid = Request.QueryString("coid").ToString
                        tdtasknum.InnerHtml = tnum
                        lbldid.Value = deptid
                        lblclid.Value = cellid
                        If cellid = "none" Then
                            cellid = "0"
                        End If
                        lbleqid.Value = eqid
                        lblcoid.Value = comid
                        lblfuid.Value = funid
                        txtfin.Value = "0"
                        rat.Open()
                        'CheckDev(tid)
                        BindHead(deptid, cellid, eqid, comid, funid)
                        PopTaskFailModes(tid)
                        PopoTaskFailModes(tid)
                        PopParts(tid)
                        GetDetails(tid)
                        GetRationale(tid)
                        Dim lock, lockby As String
                        Dim user As String = lblusername.Value

                        rat.Dispose()
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr278" , "archrationaletpm.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "nodeptid"
                    End If
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr279" , "archrationaletpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "nodeptid"
                End Try
            End If
        End If

    End Sub

    Private Sub GetRationale(ByVal tid As String)
        rev = lblrev.Value
        sql = "usp_getRationalearchtpm '" & tid & "','" & rev & "'"
        dr = rat.GetRdrData(sql)

        While dr.Read
            txtfm.Value = dr.Item("fail").ToString
            txttd.Value = dr.Item("taskdesc").ToString
            txttt.InnerHtml = dr.Item("tasktype").ToString
            txtpdm.InnerHtml = dr.Item("pdm").ToString
            txtsr.InnerHtml = dr.Item("skill").ToString
            txtfr.InnerHtml = dr.Item("freq").ToString
            txteqs.InnerHtml = dr.Item("eqstat").ToString
            txtpart.InnerHtml = dr.Item("part").ToString
            txttool.InnerHtml = dr.Item("tool").ToString
            txtlube.InnerHtml = dr.Item("lube").ToString

            txtlm.InnerHtml = dr.Item("labmin").ToString
            txtdt.InnerHtml = dr.Item("down").ToString
        End While
        dr.Close()
    End Sub
    Private Sub GetDetails(ByVal tid As String)
        rev = lblrev.Value
        sql = "select * from pmtaskstpmarch where pmtskid ='" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        While dr.Read
            txtodesc.Value = dr.Item("otaskdesc").ToString
            txtdesc.Value = dr.Item("taskdesc").ToString
            tdott.InnerHtml = dr.Item("origtasktype").ToString
            tdtt.InnerHtml = dr.Item("tasktype").ToString
            tdopdm.InnerHtml = dr.Item("origpretech").ToString
            tdpdm.InnerHtml = dr.Item("pretech").ToString
            tdosr.InnerHtml = dr.Item("origskill").ToString
            tdsr.InnerHtml = dr.Item("skill").ToString
            tdofreq.InnerHtml = dr.Item("origfreq").ToString
            tdfreq.InnerHtml = dr.Item("freq").ToString
            tdoeqs.InnerHtml = dr.Item("origrd").ToString
            tdeqs.InnerHtml = dr.Item("rd").ToString

            tdlm.InnerHtml = dr.Item("tTime").ToString
            tdolm.InnerHtml = dr.Item("origtTime").ToString 'orig
            tdodt.InnerHtml = dr.Item("origrdt").ToString
            tddt.InnerHtml = dr.Item("rdt").ToString
        End While
        dr.Close()

        If Len(tdott.InnerHtml) = 0 Then
            tdott.InnerHtml = "&nbsp;"
        End If
        If Len(tdtt.InnerHtml) = 0 Then
            tdtt.InnerHtml = "&nbsp;"
        End If
        If Len(tdopdm.InnerHtml) = 0 Then
            tdopdm.InnerHtml = "&nbsp;"
        End If
        If Len(tdpdm.InnerHtml) = 0 Then
            tdpdm.InnerHtml = "&nbsp;"
        End If
        If Len(tdosr.InnerHtml) = 0 Then
            tdosr.InnerHtml = "&nbsp;"
        End If
        If Len(tdsr.InnerHtml) = 0 Then
            tdsr.InnerHtml = "&nbsp;"
        End If
        If Len(tdofreq.InnerHtml) = 0 Then
            tdofreq.InnerHtml = "&nbsp;"
        End If
        If Len(tdfreq.InnerHtml) = 0 Then
            tdfreq.InnerHtml = "&nbsp;"
        End If
        If Len(tdoeqs.InnerHtml) = 0 Then
            tdoeqs.InnerHtml = "&nbsp;"
        End If
        If Len(tdeqs.InnerHtml) = 0 Then
            tdeqs.InnerHtml = "&nbsp;"
        End If
        If Len(tdolm.InnerHtml) = 0 Then
            tdolm.InnerHtml = "&nbsp;"
        End If
        If Len(tdlm.InnerHtml) = 0 Then
            tdlm.InnerHtml = "&nbsp;"
        End If
        If Len(tdodt.InnerHtml) = 0 Then
            tdott.InnerHtml = "&nbsp;"
        End If
        If Len(tddt.InnerHtml) = 0 Then
            tddt.InnerHtml = "&nbsp;"
        End If
    End Sub
    Private Sub BindHead(ByVal deptid As String, ByVal cellid As String, ByVal eqid As String, ByVal comid As String, ByVal funid As String)
        rev = lblrev.Value
        sql = "usp_getRationaleHeadarchtpm '" & deptid & "', '" & cellid & "', '" & eqid & "', '" & funid & "', '" & comid & "','" & rev & "'"
        dr = rat.GetRdrData(sql)
        'dept_line, cell_name, eqnum, func, compnum
        While dr.Read
            tddept.InnerHtml = dr.Item("dept").ToString
            tdcell.InnerHtml = dr.Item("cell").ToString
            tdeq.InnerHtml = dr.Item("eq").ToString
            tdfunc.InnerHtml = dr.Item("func").ToString
            tdcomp.InnerHtml = dr.Item("co").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopParts(ByVal tid As String)
        rev = lblrev.Value
        sql = "select * from pmtaskpartstpmarch where pmtskid = '" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        lbparts.DataSource = dr
        lbparts.DataTextField = "itemnum"
        lbparts.DataValueField = "tskpartid"
        lbparts.DataBind()
        dr.Close()
        sql = "select * from pmotaskpartstpmarch where pmtskid = '" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        lboparts.DataSource = dr
        lboparts.DataTextField = "itemnum"
        lboparts.DataValueField = "tskpartid"
        lboparts.DataBind()
        dr.Close()

        sql = "select * from pmtasktoolstpmarch where pmtskid = '" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        lbtools.DataSource = dr
        lbtools.DataTextField = "toolnum"
        lbtools.DataValueField = "tsktoolid"
        lbtools.DataBind()
        dr.Close()
        sql = "select * from pmotasktoolstpmarch where pmtskid = '" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        lbotools.DataSource = dr
        lbotools.DataTextField = "toolnum"
        lbotools.DataValueField = "tsktoolid"
        lbotools.DataBind()
        dr.Close()

        sql = "select * from pmtasklubestpmarch where pmtskid = '" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        lblubes.DataSource = dr
        lblubes.DataTextField = "lubenum"
        lblubes.DataValueField = "tsklubeid"
        lblubes.DataBind()
        dr.Close()
        sql = "select * from pmotasklubestpmarch where pmtskid = '" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        lbolubes.DataSource = dr
        lbolubes.DataTextField = "lubenum"
        lbolubes.DataValueField = "tsklubeid"
        lbolubes.DataBind()
        dr.Close()
    End Sub
    Private Sub PopTaskFailModes(ByVal tid As String)
        rev = lblrev.Value
        sql = "select * from pmtaskfailmodestpmarch where taskid = '" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        lbfm.DataSource = dr
        lbfm.DataTextField = "failuremode"
        lbfm.DataValueField = "failid"
        lbfm.DataBind()
        dr.Close()
    End Sub
    Private Sub PopoTaskFailModes(ByVal tid As String)
        rev = lblrev.Value
        sql = "select * from pmotaskfailmodestpmarch where taskid = '" & tid & "' and rev = '" & rev & "'"
        dr = rat.GetRdrData(sql)
        lbofm.DataSource = dr
        lbofm.DataTextField = "failuremode"
        lbofm.DataValueField = "failid"
        lbofm.DataBind()
        dr.Close()
    End Sub


	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang375.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang375")
		Catch ex As Exception
		End Try
		Try
			lang376.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang376")
		Catch ex As Exception
		End Try
		Try
			lang377.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang377")
		Catch ex As Exception
		End Try
		Try
			lang378.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang378")
		Catch ex As Exception
		End Try
		Try
			lang379.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang379")
		Catch ex As Exception
		End Try
		Try
			lang380.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang380")
		Catch ex As Exception
		End Try
		Try
			lang381.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang381")
		Catch ex As Exception
		End Try
		Try
			lang382.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang382")
		Catch ex As Exception
		End Try
		Try
			lang383.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang383")
		Catch ex As Exception
		End Try
		Try
			lang384.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang384")
		Catch ex As Exception
		End Try
		Try
			lang385.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang385")
		Catch ex As Exception
		End Try
		Try
			lang386.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang386")
		Catch ex As Exception
		End Try
		Try
			lang387.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang387")
		Catch ex As Exception
		End Try
		Try
			lang388.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang388")
		Catch ex As Exception
		End Try
		Try
			lang389.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang389")
		Catch ex As Exception
		End Try
		Try
			lang390.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang390")
		Catch ex As Exception
		End Try
		Try
			lang391.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang391")
		Catch ex As Exception
		End Try
		Try
			lang392.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang392")
		Catch ex As Exception
		End Try
		Try
			lang393.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang393")
		Catch ex As Exception
		End Try
		Try
			lang394.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang394")
		Catch ex As Exception
		End Try
		Try
			lang395.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang395")
		Catch ex As Exception
		End Try
		Try
			lang396.Text = axlabs.GetASPXPage("archrationaletpm.aspx","lang396")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			ibsvrtn.Attributes.Add("src" , "../images2/eng/bgbuttons/save.gif")
			ElseIf lang = "fre" Then
			ibsvrtn.Attributes.Add("src" , "../images2/fre/bgbuttons/save.gif")
			ElseIf lang = "ger" Then
			ibsvrtn.Attributes.Add("src" , "../images2/ger/bgbuttons/save.gif")
			ElseIf lang = "ita" Then
			ibsvrtn.Attributes.Add("src" , "../images2/ita/bgbuttons/save.gif")
			ElseIf lang = "spa" Then
			ibsvrtn.Attributes.Add("src" , "../images2/spa/bgbuttons/save.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class
