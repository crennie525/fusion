﻿
$("#ddDisplay").change(function () {

	GetWorkOrderTypeCounts($(this).val(), $("#ddStatus").val(), $("#ddWOEquipmentFilter").val(), $("#ddSupervisor").val(), $("#ddType").val(), $("#startdate").val(), $("#enddate").val());
});

$("#ddStatus").change(function () {

	GetWorkOrderTypeCounts($("#ddDisplay").val(), $(this).val(), $("#ddWOEquipmentFilter").val(), $("#ddSupervisor").val(), $("#ddType").val(), $("#startdate").val(), $("#enddate").val());
});

$("#ddWOEquipmentFilter").change(function () {

	GetWorkOrderTypeCounts($("#ddDisplay").val(), $("#ddStatus").val(), $(this).val(), $("#ddSupervisor").val(), $("#ddType").val(), $("#startdate").val(), $("#enddate").val());
});

$("#ddSupervisor").change(function () {

	GetWorkOrderTypeCounts($("#ddDisplay").val(), $("#ddStatus").val(), $("#ddWOEquipmentFilter").val(), $(this).val(), $("#ddType").val(), $("#startdate").val(), $("#enddate").val());
});

$("#ddType").change(function () {

	GetWorkOrderTypeCounts($("#ddDisplay").val(), $("#ddStatus").val(), $("#ddWOEquipmentFilter").val(), $("#ddSupervisor").val(), $(this).val(), $("#startdate").val(), $("#enddate").val());
});

$("#ddPMOStatusSites").change(function () {

    $("#tdChartLeft").html("");
    $("#tdChartRight").html("");
    GetPMOStatusCounts($(this).val(), $("#ddPMOStatusAssetClasses").val());
});

$("#ddPMOStatusAssetClasses").change(function () {

    $("#tdChartLeft").html("");
    $("#tdChartRight").html("");
    GetPMOStatusCounts($("#ddPMOStatusSites").val(), $(this).val());
});

$("#ddTimeSavedSites").change(function () {

    $("#tdChartLeft").html("");
    $("#tdChartRight").html("");
    GetPMOHours($(this).val(), $("#ddPMOTimeSavingAssetClasses").val());
});

$("#ddPMOTimeSavingAssetClasses").change(function () {

    $("#tdChartLeft").html("");
    $("#tdChartRight").html("");
    GetPMOHours($("#ddTimeSavedSites").val(), $(this).val());
});

$("#ddMaterialSavedSites").change(function () {

    $("#tdChartLeft").html("");
    $("#tdChartRight").html("");
    GetPMOMaterials($(this).val(), $("#ddMaterialSavingAssetClasses").val());
});

$("#ddMaterialSavingAssetClasses").change(function () {

    $("#tdChartLeft").html("");
    $("#tdChartRight").html("");
    GetPMOMaterials($("#ddMaterialSavedSites").val(), $(this).val());
});

$("#ddParentChildSites").change(function () {

    GetEquipmentMasterRecords($(this).val(), $("#ddParentChildAssetClasses").val(), $("#ddParentChildEquipmentFilter").val());
});

$("#ddParentChildAssetClasses").change(function () {

    GetEquipmentMasterRecords($("#ddParentChildSites").val(), $(this).val(), $("#ddParentChildEquipmentFilter").val());
});

$("#ddParentChildEquipmentFilter").change(function () {

    GetEquipmentMasterRecords($("#ddParentChildSites").val(), $("#ddParentChildAssetClasses").val(), $(this).val());
});

$("#ddEqNotStartedSites").change(function () {

    GetNotStartedRecords($(this).val(), $("#ddEqNotStartedAssetClasses").val(), $("#ddEqNotStartedEquipmentFilter").val());
});

$("#ddEqNotStartedAssetClasses").change(function () {

    GetNotStartedRecords($("#ddEqNotStartedSites").val(), $(this).val(), $("#ddEqNotStartedEquipmentFilter").val());
});

$("#ddEqNotStartedEquipmentFilter").change(function () {

    GetNotStartedRecords($("#ddEqNotStartedSites").val(), $("#ddEqNotStartedAssetClasses").val(), $(this).val());
});

function BuildChartData(item, chartData) {

    //get the counts based on the id
    if (item.PMOStatusID == 1) {
        chartData.notstarted = item.StatusCount;
    }
    else if (item.PMOStatusID == 2) {
        chartData.inprogress = item.StatusCount;
    }
    else if (item.PMOStatusID == 3) {
        chartData.optimized = item.StatusCount;
    }

    //keep track of the y max for each site
    if (item.StatusCount >= chartData.yMax) {

        chartData.yMax = item.StatusCount + 30;
    }
}

function GetSiteName(siteAbbrev) {
    var siteName;
    switch (siteAbbrev) {
        case "JU":
            siteName = "Juncos";
            break;
        case "WG":
            siteName = "Rhode Island";
            break;
        case "TO MFG":
            siteName = "Thousand Oaks";
            break;
        case "DU":
            siteName = "Dublin";
            break;
        case "TO Site/GFO":
            siteName = "Thousand Oaks and GFO Sites";
            break;
        default:
            siteName = siteAbbrev;
            break;
    }

    return siteName;
}

function GetPMOHours(siteId, assetClassId) {
    
    $.ajax({
        type: "POST",
        cache: false,
        url: "../dashboard/dashboardwebdata.aspx/GetPMOHours",
        data: "{'siteId':" + siteId + ",'assetClassId':" + assetClassId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            if (response.d != null && response.d != "") {

                var data = $.parseJSON(response.d);

                var chartCount = 0;
                var chartId;
                var yMax;
                var siteName;

                //spin through the data array
                for (var i = 0; i < data.length; i++) {

                    chartCount++;
                    chartId = "chart" + chartCount.toString();

                    var currentTd;
                    if (chartCount % 2 == 0) {
                        currentTd = $("#tdChartRight");
                    } else {
                        currentTd = $("#tdChartLeft");
                    }

                    yMax = 100;
                    if (data[i].OriginalHours > yMax) {
                        if (data[i].OriginalHours < 1000) {
                            yMax = data[i].OriginalHours + 100;
                        } else {
                            yMax = data[i].OriginalHours + 1000;
                        }
                    }

                    if (data[i].OptimizedHours > yMax) {
                        if (data[i].OptimizedHours < 1000) {
                            yMax = data[i].OptimizedHours + 100;
                        } else {
                            yMax = data[i].OptimizedHours + 1000;
                        }
                    }

                    if (data[i].SiteName != "") {
                        siteName = GetSiteName(data[i].SiteName);
                    } else {
                        siteName = $("#ddTimeSavedSites option:selected").text();
                    }

                    //set the title
                    $("<div>").css("text-align", "center").css("padding-top", "15px").text(siteName).appendTo(currentTd);
                    $("<div>").css("width", "550px").attr("id", chartId).appendTo($(currentTd));

                    //calc the difference in hours
                    var hourDecrease = data[i].OriginalHours - data[i].OptimizedHours;
                    $("<div>").css("text-align", "center").css("padding-top", "10px").text("Hours Saved: " + hourDecrease).appendTo(currentTd);

                    //render the chart
                    $.jqplot(chartId, [[data[i].OriginalHours, data[i].OptimizedHours]], {
                        seriesColors: ["#004CFF", "#00FF00"],
                        animate: !$.jqplot.use_excanvas,
                        seriesDefaults: {
                            renderer: $.jqplot.BarRenderer,
                            pointLabels: { show: true },
                            rendererOptions: {
                                fillToZero: true,
                                highlightMouseDown: true,
                                varyBarColor: true
                            }
                        },

                        axes: {
                            xaxis: {
                                renderer: $.jqplot.CategoryAxisRenderer,
                                ticks: ['Original Hours', 'Optimized Hours']
                            },
                            yaxis: {
                                pad: 1.05,
                                max: yMax,
                                min: 0,
                                tickOptions: { formatString: "%#.2f" }
                            }
                        }
                    });
                }
            }
        }
    });
}

function RenderPieChart(data, title, id)
{
	var pieData = [];
	for (var i = 0; i < data.length; i++) {
		pieData.push(
			[data[i].Display, data[i].Count]
		);
	}

	$.jqplot(id, [pieData],
		{
			title: title,
			gridPadding: { top: 28, right: 105, bottom: 23, left: 0 },
			height: 340,
			grid: {
				drawBorder: false,
				shadow: false
			},
			seriesDefaults: {
				renderer: jQuery.jqplot.PieRenderer,
				rendererOptions: {
					diameter: 240,
					showDataLabels: true,
					dataLabels: 'value'
				}
			},
			legend: { show: true, location: 'e', placement: 'outside' },
			seriesColors: ["#d62d20", "#0057e7", "#008744", "#ffa700", "#9900ff", "#33ccff", "#ff6600","#00ff00"]
		}
	);
}

function GetWorkOrderTypeCounts(display,status,eqid,supervisor,type,startdate,enddate) 
{
	$.ajax({
		type: "POST",
		cache: false,
		url: "../dashboard/dashboardwebdata.aspx/GetWorkOrderTypeCounts",
		data: "{'siteId':" + $("#hSiteId").val() + ",'display':'" + display + "','status':'" + status + "','eqid':" + eqid + ",'supervisor':'" + supervisor + "','type':'"
			+ type + "','startdate':'" + startdate + "','enddate':'" + enddate + "'}",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (response) {

			$("#chart1").html("");
			$("#chart2").html("");
			$("#chart3").html("");

			if (response.d.Displays != null)
				RenderPieChart(response.d.Displays, 'All Work Orders', 'chart1');

			if (response.d.DisplaysGreaterThan30Days != null)
				RenderPieChart(response.d.DisplaysGreaterThan30Days, 'Work Orders Greater Than 30 Days', 'chart2');

			if (response.d.DisplaysGreaterThan60Days != null)
				RenderPieChart(response.d.DisplaysGreaterThan60Days, 'Work Orders Greater Than 60 Days', 'chart3')
		}
	});
}

function GetEquipmentMasterRecords(siteId, assetClassId, equipmentFilter) {

    $.ajax({
        type: "POST",
        cache: false,
        url: "../dashboard/dashboardwebdata.aspx/GetMasterRecordHierarchy",
        data: "{'siteId':" + siteId + ",'assetClassId':" + assetClassId + ",'equipmentFilter':'" + equipmentFilter + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#MyTreeGrid').treegrid({
                data: response.d,
                rownumbers: true,
                idField: 'id',
                treeField: 'name',
                columns: [[
                    { title: 'Equipment Name', field: 'name', width: 250 },
                    { field: 'EquipmentDesc', title: 'Equipment Desc', width: 400, align: 'left' },
                    { field: 'ChildTotal', title: 'Child Total', width: 100, align: 'left' }
                ]]
            });
        }
    });
}

function GetNotStartedRecords(siteId, assetClassId, equipmentFilter) {

    $.ajax({
        type: "POST",
        cache: false,
        url: "../dashboard/dashboardwebdata.aspx/GetNotStartedRecords",
        data: "{'siteId':" + siteId + ",'assetClassId':" + assetClassId + ",'equipmentFilter':'" + equipmentFilter + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('#MyGrid').datagrid({
                data: response.d,
                rownumbers: true,
                idField: 'id',
                columns: [[
                    { title: 'Equipment Name', field: 'name', width: 175 },
                    { field: 'EquipmentDesc', title: 'Equipment Desc', width: 475, align: 'left' },
                    { field: 'ChildTotal', title: 'Child Total', width: 100, align: 'left' }
                ]]
            });
        }
    });
}

function GetPMOMaterials(siteId, assetClassId) {

    $.ajax({
        type: "POST",
        cache: false,
        url: "../dashboard/dashboardwebdata.aspx/GetPMOMaterialsSaved",
        data: "{'siteId':" + siteId + ",'assetClassId':" + assetClassId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            if (response.d != null && response.d != "") {

                var data = $.parseJSON(response.d);

                var chartCount = 0;
                var chartId;
                var yMax;
                var siteName;

                //spin through the data array
                for (var i = 0; i < data.length; i++) {

                    chartCount++;
                    chartId = "chart" + chartCount.toString();

                    var currentTd;
                    if (chartCount % 2 == 0) {
                        currentTd = $("#tdChartRight");
                    } else {
                        currentTd = $("#tdChartLeft");
                    }

                    yMax = 100;
                    if (data[i].OriginalMaterials > yMax) {
                        if (data[i].OriginalMaterials < 1000) {
                            yMax = data[i].OriginalMaterials + 100;
                        } else if (data[i].OriginalMaterials >= 1000 && data[i].OriginalMaterials < 10000) {
                            yMax = data[i].OriginalMaterials + 1000;
                        } else if (data[i].OriginalMaterials >= 10000 && data[i].OriginalMaterials < 100000) {
                            yMax = data[i].OriginalMaterials + 10000;
                        } else {
                            yMax = data[i].OriginalMaterials + 100000;
                        }
                    }

                    if (data[i].OptimizedMaterials > yMax) {
                        if (data[i].OptimizedMaterials < 1000) {
                            yMax = data[i].OptimizedMaterials + 100;
                        } else if (data[i].OptimizedMaterials >= 1000 && data[i].OptimizedMaterials < 10000) {
                            yMax = data[i].OptimizedMaterials + 1000;
                        } else if (data[i].OptimizedMaterials >= 10000 && data[i].OptimizedMaterials < 100000) {
                            yMax = data[i].OptimizedMaterials + 10000;
                        } else {
                            yMax = data[i].OptimizedMaterials + 100000;
                        }
                    }

                    if (data[i].SiteName != "") {
                        siteName = GetSiteName(data[i].SiteName);
                    } else {
                        siteName = $("#ddMaterialSavedSites option:selected").text();
                    }

                    //set the title
                    $("<div>").css("text-align", "center").css("padding-top", "15px").text(siteName).appendTo(currentTd);
                    $("<div>").css("width", "550px").attr("id", chartId).appendTo($(currentTd));

                    //render the chart
                    $.jqplot(chartId, [[data[i].OriginalMaterials, data[i].OptimizedMaterials]], {
                        seriesColors: ["#004CFF", "#00FF00"],
                        animate: !$.jqplot.use_excanvas,
                        seriesDefaults: {
                            renderer: $.jqplot.BarRenderer,
                            pointLabels: { show: true },
                            rendererOptions: {
                                fillToZero: true,
                                highlightMouseDown: true,
                                varyBarColor: true
                            }
                        },

                        axes: {
                            xaxis: {
                                renderer: $.jqplot.CategoryAxisRenderer,
                                ticks: ['Original Materials', 'Optimized Materials']
                            },
                            yaxis: {
                                pad: 1.05,
                                max: yMax,
                                min: 0,
                                tickOptions: { formatString: "$%'#.2f" }
                            }
                        }
                    });
                }
            }
        }
    });
}

function BuildPMOStatusChart(siteName, chartCount, chartData) {
    
    var chartId = "chart" + chartCount.toString();

    var currentTd;
    if (chartCount % 2 == 0) {
        currentTd = $("#tdChartRight");
    } else {
        currentTd = $("#tdChartLeft");
    }

    //set the title
    $("<div>").css("text-align", "center").css("padding-top", "15px").text(siteName).appendTo(currentTd);
    $("<div>").css("width", "550px").attr("id", chartId).appendTo($(currentTd));

    //render the chart
    $.jqplot(chartId, [[chartData.notstarted, chartData.optimized]], {
        seriesColors: ["#004CFF", "#00FF00"],
        animate: !$.jqplot.use_excanvas,
        seriesDefaults: {
            renderer: $.jqplot.BarRenderer,
            pointLabels: { show: true },
            rendererOptions: {
                fillToZero: true,
                highlightMouseDown: true,
                varyBarColor: true
            }
        },

        axes: {
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ['Not Started', 'Optimized']
            },
            yaxis: {
                pad: 1.05,
                max: chartData.yMax,
                min: 0,
                tickOptions: { formatString: "%'d" }
            }
        }
    });
}

function GetPMOStatusCounts(siteId, assetClassId) {

    $.ajax({
        type: "POST",
        cache: false, 
        url: "../dashboard/dashboardwebdata.aspx/GetPMOStatusCounts",
        data: "{'siteId':" + siteId + ",'assetClassId':" + assetClassId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {

            if (response.d != null && response.d != "") {

                var chartData = { notstarted: 0, inprogress: 0, optimized: 0, yMax: 800 };
                var data = $.parseJSON(response.d);
                var chartCount = 0;
                var siteName;
                
                //spin through the data array
                for (var i = 0; i < data.length; i++) {

                    //if we have all the counts,
                    if ((i != 0 && data[i - 1].Site != data[i].Site)) {

                        siteName =  GetSiteName(data[i - 1].Site);
                        chartCount++;
                        BuildPMOStatusChart(siteName, chartCount, chartData);

                        chartData.notstarted = 0;
                        chartData.inprogress = 0;
                        chartData.optimized = 0;
                        chartData.yMax = 800;
                    }

                    BuildChartData(data[i], chartData);
                }

                siteName = GetSiteName(data[data.length - 1].Site);
                chartCount++;
                BuildPMOStatusChart(siteName, chartCount, chartData);
            }
        }
    });
}