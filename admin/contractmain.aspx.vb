

'********************************************************
'*
'********************************************************



Imports System.Data.sqlclient
Public Class contractmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang163 As System.Web.UI.WebControls.Label

	Protected WithEvents lang162 As System.Web.UI.WebControls.Label

	Protected WithEvents lang161 As System.Web.UI.WebControls.Label

	Protected WithEvents lang160 As System.Web.UI.WebControls.Label

	Protected WithEvents lang159 As System.Web.UI.WebControls.Label

	Protected WithEvents lang158 As System.Web.UI.WebControls.Label

	Protected WithEvents lang157 As System.Web.UI.WebControls.Label

	Protected WithEvents lang156 As System.Web.UI.WebControls.Label

	Protected WithEvents lang155 As System.Web.UI.WebControls.Label

	Protected WithEvents lang154 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, ro As String
    Dim svc As New Utilities
    Protected WithEvents ifsc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgstat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldcontract As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifcraft As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Tr5 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtcontract As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtvcont As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtvend As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtvdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txteq As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdetails As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldragid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrowid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                imgadd.Attributes.Add("onclick", "")
                Img1.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                Img1.Attributes.Add("onclick", "")
            End If
            lblpgstat.Value = "add"
        Else
            If Request.Form("lblsubmit") = "sc" Then
                svc.Open()
                GetContract()
                lblpgstat.Value = "edit"
                svc.Dispose()
            ElseIf Request.Form("lblsubmit") = "addsc" Then
                svc.Open()
                AddContract()
                svc.Dispose()
            ElseIf Request.Form("lblsubmit") = "savesc" Then
                svc.Open()
                SaveContract()
                svc.Dispose()
            End If
            lblsubmit.Value = ""
        End If
    End Sub
    Private Sub SaveContract()
        Dim ncont, ocont As String
        Dim cdesc, vend, eq, vcont, sdets, ldets As String
        Dim cnt As Integer
        ncont = txtcontract.Text '= dr.Item("contract").ToString
        ncont = svc.ModString2(ncont)
        ocont = lbloldcontract.Value '= dr.Item("contract").ToString
        ocont = svc.ModString1(ocont)
        If Len(ncont) > 50 Then
            Dim strMessage As String = "Contract Name Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'len > 12
            Exit Sub
        Else
            If ncont <> ocont Then
                sql = "select count(*) from servicecontract where contract = '" & ncont & "'"
                cnt = svc.Scalar(sql)
            Else
                cnt = 0
            End If
            If cnt = 0 Then
                cdesc = txtcdesc.Text '= dr.Item("description").ToString
                cdesc = svc.ModString2(cdesc)
                If Len(cdesc) > 50 Then
                    Dim strMessage As String = "Contract Description Limited to 50 Characters"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    'len > 50
                    Exit Sub
                End If
                vend = txtvend.Text '= dr.Item("vendor").ToString
                eq = txteq.Text '= dr.Item("eqnum").ToString
                vcont = txtvcont.Text '= dr.Item("vendorscontractnum").ToString
                vcont = svc.ModString2(vcont)
                If Len(vcont) > 50 Then
                    Dim strMessage As String = "Vendor Reference# Limited to 50 Characters"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    'len > 12
                    Exit Sub
                End If
                ldets = txtdetails.Text

                'update sc
                Dim cmd As New SqlCommand("exec usp_SaveSC @ocont, @ncont, @cdesc, @vend, @eqnum, @vcont")
                Dim param0 = New SqlParameter("@ocont", SqlDbType.VarChar)
                If ocont <> "" Then
                    param0.Value = ocont
                Else
                    param0.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param0)

                Dim param01 = New SqlParameter("@ncont", SqlDbType.VarChar)
                param01.Value = ncont
                cmd.Parameters.Add(param01)
                Dim param1 = New SqlParameter("@cdesc", SqlDbType.VarChar)
                If cdesc <> "" Then
                    param1.Value = cdesc
                Else
                    param1.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param1)
                Dim param4 = New SqlParameter("@vend", SqlDbType.VarChar)
                If vend <> "" Then
                    param4.Value = vend
                Else
                    param4.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param4)
                Dim param6 = New SqlParameter("@eqnum", SqlDbType.VarChar)
                param6.Value = eq
                cmd.Parameters.Add(param6)
                Dim param7 = New SqlParameter("@vcont", SqlDbType.VarChar)
                If vcont <> "" Then
                    param7.Value = vcont
                Else
                    param7.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param7)
                svc.UpdateHack(cmd)

                'update details
                If ldets <> "" Then
                    sql = "usp_genwebldkeySC '" & ldets & "','" & ncont & "'"
                    svc.Update(sql)
                End If

                lblsc.Value = ncont
                GetContract()
            Else
                Dim strMessage As String = "This Contract is Already in the List"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'cnt > 0
                Exit Sub
            End If
        End If

    End Sub
    Private Sub AddContract()
        Dim ncont, ocont As String
        Dim cdesc, vend, eq, vcont, sdets, ldets As String
        ncont = txtcontract.Text '= dr.Item("contract").ToString
        ncont = svc.ModString2(ncont)
        ocont = lbloldcontract.Value '= dr.Item("contract").ToString
        ocont = svc.ModString2(ocont)
        If Len(ncont) > 12 Then
            Dim strMessage As String = "Contract Name Limited to 12 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'len > 12
            Exit Sub
        Else
            If ncont <> ocont Then
                Dim cnt As Integer
                sql = "select count(*) from servicecontract where contract = '" & ncont & "'"
                cnt = svc.Scalar(sql)
                If cnt = 0 Then
                    cdesc = txtcdesc.Text '= dr.Item("description").ToString
                    cdesc = svc.ModString2(cdesc)
                    If Len(cdesc) > 50 Then
                        Dim strMessage As String = "Contract Description Limited to 50 Characters"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        'len > 50
                        Exit Sub
                    End If
                    vend = txtvend.Text '= dr.Item("vendor").ToString
                    eq = txteq.Text '= dr.Item("eqnum").ToString
                    vcont = txtvcont.Text '= dr.Item("vendorscontractnum").ToString
                    vcont = svc.ModString2(vcont)
                    If Len(vcont) > 50 Then
                        Dim strMessage As String = "Vendor Reference# Limited to 50 Characters"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        'len > 12
                        Exit Sub
                    End If
                    ldets = txtdetails.Text

                    'update sc
                    Dim cmd As New SqlCommand("exec usp_InsertSC @ocont, @ncont, @cdesc, @vend, @eqnum, @vcont")
                    Dim param0 = New SqlParameter("@ocont", SqlDbType.VarChar)
                    If ocont <> "" Then
                        param0.Value = ocont
                    Else
                        param0.Value = System.DBNull.Value
                    End If
                    cmd.Parameters.Add(param0)

                    Dim param01 = New SqlParameter("@ncont", SqlDbType.VarChar)
                    param01.Value = ncont
                    cmd.Parameters.Add(param01)
                    Dim param1 = New SqlParameter("@cdesc", SqlDbType.VarChar)
                    If cdesc <> "" Then
                        param1.Value = cdesc
                    Else
                        param1.Value = System.DBNull.Value
                    End If
                    cmd.Parameters.Add(param1)
                    Dim param4 = New SqlParameter("@vend", SqlDbType.VarChar)
                    If vend <> "" Then
                        param4.Value = vend
                    Else
                        param4.Value = System.DBNull.Value
                    End If
                    cmd.Parameters.Add(param4)
                    Dim param6 = New SqlParameter("@eqnum", SqlDbType.VarChar)
                    param6.Value = eq
                    cmd.Parameters.Add(param6)
                    Dim param7 = New SqlParameter("@vcont", SqlDbType.VarChar)
                    If vcont <> "" Then
                        param7.Value = vcont
                    Else
                        param7.Value = System.DBNull.Value
                    End If
                    cmd.Parameters.Add(param7)
                    svc.UpdateHack(cmd)

                    'update details
                    If ldets <> "" Then
                        sql = "usp_genwebldkeySC '" & ldets & "','" & ncont & "'"
                        svc.Update(sql)
                    End If
                    lblsc.Value = ncont
                    GetContract()
                Else
                    Dim strMessage As String = "This Contract is Already in the List"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    'cnt > 0
                    Exit Sub
                End If
            Else
                Dim strMessage As String = "This Contract is Already in the List"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'cnt > 0
                Exit Sub
            End If
        End If


    End Sub
    Private Sub GetContract()
        Dim sc As String = lblsc.Value
        sc = svc.ModString2(sc)
        Dim cnt As Integer
        sql = "select count(*) from servicecontract where contract = '" & sc & "'"
        cnt = svc.Scalar(sql)
        If cnt <> 0 Then
            sql = "select s.*, l.ldtext, c.name, e.eqdesc as edesc from servicecontract s " _
                   + "left join longdescription l on l.ldkey = s.ldkey " _
                   + "left join invcompanies c on c.company = s.vendor " _
                   + "left join equipment e on e.eqnum = s.eqnum " _
                   + "where s.contract = '" & sc & "'"
            dr = svc.GetRdrData(sql)
            While dr.Read
                txtcontract.Text = dr.Item("contract").ToString
                lbloldcontract.Value = dr.Item("contract").ToString
                lblsc.Value = dr.Item("contract").ToString
                txtcdesc.Text = dr.Item("description").ToString
                txtdetails.Text = dr.Item("ldtext").ToString
                txtvend.Text = dr.Item("vendor").ToString
                txtvdesc.Text = dr.Item("name").ToString
                txteq.Text = dr.Item("eqnum").ToString
                txteqdesc.Text = dr.Item("edesc").ToString
                txtvcont.Text = dr.Item("vendorscontract").ToString
            End While
            dr.Close()
        Else
            Dim strMessage As String = "No Record Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'cnt > 0
            Exit Sub
        End If
       
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang154.Text = axlabs.GetASPXPage("contractmain.aspx", "lang154")
        Catch ex As Exception
        End Try
        Try
            lang155.Text = axlabs.GetASPXPage("contractmain.aspx", "lang155")
        Catch ex As Exception
        End Try
        Try
            lang156.Text = axlabs.GetASPXPage("contractmain.aspx", "lang156")
        Catch ex As Exception
        End Try
        Try
            lang157.Text = axlabs.GetASPXPage("contractmain.aspx", "lang157")
        Catch ex As Exception
        End Try
        Try
            lang158.Text = axlabs.GetASPXPage("contractmain.aspx", "lang158")
        Catch ex As Exception
        End Try
        Try
            lang159.Text = axlabs.GetASPXPage("contractmain.aspx", "lang159")
        Catch ex As Exception
        End Try
        Try
            lang160.Text = axlabs.GetASPXPage("contractmain.aspx", "lang160")
        Catch ex As Exception
        End Try
        Try
            lang161.Text = axlabs.GetASPXPage("contractmain.aspx", "lang161")
        Catch ex As Exception
        End Try
        Try
            lang162.Text = axlabs.GetASPXPage("contractmain.aspx", "lang162")
        Catch ex As Exception
        End Try
        Try
            lang163.Text = axlabs.GetASPXPage("contractmain.aspx", "lang163")
        Catch ex As Exception
        End Try

    End Sub

End Class
