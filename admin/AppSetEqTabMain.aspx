<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetEqTabMain.aspx.vb"
    Inherits="lucy_r12.AppSetEqTabMain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetEqTabMain</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .details
        {
            display: none;
            visibility: hidden;
            font-family: Verdana;
            background-color: white;
        }
    </style>
    <script language="javascript" type="text/javascript" src="../scripts/eqtab.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetEqTabMainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        var divflg;
        
        function etab() {
            var cd = document.getElementById("lblcid").value;
            var sd = document.getElementById("lblsid").value;
            document.getElementById("ifeq").src = "AppSetEqTab.aspx";
            document.getElementById("txttab").value = "eq";
            document.getElementById("eq").className = "thdrhov plainlabel";
            document.getElementById("asdiv").style.display = "none";
            document.getElementById("asdiv").style.visibility = "hidden";
            document.getElementById("fdiv").style.display = "none";
            document.getElementById("fdiv").style.visibility = "hidden";
            document.getElementById("eqdiv").style.display = 'block';
            document.getElementById("eqdiv").style.visibility = 'visible';
            document.getElementById("ccdiv").style.display = 'none';
            document.getElementById("ccdiv").style.visibility = 'hidden';
            document.getElementById("ocdiv").style.display = 'none';
            document.getElementById("ocdiv").style.visibility = 'hidden';
            document.getElementById("ecdiv").style.display = 'none';
            document.getElementById("ecdiv").style.visibility = 'hidden';
            document.getElementById("mddiv").style.display = 'none';
            document.getElementById("mddiv").style.visibility = 'hidden';
        }
        function atab() {
            var ro = document.getElementById("lblro").value;
            var cd = document.getElementById("lblcid").value;
            var sd = document.getElementById("lblsid").value;
            document.getElementById("ifas").src = "AppSetAssetClass.aspx?cid=" + cd + "&ro=" + ro;
            document.getElementById("txttab").value = "as";
            document.getElementById("as").className = "thdrhov plainlabel";
            document.getElementById("eqdiv").style.display = "none";
            document.getElementById("eqdiv").style.visibility = "hidden";
            document.getElementById("fdiv").style.display = "none";
            document.getElementById("fdiv").style.visibility = "hidden";
            document.getElementById("asdiv").style.display = 'block';
            document.getElementById("asdiv").style.visibility = 'visible';
            document.getElementById("ccdiv").style.display = 'none';
            document.getElementById("ccdiv").style.visibility = 'hidden';
            document.getElementById("ocdiv").style.display = 'none';
            document.getElementById("ocdiv").style.visibility = 'hidden';
            document.getElementById("ecdiv").style.display = 'none';
            document.getElementById("ecdiv").style.visibility = 'hidden';
            document.getElementById("mddiv").style.display = 'none';
            document.getElementById("mddiv").style.visibility = 'hidden';
        }
        function ctab() {
            var ro = document.getElementById("lblro").value;
            var cd = document.getElementById("lblcid").value;
            var sd = document.getElementById("lblsid").value;
            document.getElementById("ifcc").src = "../complib/compclassmain.aspx?cid=" + cd + "&ro=" + ro;
            document.getElementById("txttab").value = "as";
            document.getElementById("cc").className = "thdrhov plainlabel";
            document.getElementById("eqdiv").style.display = "none";
            document.getElementById("eqdiv").style.visibility = "hidden";
            document.getElementById("fdiv").style.display = "none";
            document.getElementById("fdiv").style.visibility = "hidden";
            document.getElementById("asdiv").style.display = 'none';
            document.getElementById("asdiv").style.visibility = 'hidden';
            document.getElementById("ccdiv").style.display = 'block';
            document.getElementById("ccdiv").style.visibility = 'visible';
            document.getElementById("ocdiv").style.display = 'none';
            document.getElementById("ocdiv").style.visibility = 'hidden';
            document.getElementById("ecdiv").style.display = 'none';
            document.getElementById("ecdiv").style.visibility = 'hidden';
            document.getElementById("mddiv").style.display = 'none';
            document.getElementById("mddiv").style.visibility = 'hidden';
        }
        function ftab() {
            var cd = document.getElementById("lblcid").value;
            var sd = document.getElementById("lblsid").value;
            var coi = document.getElementById("lblcoi").value
            var cadm = document.getElementById("lblcadm").value
            var uid = document.getElementById("lbluid").value
            //alert (uid)
            if (coi == "AGR" || coi == "GLA") {
                //if (cdam == "1" || uid == "pmadmin1") {
                    document.getElementById("iff").src = "dualfailtab.aspx?cid=" + cd + "&sid=" + sd;
                //}
                }               
            else if (coi != "CAS" && coi != "AGR") {
                document.getElementById("iff").src = "AppSetComTab.aspx?cid=" + cd + "&sid=" + sd;
                }

            //document.getElementById("iff").src = "AppSetComTab.aspx?cid=" + cd + "&sid=" + sd;
            //AppSetComTab
            document.getElementById("txttab").value = "fa";
            document.getElementById("fa").className = "thdrhov plainlabel";
            document.getElementById("eqdiv").style.display = "none";
            document.getElementById("eqdiv").style.visibility = "hidden";
            document.getElementById("asdiv").style.display = "none";
            document.getElementById("asdiv").style.visibility = "hidden";
            document.getElementById("fdiv").style.display = 'block';
            document.getElementById("fdiv").style.visibility = 'visible';
            document.getElementById("ccdiv").style.display = 'none';
            document.getElementById("ccdiv").style.visibility = 'hidden';
            document.getElementById("ocdiv").style.display = 'none';
            document.getElementById("ocdiv").style.visibility = 'hidden';
            document.getElementById("ecdiv").style.display = 'none';
            document.getElementById("ecdiv").style.visibility = 'hidden';
            document.getElementById("mddiv").style.display = 'none';
            document.getElementById("mddiv").style.visibility = 'hidden';
        }
        
        function otab() {
            var cd = document.getElementById("lblcid").value;
            var sd = document.getElementById("lblsid").value;
            document.getElementById("ifoc").src = "appsetocareas.aspx?cid=" + cd + "&sid=" + sd;
            document.getElementById("txttab").value = "md";
            document.getElementById("oc").className = "thdrhov plainlabel";
            document.getElementById("eqdiv").style.display = "none";
            document.getElementById("eqdiv").style.visibility = "hidden";
            document.getElementById("asdiv").style.display = "none";
            document.getElementById("asdiv").style.visibility = "hidden";
            document.getElementById("fdiv").style.display = 'none';
            document.getElementById("fdiv").style.visibility = 'hidden';
            document.getElementById("ocdiv").style.display = 'block';
            document.getElementById("ocdiv").style.visibility = 'visible';
            document.getElementById("ccdiv").style.display = 'none';
            document.getElementById("ccdiv").style.visibility = 'hidden';
            document.getElementById("ecdiv").style.display = 'none';
            document.getElementById("ecdiv").style.visibility = 'hidden';
            document.getElementById("mddiv").style.display = 'none';
            document.getElementById("mddiv").style.visibility = 'hidden';
        }
        function ectab() {
            var cd = document.getElementById("lblcid").value;
            document.getElementById("ifec").src = "../equip/ecadmin.aspx";
            document.getElementById("txttab").value = "ec";
            document.getElementById("ec").className = "thdrhov plainlabel";
            document.getElementById("eqdiv").style.display = "none";
            document.getElementById("eqdiv").style.visibility = "hidden";
            document.getElementById("asdiv").style.display = "none";
            document.getElementById("asdiv").style.visibility = "hidden";
            document.getElementById("fdiv").style.display = 'block';
            document.getElementById("fdiv").style.visibility = 'hidden';
            document.getElementById("ocdiv").style.display = 'none';
            document.getElementById("ocdiv").style.visibility = 'hidden';
            document.getElementById("ccdiv").style.display = 'none';
            document.getElementById("ccdiv").style.visibility = 'hidden';
            document.getElementById("ecdiv").style.display = 'block';
            document.getElementById("ecdiv").style.visibility = 'visible';
            document.getElementById("mddiv").style.display = 'none';
            document.getElementById("mddiv").style.visibility = 'hidden';
        }
        function mdtab() {
            var mdba = document.getElementById("lblmdba").value
            if (mdba == 1) {
                var cd = document.getElementById("lblcid").value;
                document.getElementById("ifmd").src = "appsetmdb.aspx";
                document.getElementById("txttab").value = "md";
                document.getElementById("md").className = "thdrhov plainlabel";
                document.getElementById("eqdiv").style.display = "none";
                document.getElementById("eqdiv").style.visibility = "hidden";
                document.getElementById("asdiv").style.display = "none";
                document.getElementById("asdiv").style.visibility = "hidden";
                document.getElementById("fdiv").style.display = 'block';
                document.getElementById("fdiv").style.visibility = 'hidden';
                document.getElementById("ocdiv").style.display = 'none';
                document.getElementById("ocdiv").style.visibility = 'hidden';
                document.getElementById("ccdiv").style.display = 'none';
                document.getElementById("ccdiv").style.visibility = 'hidden';
                document.getElementById("ecdiv").style.display = 'none';
                document.getElementById("ecdiv").style.visibility = 'hidden';
                document.getElementById("mddiv").style.display = 'block';
                document.getElementById("mddiv").style.visibility = 'visible';
            }
        }

        function gettab(id) {
            document.getElementById("eq").className = "thdr plainlabel";
            document.getElementById("as").className = "thdr plainlabel";
            document.getElementById("fa").className = "thdr plainlabel";
            document.getElementById("cc").className = "thdr plainlabel";
            document.getElementById("oc").className = "thdr plainlabel";
            var coi = document.getElementById("lblcoi").value
            var cadm = document.getElementById("lblcadm").value
            //alert(coi)
            if (coi == "CAS") {
                document.getElementById("fa").className = "details";
            }
            /*else if (coi = "CAS" & cadm == 1){
            document.getElementById("fa").className = "thdr plainlabel";
            }*/
            var mdba = document.getElementById("lblmdba").value
            if (mdba == 1) {
                document.getElementById("md").className = "thdr plainlabel";
            }
            var isecd = document.getElementById("lblisecd").value;
            if (isecd == "1") {
                document.getElementById("ec").className = "thdr plainlabel";
            }
            if (id == "eq") {
                etab();
            }
            else if (id == "as") {
                atab();
            }
            else if (id == "cc") {
                ctab();
            }
            else if (id == "fa") {
                ftab();
            }
            else if (id == "oc") {
                otab();
            }
            else if (id == "ec") {
                ectab();
            }
            else if (id == "md") {
                mdtab();
            }

        }
        function checktab() {
            var app = document.getElementById("appchk").value;
            if (app == "switch") window.parent.handleapp(app);
            divflg = document.getElementById("txttab").value
            gettab(divflg);
        }
        function handledid(id) {
            document.getElementById("lbldid").value = id;
        }
        //-->
    </script>
</head>
<body onload="checktab();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div id="eqdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; background-color: transparent; width: 1000px; height: 420px;
        visibility: visible; border-top: 2px groove; top: 32px; border-right: 2px groove;
        left: 5px">
        <iframe id="ifeq" style="background-color: transparent; width: 1000px; height: 420px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="asdiv" style="border-style: groove; border-color: inherit; border-width: 2px;
        z-index: 1; position: absolute; background-color: transparent; width: 1000px;
        height: 420px; visibility: visible; left: 5px;top: 32px;">
        <iframe id="ifas" style="background-color: transparent; width: 1000px; height: 420px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="ccdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; background-color: transparent; width: 1000px; height: 420px;
        visibility: visible; border-top: 2px groove; top: 32px; border-right: 2px groove;
        left: 5px">
        <iframe id="ifcc" style="background-color: transparent; width: 1000px; height: 420px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="fdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; background-color: transparent; width: 1000px; height: 420px;
        visibility: hidden; border-top: 2px groove; top: 32px; border-right: 2px groove;
        left: 6px">
        <iframe id="iff" style="background-color: transparent; width: 1000px; height: 420px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="ocdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; background-color: transparent; width: 1000px; height: 420px;
        visibility: hidden; border-top: 2px groove; top: 32px; border-right: 2px groove;
        left: 6px">
        <iframe id="ifoc" style="background-color: transparent; width: 1000px; height: 420px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="ecdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; background-color: transparent; width: 1000px; height: 420px;
        visibility: hidden; border-top: 2px groove; top: 32px; border-right: 2px groove;
        left: 6px">
        <iframe id="ifec" style="background-color: transparent; width: 1000px; height: 420px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="mddiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; background-color: transparent; width: 1000px; height: 420px;
        visibility: hidden; border-top: 2px groove; top: 32px; border-right: 2px groove;
        left: 6px">
        <iframe id="ifmd" style="background-color: transparent; width: 1000px; height: 420px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <table style="position: absolute; top: 10px; left: 5px" cellspacing="3" cellpadding="3">
        <tr>
            <td id="eq" onclick="gettab('eq');" class="thdrhov plainlabel" width="130" height="20">
                <asp:Label ID="lang72" runat="server">Equipment Status</asp:Label>
            </td>
            <td id="as" onclick="gettab('as');" class="thdr plainlabel" width="130">
                <asp:Label ID="lang73" runat="server">Asset Class</asp:Label>
            </td>
            <td id="cc" onclick="gettab('cc');" class="thdr plainlabel" width="130">
                <asp:Label ID="lang74" runat="server">Component Class</asp:Label>
            </td>
            <td id="fa" runat="server" onclick="gettab('fa');" class="thdr plainlabel" width="130">
                <asp:Label ID="lang75" runat="server">Failure Modes</asp:Label>
            </td>
            <td id="oc" onclick="gettab('oc');" class="thdr plainlabel" width="130">
                <asp:Label ID="Label1" runat="server">Occurrence Areas</asp:Label>
            </td>
            <td id="ec" runat="server" onclick="gettab('ec');" class="thdr plainlabel" width="130">
                <asp:Label ID="Label2" runat="server">Error Codes</asp:Label>
            </td>
            <td id="md" runat="server" onclick="gettab('md');" class="thdr plainlabel" width="130">
                <asp:Label ID="Label3" runat="server">Multiple Databases</asp:Label>
            </td>
        </tr>
    </table>
    <input type="hidden" id="txttab" runat="server" name="appchk" />
    <input type="hidden" id="lbltab" runat="server" name="appchk" />
    <input type="hidden" id="lblcid" runat="server" name="appchk" />
    <input type="hidden" id="lbltl" runat="server" name="appchk" />
    <input type="hidden" id="appchk" runat="server" name="appchk" /><input type="hidden"
        id="lblsid" runat="server" name="lblsid" />
    <input type="hidden" id="lbldid" runat="server" name="lbldid" /><input type="hidden"
        id="lblro" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblisecd" runat="server" />
    <input type="hidden" id="lblmdba" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lbluid" runat="server" />
    </form>
    <div>
    </div>
</body>
</html>
