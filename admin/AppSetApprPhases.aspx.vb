

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetApprPhases
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pp As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblans As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql As String
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rev As Integer
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login, ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgphases As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblprojid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchanges As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim pid, jump, ans, cnt As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgphases.Columns(0).Visible = False
                dgphases.Columns(5).Visible = False
            End If
            jump = Request.QueryString("jump").ToString
            ans = Request.QueryString("ans").ToString
            cnt = Request.QueryString("cnt").ToString
            rev = Request.QueryString("rev").ToString
            lblans.Value = ans
            lbleqcnt.Value = cnt
            If jump = "yes" Then
                pid = Request.QueryString("pid").ToString
                lblprojid.Value = pid '"1"
            Else
                lblprojid.Value = "0"
            End If

            pp.Open()
            If rev = 0 Then
                rev = pp.Rev()
            End If
            lblrev.Value = rev
            LoadPhases()
            pp.Dispose()
        End If
    End Sub
    Private Sub LoadPhases()
        rev = lblrev.Value
        sql = "select a.*, b.grpname from pmApproval a left outer join pmApprovalGrps b on a.apprgrp = b.apprgrp " _
        + "where a.rev = '" & rev & "' order by a.listorder"
        Dim ds As New DataSet
        ds = pp.GetDSData(sql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Try
            dgphases.DataSource = dv
            dgphases.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Public Function PopulateAG() As DataSet
        rev = lblrev.Value
        Dim dslev As DataSet
        sql = "select apprgrp, grpname from pmApprovalGrps where rev = '" & rev & "' ORDER BY GRPLEVEL"
        dslev = pp.GetDSData(sql)
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub dgphases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.ItemCommand
        Dim ph, des, ci, cl, ans, cnt As String
        Dim newph As Integer
        Dim tdes As TextBox
        Dim dd As DropDownList
        rev = lblrev.Value
        If e.CommandName = "Reset" Or e.CommandName = "Add" Then
            ph = CType(e.Item.FindControl("txtaphase"), TextBox).Text
            ph = pp.ModString2(ph)
            dd = CType(e.Item.FindControl("ddaag"), DropDownList)
            ci = dd.SelectedIndex
            cl = dd.SelectedValue
            tdes = CType(e.Item.FindControl("txtaphase"), TextBox)
            des = tdes.Text
        End If
        If e.CommandName = "Reset" Then
            tdes.Text = ""
            Try
                dd.SelectedIndex = 0
            Catch ex As Exception

            End Try

        End If
        If e.CommandName = "Add" Then
            Dim phcnt As Integer
            pp.Open()
            Try
                sql = "select count(*) from pmApproval where rev = '" & rev & "'"
                phcnt = pp.Scalar(sql)
                phcnt = phcnt + 1
                sql = "insert into pmApproval " _
                + "(listorder, listitem, apprgrp, rev) values ('" & phcnt & "', '" & ph & "', '" & cl & "', '" & rev & "') " _
                + "select @@identity as 'identity'"
                'pp.Update(sql)
                newph = pp.Scalar(sql)
                lblchanges.Value = "yes"
                ans = lblans.Value
                If ans <> "0" Then
                    CheckAns()
                End If
                cnt = lbleqcnt.Value
                If cnt = "1" Then
                    CheckEq(newph)
                End If
            Catch ex As Exception
            End Try
            LoadPhases()
            pp.Dispose()
        End If
    End Sub
    Private Sub CheckEq(ByVal newph As Integer)
        sql = "usp_updateEqAppr 'phs', '" & newph & "'"
        pp.Update(sql)
    End Sub
    Private Sub CheckAns()
        Dim ans As String = lblans.Value
        If ans = "1" Then
            sql = "update equipment set apprstg2prev = 1 where apprstg2 = 1"
            pp.Update(sql)
        ElseIf ans = "2" Then
            sql = "update equipment set apprstg2 = 0 where apprstg2 = 1 and apprstg2prev <> 1"
            pp.Update(sql)
        End If
    End Sub
    Private Sub dgphases_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.EditCommand
        dgphases.EditItemIndex = e.Item.ItemIndex
        lbloldtask.Value = CType(e.Item.FindControl("A1"), Label).Text
        pp.Open()
        LoadPhases()
        pp.Dispose()
    End Sub

    Private Sub dgphases_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.CancelCommand
        dgphases.EditItemIndex = -1
        pp.Open()
        LoadPhases()
        pp.Dispose()
    End Sub

    Private Sub dgphases_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.UpdateCommand
        Dim otn, ntn, ph, ci, cl, pid, ans, cnt As String
        rev = lblrev.Value
        Dim dd As DropDownList
        otn = lbloldtask.Value
        ntn = CType(e.Item.FindControl("txtlo"), TextBox).Text
        pid = CType(e.Item.FindControl("lblpmaid"), Label).Text
        ph = CType(e.Item.FindControl("txtephase"), TextBox).Text
        ph = pp.ModString2(ph)
        dd = CType(e.Item.FindControl("ddeag"), DropDownList)
        ci = dd.SelectedIndex
        cl = dd.SelectedValue
        Try
            ntn = System.Convert.ToInt64(ntn)
            pp.Open()
            If Len(ph) <> 0 Then
                sql = "update pmApproval set listitem = '" & ph & "', " _
                + "apprgrp = '" & cl & "' " _
                + "where pmaid = '" & pid & "'"
                pp.Update(sql)
                lblchanges.Value = "yes"
                ans = lblans.Value
                If ans <> "0" Then
                    CheckAns()
                End If
                cnt = lbleqcnt.Value
                If cnt = "1" Then
                    sql = "update equipment set apprstg1 = 0"
                    pp.Update(sql)
                End If
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr5" , "AppSetApprPhases.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

            If otn <> ntn Then
                sql = "usp_reorderPMApprPhs '" & ntn & "', '" & otn & "', '" & rev & "'"
                pp.Update(sql)
                If cnt = "1" Then
                    sql = "usp_reorderPMApprEq 'phs'"
                    pp.Update(sql)
                End If
            End If
            dgphases.EditItemIndex = -1
            LoadPhases()
            pp.Dispose()

        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr6" , "AppSetApprPhases.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try

    End Sub

    Private Sub dgphases_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.DeleteCommand
        Dim tnum, pmaid, ans, cnt
        rev = lblrev.Value
        Try
            tnum = CType(e.Item.FindControl("A1"), Label).Text
            pmaid = CType(e.Item.FindControl("lblpmaida"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("txtlo"), TextBox).Text
            pmaid = CType(e.Item.FindControl("lblpmaid"), TextBox).Text
        End Try
        sql = "usp_delPMApprPhs '" & pmaid & "', '" & tnum & "', '" & rev & "'"
        pp.Open()
        pp.Update(sql)
        ans = lblans.Value
        If ans <> "0" Then
            CheckAns()
        End If
        cnt = lbleqcnt.Value
        If cnt = "1" Then
            sql = "usp_delPMApprEqPhs '" & pmaid & "'"
            pp.Update(sql)
        End If
        lblchanges.Value = "yes"
        dgphases.EditItemIndex = -1
        LoadPhases()
        pp.Dispose()
    End Sub

    Private Sub dgphases_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgphases.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                         e.Item.ItemType <> ListItemType.Footer Then
            Try
                Dim jump As HtmlAnchor = CType(e.Item.FindControl("lnkphase"), HtmlAnchor)
                Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmaid").ToString
                Dim grp As String = DataBinder.Eval(e.Item.DataItem, "listitem").ToString
                jump.Attributes("onclick") = "jump('" & id & "', '" & grp & "');"
            Catch ex As Exception

            End Try
        End If
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgphases.Columns(0).HeaderText = dlabs.GetDGPage("AppSetApprPhases.aspx","dgphases","0")
		Catch ex As Exception
		End Try
		Try
			dgphases.Columns(2).HeaderText = dlabs.GetDGPage("AppSetApprPhases.aspx","dgphases","2")
		Catch ex As Exception
		End Try
		Try
			dgphases.Columns(3).HeaderText = dlabs.GetDGPage("AppSetApprPhases.aspx","dgphases","3")
		Catch ex As Exception
		End Try
		Try
			dgphases.Columns(4).HeaderText = dlabs.GetDGPage("AppSetApprPhases.aspx","dgphases","4")
		Catch ex As Exception
		End Try
		Try
			dgphases.Columns(5).HeaderText = dlabs.GetDGPage("AppSetApprPhases.aspx","dgphases","5")
		Catch ex As Exception
		End Try

	End Sub

End Class
