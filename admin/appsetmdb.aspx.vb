﻿Imports System.Data.SqlClient
Public Class appsetmdb
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim Sort As String
    Dim Login As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            appset.Open()
            loaddgfail()

            appset.Dispose()
        End If
    End Sub
    Private Sub loaddgfail()
        sql = "select * from mdb"
        dr = appset.GetRdrData(sql)
        dgfail.DataSource = dr
        Try
            dgfail.DataBind()
        Catch ex As Exception
            dgfail.CurrentPageIndex = 0
            dgfail.DataBind()
        End Try
        dr.Close()
    End Sub

    Private Sub dgfail_CancelCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.CancelCommand
        dgfail.EditItemIndex = -1
        appset.Open()
        loaddgfail()
        appset.Dispose()
    End Sub

    Private Sub dgfail_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            'id = CType(e.Item.Cells(1).Controls(1), Label).Text
            id = CType(e.Item.FindControl("Label13a"), Label).Text
        Catch ex As Exception
            'id = CType(e.Item.Cells(1).Controls(2), TextBox).Text
            id = CType(e.Item.FindControl("Textbox9"), TextBox).Text
        End Try
        sql = "delete from mdb where mdbid = '" & id & "'"
        appset.Update(sql)

        loaddgfail()
        appset.Dispose()
    End Sub

    Private Sub dgfail_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.EditCommand
        dgfail.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        loaddgfail()
        appset.Dispose()
    End Sub

    Private Sub dgfail_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        Dim strchk As Integer = 0
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewdb"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            If lname <> "" Then
                If Len(lname) > 50 Then
                    Dim strMessage As String = "Database Name is Limited to 50 Characters"

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    Dim fail As String = lname

                    Dim faill As String = fail.ToLower
                    sql = "select count(*) from appsetmdb where lower(dbname) = '" & faill & "'"
                    strchk = appset.Scalar(sql)
                    If strchk = 0 Then
                        
                    Else
                        Dim strMessage As String = "Cannot Enter a Duplicate Database Name"

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        appset.Dispose()
                    End If
                    appset.Dispose()
                End If
            End If
        End If
    End Sub
End Class