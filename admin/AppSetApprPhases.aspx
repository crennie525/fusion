<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetApprPhases.aspx.vb"
    Inherits="lucy_r12.AppSetApprPhases" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetApprPhases</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetApprPhasesaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <asp:DataGrid ID="dgphases" Style="left: 0px; position: absolute; top: 3px" runat="server"
        AutoGenerateColumns="False" AllowCustomPaging="True" ShowFooter="True" Width="410px"
        CellSpacing="1" GridLines="None" BackColor="transparent">
        <FooterStyle Font-Size="X-Small" Font-Names="Arial"></FooterStyle>
        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
        <ItemStyle CssClass="ptransrow"></ItemStyle>
        <HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
        <Columns>
            <asp:TemplateColumn HeaderText="Edit">
                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                        CommandName="Edit"></asp:ImageButton>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                        CommandName="Reset"></asp:ImageButton>
                </FooterTemplate>
                <EditItemTemplate>
                    &nbsp;
                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                        CommandName="Update"></asp:ImageButton>
                    <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                        CommandName="Cancel"></asp:ImageButton>
                </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn Visible="False" HeaderText="ID">
                <HeaderStyle Width="50px"></HeaderStyle>
                <ItemTemplate>
                    <asp:Label ID="lblpmaida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmaid") %>'>
                    </asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label ID="lblpmaid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmaid") %>'>
                    </asp:Label>
                </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Order">
                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                <ItemTemplate>
                    <asp:Label ID="A1" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.listorder") %>'>
                    </asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtlo" runat="server" Width="30px" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.listorder") %>'>
                    </asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Approval Phase">
                <HeaderStyle Width="190px" CssClass="btmmenu plainlabel"></HeaderStyle>
                <ItemTemplate>
                    <a id="lnkphase" href="#" runat="server">
                        <%# DataBinder.Eval(Container, "DataItem.listitem") %>
                    </a>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtaphase" runat="server" CssClass="plainlabel" Width="180px" MaxLength="100"></asp:TextBox>
                </FooterTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtephase" runat="server" CssClass="plainlabel" Width="180px" Text='<%# DataBinder.Eval(Container, "DataItem.listitem") %>'
                        MaxLength="100">
                    </asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Approval Group">
                <HeaderStyle Width="110px" CssClass="btmmenu plainlabel"></HeaderStyle>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.grpname") %>'>
                    </asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList ID="ddaag" runat="server" CssClass="plainlabel" DataSource="<%# PopulateAG %>"
                        DataTextField="grpname" DataValueField="apprgrp">
                    </asp:DropDownList>
                </FooterTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="ddeag" runat="server" CssClass="plainlabel" DataSource="<%# PopulateAG %>"
                        DataTextField="grpname" DataValueField="apprgrp" SelectedIndex='<%# GetSelIndex(Container.DataItem("apprgrp")) %>'>
                    </asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Delete">
                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                        CommandName="Delete"></asp:ImageButton>
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                <FooterTemplate>
                    <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnew.gif"
                        CommandName="Add"></asp:ImageButton>
                </FooterTemplate>
            </asp:TemplateColumn>
        </Columns>
        <PagerStyle Visible="False"></PagerStyle>
    </asp:DataGrid>
    <input type="hidden" id="lblprojid" runat="server" name="lblprojid"><input type="hidden"
        id="lblchanges" runat="server" name="lblchanges">
    <input type="hidden" id="lbloldtask" runat="server"><input type="hidden" id="lblans"
        runat="server">
    <input type="hidden" id="lbleqcnt" runat="server">
    <input type="hidden" id="lblrev" runat="server">
    <input id="lbllog" type="hidden" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
