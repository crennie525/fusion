

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class logtrack
    Inherits System.Web.UI.Page
	Protected WithEvents lang165 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim rep As New Utilities
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents trlt As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents divlt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            txtpg.Value = "1"
            rep.Open()
            GetAll()
            rep.Dispose()
        Else
            If Request.Form("lblsubmit").ToString = "srchq" Then
                lblsubmit.Value = ""
                rep.Open()
                txtpg.Value = "1"
                GetAll()
                rep.Dispose()
            ElseIf Request.Form("lblsubmit").ToString = "reset" Then
                lblsubmit.Value = ""
                rep.Open()
                txtpg.Value = "1"
                GetAll()
                rep.Dispose()
            ElseIf Request.Form("lblsubmit") = "next" Then
                rep.Open()
                GetNext()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "last" Then
                rep.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetAll()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "prev" Then
                rep.Open()
                GetPrev()
                rep.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "first" Then
                rep.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetAll()
                rep.Dispose()
                lblsubmit.Value = ""
            End If
        End If
    End Sub
    Private Function ModString(ByVal str As String)
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        Return str
    End Function
    Private Sub GetAll()
        Dim sb As New StringBuilder
        Dim srch As String
        srch = lblsrch.Value
        srch = ModString(srch)
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Filter = "logintime > (select logtrack from pmsysvars) and app = ''" & appname & "'' "
        FilterCnt = "logintime > (select logtrack from pmsysvars) and app = '" & appname & "' "
        If srch <> "" Then
            Filter += "and (userid like ''%" & srch & "%'' or currentpage like ''%" & srch & "%'')"
            FilterCnt += "and (userid like '%" & srch & "%' or currentpage like '%" & srch & "%')"
        End If
        sql = "select Count(*) from logtrack where " & FilterCnt
        Dim dc As Integer = rep.PageCount(sql, PageSize) 'rep.Scalar(sql)
        If dc = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & dc
        End If

        txtpgcnt.Value = dc
        Tables = "logtrack"
        PK = "sessid"
        PageSize = "200"
        Sort = "logintime desc"
        Fields = "userid, logintime, currentpage, pageenter, logouttime"
        dr = rep.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        sb.Append("<table width=""650"">")
        sb.Append("<tr>")
        sb.Append("<td width=""130"" class=""thdrsing label"">" & tmod.getlbl("cdlbl11" , "logtrack.aspx.vb") & "</td>")
        sb.Append("<td width=""130"" class=""thdrsing label"">" & tmod.getlbl("cdlbl12" , "logtrack.aspx.vb") & "</td>")
        sb.Append("<td width=""130"" class=""thdrsing label"">" & tmod.getlbl("cdlbl13" , "logtrack.aspx.vb") & "</td>")
        sb.Append("<td width=""130"" class=""thdrsing label"">" & tmod.getlbl("cdlbl14" , "logtrack.aspx.vb") & "</td>")
        sb.Append("<td width=""150"" class=""thdrsing label"">" & tmod.getlbl("cdlbl15", "logtrack.aspx.vb") & "</td>")
        sb.Append("</tr>")

        Dim userid, logintime, currentpage, pageenter, logouttime As String
        While dr.Read
            userid = dr.Item("userid").ToString
            logintime = dr.Item("logintime").ToString
            currentpage = dr.Item("currentpage").ToString
            pageenter = dr.Item("pageenter").ToString
            logouttime = dr.Item("logouttime").ToString
            sb.Append("<tr><td class=""plainlabel"">")
            sb.Append(userid)
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"">" & logintime)
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"">" & currentpage)
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"">" & pageenter)
            sb.Append("</td>")
            sb.Append("<td class=""plainlabel"">" & logouttime)
            sb.Append("</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        divlt.InnerHtml = sb.ToString
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetAll()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr180" , "logtrack.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetAll()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr181" , "logtrack.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang165.Text = axlabs.GetASPXPage("logtrack.aspx", "lang165")
        Catch ex As Exception
        End Try

    End Sub

End Class
