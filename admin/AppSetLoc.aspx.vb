

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetLoc
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim dept As String
    Dim Sort As String = ""
    Dim cid, cnm, sid, did, sql, ro As String
    Dim dr As SqlDataReader
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbltasktype As System.Web.UI.WebControls.Label
    Protected WithEvents dgtt As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgtasktype As System.Web.UI.WebControls.Label
    Protected WithEvents ttPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ttNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ttdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtt.Columns(0).Visible = False
                dgtt.Columns(6).Visible = False
            End If

            cid = "0"
            If cid <> "" Then
                lblcid.Value = cid
                sid = HttpContext.Current.Session("dfltps").ToString()
                lblsid.Value = sid
                appset.Open()
                PopTasks(PageNumber)
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        End If
        'ttPrev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'ttPrev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'ttNext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'ttNext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    Public Function PopulateTypes() As DataSet
        Dim dslev As DataSet
        Dim appseta As New Utilities
        appseta.Open()
        cid = lblcid.Value
        sid = lblsid.Value
        sql = "select typid, loctype, locindex from pmLocTypes where (compid = '" & cid & "') or locindex = '0' order by locindex"
        dslev = appseta.GetDSData(sql)
        appseta.Dispose()
        Return dslev
    End Function
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Private Sub PopTasks(ByVal PageNumber As Integer)
        'Try
        Try
            appset.Open()
        Catch ex As Exception

        End Try
        cid = lblcid.Value
        sql = "select count(*) " _
        + "from pmloctypes where compid = '" & cid & "'"
        dgtt.VirtualItemCount = appset.Scalar(sql)
        If dgtt.VirtualItemCount = 0 Then
            lbltasktype.Text = "No Location Records Found"
            dgtt.Visible = True
            ttPrev.Visible = False
            ttNext.Visible = False

            Filter = "compid = " & cid '"compid = '" & cid & "'"
            Tables = "pmloctypes"
            PK = "typid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            appset.Dispose()

        Else
            Filter = "compid = " & cid '"compid = '" & cid & "'"
            Tables = "pmlocations"
            PK = "locid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            appset.Dispose()
            lblpgtasktype.Text = "Page " & dgtt.CurrentPageIndex + 1 & " of " & dgtt.PageCount
            lbltasktype.Text = ""
            ttPrev.Visible = True
            ttNext.Visible = True
            'checkFreqStatPgCnt()
        End If

        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub checkTTStatPgCnt()
        If (dgtt.CurrentPageIndex) = 0 Or dgtt.PageCount = 1 Then
            ttPrev.Enabled = False
        Else
            ttPrev.Enabled = True
        End If
        If dgtt.PageCount > 1 And (dgtt.CurrentPageIndex + 1 < dgtt.PageCount) Then
            ttNext.Enabled = True
        Else
            ttNext.Enabled = False
        End If
    End Sub

    Private Sub ttNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ttNext.Click
        If ((dgtt.CurrentPageIndex) < (dgtt.PageCount - 1)) Then
            dgtt.CurrentPageIndex = dgtt.CurrentPageIndex + 1
            PopTasks(dgtt.CurrentPageIndex + 1)
            checkTTStatPgCnt()
        End If
        checkTTStatPgCnt()
    End Sub

    Private Sub ttPrev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ttPrev.Click
        If (dgtt.CurrentPageIndex > 0) Then
            dgtt.CurrentPageIndex = dgtt.CurrentPageIndex - 1
            PopTasks(dgtt.CurrentPageIndex - 1)
        End If
        checkTTStatPgCnt()
    End Sub

    Private Sub dgtt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        Dim desc As String
        Dim desci As TextBox
        Dim typi As DropDownList
        Dim typ, typid As String
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewtt"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            desci = CType(e.Item.FindControl("txtdesc"), TextBox)
            desc = desci.Text
            desc = appset.ModString1(desc)
            typi = CType(e.Item.FindControl("ddtyp"), DropDownList)
            typ = typi.SelectedItem.ToString
            typid = typi.SelectedValue.ToString
            'desc = Replace(desc, "'", Chr(180), , , vbTextCompare)

            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr67" , "AppSetLoc.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        cid = lblcid.Value
                        sid = lblsid.Value
                        Dim tt As String
                        Dim strchk As Integer
                        Dim faill As String = lname.ToLower
                        sql = "select count(*) from pmlocations where lower(location) = '" & lname & "' and compid = '" & cid & "' and siteid = '" & sid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "insert into pmlocations (location, description, typid, type, compid, siteid) values " _
                            + "('" & lname & "','" & desc & "','" & typid & "','" & typ & "','" & cid & "','" & sid & "')"
                            appset.Update(sql)
                            Dim ttcnt As Integer = dgtt.VirtualItemCount + 1

                            lnamei.Text = ""
                            sql = "select Count(*) from pmlocations " _
                               + "where compid = '" & cid & "' and siteid = '" & sid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopTasks(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr68" , "AppSetLoc.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If

                    End If

                End If
            End If
        End If
    End Sub

    Private Sub dgtt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.DeleteCommand
        appset.Open()
        Dim id As String
        cid = lblcid.Value
        sid = lblsid.Value
        Try
            id = CType(e.Item.FindControl("lblttids"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblttide"), Label).Text
        End Try
        'id = CType(e.Item.Cells(1).Controls(1), Label).Text
        sql = "select count(*) from pmJobPlans where locid = '" & id & "'"
        Dim tcnt As Integer = appset.Scalar(sql)
        sql = "select count(*) from Equipment where locid = '" & id & "'"
        Dim tcnt1 As Integer = appset.Scalar(sql)
        If tcnt = 0 And tcnt1 = 0 Then
            sql = "delete from pmLocations where locid = '" & id & "'"
            appset.Update(sql)
            dgtt.EditItemIndex = -1
            sql = "select Count(*) from pmLocations " _
                   + "where compid = '" & cid & "' and siteid = '" & sid & "'"
            PageNumber = appset.PageCount(sql, PageSize)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            If dgtt.CurrentPageIndex > PageNumber Then
                dgtt.CurrentPageIndex = PageNumber - 1
            End If
            If dgtt.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgtt.CurrentPageIndex + 1
            End If
            PopTasks(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr69" , "AppSetLoc.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
    End Sub

    Private Sub dgtt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.EditCommand
        PageNumber = dgtt.CurrentPageIndex + 1
        dgtt.EditItemIndex = e.Item.ItemIndex
        PopTasks(PageNumber)
    End Sub

    Private Sub dgtt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.CancelCommand
        dgtt.EditItemIndex = -1
        PopTasks(PageNumber)
    End Sub

    Private Sub dgtt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.UpdateCommand
        appset.Open()
        Dim id, loc, desc, typ, typid As String
        id = CType(e.Item.FindControl("lblttide"), Label).Text
        loc = CType(e.Item.FindControl("txtloc"), TextBox).Text
        desc = CType(e.Item.FindControl("txtdesce"), TextBox).Text
        typid = CType(e.Item.FindControl("lbltypid"), Label).Text
        typ = CType(e.Item.FindControl("ddtype"), DropDownList).SelectedItem.ToString
        loc = appset.ModString1(loc)
        sid = lblsid.Value
        desc = appset.ModString1(desc)
        Dim faill As String = desc.ToLower
        sql = "select count(*) from pmLocations where lower(location) = '" & loc & "' and compid = '" & cid & "' and siteid = '" & sid & "'"
        Dim strchk As Integer
        strchk = appset.Scalar(sql)
        If strchk = 0 Then
            sql = "update pmLocations set location = " _
            + "'" & loc & "', description = '" & desc & "', " _
            + "typid = '" & typid & "', type = '" & typ & "' where locid = '" & id & "'"
            appset.Update(sql)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            PopTasks(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr70" , "AppSetLoc.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgtt.Columns(0).HeaderText = dlabs.GetDGPage("AppSetLoc.aspx","dgtt","0")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(3).HeaderText = dlabs.GetDGPage("AppSetLoc.aspx","dgtt","3")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(4).HeaderText = dlabs.GetDGPage("AppSetLoc.aspx","dgtt","4")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(5).HeaderText = dlabs.GetDGPage("AppSetLoc.aspx","dgtt","5")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(6).HeaderText = dlabs.GetDGPage("AppSetLoc.aspx","dgtt","6")
		Catch ex As Exception
		End Try

	End Sub

End Class
