

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class tpmvars
    Inherits System.Web.UI.Page
	Protected WithEvents lang193 As System.Web.UI.WebControls.Label

	Protected WithEvents lang192 As System.Web.UI.WebControls.Label

	Protected WithEvents lang191 As System.Web.UI.WebControls.Label

	Protected WithEvents lang190 As System.Web.UI.WebControls.Label

	Protected WithEvents lang189 As System.Web.UI.WebControls.Label

	Protected WithEvents lang188 As System.Web.UI.WebControls.Label

	Protected WithEvents lang187 As System.Web.UI.WebControls.Label

	Protected WithEvents lang186 As System.Web.UI.WebControls.Label

	Protected WithEvents lang185 As System.Web.UI.WebControls.Label

	Protected WithEvents lang184 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Protected WithEvents lbskillmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbsiteskills As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblttmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnfromtt As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btntott As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbltpmtt As System.Web.UI.WebControls.ListBox
    Protected WithEvents txtmins As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblstatmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbltpmstat As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntostat As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromstat As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tpm As New Utilities
    Protected WithEvents cbact As System.Web.UI.WebControls.CheckBox
    Dim ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btntocomp.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                btntocomp.Enabled = False
                btnfromcomp.ImageUrl = "../images/appbuttons/minibuttons/backgraybg.gif"
                btnfromcomp.Enabled = False
                ImageButton1.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                ImageButton1.Enabled = False
                btnfromtt.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                btnfromtt.Enabled = False
                btntott.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                btntott.Enabled = False
                btnfromstat.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                btnfromstat.Enabled = False
                btntostat.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                btntostat.Enabled = False
            End If
            tpm.Open()
            LoadFreqs()
            LoadVals()
            PopSkills()
            PopSiteSkills()
            PopTypes()
            PopSiteTypes()
            PopStats()
            PopSiteStats()
            tpm.Dispose()

        End If
    End Sub
    Private Sub LoadVals()
        Dim tuse As String
        sql = "select tpmfreq, tpmmins, tpmuse from pmsysvars"
        dr = tpm.GetRdrData(sql)
        While dr.Read
            Try
                txtfreq.Text = dr.Item("tpmfreq").ToString
            Catch ex As Exception

            End Try
            txtmins.Text = dr.Item("tpmmins").ToString
            tuse = dr.Item("tpmuse").ToString
            If tuse = "1" Then
                cbact.Checked = True
            Else
                cbact.Checked = False
            End If
        End While
        dr.Close()
    End Sub
    Private Sub LoadFreqs()
        

    End Sub
    Private Sub PopSkills()
        sql = "select * from pmSkills where skillid not in (select ddid from tpmvars where ddtable = 'pmSkills') and skill <> 'Select'"
        dr = tpm.GetRdrData(sql)
        lbskillmaster.DataSource = dr
        lbskillmaster.DataValueField = "skillid"
        lbskillmaster.DataTextField = "skill"
        Try
            lbskillmaster.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lbskillmaster.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopSiteSkills()
        sql = "select * from tpmvars where ddtable = 'pmSkills'"
        dr = tpm.GetRdrData(sql)
        lbsiteskills.DataSource = dr
        lbsiteskills.DataValueField = "ddid"
        lbsiteskills.DataTextField = "ddval"
        Try
            lbsiteskills.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lbsiteskills.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        Dim Item As ListItem
        Dim s, ss As String
        tpm.Open()
        For Each Item In lbskillmaster.Items
            If Item.Selected Then
                ss = Item.ToString
                s = Item.Value.ToString
                GetItems(s, ss)
            End If
        Next
        PopSiteSkills()
        PopSkills()
        tpm.Dispose()
    End Sub
    Private Sub GetItems(ByVal skillid As String, ByVal skillstr As String)
        Dim fcnt As Integer
        sql = "select count(*) from tpmvars where ddtable = 'pmSkills' and " _
        + "ddid = '" & skillid & "'"
        fcnt = tpm.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into tpmvars (ddtable, ddid, ddval) values ('pmSkills', '" & skillid & "','" & skillstr & "')"
            tpm.Update(sql)
        End If
    End Sub
    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        Dim Item As ListItem
        Dim s As String
        tpm.Open()
        For Each Item In lbsiteskills.Items
            If Item.Selected Then
                s = Item.Value.ToString
                RemItems(s)
            End If
        Next
        PopSiteSkills()
        PopSkills()
        tpm.Dispose()
    End Sub
    Private Sub RemItems(ByVal skillid As String)
        sql = "delete from tpmvars where ddtable = 'pmSkills' and ddid = '" & skillid & "'"
        tpm.Update(sql)

    End Sub



    Private Sub PopTypes()
        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where ttid not in (select ddid from tpmvars where ddtable = 'pmTaskTypes') " _
        + "and tasktype <> 'Select'" 'and ttid in (2, 3, 5) 
        dr = tpm.GetRdrData(sql)
        lblttmaster.DataSource = dr
        lblttmaster.DataValueField = "ttid"
        lblttmaster.DataTextField = "tasktype"
        Try
            lblttmaster.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lblttmaster.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopSiteTypes()
        sql = "select * from tpmvars where ddtable = 'pmTaskTypes'"
        dr = tpm.GetRdrData(sql)
        lbltpmtt.DataSource = dr
        lbltpmtt.DataValueField = "ddid"
        lbltpmtt.DataTextField = "ddval"
        Try
            lbltpmtt.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lbltpmtt.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub btnfromtt_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromtt.Click
        ToCompt()
    End Sub
    Private Sub ToCompt()
        Dim Item As ListItem
        Dim s, ss As String
        tpm.Open()
        For Each Item In lblttmaster.Items
            If Item.Selected Then
                ss = Item.ToString
                s = Item.Value.ToString
                GetItemst(s, ss)
            End If
        Next
        PopTypes()
        PopSiteTypes()
        tpm.Dispose()
    End Sub
    Private Sub GetItemst(ByVal skillid As String, ByVal skillstr As String)
        Dim fcnt As Integer
        sql = "select count(*) from tpmvars where ddtable = 'pmTaskTypes' and " _
        + "ddid = '" & skillid & "'"
        fcnt = tpm.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into tpmvars (ddtable, ddid, ddval) values ('pmTaskTypes', '" & skillid & "','" & skillstr & "')"
            tpm.Update(sql)
        End If
    End Sub
    Private Sub btntott_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntott.Click
        FromCompt()
    End Sub
    Private Sub FromCompt()
        Dim Item As ListItem
        Dim s As String
        tpm.Open()
        For Each Item In lbltpmtt.Items
            If Item.Selected Then
                s = Item.Value.ToString
                RemItemst(s)
            End If
        Next
        PopTypes()
        PopSiteTypes()
        tpm.Dispose()
    End Sub
    Private Sub RemItemst(ByVal skillid As String)
        sql = "delete from tpmvars where ddtable = 'pmTaskTypes' and ddid = '" & skillid & "'"
        tpm.Update(sql)

    End Sub

    Private Sub PopStats()
        sql = "select statid, status " _
        + "from pmStatus where statid not in (select ddid from tpmvars where ddtable = 'pmStatus') and status <> 'Select'"
        dr = tpm.GetRdrData(sql)
        lblstatmaster.DataSource = dr
        lblstatmaster.DataValueField = "statid"
        lblstatmaster.DataTextField = "status"
        Try
            lblstatmaster.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lblttmaster.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopSiteStats()
        sql = "select * from tpmvars where ddtable = 'pmStatus'"
        dr = tpm.GetRdrData(sql)
        lbltpmstat.DataSource = dr
        lbltpmstat.DataValueField = "ddid"
        lbltpmstat.DataTextField = "ddval"
        Try
            lbltpmstat.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            lbltpmstat.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub btntostat_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntostat.Click
        ToComps()
    End Sub
    Private Sub ToComps()
        Dim Item As ListItem
        Dim s, ss As String
        tpm.Open()
        For Each Item In lblstatmaster.Items
            If Item.Selected Then
                ss = Item.ToString
                s = Item.Value.ToString
                GetItemss(s, ss)
            End If
        Next
        PopStats()
        PopSiteStats()
        tpm.Dispose()
    End Sub
    Private Sub GetItemss(ByVal skillid As String, ByVal skillstr As String)
        Dim fcnt As Integer
        sql = "select count(*) from tpmvars where ddtable = 'pmStatus' and " _
        + "ddid = '" & skillid & "'"
        fcnt = tpm.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into tpmvars (ddtable, ddid, ddval) values ('pmStatus', '" & skillid & "','" & skillstr & "')"
            tpm.Update(sql)
        End If
    End Sub

    Private Sub btnfromstat_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromstat.Click
        FromComps()
    End Sub
    Private Sub FromComps()
        Dim Item As ListItem
        Dim s As String
        tpm.Open()
        For Each Item In lbltpmstat.Items
            If Item.Selected Then
                s = Item.Value.ToString
                RemItemss(s)
            End If
        Next
        PopStats()
        PopSiteStats()
        tpm.Dispose()
    End Sub
    Private Sub RemItemss(ByVal skillid As String)
        sql = "delete from tpmvars where ddtable = 'pmStatus' and ddid = '" & skillid & "'"
        tpm.Update(sql)

    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim freq As String = txtfreq.Text
        If freq = "" Or freq = "Select" Then
            freq = "0"
        Else
            Dim frechk As Long
            Try
                frechk = System.Convert.ToInt32(freq)
            Catch ex As Exception
                Dim strMessage As String = "Frequency Must be a Non Decimal Numeric Value"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If
        Dim mins As String = txtmins.Text
        If mins <> "" Then
            Dim qtychk As Long
            Try
                qtychk = System.Convert.ToInt32(mins)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr188" , "tpmvars.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End If

        If mins = "" Then
            mins = "0"
        End If
        Dim act As String
        If cbact.Checked = True Then
            act = "1"
        Else
            act = "0"
        End If
        sql = "update pmsysvars set tpmfreq = '" & freq & "', tpmmins = '" & mins & "', tpmuse = '" & act & "'"
        tpm.Open()
        tpm.Update(sql)
        tpm.Dispose()
        Dim strMessage1 As String =  tmod.getmsg("cdstr189" , "tpmvars.aspx.vb")
 
        Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
        Exit Sub
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang184.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang184")
        Catch ex As Exception
        End Try
        Try
            lang185.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang185")
        Catch ex As Exception
        End Try
        Try
            lang186.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang186")
        Catch ex As Exception
        End Try
        Try
            lang187.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang187")
        Catch ex As Exception
        End Try
        Try
            lang188.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang188")
        Catch ex As Exception
        End Try
        Try
            lang189.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang189")
        Catch ex As Exception
        End Try
        Try
            lang190.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang190")
        Catch ex As Exception
        End Try
        Try
            lang191.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang191")
        Catch ex As Exception
        End Try
        Try
            lang192.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang192")
        Catch ex As Exception
        End Try
        Try
            lang193.Text = axlabs.GetASPXPage("tpmvars.aspx", "lang193")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ImageButton1.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                ImageButton1.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                ImageButton1.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                ImageButton1.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                ImageButton1.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
