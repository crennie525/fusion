

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class AppSetApprPlan
    Inherits System.Web.UI.Page
	Protected WithEvents lang22 As System.Web.UI.WebControls.Label

	Protected WithEvents lang21 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pp As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql, Login, ro As String
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rev As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdplan As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblprojid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim pid, jump As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            rev = Request.QueryString("rev").ToString
            'If jump = "yes" Then
            'pid = Request.QueryString("pid").ToString
            'lblprojid.Value = pid '"1"
            pp.Open()
            If rev = 0 Then
                rev = pp.Rev()
            End If
            lblrev.Value = rev
            LoadProj()
            pp.Dispose()
            'Else
            'lblprojid.Value = "0"
            'End If
        End If
    End Sub
    Private Sub LoadProj()
        Dim sb As New StringBuilder
        Dim proj As String = lblprojid.Value
        rev = lblrev.Value
        sb.Append("<table cellspacing=""0"" width=""520"" border=""0""><tr height=""20""><td width=""10px"">&nbsp;</td>" & vbCrLf)
        sb.Append("<td class=""btmmenu plainlabel"" width=""400px"">" & tmod.getlbl("cdlbl1" , "AppSetApprPlan.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""btmmenu plainlabel"" width=""110px"">" & tmod.getlbl("cdlbl2" , "AppSetApprPlan.aspx.vb") & "</td></tr>" & vbCrLf)

        sql = "select a.pmaid, a.listitem, a.apprgrp, b.pmacid, b.checkorder, b.checkitem, " _
        + "cntb = (select count(*) from pmApprovalChk b where b.pmaid = a.pmaid), " _
        + "cntt = (select count(*) from pmApprovalTasks c where c.pmacid = b.pmacid), " _
        + "c.pmtid, c.taskorder, c.apprtask, d.grpname from pmApproval a left outer join pmApprovalChk b " _
        + "on b.pmaid = a.pmaid left outer join pmApprovalTasks c on c.pmacid = b.pmacid " _
        + "left outer join pmApprovalGrps d on d.apprgrp = a.apprgrp " _
        + "where a.rev = '" & rev & "' order by a.listorder, b.checkorder, c.taskorder"

        dr = pp.GetRdrData(sql)

        Dim rowflag As Integer = 0
        Dim rowflag1 As Integer = 0
        Dim rowflag2 As Integer = 0
        Dim bg As String

        Dim gi As String = "0"
        Dim bid As String = "0"
        Dim bidchk As String = "0"
        Dim tid As String = "0"

        Dim start As String = "0"
        Dim start1 As String = "0"
        Dim bimg As String

        Dim bcnt As Integer = 0
        Dim tcnt As Integer = 0
        Dim cntb, cntt As Integer
        While dr.Read
            bid = dr.Item("pmacid").ToString
            If dr.Item("pmaid").ToString <> gi Then
                If rowflag = 0 Then
                    bg = "transrow"
                    rowflag = 1
                Else
                    bg = "transrowblue"
                    rowflag = "0"
                End If
                gi = dr.Item("pmaid").ToString
                cntb = dr.Item("cntb").ToString
                cntt = dr.Item("cntt").ToString
                If cntb <> 0 Then
                    sb.Append("<tr class=""" & bg & """ height=""20""><td><img id='1i" + gi + "' src=""../images/appbuttons/bgbuttons/plus.gif"" " & vbCrLf)
                    sb.Append("onclick=""fclose('1t" + gi + "', '1i" + gi + "');""></td>" & vbCrLf)
                Else
                    sb.Append("<tr class=""" & bg & """ height=""20""><td><img id='1i" + gi + "' src=""../images/appbuttons/bgbuttons/minus.gif"" ></td>" & vbCrLf)
                End If


                'sb.Append("<td class=""plainlabel"">&nbsp;<a href=""#"" onclick=""jump('" & gi & "', '" & dr.Item("listitem").ToString & "')"">" & dr.Item("listitem").ToString & "</a></td>" & vbCrLf)
                sb.Append("<td class=""plainlabel"">&nbsp;<a href=""#"" onclick=""jump('" & gi & "')"">" & dr.Item("listitem").ToString & "</a></td>" & vbCrLf)
                sb.Append("<td class=""plainlabel"">" & dr.Item("grpname").ToString & "</td></tr>" & vbCrLf)


                If bid <> "" AndAlso bid <> "0" Then
                    bcnt = bcnt + 1
                    cntb = dr.Item("cntb").ToString
                    cntt = dr.Item("cntt").ToString
                    tid = dr.Item("pmtid").ToString
                    bidchk = dr.Item("pmacid").ToString
                    If tid <> "" Then
                        bimg = "../images/appbuttons/bgbuttons/plus.gif"
                    Else
                        bimg = "../images/appbuttons/bgbuttons/minus.gif"
                    End If
                    sb.Append("<tr class=""details"" id='1t" + gi + "'><td>&nbsp;</td><td colspan=""2"">" & vbCrLf)
                    sb.Append("<table border=""0"" width=""500"" cellspacing=""0"">")

                    sb.Append("<tr height=""20""><td width=""10""></td>" & vbCrLf)
                    sb.Append("<td class=""tblmenuyrt"" width=""40"">" & tmod.getlbl("cdlbl3" , "AppSetApprPlan.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""tblmenuyrt"" width=""450"">" & tmod.getlbl("cdlbl4" , "AppSetApprPlan.aspx.vb") & "</td></tr>" & vbCrLf)

                    If cntt <> 0 Then
                        sb.Append("<tr height=""20""><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                        sb.Append("onclick=""fclose('2t" + bid + "', '2i" + bid + "');""></td>" & vbCrLf)
                    Else
                        sb.Append("<tr height=""20""><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                        sb.Append("></td>" & vbCrLf)
                    End If


                    sb.Append("<td class=""plainlabel"" align=""center"">" & dr.Item("checkorder").ToString & "</td>" & vbCrLf)
                    'sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""jumpb('" & bidchk & "', '" & gi & "', '" & "', '" & dr.Item("listitem").ToString & "', '" & dr.Item("checkitem").ToString & "')"">" & dr.Item("checkitem").ToString & "</a></td></tr>" & vbCrLf)
                    sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""jumpb('" & bidchk & "', '" & gi & "')"">" & dr.Item("checkitem").ToString & "</a></td></tr>" & vbCrLf)

                    If cntb = bcnt Then
                        bcnt = 0
                        sb.Append("</td></tr></table>")
                    End If
                End If
                If tid <> "" AndAlso tid <> "0" Then
                    tcnt = tcnt + 1
                    cntt = dr.Item("cntt").ToString

                    sb.Append("<tr class=""details"" id='2t" + bid + "' align=""right""><td colspan=""3"">" & vbCrLf)
                    sb.Append("<table border=""0"" width=""480"" cellspacing=""0"">")

                    sb.Append("<tr height=""20""><td class=""tblmenugrt plainlabel"" width=""40"">" & tmod.getlbl("cdlbl5" , "AppSetApprPlan.aspx.vb") & "</td>" & vbCrLf)
                    sb.Append("<td class=""tblmenugrt plainlabel"" width=""440"">" & tmod.getlbl("cdlbl6" , "AppSetApprPlan.aspx.vb") & "</td></tr>" & vbCrLf)

                    sb.Append("<tr height=""20""><td class=""plainlabel"" align=""center"" valign=""top"">" & dr.Item("taskorder").ToString & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"" >" & dr.Item("apprtask").ToString & "</td></tr>" & vbCrLf)

                    If cntt = tcnt Then
                        tcnt = 0
                        sb.Append("</td></tr></table>")
                    End If

                End If

            Else
                tid = dr.Item("pmtid").ToString
                If bid <> bidchk Then
                    bcnt = bcnt + 1
                    cntb = dr.Item("cntb").ToString
                    cntt = dr.Item("cntt").ToString
                    bidchk = dr.Item("pmacid").ToString
                    If rowflag1 = 0 Then
                        bg = "#FCEED4"
                        rowflag1 = 1
                    Else
                        bg = "white"
                        rowflag1 = "0"
                    End If
                    If tid <> "" Then
                        bimg = "../images/appbuttons/bgbuttons/plus.gif"
                    Else
                        bimg = "../images/appbuttons/bgbuttons/minus.gif"
                    End If
                    If cntt <> 0 Then
                        sb.Append("<tr class=""" & bg & """ height=""20""><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                        sb.Append("onclick=""fclose('2t" + bid + "', '2i" + bid + "');""></td>" & vbCrLf)
                    Else
                        sb.Append("<tr class=""" & bg & """ height=""20""><td><img id='2i" + bid + "' src=""" & bimg & """ " & vbCrLf)
                        sb.Append("></td>" & vbCrLf)


                    End If

                    sb.Append("<td class=""plainlabel"" align=""center"">" & dr.Item("checkorder").ToString & "</td>" & vbCrLf)
                    'sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""jumpb('" & bidchk & "', '" & gi & "', '" & "', '" & dr.Item("listitem").ToString & "', '" & dr.Item("checkitem").ToString & "')"">" & dr.Item("checkitem").ToString & "</a></td></tr>" & vbCrLf)
                    sb.Append("<td class=""plainlabel""><a href=""#"" onclick=""jumpb('" & bidchk & "', '" & gi & "')"">" & dr.Item("checkitem").ToString & "</a></td></tr>" & vbCrLf)

                    If cntb = bcnt Then
                        bcnt = 0
                        sb.Append("</td></tr></table>")
                    End If

                    If tid <> "" Then
                        tcnt = tcnt + 1
                        cntt = dr.Item("cntt").ToString

                        sb.Append("<tr class=""details"" id='1t" + gi + "' align=""right""><td colspan=""3"">" & vbCrLf)
                        sb.Append("<table border=""0"" width=""480"" cellspacing=""0"">")

                        sb.Append("<tr height=""20""><td class=""tblmenugrt"" width=""40"">" & tmod.getlbl("cdlbl7" , "AppSetApprPlan.aspx.vb") & "</td>" & vbCrLf)
                        sb.Append("<td class=""tblmenugrt"" width=""440"">" & tmod.getlbl("cdlbl8" , "AppSetApprPlan.aspx.vb") & "</td></tr>" & vbCrLf)

                        sb.Append("<tr height=""20""><td class=""plainlabel"" align=""center"" valign=""top"">" & dr.Item("taskorder").ToString & "</td>" & vbCrLf)
                        sb.Append("<td class=""plainlabel"" >" & dr.Item("apprtask").ToString & "</td></tr>" & vbCrLf)

                        If cntt = tcnt Then
                            tcnt = 0
                            sb.Append("</td></tr></table>")
                        End If

                    End If

                Else
                    If rowflag2 = 0 Then
                        bg = "#c2ffc1"
                        rowflag2 = 1
                    Else
                        bg = "white"
                        rowflag2 = "0"
                    End If

                    tcnt = tcnt + 1
                    cntt = dr.Item("cntt").ToString

                    sb.Append("<tr class=""" & bg & """ height=""20""><td class=""plainlabel"" width=""40"" align=""center"" valign=""top"">" & dr.Item("taskorder").ToString & "</td>" & vbCrLf)
                    sb.Append("<td class=""plainlabel"" width=""440"">" & dr.Item("apprtask").ToString & "</td></tr>" & vbCrLf)

                    If cntt = tcnt Then
                        tcnt = 0
                        sb.Append("</td></tr><tr><td><img src='../images/2PX.gif'></td></tr></table>")
                    End If
                End If

            End If


        End While
        dr.Close()
        sb.Append("</tr></table>")
        tdplan.InnerHtml = sb.ToString
        'Catch ex As Exception

        'End Try
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang21.Text = axlabs.GetASPXPage("AppSetApprPlan.aspx", "lang21")
        Catch ex As Exception
        End Try
        Try
            lang22.Text = axlabs.GetASPXPage("AppSetApprPlan.aspx", "lang22")
        Catch ex As Exception
        End Try

    End Sub

End Class
