

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class WoChargeConfig
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Private Pictures(15) As String
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim dept As String
    Dim Sort As String = "fldorder asc"
    Dim cid, cnm, sid, did, sql, ro As String
    Dim dr As SqlDataReader
    Protected WithEvents DropDownList1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbltasktype As System.Web.UI.WebControls.Label
    Protected WithEvents dgtt As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgtasktype As System.Web.UI.WebControls.Label
    Protected WithEvents ttPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ttNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ttdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtt.Columns(0).Visible = False
                dgtt.Columns(10).Visible = False
            End If
            cid = "0" 'Request.QueryString("cid")
            If cid <> "" Then
                lblcid.Value = cid
                cid = cid
                appset.Open()
                PopTasks(PageNumber)
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        End If
        'ttPrev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'ttPrev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'ttNext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'ttNext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        Dim fldcnt As Integer
        Try
            appset.Open()
        Catch ex As Exception

        End Try
        cid = lblcid.Value
        sql = "select count(*) " _
        + "from wochargeconfig where compid = '" & cid & "'"
        fldcnt = appset.Scalar(sql)
        dgtt.VirtualItemCount = fldcnt
        If fldcnt < 10 Then
            dgtt.ShowFooter = True
        Else
            dgtt.ShowFooter = False
        End If

        If dgtt.VirtualItemCount = 0 Then
            lbltasktype.Text = "No Configuration Records Found"
            dgtt.Visible = True
            'ttPrev.Visible = False
            'ttNext.Visible = False

            Filter = "compid = " & cid '"compid = '" & cid & "'"
            Tables = "wochargeconfig"
            PK = "wccid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            appset.Dispose()

        Else
            Filter = "compid = " & cid '"compid = '" & cid & "'"
            Tables = "wochargeconfig"
            PK = "wccid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            appset.Dispose()
            'lblpgtasktype.Text = "Page " & dgtt.CurrentPageIndex + 1 & " of " & dgtt.PageCount
            lbltasktype.Text = ""
            'ttPrev.Visible = True
            'ttNext.Visible = True
            'checkFreqStatPgCnt()
        End If

        'Catch ex As Exception

        'End Try
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Private Function GetCnt()
        Dim fldcnt As Integer
        cid = lblcid.Value
        sql = "select count(*) from wochargeconfig where compid = '" & cid & "'"
        fldcnt = appset.Scalar(sql)
        Return fldcnt
    End Function
    Private Sub ConfigGrid()
        Dim fldcnt As Integer = GetCnt()
        


    End Sub
    Private Sub dgtt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.ItemCommand
        Dim lname, ddtv, ddti, dddv, dddi, lengi, fldord As String
        Dim cbri, cbti, fldcnt As Integer
        Dim lnamei As TextBox
        Dim leng As TextBox
        Dim dd As DropDownList
        Dim ddd As DropDownList
        Dim cbr As CheckBox
        Dim cbt As CheckBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewtt"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString2(lname)

            dd = CType(e.Item.FindControl("ddtypf"), DropDownList)
            ddtv = dd.SelectedValue
            ddti = dd.SelectedIndex

            ddd = CType(e.Item.FindControl("dddelimf"), DropDownList)
            dddv = ddd.SelectedValue
            dddi = ddd.SelectedIndex

            leng = CType(e.Item.FindControl("txtlenf"), TextBox)
            lengi = leng.Text
            Dim lchk As Long
            Try
                lchk = System.Convert.ToInt64(lengi)
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr190" , "WoChargeConfig.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If lengi > 30 Then
                Dim strMessage As String =  tmod.getmsg("cdstr191" , "WoChargeConfig.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If

            cbr = CType(e.Item.FindControl("cbreqf"), CheckBox)
            cbri = cbr.Checked
            If cbri = True Then
                cbri = 1
            Else
                cbri = 0
            End If

            cbt = CType(e.Item.FindControl("cbdbf"), CheckBox)
            cbti = cbr.Checked
            If cbti = True Then
                cbti = 1
            Else
                cbti = 0
            End If
            'sql = "select count(*) from wochargeconfig where compid = '" & cid & "'"
            appset.Open()
            fldcnt = GetCnt()
            fldord = fldcnt + 1
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    Dim lennam = Len(lnamei.Text)
                    If lennam > 30 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr192" , "WoChargeConfig.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else

                        cid = lblcid.Value
                        Dim tt As String
                        Dim strchk As Integer
                        Dim faill As String = lname.ToLower
                        sql = "select count(*) from wochargeconfig where lower(fldname) = '" & faill & "' and compid = '" & cid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "insert into wochargeconfig (fldname, fldlen, fldorder, dataindex, datatype, length, delimindex, delimiter, required, todb, compid) values " _
                            + "('" & lname & "', '" & lennam & "', '" & fldord & "', '" & ddti & "', '" & ddtv & " ', '" & lengi & "', '" & dddi & "', '" & dddv & "', '" & cbri & "', '" & cbti & "','" & cid & "')"
                            appset.Update(sql)
                            Dim ttcnt As Integer = dgtt.VirtualItemCount + 1

                            lnamei.Text = ""
                            sql = "select Count(*) from wochargeconfig " _
                               + "where compid = '" & cid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopTasks(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr193" , "WoChargeConfig.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If

                    End If
                Else
                    appset.Dispose()
                End If
            End If
        End If
    End Sub

    Private Sub dgtt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.EditCommand
        lblold.Value = CType(e.Item.FindControl("lbloldfld"), Label).Text
        lbloldorder.Value = CType(e.Item.FindControl("lbloldo"), Label).Text
        PageNumber = dgtt.CurrentPageIndex + 1
        dgtt.EditItemIndex = e.Item.ItemIndex
        PopTasks(PageNumber)
    End Sub

    Private Sub dgtt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.CancelCommand
        dgtt.EditItemIndex = -1
        PopTasks(PageNumber)
    End Sub

    Private Sub dgtt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.UpdateCommand
        appset.Open()
        cid = lblcid.Value
        Dim id, desc, act, old, oldord, ord As String
        Dim lname, ddtv, ddti, dddv, dddi, lengi As String
        Dim cbri, cbti As Integer
        Dim lnamei As TextBox
        Dim leng As TextBox
        Dim dd As DropDownList
        Dim ddd As DropDownList
        Dim cbr As CheckBox
        Dim cbt As CheckBox

        lnamei = CType(e.Item.FindControl("txtnewfld"), TextBox)
        lname = lnamei.Text
        lname = appset.ModString2(lname)

        dd = CType(e.Item.FindControl("ddtype"), DropDownList)
        ddtv = dd.SelectedValue
        ddti = dd.SelectedIndex

        ddd = CType(e.Item.FindControl("dddelim"), DropDownList)
        dddv = ddd.SelectedValue
        dddi = ddd.SelectedIndex

        leng = CType(e.Item.FindControl("txtlene"), TextBox)
        lengi = leng.Text
        Dim lchk As Long
        Try
            lchk = System.Convert.ToInt64(lengi)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr194" , "WoChargeConfig.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If lengi > 30 Then
            Dim strMessage As String =  tmod.getmsg("cdstr195" , "WoChargeConfig.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        cbr = CType(e.Item.FindControl("cbreqe"), CheckBox)
        cbri = cbr.Checked
        If cbri = True Then
            cbri = 1
        Else
            cbri = 0
        End If

        cbt = CType(e.Item.FindControl("cbdbe"), CheckBox)
        cbti = cbr.Checked
        If cbti = True Then
            cbti = 1
        Else
            cbti = 0
        End If

        id = CType(e.Item.FindControl("lblttide"), Label).Text
        ord = CType(e.Item.FindControl("txtorder"), TextBox).Text
        oldord = lbloldorder.Value
        If ord = 0 Or Len(ord) = 0 Then
            ord = oldord
        End If

        Dim lennam = Len(lnamei.Text)
        If lennam > 0 Then
            Dim faill As String = lname.ToLower
            sql = "select count(*) from wochargeconfig where lower(fldname) = '" & faill & "' and compid = '" & cid & "'"
            Dim strchk As Integer
            old = lblold.Value
            If old <> lname Then
                strchk = appset.Scalar(sql)
            Else
                strchk = 0
            End If
            If strchk = 0 Then
                sql = "update wochargeconfig set fldname = " _
                + "'" & lname & "', fldlen = '" & lennam & "', " _
                + "dataindex = '" & ddti & "', datatype = '" & ddtv & "', " _
                + "length = '" & lengi & "', required = '" & cbri & "', " _
                + "delimindex = '" & dddi & "', delimiter = '" & dddv & "' " _
                + "where wccid = '" & id & "'"
                appset.Update(sql)
                If ord <> oldord Then
                    sql = "usp_reorderconfig '" & id & "', '" & cid & "', '" & ord & "', '" & oldord & "'"
                    appset.Update(sql)
                End If
                dgtt.EditItemIndex = -1
                PopTasks(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr196" , "WoChargeConfig.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
            End If
        End If

    End Sub

    Private Sub dgtt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.DeleteCommand
        Dim id, ord As String
        cid = lblcid.Value
        Try
            id = CType(e.Item.FindControl("lblttide"), Label).Text
            ord = CType(e.Item.FindControl("txtorder"), TextBox).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblttids"), Label).Text
            ord = CType(e.Item.FindControl("lblfldorder"), Label).Text
        End Try
        sql = "usp_delfield '" & id & "', '" & ord & "', '" & cid & "'"
        appset.Open()
        appset.Update(sql)
        PageNumber = appset.PageCount(sql, PageSize)
        'appset.Dispose()
        dgtt.EditItemIndex = -1
        If dgtt.CurrentPageIndex > PageNumber Then
            dgtt.CurrentPageIndex = PageNumber - 1
        End If
        If dgtt.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgtt.CurrentPageIndex + 1
        End If
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtt.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                         e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("btndel"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete Charge# Field " & _
            DataBinder.Eval(e.Item.DataItem, "fldname") & "?')"
        End If
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgtt.Columns(0).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","0")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(2).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","2")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(4).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","4")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(5).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","5")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(6).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","6")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(7).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","7")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(8).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","8")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(9).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","9")
		Catch ex As Exception
		End Try
		Try
			dgtt.Columns(10).HeaderText = dlabs.GetDGPage("WoChargeConfig.aspx","dgtt","10")
		Catch ex As Exception
		End Try

	End Sub

End Class
