﻿Imports System.Data.SqlClient
Public Class appsetwtlkup
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim stat As New Utilities
    Dim typ As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            If typ = "stat" Then
                stat.Open()
                GetStatus()
                stat.Dispose()
            ElseIf typ = "pri" Then
                stat.Open()
                GetPri()
                stat.Dispose()
            End If
        End If
    End Sub
    Private Sub GetStatus()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from wostatus where active = 1 and wostatus not in ('COMP','CAN','CLOSE')"
        Dim wostatus, wodesc As String
        dr = stat.GetRdrData(sql)
        While dr.Read
            wostatus = dr.Item("wostatus").ToString
            wodesc = dr.Item("description").ToString
            sb.Append("<tr><td class=""linklabel"" width=""100""><a href=""#"" onclick=""getstat('" & wostatus & "');"">" & wostatus & "</a></td>")
            sb.Append("<td class=""plainlabel"" width=""200"">" & wodesc & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        statdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub GetPri()
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select woprid, (cast(wopri as varchar(10)) + ' - ' + wopridesc) as wopri from wopriority order by wopri"
        Dim woprid, wopri As String
        dr = stat.GetRdrData(sql)
        While dr.Read
            woprid = dr.Item("woprid").ToString
            wopri = dr.Item("wopri").ToString
            sb.Append("<tr><td class=""linklabel"" width=""300""><a href=""#"" onclick=""getpri('" & woprid & "','" & wopri & "');"">" & wopri & "</a></td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        statdiv.InnerHtml = sb.ToString

    End Sub
End Class