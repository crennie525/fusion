<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetApprLLTasks.aspx.vb"
    Inherits="lucy_r12.AppSetApprLLTasks" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetApprLLTasks</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link href="../styles/reports.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetApprLLTasksaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="left: 0px; position: absolute; top: 3px" cellspacing="0" cellpadding="0"
        width="520">
        <tr>
            <td class="bluelabel" width="120">
                <asp:Label ID="lang19" runat="server">Approval Phase</asp:Label>
            </td>
            <td class="label" id="tdphase" width="340" runat="server">
            </td>
            <td align="right" width="60">
                <input class="lilmenu plainlabel" onmouseover="this.className='lilmenuhov plainlabel'"
                    style="width: 55px; height: 24px" onclick="ret();" onmouseout="this.className='lilmenu plainlabel'"
                    type="button" value="Return">
            </td>
        </tr>
        <tr>
            <td class="bluelabel">
                <asp:Label ID="lang20" runat="server">High Level Task</asp:Label>
            </td>
            <td class="plainlabel" id="tdtsk" colspan="2" runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <img src="../images/2PX.gif">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgphases" runat="server" AutoGenerateColumns="False" ShowFooter="True"
                    CellSpacing="1" GridLines="None">
                    <FooterStyle CssClass="plainlabel"></FooterStyle>
                    <AlternatingItemStyle CssClass="plainlabel" BackColor="#C2FFC1"></AlternatingItemStyle>
                    <ItemStyle CssClass="plainlabel"></ItemStyle>
                    <HeaderStyle CssClass="tblmenugrt plainlabel"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="80px" CssClass="tblmenugrt plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Reset"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ID">
                            <ItemTemplate>
                                <asp:Label ID="lblpmtida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmtid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmtid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ID">
                            <ItemTemplate>
                                <asp:Label ID="lblpmacida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmacid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmacid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmacid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Order">
                            <HeaderStyle Width="50px" CssClass="tblmenugrt plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskorder") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtlo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.taskorder") %>'
                                    Width="40px">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Approval Phase Sub Level Tasks">
                            <HeaderStyle Width="370px" CssClass="tblmenugrt plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lnkphase" runat="server">
											<%# DataBinder.Eval(Container, "DataItem.apprtask") %>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtaphase" runat="server" MaxLength="100" Width="360px" TextMode="MultiLine"
                                    CssClass="plainlabel"></asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtephase" runat="server" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.apprtask") %>'
                                    Width="360px" TextMode="MultiLine" CssClass="plainlabel">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete">
                            <HeaderStyle Width="50px" CssClass="tblmenugrt plainlabel"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Center"></FooterStyle>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnew.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <input id="lblgroupid" type="hidden" name="lblgroupid" runat="server"><input id="lbloldtasknum"
        type="hidden" name="lbloldtasknum" runat="server">
    <input type="hidden" id="lblphaseid" runat="server">
    <input type="hidden" id="lblans" runat="server">
    <input type="hidden" id="lbleqcnt" runat="server">
    <input type="hidden" id="lblrev" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
