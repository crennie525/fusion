

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetApprLLTasks
    Inherits System.Web.UI.Page
    Protected WithEvents lang20 As System.Web.UI.WebControls.Label

    Protected WithEvents lang19 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pp As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lblphaseid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblans As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql, ro As String
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rev As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgphases As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdphase As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblgroupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtsk As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgphases.Columns(0).Visible = False
                dgphases.Columns(5).Visible = False
            End If
            Dim id, gid, grp, tsk, ans, cnt As String
            id = Request.QueryString("id").ToString
            grp = Request.QueryString("grp").ToString
            grp = Replace(grp, "#", "%23")
            tsk = Request.QueryString("ht").ToString
            tsk = Replace(tsk, "#", "%23")
            gid = Request.QueryString("gid").ToString
            ans = Request.QueryString("ans").ToString
            cnt = Request.QueryString("cnt").ToString
            rev = Request.QueryString("rev").ToString
            lblans.Value = ans
            lbleqcnt.Value = cnt
            tdphase.InnerHtml = grp
            lblphaseid.Value = gid
            tdtsk.InnerHtml = tsk
            lblgroupid.Value = id '"1"
            pp.Open()
            If rev = 0 Then
                rev = pp.Rev()
            End If
            lblrev.Value = rev
            LoadGroup()
            LoadCheck()
            LoadPhase()
            pp.Dispose()
        End If
    End Sub
    Private Sub LoadGroup()
        Dim groupid As String = lblphaseid.Value
        sql = "select listitem from pmApproval where pmaid = '" & groupid & "'"
        dr = pp.GetRdrData(sql)
        While dr.Read
            tdphase.InnerHtml = dr.Item("listitem").ToString
        End While
        dr.Close()
    End Sub
    Private Sub LoadCheck()
        Dim groupid As String = lblgroupid.Value
        sql = "select checkitem from pmApprovalChk where pmacid = '" & groupid & "'"
        dr = pp.GetRdrData(sql)
        While dr.Read
            tdtsk.InnerHtml = dr.Item("checkitem").ToString
        End While
        dr.Close()
    End Sub
    Private Sub LoadPhase()
        Dim groupid As String = lblgroupid.Value
        sql = "select pmtid, pmacid, taskorder, apprtask " _
        + "from pmApprovalTasks where pmacid = '" & groupid & "' order by taskorder"
        dr = pp.GetRdrData(sql)
        dgphases.DataSource = dr
        dgphases.DataBind()
        dr.Close()
    End Sub

    Private Sub dgphases_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.EditCommand
        lbloldtasknum.Value = CType(e.Item.FindControl("A1"), Label).Text
        dgphases.EditItemIndex = e.Item.ItemIndex
        pp.Open()
        LoadPhase()
        pp.Dispose()
    End Sub

    Private Sub dgphases_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.CancelCommand
        dgphases.EditItemIndex = -1
        pp.Open()
        LoadPhase()
        pp.Dispose()
    End Sub

    Private Sub dgphases_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.UpdateCommand
        Dim otn, ntn, ph, pid, pmacid, ans, cnt As String
        otn = lbloldtasknum.Value
        ntn = CType(e.Item.FindControl("txtlo"), TextBox).Text
        pid = CType(e.Item.FindControl("lblpmtid"), Label).Text
        ph = CType(e.Item.FindControl("txtephase"), TextBox).Text
        ph = pp.ModString2(ph)
        pmacid = CType(e.Item.FindControl("lblpmacid"), Label).Text
        Try
            ntn = System.Convert.ToInt64(ntn)
            pp.Open()
            If Len(ph) <> 0 Then
                sql = "update pmApprovalTasks set apprtask = '" & ph & "' " _
                + "where pmtid = '" & pid & "'"
                pp.Update(sql)
                ans = lblans.Value
                If ans <> "0" Then
                    CheckAns()
                End If
                cnt = lbleqcnt.Value
                If cnt = "1" Then
                    sql = "update equipment set apprstg1 = 0"
                    pp.Update(sql)
                    sql = "update pmApprovalEq set subappr = 0 where pmacid = '" & pmacid & "'"
                    pp.Update(sql)
                End If
            Else
                Dim strMessage As String = tmod.getmsg("cdstr3", "AppSetApprLLTasks.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

            If otn <> ntn Then
                sql = "usp_reorderPMApprPhsLL '" & pmacid & "', '" & ntn & "', '" & otn & "'"
                pp.Update(sql)
                If cnt = "1" Then
                    sql = "usp_reorderPMApprEq 'll'"
                    pp.Update(sql)
                End If
            End If
            dgphases.EditItemIndex = -1
            LoadPhase()
            pp.Dispose()
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr4", "AppSetApprLLTasks.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try


    End Sub
    Private Sub CheckAns()
        Dim ans As String = lblans.Value
        If ans = "1" Then
            sql = "update equipment set apprstg2prev = 1 where apprstg2 = 1"
            pp.Update(sql)
        ElseIf ans = "2" Then
            sql = "update equipment set apprstg2 = 0 where apprstg2 = 1 and apprstg2prev <> 1"
            pp.Update(sql)
        End If
    End Sub
    Private Sub dgphases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.ItemCommand
        Dim ph, des, ci, cl, ans, cnt As String
        Dim newll As Integer
        Dim tdes As TextBox
        Dim dd As DropDownList
        Dim groupid As String = lblgroupid.Value
        rev = lblrev.Value
        If e.CommandName = "Reset" Or e.CommandName = "Add" Then
            ph = CType(e.Item.FindControl("txtaphase"), TextBox).Text
            ph = pp.ModString2(ph)
            'dd = CType(e.Item.FindControl("ddaag"), DropDownList)
            'ci = dd.SelectedIndex
            'cl = dd.SelectedValue
            tdes = CType(e.Item.FindControl("txtaphase"), TextBox)
            'des = tdes.Text
        End If
        If e.CommandName = "Reset" Then
            tdes.Text = ""

        End If
        If e.CommandName = "Add" Then
            Dim phcnt As Integer
            pp.Open()
            Try
                sql = "select count(*) from pmApprovalTasks"
                phcnt = pp.Scalar(sql)
                phcnt = phcnt + 1
                sql = "insert into pmApprovalTasks " _
                + "(pmacid, taskorder, apprtask, rev) values ('" & groupid & "', '" & phcnt & "', '" & ph & "', '" & rev & "') " _
                + "select @@identity as 'identity'"
                'pp.Update(sql)
                newll = pp.Scalar(sql)
                ans = lblans.Value
                If ans <> "0" Then
                    CheckAns()
                End If
                cnt = lbleqcnt.Value
                If cnt = "1" Then
                    CheckEq(newll, phcnt, groupid)
                End If
            Catch ex As Exception
            End Try
            LoadPhase()
            pp.Dispose()
        End If
    End Sub
    Private Sub CheckEq(ByVal newll As Integer, ByVal phcnt As Integer, ByVal pmacid As String)
        Dim pmaid As String = lblphaseid.Value
        sql = "usp_updateEqAppr 'll', '" & pmaid & "', '" & pmacid & "', '0', '" & newll & "', '" & phcnt & "'"
        pp.Update(sql)
    End Sub
    Private Sub dgphases_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.DeleteCommand
        Dim tnum, pmtid, ans, cnt
        Try
            tnum = CType(e.Item.FindControl("A1"), Label).Text
            pmtid = CType(e.Item.FindControl("lblpmtida"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("txtlo"), TextBox).Text
            pmtid = CType(e.Item.FindControl("lblpmtid"), TextBox).Text
        End Try
        sql = "usp_delPMApprPhsLL '" & pmtid & "', '" & tnum & "'"
        pp.Open()
        pp.Update(sql)
        ans = lblans.Value
        If ans <> "0" Then
            CheckAns()
        End If
        cnt = lbleqcnt.Value
        If cnt = "1" Then
            sql = "usp_delPMApprEqPhsLL '" & pmtid & "'"
            pp.Update(sql)
        End If
        dgphases.EditItemIndex = -1
        LoadPhase()
        pp.Dispose()
    End Sub




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgphases.Columns(0).HeaderText = dlabs.GetDGPage("AppSetApprLLTasks.aspx", "dgphases", "0")
        Catch ex As Exception
        End Try
        Try
            dgphases.Columns(3).HeaderText = dlabs.GetDGPage("AppSetApprLLTasks.aspx", "dgphases", "3")
        Catch ex As Exception
        End Try
        Try
            dgphases.Columns(4).HeaderText = dlabs.GetDGPage("AppSetApprLLTasks.aspx", "dgphases", "4")
        Catch ex As Exception
        End Try
        Try
            dgphases.Columns(5).HeaderText = dlabs.GetDGPage("AppSetApprLLTasks.aspx", "dgphases", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang19.Text = axlabs.GetASPXPage("AppSetApprLLTasks.aspx", "lang19")
        Catch ex As Exception
        End Try
        Try
            lang20.Text = axlabs.GetASPXPage("AppSetApprLLTasks.aspx", "lang20")
        Catch ex As Exception
        End Try

    End Sub

End Class
