

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class AppSetApprPrev
    Inherits System.Web.UI.Page
	Protected WithEvents lang37 As System.Web.UI.WebControls.Label

	Protected WithEvents lang36 As System.Web.UI.WebControls.Label

	Protected WithEvents lang35 As System.Web.UI.WebControls.Label

	Protected WithEvents lang34 As System.Web.UI.WebControls.Label

	Protected WithEvents lang33 As System.Web.UI.WebControls.Label

	Protected WithEvents lang32 As System.Web.UI.WebControls.Label

	Protected WithEvents lang31 As System.Web.UI.WebControls.Label

	Protected WithEvents lang30 As System.Web.UI.WebControls.Label

	Protected WithEvents lang29 As System.Web.UI.WebControls.Label

	Protected WithEvents lang28 As System.Web.UI.WebControls.Label

	Protected WithEvents lang27 As System.Web.UI.WebControls.Label

	Protected WithEvents lang26 As System.Web.UI.WebControls.Label

	Protected WithEvents lang25 As System.Web.UI.WebControls.Label

	Protected WithEvents lang24 As System.Web.UI.WebControls.Label

	Protected WithEvents lang23 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim asp As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents ddrevs As System.Web.UI.WebControls.DropDownList
    Protected WithEvents trrevmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trrev As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblrevchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Dim sql As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents submit As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            Session("curlang") = lblfslang.Value
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            asp.Open()
            loadrevs()
            asp.Dispose()

        End If
    End Sub
    Private Sub loadrevs()
        Dim cnt As Integer
        sql = "select count(*) from pmApprRev where iscurrent <> 1"
        cnt = asp.Scalar(sql)
        If cnt > 0 Then
            sql = "select rev, approveddate from pmApprRev where iscurrent <> 1"
            dr = asp.GetRdrData(sql)
            ddrevs.DataSource = dr
            ddrevs.DataValueField = "rev"
            ddrevs.DataTextField = "rev"
            Try
                ddrevs.DataBind()
            Catch ex As Exception

            End Try

            ddrevs.Items.Insert(0, "Select Revision")
            ddrevs.Attributes.Add("onchange", "handlerev();")
            trrev.Attributes.Add("class", "View")
            trrevmsg.Attributes.Add("class", "Details")
            lblrevchk.Value = "ok"
            dr.Close()
        Else
            trrev.Attributes.Add("class", "Details")
            trrevmsg.Attributes.Add("class", "View")
            lblrevchk.Value = "no"
        End If
        
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang23.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang23")
        Catch ex As Exception
        End Try
        Try
            lang24.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang24")
        Catch ex As Exception
        End Try
        Try
            lang25.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang25")
        Catch ex As Exception
        End Try
        Try
            lang26.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang26")
        Catch ex As Exception
        End Try
        Try
            lang27.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang27")
        Catch ex As Exception
        End Try
        Try
            lang28.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang28")
        Catch ex As Exception
        End Try
        Try
            lang29.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang29")
        Catch ex As Exception
        End Try
        Try
            lang30.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang30")
        Catch ex As Exception
        End Try
        Try
            lang31.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang31")
        Catch ex As Exception
        End Try
        Try
            lang32.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang32")
        Catch ex As Exception
        End Try
        Try
            lang33.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang33")
        Catch ex As Exception
        End Try
        Try
            lang34.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang34")
        Catch ex As Exception
        End Try
        Try
            lang35.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang35")
        Catch ex As Exception
        End Try
        Try
            lang36.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang36")
        Catch ex As Exception
        End Try
        Try
            lang37.Text = axlabs.GetASPXPage("AppSetApprPrev.aspx", "lang37")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                Img1.Attributes.Add("src", "../images2/eng/bgbuttons/reset.gif")
            ElseIf lang = "fre" Then
                Img1.Attributes.Add("src", "../images2/fre/bgbuttons/reset.gif")
            ElseIf lang = "ger" Then
                Img1.Attributes.Add("src", "../images2/ger/bgbuttons/reset.gif")
            ElseIf lang = "ita" Then
                Img1.Attributes.Add("src", "../images2/ita/bgbuttons/reset.gif")
            ElseIf lang = "spa" Then
                Img1.Attributes.Add("src", "../images2/spa/bgbuttons/reset.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                submit.Attributes.Add("src", "../images2/eng/bgbuttons/continue.gif")
            ElseIf lang = "fre" Then
                submit.Attributes.Add("src", "../images2/fre/bgbuttons/continue.gif")
            ElseIf lang = "ger" Then
                submit.Attributes.Add("src", "../images2/ger/bgbuttons/continue.gif")
            ElseIf lang = "ita" Then
                submit.Attributes.Add("src", "../images2/ita/bgbuttons/continue.gif")
            ElseIf lang = "spa" Then
                submit.Attributes.Add("src", "../images2/spa/bgbuttons/continue.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
