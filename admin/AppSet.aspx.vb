

'********************************************************
'*
'********************************************************




Imports System.Data.SqlClient

Public Class AppSet
    Inherits System.Web.UI.Page
	Protected WithEvents lang9 As System.Web.UI.WebControls.Label

	Protected WithEvents lang8 As System.Web.UI.WebControls.Label

	Protected WithEvents lang7 As System.Web.UI.WebControls.Label

	Protected WithEvents lang6 As System.Web.UI.WebControls.Label

	Protected WithEvents lang5 As System.Web.UI.WebControls.Label

	Protected WithEvents lang4 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2 As System.Web.UI.WebControls.Label

	Protected WithEvents lang11 As System.Web.UI.WebControls.Label

	Protected WithEvents lang10 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim spName As String = "Paging_Cursor"
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 5
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Private Pictures(15) As String
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim dept As String
    Dim Sort As String = ""
    Dim cid, cnm, sid, did, sql As String
    Dim dr As SqlDataReader
    Dim appset As New Utilities
    Dim Login, ro, retab As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents lstsites As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblps As System.Web.UI.WebControls.Label
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents adddept As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtdname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtddesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgdepts As System.Web.UI.WebControls.Label
    Protected WithEvents deptPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents deptNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents lstcellsites As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    Protected WithEvents lstcelldepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblcellmsg As System.Web.UI.WebControls.Label
    Protected WithEvents dgcells As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addcell As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtcname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgcells As System.Web.UI.WebControls.Label
    Protected WithEvents cellPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cellNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbleq As System.Web.UI.WebControls.Label
    Protected WithEvents dgeqstat As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addeqstat As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtesname As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgeq As System.Web.UI.WebControls.Label
    Protected WithEvents eqstatPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents eqstatNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcom As System.Web.UI.WebControls.Label
    Protected WithEvents dgfail As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addfail As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtfname As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgfail As System.Web.UI.WebControls.Label
    Protected WithEvents failPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents failNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblfreq As System.Web.UI.WebControls.Label
    Protected WithEvents lblstat As System.Web.UI.WebControls.Label
    Protected WithEvents dgfreq As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgstat As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addfreq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents addstat As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newstat As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgfreq As System.Web.UI.WebControls.Label
    Protected WithEvents freqPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents freqNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpgstat As System.Web.UI.WebControls.Label
    Protected WithEvents statPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents statNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblmaint As System.Web.UI.WebControls.Label
    Protected WithEvents lblskill As System.Web.UI.WebControls.Label
    Protected WithEvents dgmain As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgskill As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addmain As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newmain As System.Web.UI.WebControls.TextBox
    Protected WithEvents addskill As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newskill As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgmaint As System.Web.UI.WebControls.Label
    Protected WithEvents maintPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents maintNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpgskill As System.Web.UI.WebControls.Label
    Protected WithEvents skillPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents skillNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpre As System.Web.UI.WebControls.Label
    Protected WithEvents dgpre As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addpre As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newpre As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgpre As System.Web.UI.WebControls.Label
    Protected WithEvents prePrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents xCoordHolder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoordHolder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents iftask As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifdept As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifcell As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifeq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifcom As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifloc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifwo As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifcharge As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iflabor As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifinv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifcomp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents preNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Iframe1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Iframe2 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblretab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            Try
                retab = Request.QueryString("rettab").ToString
                lblretab.Value = retab
            Catch ex As Exception
                retab = "no"
                lblretab.Value = "no"
            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try

            If ro = "1" Then
                'pgtitle.InnerHtml = "Application Setup (Read Only)"
            Else
                'pgtitle.InnerHtml = "Application Setup"
            End If
            Try
                cid = "0"
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try


            If cid <> "" Then
                lblcid.Value = cid
                cid = lblcid.Value
                sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString()
                lblsid.Value = sid

                If retab = "no" Then
                    ifloc.Attributes.Add("src", "AppSetLocTab.aspx?cid=" + cid + "&sid=" + sid)
                    lbltab.Value = "loc"
                Else
                    lbltab.Value = "labor"
                End If

            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "getbig" Then
                lblret.Value = ""
                GetBig()
            End If
        End If
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("AppSet.aspx")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetBig()
        Response.Redirect("../labor/PMLeadMan.aspx?x=yes&date=" & Now() & " Target='_top'")
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1.Text = axlabs.GetASPXPage("AppSet.aspx", "lang1")
        Catch ex As Exception
        End Try
        Try
            lang10.Text = axlabs.GetASPXPage("AppSet.aspx", "lang10")
        Catch ex As Exception
        End Try
        Try
            lang11.Text = axlabs.GetASPXPage("AppSet.aspx", "lang11")
        Catch ex As Exception
        End Try
        Try
            lang2.Text = axlabs.GetASPXPage("AppSet.aspx", "lang2")
        Catch ex As Exception
        End Try
        Try
            lang3.Text = axlabs.GetASPXPage("AppSet.aspx", "lang3")
        Catch ex As Exception
        End Try
        Try
            lang4.Text = axlabs.GetASPXPage("AppSet.aspx", "lang4")
        Catch ex As Exception
        End Try
        Try
            lang5.Text = axlabs.GetASPXPage("AppSet.aspx", "lang5")
        Catch ex As Exception
        End Try
        Try
            lang6.Text = axlabs.GetASPXPage("AppSet.aspx", "lang6")
        Catch ex As Exception
        End Try
        Try
            lang7.Text = axlabs.GetASPXPage("AppSet.aspx", "lang7")
        Catch ex As Exception
        End Try
        Try
            lang8.Text = axlabs.GetASPXPage("AppSet.aspx", "lang8")
        Catch ex As Exception
        End Try
        Try
            lang9.Text = axlabs.GetASPXPage("AppSet.aspx", "lang9")
        Catch ex As Exception
        End Try

    End Sub

End Class

