<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetApprPrev.aspx.vb"
    Inherits="lucy_r12.AppSetApprPrev" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Approval Update Warning</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetApprPrevaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="startup();" onunload="getans();">
    <form id="form1" method="post" runat="server">
    <table width="450" align="center">
        <tr>
            <td class="redlabel14" align="center">
                <br>
                <asp:Label ID="lang23" runat="server">You currently have PM Procedures stored in the PM Library that are not excluded from previous PM Approval Updates.</asp:Label><br>
                <br>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang24" runat="server">You have the options to exclude or remove these procedures, create a new PM Approval Procedure revision, or work on an existing revision in progress.</asp:Label><br>
                <br>
                <hr>
            </td>
        </tr>
        <tr id="tropt1">
            <td class="bluelabel" align="center">
                <input id="r1" onclick="getopts();" type="radio" name="rgrp"><asp:Label ID="lang25"
                    runat="server">Exclude Procedures</asp:Label>
            </td>
        </tr>
        <tr id="tropt1msg">
            <td class="plainlabel">
                <asp:Label ID="lang26" runat="server">Any PM Procedures currently approved for the PM Library will be excluded from any changes made during this session and willremain in the PM Library.</asp:Label><hr>
            </td>
        </tr>
        <tr class="details" id="tropts1">
            <td align="center">
                <table>
                    <tr>
                        <td class="label">
                            <input onclick="document.getElementById('lblchk').value='1a';" id="ro1" type="radio"
                                name="rgrp1"><asp:Label ID="lang27" runat="server">Create a new Revision using the current procedure.</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <input onclick="document.getElementById('lblchk').value='1b';" id="ro2" type="radio"
                                name="rgrp1"><asp:Label ID="lang28" runat="server">Create a new Revision from scratch.</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <input onclick="document.getElementById('lblchk').value='1c';" id="ro3" type="radio"
                                name="rgrp1"><asp:Label ID="lang29" runat="server">I just want to make revisions to the current procedure.</asp:Label>
                        </td>
                    </tr>
                </table>
                <hr>
            </td>
        </tr>
        <tr id="trrem">
            <td class="bluelabel" align="center">
                <input onclick="document.getElementById('lblchk').value=2;" id="r2" type="radio"
                    name="rgrp" checked><asp:Label ID="lang30" runat="server">Remove Procedures</asp:Label>
            </td>
        </tr>
        <tr id="trremmsg">
            <td class="plainlabel">
                <asp:Label ID="lang31" runat="server">Any PM Procedures currently approved for the PM Library will be removed from the PM Library in the event that any changes are made during this session.</asp:Label><br>
                <hr>
            </td>
        </tr>
        <tr id="trcnew">
            <td class="bluelabel" align="center">
                <input onclick="getopts2();" id="r3" type="radio" name="rgrp"><asp:Label ID="lang32"
                    runat="server">Create a new revision.</asp:Label>
            </td>
        </tr>
        <tr id="trcnewmsg">
            <td class="plainlabel">
                <asp:Label ID="lang33" runat="server">Creates a new PM Approval Procedure revision that can be edited for future consideration. Current PM Procedures will continue using the current revision until this new revision is accepted and approved.</asp:Label><br>
                <hr>
            </td>
        </tr>
        <tr class="details" id="tropts2">
            <td align="center">
                <table>
                    <tr>
                        <td class="label">
                            <input onclick="document.getElementById('lblchk').value='3a';" id="ro4" type="radio"
                                name="rgrp2"><asp:Label ID="lang34" runat="server">Create a new Revision using the current procedure.</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <input onclick="document.getElementById('lblchk').value='3b';" id="ro5" type="radio"
                                name="rgrp2"><asp:Label ID="lang35" runat="server">Create a new Revision from scratch.</asp:Label>
                        </td>
                    </tr>
                </table>
                <hr>
            </td>
        </tr>
        <tr id="trip">
            <td class="bluelabel" align="center">
                <input onclick="document.getElementById('lblchk').value=4;" id="r4" type="radio"
                    name="rgrp"><asp:Label ID="lang36" runat="server">Use an existing revision in-progress.</asp:Label><br>
            </td>
        </tr>
        <tr id="trrevmsg" runat="server">
            <td class="label" align="center">
                <asp:Label ID="lang37" runat="server">No New Revisions Are Available</asp:Label><hr>
            </td>
        </tr>
        <tr id="trrev" runat="server">
            <td align="center">
                <asp:DropDownList ID="ddrevs" runat="server">
                </asp:DropDownList>
                <hr>
            </td>
        </tr>
        <tr>
            <td class="plainlabel" align="center">
                <img id="submit" onclick="cont();" src="../images/appbuttons/bgbuttons/continue.gif"
                    runat="server">
                &nbsp;&nbsp;<img id="Img1" onclick="resetit();" src="../images/appbuttons/bgbuttons/reset.gif"
                    runat="server">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblchk" runat="server"><input type="hidden" id="lblrevchk"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
