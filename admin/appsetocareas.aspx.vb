﻿Imports System.Data.SqlClient
Public Class appsetocareas
    Inherits System.Web.UI.Page
    Dim cid, sql, sid, did, ro As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim dr As SqlDataReader
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim Sort As String
    Dim Login As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgfail.Columns(0).Visible = False
                dgfail.Columns(3).Visible = False
            End If
            Try
                cid = "0"
                lblsid.Value = HttpContext.Current.Session("dfltps").ToString
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try

            If cid <> "" Then
                lblcid.Value = cid
                cid = lblcid.Value
                appset.Open()
                PopFail(PageNumber)
                appset.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopFail(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopFail(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub

    Protected Sub ibtnsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        PageNumber = 1
        appset.Open()
        PopFail(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopFail(ByVal PageNumber As Integer)
        'Try
        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        cid = lblcid.Value
        If Len(srch) > 0 Then
            Filter = "ocarea like ''%" & srch & "%''"
            FilterCnt = "ocarea like '%" & srch & "%'"
        Else
            Filter = "oaid is not null"
            FilterCnt = "oaid is not null"
        End If
        sql = "select count(*) " _
              + "from ocareas where " & FilterCnt

        dgfail.VirtualItemCount = appset.Scalar(sql)
        If dgfail.VirtualItemCount = 0 Then
            lblcom.Text = "No Occurrence Area Records Found"
            Fields = "*"
            Tables = "ocareas"
            PK = "oaid"
            PageSize = "10"
            Sort = "ocarea"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgfail.DataSource = dr
            Try
                dgfail.DataBind()
            Catch ex As Exception
                dgfail.CurrentPageIndex = 0
                dgfail.DataBind()
            End Try
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgfail.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgfail.PageCount
        Else
            Fields = "*"
            Tables = "ocareas"
            PK = "oaid"
            PageSize = "10"
            Sort = "ocarea"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgfail.DataSource = dr
            Try
                dgfail.DataBind()
            Catch ex As Exception
                dgfail.CurrentPageIndex = 0
                dgfail.DataBind()
            End Try
            dr.Close()
            lblcom.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgfail.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgfail.PageCount
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopFail(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr20", "AppSetComTab.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopFail(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr21", "AppSetComTab.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgfail_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.CancelCommand
        dgfail.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopFail(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgfail_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            'id = CType(e.Item.Cells(1).Controls(1), Label).Text
            id = CType(e.Item.FindControl("Label13"), Label).Text
        Catch ex As Exception
            'id = CType(e.Item.Cells(1).Controls(2), TextBox).Text
            id = CType(e.Item.FindControl("Textbox9"), TextBox).Text
        End Try

        Dim fcnt As Integer = 0
        sql = "select count(*) from componentfailmodes where oaid = '" & id & "'"
        fcnt = appset.Scalar(sql)
        If fcnt = 0 Then
            sql = "delete from ocareas where oaid = '" & id & "'"
            appset.Update(sql)
            cid = lblcid.Value
            Dim failcnt As Integer = dgfail.VirtualItemCount - 1
            sql = "update AdminTasks set " _
            + "fail = '" & failcnt & "' where cid = '" & cid & "'"
            appset.Update(sql)


        Else
            Dim strMessage As String = "This Occurrence Area is Assigned to Components and Cannot be Deleted"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        sql = "select Count(*) from ocareas"
        PageNumber = appset.PageCount(sql, PageSize)
        'appset.Dispose()
        dgfail.EditItemIndex = -1
        If dgfail.CurrentPageIndex > PageNumber Then
            dgfail.CurrentPageIndex = PageNumber - 1
        End If
        If dgfail.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgfail.CurrentPageIndex + 1
        End If

        PopFail(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgfail_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.EditCommand
        PageNumber = txtpg.Value
        lblofm.Value = CType(e.Item.FindControl("Label14"), Label).Text
        dgfail.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopFail(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgfail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewfm"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            If lname <> "" Then
                If Len(lname) > 50 Then
                    Dim strMessage As String = "Occurrence Area is Limited to 50 Characters"

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    Dim fail As String = lname
                    cid = lblcid.Value
                    'sid = lblsid.Value
                    Dim strchk As Integer
                    Dim faill As String = fail.ToLower
                    sql = "select count(*) from ocareas where lower(ocarea) = '" & faill & "'"
                    strchk = appset.Scalar(sql)
                    If strchk = 0 Then
                        sql = "insert into ocareas " _
                        + "(ocarea) values ('" & lname & "')"
                        appset.Update(sql)
                        Dim failcnt As Integer = dgfail.VirtualItemCount + 1
                        dgfail.EditItemIndex = -1
                        PageNumber = 1
                        PopFail(PageNumber)
                    Else
                        Dim strMessage As String = "Cannot Enter a Duplicate Occurrence Area"

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        appset.Dispose()
                    End If
                    appset.Dispose()
                End If
            End If
        End If
    End Sub

    Private Sub dgfail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgfail.ItemDataBound

    End Sub

    Private Sub dgfail_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.UpdateCommand
        appset.Open()
        Dim id, desc As String
        cid = lblcid.Value
        id = CType(e.Item.FindControl("TextBox9"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        Dim ofm As String = lblofm.Value

        Dim faillo As String = ofm.ToLower
        Dim faill As String = desc.ToLower
        If faillo <> faill Then
            sql = "select count(*) from ocareas where lower(ocarea) = '" & faill & "'"
            Dim strchk As Integer
            strchk = appset.Scalar(sql)
            If strchk = 0 Then
                sql = "update ocareas set ocarea = " _
                       + "'" & desc & "' where oaid = '" & id & "'"
                appset.Update(sql)
            Else
                Dim strMessage As String = "Cannot Enter a Duplicate Occurrence Area"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'appset.Dispose()
            End If
        End If
        'appset.Dispose()
        dgfail.EditItemIndex = -1
        PopFail(PageNumber)
        appset.Dispose()
    End Sub
End Class