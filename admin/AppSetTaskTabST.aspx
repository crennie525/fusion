<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetTaskTabST.aspx.vb"
    Inherits="lucy_r12.AppSetTaskTabST" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetTaskTabST</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetTaskTabSTaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: -5px; left: 0px" width="600">
        <tr>
            <td width="100">
            </td>
            <td width="180">
            </td>
            <td width="20">
            </td>
            <td width="300">
            </td>
        </tr>
        <tr>
            <td style="height: 16px" colspan="3">
                <asp:Label ID="lblskill" runat="server" ForeColor="Red" Width="290px" Font-Bold="True"
                    Font-Names="Arial" Font-Size="X-Small"></asp:Label>
            </td>
            <td class="plainlabelblue" align="center" valign="middle" rowspan="4">
                <asp:Label ID="lang112" runat="server">Some Skills cannot be edited or deleted as they are required for some Fusion Operations.</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang113" runat="server">Search Skills</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="3">
                <asp:DataGrid ID="dgskill" runat="server" ShowFooter="True" CellPadding="0" GridLines="None"
                    AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue" Height="20px"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow" Height="20px"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="imgedit" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                &nbsp;
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton31" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton32" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblskillidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'
                                    NAME="Label21">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblskillid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skillid") %>'
                                    NAME="Label21">
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Skill">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label22" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewskill" runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox18" runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.skill") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Rate">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewrate" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rate") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox1" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.rate") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="OT Rate">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.otrate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewotrate" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.otrate") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="txtotrate" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.otrate") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
            <td width="20" valign="top">
                <img src="../images/appbuttons/minibuttons/plusminus.gif" onmouseover="return overlib('Choose Skills for this Plant Site ')"
                    onclick="getss();" onmouseout="return nd()" width="20" height="20">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblcid" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="txtpg" runat="server" name="Hidden1"><input type="hidden"
        id="txtpgcnt" runat="server" name="txtpgcnt">
    <input type="hidden" id="lblret" runat="server" name="lblret"><input type="hidden"
        id="lblold" runat="server" name="lblold">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
