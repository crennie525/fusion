

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class AppSetLT
    Inherits System.Web.UI.Page
	Protected WithEvents lang89 As System.Web.UI.WebControls.Label

	Protected WithEvents lang88 As System.Web.UI.WebControls.Label

	Protected WithEvents lang87 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim dept As String
    Dim Sort As String = "loctype"
    Dim cid, cnm, sid, did, sql, ro As String
    Dim dr As SqlDataReader
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents imgedit As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents imgdel As System.Web.UI.WebControls.ImageButton
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbltasktype As System.Web.UI.WebControls.Label
    Protected WithEvents dgtt As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ttdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtt.Columns(0).Visible = False
                dgtt.Columns(3).Visible = False
            End If
            cid = Request.QueryString("cid")
            If cid <> "" Then
                lblcid.Value = cid
                appset.Open()
                PopTasks(PageNumber)
                appset.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr71" , "AppSetLT.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr72" , "AppSetLT.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        'Try

        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
       srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "loctype like ''%" & srch & "%'' and compid = ''" & cid & "'' and typid <> 0"
            FilterCnt = "loctype like '%" & srch & "%' and compid = '" & cid & "' and typid <> 0"
        Else
            Filter = "compid = ''" & cid & "'' and typid <> 0"
            FilterCnt = "compid = '" & cid & "' and typid <> 0"
        End If
        sql = "select count(*) " _
        + "from pmloctypes where " & FilterCnt 'compid = '" & cid & "'"
        dgtt.VirtualItemCount = appset.Scalar(sql)
        If dgtt.VirtualItemCount = 0 Then
            lbltasktype.Text = "No Location Type Records Found"
            dgtt.Visible = True
            Tables = "pmloctypes"
            PK = "typid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        Else
            Tables = "pmloctypes"
            PK = "typid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            lbltasktype.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        End If

        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub dgtt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewtt"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr73" , "AppSetLT.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        cid = lblcid.Value
                        Dim tt As String
                        Dim strchk As Integer
                        Dim faill As String = lname.ToLower
                        sql = "select count(*) from pmLocTypes where lower(loctype) = '" & lname & "' and compid = '" & cid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "sp_addLocType '" & lname & "', " & cid
                            appset.Update(sql)
                            Dim ttcnt As Integer = dgtt.VirtualItemCount + 1

                            lnamei.Text = ""
                            sql = "select Count(*) from pmLocTypes " _
                               + "where compid = '" & cid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopTasks(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr74" , "AppSetLT.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If
                        appset.Dispose()
                    End If

                End If
            End If
        End If
    End Sub

    Private Sub dgtt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.DeleteCommand
        appset.Open()
        Dim id, lid As String
        cid = lblcid.Value
        Try
            id = CType(e.Item.FindControl("lblttids"), Label).Text
            lid = CType(e.Item.FindControl("lblloctypei"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblttide"), Label).Text
            lid = CType(e.Item.FindControl("lblloctypee"), TextBox).Text
        End Try
        'id = CType(e.Item.Cells(1).Controls(1), Label).Text
        sql = "select count(*) from pmLocations where type = '" & lid & "'"
        Dim tcnt As Integer = appset.Scalar(sql)
        If tcnt = 0 Then
            sql = "sp_delLocType '" & id & "', '" & cid & "'"
            appset.Update(sql)
            dgtt.EditItemIndex = -1
            sql = "select Count(*) from pmLocTypes " _
                   + "where compid = '" & cid & "'"
            PageNumber = appset.PageCount(sql, PageSize)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            If dgtt.CurrentPageIndex > PageNumber Then
                dgtt.CurrentPageIndex = PageNumber - 1
            End If
            If dgtt.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgtt.CurrentPageIndex + 1
            End If
            PopTasks(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr75" , "AppSetLT.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub

    Private Sub dgtt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("lblloctypei"), Label).Text
        dgtt.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.CancelCommand
        dgtt.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.UpdateCommand
        appset.Open()
        Dim id, desc As String
        id = CType(e.Item.FindControl("lblttide"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        'desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        Dim old As String = lblold.Value
        If old <> desc Then
            Dim faill As String = desc.ToLower
            sql = "select count(*) from pmLocTypes where lower(loctype) = '" & desc & "' and compid = '" & cid & "'"
            Dim strchk As Integer
            strchk = appset.Scalar(sql)
            If strchk = 0 Then
                sql = "update pmLocTypes set loctype = " _
                + "'" & desc & "' where typid = '" & id & "';update pmlocations set " _
                + "type = '" & desc & "' where type = '" & old & "'"
                appset.Update(sql)
                'appset.Dispose()
                dgtt.EditItemIndex = -1
                PopTasks(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr76" , "AppSetLT.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
            End If
        Else
            dgtt.EditItemIndex = -1
            PopTasks(PageNumber)
        End If
        appset.Dispose()
    End Sub


	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtt.Columns(0).HeaderText = dlabs.GetDGPage("AppSetLT.aspx", "dgtt", "0")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(2).HeaderText = dlabs.GetDGPage("AppSetLT.aspx", "dgtt", "2")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(3).HeaderText = dlabs.GetDGPage("AppSetLT.aspx", "dgtt", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang87.Text = axlabs.GetASPXPage("AppSetLT.aspx", "lang87")
        Catch ex As Exception
        End Try
        Try
            lang88.Text = axlabs.GetASPXPage("AppSetLT.aspx", "lang88")
        Catch ex As Exception
        End Try
        Try
            lang89.Text = axlabs.GetASPXPage("AppSetLT.aspx", "lang89")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub dgtt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtt.ItemDataBound
        Dim ibfm As ImageButton
        Dim ibfm1 As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim stat As String = DataBinder.Eval(e.Item.DataItem, "loctype").ToString
            If stat = "POSITION" Or stat = "EQUIPMENT" Then
                ibfm = CType(e.Item.FindControl("imgedit"), ImageButton)
                ibfm.Attributes.Add("class", "details")
                ibfm1 = CType(e.Item.FindControl("imgdel"), ImageButton)
                ibfm1.Attributes.Add("class", "details")

            End If
        End If
    End Sub
End Class
