<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="logtrack.aspx.vb" Inherits="lucy_r12.logtrack" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>logtrack</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/reportnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/logtrackaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="bluelabel" width="250">
                            <asp:Label ID="lang165" runat="server">Search By User Name or Page</asp:Label>
                        </td>
                        <td width="210">
                            <asp:TextBox ID="txtsrch" runat="server" Width="200px"></asp:TextBox>
                        </td>
                        <td width="20">
                            <img src="../images/appbuttons/minibuttons/srchsm.gif" onclick="srchq();">
                        </td>
                        <td width="20">
                            <img src="../images/appbuttons/minibuttons/refreshit.gif" onclick="resetq();">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trlt" runat="server">
            <td>
                <div id="divlt" style="border-bottom: black 1px solid; border-left: black 1px solid;
                    width: 720px; height: 260px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                    runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="190">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblsubmit" type="hidden" runat="server" name="lblsubmit">
    <input id="txtpgcnt" type="hidden" runat="server" name="txtpgcnt"><input id="txtpg"
        type="hidden" runat="server" name="txtpg">
    <input type="hidden" id="lblusername" runat="server" name="lblusername">
    <input type="hidden" id="lblcurr" runat="server" name="lblcurr">
    <input type="hidden" id="lblsrch" runat="server" name="lblsrch">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
