

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class comptypes
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim dr As SqlDataReader
    Protected WithEvents lblpre As System.Web.UI.WebControls.Label
    Protected WithEvents dgskill As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnprev As System.Web.UI.WebControls.Button
    Protected WithEvents btnnext As System.Web.UI.WebControls.Button
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            PopSkill(PageNumber)
        End If
    End Sub
    Private Sub PopSkill(ByVal PageNumber As Integer)
        'Try
        Try
            appset.Open()
        Catch ex As Exception

        End Try
        cid = lblcid.Value
        sql = "select count(*) " _
        + "from valuelist where listname = 'COMPTYPE'"
        dgskill.VirtualItemCount = appset.Scalar(sql)
        If dgskill.VirtualItemCount = 0 Then
            lblpre.Text = "No Company Type Records Found"
            dgskill.Visible = True
            btnprev.Visible = False
            btnnext.Visible = False

            Filter = "listname = '0'"
            Tables = "valuelist"
            PK = "value"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgskill.DataSource = dr
            dgskill.DataBind()
            dr.Close()
            appset.Dispose()

        Else
            Filter = "listname = ''COMPTYPE''"
            Tables = "valuelist"
            PK = "value"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgskill.DataSource = dr
            dgskill.DataBind()
            dr.Close()
            appset.Dispose()
            tdpg.InnerHtml = "Page " & dgskill.CurrentPageIndex + 1 & " of " & dgskill.PageCount
            lblpre.Text = ""
            btnprev.Visible = True
            btnnext.Visible = True

        End If
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub dgskill_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.CancelCommand
        dgskill.EditItemIndex = -1
        PopSkill(PageNumber)
    End Sub

    Private Sub dgskill_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.EditCommand
        PageNumber = dgskill.CurrentPageIndex + 1
        dgskill.EditItemIndex = e.Item.ItemIndex
        PopSkill(PageNumber)
    End Sub

    Private Sub dgskill_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.UpdateCommand
        appset.Open()
        Dim id, desc As String
        id = CType(e.Item.FindControl("txtws"), TextBox).Text
        id = Replace(id, "'", Chr(180), , , vbTextCompare)
        desc = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        Dim faill As String = id.ToLower
        cid = lblcid.Value
        sql = "select count(*) from valuelist where lower(value) = '" & faill & "'"
        Dim strchk As Integer
        strchk = appset.Scalar(sql)
        If strchk = 0 Or strchk = 1 Then
            sql = "update valuelist set value = " _
            + "'" & id & "', valdesc = '" & desc & "' where value = '" & id & "' and listname = 'COMPTYPE'"
            appset.Update(sql)
            'appset.Dispose()
            dgskill.EditItemIndex = -1
            PopSkill(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr175" , "comptypes.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
    End Sub
    Private Sub checkPrePgCnt()
        If (dgskill.CurrentPageIndex) = 0 Or dgskill.PageCount = 1 Then
            btnprev.Enabled = False
        Else
            btnprev.Enabled = True
        End If
        If dgskill.PageCount > 1 And (dgskill.CurrentPageIndex + 1 < dgskill.PageCount) Then
            btnnext.Enabled = True
        Else
            btnnext.Enabled = False
        End If
    End Sub
    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnext.Click
        If ((dgskill.CurrentPageIndex) < (dgskill.PageCount - 1)) Then
            dgskill.CurrentPageIndex = dgskill.CurrentPageIndex + 1
            PopSkill(dgskill.CurrentPageIndex + 1)
            checkPrePgCnt()
        End If
        checkPrePgCnt()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnprev.Click
        If (dgskill.CurrentPageIndex > 0) Then
            dgskill.CurrentPageIndex = dgskill.CurrentPageIndex - 1
            PopSkill(dgskill.CurrentPageIndex - 1)
        End If
        checkPrePgCnt()
    End Sub

    Private Sub dgskill_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.ItemCommand
        Dim lname, desc As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtwsa"), TextBox)
            lname = lnamei.Text
            lname = Replace(lname, "'", Chr(180), , , vbTextCompare)
            desc = CType(e.Item.FindControl("txtdesca"), TextBox).Text
            desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
            'Dim faill As String = lname.ToLower
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr176" , "comptypes.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        Dim stat As String
                        cid = lblcid.Value
                        stat = lname
                        Dim strchk As Integer
                        Dim faill As String = stat.ToLower
                        sql = "select count(*) from valuelist where lower(value) = '" & faill & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "insert into valuelist (listname, value, valdesc, defaults, type) values ('COMPTYPE', '" & lname & "','" & desc & "','Y','4')"
                            appset.Update(sql)
                            Dim statcnt As Integer = dgskill.VirtualItemCount + 1
                            lnamei.Text = ""
                            sql = "select Count(*) from valuelist " _
                                            + "where listname = 'COMPTYPE'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopSkill(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr177" , "comptypes.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If

                    End If

                End If
            End If
        End If
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgskill.Columns(0).HeaderText = dlabs.GetDGPage("comptypes.aspx","dgskill","0")
		Catch ex As Exception
		End Try
		Try
			dgskill.Columns(1).HeaderText = dlabs.GetDGPage("comptypes.aspx","dgskill","1")
		Catch ex As Exception
		End Try
		Try
			dgskill.Columns(2).HeaderText = dlabs.GetDGPage("comptypes.aspx","dgskill","2")
		Catch ex As Exception
		End Try
		Try
			dgskill.Columns(3).HeaderText = dlabs.GetDGPage("comptypes.aspx","dgskill","3")
		Catch ex As Exception
		End Try

	End Sub

End Class
