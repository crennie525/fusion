<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetWo.aspx.vb" Inherits="lucy_r12.AppSetWo" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetWo</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .details
        {
            display: none;
            visibility: hidden;
            font-family: Verdana;
            background-color: white;
        }
    </style>
    <script language="javascript" type="text/javascript" src="../scripts/wotab.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetWoaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        var divflg;

        function ltab() {
            var coi = document.getElementById("lblcoi").value
            var cd = document.getElementById("lblcid").value;
            document.getElementById("ifl").src = "AppSetWoType.aspx?cid=" + cd;
            document.getElementById("txttab").value = "l";
            document.getElementById("l").className = "thdrhov plainlabel";
            document.getElementById("ltdiv").style.display = "none";
            document.getElementById("ltdiv").style.visibility = "hidden";
            document.getElementById("ldiv").style.display = 'block';
            document.getElementById("ldiv").style.visibility = 'visible';
            document.getElementById("rcdiv").style.display = "none";
            document.getElementById("rcdiv").style.visibility = 'hidden';
            if (coi != "NISS" && coi != "ELUX") {
                document.getElementById("pridiv").style.display = 'none';
                document.getElementById("pridiv").style.visibility = 'hidden';
            }
        }
        function lrtab() {
            var coi = document.getElementById("lblcoi").value
            var cd = document.getElementById("lblcid").value;
            document.getElementById("ifl").src = "appsetwovars.aspx?cid=" + cd;
            document.getElementById("txttab").value = "lr";
            document.getElementById("lr").className = "thdrhov plainlabel";
            document.getElementById("ltdiv").style.display = "none";
            document.getElementById("ltdiv").style.visibility = "hidden";
            document.getElementById("ldiv").style.display = 'block';
            document.getElementById("ldiv").style.visibility = 'visible';
            document.getElementById("rcdiv").style.display = "none";
            document.getElementById("rcdiv").style.visibility = 'hidden';
            if (coi != "NISS" && coi != "ELUX") {
                document.getElementById("pridiv").style.display = 'none';
                document.getElementById("pridiv").style.visibility = 'hidden';
            }
        }
        function lttab() {
            var coi = document.getElementById("lblcoi").value
            var cd = document.getElementById("lblcid").value;
            document.getElementById("ifl").src = "AppSetWoStatus.aspx?cid=" + cd;
            document.getElementById("txttab").value = "l";
            document.getElementById("lt").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("rcdiv").style.display = "none";
            document.getElementById("rcdiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'block';
            document.getElementById("ltdiv").style.visibility = 'visible';
            if (coi != "NISS" && coi != "ELUX") {
                document.getElementById("pridiv").style.display = 'none';
                document.getElementById("pridiv").style.visibility = 'hidden';
            }
        }
        function rctab() {
            var coi = document.getElementById("lblcoi").value
            var cd = document.getElementById("lblcid").value;
            document.getElementById("ifrc").src = "appsetrootcause.aspx?cid=" + cd;
            document.getElementById("txttab").value = "rc";
            document.getElementById("rc").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("rcdiv").style.display = "block";
            document.getElementById("rcdiv").style.visibility = 'visible';
            document.getElementById("ltdiv").style.display = 'none';
            document.getElementById("ltdiv").style.visibility = 'hidden';
            if (coi != "NISS" && coi != "ELUX") {
                document.getElementById("pridiv").style.display = 'none';
                document.getElementById("pridiv").style.visibility = 'hidden';
            }
        }
        function pritab() {
            var coi = document.getElementById("lblcoi").value
            var cd = document.getElementById("lblcid").value;
            document.getElementById("ifpri").src = "appsetwopriority.aspx?cid=" + cd;
            document.getElementById("txttab").value = "pri";
            document.getElementById("pri").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("rcdiv").style.display = "block";
            document.getElementById("rcdiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'none';
            document.getElementById("ltdiv").style.visibility = 'hidden';
            if (coi != "NISS" && coi != "ELUX") {
                document.getElementById("pridiv").style.display = 'block';
                document.getElementById("pridiv").style.visibility = 'visible';
            }
        }
        function gettab(id) {
        var coi = document.getElementById("lblcoi").value
            document.getElementById("lt").className = "thdr plainlabel";
            document.getElementById("l").className = "thdr plainlabel";
            document.getElementById("lr").className = "thdr plainlabel";
            document.getElementById("rc").className = "thdr plainlabel";
            if (coi != "NISS" && coi != "ELUX") {
                document.getElementById("pri").className = "thdr plainlabel";
            }
            if (id == "lt") {
                lttab();
            }
            else if (id == "l") {
                ltab();
            }
            else if (id == "lr") {
                lrtab();
            }
            else if (id == "rc") {
                rctab();
            }
            else if (id == "pri") {
                pritab();
            }
        }
        function checktab() {
            var app = document.getElementById("appchk").value;
            if (app == "switch") window.parent.handleapp(app);
            divflg = document.getElementById("txttab").value
            gettab(divflg);
        }
    //-->
    </script>
</head>
<body onload="checktab();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div id="ltdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 5px; visibility: visible; border-left: 2px groove; width: 870px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="iflt" style="width: 850px; height: 400px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="ldiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 870px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="ifl" style="width: 850px; height: 400px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="rcdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 870px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="ifrc" style="width: 850px; height: 400px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="pridiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 870px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="ifpri" style="width: 850px; height: 400px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <table style="left: 5px; position: absolute; top: 10px" cellspacing="3" cellpadding="3" width="850">
        <tr height="20">
            <td id="lt" onclick="gettab('lt');" class="thdrhov plainlabel" width="140">
                <asp:Label ID="lang116" runat="server">Work Order Status</asp:Label>
            </td>
            <td id="l" onclick="gettab('l');" class="thdr plainlabel" width="140">
                <asp:Label ID="lang117" runat="server">Work Order Type</asp:Label>
            </td>
            <td id="pri" onclick="gettab('pri');" class="thdr plainlabel" width="140" runat="server">
                <asp:Label ID="Label1" runat="server">Work Order Priority</asp:Label>
            </td>
            <td id="rc" onclick="gettab('rc');" class="thdr plainlabel" width="160">
                Root Cause
            </td>
            <td id="lr" onclick="gettab('lr');" class="thdr plainlabel" width="300">
                <asp:Label ID="lang118" runat="server">Labor\Cost Reporting Defaults</asp:Label>
            </td>
        </tr>
    </table>
    <input type="hidden" id="txttab" runat="server" name="appchk">
    <input type="hidden" id="lbltab" runat="server" name="appchk">
    <input type="hidden" id="lblcid" runat="server" name="appchk">
    <input type="hidden" id="lbltl" runat="server" name="appchk">
    <input type="hidden" id="appchk" runat="server" name="appchk"><input type="hidden"
        id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    </form>
</body>
</html>
