<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChargeSelect.aspx.vb"
    Inherits="lucy_r12.ChargeSelect" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ChargeSelect</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/ChargeSelectaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function refit() {
            //alert()
            var dobj = document.getElementById("ddtype");
            dobj.selectedIndex = 0;
            document.getElementById("txtsrch").value = ""
            document.getElementById("lblret").value = "first"
            document.getElementById("form1").submit();
        }
        function getcharge(val) {
        //alert(val)
            var who = document.getElementById("lblwho").value;
            if (who == "wo") {
                document.getElementById("lblval").value = val;
                document.getElementById("lblsubmit").value = "upwo";
                document.getElementById("form1").submit();
            }
            else {

                window.parent.handleret(val);
            }
        }
        function checkit() {
        
            window.setTimeout("setref();", 300000);
            var ret = document.getElementById("lblret").value;
            var val = document.getElementById("lblval").value;
            //alert(ret)
            if (ret == "go") {
                window.parent.handleret(val);
            }
        }
        function setref() {
            //window.parent.handlesup("");
        }
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="bluelabel">
                <asp:Label ID="lang134" runat="server">Search</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
            <td>
            <img src="../images/appbuttons/minibuttons/refreshit.gif" onclick="refit();" alt="" />
            </td>
        </tr>
        <tr id="trtype" runat="server">
            <td class="bluelabel">
                <asp:Label ID="lang135" runat="server">Filter By:</asp:Label>
            </td>
            <td colspan="3">
                <asp:DropDownList ID="ddtype" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div style="border-bottom: black 1px solid; border-left: black 1px solid; height: 200px; width: 300px;
                    overflow: auto; border-top: black 1px solid; border-right: black 1px solid" id="chargediv"
                    runat="server">
                </div>
            </td>
        </tr>
        <tr>
        <td colspan="4" align="center">
         <table>
        <tr class="tbg">
            <td width="18">
            </td>
            <td width="180">
            </td>
            <td width="18">
            </td>
        </tr>
        <tr class="tbg">
            <td>
                <asp:ImageButton ID="ttPrev" runat="server" ImageUrl="../images/appbuttons/minibuttons/prevarrowbg.gif">
                </asp:ImageButton>
            </td>
            <td align="center">
                <asp:Label ID="lblpg" runat="server" ForeColor="Blue" Font-Names="Arial" Font-Size="X-Small"></asp:Label>
            </td>
            <td>
                <asp:ImageButton ID="ttNext" runat="server" ImageUrl="../images/appbuttons/minibuttons/nextarrowbg.gif">
                </asp:ImageButton>
            </td>
        </tr>
    </table>
        </td>
        </tr>
    </table>
   
    <input type="hidden" id="lblcid" runat="server">
    <input type="hidden" id="lblfilt" runat="server">
    <input type="hidden" id="lblfiltd" runat="server">
    <input type="hidden" id="lblpgcnt" runat="server">
    <input type="hidden" id="lblro" runat="server"><input type="hidden" id="lblwho" runat="server">
    <input type="hidden" id="lblwonum" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server"><input type="hidden" id="lblval"
        runat="server">
    <input type="hidden" id="lblret" runat="server">
    <input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt"
        type="hidden" name="txtpgcnt" runat="server">
    </form>
</body>
</html>
