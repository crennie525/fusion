﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="appsetrootcause.aspx.vb" Inherits="lucy_r12.appsetrootcause" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Root Cause</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script type="text/javascript" language="javascript">
    <!--

    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table class="tbg" id="ttdiv" cellspacing="0" cellpadding="0" runat="server">
        <tr>
            <td width="100">
            </td>
            <td width="180">
            </td>
            <td width="140">
            </td>
            <td width="320"></td>
        </tr>
        <tr class="tbg">
            <td style="height: 16px" colspan="3">
                <asp:Label ID="lbltasktype" runat="server" Font-Size="X-Small" Font-Names="Arial"
                    Font-Bold="True" Width="256px" ForeColor="Red"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang125" runat="server">Search</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
            <td class="label" align="center">Choose Work Types That Require Root Cause Entry</td>
        </tr>
        <tr class="tbg">
            <td valign="top" align="center" colspan="3">
                <asp:DataGrid ID="dgtt" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                    AllowPaging="True" GridLines="None" CellPadding="0" CellSpacing="2" ShowFooter="True"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="imgedit" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit This Task Type" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton27" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton28" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblttids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rootid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rootid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Root Cause">
                            <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.rootcause") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewtt" runat="server" Width="240px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.rootcause") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox16" runat="server" Width="240px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.rootcause") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        
                       
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid><br />
            </td>
            <td align="center" valign="top">
            <table>
            <tr>
            <td class="bluelabel" align="center">Available</td>
            <td>&nbsp;</td>
            <td class="bluelabel" align="center">Required</td>
            </tr>
            <tr>
            <td class="plainlabel">
                <asp:ListBox ID="lbwt" runat="server" Height="150px" Width="130px"></asp:ListBox></td>
            <td align="center" valign="middle"><asp:ImageButton
                ID="ImageButton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif" /><br />
                <asp:ImageButton
                ID="ImageButton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif" />

            </td>
            <td class="plainlabel"><asp:ListBox ID="lbrq" runat="server" Height="150px" Width="130px"></asp:ListBox></td>
            </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblrqstr" runat="server" />
    <input type="hidden" id="lblold" runat="server" />
    <input type="hidden" id="txtpg" runat="server" /><input type="hidden"
        id="txtpgcnt" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
