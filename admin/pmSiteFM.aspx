<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmSiteFM.aspx.vb" Inherits="lucy_r12.pmSiteFM" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmSiteFM</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmSiteFMaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table width="422">
        <tr>
            <td class="thdrsingrt label" colspan="3">
                <asp:Label ID="lang171" runat="server">Add/Edit Site Failure Modes Mini Dialog</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" width="172" height="26">
                <asp:Label ID="lang172" runat="server">Current Plant Site:</asp:Label>
            </td>
            <td class="labellt" id="tdsite" width="250" height="26" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border-right: #0000ff 1px solid; border-top: #0000ff 1px solid; border-left: #0000ff 1px solid;
                    border-bottom: #0000ff 1px solid">
            </td>
        </tr>
    </table>
    <table width="422">
        <tr id="trmsg" runat="server" class="details">
            <td colspan="3" align="center">
                <asp:CheckBox ID="cbprep" runat="server" CssClass="redlabel" AutoPostBack="True"
                    Text="Use Site Failure Modes?"></asp:CheckBox>
            </td>
        </tr>
        <tr id="trmsg1" runat="server" class="details">
            <td colspan="3" align="center">
                <asp:CheckBox ID="cbunprep" runat="server" CssClass="redlabel" AutoPostBack="True"
                    Text="Do Not Use Site Failure Modes?"></asp:CheckBox>
            </td>
        </tr>
        <tr>
            <td class="label" align="center">
                <asp:Label ID="lang173" runat="server">Available Failure Modes</asp:Label>
            </td>
            <td>
            </td>
            <td class="label" align="center">
                <asp:Label ID="lang174" runat="server">Site Failure Modes</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" width="200">
                <asp:ListBox ID="lbfailmaster" runat="server" Height="150px" SelectionMode="Multiple"
                    Width="170px"></asp:ListBox>
            </td>
            <td valign="middle" align="center" width="22">
                <img class="details" id="todis" runat="server" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                    width="20" height="20">
                <img class="details" id="fromdis" runat="server" src="../images/appbuttons/minibuttons/backgraybg.gif"
                    width="20" height="20"><br>
                <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                </asp:ImageButton><asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                </asp:ImageButton>
            </td>
            <td align="center" width="200">
                <asp:ListBox ID="lbsitefail" runat="server" Height="150px" SelectionMode="Multiple"
                    Width="170px"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">
                <img id="btnreturn" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
                    runat="server" width="69" height="19">
            </td>
        </tr>
        <tr>
            <td class="bluelabel" colspan="3">
                <asp:Label ID="lang175" runat="server">List Box Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" colspan="3">
                <asp:RadioButtonList ID="cbopts" runat="server" Width="400px" CssClass="labellt"
                    AutoPostBack="True">
                    <asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
                    <asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="note" colspan="3">
                <asp:Label ID="lang176" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Site Failure Mode list if only one item is present.</asp:Label>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lblco" type="hidden" name="lblco" runat="server"><input id="lblapp" type="hidden"
        name="lblapp" runat="server">
    <input id="lblopt" type="hidden" name="lblopt" runat="server"><input type="hidden"
        id="lblfailchk" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
