<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="compmain.aspx.vb" Inherits="lucy_r12.compmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>compmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/compmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function getcomp() {
            var eReturn = window.showModalDialog("admindialog.aspx?list=comp&typ=all", "", "dialogHeight:500px; dialogWidth:640px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    jumpto2(eReturn);
                }
            }
        }
        function jumpto2(id) {

            if (id != "") {
                var cd = id.split(",")
                //alert(cd[0])
                window.location = "compmain.aspx?jump=yes&co=" + cd[0];
            }
        }
        //-->
    </script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 7px; position: absolute; top: 4px" cellspacing="2" cellpadding="0"
        width="722">
        <tbody>
            <tr height="20">
                <td class="thdrhov plainlabel" id="tdcomp" onclick="gettab('co');" width="180">
                    <asp:Label ID="lang137" runat="server">Companies</asp:Label>
                </td>
                <td class="thdr plainlabel" id="tdcont" onclick="gettab('sc');" width="180">
                    <asp:Label ID="lang138" runat="server">Service Contracts</asp:Label>
                </td>
                <td width="362">
                    &nbsp;
                </td>
            </tr>
            <tr id="trcomp">
                <td colspan="3">
                    <table cellspacing="0" cellpadding="2" width="722">
                        <tr>
                            <td class="thdrsing label" width="722" bgcolor="#1d11ef" colspan="3">
                                <asp:Label ID="lang139" runat="server">Add/Edit Companies</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3">
                                <table cellpadding="0" width="680" border="0">
                                    <tr>
                                        <td width="120">
                                        </td>
                                        <td width="190">
                                        </td>
                                        <td width="22">
                                        </td>
                                        <td width="22">
                                        </td>
                                        <td width="22">
                                        </td>
                                        <td width="22">
                                        </td>
                                        <td width="8">
                                        </td>
                                        <td width="110">
                                        </td>
                                        <td width="172">
                                        </td>
                                    </tr>
                                    <tr id="Tr5" runat="server">
                                        <td class="label">
                                            <asp:Label ID="lang140" runat="server">Company Code</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtcode" runat="server" Width="180px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                        <td>
                                            <img id="imgadd" onclick="addloc();" src="../images/appbuttons/minibuttons/addnew.gif"
                                                runat="server">
                                        </td>
                                        <td>
                                            <img id="imgsrch" onclick="getcomp();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                                runat="server">
                                        </td>
                                        <td>
                                            <img id="Img1" onclick="saveloc();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                                runat="server">
                                        </td>
                                        <td>
                                            <img onclick="jumpto('');" src="../images/appbuttons/minibuttons/srchsm.gif">
                                        </td>
                                    </tr>
                                    <tr id="deptdiv" runat="server">
                                        <td class="label">
                                            <asp:Label ID="lang141" runat="server">Company Name</asp:Label>
                                        </td>
                                        <td colspan="8">
                                            <asp:TextBox ID="txtloc" runat="server" Width="490px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr2" runat="server">
                                        <td class="label">
                                            <asp:Label ID="lang142" runat="server">Home Page</asp:Label>
                                        </td>
                                        <td colspan="8">
                                            <asp:TextBox ID="txthome" runat="server" Width="490px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr1" runat="server">
                                        <td class="label">
                                            <asp:Label ID="lang143" runat="server">Company Type</asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddtype" runat="server" Width="180px" CssClass="plainlabel"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="thdrsing label" width="722" bgcolor="#1d11ef" colspan="3">
                                <asp:Label ID="lang144" runat="server">Address</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="3">
                                <table cellpadding="0" width="680" border="0">
                                    <tr>
                                        <td width="90">
                                        </td>
                                        <td width="190">
                                        </td>
                                        <td width="110">
                                        </td>
                                        <td width="202">
                                        </td>
                                        <td width="22">
                                        </td>
                                        <td width="22">
                                        </td>
                                        <td width="22">
                                        </td>
                                        <td width="22">
                                        </td>
                                    </tr>
                                    <tr id="Tr3" runat="server">
                                        <td class="label">
                                            <asp:Label ID="lang145" runat="server">Street</asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="txtadd1" runat="server" Width="490px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr4" runat="server">
                                        <td class="label">
                                            <asp:Label ID="lang146" runat="server">City</asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="txtadd2" runat="server" Width="490px" CssClass="plainlabel"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang147" runat="server">State</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtadd3" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang148" runat="server">Zip Code</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtadd4" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            <asp:Label ID="lang149" runat="server">Phone</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtph" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="label">
                                            <asp:Label ID="lang150" runat="server">Fax</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtfax" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="details" width="722" bgcolor="#1d11ef" colspan="3">
                                <asp:Label ID="lang151" runat="server">Contacts</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:DataGrid ID="dglabor" runat="server" ShowFooter="True" AllowCustomPaging="True"
                                    AllowPaging="True" PageSize="5" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                                    <HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Edit">
                                            <HeaderStyle Width="60px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                                    CommandName="Edit"></asp:ImageButton>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                                    CommandName="Reset"></asp:ImageButton>
                                            </FooterTemplate>
                                            <EditItemTemplate>
                                                &nbsp;
                                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1"
                                                    CommandName="Update"></asp:ImageButton>
                                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                                    CommandName="Cancel"></asp:ImageButton>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Contact">
                                            <HeaderStyle Width="120px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.contact") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtcontacta" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.contact") %>'>
                                                </asp:TextBox>
                                            </FooterTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtcontact" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.contact") %>'>
                                                </asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Position">
                                            <HeaderStyle Width="120px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.position") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtpositiona" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.position") %>'>
                                                </asp:TextBox>
                                            </FooterTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtposition" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.position") %>'>
                                                </asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Phone">
                                            <HeaderStyle Width="120px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.voicephone") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtphonea" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.voicephone") %>'>
                                                </asp:TextBox>
                                            </FooterTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtphone" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.voicephone") %>'>
                                                </asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Fax">
                                            <HeaderStyle Width="120px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.faxphone") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtfaxa" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.faxphone") %>'>
                                                </asp:TextBox>
                                            </FooterTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="Textbox1" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.faxphone") %>'>
                                                </asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Email">
                                            <HeaderStyle Width="120px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtemaila" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
                                                </asp:TextBox>
                                            </FooterTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtemail" runat="server" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
                                                </asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Delete">
                                            <HeaderStyle Width="50px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                                    CommandName="Delete"></asp:ImageButton>
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                            <FooterTemplate>
                                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnew.gif"
                                                    CommandName="Add"></asp:ImageButton>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle Visible="False"></PagerStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trcont" class="details">
                <td colspan="3" valign="top">
                    <iframe id="ifwr" style="width: 725px; height: 500px; background-color: transparent"
                        src="" frameborder="no" scrolling="auto" runat="server" allowtransparency></iframe>
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" class="plainlabelred" colspan="3">
                    <asp:Label ID="lang152" runat="server">Add Companies if you will be using Fusion for Inventory Control or Work Order Management.</asp:Label>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="details" id="subdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 400px; border-bottom: black 1px solid;
        height: 320px">
        <table cellspacing="0" cellpadding="0" width="400" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="whtlbl">
                    <asp:Label ID="lang153" runat="server">Company Lookup</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closecraft();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
                        width="18"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="ifcraft" style="width: 400px; height: 300px" frameborder="no" runat="server">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblco" type="hidden" runat="server"><input id="lblfail" type="hidden"
        runat="server" name="lblfail">
    <input id="lblsubmit" type="hidden" runat="server" name="lblsubmit"><input id="lblchild"
        type="hidden" runat="server" name="lblchild">
    <input type="hidden" id="lbltype" runat="server" name="lbltype">
    <input type="hidden" id="lbloldfail" runat="server" name="lbloldfail">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
