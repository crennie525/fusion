

'********************************************************
'*
'********************************************************



Public Class AppSetWo
    Inherits System.Web.UI.Page
	Protected WithEvents lang118 As System.Web.UI.WebControls.Label

	Protected WithEvents lang117 As System.Web.UI.WebControls.Label

	Protected WithEvents lang116 As System.Web.UI.WebControls.Label
    Dim mu As New mmenu_utils_a
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim coi As String
    Dim cid, sid, ro As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pri As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iflt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifl As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txttab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        coi = mu.COMPI
        lblcoi.Value = coi
        If coi = "NISS" Or coi = "ELUX" Then
            pri.Attributes.Add("class", "details")
        End If

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        cid = "0"
        If cid <> "" Then
            lblcid.Value = cid
            cid = lblcid.Value
            iflt.Attributes.Add("src", "AppSetWoStatus.aspx?cid=" + cid)
            lbltab.Value = "lt"
        End If
        Try
            ro = HttpContext.Current.Session("ro").ToString
        Catch ex As Exception
            ro = "0"
        End Try
        lblro.Value = ro
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang116.Text = axlabs.GetASPXPage("AppSetWo.aspx","lang116")
		Catch ex As Exception
		End Try
		Try
			lang117.Text = axlabs.GetASPXPage("AppSetWo.aspx","lang117")
		Catch ex As Exception
		End Try
		Try
			lang118.Text = axlabs.GetASPXPage("AppSetWo.aspx","lang118")
		Catch ex As Exception
		End Try

	End Sub

End Class
