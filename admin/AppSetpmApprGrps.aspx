<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetpmApprGrps.aspx.vb"
    Inherits="lucy_r12.AppSetpmApprGrps" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetpmApprGrps</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetpmApprGrpsaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table width="420" cellspacing="0" cellpadding="0" style="left: 0px; position: absolute;
        top: 0px">
        <tr class="details">
            <td colspan="4">
                <asp:Label ID="lblpre" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
                    Width="310px" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="plainlabel">
                <asp:Label ID="lang97" runat="server">Note: Each Approval Level is granted edit access to all Approval Levels below it.</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:DataGrid ID="dgac" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                    AllowPaging="True" GridLines="None" CellPadding="0" ShowFooter="True" PageSize="3"
                    CellSpacing="1" BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton34" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit This Asset Class" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                &nbsp;
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton35" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton36" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblapprgrpa" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.apprgrp") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblapprgrp" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.apprgrp") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Level">
                            <HeaderStyle Width="40px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="A1" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.grplevel") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtlo" runat="server" Width="30px" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.grplevel") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Approval Group">
                            <HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label24" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.grpname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewac" runat="server" MaxLength="50" Width="150px" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.grpname") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox20" runat="server" MaxLength="50" Width="150px" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.grpname") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Add/Remove">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label1" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.addrem") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddaddremftr" runat="server" Width="60px" CssClass="plainlabel">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                </asp:DropDownList>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddaddrem" runat="server" CssClass="plainlabel" Width="60px"
                                    SelectedIndex='<%# GetSelIndex(Container.DataItem("addremid")) %>'>
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">No</asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Add Users">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;
                                <img id="ibtnppl" runat="server" src="../images/appbuttons/minibuttons/people2.gif">
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;
                                <asp:ImageButton ID="Imagebutton37" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblpgac" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
                    ForeColor="Blue"></asp:Label>
            </td>
            <td align="right">
                <asp:ImageButton ID="prePrev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif">
                </asp:ImageButton>
            </td>
            <td align="right">
                <asp:ImageButton ID="preNext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td width="70">
            </td>
            <td width="100">
            </td>
            <td width="70">
            </td>
            <td width="70">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblcid" runat="server" name="lblcid">
    <input type="hidden" id="lbloldtasknum" runat="server">
    <input type="hidden" id="lblchanges" runat="server">
    <input type="hidden" id="lblrev" runat="server">
    <input id="lbllog" type="hidden" runat="server" name="lbllog">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
