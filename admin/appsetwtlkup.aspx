﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="appsetwtlkup.aspx.vb" Inherits="lucy_r12.appsetwtlkup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lookup Dialog</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function getstat(ret) {
            window.returnValue = ret;
            window.close();
        }
        function getpri(ret, ret1) {
            window.returnValue = ret + "~" + ret1;
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table style="position: absolute; top: 4px; left: 4px">
        <tr>
            <td>
                <div id="statdiv" style="border-bottom: black 1px solid; border-left: black 1px solid;
                    height: 200px; overflow: auto; border-top: black 1px solid; border-right: black 1px solid"
                    runat="server">
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
