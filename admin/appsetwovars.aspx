<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="appsetwovars.aspx.vb"
    Inherits="lucy_r12.appsetwovars" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>appsetwovars</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table width="605">
        <tr id="trp" height="24" runat="server">
            <td class="thdrsing label" width="300">
                <asp:Label ID="lang126" runat="server">Labor Reporting Time Entry Settings</asp:Label>
            </td>
            <td width="5">
                &nbsp;
            </td>
            <td class="thdrsing label" width="300">
                <asp:Label ID="lang127" runat="server">PM Down Time Entry Settings</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="rblv" runat="server" AutoPostBack="True" CssClass="plainlabelblue">
                    <asp:ListItem Value="lvte">Time Entry Only</asp:ListItem>
                    <asp:ListItem Value="lvst" Selected="True">Start Time \ Stop Time Entry</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
            <td>
                <asp:RadioButtonList ID="rbdt" runat="server" AutoPostBack="True" CssClass="plainlabelblue">
                    <asp:ListItem Value="lvtdt">Use Total Down Time</asp:ListItem>
                    <asp:ListItem Value="lvdtt" Selected="True">Enter Down Time per Task</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="Tr1" height="24" runat="server">
            <td class="thdrsing label">
                <asp:Label ID="lang128" runat="server">Labor Reporting Work Order Actual Cost Settings</asp:Label>
            </td>
            <td width="5">
                &nbsp;
            </td>
            <td class="thdrsing label" width="300">
                <asp:Label ID="lang129" runat="server">PM Task Time Entry Settings</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="rblca" runat="server" AutoPostBack="True" CssClass="plainlabelblue">
                    <asp:ListItem Value="lcask" Selected="True">Use Skill Rate (Lead Craft Skill Rate)</asp:ListItem>
                    <asp:ListItem Value="lcaext">Labor Costs Imported (i.e. JDE, etc)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
            <td>
                <asp:RadioButtonList ID="rbttt" runat="server" AutoPostBack="True" CssClass="plainlabelblue">
                    <asp:ListItem Value="lvttt">Use Total Task Time</asp:ListItem>
                    <asp:ListItem Value="lvtpt" Selected="True">Enter Time per Task</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="Tr2" height="24" runat="server">
            <td class="thdrsing label">
                <asp:Label ID="lang130" runat="server">Labor Reporting Work Estimated Cost Settings</asp:Label>
            </td>
            <td width="5">
                &nbsp;
            </td>
            <td class="thdrsing label" width="300">
                <asp:Label ID="lang131" runat="server">TPM Down Time Entry Settings</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="rblce" runat="server" AutoPostBack="True" CssClass="plainlabelblue">
                    <asp:ListItem Value="lcelb">Use Labor Rate (Lead Craft Rate)</asp:ListItem>
                    <asp:ListItem Value="lcesk" Selected="True">Use Skill Rate (Lead Craft Skill Rate)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
            <td>
                <asp:RadioButtonList ID="rbdtt" runat="server" AutoPostBack="True" CssClass="plainlabelblue">
                    <asp:ListItem Value="lvtdtt">Use Total Down Time</asp:ListItem>
                    <asp:ListItem Value="lvdttt" Selected="True">Enter Down Time per Task</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="Tr3" height="24" runat="server">
            <td class="thdrsing label">
                <asp:Label ID="lang132" runat="server">Material Usage Settings</asp:Label>
            </td>
            <td width="5">
                &nbsp;
            </td>
            <td class="thdrsing label" width="300">
                <asp:Label ID="lang133" runat="server">TPM Task Time Entry Settings</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="rbptl" runat="server" CssClass="plainlabelblue" AutoPostBack="True">
                    <asp:ListItem Value="inv">Assigned From PM Inventory</asp:ListItem>
                    <asp:ListItem Value="use" Selected="True">User Entry Required</asp:ListItem>
                    <asp:ListItem Value="ext">Material Costs Imported (i.e. JDE, etc)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
            <td>
                <asp:RadioButtonList ID="rbtttt" runat="server" AutoPostBack="True" CssClass="plainlabelblue">
                    <asp:ListItem Value="lvtttt">Use Total Task Time</asp:ListItem>
                    <asp:ListItem Value="lvtptt" Selected="True">Enter Time per Task</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
