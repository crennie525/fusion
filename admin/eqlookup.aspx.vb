

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class eqlookup
    Inherits System.Web.UI.Page
	Protected WithEvents lang164 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim mat As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 30
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String = "eqnum"
    Dim list, typ As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblps As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllist As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            mat.Open()
            GetMat(PageNumber)
            mat.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                mat.Open()
                GetNext()
                mat.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                mat.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetMat(PageNumber)
                mat.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                mat.Open()
                GetPrev()
                mat.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                mat.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetMat(PageNumber)
                mat.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        mat.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        GetMat(PageNumber)
        mat.Dispose()
    End Sub
    Private Sub GetMat(ByVal PageNumber As Integer)
        typ = lbltyp.Value
        Dim f1, f2 As String
        Tables = "equipment"
        PK = "eqid"
        Fields = "eqid, eqnum, eqdesc"
        Dim srch As String
        srch = txtsrch.Text
        srch = mat.ModString3(srch)
        If Len(srch) > 0 Then
            If Filter = "" Then
                Filter = "eqnum like ''%" & srch & "%'' or eqdesc like ''%" & srch & "%''"
                FilterCnt = "eqnum like '%" & srch & "%' or eqdesc like '%" & srch & "%'"
            Else
                Filter = " and eqnum like ''%" & srch & "%'' or eqdesc like ''%" & srch & "%''"
                FilterCnt = " and eqnum like '%" & srch & "%' or eqdesc like '%" & srch & "%'"
            End If
        End If
        If FilterCnt <> "" Then
            sql = "select Count(*) from equipment where " & FilterCnt
        Else
            sql = "select Count(*) from equipment"
        End If

        Dim dc As Integer = mat.Scalar(sql)

        dgdepts.VirtualItemCount = dc
        If dc = 0 Then
            lblps.Visible = True
            lblps.Text = "No Records Found"
            dgdepts.Visible = True

            PageSize = "100"
            dr = mat.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgdepts.DataSource = dr
            Try
                dgdepts.DataBind()
            Catch ex As Exception
                dgdepts.CurrentPageIndex = 0
                dgdepts.DataBind()
            End Try
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
        Else
            dgdepts.Visible = True
            lblps.Visible = False

            PageSize = "100"
            dr = mat.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgdepts.DataSource = dr
            'Try
            dgdepts.DataBind()
            'Catch ex As Exception
            'dgdepts.CurrentPageIndex = 0
            'dgdepts.DataBind()
            'End Try
            dr.Close()
            lblps.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetMat(PageNumber)
        Catch ex As Exception
            mat.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr178" , "eqlookup.aspx.vb")
 
            mat.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetMat(PageNumber)
        Catch ex As Exception
            mat.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr179" , "eqlookup.aspx.vb")
 
            mat.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgdepts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdepts.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lb As HtmlAnchor
            lb = CType(e.Item.FindControl("lbv"), HtmlAnchor)
            Dim mid, mi, md As String
            mid = CType(e.Item.FindControl("Label1"), Label).Text
            mi = CType(e.Item.FindControl("lblv"), Label).Text
            mi = mat.ModString3(mi)
            md = CType(e.Item.FindControl("lbld"), Label).Text
            md = mat.ModString3(md)
            lb.Attributes.Add("onclick", "getmat('" & mi & "','" & md & "','" & mid & "')")
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("eqlookup.aspx", "dgdepts", "0")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(2).HeaderText = dlabs.GetDGPage("eqlookup.aspx", "dgdepts", "2")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang164.Text = axlabs.GetASPXPage("eqlookup.aspx", "lang164")
        Catch ex As Exception
        End Try

    End Sub

End Class
