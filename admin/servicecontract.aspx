<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="servicecontract.aspx.vb"
    Inherits="lucy_r12.servicecontract" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>servicecontract</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/servicecontractaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table width="500">
        <tr>
            <td colspan="3" class="label">
                <asp:Label ID="lang183" runat="server">Search Service Contracts</asp:Label><asp:TextBox
                    ID="txtsrch" runat="server"></asp:TextBox><asp:ImageButton ID="btnsrch1" runat="server"
                        Height="20px" Width="20px" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                    </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="500">
                    <tbody>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgeq" runat="server" CellPadding="0" GridLines="None" AllowPaging="True"
                                    AllowCustomPaging="True" AutoGenerateColumns="False" PageSize="20" CellSpacing="1"
                                    BackColor="transparent">
                                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                                    <HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="contract">
                                            <HeaderStyle Width="0px"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:ButtonColumn Text="Select" DataTextField="contract" HeaderText="Contract" CommandName="Select">
                                            <HeaderStyle Width="120px"></HeaderStyle>
                                        </asp:ButtonColumn>
                                        <asp:BoundColumn DataField="description" HeaderText="Description">
                                            <HeaderStyle Width="380px"></HeaderStyle>
                                        </asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Visible="False"></PagerStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblpm" runat="server" name="lblpm">
    <input type="hidden" id="lblds" runat="server" name="lblds"><input type="hidden"
        id="lblgl" runat="server" name="lblgl">
    <input type="hidden" id="lblch" runat="server" name="lblch"><input type="hidden"
        id="lbleq" runat="server" name="lbleq">
    <input type="hidden" id="lbltyp" runat="server" name="lbltyp"><input type="hidden"
        id="lblwo" runat="server" name="lblwo">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
