<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetWoCharge.aspx.vb"
    Inherits="lucy_r12.AppSetWoCharge" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetWoCharge</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .details
        {
            display: none;
            visibility: hidden;
            font-family: Verdana;
            background-color: white;
        }
    </style>
    <script language="javascript" type="text/javascript" src="../scripts/chargetab.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetWoChargeaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checktab();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div id="ltdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 5px; visibility: visible; border-left: 2px groove; width: 400px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="iflt" style="width: 500px; height: 340px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="ldiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 800px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="ifl" style="width: 800px; height: 340px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <table style="left: 5px; position: absolute; top: 10px" cellspacing="3" cellpadding="3">
        <tr height="20">
            <td id="lt" onclick="gettab('lt');" class="thdrhov plainlabel" width="100">
                <asp:Label ID="lang119" runat="server">Charge Types</asp:Label>
            </td>
            <td id="l" onclick="gettab('l');" class="thdr plainlabel" width="100">
                <asp:Label ID="lang120" runat="server">Charge Config</asp:Label>
            </td>
            <td id="e" onclick="gettab('e');" class="thdr plainlabel" width="100">
                <asp:Label ID="lang121" runat="server">Charge Entry</asp:Label>
            </td>
        </tr>
    </table>
    <input type="hidden" id="txttab" runat="server" name="appchk">
    <input type="hidden" id="lbltab" runat="server" name="appchk">
    <input type="hidden" id="lblcid" runat="server" name="appchk">
    <input type="hidden" id="lbltl" runat="server" name="appchk">
    <input type="hidden" id="appchk" runat="server" name="appchk">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
