<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChargeDialog.aspx.vb"
    Inherits="lucy_r12.ChargeDialog" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title runat="server" id="pgtitle">Charge Lookup</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/ChargeDialogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function getcharge(val) {
        //alert(val)
            var who = document.getElementById("lblwho").value;
            if (who == "wo") {
                document.getElementById("lblval").value = val;
                document.getElementById("lblsubmit").value = "upwo";
                document.getElementById("form1").submit();
            }
            else {
                handleret(val);
            }
        }
        function checkit() {
            window.setTimeout("setref();", 300000);
            var ret = document.getElementById("lblret").value;
            if (ret == "go") {
                val = document.getElementById("lblval").value;
                handleret(val);
            }
        }
        function setref() {
            //window.parent.handlesup("");
        }
        function handleret(val) {
        //alert(val)
            window.returnValue = val;
            window.close();
        }
		
    </script>
</head>
<body onload="checkit();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifcharge" src="" width="100%" height="100%" frameborder="no" runat="server">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    </form>
</body>
</html>
