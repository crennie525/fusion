

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetpmAppr
    Inherits System.Web.UI.Page
	Protected WithEvents lang96 As System.Web.UI.WebControls.Label

	Protected WithEvents lang95 As System.Web.UI.WebControls.Label

	Protected WithEvents lang94 As System.Web.UI.WebControls.Label

	Protected WithEvents lang93 As System.Web.UI.WebControls.Label

	Protected WithEvents lang92 As System.Web.UI.WebControls.Label

	Protected WithEvents lang91 As System.Web.UI.WebControls.Label

	Protected WithEvents lang90 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim appr As New Utilities
    Dim dr As SqlDataReader
    Dim sql, Login As String
    Protected WithEvents lblapprflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim apprcnt, pmappr, rev, ro As Integer
    Protected WithEvents lblans As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifgrps As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ddrevs As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdapprdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdapprby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcopy As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgdel As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcheck As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbleqcnt As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddag As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtnewitem As System.Web.UI.WebControls.TextBox
    Protected WithEvents ifphase As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifplan As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Iframe1 As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                imgadd.Attributes.Add("onclick", "")
                imgdel.Attributes.Add("src", "../images/appbuttons/minibuttons/deldis.gif")
                imgdel.Attributes.Add("onclick", "")
                imgcopy.Attributes.Add("onclick", "")
                imgcheck.Attributes.Add("onclick", "")
                imgcomp.Attributes.Add("onclick", "")
            End If
            appr.Open()
            rev = appr.Rev()
            lblrev.Value = rev
            CheckApprCnt()
            loadrevs()
            appr.Dispose()
        Else
            If Len(Request.Form("lblans")) > 0 Then
                Dim ans As String = Request.Form("lblans")
                If ans = "1a" Or ans = "3a" Then
                    appr.Open()
                    NewC()
                    loadrevs()
                    appr.Dispose()
                ElseIf ans = "1b" Or ans = "3b" Then
                    appr.Open()
                    NewS()
                    loadrevs()
                    appr.Dispose()
                ElseIf ans = "5" Then
                    lblans.Value = ""
                    appr.Open()
                    DelRev()
                    rev = appr.Rev()
                    lblrev.Value = rev
                    loadrevs()
                    appr.Dispose()
                ElseIf ans = "6" Then
                    lblans.Value = ""
                    appr.Open()
                    ApprRev()
                    loadrevs()
                    appr.Dispose()
                ElseIf ans = "7" Then
                    lblans.Value = ""
                    appr.Open()
                    ArchRev()
                    loadrevs()
                    appr.Dispose()
                End If
            End If
        End If
    End Sub
    Private Sub ArchRev()
        rev = lblrev.Value
        sql = "usp_ArchRev '" & rev & "'"
        appr.Update(sql)
    End Sub
    Private Sub ApprRev()
        rev = lblrev.Value
        Try
            Dim aby As String = HttpContext.Current.Session("username").ToString
            sql = "update pmApprRev set approveddate = getDate(), approvedby = '" & aby & "'"
            appr.Update(sql)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub DelRev()
        rev = lblrev.Value
        sql = "usp_delrev '" & rev & "'"
        appr.Update(sql)

    End Sub
    Private Sub ddrevs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddrevs.SelectedIndexChanged
        Dim ans, cnt As String
        If ddrevs.SelectedIndex <> 0 Then
            rev = ddrevs.SelectedValue.ToString
            lblrev.Value = rev
            ans = "0"
            lblans.Value = ans
            cnt = "0"
            lbleqcnt.Value = cnt
            appr.Open()
            loadrevs()
            appr.Dispose()
            ro = lblro.Value
            ifphase.Attributes.Add("src", "AppSetApprPhases.aspx?jump=no&ans=" & ans & "&cnt=" & cnt & "&rev=" & rev & "&ro=" & ro)
            ifplan.Attributes.Add("src", "AppSetApprPlan.aspx?jump=no&ans=" & ans & "&cnt=" & cnt & "&rev=" & rev & "&ro=" & ro)
            ifgrps.Attributes.Add("src", "AppSetpmApprGrps.aspx?rev=" & rev & "&ro=" & ro)
        Else
            rev = lblrev.Value
            ddrevs.SelectedValue = rev
        End If
        
    End Sub
    Private Sub loadrevs()
        Dim isc As String
        rev = lblrev.Value
        sql = "select rev, approveddate from pmApprRev"
        dr = appr.GetRdrData(sql)
        ddrevs.DataSource = dr
        ddrevs.DataValueField = "rev"
        ddrevs.DataTextField = "rev"
        Try
            ddrevs.DataBind()
        Catch ex As Exception

        End Try

        ddrevs.Items.Insert(0, "Select Revision")
        'ddrevs.Attributes.Add("onchange", "handlerev();")
        dr.Close()
        Try
            ddrevs.SelectedValue = rev
        Catch ex As Exception

        End Try

        sql = "select approvedby, Convert(char(10),approveddate,101) as 'apprdate', iscurrent from " _
        + "pmApprRev where rev = '" & rev & "'"
        dr = appr.GetRdrData(sql)
        While dr.Read
            tdapprby.InnerHtml = dr.Item("approvedby").ToString
            tdapprdate.InnerHtml = dr.Item("apprdate").ToString
            isc = dr.Item("iscurrent").ToString
        End While
        dr.Close()
        If isc = "1" Then
            tdcurr.InnerHtml = "Yes"
        Else
            tdcurr.InnerHtml = "No"
        End If
        If lblapprflg.Value <> "1" Then
            Dim ans, cnt As String
            rev = lblrev.Value
            ans = "0"
            cnt = lbleqcnt.Value
            ro = lblro.Value
            ifphase.Attributes.Add("src", "AppSetApprPhases.aspx?jump=no&ans=" & ans & "&cnt=" & cnt & "&rev=" & rev & "&ro=" & ro)
            ifplan.Attributes.Add("src", "AppSetApprPlan.aspx?jump=no&ans=" & ans & "&cnt=" & cnt & "&rev=" & rev & "&ro=" & ro)
            ifgrps.Attributes.Add("src", "AppSetpmApprGrps.aspx?rev=" & rev & "&ro=" & ro)
        End If
    End Sub
    Private Sub NewC()
        rev = lblrev.Value
        Dim newrev As Integer
        Dim ans As String = "0"
        Dim cnt As String = "0"
        sql = "usp_AddApprRevCopy '" & rev & "'"
        newrev = appr.Scalar(sql)
        lblrev.Value = newrev
        lblans.Value = "0"
        lbleqcnt.Value = "0"
        ro = lblro.Value
        ifphase.Attributes.Add("src", "AppSetApprPhases.aspx?jump=no&ans=" & ans & "&cnt=" & cnt & "&rev=" & newrev & "&ro=" & ro)
        ifplan.Attributes.Add("src", "AppSetApprPlan.aspx?jump=no&ans=" & ans & "&cnt=" & cnt & "&rev=" & newrev & "&ro=" & ro)
        ifgrps.Attributes.Add("src", "AppSetpmApprGrps.aspx?rev=" & newrev & "&ro=" & ro)
    End Sub
    Private Sub NewS()
        Dim newrev As Integer
        Dim ans As String = "0"
        Dim cnt As String = "0"
        sql = "usp_AddApprRevNew"
        newrev = appr.Scalar(sql)
        lblrev.Value = newrev
        lblans.Value = "0"
        lbleqcnt.Value = "0"
        ro = lblro.Value
        ifphase.Attributes.Add("src", "AppSetApprPhases.aspx?jump=no&ans=" & ans & "&cnt=" & cnt & "&rev=" & newrev & "&ro=" & ro)
        ifplan.Attributes.Add("src", "AppSetApprPlan.aspx?jump=no&ans=" & ans & "&cnt=" & cnt & "&rev=" & newrev & "&ro=" & ro)
        ifgrps.Attributes.Add("src", "AppSetpmApprGrps.aspx?rev=" & newrev & "&ro=" & ro)
    End Sub
    Private Sub CheckApprCnt()
        rev = lblrev.Value
        sql = "select count(*) from equipment where apprstg2 = 1 and apprstg2prev <> 1"
        apprcnt = appr.Scalar(sql)
        If apprcnt > 0 Then
            lblapprflg.Value = "1"
        End If

        rev = lblrev.Value
        sql = "select count(*) from pmApprovalEq" ' where rev = '" & rev & "'"
        pmappr = appr.Scalar(sql)
        If pmappr > 0 Then
            lbleqcnt.Value = "1"
        Else
            lbleqcnt.Value = "0"
        End If

    End Sub


	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang90.Text = axlabs.GetASPXPage("AppSetpmAppr.aspx","lang90")
		Catch ex As Exception
		End Try
		Try
			lang91.Text = axlabs.GetASPXPage("AppSetpmAppr.aspx","lang91")
		Catch ex As Exception
		End Try
		Try
			lang92.Text = axlabs.GetASPXPage("AppSetpmAppr.aspx","lang92")
		Catch ex As Exception
		End Try
		Try
			lang93.Text = axlabs.GetASPXPage("AppSetpmAppr.aspx","lang93")
		Catch ex As Exception
		End Try
		Try
			lang94.Text = axlabs.GetASPXPage("AppSetpmAppr.aspx","lang94")
		Catch ex As Exception
		End Try
		Try
			lang95.Text = axlabs.GetASPXPage("AppSetpmAppr.aspx","lang95")
		Catch ex As Exception
		End Try
		Try
			lang96.Text = axlabs.GetASPXPage("AppSetpmAppr.aspx","lang96")
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			imgadd.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("AppSetpmAppr.aspx","imgadd") & "', ABOVE, LEFT)")
			imgadd.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgcheck.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("AppSetpmAppr.aspx","imgcheck") & "', ABOVE, LEFT)")
			imgcheck.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgcomp.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("AppSetpmAppr.aspx","imgcomp") & "', ABOVE, LEFT)")
			imgcomp.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgcopy.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("AppSetpmAppr.aspx","imgcopy") & "', ABOVE, LEFT)")
			imgcopy.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			imgdel.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("AppSetpmAppr.aspx","imgdel") & "', ABOVE, LEFT)")
			imgdel.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
