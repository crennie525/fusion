<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="contractmain.aspx.vb"
    Inherits="lucy_r12.contractmain" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>contractmain</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/dragitem.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" type="text/javascript" src="../scripts1/contractmainaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 1px; position: absolute; top: 1px" cellspacing="0" cellpadding="2"
        width="722">
        <tr>
            <td class="thdrsing label" width="722" bgcolor="#1d11ef" colspan="3">
                <asp:Label ID="lang154" runat="server">Add/Edit Service Contracts</asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="3">
                <table cellpadding="0" width="680" border="0">
                    <tr>
                        <td width="120">
                        </td>
                        <td width="150">
                        </td>
                        <td width="22">
                        </td>
                        <td width="22">
                        </td>
                        <td width="22">
                        </td>
                        <td width="22">
                        </td>
                        <td width="8">
                        </td>
                        <td width="110">
                        </td>
                        <td width="212">
                        </td>
                    </tr>
                    <tr id="Tr5" runat="server">
                        <td class="label">
                            <asp:Label ID="lang155" runat="server">Contract</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtcontract" runat="server" Width="140px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img id="imgadd" onclick="addsc();" src="../images/appbuttons/minibuttons/addnew.gif"
                                runat="server">
                        </td>
                        <td>
                            <img id="imgsrch" onclick="getcomp();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                runat="server">
                        </td>
                        <td>
                            <img id="Img1" onclick="savesc();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                runat="server">
                        </td>
                        <td>
                            <img onclick="jumpto('');" src="../images/appbuttons/minibuttons/srchsm.gif">
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtcdesc" runat="server" Width="330px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang156" runat="server">Vendor</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtvend" runat="server" Width="140px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img id="Img2" onclick="getcomp1();" src="../images/appbuttons/minibuttons/magnifier.gif"
                                runat="server">
                        </td>
                        <td colspan="6">
                            <asp:TextBox ID="txtvdesc" runat="server" Width="402px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang157" runat="server">Equipment</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txteq" runat="server" Width="140px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td>
                            <img onclick="GetLocEq();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td colspan="6">
                            <asp:TextBox ID="txteqdesc" runat="server" Width="402px" ReadOnly="True" Font-Names="Arial"
                                Font-Size="X-Small" CssClass="plainlabel"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="lang158" runat="server">Vendor Ref#</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtvcont" runat="server" Width="140px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td class="label" colspan="5">
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="thdrsing label" width="722" bgcolor="#1d11ef" colspan="3">
                <asp:Label ID="lang159" runat="server">Contract Details</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:TextBox ID="txtdetails" runat="server" Width="670px" CssClass="plainlabel" Height="220px"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" class="plainlabelred" colspan="3">
                <asp:Label ID="lang160" runat="server">Add Service Contracts if you will be using Fusion for Work Order Management.</asp:Label>
            </td>
        </tr>
    </table>
    <div onmouseup="mouseUp();restore('ifsc', 'trsc');" class="details" onmousedown="makeDraggable(this, 'ifsc', 'trsc');"
        id="scdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 550px; border-bottom: black 1px solid;
        height: 460px">
        <table cellspacing="0" cellpadding="0" width="530" class="tbg">
            <tr bgcolor="blue" height="20">
                <td class="whtlbl">
                    <asp:Label ID="lang161" runat="server">Service Contract Lookup</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closesc();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
                        width="18"><br>
                </td>
            </tr>
            <tr class="details" id="trsc" height="460">
                <td class="bluelabelb" valign="middle" align="center" colspan="2">
                    <asp:Label ID="lang162" runat="server">Moving Window...</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="ifsc" style="width: 550px; height: 460px" src="" frameborder="no" runat="server">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    <div class="details" id="subdiv" style="border-right: black 1px solid; border-top: black 1px solid;
        z-index: 999; border-left: black 1px solid; width: 400px; border-bottom: black 1px solid;
        height: 320px">
        <table cellspacing="0" cellpadding="0" width="400" bgcolor="white">
            <tr bgcolor="blue" height="20">
                <td class="whtlbl">
                    <asp:Label ID="lang163" runat="server">Company Lookup</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closecraft();" height="18" alt="" src="../images/appbuttons/minibuttons/close.gif"
                        width="18"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="ifcraft" style="width: 400px; height: 300px; background-color: transparent"
                        src="" frameborder="no" runat="server" allowtransparency></iframe>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lbldragid" type="hidden" name="lbldragid" runat="server"><input id="lblrowid"
        type="hidden" name="lblrowid" runat="server">
    <input id="lblsc" type="hidden" runat="server"><input id="lblpgstat" type="hidden"
        name="lblpgstat" runat="server">
    <input id="lbloldcontract" type="hidden" name="lbloldcontract" runat="server">
    <input type="hidden" id="lbleqid" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
