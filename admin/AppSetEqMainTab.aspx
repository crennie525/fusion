<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetEqMainTab.aspx.vb"
    Inherits="lucy_r12.AppSetEqMainTab" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetEqMainTab</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetEqMainTabaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div id="eqsdiv" style="left: 5px; width: 350px; position: absolute; top: 0px; height: 430px;">
        <iframe id="ifeq" style="width: 350px; height: 430px" name="ifeq" src="" frameborder="no"
            runat="server"></iframe>
    </div>
    <div id="eqacdiv" style="z-index: 105; left: 345px; border-left: 2px groove; width: 350px;
        position: absolute; top: 0px; height: 430px">
        <iframe id="ifac" style="width: 350px; height: 430px" name="ifeq" src="" frameborder="no"
            runat="server"></iframe>
    </div>
    <input type="hidden" id="lblcid" runat="server">
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
