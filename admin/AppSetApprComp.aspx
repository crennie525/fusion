<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetApprComp.aspx.vb"
    Inherits="lucy_r12.AppSetApprComp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetApprComp</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetApprCompaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onunload="getans();">
    <form id="form1" method="post" runat="server">
    <table width="430" align="center">
        <tr>
            <td class="redlabel14" align="center">
                <br>
                <asp:Label ID="lang12" runat="server">You have chosen to make the following changes;</asp:Label><br>
                <br>
            </td>
        </tr>
        <tr>
            <td class="label" align="center">
                <asp:Label ID="LabelMR" runat="server" CssClass="label">Make Revision</asp:Label>&nbsp;
                <asp:Label ID="lblnew" runat="server" CssClass="plainlabelred"></asp:Label>&nbsp;
                <asp:Label ID="LabelMR2" runat="server" CssClass="label">the New Current Revision</asp:Label><br>
            </td>
        </tr>
        <tr>
            <td class="label" align="center">
                <asp:Label ID="lang13" runat="server">Archive Current Revision</asp:Label><asp:Label
                    ID="lblold" runat="server" CssClass="plainlabelred"></asp:Label><br>
                <br>
                <hr>
            </td>
        </tr>
        <tr>
            <td class="plainlabel">
                <asp:Label ID="lang14" runat="server">Any PM Procedures currently approved for use in the PM Library using the Current Revision will remain in the PM Library.</asp:Label><br>
                <br>
                <asp:Label ID="lang15" runat="server">PM Approval records for the above procedures will be archived and available for viewing in the PM Approval Module in Archive Mode.</asp:Label><br>
                <br>
                <asp:Label ID="lang16" runat="server">PM Approval records for the above procedures can be recreated for the New Current Revision with the option of removing them from the PM Library if desired.</asp:Label><br>
                <br>
                <hr>
            </td>
        </tr>
        <tr>
            <td class="label" align="center">
                <asp:Label ID="lang17" runat="server">Do You Wish to Continue?</asp:Label><br>
                <br>
            </td>
        </tr>
        <tr>
            <td align="center">
                <input id="byes" runat="server" class="lilmenu plainlabel" onmouseover="this.className='lilmenuhov plainlabel'"
                    style="width: 55px; height: 24px" onclick="appr('yes');" onmouseout="this.className='lilmenu plainlabel'"
                    type="button" value="Yes">&nbsp;&nbsp;
                <input id="bno" runat="server" class="lilmenu plainlabel" onmouseover="this.className='lilmenuhov plainlabel'"
                    style="width: 55px; height: 24px" onclick="appr('no');" onmouseout="this.className='lilmenu plainlabel'"
                    type="button" value="No">
            </td>
        </tr>
    </table>
    <input id="lblchk" type="hidden" name="lblchk" runat="server"><input id="lblrevchk"
        type="hidden" name="lblrevchk" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
