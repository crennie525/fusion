<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetLoc.aspx.vb" Inherits="lucy_r12.AppSetLoc" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetLoc</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetLocaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table class="tbg" id="ttdiv" cellspacing="0" cellpadding="0" runat="server" width="300">
        <tr class="tbg">
            <td width="80">
            </td>
            <td width="70">
            </td>
            <td width="50">
            </td>
            <td width="50">
            </td>
        </tr>
        <tr class="tbg">
            <td style="height: 16px" colspan="4">
                <asp:Label ID="lbltasktype" runat="server" Font-Size="X-Small" Font-Names="Arial"
                    Font-Bold="True" Width="256px" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr class="tbg">
            <td valign="top" align="center" colspan="4">
                <asp:DataGrid ID="dgtt" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                    AllowPaging="True" GridLines="None" CellPadding="0" ShowFooter="True" CellSpacing="1"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton26" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit This Task Type" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton27" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton28" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblttids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.locid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.locid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.typid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbltypid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.typid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Location">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewtt" runat="server" Width="124px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="txtloc" runat="server" Width="124px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Description">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtdesc" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="txtdesce" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Type">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.type") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddtyp" runat="server" DataSource="<%# PopulateTypes %>" DataTextField="loctype"
                                    DataValueField="typid">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddtype" runat="server" DataSource="<%# PopulateTypes %>" DataTextField="loctype"
                                    DataValueField="typid" SelectedIndex='<%# GetSelIndex(Container.DataItem("locindex")) %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="Imagebutton17" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid><br>
            </td>
        </tr>
        <tr class="tbg">
            <td colspan="2">
                <asp:Label ID="lblpgtasktype" runat="server" Font-Size="X-Small" Font-Names="Arial"
                    Font-Bold="True" ForeColor="Blue"></asp:Label>
            </td>
            <td align="right">
                <asp:ImageButton ID="ttPrev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif">
                </asp:ImageButton>
            </td>
            <td align="right">
                <asp:ImageButton ID="ttNext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif">
                </asp:ImageButton>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblcid" runat="server" name="lblcid"><input type="hidden"
        id="lblsid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
