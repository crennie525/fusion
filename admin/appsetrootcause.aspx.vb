﻿Imports System.Data.SqlClient
Public Class appsetrootcause
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Private Pictures(15) As String
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim dept As String
    Dim Sort As String = ""
    Dim cid, cnm, sid, did, sql, ro, rqstr As String
    Dim dr As SqlDataReader
    Dim appset As New Utilities
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetDGLangs()
        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtt.Columns(0).Visible = False
                dgtt.Columns(2).Visible = False
            End If
            
            appset.Open()
            PopTasks(PageNumber)
            loadrq()
            loadwt()
            appset.Dispose()
            
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer, Optional ByVal Sort As String = "rootcause")

        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "rootcause like ''%" & srch & "%''"
            FilterCnt = "rootcause like '%" & srch & "%'"
        Else
            Filter = "rootcause like ''%%''"
            FilterCnt = "rootcause like '%%'"
        End If
        sql = "select count(*) " _
        + "from rootcause where " & FilterCnt 'compid = '" & cid & "'"
        dgtt.VirtualItemCount = appset.Scalar(sql)
        If dgtt.VirtualItemCount = 0 Then
            lbltasktype.Text = "No Work Order Type Records Found"
            dgtt.Visible = True
            Tables = "rootcause"
            PK = "rootid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        Else
            Tables = "rootcause"
            PK = "rootid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            lbltasktype.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr135", "AppSetWoType.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr136", "AppSetWoType.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgtt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.CancelCommand
        dgtt.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.DeleteCommand
        appset.Open()
        Dim id, desc As String

        id = CType(e.Item.Cells(1).Controls(1), Label).Text
        Try
            desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        Catch ex As Exception
            desc = CType(e.Item.Cells(2).Controls(1), Label).Text
        End Try

        sql = "select count(*) from worootcause where rootcause = '" & desc & "'"
        Dim tcnt As Integer = appset.Scalar(sql)
        If tcnt = 0 Then
            sql = "delete from rootcause where rootid = '" & id & "'"
            appset.Update(sql)
            dgtt.EditItemIndex = -1
            sql = "select Count(*) from rootcause"
            PageNumber = appset.PageCount(sql, PageSize)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            If dgtt.CurrentPageIndex > PageNumber Then
                dgtt.CurrentPageIndex = PageNumber - 1
            End If
            If dgtt.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgtt.CurrentPageIndex + 1
            End If
            PopTasks(PageNumber)
        Else
            Dim strMessage As String = "This Root Cause is used in an existing Work Order and Cannot Be Deleted."

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub

    Private Sub dgtt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.EditCommand
        lblold.Value = CType(e.Item.Cells(2).Controls(1), Label).Text
        PageNumber = txtpg.Value
        dgtt.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.ItemCommand
        Dim lname, wosd As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewtt"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)

           
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String = tmod.getmsg("cdstr137", "AppSetWoType.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    
                    Else
                        appset.Open()

                        Dim tt As String
                        Dim strchk As Integer
                        Dim faill As String = lname.ToLower
                        sql = "select count(*) from rootcause where lower(rootcause) = '" & faill & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "insert into rootcause (rootcause) values ('" & lname & "')"
                            appset.Update(sql)
                            Dim ttcnt As Integer = dgtt.VirtualItemCount + 1

                            lnamei.Text = ""
                            sql = "select Count(*) from rootcause"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopTasks(PageNumber, "rootid")
                        Else
                            Dim strMessage As String = tmod.getmsg("cdstr139", "AppSetWoType.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If
                        appset.Dispose()
                    End If

                End If
            End If
        End If
    End Sub

    Private Sub dgtt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtt.ItemDataBound
        Dim ibfm As ImageButton
        Dim ibfm1 As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'Dim stat As String = DataBinder.Eval(e.Item.DataItem, "wotype").ToString
            'If stat = "PM" Or stat = "SWR" Or stat = "TPM" Then
            'ibfm = CType(e.Item.FindControl("imgedit"), ImageButton)
            'ibfm.Attributes.Add("class", "details")
            'ibfm1 = CType(e.Item.FindControl("imgdel"), ImageButton)
            'ibfm1.Attributes.Add("class", "details")

            'End If
        End If
    End Sub

    Private Sub dgtt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.UpdateCommand
        appset.Open()
        Dim id, desc, act, old, wosd As String
        id = CType(e.Item.FindControl("lblttide"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        If Len(desc) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr141", "AppSetWoType.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        

        Dim faill As String = desc.ToLower
        sql = "select count(*) from rootcause where lower(rootcause) = '" & faill & "'"
        Dim strchk As Integer
        old = lblold.Value
        If old <> desc Then
            strchk = appset.Scalar(sql)
        Else
            strchk = 0
        End If
        If strchk = 0 Then
            sql = "update rootcause set rootcause = " _
            + "'" & desc & "' where rootid = '" & id & "'; update worootcause set rootcause = '" & desc & "' " _
            + "where rootcause = '" & desc & "'"
            appset.Update(sql)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            PopTasks(PageNumber)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr143", "AppSetWoType.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub
    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtt.Columns(0).HeaderText = dlabs.GetDGPage("AppSetWoType.aspx", "dgtt", "0")
        Catch ex As Exception
        End Try
        
        Try
            dgtt.Columns(3).HeaderText = dlabs.GetDGPage("AppSetWoType.aspx", "dgtt", "5")
        Catch ex As Exception
        End Try

    End Sub
    Private Sub loadwt()
        If rqstr <> "" Then
            Dim reqstrar() As String = rqstr.Split(",")
            Dim nouse As String
            For i = 0 To reqstrar.Length - 1
                If nouse = "" Then
                    nouse = "'" & reqstrar(i) & "'"
                Else
                    nouse += ",'" & reqstrar(i) & "'"
                End If
            Next
            sql = "select wotype from wotype where wotype not in (" & nouse & ")"
        Else
            sql = "select wotype from wotype"
        End If
        dr = appset.GetRdrData(sql)
        lbwt.DataSource = dr
        lbwt.DataValueField = "wotype"
        lbwt.DataTextField = "wotype"
        lbwt.DataBind()

    End Sub
    Private Sub loadrq()
        sql = "select [default] from wovars where [column] = 'root'"
        Try
            rqstr = appset.strScalar(sql)
        Catch ex As Exception
            rqstr = ""
        End Try
        lblrqstr.Value = rqstr
        lbrq.Items.Clear()
        If rqstr <> "" Then
            Dim reqstrar() As String = rqstr.Split(",")
            Dim i As Integer
            For i = 0 To reqstrar.Length - 1
                lbrq.Items.Add(reqstrar(i))
            Next
        End If
        
    End Sub
    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        'to
        Dim s, ns As String
        For Each Item In lbwt.Items
            If Item.Selected Then
                s = Item.Value.ToString
                If ns = "" Then
                    ns = s
                Else
                    ns += "," & s
                End If
            End If
        Next
        rqstr = lblrqstr.Value
        If rqstr = "" Then
            rqstr = ns
        Else
            rqstr += "," & ns
        End If

        appset.Open()
        sql = "update wovars set [default] = '" & rqstr & "' where [column] = 'root'"
        appset.Update(sql)
        loadrq()
        loadwt()
        appset.Dispose()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        'from
        rqstr = lblrqstr.Value
        Dim i As Integer
        Dim s, ns As String
        For Each Item In lbrq.Items
            If Item.Selected Then
                s = Item.Value.ToString
                Dim rqar() As String = rqstr.Split(",")
                For i = 0 To rqar.Length - 1
                    If s <> rqar(i) Then
                        If ns = "" Then
                            ns = s
                        Else
                            ns += "," & s
                        End If
                    End If
                Next
                rqstr = ns
            End If
        Next

        appset.Open()
        sql = "update wovars set [default] = '" & rqstr & "' where [column] = 'root'"
        appset.Update(sql)
        loadrq()
        loadwt()
        appset.Dispose()
    End Sub
End Class