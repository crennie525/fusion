

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class AppSetDeptTrans
    Inherits System.Web.UI.Page
	Protected WithEvents lang51 As System.Web.UI.WebControls.Label

	Protected WithEvents lang50 As System.Web.UI.WebControls.Label

	Protected WithEvents lang49 As System.Web.UI.WebControls.Label

	Protected WithEvents lang48 As System.Web.UI.WebControls.Label

	Protected WithEvents lang47 As System.Web.UI.WebControls.Label

	Protected WithEvents lang46 As System.Web.UI.WebControls.Label

	Protected WithEvents lang45 As System.Web.UI.WebControls.Label

	Protected WithEvents lang44 As System.Web.UI.WebControls.Label

	Protected WithEvents lang43 As System.Web.UI.WebControls.Label

	Protected WithEvents lang42 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim dts As New Utilities
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rptrdept As System.Web.UI.WebControls.Repeater
    Protected WithEvents rptrdept1 As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblcbonoff As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btntrans As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents lbleqcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dddepts1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells1 As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            btntrans.Attributes.Add("class", "details")
            Dim sid As String
            sid = HttpContext.Current.Session("dfltps").ToString()
            lblsid.Value = sid
            dts.Open()
            PopDepts(sid)
            dts.Dispose()
        Else
            If Request.Form("lblsubmit") = "trans" Then
                lblsubmit.Value = ""
                dts.Open()
                TransDept()
                dts.Dispose()
            End If
        End If
    End Sub
    Private Sub TransDept()
        Dim did, clid, did1, clid1, eqid, sid, chk, oclid, oclid1 As String
        did = lbldid.Value
        clid = lblclid.Value
        oclid = clid
        did1 = lbldid1.Value
        clid1 = lblclid1.Value
        oclid1 = clid1
        sid = lblsid.Value
        If clid = "-1" Then
            clid = 0
        End If
        If clid1 = "-1" Then
            clid1 = 0
        End If
        Dim cb As CheckBox
        Dim ei As String
        chk = CheckMatch(did, did1, clid, clid1)
        If chk = "ok" Then
            If did <> "" And did1 <> "" Then
                If clid <> "" And clid <> "" Then
                    chk = "ok"
                Else
                    chk = "no"
                End If
            End If
        End If
        If chk = "ok" Then
            For Each i As RepeaterItem In rptrdept1.Items
                If i.ItemType = ListItemType.Item Or i.ItemType = ListItemType.AlternatingItem Then
                    If i.ItemType = ListItemType.Item Then
                        cb = CType(i.FindControl("cb1"), CheckBox)
                        ei = CType(i.FindControl("lbleqiditem"), Label).Text
                    ElseIf i.ItemType = ListItemType.AlternatingItem Then
                        cb = CType(i.FindControl("cb2"), CheckBox)
                        ei = CType(i.FindControl("lbleqidalt"), Label).Text
                    End If
                    If cb.Checked = True Then
                        sql = "update equipment set dept_id = '" & did & "', cellid = '" & clid & "' " _
                        + "where dept_id = '" & did1 & "' and (cellid = '" & clid1 & "' or cellid is null) and eqid = '" & ei & "' " _
                        + "and siteid = '" & sid & "'; " _
                        + "update pmtasks set deptid = '" & did & "', cellid = '" & clid & "' where eqid = '" & ei & "' " _
                        + "and siteid = '" & sid & "'; " _
                        + "update pmtaskstpm set deptid = '" & did & "', cellid = '" & clid & "' where eqid = '" & ei & "' " _
                        + "and siteid = '" & sid & "'"
                        dts.Update(sql)
                    End If

                End If
            Next
            PopEq(did, oclid)
            PopEq1(did1, oclid1)
        End If
       
    End Sub
    Private Sub PopDepts(ByVal sid As String)
        Dim filt, dt, val As String
        filt = " where siteid = '" & sid & "'"
        sql = "select count(*) from Dept where siteid = '" & sid & "'"
        Dim dcnt As Integer = dts.Scalar(sql)
        If dcnt > 0 Then
            dt = "Dept"
            val = "dept_id , dept_line "
            dr = dts.GetList(dt, val, filt)
            dddepts.DataSource = dr
            dddepts.DataTextField = "dept_line"
            dddepts.DataValueField = "dept_id"
            Try
                dddepts.DataBind()
            Catch ex As Exception

            End Try

            dr.Close()
            dr = dts.GetList(dt, val, filt)
            dddepts1.DataSource = dr
            dddepts1.DataTextField = "dept_line"
            dddepts1.DataValueField = "dept_id"
            Try
                dddepts1.DataBind()
            Catch ex As Exception

            End Try

            dr.Close()

            dddepts.Enabled = True
            dddepts1.Enabled = True
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr32" , "AppSetDeptTrans.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            dddepts.Enabled = False
            dddepts1.Enabled = False
        End If
        dddepts.Items.Insert(0, "Select Department")
        dddepts1.Items.Insert(0, "Select Department")
        ddcells.Enabled = False
        ddcells1.Enabled = False
    End Sub

    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        Dim did, clid, chk, did1, clid1 As String
        did = dddepts.SelectedValue
        Dim dcnt As Integer
        If dddepts.SelectedIndex <> 0 And dddepts.SelectedIndex <> -1 Then
            lbldid.Value = did
            dts.Open()
            sql = "select count(*) from cells where dept_id = '" & did & "' and cellid <> 0"
            dcnt = dts.Scalar(sql)
            If dcnt <> 0 Then
                PopCells(did)
                ddcells.Enabled = True
                rptrdept.DataSource = Nothing
                rptrdept.DataBind()
            Else
                ddcells.Enabled = False
                lblclid.Value = "-1"
                clid = "-1"
                did1 = lbldid1.Value
                clid1 = lblclid1.Value
                chk = CheckMatch(did, did1, clid, clid1)
                If chk = "ok" Then
                    PopEq(did, clid)
                Else
                    rptrdept.DataSource = Nothing
                    rptrdept.DataBind()
                    Dim strMessage As String = chk
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If

            End If
            dts.Dispose()
        Else
            rptrdept.DataSource = Nothing
            rptrdept.DataBind()
        End If

    End Sub
    Private Sub PopCells(ByVal dept As String)
        Dim filt, dt, val As String
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = dts.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        Try
            ddcells.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")

    End Sub

    Private Sub dddepts1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts1.SelectedIndexChanged
        Dim did, clid, chk, did1, clid1 As String
        lbleqcnt.Value = "0"
        did = dddepts1.SelectedValue
        Dim dcnt As Integer
        If dddepts1.SelectedIndex <> 0 And dddepts1.SelectedIndex <> -1 Then
            lbldid1.Value = did
            dts.Open()
            sql = "select count(*) from cells where dept_id = '" & did & "' and cellid <> 0"
            dcnt = dts.Scalar(sql)
            If dcnt <> 0 Then
                PopCells1(did)
                ddcells1.Enabled = True
                rptrdept1.DataSource = Nothing
                rptrdept1.DataBind()
            Else
                ddcells1.Enabled = False
                lblclid1.Value = "-1"
                clid = "-1"
                did1 = lbldid.Value
                clid1 = lblclid.Value
                chk = CheckMatch(did, did1, clid, clid1)
                If chk = "ok" Then
                    PopEq1(did, clid)
                Else
                    rptrdept1.DataSource = Nothing
                    rptrdept1.DataBind()
                    btntrans.Attributes.Add("class", "details")
                    Dim strMessage As String = chk
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If


            End If
            dts.Dispose()
        Else
            rptrdept1.DataSource = Nothing
            rptrdept1.DataBind()
        End If

    End Sub
    Private Sub PopCells1(ByVal dept As String)

        Dim filt, dt, val As String
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = dts.GetList(dt, val, filt)
        ddcells1.DataSource = dr
        ddcells1.DataTextField = "cell_name"
        ddcells1.DataValueField = "cellid"
        Try
            ddcells1.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddcells1.Items.Insert(0, "Select Station/Cell")

    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        Dim clid, did, chk, clid1, did1 As String
        If ddcells.SelectedIndex <> 0 And ddcells.SelectedIndex <> -1 Then
            clid = ddcells.SelectedValue
            lblclid.Value = clid
            did = lbldid.Value
            did1 = lbldid1.Value
            clid1 = lblclid1.Value
            chk = CheckMatch(did, did1, clid, clid1)
            If chk = "ok" Then
                dts.Open()
                PopEq(did, clid)
                dts.Dispose()
            Else
                rptrdept.DataSource = Nothing
                rptrdept.DataBind()
                Dim strMessage As String = chk
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            rptrdept.DataSource = Nothing
            rptrdept.DataBind()
        End If
    End Sub
    Private Sub PopEq(ByVal did As String, ByVal clid As String)
        If clid <> "" And clid <> "-1" Then
            sql = "select eqid, eqnum, eqdesc from equipment where dept_id = '" & did & "' and cellid = '" & clid & "'"
        Else
            sql = "select eqid, eqnum, eqdesc from equipment where dept_id = '" & did & "'"
        End If
        Dim did1, clid1, cnt As String
        did1 = lbldid1.Value
        clid1 = lblclid1.Value
        cnt = lbleqcnt.Value
        If cnt = "" Then
            cnt = "0"
        End If
        If did1 = "" Then
            btntrans.Attributes.Add("class", "details")
        ElseIf clid1 <> "" And cnt <> "0" Then
            btntrans.Attributes.Add("class", "view")
        Else
            btntrans.Attributes.Add("class", "details")
        End If

        dr = dts.GetRdrData(sql)
        rptrdept.DataSource = dr
        rptrdept.DataBind()
        dr.Close()
    End Sub

    Private Sub ddcells1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells1.SelectedIndexChanged
        lbleqcnt.Value = "0"
        Dim clid, did, chk, clid1, did1 As String
        If ddcells1.SelectedIndex <> 0 And ddcells1.SelectedIndex <> -1 Then
            clid1 = ddcells1.SelectedValue
            lblclid1.Value = clid1
            did1 = lbldid1.Value
            did = lbldid.Value
            clid = lblclid.Value
            chk = CheckMatch(did, did1, clid, clid1)
            If chk = "ok" Then
                dts.Open()
                PopEq1(did1, clid1)
                dts.Dispose()
            Else
                rptrdept1.DataSource = Nothing
                rptrdept1.DataBind()
                btntrans.Attributes.Add("class", "details")
                Dim strMessage As String = chk
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            rptrdept1.DataSource = Nothing
            rptrdept1.DataBind()
            btntrans.Attributes.Add("class", "details")
        End If
    End Sub
    Private Sub PopEq1(ByVal did As String, ByVal clid As String)
        Dim cnt As Integer
        If clid <> "" And clid <> "-1" Then
            sql = "select count(*) from equipment where dept_id = '" & did & "' and cellid = '" & clid & "'"
            cnt = dts.Scalar(sql)
            sql = "select eqid, eqnum, eqdesc from equipment where dept_id = '" & did & "' and cellid = '" & clid & "'"
        Else
            sql = "select count(*) from equipment where dept_id = '" & did & "'"
            cnt = dts.Scalar(sql)
            sql = "select eqid, eqnum, eqdesc from equipment where dept_id = '" & did & "'"
        End If
        Dim did1, clid1 As String
        did1 = lbldid.Value
        clid1 = lblclid.Value
        lbleqcnt.Value = cnt
        If cnt > 0 Then
            If did1 = "" Then
                btntrans.Attributes.Add("class", "details")
            ElseIf clid1 <> "" Then
                btntrans.Attributes.Add("class", "view")
            Else
                btntrans.Attributes.Add("class", "details")
            End If
        Else
            btntrans.Attributes.Add("class", "details")
        End If
        dr = dts.GetRdrData(sql)
        rptrdept1.DataSource = dr
        rptrdept1.DataBind()
        dr.Close()
    End Sub
    Private Function CheckMatch(ByVal did As String, ByVal did1 As String, ByVal clid As String, ByVal clid1 As String) As String
        Dim chk As String
        If did = "" Or did1 = "" Then
            chk = "ok"
        ElseIf clid = "" Or clid1 = "" Then
            chk = "ok"
        ElseIf did = did1 Then
            If clid = clid1 Then
                chk = "Destination is same as Source"
            Else
                chk = "ok"
            End If
        Else
            chk = "ok"
        End If
        Return chk
    End Function

    Private Sub rptrdept1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrdept1.ItemDataBound
        Dim cb As CheckBox
        Dim ci As String
        If e.Item.ItemType <> ListItemType.Header And _
                                e.Item.ItemType <> ListItemType.Footer Then
            Try
                cb = CType(e.Item.FindControl("cb1"), CheckBox)
                ci = CType(e.Item.FindControl("lbleqiditem"), Label).Text
            Catch ex As Exception
                cb = CType(e.Item.FindControl("cb2"), CheckBox)
                ci = CType(e.Item.FindControl("lbleqidalt"), Label).Text
            End Try

            If ci = "" Then
                cb.Enabled = False
            Else
                cb.Attributes.Add("onclick", "checkcb(this.checked);")
            End If
        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang42 As Label
			lang42 = CType(e.Item.FindControl("lang42"), Label)
			lang42.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang42")
		Catch ex As Exception
		End Try
		Try
                Dim lang43 As Label
			lang43 = CType(e.Item.FindControl("lang43"), Label)
			lang43.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang43")
		Catch ex As Exception
		End Try
		Try
                Dim lang44 As Label
			lang44 = CType(e.Item.FindControl("lang44"), Label)
			lang44.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang44")
		Catch ex As Exception
		End Try
		Try
                Dim lang45 As Label
			lang45 = CType(e.Item.FindControl("lang45"), Label)
			lang45.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang45")
		Catch ex As Exception
		End Try
		Try
                Dim lang46 As Label
			lang46 = CType(e.Item.FindControl("lang46"), Label)
			lang46.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang46")
		Catch ex As Exception
		End Try
		Try
                Dim lang47 As Label
			lang47 = CType(e.Item.FindControl("lang47"), Label)
			lang47.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang47")
		Catch ex As Exception
		End Try
		Try
                Dim lang48 As Label
			lang48 = CType(e.Item.FindControl("lang48"), Label)
			lang48.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang48")
		Catch ex As Exception
		End Try
		Try
                Dim lang49 As Label
			lang49 = CType(e.Item.FindControl("lang49"), Label)
			lang49.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang49")
		Catch ex As Exception
		End Try
		Try
                Dim lang50 As Label
			lang50 = CType(e.Item.FindControl("lang50"), Label)
			lang50.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang50")
		Catch ex As Exception
		End Try
		Try
                Dim lang51 As Label
			lang51 = CType(e.Item.FindControl("lang51"), Label)
			lang51.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang51")
		Catch ex As Exception
		End Try

End If

 
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang42 As Label
			lang42 = CType(e.Item.FindControl("lang42"), Label)
			lang42.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang42")
		Catch ex As Exception
		End Try
		Try
                Dim lang43 As Label
			lang43 = CType(e.Item.FindControl("lang43"), Label)
			lang43.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang43")
		Catch ex As Exception
		End Try
		Try
                Dim lang44 As Label
			lang44 = CType(e.Item.FindControl("lang44"), Label)
			lang44.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang44")
		Catch ex As Exception
		End Try
		Try
                Dim lang45 As Label
			lang45 = CType(e.Item.FindControl("lang45"), Label)
			lang45.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang45")
		Catch ex As Exception
		End Try
		Try
                Dim lang46 As Label
			lang46 = CType(e.Item.FindControl("lang46"), Label)
			lang46.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang46")
		Catch ex As Exception
		End Try
		Try
                Dim lang47 As Label
			lang47 = CType(e.Item.FindControl("lang47"), Label)
			lang47.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang47")
		Catch ex As Exception
		End Try
		Try
                Dim lang48 As Label
			lang48 = CType(e.Item.FindControl("lang48"), Label)
			lang48.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang48")
		Catch ex As Exception
		End Try
		Try
                Dim lang49 As Label
			lang49 = CType(e.Item.FindControl("lang49"), Label)
			lang49.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang49")
		Catch ex As Exception
		End Try
		Try
                Dim lang50 As Label
			lang50 = CType(e.Item.FindControl("lang50"), Label)
			lang50.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang50")
		Catch ex As Exception
		End Try
		Try
                Dim lang51 As Label
			lang51 = CType(e.Item.FindControl("lang51"), Label)
			lang51.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx","lang51")
		Catch ex As Exception
		End Try

End If

 End Sub

    Private Sub btntrans_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btntrans.ServerClick

    End Sub
	



    Private Sub rptrdept_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrdept.ItemDataBound

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang42.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang42")
        Catch ex As Exception
        End Try
        Try
            lang43.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang43")
        Catch ex As Exception
        End Try
        Try
            lang44.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang44")
        Catch ex As Exception
        End Try
        Try
            lang45.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang45")
        Catch ex As Exception
        End Try
        Try
            lang46.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang46")
        Catch ex As Exception
        End Try
        Try
            lang47.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang47")
        Catch ex As Exception
        End Try
        Try
            lang48.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang48")
        Catch ex As Exception
        End Try
        Try
            lang49.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang49")
        Catch ex As Exception
        End Try
        Try
            lang50.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang50")
        Catch ex As Exception
        End Try
        Try
            lang51.Text = axlabs.GetASPXPage("AppSetDeptTrans.aspx", "lang51")
        Catch ex As Exception
        End Try

    End Sub

End Class
