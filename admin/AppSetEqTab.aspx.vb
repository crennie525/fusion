

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetEqTab
    Inherits System.Web.UI.Page
	Protected WithEvents lang71 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, sid, ro As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String = "status"
    Dim dr As SqlDataReader
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbleq As System.Web.UI.WebControls.Label
    Protected WithEvents dgeqstat As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgeqstat.Columns(0).Visible = False
                dgeqstat.Columns(3).Visible = False
            End If
            Try
                cid = "0"
            Catch ex As Exception
                'Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try

            If cid <> "" Then
                lblcid.Value = cid
                cid = lblcid.Value
                appset.Open()
                PopEqStat(PageNumber)
                appset.Dispose()
            Else
                'Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopEqStat(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopEqStat(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub
    ' Equipment
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopEqStat(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopEqStat(ByVal PageNumber As Integer)
        'Try
        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "status like ''%" & srch & "%'' and compid = ''" & cid & "''"
            FilterCnt = "status like '%" & srch & "%' and compid = '" & cid & "'"
        Else
            Filter = "compid = ''" & cid & "''"
            FilterCnt = "compid = '" & cid & "'"
        End If
        sql = "select count(*) " _
        + "from EqStatus where " & FilterCnt 'compid = '" & cid & "'"
        dgeqstat.VirtualItemCount = appset.Scalar(sql)

        If dgeqstat.VirtualItemCount = 0 Then
            lbleq.Text = "No Equipment Status Records Found"
            Fields = "*"
            Tables = "EqStatus"
            PK = "statid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgeqstat.DataSource = dr
            Try
                dgeqstat.DataBind()
            Catch ex As Exception
                dgeqstat.CurrentPageIndex = 0
                dgeqstat.DataBind()
            End Try
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgeqstat.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgeqstat.PageCount
        Else
            Fields = "*"
            Tables = "EqStatus"
            PK = "statid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgeqstat.DataSource = dr
            Try
                dgeqstat.DataBind()
            Catch ex As Exception
                dgeqstat.CurrentPageIndex = 0
                dgeqstat.DataBind()
            End Try
            dr.Close()
            lbleq.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgeqstat.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgeqstat.PageCount
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopEqStat(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr44" , "AppSetEqTab.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopEqStat(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr45" , "AppSetEqTab.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgeqstat_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeqstat.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("Label11"), Label).Text
        dgeqstat.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopEqStat(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgeqstat_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeqstat.UpdateCommand
        appset.Open()
        Dim id, desc As String
        id = CType(e.Item.FindControl("lbleqstatid"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        Dim old As String = lblold.Value
        Dim cnt As Integer
        If Len(desc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr46" , "AppSetEqTab.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If old <> desc Then
            sql = "select count(*) from eqstatus where status = '" & desc & "'"
            cnt = appset.Scalar(sql)
            If cnt = 0 Then
                sql = "update EqStatus set status = " _
                + "'" & desc & "' where statid = '" & id & "';update equipment set status = '" & desc & "' where status = '" & old & "'"
                appset.Update(sql)
                dgeqstat.EditItemIndex = -1
                PopEqStat(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr47" , "AppSetEqTab.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        Else
            dgeqstat.EditItemIndex = -1
            PopEqStat(PageNumber)
        End If
        appset.Dispose()
    End Sub
    Private Sub dgeqstat_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeqstat.CancelCommand
        dgeqstat.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopEqStat(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgeqstat_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeqstat.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lbleqstatidi"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbleqstatid"), Label).Text
        End Try
        'id = CType(e.Item.Cells(1).Controls(1), Label).Text
        cid = lblcid.Value
        sql = "sp_delEQStatus '" & id & "', '" & cid & "'"
        appset.Update(sql)
        cid = lblcid.Value
        Dim eqcnt As Integer = dgeqstat.VirtualItemCount - 1
        sql = "update AdminTasks set " _
        + "equip = '" & eqcnt & "' where cid = '" & cid & "'"
        appset.Update(sql)
        sql = "select Count(*) from EqStatus " _
        + "where compid = '" & cid & "'"
        PageNumber = appset.PageCount(sql, PageSize)
        'appset.Dispose()
        dgeqstat.EditItemIndex = -1
        If dgeqstat.CurrentPageIndex > PageNumber Then
            dgeqstat.CurrentPageIndex = PageNumber - 1
        End If
        If dgeqstat.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgeqstat.CurrentPageIndex + 1
        End If
        PopEqStat(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgeqstat_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeqstat.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtneweqstat"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)

            If lnamei.Text <> "" Then
                If Len(lnamei.Text) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr48" , "AppSetEqTab.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    Dim eqstat As String
                    Dim cnt As Integer
                    sql = "select count(*) from eqstatus where status = '" & lname & "'"
                    cnt = appset.Scalar(sql)
                    If cnt = 0 Then
                        cid = lblcid.Value
                        sql = "insert into EqStatus " _
                         + "(compid, status) values ('" & cid & "', '" & lname & "')"
                        appset.Update(sql)
                        Dim eqcnt As Integer = dgeqstat.VirtualItemCount + 1
                        lnamei.Text = ""
                        cid = lblcid.Value
                        sql = "select Count(*) from EqStatus " _
                        + "where compid = '" & cid & "'"
                        PageNumber = appset.PageCount(sql, PageSize)
                        PopEqStat(PageNumber)
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr49" , "AppSetEqTab.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                    appset.Dispose()
                End If

            End If

        End If

    End Sub


	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgeqstat.Columns(0).HeaderText = dlabs.GetDGPage("AppSetEqTab.aspx", "dgeqstat", "0")
        Catch ex As Exception
        End Try
        Try
            dgeqstat.Columns(2).HeaderText = dlabs.GetDGPage("AppSetEqTab.aspx", "dgeqstat", "2")
        Catch ex As Exception
        End Try
        Try
            dgeqstat.Columns(3).HeaderText = dlabs.GetDGPage("AppSetEqTab.aspx", "dgeqstat", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang71.Text = axlabs.GetASPXPage("AppSetEqTab.aspx", "lang71")
        Catch ex As Exception
        End Try

    End Sub

End Class
