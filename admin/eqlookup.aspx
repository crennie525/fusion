<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqlookup.aspx.vb" Inherits="lucy_r12.eqlookup" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>eqlookup</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/eqlookupaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table cellspacing="2" cellpadding="2" width="520" style="left: 0px; position: absolute;
        top: 0px">
        <tr>
            <td width="80">
            </td>
            <td width="180">
            </td>
            <td width="260">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblps" runat="server" ForeColor="Red" Font-Size="Small" Font-Names="Arial"
                    Font-Bold="True" Width="376px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang164" runat="server">Search</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
                    CssClass="imgbutton"></asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="overflow: auto; height: 250px">
                    <asp:DataGrid ID="dgdepts" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                        AllowPaging="True" GridLines="None" CellPadding="0" ShowFooter="True" CellSpacing="1"
                        PageSize="100">
                        <FooterStyle BackColor="transparent"></FooterStyle>
                        <EditItemStyle Height="15px"></EditItemStyle>
                        <AlternatingItemStyle Font-Size="X-Small" Font-Names="Arial" Height="15px" BackColor="#E7F1FD">
                        </AlternatingItemStyle>
                        <ItemStyle Font-Size="X-Small" Font-Names="Arial" Height="20px" BackColor="ControlLightLight">
                        </ItemStyle>
                        <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Equipment#">
                                <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <a href="#" id="lbv" runat="server">
                                        <%# DataBinder.Eval(Container, "DataItem.eqnum") %>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Item" Visible="False">
                                <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblv" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Description">
                                <HeaderStyle Width="320px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbld" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.eqdesc") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <HeaderStyle Width="220px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                            ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lbllist" runat="server" name="lbllist">
    <input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt"
        type="hidden" name="txtpgcnt" runat="server">
    <input type="hidden" id="lblret" runat="server" name="lblret">
    <input type="hidden" id="lbltyp" runat="server" name="lbltyp">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
