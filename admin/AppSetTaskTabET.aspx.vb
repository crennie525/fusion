

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetTaskTabET
    Inherits System.Web.UI.Page
	Protected WithEvents lang110 As System.Web.UI.WebControls.Label

	Protected WithEvents lang109 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, ro As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = "status"
    Protected WithEvents lblstat As System.Web.UI.WebControls.Label
    Protected WithEvents dgstat As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgstat.Columns(0).Visible = False
                dgstat.Columns(3).Visible = False
            End If
            cid = Request.QueryString("cid")
            If cid <> "" Then
                lblcid.Value = Request.QueryString("cid")
                cid = lblcid.Value
                appset.Open()
                PopStat(PageNumber)
                appset.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopStat(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopStat(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
     
    End Sub
    ' Status
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopStat(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopStat(ByVal PageNumber As Integer)
        'Try
        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
       srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "status like ''%" & srch & "%'' and compid = ''" & cid & "'' and statid <> 0"
            FilterCnt = "status like '%" & srch & "%' and compid = '" & cid & "' and statid <> 0"
        Else
            Filter = "compid = ''" & cid & "'' and statid <> 0"
            FilterCnt = "compid = '" & cid & "' and statid <> 0"
        End If
        sql = "select count(*) " _
        + "from pmStatus where " & FilterCnt 'compid = '" & cid & "'"
        dgstat.VirtualItemCount = appset.Scalar(sql)
        If dgstat.VirtualItemCount = 0 Then
            lblstat.Text = "No Equipment Status Records Found"
            dgstat.Visible = True
            Tables = "pmStatus"
            PK = "statid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgstat.DataSource = dr
            dgstat.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgstat.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgstat.PageCount
        Else
            Tables = "pmStatus"
            PK = "statid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgstat.DataSource = dr
            dgstat.DataBind()
            dr.Close()
            lblstat.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgstat.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgstat.PageCount
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopStat(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr90" , "AppSetTaskTabET.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopStat(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr91" , "AppSetTaskTabET.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgstat_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgstat.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("Label18"), Label).Text
        dgstat.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopStat(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgstat_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgstat.UpdateCommand
        appset.Open()
        Dim id, desc As String
        cid = lblcid.Value
        id = CType(e.Item.FindControl("lblstatid"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        Dim old As String = lblold.Value
        If old <> desc Then
            Dim faill As String = desc.ToLower
            sql = "select count(*) from pmStatus where lower(status) = '" & faill & "' and compid = '" & cid & "'"
            Dim strchk As Integer
            strchk = appset.Scalar(sql)
            If strchk = 0 Then
                sql = "update pmStatus set status = " _
                + "'" & desc & "' where statid = '" & id & "'"
                appset.Update(sql)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr92" , "AppSetTaskTabET.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
            End If
            dgstat.EditItemIndex = -1
            PopStat(PageNumber)
        Else
            dgstat.EditItemIndex = -1
            PopStat(PageNumber)
        End If
        appset.Dispose()
    End Sub
    Private Sub dgstat_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgstat.CancelCommand
        dgstat.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopStat(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgstat_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgstat.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lblstatidi"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblstatid"), Label).Text
        End Try
        'id = CType(e.Item.Cells(1).Controls(1), Label).Text
        cid = lblcid.Value
        sql = "select count(*) from pmTasks where rdid = '" & id & "'"
        Dim scnt As Integer = appset.Scalar(sql)
        If scnt = 0 Then
            sql = "sp_delPMStatus '" & id & "', '" & cid & "'"
            appset.Update(sql)

            dgstat.EditItemIndex = -1
            sql = "select Count(*) from pmStatus " _
                    + "where compid = '" & cid & "'"
            PageNumber = appset.PageCount(sql, PageSize)
            'appset.Dispose()
            dgstat.EditItemIndex = -1
            If dgstat.CurrentPageIndex > PageNumber Then
                dgstat.CurrentPageIndex = PageNumber - 1
            End If
            If dgstat.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgstat.CurrentPageIndex + 1
            End If
            PopStat(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr93" , "AppSetTaskTabET.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If

        appset.Dispose()
    End Sub

    Private Sub dgstat_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgstat.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewstat"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            If lnamei.Text <> "" Then
                If lname <> "" Then
                    If Len(lname) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr94" , "AppSetTaskTabET.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        Dim stat As String
                        cid = lblcid.Value
                        stat = lname
                        Dim strchk As Integer
                        Dim faill As String = stat.ToLower
                        sql = "select count(*) from pmStatus where lower(status) = '" & faill & "' and compid = '" & cid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "sp_addPMStatus '" & lname & "', " & cid
                            appset.Update(sql)
                            Dim statcnt As Integer = dgstat.VirtualItemCount + 1
                            lnamei.Text = ""
                            sql = "select Count(*) from pmStatus " _
                                            + "where compid = '" & cid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopStat(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr95" , "AppSetTaskTabET.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If
                        appset.Dispose()
                    End If
                End If
            End If
        End If
    End Sub


    Private Sub dgstat_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgstat.ItemDataBound
        Dim ibfm As ImageButton
        Dim ibfm1 As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim stat As String = DataBinder.Eval(e.Item.DataItem, "statid").ToString
            If stat = "1" Or stat = "2" Then
                ibfm = CType(e.Item.FindControl("imgedit"), ImageButton)
                ibfm.Attributes.Add("class", "details")
                ibfm1 = CType(e.Item.FindControl("imgdel"), ImageButton)
                ibfm1.Attributes.Add("class", "details")

            End If
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgstat.Columns(0).HeaderText = dlabs.GetDGPage("AppSetTaskTabET.aspx", "dgstat", "0")
        Catch ex As Exception
        End Try
        Try
            dgstat.Columns(2).HeaderText = dlabs.GetDGPage("AppSetTaskTabET.aspx", "dgstat", "2")
        Catch ex As Exception
        End Try
        Try
            dgstat.Columns(3).HeaderText = dlabs.GetDGPage("AppSetTaskTabET.aspx", "dgstat", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang109.Text = axlabs.GetASPXPage("AppSetTaskTabET.aspx", "lang109")
        Catch ex As Exception
        End Try
        Try
            lang110.Text = axlabs.GetASPXPage("AppSetTaskTabET.aspx", "lang110")
        Catch ex As Exception
        End Try

    End Sub

End Class
