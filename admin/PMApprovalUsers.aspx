<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMApprovalUsers.aspx.vb"
    Inherits="lucy_r12.PMApprovalUsers" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Add PM Assurance Users</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/PMApprovalUsersaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table align="center">
        <tr>
            <td width="75">
            </td>
            <td width="205">
            </td>
            <td width="20">
            </td>
            <td width="80">
            </td>
        </tr>
        <tr height="20">
            <td class="label">
                <asp:Label ID="lang166" runat="server">Current Revision:</asp:Label>
            </td>
            <td colspan="3" id="tdrev" runat="server" class="plainlabel">
            </td>
        </tr>
        <tr height="20">
            <td class="label">
                <asp:Label ID="lang167" runat="server">Current Group:</asp:Label>
            </td>
            <td colspan="3" id="tdgrp" runat="server" class="plainlabel">
            </td>
        </tr>
        <tr height="20">
            <td class="label">
                <asp:Label ID="lang168" runat="server">Add/Remove?</asp:Label>
            </td>
            <td colspan="3" id="tdadd" runat="server" class="plainlabel">
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table width="400">
                    <tr>
                        <td class="bluelabel" width="185">
                            <asp:Label ID="lang169" runat="server">Available PM Logins</asp:Label>
                        </td>
                        <td valign="middle" align="center" width="20">
                        </td>
                        <td class="bluelabel" width="185">
                            <asp:Label ID="lang170" runat="server">Selected PM Logins</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ListBox ID="lbmaster" runat="server" SelectionMode="Multiple" Height="250px"
                                Width="176px"></asp:ListBox>
                        </td>
                        <td>
                            <img class="details" id="todis" runat="server" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                width="20" height="20">
                            <img class="details" id="fromdis" runat="server" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                width="20" height="20"><asp:ImageButton ID="btntolist" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                                </asp:ImageButton><br>
                            <asp:ImageButton ID="btnfromlist" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                            </asp:ImageButton>
                        </td>
                        <td>
                            <asp:ListBox ID="lbselect" runat="server" SelectionMode="Multiple" Height="250px"
                                Width="175px"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">
                            <input class="lilmenu plainlabel" onmouseover="this.className='lilmenuhov plainlabel'"
                                style="width: 55px; height: 24px" onclick="gotoforum();" onmouseout="this.className='lilmenu plainlabel'"
                                type="button" value="Submit">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblaid" type="hidden" runat="server">
    <input id="lblrev" type="hidden" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
