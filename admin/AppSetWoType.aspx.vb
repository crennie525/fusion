

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetWoType
    Inherits System.Web.UI.Page
	Protected WithEvents lang125 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Private Pictures(15) As String
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim dept As String
    Dim Sort As String = "wotype"
    Dim cid, cnm, sid, did, sql, ro As String
    Dim dr As SqlDataReader
    Protected WithEvents cbaci As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwopriid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwopri As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwostat As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblprimsg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwotid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluseprih As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
    Dim prichk As String
    Dim mu As New mmenu_utils_a
    Dim coi As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbltasktype As System.Web.UI.WebControls.Label
    Protected WithEvents dgtt As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ttdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        coi = mu.COMPI
        lblcoi.Value = coi
        If coi = "NISS" Or coi = "ELUX" Then
            dgtt.Columns(4).Visible = False
            dgtt.Columns(5).Visible = False
        End If
        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtt.Columns(0).Visible = False
                dgtt.Columns(7).Visible = False
            End If
            cid = "0" 'Request.QueryString("cid")
            If cid <> "" Then
                lblcid.Value = cid
                cid = cid
                appset.Open()
                PopTasks(PageNumber)
                appset.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "updatepri" Then
                lblret.Value = ""
                appset.Open()
                updatepri()
                PageNumber = txtpg.Value
                PopTasks(PageNumber)
                appset.Dispose()
            ElseIf Request.Form("lblret") = "undopri" Then
                lblret.Value = ""
                appset.Open()
                undopri()
                PageNumber = txtpg.Value
                PopTasks(PageNumber)
                appset.Dispose()
            End If
        End If

    End Sub
    Private Sub updatepri()
        Dim wotid As String = lblwotid.Value
        sql = "update wotype set dfltpriid = null, dfltpri = '0', usepri = '0' where wotid = '" & wotid & "'"
        appset.Update(sql)
    End Sub
    Private Sub undopri()
        Dim wotid As String = lblwotid.Value
        sql = "update wotype set dfltpriid = null, dfltpri = '0', usepri = '0' where wotid = '" & wotid & "'"
        appset.Update(sql)
    End Sub
    Private Sub checkpri()
        
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        'Try
        sql = "select [default] from wovars where [column] = 'wopriority'"

        Try
            prichk = appset.strScalar(sql)
        Catch ex As Exception
            prichk = "no"
        End Try

        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
       srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "wotype like ''%" & srch & "%'' and compid = ''" & cid & "''"
            FilterCnt = "wotype like '%" & srch & "%' and compid = '" & cid & "'"
        Else
            Filter = "compid = ''" & cid & "''"
            FilterCnt = "compid = '" & cid & "'"
        End If
        sql = "select count(*) " _
        + "from wotype where " & FilterCnt 'compid = '" & cid & "'"
        dgtt.VirtualItemCount = appset.Scalar(sql)
        If dgtt.VirtualItemCount = 0 Then
            lbltasktype.Text = "No Work Order Type Records Found"
            dgtt.Visible = True
            Tables = "wotype"
            PK = "wotid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        Else
            Tables = "wotype"
            PK = "wotid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            lbltasktype.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        End If

        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr135" , "AppSetWoType.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr136" , "AppSetWoType.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub


    Private Sub dgtt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.ItemCommand
        Dim lname, wosd As String
        Dim lnamei As TextBox
        Dim ddpria As DropDownList
        Dim pri, priid, stat, wotid As String
        If e.CommandName = "Add" Then
            appset.Open()
            sql = "select [default] from wovars where [column] = 'wopriority'"
            Try
                prichk = appset.strScalar(sql)
            Catch ex As Exception
                prichk = "no"
            End Try
            If prichk = "yes" Then
                pri = lblwopri.Value
                priid = lblwopriid.Value
                If pri = "" Then
                    pri = "0"
                End If
            Else
                ddpria = CType(e.Item.FindControl("ddpria"), DropDownList)
                pri = ddpria.SelectedValue
            End If

            stat = lblwostat.Value


            lnamei = CType(e.Item.FindControl("txtnewtt"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)

            wosd = CType(e.Item.FindControl("txtwtda"), TextBox).Text
            wosd = appset.ModString1(wosd)
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String = tmod.getmsg("cdstr137", "AppSetWoType.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    ElseIf Len(wosd) > 50 Then
                        Dim strMessage As String = tmod.getmsg("cdstr138", "AppSetWoType.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else

                        cid = lblcid.Value
                        Dim tt As String
                        Dim strchk As Integer
                        Dim faill As String = lname.ToLower
                        sql = "select count(*) from wotype where lower(wotype) = '" & faill & "' and compid = '" & cid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            If priid <> "" And prichk = "yes" Then
                                sql = "insert into wotype (wotype, description, compid, dfltpri, dfltpriid, usepri) " _
                                    + "values ('" & lname & "','" & wosd & "','" & cid & "','" & pri & "','" & priid & "','1') select @@identity"
                            Else
                                sql = "insert into wotype (wotype, description, compid, dfltpri) " _
                                    + "values ('" & lname & "','" & wosd & "','" & cid & "','" & pri & "') select @@identity"
                            End If
                            wotid = appset.strScalar(sql)
                            'appset.Update(sql)
                            If stat <> "" Then
                                sql = "update wotype set dfltstat = '" & stat & "' where wotid = '" & wotid & "'"
                                appset.Update(sql)
                            End If
                            Dim ttcnt As Integer = dgtt.VirtualItemCount + 1

                            lnamei.Text = ""
                            sql = "select Count(*) from wotype " _
                               + "where compid = '" & cid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopTasks(PageNumber)
                        Else
                            Dim strMessage As String = tmod.getmsg("cdstr139", "AppSetWoType.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

                        End If
                        'appset.Dispose()
                    End If

                End If
            End If
            appset.Dispose()
        End If
        'lbluseprih.Value = ""
        'lblwopri.Value = ""
        'lblwopriid.Value = ""
        'lblwotid.Value = ""
    End Sub

    Private Sub dgtt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.DeleteCommand
        appset.Open()
        Dim id, desc As String
        cid = lblcid.Value
        id = CType(e.Item.Cells(1).Controls(1), Label).Text
        Try
            desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        Catch ex As Exception
            desc = CType(e.Item.Cells(2).Controls(1), Label).Text
        End Try

        sql = "select count(*) from workorder where worktype = '" & desc & "'"
        Dim tcnt As Integer = appset.Scalar(sql)
        If tcnt = 0 Then
            sql = "delete from wotype where wotid = '" & id & "'"
            appset.Update(sql)
            dgtt.EditItemIndex = -1
            sql = "select Count(*) from wotype " _
                   + "where compid = '" & cid & "'"
            PageNumber = appset.PageCount(sql, PageSize)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            If dgtt.CurrentPageIndex > PageNumber Then
                dgtt.CurrentPageIndex = PageNumber - 1
            End If
            If dgtt.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgtt.CurrentPageIndex + 1
            End If
            PopTasks(PageNumber)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr140", "AppSetWoType.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub

    Private Sub dgtt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.EditCommand
        lblold.Value = CType(e.Item.Cells(2).Controls(1), Label).Text
        PageNumber = txtpg.Value
        dgtt.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.CancelCommand
        dgtt.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.UpdateCommand
        appset.Open()
        Dim ddprie As DropDownList
        Dim pri, priid, stat, wotid, usepri As String
        sql = "select [default] from wovars where [column] = 'wopriority'"
        Try
            prichk = appset.strScalar(sql)
        Catch ex As Exception
            prichk = "no"
        End Try
        'need ref to usepri here?
        'lbluseprie
        Dim lblusepri As Label = CType(e.Item.FindControl("lbluseprie"), Label)
        usepri = lblusepri.Text

        If prichk = "yes" Then
            pri = lblwopri.Value
            priid = lblwopriid.Value
            If pri = "" Then
                pri = "0"
            End If
        Else
            If usepri = "1" Then
                pri = lblwopri.Value
                priid = lblwopriid.Value
                If pri = "" Then
                    pri = "0"
                End If
            Else
                ddprie = CType(e.Item.FindControl("ddprie"), DropDownList)
                pri = ddprie.SelectedValue
            End If

        End If

        stat = lblwostat.Value


        Dim id, desc, act, old, wosd As String
        id = CType(e.Item.FindControl("lblttide"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        If Len(desc) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr141", "AppSetWoType.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        wosd = CType(e.Item.FindControl("txtwde"), TextBox).Text
        wosd = appset.ModString1(wosd)
        If Len(wosd) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr142", "AppSetWoType.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        act = CType(e.Item.FindControl("cbact"), CheckBox).Checked
        Dim faill As String = desc.ToLower
        sql = "select count(*) from wotype where lower(wotype) = '" & faill & "' and compid = '" & cid & "'"
        Dim strchk As Integer
        old = lblold.Value
        If old <> desc Then
            strchk = appset.Scalar(sql)
        Else
            strchk = 0
        End If
        'dfltpri, dfltpriid, usepri
        If strchk = 0 Then
            If priid <> "" And prichk = "yes" Then
                sql = "update wotype set wotype = " _
            + "'" & desc & "', active = '" & act & "', description = '" & wosd & "', dfltpri = '" & pri & "', " _
            + " dfltpriid = '" & priid & "', usepri = '1' where wotid = '" & id & "';update workorder set worktype = '" & desc & "' " _
            + "where worktype = '" & desc & "'"
            Else
                If priid = "" And prichk = "yes" Then
                    sql = "update wotype set wotype = " _
            + "'" & desc & "', active = '" & act & "', description = '" & wosd & "', dfltpri = '" & pri & "', " _
            + " dfltpriid = '" & priid & "', usepri = '1' where wotid = '" & id & "';update workorder set worktype = '" & desc & "' " _
            + "where worktype = '" & desc & "'"
                Else
                    If pri <> "" And pri <> "" Then
                        sql = "update wotype set wotype = " _
            + "'" & desc & "', active = '" & act & "', description = '" & wosd & "', dfltpri = '" & pri & "' where wotid = '" & id & "';update workorder set worktype = '" & desc & "' " _
            + "where worktype = '" & desc & "'"
                    Else
                        sql = "update wotype set wotype = " _
            + "'" & desc & "', active = '" & act & "', description = '" & wosd & "' where wotid = '" & id & "';update workorder set worktype = '" & desc & "' " _
            + "where worktype = '" & desc & "'"
                    End If

                End If
            End If
            appset.Update(sql)
            If stat <> "" Then
                sql = "update wotype set dfltstat = '" & stat & "' where wotid = '" & id & "'"
                appset.Update(sql)
            End If
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            PopTasks(PageNumber)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr143", "AppSetWoType.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()

        lblusepri.Text = ""
        lblwopri.Value = ""
        lblwopriid.Value = ""
        lblwotid.Value = ""
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        Try
            If Not IsDBNull(CatID) OrElse CatID <> "" Then
                iL = CatID
            Else
                CatID = 0
            End If
        Catch ex As Exception

        End Try
        
        Return iL
    End Function

    Private Sub dgtt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtt.ItemDataBound
        Dim ibfm As ImageButton
        Dim ibfm1 As ImageButton
        Dim ibpri As ImageButton
        Dim ibpria As ImageButton
        Dim ibstate As ImageButton
        Dim ibstata As ImageButton
        Dim lblpria As Label
        Dim lblprie As Label
        Dim lblstata As Label
        Dim lblstate As Label
        Dim ibpriw As ImageButton
        Dim ddpria As DropDownList
        Dim ddprie As DropDownList
        'Dim pri, priid As String
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim stat As String = DataBinder.Eval(e.Item.DataItem, "wotype").ToString
            Dim pri As String = DataBinder.Eval(e.Item.DataItem, "dfltpri").ToString
            If stat = "PM" Or stat = "SWR" Or stat = "TPM" Or stat = "MAXPM" Then
                ibfm = CType(e.Item.FindControl("imgedit"), ImageButton)
                ibfm.Attributes.Add("class", "details")
                ibfm1 = CType(e.Item.FindControl("imgdel"), ImageButton)
                ibfm1.Attributes.Add("class", "details")
            End If

        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            ibpri = CType(e.Item.FindControl("imgprie"), ImageButton)
            ibpriw = CType(e.Item.FindControl("imgpriwarn"), ImageButton)
            ibstate = CType(e.Item.FindControl("imgstate"), ImageButton)
            Dim wotid As String = DataBinder.Eval(e.Item.DataItem, "wotid").ToString
            Dim stat As String = DataBinder.Eval(e.Item.DataItem, "wotype").ToString
            If stat = "PM" Or stat = "SWR" Or stat = "TPM" Or stat = "MAXPM" Then
                ibpri.Visible = False
                ibstate.Visible = False
            End If
            lblprie = CType(e.Item.FindControl("txtprie"), Label)
            ddprie = CType(e.Item.FindControl("ddprie"), DropDownList)
            Dim usepri As String = DataBinder.Eval(e.Item.DataItem, "usepri").ToString
            Dim pri As String = DataBinder.Eval(e.Item.DataItem, "dfltpri").ToString
            Dim priid As String = DataBinder.Eval(e.Item.DataItem, "dfltpriid").ToString
            lblwopri.Value = pri
            lblwopriid.Value = priid
            Dim lblprieid As String = lblprie.ClientID.ToString
            lblstate = CType(e.Item.FindControl("txtstate"), Label)
            Dim lblstateid As String = lblstate.ClientID.ToString
            ibstate.Attributes.Add("onclick", "getstat('" & lblstateid & "');return false;")
            If prichk = "yes" Then
                If usepri = "1" Then
                    ddprie.Visible = False
                    ibpriw.Visible = False
                    ibpri.Visible = True
                    ibpri.Attributes.Add("onclick", "getpri('" & lblprieid & "');return false;")
                    lblprie.Visible = True
                Else
                    If pri <> "" And pri <> "0" Then
                        ddprie.Visible = True

                        ibpriw.Visible = True '***
                        lblprimsg.Value = "usecust"
                        ibpriw.Attributes.Add("onclick", "checkpri('" & wotid & "');return false;")

                        ibpri.Visible = False
                        lblprie.Visible = False
                    Else
                        ddprie.Visible = False
                        ibpriw.Visible = False
                        ibpri.Visible = True
                        ibpri.Attributes.Add("onclick", "getpri('" & lblprieid & "');return false;")
                        lblprie.Visible = True
                    End If
                End If
            Else
                If usepri = "1" Then
                    ddprie.Visible = False

                    ibpriw.Visible = True '***
                    lblprimsg.Value = "usestand"
                    ibpriw.Attributes.Add("onclick", "checkpri('" & wotid & "');return false;")

                    ibpri.Visible = True
                    ibpri.Attributes.Add("onclick", "getpri('" & lblprieid & "');return false;")
                    lblprie.Visible = True
                Else
                    ddprie.Visible = True
                    ibpriw.Visible = False
                    ibpri.Visible = False
                    lblprie.Visible = False
                End If
            End If


        End If
        If e.Item.ItemType = ListItemType.Footer Then
            ibpria = CType(e.Item.FindControl("imgpria"), ImageButton)
            ibstata = CType(e.Item.FindControl("imgstata"), ImageButton)
            lblpria = CType(e.Item.FindControl("txtpria"), Label)
            Dim lblpriaid As String = lblpria.ClientID.ToString
            ddpria = CType(e.Item.FindControl("ddpria"), DropDownList)
            lblstata = CType(e.Item.FindControl("txtstata"), Label)
            Dim lblstataid As String = lblstata.ClientID.ToString
            ibstata.Attributes.Add("onclick", "getstat('" & lblstataid & "');return false;")
            If prichk = "yes" Then
                ddpria.Visible = False
                ibpria.Visible = True
                ibpria.Attributes.Add("onclick", "getpri('" & lblpriaid & "');return false;")
                lblpria.Visible = True
            Else
                ddpria.Visible = True
                ibpria.Visible = False
                lblpria.Visible = True
            End If
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtt.Columns(0).HeaderText = dlabs.GetDGPage("AppSetWoType.aspx", "dgtt", "0")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(2).HeaderText = dlabs.GetDGPage("AppSetWoType.aspx", "dgtt", "2")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(3).HeaderText = dlabs.GetDGPage("AppSetWoType.aspx", "dgtt", "3")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(6).HeaderText = dlabs.GetDGPage("AppSetWoType.aspx", "dgtt", "4")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(7).HeaderText = dlabs.GetDGPage("AppSetWoType.aspx", "dgtt", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang125.Text = axlabs.GetASPXPage("AppSetWoType.aspx", "lang125")
        Catch ex As Exception
        End Try

    End Sub

End Class
