

'********************************************************
'*
'********************************************************



Public Class AppSetLabor
    Inherits System.Web.UI.Page
    Protected WithEvents lang82 As System.Web.UI.WebControls.Label

    Protected WithEvents lang81 As System.Web.UI.WebControls.Label

    Protected WithEvents lang80 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim mu As New mmenu_utils_a
    Dim cid, sid, ro, issched As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents ifs As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents iflw As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents ifpa As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lw As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents s As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblissched As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iflt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifl As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txttab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        'Put user code to initialize the page here
        'Dim app As New AppUtils
        'Dim url As String = app.Switch
        'If url <> "ok" Then
        'appchk.Value = "switch"
        'End If
        issched = mu.Is_Sched
        lblissched.Value = issched
        If issched = "0" Then
            lw.Attributes.Add("class", "thdr plainlabelgray")
            s.Attributes.Add("class", "thdr plainlabelgray")
            lw.Attributes.Add("onclick", "")
            s.Attributes.Add("onclick", "")
        End If
        cid = "0"
        If cid <> "" Then
            lblcid.Value = cid
            cid = lblcid.Value
            iflt.Attributes.Add("src", "../labor/PMLeadMan.aspx?cid=" + cid)
            lbltab.Value = "lt"
        End If
        sid = Request.QueryString("sid").ToString
        lblsid.Value = sid
        Try
            ro = HttpContext.Current.Session("ro").ToString
        Catch ex As Exception
            ro = "0"
        End Try
        lblro.Value = ro
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang80.Text = axlabs.GetASPXPage("AppSetLabor.aspx", "lang80")
        Catch ex As Exception
        End Try
        Try
            lang81.Text = axlabs.GetASPXPage("AppSetLabor.aspx", "lang81")
        Catch ex As Exception
        End Try
        Try
            lang82.Text = axlabs.GetASPXPage("AppSetLabor.aspx", "lang82")
        Catch ex As Exception
        End Try

    End Sub

End Class
