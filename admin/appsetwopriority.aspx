﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="appsetwopriority.aspx.vb" Inherits="lucy_r12.appsetwopriority" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="javascript" type="text/javascript">
        function checkpri() {
            var chk = document.getElementById("cbpri");
            var chk2 = document.getElementById("lblusepri").value;
            if (chk2 == "no") {
                document.getElementById("lblusepri").value = "yes";
                chk.checked = true
            }
            else {
                document.getElementById("lblusepri").value = "no";
                chk.checked = false
            }
            document.getElementById("lblret").value = "changepri";
            document.getElementById("form1").submit();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table class="tbg" id="ttdiv" cellspacing="0" cellpadding="0" runat="server">
        <tr>
            <td width="100">
            </td>
            <td width="180">
            </td>
            <td width="140">
            </td>
            <td width="400"></td>
        </tr>
        <tr class="tbg">
            <td style="height: 16px" colspan="3">
                <asp:Label ID="lbltasktype" runat="server" Font-Size="X-Small" Font-Names="Arial"
                    Font-Bold="True" Width="256px" ForeColor="Red"></asp:Label>
            </td>
            <td rowspan="3" align="center" valign="middle">
            <table>
            <tr>
            <td class="plainlabelblue" id="tdmsg" runat="server" align="left">To start using Custom Work Order Prioities check the check box below.<br />
            Current Work Orders with a Work Order Priority already selected will display the current select with an option to switch to Custom Work Order Priorities.</td>
            </tr>
            <tr>
            <td class="plainlabel" align="center"><input type="checkbox" id="cbpri" runat="server" onclick="checkpri();" />Use Custom Work Order Priorities</td>
            </tr>
            </table>
            </td>
        </tr>
        <tr class="tbg">
            <td valign="top" align="center" colspan="3">
                <asp:DataGrid ID="dgtt" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                    AllowPaging="True" GridLines="None" CellPadding="0" CellSpacing="2" ShowFooter="True"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="imgedit" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit This Work Order Priority" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton27" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton28" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblttids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.woprid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.woprid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Priority">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wopri") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewtt" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wopri") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox16" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wopri") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Description">
                            <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wopridesc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtwtda" runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wopridesc") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="txtwde" runat="server" Width="150px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wopridesc") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Active" Visible="false">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbaci" Enabled="False" runat="server">
                                </asp:CheckBox>
                            </ItemTemplate>
                            <EditItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbact" runat="server">
                                </asp:CheckBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid><br />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="txtpg" runat="server" name="Hidden1"><input type="hidden"
        id="txtpgcnt" runat="server" name="txtpgcnt">
    <input type="hidden" id="lblret" runat="server" name="lblret">
    <input type="hidden" id="lblusepri" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblprialert" runat="server" />
    </form>
</body>
</html>
