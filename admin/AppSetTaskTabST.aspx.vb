

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetTaskTabST
    Inherits System.Web.UI.Page
	Protected WithEvents ovid1 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang113 As System.Web.UI.WebControls.Label

	Protected WithEvents lang112 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, ro As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = "skill"
    Protected WithEvents lblskill As System.Web.UI.WebControls.Label
    Protected WithEvents dgskill As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        GetFSOVLIBS()

        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgskill.Columns(0).Visible = False
                dgskill.Columns(4).Visible = False
            End If
            cid = Request.QueryString("cid")
            If cid <> "" Then
                lblcid.Value = Request.QueryString("cid")
                lblsid.Value = HttpContext.Current.Session("dfltps").ToString
                appset.Open()
                PopSkill(PageNumber)
                appset.Dispose()
            Else
                'Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopSkill(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopSkill(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
        'skillPrev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'skillPrev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'skillNext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'skillNext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    Private Sub UpdateLabor()
        'appset.Open()
        sql = "usp_updatelaborrates"
        appset.Update(sql)
        'appset.Dispose()
    End Sub
    'Skill
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopSkill(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopSkill(ByVal PageNumber As Integer)
        'Try
        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "skill like ''%" & srch & "%'' and compid = ''" & cid & "'' and skill <> ''Select''"
            FilterCnt = "skill like '%" & srch & "%' and compid = '" & cid & "' and skill <> 'Select'"
        Else
            Filter = "compid = ''" & cid & "'' and skill <> ''Select''"
            FilterCnt = "compid = '" & cid & "' and skill <> 'Select'"
        End If
        sql = "select count(*) " _
                + "from pmSkills where " & FilterCnt 'compid = '" & cid & "'"
        dgskill.VirtualItemCount = appset.Scalar(sql)
        If dgskill.VirtualItemCount = 0 Then
            lblskill.Text = "No Skill Type Records Found"
            Tables = "pmSkills"
            PK = "skillid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgskill.DataSource = dr
            dgskill.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgskill.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgskill.PageCount
        Else

            Tables = "pmSkills"
            PK = "skillid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgskill.DataSource = dr
            dgskill.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgskill.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgskill.PageCount
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopSkill(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr102", "AppSetTaskTabST.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopSkill(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr103", "AppSetTaskTabST.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgskill_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("Label22"), Label).Text
        dgskill.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopSkill(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgskill_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.UpdateCommand
        appset.Open()
        Dim id, desc, rate, otrate As String
        id = CType(e.Item.FindControl("lblskillid"), Label).Text
        rate = CType(e.Item.FindControl("TextBox1"), TextBox).Text
        If rate = "" Then
            rate = "0"
        End If
        Dim cqtychk As Long
        Try
            cqtychk = System.Convert.ToDecimal(rate)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr104", "AppSetTaskTabST.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        otrate = CType(e.Item.FindControl("txtotrate"), TextBox).Text
        If otrate = "" Then
            otrate = "0"
        End If
        Dim cqtychk1 As Long
        Try
            cqtychk1 = System.Convert.ToDecimal(otrate)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr104", "AppSetTaskTabST.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        Dim old As String = lblold.Value
        If old <> desc Then
            Dim strchk As Integer
            Dim strchk2 As Integer
            Dim faill As String = desc.ToLower
            Dim old1 As String = old.ToLower
            If faill = old1 Then
                strchk = 0
            Else
                sql = "select count(*) from pmSkills where lower(skill) = '" & faill & "' and compid = '" & cid & "'"
                strchk = appset.Scalar(sql)
            End If

            'If strchk = 1 Then
            'sql = "select count(*) from pmSkills where skill = '" & old & "' and compid = '" & cid & "'"
            'strchk2 = appset.Scalar(sql)
            'End If

            If strchk = 0 Then
                Try
                    sql = "update pmSkills set skill = " _
                           + "'" & desc & "', rate = '" & rate & "', otrate = '" & otrate & "' " _
                           + "where skillid = '" & id & "'; " _
                            + "update PMTasks " _
                            + "set skill = '" & desc & "' where skillid = '" & id & "'; " _
                            + "update PMTasks " _
                            + "set origskill = '" & desc & "' where origskillid = '" & id & "'; " _
                            + "update pmtottime " _
                            + "set skill = '" & desc & "' where skillid = '" & id & "'; "
                    appset.Update(sql)
                    'appset.Dispose()
                    dgskill.EditItemIndex = -1
                    PopSkill(PageNumber)
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr105", "AppSetTaskTabST.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
            Else
                Dim strMessage As String = tmod.getmsg("cdstr106", "AppSetTaskTabST.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

                appset.Dispose()
                Exit Sub
            End If
        ElseIf old = desc Then
            Try
                sql = "update pmSkills set skill = " _
                       + "'" & desc & "', rate = '" & rate & "', otrate = '" & otrate & "' " _
                       + "where skillid = '" & id & "'; " _
                            + "update PMTasks " _
                            + "set skill = '" & desc & "' where skillid = '" & id & "'; " _
                            + "update PMTasks " _
                            + "set origskill = '" & desc & "' where origskillid = '" & id & "' " _
                            + "update pmtottime " _
                            + "set skill = '" & desc & "' where skillid = '" & id & "'; "
                appset.Update(sql)
                'appset.Dispose()
                dgskill.EditItemIndex = -1
                PopSkill(PageNumber)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr107", "AppSetTaskTabST.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        Else
            dgskill.EditItemIndex = -1
            PopSkill(PageNumber)
        End If
        'appset.Dispose()
        UpdateLabor()

    End Sub
    Private Sub dgskill_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.CancelCommand
        dgskill.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopSkill(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgskill_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lblskillidi"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblskillid"), Label).Text
        End Try
        'id = CType(e.Item.Cells(1).Controls(1), Label).Text
        sql = "select count(*) from pmtasks where skillid = '" & id & "'"
        Dim scnt As Integer = appset.Scalar(sql)
        If scnt = 0 Then
            sql = "select count(*) from pmsysusers where skillid = '" & id & "'"
            scnt = appset.Scalar(sql)
        End If


        If scnt = 0 Then
            cid = lblcid.Value
            sql = "sp_delPMSkill '" & id & "', '" & cid & "'"
            appset.Update(sql)
            sql = "update AdminTasks " _
              + "set skill = (skill - 1) where cid = '" & cid & "'"
            appset.Update(sql)
            dgskill.EditItemIndex = -1
            sql = "select Count(*) from Skills " _
                    + "where compid = '" & cid & "'"
            PageNumber = appset.PageCount(sql, PageSize)
            'appset.Dispose()
            dgskill.EditItemIndex = -1
            If dgskill.CurrentPageIndex > PageNumber Then
                dgskill.CurrentPageIndex = PageNumber - 1
            End If
            If dgskill.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgskill.CurrentPageIndex + 1
            End If
            PopSkill(PageNumber)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr108", "AppSetTaskTabST.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub


    Private Sub dgskill_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgskill.ItemCommand
        Dim lname, rname As String
        Dim lnamei, rnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewskill"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            rnamei = CType(e.Item.FindControl("txtnewrate"), TextBox)
            rname = rnamei.Text
            If Len(rname) = 0 Then
                rname = "0"
            End If
            Try
                Dim rate As Decimal = System.Convert.ToDecimal(rname)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr109", "AppSetTaskTabST.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try

            If lname <> "" Then
                If lname <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String = tmod.getmsg("cdstr110", "AppSetTaskTabST.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        Dim stat As String
                        cid = lblcid.Value
                        stat = lname
                        Dim strchk As Integer
                        Dim faill As String = stat.ToLower
                        sql = "select count(*) from pmSkills where lower(skill) = '" & faill & "' and compid = '" & cid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "sp_addSkill '" & lname & "', '" & rname & "', '" & cid & "'"
                            appset.Update(sql)
                            Dim skillcnt As Integer = dgskill.VirtualItemCount + 1
                            lnamei.Text = ""
                            sql = "select Count(*) from pmSkills " _
                                    + "where compid = '" & cid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopSkill(PageNumber)
                        Else
                            Dim strMessage As String = tmod.getmsg("cdstr111", "AppSetTaskTabST.aspx.vb")

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If
                        appset.Dispose()
                    End If

                End If
            End If
        End If
    End Sub


    Private Sub dgskill_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgskill.ItemDataBound
        Dim ibfm As ImageButton
        Dim ibfm1 As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim stat As String = DataBinder.Eval(e.Item.DataItem, "skill").ToString
            Dim statid As String = DataBinder.Eval(e.Item.DataItem, "skillid").ToString
            If stat = "Operator" Or statid = "2" Then
                ibfm = CType(e.Item.FindControl("imgedit"), ImageButton)
                ibfm.Attributes.Add("class", "details")
                ibfm1 = CType(e.Item.FindControl("imgdel"), ImageButton)
                ibfm1.Attributes.Add("class", "details")

            End If
        End If
    End Sub




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgskill.Columns(0).HeaderText = dlabs.GetDGPage("AppSetTaskTabST.aspx", "dgskill", "0")
        Catch ex As Exception
        End Try
        Try
            dgskill.Columns(2).HeaderText = dlabs.GetDGPage("AppSetTaskTabST.aspx", "dgskill", "2")
        Catch ex As Exception
        End Try
        Try
            dgskill.Columns(3).HeaderText = dlabs.GetDGPage("AppSetTaskTabST.aspx", "dgskill", "3")
        Catch ex As Exception
        End Try
        Try
            dgskill.Columns(5).HeaderText = dlabs.GetDGPage("AppSetTaskTabST.aspx", "dgskill", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang112.Text = axlabs.GetASPXPage("AppSetTaskTabST.aspx", "lang112")
        Catch ex As Exception
        End Try
        Try
            lang113.Text = axlabs.GetASPXPage("AppSetTaskTabST.aspx", "lang113")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("AppSetTaskTabST.aspx", "ovid1") & "')")
            ovid1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
