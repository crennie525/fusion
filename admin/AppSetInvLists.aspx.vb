

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetInvLists
    Inherits System.Web.UI.Page
	Protected WithEvents lang79 As System.Web.UI.WebControls.Label

	Protected WithEvents lang78 As System.Web.UI.WebControls.Label

	Protected WithEvents lang77 As System.Web.UI.WebControls.Label

	Protected WithEvents lang76 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, sid, list, ro As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String = "value asc"
    Dim dr As SqlDataReader
    Protected WithEvents lbllist As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddlists As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblscreenname As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblps As System.Web.UI.WebControls.Label
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tdps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgdepts.Columns(0).Visible = False

            End If
            appset.Open()
            GetLists()
            LoadDepts(PageNumber)
            appset.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadDepts(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadDepts(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub
    Private Sub GetLists()
        sql = "select * from invlists"
        dr = appset.GetRdrData(sql)
        ddlists.DataSource = dr
        ddlists.DataValueField = "listname"
        ddlists.DataTextField = "screenname"
        Try
            ddlists.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        Try
            ddlists.SelectedIndex = 0
            lbllist.Value = ddlists.SelectedValue.ToString
            lblscreenname.Value = ddlists.SelectedItem.ToString
        Catch ex As Exception

        End Try
      
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadDepts(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub LoadDepts(ByVal PageNumber As Integer)
        'Try
        list = lbllist.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        If list = "order" Then
            If Len(srch) > 0 Then
                Filter = "(orderunit like ''%" & srch & "%'')"
                FilterCnt = "(orderunit like '%" & srch & "%')"
            Else
                Filter = "orderunit is not null"
                FilterCnt = "orderunit is not null"
            End If
            sql = "select Count(*) from invorderunit where " & FilterCnt
        Else
            If Len(srch) > 0 Then
                Filter = "(value like ''%" & srch & "%'' or description like ''%" & srch & "%'') and listname = ''" & list & "''"
                FilterCnt = "(value like '%" & srch & "%' or description like '%" & srch & "%') and listname = '" & list & "'"
            Else
                Filter = "listname = ''" & list & "''"
                FilterCnt = "listname = '" & list & "'"
            End If
            sql = "select Count(*) from invvallist where " & FilterCnt
        End If



        Dim dc As Integer = appset.Scalar(sql)

        dgdepts.VirtualItemCount = dc
        If list = "order" Then
            If dc = 0 Then
                lblps.Visible = True
                lblps.Text = "No Records Found"
                dgdepts.Visible = True
                Tables = "invorderunit"
                PK = "orderunit"
                Sort = "orderunit asc"
                PageSize = "10"
                dgdepts.Columns(1).HeaderText = "Order Unit"
                dgdepts.Columns(2).HeaderText = "Conversion"
                Fields = "orderunit as value, conversion as description"
                dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
                dgdepts.DataSource = dr
                Try
                    dgdepts.DataBind()
                Catch ex As Exception
                    dgdepts.CurrentPageIndex = 0
                    dgdepts.DataBind()
                End Try
                dr.Close()
                txtpg.Value = PageNumber
                txtpgcnt.Value = dgdepts.PageCount
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            Else
                dgdepts.Visible = True
                lblps.Visible = False
                Tables = "invorderunit"
                PK = "orderunit"
                Sort = "orderunit asc"
                PageSize = "10"
                dgdepts.Columns(1).HeaderText = "Order Unit"
                dgdepts.Columns(2).HeaderText = "Conversion"
                Fields = "orderunit as value, conversion as description, ''0'' as valid"
                dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
                dgdepts.DataSource = dr
                dgdepts.DataBind()
                dr.Close()
                lblps.Text = ""
                txtpg.Value = PageNumber
                txtpgcnt.Value = dgdepts.PageCount
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            End If
        Else
            If dc = 0 Then
                lblps.Visible = True
                lblps.Text = "No Records Found"
                dgdepts.Visible = True
                Tables = "invvallist"
                PK = "valid"
                Sort = "value asc"
                PageSize = "10"
                Fields = "*"
                dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
                dgdepts.DataSource = dr
                Try
                    dgdepts.DataBind()
                Catch ex As Exception
                    dgdepts.CurrentPageIndex = 0
                    dgdepts.DataBind()
                End Try
                dr.Close()
                txtpg.Value = PageNumber
                txtpgcnt.Value = dgdepts.PageCount
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            Else
                dgdepts.Visible = True
                lblps.Visible = False
                Tables = "invvallist"
                PK = "valid"
                Sort = "value asc"
                PageSize = "10"
                Fields = "*"
                dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
                dgdepts.DataSource = dr
                dgdepts.DataBind()
                dr.Close()
                lblps.Text = ""
                txtpg.Value = PageNumber
                txtpgcnt.Value = dgdepts.PageCount
                lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadDepts(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr50" , "AppSetInvLists.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadDepts(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr51" , "AppSetInvLists.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgdepts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdepts.ItemDataBound
        'If e.Item.ItemType = ListItemType.Header Then
        'Dim name As String
        'If e.Item.ItemType = ListItemType.Item Then
        'name = CType(e.Item.FindControl("Label2"), Label).Text
        'dgdepts.Columns(1).HeaderText = name
        'ElseIf e.Item.ItemType = ListItemType.EditItem Then
        'End If
        'End If
        'If e.Item.ItemType <> ListItemType.Header And _
        '                e.Item.ItemType <> ListItemType.Footer Then
        'Dim deleteButton As ImageButton = CType(e.Item.FindControl("btndel"), ImageButton)
        'deleteButton.Attributes("onclick") = "javascript:return " & _
        '"confirm('Are you sure you want to delete Charge# Field " & _
        'DataBinder.Eval(e.Item.DataItem, "fldname") & "?')"
        'End If
    End Sub

    Private Sub ddlists_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlists.SelectedIndexChanged
        lbllist.Value = ddlists.SelectedValue.ToString
        lblscreenname.Value = ddlists.SelectedItem.ToString
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadDepts(PageNumber)
        appset.Dispose()

    End Sub

    Private Sub dgdepts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.CancelCommand
        dgdepts.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        LoadDepts(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgdepts_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("LinkButton1"), Label).Text
        dgdepts.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        LoadDepts(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgdepts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.ItemCommand
       
        Dim lid, lname, ldesc, llabel As String
        Dim lnamei, ldesci, llabeli As TextBox

        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewlname"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            ldesci = CType(e.Item.FindControl("txtnewldesc"), TextBox)
            ldesc = ldesci.Text
            ldesc = appset.ModString1(ldesc)
            llabeli = CType(e.Item.FindControl("txtnewllabel"), TextBox)
            llabel = llabeli.Text
            list = lbllist.Value
            If list = "order" Then
                Try
                    Dim rate As Decimal = System.Convert.ToDecimal(ldesc)
                Catch ex As Exception
                    Dim strMessage As String =  tmod.getmsg("cdstr52" , "AppSetInvLists.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
                If lnamei.Text <> "" Then
                    If Len(lname) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr53" , "AppSetInvLists.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        cid = lblcid.Value

                        Dim strchk As Integer
                        Dim faill As String = lname.ToLower
                        sql = "select count(*) from invorderunit where orderunit = '" & lname & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "insert into invorderunit (orderunit, conversion) " _
                            + "values ('" & lname & "', '" & ldesc & "')"
                            appset.Update(sql)
                            Dim deptcnt As Integer = dgdepts.VirtualItemCount + 1
                            If IsDBNull(dgdepts.VirtualItemCount) Or dgdepts.VirtualItemCount = 0 Then
                                deptcnt = 1
                            End If
                            lnamei.Text = ""
                            ldesci.Text = ""
                            llabeli.Text = ""
                            sql = "select Count(*) from invorderunit"
                            PageNumber = appset.PageCount(sql, PageSize)
                            LoadDepts(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr54" , "AppSetInvLists.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                    End If
                    appset.Dispose()
                End If
            Else
                If Len(ldesc) > 200 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr55" , "AppSetInvLists.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                If lnamei.Text <> "" Then
                    If Len(lname) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr56" , "AppSetInvLists.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        cid = lblcid.Value

                        Dim strchk As Integer
                        Dim faill As String = lname.ToLower
                        sql = "select count(*) from invvallist where value = '" & lname & "' and listname = '" & list & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "insert into invvallist (listname, value, description) " _
                            + "values ('" & list & "', '" & lname & "', '" & ldesc & "')"
                            appset.Update(sql)
                            Dim deptcnt As Integer = dgdepts.VirtualItemCount + 1
                            If IsDBNull(dgdepts.VirtualItemCount) Or dgdepts.VirtualItemCount = 0 Then
                                deptcnt = 1
                            End If
                            lnamei.Text = ""
                            ldesci.Text = ""
                            llabeli.Text = ""
                            sql = "select Count(*) from invvallist " _
                            + "where listname = '" & list & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            LoadDepts(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr57" , "AppSetInvLists.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                    End If
                    appset.Dispose()
                End If
            End If
            
        End If
    End Sub

    Private Sub dgdepts_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.UpdateCommand
        appset.Open()
        list = lbllist.Value
       
        Dim dept, desc, did As String
        dept = CType(e.Item.Cells(1).Controls(1), TextBox).Text
        dept = appset.ModString1(dept)
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        dept = appset.ModString1(dept)
        did = CType(e.Item.FindControl("Textbox1"), Label).Text
        
        Dim faill As String = dept.ToLower
        Dim old As String = lblold.Value
        If list = "lottype" Then
            If old = "NOLOT" Then
                Dim strMessage As String =  tmod.getmsg("cdstr58" , "AppSetInvLists.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
                Exit Sub
            End If
        End If
        If list = "order" Then
            If dept <> "" Then
                Try
                    Dim rate As Decimal = System.Convert.ToDecimal(desc)
                Catch ex As Exception
                    Dim strMessage As String =  tmod.getmsg("cdstr59" , "AppSetInvLists.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    appset.Dispose()
                    Exit Sub
                End Try
                If Len(dept) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr60" , "AppSetInvLists.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    appset.Dispose()
                    Exit Sub
                End If
                If old <> dept Then
                    sql = "select count(*) from invorderunit where orderunit = '" & dept & "'"
                    Dim strchk As Integer
                    strchk = appset.Scalar(sql)
                    If strchk = 0 Then
                        sql = "update invorderunit set orderunit = " _
                        + "'" & dept & "', conversion = '" & desc & "' where orderunit = '" & old & "'"
                        appset.Update(sql)
                        sql = "update inventory set orderunit = '" & dept & "' where orderunit = '" & old & "'"
                        appset.Update(sql)
                        'appset.Dispose()
                        dgdepts.EditItemIndex = -1
                        LoadDepts(PageNumber)
                        'Catch ex As Exception
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr61" , "AppSetInvLists.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        appset.Dispose()
                    End If
                Else
                    sql = "update invorderunit set orderunit = " _
                       + "'" & dept & "', conversion = '" & desc & "' where orderunit = '" & dept & "'"
                    appset.Update(sql)
                    dgdepts.EditItemIndex = -1
                    LoadDepts(PageNumber)
                End If
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr62" , "AppSetInvLists.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        Else
            If Len(desc) > 200 Then
                Dim strMessage As String =  tmod.getmsg("cdstr63" , "AppSetInvLists.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
                Exit Sub
            End If
            If dept <> "" Then
                If Len(dept) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr64" , "AppSetInvLists.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    appset.Dispose()
                    Exit Sub
                End If
                If old <> dept Then
                    sql = "select count(*) from invvallist where value = '" & dept & "' and listname = '" & list & "'"
                    Dim strchk As Integer
                    strchk = appset.Scalar(sql)
                    If strchk = 0 Then
                        sql = "update invvallist set value = " _
                        + "'" & dept & "', description = '" & desc & "' where valid = '" & did & "'"
                        appset.Update(sql)
                        If list = "stocktype" Then
                            sql = "update item set stocktype = '" & dept & "' where stocktype = '" & old & "' " _
                            + "update lubricants set stocktype = '" & dept & "' where stocktype = '" & old & "' " _
                            + "update tool set stocktype = '" & dept & "' where stocktype = '" & old & "' " _
                            + "update inventory set category = '" & dept & "' where category = '" & old & "'"
                        ElseIf list = "lottype" Then
                            sql = "update item set lottype = '" & dept & "' where lottype = '" & old & "'"
                        ElseIf list = "msds" Then
                            sql = "update item set msdsnum = '" & dept & "' where msdsnum = '" & old & "' " _
                            + "update lubricants set msdsnum = '" & dept & "' where msdsnum = '" & old & "'"
                        ElseIf list = "hazard" Then
                            sql = "update item set hazardid = '" & dept & "' where hazardid = '" & old & "' " _
                            + "update lubricants set hazardid = '" & dept & "' where hazardid = '" & old & "'"
                        End If
                        'location is storeroom in inventory table
                        If list = "storeroom" Then
                            sql = "update inventory set location = '" & dept & "' where location = '" & old & "'"
                        ElseIf list = "orderunit" Then
                            sql = "update inventory set orderunit = '" & dept & "' where orderunit = '" & old & "'"
                        ElseIf list = "stockcat" Then
                            sql = "update inventory set catcode = '" & dept & "' where catcode = '" & old & "'"
                        End If
                        appset.Update(sql)
                        'appset.Dispose()
                        dgdepts.EditItemIndex = -1
                        LoadDepts(PageNumber)
                        'Catch ex As Exception
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr65" , "AppSetInvLists.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        appset.Dispose()
                    End If
                Else
                    sql = "update invvallist set value = " _
                        + "'" & dept & "', description = '" & desc & "' where valid = '" & did & "'"
                    appset.Update(sql)
                    dgdepts.EditItemIndex = -1
                    LoadDepts(PageNumber)
                End If
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr66" , "AppSetInvLists.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
        appset.Dispose()
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("AppSetInvLists.aspx", "dgdepts", "0")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(1).HeaderText = dlabs.GetDGPage("AppSetInvLists.aspx", "dgdepts", "1")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(2).HeaderText = dlabs.GetDGPage("AppSetInvLists.aspx", "dgdepts", "2")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang76.Text = axlabs.GetASPXPage("AppSetInvLists.aspx", "lang76")
        Catch ex As Exception
        End Try
        Try
            lang77.Text = axlabs.GetASPXPage("AppSetInvLists.aspx", "lang77")
        Catch ex As Exception
        End Try
        Try
            lang78.Text = axlabs.GetASPXPage("AppSetInvLists.aspx", "lang78")
        Catch ex As Exception
        End Try
        Try
            lang79.Text = axlabs.GetASPXPage("AppSetInvLists.aspx", "lang79")
        Catch ex As Exception
        End Try

    End Sub

End Class
