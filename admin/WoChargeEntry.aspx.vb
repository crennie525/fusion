

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Collections

Public Class WoChargeEntry
    Inherits System.Web.UI.Page
	Protected WithEvents lang195 As System.Web.UI.WebControls.Label

	Protected WithEvents lang194 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Private Pictures(15) As String
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim dept As String
    Dim Sort As String = "comp01 asc"
    Dim cid, cnm, sid, did, sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lblcols As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
    Dim dslev As DataSet
    Dim ds As DataSet
    Dim dt(10) As String
    Dim dl(10) As String
    Protected WithEvents lbldatatype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ttfirst As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ttlast As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddfilt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim ti(10) As String
    Dim ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbltasktype As System.Web.UI.WebControls.Label
    Protected WithEvents dgtt As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgtasktype As System.Web.UI.WebControls.Label
    Protected WithEvents ttPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ttNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ttdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtt.Columns(0).Visible = False
                dgtt.Columns(13).Visible = False
            End If

            cid = "0" 'Request.QueryString("cid")
            If cid <> "" Then
                lblcid.Value = cid
                cid = cid
                appset.Open()
                ddfilt.DataSource = PopTypes()
                ddfilt.DataTextField = "wochargetype"
                ddfilt.DataValueField = "wochargetype"
                Try
                    ddfilt.DataBind()
                Catch ex As Exception

                End Try

                ddfilt.Items.Insert(0, New ListItem("Select"))
                ddfilt.Items(0).Value = 0
                lblcols.Value = GetCnt()
                ConfigureGrid()
                PopTasks(PageNumber)
                appset.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
            ttNext.Attributes.Add("onclick", "getnext();")
            ttPrev.Attributes.Add("onclick", "getprev();")
            ttfirst.Attributes.Add("onclick", "getfirst();")
            ttlast.Attributes.Add("onclick", "getlast();")
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
        'ttPrev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'ttPrev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'ttNext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'ttNext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr178", "eqlookup.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr179", "eqlookup.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Function GetCnt()
        Dim fldcnt As Integer
        cid = lblcid.Value
        sql = "select count(*) from wochargeconfig where compid = '" & cid & "'"
        fldcnt = appset.Scalar(sql)
        Return fldcnt
    End Function

    Private Sub dgtt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtt.ItemDataBound
        If e.Item.ItemType = ListItemType.Footer Then
            Dim txt As TextBox = CType(e.Item.FindControl("txtcomp01a"), TextBox)
            Dim hdr As Integer = dgtt.Columns(3).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp02a"), TextBox)
            hdr = dgtt.Columns(4).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp03a"), TextBox)
            hdr = dgtt.Columns(5).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp04a"), TextBox)
            hdr = dgtt.Columns(6).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp05a"), TextBox)
            hdr = dgtt.Columns(7).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp06a"), TextBox)
            hdr = dgtt.Columns(8).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp07a"), TextBox)
            hdr = dgtt.Columns(9).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp08a"), TextBox)
            hdr = dgtt.Columns(10).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp09a"), TextBox)
            hdr = dgtt.Columns(11).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp10a"), TextBox)
            hdr = dgtt.Columns(12).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
        ElseIf e.Item.ItemType = ListItemType.EditItem Then
            Dim txt As TextBox = CType(e.Item.FindControl("txtcomp01"), TextBox)
            Dim hdr As Integer = dgtt.Columns(3).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp02"), TextBox)
            hdr = dgtt.Columns(4).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp03"), TextBox)
            hdr = dgtt.Columns(5).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp04"), TextBox)
            hdr = dgtt.Columns(6).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp05"), TextBox)
            hdr = dgtt.Columns(7).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp06"), TextBox)
            hdr = dgtt.Columns(8).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp07"), TextBox)
            hdr = dgtt.Columns(9).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp08"), TextBox)
            hdr = dgtt.Columns(10).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp09"), TextBox)
            hdr = dgtt.Columns(11).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
            txt = CType(e.Item.FindControl("txtcomp10"), TextBox)
            hdr = dgtt.Columns(12).ItemStyle.Width.Value
            txt.Width = System.Web.UI.WebControls.Unit.Pixel(hdr)
        End If
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then

        End If
    End Sub
    Private Sub ConfigureGrid()
        Dim colindx As Integer = 2
        Dim colwid As Integer = 0
        Dim cnt As Integer = -1
        Dim hdr, dtyp As String
        cid = "0"
        Dim hdrlen, datalen, colwidth As Integer
        sql = "select * from wochargeconfig where compid = '" & cid & "' order by fldorder"
        dr = appset.GetRdrData(sql)
        While dr.Read
            colindx += 1
            cnt += 1
            hdr = dr.Item("fldname").ToString
            dgtt.Columns(colindx).HeaderText = hdr
            hdrlen = Len(hdr)
            datalen = dr.Item("length").ToString
            colwidth = GetColWidth(hdrlen, datalen)
            dgtt.Columns(colindx).ItemStyle.Width = System.Web.UI.WebControls.Unit.Pixel(colwidth)
            colwid += colwidth
            dtyp = dr.Item("datatype").ToString
            dt(cnt) = dtyp
            dl(cnt) = colwidth
            ti(cnt) = hdr
        End While
        dr.Close()
        dgtt.Columns(2).ItemStyle.Width = System.Web.UI.WebControls.Unit.Pixel(colwid)
        colwid += colwid + 220
        ttdiv.Attributes.Add("width", colwid)
        colindx += 1
        Dim i As Integer
        For i = colindx To 12
            dgtt.Columns(i).Visible = False
            colindx += 1
        Next
    End Sub
    Private Function GetColWidth(ByVal hdr As Integer, ByVal data As Integer)
        Dim col As Integer
        If hdr > data Then
            col = hdr * 6
        Else
            col = data * 6
        End If
        If col < 50 Then
            col = 50
        End If
        Return col
    End Function
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        Dim srch As String = txtsrch.Text
        Dim filt, filtd As String
        cid = lblcid.Value
        cid = "0"
        If Len(srch) <> 0 Then
            filt = "compid = " & cid & " and wocharge like '%" & srch & "%'"
            filtd = "compid = " & cid & " and wocharge like ''%" & srch & "%''"
            lblfilt.Value = filt
            lblfiltd.Value = filtd
            appset.Open()
            PopTasks(1)
            appset.Dispose()
        End If
    End Sub
    Private Sub ddfilt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddfilt.SelectedIndexChanged
        Dim srch As String = txtsrch.Text
        Dim sel As String = ddfilt.SelectedValue
        Dim filt, filtd As String
        cid = "0"
        If ddfilt.SelectedIndex <> 0 Then
            cid = lblcid.Value
            If Len(srch) <> 0 Then
                filt = "compid = " & cid & " and wocharge like '%" & srch & "%' and wochargetype = '" & sel & "'"
                filtd = "compid = " & cid & " and wocharge like ''%" & srch & "%'' and wochargetype = ''" & sel & "''"
            Else
                filt = "compid = " & cid & " and wochargetype = '" & sel & "'"
                filtd = "compid = " & cid & " and wochargetype = ''" & sel & "''"
            End If
            lblfilt.Value = filt
            lblfiltd.Value = filtd
            appset.Open()
            PopTasks(1)
            appset.Dispose()
        End If

    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        cid = "0"
        Dim fldcnt As Integer
        Dim filt, filtd As String
        If txtsrch.Text <> "" Then
            filt = lblfilt.Value
            Filter = filt
        Else
            Filter = "compid = " & cid '"compid = '" & cid & "'"
        End If
        cid = lblcid.Value
        sql = "select count(*) " _
        + "from wocharge where " & Filter 'compid = '" & cid & "'"
        fldcnt = appset.Scalar(sql)
        dgtt.VirtualItemCount = fldcnt


        Dim dv As DataView

        If dgtt.VirtualItemCount = 0 Then
            lbltasktype.Text = "No Charge Number Records Found"
            dgtt.Visible = True
            ttPrev.Visible = False
            ttNext.Visible = False
            If txtsrch.Text <> "" Then
                filtd = lblfiltd.Value
                Filter = filtd
            Else
                Filter = "compid = " & cid '"compid = '" & cid & "'"
            End If

            Tables = "wocharge"
            PK = "wochid"
            PageSize = "10"
            ds = appset.GetDSPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dv = ds.Tables(0).DefaultView
            dgtt.DataSource = dv
            dgtt.DataBind()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            'dr.Close()
        Else
            If txtsrch.Text <> "" Then
                filtd = lblfiltd.Value
                Filter = filtd
            Else
                Filter = "compid = " & cid '"compid = '" & cid & "'"
            End If
            Tables = "wocharge"
            PK = "wochid"
            PageSize = "10"
            ds = appset.GetDSPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dv = ds.Tables(0).DefaultView
            dgtt.DataSource = dv
            dgtt.DataBind()
            lblpgtasktype.Text = PageNumber & " of " & dgtt.PageCount
            '"Page " & dgtt.CurrentPageIndex + 1
            lblpgcnt.Value = dgtt.PageCount
            lbltasktype.Text = ""
            ttPrev.Visible = True
            ttNext.Visible = True
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            'checkFreqStatPgCnt()
        End If

        'Catch ex As Exception

        'End Try
    End Sub
    Public Function PopTypes() As DataSet
        cid = lblcid.Value
        cid = "0"
        sql = "select wochargetype from wochargetype where compid = '" & cid & "' or wocid = 0"
        dslev = appset.GetDSData(sql)
        Return dslev
    End Function

    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function

    Private Sub dgtt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.EditCommand
        PageNumber = dgtt.CurrentPageIndex + 1
        dgtt.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.CancelCommand
        dgtt.EditItemIndex = -1
        appset.Open()
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub
    Private Function IntChk(ByVal num As String)
        Dim ret As String
        Dim numchk As Long
        Try
            ret = "yes"
            numchk = System.Convert.ToInt64(num)
        Catch ex As Exception
            ret = "no"
        End Try
        Return ret
    End Function
    Private Sub getvals()
        Dim colindx As Integer = 2
        Dim colwid As Integer = 0
        Dim cnt As Integer = -1
        Dim hdr, dtyp As String
        cid = "0"
        Dim hdrlen, datalen, colwidth As Integer
        sql = "select * from wochargeconfig where compid = '" & cid & "' order by fldorder"
        dr = appset.GetRdrData(sql)
        While dr.Read
            cnt += 1
            hdr = dr.Item("fldname").ToString
            datalen = dr.Item("length").ToString
            colwidth = GetColWidth(hdrlen, datalen)
            dtyp = dr.Item("datatype").ToString
            dt(cnt) = dtyp
            dl(cnt) = datalen
            ti(cnt) = hdr
        End While
        dr.Close()
    End Sub
    Private Sub dgtt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.ItemCommand

        Dim txt As TextBox
        Dim ret As String
        Dim flen As Integer
        Dim cnt As Integer = lblcols.Value
        Dim c1, c2, c3, c4, c5, c6, c7, c8, c9, c0 As String
        cid = lblcid.Value
        If e.CommandName = "Add" Then
            appset.Open()
            getvals()
            txt = CType(e.Item.FindControl("txtcomp01a"), TextBox)
            c1 = txt.Text
            If RTrim(dt(0)) = "INT" Then
                ret = IntChk(c1)
                If ret = "no" Then
                    Dim strMessage As String = ti(0) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    appset.Dispose()
                    Exit Sub
                Else
                    flen = dl(0)
                    If Len(c1) > flen Then
                        Dim strMessage As String = ti(0) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If

            txt = CType(e.Item.FindControl("txtcomp02a"), TextBox)
            c2 = txt.Text
            If RTrim(dt(1)) = "INT" Then
                ret = IntChk(c2)
                If ret = "no" Then
                    Dim strMessage As String = ti(1) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(1)
                    If Len(c2) > flen Then
                        Dim strMessage As String = ti(1) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If

            txt = CType(e.Item.FindControl("txtcomp03a"), TextBox)
            c3 = txt.Text
            If RTrim(dt(2)) = "INT" Then
                ret = IntChk(c3)
                If ret = "no" Then
                    Dim strMessage As String = ti(2) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(2)
                    If Len(c3) > flen Then
                        Dim strMessage As String = ti(2) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If

            txt = CType(e.Item.FindControl("txtcomp04a"), TextBox)
            c4 = txt.Text
            If RTrim(dt(3)) = "INT" Then
                ret = IntChk(c4)
                If ret = "no" Then
                    Dim strMessage As String = ti(3) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(3)
                    If Len(c4) > flen Then
                        Dim strMessage As String = ti(3) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If

            txt = CType(e.Item.FindControl("txtcomp05a"), TextBox)
            c5 = txt.Text
            If RTrim(dt(4)) = "INT" Then
                ret = IntChk(c5)
                If ret = "no" Then
                    Dim strMessage As String = ti(4) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(4)
                    If Len(c5) > flen Then
                        Dim strMessage As String = ti(4) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If

            txt = CType(e.Item.FindControl("txtcomp06a"), TextBox)
            c6 = txt.Text
            If RTrim(dt(5)) = "INT" Then
                ret = IntChk(c6)
                If ret = "no" Then
                    Dim strMessage As String = ti(5) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(5)
                    If Len(c6) > flen Then
                        Dim strMessage As String = ti(5) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If

            txt = CType(e.Item.FindControl("txtcomp07a"), TextBox)
            c7 = txt.Text

            If RTrim(dt(6)) = "INT" Then
                ret = IntChk(c7)
                If ret = "no" Then
                    Dim strMessage As String = ti(6) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(6)
                    If Len(c7) > flen Then
                        Dim strMessage As String = ti(6) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If

            txt = CType(e.Item.FindControl("txtcomp08a"), TextBox)
            c8 = txt.Text
            If RTrim(dt(7)) = "INT" Then
                ret = IntChk(c8)
                If ret = "no" Then
                    Dim strMessage As String = ti(7) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(7)
                    If Len(c8) > flen Then
                        Dim strMessage As String = ti(7) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If

            txt = CType(e.Item.FindControl("txtcomp09a"), TextBox)
            c9 = txt.Text
            If RTrim(dt(8)) = "INT" Then
                ret = IntChk(c9)
                If ret = "no" Then
                    Dim strMessage As String = ti(8) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(8)
                    If Len(c9) > flen Then
                        Dim strMessage As String = ti(8) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If
            'End If
            'If cnt > 9 Then
            txt = CType(e.Item.FindControl("txtcomp10a"), TextBox)
            c0 = txt.Text
            If RTrim(dt(9)) = "INT" Then
                ret = IntChk(c0)
                If ret = "no" Then
                    Dim strMessage As String = ti(9) & " Must Be Numeric"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                Else
                    flen = dl(9)
                    If Len(c0) > flen Then
                        Dim strMessage As String = ti(9) & " Is Limited To " & flen & "Characters in Length"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            End If
            'End If

            Dim dd As DropDownList
            dd = CType(e.Item.FindControl("ddtypa"), DropDownList)
            Dim ddi, ddv As String
            ddi = dd.SelectedIndex
            ddv = dd.SelectedValue
            Dim ccnt As Integer
            sql = "select count(*) from wocharge where " _
            + "comp01 = '" & c1 & "' and " _
            + "comp02 = '" & c2 & "' and " _
            + "comp03 = '" & c3 & "' and " _
            + "comp04 = '" & c4 & "' and " _
            + "comp05 = '" & c5 & "' and " _
            + "comp06 = '" & c6 & "' and " _
            + "comp07 = '" & c7 & "' and " _
            + "comp08 = '" & c8 & "' and " _
            + "comp09 = '" & c9 & "' and " _
            + "comp10 = '" & c0 & "' and " _
            + "compid = '" & cid & "'"
            ccnt = appset.Scalar(sql)

            If ccnt > 0 Then
                Dim strMessage As String =  tmod.getmsg("cdstr197" , "WoChargeEntry.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
                appset.Dispose()
            Else

                Dim cmd As New SqlCommand
                cmd.CommandText = "exec usp_addchargenum @typi, @typ, @c1, @c2, @c3, @c4, @c5, @c6, @c7, @c8, @c9, @c0, @cid"
                Dim param0 = New SqlParameter("@typi", SqlDbType.Int)
                param0.Value = ddi
                cmd.Parameters.Add(param0)
                Dim param12 = New SqlParameter("@cid", SqlDbType.Int)
                param12.Value = cid
                cmd.Parameters.Add(param12)
                Dim param1 = New SqlParameter("@typ", SqlDbType.VarChar)
                param1.Value = ddv
                cmd.Parameters.Add(param1)
                Dim param2 = New SqlParameter("@c1", SqlDbType.VarChar)
                param2.Value = c1
                cmd.Parameters.Add(param2)
                Dim param3 = New SqlParameter("@c2", SqlDbType.VarChar)
                param3.Value = c2
                cmd.Parameters.Add(param3)
                Dim param4 = New SqlParameter("@c3", SqlDbType.VarChar)
                param4.Value = c3
                cmd.Parameters.Add(param4)
                Dim param5 = New SqlParameter("@c4", SqlDbType.VarChar)
                param5.Value = c4
                cmd.Parameters.Add(param5)
                Dim param6 = New SqlParameter("@c5", SqlDbType.VarChar)
                param6.Value = c5
                cmd.Parameters.Add(param6)
                Dim param7 = New SqlParameter("@c6", SqlDbType.VarChar)
                param7.Value = c6
                cmd.Parameters.Add(param7)
                Dim param8 = New SqlParameter("@c7", SqlDbType.VarChar)
                param8.Value = c7
                cmd.Parameters.Add(param8)
                Dim param9 = New SqlParameter("@c8", SqlDbType.VarChar)
                param9.Value = c8
                cmd.Parameters.Add(param9)
                Dim param10 = New SqlParameter("@c9", SqlDbType.VarChar)
                param10.Value = c9
                cmd.Parameters.Add(param10)
                Dim param11 = New SqlParameter("@c0", SqlDbType.VarChar)
                param11.Value = c0
                cmd.Parameters.Add(param11)
                appset.UpdateHack(cmd)
                sql = "select Count(*) from wochargeconfig " _
                + "where compid = '" & cid & "'"
                PageNumber = appset.PageCount(sql, PageSize)
                PopTasks(PageNumber)
                appset.Dispose()
            End If
        End If

    End Sub

    Private Sub dgtt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.UpdateCommand
        appset.Open()
        getvals()
        Dim txt As TextBox
        Dim ret As String
        Dim flen As Integer
        Dim cnt As Integer = lblcols.Value
        Dim c1, c2, c3, c4, c5, c6, c7, c8, c9, c0, id As String
        cid = lblcid.Value
        cid = "0"
        txt = CType(e.Item.FindControl("txtcomp01"), TextBox)
        c1 = txt.Text
        If RTrim(dt(0)) = "INT" Then
            ret = IntChk(c1)
            If ret = "no" Then
                Dim strMessage As String = ti(0) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
                Exit Sub
            Else
                flen = dl(0)
                If Len(c1) > flen Then
                    Dim strMessage As String = ti(0) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If

        txt = CType(e.Item.FindControl("txtcomp02"), TextBox)
        c2 = txt.Text
        If RTrim(dt(1)) = "INT" Then
            ret = IntChk(c2)
            If ret = "no" Then
                Dim strMessage As String = ti(1) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(1)
                If Len(c2) > flen Then
                    Dim strMessage As String = ti(1) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If

        txt = CType(e.Item.FindControl("txtcomp03"), TextBox)
        c3 = txt.Text
        If RTrim(dt(2)) = "INT" Then
            ret = IntChk(c3)
            If ret = "no" Then
                Dim strMessage As String = ti(2) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(2)
                If Len(c3) > flen Then
                    Dim strMessage As String = ti(2) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If

        txt = CType(e.Item.FindControl("txtcomp04"), TextBox)
        c4 = txt.Text
        If RTrim(dt(3)) = "INT" Then
            ret = IntChk(c4)
            If ret = "no" Then
                Dim strMessage As String = ti(3) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(3)
                If Len(c4) > flen Then
                    Dim strMessage As String = ti(3) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If

        txt = CType(e.Item.FindControl("txtcomp05"), TextBox)
        c5 = txt.Text
        If RTrim(dt(4)) = "INT" Then
            ret = IntChk(c5)
            If ret = "no" Then
                Dim strMessage As String = ti(4) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(4)
                If Len(c5) > flen Then
                    Dim strMessage As String = ti(4) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If

        txt = CType(e.Item.FindControl("txtcomp06"), TextBox)
        c6 = txt.Text
        If RTrim(dt(5)) = "INT" Then
            ret = IntChk(c6)
            If ret = "no" Then
                Dim strMessage As String = ti(5) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(5)
                If Len(c6) > flen Then
                    Dim strMessage As String = ti(5) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If

        txt = CType(e.Item.FindControl("txtcomp07"), TextBox)
        c7 = txt.Text

        If RTrim(dt(6)) = "INT" Then
            ret = IntChk(c7)
            If ret = "no" Then
                Dim strMessage As String = ti(6) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(6)
                If Len(c7) > flen Then
                    Dim strMessage As String = ti(6) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If

        txt = CType(e.Item.FindControl("txtcomp08"), TextBox)
        c8 = txt.Text
        If RTrim(dt(7)) = "INT" Then
            ret = IntChk(c8)
            If ret = "no" Then
                Dim strMessage As String = ti(7) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(7)
                If Len(c8) > flen Then
                    Dim strMessage As String = ti(7) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If

        txt = CType(e.Item.FindControl("txtcomp09"), TextBox)
        c9 = txt.Text
        If RTrim(dt(8)) = "INT" Then
            ret = IntChk(c9)
            If ret = "no" Then
                Dim strMessage As String = ti(8) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(8)
                If Len(c9) > flen Then
                    Dim strMessage As String = ti(8) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If
        'End If
        'If cnt > 9 Then
        txt = CType(e.Item.FindControl("txtcomp10"), TextBox)
        c0 = txt.Text
        If RTrim(dt(9)) = "INT" Then
            ret = IntChk(c0)
            If ret = "no" Then
                Dim strMessage As String = ti(9) & " Must Be Numeric"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                flen = dl(9)
                If Len(c0) > flen Then
                    Dim strMessage As String = ti(9) & " Is Limited To " & flen & "Characters in Length"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        End If
        id = CType(e.Item.FindControl("lblttide"), Label).Text

        Dim dd As DropDownList
        dd = CType(e.Item.FindControl("ddtype"), DropDownList)
        Dim ddi, ddv As String
        ddi = dd.SelectedIndex
        ddv = dd.SelectedValue
        Dim ccnt As Integer
        sql = "select count(*) from wocharge where " _
        + "comp01 = '" & c1 & "' and " _
        + "comp02 = '" & c2 & "' and " _
        + "comp03 = '" & c3 & "' and " _
        + "comp04 = '" & c4 & "' and " _
        + "comp05 = '" & c5 & "' and " _
        + "comp06 = '" & c6 & "' and " _
        + "comp07 = '" & c7 & "' and " _
        + "comp08 = '" & c8 & "' and " _
        + "comp09 = '" & c9 & "' and " _
        + "comp10 = '" & c0 & "' and " _
        + "compid = '" & cid & "'"
        ccnt = appset.Scalar(sql)

        If ccnt > 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr198" , "WoChargeEntry.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
            appset.Dispose()
        Else

            Dim cmd As New SqlCommand
            cmd.CommandText = "exec usp_upchargenum @typi, @typ, @c1, @c2, @c3, @c4, @c5, @c6, @c7, @c8, @c9, @c0, @cid, @id"
            Dim param0 = New SqlParameter("@typi", SqlDbType.Int)
            param0.Value = ddi
            cmd.Parameters.Add(param0)
            Dim param12 = New SqlParameter("@cid", SqlDbType.Int)
            param12.Value = cid
            cmd.Parameters.Add(param12)
            Dim param13 = New SqlParameter("@id", SqlDbType.Int)
            param13.Value = id
            cmd.Parameters.Add(param13)
            Dim param1 = New SqlParameter("@typ", SqlDbType.VarChar)
            param1.Value = ddv
            cmd.Parameters.Add(param1)
            Dim param2 = New SqlParameter("@c1", SqlDbType.VarChar)
            param2.Value = c1
            cmd.Parameters.Add(param2)
            Dim param3 = New SqlParameter("@c2", SqlDbType.VarChar)
            param3.Value = c2
            cmd.Parameters.Add(param3)
            Dim param4 = New SqlParameter("@c3", SqlDbType.VarChar)
            param4.Value = c3
            cmd.Parameters.Add(param4)
            Dim param5 = New SqlParameter("@c4", SqlDbType.VarChar)
            param5.Value = c4
            cmd.Parameters.Add(param5)
            Dim param6 = New SqlParameter("@c5", SqlDbType.VarChar)
            param6.Value = c5
            cmd.Parameters.Add(param6)
            Dim param7 = New SqlParameter("@c6", SqlDbType.VarChar)
            param7.Value = c6
            cmd.Parameters.Add(param7)
            Dim param8 = New SqlParameter("@c7", SqlDbType.VarChar)
            param8.Value = c7
            cmd.Parameters.Add(param8)
            Dim param9 = New SqlParameter("@c8", SqlDbType.VarChar)
            param9.Value = c8
            cmd.Parameters.Add(param9)
            Dim param10 = New SqlParameter("@c9", SqlDbType.VarChar)
            param10.Value = c9
            cmd.Parameters.Add(param10)
            Dim param11 = New SqlParameter("@c0", SqlDbType.VarChar)
            param11.Value = c0
            cmd.Parameters.Add(param11)

            Dim test As String = cmd.CommandText
            appset.UpdateHack(cmd)
            sql = "select Count(*) from wochargeconfig " _
            + "where compid = '" & cid & "'"
            PageNumber = appset.PageCount(sql, PageSize)
            dgtt.EditItemIndex = -1
            PopTasks(PageNumber)
            appset.Dispose()
        End If
    End Sub

    Private Sub dgtt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.DeleteCommand
        Dim id As String
        cid = lblcid.Value
        cid = "0"
        Try
            id = CType(e.Item.FindControl("lblttide"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblttids"), Label).Text
        End Try
        sql = "usp_delcharge '" & id & "', '" & cid & "'"
        appset.Open()
        appset.Update(sql)
        PageNumber = appset.PageCount(sql, PageSize)
        dgtt.EditItemIndex = -1
        If dgtt.CurrentPageIndex > PageNumber Then
            dgtt.CurrentPageIndex = PageNumber - 1
        End If
        If dgtt.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgtt.CurrentPageIndex + 1
        End If
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

   



	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtt.Columns(0).HeaderText = dlabs.GetDGPage("WoChargeEntry.aspx", "dgtt", "0")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(1).HeaderText = dlabs.GetDGPage("WoChargeEntry.aspx", "dgtt", "1")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(2).HeaderText = dlabs.GetDGPage("WoChargeEntry.aspx", "dgtt", "2")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(13).HeaderText = dlabs.GetDGPage("WoChargeEntry.aspx", "dgtt", "13")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang194.Text = axlabs.GetASPXPage("WoChargeEntry.aspx", "lang194")
        Catch ex As Exception
        End Try
        Try
            lang195.Text = axlabs.GetASPXPage("WoChargeEntry.aspx", "lang195")
        Catch ex As Exception
        End Try

    End Sub

End Class
