

'********************************************************
'*
'********************************************************




''copyright
Public Class admindialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim list, typ, wo, ro As String
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifinv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    'pgtitle.InnerHtml = "Admin Dialog (Read Only)"
                Else
                    'pgtitle.InnerHtml = "Admin Dialog"
                End If
            Catch ex As Exception

            End Try
            list = Request.QueryString("list").ToString
            typ = Request.QueryString("typ").ToString
            If list = "comp" Then
                ifinv.Attributes.Add("src", "complook.aspx?typ=" & typ)
            ElseIf list = "cont" Then
                If typ = "wo" Then
                    wo = Request.QueryString("wo").ToString
                    ifinv.Attributes.Add("src", "servicecontract.aspx?typ=" & typ & "&wo=" & wo)
                Else
                    ifinv.Attributes.Add("src", "servicecontract.aspx?typ=" & typ)
                End If
            Else
                ifinv.Attributes.Add("src", "eqlookup.aspx?typ=" & typ)
            End If

        End If
    End Sub

End Class
