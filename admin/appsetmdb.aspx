﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="appsetmdb.aspx.vb" Inherits="lucy_r12.appsetmdb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" src="../scripts/gridnav.js" type="text/javascript"></script>
    <script language="JavaScript" src="../scripts/overlib1.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td width="80">
                </td>
                <td width="200">
                </td>
                <td width="20">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblcom" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"
                        ForeColor="Red"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td colspan="3" align="center">
                    <asp:DataGrid ID="dgfail" runat="server" ShowFooter="True" CellPadding="0" GridLines="None"
                        AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1"
                        BackColor="transparent">
                        <FooterStyle BackColor="transparent"></FooterStyle>
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="50px" Height="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="Imagebutton10" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        ToolTip="Edit This Failure Mode" CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                        CommandName="Add"></asp:ImageButton>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="Imagebutton11" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton15" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="True" HeaderText="Server">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.srvr") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="txtsrv" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.srvr") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewsrv" runat="server" MaxLength="50">
                                    </asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Database">
                                <HeaderStyle Width="156px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dbname") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewdb" runat="server" MaxLength="50">
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:TextBox ID="txtmdb" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.dbname") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Title">
                                <HeaderStyle Width="156px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="Label14a" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.title") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewtit" runat="server" MaxLength="50">
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:TextBox ID="txttit" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.title") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Application">
                                <HeaderStyle Width="156px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="Label14b" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.apname") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewap" runat="server" MaxLength="50">
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:TextBox ID="txtap" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.apname") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Remove">
                                <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="Imagebutton17" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="Label13a" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mdid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Textbox9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mdid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
            

        </table>
        <input id="lblcid" type="hidden" runat="server" />
        <input id="appchk" type="hidden" name="appchk" runat="server" />
        <input id="lblsid" type="hidden" runat="server" />
        <input id="lbllog" type="hidden" runat="server" />
        <input type="hidden" id="lblofm" runat="server" />
        <input type="hidden" id="txtpgcnt" runat="server" name="txtpgcnt" />
        <input type="hidden" id="lblret" runat="server" name="lblret" />
        <input type="hidden" id="lblro" runat="server" />
    </div>
    </form>
</body>
</html>
