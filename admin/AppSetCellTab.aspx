<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetCellTab.aspx.vb"
    Inherits="lucy_r12.AppSetCellTab" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetCellTab</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetCellTabaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="handleapp();checkit();">
    <form id="form1" method="post" runat="server">
    <table cellspacing="0" cellpadding="0" width="550">
        <tr>
            <td width="100">
            </td>
            <td width="180">
            </td>
            <td width="270">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="Label12" runat="server" Width="80px" Font-Bold="True" Font-Names="Arial"
                    Font-Size="X-Small" CssClass="label">Level1</asp:Label><asp:DropDownList ID="lstcelldepts"
                        runat="server" AutoPostBack="True">
                    </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblcellmsg" runat="server" Width="352px" Font-Bold="True" Font-Names="Arial"
                    Font-Size="Small" ForeColor="Red">Waiting For Department Selection...</asp:Label>
            </td>
        </tr>
        <tr id="trsearchrow" runat="server">
            <td class="label">
                <asp:Label ID="lang39" runat="server">Search Cells</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgcells" runat="server" CellPadding="0" GridLines="None" AllowPaging="True"
                    AllowCustomPaging="True" AutoGenerateColumns="False" ShowFooter="True" CellSpacing="1"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <HeaderStyle Height="22px"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton5" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    ToolTip="Save Changes" CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    ToolTip="Cancel Changes" CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Cell">
                            <HeaderStyle Width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Width="230px" Text='<%# DataBinder.Eval(Container, "DataItem.Cell_Name") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewl2name" runat="server" MaxLength="50" Width="230px" Text='<%# DataBinder.Eval(Container, "DataItem.Cell_Name") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="Textbox4" runat="server" Width="230px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.Cell_Name") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Description">
                            <HeaderStyle Width="240px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Width="230px" Text='<%# DataBinder.Eval(Container, "DataItem.Cell_Desc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewl2desc" runat="server" MaxLength="100" Width="230px"></asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" MaxLength="100" Width="230px" Text='<%# DataBinder.Eval(Container, "DataItem.Cell_Desc") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblcellidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cellid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblcellid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cellid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton16" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                        ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr id="trnavrow" runat="server">
            <td colspan="3" align="center">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" runat="server"><input id="lblsid" type="hidden"
        runat="server">
    <input id="lbldid" type="hidden" runat="server"><input type="hidden" id="appchk"
        runat="server" name="appchk">
    <input type="hidden" id="lbllog" runat="server"><input type="hidden" id="txtpg" runat="server"
        name="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" name="txtpgcnt">
    <input type="hidden" id="lblret" runat="server" name="lblret"><input type="hidden"
        id="lblold" runat="server" name="lblold">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
