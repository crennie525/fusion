<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetpmAppr.aspx.vb"
    Inherits="lucy_r12.AppSetpmAppr" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PM Assurance</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="javascript" type="text/javascript" src="../scripts/pmtaskdivfunc_1016b.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetpmAppraspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="startup();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table width="980" style="z-index: 1; left: 7px; position: absolute; top: 80px">
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td class="label" width="120">
                            <asp:Label ID="lang90" runat="server">Selected Revision</asp:Label>
                        </td>
                        <td width="100">
                            <asp:DropDownList ID="ddrevs" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="label" width="90">
                            <asp:Label ID="lang91" runat="server">Last Approved</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdapprdate" runat="server" width="100">
                        </td>
                        <td class="label" width="80">
                            <asp:Label ID="lang92" runat="server">Approved By</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdapprby" runat="server" width="170">
                        </td>
                        <td class="label" width="70">
                            <asp:Label ID="lang93" runat="server">Is Current?</asp:Label>
                        </td>
                        <td class="plainlabel" id="tdcurr" runat="server" width="140">
                        </td>
                        <td width="20">
                            <img id="imgadd" runat="server" alt="" src="../images/appbuttons/minibuttons/addnew.gif"
                                onmouseover="return overlib('Add a New Revision from scratch', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="addn();">
                        </td>
                        <td width="20">
                            <img id="imgcopy" runat="server" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                onmouseover="return overlib('Add a New Revision using this Revision as a Template', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="addc();">
                        </td>
                        <td width="20">
                            <img id="imgdel" runat="server" alt="" src="../images/appbuttons/minibuttons/del.gif"
                                onmouseover="return overlib('Delete this Revision', ABOVE, LEFT)" onmouseout="return nd()"
                                onclick="delchk();">
                        </td>
                        <td width="20">
                            <img id="imgcheck" runat="server" alt="" src="../images/appbuttons/minibuttons/checked.gif"
                                onmouseover="return overlib('Approve or update the Approval Date for this Revision', ABOVE, LEFT)"
                                onmouseout="return nd()" onclick="apprme();">
                        </td>
                        <td width="20">
                            <img id="imgcomp" runat="server" alt="" src="../images/appbuttons/minibuttons/comp.gif"
                                onmouseover="return overlib('Make this the Current Revision', ABOVE, LEFT)" onmouseout="return nd()"
                                onclick="currme();">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="thdrsingrt label" width="445">
                <asp:Label ID="lang94" runat="server">PM Assurance Phases Setup</asp:Label>
            </td>
            <td width="5">
                &nbsp;
            </td>
            <td class="thdrsingrt label" width="530">
                <asp:Label ID="lang95" runat="server">PM Assurance Checklist Setup</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 444px" valign="top">
                <iframe id="ifphase" style="width: 440px; height: 300px; background-color: transparent"
                    src="AppSetApprPhases.aspx?jump=no&amp;ans=0&amp;cnt=0&amp;rev=0" frameborder="no"
                    runat="server" allowtransparency></iframe>
            </td>
            <td>
            </td>
            <td valign="top" rowspan="4">
                <iframe id="ifplan" style="width: 525px; height: 480px; background-color: transparent"
                    src="AppSetApprPlan.aspx?jump=no&amp;ans=0&amp;cnt=0&amp;rev=0" frameborder="no"
                    runat="server" allowtransparency></iframe>
            </td>
        </tr>
        <tr height="20">
            <td class="thdrsingrt label">
                <asp:Label ID="lang96" runat="server">PM Assurance Approval Groups Setup</asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 444px" valign="top">
                <iframe id="ifgrps" style="width: 440px; height: 170px; background-color: transparent"
                    align="top" src="AppSetpmApprGrps.aspx?rev=0" frameborder="no" runat="server"
                    allowtransparency></iframe>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <input id="lblapprflg" type="hidden" runat="server"><input id="lblans" type="hidden"
        runat="server">
    <input id="lbleqcnt" type="hidden" runat="server">
    <input id="lblrev" type="hidden" runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
</html>
