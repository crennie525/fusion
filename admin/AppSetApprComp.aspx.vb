

'********************************************************
'*
'********************************************************




Public Class AppSetApprComp
    Inherits System.Web.UI.Page
	Protected WithEvents lang17 As System.Web.UI.WebControls.Label

	Protected WithEvents lang16 As System.Web.UI.WebControls.Label

	Protected WithEvents lang15 As System.Web.UI.WebControls.Label

	Protected WithEvents lang14 As System.Web.UI.WebControls.Label

	Protected WithEvents lang13 As System.Web.UI.WebControls.Label

	Protected WithEvents lang12 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pp As New Utilities
    Dim rev, ro As String
    Protected WithEvents byes As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents bno As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents LabelMR As System.Web.UI.WebControls.Label
    Protected WithEvents LabelMR2 As System.Web.UI.WebControls.Label
    Dim orev As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrevchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnew As System.Web.UI.WebControls.Label
    Protected WithEvents lblold As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        rev = Request.QueryString("rev").ToString
        lblnew.Text = rev
        pp.Open()
        orev = pp.Rev()
        pp.Dispose()
        lblold.Text = orev
        lblchk.Value = "no"
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            LabelMR.Text = axlabs.GetASPXPage("AppSetApprComp.aspx", "LabelMR")
        Catch ex As Exception
        End Try
        Try
            LabelMR2.Text = axlabs.GetASPXPage("AppSetApprComp.aspx", "LabelMR2")
        Catch ex As Exception
        End Try
        Try
            lang12.Text = axlabs.GetASPXPage("AppSetApprComp.aspx", "lang12")
        Catch ex As Exception
        End Try
        Try
            lang13.Text = axlabs.GetASPXPage("AppSetApprComp.aspx", "lang13")
        Catch ex As Exception
        End Try
        Try
            lang14.Text = axlabs.GetASPXPage("AppSetApprComp.aspx", "lang14")
        Catch ex As Exception
        End Try
        Try
            lang15.Text = axlabs.GetASPXPage("AppSetApprComp.aspx", "lang15")
        Catch ex As Exception
        End Try
        Try
            lang16.Text = axlabs.GetASPXPage("AppSetApprComp.aspx", "lang16")
        Catch ex As Exception
        End Try
        Try
            lang17.Text = axlabs.GetASPXPage("AppSetApprComp.aspx", "lang17")
        Catch ex As Exception
        End Try

    End Sub

End Class
