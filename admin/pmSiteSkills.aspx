<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmSiteSkills.aspx.vb"
    Inherits="lucy_r12.pmSiteSkills" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>pmSiteSkills</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmSiteSkillsaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table width="422">
        <tr>
            <td class="thdrsingrt label" colspan="3">
                <asp:Label ID="lang177" runat="server">Add/Edit Site Skills Mini Dialog</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" width="172" height="26">
                <asp:Label ID="lang178" runat="server">Current Plant Site:</asp:Label>
            </td>
            <td class="labellt" id="tdsite" width="250" height="26" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border-bottom: #0000ff 1px solid; border-left: #0000ff 1px solid; border-top: #0000ff 1px solid;
                    border-right: #0000ff 1px solid">
            </td>
        </tr>
        <tr id="trmsg" runat="server" class="details">
            <td colspan="2" align="center">
                <asp:CheckBox ID="cbprep" runat="server" CssClass="redlabel" AutoPostBack="True"
                    Text="Use Site Skills?"></asp:CheckBox>
            </td>
        </tr>
        <tr id="trmsg2" runat="server" class="details">
            <td colspan="2" align="center">
                <asp:CheckBox ID="cbundo" runat="server" CssClass="redlabel" AutoPostBack="True"
                    Text="Stop Using Site Skills?"></asp:CheckBox>
            </td>
        </tr>
    </table>
    <table width="422">
        <tr>
            <td class="label" align="center">
                <asp:Label ID="lang179" runat="server">Available Craft/Skills</asp:Label>
            </td>
            <td>
            </td>
            <td class="label" align="center">
                <asp:Label ID="lang180" runat="server">Site Craft/Skills</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" width="200">
                <asp:ListBox ID="lbskillmaster" runat="server" Width="170px" SelectionMode="Multiple"
                    Height="150px"></asp:ListBox>
            </td>
            <td valign="middle" align="center" width="22">
                <br>
                <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                </asp:ImageButton><asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                </asp:ImageButton>
            </td>
            <td align="center" width="200">
                <asp:ListBox ID="lbsiteskills" runat="server" Width="170px" SelectionMode="Multiple"
                    Height="150px"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">
                <img id="btnreturn" runat='server' onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
                    width="69" height="19">
            </td>
        </tr>
        <tr>
            <td class="bluelabel" colspan="3">
                <asp:Label ID="lang181" runat="server">List Box Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" colspan="3">
                <asp:RadioButtonList ID="cbopts" runat="server" Width="400px" AutoPostBack="True"
                    CssClass="labellt">
                    <asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
                    <asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="note" colspan="3">
                <asp:Label ID="lang182" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Site Craft/Skills list if only one item is present.</asp:Label>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" runat="server" name="lblcid">
    <input id="lblsid" type="hidden" runat="server" name="lblsid">
    <input id="lblfuid" type="hidden" runat="server" name="lblfuid">
    <input id="lblcoid" type="hidden" runat="server" name="lblcoid">
    <input id="lblco" type="hidden" runat="server" name="lblco"><input id="lblapp" type="hidden"
        runat="server" name="lblapp">
    <input type="hidden" id="lblopt" runat="server" name="lblopt">
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="lblsitecnt" runat="server"><input type="hidden" id="lblcurrcnt"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
