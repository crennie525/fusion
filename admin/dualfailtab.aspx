﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dualfailtab.aspx.vb" Inherits="lucy_r12.dualfailtab" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetComTabaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" style="position: absolute; top: 10px" method="post" runat="server">
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td width="80">
            </td>
            <td width="200">
            </td>
            <td width="20">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblcom" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"
                    ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="bluelabel">
                <asp:Label ID="lang40" runat="server">Search</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
                    CssClass="imgbutton"></asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:DataGrid ID="dgfail" runat="server" ShowFooter="True" CellPadding="0" GridLines="None"
                    AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="50px" Height="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton10" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit This Failure Mode" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                &nbsp;
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton11" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton15" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Failure Mode">
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.failid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Textbox9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.failid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="English">
                            <HeaderStyle Width="200px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.failuremode") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewfm" runat="server" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.failid") %>'
                                    NAME="Textbox1" Width="200px">
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox10" runat="server" Width="200px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.failuremode") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="French">
                            <HeaderStyle Width="200px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label14f" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fmeng") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewfmf" runat="server" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.failid") %>'
                                    NAME="Textbox1" Width="200px">
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox11" runat="server" Width="200px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.fmeng") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ISO ID">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label14i" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isoid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewfmi" runat="server" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.failid") %>'
                                    NAME="Textbox1" Width="100px">
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox10a" runat="server" Width="100px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.isoid") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="Imagebutton17" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                &nbsp;
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" runat="server" />
    <input id="appchk" type="hidden" name="appchk" runat="server" />
    <input id="lblsid" type="hidden" runat="server" />
    <input id="lbllog" type="hidden" runat="server" />
    <input type="hidden" id="lblofm" runat="server" />
    <input type="hidden" id="lblofmf" runat="server" />
    <input type="hidden" id="txtpg" runat="server" name="Hidden1" />
    <input type="hidden" id="txtpgcnt" runat="server" name="txtpgcnt" />
    <input type="hidden" id="lblret" runat="server" name="lblret" />
    <input type="hidden" id="lblro" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
