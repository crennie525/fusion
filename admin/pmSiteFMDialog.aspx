<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pmSiteFMDialog.aspx.vb"
    Inherits="lucy_r12.pmSiteFMDialog" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title runat="server" id="pgtitle">pmSiteFMDialog</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/sessrefdialog.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/pmSiteFMDialogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onunload="handleexit();" onload="resetsess();pageScroll();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifss" runat="server" src="" width="100%" height="100%" frameborder="no">
    </iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblsessrefresh" runat="server" name="lblsessrefresh">
    <input id="lblcid" type="hidden" runat="server" name="lblcid">
    <input id="lblsid" type="hidden" runat="server" name="lblsid">
    <input id="lblfuid" type="hidden" runat="server" name="lblfuid">
    <input id="lblcoid" type="hidden" runat="server" name="lblcoid">
    <input id="lblco" type="hidden" runat="server" name="lblco"><input id="lblapp" type="hidden"
        runat="server" name="lblapp">
    <input type="hidden" id="lblopt" runat="server" name="lblopt">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
