<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetTaskTab.aspx.vb"
    Inherits="lucy_r12.AppSetTaskTab" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetTaskTab</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <style type="text/css">
        .details
        {
            display: none;
            visibility: hidden;
            font-family: Verdana;
            background-color: white;
        }
    </style>
    <script language="javascript" type="text/javascript" src="../scripts/tasktab.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetTaskTabaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checktab();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div id="ttdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 5px; visibility: visible; border-left: 2px groove; width: 750px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 360px; background-color: transparent">
        <iframe id="iftt" style="width: 750px; height: 360px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="ptdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 750px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 360px; background-color: transparent">
        <iframe id="ifpt" style="width: 750px; height: 360px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="stdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 750px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 360px; background-color: transparent">
        <iframe id="ifst" style="width: 750px; height: 360px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="etdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 750px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 360px; background-color: transparent">
        <iframe id="ifet" style="width: 750px; height: 360px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="frdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 750px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 360px; background-color: transparent">
        <iframe id="iffr" style="width: 750px; height: 360px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="pmadj" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 750px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 360px; background-color: transparent">
        <iframe id="ifpmadj" style="width: 750px; height: 360px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="tpmadj" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 750px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 360px; background-color: transparent">
        <iframe id="iftpmadj" style="width: 750px; height: 360px" src="" frameborder="no"
            runat="server"></iframe>
    </div>
    <table style="left: 5px; position: absolute; top: 10px" cellspacing="3" cellpadding="3">
        <tr height="20">
            <td id="tt" onclick="gettab('tt');" class="thdrhov plainlabel">
                <asp:Label ID="lang102" runat="server">Task Types</asp:Label>
            </td>
            <td class="details" id="fr" onclick="gettab('fr');">
            </td>
            <td id="pt" onclick="gettab('pt');" class="thdr plainlabel">
                <asp:Label ID="lang103" runat="server">PdM Tech</asp:Label>
            </td>
            <td id="st" onclick="gettab('st');" class="thdr plainlabel">
                <asp:Label ID="lang104" runat="server">Skill Types</asp:Label>
            </td>
            <td id="et" onclick="gettab('et');" class="thdr plainlabel">
                <asp:Label ID="lang105" runat="server">PM Eq Status</asp:Label>
            </td>
            <td id="pcnt" onclick="gettab('pa');" class="thdr plainlabel">
                <asp:Label ID="lang106" runat="server">PM Pass/Fail Adj</asp:Label>
            </td>
            <td id="tpcnt" onclick="gettab('tpa');" class="thdr plainlabel">
                <asp:Label ID="lang107" runat="server">TPM Pass/Fail Adj</asp:Label>
            </td>
            <td id="tpmcnt" onclick="gettpm();" class="thdr plainlabel">
                <asp:Label ID="lang108" runat="server">TPM Alert</asp:Label>
            </td>
        </tr>
    </table>
    <input type="hidden" id="txttab" runat="server" name="appchk">
    <input type="hidden" id="lbltab" runat="server" name="appchk">
    <input type="hidden" id="lblcid" runat="server" name="appchk">
    <input type="hidden" id="lbltl" runat="server" name="appchk">
    <input type="hidden" id="appchk" runat="server" name="appchk">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
</html>
