

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetComTab
    Inherits System.Web.UI.Page
    Protected WithEvents btnaddtosite As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang40 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, sid, did, ro As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim dr As SqlDataReader
    Dim appset As New Utilities
    Protected WithEvents lblcom As System.Web.UI.WebControls.Label
    Protected WithEvents dgfail As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Sort As String
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblofm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()

        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                dgfail.Columns(0).Visible = False
                dgfail.Columns(3).Visible = False
            End If
            Try
                cid = "0"
                lblsid.Value = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try

            If cid <> "" Then
                lblcid.Value = cid
                cid = lblcid.Value
                appset.Open()
                PopFail(PageNumber)
                appset.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopFail(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopFail(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        PageNumber = 1
        appset.Open()
        PopFail(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopFail(ByVal PageNumber As Integer)
        'Try
        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        cid = lblcid.Value
        If Len(srch) > 0 Then
            Filter = "failuremode like ''%" & srch & "%'' and compid = ''" & cid & "''"
            FilterCnt = "failuremode like '%" & srch & "%' and compid = '" & cid & "'"
        Else
            Filter = "compid = ''" & cid & "''"
            FilterCnt = "compid = '" & cid & "'"
        End If
        sql = "select count(*) " _
              + "from FailureModes where " & FilterCnt 'compid = '" & cid & "'"

        dgfail.VirtualItemCount = appset.Scalar(sql)
        If dgfail.VirtualItemCount = 0 Then
            lblcom.Text = "No Failure Mode Records Found"
            Fields = "*"
            Tables = "FailureModes"
            PK = "failid"
            PageSize = "10"
            Sort = "failuremode"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgfail.DataSource = dr
            Try
                dgfail.DataBind()
            Catch ex As Exception
                dgfail.CurrentPageIndex = 0
                dgfail.DataBind()
            End Try
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgfail.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgfail.PageCount
        Else
            Fields = "*"
            Tables = "FailureModes"
            PK = "failid"
            PageSize = "10"
            Sort = "failuremode"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgfail.DataSource = dr
            Try
                dgfail.DataBind()
            Catch ex As Exception
                dgfail.CurrentPageIndex = 0
                dgfail.DataBind()
            End Try
            dr.Close()
            lblcom.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgfail.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgfail.PageCount
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopFail(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr20", "AppSetComTab.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopFail(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr21", "AppSetComTab.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgfail_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.EditCommand
        PageNumber = txtpg.Value
        lblofm.Value = CType(e.Item.FindControl("Label14"), Label).Text
        dgfail.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopFail(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgfail_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.UpdateCommand
        appset.Open()
        Dim id, desc As String
        cid = lblcid.Value
        id = CType(e.Item.FindControl("TextBox9"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        Dim ofm As String = lblofm.Value

        Dim faillo As String = ofm.ToLower
        Dim faill As String = desc.ToLower
        faill = RTrim(faill)
        desc = RTrim(desc)
        If faillo <> faill Then
            sql = "select count(*) from FailureModes where rtrim(lower(failuremode)) = '" & faill & "' and compid = '" & cid & "'"
            Dim strchk As Integer
            strchk = appset.Scalar(sql)
            If strchk = 0 Then
                sql = "update FailureModes set failuremode = " _
                       + "'" & desc & "' where failid = '" & id & "'"
                appset.Update(sql)
                sql = "exec usp_update_comp_class_fm '" & id & "'"
                appset.Update(sql)
            Else
                Dim strMessage As String = tmod.getmsg("cdstr22", "AppSetComTab.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'appset.Dispose()
            End If
        End If
        'appset.Dispose()
        dgfail.EditItemIndex = -1
        PopFail(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgfail_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.CancelCommand
        dgfail.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopFail(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgfail_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            'id = CType(e.Item.Cells(1).Controls(1), Label).Text
            id = CType(e.Item.FindControl("Label13"), Label).Text
        Catch ex As Exception
            'id = CType(e.Item.Cells(1).Controls(2), TextBox).Text
            id = CType(e.Item.FindControl("Textbox9"), TextBox).Text
        End Try

        Dim fcnt As Integer
        sql = "select count(*) from pmtaskfailmodes where parentfailid = '" & id & "'"
        fcnt = appset.Scalar(sql)
        sql = "select count(*) from pmtaskfailmodestpm where parentfailid = '" & id & "'"
        fcnt += appset.Scalar(sql)
        If fcnt = 0 Then
            sql = "delete from FailureModes where failid = '" & id & "'"
            appset.Update(sql)
            cid = lblcid.Value
            Dim failcnt As Integer = dgfail.VirtualItemCount - 1
            sql = "update AdminTasks set " _
            + "fail = '" & failcnt & "' where cid = '" & cid & "'"
            appset.Update(sql)
            sql = "delete from compClassSubFM where failid = '" & id & "'"
            appset.Update(sql)
            sql = "exec usp_update_comp_class_fm '" & id & "'"
            appset.Update(sql)

        Else
            Dim strMessage As String = tmod.getmsg("cdstr23", "AppSetComTab.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        sql = "select Count(*) from FailureModes " _
            + "where compid = '" & cid & "'"
        PageNumber = appset.PageCount(sql, PageSize)
        'appset.Dispose()
        dgfail.EditItemIndex = -1
        If dgfail.CurrentPageIndex > PageNumber Then
            dgfail.CurrentPageIndex = PageNumber - 1
        End If
        If dgfail.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgfail.CurrentPageIndex + 1
        End If

        PopFail(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgfail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfail.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewfm"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            If lname <> "" Then
                If Len(lname) > 50 Then
                    Dim strMessage As String = tmod.getmsg("cdstr24", "AppSetComTab.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    Dim fail As String = lname
                    cid = lblcid.Value
                    'sid = lblsid.Value
                    Dim strchk As Integer
                    Dim faill As String = fail.ToLower
                    faill = RTrim(faill)
                    lname = RTrim(lname)
                    sql = "select count(*) from FailureModes where rtrim(lower(failuremode)) = '" & faill & "' and compid = '" & cid & "'"
                    strchk = appset.Scalar(sql)
                    If strchk = 0 Then
                        sql = "insert into FailureModes " _
                        + "(compid, failuremode, fmeng) values ('" & cid & "', '" & lname & "','" & lname & "')"
                        appset.Update(sql)
                        Dim failcnt As Integer = dgfail.VirtualItemCount + 1
                        dgfail.EditItemIndex = -1
                        sql = "select Count(*) from FailureModes " _
                                + "where compid = '" & cid & "'"
                        PageNumber = 1 'appset.PageCount(sql, PageSize)
                        PopFail(PageNumber)
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr25", "AppSetComTab.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        appset.Dispose()
                    End If
                    appset.Dispose()
                End If
            End If
        End If
    End Sub






    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgfail.Columns(0).HeaderText = dlabs.GetDGPage("AppSetComTab.aspx", "dgfail", "0")
        Catch ex As Exception
        End Try
        Try
            dgfail.Columns(2).HeaderText = dlabs.GetDGPage("AppSetComTab.aspx", "dgfail", "2")
        Catch ex As Exception
        End Try
        Try
            dgfail.Columns(3).HeaderText = dlabs.GetDGPage("AppSetComTab.aspx", "dgfail", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang40.Text = axlabs.GetASPXPage("AppSetComTab.aspx", "lang40")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddtosite.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("AppSetComTab.aspx", "btnaddtosite") & "')")
            btnaddtosite.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
