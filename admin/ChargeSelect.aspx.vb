

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class ChargeSelect
    Inherits System.Web.UI.Page
	Protected WithEvents lang135 As System.Web.UI.WebControls.Label

	Protected WithEvents lang134 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, supid, name, srch, cid, typ, skill, who, wo As String
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Dim Sort As String = ""
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents trtype As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents chargediv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ttPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ttNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwonum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblval As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim ch As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpost As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
                wo = Request.QueryString("wo").ToString
                lblwho.Value = who
                lblwonum.Value = wo
            Catch ex As Exception

            End Try
            ch.Open()
            LoadType()
            PopTasks(PageNumber)
            ch.Dispose()
            ttNext.Attributes.Add("onclick", "getnext();")
            ttPrev.Attributes.Add("onclick", "getprev();")
        Else
            If Request.Form("lblsubmit") = "upwo" Then
                lblsubmit.Value = ""
                ch.Open()
                upwo()
                ch.Dispose()
            End If
            Dim tst As String = Request.Form("lblret")
            If Request.Form("lblret") = "next" Then
                ch.Open()
                GetNext()
                ch.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                ch.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                ch.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                ch.Open()
                GetPrev()
                ch.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                ch.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                ch.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            ch.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr178", "eqlookup.aspx.vb")

            ch.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            ch.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr179", "eqlookup.aspx.vb")

            ch.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub upwo()
        wo = lblwonum.Value
        Dim val As String = lblval.Value
        sql = "update workorder set chargenum = '" & val & "' where wonum = '" & wo & "'"
        ch.Update(sql)
        lblret.Value = "go"
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        Dim fldcnt As Integer
        Dim filt, filtd As String
        Try
            ch.Open()
        Catch ex As Exception

        End Try
        cid = "0" 'lblcid.Value

        If txtsrch.Text <> "" Then
            filt = lblfilt.Value
            Filter = filt
        Else
            Filter = "compid = " & cid
        End If
        sql = "select count(*) " _
        + "from wocharge where " & Filter
        fldcnt = ch.Scalar(sql)
        fldcnt = ch.PageCount100(fldcnt, 100)

        If txtsrch.Text <> "" Then
            filtd = lblfiltd.Value
            Filter = filtd
        Else
            Filter = "compid = ''" & cid & "''"
        End If
        Tables = "wocharge"
        PK = "wochid"
        PageSize = "100"
        dr = ch.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        While dr.Read
            name = dr.Item("wocharge").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getcharge('" & name & "');"">" & name & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        chargediv.InnerHtml = sb.ToString
        txtpg.Value = PageNumber
        txtpgcnt.Value = fldcnt
        If fldcnt = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & fldcnt
        End If

        lblpgcnt.Value = fldcnt
        ttPrev.Visible = True
        ttNext.Visible = True

    End Sub
    Private Sub LoadCharge(ByVal filt As String)
        txtpg.Value = "1"
        txtpgcnt.Value = "1"

        srch = txtsrch.Text
        If Len(srch) > 0 Then
            srch = ch.ModString2(srch)
            sql = "select wocharge from wocharge where wocharge like '%" & srch & "%'"
        Else
            sql = "select wocharge from wocharge"
        End If
        sql = "select wocharge from wocharge where " & filt
        Dim pcnt As Integer = 0
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        dr = ch.GetRdrData(sql)
        While dr.Read
            pcnt += 1
            name = dr.Item("wocharge").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getcharge('" & name & "');"">" & name & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        chargediv.InnerHtml = sb.ToString
        If pcnt = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page 1 of 1"
        End If
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        'Dim srch As String = txtsrch.Text
        'Dim filt, filtd As String
        cid = "0" 'lblcid.Value
        
        Dim srch As String = txtsrch.Text
        srch = ch.ModString2(srch)
        Dim sel As String = ddtype.SelectedValue
        Dim filt, filtd As String
        If ddtype.SelectedIndex <> 0 Then
            cid = "0" 'lblcid.Value
            If Len(srch) <> 0 Then
                filt = "compid = " & cid & " and wocharge like '%" & srch & "%' and wochargetype = '" & sel & "'"
                filtd = "compid = " & cid & " and wocharge like ''%" & srch & "%'' and wochargetype = ''" & sel & "''"
            Else
                filt = "compid = " & cid & " and wochargetype = '" & sel & "'"
                filtd = "compid = " & cid & " and wochargetype = ''" & sel & "''"
            End If
        Else
            If Len(srch) <> 0 Then
                filt = "compid = " & cid & " and wocharge like '%" & srch & "%'"
                filtd = "compid = " & cid & " and wocharge like ''%" & srch & "%''"
            Else
                Exit Sub
            End If
        End If
            lblfilt.Value = filt
            lblfiltd.Value = filtd
            ch.Open()
            LoadCharge(filt)
            ch.Dispose()
    End Sub
    Private Sub LoadType()
        cid = "0"
        sql = "select wochargetype " _
        + "from wochargetype where compid = '" & cid & "' and active = 1"
        dr = ch.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataTextField = "wochargetype"
        ddtype.DataValueField = "wochargetype"
        Try
            ddtype.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0

    End Sub

    Private Sub ddtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddtype.SelectedIndexChanged
        Dim srch As String = txtsrch.Text
        Dim sel As String = ddtype.SelectedValue
        Dim filt, filtd As String
        If ddtype.SelectedIndex <> 0 Then
            cid = "0" 'lblcid.Value
            If Len(srch) <> 0 Then
                filt = "compid = " & cid & " and wocharge like '%" & srch & "%' and wochargetype = '" & sel & "'"
                filtd = "compid = " & cid & " and wocharge like ''%" & srch & "%'' and wochargetype = ''" & sel & "''"
            Else
                filt = "compid = " & cid & " and wochargetype = '" & sel & "'"
                filtd = "compid = " & cid & " and wochargetype = ''" & sel & "''"
            End If
            lblfilt.Value = filt
            lblfiltd.Value = filtd
            ch.Open()
            LoadCharge(filt)
            ch.Dispose()
        End If
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang134.Text = axlabs.GetASPXPage("ChargeSelect.aspx", "lang134")
        Catch ex As Exception
        End Try
        Try
            lang135.Text = axlabs.GetASPXPage("ChargeSelect.aspx", "lang135")
        Catch ex As Exception
        End Try

    End Sub

End Class
