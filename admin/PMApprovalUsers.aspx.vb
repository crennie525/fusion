

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class PMApprovalUsers
    Inherits System.Web.UI.Page
	Protected WithEvents lang170 As System.Web.UI.WebControls.Label

	Protected WithEvents lang169 As System.Web.UI.WebControls.Label

	Protected WithEvents lang168 As System.Web.UI.WebControls.Label

	Protected WithEvents lang167 As System.Web.UI.WebControls.Label

	Protected WithEvents lang166 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim lab As New Utilities
    Dim sql, aid, rev As String
    Dim dr As SqlDataReader
    Dim admin, fid, Filter, ro As String 'temp
    Protected WithEvents tdgrp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdrev As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdadd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblaid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntolist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromlist As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbselect As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdforum As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                If ro = "1" Then
                    'cbopts.Enabled = False
                    btntolist.Visible = False
                    btnfromlist.Visible = False
                    todis.Attributes.Add("class", "view")
                    fromdis.Attributes.Add("class", "view")
                End If
            End If
            aid = Request.QueryString("id").ToString
            lblaid.Value = aid
            rev = Request.QueryString("rev").ToString
            lblrev.Value = rev
            tdrev.InnerHtml = rev
            lab.Open()
            GetGrp()
            GetLabor()
            GetSelect()
            lab.Dispose()

        End If

    End Sub
    Private Sub GetGrp()
        aid = lblaid.Value
        sql = "select grpname, addrem from pmApprovalGrps where apprgrp = '" & aid & "'"
        dr = lab.GetRdrData(sql)
        While dr.Read
            tdgrp.InnerHtml = dr.Item("grpname").ToString
            tdadd.InnerHtml = dr.Item("addrem").ToString
        End While
        dr.Close()
    End Sub
    Private Sub GetLabor()
        aid = lblaid.Value
        sql = "select uid, username from pmsysusers where uid not in (select uid from " _
        + "pmsysusers where apprgrp <> '" & aid & "')"

        dr = lab.GetRdrData(sql)
        lbmaster.DataSource = dr
        lbmaster.DataValueField = "uid"
        lbmaster.DataTextField = "username"
        lbmaster.DataBind()
        dr.Close()

    End Sub
    Private Sub GetSelect()
        aid = lblaid.Value
        sql = "select uid, username from pmsysusers where apprgrp = '" & aid & "'"

        dr = lab.GetRdrData(sql)
        lbselect.DataSource = dr
        lbselect.DataValueField = "uid"
        lbselect.DataTextField = "username"
        lbselect.DataBind()
        dr.Close()
    End Sub
    Private Sub GetItems(ByVal id As String, ByVal str As String)
        aid = lblaid.Value
        sql = "update pmsysusers set apprgrp = '" & aid & "' where uid = '" & id & "'"
        lab.Update(sql)
    End Sub
    Private Sub RemItems(ByVal id As String, ByVal str As String)
        aid = lblaid.Value
        sql = "update pmsysusers set apprgrp = '0' where uid = '" & id & "'"
        lab.Update(sql)

    End Sub

    Private Sub btntolist_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntolist.Click
        Dim Item As ListItem
        Dim f, fi As String
        lab.Open()
        For Each Item In lbmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                GetItems(fi, f)
            End If
        Next

        GetLabor()
        GetSelect()
        lab.Dispose()
    End Sub

    Private Sub btnfromlist_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromlist.Click
        Dim Item As ListItem
        Dim f, fi As String
        lab.Open()
        For Each Item In lbselect.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        GetLabor()
        GetSelect()
        lab.Dispose()
    End Sub

    Private Sub ibtnsrch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lab.Open()
        GetLabor()
        lab.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang166.Text = axlabs.GetASPXPage("PMApprovalUsers.aspx", "lang166")
        Catch ex As Exception
        End Try
        Try
            lang167.Text = axlabs.GetASPXPage("PMApprovalUsers.aspx", "lang167")
        Catch ex As Exception
        End Try
        Try
            lang168.Text = axlabs.GetASPXPage("PMApprovalUsers.aspx", "lang168")
        Catch ex As Exception
        End Try
        Try
            lang169.Text = axlabs.GetASPXPage("PMApprovalUsers.aspx", "lang169")
        Catch ex As Exception
        End Try
        Try
            lang170.Text = axlabs.GetASPXPage("PMApprovalUsers.aspx", "lang170")
        Catch ex As Exception
        End Try

    End Sub

End Class
