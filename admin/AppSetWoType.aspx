<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetWoType.aspx.vb"
    Inherits="lucy_r12.AppSetWoType" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetWoType</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetWoTypeaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function checkpri(wotid) {
            var chk = document.getElementById("lblprimsg").value;
            if (chk == "usecust") {
                var conf = confirm("Your System is set for Custom Work Order Priorities\nWould you like to reset your current Default Priority\nselection and reload the Work Type Grid?")
                if (conf == true) {
                    document.getElementById("lblwotid").value = wotid;
                    document.getElementById("lblret").value = "updatepri";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }
            }
            else {
                var conf = confirm("Your System is set for Standard Work Order Priorities\nWould you like to reset your current Default Priority\nselection and reload the Work Type Grid?")
                if (conf == true) {
                    document.getElementById("lblwotid").value = wotid;
                    document.getElementById("lblret").value = "undopri";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }
            }
        }
        function getpri(who) {
            var eReturn = window.showModalDialog("appsetwtlkup.aspx?typ=pri", "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    var earr = eReturn.split("~");
                    document.getElementById(who).innerHTML = earr[1];
                    document.getElementById("lblwopri").value = earr[1];
                    document.getElementById("lblwopriid").value = earr[0];
                    //alert(document.getElementById("lblwopriid").value)
                }
            }
        }
        function getstat(who) {
            var eReturn = window.showModalDialog("appsetwtlkup.aspx?typ=stat", "", "dialogHeight:370px; dialogWidth:370px; resizable=yes");
            if (eReturn) {
                if (eReturn != "") {
                    document.getElementById(who).innerHTML = eReturn;
                    document.getElementById("lblwostat").value = eReturn;
                }
            }
        }
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table class="tbg" id="ttdiv" cellspacing="0" cellpadding="0" runat="server">
        <tr>
            <td width="100">
            </td>
            <td width="180">
            </td>
            <td width="140">
            </td>
        </tr>
        <tr class="tbg">
            <td style="height: 16px" colspan="3">
                <asp:Label ID="lbltasktype" runat="server" Font-Size="X-Small" Font-Names="Arial"
                    Font-Bold="True" Width="256px" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang125" runat="server">Search Type</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr class="tbg">
            <td valign="top" align="center" colspan="3">
                <asp:DataGrid ID="dgtt" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                    AllowPaging="True" GridLines="None" CellPadding="0" CellSpacing="2" ShowFooter="True"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="imgedit" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit This Task Type" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton27" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton28" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblttids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wotid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wotid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Work Order Type">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wotype") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewtt" runat="server" Width="100px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wotype") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox16" runat="server" Width="100px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wotype") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Description">
                            <HeaderStyle Width="130px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtwtda" runat="server" Width="130px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="txtwde" runat="server" Width="130px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Default Priority">
                            <HeaderStyle Width="190px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Labelpri" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dfltpri") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="txtpria" runat="server" Width="150px" MaxLength="50"  CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.dfltpri") %>'>
                                </asp:Label><asp:ImageButton id="imgpria" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif"  ></asp:ImageButton>
                                <asp:DropDownList ID="ddpria" runat="server" CssClass="plainlabel">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="txtprie" runat="server" Width="130px" MaxLength="50" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.dfltpri") %>'>
                                </asp:Label><asp:ImageButton id="imgprie" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif"></asp:ImageButton>
                                <asp:DropDownList ID="ddprie" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("dfltpri")) %>'>
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                            </asp:DropDownList>
                            <asp:ImageButton src="../images/appbuttons/minibuttons/gwarning.gif" alt="" id="imgpriwarn" runat="server" /><asp:ImageButton />
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Default Status">
                            <HeaderStyle Width="130px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Labelstat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dfltstat") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="txtstata" runat="server" Width="110px" MaxLength="50" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.dfltstat") %>'>
                                </asp:Label><asp:ImageButton id="imgstata" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="txtstate" runat="server" Width="110px" MaxLength="50" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.dfltstat") %>'>
                                </asp:Label><asp:ImageButton id="imgstate" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Active">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbaci" Enabled="False" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.active") %>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                            <EditItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbact" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.active") %>'>
                                </asp:CheckBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblpriids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dfltpriid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpriide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dfltpriid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblusepris" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.usepri") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbluseprie" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.usepri") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid><br>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblcid" runat="server" name="lblcid">
    <input type="hidden" id="lblold" runat="server">
    <input type="hidden" id="txtpg" runat="server" name="Hidden1"><input type="hidden"
        id="txtpgcnt" runat="server" name="txtpgcnt">
    <input type="hidden" id="lblret" runat="server" name="lblret">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblwopriid" runat="server" />
    <input type="hidden" id="lblwopri" runat="server" />
    <input type="hidden" id="lblwostat" runat="server" />
    <input type="hidden" id="lblprimsg" runat="server" />
    <input type="hidden" id="lblwotid" runat="server" />
    <input type="hidden" id="lbluseprih" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    </form>
</body>
</html>
