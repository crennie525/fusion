<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetInvLists.aspx.vb"
    Inherits="lucy_r12.AppSetInvLists" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetInvLists</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetInvListsaspx_1.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="handledid();checkit();">
    <form id="form1" method="post" runat="server">
    <table cellspacing="2" cellpadding="2" width="820">
        <tr>
            <td width="120">
            </td>
            <td width="180">
            </td>
            <td width="220">
            </td>
            <td width="300">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblps" runat="server" ForeColor="Red" Font-Size="Small" Font-Names="Arial"
                    Font-Bold="True" Width="376px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang76" runat="server">Inventory List</asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="ddlists" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang77" runat="server">Search List Values</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
                    CssClass="imgbutton"></asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgdepts" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                    AllowPaging="True" GridLines="None" CellPadding="0" ShowFooter="True" CellSpacing="1"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="List Value">
                            <HeaderStyle Width="140px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="LinkButton1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewlname" runat="server" Width="140px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="Textbox2" runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Description">
                            <HeaderStyle Width="320px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewldesc" runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="Textbox3" runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle Width="220px"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.valid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewllabel" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.valid") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Textbox1" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.valid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbldepta" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbldepte" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                        ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                </asp:DataGrid>
            </td>
            <td align="center" valign="middle" class="plainlabelred">
                <asp:Label ID="lang78" runat="server">Inventory Lists are used in the Inventory Control Module.</asp:Label><br>
                <asp:Label ID="lang79" runat="server">Inventory List entries Are Not Required if your inventory is controlled outside of Fusion.</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lblsid"
        type="hidden" name="lblsid" runat="server">
    <input id="lblpar" type="hidden" name="lblpar" runat="server"><input id="lbldid"
        type="hidden" name="lbldid" runat="server">
    <input id="lblpchk" type="hidden" name="lblpchk" runat="server"><input id="appchk"
        type="hidden" name="appchk" runat="server">
    <input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt"
        type="hidden" name="txtpgcnt" runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server">
    <input id="lblold" type="hidden" name="lblold" runat="server">
    <input id="lbllist" type="hidden" runat="server"><input id="lblscreenname" type="hidden"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
