<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="admindialog.aspx.vb" Inherits="lucy_r12.admindialog" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title id="pgtitle" runat="server">Admin Dialog</title>
    <script language="JavaScript" type="text/javascript" src="../scripts1/admindialogaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/jscript">
    <!--
        function handlecraft(ret) {
            if (ret == "") ret = "can";
            window.returnValue = ret;
            window.close();
        }
        //-->
    </script>
</head>
<body onload="pageScroll();">
    <form id="form1" method="post" runat="server">
    <iframe id="ifinv" runat="server" width="100%" height="100%" frameborder="0"></iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;">
    </iframe>
    <script type="text/javascript">
        document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
