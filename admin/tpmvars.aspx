<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tpmvars.aspx.vb" Inherits="lucy_r12.tpmvars" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>tpmvars</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table>
        <tr>
            <td class="redlabel" colspan="3" align="center">
                <asp:CheckBox ID="cbact" runat="server" Text="Activate TPM Task Alert"></asp:CheckBox>
            </td>
        </tr>
        <tr>
            <td class="label" width="230">
                <asp:Label ID="lang184" runat="server">Frequency Less Than or Equal to</asp:Label>
            </td>
            <td width="80">
                <asp:TextBox ID="txtfreq" runat="server" CssClass="plainlabel" Width="80px"></asp:TextBox>
            </td>
            <td class="label" width="140">
                <asp:Label ID="lang185" runat="server">Days</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" width="230">
                <asp:Label ID="lang186" runat="server">Time Required Less Than or Equal to</asp:Label>
            </td>
            <td width="80">
                <asp:TextBox ID="txtmins" runat="server" CssClass="plainlabel" Width="60px"></asp:TextBox>
            </td>
            <td class="label" width="140">
                <asp:Label ID="lang187" runat="server">Minutes</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td class="bluelabel" align="center">
                            <asp:Label ID="lang188" runat="server">Available Task Types</asp:Label>
                        </td>
                        <td>
                        </td>
                        <td class="bluelabel" align="center">
                            <asp:Label ID="lang189" runat="server">TPM Task Types</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" width="200">
                            <asp:ListBox ID="lblttmaster" runat="server" Width="170px" Height="80px" SelectionMode="Multiple">
                            </asp:ListBox>
                        </td>
                        <td valign="middle" align="center" width="22">
                            <asp:ImageButton ID="btnfromtt" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                            </asp:ImageButton><asp:ImageButton ID="btntott" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                            </asp:ImageButton>
                        </td>
                        <td align="center" width="200">
                            <asp:ListBox ID="lbltpmtt" runat="server" Width="170px" Height="80px" SelectionMode="Multiple">
                            </asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td class="bluelabel" align="center">
                            <asp:Label ID="lang190" runat="server">Available Craft/Skills</asp:Label>
                        </td>
                        <td>
                        </td>
                        <td class="bluelabel" align="center">
                            <asp:Label ID="lang191" runat="server">TPM Craft/Skills</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" width="200">
                            <asp:ListBox ID="lbskillmaster" runat="server" Width="170px" Height="120px" SelectionMode="Multiple">
                            </asp:ListBox>
                        </td>
                        <td valign="middle" align="center" width="22">
                            <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                            </asp:ImageButton><asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                            </asp:ImageButton>
                        </td>
                        <td align="center" width="200">
                            <asp:ListBox ID="lbsiteskills" runat="server" Width="170px" Height="120px" SelectionMode="Multiple">
                            </asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td class="bluelabel" align="center">
                            <asp:Label ID="lang192" runat="server">Available Equipment Stats</asp:Label>
                        </td>
                        <td>
                        </td>
                        <td class="bluelabel" align="center">
                            <asp:Label ID="lang193" runat="server">TPM Equipment Stats</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" width="200">
                            <asp:ListBox ID="lblstatmaster" runat="server" Width="170px" Height="80px" SelectionMode="Multiple">
                            </asp:ListBox>
                        </td>
                        <td valign="middle" align="center" width="22">
                            <asp:ImageButton ID="btntostat" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                            </asp:ImageButton><asp:ImageButton ID="btnfromstat" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                            </asp:ImageButton>
                        </td>
                        <td align="center" width="200">
                            <asp:ListBox ID="lbltpmstat" runat="server" Width="170px" Height="80px" SelectionMode="Multiple">
                            </asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif">
                </asp:ImageButton>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
