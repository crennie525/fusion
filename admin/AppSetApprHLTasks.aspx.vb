

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetApprHLTasks
    Inherits System.Web.UI.Page
	Protected WithEvents lang18 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim pp As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents lblgroupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblans As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql, ro As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rev As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgphases As System.Web.UI.WebControls.DataGrid
    Protected WithEvents tdphase As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgphases.Columns(0).Visible = False
                dgphases.Columns(5).Visible = False
            End If
            Dim id, pid, grp, ans, cnt As String
            id = Request.QueryString("id").ToString
            'grp = Request.QueryString("grp").ToString
            ans = Request.QueryString("ans").ToString
            cnt = Request.QueryString("cnt").ToString
            rev = Request.QueryString("rev").ToString
            lblans.Value = ans
            lbleqcnt.Value = cnt
            'tdphase.InnerHtml = grp
            lblgroupid.Value = id '"1"
            pp.Open()
            If rev = 0 Then
                rev = pp.Rev()
            End If
            lblrev.Value = rev
            LoadGroup()
            LoadPhase()
            pp.Dispose()
        End If

    End Sub
    Private Sub LoadGroup()
        Dim groupid As String = lblgroupid.Value
        sql = "select listitem from pmApproval where pmaid = '" & groupid & "'"
        dr = pp.GetRdrData(sql)
        While dr.Read
            tdphase.InnerHtml = dr.Item("listitem").ToString
        End While
        dr.Close()
    End Sub
    Private Sub LoadPhase()
        Dim groupid As String = lblgroupid.Value
        sql = "select pmacid, pmaid, checkorder, checkitem " _
        + "from pmApprovalChk where pmaid = '" & groupid & "' order by checkorder"
        dr = pp.GetRdrData(sql)
        dgphases.DataSource = dr
        dgphases.DataBind()
        dr.Close()
    End Sub

    Private Sub dgphases_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.EditCommand

        'lbloldtask.Value = CType(e.Item.FindControl("lblta"), Label).Text
        lbloldtasknum.Value = CType(e.Item.FindControl("A1"), Label).Text
        dgphases.EditItemIndex = e.Item.ItemIndex
        pp.Open()
        LoadPhase()
        pp.Dispose()
    End Sub

    Private Sub dgphases_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.CancelCommand
        dgphases.EditItemIndex = -1
        pp.Open()
        LoadPhase()
        pp.Dispose()
    End Sub

    Private Sub dgphases_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.UpdateCommand
        Dim otn, ntn, ph, pid, pmaid, ans, cnt As String

        otn = lbloldtasknum.Value
        ntn = CType(e.Item.FindControl("txtlo"), TextBox).Text
        pid = CType(e.Item.FindControl("lblpmacid"), Label).Text
        ph = CType(e.Item.FindControl("txtephase"), TextBox).Text
        ph = pp.ModString2(ph)
        pmaid = CType(e.Item.FindControl("lblpmaid"), Label).Text
        Try
            ntn = System.Convert.ToInt64(ntn)
            pp.Open()
            If Len(ph) <> 0 Then
                sql = "update pmApprovalChk set checkitem = '" & ph & "' " _
                + "where pmacid = '" & pid & "'"
                pp.Update(sql)
                ans = lblans.Value
                If ans <> "0" Then
                    CheckAns()
                End If
                cnt = lbleqcnt.Value
                If cnt = "1" Then
                    sql = "update equipment set apprstg1 = 0"
                    pp.Update(sql)
                End If
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1" , "AppSetApprHLTasks.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

            If otn <> ntn Then
                sql = "usp_reorderPMApprPhsHL '" & pmaid & "', '" & ntn & "', '" & otn & "'"
                pp.Update(sql)
                If cnt = "1" Then
                    sql = "usp_reorderPMApprEq 'hl'"
                    pp.Update(sql)
                End If
            End If
            dgphases.EditItemIndex = -1
            LoadPhase()
            pp.Dispose()
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr2" , "AppSetApprHLTasks.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try


    End Sub
    Private Sub CheckAns()
        Dim ans As String = lblans.Value
        If ans = "1" Then
            sql = "update equipment set apprstg2prev = 1 where apprstg2 = 1"
            pp.Update(sql)
        ElseIf ans = "2" Then
            sql = "update equipment set apprstg2 = 0 where apprstg2 = 1 and apprstg2prev <> 1"
            pp.Update(sql)
        End If

    End Sub
    Private Sub dgphases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.ItemCommand
        Dim ph, des, ci, cl, ans, cnt As String
        Dim newhl As Integer
        Dim tdes As TextBox
        Dim dd As DropDownList
        rev = lblrev.Value
        Dim groupid As String = lblgroupid.Value
        If e.CommandName = "Reset" Or e.CommandName = "Add" Then
            ph = CType(e.Item.FindControl("txtaphase"), TextBox).Text
            ph = pp.ModString2(ph)
            'dd = CType(e.Item.FindControl("ddaag"), DropDownList)
            'ci = dd.SelectedIndex
            'cl = dd.SelectedValue
            tdes = CType(e.Item.FindControl("txtaphase"), TextBox)
            'des = tdes.Text
        End If
        If e.CommandName = "Reset" Then
            tdes.Text = ""
            'dd.SelectedIndex = 0
        End If
        If e.CommandName = "Add" Then
            Dim phcnt As Integer
            pp.Open()
            Try
                sql = "select count(*) from pmApprovalChk where rev = '" & rev & "' and pmaid = '" & groupid & "'"
                phcnt = pp.Scalar(sql)
                phcnt = phcnt + 1
                sql = "insert into pmApprovalChk " _
                + "(pmaid, checkorder, checkitem, rev) values ('" & groupid & "', '" & phcnt & "', '" & ph & "', '" & rev & "') " _
                + "select @@identity as 'identity'"
                'pp.Update(sql)
                newhl = pp.Scalar(sql)
                ans = lblans.Value
                If ans <> "0" Then
                    CheckAns()
                End If
                cnt = lbleqcnt.Value
                If cnt = "1" Then
                    CheckEq(newhl, phcnt, groupid)
                End If
            Catch ex As Exception
            End Try
            LoadPhase()
            pp.Dispose()
        End If
    End Sub
    Private Sub CheckEq(ByVal newhl As Integer, ByVal phcnt As Integer, ByVal pmaid As String)
        sql = "usp_updateEqAppr 'hl', '" & pmaid & "', '" & newhl & "', '" & phcnt & "'"
        pp.Update(sql)
    End Sub
    Private Sub dgphases_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgphases.DeleteCommand
        Dim tnum, pmacid, ans, cnt
        Try
            tnum = CType(e.Item.FindControl("A1"), Label).Text
            pmacid = CType(e.Item.FindControl("lblpmacida"), Label).Text
        Catch ex As Exception
            tnum = CType(e.Item.FindControl("txtlo"), TextBox).Text
            pmacid = CType(e.Item.FindControl("lblpmacid"), TextBox).Text
        End Try
        sql = "usp_delPMApprPhsHL '" & pmacid & "', '" & tnum & "'"
        pp.Open()
        pp.Update(sql)
        ans = lblans.Value
        If ans <> "0" Then
            CheckAns()
        End If
        cnt = lbleqcnt.Value
        If cnt = "1" Then
            sql = "usp_delPMApprEqHL '" & pmacid & "'"
            pp.Update(sql)
        End If
        dgphases.EditItemIndex = -1
        LoadPhase()
        pp.Dispose()
    End Sub

    Private Sub dgphases_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgphases.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                         e.Item.ItemType <> ListItemType.Footer Then



            Try
                Dim jump As HtmlAnchor = CType(e.Item.FindControl("lnkphase"), HtmlAnchor)
                Dim id As String = DataBinder.Eval(e.Item.DataItem, "pmacid").ToString
                Dim gid As String = lblgroupid.Value 'phase id
                Dim nme As String = DataBinder.Eval(e.Item.DataItem, "checkitem").ToString
                nme = Replace(nme, "#", "%23")
                jump.Attributes("onclick") = "jump('" & id & "', '" & gid & "', '" & nme & "');"
            Catch ex As Exception

            End Try
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgphases.Columns(0).HeaderText = dlabs.GetDGPage("AppSetApprHLTasks.aspx", "dgphases", "0")
        Catch ex As Exception
        End Try
        Try
            dgphases.Columns(3).HeaderText = dlabs.GetDGPage("AppSetApprHLTasks.aspx", "dgphases", "3")
        Catch ex As Exception
        End Try
        Try
            dgphases.Columns(4).HeaderText = dlabs.GetDGPage("AppSetApprHLTasks.aspx", "dgphases", "4")
        Catch ex As Exception
        End Try
        Try
            dgphases.Columns(5).HeaderText = dlabs.GetDGPage("AppSetApprHLTasks.aspx", "dgphases", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang18.Text = axlabs.GetASPXPage("AppSetApprHLTasks.aspx", "lang18")
        Catch ex As Exception
        End Try

    End Sub

End Class
