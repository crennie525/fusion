

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetpmApprGrps
    Inherits System.Web.UI.Page
	Protected WithEvents lang97 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, Login, ro As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 3
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim dr As SqlDataReader
    Protected WithEvents lbloldtasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchanges As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
    Protected WithEvents lblrev As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rev As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpre As System.Web.UI.WebControls.Label
    Protected WithEvents dgac As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgac As System.Web.UI.WebControls.Label
    Protected WithEvents prePrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents preNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            'ro = Request.QueryString("ro").ToString
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgac.Columns(0).Visible = False
                dgac.Columns(6).Visible = False
            End If
            rev = Request.QueryString("rev").ToString
            appset.Open()
            If rev = 0 Then
                rev = appset.Rev()
            End If
            lblrev.Value = rev
            PopGrps(PageNumber)
            appset.Dispose()

        End If
        'prePrev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'prePrev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        ''preNext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'preNext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    Private Sub PopGrps(ByVal PageNumber As Integer)
        'Try
        Try

        Catch ex As Exception

        End Try
        cid = lblcid.Value
        rev = lblrev.Value
        sql = "select count(*) " _
        + "from pmApprovalGrps where apprgrp <> 0 and rev = '" & rev & "'"
        dgac.VirtualItemCount = appset.Scalar(sql)
        If dgac.VirtualItemCount = 0 Then
            lblpre.Text = "No Approval Group Records Found"
            dgac.Visible = True
            prePrev.Visible = False
            preNext.Visible = False
            Filter = "apprgrp <> ''0'' and rev = ''" & rev & "''"
            Tables = "pmApprovalGrps"
            PK = "apprgrp"
            PageSize = "3"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgac.DataSource = dr
            dgac.DataBind()
            dr.Close()
            'appset.Dispose()
        Else
            Filter = "apprgrp <> ''0'' and rev = ''" & rev & "''"
            Tables = "pmApprovalGrps"
            PK = "apprgrp"
            PageSize = "3"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgac.DataSource = dr
            dgac.DataBind()
            dr.Close()
            'appset.Dispose()
            lblpgac.Text = "Page " & dgac.CurrentPageIndex + 1 & " of " & dgac.PageCount
            lblpre.Text = ""
            prePrev.Visible = True
            preNext.Visible = True

        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            iL = CatID
        Else
            CatID = 0
        End If
        Return iL
    End Function
    Private Sub dgac_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.EditCommand
        PageNumber = dgac.CurrentPageIndex + 1
        lbloldtasknum.Value = CType(e.Item.FindControl("A1"), Label).Text
        dgac.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopGrps(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgac_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.UpdateCommand
        Dim id, desc, otn, ntn, addrem, addremstr As String
        rev = lblrev.Value
        otn = lbloldtasknum.Value
        ntn = CType(e.Item.FindControl("txtlo"), TextBox).Text
        ntn = appset.ModString2(ntn)
        id = CType(e.Item.FindControl("lblapprgrp"), Label).Text
        desc = CType(e.Item.FindControl("Textbox20"), TextBox).Text
        desc = appset.ModString2(desc)
        addrem = CType(e.Item.FindControl("ddaddrem"), DropDownList).SelectedValue
        If addrem = "0" Then
            addrem = "2"
        End If
        If addrem = "1" Then
            addremstr = "Yes"
        ElseIf addrem = "2" Then
            addremstr = "No"
        End If
        Try
            ntn = System.Convert.ToInt64(ntn)
            If Len(desc) > 0 Then
                If Len(desc) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr77" , "AppSetpmApprGrps.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    sql = "update pmApprovalGrps set grpname = " _
                    + "'" & desc & "', addremid = '" & addrem & "', " _
                    + "addrem = '" & addremstr & "' where apprgrp = '" & id & "' and rev = '" & rev & "'"
                    appset.Update(sql)
                    lblchanges.Value = "yes"
                    If otn <> ntn Then
                        sql = "usp_reorderPMApprGrps '" & ntn & "', '" & otn & "', '" & rev & "'"
                        appset.Update(sql)
                    End If
                    dgac.EditItemIndex = -1
                    PopGrps(PageNumber)
                    appset.Dispose()
                End If
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr78" , "AppSetpmApprGrps.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr79" , "AppSetpmApprGrps.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try



    End Sub

    Private Sub dgac_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.CancelCommand
        dgac.EditItemIndex = -1
        appset.Open()
        PopGrps(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgac_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.DeleteCommand
        appset.Open()
        Dim id, ntn As String
        rev = lblrev.Value
        Try
            ntn = CType(e.Item.FindControl("A1"), Label).Text
            id = CType(e.Item.FindControl("lblapprgrpa"), Label).Text
        Catch ex As Exception
            ntn = CType(e.Item.FindControl("txtlo"), TextBox).Text
            id = CType(e.Item.FindControl("lblapprgrp"), Label).Text
        End Try
        Dim currcnt As Integer
        sql = "select count(*) from pmApproval where apprgrp = '" & id & "' and rev = '" & rev & "'"
        currcnt = appset.Scalar(sql)
        If currcnt <> 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr80" , "AppSetpmApprGrps.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        sql = "usp_delApprgrp '" & id & "', '" & ntn & "', '" & rev & "'"
        appset.Update(sql)
        dgac.EditItemIndex = -1
        sql = "select Count(*) from pmApprovalGrps where apprgrp <> 0 and rev = '" & rev & "'"
        PageNumber = appset.PageCount(sql, PageSize)
        'appset.Dispose()
        dgac.EditItemIndex = -1
        lblchanges.Value = "yes"
        If dgac.CurrentPageIndex > PageNumber Then
            dgac.CurrentPageIndex = PageNumber - 1
        End If
        If dgac.CurrentPageIndex < PageNumber - 2 Then
            PageNumber = dgac.CurrentPageIndex + 1
        End If
        PopGrps(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub prePrev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles prePrev.Click
        If (dgac.CurrentPageIndex > 0) Then
            dgac.CurrentPageIndex = dgac.CurrentPageIndex - 1
            appset.Open()
            PopGrps(dgac.CurrentPageIndex - 1)
            appset.Dispose()
        End If
        checkPrePgCnt()
    End Sub
    Private Sub checkPrePgCnt()
        If (dgac.CurrentPageIndex) = 0 Or dgac.PageCount = 1 Then
            prePrev.Enabled = False
        Else
            prePrev.Enabled = True
        End If
        If dgac.PageCount > 1 And (dgac.CurrentPageIndex + 1 < dgac.PageCount) Then
            preNext.Enabled = True
        Else
            preNext.Enabled = False
        End If
    End Sub

    Private Sub preNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles preNext.Click
        If ((dgac.CurrentPageIndex) < (dgac.PageCount - 1)) Then
            dgac.CurrentPageIndex = dgac.CurrentPageIndex + 1
            appset.Open()
            PopGrps(dgac.CurrentPageIndex + 1)
            appset.Dispose()
            checkPrePgCnt()
        End If
        checkPrePgCnt()
    End Sub

    Private Sub dgac_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgac.ItemCommand
        Dim lname, addrem, addremstr As String
        Dim lnamei As TextBox
        Dim grpcnt As Integer
        rev = lblrev.Value
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewac"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString2(lname)
            addrem = CType(e.Item.FindControl("ddaddremftr"), DropDownList).SelectedValue
            If addrem = "0" Then
                addrem = "2"
            End If
            If addrem = "1" Then
                addremstr = "Yes"
            ElseIf addrem = "2" Then
                addremstr = "No"
            End If
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr81" , "AppSetpmApprGrps.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        Dim stat As String
                        cid = lblcid.Value
                        stat = lnamei.Text
                        sql = "select Count(*) from pmApprovalGrps where apprgrp <> 0 and rev = '" & rev & "'"
                        grpcnt = appset.Scalar(sql)
                        grpcnt = grpcnt + 1
                        sql = "insert into pmApprovalGrps (grpname, grplevel, addremid, addrem, rev) values " _
                        + "('" & lname & "', '" & grpcnt & "', '" & addrem & "', '" & addremstr & "', '" & rev & "')"
                        appset.Update(sql)
                        lblchanges.Value = "yes"
                        Dim statcnt As Integer = dgac.VirtualItemCount + 1
                        lnamei.Text = ""
                        sql = "select Count(*) from pmApprovalGrps where apprgrp <> 0 and rev = '" & rev & "'"
                        PageNumber = appset.PageCount(sql, PageSize)
                        PopGrps(PageNumber)
                        appset.Dispose()
                    End If
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr82" , "AppSetpmApprGrps.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
        End If
    End Sub

    Private Sub dgac_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgac.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                                 e.Item.ItemType <> ListItemType.Footer Then

            Try
                Dim id As String = DataBinder.Eval(e.Item.DataItem, "apprgrp").ToString
                Dim img As HtmlImage = CType(e.Item.FindControl("ibtnppl"), HtmlImage)
                img.Attributes("onclick") = "getusers('" & id & "');"
            Catch ex As Exception

            End Try
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgac.Columns(0).HeaderText = dlabs.GetDGPage("AppSetpmApprGrps.aspx", "dgac", "0")
        Catch ex As Exception
        End Try
        Try
            dgac.Columns(2).HeaderText = dlabs.GetDGPage("AppSetpmApprGrps.aspx", "dgac", "2")
        Catch ex As Exception
        End Try
        Try
            dgac.Columns(3).HeaderText = dlabs.GetDGPage("AppSetpmApprGrps.aspx", "dgac", "3")
        Catch ex As Exception
        End Try
        Try
            dgac.Columns(4).HeaderText = dlabs.GetDGPage("AppSetpmApprGrps.aspx", "dgac", "4")
        Catch ex As Exception
        End Try
        Try
            dgac.Columns(5).HeaderText = dlabs.GetDGPage("AppSetpmApprGrps.aspx", "dgac", "5")
        Catch ex As Exception
        End Try
        Try
            dgac.Columns(6).HeaderText = dlabs.GetDGPage("AppSetpmApprGrps.aspx", "dgac", "6")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang97.Text = axlabs.GetASPXPage("AppSetpmApprGrps.aspx", "lang97")
        Catch ex As Exception
        End Try

    End Sub

End Class
