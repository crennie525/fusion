

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmSiteSkills
    Inherits System.Web.UI.Page
    Protected WithEvents lang182 As System.Web.UI.WebControls.Label

    Protected WithEvents lang181 As System.Web.UI.WebControls.Label

    Protected WithEvents lang180 As System.Web.UI.WebControls.Label

    Protected WithEvents lang179 As System.Web.UI.WebControls.Label

    Protected WithEvents lang178 As System.Web.UI.WebControls.Label

    Protected WithEvents lang177 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim skill As New Utilities
    Dim cid, sid, sname, app, Login, ro As String
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsitecnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trmsg2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents cbprep As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbundo As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbskillmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbsiteskills As System.Web.UI.WebControls.ListBox
    Protected WithEvents tdsite As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        Page.EnableViewState = True
        If Not IsPostBack Then
            Try
                'ro = Request.QueryString("ro").ToString
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    btntocomp.ImageUrl = "../images/appbuttons/minibuttons/forwardgraybg.gif"
                    btntocomp.Enabled = False
                    btnfromcomp.ImageUrl = "../images/appbuttons/minibuttons/backgraybg.gif"
                    btnfromcomp.Enabled = False
                    cbopts.Enabled = False
                End If
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                If Len(sid) <> 0 AndAlso sid <> "" AndAlso sid <> "0" Then
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    'Session("comp") = cid
                    skill.Open()
                    PopSite(sid)
                    CheckStat(cid, sid)
                    skill.Dispose()
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr183", "pmSiteSkills.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "nodeptid"
                End If
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr184", "pmSiteSkills.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "nodeptid"
            End Try

        End If
        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub cbundo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbundo.CheckedChanged
        skill.Open()
        sid = lblsid.Value
        cid = lblcid.Value
        sql = "usp_undositeskills '" & sid & "'"
        skill.Update(sql)
        PopSkills(cid, sid)
        PopSiteSkills(cid, sid)
        btntocomp.Enabled = True
        btnfromcomp.Enabled = True
        lbskillmaster.Enabled = True
        lbsiteskills.Enabled = True
        cbopts.Enabled = True
        trmsg.Attributes.Add("class", "view")
        trmsg2.Attributes.Add("class", "details")
        skill.Dispose()
    End Sub
    Private Sub cbprep_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbprep.CheckedChanged
        Dim scnt, ccnt As Integer
        skill.Open()
        sid = lblsid.Value
        cid = lblcid.Value
        sql = "select count(distinct skillid) from pmtasks where siteid = '" & sid & "' and skillid <> 0 " _
        + "and skillid is not null and " _
        + "skillid not in (select skillid from pmsiteskills where siteid = '" & sid & "')"
        scnt = skill.Scalar(sql)
        If scnt <> 0 Then
            sql = "usp_prepsiteskill '" & cid & "', '" & sid & "'"
            skill.Update(sql)
        End If
        PopSkills(cid, sid)
        PopSiteSkills(cid, sid)
        btntocomp.Enabled = True
        btnfromcomp.Enabled = True
        lbskillmaster.Enabled = True
        lbsiteskills.Enabled = True
        cbopts.Enabled = True
        trmsg.Attributes.Add("class", "details")
        trmsg2.Attributes.Add("class", "view")
        skill.Dispose()
    End Sub
    Private Sub CheckStat(ByVal cid As String, ByVal sid As String)
        Dim scnt, ccnt As Integer
        sql = "select count(*) from pmsiteskills where siteid = '" & sid & "'"
        ccnt = skill.Scalar(sql)
        If ccnt = 0 Then
            PopSkills(cid, sid)
            PopSiteSkills(cid, sid)
            btntocomp.Enabled = False
            btnfromcomp.Enabled = False
            lbskillmaster.Enabled = False
            lbsiteskills.Enabled = False
            cbopts.Enabled = False
            trmsg.Attributes.Add("class", "view")
            trmsg2.Attributes.Add("class", "details")
        Else
            sql = "select count(distinct skillid) from pmtasks where siteid = '" & sid & "' and skillid <> 0 " _
            + "and skillid is not null and " _
            + "skillid not in (select skillid from pmsiteskills where siteid = '" & sid & "')"
            scnt = skill.Scalar(sql)
            If scnt <> 0 Then
                sql = "usp_prepsiteskill '" & cid & "', '" & sid & "'"
                skill.Update(sql)
            End If
            PopSkills(cid, sid)
            PopSiteSkills(cid, sid)
            btntocomp.Enabled = True
            btnfromcomp.Enabled = True
            lbskillmaster.Enabled = True
            lbsiteskills.Enabled = True
            cbopts.Enabled = True
            trmsg.Attributes.Add("class", "details")
            trmsg2.Attributes.Add("class", "view")
        End If



    End Sub
    Private Sub PopSite(ByVal sid As String)
        sql = "select sitename from sites where siteid = '" & sid & "'"
        dr = skill.GetRdrData(sql)
        While dr.Read
            tdsite.InnerHtml = dr.Item("sitename").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopSkills(ByVal cid As String, ByVal sid As String)
        sql = "select * from pmSkills where skillid not in (select skillid from pmsiteskills where siteid = '" & sid & "') and skill <> 'Select'"
        dr = skill.GetRdrData(sql)
        lbskillmaster.DataSource = dr
        lbskillmaster.DataValueField = "skillid"
        lbskillmaster.DataTextField = "skill"
        lbskillmaster.DataBind()
        dr.Close()
        Try
            lbskillmaster.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopSiteSkills(ByVal cid As String, ByVal sid As String)

        'sql = "select count(*) from pmSiteSkills where compid = '" & cid & "' and siteid = '" & sid & "'"

        sql = "select * from pmSiteSkills where compid = '" & cid & "' and siteid = '" & sid & "'"
        dr = skill.GetRdrData(sql)
        lbsiteskills.DataSource = dr
        lbsiteskills.DataValueField = "skillid"
        lbsiteskills.DataTextField = "skill"
        lbsiteskills.DataBind()
        dr.Close()
        Try
            lbsiteskills.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetItems(ByVal skillid As String, ByVal skillstr As String)
        cid = lblcid.Value
        'cid = "0"
        sid = lblsid.Value
        'sid = HttpContext.Current.Session("sid").ToString()
        app = lblapp.Value
        Dim fcnt As Integer
        sql = "select count(*) from pmSiteSkills where compid = '" & cid & "' and " _
        + "siteid = '" & sid & "' and skillid = '" & skillid & "'"
        fcnt = skill.Scalar(sql)
        If skillstr <> "" Then
            If fcnt = 0 Then
                sql = "usp_addSiteSkill '" & cid & "', '" & sid & "', '" & skillid & "', '" & skillstr & "'"
                skill.Update(sql)
            End If
        End If
    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        cid = lblcid.Value
        'cid = "0"
        sid = lblsid.Value
        'sid = HttpContext.Current.Session("sid").ToString()
        Dim Item As ListItem
        Dim s, ss As String
        skill.Open()
        For Each Item In lbskillmaster.Items
            If Item.Selected Then
                ss = Item.ToString
                s = Item.Value.ToString
                If ss <> "" Then
                    GetItems(s, ss)
                End If

            End If
        Next
        PopSiteSkills(cid, sid)
        PopSkills(cid, sid)
        skill.Dispose()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        cid = lblcid.Value
        'cid = "0"
        sid = lblsid.Value
        'sid = HttpContext.Current.Session("sid").ToString()
        Dim Item As ListItem
        Dim s As String
        skill.Open()
        For Each Item In lbsiteskills.Items
            If Item.Selected Then
                s = Item.Value.ToString
                RemItems(s)
            End If
        Next
        PopSiteSkills(cid, sid)
        PopSkills(cid, sid)
        skill.Dispose()
    End Sub
    Private Sub RemItems(ByVal skillid As String)
        sid = lblsid.Value
        'sid = HttpContext.Current.Session("sid").ToString()
        Try
            sql = "select count(*) from pmtasks where skillid = '" & skillid & "' and siteid = '" & sid & "'"
            Dim scnt As Integer = skill.Scalar(sql)
            If scnt = 0 Then
                sql = "usp_delSiteSkill '" & sid & "', '" & skillid & "'"
                skill.Update(sql)
            Else
                Dim strMessage As String = tmod.getmsg("cdstr185", "pmSiteSkills.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbskillmaster.AutoPostBack = False
            lbsiteskills.AutoPostBack = False
        Else
            lbskillmaster.AutoPostBack = True
            lbsiteskills.AutoPostBack = True
        End If
    End Sub












    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang177.Text = axlabs.GetASPXPage("pmSiteSkills.aspx", "lang177")
        Catch ex As Exception
        End Try
        Try
            lang178.Text = axlabs.GetASPXPage("pmSiteSkills.aspx", "lang178")
        Catch ex As Exception
        End Try
        Try
            lang179.Text = axlabs.GetASPXPage("pmSiteSkills.aspx", "lang179")
        Catch ex As Exception
        End Try
        Try
            lang180.Text = axlabs.GetASPXPage("pmSiteSkills.aspx", "lang180")
        Catch ex As Exception
        End Try
        Try
            lang181.Text = axlabs.GetASPXPage("pmSiteSkills.aspx", "lang181")
        Catch ex As Exception
        End Try
        Try
            lang182.Text = axlabs.GetASPXPage("pmSiteSkills.aspx", "lang182")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    
End Class
