<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetDeptTab.aspx.vb"
    Inherits="lucy_r12.AppSetDeptTab" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetDeptTab</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetDeptTabaspx_1.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="handledid();checkit();">
    <form id="form1" method="post" runat="server">
    <table cellspacing="0" cellpadding="2" width="520">
        <tr>
            <td width="120">
            </td>
            <td width="180">
            </td>
            <td width="220">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblps" runat="server" Width="376px" Font-Bold="True" Font-Names="Arial"
                    Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="bluelabel" id="tdps" runat="server" colspan="3">
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang41" runat="server">Search Departments</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgdepts" runat="server" ShowFooter="True" CellPadding="0" GridLines="None"
                    AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <EditItemStyle Height="15px"></EditItemStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Department">
                            <HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Select" Text='<%# DataBinder.Eval(Container, "DataItem.Dept_Line") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewlname" runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.Dept_Line") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="Textbox2" runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.Dept_Line") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Description">
                            <HeaderStyle Width="220px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.Dept_Desc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewldesc" runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.Dept_Desc") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="Textbox3" runat="server" Width="210px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.Dept_Desc") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle Width="220px"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.dept_id") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewllabel" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.dept_id") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="Textbox1" runat="server" Width="210px" Text='<%# DataBinder.Eval(Container, "DataItem.dept_id") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lbldepta" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dept_id") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbldepte" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dept_id") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="Imagebutton16" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" Height="20px" Font-Size="Small" Font-Names="Arial" Font-Bold="True"
                        ForeColor="White" BackColor="Blue" Wrap="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="26">
                <asp:Label ID="lbldfltd" runat="server" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table>
    <div class="details" id="psdiv" style="z-index: 116; position: absolute; padding-left: 0px;
        width: 220px; padding-right: 0px; height: 32px; top: 800px; left: 8px">
        <table cellspacing="0" cellpadding="0" width="220" bgcolor="white">
            <tr bgcolor="blue">
                <td>
                    &nbsp;&nbsp;
                    <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"
                        ForeColor="White">Change Default Plant Site</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closePS();" alt="" src="../images/appbuttons/close.gif"><br>
                </td>
            </tr>
            <tr class="tbg" height="30">
                <td style="border-bottom: gray 1px solid; border-left: gray 1px solid; border-top: gray 1px solid;
                    border-right: gray 1px solid" align="center" colspan="2">
                    <asp:DropDownList ID="ddps" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblcid" type="hidden" runat="server"><input id="lblsid" type="hidden"
        runat="server">
    <input id="lblpar" type="hidden" runat="server"><input id="lbldid" type="hidden"
        runat="server">
    <input id="lblpchk" type="hidden" runat="server"><input id="appchk" type="hidden"
        name="appchk" runat="server">
    <input type="hidden" id="txtpg" runat="server" name="Hidden1"><input type="hidden"
        id="txtpgcnt" runat="server" name="txtpgcnt">
    <input type="hidden" id="lblret" runat="server" name="lblret">
    <input type="hidden" id="lblold" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
