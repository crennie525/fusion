<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetApprPlan.aspx.vb"
    Inherits="lucy_r12.AppSetApprPlan" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetApprPlan</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetApprPlanaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body onload="checkit();" class="tbg">
    <form id="form1" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" style="left: 0px; position: absolute; top: 3px">
        <tr>
            <td class="plainlabel">
                <asp:Label ID="lang21" runat="server">Click on an Approval Phase Link to update High Level Tasks</asp:Label><br>
                <asp:Label ID="lang22" runat="server">Click on a High Level Task Link to update Sub Tasks (if needed)</asp:Label>
            </td>
        </tr>
        <tr>
            <td id="tdplan" runat="server">
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblprojid" runat="server" name="lblprojid">
    <input type="hidden" id="lblrev" runat="server">
    <input id="lbllog" type="hidden" runat="server" name="lbllog">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
