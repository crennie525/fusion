

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetCellTab
    Inherits System.Web.UI.Page
	Protected WithEvents lang39 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim did, cid, sql, sid, ro As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String = "cell_name"
    Dim dr As SqlDataReader
    Protected WithEvents tdcn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tbll2 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents trsearchrow As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trnavrow As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    Protected WithEvents lstcelldepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblcellmsg As System.Web.UI.WebControls.Label
    Protected WithEvents dgcells As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addcell As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtcname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtcdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
       
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgcells.Columns(0).Visible = False
            End If
            Try
                cid = "0" 
            Catch ex As Exception
                'Response.Redirect("NewLogin.aspx")
            End Try

            If cid <> "" Then
                lblcid.Value = cid
                cid = lblcid.Value
                sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString()
                lblsid.Value = sid
                Try
                    did = Request.QueryString("did").ToString
                    lbldid.Value = did
                Catch ex As Exception
                    lbldid.Value = "0"
                End Try
                appset.Open()
                PopDeptList()
                appset.Dispose()
            Else
                'Response.Redirect("NewLogin.aspx")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadCells(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadCells(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub
    ' Stations/Cells
    
    Private Sub PopDeptList()
        cid = lblcid.Value
        sid = lblsid.Value
        sql = "select Dept_ID, Dept_Line " _
        + "from Dept where compid = '" & cid & "' and siteid = '" & sid & "' order by Dept_Line"
        dr = appset.GetRdrData(sql)
        lstcelldepts.DataSource = dr
        lstcelldepts.DataTextField = "Dept_Line"
        lstcelldepts.DataValueField = "Dept_ID"
        Try
            lstcelldepts.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()

        lstcelldepts.Items.Insert(0, "Select Department")
        lblcellmsg.Visible = True
        did = lbldid.Value
        If did <> "0" Then
            lstcelldepts.SelectedValue = did
        End If
        If did <> "0" Then
            LoadCells(PageNumber)
            appset.Dispose()
        Else
            trnavrow.Attributes.Add("class", "details")
            trsearchrow.Attributes.Add("class", "details")
            appset.Dispose()
        End If
    End Sub
   

    Private Sub lstcelldepts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstcelldepts.SelectedIndexChanged
        If lstcelldepts.SelectedIndex <> 0 Then
            did = lstcelldepts.SelectedValue
            appset.Open()
            LoadCells(PageNumber)
            appset.Dispose()
        End If
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadCells(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub LoadCells(ByVal PageNumber As Integer)
        'Try

        did = lstcelldepts.SelectedValue
        sid = lblsid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "cell_name like ''%" & srch & "%'' and dept_id = ''" & did & "'' and siteid = ''" & sid & "''"
            FilterCnt = "cell_name like '%" & srch & "%' and dept_id = '" & did & "' and siteid = '" & sid & "'"
        Else
            Filter = "dept_id = ''" & did & "'' and siteid = ''" & sid & "''"
            FilterCnt = "dept_id = '" & did & "' and siteid = '" & sid & "'"
        End If
        sql = "select Count(*) from Cells " _
        + "where " & FilterCnt 'Dept_ID = '" & did & "' and siteid = '" & sid & "'"

        Dim l2cnt As Integer
        l2cnt = appset.Scalar(sql)
        dgcells.VirtualItemCount = l2cnt 'appset.Scalar(sql)
        If dgcells.VirtualItemCount = 0 Then
            lblcellmsg.Visible = True
            lblcellmsg.Text = "No Cell Records Found"
            dgcells.Visible = True
            lblcellmsg.Visible = False
            Tables = "Cells"
            PK = "cellid"
            PageSize = "10"
            cid = lblcid.Value
            Fields = "*"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgcells.DataSource = dr
            Try
                dgcells.DataBind()
            Catch ex As Exception
                dgcells.CurrentPageIndex = 0
                dgcells.DataBind()
            End Try
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgcells.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgcells.PageCount

        Else
            dgcells.Visible = True
            lblcellmsg.Visible = False
            Tables = "Cells"
            PK = "cellid"
            PageSize = "10"
            cid = lblcid.Value
            Fields = "*"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgcells.DataSource = dr
            Try
                dgcells.DataBind()
            Catch ex As Exception
                dgcells.CurrentPageIndex = 0
                dgcells.DataBind()
            End Try
            dr.Close()
            lblcellmsg.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgcells.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgcells.PageCount
            trnavrow.Attributes.Add("class", "view")
            trsearchrow.Attributes.Add("class", "view")
        End If


        'End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadCells(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr15" , "AppSetCellTab.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadCells(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr16" , "AppSetCellTab.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgcells_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcells.UpdateCommand
        'Try
        appset.Open()
        did = lstcelldepts.SelectedValue
        Dim dept, desc, clid As String
        dept = CType(e.Item.Cells(1).Controls(1), TextBox).Text
        dept = appset.ModString1(dept)

        desc = CType(e.Item.FindControl("TextBox1"), TextBox).Text
        desc = appset.ModString1(desc)
        Dim old As String = lblold.Value
        If old <> dept Then
            clid = CType(e.Item.FindControl("lblcellid"), Label).Text
            sql = "update Cells set Cell_Desc = " _
            + "'" & desc & "', Cell_Name = '" & dept & "' where Dept_ID = '" & did & "' and cellid = '" & clid & "'"
            appset.Update(sql)
            dgcells.EditItemIndex = -1
            LoadCells(PageNumber)
        ElseIf old = dept Then
            clid = CType(e.Item.FindControl("lblcellid"), Label).Text
            sql = "update Cells set Cell_Desc = " _
            + "'" & desc & "', Cell_Name = '" & dept & "' where Dept_ID = '" & did & "' and cellid = '" & clid & "'"
            appset.Update(sql)
            dgcells.EditItemIndex = -1
            LoadCells(PageNumber)
        Else
            dgcells.EditItemIndex = -1
            LoadCells(PageNumber)
        End If

        appset.Dispose()
    End Sub

    Private Sub dgcells_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcells.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("Label9"), Label).Text
        dgcells.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        LoadCells(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgcells_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcells.CancelCommand
        dgcells.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        LoadCells(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub addcell_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addcell.Click
        Dim cn, cd As String
        If txtcname.Text <> "" Then
            If Len(txtcname.Text) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr17" , "AppSetCellTab.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                appset.Open()
                cn = txtcname.Text
                cn = appset.ModString1(cn)

                cd = txtcdesc.Text
                cd = appset.ModString1(cd)

                cid = lblcid.Value
                sid = lblsid.Value
                did = lstcelldepts.SelectedValue
                sql = "insert into Cells (compid, siteid, Dept_ID, Cell_Name, Cell_Desc) " _
                + "values ('" & cid & "', '" & sid & "', '" & did & "', '" & cn & "', '" & cd & "')"
                appset.Update(sql)
                Dim cellcnt As Integer = dgcells.VirtualItemCount + 1
                If IsDBNull(dgcells.VirtualItemCount) Or dgcells.VirtualItemCount = 0 Then
                    cellcnt = 1
                End If
                sql = "update AdminTasks " _
                + "set cells = cells + '" & cellcnt & "' where cid = '" & cid & "'"
                appset.Update(sql)
                txtcname.Text = ""
                txtcdesc.Text = ""
                cid = lblcid.Value
                sql = "select Count(*) from cells " _
                + "where compid = '" & cid & "' and siteid = '" & sid & "'"
                PageNumber = appset.PageCount(sql, PageSize)
                LoadCells(PageNumber)
                appset.Dispose()
            End If

        End If
    End Sub

    Private Sub dgcells_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcells.ItemCommand
        Dim lid, lname, ldesc, llabel As String
        Dim lnamei, ldesci, llabeli As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewl2name"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            ldesci = CType(e.Item.FindControl("txtnewl2desc"), TextBox)
            ldesc = ldesci.Text
            ldesc = appset.ModString1(ldesc)
            If lnamei.Text <> "" Then
                If Len(lnamei.Text) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr18" , "AppSetCellTab.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    cid = lblcid.Value
                    sid = lblsid.Value
                    did = lstcelldepts.SelectedValue
                    sql = "insert into Cells (compid, siteid, Dept_ID, Cell_Name, Cell_Desc) " _
                    + "values ('" & cid & "', '" & sid & "', '" & did & "', '" & lname & "', '" & ldesc & "')"
                    appset.Update(sql)
                    Dim cellcnt As Integer = dgcells.VirtualItemCount + 1
                    If IsDBNull(dgcells.VirtualItemCount) Or dgcells.VirtualItemCount = 0 Then
                        cellcnt = 1
                    End If
                    lnamei.Text = ""
                    ldesci.Text = ""
                    cid = lblcid.Value
                    sql = "select Count(*) from cells " _
                    + "where compid = '" & cid & "' and siteid = '" & sid & "' and dept_id = '" & did & "'"
                    PageNumber = appset.PageCount(sql, PageSize)
                    LoadCells(PageNumber)
                    appset.Dispose()
                End If

            End If
        End If

    End Sub


    Private Sub dgcells_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcells.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lblcellidi"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblcellid"), Label).Text
        End Try

        Dim ecnt As Integer
        sql = "select count(*) from equipment where cellid = '" & id & "'"
        ecnt = appset.Scalar(sql)
        If ecnt <> 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr19" , "AppSetCellTab.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
            Exit Sub
        Else
            sql = "delete from Cells where cellid = '" & id & "'"
            appset.Update(sql)
            cid = lblcid.Value
            sid = lblsid.Value
            did = lstcelldepts.SelectedValue
            PageSize = 10
            sql = "select Count(*) from cells " _
            + "where compid = '" & cid & "' and siteid = '" & sid & "' and dept_id = '" & did & "'"
            PageNumber = appset.PageCount(sql, PageSize)
            LoadCells(PageNumber)

        End If
        appset.Dispose()
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgcells.Columns(0).HeaderText = dlabs.GetDGPage("AppSetCellTab.aspx", "dgcells", "0")
        Catch ex As Exception
        End Try
        Try
            dgcells.Columns(1).HeaderText = dlabs.GetDGPage("AppSetCellTab.aspx", "dgcells", "1")
        Catch ex As Exception
        End Try
        Try
            dgcells.Columns(2).HeaderText = dlabs.GetDGPage("AppSetCellTab.aspx", "dgcells", "2")
        Catch ex As Exception
        End Try
        Try
            dgcells.Columns(4).HeaderText = dlabs.GetDGPage("AppSetCellTab.aspx", "dgcells", "4")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label12.Text = axlabs.GetASPXPage("AppSetCellTab.aspx", "Label12")
        Catch ex As Exception
        End Try
        Try
            lang39.Text = axlabs.GetASPXPage("AppSetCellTab.aspx", "lang39")
        Catch ex As Exception
        End Try
        Try
            lblcellmsg.Text = axlabs.GetASPXPage("AppSetCellTab.aspx", "lblcellmsg")
        Catch ex As Exception
        End Try

    End Sub

End Class
