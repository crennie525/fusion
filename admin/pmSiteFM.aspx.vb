

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmSiteFM
    Inherits System.Web.UI.Page
    Protected WithEvents lang176 As System.Web.UI.WebControls.Label

    Protected WithEvents lang175 As System.Web.UI.WebControls.Label

    Protected WithEvents lang174 As System.Web.UI.WebControls.Label

    Protected WithEvents lang173 As System.Web.UI.WebControls.Label

    Protected WithEvents lang172 As System.Web.UI.WebControls.Label

    Protected WithEvents lang171 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim fail As New Utilities
    Dim cid, sid, sname, app, ro As String
    Protected WithEvents lbsitefail As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trmsg1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents cbprep As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbunprep As System.Web.UI.WebControls.CheckBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents tdsite As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Page.EnableViewState = True
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                cbopts.Enabled = False
                btntocomp.Visible = False
                btnfromcomp.Visible = False
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")
            End If
            cid = Request.QueryString("cid").ToString
            lblcid.Value = cid
            'Session("comp") = cid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            'sname = Request.QueryString("sname").ToString
            'tdsite.InnerHtml = sname
            fail.Open()
            checkstat(sid)
            PopSite(sid)
            PopFail(cid)

            PopSiteFail(cid, sid)
            fail.Dispose()
        End If
        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    
    Private Sub checkstat(ByVal sid As String)
        Dim chk As String
        Dim scnt As Integer
        cid = lblcid.Value
        sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
        scnt = fail.Scalar(sql)
        If scnt = 0 Then
            lblfailchk.Value = "open"
            chk = "open"
        Else
            lblfailchk.Value = "site"
            chk = "site"
        End If
        If chk = "site" Then
            sql = "usp_updateSiteFM '" & cid & "', '" & sid & "'"
            fail.Update(sql)
            lblfailchk.Value = "site"
            trmsg1.Attributes.Add("class", "view")
            trmsg.Attributes.Add("class", "details")
            btntocomp.Enabled = True
            btnfromcomp.Enabled = True
        Else
            trmsg1.Attributes.Add("class", "details")
            trmsg.Attributes.Add("class", "view")
            btntocomp.Enabled = False
            btnfromcomp.Enabled = False
        End If
    End Sub
    Private Sub PopSite(ByVal sid As String)

        sql = "select sitename from sites where siteid = '" & sid & "'"
        dr = fail.GetRdrData(sql)
        While dr.Read
            tdsite.InnerHtml = dr.Item("sitename").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopFail(ByVal cid As String)
        sql = "select * from failuremodes where  failid not in (" _
         + "select failid from pmSiteFM where compid = '" & cid & "' and siteid = '" & sid & "') order by failuremode"
        dr = fail.GetRdrData(sql) 'compid = '" & cid & "' and
        lbfailmaster.DataSource = dr
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataBind()
        dr.Close()
        Try
            lbfailmaster.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopSiteFail(ByVal cid As String, ByVal sid As String)
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        'If chk = "" Then
        'sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
        'scnt = fail.Scalar(sql)
        'If scnt = 0 Then
        'lblfailchk.Value = "open"
        'chk = "open"
        'Else
        'lblfailchk.Value = "site"
        'chk = "site"
        'End If
        'End If
        'If chk = "site" Then
        'sql = "usp_updateSiteFM '" & cid & "', '" & sid & "'"
        'fail.Update(sql)
        'lblfailchk.Value = "site"
        'End If
        sql = "select distinct failuremode, failid from pmSiteFM where compid = '" & cid & "' and siteid = '" & sid & "' order by failuremode"
        dr = fail.GetRdrData(sql)
        lbsitefail.DataSource = dr
        lbsitefail.DataValueField = "failid"
        lbsitefail.DataTextField = "failuremode"
        lbsitefail.DataBind()
        dr.Close()
        Try
            lbsitefail.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub GetItems(ByVal failid As String, ByVal failstr As String)
        cid = lblcid.Value
        'cid = "0"
        sid = lblsid.Value
        'sid = HttpContext.Current.Session("sid").ToString()
        app = lblapp.Value
        Dim fcnt As Integer
        sql = "select count(*) from pmSiteFM where compid = '" & cid & "' and " _
        + "siteid = '" & sid & "' and failid = '" & failid & "'"
        fcnt = fail.Scalar(sql)
        If fcnt = 0 Then
            sql = "usp_addSiteFM '" & cid & "', '" & sid & "', '" & failid & "', '" & failstr & "'"
            fail.Update(sql)
        End If
    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        cid = lblcid.Value
        'cid = "0"
        sid = lblsid.Value
        'sid = HttpContext.Current.Session("sid").ToString()
        Dim Item As ListItem
        Dim s, ss As String
        fail.Open()
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                ss = Item.ToString
                s = Item.Value.ToString
                GetItems(s, ss)
            End If
        Next
        PopSiteFail(cid, sid)
        PopFail(cid)
        fail.Dispose()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        cid = lblcid.Value
        'cid = "0"
        sid = lblsid.Value
        'sid = HttpContext.Current.Session("sid").ToString()
        Dim Item As ListItem
        Dim s As String
        fail.Open()
        For Each Item In lbsitefail.Items
            If Item.Selected Then
                s = Item.Value.ToString
                RemItems(s)
            End If
        Next
        PopSiteFail(cid, sid)
        PopFail(cid)
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal failid As String)
        sid = lblsid.Value
        'sid = HttpContext.Current.Session("sid").ToString()
        Try
            sql = "select count(*) from componentfailmodes where failid = '" & failid & "' and siteid = '" & sid & "'"
            Dim scnt As Integer = fail.Scalar(sql)
            If scnt = 0 Then
                'sql = "usp_delSiteFM '" & sid & "', '" & failid & "'"
                sql = "delete from pmSiteFM where siteid = '" & sid & "' and failid = '" & failid & "'"
                fail.Update(sql)
            Else
                Dim strMessage As String = "Failure Modes used by a Component in this Site cannot be removed"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged
        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbfailmaster.AutoPostBack = False
            lbsitefail.AutoPostBack = False
        Else
            lbfailmaster.AutoPostBack = True
            lbsitefail.AutoPostBack = True
        End If
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang171.Text = axlabs.GetASPXPage("pmSiteFM.aspx", "lang171")
        Catch ex As Exception
        End Try
        Try
            lang172.Text = axlabs.GetASPXPage("pmSiteFM.aspx", "lang172")
        Catch ex As Exception
        End Try
        Try
            lang173.Text = axlabs.GetASPXPage("pmSiteFM.aspx", "lang173")
        Catch ex As Exception
        End Try
        Try
            lang174.Text = axlabs.GetASPXPage("pmSiteFM.aspx", "lang174")
        Catch ex As Exception
        End Try
        Try
            lang175.Text = axlabs.GetASPXPage("pmSiteFM.aspx", "lang175")
        Catch ex As Exception
        End Try
        Try
            lang176.Text = axlabs.GetASPXPage("pmSiteFM.aspx", "lang176")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub
    Private Sub undosite()
        sid = lblsid.Value
        cid = lblcid.Value
        sql = "delete from pmSiteFM where siteid = '" & sid & "'"
        fail.Update(sql)
        PopSiteFail(cid, sid)
        PopFail(cid)
        trmsg.Attributes.Add("class", "view")
        trmsg1.Attributes.Add("class", "details")
        btntocomp.Enabled = False
        btnfromcomp.Enabled = False
    End Sub
    Private Sub makesite()
        sid = lblsid.Value
        cid = lblcid.Value
        lblfailchk.Value = "site"
        sql = "usp_updateSiteFM '" & cid & "', '" & sid & "'"
        fail.Update(sql)
        PopSiteFail(cid, sid)
        PopFail(cid)
        trmsg.Attributes.Add("class", "details")
        trmsg1.Attributes.Add("class", "view")
        btntocomp.Enabled = True
        btnfromcomp.Enabled = True
    End Sub

    Private Sub cbprep_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbprep.CheckedChanged
        fail.Open()
        makesite()
        fail.Dispose()
    End Sub

    Private Sub cbunprep_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbunprep.CheckedChanged
        fail.Open()
        undosite()
        fail.Dispose()
    End Sub
End Class
