

'********************************************************
'*
'********************************************************



Public Class AppSetLocTab
    Inherits System.Web.UI.Page
	Protected WithEvents lang86 As System.Web.UI.WebControls.Label

	Protected WithEvents lang85 As System.Web.UI.WebControls.Label

	Protected WithEvents lang84 As System.Web.UI.WebControls.Label

	Protected WithEvents lang83 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sid As String
    Protected WithEvents ifdp As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifcl As System.Web.UI.HtmlControls.HtmlGenericControl
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txttab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents iflt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifl As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        cid = "0"
        sid = Request.QueryString("sid").ToString
        If cid <> "" Then
            lblcid.Value = cid
            lblsid.value = sid
            'iflt.Attributes.Add("src", "AppSetLT.aspx?cid=" + cid)
            'lbltab.Value = "lt"
            ifdp.Attributes.Add("src", "AppSetDeptTab.aspx?cid=" + cid + "&sid=" + sid)
            txttab.Value = "dp"
        End If
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang83.Text = axlabs.GetASPXPage("AppSetLocTab.aspx", "lang83")
        Catch ex As Exception
        End Try
        Try
            lang84.Text = axlabs.GetASPXPage("AppSetLocTab.aspx", "lang84")
        Catch ex As Exception
        End Try
        Try
            lang85.Text = axlabs.GetASPXPage("AppSetLocTab.aspx", "lang85")
        Catch ex As Exception
        End Try
        Try
            lang86.Text = axlabs.GetASPXPage("AppSetLocTab.aspx", "lang86")
        Catch ex As Exception
        End Try

    End Sub

End Class
