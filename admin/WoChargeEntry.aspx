<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WoChargeEntry.aspx.vb"
    Inherits="lucy_r12.WoChargeEntry" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WoChargeEntry</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/WoChargeEntryaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        function getnext() {
       
            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            //alert(cnt + "," + pg)
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        //-->
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table>
        <tr class="tbg">
            <td width="18">
            </td>
            <td width="18">
            </td>
            <td width="150">
            </td>
            <td width="18">
            </td>
            <td width="18">
            </td>
            <td width="20">
            </td>
            <td width="120">
            </td>
            <td width="150">
            </td>
            <td width="30">
            </td>
            <td width="60">
            </td>
            <td width="60">
            </td>
            <td width="20">
            </td>
        </tr>
        <tr class="tbg">
            <td>
                <asp:ImageButton ID="ttfirst" runat="server" ImageUrl="../images/appbuttons/minibuttons/tostartbg.gif">
                </asp:ImageButton>
            </td>
            <td>
                <asp:ImageButton ID="ttPrev" runat="server" ImageUrl="../images/appbuttons/minibuttons/prevarrowbg.gif">
                </asp:ImageButton>
            </td>
            <td align="center">
                <asp:Label ID="lblpgtasktype" runat="server" ForeColor="Blue" Font-Names="Arial"
                    Font-Size="X-Small"></asp:Label>
            </td>
            <td>
                <asp:ImageButton ID="ttNext" runat="server" ImageUrl="../images/appbuttons/minibuttons/nextarrowbg.gif">
                </asp:ImageButton>
            </td>
            <td>
                <asp:ImageButton ID="ttlast" runat="server" ImageUrl="../images/appbuttons/minibuttons/tolastbg.gif">
                </asp:ImageButton>
            </td>
            <td>
                &nbsp;
            </td>
            <td class="label">
                <asp:Label ID="lang194" runat="server">Search Charge Number</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="144px" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
            <td class="label">
                <asp:Label ID="lang195" runat="server">Filter By:</asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddfilt" runat="server" CssClass="plainlabel" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table id="ttdiv" cellspacing="0" cellpadding="0" width="900" class="tbg" runat="server">
        <tr class="tbg">
            <td style="height: 16px">
                <asp:Label ID="lbltasktype" runat="server" ForeColor="Red" Font-Names="Arial" Font-Size="X-Small"
                    Width="256px" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr class="tbg">
            <td valign="top" align="left">
                <asp:DataGrid ID="dgtt" runat="server" ShowFooter="True" CellSpacing="2" CellPadding="0"
                    GridLines="None" AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton26" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit This Task Type" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton27" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton28" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Charge Type">
                            <HeaderStyle Width="120px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label20" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.wochargetype") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddtypa" runat="server" CssClass="plainlabel" DataSource="<%# PopTypes %>"
                                    DataTextField="wochargetype" DataValueField="wochargetype">
                                </asp:DropDownList>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" DataSource="<%# PopTypes %>"
                                    DataTextField="wochargetype" DataValueField="wochargetype" SelectedIndex='<%# GetSelIndex(Container.DataItem("wochargetypeindex")) %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Charge Number">
                            <HeaderStyle Width="120px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.wocharge") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="Label2" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.wocharge") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp01">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp01" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp01") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp01a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp01") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp01" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp01") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp02">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp02" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp02") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp02a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp02") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp02" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp02") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp03">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp03" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp03") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp03a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp03") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp03" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp03") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp04">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp04" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp04") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp04a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp04") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp04" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp04") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp05">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp05" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp05") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp05a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp05") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp05" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp05") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp06">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp06" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp06") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp06a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp06") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp06" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp06") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp07">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp07" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp07") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp07a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp07") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp07" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp07") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp08">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp08" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp08") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp08a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp08") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp08" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp08") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp09">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp09" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp09") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp09a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp09") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp09" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp09") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Comp10">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblcomp10" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.comp10") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtcomp10a" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp10") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtcomp10" runat="server" Width="50px" MaxLength="50" CssClass="plainlabel"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.comp10") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="50px" CssClass="thdrsingg plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="btndel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblttids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wochid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wochid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid><br>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblold" type="hidden" name="lblold" runat="server">
    <input id="lblcols" type="hidden" runat="server">
    <input id="lbldatatype" type="hidden" runat="server">
    <input id="lblpgcnt" type="hidden" runat="server">
    <input type="hidden" id="lblfilt" runat="server">
    <input type="hidden" id="lblfiltd" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt"
        type="hidden" name="txtpgcnt" runat="server">
        <input type="hidden" id="lblret" runat="server" />
    </form>
</body>
</html>
