

'********************************************************
'*
'********************************************************



Public Class AppSetEqTabMain
    Inherits System.Web.UI.Page
	Protected WithEvents lang75 As System.Web.UI.WebControls.Label

	Protected WithEvents lang74 As System.Web.UI.WebControls.Label

	Protected WithEvents lang73 As System.Web.UI.WebControls.Label

	Protected WithEvents lang72 As System.Web.UI.WebControls.Label
    Dim mu As New mmenu_utils_a
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sid, ro, isecd, mdba, coi, cadm, uid As String
    Protected WithEvents ifcc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisecd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmdba As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ec As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents fa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents md As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifeq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifas As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iff As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txttab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim comi As New mmenu_utils_a
        Dim coi As String = comi.COMPI
        lblcoi.Value = coi
        GetFSLangs()
        Try
            mdba = mu.MDBI.ToString
        Catch ex As Exception
            mdba = 0
        End Try
        lblmdba.Value = mdba
        If mdba = 1 Then
            md.Attributes.Add("class", "view")
        Else
            md.Attributes.Add("class", "details")
        End If
        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        cadm = HttpContext.Current.Session("cadm").ToString()
        lblcadm.Value = cadm
        uid = HttpContext.Current.Session("uid").ToString()
        lbluid.Value = uid
        'Put user code to initialize the page here
        isecd = mu.ECD
        lblisecd.Value = isecd
        If isecd = "1" Then
            ec.Attributes.Add("class", "view")
        Else
            ec.Attributes.Add("class", "details")
        End If
        If coi = "CAS" Then
            fa.Attributes.Add("class", "details")
        Else
            fa.Attributes.Add("class", "view")
        End If
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        cid = "0"
        sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString()
        If cid <> "" Then
            lblcid.Value = cid
            lblsid.Value = sid
            'iflt.Attributes.Add("src", "AppSetLT.aspx?cid=" + cid)
            'lbltab.Value = "lt"
            ifeq.Attributes.Add("src", "AppSetEqTab.aspx?cid=" + cid + "&sid=" + sid)
            txttab.Value = "eq"
        End If
        Try
            ro = HttpContext.Current.Session("ro").ToString
        Catch ex As Exception
            ro = "0"
        End Try
        lblro.Value = ro
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang72.Text = axlabs.GetASPXPage("AppSetEqTabMain.aspx", "lang72")
        Catch ex As Exception
        End Try
        Try
            lang73.Text = axlabs.GetASPXPage("AppSetEqTabMain.aspx", "lang73")
        Catch ex As Exception
        End Try
        Try
            lang74.Text = axlabs.GetASPXPage("AppSetEqTabMain.aspx", "lang74")
        Catch ex As Exception
        End Try
        Try
            lang75.Text = axlabs.GetASPXPage("AppSetEqTabMain.aspx", "lang75")
        Catch ex As Exception
        End Try

    End Sub

End Class
