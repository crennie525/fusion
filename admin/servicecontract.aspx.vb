

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class servicecontract
    Inherits System.Web.UI.Page
    Protected WithEvents lang183 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim spName As String = "Paging_Cursor"
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 30
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim dr As SqlDataReader
    Dim sql As String
    Dim eq As New Utilities
    Dim typ, wo, pm, lang As String
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsrch1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgeq As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents lblpm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblds As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwo As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
        End Try
        lang = lblfslang.Value
        If lang = "fre" Then
            dgeq.Columns(1).HeaderText = "Contrat"
        End If
        Dim tst As String = Request.QueryString.ToString
        'utils.GetAscii(Me, tst, "not")
        If Not Me.IsPostBack Then
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            If typ = "wo" Then
                wo = Request.QueryString("wo").ToString
                lblwo.Value = wo
            End If

            eq.Open()
            LoadEq(PageNumber)
            eq.Dispose()
        End If
    End Sub
    Private Sub LoadEq(ByVal PageNumber As Integer)

        Dim lang As String = lblfslang.Value
        Dim srch As String = txtsrch.Text
        srch = eq.ModString2(srch)
        If Len(txtsrch.Text) > 0 Then
            Filter = "contract like '%" & srch & "%' " _
            + "or description like '%" & srch & "%' "
            FilterCNT = "where contract like '%" & srch & "%' " _
            + "or description like '%" & srch & "%' "
        Else
            Filter = ""
            FilterCNT = ""
        End If
        Dim cmd As New SqlCommand
        'If Len(srch) > 0 Then
        'cmd.CommandText = "exec usp_searchsc @srch"
        ''Dim param1 = New SqlParameter("@srch", SqlDbType.VarChar)
        'param1.Value = txtsrch.Text
        'cmd.Parameters.Add(param1)
        'Else
        '    cmd.CommandText = "exec usp_searchsc"
        'End If
        sql = "SELECT Count(*) FROM servicecontract " & FilterCNT
        Dim eqcnt As Integer
        eqcnt = eq.Scalar(sql) '.ScalarHack(cmd)
        dgeq.VirtualItemCount = eqcnt

        Tables = "servicecontract"
        PK = "contract"
        Sort = "contract desc"
        PageSize = "20"

        dr = eq.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        dgeq.DataSource = dr

        dgeq.DataBind()


        dr.Close()
        lblpg.Text = "Page " & dgeq.CurrentPageIndex + 1 & " of " & dgeq.PageCount
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub btnsrch1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsrch1.Click
        eq.Open()
        LoadEq(PageNumber)
        eq.Dispose()
    End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If ((dgeq.CurrentPageIndex) < (dgeq.PageCount - 1)) Then
            dgeq.CurrentPageIndex = dgeq.CurrentPageIndex + 1
            eq.Open()
            LoadEq(dgeq.CurrentPageIndex + 1)
            eq.Dispose()
            checkDataPgCnt()
        End If
        checkDataPgCnt()
    End Sub
    Private Sub checkDataPgCnt()

        If (dgeq.CurrentPageIndex) = 0 Or dgeq.PageCount = 1 Then
            'btnprev.Enabled = False
        Else
            'btnprev.Enabled = True
        End If
        If dgeq.PageCount > 1 And (dgeq.CurrentPageIndex + 1 < dgeq.PageCount) Then
            'btnnext.Enabled = True
        Else
            'btnnext.Enabled = False
        End If

    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If (dgeq.CurrentPageIndex > 0) Then
            dgeq.CurrentPageIndex = dgeq.CurrentPageIndex - 1
            eq.Open()
            LoadEq(dgeq.CurrentPageIndex - 1)
            eq.Dispose()
        End If
        checkDataPgCnt()
    End Sub

    Private Sub dgeq_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeq.ItemCommand
        If (e.CommandName = "Select") Then
            Dim eqs As String = e.Item.Cells(0).Text
            'Dim loc As String = e.Item.Cells(3).Text
            'Dim ds As String = e.Item.Cells(2).Text
            'Dim gl As String = e.Item.Cells(4).Text
            'lblch.Value = "1"
            lbleq.Value = eqs
            'lblds.Value = ds
            'lblgl.Value = gl
            typ = lbltyp.Value
            If typ = "wo" Then
                wo = lblwo.Value
                lblpm.Value = eqs
                'Dim cmd As New SqlCommand("exec usp_upwosc @wo, @sc")
                'Dim param0 = New SqlParameter("@wo", SqlDbType.VarChar)
                'param0.Value = wo
                'cmd.Parameters.Add(param0)
                'Dim param1 = New SqlParameter("@sc", SqlDbType.VarChar)
                'param1.Value = eqs
                'cmd.Parameters.Add(param1)
                lblch.Value = "2"
                sql = "update workorder set contract = '" & eqs & "' where wonum = '" & wo & "'"
                'sql = "update workorder set eqnum = '" & eqs & "', location = '" & loc & "', " _
                '+ "glaccount = '" & gl & "', problemcode = NULL, wo20 = 'EXPE' where wonum = '" & wo & "'"
                eq.Open()
                'eq.UpdateHack(cmd)
                eq.Update(sql)
                eq.Dispose()
            Else
                lblpm.Value = eqs
                lblch.Value = "2"
            End If

        End If
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang183.Text = axlabs.GetASPXPage("servicecontract.aspx", "lang183")
        Catch ex As Exception
        End Try

    End Sub

End Class
