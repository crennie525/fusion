<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetLocTab.aspx.vb"
    Inherits="lucy_r12.AppSetLocTab" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetLocTab</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/loctab.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetLocTabaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
    <!--
        var divflg;

        function checktab() {
            var app = document.getElementById("appchk").value;
            if (app == "switch") window.parent.handleapp(app);
            divflg = document.getElementById("txttab").value
            gettab(divflg);
        }
        function handledid(id) {
            document.getElementById("lbldid").value = id;
        }
        function gettab(id) {
            document.getElementById("dp").className = "thdr plainlabel";
            document.getElementById("dt").className = "thdr plainlabel";
            document.getElementById("cl").className = "thdr plainlabel";
            document.getElementById("lt").className = "thdr plainlabel";
            document.getElementById("l").className = "thdr plainlabel";
            if (id == "lt") {
                lttab();
            }
            else if (id == "l") {
                ltab();
            }
            else if (id == "dp") {
                dtab();
            }
            else if (id == "cl") {
                ctab();
            }
            else if (id == "dt") {
                dttab();
            }
        }


        function dtab() {
            var cd = document.getElementById("lblcid").value;
            var sd = document.getElementById("lblsid").value;
            document.getElementById("ifdp").src = "AppSetDeptTab.aspx?cid=" + cd + "&sid=" + sd;
            document.getElementById("txttab").value = "dp";
            document.getElementById("dp").className = "thdrhov plainlabel";
            document.getElementById("ltdiv").style.display = "none";
            document.getElementById("ltdiv").style.visibility = "hidden";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = "hidden";
            document.getElementById("cldiv").style.display = "none";
            document.getElementById("cldiv").style.visibility = "hidden";
            document.getElementById("dtdiv").style.display = 'none';
            document.getElementById("dtdiv").style.visibility = 'hidden';
            document.getElementById("dpdiv").style.display = 'block';
            document.getElementById("dpdiv").style.visibility = 'visible';
        }
        function ctab() {
            var cd = document.getElementById("lblcid").value;
            var sd = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldid").value;
            if (isNaN(parseInt(did))) did = "0";
            document.getElementById("ifcl").src = "AppSetCellTab.aspx?cid=" + cd + "&sid=" + sd + "&did=" + did;
            document.getElementById("txttab").value = "cl";
            document.getElementById("cl").className = "thdrhov plainlabel";
            document.getElementById("ltdiv").style.display = "none";
            document.getElementById("ltdiv").style.visibility = "hidden";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = "hidden";
            document.getElementById("dpdiv").style.display = "none";
            document.getElementById("dpdiv").style.visibility = "hidden";
            document.getElementById("dtdiv").style.display = 'none';
            document.getElementById("dtdiv").style.visibility = 'hidden';
            document.getElementById("cldiv").style.display = 'block';
            document.getElementById("cldiv").style.visibility = 'visible';
        }
        function ltab() {
            var cd = document.getElementById("lblcid").value;
            var sid = document.getElementById("lblsid").value
            document.getElementById("ifl").src = "AppSetSYS.aspx?cid=" + cd + "&sid=" + sid;
            document.getElementById("txttab").value = "l";
            document.getElementById("l").className = "thdrhov plainlabel";
            document.getElementById("ltdiv").style.display = "none";
            document.getElementById("ltdiv").style.visibility = "hidden";
            document.getElementById("dpdiv").style.display = "none";
            document.getElementById("dpdiv").style.visibility = "hidden";
            document.getElementById("cldiv").style.display = "none";
            document.getElementById("cldiv").style.visibility = "hidden";
            document.getElementById("dtdiv").style.display = 'none';
            document.getElementById("dtdiv").style.visibility = 'hidden';
            document.getElementById("ldiv").style.display = 'block';
            document.getElementById("ldiv").style.visibility = 'visible';
        }
        function lttab() {
            var cd = document.getElementById("lblcid").value;
            document.getElementById("iflt").src = "AppSetLT.aspx?cid=" + cd;
            document.getElementById("txttab").value = "lt";
            document.getElementById("lt").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("dpdiv").style.display = "none";
            document.getElementById("dpdiv").style.visibility = "hidden";
            document.getElementById("cldiv").style.display = "none";
            document.getElementById("cldiv").style.visibility = "hidden";
            document.getElementById("dtdiv").style.display = 'none';
            document.getElementById("dtdiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'block';
            document.getElementById("ltdiv").style.visibility = 'visible';
        }
        function dttab() {
            var cd = document.getElementById("lblcid").value;
            var sd = document.getElementById("lblsid").value;
            document.getElementById("ifdt").src = "AppSetDeptTrans.aspx?cid=" + sd;
            document.getElementById("txttab").value = "dt";
            document.getElementById("dt").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("dpdiv").style.display = "none";
            document.getElementById("dpdiv").style.visibility = "hidden";
            document.getElementById("cldiv").style.display = "none";
            document.getElementById("cldiv").style.visibility = "hidden";
            document.getElementById("ltdiv").style.display = 'none';
            document.getElementById("ltdiv").style.visibility = 'hidden';
            document.getElementById("dtdiv").style.display = 'block';
            document.getElementById("dtdiv").style.visibility = 'visible';
        }
	
	
	
	//-->
    </script>
</head>
<body onload="checktab();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div id="dpdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 5px; visibility: visible; border-left: 2px groove; width: 780px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="ifdp" style="width: 780px; height: 420px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="cldiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 5px; visibility: visible; border-left: 2px groove; width: 780px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="ifcl" style="width: 780px; height: 420px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="ltdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 5px; visibility: visible; border-left: 2px groove; width: 780px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="iflt" style="width: 780px; height: 420px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <div id="ldiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 780px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="ifl" style="width: 780px; height: 420px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
     <div id="dtdiv" style="border-right: 2px groove; border-top: 2px groove; z-index: 1;
        left: 6px; visibility: hidden; border-left: 2px groove; width: 900px; border-bottom: 2px groove;
        position: absolute; top: 32px; height: 340px; background-color: transparent">
        <iframe id="ifdt" style="width: 900px; height: 420px; background-color: transparent"
            src="" frameborder="no" runat="server" allowtransparency></iframe>
    </div>
    <table style="left: 5px; position: absolute; top: 10px" cellspacing="3" cellpadding="3">
        <tr height="20">
            <td id="dp" onclick="gettab('dp');" class="thdrhov plainlabel" width="100">
                <asp:Label ID="lang83" runat="server">Departments</asp:Label>
            </td>
             
            <td id="cl" onclick="gettab('cl');" class="thdr plainlabel" width="100">
                <asp:Label ID="lang84" runat="server">Station\Cells</asp:Label>
            </td>
            <td id="dt" onclick="gettab('dt');" class="thdrhov plainlabel" width="200">
                <asp:Label ID="Label1" runat="server">Department Equipment Transfer</asp:Label>
            </td>
            <td id="lt" onclick="gettab('lt');" class="thdr plainlabel" width="140">
                <asp:Label ID="lang85" runat="server">Location Types</asp:Label>
            </td>
            <td id="l" onclick="gettab('l');" class="thdr plainlabel" width="140">
                <asp:Label ID="lang86" runat="server">Location Systems</asp:Label>
            </td>
        </tr>
    </table>
    <input type="hidden" id="txttab" runat="server" name="appchk">
    <input type="hidden" id="lbltab" runat="server" name="appchk">
    <input type="hidden" id="lblcid" runat="server" name="appchk">
    <input type="hidden" id="lbltl" runat="server" name="appchk">
    <input type="hidden" id="appchk" runat="server" name="appchk"><input type="hidden"
        id="lblsid" runat="server">
    <input type="hidden" id="lbldid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
