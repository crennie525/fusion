﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="appsetocareas.aspx.vb"
    Inherits="lucy_r12.appsetocareas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" src="../scripts/gridnav.js" type="text/javascript"></script>
    <script language="JavaScript" src="../scripts/overlib1.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td width="80">
                </td>
                <td width="200">
                </td>
                <td width="20">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblcom" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"
                        ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="bluelabel">
                    <asp:Label ID="lang40" runat="server">Search</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtsrch" runat="server" Width="190px"></asp:TextBox>
                </td>
                <td>
                    <asp:ImageButton ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
                        CssClass="imgbutton"></asp:ImageButton>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:DataGrid ID="dgfail" runat="server" ShowFooter="True" CellPadding="0" GridLines="None"
                        AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" CellSpacing="1"
                        BackColor="transparent">
                        <FooterStyle BackColor="transparent"></FooterStyle>
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="50px" Height="20px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="Imagebutton10" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        ToolTip="Edit This Failure Mode" CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                        CommandName="Add"></asp:ImageButton>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="Imagebutton11" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton15" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="Failure Mode">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.oaid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="Textbox9" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.oaid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Description">
                                <HeaderStyle Width="156px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ocarea") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewfm" runat="server" MaxLength="50" NAME="Textbox1">
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:TextBox ID="Textbox10" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.ocarea") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Remove">
                                <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="Imagebutton17" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                        border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" width="20">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td width="20">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" class="details">
                    <img id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
                        onclick="getss();" onmouseout="return nd()" height="20" src="../images/appbuttons/minibuttons/plusminus.gif"
                        width="20">
                </td>
            </tr>
        </table>
        <input id="lblcid" type="hidden" runat="server"><input id="appchk" type="hidden"
            name="appchk" runat="server">
        <input id="lblsid" type="hidden" runat="server"><input id="lbllog" type="hidden"
            runat="server">
        <input type="hidden" id="lblofm" runat="server"><input type="hidden" id="txtpg" runat="server"
            name="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" name="txtpgcnt">
        <input type="hidden" id="lblret" runat="server" name="lblret">
        <input type="hidden" id="lblro" runat="server">
    </div>
    </form>
</body>
</html>
