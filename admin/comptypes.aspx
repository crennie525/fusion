<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="comptypes.aspx.vb" Inherits="lucy_r12.comptypes" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>comptypes</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../css/menu.css" type="text/css" rel="stylesheet" />
    <link href="../css/maxcss.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/comptypesaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg">
    <form id="form1" method="post" runat="server">
    <table style="left: 0px; position: absolute; top: 0px">
        <tr>
            <td width="70">
            </td>
            <td width="100">
            </td>
            <td width="70">
            </td>
            <td width="60">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="lblpre" runat="server" ForeColor="Red" Width="304px" Font-Bold="True"
                    Font-Names="Arial" Font-Size="X-Small"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:DataGrid ID="dgskill" runat="server" ShowFooter="True" CellPadding="0" GridLines="None"
                    AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle BackColor="#E7F1FD"></AlternatingItemStyle>
                    <ItemStyle Font-Size="X-Small" Font-Names="Arial" BackColor="transparent"></ItemStyle>
                    <HeaderStyle CssClass="tblmenurt"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="60px"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../images/lilpen2.gif"
                                    CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="../images/liltabcan.gif"
                                    CommandName="Reset"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../images/SAVEDisk.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="../images/liltabcan.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Type">
                            <HeaderStyle Width="100px"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label24" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtwsa" runat="server" Width="94px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="txtws" runat="server" Width="94px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.value") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Description">
                            <HeaderStyle Width="100px"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.valdesc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtdesca" runat="server" Width="194px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.valdesc") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="txtdesc" runat="server" Width="194px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.valdesc") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete">
                            <HeaderStyle Width="50px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/liltabdel2.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Center"></FooterStyle>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../images/addnew.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td class="hdr" id="tdpg" runat="server" colspan="2">
            </td>
            <td align="right" colspan="2">
                <asp:Button ID="btnprev" runat="server" Height="24px" Text="Prev" CssClass="lilmenu plainlabel">
                </asp:Button><asp:Button ID="btnnext" runat="server" Height="24px" Text="Next" CssClass="lilmenu plainlabel">
                </asp:Button>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" runat="server" name="lblcid">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
