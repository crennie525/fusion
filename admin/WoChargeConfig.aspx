<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WoChargeConfig.aspx.vb"
    Inherits="lucy_r12.WoChargeConfig" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WoChargeConfig</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/WoChargeConfigaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table id="ttdiv" cellspacing="0" cellpadding="0" width="680" class="tbg" runat="server"
        style="left: 5px; position: absolute; top: 0px">
        <tr class="tbg">
            <td width="480">
            </td>
            <td width="70">
            </td>
            <td width="60">
            </td>
            <td width="70">
            </td>
        </tr>
        <tr class="tbg">
            <td style="height: 16px" colspan="4">
                <asp:Label ID="lbltasktype" runat="server" Font-Size="X-Small" Font-Names="Arial"
                    Font-Bold="True" Width="256px" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr class="tbg">
            <td valign="top" align="center" colspan="4">
                <asp:DataGrid ID="dgtt" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                    AllowPaging="True" GridLines="None" CellPadding="0" CellSpacing="2" ShowFooter="True"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton26" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    ToolTip="Edit This Task Type" CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="Imagebutton27" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton28" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblttids" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wccid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblttide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wccid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Field Name">
                            <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbloldfld" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.fldname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewtt" runat="server" CssClass="plainlabel" Width="150px" MaxLength="50"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.fldname") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtnewfld" runat="server" CssClass="plainlabel" Width="150px" MaxLength="50"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.fldname") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lbloldo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fldorder") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lbloldord" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fldorder") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Field Order">
                            <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblfldorder" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fldorder") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtorder" runat="server" Width="40px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.fldorder") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Data Type">
                            <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.datatype") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddtypf" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="INT">INT</asp:ListItem>
                                    <asp:ListItem Value="ALN">ALN</asp:ListItem>
                                </asp:DropDownList>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddtype" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("dataindex")) %>'>
                                    <asp:ListItem Value="INT">INT</asp:ListItem>
                                    <asp:ListItem Value="ALN">ALN</asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Length">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.length") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtlenf" runat="server" Width="40px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.length") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtlene" runat="server" Width="40px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.length") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Required?">
                            <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbaci" Enabled="False" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.required") %>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                            <FooterTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbreqf" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.required") %>'>
                                </asp:CheckBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbreqe" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.required") %>'>
                                </asp:CheckBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delimiter">
                            <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label3" runat="server" CssClass="plainlabel" Text='<%# DataBinder.Eval(Container, "DataItem.delimiter") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="dddelimf" runat="server" CssClass="plainlabel">
                                    <asp:ListItem Value="None">None</asp:ListItem>
                                    <asp:ListItem Value="/ ">/  slash</asp:ListItem>
                                    <asp:ListItem Value="-">- dash</asp:ListItem>
                                    <asp:ListItem Value="*">* asterick</asp:ListItem>
                                    <asp:ListItem Value=".">. period</asp:ListItem>
                                </asp:DropDownList>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="dddelim" runat="server" CssClass="plainlabel" SelectedIndex='<%# GetSelIndex(Container.DataItem("delimindex")) %>'>
                                    <asp:ListItem Value="None">None</asp:ListItem>
                                    <asp:ListItem Value="/ ">/  slash</asp:ListItem>
                                    <asp:ListItem Value="-">- dash</asp:ListItem>
                                    <asp:ListItem Value="*">* asterick</asp:ListItem>
                                    <asp:ListItem Value=".">. period</asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="To DB?">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="Checkbox1" Enabled="False" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.required") %>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                            <FooterTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbdbf" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.required") %>'>
                                </asp:CheckBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:CheckBox ID="cbdbe" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.required") %>'>
                                </asp:CheckBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="btndel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid><br>
            </td>
        </tr>
    </table>
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="lblold" type="hidden" name="lblold" runat="server">
    <input type="hidden" id="lbloldorder" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
