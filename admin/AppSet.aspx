<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSet.aspx.vb" Inherits="lucy_r12.AppSet" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title id="pgtitle">AppSet</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript" src="../scripts/appset_3.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
</head>
<body class="tbg" onload="checktab();pageScroll();">
    <form id="form1" method="post" runat="server">
    <table style="z-index: 1; position: absolute; top: 84px; left: 4px" cellspacing="0"
        cellpadding="1" width="1040">
        <tr>
            <td class="thdrsinglft" width="26">
                <img src="../images/appbuttons/minibuttons/pm3.gif" border="0" alt="" />
            </td>
            <td class="thdrsingrt label" align="left" width="874">
                <asp:Label ID="lang1" runat="server">PM Application Set-Up</asp:Label>
            </td>
        </tr>
    </table>
    <table style="z-index: 1; position: absolute; top: 112px; left: 4px" cellspacing="2"
        cellpadding="1" width="1040">
        <tr>
            <td>
                <img src="../images/appbuttons/minibuttons/6PX.gif" width="6" height="6" alt="" />
            </td>
        </tr>
        <tr>
            <td height="20" class="thdr plainlabel" id="loctab" onclick="gettab('loc');" width="90">
                <asp:Label ID="lang2" runat="server">Locations</asp:Label>
            </td>
            <td class="thdr plainlabel" id="eqtab" onclick="gettab('eq');" width="110">
                <asp:Label ID="lang3" runat="server">Equipment</asp:Label>
            </td>
            <td class="thdr plainlabel" id="tasktab" onclick="gettab('task');" width="100">
                <asp:Label ID="lang4" runat="server">PM Tasks</asp:Label>
            </td>
            <td class="thdr plainlabel" id="invtab" onclick="gettab('inv');" width="100">
                <asp:Label ID="lang5" runat="server">Inventory</asp:Label>
            </td>
            <td class="thdr plainlabel" id="worktab" onclick="gettab('work');" width="100">
                <asp:Label ID="lang6" runat="server">Work Orders</asp:Label>
            </td>
            <td class="thdr plainlabel" id="chargetab" onclick="gettab('charge');" width="100">
                <asp:Label ID="lang7" runat="server">Charge Numbers</asp:Label>
            </td>
            <td class="thdr plainlabel" id="labortab" onclick="gettab('labor');" width="100">
                <asp:Label ID="lang8" runat="server">Labor</asp:Label>
            </td>
            <td class="thdr plainlabel" id="comptab" onclick="gettab('comp');" width="100">
                <asp:Label ID="lang9" runat="server">Companies</asp:Label>
            </td>
            <td class="details" id="emtab" onclick="gettab('em');" width="100">
                <asp:Label ID="lang10" runat="server">Email Service</asp:Label>
            </td>
            <td class="thdr plainlabel" id="lttab" onclick="gettab('lt');" width="100">
                <asp:Label ID="lang11" runat="server">Login Tracking</asp:Label>
            </td>
        </tr>
    </table>
    <div id="deptdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; background-color: transparent; width: 1040px; height: 460px;
        visibility: hidden; border-top: 2px groove; top: 150px; border-right: 2px groove;
        left: 4px">
        <iframe id="ifdept" style="background-color: transparent; width: 700px; height: 460px"
            name="ifdept" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="celldiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; visibility: hidden; border-top: 2px groove;
        top: 1120px; border-right: 2px groove; left: 4px">
        <iframe id="ifcell" style="background-color: transparent; width: 700px; height: 460px"
            name="ifcell" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="eqdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 1660px; border-right: 2px groove; left: 4px">
        <iframe id="ifeq" style="background-color: transparent; width: 1040px; height: 460px"
            name="ifeq" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="comdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2160px; border-right: 2px groove; left: 4px">
        <iframe id="ifcom" style="background-color: transparent; width: 700px; height: 460px"
            name="ifcom" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="taskdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="iftask" style="background-color: transparent; width: 800px; height: 460px"
            name="iftask" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="locdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="ifloc" style="background-color: transparent; width: 1040px; height: 460px"
            name="iftask" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="wodiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="ifwo" style="background-color: transparent; width: 1040px; height: 460px"
            name="iftask" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="chargediv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="ifcharge" style="background-color: transparent; width: 890px; height: 460px"
            name="iftask" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="labordiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="iflabor" style="background-color: transparent; width: 1030px; height: 460px"
            name="iflabor" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="invdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="ifinv" style="background-color: transparent; width: 890px; height: 460px"
            name="ifinv" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="compdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="ifcomp" style="background-color: transparent; width: 890px; height: 460px"
            name="ifinv" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="emdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="Iframe1" style="background-color: transparent; width: 1020px; height: 460px"
            name="ifem" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <div id="ltdiv" style="z-index: 1; border-bottom: 2px groove; position: absolute;
        border-left: 2px groove; width: 1040px; height: 460px; border-top: 2px groove;
        top: 2660px; border-right: 2px groove; left: 4px">
        <iframe id="iflt" style="background-color: transparent; width: 1020px; height: 460px"
            name="iflt" src="" frameborder="0" runat="server" allowtransparency></iframe>
    </div>
    <input type="hidden" id="xCoordHolder" runat="server" />
    <input type="hidden" id="yCoordHolder" runat="server" name="Hidden1" /><input id="lblsid"
        type="hidden" runat="server" /><input id="lbldid" type="hidden" runat="server" />
    <input id="lbltab" type="hidden" runat="server" /><input id="lblcid" type="hidden"
        runat="server" />
    <input type="hidden" id="lblfslang" runat="server" /><input type="hidden" id="lblret"
        runat="server" />
    <input type="hidden" id="lblretab" runat="server" />
    </form>
    <uc1:mmenu1 ID="Mmenu11" runat="server"></uc1:mmenu1>
</body>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
</html>
