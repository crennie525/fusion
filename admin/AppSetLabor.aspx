<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetLabor.aspx.vb" Inherits="lucy_r12.AppSetLabor" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetLabor</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .details
        {
            background-color: white;
            display: none;
            font-family: Verdana;
            visibility: hidden;
        }
    </style>
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetLaboraspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        <!--
        function chkscroll() {
            window.parent.pageScroll();
        }
        function getbig() {
            window.parent.getbig();
        }

        function changeImg(id, img) {
            var chk = document.getElementById("txttab").value;
            if (chk != id) {
                document.getElementById(id).src = img;
            }
        }
        function setref() {
            window.parent.setref();
        }
        var divflg;
        function handlechange() {

        }
        function ltab() {
            var cd = document.getElementById("lblcid").value;
            var ro = document.getElementById("lblro").value;
            document.getElementById("ifl").src = "../labor/PMSuperMan.aspx?cid=" + cd + "&who=super&ro=" + ro;
            document.getElementById("txttab").value = "l";
            document.getElementById("l").className = "thdrhov plainlabel";
            document.getElementById("ltdiv").style.display = "none";
            document.getElementById("ltdiv").style.visibility = "hidden";
            document.getElementById("ldiv").style.display = 'block';
            document.getElementById("ldiv").style.visibility = 'visible';
            document.getElementById("pdiv").style.display = 'none';
            document.getElementById("pdiv").style.visibility = 'hidden';
            document.getElementById("padiv").style.display = 'none';
            document.getElementById("padiv").style.visibility = 'hidden';
            document.getElementById("lwdiv").style.display = "none";
            document.getElementById("lwdiv").style.visibility = 'hidden';
            document.getElementById("ssdiv").style.display = 'none';
            document.getElementById("ssdiv").style.visibility = 'hidden';
        }
        function ptab() {
            //alert("Planner Administration is Temporarily Disabled")
            var cd = document.getElementById("lblcid").value;
            var ro = document.getElementById("lblro").value;
            document.getElementById("ifp").src = "../labor/PMSuperMan.aspx?cid=" + cd + "&who=planner&ro=" + ro;
            document.getElementById("txttab").value = "p";
            document.getElementById("p").className = "thdrhov plainlabel";
            document.getElementById("ltdiv").style.display = "none";
            document.getElementById("ltdiv").style.visibility = "hidden";
            document.getElementById("ldiv").style.display = 'none';
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("pdiv").style.display = 'block';
            document.getElementById("pdiv").style.visibility = 'visible';
            document.getElementById("padiv").style.display = 'none';
            document.getElementById("padiv").style.visibility = 'hidden';
            document.getElementById("lwdiv").style.display = "none";
            document.getElementById("lwdiv").style.visibility = 'hidden';
            document.getElementById("ssdiv").style.display = 'none';
            document.getElementById("ssdiv").style.visibility = 'hidden';
        }
        function lttab() {
            var cd = document.getElementById("lblcid").value;
            var ro = document.getElementById("lblro").value;
            document.getElementById("iflt").src = "../labor/PMLeadMan.aspx?x=no&cid=" + cd + " &ro=" + ro;
            document.getElementById("txttab").value = "lt";
            document.getElementById("lt").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'block';
            document.getElementById("ltdiv").style.visibility = 'visible';
            document.getElementById("pdiv").style.display = 'none';
            document.getElementById("pdiv").style.visibility = 'hidden';
            document.getElementById("padiv").style.display = 'none';
            document.getElementById("padiv").style.visibility = 'hidden';
            document.getElementById("lwdiv").style.display = "none";
            document.getElementById("lwdiv").style.visibility = 'hidden';
            document.getElementById("ssdiv").style.display = 'none';
            document.getElementById("ssdiv").style.visibility = 'hidden';
        }

        function stab() {
            var cd = document.getElementById("lblcid").value;
            var ro = document.getElementById("lblro").value;
            //document.getElementById("iflt").src="../labor/PMLeadMan.aspx?cid=" + cd + " &ro=" + ro;
            document.getElementById("txttab").value="s";
            document.getElementById("s").className="thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'none';
            document.getElementById("ltdiv").style.visibility = 'hidden';
            document.getElementById("pdiv").style.display = 'none';
            document.getElementById("pdiv").style.visibility = 'hidden';
            document.getElementById("padiv").style.display = 'none';
            document.getElementById("padiv").style.visibility = 'hidden';
            document.getElementById("lwdiv").style.display = "none";
            document.getElementById("lwdiv").style.visibility = 'hidden';
            document.getElementById("ssdiv").style.display = 'none';
            document.getElementById("ssdiv").style.visibility = 'hidden';
            //window.open("../labor/labshedmain.aspx");
            //alert("Not Available in Demo Mode")
            window.showModalDialog("../labor/labschedmaindialog.aspx", "", "dialogHeight:750px; dialogWidth:1040px; resizable=yes");
        }

        function wtab() {
            var cd = document.getElementById("lblcid").value;
            var ro = document.getElementById("lblro").value;
            //document.getElementById("iflt").src="../labor/PMLeadMan.aspx?cid=" + cd + " &ro=" + ro;
            document.getElementById("txttab").value="w";
            document.getElementById("w").className="thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'none';
            document.getElementById("ltdiv").style.visibility = 'hidden';
            document.getElementById("pdiv").style.display = 'none';
            document.getElementById("pdiv").style.visibility = 'hidden';
            document.getElementById("padiv").style.display = 'none';
            document.getElementById("padiv").style.visibility = 'hidden';
            document.getElementById("lwdiv").style.display = "none";
            document.getElementById("lwdiv").style.visibility = 'hidden';
            document.getElementById("ssdiv").style.display = 'none';
            document.getElementById("ssdiv").style.visibility = 'hidden';
            //window.open("../labor/workareas.aspx");
            window.showModalDialog("../labor/workareasdialog.aspx", "", "dialogHeight:750px; dialogWidth:860px; resizable=yes");
            //alert("Not Available in Demo Mode")
        }

        function lwtab() {
            var cd = document.getElementById("lblcid").value;
            var ro = document.getElementById("lblro").value;
            document.getElementById("iflw").src = "../labor/labweeks.aspx?x=no&cid=" + cd + " &ro=" + ro;
            document.getElementById("txttab").value = "lw";
            document.getElementById("lw").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'none';
            document.getElementById("ltdiv").style.visibility = 'hidden';
            document.getElementById("pdiv").style.display = 'none';
            document.getElementById("pdiv").style.visibility = 'hidden';
            document.getElementById("padiv").style.display = 'none';
            document.getElementById("padiv").style.visibility = 'hidden';
            document.getElementById("lwdiv").style.display = "block";
            document.getElementById("lwdiv").style.visibility = 'visible';
            document.getElementById("ssdiv").style.display = 'none';
            document.getElementById("ssdiv").style.visibility = 'hidden';
        }

        function patab() {
            var cd = document.getElementById("lblcid").value;
            var ro = document.getElementById("lblro").value;
            document.getElementById("ifpa").src = "../labor/planarea.aspx?x=no&cid=" + cd + " &ro=" + ro;
            document.getElementById("txttab").value = "pa";
            document.getElementById("pa").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'none';
            document.getElementById("ltdiv").style.visibility = 'hidden';
            document.getElementById("pdiv").style.display = 'none';
            document.getElementById("pdiv").style.visibility = 'hidden';
            document.getElementById("lwdiv").style.display = 'none';
            document.getElementById("lwdiv").style.visibility = 'hidden';
            document.getElementById("padiv").style.display = "block";
            document.getElementById("padiv").style.visibility = 'visible';
            document.getElementById("ssdiv").style.display = 'none';
            document.getElementById("ssdiv").style.visibility = 'hidden';
        }

        function sstab() {
            var sid = document.getElementById("lblsid").value;
            var ro = document.getElementById("lblro").value;
            document.getElementById("ifss").src = "../labor/super_replace.aspx?sid=" + sid;
            document.getElementById("txttab").value = "ss";
            document.getElementById("ss").className = "thdrhov plainlabel";
            document.getElementById("ldiv").style.display = "none";
            document.getElementById("ldiv").style.visibility = 'hidden';
            document.getElementById("ltdiv").style.display = 'none';
            document.getElementById("ltdiv").style.visibility = 'hidden';
            document.getElementById("ssdiv").style.display = 'block';
            document.getElementById("ssdiv").style.visibility = 'visible';
            document.getElementById("pdiv").style.display = 'none';
            document.getElementById("pdiv").style.visibility = 'hidden';
            document.getElementById("lwdiv").style.display = 'none';
            document.getElementById("lwdiv").style.visibility = 'hidden';
            document.getElementById("padiv").style.display = "none";
            document.getElementById("padiv").style.visibility = 'hidden';
        }

        function gettab(id) {
            var issched = document.getElementById("lblissched").value;
            //if (id != "s" && id != "w") {
                document.getElementById("lt").className = "thdr plainlabel";
                document.getElementById("l").className = "thdr plainlabel";
                document.getElementById("p").className = "thdr plainlabel";
                document.getElementById("w").className = "thdr plainlabel";
                document.getElementById("pa").className = "thdr plainlabel";
                document.getElementById("ss").className = "thdr plainlabel";
                if (issched == "1") {
                    document.getElementById("lw").className = "thdr plainlabel";
                    document.getElementById("s").className = "thdr plainlabel";
                }
            //}
            if (id == "lt") {
                lttab();
            }
            else if (id == "l") {
                ltab();
            }
            else if (id == "p") {
                ptab();
            }
            else if (id == "s") {
                stab();
            }
            else if (id == "w") {
                wtab();
            }
            else if (id == "lw") {
                lwtab();
            }
            else if (id == "pa") {
                patab();
            }
            else if (id == "ss") {
                sstab();
            }
        }
        function checktab() {
            var app = document.getElementById("appchk").value;
            if (app == "switch") window.parent.handleapp(app);
            divflg = document.getElementById("txttab").value
            gettab(divflg);
        }
        function pageScroll() {
            window.parent.pageScroll();
            //scrolldelay = setTimeout('pageScroll()', 200); 
        } 
            //-->
    </script>
</head>
<body onload="checktab();pageScroll();" class="tbg">
    <form id="form1" method="post" runat="server">
    <div id="ltdiv" style="z-index: 1; top: 42px;
        left: 6px; width: 1020px; position: absolute;" >
        <iframe id="iflt" style="background-color: white; width: 1020px; height: 400px"
            src="" frameborder="0" runat="server" ></iframe>
    </div>
    <div id="ldiv" style="z-index: 1; background-color: white; width: 1020px; height: 400px;
        visibility: hidden; top: 42px; position: absolute;
        left: 6px">
        <iframe id="ifl" style="background-color: white; width: 1020px; height: 400px"
            src="" frameborder="0" runat="server" ></iframe>
    </div>
    <div id="pdiv" style="z-index: 1; background-color: white; width: 1020px; height: 400px;
        visibility: hidden; top: 42px; position: absolute;
        left: 6px">
        <iframe id="ifp" style="background-color: white; width: 1020px; height: 400px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="sdiv" style="z-index: 1; background-color: white; width: 1020px; height: 400px;
        visibility: hidden; top: 42px; position: absolute;
        left: 6px">
        <iframe id="ifs" style="background-color: white; width: 1020px; height: 400px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="lwdiv" style="z-index: 1; background-color: white; width: 1020px; height: 400px;
        visibility: hidden; top: 42px; position: absolute;
        left: 6px">
        <iframe id="iflw" style="background-color: white; width: 1020px; height: 400px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="padiv" style="z-index: 1; background-color: white; width: 1020px; height: 400px;
        visibility: hidden; top: 42px; position: absolute;
        left: 6px">
        <iframe id="ifpa" style="background-color: white; width: 1020px; height: 400px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <div id="ssdiv" style="z-index: 1; background-color: white; width: 1020px; height: 400px;
        visibility: hidden; top: 42px; left: 6px; position: absolute;" >
        <iframe id="ifss" style="background-color: white; width: 1020px; height: 400px"
            src="" frameborder="0" runat="server"></iframe>
    </div>
    <table style="position: absolute; top: 5px; left: 6px" cellspacing="3" cellpadding="3">
        <tr>
            <td id="lt" onclick="gettab('lt');" class="thdrhov plainlabel" width="100" height="20">
                <asp:Label ID="lang80" runat="server">Lead Crafts</asp:Label>
            </td>
            <td id="l" onclick="gettab('l');" class="thdr plainlabel" width="100">
                <asp:Label ID="lang81" runat="server">Supervision</asp:Label>
            </td>
            <td id="p" onclick="gettab('p');" class="thdr plainlabel" width="100">
                <asp:Label ID="lang82" runat="server">Planners</asp:Label>
            </td>
            <td id="w" onclick="gettab('w');" class="thdr plainlabel" width="100">
                <asp:Label ID="Label2" runat="server">Work Areas</asp:Label>
            </td>
            <td id="pa" onclick="gettab('pa');" class="thdr plainlabel" width="100">
                <asp:Label ID="Label4" runat="server">Planning Areas</asp:Label>
            </td>
            <td id="lw" runat="server" onclick="gettab('lw');" class="thdr plainlabel" width="100">
                <asp:Label ID="Label3" runat="server">Labor Weeks</asp:Label>
            </td>
            <td id="s" runat="server" onclick="gettab('s');" class="thdr plainlabel" width="100">
                <asp:Label ID="Label1" runat="server">Scheduling</asp:Label>
            </td>
            <td id="ss" runat="server" onclick="gettab('ss');" class="thdr plainlabel" width="130">
                <asp:Label ID="Label5" runat="server">Supervisor Replace</asp:Label>
            </td>
        </tr>
    </table>
    <input type="hidden" id="txttab" runat="server" name="appchk">
    <input type="hidden" id="lbltab" runat="server" name="appchk">
    <input type="hidden" id="lblcid" runat="server" name="appchk">
    <input type="hidden" id="lbltl" runat="server" name="appchk">
    <input type="hidden" id="appchk" runat="server" name="appchk"><input type="hidden"
        id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblissched" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    </form>
</body>
</html>
