

'********************************************************
'*
'********************************************************






Public Class AppSetTaskTab
    Inherits System.Web.UI.Page
	Protected WithEvents lang108 As System.Web.UI.WebControls.Label

	Protected WithEvents lang107 As System.Web.UI.WebControls.Label

	Protected WithEvents lang106 As System.Web.UI.WebControls.Label

	Protected WithEvents lang105 As System.Web.UI.WebControls.Label

	Protected WithEvents lang104 As System.Web.UI.WebControls.Label

	Protected WithEvents lang103 As System.Web.UI.WebControls.Label

	Protected WithEvents lang102 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

   
    
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txttab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.WebControls.Label
    Protected WithEvents dgfreq As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addfreq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgfreq As System.Web.UI.WebControls.Label
    Protected WithEvents freqPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents freqNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblmaint As System.Web.UI.WebControls.Label
    Protected WithEvents dgmain As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addmain As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newmain As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgmaint As System.Web.UI.WebControls.Label
    Protected WithEvents maintPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents maintNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpre As System.Web.UI.WebControls.Label
    Protected WithEvents dgpre As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addpre As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newpre As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgpre As System.Web.UI.WebControls.Label
    Protected WithEvents prePrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents preNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblskill As System.Web.UI.WebControls.Label
    Protected WithEvents dgskill As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addskill As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newskill As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgskill As System.Web.UI.WebControls.Label
    Protected WithEvents skillPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents skillNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblstat As System.Web.UI.WebControls.Label
    Protected WithEvents dgstat As System.Web.UI.WebControls.DataGrid
    Protected WithEvents addstat As System.Web.UI.WebControls.ImageButton
    Protected WithEvents newstat As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpgstat As System.Web.UI.WebControls.Label
    Protected WithEvents statPrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents statNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ifpt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifst As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifet As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iffr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents iftt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim cid As String
    Protected WithEvents ifpmadj As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iftpmadj As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
       
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        cid = "0"
        If cid <> "" Then
            lblcid.Value = cid
            cid = lblcid.Value
            iftt.Attributes.Add("src", "AppSetTaskTabTT.aspx?cid=" + cid)
            lbltab.Value = "tt"
        End If
    End Sub
    

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang102.Text = axlabs.GetASPXPage("AppSetTaskTab.aspx", "lang102")
        Catch ex As Exception
        End Try
        Try
            lang103.Text = axlabs.GetASPXPage("AppSetTaskTab.aspx", "lang103")
        Catch ex As Exception
        End Try
        Try
            lang104.Text = axlabs.GetASPXPage("AppSetTaskTab.aspx", "lang104")
        Catch ex As Exception
        End Try
        Try
            lang105.Text = axlabs.GetASPXPage("AppSetTaskTab.aspx", "lang105")
        Catch ex As Exception
        End Try
        Try
            lang106.Text = axlabs.GetASPXPage("AppSetTaskTab.aspx", "lang106")
        Catch ex As Exception
        End Try
        Try
            lang107.Text = axlabs.GetASPXPage("AppSetTaskTab.aspx", "lang107")
        Catch ex As Exception
        End Try
        Try
            lang108.Text = axlabs.GetASPXPage("AppSetTaskTab.aspx", "lang108")
        Catch ex As Exception
        End Try

    End Sub

End Class
