

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class appsetwovars
    Inherits System.Web.UI.Page
	Protected WithEvents lang133 As System.Web.UI.WebControls.Label

	Protected WithEvents lang132 As System.Web.UI.WebControls.Label

	Protected WithEvents lang131 As System.Web.UI.WebControls.Label

	Protected WithEvents lang130 As System.Web.UI.WebControls.Label

	Protected WithEvents lang129 As System.Web.UI.WebControls.Label

	Protected WithEvents lang128 As System.Web.UI.WebControls.Label

	Protected WithEvents lang127 As System.Web.UI.WebControls.Label

	Protected WithEvents lang126 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim lr As New Utilities
    Dim ap As New AppUtils
    Dim tentry, acost, ecost, icost, ro, pdt, ttt, pdtt, tttt As String
    Protected WithEvents Tr3 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents rbdt As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbttt As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbdtt As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbtttt As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbptl As System.Web.UI.WebControls.RadioButtonList
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rblv As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents trp As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents rblca As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rblce As System.Web.UI.WebControls.RadioButtonList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                rblv.Enabled = False
                rblca.Enabled = False
                rblce.Enabled = False
                rbptl.Enabled = False
                rbdt.Enabled = False
                rbttt.Enabled = False

                rbdtt.Enabled = False
                rbtttt.Enabled = False
            End If
            tentry = ap.TimeEntry
            rblv.SelectedValue = tentry
            acost = ap.ActCost
            rblca.SelectedValue = acost
            ecost = ap.EstCost
            rblce.SelectedValue = ecost
            icost = ap.InvEntry
            rbptl.SelectedValue = icost
            pdt = ap.PDTEntry
            rbdt.SelectedValue = pdt
            ttt = ap.TTTEntry
            rbttt.SelectedValue = ttt

            pdtt = ap.PDTTEntry
            rbdtt.SelectedValue = pdtt
            tttt = ap.TTTTEntry
            rbtttt.SelectedValue = tttt
        End If
    End Sub
    Private Sub UpVars()
        tentry = rblv.SelectedValue
        acost = rblca.SelectedValue
        ecost = rblce.SelectedValue
        pdt = rbdt.SelectedValue

    End Sub

    Private Sub rblv_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblv.SelectedIndexChanged
        tentry = rblv.SelectedValue
        sql = "update wovars set [default] = '" & tentry & "' where [column] = 'timeentry'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr144" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub

    Private Sub rblca_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblca.SelectedIndexChanged
        acost = rblca.SelectedValue
        sql = "update wovars set [default] = '" & acost & "' where [column] = 'actlabcost'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr145" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub

    Private Sub rblce_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblce.SelectedIndexChanged
        ecost = rblca.SelectedValue
        sql = "update wovars set [default] = '" & ecost & "' where [column] = 'estlabcost'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr146" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub

    Private Sub rbptl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbptl.SelectedIndexChanged
        icost = rbptl.SelectedValue
        sql = "update wovars set [default] = '" & icost & "' where [column] = 'invuse'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr147" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub

    Private Sub rbdt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbdt.SelectedIndexChanged
        pdt = rbdt.SelectedValue
        sql = "update wovars set [default] = '" & pdt & "' where [column] = 'pdt'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr148" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub

    Private Sub rbttt_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ttt = rbttt.SelectedValue
        sql = "update wovars set [default] = '" & ttt & "' where [column] = 'pttt'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr149" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub

    
    Private Sub rbtttt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtttt.SelectedIndexChanged
        tttt = rbtttt.SelectedValue
        sql = "update wovars set [default] = '" & tttt & "' where [column] = 'ptttt'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr150" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub

    Private Sub rbdtt_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbdtt.SelectedIndexChanged
        pdtt = rbdtt.SelectedValue
        sql = "update wovars set [default] = '" & pdtt & "' where [column] = 'pdtt'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr151" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub

    Private Sub rbttt_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbttt.SelectedIndexChanged
        ttt = rbttt.SelectedValue
        sql = "update wovars set [default] = '" & ttt & "' where [column] = 'pttt'"
        lr.Open()
        lr.Update(sql)
        lr.Dispose()
        Dim strMessage As String =  tmod.getmsg("cdstr152" , "appsetwovars.aspx.vb")
 
        lr.CreateMessageAlert(Me, strMessage, "strKey1")
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang126.Text = axlabs.GetASPXPage("appsetwovars.aspx", "lang126")
        Catch ex As Exception
        End Try
        Try
            lang127.Text = axlabs.GetASPXPage("appsetwovars.aspx", "lang127")
        Catch ex As Exception
        End Try
        Try
            lang128.Text = axlabs.GetASPXPage("appsetwovars.aspx", "lang128")
        Catch ex As Exception
        End Try
        Try
            lang129.Text = axlabs.GetASPXPage("appsetwovars.aspx", "lang129")
        Catch ex As Exception
        End Try
        Try
            lang130.Text = axlabs.GetASPXPage("appsetwovars.aspx", "lang130")
        Catch ex As Exception
        End Try
        Try
            lang131.Text = axlabs.GetASPXPage("appsetwovars.aspx", "lang131")
        Catch ex As Exception
        End Try
        Try
            lang132.Text = axlabs.GetASPXPage("appsetwovars.aspx", "lang132")
        Catch ex As Exception
        End Try
        Try
            lang133.Text = axlabs.GetASPXPage("appsetwovars.aspx", "lang133")
        Catch ex As Exception
        End Try

    End Sub

End Class
