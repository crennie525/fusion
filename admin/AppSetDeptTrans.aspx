<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetDeptTrans.aspx.vb"
    Inherits="lucy_r12.AppSetDeptTrans" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetDeptTrans</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetDeptTransaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table cellspacing="3">
        <tbody>
            <tr>
                <td width="140">
                </td>
                <td width="300">
                </td>
                <td width="140">
                </td>
                <td width="280">
                </td>
            </tr>
            <tr height="24">
                <td colspan="2" class="thdrsing label">
                    <asp:Label ID="lang42" runat="server">Source Details</asp:Label>
                </td>
                <td colspan="2" class="thdrsing label">
                    <asp:Label ID="lang43" runat="server">Destination Details</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang44" runat="server">Department Source</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dddepts1" runat="server" AutoPostBack="True" Width="180px">
                    </asp:DropDownList>
                </td>
                <td class="label">
                    <asp:Label ID="lang45" runat="server">Department Destination</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="dddepts" runat="server" AutoPostBack="True" Width="180px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang46" runat="server">Station/Cell Source</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddcells1" runat="server" AutoPostBack="True" Width="180px">
                    </asp:DropDownList>
                </td>
                <td class="label">
                    <asp:Label ID="lang47" runat="server">Station/Cell Destination</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddcells" runat="server" AutoPostBack="True" Width="180px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="spdiv" style="width: 440px; height: 200px; overflow: auto">
                        <asp:Repeater ID="rptrdept1" runat="server">
                            <HeaderTemplate>
                                <table cellspacing="0">
                                    <tr class="tbg" width="400" height="24">
                                        <td class="btmmenu plainlabel" align="center">
                                            <img src="../images/appbuttons/minibuttons/checked.gif" width="16" height="16">
                                        </td>
                                        <td class="btmmenu plainlabel" width="180">
                                            <asp:Label ID="lang48" runat="server">Equipment#</asp:Label>
                                        </td>
                                        <td class="btmmenu plainlabel" width="220">
                                            <asp:Label ID="lang49" runat="server">Description</asp:Label>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg" height="22">
                                    <td class="plainlabel">
                                        <asp:CheckBox ID="cb1" runat="server"></asp:CheckBox>
                                    </td>
                                    <td class="plainlabel">
                                        &nbsp;
                                        <asp:Label ID="Label4" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="transrowblue" height="22">
                                    <td class="plainlabel">
                                        <asp:CheckBox ID="cb2" runat="server"></asp:CheckBox>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        &nbsp;
                                        <asp:Label ID="Linkbutton1" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
                <td colspan="2">
                    <div id="dsdiv" style="width: 420px; height: 200px; overflow: auto">
                        <asp:Repeater ID="rptrdept" runat="server">
                            <HeaderTemplate>
                                <table cellspacing="0">
                                    <tr class="tbg" width="400" height="24">
                                        <td class="btmmenu plainlabel" width="180">
                                            <asp:Label ID="lang50" runat="server">Equipment#</asp:Label>
                                        </td>
                                        <td class="btmmenu plainlabel" width="220">
                                            <asp:Label ID="lang51" runat="server">Description</asp:Label>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg" height="22">
                                    <td class="plainlabel">
                                        &nbsp;
                                        <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="Label5" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="transrowblue" height="22">
                                    <td class="plainlabel transrowblue">
                                        &nbsp;
                                        <asp:Label ID="Label6" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="Label7" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="Label8" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <input type="button" id="btntrans" runat="server" onclick="vertrans();" value="Transfer Selected Items">
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lbldid" runat="server">
    <input type="hidden" id="lbldid1" runat="server">
    <input type="hidden" id="lblclid" runat="server">
    <input type="hidden" id="lblclid1" runat="server"><input type="hidden" id="lblcbonoff"
        runat="server" name="lblcbonoff">
    <input type="hidden" id="lblsubmit" runat="server">
    <input type="hidden" id="lbleqcnt" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
