<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetApprHLTasks.aspx.vb"
    Inherits="lucy_r12.AppSetApprHLTasks" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetApprTasks</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript" src="../scripts1/AppSetApprHLTasksaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <table style="left: 0px; position: absolute; top: 3px" cellspacing="0" cellpadding="0"
        width="520">
        <tr>
            <td class="bluelabel" width="120">
                <asp:Label ID="lang18" runat="server">Approval Phase</asp:Label>
            </td>
            <td class="label" id="tdphase" width="340" runat="server">
            </td>
            <td align="right" width="60">
                <input class="lilmenu plainlabel" onmouseover="this.className='lilmenuhov plainlabel'"
                    style="width: 55px; height: 24px" onclick="ret();" onmouseout="this.className='lilmenu plainlabel'"
                    type="button" value="Return">
            </td>
        </tr>
        <tr>
            <td>
                <img src="../images/2PX.gif">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgphases" runat="server" Width="520px" AutoGenerateColumns="False"
                    ShowFooter="True" CellSpacing="1" GridLines="None">
                    <FooterStyle CssClass="plainlabel"></FooterStyle>
                    <AlternatingItemStyle CssClass="plainlabel" BackColor="#FCEED4"></AlternatingItemStyle>
                    <ItemStyle CssClass="plainlabel"></ItemStyle>
                    <HeaderStyle CssClass="tblmenuyrt plainlabel"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Width="80px" CssClass="plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" runat="server" CommandName="Edit" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif">
                                </asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton5" runat="server" CommandName="Reset" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif">
                                </asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton3" runat="server" CommandName="Update" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                                </asp:ImageButton>
                                <asp:ImageButton ID="ImageButton4" runat="server" CommandName="Cancel" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif">
                                </asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ID">
                            <ItemTemplate>
                                <asp:Label ID="lblpmaida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmaid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmaid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmaid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ID">
                            <ItemTemplate>
                                <asp:Label ID="lblpmacida" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmacid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblpmacid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pmacid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Order">
                            <HeaderStyle Width="50px" CssClass="plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="A1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.checkorder") %>'
                                    CssClass="plainlabel">
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtlo" runat="server" Width="40px" Text='<%# DataBinder.Eval(Container, "DataItem.checkorder") %>'
                                    CssClass="plainlabel">
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Approval Phase High Level Tasks">
                            <HeaderStyle Width="370px" CssClass="plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                <a id="lnkphase" href="#" runat="server">
                                    <%# DataBinder.Eval(Container, "DataItem.checkitem") %>
                                </a>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtaphase" runat="server" CssClass="plainlabel" Width="360px" MaxLength="100"></asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtephase" runat="server" CssClass="plainlabel" Width="360px" MaxLength="100"
                                    Text='<%# DataBinder.Eval(Container, "DataItem.checkitem") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete">
                            <HeaderStyle Width="50px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Center"></FooterStyle>
                            <FooterTemplate>
                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnew.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <input id="lblgroupid" type="hidden" runat="server"><input id="lbloldtasknum" type="hidden"
        runat="server">
    <input id="lblans" type="hidden" runat="server"><input id="lbleqcnt" type="hidden"
        runat="server">
    <input type="hidden" id="lblrev" runat="server"><input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
