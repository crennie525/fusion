<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppSetTaskTabET.aspx.vb"
    Inherits="lucy_r12.AppSetTaskTabET" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AppSetTaskTabET</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script language="JavaScript" type="text/javascript" src="../scripts/gridnav.js"></script>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script language="JavaScript" type="text/javascript"src="../scripts1/AppSetTaskTabETaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table style="left: 0px; position: absolute; top: 0px" width="600">
        <tr>
            <td width="100">
            </td>
            <td width="180">
            </td>
            <td width="20">
            </td>
            <td width="300">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="lblstat" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
                    Width="290px" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang109" runat="server">Search</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="170px"></asp:TextBox>
            </td>
            <td>
                <asp:ImageButton ID="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                </asp:ImageButton>
            </td>
            <td class="plainlabelblue" align="center" valign="middle" rowspan="4">
                <asp:Label ID="lang110" runat="server">Some Equipment Status Types cannot be edited or deleted as they are required for some Fusion Operations.</asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="3" align="center">
                <asp:DataGrid ID="dgstat" runat="server" AutoGenerateColumns="False" AllowCustomPaging="True"
                    AllowPaging="True" GridLines="None" CellPadding="0" ShowFooter="True" CellSpacing="1"
                    BackColor="transparent">
                    <FooterStyle BackColor="transparent"></FooterStyle>
                    <AlternatingItemStyle CssClass="ptransrowblue" Height="20px"></AlternatingItemStyle>
                    <ItemStyle CssClass="ptransrow" Height="20px"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Edit">
                            <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:ImageButton ID="imgedit" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                    CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                            </ItemTemplate>
                            <FooterTemplate>
                                &nbsp;
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                    CommandName="Add"></asp:ImageButton>
                            </FooterTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="Imagebutton23" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                    CommandName="Update"></asp:ImageButton>
                                <asp:ImageButton ID="Imagebutton24" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                    CommandName="Cancel"></asp:ImageButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblstatidi" runat="server" NAME="Label21" Text='<%# DataBinder.Eval(Container, "DataItem.statid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblstatid" runat="server" NAME="Label21" Text='<%# DataBinder.Eval(Container, "DataItem.statid") %>'>
                                </asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PM Eq Status">
                            <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;
                                <asp:Label ID="Label18" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtnewstat" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                </asp:TextBox>
                            </FooterTemplate>
                            <EditItemTemplate>
                                &nbsp;
                                <asp:TextBox ID="Textbox14" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Remove">
                            <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                            <ItemTemplate>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="imgdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table style="border-right: blue 1px solid; border-top: blue 1px solid; border-left: blue 1px solid;
                    border-bottom: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="lblcid" runat="server"><input type="hidden" id="txtpg" runat="server"
        name="Hidden1"><input type="hidden" id="txtpgcnt" runat="server" name="txtpgcnt">
    <input type="hidden" id="lblret" runat="server" name="lblret"><input type="hidden"
        id="lblold" runat="server" name="lblold">
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
