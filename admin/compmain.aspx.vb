

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class compmain
    Inherits System.Web.UI.Page
	Protected WithEvents lang153 As System.Web.UI.WebControls.Label

	Protected WithEvents lang152 As System.Web.UI.WebControls.Label

	Protected WithEvents lang151 As System.Web.UI.WebControls.Label

	Protected WithEvents lang150 As System.Web.UI.WebControls.Label

	Protected WithEvents lang149 As System.Web.UI.WebControls.Label

	Protected WithEvents lang148 As System.Web.UI.WebControls.Label

	Protected WithEvents lang147 As System.Web.UI.WebControls.Label

	Protected WithEvents lang146 As System.Web.UI.WebControls.Label

	Protected WithEvents lang145 As System.Web.UI.WebControls.Label

	Protected WithEvents lang144 As System.Web.UI.WebControls.Label

	Protected WithEvents lang143 As System.Web.UI.WebControls.Label

	Protected WithEvents lang142 As System.Web.UI.WebControls.Label

	Protected WithEvents lang141 As System.Web.UI.WebControls.Label

	Protected WithEvents lang140 As System.Web.UI.WebControls.Label

	Protected WithEvents lang139 As System.Web.UI.WebControls.Label

	Protected WithEvents lang138 As System.Web.UI.WebControls.Label

	Protected WithEvents lang137 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim co As New Utilities
    Dim spName As String = "Paging_Cursor"
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 5
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected WithEvents ifcraft As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim typ, jump, ret, ro As String
    Protected WithEvents txtcode As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txthome As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtadd1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtadd2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtadd3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtadd4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtph As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfax As System.Web.UI.WebControls.TextBox
    Protected WithEvents dglabor As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Tr5 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsrch As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr3 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ifwr As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Tr4 As System.Web.UI.HtmlControls.HtmlTableRow
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchild As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltype As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldfail As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtcontract As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                imgadd.Attributes.Add("onclick", "")
                Img1.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                Img1.Attributes.Add("onclick", "")
            End If
            co.Open()
            'PopSys()
            PopType()
            'GetBelongs("0")
            'GetChildren("0")
            'LoadEvents()
            Try
                jump = Request.QueryString("jump").ToString
            Catch ex As Exception

            End Try
            If jump = "yes" Then
                txtcode.Text = Request.QueryString("co").ToString
                SrchLoc()
            End If

        Else
            ret = Request.Form("lblsubmit")
            If ret = "add" Then
                lblsubmit.Value = ""
                co.Open()
                AddLoc()
                co.Dispose()
            ElseIf ret = "save" Then
                lblsubmit.Value = ""
                co.Open()
                SaveLoc()
                co.Dispose()
                'lblref.Value = "go"
            End If

        End If
    End Sub
    
    Private Sub PopType()
        'Dim cmd As New SqlCommand("exec usp_poploctype")
        sql = "select value, value + '-' + description as valdesc from invvallist where listname = 'comptype'"
        'dr = main.GetRdrDataHack(cmd)
        dr = co.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataValueField = "value"
        ddtype.DataTextField = "valdesc"
        Try
            ddtype.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddtype.Items.Insert(0, "Select")
        'ddtype.Attributes.Add("onchange", "handletyp();")
    End Sub
    Private Sub SrchLoc()

        lbloldfail.Value = ""
        txtloc.Text = ""
        txthome.Text = ""
        'txtcust.Text = dr.Item("customernum").ToString
        txtadd1.Text = ""
        txtadd2.Text = ""
        txtadd3.Text = ""
        txtadd4.Text = ""
        txtph.Text = ""
        txtfax.Text = ""
        ddtype.SelectedIndex = -1
        Dim fail, failstr, par, typ As String
        failstr = txtcode.Text
        txtcode.Text = ""
        sql = "select * from invcompanies where company like '%" & failstr & "%'"
        dr = co.GetRdrData(sql)
        While dr.Read
            txtcode.Text = dr.Item("company").ToString
            lbloldfail.Value = dr.Item("company").ToString
            txtloc.Text = dr.Item("name").ToString
            txthome.Text = dr.Item("homepage").ToString
            'txtcust.Text = dr.Item("customernum").ToString
            txtadd1.Text = dr.Item("address1").ToString
            txtadd2.Text = dr.Item("address2").ToString
            txtadd3.Text = dr.Item("address3").ToString
            txtadd4.Text = dr.Item("address4").ToString
            txtph.Text = dr.Item("phone").ToString
            txtfax.Text = dr.Item("fax").ToString
            typ = dr.Item("type").ToString
        End While
        dr.Close()
        Try
            ddtype.SelectedValue = typ
        Catch ex As Exception

        End Try
        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
        imgadd.Attributes.Add("onclick", "jumpto('1');")
    End Sub
    Private Sub SaveLoc()
        Dim cod, na, ho, cu, a1, a2, a3, a4, ph, fa, ty, old As String
        cod = txtcode.Text
        na = txtloc.Text
        ho = txthome.Text
        'cu = txtcust.Text
        a1 = txtadd1.Text
        a2 = txtadd2.Text
        a3 = txtadd3.Text
        a4 = txtadd4.Text
        ph = txtph.Text
        fa = txtfax.Text
        If Len(cod) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr155" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(na) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr156" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(na) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr157" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(a1) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr158" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(ho) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr159" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(a2) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr160" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(a3) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr161" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(a4) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr162" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(ph) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr163" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(fa) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr164" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If ddtype.SelectedIndex <> 0 Or ddtype.SelectedIndex <> -1 Then
            ty = ddtype.SelectedValue.ToString
        End If
        old = lbloldfail.Value
        sql = "select count(*) from invcompanies where company = '" & Replace(cod, "'", "''") & "'"
        Dim newcnt, chkcnt As Integer
        newcnt = co.Scalar(sql)

        If old = cod Then
            chkcnt = 1
        Else
            chkcnt = 0
        End If

        If newcnt = chkcnt Then
            Dim cmd As New SqlCommand("exec usp_upco @cod, @na, @ho, @a1, @a2, @a3, @a4, @ph, @fa, @ty, @old")

            Dim param0 = New SqlParameter("@cod", SqlDbType.VarChar)
            param0.Value = cod
            cmd.Parameters.Add(param0)

            Dim param02 = New SqlParameter("@na", SqlDbType.VarChar)
            param02.Value = na
            cmd.Parameters.Add(param02)
            Dim param01 = New SqlParameter("@ho", SqlDbType.VarChar)
            If Len(ho) > 0 Then
                param01.Value = ho
            Else
                param01.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param01)

            Dim param2 = New SqlParameter("@a1", SqlDbType.VarChar)
            If Len(a1) > 0 Then
                param2.Value = a1
            Else
                param2.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param2)
            Dim param3 = New SqlParameter("@a2", SqlDbType.VarChar)
            If Len(a2) > 0 Then
                param3.Value = a2
            Else
                param3.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param3)
            Dim param4 = New SqlParameter("@a3", SqlDbType.VarChar)
            If Len(a3) > 0 Then
                param4.Value = a3
            Else
                param4.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param4)
            Dim param5 = New SqlParameter("@a4", SqlDbType.VarChar)
            If Len(a4) > 0 Then
                param5.Value = a4
            Else
                param5.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param5)

            Dim param6 = New SqlParameter("@ph", SqlDbType.VarChar)
            If Len(ph) > 0 Then
                param6.Value = ph
            Else
                param6.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param6)
            Dim param7 = New SqlParameter("@fa", SqlDbType.VarChar)
            If Len(fa) > 0 Then
                param7.Value = fa
            Else
                param7.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param7)
            Dim param8 = New SqlParameter("@ty", SqlDbType.VarChar)
            If Len(ty) > 0 Then
                param8.Value = ty
            Else
                param8.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param8)
            Dim param9 = New SqlParameter("@old", SqlDbType.VarChar)
            param9.Value = old
            cmd.Parameters.Add(param9)


            co.UpdateHack(cmd)
        End If
    End Sub
    Private Sub AddLoc()
        Dim failstr, faildesc, typ As String
        failstr = txtcode.Text
        faildesc = txtloc.Text
        Dim cnt, fail As Integer
        Dim cod, na, ho, cu, a1, a2, a3, a4, ph, fa, ty, old As String
        cod = txtcode.Text
        na = txtloc.Text
        ho = txthome.Text
        'cu = txtcust.Text
        a1 = txtadd1.Text
        a2 = txtadd2.Text
        a3 = txtadd3.Text
        a4 = txtadd4.Text
        ph = txtph.Text
        fa = txtfax.Text
        If Len(cod) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr165" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(na) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr166" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(na) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr167" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(a1) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr168" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(ho) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr169" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(a2) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr170" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(a3) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr171" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(a4) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr172" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(ph) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr173" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(fa) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr174" , "compmain.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If ddtype.SelectedIndex <> 0 Or ddtype.SelectedIndex <> -1 Then
            ty = ddtype.SelectedValue.ToString
        Else
            ty = ""
        End If
        failstr = co.ModString2(failstr)
        faildesc = co.ModString2(faildesc)
        sql = "select count(*) from invcompanies where company = '" & failstr & "'"
        cnt = co.Scalar(sql)
        If cnt = 0 Then
            sql = "usp_addco " _
            + "'" & failstr & "','" & faildesc & "','" & ty & "','" & ho & "','" & a1 & "','" & a2 & "','" & a3 & "','" & a4 & "','" & ph & "','" & fa & "'"
            co.Update(sql)
        Else

        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dglabor.Columns(0).HeaderText = dlabs.GetDGPage("compmain.aspx", "dglabor", "0")
        Catch ex As Exception
        End Try
        Try
            dglabor.Columns(1).HeaderText = dlabs.GetDGPage("compmain.aspx", "dglabor", "1")
        Catch ex As Exception
        End Try
        Try
            dglabor.Columns(2).HeaderText = dlabs.GetDGPage("compmain.aspx", "dglabor", "2")
        Catch ex As Exception
        End Try
        Try
            dglabor.Columns(3).HeaderText = dlabs.GetDGPage("compmain.aspx", "dglabor", "3")
        Catch ex As Exception
        End Try
        Try
            dglabor.Columns(4).HeaderText = dlabs.GetDGPage("compmain.aspx", "dglabor", "4")
        Catch ex As Exception
        End Try
        Try
            dglabor.Columns(5).HeaderText = dlabs.GetDGPage("compmain.aspx", "dglabor", "5")
        Catch ex As Exception
        End Try
        Try
            dglabor.Columns(6).HeaderText = dlabs.GetDGPage("compmain.aspx", "dglabor", "6")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang137.Text = axlabs.GetASPXPage("compmain.aspx", "lang137")
        Catch ex As Exception
        End Try
        Try
            lang138.Text = axlabs.GetASPXPage("compmain.aspx", "lang138")
        Catch ex As Exception
        End Try
        Try
            lang139.Text = axlabs.GetASPXPage("compmain.aspx", "lang139")
        Catch ex As Exception
        End Try
        Try
            lang140.Text = axlabs.GetASPXPage("compmain.aspx", "lang140")
        Catch ex As Exception
        End Try
        Try
            lang141.Text = axlabs.GetASPXPage("compmain.aspx", "lang141")
        Catch ex As Exception
        End Try
        Try
            lang142.Text = axlabs.GetASPXPage("compmain.aspx", "lang142")
        Catch ex As Exception
        End Try
        Try
            lang143.Text = axlabs.GetASPXPage("compmain.aspx", "lang143")
        Catch ex As Exception
        End Try
        Try
            lang144.Text = axlabs.GetASPXPage("compmain.aspx", "lang144")
        Catch ex As Exception
        End Try
        Try
            lang145.Text = axlabs.GetASPXPage("compmain.aspx", "lang145")
        Catch ex As Exception
        End Try
        Try
            lang146.Text = axlabs.GetASPXPage("compmain.aspx", "lang146")
        Catch ex As Exception
        End Try
        Try
            lang147.Text = axlabs.GetASPXPage("compmain.aspx", "lang147")
        Catch ex As Exception
        End Try
        Try
            lang148.Text = axlabs.GetASPXPage("compmain.aspx", "lang148")
        Catch ex As Exception
        End Try
        Try
            lang149.Text = axlabs.GetASPXPage("compmain.aspx", "lang149")
        Catch ex As Exception
        End Try
        Try
            lang150.Text = axlabs.GetASPXPage("compmain.aspx", "lang150")
        Catch ex As Exception
        End Try
        Try
            lang151.Text = axlabs.GetASPXPage("compmain.aspx", "lang151")
        Catch ex As Exception
        End Try
        Try
            lang152.Text = axlabs.GetASPXPage("compmain.aspx", "lang152")
        Catch ex As Exception
        End Try
        Try
            lang153.Text = axlabs.GetASPXPage("compmain.aspx", "lang153")
        Catch ex As Exception
        End Try

    End Sub

End Class
