﻿Imports System.Data.SqlClient
Public Class appsetwopriority
    Inherits System.Web.UI.Page
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = "wopri"
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql, ro, cid As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtt.Columns(0).Visible = False
                dgtt.Columns(5).Visible = False
            End If
            appset.Open()
            checkpri()
            PopTasks(PageNumber)
            appset.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "changepri" Then
                appset.Open()
                changepri()
                checkpri()
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub changepri()
        Dim prichk As String
        prichk = lblusepri.Value
        sql = "update wovars set [default] = '" & prichk & "' where [column] = 'wopriority'"
        appset.Update(sql)
        If prichk = "yes" Then
            cbpri.Checked = True
            lblprialert.Value = "yes"
            tdmsg.InnerHtml = "To stop using Custom Work Order Prioities check the check box below.<br /><br /> " _
            + "Current Work Orders with a Custom Work Order Priority already selected will display the current selection " _
            + "with an option to switch to Standard Work Order Priorities."
        Else
            cbpri.Checked = False
            lblprialert.Value = "no"
            tdmsg.InnerHtml = "To start using Custom Work Order Prioities check the check box below.<br /><br /> " _
            + "Current Work Orders with a Work Order Priority already selected will display the current selection " _
            + "with an option to switch to Custom Work Order Priorities."
        End If
    End Sub
    Private Sub checkpri()
        sql = "select [default] from wovars where [column] = 'wopriority'"
        Dim prichk As String
        Try
            prichk = appset.strScalar(sql)
        Catch ex As Exception
            prichk = "no"
        End Try
        lblusepri.Value = prichk
        If prichk = "yes" Then
            cbpri.Checked = True
            lblprialert.Value = "yes"
            tdmsg.InnerHtml = "To stop using Custom Work Order Prioities check the check box below.<br /><br /> " _
            + "Current Work Orders with a Custom Work Order Priority already selected will display the current selection " _
            + "with an option to switch to Standard Work Order Priorities."
        Else
            cbpri.Checked = False
            lblprialert.Value = "no"
            tdmsg.InnerHtml = "To start using Custom Work Order Prioities check the check box below.<br /><br /> " _
            + "Current Work Orders with a Work Order Priority already selected will display the current selection " _
            + "with an option to switch to Custom Work Order Priorities."
        End If
    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        sql = "select count(*) from wopriority"
        dgtt.VirtualItemCount = appset.Scalar(sql)
        If dgtt.VirtualItemCount = 0 Then
            lbltasktype.Text = "No Work Order Priority Records Found"
            dgtt.Visible = True
            Tables = "wopriority"
            PK = "woprid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        Else
            Tables = "wopriority"
            PK = "woprid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            lbltasktype.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr135", "AppSetWoType.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr136", "AppSetWoType.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub

    Private Sub dgtt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.CancelCommand
        dgtt.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.DeleteCommand
        appset.Open()
        Dim id, desc As String
        id = CType(e.Item.Cells(1).Controls(1), Label).Text
        Dim tcnt As Integer = 0
        If tcnt = 0 Then
            sql = "delete from wopriority where woprid = '" & id & "'"
            appset.Update(sql)
            dgtt.EditItemIndex = -1
            sql = "select Count(*) from wopriority"
            PageNumber = appset.PageCount(sql, PageSize)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            If dgtt.CurrentPageIndex > PageNumber Then
                dgtt.CurrentPageIndex = PageNumber - 1
            End If
            If dgtt.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgtt.CurrentPageIndex + 1
            End If
            PopTasks(PageNumber)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr140", "AppSetWoType.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub

    Private Sub dgtt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.EditCommand
        'lblold.Value = CType(e.Item.Cells(2).Controls(1), Label).Text
        PageNumber = txtpg.Value
        dgtt.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.ItemCommand
        Dim lname, wosd As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewtt"), TextBox)
            lname = lnamei.Text
            Dim tnchk As Long
            Try
                tnchk = System.Convert.ToInt64(lname)
            Catch ex As Exception
                Try
                    tnchk = System.Convert.ToDecimal(lname)
                Catch ex1 As Exception
                    Dim strMessage As String = "Priority Must Be a Non-Decimal Numeric Vale"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                    Exit Sub
                End Try

            End Try

            wosd = CType(e.Item.FindControl("txtwtda"), TextBox).Text
            wosd = appset.ModString1(wosd)
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(wosd) > 50 Then
                        Dim strMessage As String = "Description is limited to 50 Characters"

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        cid = "0"
                        Dim tt As String
                        Dim strchk As Integer
                        Dim faill As String = lname
                        sql = "select count(*) from wopriority where wopri = '" & faill & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "insert into wopriority (wopri, wopridesc) values ('" & lname & "','" & wosd & "')"
                            appset.Update(sql)
                            Dim ttcnt As Integer = dgtt.VirtualItemCount + 1

                            lnamei.Text = ""
                            sql = "select Count(*) from wopriority"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopTasks(PageNumber)
                        Else
                            Dim strMessage As String = "Priority Value Must Be Unique"

                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If
                        appset.Dispose()
                    End If

                End If
            End If
        End If
    End Sub

    Private Sub dgtt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.UpdateCommand
        appset.Open()
        Dim id, desc, act, old, wosd As String
        id = CType(e.Item.FindControl("lblttide"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        Dim tnchk As Long
        Try
            tnchk = System.Convert.ToInt64(desc)
        Catch ex As Exception
            Try
                tnchk = System.Convert.ToDecimal(desc)
            Catch ex1 As Exception
                Dim strMessage As String = "Priority Must Be a Non-Decimal Numeric Vale"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
                Exit Sub
            End Try

        End Try
        wosd = CType(e.Item.FindControl("txtwde"), TextBox).Text
        wosd = appset.ModString1(wosd)
        If Len(wosd) > 50 Then
            Dim strMessage As String = "Description is limited to 50 Characters"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        act = CType(e.Item.FindControl("cbact"), CheckBox).Checked
        Dim faill As String = desc.ToLower
        sql = "select count(*) from wopriority where '" & faill & "'"
        Dim strchk As Integer
        strchk = 0
        If strchk = 0 Then
            sql = "update wopriority set wopri = " _
            + "'" & desc & "', wopridesc = '" & wosd & "' where woprid = '" & id & "';"
            appset.Update(sql)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            PopTasks(PageNumber)
        Else
            Dim strMessage As String = tmod.getmsg("cdstr143", "AppSetWoType.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub
End Class