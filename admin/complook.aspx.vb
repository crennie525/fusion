

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class complook
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim co As New Utilities
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Dim typ As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            typ = Request.QueryString("typ").ToString
            co.Open()
            LoadCo(typ)
            co.Dispose()

        End If
    End Sub
    Private Sub LoadCo(ByVal typ As String)
        Dim sb As New StringBuilder
        sb.Append("<table><tr height=""20"">" & vbCrLf)
        sb.Append("<td class=""btmmenu plainlabel"" width=""100"">" & tmod.getlbl("cdlbl9", "complook.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""btmmenu plainlabel"" width=""160"">" & tmod.getlbl("cdlbl10", "complook.aspx.vb") & "</td></tr>" & vbCrLf)
        Dim lb, nm As String
        If typ = "all" Then
            sql = "select company, name from invcompanies"
        Else
            sql = "select company, name from invcompanies where type= '" & typ & "'"
        End If
        dr = co.GetRdrData(sql)
        While dr.Read
            lb = dr.Item("company").ToString
            lb = Replace(lb, "'", Chr(180), , , vbTextCompare)
            lb = Replace(lb, """", Chr(180), , , vbTextCompare)
            nm = dr.Item("name").ToString
            nm = Replace(nm, "'", Chr(180), , , vbTextCompare)
            nm = Replace(nm, """", Chr(180), , , vbTextCompare)
            sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""hlab('" & lb & "','" & nm & "');""> " & lb & "</a></td>" & vbCrLf)
            sb.Append("<td class=""plainlabel"">" & nm & "</td></tr>" & vbCrLf)
        End While
        dr.Close()
        sb.Append("</table>")
        tdco.InnerHtml = sb.ToString
    End Sub
End Class
