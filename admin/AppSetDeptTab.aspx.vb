

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetDeptTab
    Inherits System.Web.UI.Page
	Protected WithEvents lang41 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, sid, ro As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim Sort As String = "dept_line"
    Dim dr As SqlDataReader
    Protected WithEvents txtdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblps As System.Web.UI.WebControls.Label
    Protected WithEvents tdps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Label24 As System.Web.UI.WebControls.Label
    Protected WithEvents ddps As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbldfltd As System.Web.UI.WebControls.Label
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgdepts As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgdepts.Columns(0).Visible = False
            End If
            Try
                cid = "0"
            Catch ex As Exception
                'Response.Redirect("/laipm3/NewLogin.aspx")
            End Try

            If cid <> "" Then
                lblcid.Value = cid
                cid = lblcid.Value
                sid = Request.QueryString("sid").ToString 'HttpContext.Current.Session("dfltps").ToString()
                lblsid.Value = sid
                appset.Open()
                GetSite(sid, cid)
                LoadDepts(PageNumber)
                appset.Dispose()
            End If
        Else
            If Request.Form("lblpchk") = "dfltps" Then
                sid = lblsid.Value
                cid = lblcid.Value
                Session("dfltps") = sid
                appset.Open()
                GetSite(sid, cid)
                LoadDepts(PageNumber)
                appset.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                LoadDepts(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                LoadDepts(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If

    End Sub
    Private Sub GetSite(ByVal sid As String, ByVal cid As String)
        sql = "select sitename from sites where siteid = '" & sid & "'"
        Dim site As String = appset.strScalar(sql)
        tdps.InnerHtml = "Default Plant Site is " & site
        Dim ms As String = HttpContext.Current.Session("ms").ToString()
        If ms <> "0" Then
            sql = "select siteid, sitename from sites where compid = '" & cid & "'"
            dr = appset.GetRdrData(sql)
            ddps.DataSource = dr
            ddps.DataValueField = "siteid"
            ddps.DataTextField = "sitename"
            Try
                ddps.DataBind()
            Catch ex As Exception

            End Try

            dr.Close()
            ddps.Items.Insert(0, "Select Plant Site")
            ddps.Attributes.Add("onchange", "handleps();")
        Else
        End If
    End Sub
    ' Departments
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        LoadDepts(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub LoadDepts(ByVal PageNumber As Integer)
        'Try
        cid = lblcid.Value
        sid = lblsid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "dept_line like ''%" & srch & "%'' and compid = ''" & cid & "'' and siteid = ''" & sid & "''"
            FilterCnt = "dept_line like '%" & srch & "%' and compid = '" & cid & "' and siteid = '" & sid & "'"
        Else
            Filter = "compid = ''" & cid & "'' and siteid = ''" & sid & "''"
            FilterCnt = "compid = '" & cid & "' and siteid = '" & sid & "'"
        End If
        sql = "select Count(*) from Dept " _
        + "where " & FilterCnt 'siteid = '" & sid & "'"

        Dim dc As Integer = appset.Scalar(sql)

        dgdepts.VirtualItemCount = dc
        If dc = 0 Then
            lblps.Visible = True
            lblps.Text = "No Department Records Found"
            dgdepts.Visible = True
            Tables = "Dept"
            PK = "Dept_ID"
            PageSize = "10"
            Fields = "*"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgdepts.DataSource = dr
            Try
                dgdepts.DataBind()
            Catch ex As Exception
                dgdepts.CurrentPageIndex = 0
                dgdepts.DataBind()
            End Try
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
        Else
            dgdepts.Visible = True
            lblps.Visible = False
            Tables = "Dept"
            PK = "Dept_ID"
            PageSize = "10"
            Fields = "*"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgdepts.DataSource = dr
            Try
                dgdepts.DataBind()
            Catch ex As Exception
                dgdepts.CurrentPageIndex = 0
                dgdepts.DataBind()
            End Try
            dr.Close()
            lblps.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgdepts.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgdepts.PageCount
        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            LoadDepts(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr26" , "AppSetDeptTab.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            LoadDepts(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr27" , "AppSetDeptTab.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgdepts_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.UpdateCommand
        'Try
        appset.Open()
        cid = lblcid.Value
        sid = lblsid.Value
        Dim dept, desc, did As String
        dept = CType(e.Item.Cells(1).Controls(1), TextBox).Text
        dept = appset.ModString1(dept)
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        did = CType(e.Item.FindControl("lbldepte"), Label).Text
        Dim faill As String = dept.ToLower
        Dim old As String = lblold.Value
        If old <> dept Then
            sql = "select count(*) from Dept where lower(Dept_Line) = '" & faill & "' and siteid = '" & sid & "'" 'and compid = '" & cid & "'"
            Dim strchk As Integer
            strchk = appset.Scalar(sql)
            If strchk = 0 Then
                sql = "update Dept set Dept_Desc = " _
                + "'" & desc & "', Dept_Line = '" & dept & "' where compid = '" & cid & "' and dept_id = '" & did & "'"
                appset.Update(sql)
                'appset.Dispose()
                dgdepts.EditItemIndex = -1
                LoadDepts(PageNumber)
                'Catch ex As Exception
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr28" , "AppSetDeptTab.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
            End If
        Else
            sql = "update Dept set Dept_Desc = " _
              + "'" & desc & "', Dept_Line = '" & dept & "' where compid = '" & cid & "' and dept_id = '" & did & "'"
            appset.Update(sql)
            dgdepts.EditItemIndex = -1
            LoadDepts(PageNumber)
        End If
        appset.Dispose()
        'End Try
    End Sub

    Private Sub dgdepts_EditCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("LinkButton1"), LinkButton).Text
        dgdepts.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        LoadDepts(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgdepts_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.CancelCommand
        dgdepts.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        LoadDepts(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgdepts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.ItemCommand
        If (e.CommandName = "Select") Then
            Dim did, didstr As String
            did = CType(e.Item.Cells(4).Controls(1), Label).Text
            didstr = CType(e.Item.Cells(1).Controls(1), LinkButton).Text
            'Dim strMessage As String = sidstr
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            lbldid.Value = did
            lblpar.Value = "dept"
            lbldfltd.Text = "Default Department for this session is " & didstr
        End If
        Dim lid, lname, ldesc, llabel As String
        Dim lnamei, ldesci, llabeli As TextBox

        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewlname"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)

            ldesci = CType(e.Item.FindControl("txtnewldesc"), TextBox)
            ldesc = ldesci.Text
            ldesc = appset.ModString1(ldesc)
            llabeli = CType(e.Item.FindControl("txtnewllabel"), TextBox)
            llabel = llabeli.Text

            If lnamei.Text <> "" Then
                If Len(lname) > 50 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr29" , "AppSetDeptTab.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    cid = lblcid.Value
                    sid = lblsid.Value
                    Dim strchk As Integer
                    Dim faill As String = lname.ToLower
                    sql = "select count(*) from Dept where lower(Dept_Line) = '" & faill & "' and siteid = '" & sid & "'" 'and compid = '" & cid & "'"
                    strchk = appset.Scalar(sql)
                    If strchk = 0 Then
                        sql = "insert into Dept (compid, siteid, Dept_Line, Dept_Desc) " _
                        + "values ('" & cid & "', '" & sid & "', '" & lname & "', '" & ldesc & "')"
                        appset.Update(sql)
                        Dim deptcnt As Integer = dgdepts.VirtualItemCount + 1
                        If IsDBNull(dgdepts.VirtualItemCount) Or dgdepts.VirtualItemCount = 0 Then
                            deptcnt = 1
                        End If
                        lnamei.Text = ""
                        ldesci.Text = ""
                        llabeli.Text = ""
                        sql = "select Count(*) from dept " _
                        + "where compid = '" & cid & "' and siteid = '" & sid & "'"
                        PageNumber = appset.PageCount(sql, PageSize)
                        LoadDepts(PageNumber)
                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr30" , "AppSetDeptTab.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                End If
                appset.Dispose()
            End If
        End If

    End Sub


    Private Sub dgdepts_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgdepts.DeleteCommand
        appset.Open()
        Dim id As String
        Try
            id = CType(e.Item.FindControl("lbldepta"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lbldepte"), Label).Text
        End Try

        Dim ecnt As Integer
        sql = "select count(*) from equipment where dept_id = '" & id & "'"
        ecnt = appset.Scalar(sql)
        If ecnt <> 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr31" , "AppSetDeptTab.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
            Exit Sub
        Else
            sql = "delete from dept where dept_id = '" & id & "';delete from cells where dept_id = '" & id & "'"
            appset.Update(sql)
            cid = lblcid.Value
            sid = lblsid.Value
            sql = "select Count(*) from dept " _
                    + "where compid = '" & cid & "' and siteid = '" & sid & "'"
            PageSize = 10
            PageNumber = appset.PageCount(sql, PageSize)
            LoadDepts(PageNumber)
            lbldid.Value = ""
            lblpar.Value = "dept"
        End If
        appset.Dispose()
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("AppSetDeptTab.aspx", "dgdepts", "0")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(1).HeaderText = dlabs.GetDGPage("AppSetDeptTab.aspx", "dgdepts", "1")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(2).HeaderText = dlabs.GetDGPage("AppSetDeptTab.aspx", "dgdepts", "2")
        Catch ex As Exception
        End Try
        Try
            dgdepts.Columns(5).HeaderText = dlabs.GetDGPage("AppSetDeptTab.aspx", "dgdepts", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label24.Text = axlabs.GetASPXPage("AppSetDeptTab.aspx", "Label24")
        Catch ex As Exception
        End Try
        Try
            lang41.Text = axlabs.GetASPXPage("AppSetDeptTab.aspx", "lang41")
        Catch ex As Exception
        End Try

    End Sub

End Class
