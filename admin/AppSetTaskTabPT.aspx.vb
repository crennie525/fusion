

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetTaskTabPT
    Inherits System.Web.UI.Page
	Protected WithEvents lang111 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sql, ro As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String = "PreTech"
    Dim dr As SqlDataReader
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpre As System.Web.UI.WebControls.Label
    Protected WithEvents dgpre As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblpgpre As System.Web.UI.WebControls.Label
    Protected WithEvents prePrev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents preNext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgpre.Columns(0).Visible = False
                dgpre.Columns(3).Visible = False
            End If

            cid = Request.QueryString("cid")
            If cid <> "" Then
                lblcid.Value = Request.QueryString("cid")
                cid = lblcid.Value
                appset.Open()
                PopPre(PageNumber)
                appset.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopPre(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopPre(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
        'prePrev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'prePrev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'preNext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'preNext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    ' Predictive
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopPre(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub PopPre(ByVal PageNumber As Integer)
        'Try
        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "pretech like ''%" & srch & "%'' and compid = ''" & cid & "'' and pretech <> ''Select''"
            FilterCnt = "pretech like '%" & srch & "%' and compid = '" & cid & "' and pretech <> 'Select'"
        Else
            Filter = "compid = ''" & cid & "'' and pretech <> ''Select''"
            FilterCnt = "compid = '" & cid & "' and pretech <> 'Select'"
        End If
        sql = "select count(*) " _
        + "from pmPreTech where " & FilterCnt 'compid = '" & cid & "'"
        dgpre.VirtualItemCount = appset.Scalar(sql)
        If dgpre.VirtualItemCount = 0 Then
            lblpre.Text = "No Predictive Technology Records Found"
            dgpre.Visible = True
            prePrev.Visible = False
            preNext.Visible = False

            'Filter = "compid = " & cid
            Tables = "pmPreTech"
            PK = "ptid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgpre.DataSource = dr
            dgpre.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgpre.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgpre.PageCount

        Else
            'Filter = "compid = " & cid
            Tables = "pmPreTech"
            PK = "ptid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgpre.DataSource = dr
            dgpre.DataBind()
            dr.Close()
            lblpgpre.Text = "Page " & dgpre.CurrentPageIndex + 1 & " of " & dgpre.PageCount
            lblpre.Text = ""
            prePrev.Visible = True
            preNext.Visible = True
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgpre.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgpre.PageCount

        End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopPre(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr96" , "AppSetTaskTabPT.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopPre(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr97" , "AppSetTaskTabPT.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgpre_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgpre.EditCommand
        PageNumber = txtpg.Value
        lblold.Value = CType(e.Item.FindControl("Label24"), Label).Text
        dgpre.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopPre(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgpre_UpdateCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgpre.UpdateCommand
        appset.Open()
        Dim id, desc As String
        Try
            id = CType(e.Item.FindControl("lblptidi"), Label).Text
        Catch ex As Exception
            id = CType(e.Item.FindControl("lblptid"), Label).Text
        End Try
        'id = CType(e.Item.FindControl("lblptid"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        Dim old As String = lblold.Value
        If old <> desc Then
            Dim faill As String = desc.ToLower
            cid = lblcid.Value
            sql = "select count(*) from pmPreTech where lower(pretech) = '" & faill & "' and compid = '" & cid & "'"
            Dim strchk As Integer
            strchk = appset.Scalar(sql)
            If strchk = 0 Then
                sql = "update pmPreTech set pretech = " _
                + "'" & desc & "' where ptid = '" & id & "'"
                appset.Update(sql)
                'appset.Dispose()
                dgpre.EditItemIndex = -1
                PopPre(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr98" , "AppSetTaskTabPT.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
            End If
        Else
            dgpre.EditItemIndex = -1
            PopPre(PageNumber)
        End If
        appset.Dispose()
    End Sub

    Private Sub dgpre_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgpre.CancelCommand
        dgpre.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopPre(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub dgpre_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgpre.DeleteCommand
        appset.Open()
        Dim id As String
        id = CType(e.Item.Cells(1).Controls(1), Label).Text
        Dim cid As String = lblcid.Value
        sql = "select count(*) from pmtasks where ptid = '" & id & "'"
        Dim pcnt As Integer = appset.Scalar(sql)
        If pcnt = 0 Then
            cid = lblcid.Value
            sql = "sp_delPMPreTech '" & id & "', '" & cid & "'"
            appset.Update(sql)
            dgpre.EditItemIndex = -1
            sql = "select Count(*) from pmPreTech " _
                    + "where compid = '" & cid & "'"
            PageNumber = appset.PageCount(sql, PageSize)
            'appset.Dispose()
            dgpre.EditItemIndex = -1
            If dgpre.CurrentPageIndex > PageNumber Then
                dgpre.CurrentPageIndex = PageNumber - 1
            End If
            If dgpre.CurrentPageIndex < PageNumber - 2 Then
                PageNumber = dgpre.CurrentPageIndex + 1
            End If
            PopPre(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr99" , "AppSetTaskTabPT.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub

    Private Sub prePrev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles prePrev.Click
        If (dgpre.CurrentPageIndex > 0) Then
            dgpre.CurrentPageIndex = dgpre.CurrentPageIndex - 1
            PopPre(dgpre.CurrentPageIndex - 1)
        End If
        checkPrePgCnt()
    End Sub
    Private Sub checkPrePgCnt()
        If (dgpre.CurrentPageIndex) = 0 Or dgpre.PageCount = 1 Then
            prePrev.Enabled = False
        Else
            prePrev.Enabled = True
        End If
        If dgpre.PageCount > 1 And (dgpre.CurrentPageIndex + 1 < dgpre.PageCount) Then
            preNext.Enabled = True
        Else
            preNext.Enabled = False
        End If
    End Sub
    Private Sub preNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles preNext.Click
        If ((dgpre.CurrentPageIndex) < (dgpre.PageCount - 1)) Then
            dgpre.CurrentPageIndex = dgpre.CurrentPageIndex + 1
            PopPre(dgpre.CurrentPageIndex + 1)
            checkPrePgCnt()
        End If
        checkPrePgCnt()
    End Sub



    Private Sub dgpre_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgpre.ItemCommand
        Dim lname As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewpre"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr100" , "AppSetTaskTabPT.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        Dim stat As String
                        cid = lblcid.Value
                        stat = lname
                        Dim strchk As Integer
                        Dim faill As String = stat.ToLower
                        sql = "select count(*) from pmPreTech where lower(pretech) = '" & faill & "' and compid = '" & cid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "sp_addPreTech '" & lname & "', " & cid
                            appset.Update(sql)
                            Dim statcnt As Integer = dgpre.VirtualItemCount + 1
                            lnamei.Text = ""
                            sql = "select Count(*) from pmpretech " _
                                            + "where compid = '" & cid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopPre(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr101" , "AppSetTaskTabPT.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If
                        appset.Dispose()
                    End If

                End If
            End If
        End If
    End Sub


	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgpre.Columns(0).HeaderText = dlabs.GetDGPage("AppSetTaskTabPT.aspx", "dgpre", "0")
        Catch ex As Exception
        End Try
        Try
            dgpre.Columns(2).HeaderText = dlabs.GetDGPage("AppSetTaskTabPT.aspx", "dgpre", "2")
        Catch ex As Exception
        End Try
        Try
            dgpre.Columns(3).HeaderText = dlabs.GetDGPage("AppSetTaskTabPT.aspx", "dgpre", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang111.Text = axlabs.GetASPXPage("AppSetTaskTabPT.aspx", "lang111")
        Catch ex As Exception
        End Try

    End Sub

End Class
