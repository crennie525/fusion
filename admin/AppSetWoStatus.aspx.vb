

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AppSetWoStatus
    Inherits System.Web.UI.Page
	Protected WithEvents lang124 As System.Web.UI.WebControls.Label

	Protected WithEvents lang123 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim rowcnt As Integer
    Private Pictures(15) As String
    Dim pcnt As Integer
    Dim dseq As DataSet
    Dim eqcnt As Integer
    Dim currRow As Integer
    Dim dept As String
    Dim Sort As String = "wostatus"
    Dim cid, cnm, sid, did, sql, ro As String
    Dim dr As SqlDataReader
    Protected WithEvents ddstatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbldef As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim appset As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbltasktype As System.Web.UI.WebControls.Label
    Protected WithEvents dgtt As System.Web.UI.WebControls.DataGrid
    Protected WithEvents ttdiv As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblold As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
            Session("curlang") = lblfslang.Value
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgtt.Columns(0).Visible = False
                dgtt.Columns(5).Visible = False
            End If
            cid = "0" 'Request.QueryString("cid")
            If cid <> "" Then
                lblcid.Value = cid
                cid = cid
                appset.Open()
                PopTasks(PageNumber)
                GetDefault()
                GetStatus()
                appset.Dispose()
            Else
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End If
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopTasks(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub
    Private Sub GetDefault()
        cid = lblcid.Value
        Dim def As Integer
        sql = "select wosid from wostatus where compid = '" & cid & "' and isdefault = 1"
        def = appset.Scalar(sql)
        lbldef.Value = def
    End Sub
    Private Sub ddstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddstatus.SelectedIndexChanged
        If ddstatus.SelectedIndex <> 0 Then
            Dim def As String = ddstatus.SelectedValue
            sql = "update wostatus set isdefault = 0 where compid = '" & cid & "'; update wostatus set isdefault = 1 where wosid = '" & def & "'"
            appset.Open()
            appset.Update(sql)
            lbldef.Value = def
            ddstatus.SelectedValue = def
            appset.Dispose()
        End If
    End Sub
    Private Sub GetStatus()
        cid = lblcid.Value
        sql = "select * from wostatus where compid = '" & cid & "' and active = 1"
        dr = appset.GetRdrData(sql)
        ddstatus.DataSource = dr
        ddstatus.DataTextField = "wostatus"
        ddstatus.DataValueField = "wosid"
        Try
            ddstatus.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddstatus.Items.Insert(0, "Select Default Status")
        Try
            If lbldef.Value <> "" Then
                ddstatus.SelectedValue = lbldef.Value
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopTasks(ByVal PageNumber As Integer)
        'Try

        cid = lblcid.Value
        Dim srch As String
        srch = txtsrch.Text
       srch = appset.ModString1(srch)
        If Len(srch) > 0 Then
            Filter = "wostatus like ''%" & srch & "%'' and compid = ''" & cid & "''"
            FilterCnt = "wostatus like '%" & srch & "%' and compid = '" & cid & "'"
        Else
            Filter = "compid = ''" & cid & "''"
            FilterCnt = "compid = '" & cid & "'"
        End If
        sql = "select count(*) " _
        + "from wostatus where " & FilterCnt 'compid = '" & cid & "'"
        dgtt.VirtualItemCount = appset.Scalar(sql)
        If dgtt.VirtualItemCount = 0 Then
            lbltasktype.Text = "No Work Order Status Records Found"
            dgtt.Visible = True
            Tables = "wostatus"
            PK = "wosid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        Else
            Tables = "wostatus"
            PK = "wosid"
            PageSize = "10"
            dr = appset.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
            dgtt.DataSource = dr
            dgtt.DataBind()
            dr.Close()
            lbltasktype.Text = ""
            txtpg.Value = PageNumber
            txtpgcnt.Value = dgtt.PageCount
            lblpg.Text = "Page " & PageNumber & " of " & dgtt.PageCount
        End If

        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr124" , "AppSetWoStatus.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopTasks(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr125" , "AppSetWoStatus.aspx.vb")
 
            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub dgtt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.ItemCommand
        Dim lname, wosd As String
        Dim lnamei As TextBox
        If e.CommandName = "Add" Then
            lnamei = CType(e.Item.FindControl("txtnewtt"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)

            wosd = CType(e.Item.FindControl("txtwosda"), TextBox).Text
            wosd = appset.ModString1(wosd)
            If lnamei.Text <> "" Then
                If lnamei.Text <> "" Then
                    If Len(lnamei.Text) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr126" , "AppSetWoStatus.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    ElseIf Len(wosd) > 50 Then
                        Dim strMessage As String =  tmod.getmsg("cdstr127" , "AppSetWoStatus.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Else
                        appset.Open()
                        cid = lblcid.Value
                        Dim tt As String
                        Dim strchk As Integer
                        Dim faill As String = lname.ToLower
                        sql = "select count(*) from wostatus where lower(wostatus) = '" & faill & "' and compid = '" & cid & "'"
                        strchk = appset.Scalar(sql)
                        If strchk = 0 Then
                            sql = "insert into wostatus (wostatus, description, compid) values ('" & lname & "','" & wosd & "','" & cid & "')"
                            appset.Update(sql)
                            Dim ttcnt As Integer = dgtt.VirtualItemCount + 1

                            lnamei.Text = ""
                            sql = "select Count(*) from wostatus " _
                               + "where compid = '" & cid & "'"
                            PageNumber = appset.PageCount(sql, PageSize)
                            PopTasks(PageNumber)
                        Else
                            Dim strMessage As String =  tmod.getmsg("cdstr128" , "AppSetWoStatus.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            appset.Dispose()
                        End If
                        appset.Dispose()
                    End If

                End If
            End If
        End If
    End Sub

    Private Sub dgtt_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.DeleteCommand
        appset.Open()
        Dim id, desc, def As String
        cid = lblcid.Value
        id = CType(e.Item.Cells(1).Controls(1), Label).Text
        def = lbldef.Value
        If id <> def Then
            Try
                desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
            Catch ex As Exception
                desc = CType(e.Item.Cells(2).Controls(1), Label).Text
            End Try

            sql = "select count(*) from workorder where status = '" & desc & "'"
            Dim tcnt As Integer = appset.Scalar(sql)
            If tcnt = 0 Then
                sql = "delete from wostatus where wosid = '" & id & "'"
                appset.Update(sql)
                dgtt.EditItemIndex = -1
                sql = "select Count(*) from wostatus " _
                       + "where compid = '" & cid & "'"
                PageNumber = appset.PageCount(sql, PageSize)
                'appset.Dispose()
                dgtt.EditItemIndex = -1
                If dgtt.CurrentPageIndex > PageNumber Then
                    dgtt.CurrentPageIndex = PageNumber - 1
                End If
                If dgtt.CurrentPageIndex < PageNumber - 2 Then
                    PageNumber = dgtt.CurrentPageIndex + 1
                End If
                PopTasks(PageNumber)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr129" , "AppSetWoStatus.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                appset.Dispose()
            End If
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr130" , "AppSetWoStatus.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            appset.Dispose()
        End If
        appset.Dispose()
    End Sub

    Private Sub dgtt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.EditCommand
        lblold.Value = CType(e.Item.Cells(2).Controls(1), Label).Text
        PageNumber = txtpg.Value
        dgtt.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.CancelCommand
        dgtt.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        PopTasks(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dgtt_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgtt.UpdateCommand
        appset.Open()
        Dim id, desc, act, old, def, wosd As String
        def = lbldef.Value
        id = CType(e.Item.FindControl("lblttide"), Label).Text
        desc = CType(e.Item.Cells(2).Controls(1), TextBox).Text
        desc = appset.ModString1(desc)
        If Len(desc) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr131" , "AppSetWoStatus.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        wosd = CType(e.Item.FindControl("txtwosde"), TextBox).Text
        wosd = appset.ModString1(wosd)
        If Len(wosd) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr132" , "AppSetWoStatus.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        act = CType(e.Item.FindControl("cbact"), CheckBox).Checked
        If act = False Then
            If id <> def Then
                Dim faill As String = desc.ToLower
                sql = "select count(*) from wostatus where lower(wostatus) = '" & faill & "' and compid = '" & cid & "'"
                Dim strchk As Integer
                old = lblold.Value
                If old <> desc Then
                    strchk = appset.Scalar(sql)
                Else
                    strchk = 0
                End If
                If strchk = 0 Then
                    sql = "update wostatus set wostatus = " _
                    + "'" & desc & "', active = '" & act & "', description = '" & wosd & "' where wosid = '" & id & "';update workorder set status = '" & desc & "' " _
                    + "where status = '" & desc & "';update wohist set wostatus = '" & desc & "' " _
                    + "where wostatus = '" & desc & "'"
                    appset.Update(sql)
                    'appset.Dispose()
                    dgtt.EditItemIndex = -1
                    PopTasks(PageNumber)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr133" , "AppSetWoStatus.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    appset.Dispose()
                End If
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr134" , "AppSetWoStatus.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            sql = "update wostatus set wostatus = " _
               + "'" & desc & "', active = '" & act & "', description = '" & wosd & "'  where wosid = '" & id & "';update workorder set status = '" & desc & "' " _
               + "where status = '" & desc & "'"
            appset.Update(sql)
            'appset.Dispose()
            dgtt.EditItemIndex = -1
            PopTasks(PageNumber)
        End If
        appset.Dispose()
    End Sub


    Private Sub dgtt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgtt.ItemDataBound
        Dim ibfm As ImageButton
        Dim ibfm1 As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim stat As String = DataBinder.Eval(e.Item.DataItem, "wostatus").ToString
            If stat = "COMP" Or stat = "CAN" Or stat = "WAPPR" Or stat = "APPR" Or stat = "INPRG" Or stat = "PMSCHED" Then
                ibfm = CType(e.Item.FindControl("imgedit"), ImageButton)
                ibfm.Attributes.Add("class", "details")
                ibfm1 = CType(e.Item.FindControl("imgdel"), ImageButton)
                ibfm1.Attributes.Add("class", "details")

            End If
        End If
    End Sub

    Private Sub dgtt_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgtt.Unload

    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgtt.Columns(0).HeaderText = dlabs.GetDGPage("AppSetWoStatus.aspx", "dgtt", "0")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(2).HeaderText = dlabs.GetDGPage("AppSetWoStatus.aspx", "dgtt", "2")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(3).HeaderText = dlabs.GetDGPage("AppSetWoStatus.aspx", "dgtt", "3")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(4).HeaderText = dlabs.GetDGPage("AppSetWoStatus.aspx", "dgtt", "4")
        Catch ex As Exception
        End Try
        Try
            dgtt.Columns(5).HeaderText = dlabs.GetDGPage("AppSetWoStatus.aspx", "dgtt", "5")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang123.Text = axlabs.GetASPXPage("AppSetWoStatus.aspx", "lang123")
        Catch ex As Exception
        End Try
        Try
            lang124.Text = axlabs.GetASPXPage("AppSetWoStatus.aspx", "lang124")
        Catch ex As Exception
        End Try

    End Sub

End Class
