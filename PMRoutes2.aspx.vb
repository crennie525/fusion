

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PMRoutes2
    Inherits System.Web.UI.Page
    Protected WithEvents ovid172 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang1205 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1204 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1203 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1202 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1201 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1200 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1199 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1198 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1197 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1196 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim rt As New Utilities
    Dim rid, sid, start, subm, from, ro, rostr, typ, ptid, days, shift As String
    Protected WithEvents lblroute As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrtid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblseqret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbljp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewroute As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrow As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfrom As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifrt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbldidh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcellidh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocidh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncidh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifrt1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblrtyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgsav As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents dglist As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbloldrte As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillqty As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrdid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblptid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldays As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshift As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnsavetask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ddeqstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtfreq As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddskill As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddpt As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddtype As System.Web.UI.WebControls.DropDownList
    Protected WithEvents trrb As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdseqno As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblskillidb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblttidb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblptidb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrdidb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfreqb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmtskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewttid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblolddesc As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtroute As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtroutedesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddtyp As System.Web.UI.WebControls.DropDownList
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdrt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdrtd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnsub As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents tdret As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnsub1 As System.Web.UI.HtmlControls.HtmlInputButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim Login As String
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            GetFSOVLIBS()

            GetDGLangs()

            GetFSLangs()
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                imgadd.Attributes.Add("onclick", "")
                imgsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                imgsav.Attributes.Add("onclick", "")
                btnsub.Disabled = True
            Else
                rostr = HttpContext.Current.Session("rostr").ToString
                If Len(rostr) <> 0 Then
                    ro = rt.CheckROS(rostr, "setup")
                    lblro.Value = ro
                    If ro = "1" Then
                        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                        imgadd.Attributes.Add("onclick", "")
                        imgsav.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                        imgsav.Attributes.Add("onclick", "")
                        btnsub.Disabled = True
                    End If
                End If
            End If
            If ro = "1" Then
                'pgtitle.InnerHtml = "PM Routes (Read Only)"
            Else
                'pgtitle.InnerHtml = "PM Routes"
            End If
            start = Request.QueryString("start").ToString
            from = Request.QueryString("from").ToString
            lblfrom.Value = from
            sid = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = sid
            'ddtyp.SelectedValue = "PM"
            Try
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
            Catch ex As Exception
                lbltyp.Value = "PM"
            End Try
            If typ = "RBAS" Or typ = "RBAST" Then
                trrb.Attributes.Add("class", "view")
            Else
                trrb.Attributes.Add("class", "details")
            End If
            If start = "yes" Then
                rid = Request.QueryString("rid").ToString
                lblroute.Value = rid
                rt.Open()
                GetDetails(rid)
                GetList(rid)
                If typ = "RBAS" Or typ = "RBAST" Then
                    GetLists(typ, rid)
                End If
                rt.Dispose()
            Else
                rid = "0"
                rt.Open()
                GetList(rid)
                If typ = "RBAS" Or typ = "RBAST" Then
                    GetLists(typ, rid)
                End If
                rt.Dispose()
                ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
            End If

            ddtype.Attributes.Add("onchange", "GetType('d', this.value);")
        Else
            subm = Request.Form("lblsubmit").ToString
            rid = lblroute.Value
            If subm = "addrtman" Then
                lblsubmit.Value = ""
                rt.Open()
                AddRoute()
                rt.Dispose()
            ElseIf subm = "add" Then
                lblsubmit.Value = ""
                rt.Open()
                AddSeq()
                rt.Dispose()
            ElseIf subm = "savert" Then
                lblsubmit.Value = ""
                rt.Open()
                SaveRT()

                rt.Dispose()
            ElseIf subm = "lookup" Then
                lblsubmit.Value = ""
                rt.Open()
                rid = lblroute.Value
                GetDetails(rid)
                GetList(rid)
                If typ = "RBAS" Or typ = "RBAST" Then
                    GetLists(typ, rid)
                End If
                rt.Dispose()
            ElseIf subm = "gettasks" Then
                lblsubmit.Value = ""
                lblret.Value = ""
                rt.Open()
                rid = lblroute.Value
                GetList(rid)
                If typ = "RBAS" Or typ = "RBAST" Then
                    GetLists(typ, rid)
                End If
                rt.Dispose()
            End If
        End If
    End Sub
    Private Sub GetLists(ByVal typ As String, ByVal rid As String)
        'tdseqno.InnerHtml = ""
        txtfreq.Text = ""
        txtfreq.Enabled = True
        ddtype.Enabled = True
        If typ = "RBAS" Then
            ddpt.Enabled = True
        End If
        ddskill.Enabled = True
        ddeqstat.Enabled = True

        lblskillidb.Value = ""
        lblttidb.Value = ""
        lblptidb.Value = ""
        lblrdidb.Value = ""
        lblfreqb.Value = ""

        sql = "select ttid, tasktype " _
        + "from pmTaskTypes where tasktype <> 'Select' order by compid"
        dr = rt.GetRdrData(sql)
        ddtype.DataSource = dr
        ddtype.DataBind()
        dr.Close()
        ddtype.Items.Insert(0, New ListItem("Select"))
        ddtype.Items(0).Value = 0

        sql = "select ptid, pretech " _
        + "from pmPreTech order by compid"
        dr = rt.GetRdrData(sql)
        ddpt.DataSource = dr
        ddpt.DataTextField = "pretech"
        ddpt.DataValueField = "ptid"
        ddpt.DataBind()
        dr.Close()
        ddpt.Items.Insert(0, New ListItem("None"))
        ddpt.Items(0).Value = 0

        sid = lblsid.Value
        Dim scnt As Integer
        sql = "select count(*) from pmSiteSkills where siteid = '" & sid & "'"
        scnt = rt.Scalar(sql)
        If scnt <> 0 Then
            sql = "select skillid, skill " _
            + "from pmSiteSkills where siteid = '" & sid & "' order by skill"
        Else
            sql = "select skillid, skill " _
            + "from pmSkills order by skill"
        End If
        dr = rt.GetRdrData(sql)
        ddskill.DataSource = dr
        ddskill.DataTextField = "skill"
        ddskill.DataValueField = "skillid"
        ddskill.DataBind()
        dr.Close()
        ddskill.Items.Insert(0, New ListItem("Select"))
        ddskill.Items(0).Value = 0

        sql = "select statid, status " _
        + "from pmStatus order by compid"
        dr = rt.GetRdrData(sql)
        ddeqstat.DataSource = dr
        ddeqstat.DataTextField = "status"
        ddeqstat.DataValueField = "statid"
        ddeqstat.DataBind()
        dr.Close()
        ddeqstat.Items.Insert(0, New ListItem("Select"))
        ddeqstat.Items(0).Value = 0

        Dim skillid, freq, rdid, ptid As String

        sql = "select skillid, freq, rdid, ptid from pmroutes where rid = '" & rid & "'"
        dr = rt.GetRdrData(sql)
        While dr.Read
            skillid = dr.Item("skillid").ToString
            freq = dr.Item("freq").ToString
            rdid = dr.Item("rdid").ToString
            ptid = dr.Item("ptid").ToString
        End While
        dr.Close()

        ddskill.SelectedValue = skillid
        txtfreq.Text = freq
        ddeqstat.SelectedValue = rdid
        If ptid <> "0" Then
            ddpt.SelectedValue = ptid
        Else
            ddpt.Enabled = False
        End If

        Dim ttid As String
        ttid = lblnewttid.Value
        Dim ttidcnt, ittid As Integer
        If ttid <> "" Then
            ddtype.SelectedValue = ttid
        Else
            sql = "select count(distinct ttid) from pmtasks where rteid = '" & rid & "'"
            ttidcnt = rt.Scalar(sql)
            If ttidcnt = 1 Then
                sql = "select top 1 ttid from pmtasks where rteid = '" & rid & "'"
                ittid = rt.Scalar(sql)
                ddtype.SelectedValue = ittid
            End If
        End If
    End Sub
    Private Sub GetDetails(ByVal route As String)
        Dim rte, rtd, skillid, qty, freq, rdid As String
        sql = "select isnull(ptid, 0) as ptid, rid, route, description, rttype, skillid, qty, freq, rdid, days, shift from pmroutes where rid = '" & route & "'"
        dr = rt.GetRdrData(sql)
        While dr.Read
            ptid = dr.Item("ptid").ToString
            skillid = dr.Item("skillid").ToString
            qty = dr.Item("qty").ToString
            freq = dr.Item("freq").ToString
            rdid = dr.Item("rdid").ToString
            rte = dr.Item("route").ToString
            rtd = dr.Item("description").ToString
            lbltyp.Value = dr.Item("rttype").ToString
            days = dr.Item("days").ToString
            shift = dr.Item("shift").ToString
            Try
                'ddtyp.SelectedValue = dr.Item("rttype").ToString
            Catch ex As Exception

            End Try
        End While
        dr.Close()
        lblptid.Value = ptid
        lblskillid.Value = skillid
        lblskillqty.Value = qty
        lblfreq.Value = freq
        lblrdid.Value = rdid
        lbldays.Value = days
        lblshift.Value = shift
        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/refreshit.gif")
        imgadd.Attributes.Add("onclick", "refit();")
        imgadd.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov145", "PMRoutes2.aspx.vb") & "', ABOVE, LEFT)")
        imgadd.Attributes.Add("onmouseout", "return nd()")
        txtroute.Text = rte
        lbloldrte.Value = rte
        txtroutedesc.Text = rtd
        lblolddesc.Value = rtd
        tdrt.InnerHtml = rte
        tdrtd.InnerHtml = rtd
    End Sub
    Private Sub GetList(Optional ByVal route As String = "0")
        If route <> "0" Then
            route = lblroute.Value
        End If
        
        
        typ = lbltyp.Value
        If typ = "PM" Then
            sql = "select distinct s.*, e.eqnum, n.ncnum, d.dept_line + '/' + c.cell_name as dcell, " _
        + "pmnum = (t.skill + '(' + cast(t.qty as varchar(10)) + ')/' + t.freq + ' days/' + t.rd),  j.jpnum, " _
        + "s.pmstr, s.skillid, s.skillqty, s.freq, s.rdid, '' as func, '' as task, '' as compnum, t.ttid, t.ptid, t.pmtskid " _
        + "l.location from pmroute_stops s " _
        + "left join equipment e on e.eqid = s.eqid " _
        + "left join noncritical n on n.ncid = s.ncid " _
        + "left join dept d on d.dept_id = s.deptid " _
        + "left join cells c on c.cellid = s.cellid " _
        + "left join pmtasks t on t.pmid = s.pmid and t.pmid <> 0 " _
        + "left join pmjobplans j on j.jpid = s.jpid " _
        + "left join pmlocations l on l.locid = s.locid " _
        + "where s.rid = '" & route & "' and s.rid <> '0' order by s.stopsequence"
        ElseIf typ = "RBAS" Then
            Try
                sql = "exec usp_uprteseq_tmp '" & route & "'"
                rt.Update(sql)
            Catch ex As Exception

            End Try
            ''' as location,
            sql = "select distinct e.eqnum, f.func, t.taskdesc as task, isnull(t.rteseq, 0) as stopsequence, t.rteseq, '' as ncnum,  " _
                + "'' as dcell, '' as pmnum, '' as jpnum, '' as task, t.eqid as eqid, '' as pmid, '' as jpid, " _
                + "t.pmtskid as rsid, '' as locid, '' as cellid, '' as deptid, '' as ncid, '' as pmstr, t.skillid as skillid, t.qty as skillqty, " _
                + "t.freq as freq, t.rdid as rdid, location = (select top 1 h.parent from pmlocheir h where h.location = e.eqnum), t.compnum, t.ttid, t.ptid, t.pmtskid " _
        + " from pmtasks t " _
        + "left join equipment e on e.eqid = t.eqid " _
        + "left join functions f on f.func_id = t.funcid " _
        + "where t.rteid = '" & route & "' and subtask = 0 and t.rteseq is not null order by t.rteseq"
        ElseIf typ = "RBAST" Then
            Try
                sql = "exec usp_uprteseqtpm_tmp '" & route & "'"
                rt.Update(sql)
            Catch ex As Exception

            End Try
            ''' as location,
            sql = "select distinct e.eqnum, f.func, t.taskdesc as task, isnull(t.rteseq, 0) as stopsequence, t.rteseq, '' as ncnum,  " _
                + "'' as dcell, '' as pmnum, '' as jpnum, '' as task, t.eqid as eqid, '' as pmid, '' as jpid, " _
                + "t.pmtskid as rsid, '' as locid, '' as cellid, '' as deptid, '' as ncid, '' as pmstr, t.skillid as skillid, t.qty as skillqty, " _
                + "t.freq as freq, t.rdid as rdid, location = (select top 1 h.parent from pmlocheir h where h.location = e.eqnum), t.compnum, t.ttid, t.ptid, t.pmtskid " _
        + " from pmtaskstpm t " _
        + "left join equipment e on e.eqid = t.eqid " _
        + "left join functions f on f.func_id = t.funcid " _
        + "where t.rteid = '" & route & "' and subtask = 0 and t.rteseq is not null order by t.rteseq"
        Else
            sql = "select distinct s.*, e.eqnum, n.ncnum, d.dept_line + '/' + c.cell_name as dcell, " _
        + "pmnum = t.DESCRIPTION,  j.jpnum, " _
        + "s.pmstr, s.skillid, s.skillqty, s.freq, s.rdid, '' as func, '' as task, '' as compnum, t.ttid, t.ptid, t.pmtskid " _
        + "l.location from pmroute_stops s " _
        + "left join equipment e on e.eqid = s.eqid " _
        + "left join noncritical n on n.ncid = s.ncid " _
        + "left join dept d on d.dept_id = s.deptid " _
        + "left join cells c on c.cellid = s.cellid " _
        + "left join pmmax t on t.pmid = s.pmid and t.pmid <> 0 " _
        + "left join pmjobplans j on j.jpid = s.jpid " _
        + "left join pmlocations l on l.locid = s.locid " _
        + "where s.rid = '" & route & "' and s.rid <> '0' order by s.stopsequence"
        End If


        dr = rt.GetRdrData(sql)
        dglist.DataSource = dr
        dglist.DataBind()
        dr.Close()
        ro = lblro.Value
        If ro = "1" Then
            dglist.Columns(0).Visible = False
            dglist.Columns(14).Visible = False
        End If
    End Sub
    Private Sub SaveRT()
        Dim route As String = lblroute.Value
        Dim rte As String = txtroute.Text
        rte = Replace(rte, "'", "''")
        Dim oldroute As String = lbloldrte.Value
        Dim olddesc As String = lblolddesc.Value
        Dim rtd As String = txtroutedesc.Text
        rtd = Replace(rtd, "'", "''")
        sid = lblsid.Value
        Dim typ As String = lbltyp.Value ';ddtyp.SelectedValue.ToString
        'lblrtyp.Value = typ
        If rte = oldroute Then
            If rtd = olddesc Then
                Dim strMessage As String = "No changes were made to route name or description to save."
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If

        Dim cnt As Integer
        If rte <> oldroute Then
            sql = "select count(*) from pmroutes where route = '" & rte & "' and siteid = '" & sid & "'"
            cnt = rt.Scalar(sql)
        Else
            cnt = 0
        End If
        
        If cnt > 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr498", "PMRoutes2.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf cnt = 0 Then
            sql = "usp_updateroute '" & rte & "', '" & rtd & "', '" & typ & "', '" & route & "'"
            rt.Update(sql)
            rid = lblroute.Value
            GetDetails(rid)
            GetList(rid)
            lbloldrte.Value = rte
        Else
            Dim strMessage As String = tmod.getmsg("cdstr500", "PMRoutes2.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
    Private Sub AddRoute()
        typ = lbltyp.Value
        Dim txt, txtd, uid As String
        txt = txtroute.Text
        txtd = txtroutedesc.Text
        txt = Replace(txt, "'", "''")
        txtd = Replace(txtd, "'", "''")
        lbloldrte.Value = txt
        uid = HttpContext.Current.Session("uid").ToString '"pmadmin1" '
        uid = Replace(uid, "'", "''")
        sid = lblsid.Value
        'Dim typ As String = ddtyp.SelectedValue.ToString
        lbltyp.Value = typ
        Dim rd As Integer
        Dim cnt As Integer
        sql = "select count(*) from pmroutes where route = '" & txt & "' and siteid = '" & sid & "'"
        cnt = rt.Scalar(sql)
        If cnt = 0 Then
            sql = "insert into pmroutes (siteid, route, rttype, description, createdby, createdate) " _
                   + "values ('" & sid & "','" & txt & "','" & typ & "','" & txtd & "','" & uid & "', getDate()) " _
                   + "select @@identity as 'identity'"
            rd = rt.Scalar(sql)
            lblroute.Value = rd
            GetDetails(rd)
            GetList(rd)
            If typ = "RBAS" Then
                lblret.Value = "addRBAS"
            ElseIf typ = "RBAST" Then
                lblret.Value = "addRBAST"
            End If
        Else
            Dim strMessage As String = tmod.getmsg("cdstr501", "PMRoutes2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub AddSeq()
        rid = lblroute.Value
        sql = "usp_addrtseq '" & rid & "'"
        rt.Update(sql)
        GetList(rid)
    End Sub

    Private Sub dglist_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.EditCommand
        lblrow.Value = CType(e.Item.FindControl("lblseq"), Label).Text
        rt.Open()
        Dim route As String = lblroute.Value
        dglist.EditItemIndex = e.Item.ItemIndex
        GetList(route)
        rt.Dispose()
    End Sub

    Private Sub dglist_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.CancelCommand
        dglist.EditItemIndex = -1
        rt.Open()
        Dim route As String = lblroute.Value
        GetList(route)
        rt.Dispose()
    End Sub

    Private Sub dglist_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dglist.ItemDataBound
        typ = lbltyp.Value
        If e.Item.ItemType <> ListItemType.Header And _
                                 e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibDel"), ImageButton)
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Route Stop?')"
        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim lid As String = CType(e.Item.FindControl("lbleqs"), Label).ClientID
            Dim eq As String = CType(e.Item.FindControl("lbleqid"), Label).Text
            Dim b1 As HtmlImage = CType(e.Item.FindControl("lookupeq"), HtmlImage)
            Dim lid2 As String = CType(e.Item.FindControl("lblpms"), Label).ClientID
            Dim pm As String = CType(e.Item.FindControl("lblpmid"), Label).Text
            Dim b2 As HtmlImage = CType(e.Item.FindControl("lookuppm"), HtmlImage)
            Dim lid3 As String = CType(e.Item.FindControl("lbldcell"), Label).ClientID
            Dim did As String = CType(e.Item.FindControl("lbldid"), Label).Text
            Dim clid As String = CType(e.Item.FindControl("lblclid"), Label).Text
            Dim locid As String = CType(e.Item.FindControl("lbllocid"), Label).Text
            Dim lid4 As String = CType(e.Item.FindControl("lbllocs"), Label).ClientID
            Dim lid5 As String = CType(e.Item.FindControl("lbljps"), Label).ClientID
            Dim jp As String = CType(e.Item.FindControl("lbljpid"), Label).Text
            Dim lid6 As String = CType(e.Item.FindControl("lblnc"), Label).ClientID
            Dim nc As String = CType(e.Item.FindControl("lblncid"), Label).Text
            Dim test As String = nc
            Dim ttid As String = CType(e.Item.FindControl("lblttide"), Label).Text
            Dim ptid As String = CType(e.Item.FindControl("lblptide"), Label).Text
            Dim skillid As String = CType(e.Item.FindControl("lblskillide"), Label).Text
            Dim rdid As String = CType(e.Item.FindControl("lblrdide"), Label).Text
            Dim freq As String = CType(e.Item.FindControl("lblfreqe"), Label).Text
            Dim seq As String = CType(e.Item.FindControl("txtseq"), TextBox).Text
            Dim pmtskid As String = CType(e.Item.FindControl("lblpmtskide"), Label).Text
            Dim rsid As String = CType(e.Item.FindControl("lblrsid"), Label).Text
            'Dim lid5 As String = CType(e.Item.FindControl("lbllocid"), Label).ClientID
            'Dim lid6 As String = CType(e.Item.FindControl("lbllocs"), Label).ClientID
            'Dim lid7 As String = CType(e.Item.FindControl("lbljpid"), Label).ClientID
            'Dim lid8 As String = CType(e.Item.FindControl("lbljps"), Label).ClientID
            Dim b3 As HtmlImage = CType(e.Item.FindControl("lookuploc"), HtmlImage)
            Dim b4 As HtmlImage = CType(e.Item.FindControl("lookupjp"), HtmlImage)
            Dim b5 As HtmlImage = CType(e.Item.FindControl("lookupdcell"), HtmlImage)
            Dim b6 As HtmlImage = CType(e.Item.FindControl("lookupnc"), HtmlImage)
            b6.Attributes("onclick") = "getrt('nc','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"
            b4.Attributes("onclick") = "getrt('jp','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"
            b5.Attributes("onclick") = "getrt('dc','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"
            b3.Attributes("onclick") = "srchloc('loc','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"
            If typ = "PM" Then
                b2.Attributes("onclick") = "getrt('pm','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"
            ElseIf typ = "PMMAX" Then
                b2.Attributes("onclick") = "getrt('pmmax','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"
            ElseIf typ = "RBAS" Then
                b2.Attributes("onclick") = "getrt('rbas','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"

                'txtfreq.Enabled = True
                'ddtype.Enabled = True
                'ddpt.Enabled = True
                'ddskill.Enabled = True
                'ddeqstat.Enabled = True

                'tdseqno.InnerHtml = seq
                'ddtype.SelectedValue = ttid
                'ddpt.SelectedValue = ptid
                'ddskill.SelectedValue = skillid
                'ddeqstat.SelectedValue = rdid
                'txtfreq.Text = freq

                lblptidb.Value = ptid
                lblskillidb.Value = skillid
                lblttidb.Value = ttid
                lblfreqb.Value = freq
                lblrdid.Value = rdid
                lblpmtskid.Value = pmtskid
                lblrsid.Value = rsid

            ElseIf typ = "RBAST" Then
                b2.Attributes("onclick") = "getrt('rbast','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"

                'txtfreq.Enabled = True
                'ddtype.Enabled = True
                'ddpt.Enabled = False
                'ddskill.Enabled = False
                'ddeqstat.Enabled = True

                'tdseqno.InnerHtml = seq
                'ddtype.SelectedValue = ttid
                ''ddpt.SelectedValue = ptid
                ''ddskill.SelectedValue = skillid
                'ddeqstat.SelectedValue = rdid
                'txtfreq.Text = freq

                'lblptidb.Value = ptid
                'lblskillidb.Value = skillid
                lblttidb.Value = ttid
                lblfreqb.Value = freq
                lblrdidb.Value = rdid
                lblpmtskid.Value = pmtskid
                lblrsid.Value = rsid
            End If

            b1.Attributes("onclick") = "getrt('eq','" & lid & "','" & eq & "','" & lid2 & "','" & pm & "','" & lid3 & "','" & did & "','" & clid & "','" & lid4 & "','" & locid & "','" & lid5 & "','" & jp & "','" & lid6 & "','" & nc & "');"
        End If

        'Dim typ As String = ddtyp.SelectedValue.ToString
        If typ = "PM" Or typ = "PMMAX" Then
            dglist.Columns(2).Visible = True
            dglist.Columns(3).Visible = False
            dglist.Columns(4).Visible = False
            dglist.Columns(5).Visible = False
            dglist.Columns(6).Visible = True
            dglist.Columns(7).Visible = False
            dglist.Columns(8).Visible = True
            dglist.Columns(9).Visible = False
            dglist.Columns(10).Visible = True
            dglist.Columns(11).Visible = True
            dglist.Columns(12).Visible = False
            dglist.Columns(13).Visible = False
            dglist.Columns(14).Visible = False
            dglist.Columns(15).Visible = False
        ElseIf typ = "RBAS" Or typ = "RBAST" Then
            dglist.Columns(2).Visible = True
            dglist.Columns(3).Visible = False
            dglist.Columns(4).Visible = True
            dglist.Columns(5).Visible = False
            dglist.Columns(6).Visible = False
            dglist.Columns(7).Visible = False
            dglist.Columns(8).Visible = False
            dglist.Columns(9).Visible = False
            dglist.Columns(10).Visible = False
            dglist.Columns(11).Visible = False
            dglist.Columns(12).Visible = False
            dglist.Columns(13).Visible = False
            dglist.Columns(14).Visible = True
            dglist.Columns(15).Visible = True
        ElseIf typ = "JP" Then
            dglist.Columns(2).Visible = True
            dglist.Columns(3).Visible = False
            dglist.Columns(4).Visible = True
            dglist.Columns(5).Visible = False
            dglist.Columns(6).Visible = True
            dglist.Columns(7).Visible = False
            dglist.Columns(8).Visible = True
            dglist.Columns(9).Visible = False
            dglist.Columns(10).Visible = False
            dglist.Columns(11).Visible = False
            dglist.Columns(12).Visible = True
            dglist.Columns(13).Visible = True
            dglist.Columns(14).Visible = False
            dglist.Columns(15).Visible = False
        ElseIf typ = "EQ" Then
            dglist.Columns(2).Visible = True
            dglist.Columns(3).Visible = True
            dglist.Columns(4).Visible = False
            dglist.Columns(5).Visible = False
            dglist.Columns(6).Visible = False
            dglist.Columns(7).Visible = False
            dglist.Columns(8).Visible = False
            dglist.Columns(9).Visible = False
            dglist.Columns(10).Visible = False
            dglist.Columns(11).Visible = False
            dglist.Columns(12).Visible = False
            dglist.Columns(13).Visible = False
            dglist.Columns(14).Visible = False
            dglist.Columns(15).Visible = False
        ElseIf typ = "NC" Then
            dglist.Columns(2).Visible = False
            dglist.Columns(3).Visible = False
            dglist.Columns(4).Visible = True
            dglist.Columns(5).Visible = True
            dglist.Columns(6).Visible = False
            dglist.Columns(7).Visible = False
            dglist.Columns(8).Visible = False
            dglist.Columns(9).Visible = False
            dglist.Columns(10).Visible = False
            dglist.Columns(11).Visible = False
            dglist.Columns(12).Visible = False
            dglist.Columns(13).Visible = False
            dglist.Columns(14).Visible = False
            dglist.Columns(15).Visible = False
        ElseIf typ = "DC" Then
            dglist.Columns(2).Visible = False
            dglist.Columns(3).Visible = False
            dglist.Columns(4).Visible = False
            dglist.Columns(5).Visible = False
            dglist.Columns(6).Visible = True
            dglist.Columns(7).Visible = True
            dglist.Columns(8).Visible = False
            dglist.Columns(9).Visible = False
            dglist.Columns(10).Visible = False
            dglist.Columns(11).Visible = False
            dglist.Columns(12).Visible = False
            dglist.Columns(13).Visible = False
            dglist.Columns(14).Visible = False
            dglist.Columns(15).Visible = False
        ElseIf typ = "LO" Then
            dglist.Columns(2).Visible = False
            dglist.Columns(3).Visible = False
            dglist.Columns(4).Visible = False
            dglist.Columns(5).Visible = False
            dglist.Columns(6).Visible = False
            dglist.Columns(7).Visible = False
            dglist.Columns(8).Visible = True
            dglist.Columns(9).Visible = True
            dglist.Columns(10).Visible = False
            dglist.Columns(11).Visible = False
            dglist.Columns(12).Visible = False
            dglist.Columns(13).Visible = False
            dglist.Columns(14).Visible = False
            dglist.Columns(15).Visible = False
        End If
    End Sub

    Private Sub dglist_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.DeleteCommand
        dglist.EditItemIndex = -1
        Dim rsid, rteid As String
        rteid = lblroute.Value
        Try
            rsid = CType(e.Item.FindControl("lblrsid"), Label).Text
        Catch ex As Exception
            rsid = CType(e.Item.FindControl("lblrsidi"), Label).Text
        End Try
        Dim typ As String = lbltyp.Value
        If typ = "RBAS" Then
            sql = "usp_rem_rbas_seq '" & rteid & "','" & rsid & "'"
        ElseIf typ = "RBAST" Then
            sql = "usp_rem_rbas_seqT '" & rteid & "','" & rsid & "'"
        Else
            sql = "usp_delroutestop '" & rsid & "'"
        End If

        rt.Open()
        rt.Update(sql)
        Dim route As String = lblroute.Value
        dglist.EditItemIndex = -1
        GetList(route)
        rt.Dispose()
    End Sub

    Private Sub dglist_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.UpdateCommand
        Dim orte, crte, rteid As String
        rteid = lblroute.Value
        orte = lbloldrte.Value
        crte = txtroute.Text
        Dim otyp, ctyp As String
        otyp = lbltyp.Value
        ctyp = lbltyp.Value 'ddtyp.SelectedValue
        Dim rsid, rid, eqid, pmid, jpid, st, ost, did, clid, lid, ncid As String

        If orte <> crte Then
            Dim strMessage As String = tmod.getmsg("cdstr502", "PMRoutes2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf otyp <> ctyp Then
            Dim strMessage As String = tmod.getmsg("cdstr503", "PMRoutes2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else

            ost = lblrow.Value
            rid = lblroute.Value
            rsid = CType(e.Item.FindControl("lblrsid"), Label).Text

            eqid = lbleq.Value
            If eqid = "" Or eqid = "0" Then
                eqid = CType(e.Item.FindControl("lbleqid"), Label).Text
            Else

            End If
            pmid = lblpm.Value
            If pmid = "" Or pmid = "0" Then
                pmid = CType(e.Item.FindControl("lblpmid"), Label).Text
            Else

            End If
            jpid = lbljp.Value
            If jpid = "" Or jpid = "0" Then
                jpid = CType(e.Item.FindControl("lbljpid"), Label).Text
            Else

            End If
            did = lbldidh.Value
            If did = "" Or did = "0" Then
                did = CType(e.Item.FindControl("lbldid"), Label).Text
            End If
            clid = lblcellidh.Value
            If clid = "" Or clid = "0" Then
                clid = CType(e.Item.FindControl("lblclid"), Label).Text
            Else

            End If
            lid = lbllocidh.Value
            If lid = "" Or lid = "0" Then
                lid = CType(e.Item.FindControl("lbllocid"), Label).Text
            Else

            End If
            ncid = lblncidh.Value
            If ncid = "0" Or ncid = "0" Then
                ncid = CType(e.Item.FindControl("lblncid"), Label).Text
            Else

            End If
            rt.Open()

            Dim pmstr, skillid, skillqty, freq, rdid, eqnum, parent As String
            st = CType(e.Item.FindControl("txtseq"), TextBox).Text

            'If otyp = "RBAS" Then
            eqid = lbleq.Value
            If eqid = "" Or eqid = "0" Then
                eqid = CType(e.Item.FindControl("lbleqid"), Label).Text
            End If
            pmstr = lblpmstr.Value
            If pmstr = "" Or pmstr = "0" Then
                pmstr = CType(e.Item.FindControl("lblpmstre"), Label).Text
            End If
            skillid = lblskillid.Value
            If skillid = "" Or skillid = "0" Then
                skillid = CType(e.Item.FindControl("lblskillide"), Label).Text
            End If
            skillqty = lblskillqty.Value
            If skillqty = "" Or skillqty = "0" Then
                skillqty = CType(e.Item.FindControl("lblskillqtye"), Label).Text
            End If
            freq = lblfreq.Value
            If freq = "" Or freq = "0" Then
                freq = CType(e.Item.FindControl("lblfreqe"), Label).Text
            End If
            rdid = lblrdid.Value
            If rdid = "" Or rdid = "0" Then
                rdid = CType(e.Item.FindControl("lblrdide"), Label).Text
            End If
            'usp_rbas_rev_seq(@rteid int, @pmtskid int, @skillid int, @qty int, @freq int, @rdid int, @rte int, @oldrte int)
            Dim stchk As Long
            Try

                stchk = System.Convert.ToInt64(st)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr238", "PMTaskDivFunc.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            If stchk < 1 Then
                st = ost
                Dim strMessage As String = "Task Number Must Be Greater Than Zero!"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else

            End If
            If otyp = "RBAS" Then
                sql = "usp_rbas_rev_seq '" & rteid & "','" & rsid & "','" & skillid & "','" & skillqty & "','" & freq & "','" & rdid & "','" & st & "','" & ost & "'"
            ElseIf otyp = "RBAST" Then
                sql = "usp_rbas_rev_seqT '" & rteid & "','" & rsid & "','" & skillid & "','" & skillqty & "','" & freq & "','" & rdid & "','" & st & "','" & ost & "'"
            Else
                sql = "update pmroute_stops set eqid = '" & eqid & "', " _
            + "pmid = '" & pmid & "', jpid = '" & jpid & "', " _
            + "deptid = '" & did & "', cellid = '" & clid & "', " _
            + "ncid = '" & ncid & "', locid = '" & lid & "' " _
            + "where rsid = '" & rsid & "'"
            End If
            Dim tst As String = sql
            Dim typ As String = lbltyp.Value
            rt.Update(sql)
            If ost <> st Then
                If typ <> "RBAS" Then
                    sql = "usp_reroutestops '" & rid & "', '" & st & "', '" & ost & "'"
                    rt.Update(sql)
                End If


            End If
            'PM Update Here

            If typ = "PM" Then
                sql = "update pm set rid = '" & rid & "' where pmid = '" & pmid & "'"
                rt.Update(sql)
            ElseIf typ = "PMMAX" Then
                sql = "update pmmax set pm1 = '" & rid & "' where pmid = '" & pmid & "'"
                rt.Update(sql)
            ElseIf (typ = "JP") Then
                sql = "update pmjobplans set rid = '" & rid & "' where jpid = '" & jpid & "'"
                rt.Update(sql)
            ElseIf (typ = "RBAS") Then
                'sql = "update pmtasks set rid = '" & rid & "' where jpid = '" & jpid & "'"
                'rt.Update(sql)
            End If
            Dim route As String = lblroute.Value
            dglist.EditItemIndex = -1
            GetList(route)
            GetLists(otyp, rteid)
            rt.Dispose()
        End If
        'End If

    End Sub




    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dglist.Columns(0).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "0")
        Catch ex As Exception
        End Try
        Try
            'dglist.Columns(2).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "2")
        Catch ex As Exception
        End Try
        Try
            'dglist.Columns(3).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "3")
        Catch ex As Exception
        End Try
        Try
            'dglist.Columns(4).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "4")
        Catch ex As Exception
        End Try
        Try
            'dglist.Columns(5).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "5")
        Catch ex As Exception
        End Try
        Try
            'dglist.Columns(6).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "6")
        Catch ex As Exception
        End Try
        Try
            'dglist.Columns(7).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "7")
        Catch ex As Exception
        End Try
        Try
            dglist.Columns(8).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "8")
        Catch ex As Exception
        End Try
        Try
            dglist.Columns(11).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "11")
        Catch ex As Exception
        End Try
        Try
            dglist.Columns(12).HeaderText = dlabs.GetDGPage("PMRoutes2.aspx", "dglist", "12")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1196.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1196")
        Catch ex As Exception
        End Try
        Try
            lang1197.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1197")
        Catch ex As Exception
        End Try
        Try
            lang1198.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1198")
        Catch ex As Exception
        End Try
        Try
            lang1199.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1199")
        Catch ex As Exception
        End Try
        Try
            lang1200.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1200")
        Catch ex As Exception
        End Try
        Try
            lang1201.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1201")
        Catch ex As Exception
        End Try
        Try
            lang1202.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1202")
        Catch ex As Exception
        End Try
        Try
            lang1203.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1203")
        Catch ex As Exception
        End Try
        Try
            lang1204.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1204")
        Catch ex As Exception
        End Try
        Try
            lang1205.Text = axlabs.GetASPXPage("PMRoutes2.aspx", "lang1205")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgadd.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMRoutes2.aspx", "imgadd") & "')")
            imgadd.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsav.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMRoutes2.aspx", "imgsav") & "')")
            imgsav.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid172.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PMRoutes2.aspx", "ovid172") & "')")
            ovid172.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

    Protected Sub btnsavetask_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnsavetask.Click
        typ = lbltyp.Value
        rid = lblroute.Value
        rt.Open()
        'save stuff here
        'Dim oldskillid, oldttid, oldptid, oldrdid, oldfreq As String
        'oldskillid = lblskillidb.Value
        'oldttid = lblttidb.Value
        'oldptid = lblptidb.Value
        'oldrdid = lblrdidb.Value
        'oldfreq = lblfreqb.Value

        Dim newskillid, newttid, newptid, newrdid, newfreq As String
        newskillid = ddskill.SelectedValue
        newttid = ddtype.SelectedValue
        newptid = ddpt.SelectedValue
        newrdid = ddeqstat.SelectedValue
        newfreq = txtfreq.Text

        Dim newskill, newtt, newpt, newrd As String
        newskill = ddskill.SelectedItem.Text
        newtt = ddtype.SelectedItem.Text
        newpt = ddpt.SelectedItem.Text
        newrd = ddeqstat.SelectedItem.Text

        'Dim dflag As Integer = 0
        'If oldskillid <> newskillid Then
        'dflag = 1
        'ElseIf oldptid <> newptid Then
        'dflag = 1
        'ElseIf oldrdid <> newrdid Then
        'dflag = 1
        'ElseIf oldfreq <> newfreq Then
        'dflag = 1
        'ElseIf oldttid = "7" And newttid <> "7" Then
        'dflag = 1
        'newptid = "0"
        'newpt = ""
        'ElseIf newttid = "7" And oldttid <> "7" Then
        'dflag = 1

        'End If

        'Dim pmtskid As String = lblpmtskid.Value

        'If typ = "RBAS" Then
        'If (oldttid = "7" And newttid <> "7") Or newttid <> "7" Then
        'sql = "update pmtasks set skillid = '" & newskillid & "', skill = '" & newskill & "', ptid = NULL, pretech = NULL, " _
        '+ "rdid = '" & newrdid & "', freq = '" & newfreq & "', ttid = '" & newttid & "' " _
        '+ "where pmtskid = '" & pmtskid & "'"
        'Else
        'sql = "update pmtasks set skillid = '" & newskillid & "', skill = '" & newskill & "', ptid = '" & newptid & "', " _
        '+ "pretech = '" & newpt & "', rdid = '" & newrdid & "', freq = '" & newfreq & "', ttid = '" & newttid & "' " _
        '+ "where pmtskid = '" & pmtskid & "'"
        'End If
        'ElseIf typ = "RBAST" Then
        'sql = "update pmtaskstpm set " _
        '+ "rdid = '" & newrdid & "', freq = '" & newfreq & "', ttid = '" & newttid & "' " _
        '+ "where pmtskid = '" & pmtskid & "'"
        'End If

        'rt.Update(sql)

        'If dflag = 1 Then
        'Dim rsid, rteid As String
        'rteid = lblroute.Value
        'rsid = lblrsid.Value
        'If typ = "RBAS" Then
        'sql = "usp_rem_rbas_seq '" & rteid & "','" & rsid & "'"
        'ElseIf typ = "RBAST" Then
        'sql = "usp_rem_rbas_seqT '" & rteid & "','" & rsid & "'"
        'End If
        'rt.Update(sql)
        'End If

        'might need a check for the seq field in case the change on top is just task type - PdM (task type) is the question -- save

        If newrdid = 1 Then
            newrd = "Running"
        ElseIf newrdid = 2 Then
            newrd = "Down"
        Else
            newrd = ""
        End If
        If typ = "RBAS" Then
            If newptid = "0" Then
                If newttid <> "0" Then
                    sql = "update pmtasks set skillid = '" & newskillid & "', skill = '" & newskill & "', ptid = NULL, pretech = NULL, " _
                + "rdid = '" & newrdid & "', rd = '" & newrd & "', freq = '" & newfreq & "', ttid = '" & newttid & "' " _
                + "where rteid = '" & rid & "'"
                Else
                    sql = "update pmtasks set skillid = '" & newskillid & "', skill = '" & newskill & "', ptid = NULL, pretech = NULL, " _
                + "rdid = '" & newrdid & "', rd = '" & newrd & "', freq = '" & newfreq & "' " _
                + "where rteid = '" & rid & "'"
                End If
                
            Else
                If newttid <> "0" Then
                    sql = "update pmtasks set skillid = '" & newskillid & "', skill = '" & newskill & "', ptid = '" & newptid & "', " _
                + "pretech = '" & newpt & "', rdid = '" & newrdid & "', rd = '" & newrd & "', freq = '" & newfreq & "', ttid = '" & newttid & "' " _
                + "where rteid = '" & rid & "'"
                Else
                    sql = "update pmtasks set skillid = '" & newskillid & "', skill = '" & newskill & "', ptid = '" & newptid & "', " _
                    + "pretech = '" & newpt & "', rdid = '" & newrdid & "', rd = '" & newrd & "', freq = '" & newfreq & "' " _
                    + "where rteid = '" & rid & "'"
                End If
                
            End If
            rt.Update(sql)

        ElseIf typ = "RBAST" Then
            If newttid <> "0" Then
                sql = "update pmtaskstpm set " _
                            + "rdid = '" & newrdid & "', rd = '" & newrd & "', freq = '" & newfreq & "', ttid = '" & newttid & "' " _
                            + "where rteid = '" & rid & "'"
                rt.Update(sql)
            Else
                sql = "update pmtaskstpm set " _
                            + "rdid = '" & newrdid & "', rd = '" & newrd & "', freq = '" & newfreq & "' " _
                            + "where rteid = '" & rid & "'"
                rt.Update(sql)
            End If
            

        End If

        Dim sqty As String
        sql = "select qty from pmroutes where rid = '" & rid & "'"
        sqty = rt.strScalar(sql)

        Dim pms As String
        If typ = "RBAS" Then
            If newptid = 0 Then
                pms = newskill & "(" & sqty & ")/" & newfreq & "/" & newrd

            Else
                pms = "PDM-" & newpt & "-" & newskill & "(" & sqty & ")/" & newfreq & "/" & newrd

            End If
        Else
            pms = "Operator(" & sqty & ")/" & newfreq & "/" & newrd

        End If
        
        sql = "update pmroutes set skillid = '" & newskillid & "', freq = '" & newfreq & "', rdid = '" & newrdid & "', " _
            + "ptid = '" & newptid & "', pm = '" & pms & "' where rid = '" & rid & "'"

        rt.Update(sql)

        Dim ttid As String
        ttid = ddtype.SelectedValue
        lblnewttid.Value = ttid

        dglist.EditItemIndex = -1
        GetList(rid)
        GetLists(typ, rid)
        rt.Dispose()

    End Sub
End Class
