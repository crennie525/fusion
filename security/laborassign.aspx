<%@ Page Language="vb" AutoEventWireup="false" Codebehind="laborassign.aspx.vb" Inherits="lucy_r12.laborassign" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>laborassign</title>
		
		
		
		
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/laborassignaspx.js"></script>
        <script language="JavaScript" src="../scripts/gridnavopt_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script language="javascript" type="text/javascript">
         function srchloc1() {
             //alert("Temporarily Disabled")
             var sid = document.getElementById("lbldps").value;
             var wo = "";
             var typ = "lu"
             var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lul&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
             if (eReturn) {
                 var ret = eReturn.split("~");
                 document.getElementById("lbllid").value = ret[0];
                 document.getElementById("lblloc").innerHTML = ret[1];
                 document.getElementById("lbllocstr").value = ret[1];
                 //document.getElementById("lblsubmit").value = "srch";
                 //document.getElementById("form1").submit();
                 document.getElementById("lblpchk").value = "loc"
                 document.getElementById("form1").submit();
             }
         }
         function undoloc() {
             document.getElementById("lblret").value = "undoloc"
             document.getElementById("form1").submit();
         }
     </script>
	</head>
	<body bgColor="white" onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table width="422">
				<tr>
					<td class="thdrsing label" colSpan="3"><asp:Label id="lang3460" runat="server">Add/Edit User Equipment Assignments</asp:Label></td>
				</tr>
				<tr>
					<td style="BORDER-BOTTOM: 2px groove" colSpan="3">
						<table>
							<tr height="22">
								<td colspan="2" align="center" class="redlabel" id="tdmode" runat="server"></td>
							</tr>
							<tr height="22">
								<td class="bluelabel" width="100"><asp:Label id="lang3461" runat="server">User Name:</asp:Label></td>
								<td class="label" id="tduser" runat="server" width="200"></td>
							</tr>
							<tr height="22">
								<td class="bluelabel"><asp:Label id="lang3462" runat="server">Skill:</asp:Label></td>
								<td class="label" id="tdskill" runat="server"></td>
							</tr>
							<tr height="22">
								<td class="bluelabel"><asp:Label id="lang3463" runat="server">Supervisor:</asp:Label></td>
								<td class="label" id="tdsuper" runat="server"></td>
							</tr>
							<tr height="22">
								<td class="bluelabel"><asp:Label id="lang3464" runat="server">Shift:</asp:Label></td>
								<td class="label" id="tdshift" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="BORDER-BOTTOM: 2px groove" colSpan="3">
						<table>
                        <tr>
                        <td class="label">Plant Site</td>
                        <td colspan="2"><asp:dropdownlist id="ddps" runat="server" CssClass="plainlabel" 
                                Width="180px" AutoPostBack="True"></asp:dropdownlist></td>
                        </tr>
							<tr id="deptdiv" runat="server">
								<td class="label"><asp:Label id="lang3465" runat="server">Filter By Department</asp:Label></td>
								<td class="label"><asp:dropdownlist id="dddepts" runat="server" Width="170px" CssClass="plainlabel" AutoPostBack="True"></asp:dropdownlist></td>
								<td class="label"><IMG id="imgsw" onmouseover="return overlib('Clear Filter and View All Records', LEFT)" onclick="undoloc();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/switch.gif" runat="server"></td>
							</tr>
							<tr id="celldiv" runat="server">
								<td class="label"><asp:Label id="lang3466" runat="server">Filter By Station/Cell</asp:Label></td>
								<td class="label"><asp:dropdownlist id="ddcells" runat="server" Width="170px" CssClass="plainlabel" AutoPostBack="True"
										Enabled="False"></asp:dropdownlist></td>
								<td class="label"></td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3467" runat="server">Filter By Location</asp:Label></td>
								<td class="label"><asp:label id="lblloc" runat="server" Height="20px" Width="170px" CssClass="plainlabel" BorderStyle="Solid"
										BorderWidth="1px" BorderColor="#8080FF"></asp:label></td>
								<td class="label"><IMG onmouseover="return overlib('Use Locations', LEFT)" onclick="srchloc1();" onmouseout="return nd()"
										src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label" align="center"><asp:Label id="lang3468" runat="server">Available Equipment</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang3469" runat="server">User Equipment</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="200"><asp:listbox id="lbps" runat="server" Height="150px" SelectionMode="Multiple" Width="170px"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22"><br>
						<asp:imagebutton id="btntouser" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><asp:imagebutton id="btnfromuser" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
					<td align="center" width="200"><asp:listbox id="lbuserps" runat="server" Height="150px" SelectionMode="Multiple" Width="170px"></asp:listbox></td>
				</tr>
                <tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img5" onclick="getfirst('i');" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img6" onclick="getprev('i');" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="100"><asp:label id="lblipg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img7" onclick="getnext('i');" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="Img8" onclick="getlast('i');" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><IMG id="usa" onclick="saveapps();" height="19" alt="" src="../images/appbuttons/bgbuttons/save.gif"
							width="69" runat="server"></td>
				</tr>
			</table>
			<input id="lbluid" type="hidden" name="lbluid" runat="server"> <input id="lbltyp" type="hidden" runat="server">
			<input id="lbldept" type="hidden" runat="server"> <input type="hidden" id="lblchk" runat="server">
			<input type="hidden" id="lblclid" runat="server"> <input type="hidden" id="lbllid" runat="server">
			<input type="hidden" id="lblpchk" runat="server"> <input type="hidden" id="lblskill" runat="server">
			<input type="hidden" id="lblskillid" runat="server"> <input type="hidden" id="lblskillindex" runat="server">
			<input type="hidden" id="lbluserid" runat="server"> <input type="hidden" id="lblsuperid" runat="server">
			<input type="hidden" id="lblshift" runat="server"> <input type="hidden" id="lblret" runat="server">
            <input type="hidden" id="lbletyp" runat="server" /><input id="lbllocstr" type="hidden" runat="server">
		<input type="hidden" id="lbldps" runat="server" />
        <input id="txtipg" type="hidden" runat="server"/>
        <input id="txtipgcnt" type="hidden" runat="server"/>
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
