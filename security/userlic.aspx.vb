

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class userlic
    Inherits System.Web.UI.Page
	Protected WithEvents lang3570 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3569 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3568 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3567 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3566 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3565 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim lic As New Utilities
    Dim uid, app, admin As String
    Protected WithEvents iflic As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim dfltps, userid, usrname, islabor, isplanner, issuper, ro, appstr, Logged_In, curlang, ms, psite, sid As String
    Dim ret As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btndont As System.Web.UI.WebControls.Button
    Protected WithEvents btnaccept As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
        GetFSLangs()
        If Not IsPostBack Then
            Try
                sid = Request.QueryString("dfltps").ToString
            Catch ex As Exception
                sid = Request.QueryString("sid").ToString
            End Try
            dfltps = sid
            userid = Request.QueryString("userid").ToString
            usrname = Request.QueryString("usrname").ToString
            islabor = Request.QueryString("islabor").ToString
            isplanner = Request.QueryString("isplanner").ToString
            issuper = Request.QueryString("issuper").ToString
            ro = Request.QueryString("ro").ToString
            appstr = Request.QueryString("appstr").ToString
            Logged_In = Request.QueryString("Logged_In").ToString
            curlang = Request.QueryString("curlang").ToString
            ms = Request.QueryString("ms").ToString
            psite = Request.QueryString("psite").ToString

            ret = "?sid=" + dfltps + "&userid=" + userid + "&usrname=" + usrname + "&islabor=" + islabor + "&isplanner=" + isplanner + "" _
            + "&issuper=" + issuper + "&ro=" + ro + "&appstr=" + appstr + "&curlang=" + curlang + "" _
            + "&Logged_In=" + Logged_In + "&ms=" + ms + "&psite=" + psite
            lblret.Value = ret
        End If

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
    End Sub

    Private Sub btnaccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaccept.Click
        uid = HttpContext.Current.Session("uid").ToString
        admin = HttpContext.Current.Session("pmadmin").ToString()
        lic.Open()
        If admin = "no" Then
            sql = "update pmsysusers set userlicense = '1' where uid = '" & uid & "'"
        Else
            Dim amdb As String = "laiadmin"
            Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
            sql = "update [" & srvr & "].[" & amdb & "].[dbo].[pmsysusers] set userlicense = '1' where uid = '" & uid & "'"
        End If

        lic.Update(sql)
        lic.Dispose()
        Session("ua") = "1"
        ret = lblret.Value
        Response.Redirect("../mainmenu/NewMainMenu2.aspx" + ret)
    End Sub

    Private Sub btndont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndont.Click
        Session("Logged_In") = "no"
        Response.Redirect("../NewLogin.aspx")
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3565.Text = axlabs.GetASPXPage("userlic.aspx", "lang3565")
        Catch ex As Exception
        End Try
        Try
            lang3566.Text = axlabs.GetASPXPage("userlic.aspx", "lang3566")
        Catch ex As Exception
        End Try
        Try
            lang3567.Text = axlabs.GetASPXPage("userlic.aspx", "lang3567")
        Catch ex As Exception
        End Try
        Try
            lang3568.Text = axlabs.GetASPXPage("userlic.aspx", "lang3568")
        Catch ex As Exception
        End Try
        Try
            lang3569.Text = axlabs.GetASPXPage("userlic.aspx", "lang3569")
        Catch ex As Exception
        End Try
        Try
            lang3570.Text = axlabs.GetASPXPage("userlic.aspx", "lang3570")
        Catch ex As Exception
        End Try

    End Sub

End Class
