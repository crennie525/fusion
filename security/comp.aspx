<%@ Page Language="vb" AutoEventWireup="false" Codebehind="comp.aspx.vb" Inherits="lucy_r12.comp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>comp</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/compaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0">
				<tr>
					<td class="label" width="120"><asp:label id="lblname" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Choose Company</asp:label></td>
					<td>
						<asp:DropDownList id="ddcomps" runat="server"></asp:DropDownList></td>
				</tr>
			</table>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
