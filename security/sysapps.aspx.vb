﻿Imports System.Data.SqlClient
Imports System.Text

Public Class sysapps
    Inherits System.Web.UI.Page
    Dim us As New Utilities
    Dim sql As String
    Dim spid As String
    Dim dr As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        spid = Request.QueryString("spid").ToString
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 300px;  HEIGHT: 280px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table>")
        sql = "select s.appname from pmapps s left join sysapps a on a.app = s.app where a.spid = '" & spid & "' order by s.appname"
        us.Open()
        dr = us.GetRdrData(sql)
        While (dr.Read)
            sb.Append("<tr>")
            sb.Append("<td class=""plainlabelblue"">" & dr.Item("appname").ToString & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        us.Dispose()
        sb.Append("</table>")
        sb.Append("</div>")
        tdapps.InnerHtml = sb.ToString

    End Sub

End Class