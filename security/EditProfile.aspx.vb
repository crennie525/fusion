

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class EditProfile
    Inherits System.Web.UI.Page
	Protected WithEvents lang3434 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3433 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3432 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3431 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3430 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3429 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3428 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, uid, usern As String
    Dim sec As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents txtuname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmitprofile As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtp1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtp2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnchangepass As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcurr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtuid As System.Web.UI.WebControls.TextBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            uid = Request.QueryString("uid").ToString
            txtuid.Text = uid
            lblcurr.Value = uid
            usern = Request.QueryString("user").ToString
            txtuname.Text = usern
            sec.Open()
            LoadProfile(uid)
            sec.Dispose()
        End If
        'btnsubmitprofile.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btnsubmitprofile.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
        'btnchangepass.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btnchangepass.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
    End Sub
    Private Sub LoadProfile(ByVal uid As String)
        Dim name, phone, email As String
        sql = "select uid, username, phonenum, email from pmsysusers where uid = '" & uid & "'"
        dr = sec.GetRdrData(sql)
        While dr.Read
            txtuname.Text = dr.Item("username").ToString
            txtphone.Text = dr.Item("phonenum").ToString
            txtemail.Text = dr.Item("email").ToString
            txtuid.Text = dr.Item("uid").ToString
        End While
        dr.Close()
    End Sub
    Private Sub btnsubmitprofile_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsubmitprofile.Click
        Dim name, phone, email, uid As String
        name = txtuname.Text
        Dim ustr As String = Replace(name, "'", Chr(180), , , vbTextCompare)
        phone = txtphone.Text
        email = txtemail.Text
        email = Replace(email, "'", "''")
        uid = txtuid.Text
        Dim ui As String = lblcurr.Value
        sec.Open()
        Dim uicnt As Integer
        sql = "select count(*) from pmsysusers where uid = '" & uid & "'"
        uicnt = sec.Scalar(sql)
        If uicnt = 0 Or uid = ui Then
            sql = "update pmsysusers set username = '" & ustr & "', " _
            + " phonenum = '" & phone & "',  email = '" & email & "', uid ='" & uid & "' where uid = '" & ui & "'"
            sec.Update(sql)
            lblcurr.Value = uid
            LoadProfile(uid)
            Dim strMessage As String =  tmod.getmsg("cdstr1640" , "EditProfile.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1641" , "EditProfile.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        sec.Dispose()

    End Sub

    Private Sub btnchangepass_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnchangepass.Click
        Dim ui, uid, p1, p2 As String
        uid = lblcurr.Value
        p1 = txtp1.Text
        p2 = txtp2.Text
        sec.Open()
        If Len(p1) = 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1642" , "EditProfile.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf Len(p2) = 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1643" , "EditProfile.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf p1 <> p2 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1644" , "EditProfile.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            p2 = sec.Encrypt(p2)
            sql = "update PMSysUsers set passwrd = '" & p2 & "' where uid = '" & uid & "'"
            sec.Update(sql)
            Dim strMessage As String =  tmod.getmsg("cdstr1645" , "EditProfile.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3428.Text = axlabs.GetASPXPage("EditProfile.aspx", "lang3428")
        Catch ex As Exception
        End Try
        Try
            lang3429.Text = axlabs.GetASPXPage("EditProfile.aspx", "lang3429")
        Catch ex As Exception
        End Try
        Try
            lang3430.Text = axlabs.GetASPXPage("EditProfile.aspx", "lang3430")
        Catch ex As Exception
        End Try
        Try
            lang3431.Text = axlabs.GetASPXPage("EditProfile.aspx", "lang3431")
        Catch ex As Exception
        End Try
        Try
            lang3432.Text = axlabs.GetASPXPage("EditProfile.aspx", "lang3432")
        Catch ex As Exception
        End Try
        Try
            lang3433.Text = axlabs.GetASPXPage("EditProfile.aspx", "lang3433")
        Catch ex As Exception
        End Try
        Try
            lang3434.Text = axlabs.GetASPXPage("EditProfile.aspx", "lang3434")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnchangepass.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnchangepass.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnchangepass.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnchangepass.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnchangepass.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
