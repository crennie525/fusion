<%@ Page Language="vb" AutoEventWireup="false" Codebehind="llabor.aspx.vb" Inherits="lucy_r12.llabor" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>llabor</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/llaboraspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 4px; POSITION: absolute; TOP: 4px" cellSpacing="1" cellPadding="1"
				width="460">
				<tr id="tr1" runat="server">
					<td colSpan="4">
						<div id="dvlabor" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; HEIGHT: 200px; WIDTH: 460px"
							runat="server"></div>
					</td>
				</tr>
			</table>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
