﻿Public Class sysprofileseditdialog
    Inherits System.Web.UI.Page
    Dim admin, cadm, spid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            admin = Request.QueryString("admin").ToString
            cadm = Request.QueryString("cadm").ToString
            spid = Request.QueryString("spid").ToString
            ifmeter.Attributes.Add("src", "sysprofilesedit.aspx?admin=" + admin + "&cadm=" + cadm + "&spid=" + spid + "&date=" + Now)
        End If
    End Sub

End Class