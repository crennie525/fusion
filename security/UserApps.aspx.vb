

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class UserApps
    Inherits System.Web.UI.Page
	Protected WithEvents lang3564 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3563 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3562 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3561 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim ua, uid, uname, fld, inprg, ro, cadm, ap, aps As String
    Protected WithEvents lblua As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents usa As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapps As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblexit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblinprog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim us As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbps As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntouser As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromuser As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbuserps As System.Web.UI.WebControls.ListBox
    Protected WithEvents tduser As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            ua = HttpContext.Current.Session("uid").ToString()
            uid = Request.QueryString("uid").ToString
            uname = Request.QueryString("uname").ToString
            fld = Request.QueryString("fld").ToString
            inprg = Request.QueryString("inprg").ToString
            cadm = Request.QueryString("cadm").ToString
            ap = Request.QueryString("ap").ToString
            aps = Request.QueryString("aps").ToString
            tduser.InnerHtml = uname
            lbluid.Value = uid
            lblfld.Value = fld
            lblinprog.Value = inprg
            lblua.Value = ua
            lblcadm.Value = cadm
            lblapps.Value = ap
            lblappstr.Value = aps
            us.Open()

            CheckRO(uid)

            If inprg = "no" AndAlso fld <> "cur" Then
                sql = "usp_checkUIDApps '" & uid & "'"
                us.Update(sql)
            End If
            Dim cnt As Integer
            If inprg = "yes" AndAlso fld = "new" Then
                cnt = CheckUID(uid)
            Else
                cnt = 0
            End If

            If cnt = 0 Then
                PopPSListIntra(ua, uid)
                PopUserList(uid)
            Else
                lblfld.Value = "taken"
            End If

            us.Dispose()
        Else
            If Request.Form("lblsubmit") = "saveapps" Then
                lblsubmit.Value = ""
                UpUser()
            End If
        End If
        'usa.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'usa.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
    End Sub
    Private Sub CheckRO(ByVal uid As String)
        Dim cnt As Integer
        sql = "select count(*) from pmSysUsers where uid = '" & uid & "' and readonly = '1'"
        cnt = us.Scalar(sql)
        lblro.Value = cnt
        If cnt = 1 Then
            sql = "delete from pmUserApps where app = 'ua' or app = 'uap' and uid = '" & uid & "'"
            us.Update(sql)
        End If
       
    End Sub
    Private Function CheckUID(ByVal uid As String) As Integer
        Dim cnt As Integer
        sql = "select count(*) from pmSysUsers where uid = '" & uid & "'"
        cnt = us.Scalar(sql)
        Return cnt
    End Function
    Private Sub PopPSListIntra(ByVal ua As String, ByVal uid As String)
        Dim intra As New intra_utils
        Dim dsCust As New DataSet
        dsCust = intra.CustApps
        Dim scnt As Integer

        Dim dt, val, filt, ap As String
        dt = "pmUserApps"
        val = "app, appname"
        ro = lblro.Value
        cadm = lblcadm.Value
        inprg = lblinprog.Value

        If inprg = "yes" Then
            Try
                ap = lblapps.Value
                ap = ap.Remove(ap.Length - 1, 1)
                ap = ap.Replace("~", "','")
                ap = "'" + ap + "'"
            Catch ex As Exception
                ap = ""
            End Try

        End If
        Dim dsUser As New DataSet
        If ua = "pmadmin" OrElse ua = "pmadmin1" OrElse cadm = "1" Then
            If ro <> "1" Then
                If inprg = "no" Then
                    sql = "select app, appname from pmApps " _
                    + "where app not in (" _
                    + "select app from pmUserApps where uid = '" & uid & "') and app <> 'ua' and app <> 'uap' "
                Else
                    If ap <> "" Then
                        sql = "select app, appname from pmApps " _
                                         + "where app not in (" & ap & ") and app <> 'ua' and app <> 'uap'"
                    Else
                        sql = "select app, appname from pmApps where app <> 'ua'"
                    End If

                End If

            Else
                If inprg = "no" Then
                    sql = "select app, appname from pmApps " _
                    + "where app not in (" _
                    + "select app from pmUserApps where uid = '" & uid & "') and app not in ('ua','uap')"
                Else
                    If ap <> "" Then
                        sql = "select app, appname from pmApps " _
                                           + "where app not in (" & ap & ") and app not in ('ua','uap')"
                    Else
                        sql = "select app, appname from pmApps " _
                   + "where app not in ('ua','uap')"
                    End If

                End If

            End If

        Else
            If ro <> "1" Then
                sql = "select u.app, p.appname from pmUserApps u left join pmApps p on p.app = u.app where u.uid = '" & ua & "' " _
                + "and u.app not in (" _
                + "select app from pmUserApps where uid = '" & uid & "')"
            Else
                sql = "select u.app, p.appname from pmUserApps u left join pmApps p on p.app = u.app where u.uid = '" & ua & "' " _
                           + "and u.app not in (" _
                          + "select app from pmUserApps where uid = '" & uid & "') u.and app not in ('ua','uap')"
            End If

        End If
        'dsUser = us.GetDSData(sql)
        'Dim dtCust As New DataTable
        'dtCust = dsCust.Tables(0)
        'Dim dtUser As New DataTable
        'dtUser = dsUser.Tables(0)
        Dim ds As New DataSet
        'ds = GetAdminSites(dtCust, dtUser)
        'Dim tst As Integer = ds.Tables(0).Rows.Count

        ds = us.GetDSData(sql)
        lbps.DataSource = ds
        lbps.DataTextField = ds.Tables(0).Columns(1).ToString '"appname"
        lbps.DataValueField = ds.Tables(0).Columns(0).ToString '"app"
        lbps.DataBind()


    End Sub
    Private Function GetAdminSites(ByVal CustTable As DataTable, ByVal UserTable As DataTable) As DataSet
        Dim dtDrop As New DataTable("tblapps")
        Dim dtcol As DataColumn
        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "app"
        dtDrop.Columns.Add(dtcol)

        dtcol = New DataColumn
        dtcol.DataType = System.Type.GetType("System.String")
        dtcol.ColumnName = "appname"
        dtDrop.Columns.Add(dtcol)

        Dim dsDrop As New DataSet
        Dim drC As DataRow
        Dim drU As DataRow

        Dim tst As Integer = CustTable.Rows.Count

        Dim UserValueID As Object = DBNull.Value
        Dim UserValueSite As Object = DBNull.Value
        Dim CustValueID As Object = DBNull.Value
        Dim CustValueSite As Object = DBNull.Value
        For Each drC In UserTable.Select()
            UserValueID = drC(0)
            UserValueSite = drC(1)
            Dim frow As DataRow() = CustTable.Select("app = '" & UserValueID & "'")
            If frow.Length <> 0 Then
                Dim drD As DataRow = dtDrop.NewRow
                drD(0) = UserValueID
                drD(1) = UserValueSite
                dtDrop.Rows.Add(drD)
            End If
        Next

        Dim ds As New DataSet
        ds.Tables.Add(dtDrop)
        Return ds

    End Function
    Private Sub PopPSList(ByVal ua As String, ByVal uid As String)
        Dim dt, val, filt As String
        dt = "pmUserApps"
        val = "app, appname"
        ro = lblro.Value
        cadm = lblcadm.Value
        If ua = "pmadmin" OrElse ua = "pmadmin1" Or cadm = "1" Then
            If ro <> "1" Then
                sql = "select app, appname from pmApps " _
                           + "where app not in (" _
                           + "select app from pmUserApps where uid = '" & uid & "')"
            Else
                sql = "select app, appname from pmApps " _
                           + "where app not in (" _
                           + "select app from pmUserApps where uid = '" & uid & "') and app not in ('ua','uap')"
            End If

        Else
            If ro <> "1" Then
                sql = "select u.app, p.appname from pmUserApps u, pmApps p where u.uid = '" & ua & "' " _
                           + "and u.app not in (" _
                           + "select app from pmUserApps where uid = '" & uid & "')"
            Else
                sql = "select u.app, p.appname from pmUserApps u, pmApps p where u.uid = '" & ua & "' " _
                           + "and u.app not in (" _
                          + "select app from pmUserApps where uid = '" & uid & "') and app not in ('ua','uap')"
            End If

        End If
        dr = us.GetRdrData(sql)
        lbps.DataSource = dr
        lbps.DataTextField = "appname"
        lbps.DataValueField = "app"
        lbps.DataBind()
        dr.Close()
    End Sub
    Private Sub PopUserList(ByVal uid As String)
        inprg = lblinprog.Value
        If inprg = "yes" Then
            Dim dtDrop As New DataTable("tblapps")
            Dim dtcol As DataColumn
            dtcol = New DataColumn
            dtcol.DataType = System.Type.GetType("System.String")
            dtcol.ColumnName = "app"
            dtDrop.Columns.Add(dtcol)

            dtcol = New DataColumn
            dtcol.DataType = System.Type.GetType("System.String")
            dtcol.ColumnName = "appname"
            dtDrop.Columns.Add(dtcol)

            Dim drC As DataRow

            Dim app As String = lblapps.Value
            Dim appstr As String = lblappstr.Value
            Try
                app = app.Remove(app.Length - 1, 1)
                appstr = appstr.Remove(appstr.Length - 1, 1)
                app = Replace(app, ",", "~", , , vbTextCompare)
                appstr = Replace(appstr, ",", "~", , , vbTextCompare)
            Catch ex As Exception
                app = ""
                appstr = ""
            End Try
            


            Dim sarr() As String = app.Split("~")
            Dim starr() As String = appstr.Split("~")

            Dim i As Integer
            For i = 0 To sarr.Length - 1
                drC = dtDrop.NewRow
                drC("app") = sarr(i)
                drC("appname") = starr(i)
                dtDrop.Rows.Add(drC)
            Next

            Dim dsDrop As New DataSet
            dsDrop.Tables.Add(dtDrop)
            Try
                lbuserps.DataSource = dsDrop
                lbuserps.DataValueField = "app"
                lbuserps.DataTextField = "appname"
                lbuserps.DataBind()
                lbuserps.SelectedIndex = 0
            Catch ex As Exception

            End Try

        Else
            sql = "select u.app, p.appname from pmUserApps u left join pmApps p on p.app = u.app where u.uid = '" & uid & "'" ' and p.app <> 'uap' or p.app <> 'ua'"
            dr = us.GetRdrData(sql)
            lbuserps.DataSource = dr
            lbuserps.DataValueField = "app"
            lbuserps.DataTextField = "appname"
            lbuserps.DataBind()
            dr.Close()
            lbuserps.SelectedIndex = 0
        End If

        'If lblfld.Value = "new" Then
        'lblapps.Value = ""
        'dr = us.GetRdrData(sql)
        'While dr.Read
        'lblapps.Value += dr.Item("appname").ToString + "~"
        'End While
        'dr.Close()
        'End If
    End Sub
    Private Sub ToUser()
        uid = lbluid.Value
        ua = lblua.Value
        inprg = lblinprog.Value
        Dim Item As ListItem
        Dim s, si, appstr As String
        us.Open()
        For Each Item In lbps.Items
            If Item.Selected Then
                s = Item.ToString
                si = Item.Value.ToString
                If inprg = "yes" Then
                    lblapps.Value += si + "~"
                    lblappstr.Value += s + "~"
                Else
                    GetItems(si, s)
                End If

            End If
        Next
        'UpUser()
        PopPSListIntra(ua, uid)
        PopUserList(uid)
        us.Dispose()
    End Sub
    Private Sub GetItems(ByVal app As String, ByVal appname As String)
        uid = lbluid.Value
        inprg = lblinprog.Value
        Dim stat As String
        Dim scnt As Integer
        sql = "select count(*) from pmUserApps where app = '" & app & "' " _
        + "and uid = '" & uid & "'"
        scnt = us.Scalar(sql)
        If lblfld.Value = "new" Then
            stat = "1"
        Else
            stat = "0"
        End If
        ro = lblro.Value
        If scnt = 0 Then
            If ro = "1" Then
                If app <> "asua" And app <> "asu" Then
                    sql = "insert into pmUserApps (uid, app, stat) values ('" & uid & "', '" & app & "', '" & stat & "')"
                    us.Update(sql)
                End If
            Else
                sql = "insert into pmUserApps (uid, app, stat) values ('" & uid & "', '" & app & "', '" & stat & "')"
                us.Update(sql)
            End If

        End If
    End Sub
    Private Sub RemItems(ByVal app As String, ByVal appname As String)
        uid = lbluid.Value
        inprg = lblinprog.Value
        Dim ap, aps As String
        Dim i As Integer
        If inprg = "no" Then
            Try
                sql = "delete from pmUserApps where app = '" & app & "' and uid = '" & uid & "'"
                us.Update(sql)
            Catch ex As Exception

            End Try
        Else
            ap = lblapps.Value
            aps = lblappstr.Value
            lblapps.Value = ""
            lblappstr.Value = ""
            Try
                ap = ap.Remove(ap.Length - 1, 1)
                aps = aps.Remove(aps.Length - 1, 1)
            Catch ex As Exception
                ap = ""
                aps = ""
            End Try

            Dim aparr() As String = ap.Split("~")
            Dim apsarr() As String = aps.Split("~")
            For i = 0 To aparr.Length - 1
                If aparr(i) <> app Then
                    lblapps.Value += aparr(i) + "~"
                    lblappstr.Value += apsarr(i) + "~"
                End If
            Next
        End If
    End Sub
    Private Sub FromUser()
        uid = lbluid.Value
        ua = lblua.Value
        Dim Item As ListItem
        Dim s, si As String
        us.Open()
        For Each Item In lbuserps.Items
            If Item.Selected Then
                s = Item.ToString
                si = Item.Value.ToString
                RemItems(si, s)
            End If
        Next
        'UpUser()
        PopPSListIntra(ua, uid)
        PopUserList(uid)
        us.Dispose()
    End Sub
    Private Sub UpUser()
        uid = lbluid.Value
        ua = lblua.Value
        Dim Item As ListItem
        Dim s, si, appstr As String
        us.Open()
        For Each Item In lbuserps.Items
            'If Item.Selected Then
            s = Item.ToString
            si = Item.Value.ToString
            If appstr = "" Then
                appstr = si
            Else
                appstr += "," & si
            End If
            'End If
        Next
        sql = "update pmsysusers set appstr = '" & appstr & "' where uid = '" & uid & "'"
        us.Update(sql)
        lblexit.Value = "yes"
        us.Dispose()
    End Sub
    Private Sub btntouser_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntouser.Click
        ToUser()
    End Sub

    Private Sub btnfromuser_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromuser.Click
        FromUser()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3561.Text = axlabs.GetASPXPage("UserApps.aspx", "lang3561")
        Catch ex As Exception
        End Try
        Try
            lang3562.Text = axlabs.GetASPXPage("UserApps.aspx", "lang3562")
        Catch ex As Exception
        End Try
        Try
            lang3563.Text = axlabs.GetASPXPage("UserApps.aspx", "lang3563")
        Catch ex As Exception
        End Try
        Try
            lang3564.Text = axlabs.GetASPXPage("UserApps.aspx", "lang3564")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                usa.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                usa.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                usa.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                usa.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                usa.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
