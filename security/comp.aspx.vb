

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class comp
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lblname As System.Web.UI.WebControls.Label
    Dim comp As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddcomps As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            sql = "select * from Company"
            comp.Open()
            dr = comp.GetRdrData(sql)
            ddcomps.DataSource = dr
            ddcomps.DataTextField = "compname"
            ddcomps.DataValueField = "compid"
            ddcomps.DataBind()
            dr.Close()
            comp.Dispose()
            ddcomps.Items.Insert(0, "Select Company")
        End If
        ddcomps.Attributes.Add("onchange", "getcomp();")
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lblname.Text = axlabs.GetASPXPage("comp.aspx","lblname")
		Catch ex As Exception
		End Try

	End Sub

End Class
