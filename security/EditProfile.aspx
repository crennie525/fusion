<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EditProfile.aspx.vb" Inherits="lucy_r12.EditProfile" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>EditProfile</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/EditProfileaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body bgColor="white"  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="thdrsingrt label" colSpan="2"><asp:Label id="lang3428" runat="server">Add/Edit User Profile</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3429" runat="server">Screen Name</asp:Label></td>
					<td><asp:textbox id="txtuname" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3430" runat="server">Login ID</asp:Label></td>
					<td><asp:textbox id="txtuid" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3431" runat="server">Phone Number</asp:Label></td>
					<td><asp:textbox id="txtphone" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3432" runat="server">Email Address</asp:Label></td>
					<td><asp:textbox id="txtemail" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td align="right" colSpan="2"><asp:imagebutton id="btnsubmitprofile" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3433" runat="server">Password</asp:Label></td>
					<td><asp:textbox id="txtp1" runat="server" TextMode="Password"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3434" runat="server">Verify Password</asp:Label></td>
					<td><asp:textbox id="txtp2" runat="server" TextMode="Password"></asp:textbox></td>
				</tr>
				<TR>
					<td align="right" colSpan="2"><asp:imagebutton id="btnchangepass" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
				</TR>
			</table>
			<input type="hidden" id="lblcurr" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
