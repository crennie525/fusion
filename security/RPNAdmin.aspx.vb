
Public Class RPNAdmin
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hOccuranceRate As System.Web.UI.WebControls.HiddenField
    Protected WithEvents hDetectionProbability As System.Web.UI.WebControls.HiddenField
    Protected WithEvents hQualityImpact As System.Web.UI.WebControls.HiddenField
    Protected WithEvents hDowntimeImpact As System.Web.UI.WebControls.HiddenField
    Protected WithEvents hCostImpact As System.Web.UI.WebControls.HiddenField
    Protected WithEvents hSafetyImpact As System.Web.UI.WebControls.HiddenField
    Protected WithEvents hRPN As System.Web.UI.WebControls.HiddenField
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then

        End If

    End Sub

End Class
