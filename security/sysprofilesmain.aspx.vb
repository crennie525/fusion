﻿Imports System.Data.SqlClient
Imports System.Text

Public Class sysprofilesmain
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim us As New Utilities
    Dim tmod As New transmod
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 10
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim dr As SqlDataReader
    Dim admin, cadm As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            admin = Request.QueryString("admin").ToString
            cadm = Request.QueryString("cadm").ToString
            lbladmin.Value = admin
            lblcadm.Value = cadm

            us.Open()
            getprofs(1)
            us.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                us.Open()
                GetNext()
                us.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                us.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                getprofs(PageNumber)
                us.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                us.Open()
                GetPrev()
                us.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                us.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                getprofs(PageNumber)
                us.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "remsp" Then
                us.Open()
                Dim spid As String = lblspid.Value
                remsp(spid)
                PageNumber = txtpg.Value
                getprofs(PageNumber)
                us.Dispose()
                lblret.Value = ""
                lblspid.Value = ""
            ElseIf Request.Form("lblret") = "retsp" Then
                us.Open()
                PageNumber = txtpg.Value
                getprofs(PageNumber)
                us.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub remsp(ByVal spid As String)
        sql = "delete from sysprofiles where spid = '" & spid & "'; delete from sysapps where spid = '" & spid & "'"
        us.Update(sql)
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getprofs(PageNumber)
        Catch ex As Exception
            us.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getprofs(PageNumber)
        Catch ex As Exception
            us.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getprofs(ByVal PageNumber As Integer)
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("<table>")
        sb.Append("<tr>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""40"">Edit</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""150"">Profile Name</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""50"">Shift</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""150"">Skill</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">Administrator</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""50"">Labor</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">Supervisor</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">Planner</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""80"">Applications</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""60"">Remove</td>")
        sb.Append("</tr>")
        Dim intPgNav, intPgCnt As Integer
        sql = "select count(*) from sysprofiles"
        intPgCnt = us.Scalar(sql)
        Tables = "sysprofiles"
        PK = "spid"
        PageSize = "15"
        intPgNav = us.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        Sort = "spname"

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
            sb.Append("<tr><td class=""plainlabelred"" colspan=""10"" align=""center"" height=""22"">No Records Found - Plase Add a New Profile</td></tr>")
            sb.Append("</table>")
            tdprof.InnerHtml = sb.ToString
            Exit Sub
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
            dr = us.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        End If
        Dim rbgint As Integer = 0
        Dim rbg As String = "plainlabel transrowblue"
        Dim adm, lab, sup, pla, shi, ski, spid, spname, appstr As String
        While dr.Read
            If rbgint = 0 Then
                rbg = "plainlabel"
                rbgint = 1
            Else
                rbg = "plainlabel transrowblue"
                rbgint = 0
            End If
            spname = dr.Item("spname").ToString
            adm = dr.Item("admin").ToString
            lab = dr.Item("islabor").ToString
            sup = dr.Item("issuper").ToString
            pla = dr.Item("isplanner").ToString
            shi = dr.Item("shift").ToString
            ski = dr.Item("skill").ToString
            spid = dr.Item("spid").ToString
            appstr = dr.Item("appstr").ToString

            sb.Append("<tr>")
            sb.Append("<td class=""" & rbg & """><img src='../images/appbuttons/minibuttons/pencilnobg.gif' onclick=""editprof('" & spid & "');""></td>")
            If appstr <> "" Then
                sb.Append("<td class=""" & rbg & """><a href=""#"" onclick=""getprof('" & spid & "','" & spname & "');"">" & dr.Item("spname").ToString & "</a></td>")
            Else
                sb.Append("<td class=""" & rbg & """>" & dr.Item("spname").ToString & "</td>")
            End If

            If adm = "0" Then
                adm = "no"
            Else
                adm = "yes"
            End If
            If lab = "0" Then
                lab = "no"
            Else
                lab = "yes"
            End If
            If sup = "0" Then
                sup = "no"
            Else
                sup = "yes"
            End If
            If pla = "0" Then
                pla = "no"
            Else
                pla = "yes"
            End If
            If shi = "0" Then
                shi = "Any"
            ElseIf shi = "1" Then
                shi = "First"
            ElseIf shi = "2" Then
                shi = "Second"
            Else
                shi = "Third"
            End If
            If ski = "" Then
                ski = "NA"
            End If

            sb.Append("<td class=""" & rbg & """>" & shi & "</td>")
            sb.Append("<td class=""" & rbg & """>" & ski & "</td>")
            sb.Append("<td class=""" & rbg & """>" & adm & "</td>")
            sb.Append("<td class=""" & rbg & """>" & lab & "</td>")
            sb.Append("<td class=""" & rbg & """>" & sup & "</td>")
            sb.Append("<td class=""" & rbg & """>" & pla & "</td>")
            sb.Append("<td class=""" & rbg & """><img src='../images/appbuttons/minibuttons/magnifier.gif' onclick=""getapps('" & spid & "');""></td>")
            sb.Append("<td class=""" & rbg & """><img src='../images/appbuttons/minibuttons/del.gif' onclick=""remsp('" & spid & "');""></td>")
            sb.Append("</tr>")
        End While
        dr.Close()




        sb.Append("</table>")
        tdprof.InnerHtml = sb.ToString
    End Sub
End Class