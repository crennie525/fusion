<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UserSites.aspx.vb" Inherits="lucy_r12.UserSites" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UserSites</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/UserSitesaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body bgColor="white"  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table width="682">
				<tr>
					<td class="thdrsingrt label" colSpan="3"><asp:Label id="lang3571" runat="server">Add/Edit User Sites Mini Dialog</asp:Label></td>
				</tr>
				<tr>
					<td style="BORDER-BOTTOM: 2px groove" colSpan="3">
						<table>
							<tr>
								<td class="bluelabel"><asp:Label id="lang3572" runat="server">User Name:</asp:Label></td>
								<td class="label" id="tduser" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label" align="center"><asp:Label id="lang3573" runat="server">Available Plant Sites</asp:Label></td>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang3574" runat="server">User Plant Sites</asp:Label></td>
				</tr>
				<tr>
					<td align="center" width="330"><asp:listbox id="lbps" runat="server" Height="150px" SelectionMode="Multiple" Width="330px"></asp:listbox></td>
					<td vAlign="middle" align="center" width="22"><br>
						<asp:imagebutton id="btntouser" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><asp:imagebutton id="btnfromuser" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
					<td align="center" width="330"><asp:listbox id="lbuserps" runat="server" Height="150px" SelectionMode="Multiple" Width="330px"></asp:listbox></td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><IMG id="usr" runat="server" onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/save.gif"
							width="69" height="19"></td>
				</tr>
			</table>
			<input id="lbluid" type="hidden" runat="server"><input type="hidden" id="lblfld" runat="server">
			<input type="hidden" id="lblsid" runat="server"><input type="hidden" id="lbladmin" runat="server">
			<input type="hidden" id="lblcadm" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
