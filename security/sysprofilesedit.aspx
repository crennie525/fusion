﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="sysprofilesedit.aspx.vb" Inherits="lucy_r12.sysprofilesedit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function savedets() {
            var chk = document.getElementById("txtprof").value;
            if (chk != "") {
                document.getElementById("lblsubmit").value = "savedets";
                document.getElementById("form1").submit();
            }
            else {
                alert("Profile Name Required")
            }
        }
        function checkret() {
            var chk = document.getElementById("lblsubmit").value;
            if (chk == "goback") {
                window.parent.handlereturn();
            }
        }
        function getskill(typ) {

            var eReturn = window.showModalDialog("lskilldialog.aspx", "", "dialogHeight:260px; dialogWidth:260px; resizable=yes");
            if (eReturn) {
                var retarr = eReturn.split(",")

                //document.getElementById("lblskillindexr").value = retarr[2];
                document.getElementById("lblskillid").value = retarr[1];
                document.getElementById("txtskillo").value = retarr[0];
                document.getElementById("lblskill").value = retarr[0];

            }

        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td class="label">
                    Profile Name
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtprof" runat="server" CssClass="plainlabel" MaxLength="200" Width="160px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang3546" runat="server">User Shift</asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddshifto" runat="server" CssClass="plainlabel">
                        <asp:ListItem Value="0" Selected="True">Any</asp:ListItem>
                        <asp:ListItem Value="1">First</asp:ListItem>
                        <asp:ListItem Value="2">Second</asp:ListItem>
                        <asp:ListItem Value="3">Third</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang3547" runat="server">User Skill</asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtskillo" runat="server" CssClass="plainlabel" MaxLength="200"
                        Width="160px"></asp:TextBox><img onclick="getskill('old');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                            border="0">
                </td>
            </tr>
            <tr>
                <td class="bluelabel" align="center" colspan="3">
                    <asp:CheckBox ID="cbadmin" runat="server" Text="User Is An Administrator"></asp:CheckBox>
                    &nbsp;<asp:CheckBox ID="cbplanner" runat="server" Text="User Is A Planner"></asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td class="bluelabel" align="center" colspan="3">
                    <asp:CheckBox ID="cbsuper" runat="server" Text="User Is A Supervisor"></asp:CheckBox>&nbsp;
                    <asp:CheckBox ID="cblabor" runat="server" Text="User Is Labor"></asp:CheckBox>
                </td>
            </tr>
            
            <tr>
                <td colspan="3" id="tdapps" runat="server">
                    <table>
                        <tr>
                            <td class="label" align="center">
                                <asp:Label ID="lang3563" runat="server">Available Applications</asp:Label>
                            </td>
                            <td>
                            </td>
                            <td class="label" align="center">
                                <asp:Label ID="lang3564" runat="server">Profile Applications</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" width="200">
                                <asp:ListBox ID="lbps" runat="server" Width="170px" SelectionMode="Multiple" Height="150px">
                                </asp:ListBox>
                            </td>
                            <td valign="middle" align="center" width="22">
                                <br>
                                <asp:ImageButton ID="btntouser" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                                </asp:ImageButton><asp:ImageButton ID="btnfromuser" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                                </asp:ImageButton>
                            </td>
                            <td align="center" width="200">
                                <asp:ListBox ID="lbuserps" runat="server" Width="170px" SelectionMode="Multiple"
                                    Height="150px"></asp:ListBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trsave" runat="server">
            <td colspan="3" align="right">
            <table>
            <tr>
            <td class="plainlabelblue">Save Changes and Return</td>
            <td><img alt="" src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="savedets();" /></td>
            </tr>
            </table>
            </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lbladmin" runat="server" />
    <input type="hidden" id="lblspid" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblapps" runat="server" />
    <input type="hidden" id="lblappstr" runat="server" />
    <input type="hidden" id="lbloldprof" runat="server" />
    <input type="hidden" id="lbloldskillid" runat="server" />

    </form>
</body>
</html>
