﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="sysbulk.aspx.vb" Inherits="lucy_r12.sysbulk" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function getbulk() {
            document.getElementById("lblsubmit").value = "go"
            document.getElementById("form1").submit();
        }
        function getprof() {
            var admin = document.getElementById("lbladmin").value;
            var cadm = document.getElementById("lblcadm").value;
            var eReturn = window.showModalDialog("sysprofilesmaindialog.aspx?admin=" + admin + "&cadm=" + cadm + "&date=" + Date(), "", "dialogHeight:510px; dialogWidth:1020px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                var ret = eReturn.split(",")
                document.getElementById("lblspid").value = ret[0];
                document.getElementById("lblspname").value = ret[1];
                //document.getElementById("lblprofe").innerHTML = ret[1];
                document.getElementById("tr11").className = "details";
                //document.getElementById("treman2").className = "details";
                //document.getElementById("treman3").className = "details";
                //document.getElementById("treman4").className = "details";
                document.getElementById("tdprof").innerHTML = "You Selected Profile " + ret[1] + "<br />Click the Submit Button to Add your user list to this Profile"
                document.getElementById("tr12").className = "view";
            }
        }
        function checkps() {
            var chk = document.getElementById("ddps").value;
            if (chk != "Select Plant Site") {
                document.getElementById("dfltsid").value = chk;
                document.getElementById("tr9").className = "details";
                document.getElementById("tr10").className = "details";
                document.getElementById("tr11").className = "view";
            }
        }
        function getsuper(typ) {
            var uid = "";
            var skill = "";
                var ro = "0"; //document.getElementById("lblro").value;
                var eReturn = window.showModalDialog("lsuperdialog.aspx?typ=sup&skill=" + skill + "&ro=" + ro, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
                if (eReturn) {
                    if (eReturn != "log") {
                        if (eReturn != "") {
                            var retarr = eReturn.split(",")
                            document.getElementById("lblsupid").value = retarr[1];
                            document.getElementById("txtsupero").value = retarr[0];
                            document.getElementById("lblsup").value = retarr[0];
                            document.getElementById("tr8").className = "details";
                            document.getElementById("tr9").className = "view";
                            document.getElementById("tr10").className = "view";
                        }  
                    }  
                }
        }
        function getcal(fld) {
            
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                document.getElementById(fld).value = eReturn;
                document.getElementById("lblexpire").value = eReturn;
                document.getElementById("tr4").className = "details"
                document.getElementById("tr5").className = "details"
                document.getElementById("tr6").className = "details"
                document.getElementById("tr7").className = "view"
                document.getElementById("tr7a").className = "view"
            }
        }
        function checksup(who) {
            if (who == "y") {
                document.getElementById("tr7").className = "view"
                document.getElementById("tr7a").className = "details"
                document.getElementById("tr8").className = "view"
                document.getElementById("lblneedsup").value = "yes";
            }
            else {
                document.getElementById("tr7").className = "details"
                document.getElementById("tr7a").className = "details"
                document.getElementById("tr8").className = "details"
                document.getElementById("tr9").className = "view"
                document.getElementById("tr10").className = "view"
                document.getElementById("lblneedsup").value = "no";
            }
            
        }
        function checkDate(who) {
            if (who == "y") {
                document.getElementById("tr4").className = "details"
                document.getElementById("tr5").className = "details"
                document.getElementById("tr6").className = "view"
                document.getElementById("lbldoesexpire").value = "yes";
            }
            else {
                document.getElementById("tr4").className = "details"
                document.getElementById("tr5").className = "details"
                document.getElementById("tr6").className = "details"
                document.getElementById("tr7").className = "view"
                document.getElementById("tr7a").className = "view"
                document.getElementById("lbldoesexpire").value = "no";
            }
        }



    </script>
    <style type="text/css">
        .inslabel
        {
            font-family: arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
        }
        .inslabelb
        {
            font-family: arial, MS Sans Serif, sans-serif, Verdana;
            font-size: 12px;
            text-decoration: none;
            color: black;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr id="tr1" runat="server">
                <td class="inslabel" colspan="2">
                    To Bulk Add Users you will need an Excel File Spreadsheet.<br />
                    The first row of this Spreadsheet should have 5 columns labelled as follows;<br />
                    ID, PASS, NAME, PHONE, EMAIL<br />
                    The ID column should contain the Login ID for each user and is required (50 Characters Max)<br />
                    The PASS column should contain the initial Password each user will use and is required<br />
                    The NAME column should contain the screen name for each user and is required (50 Characters Max)<br />
                    The PHONE column should contain the Phone Number for each user and is not required (50 Characters Max)<br />
                    The EMAIL column should contain the Email Address for each user and is not required (200 Characters Max)<br />
                    <br />
                    Notes:<br />
                    <br />
                    Your Spreadsheet should be located on the 1st tab and should not include any notes.<br />
                    <br />
                    No column values should contain commas.
                    <br />
                    If you would like to include a supervisor you will need a separate Spreadsheet for
                    each supervisor.<br />
                    <br />
                    When your Spreadsheet is ready you will need to save it as a CSV (Comma Delimited) (*.csv).<br />
                    <br />
                </td>
            </tr>
            <tr id="tr2" runat="server">
                <td colspan="2" class="inslabelb">
                    To Click the Browse Button to locate your Comma Delimited File and then Click the Upload Button<br />
                    If you are prompted to Save Changes to Your File Click the Save Button to Continue<br />
                    <br />
                </td>
            </tr>
            <tr id="tr3" runat="server">
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td class="label">
                                <input class="3DButton" id="File1" type="file" size="32" name="File1" runat="server" />
                            </td>
                            <td>
                                <input class="3DButton" id="btnupload" type="button" value="Upload" name="btnupload"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr4" runat="server">
                <td colspan="2" class="inslabelb">
                    Will the Login Credentials for this list of Users Expire?<br />
                    <br />
                </td>
            </tr>
            <tr id="tr5" runat="server">
                <td colspan="2" class="inslabelb" align="center">
                    <input id="rbxy" type="radio" name="rbx" runat="server" onclick="checkDate('y');"><asp:Label ID="lang3553"
                        runat="server">Yes</asp:Label><input id="rbxn" onclick="checkDate('n');" type="radio"
                            name="rbx" runat="server"><asp:Label ID="lang3554" runat="server">No</asp:Label>
                </td>
            </tr>
            <tr id="tr6" runat="server">
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td class="inslabelb">
                                <asp:Label ID="lang3555" runat="server">Expires On</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtexpd" runat="server" CssClass="plainlabel" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                <img onclick="getcal('txtexpd');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                                    width="19">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr7" runat="server">
                <td colspan="2" class="inslabelb">
                    Does this list of Users require a Supervisor?<br />
                    <br />
                </td>
            </tr>
            <tr id="tr7a" runat="server">
                <td colspan="2" class="inslabelb" align="center">
                    <input id="rbysup" type="radio" name="rbs" runat="server" onclick="checksup('y');" />Yes
                    <input id="rbnsup" onclick="checksup('n');" type="radio" name="rbs" runat="server" />No
                </td>
            </tr>
            <tr id="tr8" runat="server">
                <td align="center" colspan="2">
                    <table>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang3548" runat="server">Supervisor</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtsupero" runat="server" CssClass="plainlabel" MaxLength="200"
                                    Width="160px"></asp:TextBox><img onclick="getsuper('old');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                                        border="0">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr9" runat="server">
                <td colspan="2" class="inslabelb">
                    Please Select a Default Plant Site<br />
                    <br />
                </td>
            </tr>
            <tr id="tr10" runat="server">
                <td align="center" colspan="2">
                    <table>
                        <tr>
                            <td class="label">
                                <asp:Label ID="lang3549" runat="server">Default Plant Site</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddps" runat="server" CssClass="plainlabel" Width="180px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr11" runat="server">
                <td class="inslabelb">
                    Please Select or Create a Profile&nbsp;&nbsp;<img src="../images/appbuttons/minibuttons/addmod.gif" alt="" onclick="getprof();" />
                </td>
               
            </tr>
            <tr id="tr12" runat="server">
            <td colspan="2" align="center">
            <table>
            <tr>
            <td class="inslabelb" id="tdprof" runat="server" align="center" height="22">
                     
                </td>
            </tr>
            <tr>
            <td align="center"><input type="button" id="btnaddbulk" value="Submit" onclick="getbulk();" /></td>
            </tr>
            </table>
            </td>
                
               
            </tr>
        </table>
    </div>
    <input type="hidden" id="lbldoctype" runat="server" />
    <input type="hidden" id="lblfpath" runat="server" />
    <input type="hidden" id="lbldoesexpire" runat="server" />
    <input type="hidden" id="lblexpire" runat="server" />
    <input type="hidden" id="lblneedsup" runat="server" />
    <input type="hidden" id="lblsupid" runat="server" />
    <input type="hidden" id="lblsup" runat="server" />
    <input type="hidden" id="dfltsid" runat="server" />
    <input type="hidden" id="lblspid" runat="server" />
    <input type="hidden" id="lbladmin" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lblspname" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblappstr" runat="server" />
    <input type="hidden" id="lblerr" runat="server" />
    </form>
</body>
</html>
