

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class UserSites
    Inherits System.Web.UI.Page
	Protected WithEvents lang3574 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3573 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3572 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3571 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim uid, uname, fld, sid, admin, cadm As String
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tduser As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents usr As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladmin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim us As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbps As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbuserps As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntouser As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromuser As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            uid = Request.QueryString("uid").ToString
            uname = Request.QueryString("uname").ToString
            fld = Request.QueryString("fld").ToString
            sid = Request.QueryString("sid").ToString
            admin = Request.QueryString("admin").ToString
            cadm = HttpContext.Current.Session("cadm").ToString()
            lblcadm.Value = cadm
            lbluid.Value = uid
            lblfld.Value = fld
            lblsid.Value = sid
            lbladmin.Value = admin
            us.Open()
            CheckDPS(uid, sid)
            PopPSList(uid)
            PopUserList(uid)
            us.Dispose()
        End If
        'usr.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'usr.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
    End Sub
    Private Sub CheckDPS(ByVal uid As String, ByVal sid As String)
        Dim stat As String
        If lblfld.Value = "new" Then
            stat = "1"
        Else
            stat = "0"
        End If
        sql = "usp_checkDPS '" & uid & "', '" & sid & "', '" & stat & "'"
        us.Update(sql)
    End Sub
    Private Sub PopPSList(ByVal uid As String)
        Dim dt, val, filt As String
        admin = lbladmin.Value
        dt = "Sites"
        val = "siteid, sitename"
        cadm = lblcadm.Value
        If admin = "pmadmin" Or admin = "pmadmin1" Or cadm = "1" Then
            filt = " where siteid not in (" _
            + "select siteid from pmUserSites where uid = '" & uid & "')"
        Else
            filt = " where siteid not in (" _
            + "select siteid from pmUserSites where uid = '" & uid & "') and " _
            + "siteid in (select siteid from pmUserSites where uid = '" & admin & "')"
        End If

        dr = us.GetList(dt, val, filt)
        lbps.DataSource = dr
        lbps.DataTextField = "sitename"
        lbps.DataValueField = "siteid"
        lbps.DataBind()
        dr.Close()
    End Sub
    Private Sub PopUserList(ByVal uid As String)
        sql = "select u.siteid, s.sitename from pmUserSites u join Sites s on s.siteid = u.siteid " _
        + "and u.uid = '" & uid & "'"
        dr = us.GetRdrData(sql)
        lbuserps.DataSource = dr
        lbuserps.DataValueField = "siteid"
        lbuserps.DataTextField = "sitename"
        lbuserps.DataBind()
        dr.Close()
        lbuserps.SelectedIndex = 0
    End Sub

    Private Sub btntouser_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntouser.Click
        ToUser()

    End Sub
    Private Sub ToUser()
        uid = lbluid.Value
        Dim Item As ListItem
        Dim s, si As String
        us.Open()
        For Each Item In lbps.Items
            If Item.Selected Then
                s = Item.ToString
                si = Item.Value.ToString
                GetItems(si, s)
            End If
        Next
        PopPSList(uid)
        PopUserList(uid)
        us.Dispose()
    End Sub
    Private Sub GetItems(ByVal siteid As String, ByVal sitename As String)
        uid = lbluid.Value
        Dim stat As String
        Dim scnt As Integer
        sql = "select count(*) from pmUserSites where siteid = '" & siteid & "' " _
        + "and uid = '" & uid & "'"
        scnt = us.Scalar(sql)
        If lblfld.Value = "new" Then
            stat = "1"
        Else
            stat = "0"
        End If
        If scnt = 0 Then
            sql = "insert into pmUserSites (uid, siteid, stat) values ('" & uid & "', '" & siteid & "', '" & stat & "')"
            us.Update(sql)
        End If
    End Sub

    Private Sub btnfromuser_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromuser.Click
        FromUser()
    End Sub
    Private Sub RemItems(ByVal siteid As String, ByVal sitename As String)
        uid = lbluid.Value
        Try
            sql = "delete from pmUserSites where siteid = '" & siteid & "' and uid = '" & uid & "'"
            us.Update(sql)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub FromUser()
        uid = lbluid.Value
        Dim Item As ListItem
        Dim s, si As String
        us.Open()
        For Each Item In lbuserps.Items
            If Item.Selected Then
                s = Item.ToString
                si = Item.Value.ToString
                RemItems(si, s)
            End If
        Next
        PopPSList(uid)
        PopUserList(uid)
        us.Dispose()
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3571.Text = axlabs.GetASPXPage("UserSites.aspx", "lang3571")
        Catch ex As Exception
        End Try
        Try
            lang3572.Text = axlabs.GetASPXPage("UserSites.aspx", "lang3572")
        Catch ex As Exception
        End Try
        Try
            lang3573.Text = axlabs.GetASPXPage("UserSites.aspx", "lang3573")
        Catch ex As Exception
        End Try
        Try
            lang3574.Text = axlabs.GetASPXPage("UserSites.aspx", "lang3574")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                usr.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                usr.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                usr.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                usr.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                usr.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
