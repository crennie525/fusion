<%@ Page Language="vb" AutoEventWireup="false" Codebehind="superassign.aspx.vb" Inherits="lucy_r12.superassign" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>superassign</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/superassignaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checksel();" >
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; LEFT: 2px; POSITION: absolute; TOP: 2px" cellSpacing="0" cellPadding="2"
				width="960">
				<tr>
					<td class="thdrsinglft" align="left" width="26"><IMG src="../images/appbuttons/minibuttons/woreq.gif" border="0"></td>
					<td class="thdrsingrt label" width="934"><asp:Label id="lang3474" runat="server">Supervisor Assignment Administration</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td class="label" width="80"><asp:Label id="lang3475" runat="server">Supervisor</asp:Label></td>
								<td width="180" id="tdsuper" runat="server" class="plainlabelblue"></td>
								<td class="label" width="80"><asp:Label id="lang3476" runat="server">Plant Site</asp:Label></td>
								<td width="180"><asp:dropdownlist id="ddsite" runat="server"></asp:dropdownlist></td>
								<td class="label" width="60"><asp:Label id="lang3477" runat="server">Shift</asp:Label></td>
								<td class="plainlabel" id="tdshift" width="60" runat="server"></td>
								<td class="label" width="60"><asp:Label id="lang3478" runat="server">Phone</asp:Label></td>
								<td class="plainlabel" id="tdphone" width="80" runat="server"></td>
								<td class="label" width="60"><asp:Label id="lang3479" runat="server">Email</asp:Label></td>
								<td class="plainlabel" id="tdemail" width="200" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table width="960">
							<tr>
								<td class="thdrsingg label" width="300"><asp:Label id="lang3480" runat="server">Labor Assignments (Reference)</asp:Label></td>
								<td class="thdrsingg label" width="660"><asp:Label id="lang3481" runat="server">Skill Assignments</asp:Label></td>
							</tr>
							<tr>
								<td rowSpan="7">
									<table cellSpacing="0" cellPadding="0">
										<TBODY>
											<tr>
												<td><iframe id="getlab" style="BACKGROUND-COLOR: transparent" src="../labor/PMSuperSelect.aspx?typ=wait&amp;skill=&amp;wr=&amp;ro="
														frameBorder="no" width="300" scrolling="no" height="125" allowTransparency runat="server"></iframe>
												</td>
								</td>
							</tr>
							<tr height="20">
								<td class="thdrsingg label" width="300"><asp:Label id="lang3482" runat="server">Assignment Conflicts</asp:Label></td>
							</tr>
							<tr>
								<td id="tdconflicts" vAlign="top" rowSpan="6" runat="server"><iframe id="ifcon" style="BACKGROUND-COLOR: transparent" src="../labor/conflicts.aspx" frameBorder="no"
										width="300" scrolling="auto" height="280" allowTransparency runat="server"></iframe>
								</td>
							</tr>
						</table>
					<td><iframe id="getskill" style="BACKGROUND-COLOR: transparent" src="../labor/transassign.aspx?typ=wait&amp;supid=&amp;wr=&amp;ro="
							frameBorder="no" width="660" scrolling="no" height="85" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="thdrsingg label" width="660"><asp:Label id="lang3483" runat="server">Department Assignments</asp:Label></td>
				</tr>
				<tr>
					<td><iframe id="getdept" style="BACKGROUND-COLOR: transparent" src="../labor/transassign.aspx?typ=wait&amp;supid=&amp;wr=&amp;ro="
							frameBorder="no" width="660" scrolling="no" height="85" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="thdrsingg label" width="660"><asp:Label id="lang3484" runat="server">Location Assignments</asp:Label></td>
				</tr>
				<tr>
					<td><iframe id="getloc" style="BACKGROUND-COLOR: transparent" src="../labor/transassign.aspx?typ=wait&amp;supid=&amp;wr=&amp;ro="
							frameBorder="no" width="660" scrolling="no" height="85" allowTransparency runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="thdrsingg label" width="660"><asp:Label id="lang3485" runat="server">Equipment Assignments</asp:Label></td>
				</tr>
				<tr>
					<td><iframe id="geteq" style="BACKGROUND-COLOR: transparent" src="../labor/transassign.aspx?typ=wait&amp;supid=&amp;wr=&amp;ro="
							frameBorder="no" width="660" scrolling="no" height="85" allowTransparency runat="server"></iframe>
					</td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE></TABLE></TD></TR></TABLE><input id="lblsid" type="hidden" runat="server" NAME="lblsid">
			<input id="lblsupid" type="hidden" runat="server" NAME="lblsupid"><input id="lblsel" type="hidden" runat="server" NAME="lblsel">
			<input id="lblsup" type="hidden" runat="server" NAME="lblsup"><input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="lblro" runat="server" NAME="lblro"><input type="hidden" id="lblsupret" runat="server" NAME="lblsupret">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
