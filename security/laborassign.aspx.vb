

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class laborassign
    Inherits System.Web.UI.Page
    Protected WithEvents ovid289 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang3469 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3468 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3467 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3466 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3465 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3464 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3463 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3462 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3461 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3460 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim la As New Utilities
    Dim uid, lname, si, su, ski, sk As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim intPgNav As Integer
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.WebControls.Label
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskill As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillindex As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuperid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdmode As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblshift As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdskill As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsuper As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdshift As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbletyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllocstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldps As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddps As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtipg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblipg As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbps As System.Web.UI.WebControls.ListBox
    Protected WithEvents btntouser As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromuser As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbuserps As System.Web.UI.WebControls.ListBox
    Protected WithEvents tduser As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents usa As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblua As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapps As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblexit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents deptdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents celldiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgsw As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Dim typ, id, nme, sk, ski, su, sui, sht, dps As String
            typ = Request.QueryString("typ").ToString
            id = Request.QueryString("id").ToString
            nme = Request.QueryString("nme").ToString
            sk = Request.QueryString("sk").ToString
            ski = Request.QueryString("ski").ToString
            su = Request.QueryString("su").ToString
            sui = Request.QueryString("sui").ToString
            sht = Request.QueryString("sht").ToString
            dps = Request.QueryString("dps").ToString
            tduser.InnerHtml = nme
            'txtskillo.Text = sk
            'txtsupero.Text = su
            tdskill.InnerHtml = sk
            tdsuper.InnerHtml = su
            lbletyp.Value = typ
            lbluserid.Value = id
            lblskillid.Value = ski
            lblsuperid.Value = sui
            lblshift.Value = sht
            lbldps.Value = dps
            Dim shtstr As String
            If sht = "0" Then
                shtstr = "Any"
            ElseIf sht = "1" Then
                shtstr = "First"
            ElseIf sht = "2" Then
                shtstr = "Second"
            ElseIf sht = "3" Then
                shtstr = "Third"
            End If
            tdshift.InnerHtml = shtstr
            Try
                'ddshift.SelectedValue = sht
            Catch ex As Exception

            End Try

            If typ = "new" Then
                tdmode.InnerHtml = "New User Mode"
                'txtskillo.Enabled = False
                'txtsupero.Enabled = False
                'ddshift.Enabled = False
                'imgsk.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
                'imgsu.Attributes.Add("src", "../images/appbuttons/minibuttons/magnifierdis.gif")
            Else
                tdmode.InnerHtml = "Update User Mode"
            End If
            la.Open()
            PopSites(dps, id)
            PopDepts(dps)
            PopEquipment(PageNumber)
            PopLabEquipment()
            la.Dispose()
        Else
            If Request.Form("lblpchk") = "loc" Then
                lblpchk.Value = ""
                Dim dept, cell, lid As String
                dept = lbldept.Value
                cell = lblclid.Value
                If dept = "" Then
                    dept = "0"
                End If
                If cell = "" Then
                    cell = "0"
                End If
                la.Open()
                lid = lbllid.Value
                PageNumber = 1
                txtipg.Value = PageNumber
                PopEquipment(PageNumber, dept, cell, lid)
                PopLabEquipment(dept, cell, lid)
                la.Dispose()
            ElseIf Request.Form("lblpchk") = "upuser" Then
                lblpchk.Value = ""
                la.Open()
                UpUser()
                la.Dispose()

            End If

            If Request.Form("lblret") = "inext" Then
                la.Open()
                GetiNext()
                la.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ilast" Then
                la.Open()
                PageNumber = txtipgcnt.Value
                txtipg.Value = PageNumber
                Dim dept, cell, lid As String
                dept = lbldept.Value
                cell = lblclid.Value
                If dept = "" Then
                    dept = "0"
                End If
                If cell = "" Then
                    cell = "0"
                End If
                lid = lbllid.Value
                PopEquipment(PageNumber, dept, cell, lid)
                la.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "iprev" Then
                la.Open()
                GetiPrev()
                la.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ifirst" Then
                la.Open()
                PageNumber = 1
                txtipg.Value = PageNumber
                Dim dept, cell, lid As String
                dept = lbldept.Value
                cell = lblclid.Value
                If dept = "" Then
                    dept = "0"
                End If
                If cell = "" Then
                    cell = "0"
                End If
                lid = lbllid.Value
                PopEquipment(PageNumber, dept, cell, lid)
                la.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "undoloc" Then
                la.Open()
                PageNumber = 1
                txtipg.Value = PageNumber
                lbldept.Value = ""
                lblclid.Value = ""
                dddepts.SelectedIndex = 0
                ddcells.SelectedIndex = 0
                lblloc.Text = ""
                lbllid.Value = ""
                PopEquipment(PageNumber)
                PopLabEquipment()
                la.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetiNext()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg + 1
            txtipg.Value = PageNumber
            Dim dept, cell, lid As String
            dept = lbldept.Value
            cell = lblclid.Value
            If dept = "" Then
                dept = "0"
            End If
            If cell = "" Then
                cell = "0"
            End If
            lid = lbllid.Value
            PopEquipment(PageNumber, dept, cell, lid)
        Catch ex As Exception
            la.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1695", "useradmin2.aspx.vb")

            la.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetiPrev()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg - 1
            txtipg.Value = PageNumber
            Dim dept, cell, lid As String
            dept = lbldept.Value
            cell = lblclid.Value
            If dept = "" Then
                dept = "0"
            End If
            If cell = "" Then
                cell = "0"
            End If
            lid = lbllid.Value
            PopEquipment(PageNumber, dept, cell, lid)
        Catch ex As Exception
            la.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1696", "useradmin2.aspx.vb")

            la.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopSites(ByVal dps As String, ByVal uid As String)
        sql = "select u.siteid, s.sitename from pmUserSites u " _
         + "left join pmsysusers p on p.uid = u.uid " _
          + "left join sites s on s.siteid = u.siteid where p.userid = '" & uid & "'"
        dr = la.GetRdrData(sql)
        ddps.DataSource = dr
        ddps.DataValueField = "siteid"
        ddps.DataTextField = "sitename"
        'ddps.DataSource = dr
        ddps.DataBind()
        dr.Close()
        ddps.Items.Insert(0, "Select Plant Site")
        ddps.Enabled = True
        Try
            ddps.SelectedValue = dps

        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpUser()
        'document.getElementById("lblskillindex").value = retarr[2];
        'document.getElementById("lblskillid").value = retarr[1];
        'document.getElementById("txtskillo").value = retarr[0];
        'document.getElementById("lblsuperid").value = retarr[1];
        'document.getElementById("txtsupero").value = retarr[0];
    End Sub
    Private Sub PopDepts(ByVal dps As String)

        sql = "select count(*) from Dept where siteid = '" & dps & "'"
        Dim dt, val, filt As String
        Dim dcnt As Integer = la.Scalar(sql)
        If dcnt > 0 Then
            'dt = "Dept"
            'val = "dept_id , dept_line "
            'filt = ""
            'dr = la.GetList(dt, val, filt)
            sql = "select dept_id , dept_line from dept where siteid = '" & dps & "'"
            dr = la.GetRdrData(sql)
            dddepts.DataSource = dr
            dddepts.DataTextField = "dept_line"
            dddepts.DataValueField = "dept_id"
            dddepts.DataBind()
            dr.Close()
            dddepts.Enabled = True
        Else
            'Dim strMessage As String = tmod.getmsg("cdstr1664", "laborassign.aspx.vb")
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            dddepts.Enabled = False
        End If
        dddepts.Items.Insert(0, "Select Department")
    End Sub

    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        Dim Filter As String
        PageNumber = 1
        If dddepts.SelectedIndex <> 0 And dddepts.SelectedIndex <> -1 Then
            Dim dept As String = dddepts.SelectedValue.ToString
            la.Open()
            If dddepts.SelectedIndex <> 0 And dddepts.SelectedIndex <> -1 Then
                'celldiv.Style.Add("display", "none")
                'celldiv.Style.Add("visibility", "hidden")
                CheckTyp(dept)
                Dim deptname As String = dddepts.SelectedItem.ToString
                lbldept.Value = dept
                Filter = dept
                Dim chk As String = CellCheck(Filter)
                If chk = "no" Then
                    lblclid.Value = "0"
                    lblchk.Value = "no"
                    ddcells.Enabled = False
                    PopEquipment(PageNumber, dept)
                    PopLabEquipment(dept)
                Else
                    lblchk.Value = "yes"
                    ddcells.Enabled = True
                    PopEquipment(PageNumber, dept)
                    PopLabEquipment(dept)
                    PopCells(dept)
                End If
            Else
                ddcells.Enabled = False
            End If
            la.Dispose()
        End If
    End Sub
    Private Sub CheckTyp(ByVal dept As String, Optional ByVal loc As String = "N")
        Dim typ As String
        Dim cnt As Integer
        If loc = "N" Then
            sql = "select count(locid) from equipment where dept_id = '" & dept & "' and locid <> 0 and locid is not null"
            cnt = la.Scalar(sql)
            If cnt = 0 Then
                lbltyp.Value = "reg"
            Else
                lbltyp.Value = "dloc"
            End If
        Else
            If dept = "" Then
                lbltyp.Value = "loc"
            Else
                sql = "select count(dept_id) from equipment where locid = '" & loc & "' and dept_id <> 0 and dept_id is not null"
                cnt = la.Scalar(sql)
                If cnt = 0 Then
                    lbltyp.Value = "loc"
                Else
                    lbltyp.Value = "dloc"
                End If
            End If
        End If

    End Sub
    Public Function CellCheck(ByVal filt As String) As String
        Dim cells As String
        Dim cellcnt As Integer
        sql = "select count(*) from Cells where Dept_ID = '" & filt & "'"
        cellcnt = la.Scalar(sql)
        Dim nocellcnt As Integer
        If cellcnt = 0 Then
            cells = "no"
        ElseIf cellcnt = 1 Then
            sql = "select count(*) from Cells where Dept_ID = '" & filt & "' and cell_name = 'No Cells'"
            nocellcnt = la.Scalar(sql)
            If nocellcnt = 1 Then
                cells = "no"
            Else
                cells = "yes"
            End If
        Else
            cells = "yes"
        End If
        Return cells
    End Function
    Private Sub PopCells(ByVal dept As String)
        Dim filt, dt, val As String
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = la.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
        celldiv.Style.Add("display", "block")
        celldiv.Style.Add("visibility", "visible")
    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        Dim dept As String
        dept = lbldept.Value
        PageNumber = 1
        Dim cell As String = ddcells.SelectedValue.ToString
        If ddcells.SelectedIndex <> 0 Then
            Try
                la.Open()
                lblclid.Value = cell
                PopEquipment(PageNumber, dept, cell)
                PopLabEquipment(dept, cell)
                la.Dispose()
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr1665", "laborassign.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        End If
    End Sub
    Private Sub PopEquipment(ByVal PageNumber As String, Optional ByVal dept As String = "0", Optional ByVal cell As String = "0", Optional ByVal lid As String = "0")
        Dim typ As String = lbletyp.Value
        Dim id As String = lbluserid.Value
        Dim dps As String = lbldps.Value
        Dim filt, filtsp As String
        If typ = "old" Then
            filt = "(select eqid from pmlaborlocs where laborid = '" & id & "') and siteid = '" & dps & "'"
            filtsp = "(select eqid from pmlaborlocs where laborid = ''" & id & "'') and siteid = ''" & dps & "''"
        Else
            filt = "(select eqid from pmlaborlocs where tmpid = '" & id & "') and siteid = '" & dps & "'"
            filtsp = "(select eqid from pmlaborlocs where tmpid = ''" & id & "'') and siteid = ''" & dps & "''"
        End If

        If dept = "0" And cell = "0" And lid = "0" Then
            sql = "select count(*) from equipment where eqid not in " & filt
            Filter = "eqid not in " & filt
        ElseIf dept <> "0" And cell = "0" And lid = "0" Then
            sql = "select count(*) from equipment where dept_id = '" & dept & "' and eqid not in " & filt
            Filter = "dept_id = '" & dept & "' and eqid not in " & filt
        ElseIf dept <> "0" And cell <> "0" And lid = "0" Then
            sql = "select count(*) from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' and eqid not in " & filt
            Filter = "dept_id = '" & dept & "' and cellid = '" & cell & "' and eqid not in " & filt
        ElseIf dept <> "0" And cell <> "0" And lid <> "0" Then
            sql = "select count(*) from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & lid & "' and eqid not in " & filt
            Filter = "dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & lid & "' and eqid not in " & filt
        ElseIf dept <> "0" And cell = "0" And lid <> "0" Then
            sql = "select count(*) from equipment where dept_id = '" & dept & "' and locid = '" & lid & "' and eqid not in " & filt
            Filter = "dept_id = '" & dept & "' and locid = '" & lid & "' and eqid not in " & filt
        ElseIf dept = "0" And cell = "0" And lid <> "0" Then
            Dim par As String = lbllocstr.Value
            lblloc.Text = par
            sql = "select count(*) from equipment where eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "') and eqid not in " & filt
            Filter = "eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "') and eqid not in " & filt

        Else
            Dim par As String = lbllocstr.Value
            lblloc.Text = par
            sql = "select count(*) from equipment where eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "') and eqid not in " & filt
            Filter = "eqnum in (select h.location from pmlocheir h where h.parent = '" & par & "') and eqid not in " & filt
        End If

        txtipg.Value = PageNumber
        'intPgNav = adm.PageCount(intPgCnt, PageSize)

        intPgNav = la.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblipg.Text = "Page 0 of 0"
        Else
            lblipg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtipgcnt.Value = intPgNav

        Tables = "equipment"
        PK = "eqid"
        Sort = "eqnum desc"
        dr = la.GetPageHack(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        'dr = la.GetRdrData(sql)
        lbps.DataSource = dr
        lbps.DataTextField = "eqnum"
        lbps.DataValueField = "eqid"
        lbps.DataBind()
        dr.Close()
    End Sub
    Private Sub PopLabEquipment(Optional ByVal dept As String = "0", Optional ByVal cell As String = "0", Optional ByVal lid As String = "0")
        Dim userid As String = lbluserid.Value
        Dim typ As String = lbletyp.Value
        Dim filt As String
        If typ = "old" Then
            filt = "l.laborid = '" & userid & "'"
        Else
            filt = "l.tmpid = '" & userid & "'"
        End If
        If dept = "0" And cell = "0" And lid = "0" Then
            sql = "select l.eqid, e.eqnum from pmlaborlocs l left join equipment e on e.eqid = l.eqid where " & filt
        ElseIf dept <> "0" And cell = "0" And lid = "0" Then
            sql = "select l.eqid, e.eqnum from pmlaborlocs l left join equipment e on e.eqid = l.eqid where l.dept_id = '" & dept & "' and  " & filt
        ElseIf dept <> "0" And cell <> "0" And lid = "0" Then
            sql = "select l.eqid, e.eqnum from pmlaborlocs l left join equipment e on e.eqid = l.eqid where l.dept_id = '" & dept & "' and l.cellid = '" & cell & "' and  " & filt
        ElseIf dept <> "0" And cell <> "0" And lid <> "0" Then
            sql = "select l.eqid, e.eqnum from pmlaborlocs l left join equipment e on e.eqid = l.eqid where l.dept_id = '" & dept & "' and l.cellid = '" & cell & "' and l.locid = '" & lid & "' and  " & filt
        ElseIf dept <> "0" And cell = "0" And lid <> "0" Then
            sql = "select l.eqid, e.eqnum from pmlaborlocs l left join equipment e on e.eqid = l.eqid where l.dept_id = '" & dept & "' and l.locid = '" & lid & "' and  " & filt
        ElseIf dept = "0" And cell = "0" And lid <> "0" Then
            sql = "select l.eqid, e.eqnum from pmlaborlocs l left join equipment e on e.eqid = l.eqid where l.locid = '" & lid & "' and  " & filt
        Else
            sql = "select l.eqid, e.eqnum from pmlaborlocs l left join equipment e on e.eqid = l.eqid where l.dept_id = '" & dept & "' and l.cellid = '" & cell & "' and l.locid = '" & lid & "' and  " & filt
        End If
        dr = la.GetRdrData(sql)
        lbuserps.DataSource = dr
        lbuserps.DataTextField = "eqnum"
        lbuserps.DataValueField = "eqid"
        lbuserps.DataBind()
        dr.Close()
    End Sub

    Private Sub btntouser_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntouser.Click
        ToUser()
    End Sub
    Private Sub ToUser()
        Dim Item As ListItem
        Dim ei As String
        la.Open()
        For Each Item In lbps.Items
            If Item.Selected Then
                ei = Item.Value.ToString
                GetItems(ei)
            End If
        Next
        Dim dept, cell, lid As String
        dept = lbldept.Value
        cell = lblclid.Value
        If dept = "" Then
            dept = "0"
        End If
        If cell = "" Then
            cell = "0"
        End If
        lid = lbllid.Value
        If lid = "" Then
            lid = "0"
        End If
        PageNumber = txtipgcnt.Value
        PopEquipment(PageNumber, dept, cell, lid)
        PopLabEquipment(dept, cell, lid)
        la.Dispose()
    End Sub
    Private Sub GetItems(ByVal ei As String)
        Dim id As String = lbluserid.Value
        Dim typ As String = lbletyp.Value
        sql = "usp_labtouser '" & id & "', '" & ei & "', '" & typ & "'"
        la.Update(sql)
    End Sub

    Private Sub btnfromuser_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromuser.Click
        FromUser()
    End Sub
    Private Sub FromUser()
        Dim Item As ListItem
        Dim ei As String
        la.Open()
        For Each Item In lbuserps.Items
            If Item.Selected Then
                ei = Item.Value.ToString
                RemItems(ei)
            End If
        Next
        Dim dept, cell, lid As String
        dept = lbldept.Value
        cell = lblclid.Value
        If dept = "" Then
            dept = "0"
        End If
        If cell = "" Then
            cell = "0"
        End If
        lid = lbllid.Value
        If lid = "" Then
            lid = "0"
        End If
        PageNumber = txtipgcnt.Value
        PopEquipment(PageNumber, dept, cell, lid)
        PopLabEquipment(dept, cell, lid)
        la.Dispose()
    End Sub
    Private Sub RemItems(ByVal ei As String)
        Dim id As String = lbluserid.Value
        Dim typ As String = lbletyp.Value
        If typ = "old" Then
            sql = "delete from pmlaborlocs where eqid = '" & ei & "' and laborid = '" & id & "'"
        Else
            sql = "delete from pmlaborlocs where eqid = '" & ei & "' and tmpid = '" & id & "'"
        End If
        la.Update(sql)
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3460.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3460")
        Catch ex As Exception
        End Try
        Try
            lang3461.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3461")
        Catch ex As Exception
        End Try
        Try
            lang3462.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3462")
        Catch ex As Exception
        End Try
        Try
            lang3463.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3463")
        Catch ex As Exception
        End Try
        Try
            lang3464.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3464")
        Catch ex As Exception
        End Try
        Try
            lang3465.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3465")
        Catch ex As Exception
        End Try
        Try
            lang3466.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3466")
        Catch ex As Exception
        End Try
        Try
            lang3467.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3467")
        Catch ex As Exception
        End Try
        Try
            lang3468.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3468")
        Catch ex As Exception
        End Try
        Try
            lang3469.Text = axlabs.GetASPXPage("laborassign.aspx", "lang3469")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                usa.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                usa.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                usa.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                usa.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                usa.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgsw.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("laborassign.aspx", "imgsw") & "')")
            imgsw.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid289.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("laborassign.aspx", "ovid289") & "')")
            ovid289.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub ddps_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddps.SelectedIndexChanged
        Dim ps As String = lbldps.Value
        If ddps.SelectedIndex <> 0 And ddps.SelectedIndex <> -1 Then
            ps = ddps.SelectedValue
            lbldps.Value = ps

        End If
        PageNumber = 1
        txtipg.Value = PageNumber
        la.Open()
        PopDepts(ps)
        PopCells("0")
        PopEquipment(PageNumber)
        PopLabEquipment()
        la.Dispose()

    End Sub
End Class
