

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class EditProfile_v2
    Inherits System.Web.UI.Page
	Protected WithEvents lang3459 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3458 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3457 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3456 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3455 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3454 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3453 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3452 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3451 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3450 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3449 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3448 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3447 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3446 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3445 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3444 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3443 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3442 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3441 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3440 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3439 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3438 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3437 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3436 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3435 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, uid, usern, cid As String
    Dim sec As New Utilities
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents usitesdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents uappsdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcurrpic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents pgtitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtuname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtuid As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmitprofile As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtp1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtp2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnchangepass As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcurr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtaltphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaltemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtjt As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpo As System.Web.UI.WebControls.TextBox
    Protected WithEvents btneditforumprofile As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents MyFile As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents btnupload As System.Web.UI.HtmlControls.HtmlInputButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            lbldb.Value = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            uid = HttpContext.Current.Session("uid").ToString()
            txtuid.Text = uid
            lblcurr.Value = uid
            usern = HttpContext.Current.Session("username").ToString()
            txtuname.Text = usern
            lblcurrn.Value = usern
            sec.Open()
            LoadProfile(uid)
            GetSites(uid)
            GetApps(uid)
            sec.Dispose()
        End If
        'btnsubmitprofile.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btnsubmitprofile.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
        'btnchangepass.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btnchangepass.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
        'btneditforumprofile.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btneditforumprofile.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
    End Sub
    Private Sub LoadProfile(ByVal uid As String)
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim name, phone, email, ro As String
        sql = "select readonly from pmuserapps where uid = '" & uid & "' and app = 'ep'"

        sql = "select u.uid, u.username, u.phonenum, u.altphonenum, u.email, u.altemail, u.readonly from pmsysusers u " _
        + "left join pmuserapps a on a.uid = u.uid and a.app = 'ep' where u.uid = '" & uid & "'"
        dr = sec.GetRdrData(sql)
        While dr.Read
            txtuname.Text = dr.Item("username").ToString
            txtphone.Text = dr.Item("phonenum").ToString
            txtaltphone.Text = dr.Item("altphonenum").ToString
            txtemail.Text = dr.Item("email").ToString
            txtaltemail.Text = dr.Item("altemail").ToString
            txtuid.Text = dr.Item("uid").ToString
            ro = dr.Item("readonly").ToString
        End While
        dr.Close()
        Dim pic As String
        sql = "select title, intro, pic from [web_profiles2] where uid = '" & uid & "'"
        dr = sec.GetRdrData(sql)
        If dr.HasRows Then
            While dr.Read
                txtjt.Text = dr.Item("title").ToString
                txtpo.Text = dr.Item("intro").ToString
                pic = dr.Item("pic").ToString
            End While
        End If
        dr.Close()
        lblcurrpic.Value = pic
        Dim src As String = "../userimages/"
        If Len(pic) <> 0 Then

            imgeq.Src = src & pic
        End If
        If ro = "1" Then
            btnsubmitprofile.ImageUrl = "../images/appbuttons/bgbuttons/submitdis.gif"
            btnsubmitprofile.Enabled = False

            btneditforumprofile.ImageUrl = "../images/appbuttons/bgbuttons/submitdis.gif"
            btneditforumprofile.Enabled = False

            btnchangepass.ImageUrl = "../images/appbuttons/bgbuttons/submitdis.gif"
            btnchangepass.Enabled = False

            btnupload.Disabled = True

            lblro.Value = ro
            'pgtitle.InnerHtml = "Edit Profile (Read Only)"
            End If
    End Sub

    Private Sub btnsubmitprofile_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsubmitprofile.Click
        Dim name, phone, altphone, email, altemail, uid, ro As String
        ro = lblro.Value
        If ro <> "1" Then
            name = txtuname.Text
            Dim ustr As String = Replace(name, "'", Chr(180), , , vbTextCompare)
            If Len(ustr) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1646" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            phone = txtphone.Text
            phone = Replace(phone, "'", Chr(180), , , vbTextCompare)
            phone = Replace(phone, "--", "-", , , vbTextCompare)
            phone = Replace(phone, ";", ":", , , vbTextCompare)
            If Len(phone) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1647" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            altphone = txtaltphone.Text
            altphone = Replace(altphone, "'", Chr(180), , , vbTextCompare)
            altphone = Replace(altphone, "--", "-", , , vbTextCompare)
            altphone = Replace(altphone, ";", ":", , , vbTextCompare)
            If Len(altphone) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1648" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            email = txtemail.Text
            email = Replace(email, "'", "''")
            altemail = txtaltemail.Text
            altemail = Replace(altemail, "'", "''")
            uid = txtuid.Text
            uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
            uid = Replace(uid, "--", "-", , , vbTextCompare)
            uid = Replace(uid, ";", ":", , , vbTextCompare)
            Dim ui As String = lblcurr.Value
            ui = Replace(ui, "'", Chr(180), , , vbTextCompare)
            Dim uin As String = lblcurrn.Value
            uin = Replace(uin, "'", Chr(180), , , vbTextCompare)
            sec.Open()
            If Len(name) = 0 Or name = "" Then
                Dim strMessage As String =  tmod.getmsg("cdstr1649" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ElseIf Len(email) = 0 Or email = "" Then
                Dim strMessage As String =  tmod.getmsg("cdstr1650" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ElseIf Len(uid) = 0 Or uid = "" Then
                Dim strMessage As String =  tmod.getmsg("cdstr1651" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                Dim uicnt As Integer
                sql = "select count(*) from pmsysusers where uid = '" & uid & "'"
                uicnt = sec.Scalar(sql)
                Dim uncnt As Integer
                sql = "select count(*) from pmsysusers where username = '" & ustr & "'"
                uncnt = sec.Scalar(sql)
                If uncnt = 0 Or ustr = uin Then
                    If (uicnt = 0 Or uid = ui) Then
                        'needs to update web profile 2 too
                        Dim cmd As New SqlCommand("exec usp_upprofile @ustr, @phone, @altphone, @email, @altemail, @uid, @ui")
                        Dim param0 = New SqlParameter("@ustr", SqlDbType.VarChar)
                        param0.Value = ustr
                        cmd.Parameters.Add(param0)
                        Dim param01 = New SqlParameter("@phone", SqlDbType.VarChar)
                        If phone = "" Then
                            param01.Value = System.DBNull.Value
                        Else
                            param01.Value = phone
                        End If
                        cmd.Parameters.Add(param01)
                        Dim param02 = New SqlParameter("@altphone", SqlDbType.VarChar)
                        If altphone = "" Then
                            param02.Value = System.DBNull.Value
                        Else
                            param02.Value = altphone
                        End If
                        cmd.Parameters.Add(param02)
                        Dim param03 = New SqlParameter("@email", SqlDbType.VarChar)
                        param03.Value = email
                        cmd.Parameters.Add(param03)
                        Dim param04 = New SqlParameter("@altemail", SqlDbType.VarChar)
                        If altemail = "" Then
                            param04.Value = System.DBNull.Value
                        Else
                            param04.Value = altemail
                        End If
                        cmd.Parameters.Add(param04)
                        Dim param05 = New SqlParameter("@uid", SqlDbType.VarChar)
                        param05.Value = uid
                        cmd.Parameters.Add(param05)
                        Dim param06 = New SqlParameter("@ui", SqlDbType.VarChar)
                        param06.Value = ui
                        cmd.Parameters.Add(param06)

                        sec.UpdateHack(cmd)

                        'sql = "update pmsysusers set username = '" & ustr & "', " _
                        '+ " phonenum = '" & phone & "', altphonenum = '" & altphone & "', " _
                        '+ " email = '" & email & "', altemail = '" & altemail & "', uid ='" & uid & "' where uid = '" & ui & "'"
                        'sec.Update(sql)
                        'sql = "update pmuserapps set uid = '" & uid & "' where uid = '" & ui & "';update pmusersites set uid = '" & uid & "' where uid = '" & ui & "'"
                        'sec.Update(sql)
                        lblcurr.Value = uid
                        LoadProfile(uid)
                        Dim strMessage As String =  tmod.getmsg("cdstr1652" , "EditProfile_v2.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

                    Else
                        Dim strMessage As String =  tmod.getmsg("cdstr1653" , "EditProfile_v2.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1654" , "EditProfile_v2.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If

            End If

            sec.Dispose()
        End If
        

    End Sub

    Private Sub btnchangepass_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnchangepass.Click
        Dim ui, uid, p1, p2, ro As String
        ro = lblro.Value
        If ro <> "1" Then
            uid = lblcurr.Value
            uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
            p1 = txtp1.Text
            p2 = txtp2.Text
            sec.Open()
            If Len(p1) = 0 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1655" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ElseIf Len(p1) > 8 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1656" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ElseIf Len(p2) = 0 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1657" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ElseIf p1 <> p2 Then
                Dim strMessage As String =  tmod.getmsg("cdstr1658" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                p2 = sec.Encrypt(p2)
                sql = "update PMSysUsers set passwrd = '" & p2 & "' where uid = '" & uid & "'"
                sec.Update(sql)
                Dim strMessage As String =  tmod.getmsg("cdstr1659" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
        

    End Sub

    Private Sub btneditforumprofile_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btneditforumprofile.Click
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim uid, jt, po, ro As String
        ro = lblro.Value
        If ro <> "1" Then
            uid = lblcurr.Value 'txtuid.Text
            uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
            jt = txtjt.Text
            jt = Replace(jt, "'", Chr(180), , , vbTextCompare)
            jt = Replace(jt, "--", "-", , , vbTextCompare)
            jt = Replace(jt, "--", "-", , , vbTextCompare)
            po = txtpo.Text
            po = Replace(po, "'", Chr(180), , , vbTextCompare)
            po = Replace(po, "--", "-", , , vbTextCompare)
            po = Replace(po, "--", "-", , , vbTextCompare)
            Dim ucnt As Integer
            sec.Open()
            If Len(uid) <> 0 Or uid <> "" Then
                sql = "select count(*) from [web_profiles2] where uid = '" & uid & "'"
                ucnt = sec.Scalar(sql)
                Dim cmd As New SqlCommand("exec usp_upfprofile @uid, @jt, @po, @ucnt")
                Dim param0 = New SqlParameter("@uid", SqlDbType.VarChar)
                param0.Value = uid
                cmd.Parameters.Add(param0)

                Dim param01 = New SqlParameter("@jt", SqlDbType.VarChar)
                If jt = "" Then
                    param01.Value = System.DBNull.Value
                Else
                    param01.Value = jt
                End If
                cmd.Parameters.Add(param01)

                Dim param02 = New SqlParameter("@po", SqlDbType.Text)
                If po = "" Then
                    param02.Value = System.DBNull.Value
                Else
                    param02.Value = po
                End If
                cmd.Parameters.Add(param02)

                Dim param03 = New SqlParameter("@ucnt", SqlDbType.Int)
                param03.Value = ucnt
                cmd.Parameters.Add(param03)

                sec.UpdateHack(cmd)

                'If ucnt = 0 Then
                'sql = "insert into [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] (uid, title, intro) values " _
                '+ "('" & uid & "', '" & jt & "', '" & po & "')"
                'Else
                'sql = "update [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] set title = '" & jt & "', " _
                '+ "intro = '" & po & "' " _
                '+ "where uid = '" & uid & "'"
                'End If
                'sec.Update(sql)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1660" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            sec.Dispose()
        End If
        
    End Sub

    Private Sub btnupload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.ServerClick
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim ro As String
        ro = lblro.Value
        If ro <> "1" Then
            If Not (MyFile.PostedFile Is Nothing) Then
                'Check to make sure we actually have a file to upload
                Dim strLongFilePath As String = MyFile.PostedFile.FileName
                Dim intFileNameLength As Integer = InStr(1, StrReverse(strLongFilePath), "\")
                Dim strFileName As String = Mid(strLongFilePath, (Len(strLongFilePath) - intFileNameLength) + 2)
                Dim uid As String = txtuid.Text
                Dim currfilem As String = lblcurrpic.Value
                Dim pid As String
                If Len(currfilem) <> 0 OrElse currfilem <> "" Then
                    pid = Right(currfilem, 5)
                    pid = Left(pid, 1)
                    If pid = "a" Then
                        pid = "b"
                    Else
                        pid = "a"
                    End If
                Else
                    pid = "a"
                End If

                Dim newstr As String = "a-userImg" & uid & "-" & pid & ".jpg"
                Dim thumbstr As String = "atn-userImg" & uid & "-" & pid & ".jpg"
                Dim medstr As String = "atm-userImg" & uid & "-" & pid & ".jpg"
                lblcurrpic.Value = medstr
                Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                Dim strto As String = appstr + "/userimages/"
                Dim strto1 As String = appstr + "\userimages\"

                If Len(uid) <> 0 Or uid <> "" Then
                    Select Case MyFile.PostedFile.ContentType
                        Case "image/pjpeg", "image/jpeg"  'Make sure we are getting a valid JPG image

                            If Len(currfilem) <> 0 OrElse currfilem <> "" Then
                                Dim currfile As String = Replace(currfilem, "atm-userImg", "a-userImg")
                                Dim currfilen As String = Replace(currfilem, "atm-userImg", "atn-userImg")
                                If File.Exists(Server.MapPath("\") & strto & currfile) Then
                                    File.Delete(Server.MapPath("\") & strto & currfile)
                                End If
                                If File.Exists(Server.MapPath("\") & strto & currfilem) Then
                                    File.Delete(Server.MapPath("\") & strto & currfilem)
                                End If
                                If File.Exists(Server.MapPath("\") & strto & currfilen) Then
                                    File.Delete(Server.MapPath("\") & strto & currfilen)
                                End If
                            End If

                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & newstr)

                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & thumbstr)

                            MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & medstr)

                            Dim fsimg As System.Drawing.Image
                            'Response.ContentType = "image/jpeg"
                            fsimg = System.Drawing.Image.FromFile(Server.MapPath("\") & strto & thumbstr)

                            fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                            fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                            Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                            dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                            Dim tnImg As System.Drawing.Image
                            Dim iw, ih As Integer
                            iw = 60
                            ih = 60
                            tnImg = fsimg.GetThumbnailImage(iw, ih, dummyCallBack, IntPtr.Zero)
                            tnImg.Save(Server.MapPath("\") & strto & thumbstr)
                            Dim tmImg As System.Drawing.Image
                            Dim iwm, ihm As Integer
                            iwm = 216
                            ihm = 216
                            tmImg = fsimg.GetThumbnailImage(iwm, ihm, dummyCallBack, IntPtr.Zero)
                            tmImg.Save(Server.MapPath("\") & strto & medstr)
                            tnImg.Dispose()
                            tmImg.Dispose()
                            fsimg.Dispose()
                            Dim savstr As String = "../userimages/" & newstr
                            Dim savtnstr As String = "../userimages/" & thumbstr
                            Dim savtmstr As String = "../userimages/" & medstr
                            imgeq.Src = savtmstr
                            Dim ucnt As Integer
                            sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] where uid = '" & uid & "'"
                            sec.Open()
                            ucnt = sec.Scalar(sql)
                            If ucnt = 0 Then
                                sql = "insert into [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] (uid, pic) values " _
                                + "('" & uid & "', '" & medstr & "')"
                            Else
                                sql = "update [" & srvr & "].[" & mdb & "].[dbo].[web_profiles2] set pic = '" & medstr & "' " _
                                + "where uid = '" & uid & "'"
                            End If
                            sec.Update(sql)
                            sec.Dispose()
                            'Response.Redirect("EditProfile_v2.aspx?date=" + Now)
                        Case Else
                            Dim strMessage As String =  tmod.getmsg("cdstr1661" , "EditProfile_v2.aspx.vb")
 
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End Select
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1662" , "EditProfile_v2.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1663" , "EditProfile_v2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
        
    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Private Sub GetSites(ByVal uid As String)
        Dim sb As New System.Text.StringBuilder
        Dim scnt As Integer
        If uid = "pmadmin" Or uid = "pmadmin1" Then
            sql = "select count(*) from sites where compid = '" & cid & "'"
        Else
            sql = "select count(*) from pmUserSites where uid = '" & uid & "'"
        End If

        scnt = sec.Scalar(sql)
        If scnt > 0 Then
            If uid = "pmadmin" Or uid = "pmadmin1" Then
                sql = "select siteid, sitename from sites where compid = '" & cid & "'"
            Else
                sql = "select u.siteid, s.sitename from pmUserSites u " _
                + "left join sites s on s.siteid = u.siteid where u.uid = '" & uid & "'"
            End If
            dr = sec.GetRdrData(sql)
            While dr.Read
                sb.Append(dr.Item("sitename").ToString() & "<br>")
            End While
            dr.Close()
            usitesdiv.InnerHtml = sb.ToString
        End If

    End Sub
    Private Sub GetApps(ByVal uid As String)
        Dim sb As New System.Text.StringBuilder
        Dim scnt As Integer
        If uid = "pmadmin" Or uid = "pmadmin1" Then
            sql = "select * from pmapps"
        Else
            sql = "select u.app, p.appname from pmUserApps u left join pmApps p on " _
                        + "p.app = u.app where u.uid = '" & uid & "' "
        End If
        dr = sec.GetRdrData(sql)
        While dr.Read
            sb.Append(dr.Item("appname").ToString() & "<br>")
        End While
        dr.Close()
        uappsdiv.InnerHtml = sb.ToString
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3435.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3435")
        Catch ex As Exception
        End Try
        Try
            lang3436.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3436")
        Catch ex As Exception
        End Try
        Try
            lang3437.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3437")
        Catch ex As Exception
        End Try
        Try
            lang3438.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3438")
        Catch ex As Exception
        End Try
        Try
            lang3439.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3439")
        Catch ex As Exception
        End Try
        Try
            lang3440.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3440")
        Catch ex As Exception
        End Try
        Try
            lang3441.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3441")
        Catch ex As Exception
        End Try
        Try
            lang3442.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3442")
        Catch ex As Exception
        End Try
        Try
            lang3443.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3443")
        Catch ex As Exception
        End Try
        Try
            lang3444.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3444")
        Catch ex As Exception
        End Try
        Try
            lang3445.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3445")
        Catch ex As Exception
        End Try
        Try
            lang3446.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3446")
        Catch ex As Exception
        End Try
        Try
            lang3447.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3447")
        Catch ex As Exception
        End Try
        Try
            lang3448.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3448")
        Catch ex As Exception
        End Try
        Try
            lang3449.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3449")
        Catch ex As Exception
        End Try
        Try
            lang3450.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3450")
        Catch ex As Exception
        End Try
        Try
            lang3451.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3451")
        Catch ex As Exception
        End Try
        Try
            lang3452.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3452")
        Catch ex As Exception
        End Try
        Try
            lang3453.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3453")
        Catch ex As Exception
        End Try
        Try
            lang3454.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3454")
        Catch ex As Exception
        End Try
        Try
            lang3455.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3455")
        Catch ex As Exception
        End Try
        Try
            lang3456.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3456")
        Catch ex As Exception
        End Try
        Try
            lang3457.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3457")
        Catch ex As Exception
        End Try
        Try
            lang3458.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3458")
        Catch ex As Exception
        End Try
        Try
            lang3459.Text = axlabs.GetASPXPage("EditProfile_v2.aspx", "lang3459")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnchangepass.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnchangepass.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnchangepass.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnchangepass.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnchangepass.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btneditforumprofile.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btneditforumprofile.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btneditforumprofile.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btneditforumprofile.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btneditforumprofile.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
