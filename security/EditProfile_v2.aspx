<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EditProfile_v2.aspx.vb" Inherits="lucy_r12.EditProfile_v2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="pgtitle">Edit Profile</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/EditProfile_v2aspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; POSITION: absolute; TOP: 2px; LEFT: 2px" width="980">
				<tr>
					<td class="thdrsing label" colSpan="3"><asp:Label id="lang3435" runat="server">General Information</asp:Label></td>
					<td width="5">&nbsp;</td>
					<td class="thdrsing label" width="260"><asp:Label id="lang3436" runat="server">Security Information</asp:Label></td>
				</tr>
				<tr>
					<td class="label" width="120"><asp:Label id="lang3437" runat="server">Full Name</asp:Label></td>
					<td width="160"><asp:textbox id="txtuname" runat="server" MaxLength="50"></asp:textbox></td>
					<td class="plainlabelblue" width="440"><asp:Label id="lang3438" runat="server">As you would like it to appear at the top of the screen, in the PM Library, Forums, etc.</asp:Label></td>
					<td></td>
					<td vAlign="top" rowSpan="7">
						<table>
							<tr>
								<td class="label"><asp:Label id="lang3439" runat="server">Authorized Plant Sites</asp:Label></td>
							</tr>
							<tr>
								<td>
									<div class="plainlabel" id="usitesdiv" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; PADDING-BOTTOM: 3px; PADDING-LEFT: 3px; WIDTH: 240px; PADDING-RIGHT: 3px; HEIGHT: 70px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid; PADDING-TOP: 3px"
										runat="server"></div>
								</td>
							</tr>
							<tr>
								<td class="label"><asp:Label id="lang3440" runat="server">Authorized Applications</asp:Label></td>
							</tr>
							<tr>
								<td>
									<div class="plainlabel" id="uappsdiv" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; PADDING-BOTTOM: 3px; PADDING-LEFT: 3px; WIDTH: 240px; PADDING-RIGHT: 3px; HEIGHT: 70px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid; PADDING-TOP: 3px"
										runat="server"></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3441" runat="server">Login ID</asp:Label></td>
					<td><asp:textbox id="txtuid" tabIndex="1" runat="server" MaxLength="50"></asp:textbox></td>
					<td class="plainlabelblue"><asp:Label id="lang3442" runat="server">Something you´ll easily remember</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3443" runat="server">Phone Number</asp:Label></td>
					<td><asp:textbox id="txtphone" tabIndex="2" runat="server" MaxLength="50"></asp:textbox></td>
					<td class="plainlabelblue"><asp:Label id="lang3444" runat="server">Full work phone number or extension</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3445" runat="server">Alt Phone Number</asp:Label></td>
					<td><asp:textbox id="txtaltphone" tabIndex="3" runat="server" MaxLength="50"></asp:textbox></td>
					<td class="plainlabelblue"><asp:Label id="lang3446" runat="server">Optional alternative number where you can be reached (e.g., home, cell, etc.)</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3447" runat="server">Email Address</asp:Label></td>
					<td><asp:textbox id="txtemail" tabIndex="4" runat="server" MaxLength="200"></asp:textbox></td>
					<td class="plainlabelblue"><asp:Label id="lang3448" runat="server">Your Company Email Address</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3449" runat="server">Alt Email Address</asp:Label></td>
					<td><asp:textbox id="txtaltemail" tabIndex="5" runat="server" MaxLength="200"></asp:textbox></td>
					<td class="plainlabelblue"><asp:Label id="lang3450" runat="server">Optional alternative email address where you can be reached (e.g., home, blackberry, etc.)</asp:Label></td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><asp:imagebutton id="btnsubmitprofile" tabIndex="7" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td class="thdrsing label" colSpan="3"><asp:Label id="lang3451" runat="server">Change Password</asp:Label></td>
					<td></td>
					<td class="thdrsing label"><asp:Label id="lang3452" runat="server">Forum Pic or Logo</asp:Label></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3453" runat="server">Password</asp:Label></td>
					<td><asp:textbox id="txtp1" tabIndex="8" runat="server" MaxLength="20" TextMode="Password"></asp:textbox></td>
					<td class="plainlabelblue"><asp:Label id="lang3454" runat="server">Something you´ll easily remember</asp:Label></td>
					<td></td>
					<td vAlign="middle" align="center" rowSpan="7">
						<table cellPadding="0">
							<tr>
								<td align="center" colSpan="3"><A onclick="getbig();" href="#"><IMG id="imgeq" height="216" src="../images/nophoto.gif" width="216" border="0" runat="server"></A></td>
							</tr>
							<tr>
								<td align="center" colSpan="3"><INPUT class="plainlabel" id="MyFile" style="WIDTH: 185px; HEIGHT: 24px" type="file" size="11"
										name="MyFile" RunAt="Server"><input class="lilmenu plainlabel" id="btnupload" style="WIDTH: 55px; HEIGHT: 25px" type="button"
										value="Upload" name="btnupload" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3455" runat="server">Verify Password</asp:Label></td>
					<td><asp:textbox id="txtp2" tabIndex="9" runat="server" MaxLength="20" TextMode="Password"></asp:textbox></td>
					<td class="plainlabelblue"><asp:Label id="lang3456" runat="server">Verify your new password</asp:Label></td>
				</tr>
				<TR>
					<td align="right" colSpan="3"><asp:imagebutton id="btnchangepass" tabIndex="10" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
				</TR>
				<tr>
					<td class="thdrsing label" colSpan="3"><asp:Label id="lang3457" runat="server">PM Forums Profile</asp:Label></td>
				</tr>
				<tr>
					<td class="label" style="HEIGHT: 27px"><asp:Label id="lang3458" runat="server">Job Title</asp:Label></td>
					<td style="HEIGHT: 27px" colSpan="2"><asp:textbox id="txtjt" tabIndex="11" runat="server" MaxLength="100" Width="580"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label" vAlign="top"><asp:Label id="lang3459" runat="server">Profile Overview</asp:Label></td>
					<td colSpan="2"><asp:textbox id="txtpo" tabIndex="12" runat="server" TextMode="MultiLine" Width="580" Height="60px"></asp:textbox></td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><asp:imagebutton id="btneditforumprofile" tabIndex="13" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
				</tr>
			</table>
			<input id="lblcurr" type="hidden" name="lblcurr" runat="server"> <input id="lbldb" type="hidden" runat="server">
			<input id="lblcurrpic" type="hidden" runat="server"><input id="lblcurrn" type="hidden" runat="server">
			<input id="lblro" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
