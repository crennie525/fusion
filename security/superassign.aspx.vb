

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class superassign
    Inherits System.Web.UI.Page
	Protected WithEvents lang3485 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3484 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3483 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3482 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3481 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3480 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3479 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3478 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3477 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3476 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3475 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3474 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sup As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim sid, supid, ro As String
    Protected WithEvents ddsite As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdemail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdconflicts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifcon As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents getdept As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents getloc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsup As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsupret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdsuper As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdmode As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdshift As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents getlab As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents getskill As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        'Dim sitst As String = Request.QueryString.ToString
        'siutils.GetAscii(Me, sitst)

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            lblsid.Value = HttpContext.Current.Session("dfltps").ToString
            supid = Request.QueryString("supid").ToString
            lblsupid.Value = supid
            sup.Open()
            GetSupDets()
            PopSites(supid)
            sup.Dispose()

        End If
        ddsite.Attributes.Add("onchange", "getsite(this.value);")
    End Sub
    Private Sub GetSupDets()
        supid = lblsupid.Value
        sql = "select username, shift, phonenum, email from pmsysusers where userid = '" & supid & "'"
        dr = sup.GetRdrData(sql)
        While dr.Read
            tdsuper.InnerHtml = dr.Item("username").ToString
            tdshift.InnerHtml = dr.Item("shift").ToString
            tdphone.InnerHtml = dr.Item("phonenum").ToString
            tdemail.InnerHtml = dr.Item("email").ToString
        End While
        dr.Close()
    End Sub
    Private Sub PopSites(ByVal supid As String)
        sql = "select u.siteid, s.sitename from pmusersites u left join sites s on s.siteid = u.siteid " _
        + "left join pmsysusers p on p.uid = u.uid where p.userid = '" & supid & "'"
        dr = sup.GetRdrData(sql)
        ddsite.DataSource = dr
        ddsite.DataTextField = "sitename"
        ddsite.DataValueField = "siteid"
        ddsite.DataBind()
        dr.Close()
        ddsite.Items.Insert(0, "Select Site")
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3474.Text = axlabs.GetASPXPage("superassign.aspx","lang3474")
		Catch ex As Exception
		End Try
		Try
			lang3475.Text = axlabs.GetASPXPage("superassign.aspx","lang3475")
		Catch ex As Exception
		End Try
		Try
			lang3476.Text = axlabs.GetASPXPage("superassign.aspx","lang3476")
		Catch ex As Exception
		End Try
		Try
			lang3477.Text = axlabs.GetASPXPage("superassign.aspx","lang3477")
		Catch ex As Exception
		End Try
		Try
			lang3478.Text = axlabs.GetASPXPage("superassign.aspx","lang3478")
		Catch ex As Exception
		End Try
		Try
			lang3479.Text = axlabs.GetASPXPage("superassign.aspx","lang3479")
		Catch ex As Exception
		End Try
		Try
			lang3480.Text = axlabs.GetASPXPage("superassign.aspx","lang3480")
		Catch ex As Exception
		End Try
		Try
			lang3481.Text = axlabs.GetASPXPage("superassign.aspx","lang3481")
		Catch ex As Exception
		End Try
		Try
			lang3482.Text = axlabs.GetASPXPage("superassign.aspx","lang3482")
		Catch ex As Exception
		End Try
		Try
			lang3483.Text = axlabs.GetASPXPage("superassign.aspx","lang3483")
		Catch ex As Exception
		End Try
		Try
			lang3484.Text = axlabs.GetASPXPage("superassign.aspx","lang3484")
		Catch ex As Exception
		End Try
		Try
			lang3485.Text = axlabs.GetASPXPage("superassign.aspx","lang3485")
		Catch ex As Exception
		End Try

	End Sub

End Class
