

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class lsuper
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim su As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents dvsuper As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            su.Open()
            GetSuper()
            su.Dispose()
        End If
    End Sub
    Private Sub GetSuper()
        Dim super, superi As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from pmsysusers where issuper <> 0"
        dr = su.GetRdrData(sql)
        While dr.Read
            super = dr.Item("username").ToString
            superi = dr.Item("userid").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getsuper('" & super & "', '" & superi & "');"">" & super & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        dvsuper.InnerHtml = sb.ToString
    End Sub
End Class
