

'********************************************************
'*
'********************************************************



Public Class chngpass
    Inherits System.Web.UI.Page
	Protected WithEvents lang3426 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3425 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Private usr, pas, sql As String
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtp1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtp2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtuser As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        txtuser.Value = Request.QueryString("user")

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSubmit.Click
        Dim enpas As New Utilities
        usr = txtuser.Value
        pas = txtp2.Text
        pas = enpas.Encrypt(pas)
        enpas.Open()
        sql = "update PMSysUsers set passwrd = '" & pas & "' where uid = '" & usr & "'"
        TextBox1.Text = sql
        enpas.Update(sql)
        enpas.Dispose()
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3425.Text = axlabs.GetASPXPage("chngpass.aspx", "lang3425")
        Catch ex As Exception
        End Try
        Try
            lang3426.Text = axlabs.GetASPXPage("chngpass.aspx", "lang3426")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnSubmit.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnSubmit.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnSubmit.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnSubmit.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnSubmit.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
