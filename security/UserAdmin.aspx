<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UserAdmin.aspx.vb" Inherits="lucy_r12.UserAdmin" %>
<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UserAdmin</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../scripts/gridnavopt_1.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script language="JavaScript" src="../scripts1/UserAdminaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; LEFT: 5px; POSITION: absolute; TOP: 81px" cellSpacing="2" cellPadding="0"
				width="980" border="0">
				<TBODY>
					<tr>
						<td width="230"></td>
						<td width="220"></td>
						<td width="220"></td>
						<td width="240"></td>
					</tr>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang3486" runat="server">PM User Administration</asp:Label></td>
						<td class="thdrsingred label"><asp:Label id="lang3487" runat="server">PM New User Dialog</asp:Label></td>
					</tr>
					<tr>
						<td vAlign="top" align="left" colSpan="3">
							<table>
								<TBODY>
									<tr>
										<td vAlign="top" colSpan="3" height="135">
											<div id="ispdiv" style="OVERFLOW: auto; HEIGHT: 140px" onscroll="SetiDivPosition();"><asp:repeater id="dgusers" runat="server">
													<HeaderTemplate>
														<table cellspacing="0">
															<tr>
																<td class="btmmenu plainlabel" width="120px"><asp:Label id="lang3488" runat="server">Login ID</asp:Label></td>
																<td class="btmmenu plainlabel" width="200px"><asp:Label id="lang3489" runat="server">User Name</asp:Label></td>
																<td class="btmmenu plainlabel" width="100px"><asp:Label id="lang3490" runat="server">Phone</asp:Label></td>
																<td class="btmmenu plainlabel" width="200px"><asp:Label id="lang3491" runat="server">Email</asp:Label></td>
															</tr>
													</HeaderTemplate>
													<ItemTemplate>
														<tr class="tbg">
															<td class="plainlabel">
																<asp:radiobutton AutoPostBack="True" OnCheckedChanged="GetUser" id="rbuid" Text='<%# DataBinder.Eval(Container.DataItem,"uid")%>' runat="server"  /></td>
															<td class="plainlabel">
																<asp:label ID="lblusername" Text='<%# DataBinder.Eval(Container.DataItem,"username")%>' Runat = server>
																</asp:label></td>
															<td class="plainlabel">
																<asp:label ID="lblphone" Text='<%# DataBinder.Eval(Container.DataItem,"phonenum")%>' Runat = server>
																</asp:label></td>
															<td class="plainlabel">
																<a href='mailto:<%# DataBinder.Eval(Container.DataItem,"email")%>' >
																	<asp:label ID="lblemail" Text='<%# DataBinder.Eval(Container.DataItem,"email")%>' Runat = server>
																	</asp:label></a></td>
															<td class="details"><asp:Label ID="lblindex" Text='<%# DataBinder.Eval(Container.DataItem,"groupindex")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblstatus" Text='<%# DataBinder.Eval(Container.DataItem,"status")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblexp" Text='<%# DataBinder.Eval(Container.DataItem,"expires")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblexpd" Text='<%# DataBinder.Eval(Container.DataItem,"expiration")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbldps" Text='<%# DataBinder.Eval(Container.DataItem,"dfltps")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblps" Text='<%# DataBinder.Eval(Container.DataItem,"ps")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblms" Text='<%# DataBinder.Eval(Container.DataItem,"multisite")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblro" Text='<%# DataBinder.Eval(Container.DataItem,"readonly")%>' Runat = server>
																</asp:Label></td>
														</tr>
													</ItemTemplate>
													<AlternatingItemTemplate>
														<tr>
															<td class="plainlabel transrowblue">
																<asp:radiobutton AutoPostBack="True" OnCheckedChanged="GetUser" id="rbuida" Text='<%# DataBinder.Eval(Container.DataItem,"uid")%>' runat="server"  /></td>
															<td class="plainlabel transrowblue">
																<asp:label ID="lblusernamea" Text='<%# DataBinder.Eval(Container.DataItem,"username")%>' Runat = server>
																</asp:label></td>
															<td class="plainlabel transrowblue">
																<asp:label ID="lblphonea" Text='<%# DataBinder.Eval(Container.DataItem,"phonenum")%>' Runat = server>
																</asp:label></td>
															<td class="plainlabel transrowblue">
																<a href='mailto:<%# DataBinder.Eval(Container.DataItem,"email")%>' >
																	<asp:label ID="lblemaila" Text='<%# DataBinder.Eval(Container.DataItem,"email")%>' Runat = server>
																	</asp:label></a></td>
															<td class="details"><asp:Label ID="lblindexa" Text='<%# DataBinder.Eval(Container.DataItem,"groupindex")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblstatusa" Text='<%# DataBinder.Eval(Container.DataItem,"status")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblexpa" Text='<%# DataBinder.Eval(Container.DataItem,"expires")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblexpda" Text='<%# DataBinder.Eval(Container.DataItem,"expiration")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbldpsa" Text='<%# DataBinder.Eval(Container.DataItem,"dfltps")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblpsa" Text='<%# DataBinder.Eval(Container.DataItem,"ps")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblmsa" Text='<%# DataBinder.Eval(Container.DataItem,"multisite")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblroa" Text='<%# DataBinder.Eval(Container.DataItem,"readonly")%>' Runat = server>
																</asp:Label></td>
														</tr>
													</AlternatingItemTemplate>
													<FooterTemplate>
							</table>
							</FooterTemplate> </asp:repeater></DIV></td>
					</tr>
					<tr>
						<td align="center" colSpan="3">
							<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img5" onclick="getfirst('i');" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img6" onclick="getprev('i');" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblipg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img7" onclick="getnext('i');" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td width="20"><IMG id="Img8" onclick="getlast('i');" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="details">
						<td colSpan="3">
							<table>
								<tr>
									<td class="bluelabel" id="tdpg" width="470" runat="server"></td>
									<td align="right" width="150"><asp:imagebutton id="btnprev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnnext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td vAlign="top" align="left" width="220">
							<table style="BORDER-RIGHT: gray 1px groove; BORDER-TOP: gray 1px groove; BORDER-LEFT: gray 1px groove; BORDER-BOTTOM: gray 1px groove"
								cellSpacing="0" cellPadding="1" width="220">
								<tr>
									<td class="thdrsingrt label"><asp:Label id="lang3492" runat="server">Selected User Profile</asp:Label></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang3493" runat="server">Login ID</asp:Label></td>
								</tr>
								<tr>
									<td>&nbsp;<asp:textbox id="txtuserid" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang3494" runat="server">User Name</asp:Label></td>
								</tr>
								<tr>
									<td>&nbsp;<asp:textbox id="txtusername" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang3495" runat="server">Phone#</asp:Label></td>
								</tr>
								<tr>
									<td>&nbsp;<asp:textbox id="txtphone" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang3496" runat="server">Email Address</asp:Label></td>
								</tr>
								<tr>
									<td>&nbsp;<asp:textbox id="txtemail" runat="server" CssClass="plainlabel" Width="192px"></asp:textbox></td>
								</tr>
								<tr height="20">
									<td class="bluelabel" align="center"><asp:checkbox id="cbro" runat="server" Text="User Is Read-Only"></asp:checkbox></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang3497" runat="server">Default Plant Site</asp:Label></td>
								</tr>
								<tr>
									<td style="HEIGHT: 15px">&nbsp;
										<asp:dropdownlist id="ddps" runat="server" CssClass="plainlabel" Width="192px"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td class="label" align="center">
										<table>
											<tr>
												<td class="label"><asp:Label id="lang3498" runat="server">Multiple Sites?</asp:Label></td>
												<td><IMG id="btnaddnewfail" onmouseover="return overlib('Add Muliple Site Access to Selected User')"
														onclick="GetFailDiv('cur');" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
														width="20"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="right"><asp:imagebutton id="btnsubmitprofile" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton>&nbsp;</td>
								</tr>
							</table>
						</td>
						<td vAlign="top" align="left" width="220">
							<table style="BORDER-RIGHT: gray 1px groove; BORDER-TOP: gray 1px groove; BORDER-LEFT: gray 1px groove; BORDER-BOTTOM: gray 1px groove"
								cellSpacing="0" cellPadding="1" width="220">
								<tr>
									<td class="thdrsingrt label"><asp:Label id="lang3499" runat="server">Selected User Applications</asp:Label></td>
								</tr>
								<tr height="238">
									<td class="plainlabel" id="tdapps" vAlign="top" runat="server"></td>
								</tr>
								<tr>
									<td align="right" colSpan="2"><IMG id="addapp" onclick="getuserapps('cur');" height="19" alt="" src="../images/appbuttons/bgbuttons/badd.gif"
											width="69" runat="server">&nbsp;</td>
								</tr>
							</table>
						</td>
						<td vAlign="top" align="left" width="230">
							<table style="BORDER-RIGHT: gray 1px groove; BORDER-TOP: gray 1px groove; BORDER-LEFT: gray 1px groove; BORDER-BOTTOM: gray 1px groove"
								cellSpacing="0" cellPadding="1">
								<tr>
									<td class="thdrsingrt label" colSpan="2"><asp:Label id="lang3500" runat="server">Selected User Password</asp:Label></td>
								</tr>
								<tr>
									<td class="label" align="center" colSpan="2"><asp:radiobuttonlist id="rbactive" runat="server" CssClass="plainlabel" RepeatLayout="Flow" Height="26px"
											RepeatDirection="Horizontal">
											<asp:ListItem Value="active">active</asp:ListItem>
											<asp:ListItem Value="inactive">inactive</asp:ListItem>
										</asp:radiobuttonlist></td>
								</tr>
								<tr>
									<td class="label" vAlign="middle" width="110"><asp:Label id="lang3501" runat="server">Expires?</asp:Label></td>
									<td class="label" width="120"><asp:radiobuttonlist id="rbexpires" runat="server" CssClass="plainlabel" RepeatLayout="Flow" Height="0px"
											RepeatDirection="Horizontal">
											<asp:ListItem Value="Y">Yes</asp:ListItem>
											<asp:ListItem Value="N">No</asp:ListItem>
										</asp:radiobuttonlist></td>
								</tr>
								<tr>
									<td class="LABEL"><asp:Label id="lang3502" runat="server">Expires On</asp:Label></td>
									<td><asp:textbox id="txtexpd" runat="server" CssClass="plainlabel" ReadOnly="True"></asp:textbox><IMG onclick="getcal('txtexpd');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
											width="19"></td>
								</tr>
								<tr>
									<td align="right" colSpan="2"><asp:imagebutton id="btnusercr" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton>&nbsp;&nbsp;</td>
								</tr>
								<tr>
									<td class="labelibl" id="tdpwd" colSpan="2" height="20" runat="server"></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang3503" runat="server">Password</asp:Label></td>
									<td><asp:textbox id="txtp1" runat="server" CssClass="plainlabel" MaxLength="20" TextMode="Password"></asp:textbox></td>
								</tr>
								<tr>
									<td class="label"><asp:Label id="lang3504" runat="server">Verify Password</asp:Label></td>
									<td><asp:textbox id="txtp2" runat="server" CssClass="plainlabel" MaxLength="20" TextMode="Password"></asp:textbox></td>
								</tr>
								<tr>
									<td align="right" colSpan="2"><asp:imagebutton id="btnsubmitpass" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton>&nbsp;&nbsp;</td>
								</tr>
							</table>
							<table>
								<tr>
									<td class="thdrsingorange label" width="230"><asp:Label id="lang3505" runat="server">PM User Stats</asp:Label></td>
								</tr>
							</table>
							<table style="BORDER-RIGHT: gray 1px groove; BORDER-TOP: gray 1px groove; BORDER-LEFT: gray 1px groove; BORDER-BOTTOM: gray 1px groove">
								<tr height="20">
									<td class="label" width="180"><asp:Label id="lang3506" runat="server">User Licenses Available</asp:Label></td>
									<td class="plainlabel" id="tdlic" align="center" width="50" runat="server"></td>
								</tr>
								<tr height="20">
									<td class="label"><asp:Label id="lang3507" runat="server">Current Active Users</asp:Label></td>
									<td class="plainlabel" id="tdusers" align="center" runat="server"></td>
								</tr>
								<tr>
									<td colSpan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</TBODY>
			</table>
			</TD>
			<td vAlign="top" width="240">
				<table cellSpacing="0" cellPadding="0" width="240">
					<tr height="20">
						<td class="label"><asp:Label id="lang3508" runat="server">New Login ID</asp:Label></td>
						<td><asp:textbox id="txtnewuid" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang3509" runat="server">Password</asp:Label></td>
						<td><asp:textbox id="txtp1new" runat="server" CssClass="plainlabel" MaxLength="20" TextMode="Password"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang3510" runat="server">Verify Password</asp:Label></td>
						<td><asp:textbox id="txtp2new" runat="server" CssClass="plainlabel" MaxLength="20" TextMode="Password"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label" vAlign="middle"><asp:Label id="lang3511" runat="server">Expires?</asp:Label></td>
						<td class="label"><asp:radiobuttonlist id="rbexpiresnew" runat="server" CssClass="plainlabel" RepeatLayout="Flow" Height="0px"
								RepeatDirection="Horizontal">
								<asp:ListItem Value="Y">Yes</asp:ListItem>
								<asp:ListItem Value="N" Selected="True">No</asp:ListItem>
							</asp:radiobuttonlist></td>
					</tr>
					<tr>
						<td class="LABEL"><asp:Label id="lang3512" runat="server">Expires On</asp:Label></td>
						<td><asp:textbox id="txtexpdnew" runat="server" CssClass="plainlabel" ReadOnly="True"></asp:textbox><IMG onclick="getcal('txtexpdnew');" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
								width="19"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang3513" runat="server">User Name</asp:Label></td>
						<td><asp:textbox id="txtusernamenew" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang3514" runat="server">Phone#</asp:Label></td>
						<td><asp:textbox id="txtphonenew" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label" colSpan="2"><asp:Label id="lang3515" runat="server">Email Address</asp:Label></td>
					</tr>
					<tr>
						<td colSpan="2"><asp:textbox id="txtemailnew" runat="server" CssClass="plainlabel" MaxLength="200" Width="192px"></asp:textbox></td>
					</tr>
					<tr height="20">
						<td class="bluelabel" colspan="2" align="center"><asp:checkbox id="cbroa" runat="server" Text="User Is Read-Only"></asp:checkbox></td>
					</tr>
					<tr>
						<td class="label" colSpan="2"><asp:Label id="lang3516" runat="server">Default Plant Site</asp:Label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 15px" colSpan="2"><asp:dropdownlist id="ddpsnew" runat="server" CssClass="plainlabel" Width="192px"></asp:dropdownlist><IMG id="btnaddnewsites" onmouseover="return overlib('Add Muliple Site Access to Selected User')"
								onclick="GetFailDiv('new');" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif" width="20"></td>
					</tr>
					<tr>
						<td class="label" colSpan="2"><asp:Label id="lang3517" runat="server">User Skill</asp:Label></td>
					</tr>
					<tr>
						<td colspan="2"><asp:textbox id="Textbox1" runat="server" CssClass="plainlabel" MaxLength="200" Width="192px"></asp:textbox>
							<IMG onclick="getsuper('lead');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
								border="0"></td>
					</tr>
					<tr>
						<td class="label" colSpan="2"><asp:Label id="lang3518" runat="server">Supervisor</asp:Label></td>
					</tr>
					<tr>
						<td colspan="2"><asp:textbox id="Textbox2" runat="server" CssClass="plainlabel" MaxLength="200" Width="192px"></asp:textbox>
							<IMG onclick="getsuper('sup');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
								border="0"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang3519" runat="server">User Applications</asp:Label></td>
						<td align="right"><IMG id="addappnew" onclick="getuserapps('new');" height="19" alt="" src="../images/appbuttons/bgbuttons/badd.gif"
								width="69" runat="server"></td>
					</tr>
					<tr height="100">
						<td class="plainlabel" id="newuappsdiv" style="BORDER-RIGHT: 2px groove; BORDER-TOP: 1px groove; OVERFLOW: auto; BORDER-LEFT: 1px groove; BORDER-BOTTOM: 2px groove; HEIGHT: 100px"
							vAlign="top" colSpan="2" runat="server">&nbsp;
						</td>
					</tr>
					<tr>
						<td align="right" colSpan="2"><IMG id="btnaddnewlogin" onclick="valnew();" height="19" src="../images/appbuttons/bgbuttons/submit.gif"
								width="71" runat="server"></td>
					</tr>
				</table>
			</td>
			</TR></TBODY></TABLE><input id="lblcid" type="hidden" runat="server"> <input id="lblsid" type="hidden" runat="server">
			<input id="lbluserid" type="hidden" runat="server"><input id="txtpg" type="hidden" runat="server">
			<input id="lbllic" type="hidden" runat="server"> <input id="lblusers" type="hidden" runat="server">
			<input id="lblnew" type="hidden" runat="server"><input id="lblnewuid" type="hidden" runat="server"><input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="lblpchk" type="hidden" runat="server"><input id="lbluid" type="hidden" runat="server">
			<input id="lblnewcheck" type="hidden" name="lblnewcheck" runat="server"><input id="lbladmin" type="hidden" name="lbladmin" runat="server">
			<input id="ispdivy" type="hidden" runat="server"> <input id="txtipg" type="hidden" name="Hidden2" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="txtipgcnt" type="hidden" name="txtipgcnt" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
