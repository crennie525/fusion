<%@ Page Language="vb" AutoEventWireup="false" Codebehind="chngpass.aspx.vb" Inherits="lucy_r12.chngpass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>chngpass</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="label"><asp:Label id="lang3425" runat="server">Password</asp:Label></td>
					<td><asp:textbox id="txtp1" runat="server" TextMode="Password"></asp:textbox></td>
					<td></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang3426" runat="server">Confirm Password</asp:Label></td>
					<td><asp:textbox id="txtp2" runat="server" TextMode="Password"></asp:textbox></td>
					<td></td>
				</tr>
				<tr>
					<td align="right" colSpan="3"><asp:imagebutton id="btnSubmit" runat="server" ImageUrl="../images/appbuttons/submitbg.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<asp:TextBox id="TextBox1" runat="server" Width="256px"></asp:TextBox></td>
				</tr>
			</table>
			<input id="txtuser" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
