﻿Imports System.Configuration
Imports System
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.Net.Mail
Public Class sysbulk
    Inherits System.Web.UI.Page
    Dim od As New excel2csv
    Dim adm As New Utilities
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim filename, admin, cadm, cid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            admin = Request.QueryString("admin").ToString
            cadm = Request.QueryString("cadm").ToString
            lbladmin.Value = admin
            lblcadm.Value = cadm
            ddps.Attributes.Add("onchange", "checkps();")
            tr1.Attributes.Add("class", "view")
            tr2.Attributes.Add("class", "view")
            tr3.Attributes.Add("class", "view")
            tr4.Attributes.Add("class", "details")
            tr5.Attributes.Add("class", "details")
            tr6.Attributes.Add("class", "details")
            tr7.Attributes.Add("class", "details")
            tr7a.Attributes.Add("class", "details")
            tr8.Attributes.Add("class", "details")
            tr9.Attributes.Add("class", "details")
            tr10.Attributes.Add("class", "details")
            tr11.Attributes.Add("class", "details")
            tr12.Attributes.Add("class", "details")
            adm.Open()
            GetSites()
            adm.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                adm.Open()
                bulkload()
                adm.Dispose()


                tr1.Attributes.Add("class", "view")
                tr2.Attributes.Add("class", "view")
                tr3.Attributes.Add("class", "view")
                tr4.Attributes.Add("class", "details")
                tr5.Attributes.Add("class", "details")
                tr6.Attributes.Add("class", "details")
                tr7.Attributes.Add("class", "details")
                tr7a.Attributes.Add("class", "details")
                tr8.Attributes.Add("class", "details")
                tr9.Attributes.Add("class", "details")
                tr10.Attributes.Add("class", "details")
                tr11.Attributes.Add("class", "details")
                tr12.Attributes.Add("class", "details")

                lblfpath.Value = ""
                lbldoesexpire.Value = ""
                lblexpire.Value = ""
                lblneedsup.Value = ""
                lblsupid.Value = ""
                lblsup.Value = ""
                lblspid.Value = ""
                dfltsid.Value = ""
            End If

        End If
    End Sub
    Private Sub bulkload()
        Dim fpath, doesexpire, expire, needsup, supid, sup, spid, df As String
        fpath = lblfpath.Value
        doesexpire = lbldoesexpire.Value
        expire = lblexpire.Value
        needsup = lblneedsup.Value
        supid = lblsupid.Value
        sup = lblsup.Value
        spid = lblspid.Value
        df = dfltsid.Value
        Dim errflag As Integer = 0
        Dim FilePath As String = fpath
        
        If errflag = 0 Then
            Dim sr1 As StreamReader = New StreamReader(FilePath)
            Dim line As String
            line = sr1.ReadLine()
            Do While (sr1.Peek() >= 0)
                line = sr1.ReadLine()
                If Len(line) > 0 Then
                    Dim larr() As String = line.Split(",")
                    Dim c0 As String = RTrim(larr(0))
                    Dim c1 As String = RTrim(larr(1))
                    Dim c2 As String = RTrim(larr(2))
                    Dim c3 As String = RTrim(larr(3))
                    Dim c4 As String = RTrim(larr(4))

                    c1 = Replace(c1, "'", Chr(180), , , vbTextCompare)
                    c1 = Replace(c1, """", Chr(180) & Chr(180), , , vbTextCompare)
                    c2 = Replace(c2, "'", Chr(180), , , vbTextCompare)
                    c2 = Replace(c2, """", Chr(180) & Chr(180), , , vbTextCompare)
                    c3 = Replace(c3, "'", Chr(180), , , vbTextCompare)
                    c3 = Replace(c3, """", Chr(180) & Chr(180), , , vbTextCompare)
                    c4 = Replace(c4, "'", Chr(180), , , vbTextCompare)
                    c4 = Replace(c4, """", Chr(180) & Chr(180), , , vbTextCompare)
                    addUser2(c2, c1, c3, c4, df, c0, doesexpire)
                End If
            Loop
        End If
        
    End Sub
    Private Sub addUser2(ByVal sname As String, ByVal pass As String, ByVal pho As String, ByVal ema As String, ByVal dps As String, ByVal uid As String, ByVal doesexpire As String)
        'Dim uc, au As String
        'uc = lbllic.Value
        'au = lblusers.Value
        cid = "0"
        Dim exp, ro, admn, isl, iss, tmp, plnr As String
        sname = Replace(sname, "'", Chr(180), , , vbTextCompare)
        If Len(sname) > 50 Then
            Exit Sub
        End If
        If Len(sname) = 0 Then
            Exit Sub
        End If
        Dim strpass As String = pass
        pass = adm.Encrypt(pass)
        If Len(pho) > 50 Then
            Exit Sub
        End If
        'ema = Replace(ema, "'", "''")
        'dps = Replace(dps, "'", Chr(180), , , vbTextCompare)
        'uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
        If Len(uid) > 50 Then
            Exit Sub
        End If
        If Len(uid) = 0 Then
            'Dim strMessage As String = tmod.getmsg("cdstr1691", "useradmin2.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim uidcnt As Integer
        'adm.Open()
        Dim ucnt, ncnt As String
        sql = "select count(*) from PMSysUsers where uid = '" & uid & "' or username = '" & sname & "'"
        sql = "select ucnt = (select count(uid) from PMSysUsers where uid = '" & uid & "'), ncnt = (select count(username) from PMSysUsers where username = '" & sname & "') "
        dr = adm.GetRdrData(sql)
        While dr.Read
            ucnt = dr.Item("ucnt").ToString
            ncnt = dr.Item("ncnt").ToString
        End While
        dr.Close()
        Dim iucnt, incnt As Integer
        Try
            iucnt = System.Convert.ToInt32(ucnt)
        Catch ex As Exception
            iucnt = 0
        End Try
        Try
            incnt = System.Convert.ToInt32(ncnt)
        Catch ex As Exception
            incnt = 0
        End Try
        uidcnt = iucnt + incnt

        'add spid input here
        'admn, isl, iss, plnr
        Dim appstr As String
        Dim spid As String = lblspid.Value
        Dim super, superi, superin, skill, skilli, skillin, sht As String
        sql = "select * from sysprofiles where spid = '" & spid & "'"
        dr = adm.GetRdrData(sql)
        While dr.Read
            admn = dr.Item("admin").ToString
            isl = dr.Item("islabor").ToString
            iss = dr.Item("issuper").ToString
            plnr = dr.Item("isplanner").ToString
            skill = dr.Item("skill").ToString
            skilli = dr.Item("skillid").ToString
            skillin = "0"
            sht = dr.Item("shift").ToString
            appstr = dr.Item("appstr").ToString
        End While
        dr.Close()
        lblappstr.Value = appstr
        super = lblsup.Value
        superi = lblsupid.Value
        superin = "0"

        If uidcnt = 0 Then
            Dim stst As String
            stst = "exec usp_newuser1 '" & ema & "', '" & pho & "', '" & pass & "', '" & sname & "', " _
           + "'" & cid & "', '" & uid & "', '" & dps & "', '" & ro & "', '" & admn & "', " _
            + "'" & super & "', '" & superin & "', '" & superi & "', " _
            + "'" & skill & "', '" & skillin & "', '" & skilli & "', " _
            + "'" & isl & "', '" & iss & "', '" & tmp & "', '" & sht & "', '" & plnr & "'"

            Dim cmd As New SqlCommand("exec usp_newuser1 @email, @phonenum, @passwrd, @username, " _
            + "@compid, @uid, @dfltps, @readonly, @admin, " _
            + "@super, @superindex, @superid, " _
            + "@skill, @skillindex, @skillid, " _
            + "@islabor, @issuper, @tmpid, @shift, @plnr")

            Dim param01 = New SqlParameter("@email", SqlDbType.VarChar)
            If Len(ema) > 0 Then
                param01.Value = ema
            Else
                param01.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@phonenum", SqlDbType.VarChar)
            If Len(pho) > 0 Then
                param02.Value = pho
            Else
                param02.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@passwrd", SqlDbType.VarChar)
            If Len(pass) > 0 Then
                param03.Value = pass
            Else
                param03.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@username", SqlDbType.VarChar)
            If Len(sname) > 0 Then
                param04.Value = sname
            Else
                param04.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@compid", SqlDbType.VarChar)
            If Len(cid) > 0 Then
                param05.Value = cid
            Else
                param05.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param05)
            Dim param06 = New SqlParameter("@uid", SqlDbType.VarChar)
            If Len(uid) > 0 Then
                param06.Value = uid
            Else
                param06.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param06)
            Dim param07 = New SqlParameter("@dfltps", SqlDbType.VarChar)
            If Len(dps) > 0 Then
                param07.Value = dps
            Else
                param07.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param07)
            Dim param08 = New SqlParameter("@readonly", SqlDbType.VarChar)
            If Len(ro) > 0 Then
                param08.Value = ro
            Else
                param08.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param08)
            Dim param09 = New SqlParameter("@admin", SqlDbType.VarChar)
            If Len(admn) > 0 Then
                param09.Value = admn
            Else
                param09.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param09)
            Dim param010 = New SqlParameter("@super", SqlDbType.VarChar)
            If Len(super) > 0 Then
                param010.Value = super
            Else
                param010.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param010)
            Dim param011 = New SqlParameter("@superindex", SqlDbType.VarChar)
            If Len(superin) > 0 Then
                param011.Value = superin
            Else
                param011.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param011)
            Dim param012 = New SqlParameter("@superid", SqlDbType.VarChar)
            If Len(superi) > 0 Then
                param012.Value = superi
            Else
                param012.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param012)
            Dim param013 = New SqlParameter("@skill", SqlDbType.VarChar)
            If Len(skill) > 0 Then
                param013.Value = skill
            Else
                param013.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param013)
            Dim param014 = New SqlParameter("@skillindex", SqlDbType.VarChar)
            If Len(skillin) > 0 Then
                param014.Value = skillin
            Else
                param014.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param014)
            Dim param015 = New SqlParameter("@skillid", SqlDbType.VarChar)
            If Len(skilli) > 0 Then
                param015.Value = skilli
            Else
                param015.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param015)
            Dim param016 = New SqlParameter("@islabor", SqlDbType.VarChar)
            If Len(isl) > 0 Then
                param016.Value = isl
            Else
                param016.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param016)
            Dim param017 = New SqlParameter("@issuper", SqlDbType.VarChar)
            If Len(iss) > 0 Then
                param017.Value = iss
            Else
                param017.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param017)
            Dim param018 = New SqlParameter("@tmpid", SqlDbType.VarChar)
            If Len(tmp) > 0 Then
                param018.Value = tmp
            Else
                param018.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param018)
            Dim param019 = New SqlParameter("@shift", SqlDbType.VarChar)
            If Len(sht) > 0 Then
                param019.Value = sht
            Else
                param019.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param019)
            Dim param020 = New SqlParameter("@plnr", SqlDbType.VarChar)
            If Len(plnr) > 0 Then
                param020.Value = plnr
            Else
                param020.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param020)

            adm.UpdateHack(cmd)

            sql = "insert into pmusersites (uid, siteid) values ('" & uid & "', '" & dps & "')"
            adm.Update(sql)

            UpUser2(uid, admn)

            If ema <> "" Then
                SendIt("new", ema, uid, strpass, uid)
            End If



            If doesexpire = "yes" Then
                exp = lblexpire.Value
                If Len(exp) <> 0 Then
                    sql = "update PMSysUsers set expiration = '" & exp & "', expid = 1, expires = 'Y' where uid = '" & uid & "'"
                    adm.Update(sql)
                Else
                    'Dim strMessage As String = tmod.getmsg("cdstr1693", "useradmin2.aspx.vb")

                    'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
            End If
        Else
            'Dim strMessage As String = tmod.getmsg("cdstr1694", "useradmin2.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        lblappstr.Value = ""
        'adm.Dispose()
    End Sub
    Private Sub SendIt(ByVal typ As String, ByVal ema As String, ByVal uid As String, ByVal pas As String, ByVal lid As String)
        Dim email As New System.Web.Mail.MailMessage
        'Dim lid, pas As String
        'lid = txtnewuid.Text
        'pas = txtp2new.Text
        Dim str14, str4, str5, port, spass, sauth, sfrom As String
        str14 = System.Configuration.ConfigurationManager.AppSettings("custSMTP")
        str4 = System.Configuration.ConfigurationManager.AppSettings("custSendUser")
        str5 = System.Configuration.ConfigurationManager.AppSettings("custSendUsing")
        port = System.Configuration.ConfigurationManager.AppSettings("custSMTPPort")
        spass = System.Configuration.ConfigurationManager.AppSettings("custSendPass")
        sauth = System.Configuration.ConfigurationManager.AppSettings("custAMTPAuth")
        sfrom = System.Configuration.ConfigurationManager.AppSettings("custSendFrom")
        email.To = ema
        email.From = sfrom
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = str5
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = str14
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = str4
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = spass
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = sauth
        email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = port

        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If

        email.Body = AlertBody(lid, pas)
        email.Subject = "Your New PM Login ID"
        email.BodyFormat = Web.Mail.MailFormat.Html
        'System.Web.Mail.SmtpMail.SmtpServer.Insert(0, str14)
        Try
            System.Web.Mail.SmtpMail.Send(email)
        Catch ex As Exception
            Try
                Dim mail As New System.Net.Mail.MailMessage
                mail.From = New System.Net.Mail.MailAddress(sfrom)
                mail.To.Add(ema)
                mail.BodyEncoding = Encoding.Unicode
                mail.Subject = "Your New PMO Login ID"
                mail.Body = AlertBody(lid, pas)
                mail.IsBodyHtml = True
                Dim smtp As SmtpClient = New SmtpClient(str14)
                smtp.Send(mail)
            Catch ex0 As Exception
                'Dim strMessage As String = "Problem Sending Email"
                'adm.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try

        End Try
    End Sub
    Private Function AlertBody(ByVal lid As String, ByVal pas As String) As String
        Dim body As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim mu As New mmenu_utils_a
        body = "<table width='800px' style='font-size:10pt; font-family:Verdana;'>"
        body &= "<tr><td>" & mu.AppIntro & "<br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Your New User ID is " & lid & "<br></td></tr>" & vbCrLf
        body &= "<tr><td>And Your New Password is " & pas & "<br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & mu.AppAccess & "&nbsp;&nbsp;<a href='" & urlname & "'>" & mu.AppLink & "</td></tr>" & vbCrLf & vbCrLf
        body &= "</table>"
        Return body
    End Function
    Private Sub UpUser2(ByVal uid As String, ByVal admn As String)
        Dim spid As String = lblspid.Value
        Dim ap, aps, appstr As String

        aps = lblappstr.Value

        If admn = "1" Then
            sql = "update pmsysusers set appstr = 'all' where uid = '" & uid & "'"
        Else
            sql = "update pmsysusers set appstr = '" & aps & "' where uid = '" & uid & "'"
        End If
        adm.Update(sql)
        'Dim aparr() As String = ap.Split(",")
        ' Dim apsarr() As String = aps.Split(",")
        Dim stat As String = "1"
        sql = "insert into pmUserApps (uid, app, stat) select '" & uid & "', app, '" & stat & "' from sysapps where spid = '" & spid & "'"
        adm.Update(sql)

        'us.Dispose()
    End Sub
    Private Sub btnupload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.ServerClick
        If Not (File1.PostedFile Is Nothing) Then
            Import(File1)
        End If
    End Sub
    Private Sub GetSites()
        cid = "0"
        Dim scnt As Integer
        admin = lbladmin.Value
        cadm = lblcadm.Value
        If admin = "pmadmin" Or admin = "pmadmin1" Or cadm = "1" Then
            sql = "select count(*) from sites where compid = '" & cid & "'"
        Else
            sql = "select count(*) from pmUserSites where uid = '" & admin & "'"
        End If
        scnt = adm.Scalar(sql)
        If scnt > 0 Then
            If admin = "pmadmin" Or admin = "pmadmin1" Or cadm = "1" Then
                sql = "select siteid, sitename from sites where compid = '" & cid & "'"
            Else
                sql = "select u.siteid, s.sitename from pmUserSites u " _
                + "left join sites s on s.siteid = u.siteid where u.uid = '" & admin & "'"
            End If
            dr = adm.GetRdrData(sql)
            ddps.DataSource = dr
            ddps.DataValueField = "siteid"
            ddps.DataTextField = "sitename"
            ddps.DataBind()
            dr.Close()
            ddps.Items.Insert(0, "Select Plant Site")
            ddps.Enabled = True

        Else
            ddps.Items.Insert(0, "Select Plant Site")
            ddps.Enabled = False

        End If

    End Sub
    Private Sub Import(ByVal MyFile As HtmlInputFile)

        If Not (MyFile.PostedFile Is Nothing) Then

            'Check to make sure we actually have a file to upload
            Dim strLongFilePath As String = MyFile.PostedFile.FileName
            Dim intFileNameLength As Integer = InStr(1, StrReverse(strLongFilePath), "\")
            Dim strFileName As String = Mid(strLongFilePath, (Len(strLongFilePath) - intFileNameLength) + 2)
            If Len(strFileName) = 0 Then
                strFileName = strLongFilePath
            End If
            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim strto As String = appstr + "/eqimages/"
            Dim strScript As String
            Dim indx As Integer = strFileName.IndexOf(".csv")
            Dim dupflg As Integer = 0
            Dim dupchk As Integer = 1
            Dim probi As Integer = 0
            Dim fname As String = ""
            Dim errchk As Integer = 0
            Dim errmsg As String = ""
            If Len(strFileName) <> 0 And indx <> -1 Then
                Dim FilePath As String = HttpContext.Current.Server.MapPath("..\eqimages\" & strFileName)
                If File.Exists(FilePath) Then
                    dupflg = 1
                    fname = Mid(strFileName, 1, indx) & "(1).csv"
                    FilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                    While dupflg = 1
                        If File.Exists(FilePath) Then
                            dupchk += 1
                            fname = Mid(fname, 1, indx) & "(" & dupchk & ").csv"
                            FilePath = HttpContext.Current.Server.MapPath("..\eqimages\" & fname)
                        Else
                            dupflg = 0
                        End If
                        If dupchk = 5 Then
                            probi = 1
                            Exit While
                        End If
                    End While
                End If
                Dim tst As String = MyFile.PostedFile.ContentType.ToString
                If probi = 0 Then
                    Try
                        MyFile.PostedFile.SaveAs(FilePath)
                    Catch ex As Exception
                        lbldoctype.Value = "na"
                    End Try


                End If

                Dim doctype As String = lbldoctype.Value
                If doctype <> "na" Then
                    Dim sr As StreamReader = New StreamReader(FilePath)
                    Dim line As String
                    line = sr.ReadLine()
                    Dim larr() As String = line.Split(",")
                    Dim c0 As String = larr(0)
                    If c0.ToLower = "id" Then
                        Dim c1 As String = larr(1)
                        If c1.ToLower = "pass" Then
                            Dim c2 As String = larr(2)
                            If c2.ToLower = "name" Then
                                Dim c3 As String = larr(3)
                                If c3.ToLower = "phone" Then
                                    Dim c4 As String = larr(4)
                                    If c4.ToLower = "email" Then
                                        errchk = 0
                                    Else
                                        errchk = 1
                                    End If
                                Else
                                    errchk = 1
                                End If
                            Else
                                errchk = 1
                            End If
                        Else
                            errchk = 1
                        End If
                    Else
                        errchk = 1
                    End If
                    If errchk = 1 Then
                        errmsg = "First Row of Excel Spreadsheet (CSV File) Incorrect"
                        Utilities.CreateMessageAlert(Me, errmsg, "strKey1")
                    Else
                        errchk = 0

                        Dim sr1 As StreamReader = New StreamReader(FilePath)
                        Dim line1 As String
                        line1 = sr1.ReadLine()
                        Do While (sr1.Peek() >= 0)
                            line1 = sr1.ReadLine()
                            If Len(line1) > 0 Then
                                Dim larr1() As String = line1.Split(",")
                                Dim c00 As String = RTrim(larr1(0))
                                Dim c1 As String = RTrim(larr1(1))
                                Dim c2 As String = RTrim(larr1(2))
                                Dim c3 As String = RTrim(larr1(3))
                                Dim c4 As String = RTrim(larr1(4))
                                Try
                                    Dim c5 As String = RTrim(larr1(5))
                                    lblerr.Value = "xtra"
                                    errchk = 1
                                    Exit Do
                                Catch ex As Exception
                                    'reverse catch - number of columns appears to be correct
                                End Try

                            End If
                        Loop
                        If errchk = 0 Then
                            Dim lcnt As Integer = 0
                            Do While (sr.Peek() >= 0)
                                line = sr.ReadLine
                                If Len(line) > 0 Then

                                    'check for missing required values
                                    Dim larr2() As String = line.Split(",")
                                    If Len(larr2(0)) <> 0 Then
                                        lcnt += 1
                                        If Len(larr2(0)) = 0 Then
                                            errchk = 1
                                        ElseIf Len(larr2(1)) = 0 Then
                                            errchk = 1
                                        ElseIf Len(larr2(2)) = 0 Then
                                            errchk = 1
                                        End If
                                    End If

                                End If
                                If errchk = 1 Then
                                    Exit Do
                                End If
                            Loop
                            If errchk = 1 Then
                                errmsg = "Required Values are Missing in Some Columns"
                                Utilities.CreateMessageAlert(Me, errmsg, "strKey1")
                            Else
                                'last check for lengths
                                Dim lenchk As Integer = 0
                                Dim sr2 As StreamReader = New StreamReader(FilePath)
                                Dim line2 As String
                                line2 = sr2.ReadLine()
                                Do While (sr2.Peek() >= 0)
                                    line2 = sr2.ReadLine()
                                    If Len(line2) > 0 Then
                                        Dim larr2() As String = line2.Split(",")
                                        Dim c000 As String = RTrim(larr2(0))
                                        Dim c10 As String = RTrim(larr2(1))
                                        Dim c20 As String = RTrim(larr2(2))
                                        Dim c30 As String = RTrim(larr2(3))
                                        Dim c40 As String = RTrim(larr2(4))
                                        If Len(c000) > 50 Then
                                            lenchk += 1
                                        End If
                                        If Len(c20) > 50 Then
                                            lenchk += 1
                                        End If
                                        If Len(c30) > 50 Then
                                            lenchk += 1
                                        End If
                                        If Len(430) > 200 Then
                                            lenchk += 1
                                        End If

                                    End If
                                Loop
                                If lenchk = 0 Then
                                    lblfpath.Value = FilePath
                                    tr1.Attributes.Add("class", "details")
                                    tr2.Attributes.Add("class", "details")
                                    tr3.Attributes.Add("class", "details")
                                    tr4.Attributes.Add("class", "view")
                                    tr5.Attributes.Add("class", "view")
                                    tr6.Attributes.Add("class", "details")
                                    tr7.Attributes.Add("class", "details")
                                    tr7a.Attributes.Add("class", "details")
                                    tr8.Attributes.Add("class", "details")
                                    tr9.Attributes.Add("class", "details")
                                    tr10.Attributes.Add("class", "details")
                                    tr11.Attributes.Add("class", "details")
                                    tr12.Attributes.Add("class", "details")
                                    errmsg = "You have " & lcnt & " rows that appear to be ready for processing"
                                    Utilities.CreateMessageAlert(Me, errmsg, "strKey1")
                                Else
                                    errmsg = lenchk & " Values Exceed Maximum Character Lengths\nPlease Review Document Before Processing"
                                    Utilities.CreateMessageAlert(Me, errmsg, "strKey1")
                                End If
                                
                            End If
                        Else
                            errmsg = "Incorrect Number of Columns Detected\nPlease Check For and Remove any Extra Commas in your\nID, PASS, NAME, PHONE, and EMAIL Columns"
                            Utilities.CreateMessageAlert(Me, errmsg, "strKey1")
                        End If
                        
                    End If
                Else
                    errmsg = "Problem Saving File"
                    Utilities.CreateMessageAlert(Me, errmsg, "strKey1")
                End If
            Else
                errmsg = "Your Selected File does not appear to be a Comma Separated File (csv)."
                Utilities.CreateMessageAlert(Me, errmsg, "strKey1")

            End If
        End If
    End Sub
    
End Class