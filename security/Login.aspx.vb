

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class Login
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim login As New Utilities
    Dim psite, lo, sessid, cursess As String
    Dim exp, expd, li As String
    Private comp, compname, usrlev, usrname, dfltps, ms, sql, loggedin, grpndx, app, admin, cadm, uid, ua As String
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblname As System.Web.UI.WebControls.Label
    Protected WithEvents txtuid As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpass As System.Web.UI.WebControls.Label
    Protected WithEvents txtpass As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnlogin As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here


        'btnlogin.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ylogin.gif'")
        'btnlogin.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/blogin.gif'")
    End Sub
    Private Sub btnlogin_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnlogin.Click

        login.Open()
        Dim u, p, s As String
        u = txtuid.Text
        u = Replace(u, "'", Chr(180), , , vbTextCompare)
        p = txtpass.Text
        p = login.Encrypt(p)
        sql = "select count(*) from PMSysUsers where uid = '" & u & "' and passwrd = '" & p & "'"
        Dim chk, achk As Integer
        chk = login.Scalar(sql)
        If chk = 0 Then
            Dim aconn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("aConn"))
            aconn.Open()
            Dim cmd As New SqlCommand
            cmd.CommandText = sql
            cmd.Connection = aconn
            cmd.CommandTimeout = 120
            achk = cmd.ExecuteScalar()
            If achk = 1 Then
                sql = "usp_getgrpndx '" & u & "'"
                cmd.CommandText = sql
                cmd.Connection = aconn
                cmd.CommandTimeout = 120
                dr = cmd.ExecuteReader()
                'dr = login.GetRdrData(sql)
                While dr.Read
                    comp = dr.Item("compid").ToString
                    compname = dr.Item("compname").ToString
                    usrlev = dr.Item("groupname").ToString
                    cadm = dr.Item("admin").ToString
                    usrname = dr.Item("username").ToString
                    dfltps = dr.Item("dfltps").ToString
                    ms = dr.Item("multisite").ToString
                    uid = dr.Item("uid").ToString
                    exp = dr.Item("expires").ToString
                    expd = dr.Item("expiration").ToString
                    li = dr.Item("loggedin").ToString
                    ua = dr.Item("userlicense").ToString
                    psite = dr.Item("sitename").ToString
                End While
                dr.Close()
            Else
                lblchk.Value = "fail"
                Exit Sub
                aconn.Close()
            End If
        ElseIf chk = 1 Then
            sql = "usp_getgrpndx '" & u & "'"
            dr = login.GetRdrData(sql)
            While dr.Read
                comp = dr.Item("compid").ToString
                compname = dr.Item("compname").ToString
                usrlev = dr.Item("groupname").ToString
                cadm = dr.Item("admin").ToString
                usrname = dr.Item("username").ToString
                dfltps = dr.Item("dfltps").ToString
                ms = dr.Item("multisite").ToString
                uid = dr.Item("uid").ToString
                exp = dr.Item("expires").ToString
                expd = dr.Item("expiration").ToString
                li = dr.Item("loggedin").ToString
                ua = dr.Item("userlicense").ToString
                psite = dr.Item("sitename").ToString
            End While
            dr.Close()
        Else
            lblchk.Value = "fail"
            Exit Sub
        End If
        Dim expflg As Integer = 0
        If exp = "Y" Then
            Dim dat As Date = Now
            If dat > expd Then
                expflg = 1
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1666" , "Login.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
        If expflg <> 1 Then
            Dim ustr As String = Replace(usrname, "'", Chr(180), , , vbTextCompare)
            Dim cap As String = System.Configuration.ConfigurationManager.AppSettings.Item("custAppName").ToString
            Dim sessid As String = HttpContext.Current.Session.SessionID
            Session("sess") = sessid
            sql = "insert into logtrack (userid, logintime, app, sessionid) values ('" & ustr & "', getDate() , '" & cap & "', '" & sessid & "')"
            login.Update(sql)
            Session("comp") = comp
            Session("compname") = compname
            Session("userlevel") = usrlev
            Session("cadm") = cadm
            Session("grpndx") = grpndx
            Session("username") = usrname
            Session("uid") = u
            Session("dfltps") = dfltps
            Session("psite") = psite
            Session("ms") = ms
            Session("Logged_In") = "yes"
            Session("ua") = ua
            If achk = 1 Then
                lblchk.Value = "adm"

            Else
                lblchk.Value = "ok"
            End If
        Else
            lblchk.Value = "fail"
        End If
        login.Dispose()

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			Label1.Text = axlabs.GetASPXPage("Login.aspx","Label1")
		Catch ex As Exception
		End Try
		Try
			lblname.Text = axlabs.GetASPXPage("Login.aspx","lblname")
		Catch ex As Exception
		End Try

	End Sub

End Class
