

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class llabor
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim la As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents dvlabor As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            la.Open()
            GetLabor()
            la.Dispose()
        End If
    End Sub
    Private Sub GetLabor()
        Dim labor, labori, skill, skilli, skillin, super, superi As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from pmsysusers where islabor <> 0"
        dr = la.GetRdrData(sql)
        While dr.Read
            labor = dr.Item("username").ToString
            labori = dr.Item("userid").ToString
            skill = dr.Item("skill").ToString
            skilli = dr.Item("skillid").ToString
            skillin = dr.Item("skillindex").ToString
            super = dr.Item("super").ToString
            superi = dr.Item("superid").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getlabor('" & labor & "', '" & labori & "', '" & skill & "', '" & skilli & "', '" & skillin & "', '" & super & "', '" & superi & "');"">" & labor & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & skill & "</td>")
            sb.Append("<td class=""plainlabel"">" & super & "</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        dvlabor.InnerHtml = sb.ToString
    End Sub
End Class
