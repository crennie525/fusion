﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="sysprofilesmain.aspx.vb" Inherits="lucy_r12.sysprofilesmain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link href="../styles/reports.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function editprof(spid) {
            var admin = document.getElementById("lbladmin").value;
            var cadm = document.getElementById("lblcadm").value;
            var eReturn = window.showModalDialog("sysprofileseditdialog.aspx?spid=" + spid + "&admin=" + admin + "&cadm=" + cadm + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblret").value = "retsp";
                document.getElementById("form1").submit();
            }
        }
        function addprof() {
            var admin = document.getElementById("lbladmin").value;
            var cadm = document.getElementById("lblcadm").value;
            var eReturn = window.showModalDialog("sysprofilesdialog.aspx?admin=" + admin + "&cadm=" + cadm + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblret").value = "retsp";
                document.getElementById("form1").submit();
            }
        }
        function getprof(spid, spname) {
            window.parent.handlereturn(spid, spname);
        }
        function getapps(spid) {
            var eReturn = window.showModalDialog("sysapps.aspx?spid=" + spid + "&date=" + Date(), "", "dialogHeight:330px; dialogWidth:350px; resizable=yes");
        }
        function remsp(spid) {
            var decision = confirm("Are you sure you want to Remove this Security Profile?")
            if (decision == true) {
                document.getElementById("lblspid").value = spid;
                document.getElementById("lblret").value = "remsp";
                document.getElementById("form1").submit();
            }
            else {
                alert("Action Cancelled")
            }
        }
    function getnext() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg < cnt) {
            document.getElementById("lblret").value = "next"
            document.getElementById("form1").submit();
        }
    }
    function getlast() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg < cnt) {
            document.getElementById("lblret").value = "last"
            document.getElementById("form1").submit();
        }
    }
    function getprev() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg > 1) {
            document.getElementById("lblret").value = "prev"
            document.getElementById("form1").submit();
        }
    }
    function getfirst() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg > 1) {
            document.getElementById("lblret").value = "first"
            document.getElementById("form1").submit();
        }
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td>
    <table>
    <tr>
    <td class="bluelabel">Add a New Profile</td>
    <td><img alt="" src="../images/appbuttons/minibuttons/addmod.gif" onclick="addprof();" /></td>
    </tr>
    </table>
    </td>
    
    </tr>
    <tr>
    <td id="tdprof" runat="server"></td>
    </tr>
    <tr>
					<td align="center">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
    </table>
    </div>
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lbladmin" runat="server" />
    <input type="hidden" id="lblspid" runat="server" />
    </form>
</body>
</html>
