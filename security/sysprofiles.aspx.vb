﻿Imports System.Data.SqlClient
Public Class sysprofiles
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim ua, uid, uname, fld, inprg, ro, cadm, ap, aps, admin, spid As String
    Dim us As New Utilities
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            admin = Request.QueryString("admin").ToString
            cadm = Request.QueryString("cadm").ToString
            lbladmin.Value = admin
            lblcadm.Value = cadm
        Else
            If Request.Form("lblsubmit") = "savedets" Then
                lblsubmit.Value = ""
                us.Open()
                addprof()
                us.Dispose()

            End If
        End If
    End Sub
    Private Sub addprof()
        Dim prof As String = txtprof.Text
        prof = us.ModString2(prof)
        If Len(prof) > 50 Then
            Dim strMessage As String = "Profile Name Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim pcnt As Integer = 0
        sql = "select count(*) from sysprofiles where spname = '" & prof & "'"
        pcnt = us.Scalar(sql)
        If pcnt > 0 Then
            Dim strMessage As String = "This Profile Name Already Exists"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim skill As String = lblskill.Value
        Dim skillid = lblskillid.Value
        Dim shift As String
        If ddshifto.SelectedIndex = 0 Or ddshifto.SelectedIndex = -1 Then
            shift = "0"
        Else
            shift = ddshifto.SelectedValue
        End If
        Dim adm, lab, sup, pla As String
        If cbadmin.Checked = True Then
            adm = "1"
        Else
            adm = "0"
        End If
        If cblabor.Checked = True Then
            lab = "1"
        Else
            lab = "0"
        End If
        If cbsuper.Checked = True Then
            sup = "1"
        Else
            sup = "0"
        End If
        If cbplanner.Checked = True Then
            pla = "1"
        Else
            pla = "0"
        End If
        sql = "insert into sysprofiles (spname, admin, islabor, issuper, isplanner, skill, skillid, shift) " _
            + "values ('" & prof & "','" & adm & "','" & lab & "','" & sup & "','" & pla & "','" & skill & "','" & skillid & "','" & shift & "') select @@identity"
        Dim spi As Integer = us.Scalar(sql)
        spid = spi
        lblspid.Value = spid
        If skill = "" Then
            sql = "update sysprofiles set skillid = null, skill = null where spid = '" & spi & "'"
            us.Update(sql)
        End If
        PopSysList(spid)
        ua = lbladmin.Value
        PopPSList(ua)
        trsave.Attributes.Add("class", "details")
        tdapps.Attributes.Add("class", "view")
    End Sub
    Private Sub getappstr(ByVal spid As String)
        Dim appstr As String = ""
        Dim app As String = ""
        sql = "select u.app, p.appname from sysapps u left join pmApps p on p.app = u.app where u.spid = '" & spid & "'"
        dr = us.GetRdrData(sql)
        While dr.Read
            app = dr.Item("app").ToString
            If appstr = "" Then
                appstr = app
            Else
                appstr += "," & app
            End If
        End While
        dr.Close()
        lblappstr.Value = appstr
        If appstr <> "" Then
            sql = "update sysprofiles set appstr = '" & appstr & "' where spid = '" & spid & "'"
            us.Update(sql)
        End If
    End Sub
    Private Sub PopSysList(ByVal spid As String)
        sql = "select u.app, p.appname from sysapps u left join pmApps p on p.app = u.app where u.spid = '" & spid & "'"
        dr = us.GetRdrData(sql)
        lbuserps.DataSource = dr
        lbuserps.DataValueField = "app"
        lbuserps.DataTextField = "appname"
        lbuserps.DataBind()
        dr.Close()
        Try
            lbuserps.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Private Sub PopPSList(ByVal ua As String)
        Dim dt, val As String
        dt = "pmUserApps"
        val = "app, appname"
        spid = lblspid.Value
        cadm = lblcadm.Value
        If ua = "pmadmin" OrElse ua = "pmadmin1" Or cadm = "1" Then
            sql = "select app, appname from pmApps " _
                    + "where app not in (" _
                    + "select app from sysapps where spid = '" & spid & "') and app <> 'ua' and app <> 'uap' "

        Else
            sql = "select u.app, p.appname from pmUserApps u left join pmApps p on p.app = u.app where u.uid = '" & ua & "' " _
              + "and u.app not in (" _
              + "select app from sysapps where spid = '" & spid & "')"

        End If
        dr = us.GetRdrData(sql)
        lbps.DataSource = dr
        lbps.DataTextField = "appname"
        lbps.DataValueField = "app"
        lbps.DataBind()
        dr.Close()
    End Sub

    Protected Sub btntouser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntouser.Click
        ua = lbladmin.Value
        spid = lblspid.Value
        Dim Item As ListItem
        Dim s, si, appstr As String
        appstr = lblappstr.Value
        us.Open()
        For Each Item In lbps.Items
            If Item.Selected Then
                s = Item.ToString
                si = Item.Value.ToString
                If appstr = "" Then
                    appstr = si
                Else
                    appstr += "," & si
                End If
                GetItems(si, s)
            End If
        Next
        'If appstr <> "" Then
        'sql = "update sysprofiles set appstr = '" & appstr & "' where spid = '" & spid & "'"
        'us.Update(sql)
        'End If
        getappstr(spid)
        PopSysList(spid)
        PopPSList(ua)
        us.Dispose()

    End Sub
    Private Sub GetItems(ByVal app As String, ByVal appname As String)
        spid = lblspid.Value

        Dim scnt As Integer
        sql = "select count(*) from sysapps where app = '" & app & "' " _
        + "and spid = '" & spid & "'"
        scnt = us.Scalar(sql)
        If scnt = 0 Then
            sql = "insert into sysapps (spid, app) values ('" & spid & "', '" & app & "')"
                us.Update(sql)
        End If
    End Sub
    Protected Sub btnfromuser_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromuser.Click
       spid = lblspid.Value
        ua = lbladmin.Value
        Dim Item As ListItem
        Dim s, si As String
        us.Open()
        For Each Item In lbuserps.Items
            If Item.Selected Then
                s = Item.ToString
                si = Item.Value.ToString
                RemItems(si, s)
            End If
        Next
        getappstr(spid)
        PopSysList(spid)
        PopPSList(ua)
        us.Dispose()
    End Sub
    Private Sub RemItems(ByVal app As String, ByVal appname As String)
        spid = lblspid.Value
        Dim aps, appstr As String
        Dim i As Integer
        sql = "delete from sysapps where app = '" & app & "' and spid = '" & spid & "'"
        us.Update(sql)
        aps = lblappstr.Value
        'lblappstr.Value = ""
        'Dim apsarr() As String = aps.Split(",")
        'For i = 0 To apsarr.Length - 1
        'If apsarr(i) <> app Then
        'If appstr = "" Then
        'appstr = apsarr(i)
        'Else
        'appstr += "," & apsarr(i)
        'End If
        'End If
        'Next
        'lblappstr.Value = appstr
        'If appstr <> "" Then
        'sql = "update sysprofiles set appstr = '" & appstr & "' where spid = '" & spid & "'"
        'us.Update(sql)
        'End If
    End Sub
End Class