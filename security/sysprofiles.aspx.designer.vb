﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class sysprofiles

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''txtprof control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtprof As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lang3546 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang3546 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddshifto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddshifto As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lang3547 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang3547 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtskillo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtskillo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cbadmin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbadmin As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cbplanner control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbplanner As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cbsuper control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbsuper As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cblabor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cblabor As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''trsave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trsave As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''tdapps control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdapps As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lang3563 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang3563 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lang3564 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lang3564 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbps control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbps As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''btntouser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btntouser As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''btnfromuser control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnfromuser As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''lbuserps control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbuserps As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''lblcadm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcadm As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lbladmin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbladmin As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblspid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblspid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskill control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskill As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblskillid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblskillid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsubmit As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblapps control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblapps As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblappstr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblappstr As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
