

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Web.Mail

Public Class UserAdmin
    Inherits System.Web.UI.Page
	Protected WithEvents btnaddnewsites As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents btnaddnewfail As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang3519 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3518 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3517 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3516 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3515 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3514 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3513 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3512 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3511 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3510 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3509 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3508 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3507 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3506 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3505 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3504 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3503 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3502 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3501 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3500 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3499 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3498 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3497 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3496 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3495 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3494 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3493 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3492 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3491 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3490 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3489 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3488 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3487 As System.Web.UI.WebControls.Label

	Protected WithEvents lang3486 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim adm As New Utilities
    Dim cid, ps, uid, sess, psite, admin As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Dim Login As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgusers As System.Web.UI.WebControls.Repeater
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtusername As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmitprofile As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rbgroup As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbactive As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents rbexpires As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents txtp1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtp2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnsubmitpass As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbllic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusers As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnew As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnusercr As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdpwd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents rblms As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents ddps As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdapps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents addapp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtnewuid As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnaddlogin As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdlic As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdusers As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtp1new As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtp2new As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtexpd As System.Web.UI.WebControls.TextBox
    Protected WithEvents rbexpiresnew As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents txtexpdnew As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtuserid As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtusernamenew As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphonenew As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemailnew As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddpsnew As System.Web.UI.WebControls.DropDownList
    Protected WithEvents addappnew As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnaddnewlogin As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblnewcheck As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladmin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents newuappsdiv As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblipg As System.Web.UI.WebControls.Label
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img7 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img8 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbro As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbroa As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            Try
                sess = HttpContext.Current.Session("Logged_IN").ToString()
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try
            cid = HttpContext.Current.Session("comp").ToString
            admin = HttpContext.Current.Session("uid").ToString
            lblcid.Value = cid
            lbladmin.Value = admin
            ps = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = ps
            adm.Open()
            LoadUsers(PageNumber, cid)
            GetSites(cid)
            GetStats(cid, ps)
            adm.Dispose()
        Else
            If Request.Form("lblpchk").ToString = "apps" Then
                Dim ui As String = txtuserid.Text
                lblpchk.Value = ""
                GetApps(ui)
            ElseIf Request.Form("lblpchk").ToString = "apps" Then
                Dim ui As String = txtnewuid.Text
                lblpchk.Value = ""
                GetApps(ui, "new")
            End If
            If Request.Form("lblnewcheck") = "ok" Then
                lblnewcheck.Value = ""
                addUser()
            End If
            If Request.Form("lblret") = "inext" Then
                adm.Open()
                GetiNext()
                adm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ilast" Then
                adm.Open()
                PageNumber = txtipgcnt.Value
                txtipg.Value = PageNumber
                LoadUsers(PageNumber, "0")
                adm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "iprev" Then
                adm.Open()
                GetiPrev()
                adm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ifirst" Then
                adm.Open()
                PageNumber = 1
                txtipg.Value = PageNumber
                LoadUsers(PageNumber, "0")
                adm.Dispose()
                lblret.Value = ""
            End If
        End If
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("UserAdmin.aspx")
        Catch ex As Exception

        End Try
        'addapp.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'addapp.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'btnnext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'btnnext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
        'btnprev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'btnprev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'btnsubmitpass.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btnsubmitpass.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
        'btnusercr.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btnusercr.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
        'btnaddnewlogin.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btnaddnewlogin.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")
        'addappnew.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'addappnew.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'btnsubmitprofile.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/submithov.gif'")
        'btnsubmitprofile.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/submit.gif'")

    End Sub

    Private Sub SendIt(ByVal typ As String, ByVal ema As String, ByVal uid As String)
        Dim email As New System.Web.Mail.MailMessage

        Dim lid, pas As String
        lid = txtnewuid.Text
        pas = txtp2new.Text

        email.To = ema
        email.From = "system_admin@laisoftware.com"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 1
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "mail.laisoftware.com"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = "system_admin"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "sysadm1"
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
        email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = 25

        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If

        email.Body = AlertBody(lid, pas)
        email.Subject = "Your New PM Login ID"
        email.BodyFormat = Web.Mail.MailFormat.Html
        System.Web.Mail.SmtpMail.SmtpServer.Insert(0, "mail.laisoftware.com")
        System.Web.Mail.SmtpMail.Send(email)
    End Sub
    Private Function AlertBody(ByVal lid As String, ByVal pas As String) As String
        Dim body As String
        body = "<table width='800px' style='font-size:8pt; font-family:Verdana;'>"

        body &= "<tr><td>" & tmod.getlbl("cdlbl1254" , "UserAdmin.aspx.vb") & "<br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Your New User ID is      " & lid & "<br></td></tr>" & vbCrLf
        body &= "<tr><td>And Your New Password is " & pas & "<br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & tmod.getlbl("cdlbl1255", "UserAdmin.aspx.vb") & "<a href='http://www.laisoftware.com'></td></tr>" & vbCrLf & vbCrLf
        body &= "</table>"

        Return body
    End Function
    Private Sub GetSites(ByVal cid As String)
        Dim scnt As Integer
        admin = lbladmin.Value
        If admin = "pmadmin" Or admin = "pmadmin1" Then
            sql = "select count(*) from sites where compid = '" & cid & "'"
        Else
            sql = "select count(*) from pmUserSites where uid = '" & admin & "'"
        End If

        scnt = adm.Scalar(sql)
        If scnt > 0 Then
            If admin = "pmadmin" Or admin = "pmadmin1" Then
                sql = "select siteid, sitename from sites where compid = '" & cid & "'"
            Else
                sql = "select u.siteid, s.sitename from pmUserSites u " _
                + "left join sites s on s.siteid = u.siteid where u.uid = '" & admin & "'"
            End If
            dr = adm.GetRdrData(sql)
            ddps.DataSource = dr
            ddps.DataValueField = "siteid"
            ddps.DataTextField = "sitename"
            ddps.DataBind()
            dr.Close()
            ddps.Items.Insert(0, "Select Plant Site")
            ddps.Enabled = True
            dr = adm.GetRdrData(sql)
            ddpsnew.DataSource = dr
            ddpsnew.DataValueField = "siteid"
            ddpsnew.DataTextField = "sitename"
            ddpsnew.DataBind()
            dr.Close()
            ddpsnew.Items.Insert(0, "Select Plant Site")
            ddpsnew.Enabled = True
        Else
            ddps.Items.Insert(0, "Select Plant Site")
            ddps.Enabled = False
            ddpsnew.Items.Insert(0, "Select Plant Site")
            ddpsnew.Enabled = True
        End If

    End Sub
    Private Sub LoadUsers(ByVal PageNumber As Integer, ByVal cid As String) ', ByVal ps As String
        Dim intPgCnt As Integer
        sql = "select count(*) from pmsysusers where compid = '" & cid & "' and admin <> 1" ' and dfltps = '" & ps & "'"

        'intPgCnt = adm.Scalar(sql)
        txtpg.Value = PageNumber
        'intPgNav = adm.PageCount(intPgCnt, PageSize)

        intPgNav = adm.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblipg.Text = "Page 0 of 0"
        Else
            lblipg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtipgcnt.Value = intPgNav


        admin = lbladmin.Value
        If admin = "pmadmin" Or admin = "pmadmin1" Then
            sql = "usp_getAllUserscadm '" & cid & "', '" & PageNumber & "', '" & PageSize & "'"
        Else
            sql = "usp_getAllUserspsadm '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & admin & "'"
        End If
        dr = adm.GetRdrData(sql)
        dgusers.DataSource = dr
        dgusers.DataBind()
        dr.Close()
        updatepos(intPgNav)
    End Sub
    Private Sub GetiNext()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg + 1
            txtipg.Value = PageNumber
            LoadUsers(PageNumber, "0")
        Catch ex As Exception
            adm.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1670" , "UserAdmin.aspx.vb")
 
            adm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetiPrev()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg - 1
            txtipg.Value = PageNumber
            LoadUsers(PageNumber, "0")
        Catch ex As Exception
            adm.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr1671" , "UserAdmin.aspx.vb")
 
            adm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub updatepos(ByVal intPgNav)
        PageNumber = txtpg.Value
        If intPgNav = 0 Then
            tdpg.InnerHtml = "Page 0 of 0"
        Else
            tdpg.InnerHtml = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber = 1 And intPgNav = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = False

        ElseIf PageNumber = 1 And intPgNav > 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = True

        ElseIf PageNumber = intPgNav Then
            btnnext.Enabled = False
            btnprev.Enabled = True

        Else
            btnnext.Enabled = True
            btnprev.Enabled = True
        End If
    End Sub

    Public Sub GetUser(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rb As RadioButton = New RadioButton
        Dim uid, user, phone, email, grp, status, exp, expd, dps, ms, ro As String
        rb = sender
        txtuserid.Text = rb.ClientID
        For Each i As RepeaterItem In dgusers.Items
            If i.ItemType <> ListItemType.AlternatingItem Then
                rb = CType(i.FindControl("rbuid"), RadioButton)
                user = CType(i.FindControl("lblusername"), Label).Text
                phone = CType(i.FindControl("lblphone"), Label).Text
                email = CType(i.FindControl("lblemail"), Label).Text
                grp = CType(i.FindControl("lblindex"), Label).Text
                status = CType(i.FindControl("lblstatus"), Label).Text
                exp = CType(i.FindControl("lblexp"), Label).Text
                expd = CType(i.FindControl("lblexpd"), Label).Text
                dps = CType(i.FindControl("lbldps"), Label).Text
                ps = CType(i.FindControl("lblps"), Label).Text
                ms = CType(i.FindControl("lblms"), Label).Text
                ro = CType(i.FindControl("lblro"), Label).Text
                rb.Checked = False
            Else
                rb = CType(i.FindControl("rbuida"), RadioButton)
                user = CType(i.FindControl("lblusernamea"), Label).Text
                phone = CType(i.FindControl("lblphonea"), Label).Text
                email = CType(i.FindControl("lblemaila"), Label).Text
                grp = CType(i.FindControl("lblindexa"), Label).Text
                status = CType(i.FindControl("lblstatusa"), Label).Text
                exp = CType(i.FindControl("lblexpa"), Label).Text
                expd = CType(i.FindControl("lblexpda"), Label).Text
                dps = CType(i.FindControl("lbldpsa"), Label).Text
                ps = CType(i.FindControl("lblpsa"), Label).Text
                ms = CType(i.FindControl("lblmsa"), Label).Text
                ro = CType(i.FindControl("lblroa"), Label).Text
                rb.Checked = False
            End If

            If txtuserid.Text = rb.ClientID Then
                rb.Checked = True
                txtuserid.Text = rb.Text.Trim
                uid = txtuserid.Text
                lbluserid.Value = rb.Text.Trim
                txtusername.Text = user
                txtphone.Text = phone
                txtemail.Text = email
                rbactive.SelectedValue = status
                rbexpires.SelectedValue = exp
                txtexpd.Text = expd
                If ps = "0" Then
                    tdpwd.InnerHtml = "No Password Created For This User"
                Else
                    tdpwd.InnerHtml = ""
                End If
                If ro = "1" Then
                    cbro.Checked = True
                End If
                GetApps(uid)
                ddps.SelectedValue = dps
            End If
        Next
    End Sub
    Private Sub GetApps(ByVal uid As String, Optional ByVal fld As String = "cur")
        Dim sb As New System.Text.StringBuilder
        sql = "select u.app, p.appname from pmUserApps u left join pmApps p on " _
                        + "p.app = u.app where u.uid = '" & uid & "' "
        adm.Open()
        dr = adm.GetRdrData(sql)
        While dr.Read
            sb.Append(dr.Item("appname").ToString() & "<br>")
        End While
        dr.Close()
        adm.Dispose()
        If fld = "cur" Then
            tdapps.InnerHtml = sb.ToString
        ElseIf fld = "new" Then
            newuappsdiv.InnerHtml = sb.ToString
        End If

    End Sub
    Private Sub GetStats(ByVal cid As String, ByVal ps As String)
        Dim uc As Integer = adm.Users(cid) '5
        tdlic.InnerHtml = uc
        lbllic.Value = uc
        Dim au As Integer
        sql = "select count(*) from pmsysusers where compid = '" & cid & "' and status = 'active' and userid <> 0" 'and dfltps = '" & ps & "'
        au = adm.Scalar(sql)
        tdusers.InnerHtml = au
        lblusers.Value = au

        If au < uc Then
            lblnew.Value = "ok"
            'Dim strMessage As String = lblnew.Value
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            lblnew.Value = "no"
        End If
    End Sub

    Private Sub btnsubmitprofile_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsubmitprofile.Click
        Dim name, phone, email, uid, ms, ro, ouid As String
        name = txtusername.Text
        Dim ustr As String = Replace(name, "'", Chr(180), , , vbTextCompare)
        If Len(ustr) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1672" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        phone = txtphone.Text
        phone = Replace(phone, "'", Chr(180), , , vbTextCompare)
        If Len(phone) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1673" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        email = txtemail.Text
        email = Replace(email, "'", "''")
        uid = txtuserid.Text
        uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
        If Len(uid) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1674" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ouid = lbluserid.Value
        cid = lblcid.Value
        ps = ddps.SelectedValue
        If cbro.Checked = True Then
            ro = "1"
        Else
            ro = "0"
        End If
        adm.Open()
        If ddps.SelectedIndex <> 0 Then
            Dim ui As String = lbluserid.Value
            Dim uicnt As Integer
            sql = "select count(*) from pmsysusers where uid = '" & uid & "'"
            uicnt = adm.Scalar(sql)
            If uicnt = 0 Or uid = ui Then
                sql = "update pmsysusers set username = '" & ustr & "', " _
                + " phonenum = '" & phone & "',  email = '" & email & "', uid ='" & uid & "', " _
                + "dfltps = '" & ps & "', multisite = '1', readonly = '" & ro & "' where uid = '" & ui & "'; " _
                + "delete from pmUserApps where app = 'uap' or app = 'ua' and uid = '" & uid & "';"
                adm.Update(sql)
                If uid <> ouid Then
                    sql = "update pmUserApps set uid = '" & uid & "' where uid = '" & ouid & "'; " _
                    + "update pmUserSites set uid = '" & uid & "' where uid = '" & ouid & "'"
                    adm.Update(sql)
                End If
                lbluserid.Value = uid
                LoadUsers(PageNumber, cid)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1675" , "UserAdmin.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1676" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        LoadUsers(PageNumber, cid)
        GetSites(cid)
        GetStats(cid, ps)
        adm.Dispose()
    End Sub

    Private Sub btnsubmitpass_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsubmitpass.Click
        Dim ui, uid, p1, p2 As String
        uid = lbluserid.Value
        cid = lblcid.Value
        ps = lblsid.Value
        p1 = txtp1.Text
        p2 = txtp2.Text
        adm.Open()
        If Len(p1) = 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1677" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf Len(p2) = 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1678" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf p1 <> p2 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1679" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            p2 = adm.Encrypt(p2)
            sql = "update PMSysUsers set passwrd = '" & p2 & "' where uid = '" & uid & "'"
            adm.Update(sql)
            tdpwd.InnerHtml = ""
            Dim strMessage As String =  tmod.getmsg("cdstr1680" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

        End If

    End Sub
    Private Sub UpUser(ByVal uid As String)
        'uid = lbluid.Value
        sql = "select u.app, p.appname from pmUserApps u left join pmApps p on p.app = u.app where u.uid = '" & uid & "' "
        Dim Item As ListItem
        Dim s, si, appstr As String
        'adm.Open()
        dr = adm.GetRdrData(sql)
        While dr.Read
            If appstr = "" Then
                appstr = dr.Item("app").ToString
            Else
                appstr += "," & dr.Item("app").ToString
            End If
        End While
        dr.Close()
        sql = "update pmsysusers set appstr = '" & appstr & "' where uid = '" & uid & "'"
        adm.Update(sql)
        'us.Dispose()
    End Sub
    Private Sub addUser()
        Dim uc, au As String
        uc = lbllic.Value
        au = lblusers.Value
        cid = lblcid.Value
        Dim sname, pass, exp, pho, ema, dps, ro As String
        sname = txtusernamenew.Text
        sname = Replace(sname, "'", Chr(180), , , vbTextCompare)
        If Len(sname) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1681" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pass = txtp2new.Text
        pass = adm.Encrypt(pass)
        pho = txtphonenew.Text
        If Len(pho) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1682" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ema = txtemailnew.Text
        ema = Replace(ema, "'", "''")
        dps = ddpsnew.SelectedValue.ToString
        dps = Replace(dps, "'", Chr(180), , , vbTextCompare)
        uid = txtnewuid.Text
        uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
        If Len(uid) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1683" , "UserAdmin.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim uidcnt As Integer
        adm.Open()
        sql = "select count(*) from PMSysUsers where uid = '" & uid & "' or username = '" & sname & "'"
        uidcnt = adm.Scalar(sql)
        Dim chk As String = tdusers.InnerHtml
        If cbroa.Checked = True Then
            ro = "1"
        Else
            ro = "0"
        End If
        If lblnew.Value = "ok" Then
            If uidcnt = 0 Then
                sql = "insert into PMSysUsers (email, phonenum, passwrd, username, compid, uid, dfltps, groupindex, groupname, status, readonly) values " _
                + "('" & ema & "', '" & pho & "', '" & pass & "', '" & sname & "', '" & cid & "', '" & uid & "', '" & dps & "', '6', 'pmnomanuser', 'active', '" & ro & "'); " '_
                '+ "delete from pmUserApps where app = 'uap' or app = 'ua' and uid = '" & uid & "'"
                adm.Update(sql)
                sql = "insert into pmusersites (uid, siteid) values ('" & uid & "', '" & dps & "')"
                adm.Update(sql)
                lbluserid.Value = ""
                UpUser(uid)
                LoadUsers(PageNumber, cid)
                GetStats(cid, ps)
                SendIt("new", ema, uid)
                txtuserid.Text = ""
                txtnewuid.Text = ""
                lblnewuid.Value = ""
                txtusernamenew.Text = ""
                txtphonenew.Text = ""
                txtemailnew.Text = ""
                rbexpiresnew.SelectedValue = "N"
                txtexpdnew.Text = ""
                newuappsdiv.InnerHtml = "&nbsp;"
                ddpsnew.SelectedIndex = 0

            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1684" , "UserAdmin.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            If rbexpiresnew.SelectedValue.ToString = "Y" Then
                exp = txtexpdnew.Text
                If Len(exp) <> 0 Then
                    sql = "update PMSysUsers set exp = '" & exp & "' where uid = '" & uid & "'"
                    adm.Update(sql)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr1685" , "UserAdmin.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If

        End If

        LoadUsers(PageNumber, cid)
        GetSites(cid)
        GetStats(cid, ps)
        adm.Dispose()
    End Sub

    Private Sub btnusercr_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnusercr.Click
        Dim exp As String = txtexpd.Text
        Dim exps As String = rbexpires.SelectedValue.ToString
        Dim act As String = rbactive.SelectedValue.ToString
        cid = lblcid.Value
        Dim uc As Integer = adm.Users(cid)
        cid = lblcid.Value
        ps = lblsid.Value
        uid = txtuserid.Text
        sql = "update pmsysusers set status = '" & act & "', " _
        + "expires = '" & exps & "', expiration = '" & exp & "' where uid = '" & uid & "'"
        adm.Open()
        If act = "active" Then
            If lblnew.Value = "ok" Then
                adm.Update(sql)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1686" , "UserAdmin.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                rbactive.SelectedValue = "inactive"
            End If
        Else
            adm.Update(sql)
        End If
        GetStats(cid, ps)
        adm.Dispose()

    End Sub

    Private Sub dgusers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles dgusers.ItemDataBound
        uid = lbluserid.Value
        Dim user, phone, email, grp, status, exp, expd, dps
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            If e.Item.ItemType <> ListItemType.AlternatingItem Then
                Dim rb As RadioButton = CType(e.Item.FindControl("rbuid"), RadioButton)
                If rb.Text.Trim = uid Then
                    rb.Checked = True
                    user = CType(e.Item.FindControl("lblusername"), Label).Text
                    phone = CType(e.Item.FindControl("lblphone"), Label).Text
                    email = CType(e.Item.FindControl("lblemail"), Label).Text
                    grp = CType(e.Item.FindControl("lblindex"), Label).Text
                    status = CType(e.Item.FindControl("lblstatus"), Label).Text
                    exp = CType(e.Item.FindControl("lblexp"), Label).Text
                    expd = CType(e.Item.FindControl("lblexpd"), Label).Text
                    dps = CType(e.Item.FindControl("lbldps"), Label).Text
                    txtuserid.Text = rb.Text.Trim
                    lbluserid.Value = rb.Text.Trim
                    txtusername.Text = user
                    txtphone.Text = phone
                    txtemail.Text = email
                    rbactive.SelectedValue = status
                    rbexpires.SelectedValue = exp
                    txtexpd.Text = expd
                End If
            Else
                Dim rb As RadioButton = CType(e.Item.FindControl("rbuida"), RadioButton)
                If rb.Text.Trim = uid Then
                    rb.Checked = True
                    user = CType(e.Item.FindControl("lblusernamea"), Label).Text
                    phone = CType(e.Item.FindControl("lblphonea"), Label).Text
                    email = CType(e.Item.FindControl("lblemaila"), Label).Text
                    grp = CType(e.Item.FindControl("lblindexa"), Label).Text
                    status = CType(e.Item.FindControl("lblstatusa"), Label).Text
                    exp = CType(e.Item.FindControl("lblexpa"), Label).Text
                    expd = CType(e.Item.FindControl("lblexpda"), Label).Text
                    dps = CType(e.Item.FindControl("lbldpsa"), Label).Text
                    txtuserid.Text = rb.Text.Trim
                    lbluserid.Value = rb.Text.Trim
                    txtusername.Text = user
                    txtphone.Text = phone
                    txtemail.Text = email
                    rbactive.SelectedValue = status
                    rbexpires.SelectedValue = exp
                    txtexpd.Text = expd
                End If
            End If
        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang3486 As Label
			lang3486 = CType(e.Item.FindControl("lang3486"), Label)
			lang3486.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3486")
		Catch ex As Exception
		End Try
		Try
                Dim lang3487 As Label
			lang3487 = CType(e.Item.FindControl("lang3487"), Label)
			lang3487.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3487")
		Catch ex As Exception
		End Try
		Try
                Dim lang3488 As Label
			lang3488 = CType(e.Item.FindControl("lang3488"), Label)
			lang3488.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3488")
		Catch ex As Exception
		End Try
		Try
                Dim lang3489 As Label
			lang3489 = CType(e.Item.FindControl("lang3489"), Label)
			lang3489.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3489")
		Catch ex As Exception
		End Try
		Try
                Dim lang3490 As Label
			lang3490 = CType(e.Item.FindControl("lang3490"), Label)
			lang3490.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3490")
		Catch ex As Exception
		End Try
		Try
                Dim lang3491 As Label
			lang3491 = CType(e.Item.FindControl("lang3491"), Label)
			lang3491.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3491")
		Catch ex As Exception
		End Try
		Try
                Dim lang3492 As Label
			lang3492 = CType(e.Item.FindControl("lang3492"), Label)
			lang3492.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3492")
		Catch ex As Exception
		End Try
		Try
                Dim lang3493 As Label
			lang3493 = CType(e.Item.FindControl("lang3493"), Label)
			lang3493.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3493")
		Catch ex As Exception
		End Try
		Try
                Dim lang3494 As Label
			lang3494 = CType(e.Item.FindControl("lang3494"), Label)
			lang3494.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3494")
		Catch ex As Exception
		End Try
		Try
                Dim lang3495 As Label
			lang3495 = CType(e.Item.FindControl("lang3495"), Label)
			lang3495.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3495")
		Catch ex As Exception
		End Try
		Try
                Dim lang3496 As Label
			lang3496 = CType(e.Item.FindControl("lang3496"), Label)
			lang3496.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3496")
		Catch ex As Exception
		End Try
		Try
                Dim lang3497 As Label
			lang3497 = CType(e.Item.FindControl("lang3497"), Label)
			lang3497.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3497")
		Catch ex As Exception
		End Try
		Try
                Dim lang3498 As Label
			lang3498 = CType(e.Item.FindControl("lang3498"), Label)
			lang3498.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3498")
		Catch ex As Exception
		End Try
		Try
                Dim lang3499 As Label
			lang3499 = CType(e.Item.FindControl("lang3499"), Label)
			lang3499.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3499")
		Catch ex As Exception
		End Try
		Try
                Dim lang3500 As Label
			lang3500 = CType(e.Item.FindControl("lang3500"), Label)
			lang3500.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3500")
		Catch ex As Exception
		End Try
		Try
                Dim lang3501 As Label
			lang3501 = CType(e.Item.FindControl("lang3501"), Label)
			lang3501.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3501")
		Catch ex As Exception
		End Try
		Try
                Dim lang3502 As Label
			lang3502 = CType(e.Item.FindControl("lang3502"), Label)
			lang3502.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3502")
		Catch ex As Exception
		End Try
		Try
                Dim lang3503 As Label
			lang3503 = CType(e.Item.FindControl("lang3503"), Label)
			lang3503.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3503")
		Catch ex As Exception
		End Try
		Try
                Dim lang3504 As Label
			lang3504 = CType(e.Item.FindControl("lang3504"), Label)
			lang3504.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3504")
		Catch ex As Exception
		End Try
		Try
                Dim lang3505 As Label
			lang3505 = CType(e.Item.FindControl("lang3505"), Label)
			lang3505.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3505")
		Catch ex As Exception
		End Try
		Try
                Dim lang3506 As Label
			lang3506 = CType(e.Item.FindControl("lang3506"), Label)
			lang3506.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3506")
		Catch ex As Exception
		End Try
		Try
                Dim lang3507 As Label
			lang3507 = CType(e.Item.FindControl("lang3507"), Label)
			lang3507.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3507")
		Catch ex As Exception
		End Try
		Try
                Dim lang3508 As Label
			lang3508 = CType(e.Item.FindControl("lang3508"), Label)
			lang3508.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3508")
		Catch ex As Exception
		End Try
		Try
                Dim lang3509 As Label
			lang3509 = CType(e.Item.FindControl("lang3509"), Label)
			lang3509.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3509")
		Catch ex As Exception
		End Try
		Try
                Dim lang3510 As Label
			lang3510 = CType(e.Item.FindControl("lang3510"), Label)
			lang3510.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3510")
		Catch ex As Exception
		End Try
		Try
                Dim lang3511 As Label
			lang3511 = CType(e.Item.FindControl("lang3511"), Label)
			lang3511.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3511")
		Catch ex As Exception
		End Try
		Try
                Dim lang3512 As Label
			lang3512 = CType(e.Item.FindControl("lang3512"), Label)
			lang3512.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3512")
		Catch ex As Exception
		End Try
		Try
                Dim lang3513 As Label
			lang3513 = CType(e.Item.FindControl("lang3513"), Label)
			lang3513.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3513")
		Catch ex As Exception
		End Try
		Try
                Dim lang3514 As Label
			lang3514 = CType(e.Item.FindControl("lang3514"), Label)
			lang3514.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3514")
		Catch ex As Exception
		End Try
		Try
                Dim lang3515 As Label
			lang3515 = CType(e.Item.FindControl("lang3515"), Label)
			lang3515.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3515")
		Catch ex As Exception
		End Try
		Try
                Dim lang3516 As Label
			lang3516 = CType(e.Item.FindControl("lang3516"), Label)
			lang3516.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3516")
		Catch ex As Exception
		End Try
		Try
                Dim lang3517 As Label
			lang3517 = CType(e.Item.FindControl("lang3517"), Label)
			lang3517.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3517")
		Catch ex As Exception
		End Try
		Try
                Dim lang3518 As Label
			lang3518 = CType(e.Item.FindControl("lang3518"), Label)
			lang3518.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3518")
		Catch ex As Exception
		End Try
		Try
                Dim lang3519 As Label
			lang3519 = CType(e.Item.FindControl("lang3519"), Label)
			lang3519.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3519")
		Catch ex As Exception
		End Try

End If

 End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim pg As Integer = txtpg.Value
        ps = lblsid.Value
        uid = txtnewuid.Text
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        adm.Open()
        LoadUsers(PageNumber, cid)
        adm.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprev.Click
        Dim pg As Integer = txtpg.Value
        ps = lblsid.Value
        uid = txtnewuid.Text
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        adm.Open()
        LoadUsers(PageNumber, cid)
        adm.Dispose()
    End Sub

    Private Sub Imagebutton3_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim rb As String = rbgroup.SelectedValue.ToString
        uid = lbluserid.Value
        cid = lblcid.Value
        sql = "update pmsysusers set groupindex = '" & rb & "' where compid = '" & cid & "' and uid = '" & uid & "'"
        txtpg.Value = PageNumber
        adm.Open()
        adm.Update(sql)
        LoadUsers(PageNumber, cid)
        adm.Dispose()

    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang3486.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3486")
		Catch ex As Exception
		End Try
		Try
			lang3487.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3487")
		Catch ex As Exception
		End Try
		Try
			lang3488.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3488")
		Catch ex As Exception
		End Try
		Try
			lang3489.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3489")
		Catch ex As Exception
		End Try
		Try
			lang3490.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3490")
		Catch ex As Exception
		End Try
		Try
			lang3491.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3491")
		Catch ex As Exception
		End Try
		Try
			lang3492.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3492")
		Catch ex As Exception
		End Try
		Try
			lang3493.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3493")
		Catch ex As Exception
		End Try
		Try
			lang3494.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3494")
		Catch ex As Exception
		End Try
		Try
			lang3495.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3495")
		Catch ex As Exception
		End Try
		Try
			lang3496.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3496")
		Catch ex As Exception
		End Try
		Try
			lang3497.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3497")
		Catch ex As Exception
		End Try
		Try
			lang3498.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3498")
		Catch ex As Exception
		End Try
		Try
			lang3499.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3499")
		Catch ex As Exception
		End Try
		Try
			lang3500.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3500")
		Catch ex As Exception
		End Try
		Try
			lang3501.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3501")
		Catch ex As Exception
		End Try
		Try
			lang3502.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3502")
		Catch ex As Exception
		End Try
		Try
			lang3503.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3503")
		Catch ex As Exception
		End Try
		Try
			lang3504.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3504")
		Catch ex As Exception
		End Try
		Try
			lang3505.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3505")
		Catch ex As Exception
		End Try
		Try
			lang3506.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3506")
		Catch ex As Exception
		End Try
		Try
			lang3507.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3507")
		Catch ex As Exception
		End Try
		Try
			lang3508.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3508")
		Catch ex As Exception
		End Try
		Try
			lang3509.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3509")
		Catch ex As Exception
		End Try
		Try
			lang3510.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3510")
		Catch ex As Exception
		End Try
		Try
			lang3511.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3511")
		Catch ex As Exception
		End Try
		Try
			lang3512.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3512")
		Catch ex As Exception
		End Try
		Try
			lang3513.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3513")
		Catch ex As Exception
		End Try
		Try
			lang3514.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3514")
		Catch ex As Exception
		End Try
		Try
			lang3515.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3515")
		Catch ex As Exception
		End Try
		Try
			lang3516.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3516")
		Catch ex As Exception
		End Try
		Try
			lang3517.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3517")
		Catch ex As Exception
		End Try
		Try
			lang3518.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3518")
		Catch ex As Exception
		End Try
		Try
			lang3519.Text = axlabs.GetASPXPage("UserAdmin.aspx","lang3519")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			addapp.Attributes.Add("src" , "../images2/eng/bgbuttons/badd.gif")
			ElseIf lang = "fre" Then
			addapp.Attributes.Add("src" , "../images2/fre/bgbuttons/badd.gif")
			ElseIf lang = "ger" Then
			addapp.Attributes.Add("src" , "../images2/ger/bgbuttons/badd.gif")
			ElseIf lang = "ita" Then
			addapp.Attributes.Add("src" , "../images2/ita/bgbuttons/badd.gif")
			ElseIf lang = "spa" Then
			addapp.Attributes.Add("src" , "../images2/spa/bgbuttons/badd.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			addappnew.Attributes.Add("src" , "../images2/eng/bgbuttons/badd.gif")
			ElseIf lang = "fre" Then
			addappnew.Attributes.Add("src" , "../images2/fre/bgbuttons/badd.gif")
			ElseIf lang = "ger" Then
			addappnew.Attributes.Add("src" , "../images2/ger/bgbuttons/badd.gif")
			ElseIf lang = "ita" Then
			addappnew.Attributes.Add("src" , "../images2/ita/bgbuttons/badd.gif")
			ElseIf lang = "spa" Then
			addappnew.Attributes.Add("src" , "../images2/spa/bgbuttons/badd.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			btnaddnewlogin.Attributes.Add("src" , "../images2/eng/bgbuttons/submit.gif")
			ElseIf lang = "fre" Then
			btnaddnewlogin.Attributes.Add("src" , "../images2/fre/bgbuttons/submit.gif")
			ElseIf lang = "ger" Then
			btnaddnewlogin.Attributes.Add("src" , "../images2/ger/bgbuttons/submit.gif")
			ElseIf lang = "ita" Then
			btnaddnewlogin.Attributes.Add("src" , "../images2/ita/bgbuttons/submit.gif")
			ElseIf lang = "spa" Then
			btnaddnewlogin.Attributes.Add("src" , "../images2/spa/bgbuttons/submit.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			btnsubmitpass.Attributes.Add("src" , "../images2/eng/bgbuttons/submit.gif")
			ElseIf lang = "fre" Then
			btnsubmitpass.Attributes.Add("src" , "../images2/fre/bgbuttons/submit.gif")
			ElseIf lang = "ger" Then
			btnsubmitpass.Attributes.Add("src" , "../images2/ger/bgbuttons/submit.gif")
			ElseIf lang = "ita" Then
			btnsubmitpass.Attributes.Add("src" , "../images2/ita/bgbuttons/submit.gif")
			ElseIf lang = "spa" Then
			btnsubmitpass.Attributes.Add("src" , "../images2/spa/bgbuttons/submit.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			btnsubmitprofile.Attributes.Add("src" , "../images2/eng/bgbuttons/submit.gif")
			ElseIf lang = "fre" Then
			btnsubmitprofile.Attributes.Add("src" , "../images2/fre/bgbuttons/submit.gif")
			ElseIf lang = "ger" Then
			btnsubmitprofile.Attributes.Add("src" , "../images2/ger/bgbuttons/submit.gif")
			ElseIf lang = "ita" Then
			btnsubmitprofile.Attributes.Add("src" , "../images2/ita/bgbuttons/submit.gif")
			ElseIf lang = "spa" Then
			btnsubmitprofile.Attributes.Add("src" , "../images2/spa/bgbuttons/submit.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			btnusercr.Attributes.Add("src" , "../images2/eng/bgbuttons/submit.gif")
			ElseIf lang = "fre" Then
			btnusercr.Attributes.Add("src" , "../images2/fre/bgbuttons/submit.gif")
			ElseIf lang = "ger" Then
			btnusercr.Attributes.Add("src" , "../images2/ger/bgbuttons/submit.gif")
			ElseIf lang = "ita" Then
			btnusercr.Attributes.Add("src" , "../images2/ita/bgbuttons/submit.gif")
			ElseIf lang = "spa" Then
			btnusercr.Attributes.Add("src" , "../images2/spa/bgbuttons/submit.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetFSOVLIBS()
		Dim axovlib as New aspxovlib
		Try
			btnaddnewfail.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("UserAdmin.aspx","btnaddnewfail") & "')")
			btnaddnewfail.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try
		Try
			btnaddnewsites.Attributes.Add("onmouseover" , "return overlib('" & axovlib.GetASPXOVLIB("UserAdmin.aspx","btnaddnewsites") & "')")
			btnaddnewsites.Attributes.Add("onmouseout" , "return nd()")
		Catch ex As Exception
		End Try

	End Sub

End Class
