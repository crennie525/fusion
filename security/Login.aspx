<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="lucy_r12.Login" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Login</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<script language="JavaScript" src="../scripts1/Loginaspx_1.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="showFocus();chk();" >
		<form id="form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="320">
				<tr>
					<td width="100"></td>
					<td width="120"></td>
					<td width="100"></td>
				</tr>
				<tr>
					<td class="label"><asp:label id="lblname" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">User Name</asp:label></td>
					<td><asp:textbox id="txtuid" runat="server" Font-Size="X-Small" Font-Names="Arial" Width="120px"></asp:textbox></td>
					<td></td>
				</tr>
				<tr>
					<td class="label"><asp:label id="Label1" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Password</asp:label></td>
					<td><asp:textbox id="txtpass" runat="server" Font-Size="X-Small" Font-Names="Arial" Width="120px"
							TextMode="Password"></asp:textbox></td>
					<td align="center"><asp:imagebutton id="btnlogin" runat="server" ImageUrl="../images/appbuttons/bgbuttons/blogin.gif"></asp:imagebutton></td>
				</tr>
			</table>
			<input id="lblchk" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
