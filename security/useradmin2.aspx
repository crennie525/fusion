<%@ Page Language="vb" AutoEventWireup="false" Codebehind="useradmin2.aspx.vb" Inherits="lucy_r12.useradmin2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>useradmin2</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="../scripts/gridnavopt_1.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
		<link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
		<script language="JavaScript" src="../scripts1/useradmin2aspx_1.js"></script>
		<script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script language="javascript" type="text/javascript">
            function getskillOld() {
                var uid = document.getElementById("txtuserid").value;
                if (uid != "") {
                    var eReturn = window.showModalDialog("lskilldialog.aspx", getskillOldCallback, "dialogHeight:260px; dialogWidth:260px; resizable=yes");
                    if (eReturn) {
                        getskillOldCallback(eReturn);
                    }
                } else {
                    alert("No user selected.");
                }
            }

            function getskillOldCallback(eReturn) {
                var retarr = eReturn.split(",");
                document.getElementById("lblskillindexr").value = retarr[2];
                document.getElementById("lblskillidr").value = retarr[1];
                document.getElementById("txtskillo").value = retarr[0];
            }

            function getskillNew() {
                var uid = document.getElementById("txtnewuid").value;
                if (uid != "") {
                    var eReturn = window.showModalDialog("lskilldialog.aspx", getskillNewCallback, "dialogHeight:260px; dialogWidth:260px; resizable=yes");
                    if (eReturn) {
                        getskillNewCallback(eReturn);
                    }
                } else {
                    alert("No user selected.");
                }
            }

            function getskillNewCallback(eReturn) {
                var retarr = eReturn.split(",");
                document.getElementById("lblskillindexrn").value = retarr[2];
                document.getElementById("lblskillidrn").value = retarr[1];
                document.getElementById("txtskilln").value = retarr[0];
            }

            function valnew2() {
                if (document.getElementById("txtnewuid").value == "") {
                    alert("New User ID has not been entered!")
                }
                else if (document.getElementById("txtp1new").value == "") {
                    alert("New User Password has not been entered!")
                }
                else if (document.getElementById("txtp2new").value == "") {
                    alert("New User Password has not been verified!")
                }
                else if (document.getElementById("txtp2new").value != document.getElementById("txtp1new").value) {
                    alert("Passwords do not match!")
                }
                else if (document.getElementById("txtemailnew").value == "") {
                    alert("New User Email Address has not been entered!")
                }
                else if (document.getElementById("ddpsnew").value == "Select Plant Site") {
                    alert("New User Default Plant Site has not been selected!")
                }
               
                else {
                    document.getElementById("lblnewcheck").value = "ok1"
                    document.getElementById("form1").submit();
                }
            }
            function getbulk() {
                var admin = document.getElementById("lbladmin").value;
                var cadm = document.getElementById("lblcadm").value;
                //"sysprofilesmain.aspx?admin=" + admin + "&cadm=" + cadm + "&date=" +
                var eReturn = window.showModalDialog("sysbulkdialog.aspx?admin=" + admin + "&cadm=" + cadm + "&date=" + Date(), "", "dialogHeight:510px; dialogWidth:1020px; resizable=yes");
                if (eReturn) {
                    //alert(eReturn)
                    document.getElementById("lblret").value = "ifirst"
                    document.getElementById("form1").submit();
                }
            }
            function getprof() {
                var admin = document.getElementById("lbladmin").value;
                var cadm = document.getElementById("lblcadm").value;
                var eReturn = window.showModalDialog("sysprofilesmaindialog.aspx?admin=" + admin + "&cadm=" + cadm + "&date=" + Date(), getprofCallback, "dialogHeight:510px; dialogWidth:1020px; resizable=yes");
                if (eReturn) {
                    getprofCallback(eReturn);
                }
            }

            function getprofCallback(eReturn) {
                var ret = eReturn.split(",");
                document.getElementById("lblspid").value = ret[0];
                document.getElementById("lblspname").value = ret[1];
                document.getElementById("lblprofe").innerHTML = ret[1];
                document.getElementById("treman").className = "details";
                document.getElementById("treman2").className = "details";
                document.getElementById("treman3").className = "details";
                document.getElementById("treman4").className = "details";
                document.getElementById("traddnew2").className = "view";
            }

            function loseprof() {
                document.getElementById("lblspid").value = "";
                document.getElementById("lblspname").value = "";
                document.getElementById("lblprofe").innerHTML = "";
                document.getElementById("treman").className = "view";
                document.getElementById("treman2").className = "view";
                document.getElementById("treman3").className = "view";
                document.getElementById("treman4").className = "view";
                document.getElementById("traddnew2").className = "details";
            }
            function getlabor(typ) {
                var chk = 0;
                var nme = "";
                var id = "";
                var sk = "";
                var ski = "";
                var su = "";
                var sui = "";
                var sht = "";

                if (typ == "new") {
                    var nbx = document.getElementById("cblabora");
                    if (nbx.checked == true) {
                        chk = 1;
                        nme = document.getElementById("txtusernamenew").value;
                        id = document.getElementById("lbltempid").value;
                        sk = document.getElementById("txtskilln").value;
                        ski = document.getElementById("lblskillidrn").value;
                        su = document.getElementById("txtsupern").value;
                        sui = document.getElementById("lblsuperidrn").value;
                        sht = document.getElementById("ddshiftn").value;
                    }
                }
                if (typ == "old") {
                    var obx = document.getElementById("cblabor");
                    if (obx.checked == true) {
                        chk = 1;
                        nme = document.getElementById("txtusername").value;
                        id = document.getElementById("lbluseridr").value;
                        sk = document.getElementById("txtskillo").value;
                        ski = document.getElementById("lblskillidr").value;
                        su = document.getElementById("txtsupero").value;
                        sui = document.getElementById("lblsuperidr").value;
                        sht = document.getElementById("ddshifto").value;
                        var dps = document.getElementById("lbldps").value;
                    }
                }
                if (chk == 1) {
                    var eReturn = window.showModalDialog("laborassigndialog.aspx?typ=" + typ + "&nme=" + nme + "&id=" + id + "&sk=" + sk + "&ski=" + ski + "&su=" + su + "&sui=" + sui + "&sht=" + sht + "&dps=" + dps, "", "dialogHeight:510px; dialogWidth:510px; resizable=yes");
                }
            }
            function refit() {
               document.getElementById("txtsrch").value = "";
               document.getElementById("lblret").value = "ifirst";
               document.getElementById("form1").submit();
            }
        </script>
	</HEAD>
	<body class="tbg" >
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 1; POSITION: absolute; TOP: 2px; LEFT: 2px" cellSpacing="2" cellPadding="0"
				width="1080" border="0">
				<tr>
					<td width="300"></td>
					<td width="180"></td>
					<td width="250"></td>
					<td width="300"></td>
				</tr>
				<tr>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td class="thdrsinglft" width="26"><IMG src="../images/appbuttons/minibuttons/pm3.gif" border="0"></td>
								<td class="thdrsingrt label" width="704"><asp:Label id="lang3520" runat="server">PM User Administration</asp:Label></td>
							</tr>
						</table>
					</td>
					<td>
						<table cellSpacing="0" cellPadding="0">
							<tr>
								<td class="thdrsingorangelft" width="26"><IMG src="../images/appbuttons/minibuttons/pm3red.gif" border="0"></td>
								<td class="thdrsingorangert label" width="274"><asp:Label id="lang3521" runat="server">New User Dialog</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td vAlign="top" align="left" colSpan="3">
						<table>
							<TBODY>
								<tr>
									<td vAlign="top" colSpan="3">
										<table>
											<tr>
												<td class="label"><asp:Label id="lang3522" runat="server">Search Users</asp:Label></td>
												<td>
													<asp:TextBox id="txtsrch" runat="server"></asp:TextBox></td>
												<td><img src="../images/appbuttons/minibuttons/srchsm.gif" onclick="srchusers();"></td>
                                                <td><img src="../images/appbuttons/minibuttons/refreshit.gif" onclick="refit();"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td vAlign="top" colSpan="3" height="135">
										<div id="ispdiv" style="HEIGHT: 140px; OVERFLOW: auto" onscroll="SetiDivPosition();"><asp:repeater id="dgusers" runat="server">
												<HeaderTemplate>
													<table cellspacing="1">
														<tr height="20">
															<td class="btmmenu plainlabel" width="120px"><asp:Label id="lang3523" runat="server">Login ID</asp:Label></td>
															<td class="btmmenu plainlabel" width="160px"><asp:Label id="lang3524" runat="server">User Name</asp:Label></td>
															<td class="btmmenu plainlabel" width="170px"><asp:Label id="lang3525" runat="server">Skill</asp:Label></td>
															<td class="btmmenu plainlabel" width="100px"><asp:Label id="lang3526" runat="server">Phone</asp:Label></td>
															<td class="btmmenu plainlabel" width="180px"><asp:Label id="lang3527" runat="server">Email</asp:Label></td>
														</tr>
												</HeaderTemplate>
												<ItemTemplate>
													<tr class="tbg">
														<td class="plainlabel">
															<asp:radiobutton AutoPostBack="True" OnCheckedChanged="GetUser" id="rbuid" Text='<%# DataBinder.Eval(Container.DataItem,"uid")%>' runat="server"  /></td>
														<td class="plainlabel">
															<asp:label ID="lblusername" Text='<%# DataBinder.Eval(Container.DataItem,"username")%>' Runat = server>
															</asp:label></td>
														<td class="plainlabel">
															<asp:label ID="lblskill" Text='<%# DataBinder.Eval(Container.DataItem,"skill")%>' Runat = server>
															</asp:label></td>
														<td class="plainlabel">
															<asp:label ID="lblphone" Text='<%# DataBinder.Eval(Container.DataItem,"phonenum")%>' Runat = server>
															</asp:label></td>
														<td class="plainlabel">
															<a href='mailto:<%# DataBinder.Eval(Container.DataItem,"email")%>' >
																<asp:label ID="lblemail" Text='<%# DataBinder.Eval(Container.DataItem,"email")%>' Runat = server>
																</asp:label></a></td>
														<td class="details"><asp:Label ID="lblindex" Text='<%# DataBinder.Eval(Container.DataItem,"groupindex")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblstatus" Text='<%# DataBinder.Eval(Container.DataItem,"status")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblexp" Text='<%# DataBinder.Eval(Container.DataItem,"expires")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblexpd" Text='<%# DataBinder.Eval(Container.DataItem,"expiration")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbldps" Text='<%# DataBinder.Eval(Container.DataItem,"dfltps")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblps" Text='<%# DataBinder.Eval(Container.DataItem,"ps")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblms" Text='<%# DataBinder.Eval(Container.DataItem,"multisite")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblro" Text='<%# DataBinder.Eval(Container.DataItem,"readonly")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblskillid" Text='<%# DataBinder.Eval(Container.DataItem,"skillid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblsuper" Text='<%# DataBinder.Eval(Container.DataItem,"super")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblsuperid" Text='<%# DataBinder.Eval(Container.DataItem,"superid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblislabor" Text='<%# DataBinder.Eval(Container.DataItem,"islabor")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblissuper" Text='<%# DataBinder.Eval(Container.DataItem,"issuper")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbladmini" Text='<%# DataBinder.Eval(Container.DataItem,"admin")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblshifti" Text='<%# DataBinder.Eval(Container.DataItem,"shift")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbluseridi" Text='<%# DataBinder.Eval(Container.DataItem,"userid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblplnri" Text='<%# DataBinder.Eval(Container.DataItem,"isplanner")%>' Runat = server>
															</asp:Label></td>
													</tr>
												</ItemTemplate>
												<AlternatingItemTemplate>
													<tr>
														<td class="plainlabel transrowblue">
															<asp:radiobutton AutoPostBack="True" OnCheckedChanged="GetUser" id="rbuida" Text='<%# DataBinder.Eval(Container.DataItem,"uid")%>' runat="server"  /></td>
														<td class="plainlabel transrowblue">
															<asp:label ID="lblusernamea" Text='<%# DataBinder.Eval(Container.DataItem,"username")%>' Runat = server>
															</asp:label></td>
														<td class="plainlabel transrowblue">
															<asp:label ID="lblskilla" Text='<%# DataBinder.Eval(Container.DataItem,"skill")%>' Runat = server>
															</asp:label></td>
														<td class="plainlabel transrowblue">
															<asp:label ID="lblphonea" Text='<%# DataBinder.Eval(Container.DataItem,"phonenum")%>' Runat = server>
															</asp:label></td>
														<td class="plainlabel transrowblue">
															<a href='mailto:<%# DataBinder.Eval(Container.DataItem,"email")%>' >
																<asp:label ID="lblemaila" Text='<%# DataBinder.Eval(Container.DataItem,"email")%>' Runat = server>
																</asp:label></a></td>
														<td class="details"><asp:Label ID="lblindexa" Text='<%# DataBinder.Eval(Container.DataItem,"groupindex")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblstatusa" Text='<%# DataBinder.Eval(Container.DataItem,"status")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblexpa" Text='<%# DataBinder.Eval(Container.DataItem,"expires")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblexpda" Text='<%# DataBinder.Eval(Container.DataItem,"expiration")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbldpsa" Text='<%# DataBinder.Eval(Container.DataItem,"dfltps")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblpsa" Text='<%# DataBinder.Eval(Container.DataItem,"ps")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmsa" Text='<%# DataBinder.Eval(Container.DataItem,"multisite")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblroa" Text='<%# DataBinder.Eval(Container.DataItem,"readonly")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblskillida" Text='<%# DataBinder.Eval(Container.DataItem,"skillid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblsupera" Text='<%# DataBinder.Eval(Container.DataItem,"super")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblsuperida" Text='<%# DataBinder.Eval(Container.DataItem,"superid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblislabora" Text='<%# DataBinder.Eval(Container.DataItem,"islabor")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblissupera" Text='<%# DataBinder.Eval(Container.DataItem,"issuper")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbladmina" Text='<%# DataBinder.Eval(Container.DataItem,"admin")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblshifta" Text='<%# DataBinder.Eval(Container.DataItem,"shift")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbluserida" Text='<%# DataBinder.Eval(Container.DataItem,"userid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblplnre" Text='<%# DataBinder.Eval(Container.DataItem,"isplanner")%>' Runat = server>
															</asp:Label></td>
													</tr>
												</AlternatingItemTemplate>
												<FooterTemplate>
						</table>
						</FooterTemplate> </asp:repeater></DIV></td>
				</tr>
				<tr>
					<td align="center" colSpan="3">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img5" onclick="getfirst('i');" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img6" onclick="getprev('i');" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblipg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid" width="20"><IMG id="Img7" onclick="getnext('i');" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td width="20"><IMG id="Img8" onclick="getlast('i');" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</TD>
			<td vAlign="top" rowSpan="2">
				<table style="BORDER-BOTTOM: gray 1px groove; BORDER-LEFT: gray 1px groove; BORDER-TOP: gray 1px groove; BORDER-RIGHT: gray 1px groove"
					cellSpacing="1" cellPadding="1" width="310">
					<tr>
						<td colSpan="2">
							<table cellSpacing="0" cellPadding="1">
								<tr>
									<td class="redlabel"><asp:Label id="lang3528" runat="server">New Login ID</asp:Label></td>
									<td colSpan="2"><asp:textbox id="txtnewuid" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox>
                                    <img alt="" src="../images/appbuttons/minibuttons/bulkproc.gif" onclick="getbulk();" onmouseover="return overlib('Add users in Bulk using an Excel File')" onmouseout="return nd()" /></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3529" runat="server">Password</asp:Label></td>
									<td colSpan="2"><asp:textbox id="txtp1new" runat="server" CssClass="plainlabel" MaxLength="20" TextMode="Password"></asp:textbox></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3530" runat="server">Verify Password</asp:Label></td>
									<td colSpan="2"><asp:textbox id="txtp2new" runat="server" CssClass="plainlabel" MaxLength="20" TextMode="Password"></asp:textbox></td>
								</tr>
								<tr>
									<td class="redlabel" vAlign="middle"><asp:Label id="lang3531" runat="server">Expires?</asp:Label></td>
									<td class="redlabel" colSpan="2"><asp:radiobuttonlist id="rbexpiresnew" runat="server" CssClass="redlabel" RepeatDirection="Horizontal"
											Height="0px" RepeatLayout="Flow">
											<asp:ListItem Value="Y">Yes</asp:ListItem>
											<asp:ListItem Value="N" Selected="True">No</asp:ListItem>
										</asp:radiobuttonlist></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3532" runat="server">Expires On</asp:Label></td>
									<td><asp:textbox id="txtexpdnew" runat="server" CssClass="plainlabel" ReadOnly="True"></asp:textbox></td>
									<td><IMG onclick="getcal_txtexpdnew();" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
											width="19"></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3533" runat="server">User Name</asp:Label></td>
									<td colSpan="2"><asp:textbox id="txtusernamenew" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3534" runat="server">Phone#</asp:Label></td>
									<td colSpan="2"><asp:textbox id="txtphonenew" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3535" runat="server">Email Address</asp:Label></td>
									<td colSpan="2"><asp:textbox id="txtemailnew" runat="server" CssClass="plainlabel" MaxLength="200" Width="180px"></asp:textbox></td>
								</tr>
                                <tr>
									<td class="redlabel"><asp:Label id="lang3539" runat="server">Supervisor</asp:Label></td>
									<td><asp:textbox id="txtsupern" runat="server" CssClass="plainlabel" MaxLength="200" Width="160px"></asp:textbox>
                                    </td><td>
                                    <IMG onclick="getsuper('new');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
											border="0"></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3536" runat="server">Default Plant Site</asp:Label></td>
									<td colSpan="2"><asp:dropdownlist id="ddpsnew" runat="server" CssClass="plainlabel" Width="180px"></asp:dropdownlist></td>
								</tr>
                                <tr>
                                <td colspan="3">
                                <table>
                                <tr>
                                <td class="redlabel" width="70"><asp:Label id="Label1" runat="server">Use Profile</asp:Label></td>
									<td class="plainlabelred" id="lblprofe" runat="server" width="160"></td>
                                    <td width="70"><img alt="" src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getprof();" />
                                    <img alt="" src="../images/appbuttons/minibuttons/refreshit.gif" onclick="loseprof();" /></td>
                                </tr>
                                <tr id="traddnew2" runat="server" class="details">
                                <td colspan="3" align="right"><img alt="" id="btnaddnew2" onclick="valnew2();" height="19" src="../images/appbuttons/bgbuttons/submit.gif"
								width="71" runat="server" /></td>
                                </tr>
                                </table>
                                </td>
									
								</tr>
							</table>
						</td>
					</tr>
					<tr id="treman" runat="server">
						<td colSpan="2">
							<table cellSpacing="0" cellPadding="1" width="300">
								<tr class="details">
									<td class="redlabel" align="center" colSpan="2"><asp:checkbox id="cbroa" runat="server" Text="User Is Read-Only"></asp:checkbox></td>
								</tr>
								<tr >
									<td height="18" class="redlabel" align="center" colSpan="2"><asp:checkbox id="cbadmina" runat="server" Text="User Is An Administrator"></asp:checkbox>
										&nbsp;<asp:checkbox id="cbplannera" runat="server" Text="User Is A Planner"></asp:checkbox></td>
								</tr>
								<tr >
									<td height="18" class="redlabel" align="center" colSpan="2"><asp:checkbox id="cbsupera" runat="server" Text="User Is A Supervisor"></asp:checkbox><IMG class="details" onclick="getsuperlocs('old');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
											border="0">&nbsp;<asp:checkbox id="cblabora" runat="server" Text="User Is Labor"></asp:checkbox></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3537" runat="server">User Shift</asp:Label></td>
									<td><asp:dropdownlist id="ddshiftn" runat="server" CssClass="plainlabel">
											<asp:ListItem Value="0" Selected="True">Any</asp:ListItem>
											<asp:ListItem Value="1">First</asp:ListItem>
											<asp:ListItem Value="2">Second</asp:ListItem>
											<asp:ListItem Value="3">Third</asp:ListItem>
										</asp:dropdownlist></td>
								</tr>
								<tr>
									<td class="redlabel"><asp:Label id="lang3538" runat="server">User Skill</asp:Label></td>
									<td><asp:textbox id="txtskilln" runat="server" CssClass="plainlabel" MaxLength="200" Width="160px"></asp:textbox><IMG onclick="getskillNew();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
											border="0"></td>
								</tr>
								
							</table>
						</td>
					</tr>
					<tr id="treman2" runat="server">
						<td align="center" colSpan="2">
							<table cellSpacing="0" cellPadding="1">
								<tr>
									<td class="redlabel" width="120"><asp:Label id="lang3540" runat="server">User Applications</asp:Label></td>
									<td align="right" width="120"><IMG id="addappnew" onclick="getuserapps('new');" height="19" alt="" src="../images/appbuttons/bgbuttons/badd.gif"
											width="69" runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr id="treman3" runat="server">
						<td class="plainlabel" id="newuappsdiv" vAlign="top" align="center" colSpan="2" runat="server">
							<div id="dvnewapps" style="BORDER-BOTTOM: 2px groove; BORDER-LEFT: 1px groove; WIDTH: 240px; HEIGHT: 107px; OVERFLOW: auto; BORDER-TOP: 1px groove; BORDER-RIGHT: 2px groove"
								runat="server"></div>
						</td>
					</tr>
					<tr  id="treman4" runat="server">
						<td align="right" colSpan="2"><IMG id="btnaddnewlogin" onclick="valnew();" height="19" src="../images/appbuttons/bgbuttons/submit.gif"
								width="71" runat="server"></td>
					</tr>
				</table>
			</td>
			</TR>
			<tr>
				<td vAlign="top" align="left" width="300">
					<table cellSpacing="0" cellPadding="0">
						<tr>
							<td class="thdrsingnobot label" colSpan="2"><asp:Label id="lang3541" runat="server">Selected User Profile</asp:Label></td>
						</tr>
						<tr>
							<td>
								<table style="BORDER-BOTTOM: gray 1px groove; BORDER-LEFT: gray 1px groove; BORDER-TOP: gray 1px groove; BORDER-RIGHT: gray 1px groove"
									cellSpacing="0" cellPadding="0" width="310">
									<tr>
										<td>
											<table cellSpacing="1" cellPadding="1" width="310">
												<tr>
													<td colSpan="3"><IMG src="../images/appbuttons/minibuttons/2PX.gif"></td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3542" runat="server">Login ID</asp:Label></td>
													<td colSpan="2"><asp:textbox id="txtuserid" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3543" runat="server">User Name</asp:Label></td>
													<td colSpan="2"><asp:textbox id="txtusername" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3544" runat="server">Phone#</asp:Label></td>
													<td colSpan="2"><asp:textbox id="txtphone" runat="server" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3545" runat="server">Email Address</asp:Label></td>
													<td colSpan="2"><asp:textbox id="txtemail" runat="server" CssClass="plainlabel" Width="180px"></asp:textbox></td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3546" runat="server">User Shift</asp:Label></td>
													<td colSpan="2"><asp:dropdownlist id="ddshifto" runat="server" CssClass="plainlabel">
															<asp:ListItem Value="0" Selected="True">Any</asp:ListItem>
															<asp:ListItem Value="1">First</asp:ListItem>
															<asp:ListItem Value="2">Second</asp:ListItem>
															<asp:ListItem Value="3">Third</asp:ListItem>
														</asp:dropdownlist></td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3547" runat="server">User Skill</asp:Label></td>
													<td colSpan="2"><asp:textbox id="txtskillo" runat="server" CssClass="plainlabel" MaxLength="200" Width="160px"></asp:textbox><IMG onclick="getskillOld();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
															border="0"></td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3548" runat="server">Supervisor</asp:Label></td>
													<td colSpan="2"><asp:textbox id="txtsupero" runat="server" CssClass="plainlabel" MaxLength="200" Width="160px"></asp:textbox><IMG onclick="getsuper('old');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
															border="0"></td>
												</tr>
												<tr class="details">
													<td class="bluelabel" align="center" colSpan="3"><asp:checkbox id="cbro" runat="server" Text="User Is Read-Only"></asp:checkbox></td>
												</tr>
												<tr>
													<td class="bluelabel" align="center" colSpan="3"><asp:checkbox id="cbadmin" runat="server" Text="User Is An Administrator"></asp:checkbox>
														&nbsp;<asp:checkbox id="cbplanner" runat="server" Text="User Is A Planner"></asp:checkbox></td>
												</tr>
												<tr>
													<td class="bluelabel" align="center" colSpan="3"><asp:checkbox id="cbsuper" runat="server" Text="User Is A Supervisor"></asp:checkbox><IMG class="details" onclick="getsuperlocs('old');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
															border="0">&nbsp;<asp:checkbox id="cblabor" runat="server" Text="User Is Labor"></asp:checkbox><IMG onclick="getlabor('old');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
															border="0"></td>
												</tr>
												<tr height="22">
													<td class="bluelabel" align="center" colSpan="3">&nbsp;</td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3549" runat="server">Default Plant Site</asp:Label></td>
													<td><asp:dropdownlist id="ddps" runat="server" CssClass="plainlabel" Width="180px"></asp:dropdownlist></td>
													<td><IMG id="btnaddnewfail" onmouseover="return overlib('Add Muliple Site Access to Selected User')"
															onclick="GetFailDiv('cur');" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
															width="20"></td>
												</tr>
												<tr>
													<td align="right" colSpan="3"><asp:imagebutton id="btnsubmitprofile" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td vAlign="top" align="center" width="210">
					<table cellSpacing="0" cellPadding="0">
						<tr>
							<td class="thdrsingnobot label"><asp:Label id="lang3550" runat="server">Selected User Applications</asp:Label></td>
						</tr>
						<tr>
							<td>
								<table style="BORDER-BOTTOM: gray 1px groove; BORDER-LEFT: gray 1px groove; BORDER-TOP: gray 1px groove; BORDER-RIGHT: gray 1px groove"
									cellSpacing="0" cellPadding="1" width="210">
									<tr>
										<td class="plainlabel" id="tdapps" vAlign="top" runat="server">
											<div id="dvapps" style="WIDTH: 210px; HEIGHT: 285px; OVERFLOW: auto" runat="server"></div>
										</td>
									</tr>
									<tr>
										<td align="right"><IMG id="addapp" onclick="getuserapps('cur');" height="19" alt="" src="../images/appbuttons/bgbuttons/edit.gif"
												width="69" runat="server">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td vAlign="top" align="left" width="250">
					<table cellSpacing="0" cellPadding="0">
						<tr>
							<td class="thdrsingnobot label"><asp:Label id="lang3551" runat="server">Selected User Password</asp:Label></td>
						</tr>
						<tr>
							<td>
								<table style="BORDER-BOTTOM: gray 0px groove; BORDER-LEFT: gray 1px groove; BORDER-TOP: gray 1px groove; BORDER-RIGHT: gray 1px groove"
									cellSpacing="0" cellPadding="1">
									<tr height="10">
										<td colSpan="2"><IMG src="../images/appbuttons/minibuttons/8PX.gif"></td>
									</tr>
									<tr>
										<td class="label" align="center" colSpan="2"><asp:radiobuttonlist id="rbactive" runat="server" CssClass="plainlabel" RepeatDirection="Horizontal"
												Height="26px" RepeatLayout="Flow">
												<asp:ListItem Value="active">active</asp:ListItem>
												<asp:ListItem Value="inactive">inactive</asp:ListItem>
											</asp:radiobuttonlist></td>
									</tr>
									<tr>
										<td colSpan="2">
											<table>
												<tr>
													<td class="label" vAlign="middle" width="90"><asp:Label id="lang3552" runat="server">Expires?</asp:Label></td>
													<td class="label" width="160"><input id="rbxy" type="radio" name="rbx" runat="server"><asp:Label id="lang3553" runat="server">Yes</asp:Label><input id="rbxn" onclick="checkDate();" type="radio" name="rbx" runat="server"><asp:Label id="lang3554" runat="server">No</asp:Label></td>
												</tr>
												<tr>
													<td class="LABEL"><asp:Label id="lang3555" runat="server">Expires On</asp:Label></td>
													<td><asp:textbox id="txtexpd" runat="server" CssClass="plainlabel" ReadOnly="True"></asp:textbox><IMG onclick="getcal_txtexpd();" height="19" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
															width="19"></td>
												</tr>
												<tr>
													<td align="right" colSpan="2"><asp:imagebutton id="btnusercr" runat="server" 
                                                            ImageUrl="../images/appbuttons/bgbuttons/submit.gif" style="height: 19px"></asp:imagebutton>&nbsp;&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colSpan="2">
											<table>
												<tr>
													<td class="label" width="120"><asp:Label id="lang3556" runat="server">Password</asp:Label></td>
													<td width="110"><asp:textbox id="txtp1" runat="server" CssClass="plainlabel" MaxLength="20" TextMode="Password"
															Width="100px"></asp:textbox></td>
												</tr>
												<tr>
													<td class="label"><asp:Label id="lang3557" runat="server">Verify Password</asp:Label></td>
													<td><asp:textbox id="txtp2" runat="server" CssClass="plainlabel" MaxLength="20" TextMode="Password"
															Width="100px"></asp:textbox></td>
												</tr>
												<tr>
													<td align="right" colSpan="2"><asp:imagebutton id="btnsubmitpass" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:imagebutton>&nbsp;&nbsp;</td>
												</tr>
          
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="thdrsing label" width="240"><asp:Label id="lang3558" runat="server">PM User Stats</asp:Label></td>
						</tr>
						<tr>
							<td>
								<table style="BORDER-BOTTOM: gray 1px groove; BORDER-LEFT: gray 1px groove; BORDER-TOP: gray 0px groove; BORDER-RIGHT: gray 1px groove"
									cellSpacing="3" cellPadding="3">
									<tr height="20">
										<td class="label" width="190"><asp:Label id="lang3559" runat="server">User Licenses Available</asp:Label></td>
										<td class="plainlabel" id="tdlic" align="center" width="50" runat="server"></td>
									</tr>
									<tr height="20">
										<td class="label"><asp:Label id="lang3560" runat="server">Current Active Users</asp:Label></td>
										<td class="plainlabel" id="tdusers" align="center" runat="server"></td>
									</tr>
									<tr>
										<td colSpan="2"><IMG src="../images/appbuttons/minibuttons/22PX.gif"></td>
									</tr>
									<tr>
										<td colSpan="2"><IMG src="../images/appbuttons/minibuttons/2PX.gif"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</TBODY></TABLE><input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lbluserid" type="hidden" name="lbluserid" runat="server"><input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="lbllic" type="hidden" name="lbllic" runat="server"> <input id="lblusers" type="hidden" name="lblusers" runat="server">
			<input id="lblnew" type="hidden" name="lblnew" runat="server"><input id="lblnewuid" type="hidden" name="lblnewuid" runat="server"><input id="lblchk" type="hidden" name="lblchk" runat="server">
			<input id="lblpchk" type="hidden" name="lblpchk" runat="server"><input id="lbluid" type="hidden" name="lbluid" runat="server">
			<input id="lblnewcheck" type="hidden" name="lblnewcheck" runat="server"><input id="lbladmin" type="hidden" name="lbladmin" runat="server">
			<input id="ispdivy" type="hidden" name="ispdivy" runat="server"> <input id="txtipg" type="hidden" name="Hidden2" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="txtipgcnt" type="hidden" name="txtipgcnt" runat="server">
			<input id="lblcadm" type="hidden" name="lblcadm" runat="server"><input id="lblappstr" type="hidden" name="lblappstr" runat="server">
			<input id="lblapps" type="hidden" name="lblapps" runat="server"> <input id="lblskillidr" type="hidden" runat="server">
			<input id="lblsuperidr" type="hidden" runat="server"> <input id="lblskillidrn" type="hidden" runat="server">
			<input id="lblsuperidrn" type="hidden" runat="server"> <input id="lblsuperindexr" type="hidden" runat="server">
			<input id="lblskillindexr" type="hidden" runat="server"> <input id="lblsuperindexrn" type="hidden" runat="server">
			<input id="lblskillindexrn" type="hidden" runat="server"><input id="lblshift" type="hidden" runat="server">
			<input id="lbltempid" type="hidden" runat="server"><input id="lbluseridr" type="hidden" runat="server">
			<input type="hidden" id="lblcbsuperchk" runat="server"> <input type="hidden" id="lblouname" runat="server" NAME="lblouname">
			<input type="hidden" id="lblfslang" runat="server" /><input type="hidden" id="lbldps" runat="server" />
            <input type="hidden" id="lblspid" runat="server" />
            <input type="hidden" id="lblspname" runat="server" />
         </form>
	</body>
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
</HTML>
