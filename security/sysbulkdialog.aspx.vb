﻿Public Class sysbulkdialog
    Inherits System.Web.UI.Page
    Dim admin, cadm As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            admin = Request.QueryString("admin").ToString
            cadm = Request.QueryString("cadm").ToString
            ifmeter.Attributes.Add("src", "sysbulk.aspx?admin=" + admin + "&cadm=" + cadm + "&date=" + Now)
        End If
    End Sub

End Class