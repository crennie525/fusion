

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient


Public Class lskills
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sk As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents dvskills As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            sk.Open()
            GetSkills()
            sk.Dispose()
        End If
    End Sub
    Private Sub GetSkills()
        Dim skill, skilli, skillin As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        sql = "select * from pmskills where skillid <> 0"
        dr = sk.GetRdrData(sql)
        While dr.Read
            skill = dr.Item("skill").ToString
            skilli = dr.Item("skillid").ToString
            skillin = dr.Item("skillindex").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getskill('" & skill & "', '" & skilli & "', '" & skillin & "');"">" & skill & "</a></td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        dvskills.InnerHtml = sb.ToString
    End Sub
End Class
