

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Net.Mail

Public Class useradmin2
    Inherits System.Web.UI.Page
    Protected WithEvents btnaddnewfail As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang3560 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3559 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3558 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3557 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3556 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3555 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3554 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3553 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3552 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3551 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3550 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3549 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3548 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3547 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3546 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3545 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3544 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3543 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3542 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3541 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3540 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3539 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3538 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3537 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3536 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3535 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3534 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3533 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3532 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3531 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3530 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3529 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3528 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3527 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3526 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3525 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3524 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3523 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3522 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3521 As System.Web.UI.WebControls.Label

    Protected WithEvents lang3520 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblspid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblspname As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim adm As New Utilities
    Dim cid, ps, uid, sess, psite, admin, cadm As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Protected WithEvents dgusers As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblipg As System.Web.UI.WebControls.Label
    Protected WithEvents Img5 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img7 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img8 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtuserid As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtusername As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphone As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemail As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbro As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddps As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnsubmitprofile As System.Web.UI.WebControls.ImageButton
    Protected WithEvents cbadmin As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cblabor As System.Web.UI.WebControls.CheckBox
    Protected WithEvents tdapps As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents dvapps As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents addapp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents rbactive As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents txtexpd As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnusercr As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtp1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtp2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsubmitpass As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtnewuid As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtp1new As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtp2new As System.Web.UI.WebControls.TextBox
    Protected WithEvents rbexpiresnew As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents txtexpdnew As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtusernamenew As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtphonenew As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtemailnew As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbroa As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddpsnew As System.Web.UI.WebControls.DropDownList
    Protected WithEvents addappnew As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents newuappsdiv As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents btnaddnewlogin As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdlic As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdusers As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents dvnewapps As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents cbadmina As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cblabora As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapps As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillidr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuperidr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillidrn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuperidrn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtskilln As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtsupern As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtskillo As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtsupero As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbsupera As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbsuper As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuperindexr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillindexr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsuperindexrn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblskillindexrn As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblshift As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltempid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluseridr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddshiftn As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddshifto As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblcbsuperchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbxn As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbxy As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents cbplannera As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbplanner As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblouname As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbldps As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblprofe As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents treman As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents treman2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents treman3 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents treman4 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents traddnew2 As System.Web.UI.HtmlControls.HtmlTableRow

    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluserid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusers As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnew As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewcheck As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladmin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ispdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtipgcnt As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then
            Try
                sess = HttpContext.Current.Session("Logged_IN").ToString()
            Catch ex As Exception
                Response.Redirect("../NewLogin.aspx?app=none&lo=yes")
            End Try
            cadm = HttpContext.Current.Session("cadm").ToString()
            lblcadm.Value = cadm
            cid = HttpContext.Current.Session("comp").ToString
            admin = HttpContext.Current.Session("uid").ToString
            lblcid.Value = cid
            lbladmin.Value = admin
            ps = HttpContext.Current.Session("dfltps").ToString
            lblsid.Value = ps
            adm.Open()
            GetTemp()
            LoadUsers(PageNumber, cid)
            GetSites(cid)
            GetStats(cid, ps)
            adm.Dispose()
        Else
            If Request.Form("lblpchk").ToString = "apps" Then
                Dim ui As String = txtuserid.Text
                lblpchk.Value = ""
                GetApps(ui)
            ElseIf Request.Form("lblpchk").ToString = "apps" Then
                Dim ui As String = txtnewuid.Text
                lblpchk.Value = ""
                GetApps(ui, "new")
            End If
            If Request.Form("lblnewcheck") = "ok" Then
                lblnewcheck.Value = ""
                addUser()
            ElseIf Request.Form("lblnewcheck") = "ok1" Then
                lblnewcheck.Value = ""
                addUser2()
            End If
            If Request.Form("lblret") = "inext" Then
                adm.Open()
                GetiNext()
                adm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ilast" Then
                adm.Open()
                PageNumber = txtipgcnt.Value
                txtipg.Value = PageNumber
                LoadUsers(PageNumber, "0")
                adm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "iprev" Then
                adm.Open()
                GetiPrev()
                adm.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "ifirst" Then
                adm.Open()
                PageNumber = 1
                txtipg.Value = PageNumber
                LoadUsers(PageNumber, "0")
                adm.Dispose()
                lblret.Value = ""

            End If
            Dim spid As String = lblspid.Value

            If spid <> "" Then
                lblprofe.InnerHtml = lblspname.Value
                treman.Attributes.Add("class", "details")
                treman2.Attributes.Add("class", "details")
                treman3.Attributes.Add("class", "details")
                treman4.Attributes.Add("class", "details")
                traddnew2.Attributes.Add("class", "view")
            Else
                lblprofe.InnerHtml = ""
                treman.Attributes.Add("class", "view")
                treman2.Attributes.Add("class", "view")
                treman3.Attributes.Add("class", "view")
                treman4.Attributes.Add("class", "view")
                traddnew2.Attributes.Add("class", "details")
            End If
        End If
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("UserAdmin.aspx")
        Catch ex As Exception

        End Try


        'rbexpires.Attributes.Add("onclick", "checkdate(this.value);")
    End Sub
    Private Sub GetTemp()
        sql = "usp_genlabtpmid"
        Dim tmpid As Integer
        tmpid = adm.Scalar(sql)
        lbltempid.Value = tmpid
    End Sub
    Private Sub UpUser(ByVal uid As String, ByVal admn As String)

        Dim ap, aps, appstr As String
        ap = lblapps.Value

        aps = lblappstr.Value
        Try
            ap = ap.Remove(ap.Length - 1, 1)
            aps = aps.Remove(aps.Length - 1, 1)
        Catch ex As Exception
            ap = ""
            aps = ""
        End Try

        If admn = "1" Then
            sql = "update pmsysusers set appstr = 'all' where uid = '" & uid & "'"
        Else
            sql = "update pmsysusers set appstr = '" & ap & "' where uid = '" & uid & "'"
        End If
        adm.Update(sql)
        Dim aparr() As String = ap.Split(",")
        Dim apsarr() As String = aps.Split(",")
        Dim stat As String = "1"
        Dim i As Integer
        For i = 0 To aparr.Length - 1
            Dim scnt As Integer
            sql = "select count(*) from pmUserApps where app = '" & aparr(i) & "' " _
            + "and uid = '" & uid & "'"
            scnt = adm.Scalar(sql)
            If scnt = 0 Then
                sql = "insert into pmUserApps (uid, app, stat) values ('" & uid & "', '" & aparr(i) & "', '" & stat & "')"
                adm.Update(sql)
            End If
        Next

        'us.Dispose()
    End Sub
    Private Sub UpUser2(ByVal uid As String, ByVal admn As String)
        Dim spid As String = lblspid.Value
        Dim ap, aps, appstr As String
        ap = lblapps.Value

        aps = lblappstr.Value

        If admn = "1" Then
            sql = "update pmsysusers set appstr = 'all' where uid = '" & uid & "'"
        Else
            sql = "update pmsysusers set appstr = '" & aps & "' where uid = '" & uid & "'"
        End If
        adm.Update(sql)
        Dim aparr() As String = ap.Split(",")
        Dim apsarr() As String = aps.Split(",")
        Dim stat As String = "1"
        sql = "insert into pmUserApps (uid, app, stat) select '" & uid & "', app, '" & stat & "' from sysapps where spid = '" & spid & "'"
        adm.Update(sql)

        'us.Dispose()
    End Sub
    Private Sub addUser()
        Dim uc, au As String
        uc = lbllic.Value
        au = lblusers.Value
        cid = lblcid.Value
        Dim sname, pass, exp, pho, ema, dps, ro, admn, isl, iss, tmp, plnr As String
        sname = txtusernamenew.Text
        sname = Replace(sname, "'", Chr(180), , , vbTextCompare)
        If Len(sname) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1687", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(sname) = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr1688", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pass = txtp2new.Text
        pass = adm.Encrypt(pass)
        pho = txtphonenew.Text
        If Len(pho) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1689", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ema = txtemailnew.Text
        ema = Replace(ema, "'", "''")
        dps = ddpsnew.SelectedValue.ToString
        dps = Replace(dps, "'", Chr(180), , , vbTextCompare)
        uid = txtnewuid.Text
        uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
        If Len(uid) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1690", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(uid) = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr1691", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim uidcnt As Integer
        adm.Open()
        Dim ucnt, ncnt As String
        sql = "select count(*) from PMSysUsers where uid = '" & uid & "' or username = '" & sname & "'"
        sql = "select ucnt = (select count(uid) from PMSysUsers where uid = '" & uid & "'), ncnt = (select count(username) from PMSysUsers where username = '" & sname & "') "
        dr = adm.GetRdrData(sql)
        While dr.Read
            ucnt = dr.Item("ucnt").ToString
            ncnt = dr.Item("ncnt").ToString
        End While
        dr.Close()
        Dim iucnt, incnt As Integer
        Try
            iucnt = System.Convert.ToInt32(ucnt)
        Catch ex As Exception
            iucnt = 0
        End Try
        Try
            incnt = System.Convert.ToInt32(ncnt)
        Catch ex As Exception
            incnt = 0
        End Try
        uidcnt = iucnt + incnt 'adm.Scalar(sql)
        Dim chk As String = tdusers.InnerHtml
        If cbroa.Checked = True Then
            ro = "1"
        Else
            ro = "0"
        End If
        If cbadmina.Checked = True Then
            admn = "1"
            ro = "0"
            cbro.Checked = False
        Else
            admn = "0"
        End If
        If cblabora.Checked = True Then
            isl = "1"
            ro = "0"
            cbro.Checked = False
        Else
            isl = "0"
        End If
        If cbsupera.Checked = True Then
            iss = "1"
            ro = "0"
            cbro.Checked = False
        Else
            iss = "0"
        End If
        If cbplannera.Checked = True Then
            plnr = "1"
        Else
            plnr = "0"
        End If
        Dim super, superi, superin, skill, skilli, skillin, sht As String
        super = txtsupern.Text
        superi = lblsuperidrn.Value
        superin = lblsuperindexrn.Value
        skill = txtskilln.Text
        skilli = lblskillidrn.Value
        skillin = lblskillindexrn.Value
        tmp = lbltempid.Value
        sht = ddshiftn.SelectedValue.ToString

        If lblnew.Value = "ok" Then
            If uidcnt = 0 Then
                Dim stst As String
                stst = "exec usp_newuser1 '" & ema & "', '" & pho & "', '" & pass & "', '" & sname & "', " _
               + "'" & cid & "', '" & uid & "', '" & dps & "', '" & ro & "', '" & admn & "', " _
                + "'" & super & "', '" & superin & "', '" & superi & "', " _
                + "'" & skill & "', '" & skillin & "', '" & skilli & "', " _
                + "'" & isl & "', '" & iss & "', '" & tmp & "', '" & sht & "', '" & plnr & "'"

                Dim cmd As New SqlCommand("exec usp_newuser1 @email, @phonenum, @passwrd, @username, " _
                + "@compid, @uid, @dfltps, @readonly, @admin, " _
                + "@super, @superindex, @superid, " _
                + "@skill, @skillindex, @skillid, " _
                + "@islabor, @issuper, @tmpid, @shift, @plnr")

                Dim param01 = New SqlParameter("@email", SqlDbType.VarChar)
                If Len(ema) > 0 Then
                    param01.Value = ema
                Else
                    param01.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param01)
                Dim param02 = New SqlParameter("@phonenum", SqlDbType.VarChar)
                If Len(pho) > 0 Then
                    param02.Value = pho
                Else
                    param02.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param02)
                Dim param03 = New SqlParameter("@passwrd", SqlDbType.VarChar)
                If Len(pass) > 0 Then
                    param03.Value = pass
                Else
                    param03.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param03)
                Dim param04 = New SqlParameter("@username", SqlDbType.VarChar)
                If Len(sname) > 0 Then
                    param04.Value = sname
                Else
                    param04.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@compid", SqlDbType.VarChar)
                If Len(cid) > 0 Then
                    param05.Value = cid
                Else
                    param05.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param05)
                Dim param06 = New SqlParameter("@uid", SqlDbType.VarChar)
                If Len(uid) > 0 Then
                    param06.Value = uid
                Else
                    param06.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param06)
                Dim param07 = New SqlParameter("@dfltps", SqlDbType.VarChar)
                If Len(dps) > 0 Then
                    param07.Value = dps
                Else
                    param07.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param07)
                Dim param08 = New SqlParameter("@readonly", SqlDbType.VarChar)
                If Len(ro) > 0 Then
                    param08.Value = ro
                Else
                    param08.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param08)
                Dim param09 = New SqlParameter("@admin", SqlDbType.VarChar)
                If Len(admn) > 0 Then
                    param09.Value = admn
                Else
                    param09.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param09)
                Dim param010 = New SqlParameter("@super", SqlDbType.VarChar)
                If Len(super) > 0 Then
                    param010.Value = super
                Else
                    param010.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param010)
                Dim param011 = New SqlParameter("@superindex", SqlDbType.VarChar)
                If Len(superin) > 0 Then
                    param011.Value = superin
                Else
                    param011.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param011)
                Dim param012 = New SqlParameter("@superid", SqlDbType.VarChar)
                If Len(superi) > 0 Then
                    param012.Value = superi
                Else
                    param012.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param012)
                Dim param013 = New SqlParameter("@skill", SqlDbType.VarChar)
                If Len(skill) > 0 Then
                    param013.Value = skill
                Else
                    param013.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param013)
                Dim param014 = New SqlParameter("@skillindex", SqlDbType.VarChar)
                If Len(skillin) > 0 Then
                    param014.Value = skillin
                Else
                    param014.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param014)
                Dim param015 = New SqlParameter("@skillid", SqlDbType.VarChar)
                If Len(skilli) > 0 Then
                    param015.Value = skilli
                Else
                    param015.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param015)
                Dim param016 = New SqlParameter("@islabor", SqlDbType.VarChar)
                If Len(isl) > 0 Then
                    param016.Value = isl
                Else
                    param016.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param016)
                Dim param017 = New SqlParameter("@issuper", SqlDbType.VarChar)
                If Len(iss) > 0 Then
                    param017.Value = iss
                Else
                    param017.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param017)
                Dim param018 = New SqlParameter("@tmpid", SqlDbType.VarChar)
                If Len(tmp) > 0 Then
                    param018.Value = tmp
                Else
                    param018.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param018)
                Dim param019 = New SqlParameter("@shift", SqlDbType.VarChar)
                If Len(sht) > 0 Then
                    param019.Value = sht
                Else
                    param019.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param019)
                Dim param020 = New SqlParameter("@plnr", SqlDbType.VarChar)
                If Len(plnr) > 0 Then
                    param020.Value = plnr
                Else
                    param020.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param020)

                adm.UpdateHack(cmd)

                'sql = "insert into PMSysUsers (email, phonenum, passwrd, username, compid, uid, dfltps, groupindex, groupname, status, readonly, admin) values " _
                '+ "('" & ema & "', '" & pho & "', '" & pass & "', '" & sname & "', '" & cid & "', '" & uid & "', '" & dps & "', '6', 'pmnomanuser', 'active', '" & ro & "', '" & admn & "'); " '_
                '+ "delete from pmUserApps where app = 'uap' or app = 'ua' and uid = '" & uid & "'"
                'adm.Update(sql)

                sql = "insert into pmusersites (uid, siteid) values ('" & uid & "', '" & dps & "')"
                adm.Update(sql)
                lbluserid.Value = ""
                UpUser(uid, admn)
                GetTemp()
                LoadUsers(PageNumber, cid)
                GetStats(cid, ps)
                'Try
                SendIt("new", ema, uid)
                'Catch ex As Exception
                'Dim strMessage As String = tmod.getmsg("cdstr1692", "useradmin2.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'End Try
                'txtuserid.Text = ""
                txtnewuid.Text = ""
                lblnewuid.Value = ""
                txtusernamenew.Text = ""
                txtphonenew.Text = ""
                txtemailnew.Text = ""
                rbexpiresnew.SelectedValue = "N"
                txtexpdnew.Text = ""
                'newuappsdiv.InnerHtml = ""
                dvnewapps.InnerHtml = ""
                ddpsnew.SelectedIndex = 0
                lblapps.Value = ""
                lblappstr.Value = ""
                txtsupern.Text = ""
                lblsuperidrn.Value = ""
                lblsuperindexrn.Value = ""
                txtskilln.Text = ""
                lblskillidrn.Value = ""
                lblskillindexrn.Value = ""
                lbltempid.Value = ""
                lblshift.Value = ""
                cbroa.Checked = False
                cbadmina.Checked = False
                cblabora.Checked = False
                cbsupera.Checked = False
                If rbexpiresnew.SelectedValue.ToString = "Y" Then
                    exp = txtexpdnew.Text
                    If Len(exp) <> 0 Then
                        sql = "update PMSysUsers set exp = '" & exp & "' where uid = '" & uid & "'"
                        adm.Update(sql)
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1693", "useradmin2.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            Else
                Dim strMessage As String = tmod.getmsg("cdstr1694", "useradmin2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If


        End If
        lblapps.Value = ""
        lblappstr.Value = ""

        LoadUsers(PageNumber, cid)
        GetSites(cid)
        GetStats(cid, ps)
        adm.Dispose()
    End Sub
    Private Sub addUser2()
        Dim uc, au As String
        uc = lbllic.Value
        au = lblusers.Value
        cid = lblcid.Value
        Dim sname, pass, exp, pho, ema, dps, ro, admn, isl, iss, tmp, plnr As String
        sname = txtusernamenew.Text
        sname = Replace(sname, "'", Chr(180), , , vbTextCompare)
        If Len(sname) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1687", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(sname) = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr1688", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        pass = txtp2new.Text
        pass = adm.Encrypt(pass)
        pho = txtphonenew.Text
        If Len(pho) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1689", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ema = txtemailnew.Text
        ema = Replace(ema, "'", "''")
        dps = ddpsnew.SelectedValue.ToString
        dps = Replace(dps, "'", Chr(180), , , vbTextCompare)
        uid = txtnewuid.Text
        uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
        If Len(uid) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1690", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(uid) = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr1691", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim uidcnt As Integer
        adm.Open()
        Dim ucnt, ncnt As String
        sql = "select count(*) from PMSysUsers where uid = '" & uid & "' or username = '" & sname & "'"
        sql = "select ucnt = (select count(uid) from PMSysUsers where uid = '" & uid & "'), ncnt = (select count(username) from PMSysUsers where username = '" & sname & "') "
        dr = adm.GetRdrData(sql)
        While dr.Read
            ucnt = dr.Item("ucnt").ToString
            ncnt = dr.Item("ncnt").ToString
        End While
        dr.Close()
        Dim iucnt, incnt As Integer
        Try
            iucnt = System.Convert.ToInt32(ucnt)
        Catch ex As Exception
            iucnt = 0
        End Try
        Try
            incnt = System.Convert.ToInt32(ncnt)
        Catch ex As Exception
            incnt = 0
        End Try
        uidcnt = iucnt + incnt 'adm.Scalar(sql)
        Dim chk As String = tdusers.InnerHtml


        'removing readonly opt
        If cbroa.Checked = True Then
            ro = "0"
        Else
            ro = "0"
        End If

        'add spid input here
        'admn, isl, iss, plnr
        Dim appstr As String
        Dim spid As String = lblspid.Value
        Dim super, superi, superin, skill, skilli, skillin, sht As String
        sql = "select * from sysprofiles where spid = '" & spid & "'"
        dr = adm.GetRdrData(sql)
        While dr.Read
            admn = dr.Item("admin").ToString
            isl = dr.Item("islabor").ToString
            iss = dr.Item("issuper").ToString
            plnr = dr.Item("isplanner").ToString
            skill = dr.Item("skill").ToString
            skilli = dr.Item("skillid").ToString
            skillin = "0"
            sht = dr.Item("shift").ToString
            appstr = dr.Item("appstr").ToString
        End While
        dr.Close()
        lblappstr.Value = appstr
        super = txtsupern.Text
        superi = lblsuperidrn.Value
        superin = lblsuperindexrn.Value


        If lblnew.Value = "ok" Then
            If uidcnt = 0 Then
                Dim stst As String
                stst = "exec usp_newuser1 '" & ema & "', '" & pho & "', '" & pass & "', '" & sname & "', " _
               + "'" & cid & "', '" & uid & "', '" & dps & "', '" & ro & "', '" & admn & "', " _
                + "'" & super & "', '" & superin & "', '" & superi & "', " _
                + "'" & skill & "', '" & skillin & "', '" & skilli & "', " _
                + "'" & isl & "', '" & iss & "', '" & tmp & "', '" & sht & "', '" & plnr & "'"

                Dim cmd As New SqlCommand("exec usp_newuser1 @email, @phonenum, @passwrd, @username, " _
                + "@compid, @uid, @dfltps, @readonly, @admin, " _
                + "@super, @superindex, @superid, " _
                + "@skill, @skillindex, @skillid, " _
                + "@islabor, @issuper, @tmpid, @shift, @plnr")

                Dim param01 = New SqlParameter("@email", SqlDbType.VarChar)
                If Len(ema) > 0 Then
                    param01.Value = ema
                Else
                    param01.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param01)
                Dim param02 = New SqlParameter("@phonenum", SqlDbType.VarChar)
                If Len(pho) > 0 Then
                    param02.Value = pho
                Else
                    param02.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param02)
                Dim param03 = New SqlParameter("@passwrd", SqlDbType.VarChar)
                If Len(pass) > 0 Then
                    param03.Value = pass
                Else
                    param03.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param03)
                Dim param04 = New SqlParameter("@username", SqlDbType.VarChar)
                If Len(sname) > 0 Then
                    param04.Value = sname
                Else
                    param04.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@compid", SqlDbType.VarChar)
                If Len(cid) > 0 Then
                    param05.Value = cid
                Else
                    param05.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param05)
                Dim param06 = New SqlParameter("@uid", SqlDbType.VarChar)
                If Len(uid) > 0 Then
                    param06.Value = uid
                Else
                    param06.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param06)
                Dim param07 = New SqlParameter("@dfltps", SqlDbType.VarChar)
                If Len(dps) > 0 Then
                    param07.Value = dps
                Else
                    param07.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param07)
                Dim param08 = New SqlParameter("@readonly", SqlDbType.VarChar)
                If Len(ro) > 0 Then
                    param08.Value = ro
                Else
                    param08.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param08)
                Dim param09 = New SqlParameter("@admin", SqlDbType.VarChar)
                If Len(admn) > 0 Then
                    param09.Value = admn
                Else
                    param09.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param09)
                Dim param010 = New SqlParameter("@super", SqlDbType.VarChar)
                If Len(super) > 0 Then
                    param010.Value = super
                Else
                    param010.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param010)
                Dim param011 = New SqlParameter("@superindex", SqlDbType.VarChar)
                If Len(superin) > 0 Then
                    param011.Value = superin
                Else
                    param011.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param011)
                Dim param012 = New SqlParameter("@superid", SqlDbType.VarChar)
                If Len(superi) > 0 Then
                    param012.Value = superi
                Else
                    param012.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param012)
                Dim param013 = New SqlParameter("@skill", SqlDbType.VarChar)
                If Len(skill) > 0 Then
                    param013.Value = skill
                Else
                    param013.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param013)
                Dim param014 = New SqlParameter("@skillindex", SqlDbType.VarChar)
                If Len(skillin) > 0 Then
                    param014.Value = skillin
                Else
                    param014.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param014)
                Dim param015 = New SqlParameter("@skillid", SqlDbType.VarChar)
                If Len(skilli) > 0 Then
                    param015.Value = skilli
                Else
                    param015.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param015)
                Dim param016 = New SqlParameter("@islabor", SqlDbType.VarChar)
                If Len(isl) > 0 Then
                    param016.Value = isl
                Else
                    param016.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param016)
                Dim param017 = New SqlParameter("@issuper", SqlDbType.VarChar)
                If Len(iss) > 0 Then
                    param017.Value = iss
                Else
                    param017.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param017)
                Dim param018 = New SqlParameter("@tmpid", SqlDbType.VarChar)
                If Len(tmp) > 0 Then
                    param018.Value = tmp
                Else
                    param018.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param018)
                Dim param019 = New SqlParameter("@shift", SqlDbType.VarChar)
                If Len(sht) > 0 Then
                    param019.Value = sht
                Else
                    param019.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param019)
                Dim param020 = New SqlParameter("@plnr", SqlDbType.VarChar)
                If Len(plnr) > 0 Then
                    param020.Value = plnr
                Else
                    param020.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param020)

                adm.UpdateHack(cmd)

                'sql = "insert into PMSysUsers (email, phonenum, passwrd, username, compid, uid, dfltps, groupindex, groupname, status, readonly, admin) values " _
                '+ "('" & ema & "', '" & pho & "', '" & pass & "', '" & sname & "', '" & cid & "', '" & uid & "', '" & dps & "', '6', 'pmnomanuser', 'active', '" & ro & "', '" & admn & "'); " '_
                '+ "delete from pmUserApps where app = 'uap' or app = 'ua' and uid = '" & uid & "'"
                'adm.Update(sql)

                sql = "insert into pmusersites (uid, siteid) values ('" & uid & "', '" & dps & "')"
                adm.Update(sql)
                lbluserid.Value = ""
                UpUser2(uid, admn)
                GetTemp()
                LoadUsers(PageNumber, cid)
                GetStats(cid, ps)
                'Try
                SendIt("new", ema, uid)
                'Catch ex As Exception
                'Dim strMessage As String = tmod.getmsg("cdstr1692", "useradmin2.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'End Try
                'txtuserid.Text = ""
                txtnewuid.Text = ""
                lblnewuid.Value = ""
                txtusernamenew.Text = ""
                txtphonenew.Text = ""
                txtemailnew.Text = ""
                rbexpiresnew.SelectedValue = "N"
                txtexpdnew.Text = ""
                'newuappsdiv.InnerHtml = ""
                dvnewapps.InnerHtml = ""
                ddpsnew.SelectedIndex = 0
                lblapps.Value = ""
                lblappstr.Value = ""
                txtsupern.Text = ""
                lblsuperidrn.Value = ""
                lblsuperindexrn.Value = ""
                txtskilln.Text = ""
                lblskillidrn.Value = ""
                lblskillindexrn.Value = ""
                lbltempid.Value = ""
                lblshift.Value = ""
                cbroa.Checked = False
                cbadmina.Checked = False
                cblabora.Checked = False
                cbsupera.Checked = False
                If rbexpiresnew.SelectedValue.ToString = "Y" Then
                    exp = txtexpdnew.Text
                    If Len(exp) <> 0 Then
                        sql = "update PMSysUsers set exp = '" & exp & "' where uid = '" & uid & "'"
                        adm.Update(sql)
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1693", "useradmin2.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        Exit Sub
                    End If
                End If
            Else
                Dim strMessage As String = tmod.getmsg("cdstr1694", "useradmin2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If


        End If
        lblapps.Value = ""
        lblappstr.Value = ""

        LoadUsers(PageNumber, cid)
        GetSites(cid)
        GetStats(cid, ps)
        adm.Dispose()
    End Sub
    Private Sub GetApps(ByVal uid As String, Optional ByVal fld As String = "cur")
        Dim sb As New System.Text.StringBuilder
        sql = "select distinct u.app, p.appname from pmUserApps u left join pmApps p on " _
                        + "p.app = u.app where u.uid = '" & uid & "' "
        adm.Open()
        dr = adm.GetRdrData(sql)
        While dr.Read
            sb.Append(dr.Item("appname").ToString() & "<br>")
        End While
        dr.Close()
        adm.Dispose()
        If fld = "cur" Then
            'tdapps.InnerHtml = sb.ToString
            dvapps.InnerHtml = sb.ToString
        ElseIf fld = "new" Then
            'newuappsdiv.InnerHtml = sb.ToString
            dvnewapps.InnerHtml = sb.ToString
        End If

    End Sub
    Private Sub GetStats(ByVal cid As String, ByVal ps As String)
        Dim uc As Integer = adm.Users(cid) '5
        tdlic.InnerHtml = uc
        lbllic.Value = uc
        Dim au As Integer
        sql = "select count(*) from pmsysusers where compid = '" & cid & "' and status = 'active' and userid <> 0" 'and dfltps = '" & ps & "'
        Try
            au = adm.Scalar(sql)
        Catch ex As Exception
            au = 0
        End Try
        tdusers.InnerHtml = au
        lblusers.Value = au

        If au < uc Then
            lblnew.Value = "ok"
            'Dim strMessage As String = lblnew.Value
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            lblnew.Value = "no"
        End If
    End Sub
    Private Sub SendIt(ByVal typ As String, ByVal ema As String, ByVal uid As String)
        Dim email As New System.Web.Mail.MailMessage
        Dim lid, pas As String
        lid = txtnewuid.Text
        pas = txtp2new.Text
        Dim str14, str4, str5, port, spass, sauth, sfrom As String
        str14 = System.Configuration.ConfigurationManager.AppSettings("custSMTP")
        str4 = System.Configuration.ConfigurationManager.AppSettings("custSendUser")
        str5 = System.Configuration.ConfigurationManager.AppSettings("custSendUsing")
        port = System.Configuration.ConfigurationManager.AppSettings("custSMTPPort")
        spass = System.Configuration.ConfigurationManager.AppSettings("custSendPass")
        sauth = System.Configuration.ConfigurationManager.AppSettings("custAMTPAuth")
        sfrom = System.Configuration.ConfigurationManager.AppSettings("custSendFrom")
        email.To = ema
        email.From = sfrom
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = str5
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = str14
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = str4
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = spass
        email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = sauth
        email.Fields.Item("http://schemas.Microsoft.com/cdo/configuration/smtpserverport") = port

        Dim intralog As New mmenu_utils_a
        Dim isintra As Integer = intralog.INTRA
        If isintra <> 1 Then
            email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"
        End If
        '*** uncomment below for niss sym while on LAI web server
        'email.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverpickupdirectory") = "c:\Inetpub\mailroot\pickup"

        email.Body = AlertBody(lid, pas)
        email.Subject = "Your New PMO Login ID"
        email.BodyFormat = Web.Mail.MailFormat.Html
        System.Web.Mail.SmtpMail.SmtpServer.Insert(0, str14)

        Try
            System.Web.Mail.SmtpMail.Send(email)
        Catch ex As Exception
            Try
                Dim mail As New System.Net.Mail.MailMessage
                mail.From = New System.Net.Mail.MailAddress(sfrom)
                mail.To.Add(ema)
                mail.BodyEncoding = Encoding.Unicode
                mail.Subject = "Your New PMO Login ID"
                mail.Body = AlertBody(lid, pas)
                mail.IsBodyHtml = True
                Dim smtp As SmtpClient = New SmtpClient(str14)
                smtp.Send(mail)
            Catch ex0 As Exception
                Dim strMessage As String = "Problem Sending Email"
                adm.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try

        End Try

    End Sub
    Private Function AlertBody(ByVal lid As String, ByVal pas As String) As String
        Dim body As String
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Dim appname As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim mu As New mmenu_utils_a
        body = "<table width='800px' style='font-size:10pt; font-family:Verdana;'>"
        body &= "<tr><td>" & mu.AppIntro & "<br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>Your New User ID is " & lid & "<br></td></tr>" & vbCrLf
        body &= "<tr><td>And Your New Password is " & pas & "<br></td></tr>" & vbCrLf & vbCrLf
        body &= "<tr><td>" & mu.AppAccess & "&nbsp;&nbsp;<a href='" & urlname & "'>" & mu.AppLink & "</td></tr>" & vbCrLf & vbCrLf
        body &= "</table>"
        Return body
    End Function
    Private Sub GetSites(ByVal cid As String)
        Dim scnt As Integer
        admin = lbladmin.Value
        cadm = lblcadm.Value
        If admin = "pmadmin" Or admin = "pmadmin1" Or cadm = "1" Then
            sql = "select count(*) from sites where compid = '" & cid & "'"
        Else
            sql = "select count(*) from pmUserSites where uid = '" & admin & "'"
        End If
        cadm = lblcadm.Value
        scnt = adm.Scalar(sql)
        If scnt > 0 Then
            If admin = "pmadmin" Or admin = "pmadmin1" Or cadm = "1" Then
                sql = "select siteid, sitename from sites where compid = '" & cid & "'"
            Else
                sql = "select u.siteid, s.sitename from pmUserSites u " _
                + "left join sites s on s.siteid = u.siteid where u.uid = '" & admin & "'"
            End If
            dr = adm.GetRdrData(sql)
            ddps.DataSource = dr
            ddps.DataValueField = "siteid"
            ddps.DataTextField = "sitename"
            ddps.DataBind()
            dr.Close()
            ddps.Items.Insert(0, "Select Plant Site")
            ddps.Enabled = True
            dr = adm.GetRdrData(sql)
            ddpsnew.DataSource = dr
            ddpsnew.DataValueField = "siteid"
            ddpsnew.DataTextField = "sitename"
            ddpsnew.DataBind()
            dr.Close()
            ddpsnew.Items.Insert(0, "Select Plant Site")
            ddpsnew.Enabled = True
        Else
            ddps.Items.Insert(0, "Select Plant Site")
            ddps.Enabled = False
            ddpsnew.Items.Insert(0, "Select Plant Site")
            ddpsnew.Enabled = True
        End If

    End Sub
    Private Sub LoadUsers(ByVal PageNumber As Integer, ByVal cid As String) ', ByVal ps As String
        Dim intPgCnt As Integer
        cadm = lblcadm.Value
        admin = lbladmin.Value
        Dim filter, filtercnt As String
        Dim srch As String = txtsrch.Text
        If srch <> "" Then
            filter = srch
            filter = Replace(filter, "'", Chr(180), , , vbTextCompare)
        End If
        If admin = "pmadmin" Or admin = "pmadmin1" Or cadm = "1" Then
            sql = "select count(*) from pmsysusers where compid = '" & cid & "' and userid like '%" & filter & "%' or uid like '%" & filter & "%'" ' and admin <> 1" ' and dfltps = '" & ps & "'"
        Else
            sql = "select count(*) from pmsysusers where compid = '" & cid & "' " _
            + "and uid in (select distinct uid from pmUserSites where " _
            + "siteid in (select siteid from pmUserSites where uid = '" & admin & "')) and userid like '%" & filter & "%' or uid like '%" & filter & "%'"""
        End If


        'intPgCnt = adm.Scalar(sql)
        txtipg.Value = PageNumber
        'intPgNav = adm.PageCount(intPgCnt, PageSize)

        intPgNav = adm.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblipg.Text = "Page 0 of 0"
        Else
            lblipg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtipgcnt.Value = intPgNav


        If admin = "pmadmin" Or admin = "pmadmin1" Or cadm = "1" Then
            sql = "usp_getAllUserscadm '" & cid & "', '" & PageNumber & "', '" & PageSize & "','" & filter & "'"
        Else
            sql = "usp_getAllUserspsadm '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & admin & "','" & filter & "'"
        End If
        dr = adm.GetRdrData(sql)
        dgusers.DataSource = dr
        dgusers.DataBind()
        dr.Close()
        'updatepos(intPgNav)
    End Sub
    Private Sub GetiNext()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg + 1
            txtipg.Value = PageNumber
            LoadUsers(PageNumber, "0")
        Catch ex As Exception
            adm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1695", "useradmin2.aspx.vb")

            adm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetiPrev()
        Try
            Dim pg As Integer = txtipg.Value
            PageNumber = pg - 1
            txtipg.Value = PageNumber
            LoadUsers(PageNumber, "0")
        Catch ex As Exception
            adm.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr1696", "useradmin2.aspx.vb")

            adm.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Public Sub GetUser(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rb As RadioButton = New RadioButton
        Dim uid, user, phone, email, grp, status, exp, expd, dps, ms, ro, admn, isl, iss, sht, usid, plnr, ouname As String
        Dim sk, ski, su, sui As String
        rb = sender
        txtuserid.Text = rb.ClientID
        For Each i As RepeaterItem In dgusers.Items
            If i.ItemType <> ListItemType.AlternatingItem Then
                rb = CType(i.FindControl("rbuid"), RadioButton)
                user = CType(i.FindControl("lblusername"), Label).Text

                phone = CType(i.FindControl("lblphone"), Label).Text
                email = CType(i.FindControl("lblemail"), Label).Text
                grp = CType(i.FindControl("lblindex"), Label).Text
                status = CType(i.FindControl("lblstatus"), Label).Text
                exp = CType(i.FindControl("lblexp"), Label).Text
                expd = CType(i.FindControl("lblexpd"), Label).Text
                dps = CType(i.FindControl("lbldps"), Label).Text
                ps = CType(i.FindControl("lblps"), Label).Text
                ms = CType(i.FindControl("lblms"), Label).Text
                ro = CType(i.FindControl("lblro"), Label).Text
                admn = CType(i.FindControl("lbladmini"), Label).Text
                isl = CType(i.FindControl("lblislabor"), Label).Text
                iss = CType(i.FindControl("lblissuper"), Label).Text
                sht = CType(i.FindControl("lblshifti"), Label).Text
                usid = CType(i.FindControl("lbluseridi"), Label).Text
                plnr = CType(i.FindControl("lblplnri"), Label).Text

                ski = CType(i.FindControl("lblskillid"), Label).Text
                sk = CType(i.FindControl("lblskill"), Label).Text
                sui = CType(i.FindControl("lblsuperid"), Label).Text
                su = CType(i.FindControl("lblsuper"), Label).Text

                rb.Checked = False
            Else
                rb = CType(i.FindControl("rbuida"), RadioButton)
                user = CType(i.FindControl("lblusernamea"), Label).Text

                phone = CType(i.FindControl("lblphonea"), Label).Text
                email = CType(i.FindControl("lblemaila"), Label).Text
                grp = CType(i.FindControl("lblindexa"), Label).Text
                status = CType(i.FindControl("lblstatusa"), Label).Text
                exp = CType(i.FindControl("lblexpa"), Label).Text
                expd = CType(i.FindControl("lblexpda"), Label).Text
                dps = CType(i.FindControl("lbldpsa"), Label).Text
                ps = CType(i.FindControl("lblpsa"), Label).Text
                ms = CType(i.FindControl("lblmsa"), Label).Text
                ro = CType(i.FindControl("lblroa"), Label).Text
                admn = CType(i.FindControl("lbladmina"), Label).Text
                isl = CType(i.FindControl("lblislabora"), Label).Text
                iss = CType(i.FindControl("lblissupera"), Label).Text
                sht = CType(i.FindControl("lblshifta"), Label).Text
                usid = CType(i.FindControl("lbluserida"), Label).Text
                plnr = CType(i.FindControl("lblplnre"), Label).Text

                ski = CType(i.FindControl("lblskillida"), Label).Text
                sk = CType(i.FindControl("lblskilla"), Label).Text
                sui = CType(i.FindControl("lblsuperida"), Label).Text
                su = CType(i.FindControl("lblsupera"), Label).Text

                rb.Checked = False
            End If

            If txtuserid.Text = rb.ClientID Then
                rb.Checked = True
                txtuserid.Text = rb.Text.Trim
                uid = txtuserid.Text
                lbluserid.Value = rb.Text.Trim
                txtusername.Text = user
                lblouname.Value = user
                txtphone.Text = phone
                txtemail.Text = email
                rbactive.SelectedValue = status
                If exp = "Y" Then
                    rbxy.Checked = True
                Else
                    rbxn.Checked = True
                End If
                'rbexpires.SelectedValue = exp
                txtexpd.Text = expd
                lbluseridr.Value = usid
                lblshift.Value = sht
                txtskillo.Text = sk
                lblskillidr.Value = ski
                txtsupero.Text = su
                lblsuperidr.Value = sui
                If sht = "" Then
                    sht = "0"
                End If
                lblshift.Value = sht
                Try
                    ddshifto.SelectedValue = sht
                Catch ex As Exception

                End Try

                If ps = "0" Then
                    'tdpwd.InnerHtml = "No Password Created For This User"
                Else
                    'tdpwd.InnerHtml = ""
                End If
                If ro = "1" Then
                    cbro.Checked = True
                Else
                    cbro.Checked = False
                End If
                If admn = "1" Then
                    cbadmin.Checked = True
                Else
                    cbadmin.Checked = False
                End If
                If isl = "1" Then
                    cblabor.Checked = True
                Else
                    cblabor.Checked = False
                End If
                If iss = "1" Then
                    cbsuper.Checked = True
                    lblcbsuperchk.Value = "1"
                Else
                    cbsuper.Checked = False
                    lblcbsuperchk.Value = "0"
                End If
                If plnr = "1" Then
                    cbplanner.Checked = True
                Else
                    cbplanner.Checked = False
                End If
                GetApps(uid)
                lbldps.Value = dps
                ddps.SelectedValue = dps

                'txtuserid.Text = ""
                txtnewuid.Text = ""
                lblnewuid.Value = ""
                txtusernamenew.Text = ""
                txtphonenew.Text = ""
                txtemailnew.Text = ""
                rbexpiresnew.SelectedValue = "N"
                txtexpdnew.Text = ""
                'newuappsdiv.InnerHtml = ""
                dvnewapps.InnerHtml = ""
                ddpsnew.SelectedIndex = 0
                lblapps.Value = ""
                lblappstr.Value = ""
                txtsupern.Text = ""
                lblsuperidrn.Value = ""
                lblsuperindexrn.Value = ""
                txtskilln.Text = ""
                lblskillidrn.Value = ""
                lblskillindexrn.Value = ""
                lbltempid.Value = ""
                lblshift.Value = ""
                cbroa.Checked = False
                cbadmina.Checked = False
                cblabora.Checked = False
                cbsupera.Checked = False
                ddshiftn.SelectedValue = "0"
            End If
        Next
    End Sub

    Private Sub dgusers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles dgusers.ItemDataBound
        uid = lbluserid.Value
        Dim user, phone, email, grp, status, exp, expd, dps
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            If e.Item.ItemType <> ListItemType.AlternatingItem Then
                Dim rb As RadioButton = CType(e.Item.FindControl("rbuid"), RadioButton)
                If rb.Text.Trim = uid Then
                    rb.Checked = True
                    user = CType(e.Item.FindControl("lblusername"), Label).Text
                    phone = CType(e.Item.FindControl("lblphone"), Label).Text
                    email = CType(e.Item.FindControl("lblemail"), Label).Text
                    grp = CType(e.Item.FindControl("lblindex"), Label).Text
                    status = CType(e.Item.FindControl("lblstatus"), Label).Text
                    exp = CType(e.Item.FindControl("lblexp"), Label).Text
                    expd = CType(e.Item.FindControl("lblexpd"), Label).Text
                    dps = CType(e.Item.FindControl("lbldps"), Label).Text
                    txtuserid.Text = rb.Text.Trim
                    lbluserid.Value = rb.Text.Trim
                    txtusername.Text = user
                    lblouname.Value = user
                    txtphone.Text = phone
                    txtemail.Text = email
                    rbactive.SelectedValue = status
                    If exp = "Y" Then
                        rbxy.Checked = True
                    Else
                        rbxn.Checked = True
                    End If
                    'rbexpires.SelectedValue = exp
                    txtexpd.Text = expd

                End If
            Else
                Dim rb As RadioButton = CType(e.Item.FindControl("rbuida"), RadioButton)
                If rb.Text.Trim = uid Then
                    rb.Checked = True
                    user = CType(e.Item.FindControl("lblusernamea"), Label).Text
                    phone = CType(e.Item.FindControl("lblphonea"), Label).Text
                    email = CType(e.Item.FindControl("lblemaila"), Label).Text
                    grp = CType(e.Item.FindControl("lblindexa"), Label).Text
                    status = CType(e.Item.FindControl("lblstatusa"), Label).Text
                    exp = CType(e.Item.FindControl("lblexpa"), Label).Text
                    expd = CType(e.Item.FindControl("lblexpda"), Label).Text
                    dps = CType(e.Item.FindControl("lbldpsa"), Label).Text
                    txtuserid.Text = rb.Text.Trim
                    lbluserid.Value = rb.Text.Trim
                    txtusername.Text = user
                    lblouname.Value = user
                    txtphone.Text = phone
                    txtemail.Text = email
                    rbactive.SelectedValue = status
                    If exp = "Y" Then
                        rbxy.Checked = True
                    Else
                        rbxn.Checked = True
                    End If
                    'rbexpires.SelectedValue = exp
                    txtexpd.Text = expd

                End If
            End If
        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang3520 As Label
                lang3520 = CType(e.Item.FindControl("lang3520"), Label)
                lang3520.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3520")
            Catch ex As Exception
            End Try
            Try
                Dim lang3521 As Label
                lang3521 = CType(e.Item.FindControl("lang3521"), Label)
                lang3521.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3521")
            Catch ex As Exception
            End Try
            Try
                Dim lang3522 As Label
                lang3522 = CType(e.Item.FindControl("lang3522"), Label)
                lang3522.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3522")
            Catch ex As Exception
            End Try
            Try
                Dim lang3523 As Label
                lang3523 = CType(e.Item.FindControl("lang3523"), Label)
                lang3523.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3523")
            Catch ex As Exception
            End Try
            Try
                Dim lang3524 As Label
                lang3524 = CType(e.Item.FindControl("lang3524"), Label)
                lang3524.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3524")
            Catch ex As Exception
            End Try
            Try
                Dim lang3525 As Label
                lang3525 = CType(e.Item.FindControl("lang3525"), Label)
                lang3525.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3525")
            Catch ex As Exception
            End Try
            Try
                Dim lang3526 As Label
                lang3526 = CType(e.Item.FindControl("lang3526"), Label)
                lang3526.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3526")
            Catch ex As Exception
            End Try
            Try
                Dim lang3527 As Label
                lang3527 = CType(e.Item.FindControl("lang3527"), Label)
                lang3527.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3527")
            Catch ex As Exception
            End Try
            Try
                Dim lang3528 As Label
                lang3528 = CType(e.Item.FindControl("lang3528"), Label)
                lang3528.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3528")
            Catch ex As Exception
            End Try
            Try
                Dim lang3529 As Label
                lang3529 = CType(e.Item.FindControl("lang3529"), Label)
                lang3529.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3529")
            Catch ex As Exception
            End Try
            Try
                Dim lang3530 As Label
                lang3530 = CType(e.Item.FindControl("lang3530"), Label)
                lang3530.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3530")
            Catch ex As Exception
            End Try
            Try
                Dim lang3531 As Label
                lang3531 = CType(e.Item.FindControl("lang3531"), Label)
                lang3531.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3531")
            Catch ex As Exception
            End Try
            Try
                Dim lang3532 As Label
                lang3532 = CType(e.Item.FindControl("lang3532"), Label)
                lang3532.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3532")
            Catch ex As Exception
            End Try
            Try
                Dim lang3533 As Label
                lang3533 = CType(e.Item.FindControl("lang3533"), Label)
                lang3533.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3533")
            Catch ex As Exception
            End Try
            Try
                Dim lang3534 As Label
                lang3534 = CType(e.Item.FindControl("lang3534"), Label)
                lang3534.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3534")
            Catch ex As Exception
            End Try
            Try
                Dim lang3535 As Label
                lang3535 = CType(e.Item.FindControl("lang3535"), Label)
                lang3535.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3535")
            Catch ex As Exception
            End Try
            Try
                Dim lang3536 As Label
                lang3536 = CType(e.Item.FindControl("lang3536"), Label)
                lang3536.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3536")
            Catch ex As Exception
            End Try
            Try
                Dim lang3537 As Label
                lang3537 = CType(e.Item.FindControl("lang3537"), Label)
                lang3537.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3537")
            Catch ex As Exception
            End Try
            Try
                Dim lang3538 As Label
                lang3538 = CType(e.Item.FindControl("lang3538"), Label)
                lang3538.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3538")
            Catch ex As Exception
            End Try
            Try
                Dim lang3539 As Label
                lang3539 = CType(e.Item.FindControl("lang3539"), Label)
                lang3539.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3539")
            Catch ex As Exception
            End Try
            Try
                Dim lang3540 As Label
                lang3540 = CType(e.Item.FindControl("lang3540"), Label)
                lang3540.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3540")
            Catch ex As Exception
            End Try
            Try
                Dim lang3541 As Label
                lang3541 = CType(e.Item.FindControl("lang3541"), Label)
                lang3541.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3541")
            Catch ex As Exception
            End Try
            Try
                Dim lang3542 As Label
                lang3542 = CType(e.Item.FindControl("lang3542"), Label)
                lang3542.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3542")
            Catch ex As Exception
            End Try
            Try
                Dim lang3543 As Label
                lang3543 = CType(e.Item.FindControl("lang3543"), Label)
                lang3543.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3543")
            Catch ex As Exception
            End Try
            Try
                Dim lang3544 As Label
                lang3544 = CType(e.Item.FindControl("lang3544"), Label)
                lang3544.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3544")
            Catch ex As Exception
            End Try
            Try
                Dim lang3545 As Label
                lang3545 = CType(e.Item.FindControl("lang3545"), Label)
                lang3545.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3545")
            Catch ex As Exception
            End Try
            Try
                Dim lang3546 As Label
                lang3546 = CType(e.Item.FindControl("lang3546"), Label)
                lang3546.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3546")
            Catch ex As Exception
            End Try
            Try
                Dim lang3547 As Label
                lang3547 = CType(e.Item.FindControl("lang3547"), Label)
                lang3547.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3547")
            Catch ex As Exception
            End Try
            Try
                Dim lang3548 As Label
                lang3548 = CType(e.Item.FindControl("lang3548"), Label)
                lang3548.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3548")
            Catch ex As Exception
            End Try
            Try
                Dim lang3549 As Label
                lang3549 = CType(e.Item.FindControl("lang3549"), Label)
                lang3549.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3549")
            Catch ex As Exception
            End Try
            Try
                Dim lang3550 As Label
                lang3550 = CType(e.Item.FindControl("lang3550"), Label)
                lang3550.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3550")
            Catch ex As Exception
            End Try
            Try
                Dim lang3551 As Label
                lang3551 = CType(e.Item.FindControl("lang3551"), Label)
                lang3551.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3551")
            Catch ex As Exception
            End Try
            Try
                Dim lang3552 As Label
                lang3552 = CType(e.Item.FindControl("lang3552"), Label)
                lang3552.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3552")
            Catch ex As Exception
            End Try
            Try
                Dim lang3553 As Label
                lang3553 = CType(e.Item.FindControl("lang3553"), Label)
                lang3553.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3553")
            Catch ex As Exception
            End Try
            Try
                Dim lang3554 As Label
                lang3554 = CType(e.Item.FindControl("lang3554"), Label)
                lang3554.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3554")
            Catch ex As Exception
            End Try
            Try
                Dim lang3555 As Label
                lang3555 = CType(e.Item.FindControl("lang3555"), Label)
                lang3555.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3555")
            Catch ex As Exception
            End Try
            Try
                Dim lang3556 As Label
                lang3556 = CType(e.Item.FindControl("lang3556"), Label)
                lang3556.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3556")
            Catch ex As Exception
            End Try
            Try
                Dim lang3557 As Label
                lang3557 = CType(e.Item.FindControl("lang3557"), Label)
                lang3557.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3557")
            Catch ex As Exception
            End Try
            Try
                Dim lang3558 As Label
                lang3558 = CType(e.Item.FindControl("lang3558"), Label)
                lang3558.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3558")
            Catch ex As Exception
            End Try
            Try
                Dim lang3559 As Label
                lang3559 = CType(e.Item.FindControl("lang3559"), Label)
                lang3559.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3559")
            Catch ex As Exception
            End Try
            Try
                Dim lang3560 As Label
                lang3560 = CType(e.Item.FindControl("lang3560"), Label)
                lang3560.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3560")
            Catch ex As Exception
            End Try

        End If

    End Sub

    Private Sub btnsubmitprofile_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsubmitprofile.Click
        Dim name, phone, email, uid, ms, ro, ouid, admn, isl, iss, tmp, sht, plnr, ouname As String
        name = txtusername.Text
        Dim ustr As String = Replace(name, "'", Chr(180), , , vbTextCompare)
        If Len(ustr) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1697", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(ustr) = 0 Then
            txtusername.Text = lblouname.Value
            Dim strMessage As String = tmod.getmsg("cdstr1698", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ouname = lblouname.Value
        phone = txtphone.Text
        phone = Replace(phone, "'", Chr(180), , , vbTextCompare)
        If Len(phone) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1699", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        email = txtemail.Text
        email = Replace(email, "'", "''")
        uid = txtuserid.Text
        uid = Replace(uid, "'", Chr(180), , , vbTextCompare)
        If Len(uid) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr1700", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(uid) = 0 Then
            txtuserid.Text = lbluserid.Value
            Dim strMessage As String = tmod.getmsg("cdstr1701", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        ouid = lbluserid.Value
        cid = lblcid.Value
        ps = ddps.SelectedValue
        If cbro.Checked = True Then
            ro = "1"
        Else
            ro = "0"
        End If
        If cbadmin.Checked = True Then
            admn = "1"
            ro = "0"
            cbro.Checked = False
        Else
            admn = "0"
        End If
        If cblabor.Checked = True Then
            isl = "1"
            ro = "0"
            cbro.Checked = False
        Else
            isl = "0"
        End If
        If cbsuper.Checked = True Then
            iss = "1"
            ro = "0"
            cbro.Checked = False
        Else
            iss = "0"
        End If
        If cbplanner.Checked = True Then
            plnr = "1"
        Else
            plnr = "0"
        End If
        Dim super, superi, superin, skill, skilli, skillin As String
        super = txtsupero.Text
        superi = lblsuperidr.Value
        superin = lblsuperindexr.Value
        skill = txtskillo.Text
        skilli = lblskillidr.Value
        skillin = lblskillindexr.Value

        'sht = lblshift.Value
        'If sht = "" Then
        Try
            sht = ddshifto.SelectedValue.ToString()
        Catch ex As Exception
            sht = "0"
        End Try

        'End If

        adm.Open()
        If ddps.SelectedIndex <> 0 Then
            Dim ui As String = lbluserid.Value
            Dim uidcnt As Integer
            Dim ucnt, ncnt As String
            If uid <> ouid Then
                sql = "select count(uid) from PMSysUsers where uid = '" & uid & "'"
                ucnt = adm.Scalar(sql)
            Else
                ucnt = 0
            End If
            If ustr <> ouname Then
                sql = "select count(username) from PMSysUsers where username = '" & ustr & "' "
                ncnt = adm.Scalar(sql)
            Else
                ncnt = 0
            End If

            If ucnt = 0 And ncnt = 0 Then
                Dim cmd As New SqlCommand("exec usp_upuser1 @email, @phonenum, @username, " _
                + "@compid, @uid, @dfltps, @readonly, @admin, " _
                + "@super, @superindex, @superid, " _
                + "@skill, @skillindex, @skillid, " _
                + "@islabor, @issuper, @shift, @plnr")

                Dim param01 = New SqlParameter("@email", SqlDbType.VarChar)
                If Len(email) > 0 Then
                    param01.Value = email
                Else
                    param01.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param01)
                Dim param02 = New SqlParameter("@phonenum", SqlDbType.VarChar)
                If Len(phone) > 0 Then
                    param02.Value = phone
                Else
                    param02.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param02)

                Dim param04 = New SqlParameter("@username", SqlDbType.VarChar)
                If Len(ustr) > 0 Then
                    param04.Value = ustr
                Else
                    param04.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param04)
                Dim param05 = New SqlParameter("@compid", SqlDbType.VarChar)
                If Len(cid) > 0 Then
                    param05.Value = cid
                Else
                    param05.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param05)
                Dim param06 = New SqlParameter("@uid", SqlDbType.VarChar)
                If Len(uid) > 0 Then
                    param06.Value = uid
                Else
                    param06.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param06)
                Dim param07 = New SqlParameter("@dfltps", SqlDbType.VarChar)
                If Len(ps) > 0 Then
                    param07.Value = ps
                Else
                    param07.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param07)
                Dim param08 = New SqlParameter("@readonly", SqlDbType.VarChar)
                If Len(ro) > 0 Then
                    param08.Value = ro
                Else
                    param08.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param08)
                Dim param09 = New SqlParameter("@admin", SqlDbType.VarChar)
                If Len(admn) > 0 Then
                    param09.Value = admn
                Else
                    param09.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param09)
                Dim param010 = New SqlParameter("@super", SqlDbType.VarChar)
                If Len(super) > 0 Then
                    param010.Value = super
                Else
                    param010.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param010)
                Dim param011 = New SqlParameter("@superindex", SqlDbType.VarChar)
                If Len(superin) > 0 Then
                    param011.Value = superin
                Else
                    param011.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param011)
                Dim param012 = New SqlParameter("@superid", SqlDbType.VarChar)
                If Len(superi) > 0 Then
                    param012.Value = superi
                Else
                    param012.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param012)
                Dim param013 = New SqlParameter("@skill", SqlDbType.VarChar)
                If Len(skill) > 0 Then
                    param013.Value = skill
                Else
                    param013.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param013)
                Dim param014 = New SqlParameter("@skillindex", SqlDbType.VarChar)
                If Len(skillin) > 0 Then
                    param014.Value = skillin
                Else
                    param014.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param014)
                Dim param015 = New SqlParameter("@skillid", SqlDbType.VarChar)
                If Len(skilli) > 0 Then
                    param015.Value = skilli
                Else
                    param015.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param015)
                Dim param016 = New SqlParameter("@islabor", SqlDbType.VarChar)
                If Len(isl) > 0 Then
                    param016.Value = isl
                Else
                    param016.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param016)
                Dim param017 = New SqlParameter("@issuper", SqlDbType.VarChar)
                If Len(iss) > 0 Then
                    param017.Value = iss
                Else
                    param017.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param017)
                Dim param018 = New SqlParameter("@shift", SqlDbType.VarChar)
                If Len(sht) > 0 Then
                    param018.Value = sht
                Else
                    param018.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param018)
                Dim param019 = New SqlParameter("@plnr", SqlDbType.VarChar)
                If Len(plnr) > 0 Then
                    param019.Value = plnr
                Else
                    param019.Value = System.DBNull.Value
                End If
                cmd.Parameters.Add(param019)

                adm.UpdateHack(cmd)

                If uid <> ouid Then
                    sql = "update pmUserApps set uid = '" & uid & "' where uid = '" & ouid & "'; " _
                    + "update pmUserSites set uid = '" & uid & "' where uid = '" & ouid & "'"
                    adm.Update(sql)
                End If

                lbluserid.Value = uid
                LoadUsers(PageNumber, cid)

                If cbsuper.Checked = True Then
                    lblcbsuperchk.Value = "1"
                Else
                    lblcbsuperchk.Value = "0"
                End If
            Else
                Dim strMessageq As String = tmod.getmsg("cdstr1702", "useradmin2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessageq, "strKey1")

                'Exit Sub
            End If
        Else
            Dim strMessage As String = tmod.getmsg("cdstr1703", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        LoadUsers(PageNumber, cid)
        GetSites(cid)
        GetStats(cid, ps)
        adm.Dispose()
        Try
            ddps.SelectedValue = ps
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnusercr_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnusercr.Click
        Dim exp As String '= txtexpd.Text
        Dim exps As String '= rbexpires.SelectedValue.ToString

        If rbxy.Checked = True Then
            exps = "Y"
        Else
            exps = "N"
        End If

        Dim act As String = rbactive.SelectedValue.ToString
        If exps = "Y" Then
            exp = txtexpd.Text
        Else
            exp = ""
        End If
        cid = lblcid.Value

        cid = lblcid.Value
        ps = lblsid.Value
        uid = txtuserid.Text
        sql = "update pmsysusers set status = '" & act & "', " _
       + "expires = '" & exps & "', expiration = '" & exp & "' where uid = '" & uid & "'"

        Dim cmd As New SqlCommand("exec usp_upuserexp @status, @expires, @expiration, @uid")

        Dim param01 = New SqlParameter("@status", SqlDbType.VarChar)
        If Len(act) > 0 Then
            param01.Value = act
        Else
            param01.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@expires", SqlDbType.VarChar)
        If Len(exps) > 0 Then
            param02.Value = exps
        Else
            param02.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@expiration", SqlDbType.VarChar)
        If Len(exp) > 0 Then
            param03.Value = exp
        Else
            param03.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@uid", SqlDbType.VarChar)
        If Len(uid) > 0 Then
            param04.Value = uid
        Else
            param04.Value = System.DBNull.Value
        End If
        cmd.Parameters.Add(param04)

        adm.Open()
        Dim uc As Integer = adm.Users(cid)
        If act = "active" Then
            If lblnew.Value = "ok" Then
                adm.UpdateHack(cmd)
            Else
                Dim strMessage As String = tmod.getmsg("cdstr1704", "useradmin2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                rbactive.SelectedValue = "inactive"
            End If
        Else
            adm.Update(sql)
        End If
        GetStats(cid, ps)
        PageNumber = txtipg.Value
        LoadUsers(PageNumber, "0")
        adm.Dispose()

    End Sub

    Private Sub btnsubmitpass_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsubmitpass.Click
        Dim ui, uid, p1, p2 As String
        uid = lbluserid.Value
        cid = lblcid.Value
        ps = lblsid.Value
        p1 = txtp1.Text
        p2 = txtp2.Text
        adm.Open()
        If Len(p1) = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr1705", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf Len(p2) = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr1706", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        ElseIf p1 <> p2 Then
            Dim strMessage As String = tmod.getmsg("cdstr1707", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            p2 = adm.Encrypt(p2)
            sql = "update PMSysUsers set passwrd = '" & p2 & "' where uid = '" & uid & "'"
            adm.Update(sql)
            'tdpwd.InnerHtml = ""
            Dim strMessage As String = tmod.getmsg("cdstr1708", "useradmin2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")

        End If
        Dim ema As String = txtemailnew.Text
        If ema <> "" Then
            SendIt("new", ema, uid)
        Else
            Dim strMessage As String = "No Email Address Found to Inform User"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang3520.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3520")
        Catch ex As Exception
        End Try
        Try
            lang3521.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3521")
        Catch ex As Exception
        End Try
        Try
            lang3522.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3522")
        Catch ex As Exception
        End Try
        Try
            lang3523.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3523")
        Catch ex As Exception
        End Try
        Try
            lang3524.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3524")
        Catch ex As Exception
        End Try
        Try
            lang3525.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3525")
        Catch ex As Exception
        End Try
        Try
            lang3526.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3526")
        Catch ex As Exception
        End Try
        Try
            lang3527.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3527")
        Catch ex As Exception
        End Try
        Try
            lang3528.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3528")
        Catch ex As Exception
        End Try
        Try
            lang3529.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3529")
        Catch ex As Exception
        End Try
        Try
            lang3530.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3530")
        Catch ex As Exception
        End Try
        Try
            lang3531.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3531")
        Catch ex As Exception
        End Try
        Try
            lang3532.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3532")
        Catch ex As Exception
        End Try
        Try
            lang3533.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3533")
        Catch ex As Exception
        End Try
        Try
            lang3534.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3534")
        Catch ex As Exception
        End Try
        Try
            lang3535.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3535")
        Catch ex As Exception
        End Try
        Try
            lang3536.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3536")
        Catch ex As Exception
        End Try
        Try
            lang3537.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3537")
        Catch ex As Exception
        End Try
        Try
            lang3538.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3538")
        Catch ex As Exception
        End Try
        Try
            lang3539.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3539")
        Catch ex As Exception
        End Try
        Try
            lang3540.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3540")
        Catch ex As Exception
        End Try
        Try
            lang3541.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3541")
        Catch ex As Exception
        End Try
        Try
            lang3542.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3542")
        Catch ex As Exception
        End Try
        Try
            lang3543.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3543")
        Catch ex As Exception
        End Try
        Try
            lang3544.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3544")
        Catch ex As Exception
        End Try
        Try
            lang3545.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3545")
        Catch ex As Exception
        End Try
        Try
            lang3546.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3546")
        Catch ex As Exception
        End Try
        Try
            lang3547.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3547")
        Catch ex As Exception
        End Try
        Try
            lang3548.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3548")
        Catch ex As Exception
        End Try
        Try
            lang3549.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3549")
        Catch ex As Exception
        End Try
        Try
            lang3550.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3550")
        Catch ex As Exception
        End Try
        Try
            lang3551.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3551")
        Catch ex As Exception
        End Try
        Try
            lang3552.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3552")
        Catch ex As Exception
        End Try
        Try
            lang3553.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3553")
        Catch ex As Exception
        End Try
        Try
            lang3554.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3554")
        Catch ex As Exception
        End Try
        Try
            lang3555.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3555")
        Catch ex As Exception
        End Try
        Try
            lang3556.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3556")
        Catch ex As Exception
        End Try
        Try
            lang3557.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3557")
        Catch ex As Exception
        End Try
        Try
            lang3558.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3558")
        Catch ex As Exception
        End Try
        Try
            lang3559.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3559")
        Catch ex As Exception
        End Try
        Try
            lang3560.Text = axlabs.GetASPXPage("useradmin2.aspx", "lang3560")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                addapp.Attributes.Add("src", "../images2/eng/bgbuttons/edit.gif")
            ElseIf lang = "fre" Then
                addapp.Attributes.Add("src", "../images2/fre/bgbuttons/edit.gif")
            ElseIf lang = "ger" Then
                addapp.Attributes.Add("src", "../images2/ger/bgbuttons/edit.gif")
            ElseIf lang = "ita" Then
                addapp.Attributes.Add("src", "../images2/ita/bgbuttons/edit.gif")
            ElseIf lang = "spa" Then
                addapp.Attributes.Add("src", "../images2/spa/bgbuttons/edit.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                addappnew.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                addappnew.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                addappnew.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                addappnew.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                addappnew.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnaddnewlogin.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnaddnewlogin.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnaddnewlogin.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnaddnewlogin.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnaddnewlogin.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnsubmitpass.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnsubmitpass.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnsubmitpass.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnsubmitpass.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnsubmitpass.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnsubmitprofile.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnusercr.Attributes.Add("src", "../images2/eng/bgbuttons/submit.gif")
            ElseIf lang = "fre" Then
                btnusercr.Attributes.Add("src", "../images2/fre/bgbuttons/submit.gif")
            ElseIf lang = "ger" Then
                btnusercr.Attributes.Add("src", "../images2/ger/bgbuttons/submit.gif")
            ElseIf lang = "ita" Then
                btnusercr.Attributes.Add("src", "../images2/ita/bgbuttons/submit.gif")
            ElseIf lang = "spa" Then
                btnusercr.Attributes.Add("src", "../images2/spa/bgbuttons/submit.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddnewfail.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("useradmin2.aspx", "btnaddnewfail") & "')")
            btnaddnewfail.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
