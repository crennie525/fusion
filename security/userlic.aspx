<%@ Page Language="vb" AutoEventWireup="false" Codebehind="userlic.aspx.vb" Inherits="lucy_r12.userlic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>userlic</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script language="JavaScript" src="../scripts1/userlicaspx.js"></script>
     <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td align="center" colSpan="3">
						<h2><asp:Label id="lang3565" runat="server">PM End User License Agreement</asp:Label></h2>
					</td>
				</tr>
				<tr>
					<td width="15%">&nbsp;</td>
					<td class="plainlabel" align="center" width="70%"><asp:Label id="lang3566" runat="server">Please review the license terms below before using this product.</asp:Label><br>
					</td>
					<td width="15%">&nbsp;</td>
				</tr>
				<tr>
					<td></td>
					<td class="label" align="center"><asp:Label id="lang3567" runat="server">Select Language</asp:Label><SELECT id="lang" onchange="getlang(this.value);">
							<OPTION value="0"><asp:Label id="lang3568" runat="server">Select</asp:Label></OPTION>
							<OPTION value="#English" selected>English</OPTION>
							<OPTION value="#French">French</OPTION>
							<OPTION value="#German">German</OPTION>
							<OPTION value="#Italian">Italian</OPTION>
						</SELECT></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td class="plainlabel" align="center"><font color="red"><asp:Label id="lang3569" runat="server">Please Note that while the PM End User License Agreement has been interpreted in multiple languages that LAI Reliability Systems retains the right to enforce the "English" version in the event that any legal actions are necessary.</asp:Label></font></td>
					<td></td>
				</tr>
				<tr>
					<td style="HEIGHT: 417px" colspan="3"><iframe id="iflic" src="LAI.htm" frameBorder="yes" width="920" height="420" runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="plainlabel" align="center" colspan="3"><asp:Label id="lang3570" runat="server">If you accept the terms of this agreement, click I AGREE to continue. You must accept the agreement to use the PM product</asp:Label></td>
				</tr>
				<tr>
					<td align="right" colspan="3"><asp:button id="btnaccept" runat="server" Text="I ACCEPT"></asp:button><asp:button id="btndont" runat="server" Text="I DO NOT ACCEPT"></asp:button></td>
				</tr>
			</table>
		
<input type="hidden" id="lblfslang" runat="server"/>
<input type="hidden" id="lblret" runat="server" />
</form>
	</body>
</HTML>
