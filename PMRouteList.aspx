<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PMRouteList.aspx.vb" Inherits="lucy_r12.PMRouteList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>PMRouteList</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script language="JavaScript" src="../scripts/gridnav.js"></script>
    <script language="JavaScript" src="../scripts1/PMRouteListaspx.js"></script>
    <script language="JavaScript" type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script language="javascript" type="text/javascript">
        function printpmr(rtid, typ) {
        //alert(rtid)
            var popwin = "directories=0,height=500,width=800,location=0,menubar=1,resizable=1,status=0,toolbar=1,scrollbars=1";
            if (typ == "RBAS") {
                window.open("../reports/pmrhtml7_new.aspx?rid=" + rtid, "repWin", popwin);
            }
            else if (typ == "RBAST") {
                window.open("../reports/pmrhtml7tpm_new.aspx?rid=" + rtid, "repWin", popwin);
            }
            else {
                window.open("../reports/pmrhtml3.aspx?rid=" + rtid, "repWin", popwin);
            }
            
        }
        function checkit() {
            window.setTimeout("setref();", 1205000);

        }
        function checktyp(who) {
            document.getElementById("lblrtyp").value = who;
            document.getElementById("lblsubmit").value = "swtch";
            document.getElementById("form1").submit();
        }
        function addrt(st) {
            var rtn = "rm"
            var typ = document.getElementById("lblrtyp").value;
            window.parent.handleadd(st, rtn, typ);
        }
        function rtret(id, typ) {
            //var typ = document.getElementById("lblrtyp").value;
            window.parent.handlert(id, typ);
        }
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; right: 5px; left: 5px" width="700">
        <tr>
            <td width="100">
            </td>
            <td width="140">
            </td>
            <td width="20">
            </td>
            <td width="20">
            </td>
            <td width="20">
            </td>
            <td width="430">
            </td>
        </tr>
        <tr>
            <td class="thdrsingg plainlabel" colspan="6">
                <asp:Label ID="lang1176" runat="server">Route List Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang1177" runat="server">Search Routes</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtsrch" runat="server" Width="120"></asp:TextBox>
            </td>
            <td>
                <img onmouseover="return overlib('Search Routes', ABOVE, LEFT)" onmouseout="return nd()"
                    onclick="srchrts();" src="../images/appbuttons/minibuttons/srchsm.gif">
            </td>
            <td>
                <img onmouseover="return overlib('Add/Edit Routes', ABOVE, LEFT)" onmouseout="return nd()"
                    onclick="addrt('no');" src="../images/appbuttons/minibuttons/addnew.gif">
            </td>
            <td colspan="2" class="plainlabel">
                <input type="radio" name="rptyp" id="rbpm" checked onclick="checktyp('PM');" runat="server" />PM&nbsp;&nbsp;
                <input type="radio" name="rptyp" id="rbpmj" onclick="checktyp('PMMAX');" runat="server" />PM Job Plan&nbsp;&nbsp;
                <input type="radio" name="rptyp" id="rbrbas" onclick="checktyp('RBAS');" runat="server" />Report Based&nbsp;&nbsp;
                <input type="radio" name="rptyp" id="rbrbast" onclick="checktyp('RBAST');" runat="server" />Report Based TPM
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang1178" runat="server">Sort By</asp:Label>
            </td>
            <td class="plainlabel">
                <asp:RadioButtonList ID="rbsort" runat="server" Width="200px" RepeatDirection="Horizontal"
                    Height="20px" CssClass="plainlabel">
                    <asp:ListItem Value="0" Selected="True">Route#</asp:ListItem>
                    <asp:ListItem Value="1">Type</asp:ListItem>
                    <asp:ListItem Value="2">Created By</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <img onmouseover="return overlib('Sort Ascending', ABOVE, LEFT)" onmouseout="return nd()"
                    onclick="sort('asc');" src="../images/appbuttons/minibuttons/sasc.gif">
            </td>
            <td>
                <img onmouseover="return overlib('Sort Descending', ABOVE, LEFT)" onmouseout="return nd()"
                    onclick="sort('desc');" src="../images/appbuttons/minibuttons/sdesc.gif">
            </td>
        </tr>
        <tr>
            <td id="tdlist" colspan="6" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="220">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid" width="20">
                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td width="20">
                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lblsid" type="hidden" runat="server">
    <input id="lblsubmit" type="hidden" runat="server">
    <input id="lblsort" type="hidden" runat="server">
    <input type="hidden" id="lbleqid" runat="server">
    <input type="hidden" id="lblfu" runat="server">
    <input type="hidden" id="lblco" runat="server"><input type="hidden" id="lblrtid"
        runat="server">
    <input type="hidden" id="txtpg" runat="server" name="txtpg">
    <input type="hidden" id="txtpgcnt" runat="server" name="txtpgcnt">
    <input type="hidden" id="lblret" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lbltyp" runat="server" />
    <input type="hidden" id="lblrtyp" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    </form>
</body>
</html>
