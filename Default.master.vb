Imports Izenda.AdHoc

Partial Class MasterPage1
  Inherits MasterPage

  Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
    If (HttpContext.Current Is Nothing OrElse HttpContext.Current.Session Is Nothing) Then
      Return
    End If
    Utility.CheckLimitations(True)
    Dim session As HttpSessionState
    session = HttpContext.Current.Session
    If (String.IsNullOrEmpty(session("ReportingInitialized")) OrElse DirectCast(session("ReportingInitialized"), Boolean) <> True) Then
      Return
    End If
		

		If (Not String.IsNullOrEmpty(AdHocSettings.ApplicationHeaderImageUrl)) Then
			rightLogo.Src = AdHocSettings.ApplicationHeaderImageUrl
		End If
		If (AdHocSettings.ShowDesignLinks = False) Then
			Dim script As String
			script = "<script type=""text/javascript"" language=""javascript"">"
			script += "try { $(document).ready(function() {$('.designer-only').hide(); });}catch(e){}"
			script += " try{ jq$(document).ready(function() {jq$('.designer-only').hide(); });}catch(e){} "
			script += "</script>"
			If Not (Page Is Nothing) Then
				Page.Header.Controls.Add(New LiteralControl(script))
			End If
		End If
		AdHocSettings.ShowSettingsButtonForNonAdmins = False
	End Sub

End Class
