

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PFInt_spa
    Inherits System.Web.UI.Page
	Protected WithEvents c2 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents c1 As System.Web.UI.HtmlControls.HtmlImage

    Dim tmod As New transmod
    Dim pf, lt As String
    Dim eqid, fuid, taskid, sql As String
    Dim pfint, pflt, pfans1, pfsf, pfri, login, ro As String
    Dim ecrlev, ecrlevo As String
    Dim pfu As New Utilities
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtpfi As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblecr As System.Web.UI.WebControls.Label
    Protected WithEvents ddsf As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtpfidup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtltdup As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtri As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtlt As System.Web.UI.WebControls.TextBox
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbans1 As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents txttaskid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfin As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpfi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ecrflag As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblans1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()
'Put user code to initialize the page here
        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()

        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            If lbllog.Value <> "no" Then
                Try
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro
                    If ro = "1" Then

                    End If
                    taskid = Request.QueryString("tid").ToString
                    If Len(taskid) <> 0 AndAlso taskid <> "" AndAlso taskid <> "0" Then
                        Dim ecrchk As Integer = 0
                        eqid = Request.QueryString("eqid").ToString
                        txttaskid.Value = taskid
                        sql = "usp_getPFInterval '" & taskid & "', '" & eqid & "'"
                        pfu.Open()
                        dr = pfu.GetRdrData(sql)
                        While dr.Read
                            pfint = dr("pfint")
                            pflt = dr("pflt")
                            pfans1 = dr("pfans1")
                            pfsf = dr("pfsf")
                            pfri = dr("pfri")
                            Try
                                ecrlev = dr("currecr")
                            Catch ex As Exception
                                ecrlev = "0"
                                ecrchk = 1
                            End Try
                            Try
                                ecrlevo = dr("eqecr")
                            Catch ex As Exception
                                ecrlevo = "0"
                                ecrchk = 1
                            End Try

                        End While
                        If ecrlev <> ecrlevo Then
                            ecrflag.Value = 1
                        Else
                            ecrflag.Value = 0
                        End If
                        Dim d As Decimal = ecrlevo
                        Dim currecr As Integer = Math.Round(d, 0)
                        Dim sf As Integer
                        Select Case d
                            Case Is > 8
                                sf = 3
                            Case Is > 4
                                sf = 2
                            Case Is > 0
                                sf = 1
                            Case 0
                                sf = 2
                        End Select
                        ddsf.SelectedValue = sf
                        txtpfi.Text = pfint
                        txtpfidup.Text = pfint
                        txtlt.Text = pflt
                        txtltdup.Text = pflt
                        txtri.Text = pfri
                        lblecr.Text = ecrlevo
                        pfu.Dispose()

                        txtpfi.Attributes.Add("onkeyup", "document.getElementById('txtpfidup').value=this.value;")
                        txtlt.Attributes.Add("onkeyup", "document.getElementById('txtltdup').value=this.value;")
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr989", "PFInt.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "noeqid"
                    End If
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr990", "PFInt.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End Try

            End If
        End If
        'ImageButton1.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ImageButton1.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label1.Text = axlabs.GetASPXPage("PFInt.aspx", "Label1")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ImageButton1.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ImageButton1.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ImageButton1.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ImageButton1.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ImageButton1.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        ro = lblro.Value
        If ro <> "1" Then
            taskid = txttaskid.Value

            pfint = txtpfi.Text
            Try
                pfint = System.Convert.ToInt32(pfint)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr991", "PFInt.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            pflt = txtlt.Text
            Try
                pflt = System.Convert.ToInt32(pflt)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr992", "PFInt.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            pfans1 = lblans1.Value
            pfsf = ddsf.SelectedValue
            pfri = lblpfi.Value 'txtri.Text
            lblpfi.Value = pfri
            ecrlev = lblecr.Text
            Dim ecrdec As Decimal = ecrlev
            sql = "usp_updatePFInterval '" & taskid & "', '" & pfint & "', '" & pflt & "', " _
            + "'" + pfans1 + "', '" & pfsf & "', '" & pfri & "', '" & ecrdec & "'"
            pfu.Open()
            pfu.Update(sql)
            pfu.Dispose()
        End If

        txtfin.Value = "1"
    End Sub
    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            c1.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PFInt_spa.aspx", "c1") & "')")
            c1.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            c2.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("PFInt_spa.aspx", "c2") & "')")
            c2.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
