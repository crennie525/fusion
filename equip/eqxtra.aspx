﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqxtra.aspx.vb" Inherits="lucy_r12.eqxtra" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript">
        function savex() {
            document.getElementById("lblsubmit").value = "go";
            document.getElementById("form1").submit();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="bluelabel" height="22">Equipment:</td>
    <td class="plainlabel" id="tdeqnum" runat="server"></td>
    </tr>
    <tr>
    <td class="label">POSEQIP</td>
    <td><asp:TextBox ID="txtPOSEQIP" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="label">ITEMGRP</td>
    <td><asp:TextBox ID="txtITEMGRP" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="label">STATUS</td>
    <td><asp:TextBox ID="txtSTATUS" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="label">SUBPROCESS</td>
    <td><asp:TextBox ID="txtSUBPROCESS" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="label">RESPONSIBLE</td>
    <td><asp:TextBox ID="txtRESPONSIBLE" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="label">FIXEDASSET</td>
    <td><asp:TextBox ID="txtFIXEDASSET" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="label">PRIORITY</td>
    <td><asp:TextBox ID="txtPRIORITY" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="label">CRITICALITYCLS</td>
    <td><asp:TextBox ID="txtCRITICALITYCLS" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="label">PLANNINGPOSITION</td>
    <td><asp:TextBox ID="txtPLANNINGPOSITION" runat="server"></asp:TextBox></td>
    </tr>
     <tr>
    <td class="label">structureType</td>
    <td><asp:TextBox ID="txtstructureType" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td colspan="2" align="right"><img alt="" src="../images/appbuttons/minibuttons/saveDisk1.gif" onclick="savex();" /></td>
    </tr>
    </table>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    </div>
    </form>
</body>
</html>
