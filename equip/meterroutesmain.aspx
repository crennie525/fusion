﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="meterroutesmain.aspx.vb" Inherits="lucy_r12.meterroutesmain" %>

<%@ Register src="../menu/mmenu1.ascx" tagname="mmenu1" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Meters</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript">
        function gettab(name) {
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            closeall();
            if (name == "routes") {
                document.getElementById("ifmeter").src = "meterroutelook.aspx?sid=" + sid;
                document.getElementById("tdroutes").className = "thdrhov label";
            }
            else if (name == "addroutes") {
                var rid = "0";
                document.getElementById("ifmeter").src = "meterroutes.aspx?sid=" + sid + "&routeid=" + rid + "&uid=" + uid;
                document.getElementById("tdaddroutes").className = "thdrhov label";
            } 
            else if (name == "addmeters") {
                var rid = "0";
                document.getElementById("ifmeter").src = "emeterlook.aspx?eqid=&sid=" + sid;
                document.getElementById("tdaddmeters").className = "thdrhov label";
            }
            else if (name == "read") {
                var rid = "0";
                document.getElementById("ifmeter").src = "meterhist.aspx?eqid=&sid=" + sid + "&uid=" + uid;
                document.getElementById("tdread").className = "thdrhov label";
            }
        }
        function closeall() {
            document.getElementById("tdroutes").className = "thdr label";
            document.getElementById("tdaddroutes").className = "thdr label";
            document.getElementById("tdaddmeters").className = "thdr label";
            document.getElementById("tdread").className = "thdr label";
        }
        function getprint(rid) {
        window.open("meterprint.aspx?routeid=" + rid)
        }
        function getrec(rid) {
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            document.getElementById("ifmeter").src = "../genhold.htm";
            var timer = window.setTimeout("getrec2('" + rid + "','" + sid + "','" + uid + "');", 300);
        }
        function getrec2(rid, sid, uid) {
            document.getElementById("ifmeter").src = "meterrouterec.aspx?sid=" + sid + "&routeid=" + rid + "&uid=" + uid;
        }
        function retrec() {
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            document.getElementById("ifmeter").src = "../genhold.htm";
            var timer = window.setTimeout("gettab('routes');", 300);
        }
        function getroute(rid) {
            document.getElementById("lblret").value = "yes";
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            document.getElementById("ifmeter").src = "../genhold.htm";
            var timer = window.setTimeout("getroute2('" + rid + "','" + sid + "','" + uid + "');", 300);
        }
        function getroute2(rid, sid, uid) {
            closeall();
            document.getElementById("ifmeter").src = "meterroutes.aspx?sid=" + sid + "&routeid=" + rid + "&uid=" + uid;
            document.getElementById("tdaddroutes").className = "thdrhov label";
        }
        function addroute() {
            closeall();
            document.getElementById("lblret").value = "yes";
            var sid = document.getElementById("lblsid").value;
            var uid = document.getElementById("lbluid").value;
            var rid = "";
            document.getElementById("ifmeter").src = "../genhold.htm";
            var timer = window.setTimeout("getroute2('" + rid + "','" + sid + "','" + uid + "');", 300);
        }

        function GetWidth() {
            var x = 0;
            if (self.innerHeight) {
                x = self.innerWidth;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                x = document.documentElement.clientWidth;
            }
            else if (document.body) {
                x = document.body.clientWidth;
            }
            return x;
        }

        function GetHeight() {
            var y = 0;
            if (self.innerHeight) {
                y = self.innerHeight;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                y = document.documentElement.clientHeight;
            }
            else if (document.body) {
                y = document.body.clientHeight;
            }
            return y;
        }
        var flag = 5
        function upsize(who) {
            var ret = document.getElementById("lblret").value;
            if (ret != "yes") {
                document.getElementById("lblret").value="";
                if (who != flag) {
                    flag = who;
                    document.getElementById("ifmeter").height = GetHeight();
                    window.scrollTo(0, top);
                    //alert(who)
                }
            }
        }
        
    </script>
</head>
<body onload="upsize('1');">
   <script type="text/javascript">
       document.body.onresize = function () {
           upsize('2');
       }
       self.onresize = function () {
           upsize('3');
       }
       document.documentElement.onresize = function () {
           upsize('4');
       }
     
    </script>
    <form id="form1" runat="server">
    <div>
    <table width="100%" style="Z-INDEX: 1; LEFT: 0px; POSITION: absolute; TOP: 76px">
    <tr>
    <td class="details"><a href="#" onclick="getroute('1');">Get Route</a></td>
    </tr>
    <tr>
     <td>
                    <table>
                        <tr height="22">
                            <td id="tdroutes" class="thdrhov label" onclick="gettab('routes');" style="width: 150px">
                                <asp:Label ID="lang3133" runat="server">Meter Routes</asp:Label>
                            </td>
                            <td id="tdaddroutes" class="thdr label" style="width: 150px" onclick="gettab('addroutes');" runat="server">
                                <asp:Label ID="lang3134" runat="server">Add/Edit Routes</asp:Label>
                            </td>
                            <td id="tdaddmeters" class="thdr label" style="width: 150px" onclick="gettab('addmeters');" runat="server">
                                <asp:Label ID="lang3135" runat="server">Add/Edit Meters</asp:Label>
                            </td>
                            <td id="tdread" class="thdr label" style="width: 150px" onclick="gettab('read');" runat="server">
                                <asp:Label ID="lang3136" runat="server">Meter Readings</asp:Label>
                            </td>
                            <td width="12">
                            </td>
                        </tr>
                    </table>
                </td>
    </tr>
				<tr>
					<td>
						<iframe id="ifmeter" src="../genhold.htm" runat="server" width="100%" height="100%" frameborder="0">
						</iframe>
					</td>
				</tr>
			</table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbluid" runat="server" />
    </form>
    <uc1:mmenu1 ID="mmenu11" runat="server" />
</body>
</html>
