﻿Imports System.Data.SqlClient
Public Class compdivecd3
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim necd As New Utilities
    Dim dr As SqlDataReader
    Dim ecd2id, ecd3, ecd2 As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ecd2id = Request.QueryString("ecd2").ToString
            If ecd2id <> "" Then
                Dim ecdarr() As String = ecd2id.Split("-")
                ecd2id = ecdarr(0).ToString
            End If
            lblecd2id.Value = ecd2id
            necd.Open()
            getecd2(ecd2id)
            necd.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                ecd2id = lblecd2id.Value
                ecd2 = lblecd2.Value
                necd.Open()
                checkcnt(ecd2id, ecd2)
                necd.Dispose()
            End If
        End If
    End Sub
    Private Sub getecd2(ByVal ecd2id As String)
        sql = "select ecd2 from ecd2 where ecd2id = '" & ecd2id & "'"
        ecd2 = necd.strScalar(sql)
        lblecd2.Value = ecd2
    End Sub
    Private Sub checkcnt(ByVal ecd2id As String, ByVal ecd2 As String)
        Dim ecd3cnt As Integer
        ecd3 = txtnewecd3.Text
        sql = "select count(*) from ecd3 where ecd2id = '" & ecd2id & "' and ecd3 = '" & ecd3 & "'"
        ecd3cnt = necd.Scalar(sql)
        If ecd3cnt > 0 Then
            Dim strMessage As String = "This Error Code Value Already Exists for " & ecd2
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Else
            lblret.Value = "go"
        End If

    End Sub

End Class