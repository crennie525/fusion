

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class CompDivGrid
    Inherits System.Web.UI.Page
	Protected WithEvents lang2053 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2052 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2051 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2050 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2049 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2048 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tmod As New transmod
    Dim ds As DataSet
    Dim sql As String
    Dim compg As New Utilities
    Dim dr As SqlDataReader
    Dim mu = New mmenu_utils_a
    Dim cid, eq, fu, sid, clid, did, ro, lock, lockby, Login, isecd, ecd1id, who, chld As String
    Protected WithEvents txtnewcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnremcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnaddcomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btncopycomp As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcompchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnremove2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcbonoff As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgcopy As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblisecd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd1id As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchld As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnAddEq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rptrfunc As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        If Not IsPostBack Then
            'Try
            Try
                chld = Request.QueryString("chld").ToString
                lblchld.Value = chld
                who = Request.QueryString("who").ToString
                lblwho.Value = who
            Catch ex As Exception

            End Try
            isecd = mu.ECD
            lblisecd.Value = isecd
            If isecd = "1" Then
                'btnAddEq.Attributes.Add("onclick", "getecd1();")
                btnAddEq.OnClientClick = "Javascript:return getecd1();"

            End If
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                btnAddEq.Enabled = False
                ibtnremove2.Attributes.Add("class", "details")
                imgcopy.Attributes.Add("class", "details")
            End If
            cid = Request.QueryString("cid").ToString
            lblcid.Value = cid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblcid.Value = cid
            eq = Request.QueryString("eqid").ToString
            lbleq.Value = eq
            fu = Request.QueryString("fuid").ToString
            lblfuid.Value = fu
            compg.Open()
            Dim user As String = HttpContext.Current.Session("username").ToString
            'lbluser.Value = user
            lock = CheckLock(eq)
            lockby = lbllockedby.Value
            If lock = "1" Then
                If lockby = user Then
                    lbllock.Value = "0"
                Else
                    btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                    btnAddEq.Enabled = False
                    ibtnremove2.Attributes.Add("class", "details")
                    imgcopy.Attributes.Add("class", "details")
                End If
            ElseIf lock = "0" OrElse Len(lock) = 0 Then
                btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badd.gif"
                btnAddEq.Enabled = True
                ibtnremove2.Attributes.Add("class", "view")
                imgcopy.Attributes.Add("class", "view")
            End If
            GetCurrent(fu)
            PopComp(fu, cid)
            compg.Dispose()
            'Catch ex As Exception

            'End Try
        Else
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = ""
                fu = lblfuid.Value
                cid = lblcid.Value
                compg.Open()
                PopComp(fu, cid)
                compg.Dispose()
            ElseIf Request.Form("lblcompchk") = "del" Then
                lblcompchk.Value = ""
                fu = lblfuid.Value
                cid = lblcid.Value
                compg.Open()
                DelComp()
                compg.Dispose()
            End If

        End If

        'btnAddEq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnAddEq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        chld = lblchld.Value
        Dim lock As String
        If chld = "" Or chld = "0" Then
            sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
            Try
                dr = compg.GetRdrData(sql)
                While dr.Read
                    lock = dr.Item("locked").ToString
                    lbllock.Value = dr.Item("locked").ToString
                    lbllockedby.Value = dr.Item("lockedby").ToString
                End While
                dr.Close()
            Catch ex As Exception

            End Try
        Else
            lock = "1"
            lbllock.Value = "1"
            lbllockedby.Value = "PMO Admin"
        End If
        
        Return lock
    End Function
    Private Sub GetCurrent(ByVal fu As String)
        sql = "select e.eqnum, e.eqdesc, f.func, f.spl from functions f " _
        + "left join equipment e on e.eqid = f.eqid where f.func_id = '" & fu & "'"
        Dim eqs, eqd, fs, fd As String
        dr = compg.GetRdrData(sql)
        While dr.Read
            eqs = dr.Item("eqnum").ToString
            eqd = dr.Item("eqdesc").ToString
            fs = dr.Item("func").ToString
            fd = dr.Item("spl").ToString
        End While
        dr.Close()
        If eqd <> "" Then
            tdeq.InnerHtml = eqs & " - " & eqd
        Else
            tdeq.InnerHtml = eqs
        End If
        If fd <> "" Then
            tdfunc.InnerHtml = fs & " - " & fd
        Else
            tdfunc.InnerHtml = fs
        End If

    End Sub
    Private Sub PopComp(ByVal fu As String, ByVal cid As String)
        Dim cnt As Integer
        If cid = "na" Then
            cnt = 0
            fu = 0
        Else
            sql = "select count(*) from components where func_id = '" & fu & "'" 'deptid = '" & dept & "'"
            cnt = compg.Scalar(sql)
        End If

        sql = "select * from components where func_id = '" & fu & "' order by crouting"
        ds = compg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptrfunc.DataSource = dt
        Dim i, eint As Integer
        eint = 13
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptrfunc.DataBind()

    End Sub

    Private Sub rptrfunc_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrfunc.ItemCommand
        If e.CommandName = "Select" Then
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eq = lbleq.Value
            Dim fuid As String
            fuid = lblfuid.Value
            Dim coid As String
            Try
                coid = CType(e.Item.FindControl("lblcoiditem"), Label).Text
            Catch ex As Exception
                coid = CType(e.Item.FindControl("lblcoidalt"), Label).Text
            End Try
            who = lblwho.Value
            chld = lblchld.Value
            Dim tst As String = "CompDiv.aspx?who=" & who & "&eqid=" & eq & "&fuid=" & fuid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&coid=" & coid & "&chld=" & chld
            Response.Redirect("CompDiv.aspx?who=" & who & "&eqid=" & eq & "&fuid=" & fuid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&coid=" & coid & "&chld=" & chld)


        End If
    End Sub

    Private Sub btngotofunc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("FuncDiv.aspx", True)
    End Sub

    Private Sub btnreturntoeq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("EQBot.aspx", True)
    End Sub

    Private Sub btnAddEq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddEq.Click
        ecd1id = lblecd1id.Value
        isecd = lblisecd.Value
        fu = lblfuid.Value
        cid = lblcid.Value
        sid = lblsid.Value

        Dim co As String
        co = txtnewcomp.Text
        co = compg.ModString1(co)
        'If isecd = "1" And Len(co) > 5 Then
        'Dim strMessage As String = "New Component Name (Error Code 1) Limited to 5 Characters"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        If Len(co) > 0 Then
            Dim desc As String = ""
            Dim spl As String = ""
            Dim desig As String = ""
            Dim cocnt As Integer
            compg.Open()
            sql = "select count(*) from components where func_id = '" & fu & "' " _
            + "and compnum = '" & co & "'" ' and compdesc = '" & desc & "'"
            cocnt = 0 'compg.Scalar(sql)
            If cocnt = 0 Then
                sql = "sp_addComponent " & cid & ", " & fu & ", '" & co & "', '" & desc & "', '" & spl & "', '" & desig & "','" & ecd1id & "','" & sid & "'"
                Dim coid As String '= sql
                coid = compg.Scalar(sql)
                'lblcoid.Value = coid
                lblpar.Value = "comp"
                txtnewcomp.Text = ""
                PopComp(fu, cid)
                compg.Dispose()
            Else
                Dim strMessage As String = tmod.getmsg("cdstr845", "CompDivGrid.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            Dim strMessage As String = tmod.getmsg("cdstr846", "CompDivGrid.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub

    Private Sub rptrfunc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrfunc.ItemDataBound
        Dim cb As CheckBox
        Dim ci As String
        ro = lblro.Value
        If e.Item.ItemType <> ListItemType.Header And _
                                e.Item.ItemType <> ListItemType.Footer Then
            Try
                cb = CType(e.Item.FindControl("cb2"), CheckBox)
                ci = CType(e.Item.FindControl("lblcoiditem"), Label).Text
            Catch ex As Exception
                cb = CType(e.Item.FindControl("cb3"), CheckBox)
                ci = CType(e.Item.FindControl("lblcoidalt"), Label).Text
            End Try

            If ci = "" Or ro = "1" Then
                cb.Enabled = False
            Else
                cb.Attributes.Add("onclick", "checkcb(this.checked);")
            End If

        End If

    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang2048 As Label
			lang2048 = CType(e.Item.FindControl("lang2048"), Label)
			lang2048.Text = axlabs.GetASPXPage("CompDivGrid.aspx","lang2048")
		Catch ex As Exception
		End Try
		Try
                Dim lang2049 As Label
			lang2049 = CType(e.Item.FindControl("lang2049"), Label)
			lang2049.Text = axlabs.GetASPXPage("CompDivGrid.aspx","lang2049")
		Catch ex As Exception
		End Try
		Try
                Dim lang2050 As Label
			lang2050 = CType(e.Item.FindControl("lang2050"), Label)
			lang2050.Text = axlabs.GetASPXPage("CompDivGrid.aspx","lang2050")
		Catch ex As Exception
		End Try
		Try
                Dim lang2051 As Label
			lang2051 = CType(e.Item.FindControl("lang2051"), Label)
			lang2051.Text = axlabs.GetASPXPage("CompDivGrid.aspx","lang2051")
		Catch ex As Exception
		End Try
		Try
                Dim lang2052 As Label
			lang2052 = CType(e.Item.FindControl("lang2052"), Label)
			lang2052.Text = axlabs.GetASPXPage("CompDivGrid.aspx","lang2052")
		Catch ex As Exception
		End Try
		Try
                Dim lang2053 As Label
			lang2053 = CType(e.Item.FindControl("lang2053"), Label)
			lang2053.Text = axlabs.GetASPXPage("CompDivGrid.aspx","lang2053")
		Catch ex As Exception
		End Try

End If

 End Sub
    Private Sub DelComp()
        Dim cb As CheckBox
        Dim ci As String
        'compg.Open()
        For Each i As RepeaterItem In rptrfunc.Items
            If i.ItemType = ListItemType.Item Or i.ItemType = ListItemType.AlternatingItem Then

                If i.ItemType = ListItemType.Item Then
                    cb = CType(i.FindControl("cb2"), CheckBox)
                    ci = CType(i.FindControl("lblcoiditem"), Label).Text
                ElseIf i.ItemType = ListItemType.AlternatingItem Then
                    cb = CType(i.FindControl("cb3"), CheckBox)
                    ci = CType(i.FindControl("lblcoidalt"), Label).Text
                End If

                If cb.Checked Then
                    sql = "usp_delComp '" & ci & "'"
                    compg.Update(sql)
                    lblcoid.Value = ci
                    Dim pic, picn, picm As String
                    sql = "select picurl, picurltn, picurltm from pmpictures where comid = '" & ci & "'"
                    dr = compg.GetRdrData(sql)
                    While dr.Read
                        pic = dr.Item("picurl").ToString
                        picn = dr.Item("picurltn").ToString
                        picm = dr.Item("picurltm").ToString
                        DelFuImg(pic, picn, picm)
                    End While
                    dr.Close()
                    'DelImg()
                End If
            End If
        Next

        'lblchk.Value = "rem"
        Dim fi As String = lblfuid.Value
        cid = lblcid.Value
        PopComp(fi, cid)

        'compg.Dispose()
    End Sub
    Private Sub ibtnremcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnremcomp.Click

    End Sub
    Private Sub DelFuImg(ByVal pic As String, ByVal picn As String, ByVal picm As String)
        'fuid = lblfuid.Value

        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"

        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2048.Text = axlabs.GetASPXPage("CompDivGrid.aspx", "lang2048")
        Catch ex As Exception
        End Try
        Try
            lang2049.Text = axlabs.GetASPXPage("CompDivGrid.aspx", "lang2049")
        Catch ex As Exception
        End Try
        Try
            lang2050.Text = axlabs.GetASPXPage("CompDivGrid.aspx", "lang2050")
        Catch ex As Exception
        End Try
        Try
            lang2051.Text = axlabs.GetASPXPage("CompDivGrid.aspx", "lang2051")
        Catch ex As Exception
        End Try
        Try
            lang2052.Text = axlabs.GetASPXPage("CompDivGrid.aspx", "lang2052")
        Catch ex As Exception
        End Try
        Try
            lang2053.Text = axlabs.GetASPXPage("CompDivGrid.aspx", "lang2053")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnAddEq.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnAddEq.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnAddEq.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnAddEq.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnAddEq.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgcopy.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("CompDivGrid.aspx", "imgcopy") & "')")
            imgcopy.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
