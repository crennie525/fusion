﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqcopy3test.aspx.vb" Inherits="lucy_r12.eqcopy3test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <link rel="stylesheet" type="text/css" href="../styles/reports.css" />
     <script  type="text/javascript">
	<!--
         function checktaskopts(id) {
             var chk = document.getElementById(id);
             if (chk.checked == true) {
                 document.getElementById("cbfunc").checked = true;
                 document.getElementById("cbcomp").checked = true;
             }
         }
         function checkopts(id) {
             var chk = document.getElementById(id);
             if (chk.checked == false) {
                 if (id == "cbfunc") {
                     document.getElementById("cbcomp").checked = false;
                 }
                 document.getElementById("cbpm").checked = false;
                 document.getElementById("cbtpm").checked = false;
             }
         }
    //-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <table>
   <tr>
   <td class="label">
                                            <input id="cbfunc" onclick="checkopts('cbfunc');" checked type="checkbox" name="cbfunc"
                                                runat="server"><asp:Label ID="lang2186" runat="server">Functions</asp:Label><input
                                                    id="cbcomp" onclick="checkopts('cbcomp');" checked type="checkbox" name="cbcomp"
                                                    runat="server"><asp:Label ID="lang2187" runat="server">Components</asp:Label><input
                                                        id="cbpm" onclick="checktaskopts('cbpm');" checked type="checkbox" name="cbpm"
                                                        runat="server"><asp:Label ID="lang2188" runat="server">PM Tasks</asp:Label><input
                                                            id="cbtpm" onclick="checktaskopts('cbtpm');" type="checkbox" name="cbtpm" runat="server"><asp:Label
                                                                ID="lang2189" runat="server">TPM Tasks</asp:Label>
                                        </td>
   </tr>
   <tr>
   <td class="label"><input id="cbecryn" checked type="checkbox" name="cbecryn" runat="server"><asp:Label
                                                            ID="lang2183" runat="server">Copy ECR</asp:Label><input id="cbrat" checked type="checkbox"
                                                                name="cbrat" runat="server"><asp:Label ID="lang2184" runat="server">Copy Rationale</asp:Label></td>
   </tr>
   <tr>
   <td class="label"><input id="rbro" value="rbro" type="radio" name="rbroa" runat="server"><asp:Label
                                                ID="lang2190" runat="server">Revised Only</asp:Label><input id="rbai" value="rbai"
                                                    checked type="radio" name="rbroa" runat="server"><asp:Label ID="lang2191" runat="server">As Is</asp:Label></td>
   </tr>
   <tr>
   <td>
       <asp:TextBox ID="txtnum" runat="server"></asp:TextBox><asp:Button ID="Button1"
           runat="server" Text="Button" />
   </td>
   </tr>
   <tr>
   <td class="plainlabel" id="tdmsg" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="tdmsg1" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="td1" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="td2" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="td3" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="td4" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="td5" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="tdmsg2" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="tdmsg3" runat="server"></td>
   </tr>
   <tr>
   <td class="plainlabel" id="td6" runat="server"></td>
   </tr>
   </table>
    <input type="hidden" id="lblstrmsg" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
<input type="hidden" id="lbldid" runat="server" />
<input type="hidden" id="lbloldsid" runat="server" />
<input type="hidden" id="lblclid" runat="server" />
<input type="hidden" id="lblnewid" runat="server" />
<input type="hidden" id="lblflag" runat="server" />
    </form>
</body>
</html>
