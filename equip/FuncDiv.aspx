<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FuncDiv.aspx.vb" Inherits="lucy_r12.FuncDiv" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>FuncDiv</title>
    <meta content="False" name="vs_showGrid">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    <script  type="text/javascript" src="../scripts1/FuncDivaspx_a.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div align="center" id="FreezePane" class="FreezePaneOff" style="width: 720px">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div id="overDiv" style="z-index: 1000; position: absolute; visibility: hidden">
    </div>
    <table id="eqdetdiv" style="position: absolute; top: 15px; left: 10px" cellspacing="0"
        cellpadding="0" width="675">
        <tr>
            <td style="width: 110px">
            </td>
            <td style="width: 120px">
            </td>
            <td width="85">
            </td>
            <td style="width: 140px">
            </td>
            <td style="width: 140px">
            </td>
            <td style="width: 80px">
            </td>
        </tr>
        <tr>
            <td colspan="6" style="border-bottom: #7ba4e0 thin solid">
                <table>
                    <tr>
                        <td class="bluelabel tbg" align="left" colspan="2">
                            <asp:Label ID="lang2365" runat="server">Add New Function</asp:Label>
                        </td>
                        <td class="tbg">
                            <asp:TextBox ID="txtnewfunc" runat="server" Width="192px" MaxLength="100"></asp:TextBox>
                        </td>
                        <td class="tbg">
                            <asp:ImageButton ID="btnAddFu" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                &nbsp;
            </td>
        </tr>
        <tr height="20">
            <td class="plainlabelblue">
                <asp:Label ID="lang2366" runat="server">Current Equipment:</asp:Label>
            </td>
            <td class="plainlabelblue" id="bt" colspan="3" runat="server">
            </td>
            <td align="center" valign="middle" colspan="2" rowspan="10">
                <a onclick="getbig();" href="#">
                    <img id="imgfu" src="../images/appimages/funcimg.gif" border="0" runat="server" style="width: 216px"
                        height="206"></a>
            </td>
        </tr>
        <tr height="20">
            <td class="label">
                &nbsp;
            </td>
            <td class="plainlabelblue" id="bd" colspan="3" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang2367" runat="server">Function Name</asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtfuname" runat="server" Width="240px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang2368" runat="server">Special Identifier</asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtspl" runat="server" Width="336px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                &nbsp;
            </td>
            <td align="right" colspan="3">
            </td>
        </tr>
        <tr height="20">
            <td class="label" runat="server" id="Td8">
                <asp:Label ID="lang2369" runat="server">Created By</asp:Label>
            </td>
            <td class="plainlabel" id="tdcby" runat="server">
            </td>
            <td class="label">
                <asp:Label ID="lang2370" runat="server">Create Date</asp:Label>
            </td>
            <td class="plainlabel" id="tdcdate" runat="server">
            </td>
        </tr>
        <tr height="20">
            <td class="label" runat="server" id="Td2">
                <asp:Label ID="lang2371" runat="server">Phone</asp:Label>
            </td>
            <td class="plainlabel" id="Td3" runat="server">
            </td>
            <td class="label">
                <asp:Label ID="lang2372" runat="server">Email Address</asp:Label>
            </td>
            <td class="plainlabel" id="Td4" runat="server">
            </td>
        </tr>
        <tr height="20">
            <td class="label" runat="server" id="Td1">
                <asp:Label ID="lang2373" runat="server">Modified By</asp:Label>
            </td>
            <td class="plainlabel" id="tdmby" runat="server">
            </td>
            <td class="label">
                <asp:Label ID="lang2374" runat="server">Modified Date</asp:Label>
            </td>
            <td class="plainlabel" id="tdmdate" runat="server">
            </td>
        </tr>
        <tr height="20">
            <td class="label" runat="server" id="Td5">
                <asp:Label ID="lang2375" runat="server">Phone</asp:Label>
            </td>
            <td class="plainlabel" id="Td6" runat="server">
                &nbsp;
            </td>
            <td class="label">
                <asp:Label ID="lang2376" runat="server">Email Address</asp:Label>
            </td>
            <td class="plainlabel" id="Td7" runat="server">
                &nbsp;
            </td>
        </tr>
        <tr height="30">
            <td align="right" colspan="4">
            </td>
            <td align="center" colspan="2">
                <table>
                    <tr>
                        <td class="bluelabel" style="width: 80px">
                            <asp:Label ID="lang2377" runat="server">Order</asp:Label>
                        </td>
                        <td style="width: 60px">
                            <asp:TextBox ID="txtiorder" runat="server" style="width: 40px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td style="width: 20px">
                            <img onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif">
                        </td>
                        <td style="width: 20px">
                            <img onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)"
                                onclick="addpic();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif">
                        </td>
                        <td style="width: 20px">
                            <img id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
                                src="../images/appbuttons/minibuttons/del.gif" runat="server" onmouseout="return nd()">
                        </td>
                        <td style="width: 20px">
                            <img id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)"
                                onclick="savdets();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
            <td align="center" colspan="2">
                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-right: blue 1px solid; width: 20px;">
                            <img id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid; width: 20px;">
                            <img id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                runat="server">
                        </td>
                        <td style="border-right: blue 1px solid" valign="middle" align="center" width="134">
                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:Label>
                        </td>
                        <td style="border-right: blue 1px solid; width: 20px;">
                            <img id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                runat="server">
                        </td>
                        <td style="width: 20px">
                            <img id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
                                runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="border-bottom: #7ba4e0 thin solid">
                <img src="../images/appbuttons/minibuttons/2PX.gif">
            </td>
        </tr>
        <tr>
            <tr>
                <td colspan="6">
                    <table cellspacing="0" cellpadding="0" width="744">
                        <tr>
                            <td colspan="2" id="tdgtt" runat="server">
                                <a onclick="GoToTasks(5);" href="#">
                                    <img id="btngtt" alt="" src="../images/appbuttons/bgbuttons/jtotaskssm.gif" border="0"
                                        runat="server" width="122" height="19"></a>
                            </td>
                            <td colspan="2" class="label" id="tdgttrb" runat="server">
                                <input id="rbdev" runat="server" onclick="GetType(this.value);" type="radio" value="pmdev"
                                    name="rtyp" disabled><asp:Label ID="lang2378" runat="server">PM Developer</asp:Label><input
                                        id="rbopt" runat="server" onclick="GetType(this.value);" type="radio" value="pmopt"
                                        name="rtyp" disabled><asp:Label ID="lang2379" runat="server">PM Optimizer</asp:Label><input
                                            id="rbtdev" disabled onclick="GetType(this.value);" type="radio" value="rbdev"
                                            name="rtyp" runat="server"><asp:Label ID="lang2380" runat="server">TPM Developer</asp:Label><input
                                                id="rbtopt" disabled onclick="GetType(this.value);" type="radio" value="rbopt"
                                                name="rtyp" runat="server"><asp:Label ID="lang2381" runat="server">TPM Optimizer</asp:Label>
                            </td>
                            <td align="right" colspan="2" id="tdopts" runat="server">
                                <img onclick="gridret();" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
                                    style="width: 20px" height="20"><asp:ImageButton ID="btndel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif">
                                    </asp:ImageButton><asp:ImageButton ID="btnsave" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                                    </asp:ImageButton>&nbsp;<img onmouseover="return overlib('Copy a Function Record')"
                                        onclick="GetFuncCopy();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                        style="width: 20px" height="20" id="imgcopy" runat="server">&nbsp;<img class="imgbutton details"
                                            onmouseover="return overlib('Go to the Centralized PM Library', LEFT, WIDTH, 270)"
                                            onclick="GoToPMLib();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/gotolibsm.gif"
                                            width="23" height="20">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
    </table>
    <input id="lbleqid" type="hidden" runat="server"><input id="lblcid" type="hidden"
        runat="server">
    <input id="lblfuid" type="hidden" runat="server"><input id="lblpar" type="hidden"
        runat="server">
    <input id="lblfunchold" type="hidden" runat="server">
    <input id="lblpchk" type="hidden" runat="server">
    <input id="lblpic" type="hidden" runat="server"><input id="lbluser" type="hidden"
        runat="server">
    <input type="hidden" id="lblparfuid" runat="server"><input type="hidden" id="lblsid"
        runat="server" name="lblsid">
    <input type="hidden" id="lbldid" runat="server" name="lbldid">
    <input type="hidden" id="lblclid" runat="server" name="lblclid">
    <input type="hidden" id="lbladdchk" runat="server">
    <input type="hidden" id="lbllock" runat="server">
    <input type="hidden" id="lbllockedby" runat="server"><input type="hidden" id="lblro"
        runat="server" name="lblro"><input type="hidden" id="lblappstr" runat="server" name="lblappstr">
    <input type="hidden" id="lblapp" runat="server" name="lblapp"><input type="hidden"
        id="lbldel" runat="server">
    <input id="lblpcnt" type="hidden" runat="server" name="lblpcnt">
    <input id="lblcurrp" type="hidden" runat="server" name="lblcurrp">
    <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
    <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
    <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
    <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
    <input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server">
    <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
    <input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders"
        type="hidden" name="lbliorders" runat="server">
    <input id="lbloldorder" type="hidden" runat="server" name="lbloldorder">
    <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
    <input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server">
    <input type="hidden" id="lbllog" runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblchld" runat="server" />
    </form>
</body>
</html>
