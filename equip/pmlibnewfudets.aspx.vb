

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class pmlibnewfudets
    Inherits System.Web.UI.Page
	Protected WithEvents ovid264 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid263 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid262 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2490 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2489 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2488 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2487 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2486 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim eqid, fuid, db, start, typ, sid, cid, did, clid, oloc, Login As String
    Protected WithEvents txtiorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgdel As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnremove As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rptrfuncnew As System.Web.UI.WebControls.Repeater
    Protected WithEvents tst As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ggrid As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsavdet As System.Web.UI.HtmlControls.HtmlImage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpgf As System.Web.UI.WebControls.Label
    Protected WithEvents imgfu As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirstf As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprevf As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inextf As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilastf As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcntf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrpf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgidf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimgf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimgf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliordersf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorderf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgsf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimgf As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                eqid = Request.QueryString("eqid").ToString
                db = Request.QueryString("db").ToString
                lbleqid.Value = eqid
                lbldb.Value = db
                oloc = Request.QueryString("oloc").ToString
                lbloloc.Value = oloc
                cid = Request.QueryString("cid").ToString
                sid = Request.QueryString("sid").ToString
                did = Request.QueryString("did").ToString
                clid = Request.QueryString("clid").ToString
                lblcid.Value = cid
                lblsid.Value = sid
                lbldid.Value = did
                lblclid.Value = clid

                copy.Open()
                PopFuncRev(eqid)
                copy.Dispose()
            End If
        Else
            If Request.Form("lblpchk") = "delimg" Then
                lblpchk.Value = ""
                copy.Open()
                DelImg()
                LoadEqPics()
                copy.Dispose()
                'lblchksav.Value = "yes"
            ElseIf Request.Form("lblpchk") = "checkpic" Then
                lblpchk.Value = ""
                copy.Open()
                LoadEqPics()
                copy.Dispose()
            ElseIf Request.Form("lblpchk") = "savedets" Then
                lblpchk.Value = ""
                copy.Open()
                SaveDets()
                LoadEqPics()
                copy.Dispose()
            ElseIf Request.Form("lblpchk") = "func" Then
                lblpchk.Value = ""
                eqid = lbleqid.Value
                copy.Open()
                PopFuncRev(eqid)
                copy.Dispose()
            End If
        End If
    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr999" , "pmlibnewfudets.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim old As String = oldiord 'lbloldorder.Value
            Dim neword As String = txtiorder.Text
            fuid = lblfuid.Value
            typ = "fu"
            sql = "usp_reordereqimg '" & typ & "','" & fuid & "','" & neword & "','" & old & "'"
            copy.Update(sql)
        End If
    End Sub
    Private Sub DelImg()
        fuid = lblfuid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        typ = "fu"
        sql = "usp_deleqimg '" & typ & "','" & fuid & "','" & pid & "','" & old & "'"
        copy.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
        lblcurrp.Value = "0"
    End Sub
    Private Sub PopFuncRev(ByVal eqid As String)
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
       
        sql = "select * from  [" & srvr & "].[" & mdb & "].[dbo].[functions] where eqid = '" & eqid & "' order by routing"
        dr = copy.GetRdrData(sql)
        rptrfuncnew.DataSource = dr
        rptrfuncnew.DataBind()
        dr.Close()
    End Sub
    Private Sub LoadEqPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgfu.Attributes.Add("src", "../images/appimages/funcimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgfu.Attributes.Add("src", "../images2/eng/funcimg.gif")
        ElseIf lang = "fre" Then
            imgfu.Attributes.Add("src", "../images2/fre/funcimg.gif")
        ElseIf lang = "ger" Then
            imgfu.Attributes.Add("src", "../images2/ger/funcimg.gif")
        ElseIf lang = "ita" Then
            imgfu.Attributes.Add("src", "../images2/ita/funcimg.gif")
        ElseIf lang = "spa" Then
            imgfu.Attributes.Add("src", "../images2/spa/funcimg.gif")
        End If

        Dim img, imgs, picid, eqid As String
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
       
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        Dim pcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where comid is null and funcid = '" & fuid & "'"
        'sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        pcnt = copy.Scalar(sql)
        lblpcntf.Value = pcnt

        Dim currp As String = lblcurrpf.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpgf.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpgf.Text = "Image 1 of " & pcnt
                lblcurrpf.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] p where p.comid is null and p.funcid = '" & fuid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = copy.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimgf.Value = ovimg
                    lblcurrbimgf.Value = ovbimg
                    lblcurrtimgf.Value = ovtimg
                    imgfu.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgidf.Value = picid

                    'txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgsf.Value = imgs
            lblbimgsf.Value = bimgs
            lblovimgsf.Value = ovimgs
            lblovbimgsf.Value = ovbimgs
            lblovtimgsf.Value = ovtimgs
            lbliordersf.Value = iorders
        End If
    End Sub
    Public Function HighlightRowFR(ByVal rowid As Integer) As String
        Dim bgColor = "#FFFFFF"
        Try
            Dim fi As String = lblfuid.Value
            Dim id As Integer = fi
            If rowid = id Then
                bgColor = "#D6D4FB"
            End If
        Catch ex As Exception

        End Try
        Return bgColor
    End Function


    Private Sub rptrfuncrev_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
      
    End Sub

    Private Sub ibtnremove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnremove.Click
        Dim cb As CheckBox
        Dim fi As String
        copy.Open()

        For Each i As RepeaterItem In rptrfuncnew.Items
            If i.ItemType = ListItemType.Item Or i.ItemType = ListItemType.AlternatingItem Then
                cb = CType(i.FindControl("cb1"), CheckBox)
                If cb.Checked Then
                    fi = CType(i.FindControl("lblfuncrevid"), Label).Text
                    sql = "usp_delFunc '" & fi & "'"
                    copy.Update(sql)
                    lblfuid.Value = fi
                    DelImg()
                End If
            End If
        Next
        lblchk.Value = "rem"
        Dim eq As String = lbleqid.Value
        PopFuncRev(eq)
        copy.Dispose()
    End Sub

    Private Sub rptrfuncnew_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrfuncnew.ItemCommand
        Dim fi, fp As String

        If e.CommandName = "Select" Then
            fi = CType(e.Item.FindControl("lblfuncrevid"), Label).Text
            lblfuid.Value = fi
            lblchk.Value = "yes"
            copy.Open()
            Dim eq As String = lbleqid.Value
            PopFuncRev(eq)
            LoadEqPics()
            copy.Dispose()
        End If
    End Sub

    Private Sub rptrfuncnew_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrfuncnew.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Dim fuid As String = DataBinder.Eval(e.Item.DataItem, "func_id").ToString
            eqid = lbleqid.Value
            Dim imgcoedit As HtmlImage = CType(e.Item.FindControl("imgcoedit"), HtmlImage)
            imgcoedit.Attributes.Add("onclick", "getcompedit('" & fuid & "','" & eqid & "');")

        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang2486 As Label
			lang2486 = CType(e.Item.FindControl("lang2486"), Label)
			lang2486.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx","lang2486")
		Catch ex As Exception
		End Try
		Try
                Dim lang2487 As Label
			lang2487 = CType(e.Item.FindControl("lang2487"), Label)
			lang2487.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx","lang2487")
		Catch ex As Exception
		End Try
		Try
                Dim lang2488 As Label
			lang2488 = CType(e.Item.FindControl("lang2488"), Label)
			lang2488.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx","lang2488")
		Catch ex As Exception
		End Try
		Try
                Dim lang2489 As Label
			lang2489 = CType(e.Item.FindControl("lang2489"), Label)
			lang2489.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx","lang2489")
		Catch ex As Exception
		End Try
		Try
                Dim lang2490 As Label
			lang2490 = CType(e.Item.FindControl("lang2490"), Label)
			lang2490.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx","lang2490")
		Catch ex As Exception
		End Try
		Try
                Dim lblpgf As Label
			lblpgf = CType(e.Item.FindControl("lblpgf"), Label)
			lblpgf.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx","lblpgf")
		Catch ex As Exception
		End Try

End If

 End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2486.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx", "lang2486")
        Catch ex As Exception
        End Try
        Try
            lang2487.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx", "lang2487")
        Catch ex As Exception
        End Try
        Try
            lang2488.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx", "lang2488")
        Catch ex As Exception
        End Try
        Try
            lang2489.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx", "lang2489")
        Catch ex As Exception
        End Try
        Try
            lang2490.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx", "lang2490")
        Catch ex As Exception
        End Try
        Try
            lblpgf.Text = axlabs.GetASPXPage("pmlibnewfudets.aspx", "lblpgf")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ggrid.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewfudets.aspx", "ggrid") & "')")
            ggrid.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdel.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewfudets.aspx", "imgdel") & "', ABOVE, LEFT)")
            imgdel.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewfudets.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid262.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewfudets.aspx", "ovid262") & "', ABOVE, LEFT)")
            ovid262.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid263.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewfudets.aspx", "ovid263") & "')")
            ovid263.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid264.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("pmlibnewfudets.aspx", "ovid264") & "')")
            ovid264.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
