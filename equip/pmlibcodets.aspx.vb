

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class pmlibcodets
    Inherits System.Web.UI.Page
    Protected WithEvents lang2440 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2439 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2438 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2437 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2436 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim eqid, fuid, db, start, coid, Login As String
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcntc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrpc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgsc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgidc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgsc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgsc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimgc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimgc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgsc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliordersc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorderc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgsc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimgc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrcomprev As System.Web.UI.WebControls.Repeater
    Protected WithEvents rptrfailrev As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblpgc As System.Web.UI.WebControls.Label
    Protected WithEvents imgco As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirstc As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprevc As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inextc As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilastc As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdfailrev As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                fuid = Request.QueryString("fuid").ToString
                db = Request.QueryString("db").ToString
                lblfuid.Value = fuid
                lbldb.Value = db
                copy.Open()
                PopCompRev(fuid)
                copy.Dispose()
            End If


        End If
    End Sub
    Private Sub PopCompRev(ByVal funcid As String)
        lblfuid.Value = funcid
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        mdb = mdbarr(2).ToString
        sql = "select * from [" & srvr & "].[" & mdb & "].[dbo].[components] where func_id = '" & funcid & "' order by crouting"
        dr = copy.GetRdrData(sql)
        rptrcomprev.DataSource = dr
        rptrcomprev.DataBind()
        rptrfailrev.DataSource = Nothing
        rptrfailrev.DataBind()
        dr.Close()
    End Sub
    Public Function HighlightRowCP(ByVal rowid As Integer) As String
        Dim bgColor = "#FFFFFF"
        Try
            Dim id As Integer = lblcoid.Value
            If rowid = id Then
                bgColor = "#D6D4FB"
            End If
        Catch ex As Exception

        End Try
        Return bgColor
    End Function

    Private Sub rptrcomprev_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrcomprev.ItemCommand
        Dim ci, pic, pcnt, cp, fi As String
        If e.CommandName = "Select" Then
            ci = CType(e.Item.FindControl("lblcomprevid"), Label).Text
            cp = CType(e.Item.FindControl("lblparco1"), Label).Text
            pic = CType(e.Item.FindControl("lblpurl3"), Label).Text
            pcnt = CType(e.Item.FindControl("lblpcnt3"), Label).Text
            lblcoid.Value = ci
            fi = lblfuid.Value
            copy.Open()
            fuid = lblfuid.Value
            PopCompRev(fuid)
            PopFailRev(ci)
            tdfailrev.Attributes.Add("class", "view")
            LoadEqPics()
            copy.Dispose()

        End If
    End Sub
    Private Sub PopFailRev(ByVal comid As String)
        '*** Multi Add ***
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        mdb = mdbarr(2).ToString
        '*** Multi Add End***
        sql = "select * from [" & srvr & "].[" & mdb & "].[dbo].[componentfailmodes] where comid = '" & comid & "'"
        sql = "usp_getcfall_multi '" & coid & "','" & srvr & "','" & mdb & "'"
        dr = copy.GetRdrData(sql)
        rptrfailrev.DataSource = dr
        rptrfailrev.DataBind()
        dr.Close()
    End Sub
    Private Sub LoadEqPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgco.Attributes.Add("src", "../images/appimages/compimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgco.Attributes.Add("src", "../images2/eng/compimg.gif")
        ElseIf lang = "fre" Then
            imgco.Attributes.Add("src", "../images2/fre/compimg.gif")
        ElseIf lang = "ger" Then
            imgco.Attributes.Add("src", "../images2/ger/compimg.gif")
        ElseIf lang = "ita" Then
            imgco.Attributes.Add("src", "../images2/ita/compimg.gif")
        ElseIf lang = "spa" Then
            imgco.Attributes.Add("src", "../images2/spa/compimg.gif")
        End If

        Dim img, imgs, picid, eqid As String
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        mdb = mdbarr(2).ToString
        coid = lblcoid.Value
        Dim pcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where comid is not null and comid = '" & coid & "'"
        'sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        pcnt = copy.Scalar(sql)
        lblpcntc.Value = pcnt

        Dim currp As String = lblcurrpc.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpgc.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpgc.Text = "Image 1 of " & pcnt
                lblcurrpc.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] p where p.comid is not null and p.comid = '" & coid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = copy.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimgc.Value = ovimg
                    lblcurrbimgc.Value = ovbimg
                    lblcurrtimgc.Value = ovtimg
                    imgco.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgidc.Value = picid

                    'txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgsc.Value = imgs
            lblbimgsc.Value = bimgs
            lblovimgsc.Value = ovimgs
            lblovbimgsc.Value = ovbimgs
            lblovtimgsc.Value = ovtimgs
            lbliordersc.Value = iorders
        End If
    End Sub




    Private Sub rptrcomprev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrcomprev.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2436 As Label
                lang2436 = CType(e.Item.FindControl("lang2436"), Label)
                lang2436.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2436")
            Catch ex As Exception
            End Try
            Try
                Dim lang2437 As Label
                lang2437 = CType(e.Item.FindControl("lang2437"), Label)
                lang2437.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2437")
            Catch ex As Exception
            End Try
            Try
                Dim lang2438 As Label
                lang2438 = CType(e.Item.FindControl("lang2438"), Label)
                lang2438.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2438")
            Catch ex As Exception
            End Try
            Try
                Dim lang2439 As Label
                lang2439 = CType(e.Item.FindControl("lang2439"), Label)
                lang2439.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2439")
            Catch ex As Exception
            End Try
            Try
                Dim lang2440 As Label
                lang2440 = CType(e.Item.FindControl("lang2440"), Label)
                lang2440.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2440")
            Catch ex As Exception
            End Try
            Try
                Dim lblpgc As Label
                lblpgc = CType(e.Item.FindControl("lblpgc"), Label)
                lblpgc.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lblpgc")
            Catch ex As Exception
            End Try

        End If


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2436 As Label
                lang2436 = CType(e.Item.FindControl("lang2436"), Label)
                lang2436.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2436")
            Catch ex As Exception
            End Try
            Try
                Dim lang2437 As Label
                lang2437 = CType(e.Item.FindControl("lang2437"), Label)
                lang2437.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2437")
            Catch ex As Exception
            End Try
            Try
                Dim lang2438 As Label
                lang2438 = CType(e.Item.FindControl("lang2438"), Label)
                lang2438.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2438")
            Catch ex As Exception
            End Try
            Try
                Dim lang2439 As Label
                lang2439 = CType(e.Item.FindControl("lang2439"), Label)
                lang2439.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2439")
            Catch ex As Exception
            End Try
            Try
                Dim lang2440 As Label
                lang2440 = CType(e.Item.FindControl("lang2440"), Label)
                lang2440.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2440")
            Catch ex As Exception
            End Try
            Try
                Dim lblpgc As Label
                lblpgc = CType(e.Item.FindControl("lblpgc"), Label)
                lblpgc.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lblpgc")
            Catch ex As Exception
            End Try

        End If

    End Sub

    Private Sub rptrfailrev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrfailrev.ItemDataBound

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2436.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2436")
        Catch ex As Exception
        End Try
        Try
            lang2437.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2437")
        Catch ex As Exception
        End Try
        Try
            lang2438.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2438")
        Catch ex As Exception
        End Try
        Try
            lang2439.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2439")
        Catch ex As Exception
        End Try
        Try
            lang2440.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lang2440")
        Catch ex As Exception
        End Try
        Try
            lblpgc.Text = axlabs.GetASPXPage("pmlibcodets.aspx", "lblpgc")
        Catch ex As Exception
        End Try

    End Sub

End Class
