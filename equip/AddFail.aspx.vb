

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AddFail
    Inherits System.Web.UI.Page
    Protected WithEvents lang1918 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1917 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1916 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1915 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1914 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1913 As System.Web.UI.WebControls.Label

    Protected WithEvents lang1912 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim mu As New mmenu_utils_a
    Dim dr As SqlDataReader
    Dim sql As String
    Dim comi As New mmenu_utils_a
    Dim fail As New Utilities
    Dim cid, eqid, fuid, co, cod, coid, cvalu, app, sid, Login, ro, isecd As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbfailmaster As System.Web.UI.WebControls.ListBox
    Protected WithEvents btnaddfail As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnfromcomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btntocomp As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtnewfail As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbopts As System.Web.UI.WebControls.RadioButtonList
    Protected WithEvents lblopt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblfailchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnaddtosite As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents todis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents fromdis As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddoca As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblfailmode As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbfailcomp As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblswitch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents aswitch As System.Web.UI.WebControls.Label
    Protected WithEvents imgchk As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblisecd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetecd2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgetecd3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrecd2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewecd3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd1id As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecd2txt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomi As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbfailmodes As System.Web.UI.WebControls.ListBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()



        GetFSLangs()
        Dim coi As String = comi.COMPI
        lblcomi.Value = coi
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            'Dim strMessage As String = Login
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Catch ex As Exception
            Dim strMessage As String = Login
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            lbllog.Value = "no"
            Exit Sub
        End Try

        Page.EnableViewState = True
        If Not IsPostBack Then
            'Try

            isecd = mu.ECD
            lblisecd.Value = isecd
            If isecd = "1" Then
                lbfailcomp.SelectionMode = ListSelectionMode.Single
                lbfailmaster.SelectionMode = ListSelectionMode.Single
            End If


            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            If ro = "1" Then
                btnaddfail.Enabled = False
                btnaddfail.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                cbopts.Enabled = False
                btnaddtosite.Attributes.Add("class", "details")
                btntocomp.Visible = False
                btnfromcomp.Visible = False
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")
                'imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
            Else
                'btnaddfail.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
                'btnaddfail.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
            End If
            If Len(fuid) <> 0 AndAlso fuid <> "" AndAlso fuid <> "0" Then
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                'Session("comp") = cid
                lblsid.Value = HttpContext.Current.Session("dfltps").ToString
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
                'Session("coid") = coid
                cvalu = Request.QueryString("cvalu").ToString
                tdcomp.InnerHtml = cvalu
                'pm3clients only SS
                lblopt.Value = "0"
                lbfailmodes.AutoPostBack = False
                lbfailmaster.AutoPostBack = False

                fail.Open()
                lblfailmode.Value = "norm"
                lblswitch.Value = "all"
                imgchk.Attributes.Add("class", "details")
                popoca()
                PopFail(cid, coid)
                PopFailComp(cid, coid)
                PopFailList(cid, coid)
                getecd()
                fail.Dispose()
            Else
                Dim strMessage As String = tmod.getmsg("cdstr793", "AddFail.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End If
            ' Catch ex As Exception
            'Dim strMessage As String = tmod.getmsg("cdstr794", "AddFail.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'lbllog.Value = "noeqid"
            'End Try

        End If

        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
        'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")

    End Sub
    Private Sub getecd()
        sql = "select ecd1id from components where comid = '" & coid & "'"
        dr = fail.GetRdrData(sql)
        Dim ecd1id As String
        While dr.Read
            ecd1id = dr.Item("ecd1id").ToString
        End While
        dr.Close()


    End Sub
    Private Sub popoca()
        sql = "select * from ocareas"
        dr = fail.GetRdrData(sql)
        ddoca.DataSource = dr
        ddoca.DataTextField = "ocarea"
        ddoca.DataValueField = "oaid"
        ddoca.DataBind()
        dr.Close()
        ddoca.Items.Insert(0, "ALL")
    End Sub
    Private Sub PopFailComp(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        Dim coi As String = lblcomi.Value
        faillist.Open()
        sql = "select compfailid, failuremode from ComponentFailModes where " _
          + "comid = '" & coid & "' and oaid is null order by failuremode"
        dr = faillist.GetRdrData(sql)
        lbfailcomp.DataSource = dr
        Dim lang As String = lblfslang.Value
        'If lang = "fre" Then
        'lbfailcomp.DataTextField = "ffailuremode"
        'Else
        lbfailcomp.DataTextField = "failuremode"
        'End If
        lbfailcomp.DataValueField = "compfailid"
        'lbfailcomp.DataTextField = "failuremode"
        lbfailcomp.DataBind()
        dr.Close()
        Dim fcnt As Integer = 0
        Dim sw As String = lblswitch.Value
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "All" Or ddoca.SelectedIndex = 0 Or ddoca.SelectedIndex = -1 Then
            lblfailmode.Value = "norm"
            imgchk.Attributes.Add("class", "details")
            lbfailcomp.Attributes.Add("class", "details")
            lbfailmaster.Attributes.Add("class", "view")
            aswitch.Text = "Available Failure Modes"
        Else

            sql = "select count(*) from ComponentFailModes where " _
          + "comid = '" & coid & "' and oaid is null"
            fcnt = faillist.Scalar(sql)
            If fcnt > 0 Then
                lblfailmode.Value = "rev"
                imgchk.Attributes.Add("class", "view")
                If sw = "comp" Then
                    lbfailcomp.Attributes.Add("class", "view")
                    lbfailmaster.Attributes.Add("class", "details")
                    aswitch.Text = "Unassigned Failure Modes"
                Else
                    lbfailcomp.Attributes.Add("class", "details")
                    lbfailmaster.Attributes.Add("class", "view")
                    aswitch.Text = "Available Failure Modes"
                End If
            Else
                lblfailmode.Value = "norm"
                imgchk.Attributes.Add("class", "details")
                If sw = "comp" Then
                    lbfailcomp.Attributes.Add("class", "details")
                    lbfailmaster.Attributes.Add("class", "view")
                    aswitch.Text = "Available Failure Modes"
                    lblswitch.Value = "all"

                End If
            End If
        End If
        faillist.Dispose()
    End Sub
    Private Sub PopFailList(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        faillist.Open()
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        Dim ocas As String
        Dim lang As String = lblfslang.Value
        If oca = "ALL" Then
            sql = "select distinct compfailid, failuremode from ComponentFailModes where " _
              + "comid = '" & coid & "'"
            sql = "usp_getcfall '" & coid & "', '" & lang & "'"
        Else
            sql = "select distinct compfailid, failuremode from ComponentFailModes where " _
              + "comid = '" & coid & "' and oaid = '" & oaid & "' order by failuremode"
        End If

        dr = faillist.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        If oca = "ALL" Then
            lbfailmodes.DataValueField = "compfailid"
            lbfailmodes.DataTextField = "failuremode"
        Else
            lbfailmodes.DataValueField = "compfailid"
            lbfailmodes.DataTextField = "failuremode"
        End If

        lbfailmodes.DataBind()
        dr.Close()

        faillist.Dispose()
    End Sub

    Private Sub PopFail(ByVal cid As String, ByVal comid As String)
        Dim fail As New Utilities
        Dim lang As String = lblfslang.Value
        Dim coi As String = lblcomi.Value
        fail.Open()
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        Dim dt, val, filt As String
        If coi <> "CAS" And coi <> "AGR" And coi <> "LAU" And coi <> "GLA" Then
            If chk = "" Then
                sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
                scnt = fail.Scalar(sql)
                If scnt = 0 Then
                    lblfailchk.Value = "open"
                    chk = "open"
                Else
                    lblfailchk.Value = "site"
                    chk = "site"
                End If
            End If

            If chk = "open" Then
                dt = "FailureModes"
            ElseIf chk = "site" Then
                dt = "pmSiteFM"
            Else
                Exit Sub
            End If

            val = "failid, failuremode"
        Else
            chk = "open"
            dt = "FailureModes"
            If lang = "fre" And coi = "CAS" Then
                val = "failid, fmeng"
            Else
                val = "failid, failuremode"
            End If
        End If
        
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            If chk = "site" Then
                filt = " where siteid = '" & sid & "' and failid not in (" _
                       + "select failid from componentfailmodes where comid = '" & comid & "' and oaid is null)" ' order by failuremode
            Else
                filt = " where failid not in (" _
           + "select failid from componentfailmodes where comid = '" & comid & "' and oaid is null)" ' order by failuremode
            End If
           
        Else
            If chk = "site" Then
                filt = " where siteid = '" & sid & "' and failid not in (" _
           + "select failid from componentfailmodes where comid = '" & comid & "' and oaid = '" & oaid & "')" ' order by failuremode
            Else
                filt = " where failid not in (" _
           + "select failid from componentfailmodes where comid = '" & comid & "' and oaid = '" & oaid & "')" ' order by failuremode
            End If
           
        End If

        dr = fail.GetList(dt, val, filt)
        lbfailmaster.DataSource = dr
        If lang = "fre" And coi = "CAS" Then
            lbfailmaster.DataTextField = "fmeng"
        Else
            lbfailmaster.DataTextField = "failuremode"
        End If
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
        fail.Dispose()
    End Sub

    Private Sub GetItems(ByVal failid As String, ByVal failstr As String)
        isecd = lblisecd.Value
        cid = lblcid.Value
        sid = lblsid.Value
        coid = lblcoid.Value
        Dim sw As String = lblswitch.Value
        Dim fail As New Utilities
        fail.Open()
        Dim fcnt As Integer
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            sql = "select count(*) from ComponentFailModes where " _
        + "comid = '" & coid & "' and failid = '" & failid & "' and oaid is null"
        Else
            sql = "select count(*) from ComponentFailModes where " _
        + "comid = '" & coid & "' and failid = '" & failid & "' and oaid = '" & oaid & "'"
        End If
        fcnt = fail.Scalar(sql)
        Dim ecd As String = lblcurrecd2.Value
        'lblcurrecd2.Value = ""
        Dim ecd1, ecd2, ecd3 As String
        If ecd <> "" Then
            Dim ecdarr() As String = ecd.Split("-")
            ecd2 = ecdarr(0).ToString
            ecd1 = ecdarr(1).ToString
        End If
        ecd3 = lblnewecd3.Value
        lblnewecd3.Value = ""
        Dim ecd3id, ecd3cnt As Integer
        If isecd = "1" Then
            If ecd3 <> "" Then
                sql = "select count(*) from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1 & "' and ecd2id = '" & ecd2 & "'"
                ecd3cnt = fail.Scalar(sql)
                If ecd3cnt = 0 Then
                    sql = "insert into ecd3 (ecd3, ecddesc, ecd1id, ecd2id) values ('" & ecd3 & "','" & oca & "','" & ecd1 & "','" & ecd2 & "') select @@identity"
                    ecd3id = fail.Scalar(sql)
                Else
                    sql = "select ecd3id from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1 & "' and ecd2id = '" & ecd2 & "'"
                    ecd3id = fail.Scalar(sql)
                End If
            Else
                Dim strMessage As String = "No Error Code 3 Value Created"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If


        If fcnt = 0 Then
            If oca = "ALL" Then
                oaid = ""
            End If
            If sw = "comp" And oca <> "ALL" Then
                If ecd <> "" Then
                    sql = "update ComponentFailModes set oaid = '" & oaid & "', ecd1id = '" & ecd1 & "', ecd2id = '" & ecd2 & "', ecd3id = '" & ecd3id & "' where compfailid = '" & failid & "'"
                Else
                    sql = "update ComponentFailModes set oaid = '" & oaid & "' where compfailid = '" & failid & "'"
                End If

            Else
                sql = "sp_addFailureMode '" & cid & "', '" & coid & "', '" & failid & "', '" & failstr & "','" & oaid & "','" & ecd1 & "','" & ecd2 & "','" & ecd3id & "','" & sid & "'"
            End If

            fail.Update(sql)
        End If
        fail.Dispose()

    End Sub
    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        cid = lblcid.Value
        coid = lblcoid.Value
        Dim Item As ListItem
        Dim f, fi As String
        Dim fail As New Utilities
        Dim sw As String = lblswitch.Value
        fail.Open()
        If sw = "comp" Then
            For Each Item In lbfailcomp.Items
                If Item.Selected Then
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    GetItems(fi, f)
                End If
            Next
        Else
            For Each Item In lbfailmaster.Items
                If Item.Selected Then
                    f = Item.Text.ToString
                    fi = Item.Value.ToString
                    GetItems(fi, f)
                End If
            Next
        End If

        PopFailList(cid, coid)
        PopFail(cid, coid)
        PopFailComp(cid, coid)
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal failid As String, ByVal failstr As String)
        isecd = lblisecd.Value
        coid = lblcoid.Value
        Dim sw As String = lblswitch.Value
        Dim fail As New Utilities
        fail.Open()
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            oaid = ""
        End If
        Dim ecd3id, ecd3cnt As Integer
        If isecd = "1" Then
            sql = "delete from ecd3 where ecd3id = (select ecd3id from componentfailmodes where compfailid = '" & failid & "')"
            fail.Update(sql)
        End If
        If sw = "comp" Then
            sql = "update ComponentFailModes set oaid = NULL, ecd1id = NULL, ecd2id = NULL, ecd3id = NULL where compfailid = '" & failid & "';"
            sql += "usp_deltaskoaid '" & failid & "','" & oaid & "'"
        Else

            sql = "sp_delComponentFailMode '" & coid & "', '" & failid & "','" & oaid & "'"


        End If

        fail.Update(sql)
        fail.Dispose()
    End Sub
    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        FromComp()
    End Sub
    Private Sub FromComp()
        cid = lblcid.Value
        coid = lblcoid.Value
        Dim Item As ListItem
        Dim f, fi As String
        Dim fail As New Utilities
        fail.Open()
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        PopFailList(cid, coid)
        PopFail(cid, coid)
        PopFailComp(cid, coid)
        fail.Dispose()
    End Sub
    Private Sub btnaddfail_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnaddfail.Click
        If txtnewfail.Text <> "" Then
            fail.Open()
            Dim failstr As String

            failstr = txtnewfail.Text
            failstr = Replace(failstr, "'", Chr(180), , , vbTextCompare)
            failstr = Replace(failstr, "--", "-", , , vbTextCompare)
            failstr = Replace(failstr, ";", ":", , , vbTextCompare)
            cid = lblcid.Value
            'cid = "0"
            coid = lblcoid.Value
            'coid = HttpContext.Current.Session("coid").ToString()
            sid = lblsid.Value
            Dim strchk As Integer
            sql = "select count(*) from FailureModes where failuremode = '" & failstr & "'"
            strchk = fail.Scalar(sql)
            If strchk = 0 Then
                Dim fid As Integer
                failstr = txtnewfail.Text
                sql = "insert into FailureModes " _
                + "(compid, failuremode, fmeng) values ('" & cid & "', '" & failstr & "','" & failstr & "') select @@identity as 'identity'"
                fid = fail.Scalar(sql)
                sql = "usp_addSiteFM '" & cid & "', '" & sid & "', '" & fid & "', '" & failstr & "'"
                fail.Update(sql)
            Else
                Dim strMessage As String = tmod.getmsg("cdstr795", "AddFail.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            PopFail(cid, coid)
        End If
    End Sub


    Private Sub lbfailmodes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbfailmodes.SelectedIndexChanged
        If lblopt.Value = "1" Then
            FromComp()
        End If

    End Sub


    Private Sub cbopts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopts.SelectedIndexChanged

        Dim opt As String = cbopts.SelectedValue.ToString
        lblopt.Value = opt
        If opt = "0" Then
            lbfailmodes.AutoPostBack = False
            lbfailmaster.AutoPostBack = False
        Else
            lbfailmodes.AutoPostBack = True
            lbfailmaster.AutoPostBack = True
        End If
    End Sub

    Private Sub lbfailmaster_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbfailmaster.SelectedIndexChanged
        If lblopt.Value = "1" Then
            ToComp()
        End If
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1912.Text = axlabs.GetASPXPage("AddFail.aspx", "lang1912")
        Catch ex As Exception
        End Try
        Try
            lang1913.Text = axlabs.GetASPXPage("AddFail.aspx", "lang1913")
        Catch ex As Exception
        End Try
        Try
            lang1914.Text = axlabs.GetASPXPage("AddFail.aspx", "lang1914")
        Catch ex As Exception
        End Try
        Try
            lang1915.Text = axlabs.GetASPXPage("AddFail.aspx", "lang1915")
        Catch ex As Exception
        End Try
        Try
            lang1916.Text = axlabs.GetASPXPage("AddFail.aspx", "lang1916")
        Catch ex As Exception
        End Try
        Try
            lang1917.Text = axlabs.GetASPXPage("AddFail.aspx", "lang1917")
        Catch ex As Exception
        End Try
        Try
            lang1918.Text = axlabs.GetASPXPage("AddFail.aspx", "lang1918")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnaddfail.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnaddfail.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnaddfail.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnaddfail.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnaddfail.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddtosite.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("AddFail.aspx", "btnaddtosite") & "')")
            btnaddtosite.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

    Private Sub ddoca_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddoca.SelectedIndexChanged
        Dim coid As String = lblcoid.Value
        Dim cid As String = "0"
        fail.Open()
        isecd = lblisecd.Value
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If isecd = "1" And oca <> "ALL" Then
            checkecd()
        End If
        PopFailList(cid, coid)
        PopFailComp(cid, coid)
        fail.Dispose()
    End Sub
    Private Sub checkecd()
        isecd = lblisecd.Value
        Dim ecd2, ecd2id, ecd1id As String
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        Dim coid As String = lblcoid.Value
        ecd1id = lblecd1id.Value
        If isecd = "1" Then
            sql = "select count(*) from ecd2 where ecd1id = '" & ecd1id & "' and ecddesc = '" & oca & "'"
        Else
            sql = "select count(*) from componentfailmodes where oaid = '" & oaid & "' and comid = '" & coid & "'"
        End If

        Dim oaidcnt As Integer = fail.Scalar(sql)
        If oaidcnt = 0 Then
            lblcurrecd2.Value = ""
            lblnewecd3.Value = ""
            lblgetecd2.Value = "1"
            lblecd2txt.Value = oca
            'lblecd1id.Value = ""
        Else
            If isecd = "1" Then
                sql = "select ecd1id, ecd2id from ecd2 where ecd1id = '" & ecd1id & "' and ecddesc = '" & oca & "'"
            Else
                sql = "select e.ecd2, e.ecd1id, c.ecd2id from componentfailmodes c left join ecd2 e on e.ecd2id = c.ecd2id where c.oaid = '" & oaid & "' and c.comid = '" & coid & "'"
            End If

            dr = fail.GetRdrData(sql)
            While dr.Read
                ecd2id = dr.Item("ecd2id").ToString
                ecd1id = dr.Item("ecd1id").ToString
            End While
            dr.Close()
            lblcurrecd2.Value = ecd2id & "-" & ecd1id
            lblnewecd3.Value = ""
            lblgetecd2.Value = "0"
            lblecd1id.Value = ecd1id
        End If
    End Sub
End Class
