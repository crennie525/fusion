﻿Imports System.Data.SqlClient
Public Class eqmasterselect
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim win As New Utilities
    Dim eqid, sid, acid, rbchk As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'eqid = Request.QueryString("eqid").ToString
            sid = Request.QueryString("sid").ToString
            'lbleqid.Value = eqid
            lblsid.Value = sid
            win.Open()
            getac()
            acid = "0"
            lblacid.Value = "0"
            loadmaster(sid, acid)
            win.Dispose()

        Else
            If lblret.Value = "excel" Then
                win.Open()
                BindExport()
                win.Dispose()
            ElseIf lblret.Value = "switch" Then
                lblret.Value = ""
                lblelist.Value = ""
                win.Open()
                acid = lblacid.Value
                sid = lblsid.Value
                loadmaster(sid, acid)
                loadchildlist(eqid, sid)
                win.Dispose()
            End If


        End If
    End Sub
    Private Sub ddac_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddac.SelectedIndexChanged
        acid = ddac.SelectedValue
        lblacid.Value = acid
        lblelist.Value = ""
        sid = lblsid.Value
        win.Open()
        loadmaster(sid, acid)
        loadchildlist(eqid, sid)
        win.Dispose()
    End Sub
    Private Sub getac()
        'cid = "0" ' lblcid.value
        sql = "select * from pmAssetClass"
        dr = win.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        Try
            ddac.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Private Sub loadmaster(ByVal sid As String, ByVal acid As String)
        If rbcho.Checked = True Then
            rbchk = "ch"
        Else
            rbchk = "ms"
        End If
        If acid = "0" Then
            If rbchk = "ch" Then
                sql = "select count(*) from equipment where desig = 2 and siteid = '" & sid & "' and desigmstr is not NULL"
            Else
                sql = "select count(*) from equipment where desig = 1 and siteid = '" & sid & "'"
            End If

        Else
            If rbchk = "ch" Then
                sql = "select count(*) from equipment where desig = 2 and siteid = '" & sid & "' and desigmstr is not NULL and acid = '" & acid & "'"
            Else
                sql = "select count(*) from equipment where desig = 1 and siteid = '" & sid & "' and acid = '" & acid & "'"
            End If

        End If

        Dim ccnt As Integer
        ccnt = win.Scalar(sql)
        If ccnt > 0 Then
            If acid = "0" Then
                If rbchk = "ch" Then
                    sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where desig = 2 and siteid = '" & sid & "' and desigmstr is not NULL"
                Else
                    sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where desig = 1 and siteid = '" & sid & "'"
                End If

            Else
                If rbchk = "ch" Then
                    sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where desig = 2 and siteid = '" & sid & "' and desigmstr is not NULL and acid = '" & acid & "'"
                Else
                    sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where desig = 1 and siteid = '" & sid & "' and acid = '" & acid & "'"
                End If

            End If

            dr = win.GetRdrData(sql)
            lbfailmaster.DataSource = dr
            lbfailmaster.DataTextField = "eqnum"
            lbfailmaster.DataValueField = "eqid"
            lbfailmaster.DataBind()
            dr.Close()
        Else
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub loadchildlist(ByVal eqid As String, ByVal sid As String)
        Dim elist As String
        elist = lblelist.Value
        If elist <> "" Then
            sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where eqid in (" & elist & ")" 'desig = 1 and siteid = '" & sid & "'"
            dr = win.GetRdrData(sql)
            lbfailmodes.DataSource = dr
            lbfailmodes.DataValueField = "eqid"
            lbfailmodes.DataTextField = "eqnum"
            lbfailmodes.DataBind()
            dr.Close()
        Else
            sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where eqid is NULL" 'desig = 1 and siteid = '" & sid & "'"
            dr = win.GetRdrData(sql)
            lbfailmodes.DataSource = dr
            lbfailmodes.DataValueField = "eqid"
            lbfailmodes.DataTextField = "eqnum"
            lbfailmodes.DataBind()
            dr.Close()
        End If

    End Sub

    Protected Sub btntocomp_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        eqid = lbleqid.Value
        sid = lblsid.Value
        Dim elist As String
        Dim Item As ListItem
        Dim f, fi As String
        win.Open()
        'sql = "select eqid, eqnum from equipment where desigmstr = '" & eqid & "' and siteid = '" & sid & "'"
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                'sql = "update equipment set locked = 1, desig = 2, desigmstr = '" & eqid & "', pmostat = 3 where eqid = '" & fi & "'"
                'win.Update(sql)
                If Len(elist) = 0 Then
                    elist = fi
                Else
                    elist += "," & fi
                End If
            End If

        Next
        lblelist.Value = elist
        acid = lblacid.Value
        loadmaster(sid, acid)
        loadchildlist(eqid, sid)

        win.Dispose()
    End Sub

    Protected Sub btnfromcomp_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        eqid = lbleqid.Value
        sid = lblsid.Value
        Dim elist, nlist As String
        elist = lblelist.Value
        Dim Item As ListItem
        Dim f, fi As String
        win.Open()
        'sql = "select eqid, eqnum from equipment where desigmstr = '" & eqid & "' and siteid = '" & sid & "'"
        For Each Item In lbfailmodes.Items
            If Item.Selected = False Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                If nlist = "" Then
                    nlist = fi
                Else
                    nlist += "," & fi
                End If
                'sql = "update equipment set desigmstr = NULL, pmostat = 0 where eqid = '" & fi & "'"
                'win.Update(sql)

            End If

        Next
        lblelist.Value = nlist
        acid = lblacid.Value
        loadmaster(sid, acid)
        loadchildlist(eqid, sid)

        win.Dispose()
    End Sub
    Private Sub ExportDG()
        BindExport()
        cmpDataGridToExcel.DataGridToExcelNHD(dgout, Response)
    End Sub
    Private Sub BindExport()
        Dim osql As String
        'sql = "usp_GetAssignmentReport_new2 '" & sessid & "'"
        'osql = "usp_GetAssignmentReport_excel '" & sessid & "'"
        Dim elist As String
        elist = lblelist.Value
        osql = "usp_GetAssignmentReport_excel_mch1 '" & elist & "'"
        Dim ds As New DataSet
        ds = win.GetDSData(osql)
        Dim dv As DataView
        dv = ds.Tables(0).DefaultView
        Dim cnt As Integer = ds.Tables(0).Rows.Count
        dgout.DataSource = dv
        dgout.DataBind()
    End Sub

    
End Class