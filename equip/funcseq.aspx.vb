

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class funcseq
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqid, Login, sql, ro As String
    Dim ds As DataSet
    Dim dr As SqlDataReader
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim func As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgfunc As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here

        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            Dim user As String = HttpContext.Current.Session("username").ToString
            lbluser.Value = user
        Catch ex As Exception
            lbllog.value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgfunc.Columns(0).Visible = False
                'addfunc.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                'addfunc.Enabled = False
            End If
            eqid = Request.QueryString("eqid").ToString
            lbleqid.value = eqid
            func.Open()
            BindGrid(eqid)
            func.Dispose()

        End If
        'addfunc.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'addfunc.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'ibtnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub BindGrid(ByVal eqid As String)
        sql = "select f.* from functions f " _
        + "where f.eqid = '" & eqid & "' order by f.routing"
        dr = func.GetRdrData(sql)
        dgfunc.DataSource = dr
        dgfunc.DataBind()
        dr.Close()
    End Sub

    Private Sub dgfunc_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.EditCommand
        eqid = lbleqid.Value
        dgfunc.EditItemIndex = e.Item.ItemIndex
        func.Open()
        BindGrid(eqid)
        func.Dispose()
    End Sub

    Private Sub dgfunc_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.UpdateCommand
        func.Open()
        eqid = lbleqid.Value
        Dim cid As String = "0"
        Dim user As String = lbluser.Value
        Dim mdate As DateTime
        Dim rte As String = CType(e.Item.FindControl("txtrouting"), TextBox).Text
        Dim orte As String = CType(e.Item.FindControl("lbloldrte"), Label).Text
        Dim rtechk As Long
        Try
            rtechk = System.Convert.ToInt32(rte)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr968" , "funcseq.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
      
        Dim fun, des, fid As String
        fid = CType(e.Item.FindControl("txtfuid"), Label).Text
        fun = CType(e.Item.FindControl("txtfunum"), TextBox).Text
        fun = func.ModString1(fun)

        If Len(fun) = 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr969" , "funcseq.aspx.vb")
 
            func.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        ElseIf Len(fun) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr970" , "funcseq.aspx.vb")
 
            func.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else
            Dim fcnt As Integer
            sql = "select count(*) from functions " _
            + "where eqid = '" & eqid & "' and func = '" & fun & "'"
            fcnt = func.Scalar(sql)
            If fcnt > 1 And fun <> "New Function" Then
                Dim strMessage As String =  tmod.getmsg("cdstr971" , "funcseq.aspx.vb")
 
                func.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                Dim spl As String = CType(e.Item.FindControl("txtspl"), TextBox).Text
                spl = func.ModString1(spl)
                If Len(spl) > 100 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr972" , "funcseq.aspx.vb")
 
                    func.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
                mdate = func.CNOW
                sql = "update functions set " _
                + "compid = '" & cid & "', " _
                + "eqid = '" & eqid & "', " _
                + "func = '" & fun & "', " _
                + "spl = '" & spl & "', " _
                + "modifiedby = '" & user & "', " _
                + "modifieddate = '" & mdate & "' " _
                + "where func_id = '" & fid & "'"
                Try
                    func.Update(sql)
                Catch ex As Exception
                    Dim strMessage As String =  tmod.getmsg("cdstr973" , "funcseq.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
                dgfunc.EditItemIndex = -1
                If rte <> orte And rte <> "0" Then
                    sql = "usp_rerouteFunctions '" & eqid & "', '" & rte & "', '" & orte & "'"
                    func.Update(sql)
                ElseIf rte = "0" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr974" , "funcseq.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If
            

            BindGrid(eqid)
        End If
        func.Dispose()
    End Sub

    Private Sub dgfunc_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.CancelCommand
        dgfunc.EditItemIndex = -1
        eqid = lbleqid.Value
        func.Open()
        BindGrid(eqid)
        func.Dispose()
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgfunc.Columns(0).HeaderText = dlabs.GetDGPage("funcseq.aspx","dgfunc","0")
		Catch ex As Exception
		End Try
		Try
			dgfunc.Columns(1).HeaderText = dlabs.GetDGPage("funcseq.aspx","dgfunc","1")
		Catch ex As Exception
		End Try
		Try
			dgfunc.Columns(2).HeaderText = dlabs.GetDGPage("funcseq.aspx","dgfunc","2")
		Catch ex As Exception
		End Try
		Try
			dgfunc.Columns(3).HeaderText = dlabs.GetDGPage("funcseq.aspx","dgfunc","3")
		Catch ex As Exception
		End Try

	End Sub

End Class
