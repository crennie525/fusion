<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EqBotGrid.aspx.vb" Inherits="lucy_r12.EqBotGrid" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>EqBotGrid</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
    <script  type="text/javascript" src="../scripts1/EqBotGridaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
</head>
<body class="tbg" onload="checkit();" onunload="handleeq();">
    <form id="form1" method="post" runat="server">
    <table style="position: absolute; top: 0px; left: 0px" width="720">
        <tbody>
            <tr id="tradd" runat="server">
                <td colspan="3">
                    <table>
                        <tr>
                            <td class="label tbg" style="width: 140px" align="left" height="20">
                                <asp:Label ID="lang2167" runat="server">Add New Equipment#</asp:Label>
                            </td>
                            <td style="width: 140px" class="tbg">
                                <asp:TextBox ID="txtneweq" runat="server" Width="130px" MaxLength="50"></asp:TextBox>
                            </td>
                            <td style="width: 60px" class="tbg">
                                <asp:ImageButton ID="btnAddEq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif">
                                </asp:ImageButton>
                            </td>
                            <td width="380" class="bluelabel tbg" id="tduse" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="spdiv" style="overflow: auto; width: 740px; height: 300px">
                        <asp:Repeater ID="rptreq" runat="server">
                            <HeaderTemplate>
                                <table cellspacing="0" width="720">
                                    <tr class="tbg" height="20">
                                        <td class="btmmenu plainlabel" style="width: 130px">
                                            <asp:LinkButton OnClick="SortEq" ID="lbsorteq" runat="server">
                                                <asp:Label ID="lang2168" runat="server">Equipment#</asp:Label></asp:LinkButton>
                                        </td>
                                        <td class="btmmenu plainlabel" width="295">
                                            <asp:Label ID="lang2169" runat="server">Description</asp:Label>
                                        </td>
                                        <td class="btmmenu plainlabel" width="295">
                                            <asp:Label ID="lang2170" runat="server">Special Identifier</asp:Label>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="tbg" height="20">
                                    <td class="plainlabel">
                                        &nbsp;
                                        <asp:LinkButton ID="Label4" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel">
                                        <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbleqloctypei" Text='<%# DataBinder.Eval(Container.DataItem,"loctype")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="transrowblue" height="20">
                                    <td class="plainlabel transrowblue">
                                        &nbsp;
                                        <asp:LinkButton ID="Linkbutton1" CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                            runat="server">
                                        </asp:LinkButton>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="plainlabel transrowblue">
                                        <asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                    <td class="details">
                                        <asp:Label ID="lbleqloctypea" Text='<%# DataBinder.Eval(Container.DataItem,"loctype")%>'
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" id="lblrd" runat="server">
    <input type="hidden" id="lbleqid" runat="server">
    <input type="hidden" id="lblchk" runat="server">
    <input type="hidden" id="lbldchk" runat="server">
    <input type="hidden" id="lblsid" runat="server">
    <input type="hidden" id="lblcid" runat="server">
    <input type="hidden" id="lbldid" runat="server">
    <input type="hidden" id="lblclid" runat="server"><input type="hidden" id="lbluser"
        runat="server">
    <input type="hidden" id="lbladdchk" runat="server"><input type="hidden" id="appchk"
        runat="server" name="appchk">
    <input type="hidden" id="lbllid" runat="server"><input type="hidden" id="lbltyp"
        runat="server">
    <input type="hidden" id="lbllog" runat="server"><input type="hidden" id="lblcurreqsort"
        runat="server" name="lblcurreqsort">
    <input type="hidden" id="lblpar" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    </form>
</body>
</html>
