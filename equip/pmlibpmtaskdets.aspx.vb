

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmlibpmtaskdets
    Inherits System.Web.UI.Page
	Protected WithEvents lang2525 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2524 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2523 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2522 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2521 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2520 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2519 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2518 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2517 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2516 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2515 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2514 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim eqid, db, start, fuid As String
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtaskrev As System.Web.UI.WebControls.Repeater

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                fuid = Request.QueryString("fuid").ToString
                db = Request.QueryString("db").ToString
                lblfuid.Value = fuid
                lbldb.Value = db
                copy.Open()
                PopTaskRev(fuid)
                copy.Dispose()
            Else
                tdmsg.InnerHtml = "Waiting for data..."
                trmsg.Attributes.Add("class", "view")
            End If

        End If
    End Sub
    Private Sub PopTaskRev(ByVal funcid As String)
        Dim db As String = lbldb.Value
        Dim lang As String = lblfslang.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        db = mdbarr(2).ToString
        Dim tcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & db & "].[dbo].[pmtasks] where funcid = '" & funcid & "'"
        tcnt = copy.Scalar(sql)
        If tcnt = 0 Then
            tdmsg.InnerHtml = "No Records Found"
            trmsg.Attributes.Add("class", "view")
        Else
            trmsg.Attributes.Add("class", "details")
        End If
        sql = "usp_getTaskReview '" & funcid & "', '" & db & "','" & srvr & "','" & lang & "'"
        dr = copy.GetRdrData(sql)
        rptrtaskrev.DataSource = dr
        rptrtaskrev.DataBind()
        dr.Close()
    End Sub
	



    Private Sub rptrtaskrev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtaskrev.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2514 As Label
                lang2514 = CType(e.Item.FindControl("lang2514"), Label)
                lang2514.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2514")
            Catch ex As Exception
            End Try
            Try
                Dim lang2515 As Label
                lang2515 = CType(e.Item.FindControl("lang2515"), Label)
                lang2515.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2515")
            Catch ex As Exception
            End Try
            Try
                Dim lang2516 As Label
                lang2516 = CType(e.Item.FindControl("lang2516"), Label)
                lang2516.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2516")
            Catch ex As Exception
            End Try
            Try
                Dim lang2517 As Label
                lang2517 = CType(e.Item.FindControl("lang2517"), Label)
                lang2517.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2517")
            Catch ex As Exception
            End Try
            Try
                Dim lang2518 As Label
                lang2518 = CType(e.Item.FindControl("lang2518"), Label)
                lang2518.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2518")
            Catch ex As Exception
            End Try
            Try
                Dim lang2519 As Label
                lang2519 = CType(e.Item.FindControl("lang2519"), Label)
                lang2519.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2519")
            Catch ex As Exception
            End Try
            Try
                Dim lang2520 As Label
                lang2520 = CType(e.Item.FindControl("lang2520"), Label)
                lang2520.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2520")
            Catch ex As Exception
            End Try
            Try
                Dim lang2521 As Label
                lang2521 = CType(e.Item.FindControl("lang2521"), Label)
                lang2521.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2521")
            Catch ex As Exception
            End Try
            Try
                Dim lang2522 As Label
                lang2522 = CType(e.Item.FindControl("lang2522"), Label)
                lang2522.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2522")
            Catch ex As Exception
            End Try
            Try
                Dim lang2523 As Label
                lang2523 = CType(e.Item.FindControl("lang2523"), Label)
                lang2523.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2523")
            Catch ex As Exception
            End Try
            Try
                Dim lang2524 As Label
                lang2524 = CType(e.Item.FindControl("lang2524"), Label)
                lang2524.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2524")
            Catch ex As Exception
            End Try
            Try
                Dim lang2525 As Label
                lang2525 = CType(e.Item.FindControl("lang2525"), Label)
                lang2525.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2525")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2514.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2514")
        Catch ex As Exception
        End Try
        Try
            lang2515.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2515")
        Catch ex As Exception
        End Try
        Try
            lang2516.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2516")
        Catch ex As Exception
        End Try
        Try
            lang2517.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2517")
        Catch ex As Exception
        End Try
        Try
            lang2518.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2518")
        Catch ex As Exception
        End Try
        Try
            lang2519.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2519")
        Catch ex As Exception
        End Try
        Try
            lang2520.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2520")
        Catch ex As Exception
        End Try
        Try
            lang2521.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2521")
        Catch ex As Exception
        End Try
        Try
            lang2522.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2522")
        Catch ex As Exception
        End Try
        Try
            lang2523.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2523")
        Catch ex As Exception
        End Try
        Try
            lang2524.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2524")
        Catch ex As Exception
        End Try
        Try
            lang2525.Text = axlabs.GetASPXPage("pmlibpmtaskdets.aspx", "lang2525")
        Catch ex As Exception
        End Try

    End Sub

End Class
