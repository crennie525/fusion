<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibtpmtaskdets.aspx.vb" Inherits="lucy_r12.pmlibtpmtaskdets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibtpmtaskdets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px">
				<TBODY>
					<tr id="trmsg" runat="server" height="30" class="details">
						<td colspan="7" id="tdmsg" runat="server" class="plainlabelblue"></td>
					</tr>
					<tr>
						<td colSpan="7"><div style="OVERFLOW: auto; WIDTH: 990px; HEIGHT: 260px">
								<asp:repeater id="rptrtaskrev" Runat="server">
									<HeaderTemplate>
										<table width="1280">
											<tr>
												<td width="40" class="btmmenu plainlabel"><asp:Label id="lang2526" runat="server">Task#</asp:Label></td>
												<td width="70" class="btmmenu plainlabel"><asp:Label id="lang2527" runat="server">Sub Task#</asp:Label></td>
												<td width="200" class="btmmenu plainlabel"><asp:Label id="lang2528" runat="server">Component Addressed</asp:Label></td>
												<td width="30" class="btmmenu plainlabel">Qty</td>
												<td width="220" class="btmmenu plainlabel"><asp:Label id="lang2529" runat="server">Failure Modes Addressed</asp:Label></td>
												<td width="260" class="btmmenu plainlabel"><asp:Label id="lang2530" runat="server">Task Description</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2531" runat="server">Task Type</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2532" runat="server">Operator Qty</asp:Label></td>
												<td width="40" class="btmmenu plainlabel"><asp:Label id="lang2533" runat="server">Time</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2534" runat="server">Frequency</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2535" runat="server">Eq Status</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2536" runat="server">Down Time</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg">
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' Runat = server ID="Label5">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"subtask")%>' Runat = server ID="Label6">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server ID="Label11">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"cqty")%>' Runat = server ID="Label12">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"fm1")%>' Runat = server ID="Label13">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' Runat = server ID="Label14">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasktype")%>' Runat = server ID="Label15">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"qty")%>' Runat = server ID="Label18">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"ttime")%>' Runat = server ID="Label19">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"freq")%>' Runat = server ID="Label20">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rd")%>' Runat = server ID="Label21">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rdt")%>' Runat = server ID="Label22">
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD></TR></TBODY></TABLE> <input type="hidden" id="lbldb" runat="server" NAME="lbldb">
			<input type="hidden" id="lblfuid" runat="server" NAME="lblfuid">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
