

'********************************************************
'*
'********************************************************



Public Class CompCopyMiniDialogtpm
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, eqid, fuid, sid, Login, did, clid, usr, ro As String
    Protected WithEvents ifco As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            Try
                fuid = Request.QueryString("fuid").ToString
                If Len(fuid) <> 0 AndAlso fuid <> "" AndAlso fuid <> "0" Then
                    eqid = Request.QueryString("eqid").ToString
                    cid = Request.QueryString("cid").ToString
                    sid = Request.QueryString("sid").ToString
                    did = Request.QueryString("did").ToString
                    clid = Request.QueryString("clid").ToString
                    usr = Request.QueryString("usr").ToString
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    ifco.Attributes.Add("src", "CompCopyMinitpm.aspx?sid=" & sid & "&cid=" & cid & "&eqid=" & eqid & "&fuid=" & fuid & "&did=" & did & "&clid=" & clid & "&usr=" & usr & "&ro=" & ro)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr818" , "CompCopyMiniDialogtpm.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If

            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr819" , "CompCopyMiniDialogtpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End Try

        End If
    End Sub

End Class
