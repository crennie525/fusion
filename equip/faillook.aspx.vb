﻿Imports System.Data.SqlClient
Imports System.Text

Public Class faillook
    Inherits System.Web.UI.Page
    Dim fail As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim sb As New StringBuilder
            Dim fm, fmi As String
            sb.Append("<table>")
            sql = "select * from failuremodes order by failuremode"
            fail.Open()
            dr = fail.GetRdrData(sql)
            While dr.Read
                sb.Append("<tr><td class=""plainlabel"">")
                fm = dr.Item("failuremode").ToString
                fmi = dr.Item("failid").ToString
                sb.Append("<a href=""#"" onclick=""getfm('" & fmi & "','" & fm & "')"">" & fm & "</a>")
                sb.Append("</td></tr>")
            End While
            dr.Close()
            fail.Dispose()
            sb.Append("</table>")
            fdiv.InnerHtml = sb.ToString
        End If
    End Sub

End Class