

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class funclistdet
    Inherits System.Web.UI.Page
	Protected WithEvents lang2406 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2405 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2404 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2403 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2402 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2401 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2400 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, eqid, Login, srchtyp, fuid, eqnum As String
    Dim dr As SqlDataReader
    Protected WithEvents divco As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trsave As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents xcoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ycoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rep As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divfu As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrchtyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                eqnum = Request.QueryString("eqnum").ToString
                tdeq.InnerHtml = eqnum
                rep.Open()
                GetFunctions()
                rep.Dispose()
                xcoord.Value = "0"
                ycoord.Value = "0"
            Catch ex As Exception

            End Try
        Else
            If Request.Form("lblsubmit") = "getco" Then
                lblsubmit.Value = ""
                rep.Open()
                GetComponents()
                rep.Dispose()
            End If
        End If
    End Sub
    Private Sub GetFunctions()
        eqid = lbleqid.Value
        Dim fi, fu, spl, md As String
        Dim sb As New StringBuilder

        sql = "select func_id, func, spl, modifieddate from functions where eqid = '" & eqid & "' order by routing"
        dr = rep.GetRdrData(sql)
        sb.Append("<table>")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""200"">" & tmod.getlbl("cdlbl442" , "funclistdet.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""200"">" & tmod.getlbl("cdlbl443" , "funclistdet.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""120"">" & tmod.getlbl("cdlbl444" , "funclistdet.aspx.vb") & "</td></tr>" & vbCrLf)
        Dim hbg As String = "transrowblue"
        Dim nbg As String = "transrow"
        Dim bg As String = hbg
        While dr.Read
            fi = dr.Item("func_id").ToString
            fu = dr.Item("func").ToString
            spl = dr.Item("spl").ToString
            md = dr.Item("modifieddate").ToString
            If bg = hbg Then
                bg = nbg
            Else
                bg = hbg
            End If
            sb.Append("<tr><td class=""plainlabel " & bg & """>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdfu('" & fi & "','" & fu & "','down')"">")
            sb.Append(fu)
            sb.Append("</td>")

            sb.Append("<td class=""plainlabel " & bg & """>" & spl & "</td>")

            sb.Append("<td class=""plainlabel " & bg & """>" & md & "</td>")

            sb.Append("</tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divfu.InnerHtml = sb.ToString

    End Sub
    Private Sub GetComponents()

        fuid = lblfuid.Value
        Dim ci, co, cd, cs, cg As String
        Dim sb As New StringBuilder
        srchtyp = lblsrchtyp.Value

        sql = "select comid, compnum, compdesc, spl, desig from components where func_id = '" & fuid & "' order by crouting"
        dr = rep.GetRdrData(sql)
        sb.Append("<table>")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""180"">" & tmod.getlbl("cdlbl445" , "funclistdet.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""190"">" & tmod.getlbl("cdlbl446" , "funclistdet.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""190"">" & tmod.getlbl("cdlbl447" , "funclistdet.aspx.vb") & "</td>" & vbCrLf)
        sb.Append("<td class=""thdrsingg plainlabel"" width=""190"">" & tmod.getlbl("cdlbl448" , "funclistdet.aspx.vb") & "</td></tr>" & vbCrLf)
        Dim hbg As String = "transrowblue"
        Dim nbg As String = "transrow"
        Dim bg As String = hbg
        While dr.Read
            ci = dr.Item("comid").ToString
            co = dr.Item("compnum").ToString
            cd = dr.Item("compdesc").ToString
            cs = dr.Item("spl").ToString
            cg = dr.Item("desig").ToString
            If bg = hbg Then
                bg = nbg
            Else
                bg = hbg
            End If
            sb.Append("<tr><td class=""plainlabel " & bg & """>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdco('" & ci & "','" & co & "')"">")
            sb.Append(co & "</a>")
            sb.Append("</td>")

            sb.Append("<td class=""plainlabel " & bg & """>" & cd & "</td>")
            sb.Append("<td class=""plainlabel " & bg & """>" & cs & "</td>")
            sb.Append("<td class=""plainlabel " & bg & """>" & cg & "</td>")
            sb.Append("</tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divco.InnerHtml = sb.ToString

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2400.Text = axlabs.GetASPXPage("funclistdet.aspx", "lang2400")
        Catch ex As Exception
        End Try
        Try
            lang2401.Text = axlabs.GetASPXPage("funclistdet.aspx", "lang2401")
        Catch ex As Exception
        End Try
        Try
            lang2402.Text = axlabs.GetASPXPage("funclistdet.aspx", "lang2402")
        Catch ex As Exception
        End Try
        Try
            lang2403.Text = axlabs.GetASPXPage("funclistdet.aspx", "lang2403")
        Catch ex As Exception
        End Try
        Try
            lang2404.Text = axlabs.GetASPXPage("funclistdet.aspx", "lang2404")
        Catch ex As Exception
        End Try
        Try
            lang2405.Text = axlabs.GetASPXPage("funclistdet.aspx", "lang2405")
        Catch ex As Exception
        End Try
        Try
            lang2406.Text = axlabs.GetASPXPage("funclistdet.aspx", "lang2406")
        Catch ex As Exception
        End Try

    End Sub

End Class
