<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EQBot.aspx.vb" Inherits="lucy_r12.EQBot" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>EQBot</title><a href="~/menu/mmenu1.ascx"></a>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
    <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    <script  type="text/javascript" src="../scripts/equipment_1.js"></script>
    <script  type="text/javascript" src="../scripts1/EQBotaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <style type="text/css">
        .plainlabeltmp
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 8pt;
            text-decoration: none;
            color: black;
        }
        .plainlabel11ptmp
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 11px;
            text-decoration: none;
            color: black;
        }
        .label11ptmp
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 11px;
            font-weight: bold;
            text-decoration: none;
            color: black;
        }
        .label11pbtmp
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 11px;
            font-weight: bold;
            text-decoration: none;
            color: blue;
        }
        .label11pttmp
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 11px;
            font-weight: bold;
            text-decoration: none;
            color: red;
        }
        
        .labeltmp
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 8pt;
            font-weight: bold;
            text-decoration: none;
            color: black;
        }
        .labelgrtmp
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 8pt;
            font-weight: bold;
            text-decoration: none;
            color: Gray;
        }
        .labelbluetmp
        {
            font-family: MS Sans Serif, arial, sans-serif, Verdana;
            font-size: 8pt;
            font-weight: bold;
            text-decoration: none;
            color: Blue;
        }
        #btnunlock
        {
            width: 20px;
        }
    </style>
    <script  type="text/javascript">
    <!--
        function getpmostat(stat) {
            document.getElementById("lblpchk").value = "changepmo";
            document.getElementById("form1").submit();
        }
    function checkchild(cval) {
        //alert(cval)
        var popwin = "directories=0,height=540,width=540,location=0,menubar=0,resizable=1,status=0,toolbar=0,scrollbars=1,top=100,left=400";
        if (cval == "2") {
            var eqid = document.getElementById("lbleqid").value;
            window.open("getmasterrecords.aspx?eqid=" + eqid + "&date=" + Date(), "repWin", popwin);
            //document.getElementById("lblpchk").value = "changedesig";
            //document.getElementById("form1").submit();
        }
        else {
            document.getElementById("lblpchk").value = "changedesig";
            document.getElementById("form1").submit();
        }

    }
    function checkmast() {
        document.getElementById("lblpchk").value = "changedesig";
        document.getElementById("form1").submit();
    }

        function checkdown(who) {

            var wo = ""; // document.getElementById("lblwonum").value;
            var eqid = document.getElementById("lbleqid").value;
            var usr = document.getElementById("lbluser").value;
            var isdown = document.getElementById("lblisdown").value;
            var stat = ""; // document.getElementById("lblstat").value;
            var eReturn = window.showModalDialog("woexpddialog.aspx?wo=" + wo + "&eqid=" + eqid + "&usr=" + usr + "&isdown=" + isdown + "&stat=" + stat, "", "dialogHeight:320px; dialogWidth:560px; resizable=yes");
            if (eReturn) {
                var ret = eReturn;
                document.getElementById("lblisdown").value = ret;
                if (ret == "1") {
                    cbd.checked = true;
                }
                else {
                    cbd.checked = false;
                }
            }
            else {

                //wo = document.getElementById("lblwonum").value;
                //sid = document.getElementById("lblsid").value;

                //window.location = "woman3.aspx?jump=yes&wo=" + wo + "&sid=" + sid + "&usrname=" + usr;
            }
        }
        function getcharge(typ) {
            window.parent.setref();
            var eReturn = window.showModalDialog("../admin/ChargeDialog.aspx", getchargeCallback, "dialogHeight:350px; dialogWidth:350px; resizable=yes");
            if (eReturn) {
                getchargeCallback(eReturn);
            }
        }

        function getchargeCallback(eReturn) {
            if (eReturn != "ok") {
                document.getElementById("txtcharge").value = eReturn;
                document.getElementById("lblcharge").value = eReturn;
            }
        }

        function GetEqCopy() {
            window.parent.setref();
            handleapp();
            sid = document.getElementById("lblsid").value;
            dept = document.getElementById("lbldept").value;
            cell = document.getElementById("lblclid").value;
            eqid = document.getElementById("lbleqid").value;
            lid = document.getElementById("lbllid").value;
            ro = document.getElementById("lblro").value;
            //alert("../equip/EQCopyMiniDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid)
            var eReturn = window.showModalDialog("../equip/EQCopyMiniDialog.aspx?sid=" + sid + "&dept=" + dept + "&cell=" + cell + "&eqid=" + eqid + "&lid=" + lid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:550px; dialogWidth:650px; resizable=yes");
            if (eReturn) {
                if (eReturn != "no") {
                    //document.getElementById("lbleqid").value = eReturn;
                    //document.getElementById("lblpchk").value = "eq";
                    //document.getElementById("form1").submit();
                    gridret();
                }
            }
        }
        var val;
        function GoToTasks(val) {
            var appstr = document.getElementById("lblappstr").value;
            if (appstr != "no") {
                //alert(val)
                window.parent.handletask(val);
            }
        }
        function checkit() {
            var chld = document.getElementById("lblchld").value;
            //alert(chld)
            var lockchng = document.getElementById("lbllockchk").value;
            if (lockchng == "yes") {
                document.getElementById("lbllockchk").value = "no";
                window.parent.handlearch();
            }
            var add = document.getElementById("lbladdchk").value;
            if (add == "yes") {
                document.getElementById("lbladdchk").value = "";
                var eqid = document.getElementById("lbleqid").value;
                window.parent.handleeq('eq', 'add', eqid, chld);
            }
            else {
                //alert(chld)
                var eqid = document.getElementById("lbleqid").value;
                var eqnum = document.getElementById("txteqname").value;
                window.parent.handleeq('eq', 'other', eqid, eqnum, chld);
            }
            var app = document.getElementById("appchk").value;
            if (app == "switch") window.parent.handleapp(app);
            var val = document.getElementById("lblapp").value;
            window.parent.handleswitch(val);
            var del = document.getElementById("lbldel").value;
            if (del == "1") {
                gridret();
            }
            var chk = document.getElementById("lbllog").value;
            if (chk == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
        }
        function gridret() {
            handleapp();
            var chk = document.getElementById("lblchk").value;
            var dchk = document.getElementById("lbldchk").value;
            var sid = document.getElementById("lblsid").value;
            var did = document.getElementById("lbldept").value;
            var clid = document.getElementById("lblclid").value;
            var cid = document.getElementById("lblcid").value;
            var lid = document.getElementById("lbllid").value;
            var typ = document.getElementById("lbltyp").value;
            var loc = document.getElementById("lblloc").value;
            var par = document.getElementById("lblpar").value;
            var who = document.getElementById("lblwho").value;
            var val = "";
            if (par == "dept") {
                val = did;
            }
            else if (par == "cell") {
                val = clid;
            }
            else if (par == "loc") {
                val = lid;
            }
            window.parent.handleeq(par, chk, val);
            //window.parent.handleeq('func', 'chk', '');
            //alert(par + ", " + chk)
            //alert("EQBotGrid.aspx?who=" + who + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc)
            if (dchk == "yes" && chk == "yes") {

                window.location = "EQBotGrid.aspx?who=" + who + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&clid=" + clid + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc;
            }
            else if (dchk == "yes" && chk == "no") {
                //alert("EQBotGrid.aspx?who=" + who + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc)
                window.location = "EQBotGrid.aspx?who=" + who + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&did=" + did + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc;
            }
            else if (dchk == "no") {
                window.location = "EQBotGrid.aspx?who=" + who + "&start=yes&dchk=" + dchk + "&chk=" + chk + "&sid=" + sid + "&cid=" + cid + "&lid=" + lid + "&typ=" + typ + "&loc=" + loc;
            }

        }


        function getmeter() {
            eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("../equip/emeterdialog.aspx?eqid=" + eqid, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes")
            }
        }
        function getxtra() {
            eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("eqxtradialog.aspx?eqid=" + eqid, "", "dialogHeight:500px; dialogWidth:600px; resizable=yes")
            }
        }
        function checkghost() {
            //alert("Boo!")
            var cb = document.getElementById("cbtrans");
            if (cb.checked == true) {
                document.getElementById("lblxstatus").value = "yes";
            }
            else {
                document.getElementById("lblxstatus").value = "no";
            }
        }
        function refit() {
            document.getElementById("txtcharge").value = "";
            document.getElementById("lblcharge").value = "";
            document.getElementById("lblpchk").value = "saveeq";
            document.getElementById("form1").submit();

        }
        function archit() {
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("tpmrevdialog.aspx?eqid=" + eqid, "", "dialogHeight:300px; dialogWidth:400px; resizable=yes")
            }
        }
        //-->
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div class="FreezePaneOff" id="FreezePane" style="width: 720px" align="center">
        <div class="InnerFreezePane" id="InnerFreezePane">
        </div>
    </div>
    <div id="overDiv" style="z-index: 1000; position: absolute; visibility: hidden">
    </div>
    <table id="eqdetdiv" style="position: absolute; top: 0px; left: 4px; border-spacing: 1px; padding: 0px; width: 744px; border: 0px;">
        <tr>
            <td style="width: 92px">
            </td>
            <td style="width: 140px">
            </td>
            <td style="width: 20px">
            </td>
            <td style="width: 20px">
            </td>
            <td style="width: 20px">
            </td>
            <td style="width: 20px">
            </td>
            <td style="width: 44px">
            </td>
            <td style="width: 204px">
            </td>
            <td style="width: 150px">
            </td>
            <td style="width: 80px">
            </td>
        </tr>
        <tr>
            <td style="border-bottom: #7ba4e0 thin solid" colspan="10">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="label11pbtmp" align="left" style="width: 130px">
                            <asp:Label ID="lang2142" runat="server">Add New Equipment</asp:Label>
                        </td>
                        <td style="width: 130px" class="plainlabel11ptmp" colspan="2">
                            <asp:TextBox ID="txtneweq" runat="server" Width="120px" MaxLength="50"></asp:TextBox>
                        </td>
                        <td style="width: 50px" class="tbg">
                            &nbsp;<asp:ImageButton ID="btnAddEq" runat="server" ImageUrl="../images/appbuttons/minibuttons/addmod.gif">
                            </asp:ImageButton>
                        </td>
                        <td class="bluelabel tbg" id="tduse" style="width: 180px" runat="server">
                        </td>
                        <td class="redlabel" id="tdtrans" align="right" style="width: 194px" runat="server">
                            <input id="cbtrans" onclick="gettrans();" type="checkbox" name="cbtrans" runat="server"><asp:Label
                                ID="lang2143" runat="server">Submit For Translation</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="label11ptmp">
                <asp:Label ID="lang2144" runat="server">Equipment#</asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txteqname" runat="server" style="width: 150px" MaxLength="50" CssClass="plainlabel11ptmp"></asp:TextBox>
            </td>
            <td>
                <img onclick="GetPartDiv();" src="../images/appbuttons/minibuttons/spareparts.gif">
            </td>
            <td>
                <img onclick="getmeter();" onmouseover="return overlib('Add\Edit Meters for this Asset', ABOVE, LEFT)"
                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif">
            </td>
            <td>
                <img onclick="getxtra();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0" id="imgxtra" runat="server" />
            </td>
            <td class="label11pttmp">
                
            </td>
            <td class="label11pttmp">ECR:&nbsp;
                <asp:TextBox ID="txtecr" runat="server" style="width: 48px" CssClass="plainlabel11ptmp"></asp:TextBox>&nbsp;<img
                    id="btnecr" title="Calculate ECR" onclick="getcalc();" height="20" alt="" src="../images/appbuttons/minibuttons/ecrbutton.gif"
                    style="width: 26px" align="absMiddle" runat="server">
            </td>
            <td align="center" colspan="2" rowspan="9">
                <table>
                    <tr>
                        <td>
                            <a onclick="getbig();" href="#">
                                <img id="imgeq" height="216" src="../images/appimages/eqimg1.gif" style="width: 216px" border="0"
                                    runat="server"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="labelbluetmp" style="width: 80px">
                                        <asp:Label ID="lang2159" runat="server">Order</asp:Label>
                                    </td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtiorder" runat="server" style="width: 40px" CssClass="plainlabeltmp"></asp:TextBox>
                                    </td>
                                    <td style="width: 20px">
                                        <img onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif" height="18"
                                            width="18">
                                    </td>
                                    <td style="width: 20px">
                                        <img onmouseover="return overlib('Add\Edit Images for this Asset', ABOVE, LEFT)"
                                            onclick="addpic();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif"
                                            height="18" width="18">
                                    </td>
                                    <td style="width: 20px">
                                        <img id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
                                            src="../images/appbuttons/minibuttons/del.gif" runat="server" onmouseout="return nd()"
                                            height="18" width="18">
                                    </td>
                                    <td style="width: 20px">
                                        <img id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)"
                                            onclick="savdets();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif"
                                            runat="server" height="18" width="18">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="border-right: blue 1px solid; width: 20px"">
                                        <img id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst2.gif"
                                            runat="server">
                                    </td>
                                    <td style="border-right: blue 1px solid; width: 20px">
                                        <img id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev2.gif"
                                            runat="server">
                                    </td>
                                    <td style="border-right: blue 1px solid; width: 150px" valign="middle" align="center">
                                        <asp:Label ID="lblpg" runat="server" CssClass="labelbluetmp">Image 0 of 0</asp:Label>
                                    </td>
                                    <td style="border-right: blue 1px solid; width: 20px">
                                        <img id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext2.gif"
                                            runat="server">
                                    </td>
                                    <td style="width: 20px">
                                        <img id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast2.gif"
                                            runat="server">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="label11ptmp">
                <asp:Label ID="lang2145" runat="server">Description</asp:Label>
            </td>
            <td colspan="7">
                <asp:TextBox ID="txteqdesc" runat="server" Width="425px" MaxLength="100" CssClass="plainlabel11ptmp"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label11ptmp" style="height: 23px">
                <asp:Label ID="lang2146" runat="server">Spl Identifier</asp:Label>
            </td>
            <td colspan="7">
                <asp:TextBox ID="txtspl" runat="server" Width="425px" MaxLength="250" CssClass="plainlabel11ptmp"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label11ptmp">
                <asp:Label ID="lang2147" runat="server">Location</asp:Label>
            </td>
            <td colspan="7">
                <asp:TextBox ID="txtloc" runat="server" Width="425px" MaxLength="50" Font-Size="9pt"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label11ptmp">
                OEM
            </td>
            <td colspan="2">
                <asp:DropDownList ID="ddoem" runat="server" style="width: 150px" CssClass="plainlabel11ptmp">
                </asp:DropDownList>
                <asp:TextBox ID="txtoem" runat="server" style="width: 150px" MaxLength="50" CssClass="plainlabel11ptmp"></asp:TextBox>
            </td>
            <td class="label11ptmp" colspan="4">
                <asp:Label ID="lang2148" runat="server">Model#/Type</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtmod" runat="server" Width="170px" MaxLength="50" CssClass="plainlabel11ptmp"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label11ptmp">
                <asp:Label ID="lang2149" runat="server">Serial#</asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtser" runat="server" style="width: 150px" MaxLength="50" CssClass="plainlabel11ptmp"></asp:TextBox>
            </td>
            <td class="label11ptmp" colspan="4">
                <asp:Label ID="lang2150" runat="server">Status</asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlstat" runat="server" CssClass="plainlabel11ptmp" Width="170px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="label11ptmp">
                <asp:Label ID="lang2151" runat="server">Asset Class</asp:Label>
            </td>
            <td align="left" colspan="1">
                <asp:DropDownList ID="ddac" runat="server" CssClass="plainlabel11ptmp" style="width: 150px">
                    <asp:ListItem Value="Select Asset Class">Select Asset Class</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <img class="details" id="btnaddasset" onmouseover="return overlib('Add/Edit Asset Class', LEFT)"
                    onclick="getACDiv();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
                    style="width: 20px">
            </td>
            <td class="label11ptmp" colspan="4">
                Column
            </td>
            <td class="plainlabel11ptmp" id="tdcol">
                <asp:TextBox ID="txtfpc" runat="server" Width="170px" MaxLength="50" CssClass="plainlabel11ptmp"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label11pbtmp">
                <asp:Label ID="lang2152" runat="server">Charge#</asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtcharge" runat="server" Width="165px" ReadOnly="True" CssClass="plainlabel"></asp:TextBox>
            </td>
            <td colspan="3">
                <img onclick="getcharge('wo');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0">
                <img src="../images/appbuttons/minibuttons/refreshit.gif" onclick="refit();" alt="" />
            </td>
            <td>
            </td>
            <td class="label11ptmp">
                Critical Equipment?&nbsp;<input type="checkbox" id="cbcrit" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="8" style="border-top: #7ba4e0 thin solid">
                <table cellspacing="1" cellpadding="1">
                    <tr>
                        <td width="92">
                        </td>
                        <td width="160">
                        </td>
                        <td width="92">
                        </td>
                        <td style="width: 140px">
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltmp" id="Td8" runat="server">
                            <asp:Label ID="lang2153" runat="server">Created By</asp:Label>
                        </td>
                        <td class="plainlabeltmp" id="tdcby" runat="server">
                        </td>
                        <td class="labeltmp">
                            <asp:Label ID="lang2154" runat="server">Create Date</asp:Label>
                        </td>
                        <td class="plainlabeltmp" id="tdcdate" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltmp" id="Td2" runat="server">
                            <asp:Label ID="lang2155" runat="server">Phone</asp:Label>
                        </td>
                        <td class="plainlabeltmp" id="Td3" runat="server">
                        </td>
                        <td class="labeltmp">
                            <asp:Label ID="lang2156" runat="server">Email Address</asp:Label>
                        </td>
                        <td class="plainlabeltmp" id="Td4" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltmp" id="Td1" runat="server">
                            <asp:Label ID="lang2157" runat="server">Modified By</asp:Label>
                        </td>
                        <td class="plainlabeltmp" id="tdmby" runat="server">
                        </td>
                        <td class="labeltmp">
                            <asp:Label ID="lang2158" runat="server">Modified Date</asp:Label>
                        </td>
                        <td class="plainlabeltmp" id="tdmdate" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltmp" id="Td5" runat="server">
                            <asp:Label ID="lang2160" runat="server">Phone</asp:Label>
                        </td>
                        <td class="plainlabel11ptmp" runat="server" id="Td6" runat="server">
                            &nbsp;
                        </td>
                        <td class="labeltmp">
                            <asp:Label ID="lang2161" runat="server">Email Address</asp:Label>
                        </td>
                        <td class="plainlabel11ptmp" id="Td7" runat="server">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border-top: #7ba4e0 thin solid">
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltmp" id="lblpmostat" runat="server">
                            PMO Status
                        </td>
                        <td>
                            <asp:DropDownList ID="ddpmostat" runat="server" CssClass="plainlabeltmp" Width="140px">
                                <asp:ListItem Value="0">Select PMO Status</asp:ListItem>
                                <asp:ListItem Value="1">Not Started</asp:ListItem>
                                <asp:ListItem Value="2">In Progress</asp:ListItem>
                                <asp:ListItem Value="3">Optimized</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="labeltmp" id="lblmc" runat="server">
                            Designation
                        </td>
                        <td>
                            <asp:DropDownList ID="ddmc" runat="server" CssClass="plainlabeltmp" 
                                Width="140px" AutoPostBack="True">
                                <asp:ListItem Value="0">Select Designation</asp:ListItem>
                                <asp:ListItem Value="1">Master Record</asp:ListItem>
                                <asp:ListItem Value="2">Child Record</asp:ListItem>
                                <asp:ListItem Value="3">Open</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                    <td class="labeltmp">Master Record</td>
                    <td class="plainlabeltmp" id="tdmast" runat="server"></td>
                    <td class="labeltmp" id="tcttimelbl" runat="server">Child Total Time</td>
                    <td id= "tcttime" runat="server"><asp:TextBox ID="txtcttime" runat="server" Width="35px" CssClass="plainlabel"></asp:TextBox>
                        <asp:ImageButton ID="btnsavechildtot" runat="server" 
                            ImageUrl="~/images/appbuttons/minibuttons/savedisk1.gif" />
                    </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: #7ba4e0 thin solid" colspan="8">
                <img src="../images/appbuttons/minibuttons/2PX.gif">
            </td>
            <td style="border-bottom: #7ba4e0 thin solid" colspan="2">
                <img src="../images/appbuttons/minibuttons/2PX.gif">
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <table cellpadding="0" cellspacing="0" width="744" border="0">
                    <tr>
                        <td style="width: 140px" id="tdgtt" runat="server">
                            <a onclick="GoToTasks(4);" href="#">
                                <img id="btngtt" height="19" alt="" src="../images/appbuttons/bgbuttons/jtotaskssm.gif"
                                    width="122" border="0" runat="server"></a>
                        </td>
                        <td class="label" width="250" id="tdgttrb" runat="server">
                            <input id="rbdev" disabled onclick="GetType(this.value);" type="radio" value="pmdev"
                                name="rtyp" runat="server"><asp:Label ID="lang2162" runat="server">PM Developer</asp:Label><input
                                    id="rbopt" disabled onclick="GetType(this.value);" type="radio" value="pmopt"
                                    name="rtyp" runat="server" checked><asp:Label ID="lang2163" runat="server">PM Optimizer</asp:Label><br>
                            <input id="rbtdev" disabled onclick="GetType(this.value);" type="radio" value="rbdev"
                                name="rtyp" runat="server"><asp:Label ID="lang2164" runat="server">TPM Developer</asp:Label><input
                                    id="rbtopt" disabled onclick="GetType(this.value);" type="radio" value="rbopt"
                                    name="rtyp" runat="server"><asp:Label ID="lang2165" runat="server">TPM Optimizer</asp:Label>
                        </td>
                        <td width="230">
                        </td>
                        <td align="right" style="width: 120px" id="tdopts" runat="server">
                            <img id="imgtpmrev" onmouseover="return overlib('Record TPM Revision')" onmouseout="return nd()"
                                onclick="archit();" alt="" src="../images/appbuttons/minibuttons/compresstpm.gif"
                                runat="server" />
                            <img id="btnunlock" onclick="lock();" height="20" alt="" 
                                src="../images/appbuttons/minibuttons/unlock.gif" runat="server"><img id="btnlock" onclick="unlock();" height="20" alt=""
                                    src="../images/appbuttons/minibuttons/lock.gif" style="width: 20px" runat="server"><img onclick="gridret();"
                                        height="20" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif" style="width: 20px"><asp:ImageButton
                                            ID="btndel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif">
                            </asp:ImageButton><img id="btnsave" height="20" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                style="width: 20px" runat="server" onclick="return btnsave_onclick()">&nbsp;<img onmouseover="return overlib('Copy an Equipment Record')"
                                    onclick="GetEqCopy();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                    style="width: 20px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="10" align="right">
            </td>
        </tr>
    </table>
    <div class="details" id="gldiv" style="z-index: 999; border-bottom: black 1px solid;
        border-left: black 1px solid; width: 400px; height: 350px; border-top: black 1px solid;
        top: 1500px; border-right: black 1px solid">
        <table cellspacing="0" cellpadding="0" width="400" bgcolor="white">
            <tr bgcolor="#0000cc" height="20">
                <td class="labeltblhdr">
                    <asp:Label ID="lang2166" runat="server">Translation Options</asp:Label>
                </td>
                <td align="right">
                    <img onclick="closegl();" height="18" alt="" src="../images/close.gif" width="18"><br>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="ifgl" style="width: 400px; height: 350px" src="" frameborder="no" runat="server">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    <input id="lblclid" type="hidden" name="lblclid" runat="server"><input id="lbldept"
        type="hidden" name="lbldept" runat="server">
    <input id="lblchk" type="hidden" name="lblchk" runat="server"><input id="lblsid"
        type="hidden" name="lblsid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lbleqid"
        type="hidden" name="lbleqid" runat="server">
    <input id="lblpar" type="hidden" name="lblpar" runat="server"><input id="lbldchk"
        type="hidden" name="lbldchk" runat="server">
    <input id="lblecrflg" type="hidden" name="lblecrflg" runat="server"><input id="lblpic"
        type="hidden" name="lblpic" runat="server">
    <input id="lblpchk" type="hidden" runat="server"><input id="lbluser" type="hidden"
        runat="server">
    <input id="appchk" type="hidden" name="appchk" runat="server"><input id="lblpareqid"
        type="hidden" runat="server">
    <input id="lbladdchk" type="hidden" runat="server"><input id="lbllog" type="hidden"
        runat="server">
    <input id="lbllock" type="hidden" runat="server"><input id="lbllockedby" type="hidden"
        runat="server">
    <input id="lbllockchk" type="hidden" runat="server"><input id="lbldb" type="hidden"
        runat="server">
    <input id="lbllid" type="hidden" runat="server"><input id="lbltyp" type="hidden"
        runat="server">
    <input id="lblro" type="hidden" runat="server"><input id="lblappstr" type="hidden"
        runat="server">
    <input id="lblapp" type="hidden" runat="server"><input id="lbldel" type="hidden"
        runat="server">
    <input id="lblpcnt" type="hidden" runat="server">
    <input id="lblcurrp" type="hidden" runat="server">
    <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
    <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
    <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
    <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
    <input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server">
    <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
    <input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders"
        type="hidden" name="lbliorders" runat="server">
    <input id="lbloldorder" type="hidden" runat="server">
    <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
    <input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server">
    <input type="hidden" id="lbluid" runat="server">
    <input type="hidden" id="lblfslang" runat="server" /><input type="hidden" id="lblnodel"
        runat="server">
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lbleqlid" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblghostoff" runat="server" />
    <input type="hidden" id="lblxstatus" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblcharge" runat="server" />
    <input type="hidden" id="lblisdown" runat="server" />
    <input type="hidden" id="lblischild" runat="server" />
    <input type="hidden" id="lblcadm" runat="server" />
    <input type="hidden" id="lblchld" runat="server" />
    <input type="hidden" id="lbldesig" runat="server" />
    <input type="hidden" id="lblpmostat1" runat="server" />
    </form>
</body>
</html>
