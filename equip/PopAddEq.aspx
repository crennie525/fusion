<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PopAddEq.aspx.vb" Inherits="lucy_r12.PopAddEq" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>PopAddEq</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/equipment_1.js"></script>
		<script  src="../scripts1/PopAddEqaspx_1.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg"  onload="checkeq();">
		<form id="form1" method="post" runat="server">
			<table width="440">
				<tr>
					<td class="thdrsingrt label"><asp:Label id="lang2537" runat="server">Add/Edit Equipment Mini Dialog</asp:Label></td>
				</tr>
			</table>
			<table width="440">
				<tr>
					<td style="width: 140px" class="bluelabel"><asp:Label id="lang2538" runat="server">Add New Equipment#</asp:Label></td>
					<td style="width: 120px"><asp:textbox id="txtneweq" runat="server" MaxLength="50"></asp:textbox></td>
					<td style="width: 20px"><IMG alt="" onclick="checkvals('add');" src="../images/appbuttons/minibuttons/addnewbg1.gif"
							class="imgbutton" style="width: 20px" height="20" id="imgadd" runat="server"></td>
					<td width="160" align="right"></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" width="440">
				<TBODY>
					<tr>
						<td width="85"></td>
						<td style="WIDTH: 170px" width="170"></td>
						<td style="width: 140px"></td>
						<td style="width: 60px"></td>
					</tr>
					<tr>
						<td colspan="4" class="thdrsingrt label" id="tdedit" runat="server"><asp:Label id="lang2539" runat="server">Edit Selected Equipment Record</asp:Label></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2540" runat="server">Equipment#</asp:Label></td>
						<td colSpan="3"><asp:textbox id="txteqname" runat="server" style="width: 150px" ReadOnly="True" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2541" runat="server">Description</asp:Label></td>
						<td style="WIDTH: 363px" colSpan="3"><asp:textbox id="txteqdesc" runat="server" Width="300px" MaxLength="100"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label" style="HEIGHT: 23px"><asp:Label id="lang2542" runat="server">Spl Identifier</asp:Label></td>
						<td style="WIDTH: 363px; HEIGHT: 23px" colSpan="3"><asp:textbox id="txtspl" runat="server" Width="300px" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2543" runat="server">Location</asp:Label></td>
						<td style="WIDTH: 363px" colSpan="3"><asp:textbox id="txtloc" runat="server" Width="300px" Font-Size="9pt" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label">OEM</td>
						<td style="WIDTH: 363px" colSpan="3"><asp:textbox id="txtoem" runat="server" style="width: 150px" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2544" runat="server">Model#/Type</asp:Label></td>
						<td style="WIDTH: 363px" colSpan="3"><asp:textbox id="txtmod" runat="server" style="width: 150px" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2545" runat="server">Serial#</asp:Label></td>
						<td style="WIDTH: 363px" colSpan="3"><asp:textbox id="txtser" runat="server" style="width: 150px" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2546" runat="server">Status</asp:Label></td>
						<td style="WIDTH: 363px" colSpan="3"><asp:dropdownlist id="ddlstat" runat="server"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="label" style="BORDER-BOTTOM: #7ba4e0 thin solid"><asp:Label id="lang2547" runat="server">Asset Class</asp:Label></td>
						<td style="BORDER-BOTTOM: #7ba4e0 thin solid; WIDTH: 170px" align="left">
							<asp:DropDownList id="ddac" runat="server">
								<asp:ListItem Value="Select  Class">Select  Class</asp:ListItem>
							</asp:DropDownList></td>
						<td class="redlabel" style="BORDER-BOTTOM: #7ba4e0 thin solid"><asp:Label id="lang2548" runat="server">Criticality Rating:</asp:Label></td>
						<td style="BORDER-BOTTOM: #7ba4e0 thin solid; WIDTH: 119px"><asp:textbox id="txtecr" runat="server" style="width: 48px" Font-Size="9pt"></asp:textbox>&nbsp;<IMG id="btnecr" title="Calculate ECR" onclick="getcalc();" alt="" src="../images/appbuttons/minibuttons/ecrbutton.gif"
								align="absMiddle" runat="server" class="imgbutton" style="width: 26px" height="20"></td>
					</tr>
					<tr>
						<td vAlign="bottom" align="right" colSpan="4" height="30">
							<asp:ImageButton id="ibtnsave" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:ImageButton>&nbsp;<IMG alt="" onclick="handleexit();" src="../images/appbuttons/bgbuttons/return.gif" class="imgbutton"
								id="irtn" runat="server" width="69" height="19"></td>
					</tr>
					<tr>
						<td colspan="4" class="thdrsingrt label"><asp:Label id="lang2549" runat="server">Equipment Record Reference</asp:Label></td>
					</tr>
					<tr>
						<td class="bluelabel"><asp:Label id="lang2550" runat="server">Search</asp:Label></td>
						<td colspan="3"><asp:textbox id="txtsrch" runat="server" Width="250px"></asp:textbox><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					</tr>
					<tr>
						<td colspan="4">
							<asp:repeater id="rptreq" runat="server">
								<HeaderTemplate>
									<table>
										<tr>
											<td class="btmmenu plainlabel" width="170px"><asp:Label id="lang2551" runat="server">Equipment #</asp:Label></td>
											<td class="btmmenu plainlabel" width="250px"><asp:Label id="lang2552" runat="server">Description</asp:Label></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr class="tbg">
										<td class="plainlabel">
											<asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel">
											<asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></TD></TR>
			<tr>
				<td class="bluelabel" id="tdpg" colSpan="2" runat="server"></td>
				<td colspan="2" align="right"><asp:imagebutton id="btnprev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnnext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton></td>
			</tr>
			</TBODY></TABLE> <input id="lbldept" type="hidden" runat="server"><input id="lblcell" type="hidden" runat="server">
			<input id="lblchk" type="hidden" runat="server"><input id="lblsid" type="hidden" runat="server">
			<input type="hidden" id="lbleqid" runat="server"><input type="hidden" id="lblecrflg" runat="server">
			<input type="hidden" id="lblcid" runat="server"><input type="hidden" id="lbluser" runat="server">
			<input type="hidden" id="txtpg" runat="server" NAME="txtpg"><input type="hidden" id="lbluid" runat="server">
			<input type="hidden" id="lbllog" runat="server"> <input type="hidden" id="lbllid" runat="server">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
