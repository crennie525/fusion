<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NCBot.aspx.vb" Inherits="lucy_r12.NCBot" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>NCBot</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		
        <script  type="text/javascript">
            function checkit() {
                var chk = document.getElementById("lbllog").value;
                if (chk == "no") {
                    window.parent.doref();
                }
                else {
                    window.parent.setref();
                }
            }
        function FreezeScreen(msg) {
			scroll(0,0);
			var outerPane = document.getElementById('FreezePane');
			var innerPane = document.getElementById('InnerFreezePane');
			if (outerPane) outerPane.className = 'FreezePaneOn';
			if (innerPane) innerPane.innerHTML = msg;
			document.getElementById("lblsubmit").value = "save";
			document.getElementById("form1").submit();
		}
		function gridret() {
				var chk = document.getElementById("lblchk").value;
		var dchk = document.getElementById("lbldchk").value;
        var cid = document.getElementById("lblcid").value;
		var sid = document.getElementById("lblsid").value;
		var did = document.getElementById("lbldid").value;
		var clid = document.getElementById("lblclid").value;
		var lid = document.getElementById("lbllid").value;
        window.location = "NCGrid.aspx?&dchk=" + dchk + "&chk=" + chk + "&cid=" + cid + "&sid=" + sid + "&did=" + did + "&clid=" + clid + "&lid=" + lid;

		}
		function btnupload_onclick() {

		}

        </script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<div class="FreezePaneOff" id="FreezePane" style="WIDTH: 720px" align="center">
				<div class="InnerFreezePane" id="InnerFreezePane"></div>
			</div>
			<div id="overDiv" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute"></div>
			<table id="eqdetdiv" style="LEFT: 4px; POSITION: absolute; TOP: 0px" cellSpacing="0" cellPadding="2"
				width="704">
				<TR>
					<td width="94"></td>
					<td width="230"></td>
					<td style="width: 150px"></td>
					<td style="width: 150px"></td>
					<td style="width: 80px"></td>
				</TR>
				<tr>
					<td class="bluelabel" style="BORDER-BOTTOM: #7ba4e0 thin solid"><asp:Label id="lang2426" runat="server">Add New Asset</asp:Label></td>
					<td style="BORDER-BOTTOM: #7ba4e0 thin solid"><asp:textbox id="txtneweq" runat="server" MaxLength="50" Width="220px" CssClass="plainlabel"></asp:textbox></td>
					<td style="BORDER-BOTTOM: #7ba4e0 thin solid"><asp:imagebutton id="btnAddEq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton></td>
					<td style="BORDER-BOTTOM: #7ba4e0 thin solid" colSpan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="label"></td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2427" runat="server">Asset#</asp:Label></td>
					<td colSpan="2">
						<asp:TextBox id="txtasset" runat="server" Width="220px" CssClass="plainlabel"></asp:TextBox></td>
					<td align="center" colSpan="2" rowSpan="10"><A onclick="getbig();" href="#"><IMG id="imgeq" height="216" src="../images/appimages/eqimg1.gif" style="width: 216px" border="0"
								runat="server"></A></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2428" runat="server">Description</asp:Label></td>
					<td colSpan="2">
						<asp:TextBox id="txtadesc" runat="server" Width="279px" CssClass="plainlabel"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2429" runat="server">Location</asp:Label></td>
					<td colspan="2">
						<asp:TextBox id="txtaloc" runat="server" Width="280px" CssClass="plainlabel"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2430" runat="server">Model#/Type</asp:Label></td>
					<td colspan="2">
						<asp:TextBox id="txtamod" runat="server" Width="152px" CssClass="plainlabel"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2431" runat="server">Serial#</asp:Label></td>
					<td colspan="2">
						<asp:TextBox id="txtaser" runat="server" Width="152px" CssClass="plainlabel"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="label"></td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="label"></td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="label"></td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="label"></td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="label"></td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr height="20">
					<td class="label"></td>
					<td colspan="2"></td>
					<td align="center" colSpan="2"><INPUT class="3DButton" id="MyFile" type="file" size="12" name="MyFile" RunAt="Server"><input class="3DButton" id="btnupload" type="button" value="Upload" name="btnupload" runat="server"></td>
				</tr>
				<tr height="20">
					<td style="BORDER-BOTTOM: #7ba4e0 thin solid" colSpan="5">&nbsp;</td>
				</tr>
				<tr>
					<td align="right" colSpan="5"><IMG onclick="gridret();" height="20" alt="" src="../images/appbuttons/bgbuttons/navgrid.gif"
							style="width: 20px"><IMG id="btnsave" height="20" src="../images/appbuttons/minibuttons/savedisk1.gif" style="width: 20px"
							runat="server">&nbsp;<IMG class="details" onmouseover="return overlib('Copy this Asset Record')" onclick="GetEqCopy();"
							onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif" style="width: 20px">
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbllog" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server"><input id="lbldept" type="hidden" name="lbldept" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"><input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input type="hidden" id="lbldchk" runat="server">
			<input type="hidden" id="lbllid" runat="server"> <input type="hidden" id="lbltyp" runat="server">
			<input type="hidden" id="lbleqid" runat="server"><input type="hidden" id="lblpic" runat="server">
			<input type="hidden" id="lblsubmit" runat="server"><input type="hidden" id="lblro" runat="server" NAME="lblro">
		<input type="hidden" id="lbleqnum" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
