

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class FuncCopyMini
    Inherits System.Web.UI.Page
	Protected WithEvents lang2335 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2334 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2333 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2332 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2331 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2330 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2329 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2328 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2327 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2326 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2325 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2324 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2323 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2322 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2321 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2320 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2319 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2318 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2317 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2316 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2315 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2314 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2313 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2312 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2311 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2310 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2309 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2308 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2307 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2306 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2305 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2304 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim func As New Utilities
    Dim intPgCnt, intPgNav As Integer
    Dim eqid, cid, sid, login, did, clid, ro, db, oloc As String
    Protected WithEvents lblnewfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfromeq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnsaveneweq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tblneweq As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdfuncnew As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcrby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcrdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtfuncspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdcrphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtnewfunc As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbloptions As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents ibtncopy As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcremail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td8 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td7 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td9 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdcompcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtaskcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbtpm As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbrat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents rbro As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbai As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rptrfuncrev As System.Web.UI.WebControls.Repeater
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdcurrpage As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tblfuncrev As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblrevdetails As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcmail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqspl As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not Me.IsPostBack Then
            If lbllog.Value <> "no" Then
                'Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                Try
                    db = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString 'Request.QueryString("db").ToString
                    lbldb.Value = db
                    oloc = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString 'Request.QueryString("oloc").ToString
                    lbloloc.Value = oloc
                    cbtpm.Visible = True
                    cbtpm.Checked = True
                Catch ex As Exception
                    lbldb.Value = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString  '"0"
                    oloc = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString 'Request.QueryString("oloc").ToString
                    lbloloc.Value = oloc
                    cbtpm.Visible = False
                    cbtpm.Checked = False
                End Try
                If ro = "1" Then
                    ibtncopy.Enabled = False
                    ibtncopy.ImageUrl = "../images/appbuttons/bgbuttons/copydis.gif"
                    'imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                Else
                    'ibtncopy.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/copyhov.gif'")
                    'ibtncopy.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/copy.gif'")
                End If
                eqid = Request.QueryString("eqid").ToString
                If Len(eqid) <> 0 AndAlso eqid <> "" AndAlso eqid <> "0" Then
                    cid = Request.QueryString("cid").ToString
                    sid = Request.QueryString("sid").ToString
                    did = Request.QueryString("did").ToString
                    clid = Request.QueryString("clid").ToString
                    lblsid.Value = sid
                    lbldid.Value = did
                    lblclid.Value = clid
                    lbleqid.Value = eqid
                    lblcid.Value = cid
                    func.Open()
                    PopFunc(PageNumber)
                    func.Dispose()
                    cbloptions.Attributes.Add("onclick", "checkbox();")
                    txtpg.Value = "1"
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr928" , "FuncCopyMini.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If
                'Catch ex As Exception
                'Dim strMessage As String =  tmod.getmsg("cdstr929" , "FuncCopyMini.aspx.vb")
 
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'lbllog.Value = "noeqid"
                'End Try

            End If
        Else
            If Request.Form("lblret") = "next" Then
                func.Open()
                GetNext()
                func.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                func.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopFunc(PageNumber)
                func.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                func.Open()
                GetPrev()
                func.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                func.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopFunc(PageNumber)
                func.Dispose()
                lblret.Value = ""
            End If
        End If
        'btnprev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'btnprev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'btnnext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'btnnext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")

        'ibtnsaveneweq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ibtnsaveneweq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")

        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopFunc(PageNumber)
        Catch ex As Exception
            func.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr930" , "FuncCopyMini.aspx.vb")
 
            func.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopFunc(PageNumber)
        Catch ex As Exception
            func.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr931" , "FuncCopyMini.aspx.vb")
 
            func.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopFunc(ByVal PageNumber As Integer)
        txtpg.Value = PageNumber
        eqid = lbleqid.Value
        cid = lblcid.Value
        sid = lblsid.Value
        'sid = "17"
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim intPgCnt As Integer
        If Len(srch) = 0 Then
            srch = ""
            sql = "select count(*) from functions f right join equipment e on e.eqid = f.eqid  where e.siteid = '" & sid & "'"
            'intPgCnt = func.Scalar(sql)
        Else
            Dim csrch As String

            csrch = "%" & srch & "%"
            sql = "select count(*) from functions f  right join equipment e on e.eqid = f.eqid   where (f.func like '" & csrch & "' or f.func_desc like '" & csrch & "' or f.spl like '" & csrch & "') and e.siteid = '" & sid & "'"
            'intPgCnt = func.Scalar(sql)
        End If
        'intPgNav = func.PageCount(intPgCnt, PageSize)
        'txtpg.Value = PageNumber

        PageNumber = txtpg.Value
        'intPgCnt = func.Scalar(sql)
        'intPgNav = func.PageCount(intPgCnt, PageSize)
        intPgNav = func.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        'Get Page
        sql = "usp_getAllFuncPg '" & cid & "', '" & eqid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '" & sid & "'"
        dr = func.GetRdrData(sql)
        rptrfuncrev.DataSource = dr
        rptrfuncrev.DataBind()
        dr.Close()
        updatepos(intPgCnt)
    End Sub
    Private Sub updatepos(ByVal intPgCnt)
        PageNumber = txtpg.Value
        If intPgNav = 0 Then
            tdcurrpage.InnerHtml = "Page 0 of 0"
        Else
            tdcurrpage.InnerHtml = "Page " & PageNumber & " of " & intPgNav
        End If

        If PageNumber = 1 And intPgCnt = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = False

        ElseIf PageNumber = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = True

        ElseIf PageNumber = intPgNav Then
            btnnext.Enabled = False
            btnprev.Enabled = True

        Else
            btnnext.Enabled = True
            btnprev.Enabled = True
        End If
    End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        func.Open()
        PopFunc(PageNumber)
        func.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprev.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        func.Open()
        PopFunc(PageNumber)
        func.Dispose()
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        tbltop.Attributes.Add("class", "view")
        tblfuncrev.Attributes.Add("class", "details")
        tblrevdetails.Attributes.Add("class", "details")
        'tblneweqhdr.Attributes.Add("class", "detaisl")
        tblneweq.Attributes.Add("class", "details")

        txtpg.Value = "1"
        PageNumber = txtpg.Value
        func.Open()
        PopFunc(PageNumber)
        func.Dispose()
    End Sub
    Public Sub GetFu(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rb As RadioButton = New RadioButton
        Dim eqid, eqnum, eqdesc, eqspl, loc, model, serial, oem As String
        Dim ecr, cby, mby, cdat, mdat, cph, cm, eph, em, cc, tc As String
        Dim funcid, funcdesc, funcspl
        rb = sender
        tdfunc.InnerHtml = rb.ClientID
        For Each i As RepeaterItem In rptrfuncrev.Items
            rb = CType(i.FindControl("rbfu"), RadioButton)
            eqid = CType(i.FindControl("lblfueqid"), Label).Text
            funcdesc = CType(i.FindControl("lblfuncdesc"), Label).Text
            funcspl = CType(i.FindControl("lblfuncspl"), Label).Text
            cby = CType(i.FindControl("lblcby"), Label).Text
            mby = CType(i.FindControl("lblmby"), Label).Text
            cdat = CType(i.FindControl("lblcdate"), Label).Text
            mdat = CType(i.FindControl("lblmdate"), Label).Text
            cph = CType(i.FindControl("lblcph"), Label).Text
            cm = CType(i.FindControl("lblcm"), Label).Text
            eph = CType(i.FindControl("lbleph"), Label).Text
            em = CType(i.FindControl("lblem"), Label).Text
            funcid = CType(i.FindControl("lblfuncrevid"), Label).Text
            'eqnum eqdesc eqspl
            eqnum = CType(i.FindControl("lbleqnum"), Label).Text
            eqdesc = CType(i.FindControl("lbleqdesc"), Label).Text
            eqspl = CType(i.FindControl("lbleqspl"), Label).Text

            cc = CType(i.FindControl("lblcompcnt"), Label).Text
            tc = CType(i.FindControl("lbltaskcnt"), Label).Text
            rb.Checked = False
            If tdfunc.InnerHtml = rb.ClientID Then
                lbloldfuncid.Value = funcid
                tblfuncrev.Attributes.Add("class", "view")
                tblrevdetails.Attributes.Add("class", "view")
                tblneweq.Attributes.Add("class", "details")
                rb.Checked = True
                lblfromeq.Value = eqid
                tdfunc.InnerHtml = rb.Text.Trim
                'lbldesc.Text = funcdesc
                tdspl.InnerHtml = funcspl
                tdcby.InnerHtml = cby
                tdcdate.InnerHtml = cdat
                tdcphone.InnerHtml = cph
                tdcmail.InnerHtml = cm
                tdeqnum.InnerHtml = eqnum
                tdeqdesc.InnerHtml = eqdesc
                tdeqspl.InnerHtml = eqspl

                tdcompcnt.InnerHtml = cc
                tdtaskcnt.InnerHtml = tc
            End If
        Next

    End Sub

    Private Sub ibtncopy_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtncopy.Click
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr932" , "FuncCopyMini.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            lbllog.Value = "no"
            Exit Sub
        End Try
        DoCopy()

    End Sub
    Private Sub DoCopy()
        Dim li As ListItem
        Dim listr As String = ""
        For Each li In cbloptions.Items
            If li.Selected = True Then
                listr += li.Value.ToString
            End If
        Next
        Dim tpm As String
        If cbtpm.Checked = True Then
            tpm = "Y"
        Else
            tpm = "N"
        End If
        Dim eqid As String = lbleqid.Value
        If Len(eqid) <> 0 AndAlso eqid <> "" AndAlso eqid <> "0" Then
            Dim oldfu As String = lbloldfuncid.Value
            Dim nufu As String = txtnewfunc.Text 'txt1.Text
            If Len(nufu) > 0 Then
                nufu = Replace(nufu, "'", Chr(180), , , vbTextCompare)
                nufu = Replace(nufu, "--", "-", , , vbTextCompare)
                nufu = Replace(nufu, ";", ":", , , vbTextCompare)
                cid = lblcid.Value
                sid = lblsid.Value
                did = lbldid.Value
                clid = lblclid.Value
                Dim mdb As String = lbldb.Value
                db = lbldb.Value
                Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
                Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString
                Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                Dim oloc As String = lbloloc.Value
                Dim cloc As String = ap
                Dim eqcnt, newid As Integer
                db = lbldb.Value
                If db = "0" Or db = "" Then
                    sql = "select count(*) from functions where eqid = '" & eqid & "' and func = '" & nufu & "'"
                Else
                    sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[functions] where eqid = '" & eqid & "' and func = '" & nufu & "'"
                End If

                func.Open()
                Dim fucnt As Integer = func.Scalar(sql)
                Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
                user = Replace(user, "'", Chr(180), , , vbTextCompare)
                Dim uid As String = HttpContext.Current.Session("uid").ToString '"PM Administrator"
                Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String

                If fucnt < 1 Then
                    If db = "0" Or db = "" Then
                        sql = "usp_copyFunc '" & cid & "', '" & eqid & "', '" & oldfu & "', '" & user & "', '" & nufu & "'"
                    Else
                        sql = "usp_copyFuncMDB '" & cid & "', '" & eqid & "', '" & oldfu & "', '" & user & "', '" & nufu & "', '" & mdb & "', '" & orig & "','" & srvr & "'"
                    End If
                    newid = func.Scalar(sql)
                    lblnewfuncid.Value = newid
                    Try
                        sql = "select * from pmpictures where funcid = '" & newid & "' and (comid is null or len(comid) = 0)"
                        Dim ds As New DataSet
                        ds = func.GetDSData(sql)
                        Dim fid, cmid, ofid, ocmid As String
                        Dim i As Integer
                        Dim f As Integer = 0
                        Dim c As Integer = 0
                        Dim x As Integer = ds.Tables(0).Rows.Count
                        For i = 0 To (x - 1)
                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                            ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                            cmid = ds.Tables(0).Rows(i)("comid").ToString
                            ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                            t = ds.Tables(0).Rows(i)("picurl").ToString
                            tn = ds.Tables(0).Rows(i)("picurltn").ToString
                            tm = ds.Tables(0).Rows(i)("picurltm").ToString
                            t = Replace(t, "..", "")
                            tn = Replace(tn, "..", "")
                            tm = Replace(tm, "..", "")
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"

                            ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                            ts = Replace(ts, "\", "/")
                            td = Server.MapPath("\") & cloc & t
                            td = Replace(td, "\", "/")


                            tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                            tn = Replace(tn, "\", "/")
                            tnd = Server.MapPath("\") & cloc & tn
                            tnd = Replace(tnd, "\", "/")


                            tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                            tms = Replace(tms, "\", "/")
                            tmd = Server.MapPath("\") & cloc & tm
                            tmd = Replace(tmd, "\", "/")

                            System.IO.File.Copy(ts, td, True)
                            System.IO.File.Copy(tns, tnd, True)
                            System.IO.File.Copy(tms, tmd, True)
                        Next
                    Catch ex As Exception

                    End Try
                    If listr = "ct" Then
                        If db = "0" Or db = "" Then
                            sql = "usp_copyComps '" & cid & "', '" & oldfu & "', '" & newid & "', '" & eqid & "'"
                        Else
                            sql = "usp_copyCompsMDB '" & cid & "', '" & oldfu & "', '" & newid & "', '" & eqid & "', '" & mdb & "', '" & orig & "','" & srvr & "'"
                        End If
                        If db <> "0" Then
                            Try
                                Dim ds As New DataSet
                                ds = func.GetDSData(sql)
                                Dim fid, cmid, ofid, ocmid As String
                                Dim i As Integer
                                Dim f As Integer = 0
                                Dim c As Integer = 0
                                Dim x As Integer = ds.Tables(0).Rows.Count
                                For i = 0 To (x - 1)
                                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                    fid = ds.Tables(0).Rows(i)("funcid").ToString
                                    ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                    t = ds.Tables(0).Rows(i)("picurl").ToString
                                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                    t = Replace(t, "..", "")
                                    tn = Replace(tn, "..", "")
                                    tm = Replace(tm, "..", "")
                                    If cmid = "" Then
                                        f = f + 1
                                        nt = "b-eqImg" & fid & "i"
                                        ntn = "btn-eqImg" & fid & "i"
                                        ntm = "btm-eqImg" & fid & "i"

                                        ont = "b-eqImg" & ofid & "i"
                                        ontn = "btn-eqImg" & ofid & "i"
                                        ontm = "btm-eqImg" & ofid & "i"
                                    Else
                                        c = c + 1
                                        nt = "c-eqImg" & cmid & "i"
                                        ntn = "ctn-eqImg" & cmid & "i"
                                        ntm = "ctm-eqImg" & cmid & "i"

                                        ont = "c-eqImg" & ocmid & "i"
                                        ontn = "ctn-eqImg" & ocmid & "i"
                                        ontm = "ctm-eqImg" & ocmid & "i"
                                    End If

                                    ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                                    ts = Replace(ts, "\", "/")
                                    td = Server.MapPath("\") & cloc & t
                                    td = Replace(td, "\", "/")
                                    System.IO.File.Copy(ts, td, True)

                                    tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                                    tn = Replace(tn, "\", "/")
                                    tnd = Server.MapPath("\") & cloc & tn
                                    tnd = Replace(tnd, "\", "/")
                                    System.IO.File.Copy(tns, tnd, True)

                                    tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                                    tms = Replace(tms, "\", "/")
                                    tmd = Server.MapPath("\") & cloc & tm
                                    tmd = Replace(tmd, "\", "/")
                                    System.IO.File.Copy(tms, tmd, True)
                                Next
                            Catch ex As Exception

                            End Try
                            Try
                                sql = "select pic_id, parfuncid, funcid, oldfuncid, pm_image, pm_image_med, pm_image_thumb from pmimages where funcid = '" & newid & "'"
                                Dim dsp As New DataSet
                                dsp = func.GetDSData(sql)
                                Dim fid1, pfid1 As String
                                oloc = ap
                                Dim i1 As Integer
                                Dim f1 As Integer = 0
                                Dim c1 As Integer = 0
                                Dim ot, otn, otm As String
                                Dim pict, pictn, pictm As String
                                Dim opict, opictn, opictm As String
                                Dim x1 As Integer = dsp.Tables(0).Rows.Count
                                Dim ofid1 As String
                                Dim newt, newtn, newtm

                                For i1 = 0 To (x1 - 1)
                                    pid = dsp.Tables(0).Rows(i1)("pic_id").ToString
                                    fid1 = dsp.Tables(0).Rows(i1)("funcid").ToString
                                    ofid1 = dsp.Tables(0).Rows(i1)("oldfuncid").ToString
                                    pfid1 = dsp.Tables(0).Rows(i1)("parfuncid").ToString
                                    t = dsp.Tables(0).Rows(i1)("pm_image").ToString
                                    tn = dsp.Tables(0).Rows(i1)("pm_image_thumb").ToString
                                    tm = dsp.Tables(0).Rows(i1)("pm_image_med").ToString
                                    Dim intFileNameLength As Integer
                                    intFileNameLength = InStr(1, StrReverse(t), "/")
                                    t = Mid(t, (Len(t) - intFileNameLength) + 2)
                                    intFileNameLength = InStr(1, StrReverse(tn), "/")
                                    tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                                    intFileNameLength = InStr(1, StrReverse(tm), "/")
                                    tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                                    Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                                    t = appstr & "/pmimages/" & t
                                    tn = appstr & "/pmimages/" & tn
                                    tm = appstr & "/pmimages/" & tm

                                    t = Replace(t, "http://localhost/", "")
                                    tn = Replace(tn, "http://localhost/", "")
                                    tm = Replace(tm, "http://localhost/", "")

                                    t = Replace(t, "http://", "")
                                    tn = Replace(tn, "http://", "")
                                    tm = Replace(tm, "http://", "")

                                    ts = Replace(t, "g" & fid1 & "-", "g" & ofid1 & "-")
                                    tns = Replace(tn, "g" & fid1 & "-", "g" & ofid1 & "-")
                                    tms = Replace(tm, "g" & fid1 & "-", "g" & ofid1 & "-")

                                    td = Replace(t, "g" & fid1 & "-", "g" & fid1 & "-")
                                    tnd = Replace(tn, "g" & fid1 & "-", "g" & fid1 & "-")
                                    tmd = Replace(tm, "g" & fid1 & "-", "g" & fid1 & "-")


                                    If oloc <> "" Then
                                        ts = Replace(ts, ap, oloc)
                                        tns = Replace(tns, ap, oloc)
                                        tms = Replace(tms, ap, oloc)

                                        td = Replace(td, ap, oloc)
                                        tnd = Replace(tnd, ap, oloc)
                                        tmd = Replace(tmd, ap, oloc)
                                    End If


                                    ts = Server.MapPath("\") & ts
                                    ts = Replace(ts, "\", "/")
                                    td = Server.MapPath("\") & td
                                    td = Replace(td, "\", "/")


                                    tns = Server.MapPath("\") & tns
                                    tns = Replace(tns, "\", "/")
                                    tnd = Server.MapPath("\") & tnd
                                    tnd = Replace(tnd, "\", "/")


                                    tms = Server.MapPath("\") & tms
                                    tms = Replace(tms, "\", "/")
                                    tmd = Server.MapPath("\") & tmd
                                    tmd = Replace(tmd, "\", "/")

                                    'System.IO.File.Copy(source, dest)
                                    System.IO.File.Copy(ts, td, True)
                                    System.IO.File.Copy(tns, tnd, True)
                                    System.IO.File.Copy(tms, tmd, True)


                                Next

                            Catch ex As Exception

                            End Try
                        Else
                            Try
                                Dim ds As New DataSet
                                ds = func.GetDSData(sql)
                                Dim fid, cmid, ofid, ocmid, ofid1 As String
                                Dim i As Integer
                                Dim f As Integer = 0
                                Dim c As Integer = 0
                                Dim x As Integer = ds.Tables(0).Rows.Count
                                For i = 0 To (x - 1)
                                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                    fid = ds.Tables(0).Rows(i)("funcid").ToString
                                    ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                    ofid1 = ds.Tables(0).Rows(i)("oldfuncid").ToString
                                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                    t = ds.Tables(0).Rows(i)("picurl").ToString
                                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                    t = Replace(t, "..", "")
                                    tn = Replace(tn, "..", "")
                                    tm = Replace(tm, "..", "")
                                    If cmid = "" Then
                                        f = f + 1
                                        nt = "b-eqImg" & fid & "i"
                                        ntn = "btn-eqImg" & fid & "i"
                                        ntm = "btm-eqImg" & fid & "i"

                                        ont = "b-eqImg" & ofid & "i"
                                        ontn = "btn-eqImg" & ofid & "i"
                                        ontm = "btm-eqImg" & ofid & "i"
                                    Else
                                        c = c + 1
                                        nt = "c-eqImg" & cmid & "i"
                                        ntn = "ctn-eqImg" & cmid & "i"
                                        ntm = "ctm-eqImg" & cmid & "i"

                                        ont = "c-eqImg" & ocmid & "i"
                                        ontn = "ctn-eqImg" & ocmid & "i"
                                        ontm = "ctm-eqImg" & ocmid & "i"
                                    End If

                                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                                    ts = Replace(ts, "\", "/")
                                    td = Server.MapPath("\") & ap & t
                                    td = Replace(td, "\", "/")
                                    System.IO.File.Copy(ts, td, True)

                                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                                    tn = Replace(tn, "\", "/")
                                    tnd = Server.MapPath("\") & ap & tn
                                    tnd = Replace(tnd, "\", "/")
                                    System.IO.File.Copy(tns, tnd, True)

                                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                                    tms = Replace(tms, "\", "/")
                                    tmd = Server.MapPath("\") & ap & tm
                                    tmd = Replace(tmd, "\", "/")
                                    System.IO.File.Copy(tms, tmd, True)
                                Next
                            Catch ex As Exception

                            End Try
                            Try
                                sql = "select pic_id, parfuncid, funcid, oldfuncid, pm_image, pm_image_med, pm_image_thumb from pmimages where funcid = '" & newid & "'"
                                Dim dsp As New DataSet
                                dsp = func.GetDSData(sql)
                                Dim fid1, pfid1 As String
                                oloc = ap
                                Dim i1 As Integer
                                Dim f1 As Integer = 0
                                Dim c1 As Integer = 0
                                Dim ot, otn, otm As String
                                Dim pict, pictn, pictm As String
                                Dim opict, opictn, opictm As String
                                Dim x1 As Integer = dsp.Tables(0).Rows.Count
                                Dim ofid1 As String
                                Dim newt, newtn, newtm

                                For i1 = 0 To (x1 - 1)
                                    pid = dsp.Tables(0).Rows(i1)("pic_id").ToString
                                    fid1 = dsp.Tables(0).Rows(i1)("funcid").ToString
                                    ofid1 = dsp.Tables(0).Rows(i1)("oldfuncid").ToString
                                    pfid1 = dsp.Tables(0).Rows(i1)("parfuncid").ToString
                                    t = dsp.Tables(0).Rows(i1)("pm_image").ToString
                                    tn = dsp.Tables(0).Rows(i1)("pm_image_thumb").ToString
                                    tm = dsp.Tables(0).Rows(i1)("pm_image_med").ToString
                                    Dim intFileNameLength As Integer
                                    intFileNameLength = InStr(1, StrReverse(t), "/")
                                    t = Mid(t, (Len(t) - intFileNameLength) + 2)
                                    intFileNameLength = InStr(1, StrReverse(tn), "/")
                                    tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                                    intFileNameLength = InStr(1, StrReverse(tm), "/")
                                    tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                                    Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                                    t = appstr & "/pmimages/" & t
                                    tn = appstr & "/pmimages/" & tn
                                    tm = appstr & "/pmimages/" & tm

                                    t = Replace(t, "http://localhost/", "")
                                    tn = Replace(tn, "http://localhost/", "")
                                    tm = Replace(tm, "http://localhost/", "")

                                    t = Replace(t, "http://", "")
                                    tn = Replace(tn, "http://", "")
                                    tm = Replace(tm, "http://", "")

                                    ts = Replace(t, "g" & fid1 & "-", "g" & ofid1 & "-")
                                    tns = Replace(tn, "g" & fid1 & "-", "g" & ofid1 & "-")
                                    tms = Replace(tm, "g" & fid1 & "-", "g" & ofid1 & "-")

                                    td = Replace(t, "g" & fid1 & "-", "g" & fid1 & "-")
                                    tnd = Replace(tn, "g" & fid1 & "-", "g" & fid1 & "-")
                                    tmd = Replace(tm, "g" & fid1 & "-", "g" & fid1 & "-")


                                    If oloc <> "" Then
                                        ts = Replace(ts, ap, oloc)
                                        tns = Replace(tns, ap, oloc)
                                        tms = Replace(tms, ap, oloc)

                                        td = Replace(td, ap, oloc)
                                        tnd = Replace(tnd, ap, oloc)
                                        tmd = Replace(tmd, ap, oloc)
                                    End If


                                    ts = Server.MapPath("\") & ts
                                    ts = Replace(ts, "\", "/")
                                    td = Server.MapPath("\") & td
                                    td = Replace(td, "\", "/")


                                    tns = Server.MapPath("\") & tns
                                    tns = Replace(tns, "\", "/")
                                    tnd = Server.MapPath("\") & tnd
                                    tnd = Replace(tnd, "\", "/")


                                    tms = Server.MapPath("\") & tms
                                    tms = Replace(tms, "\", "/")
                                    tmd = Server.MapPath("\") & tmd
                                    tmd = Replace(tmd, "\", "/")

                                    'System.IO.File.Copy(source, dest)
                                    System.IO.File.Copy(ts, td, True)
                                    System.IO.File.Copy(tns, tnd, True)
                                    System.IO.File.Copy(tms, tmd, True)


                                Next

                            Catch ex As Exception

                            End Try
                        End If
                        'func.Update(sql)
                        Dim ecryn As String

                        Dim roa As String
                        If rbro.Checked = True Then
                            roa = "R"
                        Else
                            roa = "A"
                        End If
                        Dim rat As String
                        If cbrat.Checked = True Then
                            rat = "Y"
                        Else
                            rat = "N"
                        End If
                        If db = "0" Or db = "" Then
                            sql = "usp_copyTasks1 '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & newid & "', '" & user & "'"
                        Else
                            If roa = "R" Then
                                sql = "usp_copyTasks1MDB '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & newid & "', '" & user & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & rat & "'"
                            Else
                                sql = "usp_copyTasks2MDB '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & newid & "', '" & user & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & rat & "'"
                            End If

                        End If
                        func.Update(sql)
                        If db <> "0" Then
                            If tpm = "Y" Then
                                sql = "usp_copyTasks1tpmMDB '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & eqid & "', '" & newid & "', '" & user & "', '" & mdb & "', '" & orig & "','" & ap & "','" & orig & "','" & srvr & "'"
                                mdb = db
                                func.Update(sql)
                                sql = "select pic_id, parfuncid, funcid, tpm_image, tpm_image_med, tpm_image_thumb from tpmimages where funcid = '" & newid & "'"
                                If mdb <> orig Then
                                    Try
                                        Dim ds As New DataSet
                                        ds = func.GetDSData(sql)
                                        Dim fid, pfid As String
                                        oloc = lbloloc.Value
                                        Dim i As Integer
                                        Dim f As Integer = 0
                                        Dim c As Integer = 0
                                        Dim ot, otn, otm As String
                                        Dim pict, pictn, pictm As String
                                        Dim opict, opictn, opictm As String
                                        Dim x As Integer = ds.Tables(0).Rows.Count
                                        For i = 0 To (x - 1)
                                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                                            pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                            t = ds.Tables(0).Rows(i)("tpm_image").ToString
                                            tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                                            tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                                            Dim intFileNameLength As Integer
                                            intFileNameLength = InStr(1, StrReverse(t), "/")
                                            t = Mid(t, (Len(t) - intFileNameLength) + 2)
                                            intFileNameLength = InStr(1, StrReverse(tn), "/")
                                            tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                                            intFileNameLength = InStr(1, StrReverse(tm), "/")
                                            tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                                            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                                            t = appstr & "/tpmimages/" & t
                                            tn = appstr & "/tpmimages/" & tn
                                            tm = appstr & "/tpmimages/" & tm
                                            t = Replace(t, "http//", "")
                                            tn = Replace(tn, "http//", "")
                                            tm = Replace(tm, "http//", "")

                                            pict = "a-newsImg" + fid
                                            pictn = "atn-newsImg" + fid
                                            pictm = "atm-newsImg" + fid

                                            opict = "a-newsImg" + pfid
                                            opictn = "atn-newsImg" + pfid
                                            opictm = "atm-newsImg" + pfid

                                            ts = Replace(t, pict, opict)
                                            tns = Replace(tn, pictn, opictn)
                                            tms = Replace(tm, pictm, opictm)

                                            ts = Replace(t, ap, oloc)
                                            tns = Replace(tn, ap, oloc)
                                            tms = Replace(tm, ap, oloc)

                                            ts = Server.MapPath("\") & ts
                                            ts = Replace(ts, "\", "/")
                                            td = Server.MapPath("\") & t
                                            td = Replace(td, "\", "/")
                                            System.IO.File.Copy(ts, td, True)

                                            tns = Server.MapPath("\") & tns
                                            tns = Replace(tns, "\", "/")
                                            tnd = Server.MapPath("\") & tn
                                            tnd = Replace(tnd, "\", "/")
                                            System.IO.File.Copy(tns, tnd, True)

                                            tms = Server.MapPath("\") & tms
                                            tms = Replace(tms, "\", "/")
                                            tmd = Server.MapPath("\") & tm
                                            tmd = Replace(tmd, "\", "/")
                                            System.IO.File.Copy(tms, tmd, True)
                                        Next
                                    Catch ex As Exception

                                    End Try
                                Else
                                    Try
                                        Dim ds As New DataSet
                                        ds = func.GetDSData(sql)
                                        Dim fid, pfid As String
                                        Dim i As Integer
                                        Dim f As Integer = 0
                                        Dim c As Integer = 0
                                        Dim ot, otn, otm As String
                                        Dim pict, pictn, pictm As String
                                        Dim opict, opictn, opictm As String
                                        Dim x As Integer = ds.Tables(0).Rows.Count
                                        For i = 0 To (x - 1)
                                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                                            pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                            t = ds.Tables(0).Rows(i)("tpm_image").ToString
                                            tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                                            tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                                            Dim intFileNameLength As Integer
                                            intFileNameLength = InStr(1, StrReverse(t), "/")
                                            t = Mid(t, (Len(t) - intFileNameLength) + 2)
                                            intFileNameLength = InStr(1, StrReverse(tn), "/")
                                            tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                                            intFileNameLength = InStr(1, StrReverse(tm), "/")
                                            tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                                            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                                            t = appstr & "/tpmimages/" & t
                                            tn = appstr & "/tpmimages/" & tn
                                            tm = appstr & "/tpmimages/" & tm
                                            t = Replace(t, "http//", "")
                                            tn = Replace(tn, "http//", "")
                                            tm = Replace(tm, "http//", "")

                                            pict = "a-newsImg" + fid
                                            pictn = "atn-newsImg" + fid
                                            pictm = "atm-newsImg" + fid

                                            opict = "a-newsImg" + pfid
                                            opictn = "atn-newsImg" + pfid
                                            opictm = "atm-newsImg" + pfid

                                            ts = Replace(t, pict, opict)
                                            tns = Replace(tn, pictn, opictn)
                                            tms = Replace(tm, pictm, opictm)

                                            ts = Server.MapPath("\") & ts
                                            ts = Replace(ts, "\", "/")
                                            td = Server.MapPath("\") & t
                                            td = Replace(td, "\", "/")
                                            System.IO.File.Copy(ts, td, True)

                                            tns = Server.MapPath("\") & tns
                                            tns = Replace(tns, "\", "/")
                                            tnd = Server.MapPath("\") & tn
                                            tnd = Replace(tnd, "\", "/")
                                            System.IO.File.Copy(tns, tnd, True)

                                            tms = Server.MapPath("\") & tms
                                            tms = Replace(tms, "\", "/")
                                            tmd = Server.MapPath("\") & tm
                                            tmd = Replace(tmd, "\", "/")
                                            System.IO.File.Copy(tms, tmd, True)
                                        Next
                                    Catch ex As Exception

                                    End Try
                                End If

                            End If
                        End If

                    ElseIf listr = "c" Then
                        srvr = System.Configuration.ConfigurationManager.AppSettings("source")
                        If db = "0" Or db = "" Then
                            sql = "usp_copyComps '" & cid & "', '" & oldfu & "', '" & newid & "'"
                        Else
                            '@cid int, @oldfuid int, @newfuid int, @eqid int, @db varchar(50), @orig varchar(50), @srvr varchar(200)
                            sql = "usp_copyCompsMDB '" & cid & "', '" & oldfu & "', '" & newid & "', '" & eqid & "', '" & mdb & "', '" & orig & "', '" & srvr & "'"
                        End If
                        If db <> "0" Then
                            Try
                                Dim ds As New DataSet
                                ds = func.GetDSData(sql)
                                Dim fid, cmid, ofid, ocmid As String
                                Dim i As Integer
                                Dim f As Integer = 0
                                Dim c As Integer = 0
                                Dim x As Integer = ds.Tables(0).Rows.Count
                                For i = 0 To (x - 1)
                                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                    fid = ds.Tables(0).Rows(i)("funcid").ToString
                                    ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                    t = ds.Tables(0).Rows(i)("picurl").ToString
                                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                    t = Replace(t, "..", "")
                                    tn = Replace(tn, "..", "")
                                    tm = Replace(tm, "..", "")
                                    If cmid = "" Then
                                        f = f + 1
                                        nt = "b-eqImg" & fid & "i"
                                        ntn = "btn-eqImg" & fid & "i"
                                        ntm = "btm-eqImg" & fid & "i"

                                        ont = "b-eqImg" & ofid & "i"
                                        ontn = "btn-eqImg" & ofid & "i"
                                        ontm = "btm-eqImg" & ofid & "i"
                                    Else
                                        c = c + 1
                                        nt = "c-eqImg" & cmid & "i"
                                        ntn = "ctn-eqImg" & cmid & "i"
                                        ntm = "ctm-eqImg" & cmid & "i"

                                        ont = "c-eqImg" & ocmid & "i"
                                        ontn = "ctn-eqImg" & ocmid & "i"
                                        ontm = "ctm-eqImg" & ocmid & "i"
                                    End If

                                    ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                                    ts = Replace(ts, "\", "/")
                                    td = Server.MapPath("\") & cloc & t
                                    td = Replace(td, "\", "/")
                                    System.IO.File.Copy(ts, td, True)

                                    tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                                    tn = Replace(tn, "\", "/")
                                    tnd = Server.MapPath("\") & cloc & tn
                                    tnd = Replace(tnd, "\", "/")
                                    System.IO.File.Copy(tns, tnd, True)

                                    tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                                    tms = Replace(tms, "\", "/")
                                    tmd = Server.MapPath("\") & cloc & tm
                                    tmd = Replace(tmd, "\", "/")
                                    System.IO.File.Copy(tms, tmd, True)
                                Next
                            Catch ex As Exception

                            End Try
                        Else
                            Try
                                Dim ds As New DataSet
                                ds = func.GetDSData(sql)
                                Dim fid, cmid, ofid, ocmid As String
                                Dim i As Integer
                                Dim f As Integer = 0
                                Dim c As Integer = 0
                                Dim x As Integer = ds.Tables(0).Rows.Count
                                For i = 0 To (x - 1)
                                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                    fid = ds.Tables(0).Rows(i)("funcid").ToString
                                    ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                    t = ds.Tables(0).Rows(i)("picurl").ToString
                                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                    t = Replace(t, "..", "")
                                    tn = Replace(tn, "..", "")
                                    tm = Replace(tm, "..", "")
                                    If cmid = "" Then
                                        f = f + 1
                                        nt = "b-eqImg" & fid & "i"
                                        ntn = "btn-eqImg" & fid & "i"
                                        ntm = "btm-eqImg" & fid & "i"

                                        ont = "b-eqImg" & ofid & "i"
                                        ontn = "btn-eqImg" & ofid & "i"
                                        ontm = "btm-eqImg" & ofid & "i"
                                    Else
                                        c = c + 1
                                        nt = "c-eqImg" & cmid & "i"
                                        ntn = "ctn-eqImg" & cmid & "i"
                                        ntm = "ctm-eqImg" & cmid & "i"

                                        ont = "c-eqImg" & ocmid & "i"
                                        ontn = "ctn-eqImg" & ocmid & "i"
                                        ontm = "ctm-eqImg" & ocmid & "i"
                                    End If

                                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                                    ts = Replace(ts, "\", "/")
                                    td = Server.MapPath("\") & ap & t
                                    td = Replace(td, "\", "/")
                                    System.IO.File.Copy(ts, td, True)

                                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                                    tn = Replace(tn, "\", "/")
                                    tnd = Server.MapPath("\") & ap & tn
                                    tnd = Replace(tnd, "\", "/")
                                    System.IO.File.Copy(tns, tnd, True)

                                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                                    tms = Replace(tms, "\", "/")
                                    tmd = Server.MapPath("\") & ap & tm
                                    tmd = Replace(tmd, "\", "/")
                                    System.IO.File.Copy(tms, tmd, True)
                                Next
                            Catch ex As Exception

                            End Try
                        End If
                    End If
                    'Try
                    'func.Update(sql)
                    'Catch ex As Exception
                    'Dim strMessage As String =  tmod.getmsg("cdstr933" , "FuncCopyMini.aspx.vb")

                    'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    'End Try
                    Dim fsi As Integer
                    Try
                        sql = "select count(*) from pmsitefm where siteid = '" & sid & "'"
                        fsi = func.Scalar(sql)
                        If fsi > 0 Then
                            sql = "usp_updateSiteFM '" & cid & "', '" & sid & "'"
                            func.Update(sql)
                        End If
                    Catch ex As Exception

                    End Try
                   

                    PopNewFunc(newid)
                    rptrfuncrev.DataSource = Nothing
                    rptrfuncrev.DataBind()
                    txtpg.Value = "1"
                    'lblcopyflg.Value = flg
                    'txtneweqnum.Text = ""
                    tbltop.Attributes.Add("class", "details")
                    tblfuncrev.Attributes.Add("class", "details")
                    tblrevdetails.Attributes.Add("class", "details")

                    'tblneweqhdr.Attributes.Add("class", "view")
                    tblneweq.Attributes.Add("class", "view")
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr934", "FuncCopyMini.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                func.Dispose()
            Else
                Dim strMessage As String = "No New Function Name Entered"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            
            Try
                login = HttpContext.Current.Session("Logged_IN").ToString()
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr935", "FuncCopyMini.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "no"
                Exit Sub
            End Try
            'Dim strMessage1 As String = tmod.getmsg("cdstr936", "FuncCopyMini.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
            'lbllog.Value = "noeqid"

            'End If
        End If


    End Sub
    Private Sub PopNewFunc(ByVal newid As Integer)
        sql = "usp_getNewFuncRecord '" & newid & "'"
        dr = func.GetRdrData(sql)
        While dr.Read
            tdfuncnew.InnerHtml = dr.Item("func").ToString
            txtfuncspl.Text = dr.Item("spl").ToString
            tdcrby.InnerHtml = dr.Item("createdby").ToString
            tdcrdate.InnerHtml = dr.Item("createdate").ToString
            tdcrphone.InnerHtml = dr.Item("cph").ToString
            tdcremail.InnerHtml = dr.Item("cm").ToString
        End While
        dr.Close()
    End Sub

    Private Sub ibtnsaveneweq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsaveneweq.Click
        Dim fuid, desc, spl As String
        fuid = lblnewfuncid.Value
        spl = txtfuncspl.Text
        spl = Replace(spl, "'", Chr(180), , , vbTextCompare)
        sql = Replace(sql, "--", "-", , , vbTextCompare)
        sql = Replace(sql, ";", ":", , , vbTextCompare)
        If Len(spl) = 0 Then spl = ""
        If Len(spl) > 250 Then
            Dim strMessage As String =  tmod.getmsg("cdstr937" , "FuncCopyMini.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        sql = "update functions set spl = '" & spl & "' where func_id = '" & fuid & "'"
        func.Open()
        Try
            func.Update(sql)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr938" , "FuncCopyMini.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try

        PopFunc(PageNumber)
        func.Dispose()
        tbltop.Attributes.Add("class", "view")
        'tblneweqhdr.Attributes.Add("class", "details")
        tblneweq.Attributes.Add("class", "details")
    End Sub


    Private Sub rptrfuncrev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrfuncrev.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                                e.Item.ItemType <> ListItemType.Footer Then
            Dim pic As HtmlImage = CType(e.Item.FindControl("imgpic"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim fid As String = DataBinder.Eval(e.Item.DataItem, "func_id").ToString
            Dim cnt As String = DataBinder.Eval(e.Item.DataItem, "fpcnt").ToString
            If cnt <> "0" Then
                pic.Attributes.Add("onclick", "getfuport('" & id & "','" & fid & "');")
                pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpic.gif")
            Else
                pic.Attributes.Add("onclick", "")
                pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpicdis.gif")
            End If
        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang2304 As Label
			lang2304 = CType(e.Item.FindControl("lang2304"), Label)
			lang2304.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2304")
		Catch ex As Exception
		End Try
		Try
                Dim lang2305 As Label
			lang2305 = CType(e.Item.FindControl("lang2305"), Label)
			lang2305.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2305")
		Catch ex As Exception
		End Try
		Try
                Dim lang2306 As Label
			lang2306 = CType(e.Item.FindControl("lang2306"), Label)
			lang2306.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2306")
		Catch ex As Exception
		End Try
		Try
                Dim lang2307 As Label
			lang2307 = CType(e.Item.FindControl("lang2307"), Label)
			lang2307.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2307")
		Catch ex As Exception
		End Try
		Try
                Dim lang2308 As Label
			lang2308 = CType(e.Item.FindControl("lang2308"), Label)
			lang2308.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2308")
		Catch ex As Exception
		End Try
		Try
                Dim lang2309 As Label
			lang2309 = CType(e.Item.FindControl("lang2309"), Label)
			lang2309.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2309")
		Catch ex As Exception
		End Try
		Try
                Dim lang2310 As Label
			lang2310 = CType(e.Item.FindControl("lang2310"), Label)
			lang2310.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2310")
		Catch ex As Exception
		End Try
		Try
                Dim lang2311 As Label
			lang2311 = CType(e.Item.FindControl("lang2311"), Label)
			lang2311.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2311")
		Catch ex As Exception
		End Try
		Try
                Dim lang2312 As Label
			lang2312 = CType(e.Item.FindControl("lang2312"), Label)
			lang2312.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2312")
		Catch ex As Exception
		End Try
		Try
                Dim lang2313 As Label
			lang2313 = CType(e.Item.FindControl("lang2313"), Label)
			lang2313.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2313")
		Catch ex As Exception
		End Try
		Try
                Dim lang2314 As Label
			lang2314 = CType(e.Item.FindControl("lang2314"), Label)
			lang2314.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2314")
		Catch ex As Exception
		End Try
		Try
                Dim lang2315 As Label
			lang2315 = CType(e.Item.FindControl("lang2315"), Label)
			lang2315.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2315")
		Catch ex As Exception
		End Try
		Try
                Dim lang2316 As Label
			lang2316 = CType(e.Item.FindControl("lang2316"), Label)
			lang2316.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2316")
		Catch ex As Exception
		End Try
		Try
                Dim lang2317 As Label
			lang2317 = CType(e.Item.FindControl("lang2317"), Label)
			lang2317.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2317")
		Catch ex As Exception
		End Try
		Try
                Dim lang2318 As Label
			lang2318 = CType(e.Item.FindControl("lang2318"), Label)
			lang2318.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2318")
		Catch ex As Exception
		End Try
		Try
                Dim lang2319 As Label
			lang2319 = CType(e.Item.FindControl("lang2319"), Label)
			lang2319.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2319")
		Catch ex As Exception
		End Try
		Try
                Dim lang2320 As Label
			lang2320 = CType(e.Item.FindControl("lang2320"), Label)
			lang2320.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2320")
		Catch ex As Exception
		End Try
		Try
                Dim lang2321 As Label
			lang2321 = CType(e.Item.FindControl("lang2321"), Label)
			lang2321.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2321")
		Catch ex As Exception
		End Try
		Try
                Dim lang2322 As Label
			lang2322 = CType(e.Item.FindControl("lang2322"), Label)
			lang2322.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2322")
		Catch ex As Exception
		End Try
		Try
                Dim lang2323 As Label
			lang2323 = CType(e.Item.FindControl("lang2323"), Label)
			lang2323.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2323")
		Catch ex As Exception
		End Try
		Try
                Dim lang2324 As Label
			lang2324 = CType(e.Item.FindControl("lang2324"), Label)
			lang2324.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2324")
		Catch ex As Exception
		End Try
		Try
                Dim lang2325 As Label
			lang2325 = CType(e.Item.FindControl("lang2325"), Label)
			lang2325.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2325")
		Catch ex As Exception
		End Try
		Try
                Dim lang2326 As Label
			lang2326 = CType(e.Item.FindControl("lang2326"), Label)
			lang2326.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2326")
		Catch ex As Exception
		End Try
		Try
                Dim lang2327 As Label
			lang2327 = CType(e.Item.FindControl("lang2327"), Label)
			lang2327.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2327")
		Catch ex As Exception
		End Try
		Try
                Dim lang2328 As Label
			lang2328 = CType(e.Item.FindControl("lang2328"), Label)
			lang2328.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2328")
		Catch ex As Exception
		End Try
		Try
                Dim lang2329 As Label
			lang2329 = CType(e.Item.FindControl("lang2329"), Label)
			lang2329.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2329")
		Catch ex As Exception
		End Try
		Try
                Dim lang2330 As Label
			lang2330 = CType(e.Item.FindControl("lang2330"), Label)
			lang2330.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2330")
		Catch ex As Exception
		End Try
		Try
                Dim lang2331 As Label
			lang2331 = CType(e.Item.FindControl("lang2331"), Label)
			lang2331.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2331")
		Catch ex As Exception
		End Try
		Try
                Dim lang2332 As Label
			lang2332 = CType(e.Item.FindControl("lang2332"), Label)
			lang2332.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2332")
		Catch ex As Exception
		End Try
		Try
                Dim lang2333 As Label
			lang2333 = CType(e.Item.FindControl("lang2333"), Label)
			lang2333.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2333")
		Catch ex As Exception
		End Try
		Try
                Dim lang2334 As Label
			lang2334 = CType(e.Item.FindControl("lang2334"), Label)
			lang2334.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2334")
		Catch ex As Exception
		End Try
		Try
                Dim lang2335 As Label
			lang2335 = CType(e.Item.FindControl("lang2335"), Label)
			lang2335.Text = axlabs.GetASPXPage("FuncCopyMini.aspx","lang2335")
		Catch ex As Exception
		End Try

End If

 End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2304.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2304")
        Catch ex As Exception
        End Try
        Try
            lang2305.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2305")
        Catch ex As Exception
        End Try
        Try
            lang2306.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2306")
        Catch ex As Exception
        End Try
        Try
            lang2307.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2307")
        Catch ex As Exception
        End Try
        Try
            lang2308.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2308")
        Catch ex As Exception
        End Try
        Try
            lang2309.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2309")
        Catch ex As Exception
        End Try
        Try
            lang2310.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2310")
        Catch ex As Exception
        End Try
        Try
            lang2311.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2311")
        Catch ex As Exception
        End Try
        Try
            lang2312.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2312")
        Catch ex As Exception
        End Try
        Try
            lang2313.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2313")
        Catch ex As Exception
        End Try
        Try
            lang2314.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2314")
        Catch ex As Exception
        End Try
        Try
            lang2315.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2315")
        Catch ex As Exception
        End Try
        Try
            lang2316.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2316")
        Catch ex As Exception
        End Try
        Try
            lang2317.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2317")
        Catch ex As Exception
        End Try
        Try
            lang2318.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2318")
        Catch ex As Exception
        End Try
        Try
            lang2319.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2319")
        Catch ex As Exception
        End Try
        Try
            lang2320.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2320")
        Catch ex As Exception
        End Try
        Try
            lang2321.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2321")
        Catch ex As Exception
        End Try
        Try
            lang2322.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2322")
        Catch ex As Exception
        End Try
        Try
            lang2323.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2323")
        Catch ex As Exception
        End Try
        Try
            lang2324.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2324")
        Catch ex As Exception
        End Try
        Try
            lang2325.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2325")
        Catch ex As Exception
        End Try
        Try
            lang2326.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2326")
        Catch ex As Exception
        End Try
        Try
            lang2327.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2327")
        Catch ex As Exception
        End Try
        Try
            lang2328.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2328")
        Catch ex As Exception
        End Try
        Try
            lang2329.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2329")
        Catch ex As Exception
        End Try
        Try
            lang2330.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2330")
        Catch ex As Exception
        End Try
        Try
            lang2331.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2331")
        Catch ex As Exception
        End Try
        Try
            lang2332.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2332")
        Catch ex As Exception
        End Try
        Try
            lang2333.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2333")
        Catch ex As Exception
        End Try
        Try
            lang2334.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2334")
        Catch ex As Exception
        End Try
        Try
            lang2335.Text = axlabs.GetASPXPage("FuncCopyMini.aspx", "lang2335")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtncopy.Attributes.Add("src", "../images2/eng/bgbuttons/copy.gif")
            ElseIf lang = "fre" Then
                ibtncopy.Attributes.Add("src", "../images2/fre/bgbuttons/copy.gif")
            ElseIf lang = "ger" Then
                ibtncopy.Attributes.Add("src", "../images2/ger/bgbuttons/copy.gif")
            ElseIf lang = "ita" Then
                ibtncopy.Attributes.Add("src", "../images2/ita/bgbuttons/copy.gif")
            ElseIf lang = "spa" Then
                ibtncopy.Attributes.Add("src", "../images2/spa/bgbuttons/copy.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
