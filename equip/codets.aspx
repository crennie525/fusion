<%@ Page Language="vb" AutoEventWireup="false" Codebehind="codets.aspx.vb" Inherits="lucy_r12.codets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>codets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="600">
				<tr>
					<td class="label" style="width: 120px"><asp:Label id="lang1919" runat="server">Component#</asp:Label></td>
					<td id="tdoldcomp" class="plainlabel" width="480" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1920" runat="server">Description</asp:Label></td>
					<td id="tdolddesc" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1921" runat="server">Special Indentifier</asp:Label></td>
					<td id="tdoldspl" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1922" runat="server">Designation</asp:Label></td>
					<td id="tdolddesig" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="label">Mfg</td>
					<td id="tdoldmfg" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1923" runat="server">Component Class</asp:Label></td>
					<td id="tdoldac" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1924" runat="server">Sub Class</asp:Label></td>
					<td id="tdoldsc" class="plainlabel" runat="server"></td>
				</tr>
				<tr>
					<td class="btmmenuy plainlabel" colSpan="5"><asp:Label id="lang1925" runat="server">Current Component Tasks (PM)</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2"><iframe style="BORDER-BOTTOM-STYLE: none; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none"
							id="geteq" height="100" src="../complib/complibtaskview2.aspx?typ=pm" frameBorder="no" width="780"
							 scrolling="yes" runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="btmmenuy plainlabel" colSpan="5"><asp:Label id="lang1926" runat="server">Current Component Tasks (TPM)</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2"><iframe style="BORDER-BOTTOM-STYLE: none; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none"
							id="geteqt" height="100" src="../complib/complibtaskview2.aspx?typ=tpm" frameBorder="no" width="780"
							 scrolling="yes" runat="server"></iframe>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblcoid" runat="server" NAME="lbleqid">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
