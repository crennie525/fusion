﻿Imports System.Data.SqlClient
Public Class meterhist
    Inherits System.Web.UI.Page
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim eqid, meterid, meter, munit, enterby, sid As String
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'mhistid, meterid, meterreading, readingdate, pmid, enterby
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            enterby = Request.QueryString("uid").ToString
            lblenterby.Value = enterby
            lblmeterid.Value = "0"
            txtpg.Value = "1"
            appset.Open()
            getmeter(PageNumber)
            appset.Dispose()
        Else
            If Request.Form("lblsubmit") = "getmeter" Then
                lblsubmit.Value = ""
                appset.Open()
                getmeter(PageNumber)
                trnew.Attributes.Add("class", "view")
                appset.Dispose()
            ElseIf Request.Form("lblsubmit") = "addread" Then
                lblsubmit.Value = ""
                appset.Open()
                addread()
                getmeter(PageNumber)
                appset.Dispose()
                tddate.InnerHtml = lblnewdate.Value
            End If
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber

                getmeter(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                getmeter(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
            tdmeter.InnerHtml = lblmeter.Value
            tdunits.InnerHtml = lblunit.Value
            tdeq.InnerHtml = lbleq.Value
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getmeter(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getmeter(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            appset.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub addread()
        Dim mread As String = txtnewreading.Text
        Dim rdate As String = lblnewdate.Value
        meterid = lblmeterid.Value
        enterby = lblenterby.Value
        sql = "insert into meterhist (meterid, meterreading, readingdate, enterby) values " _
            + "('" & meterid & "','" & mread & "','" & rdate & "','" & enterby & "'); " _
            + "update meters set lastreading = '" & mread & "', lastdate = '" & rdate & "' " _
            + "where meterid = '" & meterid & "'"
        appset.Update(sql)
        txtnewreading.Text = ""

    End Sub
    Private Sub getmeter(ByVal PageNumber As Integer)
        Dim intPgNav, intPgCnt As Integer
        eqid = lbleqid.Value
        meterid = lblmeterid.Value
        sql = "select count(*) from meterhist where meterid = '" & meterid & "'"
        intPgCnt = appset.Scalar(sql)
        If intPgCnt = 0 Then
            If meterid <> "0" Then
                Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")
                appset.CreateMessageAlert(Me, strMessage, "strKey1")
                'Exit Sub
            End If

        End If
        PageSize = "50"
        intPgNav = appset.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "h.readingdate desc"

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        Filter = "h.meterid = ''" & meterid & "''"
        'sql = "select * from meterhist where meterid = '" & meterid & "'"
        sql = "usp_getmeterlist_eq '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        dr = appset.GetRdrData(sql)
        GridView1.DataSource = dr
        GridView1.DataBind()
    End Sub

    Private Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        GridView1.EditIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        getmeter(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        PageNumber = txtpg.Value
        GridView1.EditIndex = e.NewEditIndex
        appset.Open()
        getmeter(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        Dim stopid, seq, newdate, routeid As String
        Dim index As Integer = Convert.ToInt32(e.RowIndex)
        Dim row As GridViewRow = GridView1.Rows(index)
        stopid = CType(row.FindControl("lblmhiste"), Label).Text
        seq = CType(row.FindControl("txtreading"), TextBox).Text
        If seq = "" Then
            Dim strMessage As String = "Reading Must Be a Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim seqi As Long
        Try
            seqi = System.Convert.ToInt32(seq)
        Catch ex As Exception
            Try
                seqi = System.Convert.ToDecimal(seq)
            Catch ex1 As Exception
                Dim strMessage As String = "Reading Must Be a Numeric Value"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
        End Try
        newdate = appset.CNOW
        enterby = lblenterby.Value
        sql = "update meterhist set meterreading = '" & seq & "', updatedate = '" & newdate & "', updateby = '" & enterby & "' " _
            + "where mhistid = '" & stopid & "'"
        appset.Open()
        appset.Update(sql)
        GridView1.EditIndex = -1
        PageNumber = txtpg.Value
        getmeter(PageNumber)
        appset.Dispose()
    End Sub
End Class