﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="choosemeter.aspx.vb" Inherits="lucy_r12.choosemeter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script  type="text/javascript">
        function getmeter(mtrid, mtr, uni, wku) {
            var typ = document.getElementById("lbltyp").value;
            var omtrid = document.getElementById("lblmeterid").value;
            var omtr = document.getElementById("lblmeter").value;
            var omtrido = document.getElementById("lblmeterido").value;
            var omtro = document.getElementById("lblmetero").value;
            if (typ == "orig") {
                if (omtrido != mtrid&&omtrido != "0") {
                    if (omtrido != mtrid) {
                        if (omtrido != "0" && omtrido != mtrid) {
                            var decision = confirm("Are you sure you want to change Meters?")
                            if (decision == true) {
                                document.getElementById("lblretmeterid").value = mtrid;
                                document.getElementById("lblretmeter").value = mtr;
                                document.getElementById("lblretunit").value = uni;
                                document.getElementById("lblretwkuse").value = wku;

                                document.getElementById("lblsubmit").value = "go";
                                document.getElementById("form1").submit();
                            }
                            else {
                                alert("Action Cancelled")
                            }
                        }

                    }
                    else if (omtrido == "0") {
                        document.getElementById("lblretmeterid").value = mtrid;
                        document.getElementById("lblretmeter").value = mtr;
                        document.getElementById("lblretunit").value = uni;
                        document.getElementById("lblretwkuse").value = wku;

                        document.getElementById("lblsubmit").value = "go";
                        document.getElementById("form1").submit();
                    }
                    else {
                        alert("This is the same meter already assigned to this Task")
                    }
                }
                else if (omtrido == "0") {
                    document.getElementById("lblretmeterid").value = mtrid;
                    document.getElementById("lblretmeter").value = mtr;
                    document.getElementById("lblretunit").value = uni;
                    document.getElementById("lblretwkuse").value = wku;

                    document.getElementById("lblsubmit").value = "go";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("This is the same meter already assigned to this Task")
                }
            }
            else if (typ != "orig") {
                if (omtrid != mtrid && omtrid != "0") {
                    if (omtrid != "0" && omtrid != mtrid) {
                        var decision = confirm("Are you sure you want to change Meters?");
                        if (decision == true) {
                            document.getElementById("lblretmeterid").value = mtrid;
                            document.getElementById("lblretmeter").value = mtr;
                            document.getElementById("lblretunit").value = uni;
                            document.getElementById("lblretwkuse").value = wku;

                            document.getElementById("lblsubmit").value = "go";
                            document.getElementById("form1").submit();
                        }
                        else {
                            alert("Action Cancelled")
                        }
                    }

                }
                else if (omtrid == "0") {
                        document.getElementById("lblretmeterid").value = mtrid;
                        document.getElementById("lblretmeter").value = mtr;
                        document.getElementById("lblretunit").value = uni;
                        document.getElementById("lblretwkuse").value = wku;

                        document.getElementById("lblsubmit").value = "go";
                        document.getElementById("form1").submit();
               }
               else {
                    alert("This is the same meter already assigned to this Task")
                }
            }
        }
        function checkret() {
            var ret = document.getElementById("lblsubmit").value;
            var typ = document.getElementById("lbltyp").value;
            if (ret == "return") {
                if (typ == "dad") {
                    var mtrid = document.getElementById("lblretmeterid").value;
                    var mtr = document.getElementById("lblretmeter").value;
                    var uni = document.getElementById("lblretunit").value;
                    var wku = document.getElementById("lblretwkuse").value;
                    var ret = mtrid + "~" + mtr + "~" + uni + "~" + wku;
                    //alert(ret)
                    window.parent.handlereturn(ret);
                }
                else {
                    var mtrid = document.getElementById("lblretmeterid").value;
                    window.parent.handlereturn(mtrid);
                }
            }
        }
        function addmeter() {
            eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("emeterdialog.aspx?eqid=" + eqid, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes")
                if (eReturn) {
                    document.getElementById("lblsubmit").value = "ref";
                    document.getElementById("form1").submit();
                }
            }
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="bluelabel" style="width: 100px">Equipment#</td>
    <td class="plainlabel" id="tdeq" runat="server" width="200"></td>
    <td width="30"><img alt="" onmouseover="return overlib('Add\Edit Meters for this Asset', ABOVE, LEFT)"
                                onclick="addmeter();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif"/></td>
    </tr>
    <tr>
    <td colspan="3" id="tdmeters" runat="server"></td>
    </tr>
    <tr>
    <td colspan="3" align="right"></td>
    </tr>
    </table>
    </div>
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblmeter" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblretmeterid" runat="server" />
    <input type="hidden" id="lblretmeter" runat="server" />
    <input type="hidden" id="lblretwkuse" runat="server" />
    <input type="hidden" id="lblretunit" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblmfid" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskillqty" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
     <input type="hidden" id="lblskillido" runat="server" />
            <input type="hidden" id="lblskillo" runat="server" />
            <input type="hidden" id="lblskillqtyo" runat="server" />
            <input type="hidden" id="lblmeterido" runat="server" />
            <input type="hidden" id="lblrdo" runat="server" />
            <input type="hidden" id="lblrdido" runat="server" />
            <input type="hidden" id="lbltyp" runat="server" />
            <input type="hidden" id="lblfreqo" runat="server" />
    <input type="hidden" id="lblmfido" runat="server" />
    <input type="hidden" id="lblmetero" runat="server" />

    </form>
</body>
</html>
