

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class fuimg
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim func As New Utilities
    Dim eqid, fuid As String
    Protected WithEvents ddfu As System.Web.UI.WebControls.DropDownList
    Protected WithEvents imgfu As System.Web.UI.HtmlControls.HtmlImage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            func.Open()
            PopFunc()
            LoadPics()
            func.Dispose()
            ddfu.Attributes.Add("onchange", "upfuid();")
        End If
    End Sub
    Private Sub ddfu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddfu.SelectedIndexChanged
        If ddfu.SelectedIndex <> 0 Then
            lblfuid.Value = ddfu.SelectedValue
            func.Open()
            LoadPics()
            func.Dispose()
        End If
    End Sub
    Private Sub PopFunc()
        Dim fucnt As Integer
        Dim dt, val, filt As String
        eqid = lbleqid.Value
        sql = "select count(*) from Functions where eqid = '" & eqid & "'"
        fucnt = func.Scalar(sql)
        If fucnt <> 0 Then
            dt = "Functions"
            val = "func_id, func"
            filt = " where eqid = '" & eqid & "'"
            dr = func.GetList(dt, val, filt)
            ddfu.DataSource = dr
            ddfu.DataTextField = "func"
            ddfu.DataValueField = "func_id"
            ddfu.DataBind()
            dr.Close()
            ddfu.Items.Insert(0, "Select Function")
            fuid = lblfuid.Value
            Try
                If fuid <> "0" Then
                    ddfu.SelectedValue = fuid
                End If
            Catch ex As Exception

            End Try
          
        Else
            ddfu.Items.Insert(0, "None")
        End If
    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgfu.Attributes.Add("src", "../images/appimages/funcimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgfu.Attributes.Add("src", "../images2/eng/funcimg.gif")
        ElseIf lang = "fre" Then
            imgfu.Attributes.Add("src", "../images2/fre/funcimg.gif")
        ElseIf lang = "ger" Then
            imgfu.Attributes.Add("src", "../images2/ger/funcimg.gif")
        ElseIf lang = "ita" Then
            imgfu.Attributes.Add("src", "../images2/ita/funcimg.gif")
        ElseIf lang = "spa" Then
            imgfu.Attributes.Add("src", "../images2/spa/funcimg.gif")
        End If

        Dim img, imgs, picid As String

        fuid = lblfuid.Value
        Dim pcnt As Integer
        If fuid <> "no" Then
            sql = "select count(*) from pmpictures where comid is null and funcid = '" & fuid & "'"
            pcnt = func.Scalar(sql)
            lblpcnt.Value = pcnt
        End If
        


        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from pmpictures p where p.comid is null and p.funcid = '" & fuid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = func.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    If File.Exists(img) Then
                        imgfu.Attributes.Add("src", img)
                    End If
                    'imgeq.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    'txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub


	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lblpg.Text = axlabs.GetASPXPage("fuimg.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub

End Class
