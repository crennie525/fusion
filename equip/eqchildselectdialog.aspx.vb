﻿Public Class eqchildselectdialog
    Inherits System.Web.UI.Page
    Dim eqid, sid, Login As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try


        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            sid = Request.QueryString("sid").ToString
            ifeq.Attributes.Add("src", "eqchildselect.aspx?sid=" & sid & "&eqid=" & eqid)

        End If
    End Sub

End Class