<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NCGrid.aspx.vb" Inherits="lucy_r12.NCGrid" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>NCGrid</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/NCGridaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg"  onload="checkit();" onunload="handleeq();">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px" width="720">
				<TBODY>
					<tr>
						<td colspan="3">
							<table>
								<tr height="20">
									<td class="label tbg" style="width: 140px" align="left"><asp:Label id="lang2432" runat="server">Add New Asset</asp:Label></td>
									<td style="width: 140px" class="tbg"><asp:textbox id="txtneweq" runat="server" Width="130px" MaxLength="50"></asp:textbox></td>
									<td style="width: 60px" class="tbg"><asp:imagebutton id="btnAddEq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton></td>
									<td width="380" class="bluelabel tbg" id="tduse" runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3"><asp:repeater id="rptreq" runat="server">
								<HeaderTemplate>
									<table cellspacing="0" width="720">
										<tr class="tbg" height="20">
											<td class="btmmenu plainlabel" style="width: 130px"><asp:Label id="lang2433" runat="server">Asset#</asp:Label></td>
											<td class="btmmenu plainlabel" width="295"><asp:Label id="lang2434" runat="server">Description</asp:Label></td>
											<td class="btmmenu plainlabel" width="295"><asp:Label id="lang2435" runat="server">Model#/Type</asp:Label></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr class="tbg" height="20">
										<td class="plainlabel">&nbsp;
											<asp:LinkButton ID="Label4"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"ncnum")%>' Runat = server>
											</asp:LinkButton></td>
										<td class="plainlabel">
											<asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"ncdesc")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel">
											<asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"ncmod")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbleqiditem" Text='<%# DataBinder.Eval(Container.DataItem,"ncid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbleqloctypei" Text='<%# DataBinder.Eval(Container.DataItem,"loctype")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</ItemTemplate>
								<AlternatingItemTemplate>
									<tr bgcolor="#E7F1FD" height="20">
										<td class="plainlabel">&nbsp;
											<asp:LinkButton ID="Linkbutton1"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"ncnum")%>' Runat = server>
											</asp:LinkButton></td>
										<td class="plainlabel">
											<asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"ncdesc")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel">
											<asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"ncmod")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"ncid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lbleqloctypea" Text='<%# DataBinder.Eval(Container.DataItem,"loctype")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</AlternatingItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></TD></TR></TBODY></TABLE> <input type="hidden" id="lblrd" runat="server" NAME="lblrd">
			<input type="hidden" id="lbleqid" runat="server" NAME="lbleqid"> <input type="hidden" id="lblchk" runat="server" NAME="lblchk">
			<input type="hidden" id="lbldchk" runat="server" NAME="lbldchk"> <input type="hidden" id="lblsid" runat="server" NAME="lblsid">
			<input type="hidden" id="lblcid" runat="server" NAME="lblcid"> <input type="hidden" id="lbldid" runat="server" NAME="lbldid">
			<input type="hidden" id="lblclid" runat="server" NAME="lblclid"><input type="hidden" id="lbluser" runat="server" NAME="lbluser">
			<input type="hidden" id="lbladdchk" runat="server" NAME="lbladdchk"><input type="hidden" id="appchk" runat="server" NAME="appchk">
			<input type="hidden" id="lbllid" runat="server" NAME="lbllid"><input type="hidden" id="lbltyp" runat="server" NAME="lbltyp">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
