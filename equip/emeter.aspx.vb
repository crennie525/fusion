﻿Imports System.Data.SqlClient
Public Class emeter
    Inherits System.Web.UI.Page
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim eqid As String
    Dim sql As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString '
            lbleqid.Value = eqid
            appset.Open()
            getmeters()
            appset.Dispose()

        End If
    End Sub
    Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        If Not IsDBNull(CatID) OrElse CatID <> "" Then
            If CatID = "HOURS" Then
                iL = 0
            ElseIf CatID = "MILES" Then
                iL = 1
            ElseIf CatID = "CYCLES" Then
                iL = 2
            Else
                iL = 1
            End If
        Else
            iL = 0
        End If
        Return iL
    End Function
    Private Sub getmeters()
        eqid = lbleqid.Value
        sql = "select * from meters where eqid = '" & eqid & "'"
        dr = appset.GetRdrData(sql)
        dgmeters.DataSource = dr
        dgmeters.DataBind()

    End Sub

    Private Sub dgmeters_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeters.CancelCommand
        dgmeters.EditItemIndex = -1
        appset.Open()
        getmeters()
        appset.Dispose()
    End Sub

    Private Sub dgmeters_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeters.DeleteCommand
        Dim meterid As String
        Dim meteridi As Label
        Try
            meterid = CType(e.Item.FindControl("lblmeteride"), Label).Text
        Catch ex As Exception
            meterid = CType(e.Item.FindControl("lblmeterid"), Label).Text
        End Try
        'meterid = meteridi.Text
        If meterid <> "" Then
            sql = "delete from meters where meterid = '" & meterid & "'"
            dgmeters.EditItemIndex = -1
            appset.Open()
            appset.Update(sql)
            getmeters()
            appset.Dispose()
        End If
    End Sub

    Private Sub dgmeters_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeters.EditCommand
        lblold.Value = CType(e.Item.FindControl("lblmeter"), Label).Text
        dgmeters.EditItemIndex = e.Item.ItemIndex
        appset.Open()
        getmeters()
        appset.Dispose()
    End Sub

    Private Sub dgmeters_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeters.ItemCommand
        If e.CommandName = "Add" Then
            Dim lname As String
            Dim lnamei As TextBox
            lnamei = CType(e.Item.FindControl("txtnewmeter"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)
            If Len(lname) > 50 Then
                Dim strMessage As String = "Meter ID is Limited to 50 Characters"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            Dim uname As String = lblunitret.Value
            Dim unamei As TextBox
            unamei = CType(e.Item.FindControl("txtnewunit"), TextBox)
            uname = unamei.Text.ToString
            If uname = "" Then
                Dim strMessage As String = "A Meter Unit Value is Required"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            ElseIf Len(uname) > 10 Then
                Dim strMessage As String = "Meter Unit Value is Limited to 10 Characters"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            Dim uret As String = lbluret.Value
            Dim nmeter As String
            Dim nmeteri As TextBox
            nmeteri = CType(e.Item.FindControl("txtnewuse"), TextBox)
            nmeter = nmeteri.Text
            If nmeter = "" Then
                Dim strMessage As String = "A Weekly Use Value is Required"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            Dim eqechk As Long
            Try
                eqechk = System.Convert.ToDecimal(nmeter)
                'eqechk = System.Convert.ToInt32(nmeter)
            Catch ex As Exception
                Dim strMessage As String = "Weekly Use Must Be A Numeric Value"
                'Dim strMessage As String = "Weekly Use Must Be A Non-Decimal Numeric Value"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End Try
            eqid = lbleqid.Value

            lnamei = CType(e.Item.FindControl("txtnewmeter"), TextBox)
            lname = lnamei.Text
            lname = appset.ModString1(lname)

            If lnamei.Text <> "" Then
                If Len(lnamei.Text) > 50 Then
                    Dim strMessage As String = "Meter ID is limited to 50 characters"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Else
                    appset.Open()
                    Dim eqstat As String
                    Dim cnt As Integer
                    sql = "select count(*) from meters where meter = '" & lname & "' and eqid = '" & eqid & "'"
                    cnt = appset.Scalar(sql)
                    If cnt = 0 Then
                        Dim cmd As New SqlCommand("insert into meters (eqid, meter, unit, wkuse) values " _
                                                  + "(@eqid, @meter, @unit, @wkuse)")

                        Dim param0 = New SqlParameter("@eqid", SqlDbType.VarChar)
                        param0.Value = eqid
                        cmd.Parameters.Add(param0)

                        Dim param01 = New SqlParameter("@meter", SqlDbType.VarChar)
                        If Len(lname) > 0 Then
                            param01.Value = lname
                        Else
                            param01.Value = System.DBNull.Value
                        End If
                        cmd.Parameters.Add(param01)

                        Dim param02 = New SqlParameter("@unit", SqlDbType.VarChar)
                        If Len(uname) > 0 Then
                            param02.Value = uname
                        Else
                            param02.Value = System.DBNull.Value
                        End If
                        cmd.Parameters.Add(param02)

                        Dim param03 = New SqlParameter("@wkuse", SqlDbType.VarChar)
                        If Len(nmeter) > 0 Then
                            param03.Value = nmeter
                        Else
                            param03.Value = System.DBNull.Value
                        End If
                        cmd.Parameters.Add(param03)

                        appset.UpdateHack(cmd)
                        getmeters()
                    Else
                        Dim strMessage As String = "Meter ID Must Be Unique for Equipment Record"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End If
                    appset.Dispose()
                End If
            Else
                Dim strMessage As String = "Meter ID Required"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If

        End If

    End Sub


    Private Sub dgmeters_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgmeters.UpdateCommand
        Dim lname As String
        Dim lnamei As TextBox
        lnamei = CType(e.Item.FindControl("txtmeter"), TextBox)
        lname = lnamei.Text
        lname = appset.ModString1(lname)

        Dim uname As String = lblunitret.Value
        Dim unamei As TextBox
        unamei = CType(e.Item.FindControl("txtunit"), TextBox)
        uname = unamei.Text.ToString
        If uname = "" Then
            Dim strMessage As String = "A Meter Unit Value is Required"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        ElseIf Len(uname) > 10 Then
            Dim strMessage As String = "Meter Unit Value is Limited to 10 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim uret As String = lbluret.Value
        Dim nmeter As String
        Dim nmeteri As TextBox
        nmeteri = CType(e.Item.FindControl("txtuse"), TextBox)
        nmeter = nmeteri.Text
        If nmeter = "" Then
            Dim strMessage As String = "A Weekly Use Value is Required"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim eqechk As Long
        Try
            eqechk = System.Convert.ToDecimal(nmeter)
            'eqechk = System.Convert.ToInt32(nmeter)
        Catch ex As Exception
            Dim strMessage As String = "Weekly Use Must Be A Numeric Value"
            'Dim strMessage As String = "Weekly Use Must Be A Non-Decimal Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        eqid = lbleqid.Value

        Dim meterid As String
        Dim meteridi As Label
        meteridi = CType(e.Item.FindControl("lblmeteride"), Label)
        meterid = meteridi.Text

        appset.Open()
        Dim cnt As Integer = 0
        Dim old As String = lblold.Value
        If lname <> old Then
            sql = "select count(*) from meters where meter = '" & lname & "' and eqid = '" & eqid & "'"
            cnt = appset.Scalar(sql)
        End If
        If cnt = 0 Then
            Dim cmd As New SqlCommand("update meters set meter = @meter, unit = @unit, " _
                                      + "wkuse = @wkuse where meterid = @meterid ")

            Dim param0 = New SqlParameter("@meterid", SqlDbType.VarChar)
            param0.Value = meterid
            cmd.Parameters.Add(param0)

            Dim param01 = New SqlParameter("@meter", SqlDbType.VarChar)
            If Len(lname) > 0 Then
                param01.Value = lname
            Else
                param01.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param01)

            Dim param02 = New SqlParameter("@unit", SqlDbType.VarChar)
            If Len(uname) > 0 Then
                param02.Value = uname
            Else
                param02.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param02)

            Dim param03 = New SqlParameter("@wkuse", SqlDbType.VarChar)
            If Len(nmeter) > 0 Then
                param03.Value = nmeter
            Else
                param03.Value = System.DBNull.Value
            End If
            cmd.Parameters.Add(param03)

            appset.UpdateHack(cmd)

            dgmeters.EditItemIndex = -1
            getmeters()

        End If
        appset.Dispose()
    End Sub
End Class