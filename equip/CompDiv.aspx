<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CompDiv.aspx.vb" Inherits="lucy_r12.CompDiv" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>CompDiv</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script  type="text/javascript" src="../scripts1/CompDivaspx_c.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script  type="text/javascript">
        <!--
        function getecd1() {
            var compnum = document.getElementById("txtnewcomp").value;
            //if (compnum != "") {
            var eReturn = window.showModalDialog("compdiv1dialog.aspx?compnum=" + compnum + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
            if (eReturn) {
                //alert(eReturn)
                var ret = eReturn.split("~");
                var chk = ret[0];
                if (chk != "can") {
                    var chk2 = ret[1];
                    if (chk2 != compnum) {
                        document.getElementById("txtnewcomp").value = chk2;
                    }
                    document.getElementById("lblnewecd1id").value = chk;
                    document.getElementById("form1").submit();
                }
                else {
                    return false;
                }

            }
            else {

                return false;
            }
            //}
        }
        function DisableButton(b) {
            document.getElementById("btntocomp").className = "details";
            document.getElementById("btnfromcomp").className = "details";
            document.getElementById("todis").className = "view";
            document.getElementById("fromdis").className = "view";
            var ecd2 = document.getElementById("lblcurrecd2").value;
            var chk = document.getElementById("lblisecd").value;
            if (chk == "1") {
                var eReturn = window.showModalDialog("compdivecd3dialog.aspx?ecd2=" + ecd2 + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblnewecd3").value = eReturn;
                    document.getElementById("form1").submit();
                }
            }
            else {
                document.getElementById("form1").submit();
            }

        }
        function checkit() {
            if (document.getElementById("lblcompchk").value == "movecomp") {
                document.getElementById("lblcompchk").value = "";
                var fuid = document.getElementById("lblfuid").value;
                window.parent.handleeq('func', 'chk', fuid);
            }
            var chk1 = document.getElementById("lblgetecd2").value;
            if (chk1 == "1") {
                var ecd1 = document.getElementById("lblecd1id").value;
                var ecd2txt = document.getElementById("lblecd2txt").value;
                //alert(ecd1)
                var eReturn = window.showModalDialog("compdivecd2dialog.aspx?ecd1=" + ecd1 + "&ecd2txt=" + ecd2txt + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblgetecd2").value = "";
                    //alert(eReturn)
                    document.getElementById("lblcurrecd2").value = eReturn;
                }
            }

            showcompret();
            var chk = document.getElementById("lbllog").value;
            if (chk == "no") {
                window.parent.doref();
            }
            else {
                window.parent.setref();
            }
            var par = document.getElementById("lblpar").value;
            //alert(par)
            if (par == "comp") {
                val = document.getElementById("lblcoid").value;
                //alert(val)
                window.parent.handleeq(par, "", val);
            }
            var del = document.getElementById("lbldel").value;
            if (del == "1") {
                gridret();
            }
            var val = document.getElementById("lblapp").value;
            window.parent.handleswitch(val);
            
        }
        //<asp:Label id="lang2041" runat="server">Available Failure Modes</asp:Label>
        function showcomp() {
            var chk = document.getElementById("lblswitch").value;
            if (chk == "all") {
                document.getElementById("lblswitch").value = "comp";
                document.getElementById("lbfailmaster").className = "details";
                document.getElementById("lbfailcomp").className = "view";
                document.getElementById("aswitch").innerHTML = "Unassigned Failure Modes";
            }
            else {
                document.getElementById("lblswitch").value = "all";
                document.getElementById("lbfailmaster").className = "view";
                document.getElementById("lbfailcomp").className = "details";
                document.getElementById("aswitch").innerHTML = "Available Failure Modes";
            }
        }
        function showcompret() {
            var chk = document.getElementById("lblswitch").value;
            if (chk == "all") {

                document.getElementById("lbfailmaster").className = "view";
                document.getElementById("lbfailcomp").className = "details";
                document.getElementById("aswitch").innerHTML = "Available Failure Modes";
            }
            else {

                document.getElementById("lbfailmaster").className = "details";
                document.getElementById("lbfailcomp").className = "view";
                document.getElementById("aswitch").innerHTML = "Unassigned Failure Modes";

            }
        }
        function GoToTasks(par, val) {
            //var tcnt = document.getElementById("lbltaskcnt").value;
            //alert(tcnt)
            //if(tcnt=="0") {
            //alert("No Tasks Created For This Component, Please Jump From Functions")
            //}
            //else {
            window.parent.handletask('5');
            //}
        }
        function getfuncfor() {
            var eqid = document.getElementById("lbleqid").value;
            var oldfuid = document.getElementById("lblfuid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("retfuncdialog.aspx?typ=fu&eqid=" + eqid, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    var fret = eReturn.split(",")
                    var fuid = fret[0];
                    if (oldfuid != fuid) {
                        document.getElementById("lblnewfuid").value = fuid;
                        document.getElementById("lblcompchk").value = "movecomp";
                        document.getElementById("form1").submit();
                    }
                    else {
                        alert("You chose the same Function you are working in")
                    }
                }
            }
        }
            //-->
    </script>
</head>
<body class="tbg" onload="checkit();">
    <form id="form1" method="post" runat="server">
    <div class="FreezePaneOff" id="FreezePane" style="width: 720px" align="center">
        <div class="InnerFreezePane" id="InnerFreezePane">
        </div>
    </div>
    <div id="overDiv" style="z-index: 1000; position: absolute; visibility: hidden">
    </div>
    <table id="eqdetdiv" style="position: absolute; top: 2px; left: 10px" cellspacing="0"
        cellpadding="0" width="710">
        <tr>
            <td style="width: 120px">
            </td>
            <td style="width: 90px">
            </td>
            <td style="width: 50px">
            </td>
            <td width="30">
            </td>
            <td width="255">
            </td>
            <td style="width: 140px">
            </td>
            <td style="width: 80px">
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <table cellspacing="0" cellpadding="0" class="tbg">
                    <tr style="border-bottom: #7ba4e0 thin solid" height="26">
                        <td class="bluelabel" width="136">
                            <asp:Label ID="lang2031" runat="server">Add New Component</asp:Label>
                        </td>
                        <td style="width: 130px">
                            <asp:TextBox ID="txtnewcomp" runat="server" MaxLength="100" Width="130px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td class="bluelabel" width="85">
                            <asp:Label ID="lang2032" runat="server">Description</asp:Label>
                        </td>
                        <td width="245">
                            <asp:TextBox ID="txtnewdesc" runat="server" MaxLength="100" Width="240px" CssClass="plainlabel"></asp:TextBox>
                        </td>
                        <td width="35">
                            <asp:ImageButton ID="btnAddFu" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: #7ba4e0 thin solid" colspan="7">
                <img height="6" src="../images/appbuttons/minibuttons/6px.gif" width="6">
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <table cellspacing="2" cellpadding="0">
                    <tr>
                        <td class="plainlabelblue" style="width: 100px">
                            <asp:Label ID="lang2033" runat="server">Current Equipment:</asp:Label>
                        </td>
                        <td class="plainlabelblue" id="bte" width="200" runat="server">
                        </td>
                        <td class="plainlabelblue" id="bde" width="375" runat="server">
                        </td>
                    </tr>
                    <tr>
                        <td class="plainlabelblue">
                            &nbsp;
                        </td>
                        <td class="plainlabelblue" id="btf" runat="server">
                        </td>
                        <td class="plainlabelblue" id="bdf" runat="server">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang2034" runat="server">Search Key</asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtkey" runat="server" MaxLength="100" Width="130px" CssClass="plainlabel"
                    Enabled="False"></asp:TextBox>
            </td>
            <td>
                <img class="details" src="../images/appbuttons/minibuttons/Q.gif" height="20" style="width: 20px">
            </td>
            <td class="plainlabelblue" align="center">
                <input class="details" type="checkbox" id="cbatcl" runat="server" onclick="checkcopy();">
            </td>
            <td align="center" colspan="2" rowspan="10">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" colspan="2">
                            <table>
                                <tr>
                                    <td align="center" colspan="6">
                                        <img id="imgco" onclick="getbig();" height="206" src="../images/appimages/compimg.gif"
                                            style="width: 216px" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bluelabel" style="width: 80px">
                                        <asp:Label ID="lang2035" runat="server">Order</asp:Label>
                                    </td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtiorder" runat="server" style="width: 40px" CssClass="plainlabel"></asp:TextBox>
                                    </td>
                                    <td style="width: 20px">
                                        <img onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif">
                                    </td>
                                    <td style="width: 20px">
                                        <img onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)"
                                            onclick="addpic();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/addmod.gif"
                                            id="btnupload" runat="server">
                                    </td>
                                    <td style="width: 20px">
                                        <img id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
                                            src="../images/appbuttons/minibuttons/del.gif" runat="server">
                                    </td>
                                    <td style="width: 20px">
                                        <img id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)"
                                            onclick="savdets();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif"
                                            runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="6">
                                        <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                            border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="border-right: blue 1px solid; width: 20px;">
                                                    <img id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                                        runat="server">
                                                </td>
                                                <td style="border-right: blue 1px solid; width: 20px;">
                                                    <img id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                                        runat="server">
                                                </td>
                                                <td style="border-right: blue 1px solid" valign="middle" align="center" width="134">
                                                    <asp:Label ID="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:Label>
                                                </td>
                                                <td style="border-right: blue 1px solid; width: 20px;">
                                                    <img id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                                        runat="server">
                                                </td>
                                                <td style="width: 20px">
                                                    <img id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
                                                        runat="server">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang2036" runat="server">Component</asp:Label>
            </td>
            <td colspan="4" class="label">
                <asp:TextBox ID="txtconame" runat="server" MaxLength="100" Width="180px" CssClass="plainlabel"></asp:TextBox>
                &nbsp;RTF&nbsp;<input type="checkbox" id="cbrtf" runat="server" />&nbsp;Detectable?&nbsp;<input type="checkbox" id="cbdet" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang2037" runat="server">Description</asp:Label>
            </td>
            <td colspan="4" class="label">
                <asp:TextBox ID="txtdesc" runat="server" MaxLength="100" Width="250px" CssClass="plainlabel"></asp:TextBox>
                &nbsp;&nbsp;RPN&nbsp;&nbsp;<asp:TextBox ID="txtrpn" runat="server" Width="50px" CssClass="plainlabel"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                SPL
            </td>
            <td colspan="4">
                <asp:TextBox ID="txtspl" runat="server" MaxLength="200" Width="250px" CssClass="plainlabel"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lang2038" runat="server">Designation</asp:Label>
            </td>
            <td colspan="4">
                <asp:TextBox ID="txtdesig" runat="server" MaxLength="200" Width="250px" CssClass="plainlabel"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                Mfg
            </td>
            <td colspan="4">
                <asp:TextBox ID="txtmfg" runat="server" Width="250px" CssClass="plainlabel wd250"
                    MaxLength="200"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label" height="20">
                <asp:Label ID="lang2039" runat="server">Component Class</asp:Label>
            </td>
            <td colspan="3" class="plainlabel" id="tdcclass" runat="server">
            </td>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <img id="btnaddasset" runat="server" class="view" onmouseover="return overlib('Add/Edit Asset Class', LEFT)"
                                onmouseout="return nd()" onclick="getACDiv();" alt="" src="../images/appbuttons/minibuttons/addmod.gif">
                        </td>
                        <td>
                            <img id="btncget" runat="server" class="view" onmouseover="return overlib('Lookup Asset Class', LEFT)"
                                onmouseout="return nd()" onclick="cget();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td>
                            <img id="btncrem" runat="server" class="details" src="../images/appbuttons/minibuttons/del.gif"
                                onmouseover="return overlib('Remove Asset Class from Component (Will Not Remove Failure Modes)', LEFT)"
                                onmouseout="return nd()" onclick="crem();">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="label" height="20">
                <asp:Label ID="lang2040" runat="server">Sub Class</asp:Label>
            </td>
            <td colspan="3" class="plainlabel" id="tdsclass" runat="server">
            </td>
        </tr>
        <tr>
            <td class="label" height="20">
                Occurrence Area
            </td>
            <td colspan="3">
                <asp:DropDownList ID="ddoca" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <table cellspacing="2" cellpadding="0">
                    <tbody>
                        <tr valign="middle" height="20">
                            <td width="10">
                            </td>
                            <td class="bluelabel" align="center" width="188">
                                <asp:Label ID="aswitch" runat="server">Available Failure Modes</asp:Label>
                            </td>
                            <td width="22">
                                <img src="../images/appbuttons/minibuttons/checkrel.gif" onclick="showcomp();" id="imgchk"
                                    runat="server" />
                            </td>
                            <td width="22">
                            </td>
                            <td class="bluelabel" align="center" width="210">
                                <asp:Label ID="lang2042" runat="server">Component Failure Modes</asp:Label>
                            </td>
                            <td width="10">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="2" align="center">
                                <asp:ListBox ID="lbfailmaster" runat="server" Width="200px" SelectionMode="Multiple"
                                    Height="70px"></asp:ListBox>
                                <asp:ListBox ID="lbfailcomp" runat="server" Width="200px" SelectionMode="Multiple"
                                    Height="70px" CssClass="details"></asp:ListBox>
                            </td>
                            <td valign="middle" align="center">
                                <img class="details" id="todis" height="20" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
                                    style="width: 20px" runat="server">
                                <img class="details" id="fromdis" height="20" src="../images/appbuttons/minibuttons/backgraybg.gif"
                                    style="width: 20px" runat="server">
                                <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                                </asp:ImageButton><br>
                                <asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                                </asp:ImageButton>
                                <img class="details" id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
                                    onclick="getss();" onmouseout="return nd()" height="20" src="../images/appbuttons/minibuttons/plusminus.gif"
                                    style="width: 20px">
                            </td>
                            <td align="center">
                                <asp:ListBox ID="lbfailmodes" runat="server" Width="200px" SelectionMode="Multiple"
                                    Height="70px"></asp:ListBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="label details" colspan="6">
                                <asp:Label ID="lang2043" runat="server">Add a New Failure Mode</asp:Label><asp:TextBox
                                    ID="txtnewfail" runat="server" Width="170px"></asp:TextBox>
                                &nbsp;<asp:ImageButton ID="btnaddfail" runat="server" ImageUrl="../images/appbuttons/minibuttons/addnewbg1.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: #7ba4e0 thin solid" colspan="7" class="plainlabelblue">
                To remove a failure mode that is assigned to an Occurrence Area the Occurrence Area
                needs to be selected.
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <table cellpadding="0" cellspacing="0" width="744">
                    <tr>
                        <td colspan="2" id="tdgtt" runat="server">
                            <a onclick="GoToTasks(5);" href="#">
                                <img id="btngtt" height="19" alt="" src="../images/appbuttons/bgbuttons/jtotaskssm.gif"
                                    width="122" border="0" runat="server"></a>
                        </td>
                        <td class="label" colspan="3" id="tdgttrb" runat="server">
                            <input id="rbdev" onclick="GetType(this.value);" type="radio" value="pmdev" name="rtyp"
                                runat="server"><asp:Label ID="lang2044" runat="server">PM Developer</asp:Label><input
                                    id="rbopt" onclick="GetType(this.value);" type="radio" value="pmopt" name="rtyp"
                                    runat="server"><asp:Label ID="lang2045" runat="server">PM Optimizer</asp:Label><br>
                            <input id="rbtdev" disabled onclick="GetType(this.value);" type="radio" value="rbdev"
                                name="rtyp" runat="server"><asp:Label ID="lang2046" runat="server">TPM Developer</asp:Label><input
                                    id="rbtopt" disabled onclick="GetType(this.value);" type="radio" value="rbopt"
                                    name="rtyp" runat="server"><asp:Label ID="lang2047" runat="server">TPM Optimizer</asp:Label>
                        </td>
                        <td align="right" colspan="2" id="tdopts" runat="server">
                            <img onclick="gridret();" height="20" alt="" src="../images/appbuttons/minibuttons/navgrid.gif"
                                style="width: 20px">
                                <img id="imgfor" class="details" src="../images/appbuttons/minibuttons/forwardgbg.gif" alt="" onclick="getfuncfor();" onmouseover="return overlib('Move this Compenent to another Function ')" onmouseout="return nd()" />
                                <asp:ImageButton 
                                ID="btndel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" 
                                style="height: 16px">
                                </asp:ImageButton><asp:ImageButton ID="btnsave" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif">
                                </asp:ImageButton>&nbsp;<img class="details" id="Img2" onmouseover="return overlib('Copy Components for Selected Function', ABOVE)"
                                    onclick="GetCompCopy();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
                                    style="width: 20px">&nbsp;<img class="imgbutton details" onmouseover="return overlib('Go to the Centralized PM Library', ABOVE, WIDTH, 270)"
                                        onclick="GoToPMLib();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/gotolibsm.gif"
                                        width="23">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="lbleqid" type="hidden" name="lbleqid" runat="server"><input id="lblcid"
        type="hidden" name="lblcid" runat="server">
    <input id="lblfuid" type="hidden" name="lblfuid" runat="server"><input id="lblpar"
        type="hidden" name="lblpar" runat="server">
    <input id="lblcoid" type="hidden" name="lblcoid" runat="server">
    <input id="lblpic" type="hidden" name="lblpic" runat="server">
    <input id="lbldid" type="hidden" runat="server">
    <input id="lblcompchk" type="hidden" runat="server"><input id="lblparcoid" type="hidden"
        runat="server">
    <input id="lblclid" type="hidden" runat="server"><input id="lblsid" type="hidden"
        runat="server">
    <input id="lblfailchk" type="hidden" runat="server">
    <input id="lbllock" type="hidden" name="lbllock" runat="server">
    <input id="lbllockedby" type="hidden" name="lbllockedby" runat="server">
    <input id="lbluser" type="hidden" runat="server">
    <input id="lbltaskcnt" type="hidden" runat="server">
    <input id="lblro" type="hidden" name="lblro" runat="server"><input id="lblappstr"
        type="hidden" name="lblappstr" runat="server">
    <input id="lblapp" type="hidden" name="lblapp" runat="server"><input id="lbldel"
        type="hidden" runat="server">
    <input id="lblpcnt" type="hidden" name="lblpcnt" runat="server">
    <input id="lblcurrp" type="hidden" name="lblcurrp" runat="server">
    <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
    <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
    <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
    <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
    <input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server">
    <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
    <input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders"
        type="hidden" name="lbliorders" runat="server">
    <input id="lbloldorder" type="hidden" name="lbloldorder" runat="server">
    <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
    <input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"><input id="lblcnt"
        type="hidden" runat="server">
    <input type="hidden" id="lbllog" runat="server"><input type="hidden" id="lblcohold"
        runat="server">
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblfailmode" runat="server" />
    <input type="hidden" id="lblswitch" runat="server" />
    <input type="hidden" id="lblisecd" runat="server" />
    <input type="hidden" id="lblgetecd2" runat="server" />
    <input type="hidden" id="lblgetecd3" runat="server" />
    <input type="hidden" id="lblcurrecd2" runat="server" />
    <input type="hidden" id="lblnewecd3" runat="server" />
    <input type="hidden" id="lblecd1id" runat="server" />
    <input type="hidden" id="lblecd2txt" runat="server" />
    <input type="hidden" id="lblnewecd1id" runat="server" />
    <input type="hidden" id="lblwho" runat="server" />
    <input type="hidden" id="lblnewfuid" runat="server" />
    <input type="hidden" id="lblchld" runat="server" />
    <input type="hidden" id="lblcomi" runat="server" />
    </form>
</body>
</html>
