<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibneweqdets.aspx.vb" Inherits="lucy_r12.pmlibneweqdets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibneweqdets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  src="../scripts/equipment_1.js"></script>
		<script  src="../scripts1/pmlibneweqdetsaspx_c.js"></script>
		<script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script  type="text/javascript">
            function getmeter() {
                eqid = document.getElementById("lbleqid").value;
                if (eqid != "") {
                    var eReturn = window.showModalDialog("emeterdialog.aspx?eqid=" + eqid, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes")
                }
            }
            function getxtra() {
                eqid = document.getElementById("lbleqid").value;
                if (eqid != "") {
                    var eReturn = window.showModalDialog("eqxtradialog.aspx?eqid=" + eqid, "", "dialogHeight:500px; dialogWidth:600px; resizable=yes")
                }
            }
           

        </script>
	</HEAD>
	<body class="tbg"  onload="setref();">
		<form id="form1" method="post" runat="server">
			<div class="FreezePaneOff" id="FreezePane" style="WIDTH: 720px" align="center">
				<div class="InnerFreezePane" id="InnerFreezePane"></div>
			</div>
			<div id="overDiv" style="Z-INDEX: 1000; POSITION: absolute; VISIBILITY: hidden"></div>
			<table id="eqdetdiv" style="POSITION: absolute; TOP: 0px; LEFT: 0px" cellSpacing="0" cellPadding="0"
				width="980">
				<TR>
					<td width="82"></td>
					<td width="175"></td>
					<td width="82"></td>
					<td width="411"></td>
					<td style="width: 150px"></td>
					<td style="width: 80px"></td>
				</TR>
				<tr>
					<td colSpan="2">&nbsp;</td>
					<td align="right" class="bluelabel" colSpan="2" id="tdtrans" runat="server"><input type="checkbox" id="cbtrans" runat="server" onclick="gettrans();" NAME="cbtrans"><asp:Label id="lang2466" runat="server">Submit For Translation</asp:Label></td>
					<td colSpan="2" align="right">
						<asp:imagebutton id="ibtnsaveneweq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="ibtnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"></asp:imagebutton></td>
					</TD>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2467" runat="server">Equipment#</asp:Label></td>
					<td><asp:textbox id="txteqname" runat="server" Width="120px" MaxLength="50"></asp:textbox><IMG onclick="GetPartDiv();" src="../images/appbuttons/minibuttons/spareparts.gif" class="details">
                    <img onclick="getmeter();" onmouseover="return overlib('Add\Edit Meters for this Asset', ABOVE, LEFT)"
                    onmouseout="return nd()" src="../images/appbuttons/minibuttons/meter1.gif">
                    <img onclick="getxtra();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
                    border="0" id="imgxtra" runat="server" />
                    </td>
					<td class="redlabel">ECR:</td>
					<td><asp:textbox id="txtecr" runat="server" style="width: 48px" Font-Size="9pt"></asp:textbox>&nbsp;<IMG id="btnecr" title="Calculate ECR" onclick="getcalc();" height="20" alt="" src="../images/appbuttons/minibuttons/ecrbutton.gif"
							style="width: 26px" align="absMiddle" runat="server"></td>
					<td align="center" colSpan="2" rowSpan="10"><A onclick="getbig();" href="#"><IMG id="imgeq" height="216" src="../images/appimages/eqimg1.gif" style="width: 216px" border="0"
								runat="server"></A></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2468" runat="server">Description</asp:Label></td>
					<td colSpan="3"><asp:textbox id="txteqdesc" runat="server" Width="357px" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label" style="HEIGHT: 23px"><asp:Label id="lang2469" runat="server">Spl Identifier</asp:Label></td>
					<td colSpan="3"><asp:textbox id="txtspl" runat="server" Width="357px" MaxLength="250"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2470" runat="server">Location</asp:Label></td>
					<td colSpan="3"><asp:textbox id="txtloc" runat="server" Width="358px" MaxLength="50" Font-Size="9pt"></asp:textbox></td>
				</tr>
                <tr>
                    <td class="label">Column</td>
                    <td class="plainlabel" id="tdcol">
                <asp:TextBox ID="txtfpc" runat="server" style="width: 150px" MaxLength="50" CssClass="plainlabel"></asp:TextBox>
            </td>
                    </tr>
				<tr>
					<td class="label">OEM</td>
					<td><asp:DropDownList ID="ddoem" runat="server" style="width: 150px" CssClass="plainlabel">
                    </asp:DropDownList><asp:textbox id="txtoem" runat="server" style="width: 150px" MaxLength="50" CssClass="plainlabel"></asp:textbox></td>
					<td class="label"><asp:Label id="lang2471" runat="server">Model#/Type</asp:Label></td>
					<td><asp:textbox id="txtmod" runat="server" style="width: 150px" MaxLength="50"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2472" runat="server">Serial#</asp:Label></td>
					<td><asp:textbox id="txtser" runat="server" style="width: 150px" MaxLength="50"></asp:textbox></td>
					<td class="label"><asp:Label id="lang2473" runat="server">Status</asp:Label></td>
					<td><asp:dropdownlist id="ddlstat" runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2474" runat="server">Asset Class</asp:Label></td>
					<td align="left" colSpan="3"><asp:dropdownlist id="ddac" runat="server">
							<asp:ListItem Value="Select Asset Class">Select Asset Class</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr height="20">
					<td class="bluelabel"><asp:Label id="lang2475" runat="server">Charge#</asp:Label></td>
					<td colSpan="3"><asp:textbox id="txtcharge" runat="server" Width="165px" ReadOnly="True"></asp:textbox><IMG onclick="getcharge('wo');" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"
							border="0"></td>
				</tr>
				<tr height="20">
					<td class="label" id="Td8" runat="server"><asp:Label id="lang2476" runat="server">Created By</asp:Label></td>
					<td class="plainlabel" id="tdcby" runat="server"></td>
					<td class="label"><asp:Label id="lang2477" runat="server">Create Date</asp:Label></td>
					<td class="plainlabel" id="tdcdate" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label" id="Td2" runat="server"><asp:Label id="lang2478" runat="server">Phone</asp:Label></td>
					<td class="plainlabel" id="Td3" runat="server"></td>
					<td class="label"><asp:Label id="lang2479" runat="server">Email Address</asp:Label></td>
					<td class="plainlabel" id="Td4" runat="server"></td>
				</tr>
				<tr height="20">
					<td class="label" id="Td1" runat="server"><asp:Label id="lang2480" runat="server">Modified By</asp:Label></td>
					<td class="plainlabel" id="tdmby" runat="server"></td>
					<td class="label"><asp:Label id="lang2481" runat="server">Modified Date</asp:Label></td>
					<td class="plainlabel" id="tdmdate" runat="server"></td>
					<td align="center" colSpan="2">
						<table>
							<tr>
								<td class="bluelabel" style="width: 80px"><asp:Label id="lang2482" runat="server">Order</asp:Label></td>
								<td style="width: 60px"><asp:textbox id="txtiorder" runat="server" style="width: 40px" CssClass="plainlabel"></asp:textbox></td>
								<td style="width: 20px"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
								<td style="width: 20px"><IMG onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)" onclick="addpic();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif"></td>
								<td style="width: 20px"><IMG id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/del.gif" runat="server"></td>
								<td style="width: 20px"><IMG id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)" onclick="savdets();"
										onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif" runat="server">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="20">
					<td class="label" id="Td5" runat="server"><asp:Label id="lang2483" runat="server">Phone</asp:Label></td>
					<td class="plainlabel" runat="server" ID="Td6" NAME="Td6">&nbsp;</td>
					<td class="label"><asp:Label id="lang2484" runat="server">Email Address</asp:Label></td>
					<td class="plainlabel" id="Td7" runat="server">&nbsp;</td>
					<td align="center" colSpan="2">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="134"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			
			<input id="lblclid" type="hidden" name="lblclid" runat="server"><input id="lbldept" type="hidden" name="lbldept" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"><input id="lblsid" type="hidden" name="lblsid" runat="server">
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblpar" type="hidden" name="lblpar" runat="server"><input id="lbldchk" type="hidden" name="lbldchk" runat="server">
			<input id="lblecrflg" type="hidden" name="lblecrflg" runat="server"><input id="lblpic" type="hidden" name="lblpic" runat="server">
			<input id="lblpchk" type="hidden" runat="server" NAME="lblpchk"><input id="lbluser" type="hidden" runat="server" NAME="lbluser">
			<input id="appchk" type="hidden" name="appchk" runat="server"><input id="lblpareqid" type="hidden" runat="server" NAME="lblpareqid">
			<input id="lbladdchk" type="hidden" runat="server" NAME="lbladdchk"><input id="lbllog" type="hidden" runat="server" NAME="lbllog">
			<input id="lbllock" type="hidden" runat="server" NAME="lbllock"><input id="lbllockedby" type="hidden" runat="server" NAME="lbllockedby">
			<input id="lbllockchk" type="hidden" runat="server" NAME="lbllockchk"><input id="lbldb" type="hidden" runat="server" NAME="lbldb">
			<input id="lbllid" type="hidden" runat="server" NAME="lbllid"><input id="lbltyp" type="hidden" runat="server" NAME="lbltyp">
			<input id="lblro" type="hidden" runat="server" NAME="lblro"><input id="lblappstr" type="hidden" runat="server" NAME="lblappstr">
			<input id="lblapp" type="hidden" runat="server" NAME="lblapp"><input id="lbldel" type="hidden" runat="server" NAME="lbldel">
			<input id="lblpcnt" type="hidden" runat="server" NAME="lblpcnt"> <input id="lblcurrp" type="hidden" runat="server" NAME="lblcurrp">
			<input id="lblimgs" type="hidden" name="lblimgs" runat="server"> <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
			<input id="lblovimgs" type="hidden" name="lblovimgs" runat="server"> <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
			<input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server"> <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
			<input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"> <input type="hidden" id="lblfslang" runat="server" />
            <input type="hidden" id="lbleqnum" runat="server" />
		</form>
	</body>
</HTML>
