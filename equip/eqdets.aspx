<%@ Page Language="vb" AutoEventWireup="false" Codebehind="eqdets.aspx.vb" Inherits="lucy_r12.eqdets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>eqdets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="600">
				<tr>
					<td class="label" style="width: 100px"><asp:Label id="lang2272" runat="server">Equipment#</asp:Label></td>
					<td id="tdcureq" runat="server" class="plainlabel" width="200"></td>
					<td class="redlabel" style="width: 100px">ECR:</td>
					<td class="plainlabel" id="tdecr" runat="server" width="200"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2273" runat="server">Description</asp:Label></td>
					<td colSpan="3" class="plainlabel" id="tddesc" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2274" runat="server">Spl Identifier</asp:Label></td>
					<td colSpan="3" id="tdspl" runat="server" class="plainlabel"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2275" runat="server">Location</asp:Label></td>
					<td colSpan="3" id="tdloc" runat="server" class="plainlabel"></td>
				</tr>
				<tr>
					<td class="label">OEM</td>
					<td id="tdoem" runat="server" class="plainlabel"></td>
					<td class="label"><asp:Label id="lang2276" runat="server">Model#/Type</asp:Label></td>
					<td id="tdmod" runat="server" class="plainlabel"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2277" runat="server">Serial#</asp:Label></td>
					<td class="plainlabel" id="tdsn" runat="server"></td>
					<td class="label"></td>
					<td class="plainlabel"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2278" runat="server">Asset Class</asp:Label></td>
					<td align="left" colSpan="3" class="plainlabel" id="tdac" runat="server"></td>
				</tr>
				<tr height="20">
					<td colSpan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="label" id="Td1" runat="server"><asp:Label id="lang2279" runat="server">Created By</asp:Label></td>
					<td class="plainlabel" id="tdcby" runat="server"></td>
					<td class="label"><asp:Label id="lang2280" runat="server">Create Date</asp:Label></td>
					<td class="plainlabel" id="tdcdate" runat="server"></td>
				</tr>
				
			</table>
			<input type="hidden" id="lbleqid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
