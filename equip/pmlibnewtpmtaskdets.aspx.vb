

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmlibnewtpmtaskdets
    Inherits System.Web.UI.Page
    Protected WithEvents lang2513 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2512 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2511 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2510 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2509 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2508 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2507 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2506 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2505 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2504 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2503 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim eqid, db, start, fuid, Login As String
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents remtask As System.Web.UI.WebControls.ImageButton
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtaskrev As System.Web.UI.WebControls.Repeater
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                fuid = Request.QueryString("fuid").ToString
                db = Request.QueryString("db").ToString
                lblfuid.Value = fuid
                lbldb.Value = db
                copy.Open()
                PopTaskRev(fuid)
                copy.Dispose()
            Else
                tdmsg.InnerHtml = "Waiting for data..."
                trmsg.Attributes.Add("class", "view")
            End If

        End If
    End Sub
    Private Sub PopTaskRev(ByVal funcid As String)
        Dim db As String = lbldb.Value
        Dim lang As String = lblfslang.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        db = mdbarr(2).ToString
        Dim tcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & db & "].[dbo].[pmtaskstpm] where funcid = '" & funcid & "'"
        tcnt = copy.Scalar(sql)
        If tcnt = 0 Then
            tdmsg.InnerHtml = "No Records Found"
            trmsg.Attributes.Add("class", "view")
        Else
            trmsg.Attributes.Add("class", "details")
        End If
        sql = "usp_getTaskReviewtpm '" & funcid & "', '" & db & "','" & srvr & "', '" & lang & "'"
        dr = copy.GetRdrData(sql)
        rptrtaskrev.DataSource = dr
        rptrtaskrev.DataBind()
        dr.Close()
    End Sub

    Private Sub remtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles remtask.Click
        Dim cb As CheckBox
        Dim fi As String = lblfuid.Value
        Dim tsknum, subnum As String
        copy.Open()
        For Each i As RepeaterItem In rptrtaskrev.Items
            cb = CType(i.FindControl("cb4"), CheckBox)
            If cb.Checked Then
                tsknum = CType(i.FindControl("lbltasknum"), Label).Text
                subnum = CType(i.FindControl("lblsubtasknum"), Label).Text
                If subnum = "0" Then
                    sql = "usp_delTPMTask '" & fi & "', '" & tsknum & "', '" & subnum & "'"
                Else
                    sql = "usp_delTPMSubTask '" & fi & "', '" & tsknum & "', '" & subnum & "'"
                End If
                copy.Update(sql)
            End If
        Next

        PopTaskRev(fi)
        copy.Dispose()
    End Sub




    Private Sub rptrtaskrev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtaskrev.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2503 As Label
                lang2503 = CType(e.Item.FindControl("lang2503"), Label)
                lang2503.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2503")
            Catch ex As Exception
            End Try
            Try
                Dim lang2504 As Label
                lang2504 = CType(e.Item.FindControl("lang2504"), Label)
                lang2504.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2504")
            Catch ex As Exception
            End Try
            Try
                Dim lang2505 As Label
                lang2505 = CType(e.Item.FindControl("lang2505"), Label)
                lang2505.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2505")
            Catch ex As Exception
            End Try
            Try
                Dim lang2506 As Label
                lang2506 = CType(e.Item.FindControl("lang2506"), Label)
                lang2506.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2506")
            Catch ex As Exception
            End Try
            Try
                Dim lang2507 As Label
                lang2507 = CType(e.Item.FindControl("lang2507"), Label)
                lang2507.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2507")
            Catch ex As Exception
            End Try
            Try
                Dim lang2508 As Label
                lang2508 = CType(e.Item.FindControl("lang2508"), Label)
                lang2508.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2508")
            Catch ex As Exception
            End Try
            Try
                Dim lang2509 As Label
                lang2509 = CType(e.Item.FindControl("lang2509"), Label)
                lang2509.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2509")
            Catch ex As Exception
            End Try
            Try
                Dim lang2510 As Label
                lang2510 = CType(e.Item.FindControl("lang2510"), Label)
                lang2510.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2510")
            Catch ex As Exception
            End Try
            Try
                Dim lang2511 As Label
                lang2511 = CType(e.Item.FindControl("lang2511"), Label)
                lang2511.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2511")
            Catch ex As Exception
            End Try
            Try
                Dim lang2512 As Label
                lang2512 = CType(e.Item.FindControl("lang2512"), Label)
                lang2512.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2512")
            Catch ex As Exception
            End Try
            Try
                Dim lang2513 As Label
                lang2513 = CType(e.Item.FindControl("lang2513"), Label)
                lang2513.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2513")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2503.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2503")
        Catch ex As Exception
        End Try
        Try
            lang2504.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2504")
        Catch ex As Exception
        End Try
        Try
            lang2505.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2505")
        Catch ex As Exception
        End Try
        Try
            lang2506.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2506")
        Catch ex As Exception
        End Try
        Try
            lang2507.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2507")
        Catch ex As Exception
        End Try
        Try
            lang2508.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2508")
        Catch ex As Exception
        End Try
        Try
            lang2509.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2509")
        Catch ex As Exception
        End Try
        Try
            lang2510.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2510")
        Catch ex As Exception
        End Try
        Try
            lang2511.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2511")
        Catch ex As Exception
        End Try
        Try
            lang2512.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2512")
        Catch ex As Exception
        End Try
        Try
            lang2513.Text = axlabs.GetASPXPage("pmlibnewtpmtaskdets.aspx", "lang2513")
        Catch ex As Exception
        End Try

    End Sub

End Class
