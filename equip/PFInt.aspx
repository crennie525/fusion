<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PFInt.aspx.vb" Inherits="lucy_r12.PFInt" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>PFInt</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		<script  src="../scripts1/PFIntaspx.js"></script>
		<script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script  type="text/javascript">
            function calc2() {
                var rs = document.getElementById("lbans1").innerHTML;
                var sf = document.getElementById("ddsf").value;
                if (parseInt(sf) != 0) {
                    var ans1 = Math.round(parseInt(rs) / parseInt(sf));
                    if (isNaN(ans1)) {
                        alert("Interval is not a Number")
                    }
                    else {
                        document.getElementById("txtri").value = ans1;
                        document.getElementById("lblpfi").value = ans1;
                    }
                }
                else {
                    alert("Please Select a Safety Factor")
                }
            }
            function handleexit() {
                var log = document.getElementById("lbllog").value;
                if (log == "no" || log == "noeqid") setref();
                else window.setTimeout("setref();", 1205000);
                var ecrflag = document.getElementById("ecrflag").value;
                if (ecrflag == "1") updateri();
                var fin = document.getElementById("txtfin").value;
                if (fin == "1") {
                    var pfi = document.getElementById("lblpfi").value;
                    //alert(pfi)
                    window.parent.handleexit(pfi);
                }
            }
        </script>
	</HEAD>
	<body onload="handleexit();" >
		<form id="form1" method="post" runat="server">
			&nbsp;
			<DIV class="pfbg" style="BACKGROUND-IMAGE: url(../images2/eng/pfbg.gif); POSITION: absolute; WIDTH: 629px; HEIGHT: 510px; TOP: 0px; LEFT: 0px"
				><asp:textbox id="txtpfi" style="Z-INDEX: 100; POSITION: absolute; TOP: 320px; LEFT: 320px" runat="server"
					Height="26px" Width="60px"></asp:textbox><asp:label id="lblecr" style="Z-INDEX: 112; POSITION: absolute; TOP: 216px; LEFT: 112px" runat="server"
					CssClass="label"></asp:label><asp:dropdownlist id="ddsf" style="Z-INDEX: 107; POSITION: absolute; TOP: 410px; LEFT: 320px" runat="server">
					<asp:ListItem Value="0">Select</asp:ListItem>
					<asp:ListItem Value="3">3</asp:ListItem>
					<asp:ListItem Value="2">2</asp:ListItem>
					<asp:ListItem Value="1">1</asp:ListItem>
				</asp:dropdownlist><IMG id="c2" onmouseover="return overlib('Calculate Interval', WRAP)" style="Z-INDEX: 106; POSITION: absolute; TOP: 410px; LEFT: 416px; width: 20px;"
					onclick="calc2();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/lilcalc.gif"
					height="20"><IMG id="c1" onmouseover="return overlib('Calculate', WRAP)" style="Z-INDEX: 105; POSITION: absolute; TOP: 352px; LEFT: 416px; width: 20px;"
					onclick="calc1();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/lilcalc.gif"
					height="20">
				<asp:textbox id="txtpfidup" style="Z-INDEX: 104; POSITION: absolute; TOP: 90px; LEFT: 318px"
					runat="server" Height="26px" Width="60px" Enabled="False"></asp:textbox><asp:textbox id="txtltdup" style="Z-INDEX: 103; POSITION: absolute; TOP: 198px; LEFT: 536px"
					runat="server" Height="26px" Width="60px" Enabled="False"></asp:textbox><asp:textbox id="txtri" style="Z-INDEX: 102; POSITION: absolute; TOP: 452px; LEFT: 320px" runat="server"
					Height="26px" Width="60px" ReadOnly="True"></asp:textbox><asp:textbox id="txtlt" style="Z-INDEX: 101; POSITION: absolute; TOP: 346px; LEFT: 320px" runat="server"
					Height="26px" Width="60px"></asp:textbox><asp:imagebutton id="ImageButton1" style="Z-INDEX: 108; POSITION: absolute; TOP: 450px; LEFT: 448px"
					runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"></asp:imagebutton><asp:label id="lbans1" style="Z-INDEX: 109; POSITION: absolute; TOP: 384px; LEFT: 320px" runat="server"
					CssClass="label"></asp:label><asp:label id="Label1" style="Z-INDEX: 111; POSITION: absolute; TOP: 216px; LEFT: 16px" runat="server"
					CssClass="label">Current ECR:</asp:label></DIV>
			<input id="txttaskid" type="hidden" name="txttaskid" runat="server"><input id="txtfin" type="hidden" name="txtfin" runat="server">
			<input id="lblpfi" type="hidden" name="lblpfi" runat="server"><input type="hidden" id="ecrflag" runat="server">
			<input type="hidden" id="lblans1" runat="server"><input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblro" runat="server"> <input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
