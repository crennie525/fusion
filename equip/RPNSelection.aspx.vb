
Public Class RPNSelection
	Inherits Page

#Region " Web Form Designer Generated Code "

	'This call is required by the Web Form Designer.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents hOccuranceRate As System.Web.UI.WebControls.HiddenField
	Protected WithEvents hDetectionProbability As System.Web.UI.WebControls.HiddenField
	Protected WithEvents hQualityImpact As System.Web.UI.WebControls.HiddenField
	Protected WithEvents hDowntimeImpact As System.Web.UI.WebControls.HiddenField
	Protected WithEvents hCostImpact As System.Web.UI.WebControls.HiddenField
	Protected WithEvents hSafetyImpact As System.Web.UI.WebControls.HiddenField
	Protected WithEvents hRPN As System.Web.UI.WebControls.HiddenField
	Protected WithEvents btnSave As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
	End Sub

#End Region

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		If Not Page.IsPostBack Then

			Dim coid = Request.QueryString("coid").ToString

			If coid <> String.Empty Then

				Dim dbUtil As New Utilities
				dbUtil.Open()

				Dim dbReader = dbUtil.GetRdrData("exec usp_GetComponentRPNLevels " & coid)

				If dbReader.Read Then

					hOccuranceRate.Value = If(dbReader("OccuranceRate") Is DBNull.Value, String.Empty, dbReader("OccuranceRate").ToString())
					hDetectionProbability.Value = If(dbReader("DetectionProbability") Is DBNull.Value, String.Empty, dbReader("DetectionProbability").ToString())
					hQualityImpact.Value = If(dbReader("QualityImpact") Is DBNull.Value, String.Empty, dbReader("QualityImpact").ToString())
					hDowntimeImpact.Value = If(dbReader("DowntimeImpact") Is DBNull.Value, String.Empty, dbReader("DowntimeImpact").ToString())
					hCostImpact.Value = If(dbReader("CostImpact") Is DBNull.Value, String.Empty, dbReader("CostImpact").ToString())
					hSafetyImpact.Value = If(dbReader("SafetyImpact") Is DBNull.Value, String.Empty, dbReader("SafetyImpact").ToString())
				End If

				dbReader.Close()
				dbUtil.Dispose()
			End If
		End If
	End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim coid = Request.QueryString("coid").ToString

        If coid <> String.Empty And hOccuranceRate.Value <> String.Empty And hDetectionProbability.Value <> String.Empty And
         hQualityImpact.Value <> String.Empty And hDowntimeImpact.Value <> String.Empty And
         hCostImpact.Value <> String.Empty And hSafetyImpact.Value <> String.Empty And hRPN.Value <> String.Empty Then

            Dim dbUtil As New Utilities
            dbUtil.Open()

            dbUtil.Update("exec usp_SaveComponentRPNLevels " & coid & "," & hOccuranceRate.Value & "," &
             hDetectionProbability.Value & "," & hQualityImpact.Value & "," & hDowntimeImpact.Value & "," &
             hCostImpact.Value & "," & hSafetyImpact.Value & "," & hRPN.Value)

            dbUtil.Dispose()
        End If
    End Sub
End Class
