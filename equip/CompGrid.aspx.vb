

'********************************************************
'*
'********************************************************




Imports System.Data.SqlClient

Public Class CompGrid
    Inherits System.Web.UI.Page
	Protected WithEvents lang2058 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2057 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2056 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2055 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2054 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden


    Dim cid, eqid, se, Login, sql, fuid, chk, ro, isecd, ustr, dept, cell, loc, eq, who, coid As String
    Dim mu As New mmenu_utils_a
    Protected WithEvents dgcomp As System.Web.UI.WebControls.DataGrid
    Protected WithEvents txtco As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfi As System.Web.UI.WebControls.TextBox
    Dim ds, dslev As DataSet
    Dim dr As SqlDataReader

    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblco As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents addfunc As System.Web.UI.WebControls.ImageButton
    Protected WithEvents spy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfud As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtnewcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnreturn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrouting As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewecd1id As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblustr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqidh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim comp As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsort As System.Web.UI.WebControls.Label
    Protected WithEvents ErrorLabel As System.Web.UI.WebControls.Label
    Protected WithEvents xCoord As System.Web.UI.WebControls.TextBox
    Protected WithEvents yCoord As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfu As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Property PostBackSpy() As Integer
        Get
            Return Convert.ToInt32(spy.Value)
        End Get
        Set(ByVal Value As Integer)
            spy.Value = Value.ToString()
        End Set
    End Property
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        GetDGLangs()

        GetFSLangs()

        Try
            lblfslang.value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Dim redir As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
            redir = redir & "?logout=yes"
            Response.Redirect(redir)
        End Try
        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
                dept = Request.QueryString("dept").ToString
                lbldept.Value = dept
                cell = Request.QueryString("cell").ToString
                lblcell.Value = cell
                loc = Request.QueryString("loc").ToString
                lblloc.Value = loc
                eqid = Request.QueryString("eqid").ToString
                lbleqidh.Value = eqid
                eq = Request.QueryString("eq").ToString
                lbleq.Value = eq
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
            Catch ex As Exception

            End Try
            ustr = Request.QueryString("ustr").ToString
            lblustr.Value = ustr
            isecd = mu.ECD
            'lblisecd.Value = isecd
            If isecd = "1" Then
                'lbfailcomp.SelectionMode = ListSelectionMode.Single
                'lbfailmaster.SelectionMode = ListSelectionMode.Single
                'addfunc.Attributes.Add("onclick", "getecd1();")
                addfunc.OnClientClick = "Javascript:return getecd1();"
            End If
            Try
                Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
                Dim sessrefi As Integer = sessref * 1000 * 60
                lblsessrefresh.Value = sessrefi
            Catch ex As Exception
                lblsessrefresh.Value = "300000"
            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgcomp.Columns(0).Visible = False
                addfunc.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                addfunc.Enabled = False
            End If
            Dim sid, did, clid, lid As String
            chk = Request.QueryString("chk").ToString
            lblchk.Value = chk
            cid = Request.QueryString("cid").ToString
            lblcid.Value = cid
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            lid = Request.QueryString("lid").ToString
            lbllid.Value = lid
            lblcurrsort.Value = "crouting asc" '"compnum asc"
            lblsort.Text = "List Order# Ascending"
            Dim user As String = HttpContext.Current.Session("username").ToString
            lbluser.Value = user
            comp.Open()
            BindGrid("crouting asc")
            GetHead(eqid, fuid)
            comp.Dispose()

        End If
        'addfunc.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'addfunc.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'ibtnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub GetHead(ByVal eqid As String, ByVal fuid As String)
        Dim lockedby As String
        sql = "select eqnum, eqdesc, locked, lockedby from equipment where eqid = '" & eqid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdeq.InnerHtml = dr.Item("eqnum").ToString
            tdeqd.InnerHtml = dr.Item("eqdesc").ToString
            lbleqchk.Value = dr.Item("locked").ToString
            lockedby = dr.Item("lockedby").ToString
        End While
        dr.Close()
        sql = "select func, func_desc from functions where func_id = '" & fuid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdfu.InnerHtml = dr.Item("func").ToString
            tdfud.InnerHtml = dr.Item("func_desc").ToString
        End While
        dr.Close()
        If lbleqchk.Value = "1" Then
            If lbluser.Value <> lockedby Then
                addfunc.Attributes.Add("class", "details")
                Dim strMessage As String =  tmod.getmsg("cdstr849" , "CompGrid.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                lbleqchk.Value = "0"
            End If

        End If
    End Sub
    Private Sub BindGrid(ByVal sortExp As String)
        fuid = lblfuid.Value
        sql = "select * from components where func_id = '" & fuid & "' order by " & sortExp
        dr = comp.GetRdrData(sql)
        dgcomp.DataSource = dr
        dgcomp.DataBind()
        dr.Close()
    End Sub

    Private Sub dgcomp_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcomp.EditCommand
        If lbleqchk.Value <> "1" Then
            dgcomp.Columns(3).Visible = True
            Dim coid As String
            coid = CType(e.Item.FindControl("lblcoid"), Label).Text
            lblco.Value = coid
            'dgcomp.Columns(3).Visible = False
            fuid = lblfuid.Value
            se = lblcurrsort.Value
            lblrouting.Value = CType(e.Item.FindControl("lblroute"), Label).Text
            dgcomp.EditItemIndex = e.Item.ItemIndex
            comp.Open()
            BindGrid(se)
            comp.Dispose()
        End If

    End Sub

    Private Sub dgcomp_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcomp.CancelCommand
        dgcomp.EditItemIndex = -1
        se = lblcurrsort.Value
        comp.Open()
        BindGrid(se)
        comp.Dispose()
    End Sub

    Private Sub dgcomp_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcomp.UpdateCommand
        comp.Open()
        cid = lblcid.Value
        eqid = lbleqid.Value
        Dim rte As String = CType(e.Item.FindControl("txtrouting"), TextBox).Text
        Dim orte As String = lblrouting.Value
        Dim rtechk As Long
        Try
            rtechk = System.Convert.ToInt32(rte)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr850" , "CompGrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim com, qty, fun, des, coid, spl, desig As String
        com = CType(e.Item.FindControl("txtcompnum"), TextBox).Text
        com = comp.ModString1(com)

        des = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        des = comp.ModString1(des)

        spl = CType(e.Item.FindControl("txtspl"), TextBox).Text
        spl = comp.ModString1(spl)

        desig = CType(e.Item.FindControl("txtdesig"), TextBox).Text
        desig = comp.ModString1(desig)
        fun = lblfuid.Value
        If Len(com) = 0 Then
            ErrorLabel.Text = "Component# Cannot Be NULL"
        Else
            Dim cpcnt As Integer
            sql = "select count(*) from Components " _
            + "where func_id = '" & fun & "' and compnum = '" & com & "' and compdesc = '" & des & "'"
            cpcnt = 0 'comp.Scalar(sql)
            'If cpcnt > 1 And com <> "New Component" Then
            'ErrorLabel.Text = "Component# not unique within Function"
            If cpcnt = 0 Then
                des = CType(e.Item.FindControl("txtdesc"), TextBox).Text
                coid = CType(e.Item.FindControl("lblcoide"), Label).Text
                sql = "update components set " _
                + "compdesc = '" & des & "', " _
                + "compnum = '" & com & "', " _
                + "spl = '" & spl & "', " _
                + "desig = '" & desig & "' " _
                + "where comid = '" & coid & "'"
                Try
                    comp.Update(sql)
                Catch ex As Exception
                    Dim strMessage As String =  tmod.getmsg("cdstr851" , "CompGrid.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
                If rte <> orte And rte <> "0" Then
                    sql = "usp_reroutecomponents '" & fun & "', '" & rte & "', '" & orte & "'"
                    comp.Update(sql)
                ElseIf rte = "0" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr852" , "CompGrid.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                dgcomp.EditItemIndex = -1
                se = lblcurrsort.Value
                BindGrid(se)
            End If
        End If
        comp.Dispose()
    End Sub

    Private Sub dgcomp_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgcomp.SortCommand
        comp.Open()
        BindGrid(e.SortExpression)
        comp.Dispose()
        Dim se As String = e.SortExpression
        lblcurrsort.Value = se
        Select Case se
            Case "compnum desc"
                dgcomp.Columns(2).SortExpression = "compnum asc"
                lblsort.Text = "Component# Descending"
            Case "compnum asc"
                dgcomp.Columns(2).SortExpression = "compnum desc"
                lblsort.Text = "Component# Ascending"
            Case "routing desc"
                dgcomp.Columns(1).SortExpression = "crouting asc"
                lblsort.Text = "List Order# Descending"
            Case "routing asc"
                dgcomp.Columns(1).SortExpression = "crouting desc"
                lblsort.Text = "List Order# Ascending"
        End Select
    End Sub

    Private Sub addfunc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addfunc.Click
        If Len(txtnewcomp.Text) <> 0 Then
            Dim ecd1id As String = lblnewecd1id.value
            cid = lblcid.Value
            Dim sid As String = lblsid.Value
            eqid = lbleqid.Value
            Dim fuid As String
            fuid = lblfuid.Value
            Dim newcomp As String = txtnewcomp.Text
            newcomp = comp.ModString1(newcomp)
            Dim compcnt As Integer
            sql = "select count(*) from components where compnum = '" & newcomp & "' and func_id = '" & fuid & "'"
            comp.Open()
            compcnt = comp.Scalar(sql)
            If compcnt = 0 Then
                sql = "sp_AddComponent '" & cid & "', '" & fuid & "', '" & newcomp & "', '', '', '','" & ecd1id & "','" & sid & "'"
                comp.Update(sql)
                se = lblcurrsort.Value
                BindGrid(se)
                txtnewcomp.Text = ""
            Else
                Dim strMessage As String = tmod.getmsg("cdstr853", "CompGrid.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            comp.Dispose()
        End If

    End Sub

    Private Sub ibtnreturn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnreturn.Click
        Dim sid, did, clid, eqid, fuid, lid As String
        chk = lblchk.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        lid = lbllid.Value
        ustr = lblustr.Value
        who = lblwho.Value
        dept = lbldept.Value
        cell = lblcell.Value
        loc = lblloc.Value
        coid = lblcoid.Value
        'eqid = lbleqidh.Value
        eq = lbleq.Value
        If chk = "yes" Then
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=fu&start=yes&ret=yes&lid=" & lid & "&chk=yes&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&fuid=" & fuid & "&usrname=" & ustr & "&eq=" & eq & "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&coid=" & coid)
            Else
                Response.Redirect("eqmain2.aspx?lvl=fu&start=yes&ret=yes&lid=" & lid & "&chk=yes&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&fuid=" & fuid & "&ustr=" + ustr)
            End If

        ElseIf chk = "no" Then
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=fu&start=yes&ret=yes&lid=" & lid & "&chk=no&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&fuid=" & fuid & "&usrname=" & ustr & "&eq=" & eq & "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&coid=" & coid)
            Else
                Response.Redirect("eqmain2.aspx?lvl=fu&start=yes&ret=yes&lid=" & lid & "&chk=no&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&fuid=" & fuid & "&ustr=" + ustr)
            End If

        ElseIf chk = "nn" Then
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=fu&start=yes&ret=yes&lid=" & lid & "&chk=nn&cid=" & cid & "&sid=" & sid & "&did=&clid=&eqid=" & eqid & "&fuid=" & fuid & "&usrname=" & ustr & "&eq=" & eq & "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&coid=" & coid)
            Else
                Response.Redirect("eqmain2.aspx?lvl=fu&start=yes&ret=yes&lid=" & lid & "&chk=nn&cid=" & cid & "&sid=" & sid & "&did=&clid=&eqid=" & eqid & "&fuid=" & fuid & "&ustr=" + ustr)
            End If

        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgcomp.Columns(0).HeaderText = dlabs.GetDGPage("CompGrid.aspx", "dgcomp", "0")
        Catch ex As Exception
        End Try
        Try
            dgcomp.Columns(1).HeaderText = dlabs.GetDGPage("CompGrid.aspx", "dgcomp", "1")
        Catch ex As Exception
        End Try
        Try
            dgcomp.Columns(2).HeaderText = dlabs.GetDGPage("CompGrid.aspx", "dgcomp", "2")
        Catch ex As Exception
        End Try
        Try
            dgcomp.Columns(3).HeaderText = dlabs.GetDGPage("CompGrid.aspx", "dgcomp", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label1.Text = axlabs.GetASPXPage("CompGrid.aspx", "Label1")
        Catch ex As Exception
        End Try
        Try
            Label9.Text = axlabs.GetASPXPage("CompGrid.aspx", "Label9")
        Catch ex As Exception
        End Try
        Try
            lang2054.Text = axlabs.GetASPXPage("CompGrid.aspx", "lang2054")
        Catch ex As Exception
        End Try
        Try
            lang2055.Text = axlabs.GetASPXPage("CompGrid.aspx", "lang2055")
        Catch ex As Exception
        End Try
        Try
            lang2056.Text = axlabs.GetASPXPage("CompGrid.aspx", "lang2056")
        Catch ex As Exception
        End Try
        Try
            lang2057.Text = axlabs.GetASPXPage("CompGrid.aspx", "lang2057")
        Catch ex As Exception
        End Try
        Try
            lang2058.Text = axlabs.GetASPXPage("CompGrid.aspx", "lang2058")
        Catch ex As Exception
        End Try
        Try
            lblsort.Text = axlabs.GetASPXPage("CompGrid.aspx", "lblsort")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addfunc.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                addfunc.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                addfunc.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                addfunc.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                addfunc.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
