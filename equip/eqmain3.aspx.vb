Imports System.Data.SqlClient
Imports System.Text
Public Class eqmain3
    Inherits System.Web.UI.Page
    Dim chk, sid, did, clid, cid, ret, app, lid, typ, ro, ustr As String
    Dim filt As String
    Dim sql As String
    Dim main As New Utilities
    Dim dr As SqlDataReader
    Protected WithEvents trdepts As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tddept As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcell As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrettyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trlocs As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdloc2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifeq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iffu As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifco As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifot As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lang2284 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2285 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2286 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2287 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2288 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2289 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2293 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2294 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2295 As System.Web.UI.WebControls.Label
    Protected WithEvents lang2296 As System.Web.UI.WebControls.Label
    Protected WithEvents ifarch As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgototasks As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllvl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblstart As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllochold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tddepts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlocs1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdlocs As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            Dim start As String

            start = Request.QueryString("start").ToString
            lblstart.Value = start
            lblsid.Value = Request.QueryString("sid").ToString
            ustr = Request.QueryString("ustr").ToString
            lbluser.Value = ustr
            If start = "yes" Then
                Dim lvl As String = Request.QueryString("lvl").ToString
                lbllvl.Value = lvl
                If lvl = "eq" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                    lblgototasks.Value = "1"
                ElseIf lvl = "fu" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                    Try
                        lblfuid.Value = Request.QueryString("fuid").ToString
                    Catch ex As Exception
                        lblfuid.Value = Request.QueryString("funid").ToString
                    End Try
                    lblgototasks.Value = "1"
                ElseIf lvl = "co" Then
                    lbleqid.Value = Request.QueryString("eqid").ToString
                    Try
                        lblfuid.Value = Request.QueryString("fuid").ToString
                    Catch ex As Exception
                        lblfuid.Value = Request.QueryString("funid").ToString
                    End Try
                    lblcoid.Value = Request.QueryString("comid").ToString
                    lblgototasks.Value = "1"
                Else
                    lblgototasks.Value = "1"
                End If

                lbllid.Value = Request.QueryString("lid").ToString
                lid = lbllid.Value
                lbldid.Value = Request.QueryString("did").ToString
                did = lbldid.Value
                lblclid.Value = Request.QueryString("clid").ToString
                clid = lblclid.Value
                Dim chk As String = Request.QueryString("chk").ToString
                lblchk.Value = chk

                If did <> "" Then
                    lbldchk.Value = "yes"
                    If lid <> "" And lid <> "0" Then
                        typ = "dloc"
                    Else
                        typ = "reg"
                    End If
                    If clid = "" Or clid = "0" Then
                        lblpar.Value = "cell"
                    Else
                        lblpar.Value = "dept"
                    End If
                Else
                    If lid <> "" And lid <> "0" Then
                        typ = "loc"
                        lbldchk.Value = "no"
                        lblpar.Value = "loc"
                    End If
                End If
                lbltyp.Value = typ
                main.Open()
                If typ <> "loc" Then
                    GetStuff(did, clid)
                Else
                    GetLocStuff(lid)
                End If
                main.Dispose()
            End If
        End If
    End Sub
    Private Sub GetLocStuff(ByVal lid As String)
        sql = "select location from pmlocations where locid = '" & lid & "'"
        Dim loc As String
        dr = main.GetRdrData(sql)
        While dr.Read
            loc = dr.Item("location").ToString
        End While
        dr.Close()
        trlocs.Attributes.Add("class", "view")
        tdloc2.InnerHtml = loc
    End Sub
    Private Sub GetStuff(ByVal did As String, ByVal clid As String)
        If clid = "" Or clid = "0" Then
            sql = "select d.dept_line, 'na' as 'cell_name' from dept d " _
             + "where d.dept_id = '" & did & "'"
        Else
            sql = "select d.dept_line, c.cell_name from dept d left join cells c on c.dept_id = d.dept_id " _
           + "where d.dept_id = '" & did & "' and c.cellid = '" & clid & "'"
        End If
        dr = main.GetRdrData(sql)
        Dim dept, cell As String
        While dr.Read
            dept = dr.Item("dept_line").ToString
            cell = dr.Item("cell_name").ToString
        End While
        dr.Close()
        If dept <> "" Then
            trdepts.Attributes.Add("class", "view")
            tddept.InnerHtml = dept
            lbldept.Value = dept
            If cell <> "na" Then
                tdcell.InnerHtml = cell
                lblcell.Value = cell
            End If
        End If

    End Sub
End Class
