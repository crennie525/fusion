<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddFail.aspx.vb" Inherits="lucy_r12.AddFail" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>AddFail</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    
    <script  src="../scripts1/AddFailaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script  type="text/javascript">
        function DisableButton(b) {
            document.getElementById("btntocomp").className = "details";
            document.getElementById("btnfromcomp").className = "details";
            document.getElementById("todis").className = "view";
            document.getElementById("fromdis").className = "view";
            var ecd2 = document.getElementById("lblcurrecd2").value;
            var chk = document.getElementById("lblisecd").value;
            if (chk == "1") {
                var eReturn = window.showModalDialog("compdivecd3dialog.aspx?ecd2=" + ecd2 + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblnewecd3").value = eReturn;
                    document.getElementById("form1").submit();
                }
            }
            else {
                document.getElementById("form1").submit();
            }
        }
        function showcomp() {
            var chk = document.getElementById("lblswitch").value;
            if (chk == "all") {
                document.getElementById("lblswitch").value = "comp";
                document.getElementById("lbfailmaster").className = "details";
                document.getElementById("lbfailcomp").className = "view";
                document.getElementById("aswitch").innerHTML = "Unassigned Failure Modes";
            }
            else {
                document.getElementById("lblswitch").value = "all";
                document.getElementById("lbfailmaster").className = "view";
                document.getElementById("lbfailcomp").className = "details";
                document.getElementById("aswitch").innerHTML = "Available Failure Modes";
            }
        }
        function showcompret() {
            var chk1 = document.getElementById("lblgetecd2").value;
            if (chk1 == "1") {
                var ecd1 = document.getElementById("lblecd1id").value;
                var ecd2txt = document.getElementById("lblecd2txt").value;
                //alert(ecd1)
                var eReturn = window.showModalDialog("compdivecd2dialog.aspx?ecd1=" + ecd1 + "&ecd2txt=" + ecd2txt + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblgetecd2").value = "";
                    document.getElementById("lblcurrecd2").value = eReturn;
                }
            }

            var chk = document.getElementById("lblswitch").value;
            if (chk == "all") {

                document.getElementById("lbfailmaster").className = "view";
                document.getElementById("lbfailcomp").className = "details";
                document.getElementById("aswitch").innerHTML = "Available Failure Modes";
            }
            else {

                document.getElementById("lbfailmaster").className = "details";
                document.getElementById("lbfailcomp").className = "view";
                document.getElementById("aswitch").innerHTML = "Unassigned Failure Modes";

            }
        }
    </script>
</head>
<body class="tbg" onload="checkit();showcompret();">
    <form id="form1" method="post" runat="server">
    <table width="442">
        <tr>
            <td class="thdrsingrt label" colspan="2">
                <asp:Label ID="lang1912" runat="server">Add/Edit Failure Modes Mini Dialog</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" width="172" height="26">
                <asp:Label ID="lang1913" runat="server">Current Component:</asp:Label>
            </td>
            <td class="labellt" id="tdcomp" width="250" height="26" runat="server">
            </td>
        </tr>
        <tr class="details">
            <td class="label">
                <asp:Label ID="lang1914" runat="server">Add a New Failure Mode</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtnewfail" runat="server" Width="170px" MaxLength="50"></asp:TextBox><asp:ImageButton
                    ID="btnaddfail" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border-bottom: #0000ff 1px solid; border-left: #0000ff 1px solid; border-top: #0000ff 1px solid;
                    border-right: #0000ff 1px solid">
            </td>
        </tr>
    </table>
    <table width="442">
    <tr>
    <td colspan="4">
    <table>
    <tr>
                <td class="label" height="20">Occurrence Area</td>
                <td colspan="3"><asp:DropDownList ID="ddoca" runat="server" AutoPostBack="true">
                    </asp:DropDownList></td>
                    
                </tr>
    </table>
    </td>
    </tr>
        <tr>
            <td class="bluelabel" align="center" width="188">
                <asp:Label ID="aswitch" runat="server">Available Failure Modes</asp:Label>
            </td>
            <td width="22">
                <img src="../images/appbuttons/minibuttons/checkrel.gif" onclick="showcomp();" id="imgchk"
                    runat="server" />
            </td>
            <td width="22">
            </td>
            <td class="label" align="center" width="210">
                <asp:Label ID="lang1916" runat="server">Component Failure Modes</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:ListBox ID="lbfailmaster" runat="server" Width="200px" SelectionMode="Multiple"
                    Height="150px"></asp:ListBox>
                <asp:ListBox ID="lbfailcomp" runat="server" Width="200px" SelectionMode="Multiple"
                    Height="70px" CssClass="details"></asp:ListBox>
            </td>
            <td valign="middle" align="center">
                <img src="../images/appbuttons/minibuttons/forwardgraybg.gif" class="details" id="todis"
                    style="width: 20px" height="20" runat="server">
                <img src="../images/appbuttons/minibuttons/backgraybg.gif" class="details" id="fromdis"
                    style="width: 20px" height="20" runat="server">
                <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                </asp:ImageButton>
                <asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                </asp:ImageButton><img id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
                    onclick="getss();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/plusminus.gif"
                    style="width: 20px" height="20" class="details" runat="server">
            </td>
            <td align="center">
                <asp:ListBox ID="lbfailmodes" runat="server" Width="200px" SelectionMode="Multiple"
                    Height="150px"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="4">
                <img onclick="handleexit();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
                    id="ibtnret" runat="server" width="69" height="19">
            </td>
        </tr>
        <tr>
            <td class="bluelabel" colspan="4">
                <asp:Label ID="lang1917" runat="server">List Box Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" colspan="4">
                <asp:RadioButtonList ID="cbopts" runat="server" Width="400px" AutoPostBack="True"
                    CssClass="labellt">
                    <asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
                    <asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="plainlabelblue" colspan="4">
                <asp:Label ID="lang1918" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Component Failure Modes list if only one item is present.</asp:Label>
            </td>
        </tr>
        <tr>
					<td colSpan="4" class="plainlabelblue">To remove a failure mode that is assigned to an Occurrence Area the Occurrence Area needs to be selected.</td>
				</tr>
    </table>
    <input id="lblcid" type="hidden" runat="server" name="lblcid">
    <input id="lbleqid" type="hidden" runat="server" name="lbleqid">
    <input id="lblfuid" type="hidden" runat="server" name="lblfuid">
    <input id="lblcoid" type="hidden" runat="server" name="lblcoid">
    <input id="lblco" type="hidden" runat="server" name="lblco"><input id="lblapp" type="hidden"
        runat="server" name="lblapp">
    <input type="hidden" id="lblopt" runat="server" name="lblopt"><input type="hidden"
        id="lblfailchk" runat="server">
    <input type="hidden" id="lblsid" runat="server"><input type="hidden" id="lbllog"
        runat="server">
    <input type="hidden" id="lblro" runat="server">
    <input type="hidden" id="lblfailmode" runat="server" />
    <input type="hidden" id="lblswitch" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    <input type="hidden" id="lblisecd" runat="server" />
            <input type="hidden" id="lblgetecd2" runat="server" />
            <input type="hidden" id="lblgetecd3" runat="server" />
            <input type="hidden" id="lblcurrecd2" runat="server" />
            <input type="hidden" id="lblnewecd3" runat="server" />
            <input type="hidden" id="lblecd1id" runat="server" />
            <input type="hidden" id="lblecd2txt" runat="server" />
            <input type="hidden" id="lblcomi" runat="server" />

    </form>
</body>
</html>
