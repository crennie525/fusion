

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class NCGrid
    Inherits System.Web.UI.Page
	Protected WithEvents lang2435 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2434 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2433 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2432 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ds As DataSet
    Dim sql, sqlcnt As String
    Dim eqg As New Utilities
    Dim dept, cell, which As String
    Dim chk, dchk, sid, cid As String
    Dim did, clid, eqid, filt, loc, lid, typ, ro As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtneweq As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnAddEq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater
    Protected WithEvents tduse As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                btnAddEq.Enabled = False
            End If
            Dim user As String = HttpContext.Current.Session("username").ToString
            lbluser.Value = user
            Try
                'typ = Request.QueryString("typ").ToString
                'lbltyp.Value = typ
                Try
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    dept = Request.QueryString("did").ToString
                    lbldid.Value = dept
                    cell = Request.QueryString("clid").ToString
                    lblclid.Value = cell
                Catch ex As Exception
                    lblclid.Value = "na"
                End Try


                If typ = "loc" Or typ = "dloc" Then
                    eqg.Open()
                    LoadEq(dept, cell, lid)
                    eqg.Dispose()
                    'tduse.InnerHtml = "Using Locations"
                Else
                    eqg.Open()
                    LoadEq(dept, cell)
                    eqg.Dispose()
                    lbltyp.Value = "reg"
                    'tduse.InnerHtml = "Using Departments"
                End If
            Catch ex As Exception
                '*** Need to determine why this is loading twice, once without a querystring
            End Try

        Else '***???***
            Try
                dept = Request.QueryString("did").ToString
                lbldid.Value = dept
                Try
                    cell = Request.QueryString("clid").ToString
                    lblclid.Value = cell
                Catch ex As Exception
                    lblclid.Value = "na"
                End Try
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                dchk = Request.QueryString("dchk").ToString
                lbldchk.Value = dchk
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
            Catch ex As Exception

            End Try
        End If
        'btnAddEq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnAddEq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
    End Sub
    Private Sub LoadEq(ByVal dept As String, ByVal cell As String, Optional ByVal loc As String = "0")
        Dim cnt As Integer '= 0
        If loc = "0" Then
            If dept = "na" Then
                sql = "select * from noncritical where ncid = 0"
                cnt = 0
            Else
                cnt = 1
                If cell = "no" Or cell = "" Or Len(cell) = 0 Then
                    sql = "select * from noncritical where dept_id = '" & dept & "'"
                    sqlcnt = "select count(*) from noncritical where dept_id = '" & dept & "'"
                Else
                    sql = "select * from noncritical where dept_id = '" & dept & "' and cellid = '" & cell & "'"
                    sqlcnt = "select count(*) from noncritical where dept_id = '" & dept & "' and cellid = '" & cell & "'"
                End If
            End If
        Else
            cnt = 1
            If dept <> "" Then
                If cell = "no" Or cell = "" Or Len(cell) = 0 Then
                    sql = "select * from noncritical where dept_id = '" & dept & "' and locid = '" & loc & "'"
                    sqlcnt = "select count(*) from noncritical where dept_id = '" & dept & "' and locid = '" & loc & "'"
                Else
                    sql = "select * from noncritical where dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & loc & "'"
                    sqlcnt = "select count(*) from noncritical where dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & loc & "'"
                End If
            Else
                sql = "select * from noncritical where locid = '" & loc & "'"
                sqlcnt = "select count(*) from noncritical where locid = '" & loc & "'"
            End If
        End If

        If cnt <> 0 Then
            cnt = eqg.Scalar(sqlcnt)
        End If
        ds = eqg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptreq.DataSource = dt
        Dim i, eint As Integer
        eint = 15
        'sql = "select count(*) from equipment where deptid = '" & dept & "'"
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptreq.DataBind()

    End Sub

    Private Sub btnAddEq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddEq.Click
        If lbldid.Value <> "na" Or lbllid.Value <> "" Then 'AndAlso lblclid.Value <> "na" Then

            Dim eq As String = txtneweq.Text
            eq = Replace(eq, "'", Chr(180), , , vbTextCompare)
            eq = Replace(eq, "--", "-", , , vbTextCompare)
            eq = Replace(eq, ";", ":", , , vbTextCompare)
            If Len(eq) > 0 Then
                eqg.Open()
                chk = lblchk.Value
                dchk = lbldchk.Value
                sid = lblsid.Value
                cid = lblcid.Value
                lid = lbllid.Value
                'If lid = "" Then lid = "0"
                loc = lbltyp.Value
                Dim user As String = lbluser.Value
                Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
                If loc = "reg" Or loc = "dloc" Then
                    If dchk = "yes" Then
                        did = lbldid.Value
                        If chk = "yes" Then
                            clid = lblclid.Value
                        ElseIf chk = "no" Then
                            CheckDummy("cell")
                            clid = lblclid.Value
                            If clid = "no" Then
                                clid = "0"
                            End If
                        End If

                    Else
                        CheckDummy("dept")
                        did = lbldid.Value
                    End If
                End If

                filt = "where ncnum = '" & eq & "'"
                sql = "select count(*) from noncritical " & filt
                Dim echk As Integer
                echk = eqg.Scalar(sql)
                Dim eqid As Integer
                If echk = 0 Then
                    If loc = "reg" Then
                        sql = "insert into noncritical (compid, ncnum, cellid, siteid, dept_id, loctype) values " _
                        + "('" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & did & "','reg')" _
                        + " select @@identity as 'identity'"
                    ElseIf loc = "loc" Then
                        sql = "insert into noncritical (compid, ncnum, siteid, locid, loctype) values " _
                        + "('" & cid & "', '" & eq & "', '" & sid & "', '" & lid & "','loc')" _
                        + " select @@identity as 'identity'"
                    ElseIf loc = "dloc" Then
                        sql = "insert into noncritical (compid, ncnum, cellid, siteid, locid, dept_id, loctype) values " _
                        + "('" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & lid & "', '" & did & "','dloc')" _
                        + " select @@identity as 'identity'"
                    End If

                    eqid = eqg.Scalar(sql)
                    lbleqid.Value = eqid
                    lbladdchk.Value = "yes"
                   

                    If loc = "loc" Or loc = "dloc" Then
                        LoadEq(did, clid, lid)
                    Else
                        LoadEq(did, clid)
                    End If
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr986" , "NCGrid.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                eqg.Dispose()
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr987" , "NCGrid.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If


        Else
            Dim strMessage As String =  tmod.getmsg("cdstr988" , "NCGrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If


    End Sub
    Private Sub CheckDummy(ByVal field As String)
        sid = lblsid.Value
        cid = lblcid.Value
        Dim cnt As Integer
        If field = "cell" Then
            did = lbldid.Value
            sql = "Select count(*) from cells where " _
            + "siteid = '" & sid & "' and dept_id = '" & did & "' " _
            + "and cell_name = 'No Cells'"
            cnt = eqg.Scalar(sql)
            If cnt = 0 Then
                'sql = "insert into cells (compid, siteid, dept_id, cell_name, cell_desc) values " _
                '+ "('" & cid & "', '" & sid & "', '" & did & "', 'No Cells', 'Not Using Cells') select @@identity as 'identity'"
                'clid = eqg.Scalar(sql)
                lblclid.Value = "0"
            End If
        ElseIf field = "dept" Then
            sql = "Select count(*) from dept where " _
            + "siteid = '" & sid & "' " _
            + "and dept_line = 'No Depts'"
            cnt = eqg.Scalar(sql)
            If cnt = 0 Then
                'sql = "insert into dept (compid, siteid, dept_line, dept_desc) values " _
                '+ "('" & cid & "', '" & sid & "', 'No Depts, 'Not Using Departments') select @@identity as 'identity'"
                'did = eqg.Scalar(sql)
                lbldid.Value = "0"
            End If
        End If
    End Sub

  
    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound

    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang2432 As Label
			lang2432 = CType(e.Item.FindControl("lang2432"), Label)
			lang2432.Text = axlabs.GetASPXPage("NCGrid.aspx","lang2432")
		Catch ex As Exception
		End Try
		Try
                Dim lang2433 As Label
			lang2433 = CType(e.Item.FindControl("lang2433"), Label)
			lang2433.Text = axlabs.GetASPXPage("NCGrid.aspx","lang2433")
		Catch ex As Exception
		End Try
		Try
                Dim lang2434 As Label
			lang2434 = CType(e.Item.FindControl("lang2434"), Label)
			lang2434.Text = axlabs.GetASPXPage("NCGrid.aspx","lang2434")
		Catch ex As Exception
		End Try
		Try
                Dim lang2435 As Label
			lang2435 = CType(e.Item.FindControl("lang2435"), Label)
			lang2435.Text = axlabs.GetASPXPage("NCGrid.aspx","lang2435")
		Catch ex As Exception
		End Try

End If

 End Sub

    Private Sub rptreq_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptreq.ItemCommand
        If e.CommandName = "Select" Then
            did = lbldid.Value
            clid = lblclid.Value
            chk = lblchk.Value
            dchk = lbldchk.Value
            sid = lblsid.Value
            cid = lblcid.Value
            lid = lbllid.Value
            loc = lbltyp.Value
            Try
                eqid = CType(e.Item.FindControl("lbleqiditem"), Label).Text
            Catch ex As Exception
                eqid = CType(e.Item.FindControl("lbleqidalt"), Label).Text
            End Try
            lbleqid.Value = eqid
            Response.Redirect("NCBot.aspx?start=yes&dchk=" & dchk & "&chk=" & chk & "&did=" & did & _
            "&clid=" & clid & "&sid=" & sid & "&cid=" & cid & "&eqid=" & eqid & "&lid=" & lid & "&typ=" & loc)
            'DataBinder.Eval(e.Item.DataItem, "func")
        End If
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2432.Text = axlabs.GetASPXPage("NCGrid.aspx", "lang2432")
        Catch ex As Exception
        End Try
        Try
            lang2433.Text = axlabs.GetASPXPage("NCGrid.aspx", "lang2433")
        Catch ex As Exception
        End Try
        Try
            lang2434.Text = axlabs.GetASPXPage("NCGrid.aspx", "lang2434")
        Catch ex As Exception
        End Try
        Try
            lang2435.Text = axlabs.GetASPXPage("NCGrid.aspx", "lang2435")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnAddEq.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnAddEq.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnAddEq.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnAddEq.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnAddEq.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
