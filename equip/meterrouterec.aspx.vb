﻿Imports System.Data.SqlClient
Public Class meterrouterec
    Inherits System.Web.UI.Page
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim eqid, meterid, meter, munit, enterby, sid, routeid, route, desc As String
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString '
            lblsid.Value = sid
            enterby = Request.QueryString("uid").ToString '
            lblenterby.Value = enterby
            routeid = Request.QueryString("routeid").ToString '
            If routeid = "" Or routeid = "0" Then
                routeid = "0"
                tradd.Attributes.Add("class", "details")
            Else
                tradd.Attributes.Add("class", "view")
            End If
            lblrouteid.Value = routeid
            txtpg.Value = "1"
            appset.Open()
            getdets()
            getroute(PageNumber)
            appset.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                getroute(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                getroute(PageNumber)
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "subroute" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                submitroute()
                getroute(PageNumber)
                lblret.Value = ""
            End If
        End If

    End Sub
    Private Sub submitroute()
        routeid = lblrouteid.Value
        sql = "usp_updatemeterroute '" & routeid & "'"
        appset.Update(sql)
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getroute(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getroute(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getdets()
        Dim scnt, rcnt As String
        scnt = ""
        rcnt = ""
        routeid = lblrouteid.Value
        sql = "select mroute, mdesc, scnt = (select count(*) from meterstops where mrouteid = '" & routeid & "'), " _
            + "rcnt = (select count(*) from meterstops where mrouteid = '" & routeid & "' and lastreading is not null) " _
            + "from meterroutes where mrouteid = '" & routeid & "'"
        dr = appset.GetRdrData(sql)
        While dr.Read
            route = dr.Item("mroute").ToString
            desc = dr.Item("mdesc").ToString
            scnt = dr.Item("scnt").ToString
            rcnt = dr.Item("rcnt").ToString
        End While
        dr.Close()
        lblroute.Value = route
        lbldesc.Value = desc
        lblstopcount.Value = scnt
        lblreccount.Value = rcnt
        tdroute.InnerHtml = route
        tddesc.InnerHtml = desc
        Dim scnti, rcnti As Integer
        Try
            scnti = System.Convert.ToInt32(scnt)
        Catch ex As Exception
            scnti = 0
        End Try
        Try
            rcnti = System.Convert.ToInt32(rcnt)
        Catch ex As Exception
            rcnti = 0
        End Try
        If rcnti <> 0 Then
            If rcnti = scnti Then
                tdsubmit.Attributes.Add("class", "plainlabel")
            End If
        End If
    End Sub
    Private Sub getroute(ByVal PageNumber As Integer)
        Dim intPgNav, intPgCnt As Integer
        routeid = lblrouteid.Value
        sql = "select count(*) from meterstops where mrouteid = '" & routeid & "'"
        intPgCnt = appset.Scalar(sql)
        If intPgCnt = 0 Then
            If routeid <> "0" Then
                'Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'Exit Sub
            End If

        End If
        PageSize = "50"
        intPgNav = appset.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "routeorder"

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        Filter = "mrouteid = ''" & routeid & "''"
        'sql = "select * from meterhist where meterid = '" & meterid & "'"
        sql = "usp_getroutestops_mtr '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        dr = appset.GetRdrData(sql)
        dglist.DataSource = dr
        dglist.DataBind()
        dr.Close()
    End Sub

    Private Sub dglist_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.CancelCommand
        dglist.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        getroute(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dglist_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.EditCommand
        lbloldorder.Value = CType(e.Item.FindControl("lblseq"), Label).Text
        appset.Open()
        dglist.EditItemIndex = e.Item.ItemIndex
        PageNumber = txtpg.Value
        getroute(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dglist_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dglist.ItemDataBound
        If e.Item.ItemType = ListItemType.EditItem Then
            'Imagebutton20
            Dim imgsavi As String = CType(e.Item.FindControl("txtread"), TextBox).ClientID
            Dim imgsav As ImageButton = CType(e.Item.FindControl("Imagebutton20"), ImageButton)
            Dim lastread As String = DataBinder.Eval(e.Item.DataItem, "meterlast").ToString
            imgsav.Attributes("onclick") = "javascript:return " & "checkread('" & lastread & "','" & imgsavi & "');"

        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'Imagebutton19
            Dim imgedit As ImageButton = CType(e.Item.FindControl("Imagebutton19"), ImageButton)
            imgedit.Attributes("onclick") = "javascript:return " & "checkedit();"

        End If
    End Sub

    Private Sub dglist_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.UpdateCommand
        Dim stopid, seq, newdate, routeid As String
        stopid = CType(e.Item.FindControl("lblmstopid"), Label).Text
        seq = CType(e.Item.FindControl("txtread"), TextBox).Text
        If seq = "" Then
            Dim strMessage As String = "Reading Must Be a Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim seqi As Long
        Try
            seqi = System.Convert.ToInt32(seq)
        Catch ex As Exception
            Dim strMessage As String = "Reading Must Be a Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        newdate = lblnewdate.Value
        enterby = lblenterby.Value
        'lblmrouteid
        routeid = CType(e.Item.FindControl("lblmrouteid"), Label).Text
        sql = "update meterstops set lastreading = '" & seq & "', lastdate = '" & newdate & "', lastby = '" & enterby & "', " _
            + "mrouteid = '" & routeid & "' where mstopid = '" & stopid & "'"
        appset.Open()
        appset.Update(sql)
        dglist.EditItemIndex = -1
        PageNumber = txtpg.Value
        getroute(PageNumber)
        getdets()
        appset.Dispose()
    End Sub
End Class