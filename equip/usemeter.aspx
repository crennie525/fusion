﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="usemeter.aspx.vb" Inherits="lucy_r12.usemeter" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script  type="text/javascript">
        function getmeters() {
            var eqid = document.getElementById("lbleqid").value;
            var eqnum = document.getElementById("lbleqnum").value;
            var pmtskid = document.getElementById("lblpmtskid").value;
            var fuid = document.getElementById("lblfuid").value;
            var skillid = document.getElementById("lblskillid").value;
            var skillqty = document.getElementById("lblskillqty").value;
            var rdid = document.getElementById("lblrdid").value;
            var meterid = document.getElementById("lblmeterid").value;
            var meter = document.getElementById("lblmeter").value;
            var mfid = document.getElementById("lblmfid").value;

            var skillido = document.getElementById("lblskillido").value;
            var skillqtyo = document.getElementById("lblskillqtyo").value;
            var rdido = document.getElementById("lblrdido").value;
            var meterido = document.getElementById("lblmeterido").value;
            var metero = document.getElementById("lblmetero").value;
            var mfido = document.getElementById("lblmfido").value;

            var typ = document.getElementById("lbltyp").value;
            //+ "&skillo=" + skillo + "&rdo=" + rdo
            var eReturn = window.showModalDialog("choosemeterdialog.aspx?typ=" + typ + "&eqid=" + eqid + "&eqnum=" + eqnum + "&meterid=" + meterid + "&meter="
            + meter + "&pmtskid=" + pmtskid + "&mfid=" + mfid + "&fuid=" + fuid + "&skillid=" + skillid + "&skillqty=" + skillqty 
            + "&rdid=" + rdid + "&skillido=" + skillido + "&skillqtyo=" + skillqtyo + "&rdido=" + rdido + "&metero=" + metero 
            + "&meterido=" + meterido + "&mfido=" + mfido + "&date=" + Date(), "", "dialogHeight:600px; dialogWidth:600px;resizable=yes");
            if (eReturn) {
                //alert(typ + ", " + eReturn)
                if (typ == "orig") {
                    document.getElementById("lblmeterido").value = eReturn;
                }
                else if (typ == "dad") {
                    var ret = eReturn.split("~");
                    //alert(ret)
                    document.getElementById("lblmeterid").value = ret[0];
                    document.getElementById("lblmeter").value = ret[1];
                    document.getElementById("tdmeter").innerHTML = ret[1];
                    document.getElementById("lblunit").value = ret[2];
                    document.getElementById("lblwkuse").value = ret[3];

                    document.getElementById("tdmeter").innerHTML = ret[1];
                    document.getElementById("tdunits").innerHTML = ret[2];
                    document.getElementById("tdwkuse").innerHTML = ret[3];
                }
                else {
                    document.getElementById("lblmeterid").value = eReturn;
                }
                if (typ != "dad") {
                    document.getElementById("lblsubmit").value = "getmeter";
                    document.getElementById("form1").submit();
                }

            }
             var mid;
            if (typ == "orig") {
                mid = document.getElementById("lblmeterido").value;
            }
            else {
                mid = document.getElementById("lblmeterid").value;
            }
            if (mid != "0") {
                if (mid != "") {
                    document.getElementById("lbldel").value = "";
                }
            }
        }
        function savefreq() {
            var munit = document.getElementById("lblunit").value;
            var maxdays = document.getElementById("txtmax").value;
            var freq = document.getElementById("txtfreq").value;
            var meterid = document.getElementById("lblmeterid").value;
            var meterido = document.getElementById("lblmeterido").value;
            var typ = document.getElementById("lbltyp").value;
            
            if (typ == "orig") {
                if (meterido != "") {
                    if (munit == "CYCLES") {
                        if (maxdays == "") {
                            alert("A Max Days Value is Required for Meters with Units in Cycles")
                        }
                        else if (freq == "") {
                            alert("A Frequency is Required")
                        }
                        else {
                            if (isNaN(freq)) {
                                alert("Frequency Must Be a Non-Decimal Numeric Value")
                            }
                            else if (isNaN(maxdays)) {
                                alert("Max Days Must Be a Non-Decimal Numeric Value")
                            }
                            else {
                                document.getElementById("lblsubmit").value = "savemeter";
                                document.getElementById("form1").submit();
                            }
                        }
                    }
                    else {
                        if (freq == "") {
                            alert("A Frequency is Required")
                        }
                        else {
                            if (isNaN(freq)) {
                                alert("Frequency Must Be a Non-Decimal Numeric Value")
                            }
                            else {
                                document.getElementById("lblsubmit").value = "savemeter";
                                document.getElementById("form1").submit();
                            }
                        }
                    }
                }
            }
            else {
                if (meterid != "") {
                    if (munit == "CYCLES") {
                        if (maxdays == "") {
                            alert("A Max Days Value is Required for Meters with Units in Cycles")
                        }
                        else if (freq == "") {
                            alert("A Frequency is Required")
                        }
                        else {
                            if (isNaN(freq)) {
                                alert("Frequency Must Be a Non-Decimal Numeric Value")
                            }
                            else if (isNaN(maxdays)) {
                                alert("Max Days Must Be a Non-Decimal Numeric Value")
                            }
                            else {
                                document.getElementById("lblsubmit").value = "savemeter";
                                document.getElementById("form1").submit();
                            }
                        }
                    }
                    else {
                        if (freq == "") {
                            alert("A Frequency is Required")
                        }
                        else {
                            if (isNaN(freq)) {
                                alert("Frequency Must Be a Non-Decimal Numeric Value")
                            }
                            else {
                                document.getElementById("lblsubmit").value = "savemeter";
                                document.getElementById("form1").submit();
                            }
                        }
                    }
                }
            }
            
        }
        function goback() {
            var typ = document.getElementById("lbltyp").value;
            //alert(typ)
            if (typ != "dad" && typ != "dadr") {
                window.parent.handleexit();
            }
            else {
                var munit = document.getElementById("lblunit").value;
                var maxdays = document.getElementById("txtmax").value;
                var freq = document.getElementById("txtfreq").value;
                var dayfreq = document.getElementById("lbldayfreq").value;
                var meterid = document.getElementById("lblmeterid").value;
                var meter = document.getElementById("lblmeter").value;
                var del = document.getElementById("lbldel").value;
                var ret = meterid + "~" + freq + "~" + dayfreq + "~" + munit + "~" + maxdays + "~" + meter + "~" + del;
                //alert(ret)
                window.parent.handlereturn(ret);
            }
        }
        function delit() {
            var typ = document.getElementById("lbltyp").value;
            var mid;
            if (typ == "orig") {
                mid = document.getElementById("lblmeterido").value;
            }
            else {
                mid = document.getElementById("lblmeterid").value;
            }
            //alert(mid)
            if (mid != "0") {
                if (mid != "") {
                    var decision = confirm("Are you sure you want to Delete this Meter Frequency Entry?")
                    if (decision == true) {
                        document.getElementById("lblsubmit").value = "del";
                        document.getElementById("form1").submit();
                    }
                    else {
                        alert("Action Cancelled")
                    }
                }
            }

        }
        function checkmeterid() {
            //var meterid = document.getElementById("lblmeterid").value;
            //alert(meterid)
            //onload="checkmeterid();"
        }
        function gobackrest() {
            document.getElementById("txtfreq").value = "";
            document.getElementById("txtmax").value = "";
            document.getElementById("tddayfreq").innerHTML = "";
        }
    </script>
</head>
<body >
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="bluelabel">
                                Choose A Meter
                            </td>
                            <td>
                                <img alt="" src="../images/appbuttons/minibuttons/magnifier.gif" onclick="getmeters();" />
                            </td>
                        </tr>
                        <tr class="details">
                            <td class="bluelabel">
                                Choose an Established Frequency
                            </td>
                            <td>
                                <img alt="" src="../images/appbuttons/minibuttons/magnifier.gif" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="label" style="width: 120px" height="22">
                    Meter
                </td>
                
                <td class="plainlabel" width="190" id="tdmeter" runat="server">
                </td>
                <td width="5">&nbsp;</td>
                <td class="plainlabelblue" width="425" rowspan="11" align="left" valign="middle">
                Frequency should be the required number of Units between PMs.
                <br /><br />
                The Fusion PM Manager will calculate a projected schedule date
                based on the Weekly Use value provided.<br /><br />
                If a Meter Reading Entry exceeds the selected frequency the Fusion PM Manager will automatically update 
                PM Records so they will appear in the current schedule.<br /><br />
                The Fusion PM Manager will not schedule a PM past the Last Date and Max Days value provided.
                <br /><br />
                If a Meter Reading Entry exceeds the selected frequency the Fusion PM Manager will automatically update 
                PM Records so they will appear in the current schedule.
                <br /><br />
                For PM Development and Optimization purposes Fusion calculates a frequency in days based on the above criteria.

                </td>
            </tr>
            <tr>
                <td class="label" height="22">
                    Units
                </td>
                
                <td class="plainlabel" id="tdunits" runat="server">
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="label" height="22">
                    Weekly Use
                </td>
                <td class="plainlabel" id="tdwkuse" runat="server">
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="label" height="22">
                    Function
                </td>
                <td class="plainlabel" id="tdfunc" runat="server">
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="label" height="22">
                    Skill
                </td>
                <td class="plainlabel" id="tdskill" runat="server">
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="label" height="22">
                    Skill Qty
                </td>
                <td class="plainlabel" id="tdqty" runat="server">
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="label" height="22">
                    Running\Down
                </td>
                <td class="plainlabel" id="tdrd" runat="server">
                </td>
                <td></td>
            </tr>
            
            <tr>
                <td class="label" height="22">
                    Frequency
                </td>
                <td class="plainlabel">
                    <asp:TextBox ID="txtfreq" runat="server"></asp:TextBox>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="label" height="22">
                    Max Days
                </td>
                <td class="plainlabel">
                    <asp:TextBox ID="txtmax" runat="server"></asp:TextBox>
                </td>
                <td></td>
            </tr>
             <tr>
                <td class="label" height="22">
                    Frequency in Days
                </td>
                <td class="plainlabel" id="tddayfreq" runat="server">
                </td>
                <td></td>
            </tr>
            <tr>
            <td colspan="2" class="plainlabel" align="right"><a href="#" onclick="savefreq();">Save Changes</a>
            &nbsp;&nbsp;&nbsp;<a href="#" onclick="goback();">Return</a>&nbsp;&nbsp;&nbsp;
            <a href="#" class="linklabelred" onclick="delit();" id="adel" runat="server">Delete</a>&nbsp;&nbsp;&nbsp;<img onclick="gobackrest();" src="../images/appbuttons/minibuttons/refreshit.gif"></td>
            <td></td>
            </tr>
            <tr>
            <td colspan="4" class="plainlabelred" id="tdmsg" runat="server" height="50" valign="middle" align="center">
            
            </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleqnum" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblpmtskid" runat="server" />
    <input type="hidden" id="lblskillid" runat="server" />
    <input type="hidden" id="lblskillqty" runat="server" />
    <input type="hidden" id="lblfuid" runat="server" />
    <input type="hidden" id="lblrdid" runat="server" />
    <input type="hidden" id="lblskill" runat="server" />
    <input type="hidden" id="lblrd" runat="server" />
    <input type="hidden" id="lblfunc" runat="server" />
    <input type="hidden" id="lblmeter" runat="server" />
    <input type="hidden" id="lblwkuse" runat="server" />
    <input type="hidden" id="lblunit" runat="server" />
    <input type="hidden" id="lblfreq" runat="server" />
    <input type="hidden" id="lblmax" runat="server" />
    <input type="hidden" id="lbldayfreq" runat="server" />
    <input type="hidden" id="lblmfid" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblusemetero" runat="server" />
            <input type="hidden" id="lblskillido" runat="server" />
            <input type="hidden" id="lblskillo" runat="server" />
            <input type="hidden" id="lblskillqtyo" runat="server" />
            <input type="hidden" id="lblmeterido" runat="server" />
            <input type="hidden" id="lblrdo" runat="server" />
            <input type="hidden" id="lblrdido" runat="server" />
            <input type="hidden" id="lbltyp" runat="server" />
            <input type="hidden" id="lblfreqo" runat="server" />
    <input type="hidden" id="lblmaxo" runat="server" />
    <input type="hidden" id="lbldayfreqo" runat="server" />
    <input type="hidden" id="lblmfido" runat="server" />
    <input type="hidden" id="lblmetero" runat="server" />
            <input type="hidden" id="lbltottime" runat="server" />
            <input type="hidden" id="lbldowntime" runat="server" />
            <input type="hidden" id="lbldel" runat="server" />
            <input type="hidden" id="lblwho" runat="server" />
            <input type="hidden" id="lblpmtid" runat="server" />
    </form>
</body>
</html>
