

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class FuncDiv
    Inherits System.Web.UI.Page
    Protected WithEvents ovid256 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents ovid255 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang2381 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2380 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2379 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2378 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2377 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2376 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2375 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2374 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2373 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2372 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2371 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2370 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2369 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2368 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2367 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2366 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2365 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim tmod As New transmod
    Dim func As New Utilities
    Dim dr As SqlDataReader
    Dim eq, sql, dt, val, filt, cid, fu, fud, fuid, sid, did, clid, ro, appstr, typ, Login, who, chld As String
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents bt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents bd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btngtt As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunchold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btndel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblparfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td7 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td8 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbladdchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbdev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbopt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lbldel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbtdev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbtopt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents txtiorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgdel As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsavdet As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgcopy As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdgtt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdgttrb As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdopts As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imgfu As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtfuname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnewfunc As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnAddFu As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnsave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        If Request.Form("lblpchk") = "func" Then
            lblpchk.Value = ""
            eq = lbleqid.Value
            fu = lblfuid.Value
            func.Open()
            PopFunc(fu)
            LoadPics()
            func.Dispose()
        ElseIf Request.Form("lblpchk") = "delimg" Then
            lblpchk.Value = ""
            func.Open()
            DelImg()
            LoadPics()
            func.Dispose()
            'lblchksav.Value = "yes"
        ElseIf Request.Form("lblpchk") = "checkpic" Then
            lblpchk.Value = ""
            func.Open()
            LoadPics()
            func.Dispose()
        ElseIf Request.Form("lblpchk") = "savedets" Then
            lblpchk.Value = ""
            func.Open()
            SaveDets()
            LoadPics()
            func.Dispose()
        End If
        If Not IsPostBack Then
            'Try
            Try
                chld = Request.QueryString("chld").ToString
                lblchld.Value = chld
                who = Request.QueryString("who").ToString
                lblwho.Value = who
                If who = "tab" Or who = "noloc" Or who = "eqready" Or who = "locready" Then
                    tdgtt.Attributes.Add("class", "details")
                    tdgttrb.Attributes.Add("class", "details")
                    tdopts.Attributes.Add("width", "744")
                End If
            Catch ex As Exception

            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                btnAddFu.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                btnAddFu.Enabled = False
                btndel.ImageUrl = "../images/appbuttons/minibuttons/deldis.gif"
                btndel.Enabled = False
                'btnupload.Disabled = True
                'cbtrans.Disabled = True
                btnsave.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                btnsave.Attributes.Add("onclick", "")
                'imgproc.Attributes.Add("src", "../images/appbuttons/minibuttons/uploaddis.gif")
                'imgproc.Attributes.Add("onclick", "")
            End If
            appstr = HttpContext.Current.Session("appstr").ToString() '"all" '
            CheckApp(appstr)
            cid = Request.QueryString("cid").ToString
            lblcid.Value = cid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            eq = Request.QueryString("eqid").ToString
            lbleqid.Value = eq
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            lblpar.Value = fuid
            Dim user As String = HttpContext.Current.Session("username").ToString
            lbluser.Value = user
            func.Open()

            PopFunc(fuid)
            LoadPics()
            func.Dispose()
            'Catch ex As Exception

            'End Try

        End If
        btnsave.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov279", "FuncDiv.aspx.vb") & "')")
        btnsave.Attributes.Add("onmouseout", "return nd()")
        btnsave.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
        btndel.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov280", "FuncDiv.aspx.vb") & "')")
        btndel.Attributes.Add("onmouseout", "return nd()")
        'addeditcomp.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov281" , "FuncDiv.aspx.vb") & "')")
        'addeditcomp.Attributes.Add("onmouseout", "return nd()")
        btngtt.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov282", "FuncDiv.aspx.vb") & "')")
        btngtt.Attributes.Add("onmouseout", "return nd()")
        'btnAddFu.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnAddFu.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'btngtt.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yjtotaskssm.gif'")
        'btngtt.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/jtotaskssm.gif'")
    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr954", "FuncDiv.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim old As String = lbloldorder.Value
            Dim neword As String = oldiord 'txtiorder.Text
            fuid = lblfuid.Value
            typ = "fu"
            sql = "usp_reordereqimg '" & typ & "','" & fuid & "','" & neword & "','" & old & "'"
            func.Update(sql)
        End If
    End Sub
    Private Sub DelImg()
        fuid = lblfuid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        typ = "fu"
        sql = "usp_deleqimg '" & typ & "','" & fuid & "','" & pid & "','" & old & "'"
        func.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
        lblcurrp.Value = "0"
    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgfu.Attributes.Add("src", "../images/appimages/funcimg.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgfu.Attributes.Add("src", "../images2/eng/funcimg.gif")
        ElseIf lang = "fre" Then
            imgfu.Attributes.Add("src", "../images2/fre/funcimg.gif")
        ElseIf lang = "ger" Then
            imgfu.Attributes.Add("src", "../images2/ger/funcimg.gif")
        ElseIf lang = "ita" Then
            imgfu.Attributes.Add("src", "../images2/ita/funcimg.gif")
        ElseIf lang = "spa" Then
            imgfu.Attributes.Add("src", "../images2/spa/funcimg.gif")
        End If

        Dim img, imgs, picid As String

        fuid = lblfuid.Value
        Dim pcnt As Integer
        sql = "select count(*) from pmpictures where comid is null and funcid = '" & fuid & "'"
        pcnt = func.Scalar(sql)
        lblpcnt.Value = pcnt

        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from pmpictures p where p.comid is null and p.funcid = '" & fuid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = func.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    'If File.Exists(img) Then
                    imgfu.Attributes.Add("src", img)
                    'End If

                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    txtiorder.Text = order
                End If
        If imgs = "" Then
            imgs = picid & ";" & img
        Else
            imgs += "~" & picid & ";" & img
        End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub
    Private Sub CheckApp(ByVal appstr)
        Dim apparr() As String = appstr.Split(",")
        Dim app As Integer = 0
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "dev" Then
                    rbdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "opt" Then
                    rbopt.Disabled = False
                    rbopt.Checked = True
                    app += 1
                ElseIf apparr(i) = "tpd" Then
                    rbtdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "tpo" Then
                    rbtopt.Disabled = False
                    app += 1
                End If
            Next
            If app = 0 Then
                lblappstr.Value = "no"
            Else
                lblappstr.Value = "yes"
                If rbopt.Disabled = True And rbdev.Disabled = True Then
                    If rbtopt.Disabled = True Then
                        rbtdev.Checked = True
                        lblapp.Value = "rbdev"
                    Else
                        rbtopt.Checked = True
                        lblapp.Value = "rbopt"
                    End If
                Else
                    If rbopt.Disabled = True Then
                        rbdev.Checked = True
                        lblapp.Value = "pmdev"
                    Else
                        rbopt.Checked = True
                        lblapp.Value = "pmopt"
                    End If
                End If
            End If

        Else
            lblappstr.Value = "yes"
            lblapp.Value = "pmopt"
            rbdev.Disabled = False
            rbopt.Disabled = False
            rbopt.Checked = True
            rbtdev.Disabled = False
            rbtopt.Disabled = False
        End If
    End Sub
    Private Sub CheckApp1(ByVal appstr)
        Dim apparr() As String = appstr.Split(",")
        Dim app As Integer = 0
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "dev" Then
                    rbdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "opt" Then
                    rbopt.Disabled = False
                    rbopt.Checked = True
                    app += 1
                End If
            Next
            If app = 0 Then
                lblappstr.Value = "no"
                lblapp.Value = "none"
            Else
                lblappstr.Value = "yes"
                If rbopt.Disabled = True Then
                    rbdev.Checked = True
                    lblapp.Value = "pmdev"
                Else
                    rbopt.Checked = True
                    lblapp.Value = "pmopt"
                End If
            End If

        Else
            lblappstr.Value = "yes"
            rbdev.Disabled = False
            rbopt.Disabled = False
            rbopt.Checked = True
            lblapp.Value = "pmopt"
        End If
    End Sub
    Private Sub PopFunc(ByVal fuid As String)
        GetFuncRecord(fuid)
        If dr.Read Then
            txtfuname.Text = dr.Item("func").ToString
            lblfunchold.Value = dr.Item("func").ToString
            'txtfudesc.Text = dr.Item("func_desc").ToString
            txtspl.Text = dr.Item("spl").ToString
            lblparfuid.Value = dr.Item("origparent").ToString

            Td3.InnerHtml = dr.Item("cph").ToString
            Td4.InnerHtml = dr.Item("cm").ToString
            Td6.InnerHtml = dr.Item("mph").ToString
            Td7.InnerHtml = dr.Item("mm").ToString

            tdcby.InnerHtml = dr.Item("createdby").ToString
            tdcdate.InnerHtml = dr.Item("createdate").ToString
            tdmby.InnerHtml = dr.Item("modifiedby").ToString
            tdmdate.InnerHtml = dr.Item("modifieddate").ToString
        End If
        dr.Close()
        eq = lbleqid.Value
        GetFuncParent(eq)
        If dr.Read Then
            bt.InnerHtml = "Equipment:    " & dr.Item("eqnum").ToString
            bd.InnerHtml = "Description: " & dr.Item("eqdesc").ToString
        End If
        dr.Close()
        Dim taskcnt As Integer
        sql = "select count(*) from pmtasks where funcid = '" & fuid & "'"
        taskcnt = func.Scalar(sql)
        Dim compcnt As Integer
        sql = "select count(*) from components where func_id = '" & fuid & "'"
        taskcnt = func.Scalar(sql)
        btndel.Attributes.Add("onclick", "javascript:return " & _
        "confirm('Are you sure you want to delete Function " & _
        txtfuname.Text & "\nWith " & _
        compcnt & "Component(s) and " & _
        taskcnt & " Task(s)?')")
        sql = "select top 1 picurltm from pmPictures where comid is null and funcid = '" & fuid & "'"
        Dim pic As String
        pic = func.strScalar(sql)
        lblpic.Value = pic
        Dim piccnt As Integer
        sql = "select count(*) from pmpictures where comid is null and funcid = '" & fuid & "'"
        piccnt = func.Scalar(sql)
        'lblcnt.Text = piccnt
        'If Len(pic) <> 0 Then
        'imgfu.Src = pic
        'lblcur.Text = "1"
        'Else
        '    lblcur.Text = "0"
        '    lblcnt.Text = "0"
        'End If
        'Label2.Visible = True
        'Label4.Visible = True
        'po.Visible = True
        'pr.Visible = True
        'ne.Visible = True
        lblpar.Value = "func"
        Dim lock, lockby As String
        Dim user As String = lbluser.Value
        lock = CheckLock(eq)
        If lock = "1" Then
            lockby = lbllockedby.Value
            If lockby = user Then
                lbllock.Value = "0"
            Else
                Dim strMessage As String = tmod.getmsg("cdstr955", "FuncDiv.aspx.vb") & " " & lockby & "."
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                btnsave.Attributes.Add("class", "details")
                'btnupload.Disabled = True
                btndel.Attributes.Add("class", "details")
                imgcopy.Attributes.Add("class", "details")

            End If
        ElseIf lock = "0" Then
            'LockRecord(user, eq)
        End If
    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        chld = lblchld.Value
        Dim lock As String
        If chld = "" Or chld = "0" Then
            sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
            Try
                dr = func.GetRdrData(sql)
                While dr.Read
                    lock = dr.Item("locked").ToString
                    lbllock.Value = dr.Item("locked").ToString
                    lbllockedby.Value = dr.Item("lockedby").ToString
                End While
                dr.Close()
            Catch ex As Exception

            End Try
        Else
            lock = "1"
            lbllock.Value = "1"
            lbllockedby.Value = "PMO Admin"
        End If
        
        Return lock
    End Function
    Private Sub btnAddFu_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddFu.Click
        eq = lbleqid.Value
        cid = lblcid.Value
        fu = txtnewfunc.Text
        fu = func.ModString1(fu)
        If Len(fu) > 0 Then
            Dim user As String = lbluser.Value
            Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
            Dim fudesc As String = "TBD"
            Dim fuspl As String = "TBD"
            Dim fucnt As Integer
            func.Open()
            sql = "select count(*) from functions where eqid = '" & eq & "' " _
            + "and func = '" & fu & "'"
            fucnt = func.Scalar(sql)
            If fucnt = 0 Then
                sql = "usp_addFunc '" & cid & "', '" & eq & "', '" & fu & "', '" & fudesc & "', '" & fuspl & "', '" & ustr & "'"
                fuid = func.Scalar(sql)
                lblfuid.Value = fuid
                lbladdchk.Value = "yes"
                lblpar.Value = "func"
                txtnewfunc.Text = ""
                GetFuncRecord(fuid)
                If dr.Read Then
                    txtfuname.Text = dr.Item("func").ToString
                    'txtfudesc.Text = dr.Item("func_desc").ToString
                    txtspl.Text = dr.Item("spl").ToString
                End If
                dr.Close()
                eq = lbleqid.Value
                GetFuncParent(eq)
                If dr.Read Then
                    bt.InnerHtml = "Equipment:    " & dr.Item("eqnum").ToString
                    bd.InnerHtml = "Description: " & dr.Item("eqdesc").ToString
                End If
                dr.Close()
            Else
                Dim strMessage As String = tmod.getmsg("cdstr956", "FuncDiv.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            func.Dispose()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr957", "FuncDiv.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        Dim eqid As String = lbleqid.Value
        fuid = lblfuid.Value
        fu = txtfuname.Text
        fu = func.ModString1(fu)
        Dim spl As String = txtspl.Text
        spl = func.ModString1(spl)
        Dim hold As String = lblfunchold.Value
        func.Open()
        Dim fcnt As Integer
        sql = "select count(*) from functions where func = '" & fu & "' and eqid = '" & eqid & "'"
        fcnt = func.Scalar(sql)
        Dim user As String = lbluser.Value
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        If fcnt = 0 Or fu = hold Then
            sql = "update functions set func = '" & fu & "', modifiedby = '" & ustr & "', modifieddate = getDate(), " _
                    + "spl = '" & spl & "' where func_id = '" & fuid & "'"
            Try
                func.Update(sql)
            Catch ex As Exception
                Dim strMessage As String = tmod.getmsg("cdstr958", "FuncDiv.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Try
        Else
            GetFuncRecord(fuid)
            If dr.Read Then
                txtfuname.Text = dr.Item("func").ToString
                'txtfudesc.Text = dr.Item("func_desc").ToString
                txtspl.Text = dr.Item("spl").ToString
            End If
            dr.Close()
            Dim strMessage As String = tmod.getmsg("cdstr959", "FuncDiv.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        PopFunc(fuid)
        Dim pic As String = lblpic.Value
        'If Len(pic) <> 0 Then
        'imgfu.Src = pic
        'lblcur.Text = "1"
        'Else
        '    lblcur.Text = "0"
        '    lblcnt.Text = "0"
        'End If
        'Label2.Visible = True
        'Label4.Visible = True
        LoadPics()
        func.Dispose()
    End Sub
    Public Function GetFuncParent(ByVal eq As String) As SqlDataReader
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eq & "'"
        dr = func.GetRdrData(sql)
        Return dr
    End Function
    Public Function GetFuncRecord(ByVal fu As String) As SqlDataReader
        sql = "select f.*, " _
        + "cph = (select s.phonenum from [pmsysusers] s  where  s.username = f.createdby), " _
         + "cm = (select s.email from [pmsysusers] s  where  s.username = f.createdby), " _
         + "mph = (select s.phonenum from [pmsysusers] s  where  s.username = f.modifiedby), " _
        + "mm = (select s.email from [pmsysusers] s  where  s.username = f.modifiedby) " _
        + "from functions f where f.func_id = '" & fu & "'"
        dr = func.GetRdrData(sql)
        Return dr
    End Function

    Private Sub addeditcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cid = lblcid.Value
        eq = lbleqid.Value
        fu = lblfuid.Value
       
    End Sub

    Private Sub btnreturntoeq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cid = lblcid.Value
        eq = lbleqid.Value
        Response.Redirect("EQBot.aspx", True)
    End Sub

    '
    Function ThumbnailCallback() As Boolean
        Return False
    End Function

    Private Sub btndel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btndel.Click
        fuid = lblfuid.Value
        cid = lblcid.Value
        eq = lbleqid.Value
        Dim pic, picn, picm As String
        sql = "select picurl, picurltn, picurltm from pmpictures where funcid = '" & fuid & "'"
        func.Open()
        dr = func.GetRdrData(sql)
        While dr.Read
            pic = dr.Item("picurl").ToString
            picn = dr.Item("picurltn").ToString
            picm = dr.Item("picurltm").ToString
            DelFuImg(pic, picn, picm)
        End While
        dr.Close()
        sql = "usp_delFunc '" & fuid & "'"

        func.Update(sql)
        txtfuname.Text = ""
        'txtfudesc.Text = ""
        txtspl.Text = ""
        bt.InnerHtml = ""
        bd.InnerHtml = ""
        DelImg()
        PopFunc(eq)
        func.Dispose()
        lbldel.Value = "1"
    End Sub
    Private Sub DelFuImg(ByVal pic As String, ByVal picn As String, ByVal picm As String)
        'fuid = lblfuid.Value

        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"

        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2365.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2365")
        Catch ex As Exception
        End Try
        Try
            lang2366.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2366")
        Catch ex As Exception
        End Try
        Try
            lang2367.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2367")
        Catch ex As Exception
        End Try
        Try
            lang2368.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2368")
        Catch ex As Exception
        End Try
        Try
            lang2369.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2369")
        Catch ex As Exception
        End Try
        Try
            lang2370.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2370")
        Catch ex As Exception
        End Try
        Try
            lang2371.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2371")
        Catch ex As Exception
        End Try
        Try
            lang2372.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2372")
        Catch ex As Exception
        End Try
        Try
            lang2373.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2373")
        Catch ex As Exception
        End Try
        Try
            lang2374.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2374")
        Catch ex As Exception
        End Try
        Try
            lang2375.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2375")
        Catch ex As Exception
        End Try
        Try
            lang2376.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2376")
        Catch ex As Exception
        End Try
        Try
            lang2377.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2377")
        Catch ex As Exception
        End Try
        Try
            lang2378.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2378")
        Catch ex As Exception
        End Try
        Try
            lang2379.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2379")
        Catch ex As Exception
        End Try
        Try
            lang2380.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2380")
        Catch ex As Exception
        End Try
        Try
            lang2381.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lang2381")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("FuncDiv.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnAddFu.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnAddFu.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnAddFu.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnAddFu.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnAddFu.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btngtt.Attributes.Add("src", "../images2/eng/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "fre" Then
                btngtt.Attributes.Add("src", "../images2/fre/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "ger" Then
                btngtt.Attributes.Add("src", "../images2/ger/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "ita" Then
                btngtt.Attributes.Add("src", "../images2/ita/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "spa" Then
                btngtt.Attributes.Add("src", "../images2/spa/bgbuttons/jtotaskssm.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgcopy.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("FuncDiv.aspx", "imgcopy") & "')")
            imgcopy.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdel.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("FuncDiv.aspx", "imgdel") & "', ABOVE, LEFT)")
            imgdel.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("FuncDiv.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid255.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("FuncDiv.aspx", "ovid255") & "', ABOVE, LEFT)")
            ovid255.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid256.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("FuncDiv.aspx", "ovid256") & "')")
            ovid256.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
