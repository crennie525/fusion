

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class NCBot
    Inherits System.Web.UI.Page
    Protected WithEvents ovid259 As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang2431 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2430 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2429 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2428 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2427 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2426 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eq, chk, dchk, cid, sid, did, clid, sql, filt, dt, df, val, msg, eqid, lid, typ, loc, ro As String
    Dim dr As SqlDataReader
    Dim echk As Integer
    Dim newstr, strto, saveas As String
    Dim eqr As New Utilities
    Dim Login, usr As String
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents MyFile As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents btnupload As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents btnsave As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtadesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtamod As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtaser As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblpic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtneweq As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnAddEq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtasset As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetImgEq()
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try



        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                btnAddEq.Enabled = False
                btnupload.Disabled = True
                'cbtrans.Disabled = True
                btnsave.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                btnsave.Attributes.Add("onclick", "")
                'imgproc.Attributes.Add("src", "../images/appbuttons/minibuttons/uploaddis.gif")
                'imgproc.Attributes.Add("onclick", "")
            End If
            Dim start As String = Request.QueryString("start").ToString
            If start = "yes" Then
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                dchk = Request.QueryString("dchk").ToString
                lbldchk.Value = dchk
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                If dchk = "yes" Then
                    If chk = "yes" Then
                        did = Request.QueryString("did").ToString
                        lbldept.Value = did
                        clid = Request.QueryString("clid").ToString
                        lblclid.Value = clid
                    ElseIf chk = "no" Then
                        did = Request.QueryString("did").ToString
                        lbldept.Value = did
                    End If
                End If
                eqr.Open()
                PopPage()
                eqr.Dispose()
                'Label2.Text = Request.QueryString.ToString
            End If
        Else
            If Request.Form("lblsubmit") = "save" Then
                lblsubmit.Value = ""
                eqr.Open()
                SavePage()
                PopPage()
                eqr.Dispose()
            End If
        End If
        btnsave.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov283", "NCBot.aspx.vb") & "')")
        btnsave.Attributes.Add("onmouseout", "return nd()")
        btnsave.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
        'btnAddEq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnAddEq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
    End Sub

    Public Function chkcnt(ByVal eqid As String, ByVal eq As String) As Integer
        chk = lblchk.Value
        dchk = lbldchk.Value
        If dchk = "yes" Then
            If chk = "yes" Then
                clid = lblclid.Value
                filt = "where ncnum = '" & eq & "'" ' and cellid = '" & clid & "'"
            ElseIf chk = "no" Then
                did = lbldept.Value
                filt = "where ncnum = '" & eq & "'" ' and dept_id = '" & did & "'"
            End If
        Else
            sid = lblsid.Value
            filt = "where ncnum = '" & eq & "'" ' and siteid = '" & sid & "'"
        End If

        sql = "select count(*) from noncritical " & filt
        echk = eqr.Scalar(sql)

        Return echk
    End Function
    Private Sub SavePage()
        Dim eqid, eq, eqd, loc, oem, model, ser, spl, ecr, ac, acid, lid, oeq As String
        eq = txtasset.Text
        oeq = lbleqnum.Value
        If Len(eq) <> 0 Then
            eq = Replace(eq, "'", Chr(180), , , vbTextCompare)
            eq = Replace(eq, "--", "-", , , vbTextCompare)
            eq = Replace(eq, ";", ":", , , vbTextCompare)
            eqid = lbleqid.Value
            If oeq <> eq Then
                echk = chkcnt(eqid, eq)
            Else
                echk = "0"
            End If

            eqd = txtadesc.Text
            eqd = Replace(eqd, "'", Chr(180), , , vbTextCompare)
            eqd = Replace(eqd, "--", "-", , , vbTextCompare)
            eqd = Replace(eqd, ";", ":", , , vbTextCompare)
            loc = txtaloc.Text
            loc = Replace(loc, "'", Chr(180), , , vbTextCompare)
            loc = Replace(loc, "--", "-", , , vbTextCompare)
            loc = Replace(loc, ";", ":", , , vbTextCompare)
            model = txtamod.Text
            model = Replace(model, "'", Chr(180), , , vbTextCompare)
            model = Replace(model, "--", "-", , , vbTextCompare)
            model = Replace(model, ";", ":", , , vbTextCompare)
            ser = txtaser.Text
            ser = Replace(ser, "'", Chr(180), , , vbTextCompare)
            ser = Replace(ser, "--", "-", , , vbTextCompare)
            ser = Replace(ser, ";", ":", , , vbTextCompare)

            sql = "update noncritical set " _
            + "ncnum = '" & eq & "', " _
            + "ncdesc = '" & eqd & "', " _
            + "location = '" & loc & "', " _
            + "ncmod = '" & model & "', " _
            + "serial = '" & ser & "' " _
            + "where ncid = '" & eqid & "' "
            If echk = "0" Then
                eqr.Update(sql)
            End If

        Else
            Dim strMessage As String = tmod.getmsg("cdstr980", "NCBot.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub PopPage()
        eq = lbleqid.Value
        sql = "select * from " _
        + "noncritical where ncid = '" & eq & "'"
        dr = eqr.GetRdrData(sql)

        If dr.Read Then
            txtasset.Text = dr.Item("ncnum").ToString
            lbleqnum.Value = dr.Item("ncnum").ToString
            txtadesc.Text = dr.Item("ncdesc").ToString
            txtaloc.Text = dr.Item("location").ToString
            txtamod.Text = dr.Item("ncmod").ToString
            txtaser.Text = dr.Item("serial").ToString
            lblpic.Value = dr.Item("picurl").ToString
        End If
        dr.Close()

        Dim pic As String = lblpic.Value
        If pic <> "" Then
            imgeq.Src = pic
        End If

    End Sub

    Private Sub btnAddEq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddEq.Click
        If lbldept.Value <> "na" Or lbllid.Value <> "" Then 'AndAlso lblclid.Value <> "na" Then

            Dim eq As String = txtneweq.Text
            eq = Replace(eq, "'", Chr(180), , , vbTextCompare)
            eq = Replace(eq, "--", "-", , , vbTextCompare)
            eq = Replace(eq, ";", ":", , , vbTextCompare)
            If Len(eq) > 0 Then
                eqr.Open()
                chk = lblchk.Value
                dchk = lbldchk.Value
                sid = lblsid.Value
                cid = lblcid.Value
                lid = lbllid.Value
                'If lid = "" Then lid = "0"
                loc = lbltyp.Value
                If loc = "reg" Or loc = "dloc" Then
                    If dchk = "yes" Then
                        did = lbldept.Value
                        If chk = "yes" Then
                            clid = lblclid.Value
                        ElseIf chk = "no" Then
                            CheckDummy("cell")
                            clid = lblclid.Value
                            If clid = "no" Then
                                clid = "0"
                            End If
                        End If

                    Else
                        CheckDummy("dept")
                        did = lbldept.Value
                    End If
                End If

                filt = "where ncnum = '" & eq & "'"
                sql = "select count(*) from noncritical " & filt
                Dim echk As Integer
                echk = eqr.Scalar(sql)
                Dim eqid As Integer
                If echk = 0 Then
                    If loc = "reg" Then
                        sql = "insert into noncritical (compid, ncnum, cellid, siteid, dept_id, loctype) values " _
                        + "('" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & did & "','reg')" _
                        + " select @@identity as 'identity'"
                    ElseIf loc = "loc" Then
                        sql = "insert into noncritical (compid, ncnum, siteid, locid, loctype) values " _
                        + "('" & cid & "', '" & eq & "', '" & sid & "', '" & lid & "','loc')" _
                        + " select @@identity as 'identity'"
                    ElseIf loc = "dloc" Then
                        sql = "insert into noncritical (compid, ncnum, cellid, siteid, locid, dept_id, loctype) values " _
                        + "('" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & lid & "', '" & did & "','dloc')" _
                        + " select @@identity as 'identity'"
                    End If

                    eqid = eqr.Scalar(sql)
                    lbleqid.Value = eqid
                    PopPage()
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr981", "NCBot.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                eqr.Dispose()
            Else
                Dim strMessage As String = tmod.getmsg("cdstr982", "NCBot.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            Dim strMessage As String = tmod.getmsg("cdstr983", "NCBot.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub CheckDummy(ByVal field As String)
        sid = lblsid.Value
        cid = lblcid.Value
        Dim cnt As Integer
        If field = "cell" Then
            did = lbldept.Value
            sql = "Select count(*) from cells where " _
            + "siteid = '" & sid & "' and dept_id = '" & did & "' " _
            + "and cell_name = 'No Cells'"
            cnt = eqr.Scalar(sql)
            If cnt = 0 Then
                'sql = "insert into cells (compid, siteid, dept_id, cell_name, cell_desc) values " _
                '+ "('" & cid & "', '" & sid & "', '" & did & "', 'No Cells', 'Not Using Cells') select @@identity as 'identity'"
                'clid = eqg.Scalar(sql)
                lblclid.Value = "0"
            End If
        ElseIf field = "dept" Then
            sql = "Select count(*) from dept where " _
            + "siteid = '" & sid & "' " _
            + "and dept_line = 'No Depts'"
            cnt = eqr.Scalar(sql)
            If cnt = 0 Then
                'sql = "insert into dept (compid, siteid, dept_line, dept_desc) values " _
                '+ "('" & cid & "', '" & sid & "', 'No Depts, 'Not Using Departments') select @@identity as 'identity'"
                'did = eqg.Scalar(sql)
                lbldept.Value = "0"
            End If
        End If
    End Sub

    Private Sub btnupload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.ServerClick
        If Not (MyFile.PostedFile Is Nothing) Then
            'Check to make sure we actually have a file to upload
            Dim strLongFilePath As String = MyFile.PostedFile.FileName
            Dim intFileNameLength As Integer = InStr(1, StrReverse(strLongFilePath), "\")
            Dim strFileName As String = Mid(strLongFilePath, (Len(strLongFilePath) - intFileNameLength) + 2)
            eq = lbleqid.Value
            Dim medstr As String = "atm-eqImg" & eq & ".jpg"
            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim strto As String = appstr + "/eqimages/"
            'Dim strto As String = "/eqimages/"
            Dim test As String = Server.MapPath("/") & strto & newstr
            Select Case MyFile.PostedFile.ContentType

                Case "image/pjpeg", "image/jpeg"  'Make sure we are getting a valid JPG image
                    Try
                        If File.Exists(Server.MapPath("\") & strto & medstr) Then
                            File.Delete(Server.MapPath("\") & strto & medstr)
                        End If
                    Catch ex As Exception
                        strto = "/eqimages/"
                        If File.Exists(Server.MapPath("\") & strto & medstr) Then
                            File.Delete(Server.MapPath("\") & strto & medstr)
                        End If
                    End Try
                    Try
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & medstr)
                    Catch ex As Exception
                        strto = "/eqimages/"
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & medstr)
                    End Try

                    'Response.ContentType = "image/jpeg"
                    'Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    Dim fsimg As System.Drawing.Image
                    Try
                        fsimg = System.Drawing.Image.FromFile(Server.MapPath("\") & strto & medstr)
                    Catch ex As Exception
                        strto = "/eqimages/"
                        fsimg = System.Drawing.Image.FromFile(Server.MapPath("\") & strto & medstr)
                    End Try

                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)

                    Dim tmImg As System.Drawing.Image
                    Dim iwm, ihm As Integer
                    iwm = 216
                    ihm = 216
                    tmImg = fsimg.GetThumbnailImage(iwm, ihm, dummyCallBack, IntPtr.Zero)
                    tmImg.Save(Server.MapPath("\") & strto & medstr)
                    Dim savtmstr As String = "../eqimages/" & medstr
                    imgeq.Src = savtmstr
                    eqr.Open()
                    sql = "update noncritical set picurl = '" & savtmstr & "' where ncid = '" & eq & "'"
                    eqr.Update(sql)
                    PopPage()
                    eqr.Dispose()
                Case Else
                    Dim strMessage As String = tmod.getmsg("cdstr984", "NCBot.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Select
        Else
            Dim strMessage As String = tmod.getmsg("cdstr985", "NCBot.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2426.Text = axlabs.GetASPXPage("NCBot.aspx", "lang2426")
        Catch ex As Exception
        End Try
        Try
            lang2427.Text = axlabs.GetASPXPage("NCBot.aspx", "lang2427")
        Catch ex As Exception
        End Try
        Try
            lang2428.Text = axlabs.GetASPXPage("NCBot.aspx", "lang2428")
        Catch ex As Exception
        End Try
        Try
            lang2429.Text = axlabs.GetASPXPage("NCBot.aspx", "lang2429")
        Catch ex As Exception
        End Try
        Try
            lang2430.Text = axlabs.GetASPXPage("NCBot.aspx", "lang2430")
        Catch ex As Exception
        End Try
        Try
            lang2431.Text = axlabs.GetASPXPage("NCBot.aspx", "lang2431")
        Catch ex As Exception
        End Try

    End Sub
    Private Sub GetImgEq()
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/eqimg1.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/eqimg1.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/eqimg1.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/eqimg1.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/eqimg1.gif")
        End If
    End Sub




    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                btnAddEq.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnAddEq.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnAddEq.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnAddEq.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnAddEq.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid259.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("NCBot.aspx", "ovid259") & "')")
            ovid259.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
