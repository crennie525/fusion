﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="meterroutelookmini.aspx.vb" Inherits="lucy_r12.meterroutelookmini" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script  type="text/javascript">
        function getmeter(rid) {
            FreezeScreen('Waiting for data...');
            window.parent.getroute(rid);
        }
        function addmeter() {
            FreezeScreen('Waiting for data...');
            window.parent.addroute();
        }
        function getrec(rid) {
            FreezeScreen('Waiting for data...');
            window.parent.getrec(rid);
        }
        function FreezeScreen(msg) {
            scroll(0, 0);
            var outerPane = document.getElementById('FreezePane');
            var innerPane = document.getElementById('InnerFreezePane');
            if (outerPane) outerPane.className = 'FreezePaneOn';
            if (innerPane) innerPane.innerHTML = msg;
        }
        function getprint(rid) {

            window.parent.getprint(rid);

        }
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="FreezePaneOff" id="FreezePane" align="center">
				<div class="InnerFreezePane" id="InnerFreezePane"></div>
			</div>
    <div>
        <table width="420">
        <tr>
                <td class="thdrsing label" colspan="4">
                    Meter Routes
                </td>
            </tr>
            <tr>
                <td class="label">
                    <asp:Label ID="lang1179" runat="server" style="width: 140px">Search Records</asp:Label>
                </td>
                <td style="width: 140px">
                    <asp:TextBox ID="txtsrch" runat="server"></asp:TextBox>
                </td>
                <td style="width: 50px">
                    <asp:ImageButton ID="btnsrch1" runat="server" Height="20px" Width="20px" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                    </asp:ImageButton>
                </td>
                <td width="570" class="details"><a href="#" onclick="addmeter();">Create a Meter Route</a></td>
            </tr>
            <tr>
                <td id="tdlist" runat="server" colspan="4">
                <asp:DataGrid ID="dglist" runat="server" BackColor="transparent" GridLines="None"
                        CellSpacing="1" AutoGenerateColumns="False">
                        <AlternatingItemStyle CssClass="ptransrowblue" Height="20px"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow" Height="20px"></ItemStyle>
                        <Columns>
                           
                           
                            <asp:TemplateColumn HeaderText="Meter Route">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel" Height="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton CssClass="A1" ID="lblmroutei" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mroute") %>'>
                                    </asp:LinkButton >
                                </ItemTemplate>
                                
                            </asp:TemplateColumn>
                           
                            <asp:TemplateColumn HeaderText="Description">
                                <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lblmeteri" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mdesc") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                               
                            </asp:TemplateColumn>
                            
                             <asp:TemplateColumn HeaderText="Created By" Visible="false">
                                <HeaderStyle Width="180px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label  CssClass="plainlabel" ID="lbluniti" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.createdby") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                               
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Create Date" Visible="false">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbllocrefi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.createdate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                               
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Modified By" Visible="false">
                                <HeaderStyle Width="180px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="Label5a" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.modifiedby") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                               
                            </asp:TemplateColumn>
                              <asp:TemplateColumn HeaderText="Modify Date" Visible="false">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="Label5b" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.modifieddate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                               
                            </asp:TemplateColumn>

                              <asp:TemplateColumn HeaderText="Print" Visible="false">
                                <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                   <img id="imgprint" runat="server" onmouseover="return overlib('Print Route List')" onmouseout="return nd()" src="../images/appbuttons/minibuttons/printx.gif">
                                </ItemTemplate>
                               
                            </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="Record" Visible="false">
                                <HeaderStyle Width="70px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                   <img id="imgrec" runat="server" onmouseover="return overlib('Record Readings for this Route')" onmouseout="return nd()" src="../images/appbuttons/minibuttons/meterrec.gif">
                                </ItemTemplate>
                               
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lblmrouteid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mrouteid") %>'>
                                    </asp:Label>
                                   
                                </ItemTemplate>
                               
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
					<td align="center" colspan="4">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
        </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lblret" runat="server" />
    </div>
    </form>
</body>
</html>
