<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EQCopyMini.aspx.vb" Inherits="lucy_r12.EQCopyMini" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>EQCopyMini</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<script  src="../scripts/gridnav.js"></script>
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  src="../scripts1/EQCopyMiniaspx.js"></script>
        <script  type="text/javascript">
            function checktaskopts(id) {
                var chk = document.getElementById(id);
                if (chk.checked == true) {
                    document.getElementById("cbfunc").checked = true;
                    document.getElementById("cbcomp").checked = true;
                }
            }
            function checkopts(id) {
                var chk = document.getElementById(id);
                if (chk.checked == false) {
                    if (id == "cbfunc") {
                        document.getElementById("cbcomp").checked = false;
                    }
                    document.getElementById("cbpm").checked = false;
                    document.getElementById("cbtpm").checked = false;
                }
            }
        </script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg"  onload="checkeq();">
		<form id="form1" method="post" runat="server">
			<div class="FreezePaneOff" id="FreezePane" align="center">
				<div class="InnerFreezePane" id="InnerFreezePane"></div>
			</div>
			<table id="tbltop" runat="server">
				<tr height="24">
					<td class="thdrsing label"><asp:Label id="lang2202" runat="server">Equipment Copy Mini Dialog</asp:Label></td>
				</tr>
				<tr>
					<td>
						<table id="tbleq" width="620" runat="server">
							<TBODY>
								<tr id="tbleqrec" runat="server">
									<td class="bluelabel" style="width: 80px"><asp:Label id="lang2203" runat="server">Search</asp:Label></td>
									<td width="210"><asp:textbox id="txtsrch" runat="server" Width="200px"></asp:textbox></td>
									<td width="250"><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"
											CssClass="imgbutton"></asp:imagebutton></td>
									<td align="right" style="width: 60px"><IMG alt="" onclick="handlereturn();" src="../images/appbuttons/bgbuttons/return.gif"
											id="ibtnret" runat="server" width="69" height="19"></td>
								</tr>
                                <tr>
                                <td colspan="4" class="plainlabelred" align="center">Please Use the PM Library for Copy Operations Involving Master/Child Records</td>
                                </tr>
								<tr id="treq" runat="server">
									<td colSpan="4" align="center"><div style="WIDTH: 600px; HEIGHT: 220px; OVERFLOW: auto" id="spdiv" onscroll="SetsDivPosition();"><asp:repeater id="rptreq" runat="server">
												<HeaderTemplate>
													<table>
														<tr>
															<td class="btmmenu plainlabel" width="280px"><asp:Label id="lang2204" runat="server">Equipment #</asp:Label></td>
															<td class="btmmenu plainlabel" width="15px"><img src="../images/appbuttons/minibuttons/gridpic.gif"></td>
															<td class="btmmenu plainlabel" width="305px"><asp:Label id="lang2205" runat="server">Description</asp:Label></td>
														</tr>
												</HeaderTemplate>
												<ItemTemplate>
													<tr class="tbg">
														<td class="plainlabel">
															<asp:RadioButton AutoPostBack="True"   OnCheckedChanged="GetEq1" id="rbeq" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' runat="server" />
														</td>
														<td><img src="../images/appbuttons/minibuttons/gridpic.gif" id="imgpic" runat="server"></td>
														<td class="plainlabel">
															<asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>' Runat = server>
															</asp:Label></td>
														<td class="details">
															<asp:Label ID="lblbt" Text='<%# DataBinder.Eval(Container.DataItem,"sitename") + ", " + DataBinder.Eval(Container.DataItem,"dept_line") + ", " + DataBinder.Eval(Container.DataItem,"cell_name")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbleqid" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblloc" Text='<%# DataBinder.Eval(Container.DataItem,"location")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmod" Text='<%# DataBinder.Eval(Container.DataItem,"model")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbloem" Text='<%# DataBinder.Eval(Container.DataItem,"oem")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblser" Text='<%# DataBinder.Eval(Container.DataItem,"serial")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblspl" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblcby" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmby" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblcdate" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblmdate" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblecr" Text='<%# DataBinder.Eval(Container.DataItem,"ecr")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblcph" Text='<%# DataBinder.Eval(Container.DataItem,"cph")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblcm" Text='<%# DataBinder.Eval(Container.DataItem,"cm")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lbleph" Text='<%# DataBinder.Eval(Container.DataItem,"eph")%>' Runat = server>
															</asp:Label></td>
														<td class="details"><asp:Label ID="lblem" Text='<%# DataBinder.Eval(Container.DataItem,"em")%>' Runat = server>
															</asp:Label></td>
                                                            <td class="details"><asp:Label ID="lblass" Text='<%# DataBinder.Eval(Container.DataItem,"assetclass")%>' Runat = server>
															</asp:Label></td>
													</tr>
												</ItemTemplate>
												<FooterTemplate>
						</table>
						</FooterTemplate> </asp:repeater></DIV></td>
				</tr>
				<tr>
					<td align="center" colSpan="4">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="details">
					<td class="bluelabel" id="tdpg" colSpan="2" runat="server"></td>
					<td><asp:imagebutton id="btnprev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton></td>
					<td><asp:imagebutton id="btnnext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton></td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE>
			<table id="tbleqrev" cellSpacing="0" width="620" runat="server">
				<tr>
					<td style="width: 120px"></td>
					<td style="width: 90px"></td>
					<td width="175"></td>
					<td style="width: 120px"></td>
				</tr>
				<tr height="24">
					<td class="thdrsingrt label" colspan="4"><asp:Label id="lang2206" runat="server">Equipment Record Details</asp:Label></td>
				</tr>
				<tr>
					<td class="bluelabel" height="26"><asp:Label id="lang2207" runat="server">New Equipment#</asp:Label></td>
					<td class="plainlabel" height="26" colspan="2"><asp:textbox id="txtneweqnum" runat="server" width="160px" CssClass="plainlabel" MaxLength="50"></asp:textbox></td>
					<td align="right"><asp:imagebutton id="ibtncopy" runat="server" ImageUrl="../images/appbuttons/bgbuttons/copy.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td>
					<td class="label" colspan="2"><input id="cbfunc" onclick="checkopts('cbfunc');" CHECKED type="checkbox" name="cbfunc"
							runat="server"><asp:Label id="lang2208" runat="server">Functions</asp:Label><input id="cbcomp" onclick="checkopts('cbcomp');" CHECKED type="checkbox" name="cbcomp"
							runat="server"><asp:Label id="lang2209" runat="server">Components</asp:Label><input id="cbpm" onclick="checktaskopts('cbpm');" CHECKED type="checkbox" name="cbpm" runat="server"><asp:Label id="lang2210" runat="server">PM Tasks</asp:Label><input id="cbtpm" onclick="checktaskopts('cbtpm');" type="checkbox" name="cbtpm" runat="server"><asp:Label id="lang2211" runat="server">TPM Tasks</asp:Label></td>
					<td class="label" align="center"></td>
					</td>
				</tr>
				<tr>
					<td></td>
					<td class="label" colspan="2"><input type="checkbox" CHECKED id="cbecryn" runat="server" NAME="cbecryn"><asp:Label id="lang2212" runat="server">Copy ECR</asp:Label><input type="checkbox" id="cbrat" CHECKED runat="server" NAME="cbrat"><asp:Label id="lang2213" runat="server">Copy Rationale</asp:Label><input id="rbro" type="radio" name="rbroa" runat="server" VALUE="rbro"><asp:Label id="lang2214" runat="server">Revised Only</asp:Label><input id="rbai" type="radio" CHECKED name="rbroa" runat="server" VALUE="rbai"><asp:Label id="lang2215" runat="server">As Is</asp:Label></td>
				</tr>
			</table>
			<table id="tblrevdetails" cellSpacing="0" width="620" runat="server">
				<TBODY>
					<TR>
						<td style="width: 90px"></td>
						<td width="210"></td>
						<td style="width: 90px"></td>
						<td width="210"></td>
					</TR>
					<tr>
						<td class="label"><asp:Label id="lang2216" runat="server">Belongs to:</asp:Label></td>
						<td class="plainlabel" id="tdbt" colSpan="3" runat="server"></td>
					</tr>
					<tr>
						<td colSpan="4">&nbsp;</td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2217" runat="server">Equipment#</asp:Label></td>
						<td id="tdcureq" runat="server" class="plainlabel"></td>
						<td class="redlabel">ECR:</td>
						<td class="plainlabel" id="tdecr" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2218" runat="server">Description</asp:Label></td>
						<td colSpan="3" class="plainlabel" id="tddesc" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2219" runat="server">Spl Identifier</asp:Label></td>
						<td colSpan="3" id="tdspl" runat="server" class="plainlabel"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2220" runat="server">Location</asp:Label></td>
						<td colSpan="3" id="tdloc" runat="server" class="plainlabel"></td>
					</tr>
					<tr>
						<td class="label">OEM</td>
						<td id="tdoem" runat="server" class="plainlabel"></td>
						<td class="label"><asp:Label id="lang2221" runat="server">Model#/Type</asp:Label></td>
						<td id="tdmod" runat="server" class="plainlabel"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2222" runat="server">Serial#</asp:Label></td>
						<td class="plainlabel" id="tdsn" runat="server"></td>
						<td class="label"></td>
						<td class="plainlabel"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2223" runat="server">Asset Class</asp:Label></td>
						<td align="left" colSpan="3" class="plainlabel" id="tdac" runat="server"></td>
					</tr>
					<tr height="20">
						<td colSpan="4">&nbsp;</td>
					</tr>
					<tr>
						<td class="label" id="Td1" runat="server"><asp:Label id="lang2224" runat="server">Created By</asp:Label></td>
						<td class="plainlabel" id="tdcby" runat="server"></td>
						<td class="label"><asp:Label id="lang2225" runat="server">Create Date</asp:Label></td>
						<td class="plainlabel" id="tdcdate" runat="server"></td>
					</tr>
					<tr>
						<td class="label" id="Td3" runat="server"><asp:Label id="lang2226" runat="server">Phone</asp:Label></td>
						<td class="plainlabel" id="tdcphone" runat="server"></td>
						<td class="label"><asp:Label id="lang2227" runat="server">Email Address</asp:Label></td>
						<td class="plainlabel" id="tdcmail" runat="server"></td>
					</tr>
				</TBODY>
			</table>
			<table class="details" id="tblneweq" cellSpacing="0" width="600" runat="server">
				<TBODY>
					<tr height="24">
						<td colSpan="4" class="thdrsingrt label"><asp:Label id="lang2228" runat="server">New Equipment Record Details</asp:Label></td>
					</tr>
					<TR>
						<td style="width: 90px"></td>
						<td width="210"></td>
						<td style="width: 90px"></td>
						<td width="210"></td>
					</TR>
					<tr>
						<td class="label"><asp:Label id="lang2229" runat="server">Equipment#</asp:Label></td>
						<td id="tdneweq" runat="server" class="plainlabel"></td>
						<td class="redlabel">ECR:</td>
						<td><asp:textbox id="txteqecr" runat="server" style="width: 48px" Font-Size="9pt"></asp:textbox>&nbsp;<IMG class="details" id="btnecr" title="Calculate ECR" onclick="getcalc();" alt="" src="../images/appbuttons/minibuttons/ecrbutton.gif"
								align="absMiddle" runat="server" style="width: 26px" height="20"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2230" runat="server">Description</asp:Label></td>
						<td colSpan="3"><asp:textbox id="txteqdesc" runat="server" Width="357px" MaxLength="100"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label" style="HEIGHT: 23px"><asp:Label id="lang2231" runat="server">Spl Identifier</asp:Label></td>
						<td colSpan="3"><asp:textbox id="txteqspl" runat="server" Width="357px" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2232" runat="server">Location</asp:Label></td>
						<td colSpan="3"><asp:textbox id="txteqloc" runat="server" Width="358px" Font-Size="9pt" MaxLength="50"></asp:textbox></td>
					</tr>
                    <tr>
                    <td class="label">Column</td>
                    <td class="plainlabel" id="tdcol">
                <asp:TextBox ID="txtfpc" runat="server" style="width: 150px" MaxLength="50" CssClass="plainlabel"></asp:TextBox>
            </td>
                    </tr>
					<tr>
						<td class="label">OEM</td>
						<td><asp:DropDownList ID="ddoem" runat="server" style="width: 150px" CssClass="plainlabel">
                    </asp:DropDownList></td>
						<td class="label"><asp:Label id="lang2233" runat="server">Model#/Type</asp:Label></td>
						<td><asp:textbox id="txteqmod" runat="server" style="width: 150px" MaxLength="50"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2234" runat="server">Serial#</asp:Label></td>
						<td><asp:textbox id="txteqsn" runat="server" style="width: 150px" MaxLength="50"></asp:textbox></td>
						<td class="label"><asp:Label id="lang2235" runat="server">Status</asp:Label></td>
						<td><asp:dropdownlist id="ddlstat" runat="server"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2236" runat="server">Asset Class</asp:Label></td>
						<td align="left" colSpan="3"><asp:dropdownlist id="ddac" runat="server">
								<asp:ListItem Value="Select Asset Class">Select Asset Class</asp:ListItem>
							</asp:dropdownlist><IMG class="details" id="btnaddasset" onmouseover="return overlib('Add/Edit Asset Class', LEFT)"
								onclick="getACDiv();" onmouseout="return nd()" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
								style="width: 20px" height="20"></td>
                        
					</tr>
					<tr height="20">
						<td colSpan="4">&nbsp;</td>
					</tr>
					<tr height="20">
						<td class="label" id="Td8" runat="server"><asp:Label id="lang2237" runat="server">Created By</asp:Label></td>
						<td class="plainlabel" id="tdnewcby" runat="server"></td>
						<td class="label"><asp:Label id="lang2238" runat="server">Create Date</asp:Label></td>
						<td class="plainlabel" id="tdnewcbydate" runat="server"></td>
					</tr>
					<tr height="20">
						<td class="label" id="Td2" runat="server"><asp:Label id="lang2239" runat="server">Phone</asp:Label></td>
						<td class="plainlabel" id="tdnewcbyphone" runat="server"></td>
						<td class="label"><asp:Label id="lang2240" runat="server">Email Address</asp:Label></td>
						<td class="plainlabel" id="tdnewcbymail" runat="server"></td>
					</tr>
					<tr>
						<td colspan="4" align="right"><asp:imagebutton id="ibtnsaveneweq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
					</tr>
				</TBODY>
			</table>
			<input id="lblcid" type="hidden" runat="server"><input id="txtpg" type="hidden" runat="server">
			<input id="lbleq" type="hidden" runat="server"><input id="lbllistr" type="hidden" runat="server">
			<input id="lblsid" type="hidden" runat="server"><input id="lbldid" type="hidden" runat="server">
			<input id="lblclid" type="hidden" runat="server"><input type="hidden" id="lblnewid" runat="server">
			<input type="hidden" id="lblcopyflg" runat="server"><input type="hidden" id="lblpsid" runat="server">
			<input type="hidden" id="lbllog" runat="server"> <input type="hidden" id="lbllid" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="Hidden1" type="hidden" name="Hidden1" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"> <input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit">
			<input type="hidden" id="spdivy" runat="server"><input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
