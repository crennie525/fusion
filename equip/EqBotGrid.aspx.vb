

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class EqBotGrid
    Inherits System.Web.UI.Page
	Protected WithEvents lang2170 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2169 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2168 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2167 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
   
    Protected WithEvents txtneweq As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnAddEq As System.Web.UI.WebControls.ImageButton
    Dim tmod As New transmod
    Dim ds As DataSet
    Dim sql, sqlcnt As String
    Dim eqg As New Utilities
    Dim dept, cell, which As String
    Dim chk, dchk, sid, cid, who As String
    Dim did, clid, eqid, filt, loc, lid, typ, ro, Login, start As String
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tduse As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurreqsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tradd As System.Web.UI.HtmlControls.HtmlTableRow
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim app As New AppUtils
        Dim url As String = app.Switch
        If url <> "ok" Then
            appchk.Value = "switch"
        End If
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                'start here
                Try
                    who = Request.QueryString("who").ToString
                    lblwho.Value = who
                Catch ex As Exception

                End Try
                Try
                    Dim user As String = HttpContext.Current.Session("username").ToString
                    lbluser.Value = user
                Catch ex As Exception

                End Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                    btnAddEq.Enabled = False
                End If
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                Try
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    Try
                        loc = Request.QueryString("loc").ToString
                        lblloc.Value = loc
                    Catch exin As Exception

                    End Try
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    dept = Request.QueryString("did").ToString
                    lbldid.Value = dept
                    did = dept
                    cell = Request.QueryString("clid").ToString
                    If cell = "na" Then
                        cell = ""
                    End If
                    lblclid.Value = cell
                    clid = cell
                Catch ex As Exception
                    lblclid.Value = "na"
                End Try
                lblcurreqsort.Value = "eqnum asc"
                If typ = "loc" Or typ = "dloc" Then
                    eqg.Open()
                    LoadEq(dept, cell, lid)
                    eqg.Dispose()
                Else
                    eqg.Open()
                    LoadEq(dept, cell)
                    eqg.Dispose()
                    lbltyp.Value = "reg"
                End If
                If did <> "" Then
                    If lid <> "" And lid <> "0" Then
                        typ = "dloc"
                    Else
                        typ = "reg"
                    End If
                    If clid <> "" And clid <> "0" Then
                        lblpar.Value = "cell"
                        lbldchk.Value = "yes"
                        lblchk.Value = "yes"
                    Else
                        lblpar.Value = "dept"
                        lbldchk.Value = "yes"
                        lblchk.Value = "no"
                    End If
                Else
                    If lid <> "" And lid <> "0" Then
                        typ = "loc"
                        lbldchk.Value = "no"
                        lblpar.Value = "loc"
                    End If
                End If
                'stop here
            Else
                tradd.Attributes.Add("class", "details")
                LoadEq("na", "0", "0")
            End If
        
        End If
        'btnAddEq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnAddEq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
    End Sub
    Public Sub SortEq(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sort As String = lblcurreqsort.Value
        If sort = "eqnum asc" Then
            sort = "eqnum desc"
            'tdcurrsort.InnerHtml = "Equipment# Descending"
        Else
            sort = "eqnum asc"
            'tdcurrsort.InnerHtml = "Equipment# Ascending"
        End If
        lblcurreqsort.Value = sort
        typ = lbltyp.Value
        dept = lbldid.Value
        cell = lblclid.Value
        lid = lbllid.Value
        If typ = "loc" Or typ = "dloc" Then
            eqg.Open()
            LoadEq(dept, cell, lid)
            eqg.Dispose()
            'tduse.InnerHtml = "Using Locations"
        Else
            eqg.Open()
            LoadEq(dept, cell)
            eqg.Dispose()
            lbltyp.Value = "reg"
            'tduse.InnerHtml = "Using Departments"
        End If
    End Sub
    Private Sub LoadEq(ByVal dept As String, ByVal cell As String, Optional ByVal loc As String = "0")
        Dim cnt As Integer '= 0
        Dim sort As String = lblcurreqsort.Value

        If loc = "0" Then
            If dept = "na" Or dept = "" Then
                sql = "select * from equipment where eqid is null" 'deptid = '" & dept & "'"
                cnt = 0 'sqlcnt = "select count(*) from equipment where eqid = 0"
            Else
                cnt = 1
                If cell = "no" Or cell = "" Or Len(cell) = 0 Then
                    sql = "select * from equipment where dept_id = '" & dept & "' order by " & sort
                    sqlcnt = "select count(*) from equipment where dept_id = '" & dept & "'"
                Else
                    sql = "select * from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' order by " & sort
                    sqlcnt = "select count(*) from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "'"
                End If
            End If
        Else
            cnt = 1
            If dept <> "" Then
                If cell = "no" Or cell = "" Or Len(cell) = 0 Then
                    sql = "select * from equipment where dept_id = '" & dept & "' and locid = '" & loc & "' order by " & sort
                    sqlcnt = "select count(*) from equipment where dept_id = '" & dept & "' and locid = '" & loc & "'"
                Else
                    sql = "select * from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & loc & "' order by " & sort
                    sqlcnt = "select count(*) from equipment where dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & loc & "'"
                End If
            Else
                sid = lblsid.Value
                Dim loc2 As String
                loc2 = lblloc.Value
                If loc2 = "" Or Len(loc2) = 0 Then
                    Dim sql1 As String = "select location from pmlocations where locid = '" & loc & "'"
                    loc2 = eqg.strScalar(sql1)
                    lblloc.Value = loc2
                End If
                Dim lcnt As Integer
                If loc2 <> "" Then
                    sql = "select count(*) from pmlocations where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                        + "and type = 'EQUIPMENT'"
                    lcnt = eqg.Scalar(sql)
                    If lcnt > 0 Then
                        sql = "select * from equipment where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                        + "and siteid = '" & sid & "' and dept_id is null order by " & sort
                        sqlcnt = "select count(*) from equipment where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                        + "and siteid = '" & sid & "' and dept_id is null"
                    Else
                        sql = "select * from equipment where locid = '" & loc & "' and siteid = '" & sid & "' and dept_id is null order by " & sort
                        sqlcnt = "select count(*) from equipment where locid = '" & loc & "' and siteid = '" & sid & "' and dept_id is null"
                    End If
                Else
                    sql = "select * from equipment where locid = '" & loc & "' and siteid = '" & sid & "' and dept_id is null order by " & sort
                    sqlcnt = "select count(*) from equipment where locid = '" & loc & "' and siteid = '" & sid & "' and dept_id is null"
                End If




                End If
        End If

        If cnt <> 0 Then
            cnt = eqg.Scalar(sqlcnt)
        End If
        ds = eqg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptreq.DataSource = dt
        Dim i, eint As Integer
        eint = 15
        'sql = "select count(*) from equipment where deptid = '" & dept & "'"
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptreq.DataBind()

    End Sub


    Private Sub rptreq_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptreq.ItemCommand
        If e.CommandName = "Select" Then
            did = lbldid.Value
            clid = lblclid.Value
            chk = lblchk.Value
            dchk = lbldchk.Value
            sid = lblsid.Value
            cid = lblcid.Value
            lid = lbllid.Value
            loc = lbltyp.Value
            Dim locs As String = lblloc.Value
            Try
                eqid = CType(e.Item.FindControl("lbleqiditem"), Label).Text
            Catch ex As Exception
                eqid = CType(e.Item.FindControl("lbleqidalt"), Label).Text
            End Try
            lbleqid.Value = eqid
            Dim strMessage As String = "rptreq"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            who = lblwho.Value
            Response.Redirect("EQBot.aspx?who=" & who & "&start=yes&dchk=" & dchk & "&chk=" & chk & "&did=" & did & _
            "&clid=" & clid & "&sid=" & sid & "&cid=" & cid & "&eqid=" & eqid & "&lid=" & lid & "&typ=" & loc & "&loc=" + locs)
            'DataBinder.Eval(e.Item.DataItem, "func")
        End If
    End Sub

    Private Sub btnAddEq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddEq.Click
        'If lbldid.Value <> "na" Or lbllid.Value <> "" Then 'AndAlso lblclid.Value <> "na" Then
        did = lbldid.Value
        lid = lbllid.Value
        'Dim strMessage0 As String = lbldid.Value
        'Utilities.CreateMessageAlert(Me, strMessage0, "strKey1")
        If (did <> "" Or lid <> "") And (did <> "na" Or lid <> "na") Then
            Dim eq As String = txtneweq.Text
            eq = eqg.ModString3(eq)
            If Len(eq) > 0 Then
                eqg.Open()
                chk = lblchk.Value
                dchk = lbldchk.Value
                sid = lblsid.Value
                cid = lblcid.Value
                lid = lbllid.Value
                
                did = lbldid.Value
                'If lid = "" Then lid = "0"
                loc = lbltyp.Value
                Dim user As String = lbluser.Value
                Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)

                If loc = "reg" Or loc = "dloc" Then
                    If dchk = "yes" Then
                        did = lbldid.Value
                        If chk = "yes" Then
                            clid = lblclid.Value
                        ElseIf chk = "no" Then
                            CheckDummy("cell")
                            clid = lblclid.Value
                            If clid = "no" Then
                                clid = "0"
                            End If
                        End If

                    Else
                        CheckDummy("dept")
                        did = lbldid.Value
                    End If
                End If

                filt = "where eqnum = '" & eq & "' and compid = '" & cid & "'"
                sql = "select count(*) from equipment " & filt
                Dim echk As Integer
                echk = eqg.Scalar(sql)
                If echk = 0 And lid <> "" Then
                    sql = "select count(*) from pmlocations where location = '" & eq & "'"
                    echk = eqg.Scalar(sql)
                End If
                Dim eqid As Integer
                If echk = 0 Then
                    'create procedure addneweq (@compid int, @eqnum varchar(50), @cellid int, @siteid int, @dept_id int,
                    '@lid int, @createdby varchar(50), @lockedby varchar(50), @typ varchar(10))
                    sql = "addneweq '" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & did & "', '" & lid & " ', '" & ustr & "', '" & ustr & "', '" & loc & "'"
                    'If typ = "reg" Then
                    'sql = "insert into equipment (compid, eqnum, cellid, siteid, dept_id, createdby, createdate, locked, lockedby) values " _
                    '+ "('" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & did & "', '" & ustr & "', getDate(), 1, '" & ustr & "')" _
                    '+ " select @@identity as 'identity'"
                    'ElseIf typ = "loc" Then
                    'sql = "insert into equipment (compid, eqnum, siteid, locid, createdby, createdate, locked, lockedby) values " _
                    '+ "('" & cid & "', '" & eq & "', '" & sid & "', '" & lid & "', '" & ustr & "', getDate(), 1, '" & ustr & "')" _
                    '+ " select @@identity as 'identity'"
                    'ElseIf typ = "dloc" Then
                    'sql = "insert into equipment (compid, eqnum, cellid, siteid, locid, dept_id, createdby, createdate, locked, lockedby) values " _
                    '+ "('" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & lid & "', '" & did & " ', '" & ustr & "', getDate(), 1, '" & ustr & "')" _
                    '+ " select @@identity as 'identity'"
                    'End If

                    eqid = eqg.Scalar(sql)
                    lbleqid.Value = eqid
                    lbladdchk.Value = "yes"
                    'sql = "update admintasks set eq = (select count(*) from equipment where compid = '" & cid & "') where cid = '" & cid & "'"
                    'eqg.Update(sql)
                    If loc = "loc" Or loc = "dloc" Then
                        LoadEq(did, clid, lid)
                    Else
                        LoadEq(did, clid)
                    End If

                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr872" , "EqBotGrid.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                eqg.Dispose()
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr873" , "EqBotGrid.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If


        Else
            Dim strMessage As String =  tmod.getmsg("cdstr874" , "EqBotGrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If




    End Sub
    Private Sub CheckDummy(ByVal field As String)
        sid = lblsid.Value
        cid = lblcid.Value
        Dim cnt As Integer
        If field = "cell" Then
            did = lbldid.Value
            sql = "Select count(*) from cells where compid = '" & cid & "' " _
            + "and siteid = '" & sid & "' and dept_id = '" & did & "' " _
            + "and cell_name = 'No Cells'"
            cnt = eqg.Scalar(sql)
            If cnt = 0 Then
                sql = "insert into cells (compid, siteid, dept_id, cell_name, cell_desc) values " _
                + "('" & cid & "', '" & sid & "', '" & did & "', 'No Cells', 'Not Using Cells') select @@identity as 'identity'"
                clid = eqg.Scalar(sql)
                lblclid.Value = clid
            End If
        ElseIf field = "dept" Then
            sql = "Select count(*) from dept where compid = '" & cid & "' " _
            + "and siteid = '" & sid & "' " _
            + "and dept_line = 'No Depts'"
            cnt = eqg.Scalar(sql)
            If cnt = 0 Then
                sql = "insert into dept (compid, siteid, dept_line, dept_desc) values " _
                + "('" & cid & "', '" & sid & "', 'No Depts', 'Not Using Departments') select @@identity as 'identity'"
                did = eqg.Scalar(sql)
                lbldid.Value = did
            End If
        End If
    End Sub
	



    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2167 As Label
                lang2167 = CType(e.Item.FindControl("lang2167"), Label)
                lang2167.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2167")
            Catch ex As Exception
            End Try
            Try
                Dim lang2168 As Label
                lang2168 = CType(e.Item.FindControl("lang2168"), Label)
                lang2168.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2168")
            Catch ex As Exception
            End Try
            Try
                Dim lang2169 As Label
                lang2169 = CType(e.Item.FindControl("lang2169"), Label)
                lang2169.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2169")
            Catch ex As Exception
            End Try
            Try
                Dim lang2170 As Label
                lang2170 = CType(e.Item.FindControl("lang2170"), Label)
                lang2170.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2170")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2167.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2167")
        Catch ex As Exception
        End Try
        Try
            lang2168.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2168")
        Catch ex As Exception
        End Try
        Try
            lang2169.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2169")
        Catch ex As Exception
        End Try
        Try
            lang2170.Text = axlabs.GetASPXPage("EqBotGrid.aspx", "lang2170")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnAddEq.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnAddEq.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnAddEq.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnAddEq.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnAddEq.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
