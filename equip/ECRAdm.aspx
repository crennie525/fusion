<%@ Register TagPrefix="uc1" TagName="mmenu1" Src="../menu/mmenu1.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ECRAdm.aspx.vb" Inherits="lucy_r12.ECRAdm" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>ECRAdm</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/ecradm.js"></script>
		<script  src="../scripts1/ECRAdmaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function CalcTab(id) {
         
             var cd = document.getElementById("lblcid").innerHTML;
             //var idlbl = "f" + id
             //var lbl = document.getElementById(idlbl).innerHTML;
             document.getElementById("lblcatid").value = id;
             closehref();

             var lbl = document.getElementById(id).innerHTML;
             fio = lbl.indexOf(">")
             fstr = lbl.substring(0, fio + 1)
             fstr = lbl.replace(fstr, "")
             fstr = fstr.replace("</SPAN>", "")
             fstr = fstr.replace("</span>", "")

             var eqid = document.getElementById("lbleqid").value;
             var ro = document.getElementById("lblro").value;
             if (id != "change") {
                 var lbl = document.getElementById(id).innerHTML;
                 fio = lbl.indexOf(">")
                 fstr = lbl.substring(0, fio + 1)
                 fstr = lbl.replace(fstr, "")
                 fstr = fstr.replace("</SPAN>", "")
                 fstr = fstr.replace("</span>", "")
             }
             //alert(fstr)
             //alert("levels.aspx?cid=" + cd + "&id=" + id + "&lbl=" + fstr + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date())
             document.getElementById("ifwt").src = "levels.aspx?cid=" + cd + "&id=" + id + "&lbl=" + fstr + "&eqid=" + eqid + "&ro=" + ro + "&date=" + Date();
         }
         function NewTab(id) {
             //alert(id)
             var cd = document.getElementById("lblcid").innerHTML;
             //var idlbl = "f" + id
             //var lbl = document.getElementById(idlbl).innerHTML;
             document.getElementById("lblcatid").value = id;

             closehref();

             var lbl = document.getElementById(id).innerHTML;
             fio = lbl.indexOf(">")
             fstr = lbl.substring(0, fio + 1)
             fstr = lbl.replace(fstr, "")
             fstr = fstr.replace("</SPAN>", "")
             fstr = fstr.replace("</span>", "")
             //alert(fstr)
             var ro = document.getElementById("lblro").value;
             //fstr = fstr.replace(" ", "_")
             //alert(fstr)
             //alert("weights.aspx?cid=" + cd + "&id=" + id + "&lbl=" + fstr + "&ro=" + ro + "&date=" + Date())
             document.getElementById("ifwt").src = "weights.aspx?cid=" + cd + "&id=" + id + "&lbl=" + fstr + "&ro=" + ro + "&date=" + Date();
         }
     </script>
	</HEAD>
	<body class="tbg" onload="startrow();" >
		<form id="form1" method="post" runat="server">
			<div id="FreezePane" class="FreezePaneOff" align="center">
				<div id="InnerFreezePane" class="InnerFreezePane"></div>
			</div>
			<table style="Z-INDEX: 1; POSITION: absolute; TOP: 81px; LEFT: 100px" cellSpacing="0" cellPadding="2"
				width="800">
				<tr>
					<td class="thdrsinglft" align="left" style="width: 26px"><IMG src="../images/appbuttons/minibuttons/calchd.gif" border="0"></td>
					<td class="thdrsingrt label" width="784"><asp:Label id="lang2076" runat="server">PM Equipment Criticality Calculation Administration</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td vAlign="top">
									<table width="380">
										<tr>
											<td class="hdr1" colSpan="2"><asp:Label id="lang2077" runat="server">Criteria Impacted by the Equipment</asp:Label></td>
										</tr>
										<tr>
											<td><IMG id="row1" onclick="controlrow();" height="20" alt="" src="../images/appbuttons/optdown.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2078" runat="server">Environmental, Health &amp; Safety</asp:Label></td>
										</tr>
										<tr id="r1">
											<td></td>
											<td class="hdr3"><A class="ecrlinksel" id="11A" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('11A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11A')" href="javascript:void(0)"><asp:Label id="lang2079" runat="server">Health and Safety Impact</asp:Label></A></td>
										</tr>
										<tr id="r1a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="11B" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('11B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11B')" href="javascript:void(0)"><asp:Label id="lang2080" runat="server">Environmental Impact</asp:Label></A></td>
										</tr>
										<tr id="r1b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="11C" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('11C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11C')" href="javascript:void(0)"><asp:Label id="lang2081" runat="server">Quality Impact on the Customer</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row2" onclick="controlrow2();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2082" runat="server">Business Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r2">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12A" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('12A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12A')" href="javascript:void(0)"><asp:Label id="lang2083" runat="server">Compliance Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r2a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12B" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('12B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12B')" href="javascript:void(0)"><asp:Label id="lang2084" runat="server">Facility/Utility Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r2b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12C" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('12C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12C')" href="javascript:void(0)"><asp:Label id="lang2085" runat="server">Personnel Impact - Number of People Impacted</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row3" onclick="controlrow3();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2086" runat="server">Production Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r3">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13A" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('13A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13A')" href="javascript:void(0)"><asp:Label id="lang2087" runat="server">Product/Value Stream Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13B" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('13B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13B')" href="javascript:void(0)"><asp:Label id="lang2088" runat="server">Constraint/Bottleneck � Demand vs. Capacity</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13C" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('13C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13C')" href="javascript:void(0)"><asp:Label id="lang2089" runat="server">Redundant Resource Availability</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13D" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('13D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13D')" href="javascript:void(0)"><asp:Label id="lang2090" runat="server">Controlled Environment Impact for Production</asp:Label></A></td>
										</tr>
										<tr>
											<td class="hdr1" colSpan="2"><asp:Label id="lang2091" runat="server">Criteria that Impacts the Equipment</asp:Label></td>
										</tr>
										<tr>
											<td><IMG id="row4" onclick="controlrow4();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2092" runat="server">Equipment Condition</asp:Label></td>
										</tr>
										<tr class="details" id="r4">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21A" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('21A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21A')" href="javascript:void(0)"><asp:Label id="lang2093" runat="server">Current condition</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21B" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('21B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21B')" href="javascript:void(0)"><asp:Label id="lang2094" runat="server">Age</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21C" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('21C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21C')" href="javascript:void(0)"><asp:Label id="lang2095" runat="server">Robustness</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21D" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('21D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21D')" href="javascript:void(0)"><asp:Label id="lang2096" runat="server">Equipment Quality</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row5" onclick="controlrow5();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2097" runat="server">Operational Context</asp:Label></td>
										</tr>
										<tr class="details" id="r5">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22A" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('22A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22A')" href="javascript:void(0)"><asp:Label id="lang2098" runat="server">Demand vs. capability</asp:Label></A></td>
										</tr>
										<tr class="details" id="r5a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22B" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('22B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22B')" href="javascript:void(0)"><asp:Label id="lang2099" runat="server">Operating Environment</asp:Label></A></td>
										</tr>
										<tr class="details" id="r5b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22C" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('22C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22C')" href="javascript:void(0)"><asp:Label id="lang2100" runat="server">Equipment Availability for Maintenance Access vs. Planned Work Required</asp:Label></A>
											</td>
										</tr>
										<tr>
											<td><IMG id="row6" onclick="controlrow6();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2101" runat="server">Equipment Maintainability</asp:Label></td>
										</tr>
										<tr class="details" id="r6">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23A" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('23A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23A')" href="javascript:void(0)"><asp:Label id="lang2102" runat="server">Ease of Maintenance</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23B" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('23B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23B')" href="javascript:void(0)"><asp:Label id="lang2103" runat="server">Availability of Parts</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23C" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('23C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23C')" href="javascript:void(0)"><asp:Label id="lang2104" runat="server">Availability of Vendor Resources</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23D" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('23D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23D')" href="javascript:void(0)"><asp:Label id="lang2105" runat="server">Availability of Maintenance Skills to Service</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row7" onclick="controlrow7();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2106" runat="server">Equipment Financial Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r7">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="24A" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('24A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('24A')" href="javascript:void(0)"><asp:Label id="lang2107" runat="server">Replacement Cost</asp:Label></A></td>
										</tr>
										<tr class="details" id="r7a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="24B" onmouseover="this.className = 'ecrlinkover'" onclick="NewTab('24B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('24B')" href="javascript:void(0)"><asp:Label id="lang2108" runat="server">Replaceability</asp:Label></A></td>
										</tr>
									</table>
								</td>
								<td vAlign="top">
									<table height="400" width="420">
										<TBODY>
											<tr>
												<td vAlign="top" align="left"><iframe id="ifwt" src="weights.aspx" frameBorder="no" width="400" scrolling="no" height="400"
														runat="server" style="BACKGROUND-COLOR: transparent" ></iframe>
												</td>
											</tr>
											<tr>
												<td align="right"><input type="button" id="btncalcall" runat="server" value="Update All?" onclick="checkcalcall();"></td>
											</tr>
										</TBODY>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lblcatid" type="hidden" runat="server">
			<input type="hidden" id="lblro" runat="server"><input type="hidden" id="lblsubmit" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
		<uc1:mmenu1 id="Mmenu11" runat="server"></uc1:mmenu1>
	</body>
</HTML>
