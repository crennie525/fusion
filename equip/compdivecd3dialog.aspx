﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="compdivecd3dialog.aspx.vb" Inherits="lucy_r12.compdivecd3dialog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error Code 3 Entry</title>
    <script  type ="text/javascript">
        function handlereturn(ret) {
            window.returnValue = ret;
            window.close();
        }
        function pageScroll() {
            window.scrollTo(0, top);
            //scrolldelay = setTimeout('pageScroll()', 200); 
        } 
    </script>
</head>
<body onload="pageScroll();">
    <form id="form1" runat="server">
    <div>
    <iframe id="ifecd" runat="server" width="100%" height="150%" frameborder="0"></iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    </div>
    </form>
</body>
</html>
