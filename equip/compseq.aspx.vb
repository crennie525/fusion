

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class compseq
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim fuid, Login, ro, sql, se As String
    Dim ds, dslev As DataSet
    Dim dr As SqlDataReader
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim comp As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgcomp As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Dim redir As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
            redir = redir & "?logout=yes"
            Response.Redirect(redir)
        End Try
        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgcomp.Columns(0).Visible = False
                'addfunc.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                'addfunc.Enabled = False
            End If
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            lblcurrsort.Value = "crouting asc"
            Dim user As String = HttpContext.Current.Session("username").ToString
            lbluser.Value = user
            comp.Open()
            BindGrid("crouting asc")
            comp.Dispose()

        End If
       
    End Sub
    Private Sub BindGrid(ByVal sortExp As String)
        fuid = lblfuid.Value
        sql = "select * from components where func_id = '" & fuid & "' order by " & sortExp
        dr = comp.GetRdrData(sql)
        dgcomp.DataSource = dr
        dgcomp.DataBind()
        dr.Close()
    End Sub

    Private Sub dgcomp_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcomp.EditCommand
        se = lblcurrsort.Value
        dgcomp.EditItemIndex = e.Item.ItemIndex
        comp.Open()
        BindGrid(se)
        comp.Dispose()
    End Sub

    Private Sub dgcomp_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcomp.UpdateCommand
        comp.Open()
        Dim cid As String = "0"

        Dim rte As String = CType(e.Item.FindControl("txtrouting"), TextBox).Text
        Dim orte As String = CType(e.Item.FindControl("lbloldroute"), Label).Text
        Dim rtechk As Long
        Try
            rtechk = System.Convert.ToInt32(rte)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr856" , "compseq.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim com, qty, fun, des, coid, spl, desig As String
        com = CType(e.Item.FindControl("txtcompnum"), TextBox).Text
        com = comp.ModString1(com)

        des = CType(e.Item.FindControl("txtdesc"), TextBox).Text
        des = comp.ModString1(des)

        spl = CType(e.Item.FindControl("txtspl"), TextBox).Text
        spl = comp.ModString1(spl)

        desig = CType(e.Item.FindControl("txtdesig"), TextBox).Text
        desig = comp.ModString1(desig)
        fun = lblfuid.Value
        If Len(com) = 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr857" , "compseq.aspx.vb")
 
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        ElseIf Len(com) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr858" , "compseq.aspx.vb")
 
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        ElseIf Len(des) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr859" , "compseq.aspx.vb")
 
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        ElseIf Len(spl) > 200 Then
            Dim strMessage As String =  tmod.getmsg("cdstr860" , "compseq.aspx.vb")
 
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        ElseIf Len(desig) > 200 Then
            Dim strMessage As String =  tmod.getmsg("cdstr861" , "compseq.aspx.vb")
 
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else
            Dim cpcnt As Integer
            sql = "select count(*) from Components " _
            + "where func_id = '" & fun & "' and compnum = '" & com & "' and compdesc = '" & des & "'"
            cpcnt = 0 'comp.Scalar(sql)
            
            If cpcnt = 0 Then
                des = CType(e.Item.FindControl("txtdesc"), TextBox).Text
                coid = CType(e.Item.FindControl("lblcoide"), Label).Text
                sql = "update components set " _
                + "compdesc = '" & des & "', " _
                + "compnum = '" & com & "', " _
                + "spl = '" & spl & "', " _
                + "desig = '" & desig & "' " _
                + "where comid = '" & coid & "'"
                Try
                    comp.Update(sql)
                Catch ex As Exception
                    Dim strMessage As String =  tmod.getmsg("cdstr862" , "compseq.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
                If rte <> orte And rte <> "0" Then
                    sql = "usp_reroutecomponents '" & fun & "', '" & rte & "', '" & orte & "'"
                    comp.Update(sql)
                ElseIf rte = "0" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr863" , "compseq.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                dgcomp.EditItemIndex = -1
                se = lblcurrsort.Value
                BindGrid(se)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr864" , "compseq.aspx.vb")
 
                comp.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If
        comp.Dispose()
    End Sub

    Private Sub dgcomp_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgcomp.CancelCommand
        dgcomp.EditItemIndex = -1
        se = lblcurrsort.Value
        comp.Open()
        BindGrid(se)
        comp.Dispose()
    End Sub

    Private Sub dgcomp_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgcomp.SortCommand
        comp.Open()
        BindGrid(e.SortExpression)
        comp.Dispose()
        Dim se As String = e.SortExpression
        lblcurrsort.Value = se
        Select Case se
            Case "compnum desc"
                dgcomp.Columns(2).SortExpression = "compnum asc"
                'lblsort.Text = "Component# Descending"
            Case "compnum asc"
                dgcomp.Columns(2).SortExpression = "compnum desc"
                'lblsort.Text = "Component# Ascending"
            Case "routing desc"
                dgcomp.Columns(1).SortExpression = "crouting asc"
                'lblsort.Text = "List Order# Descending"
            Case "routing asc"
                dgcomp.Columns(1).SortExpression = "crouting desc"
                'lblsort.Text = "List Order# Ascending"
        End Select
    End Sub
	Private Sub GetDGLangs()
		Dim dlabs as New dglabs
		Try
			dgcomp.Columns(0).HeaderText = dlabs.GetDGPage("compseq.aspx","dgcomp","0")
		Catch ex As Exception
		End Try
		Try
			dgcomp.Columns(1).HeaderText = dlabs.GetDGPage("compseq.aspx","dgcomp","1")
		Catch ex As Exception
		End Try
		Try
			dgcomp.Columns(2).HeaderText = dlabs.GetDGPage("compseq.aspx","dgcomp","2")
		Catch ex As Exception
		End Try
		Try
			dgcomp.Columns(3).HeaderText = dlabs.GetDGPage("compseq.aspx","dgcomp","3")
		Catch ex As Exception
		End Try
		Try
			dgcomp.Columns(5).HeaderText = dlabs.GetDGPage("compseq.aspx","dgcomp","5")
		Catch ex As Exception
		End Try
		Try
			dgcomp.Columns(6).HeaderText = dlabs.GetDGPage("compseq.aspx","dgcomp","6")
		Catch ex As Exception
		End Try

	End Sub

End Class
