<%@ Page Language="vb" AutoEventWireup="false" Codebehind="fuimg.aspx.vb" Inherits="lucy_r12.fuimg" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>fuimg</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/fuimgaspx_2.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
     function getpnext() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) + 1
			pcnt = parseInt(pcnt) - 1;
			if (currp<=pcnt) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				//alert(document.getElementById("lblcurrbimg").value)
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgfu").src=nimg;
				getdets(currp + 1, pid);
			}
			
		}
		function getplast() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(pcnt) - 1;
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgfu").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getpprev() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			if (currp>=0) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgfu").src = nimg;	
			}
			
		}
		function getpfirst() {
			var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = 0; //parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgfu").src=nimg;	
			getdets(currp + 1, pid);
		}
		function getdets(order, pic) {
        /*document.getElementById("lbloldorder").value = order;
        document.getElementById("lblimgid").value = pic;
        var iorder = document.getElementById("txtiorder");
        var ord = parseInt(order) - 1;
        //alert(ord)
        var iorders = document.getElementById("lbliorders").value;
        
        var iordersarr = iorders.split(",");
        var iordersstr = iordersarr[ord];
        //alert(iordersstr)
        if(iordersstr!="") {
        iorder.value = iordersstr;
        }
        else {
        iorder.value = "";
        }
		*/
        }
        function getport() {
		var eq = document.getElementById("lbleqid").value;
		var fu = document.getElementById("lblfuid").value;
		window.open("pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=0");
		}
		function getbig() {
		var img = document.getElementById("lblcurrimg").value;
		if(img!="") {
		var currp = document.getElementById("lblcurrp").value;
		var bimgs = document.getElementById("lblbimgs").value;
		var bimgsarr = bimgs.split(",");
		var src = bimgsarr[currp];
		src = "../eqimages/" + src;
		window.open("BigPic.aspx?src=" + src)
		}
		}
		function upfuid() {
		var fuid = document.getElementById("ddfu").value;
		if (fuid!="Select Functions"&&fuid!="None") {
		window.parent.handlefuid(fuid);
		}
		}
     </script>
	</HEAD>
	<body >
		<form id="Form2" method="post" runat="server">
			<table width="240" style="LEFT: 0px; POSITION: absolute; TOP: 0px">
				<tr>
					<td width="240" align="center">
						<asp:DropDownList id="ddfu" runat="server" AutoPostBack="True" CssClass="plainlabel" Width="220px"></asp:DropDownList></td>
				</tr>
				<tr>
					<td align="center" colSpan="2">
						<A onclick="getbig();" href="#"><IMG id="imgfu" height="216" src="../images/appimages/funcimg.gif" style="width: 216px" border="0"
								runat="server"></A></td>
				</tr>
				<tr>
					<td align="center" colSpan="2">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 140px;" vAlign="middle" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbleqid" runat="server" NAME="lbleqid"> <input id="lblpcnt" type="hidden" runat="server" NAME="lblpcnt">
			<input id="lblcurrp" type="hidden" runat="server" NAME="lblcurrp"> <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
			<input id="lblimgid" type="hidden" name="lblimgid" runat="server"> <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
			<input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server"> <input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server">
			<input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server"> <input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"> <input type="hidden" id="lblfuid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
