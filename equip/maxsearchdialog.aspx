<%@ Page Language="vb" AutoEventWireup="false" Codebehind="maxsearchdialog.aspx.vb" Inherits="lucy_r12.maxsearchdialog" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>Max Search Dialog</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script  src="../scripts/sessrefdialog.js"></script>
		<script  type="text/javascript" src="../scripts1/maxsearchdialogaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function GetWidth() {
             var x = 0;
             if (self.innerHeight) {
                 x = self.innerWidth;
             }
             else if (document.documentElement && document.documentElement.clientHeight) {
                 x = document.documentElement.clientWidth;
             }
             else if (document.body) {
                 x = document.body.clientWidth;
             }
             return x;
         }

         function GetHeight() {
             var y = 0;
             if (self.innerHeight) {
                 y = self.innerHeight;
             }
             else if (document.documentElement && document.documentElement.clientHeight) {
                 y = document.documentElement.clientHeight;
             }
             else if (document.body) {
                 y = document.body.clientHeight;
             }
             return y;
         }
         var flag = 5
         function upsize(who) {
             var ret = document.getElementById("lblret").value;
             if (ret != "yes") {
                 document.getElementById("lblret").value = "";
                 if (who != flag) {
                     flag = who;
                     document.getElementById("ifimg").height = GetHeight();
                     window.scrollTo(0, top);
                     //alert(who)
                 }
             }
         }
         function pageScroll() {
             upsize('1');
             window.scrollTo(0, top);
             //scrolldelay = setTimeout('pageScroll()', 200); pageScroll();
         } 
         //upsize('1');
     </script>
	</HEAD>
	<body  onload="resetsess();">
		<form id="form1" method="post" runat="server">
			<iframe id="ifimg" runat="server" width="100%" height="200%" frameBorder="no"></iframe>
			<iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input type="hidden" id="lbllog" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblret" runat="server" />
<input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden" runat="server" /> 
</form>
	</body>
</HTML>
