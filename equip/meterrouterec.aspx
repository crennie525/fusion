﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="meterrouterec.aspx.vb"
    Inherits="lucy_r12.meterrouterec" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script  type="text/javascript">
        function retit() {
            window.parent.retrec();
        }
        function getcal(fld) {
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                document.getElementById(fld).innerHTML = eReturn;
                document.getElementById("lblnewdate").value = eReturn;
            }
        }
        function checkedit() {
            var chk = document.getElementById("lblnewdate").value;
            if (chk == "") {
                alert("Reading Date Required")
                return false;
            }
            else {
                document.getElementById("form1").submit();
            }
        }
        function checkread(lastread, currid) {
        //alert(lastread)
            if (lastread != "") {
                var currread = document.getElementById(currid).value;
                //alert(currread)
                if (currread != "") {
                    if (parseInt(currread) < parseInt(lastread)) {
                        var decision = confirm("This Reading is Less Than the Last Reading.\nDo you want to Continue?")
                        if (decision == true) {
                            document.getElementById("form1").submit();
                        }
                        else {
                            alert("Action Cancelled")
                            return false;
                        }
                    }
                    else {
                        document.getElementById("form1").submit();
                    }
                }
                else {
                    alert("Meter Reading Required")
                    return false;
                }
            }
            else {
                document.getElementById("form1").submit();
            }
        }
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function subroute() {
            document.getElementById("lblret").value = "subroute"
            document.getElementById("form1").submit();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
        <tr>
        <td align="right"><a href="#" class="A1" onclick="retit();">Return</a></td>
        </tr>
            <tr>
                <td class="thdrsing label">
                    Meter Routes - Record Readings
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="label" style="width: 100px">
                                Route Title
                            </td>
                            <td class="plainlabel" id="tdroute" runat="server" width="160">
                            </td>
                            <td class="label" style="width: 100px">
                                Description
                            </td>
                            <td class="plainlabel" id="tddesc" runat="server" width="260">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="thdrsing label">
                    Route Stops
                </td>
            </tr>
            <tr id="tradd" runat="server">
            <td class="plainlabel">
            <table>
            <tr>
            <td class="label" style="width: 100px">
                                Reading Date
                            </td>
                            <td id="tddate" runat="server" class="plainlabel" style="width: 100px">
                            </td>
                            <td style="width: 20px">
                                <img onclick="getcal('tddate');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                    width="19" height="19">
                            </td>
                            <td width="200" align="right" class="details" id="tdsubmit" runat="server"><a href="#" onclick="subroute();">Submit Readings</a></td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dglist" runat="server" BackColor="transparent" GridLines="None"
                        CellSpacing="1" AutoGenerateColumns="False">
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Seq#">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label Width="50px" CssClass="plainlabel" ID="lblseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routeorder") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                
                                <EditItemTemplate>
                                    <asp:Label style="width: 40px" CssClass="plainlabelred" ID="txtseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routeorder") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Equipment#">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbleqnumi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label width="140px" CssClass="plainlabelred" ID="lbleqs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                    </asp:Label>
                                    
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                           
                            <asp:TemplateColumn HeaderText="Meter ID">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lblmeteri" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label  Width="140px" CssClass="plainlabelred" ID="lblmtrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:Label>
                                    
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            
                             <asp:TemplateColumn HeaderText="Meter Units">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label  Width="60px" CssClass="plainlabel" ID="lbluniti" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.unit") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabelred" ID="lbluni" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.unit") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Reading">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbllocrefi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastreading") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:textbox CssClass="plainlabelred"  Width="110px" ID="txtread" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lastreading") %>'>
                                    </asp:textbox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                           
                           
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbleqidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmeteridi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmrouteidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mrouteid") %>'>
                                    </asp:Label>
                                   <asp:Label CssClass="plainlabel" ID="lblmstopidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mstopid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbleqide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmeteride" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmrouteid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mrouteid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmstopid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mstopid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
        </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblrouteid" runat="server" />
    <input type="hidden" id="lbloldorder" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblenterby" runat="server" /><input type="hidden" id="lblroute" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblmeter" runat="server" />
    <input type="hidden" id="lblunit" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input type="hidden" id="lbldesc" runat="server" />
    <input type="hidden" id="lblnewdate" runat="server" />
    <input type="hidden" id="lblstopcount" runat="server" />
    <input type="hidden" id="lblreccount" runat="server" />
    </form>
</body>
</html>
