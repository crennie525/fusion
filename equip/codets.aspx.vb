

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class codets
    Inherits System.Web.UI.Page
	Protected WithEvents lang1926 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1925 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1924 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1923 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1922 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1921 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1920 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1919 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim coid, start As String
    Protected WithEvents geteqt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim srch As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdoldcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdolddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoldspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdolddesig As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoldmfg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoldac As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoldsc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                coid = Request.QueryString("coid").ToString
                lblcoid.Value = coid
                srch.Open()
                GetDetails(coid)
                srch.Dispose()
            End If


        End If
    End Sub
    Private Sub GetDetails(ByVal coid As String)
        sql = "select * from components where comid = '" & coid & "'"

        dr = srch.GetRdrData(sql)
        While dr.Read
            tdoldcomp.InnerHtml = dr.Item("compnum").ToString
            tdolddesc.InnerHtml = dr.Item("compdesc").ToString

            tdoldspl.InnerHtml = dr.Item("spl").ToString
            tdolddesig.InnerHtml = dr.Item("desig").ToString

            tdoldmfg.InnerHtml = dr.Item("mfg").ToString
            tdoldac.InnerHtml = dr.Item("assetclass").ToString
            tdoldsc.InnerHtml = dr.Item("subclass").ToString


        End While
        dr.Close()
        geteq.Attributes.Add("src", "../complib/complibtaskview2.aspx?typ=pm&comid=" & coid & "&tpm=N")
        geteqt.Attributes.Add("src", "../complib/complibtaskview2.aspx?typ=pm&comid=" & coid & "&tpm=Y")
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1919.Text = axlabs.GetASPXPage("codets.aspx", "lang1919")
        Catch ex As Exception
        End Try
        Try
            lang1920.Text = axlabs.GetASPXPage("codets.aspx", "lang1920")
        Catch ex As Exception
        End Try
        Try
            lang1921.Text = axlabs.GetASPXPage("codets.aspx", "lang1921")
        Catch ex As Exception
        End Try
        Try
            lang1922.Text = axlabs.GetASPXPage("codets.aspx", "lang1922")
        Catch ex As Exception
        End Try
        Try
            lang1923.Text = axlabs.GetASPXPage("codets.aspx", "lang1923")
        Catch ex As Exception
        End Try
        Try
            lang1924.Text = axlabs.GetASPXPage("codets.aspx", "lang1924")
        Catch ex As Exception
        End Try
        Try
            lang1925.Text = axlabs.GetASPXPage("codets.aspx", "lang1925")
        Catch ex As Exception
        End Try
        Try
            lang1926.Text = axlabs.GetASPXPage("codets.aspx", "lang1926")
        Catch ex As Exception
        End Try

    End Sub

End Class
