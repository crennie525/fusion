<%@ Page Language="vb" AutoEventWireup="false" Codebehind="funclistdet.aspx.vb" Inherits="lucy_r12.funclistdet" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>funclistdet</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script  type="text/javascript" src="../scripts/smartscroll.js"></script>
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script  src="../scripts1/funclistdetaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu">
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel" height="36" style="width: 140px"><asp:Label id="lang2400" runat="server">Selected Equipment</asp:Label></td>
								<td class="plainlabel" id="tdeq" runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang2401" runat="server">Functions</asp:Label></td>
				</tr>
				<tr>
					<td>
						<div style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 840px; HEIGHT: 150px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							id="divfu" runat="server"></div>
					</td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang2402" runat="server">Components</asp:Label></td>
				</tr>
				<tr>
					<td>
						<div style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 840px; HEIGHT: 150px; OVERFLOW: auto; BORDER-TOP: black 1px solid; TOP: 0px; BORDER-RIGHT: black 1px solid"
							id="divco" runat="server"></div>
					</td>
				</tr>
				<tr id="trsave" class="details" runat="server">
					<td class="plainlabel" align="right"><A onclick="checkgo('go');" href="#"><asp:Label id="lang2403" runat="server">Jump To Selection</asp:Label></A>&nbsp;&nbsp;&nbsp;&nbsp;<A onclick="checkgo('can');" href="#"><asp:Label id="lang2404" runat="server">Cancel and Return</asp:Label></A></td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td class="bluelabel" height="26" style="width: 110px"><asp:Label id="lang2405" runat="server">Selected Function</asp:Label></td>
								<td class="plainlabel" id="tdfunc"></td>
							</tr>
							<tr>
								<td class="bluelabel" height="26"><asp:Label id="lang2406" runat="server">Selected Component</asp:Label></td>
								<td class="plainlabel" id="tdcomp"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbleqid" runat="server" NAME="lbleqid"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblsrchtyp" runat="server" NAME="lblsrchtyp"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lblfunc" runat="server"> <input type="hidden" id="lblfuid" runat="server">
			<input type="hidden" id="lblcomp" runat="server" NAME="Hidden1"> <input type="hidden" id="lblcoid" runat="server" NAME="Hidden2">
			<input type="hidden" id="xcoord" runat="server" NAME="Hidden1"> <input type="hidden" id="ycoord" runat="server" NAME="Hidden2">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
