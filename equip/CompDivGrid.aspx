<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CompDivGrid.aspx.vb" Inherits="lucy_r12.CompDivGrid" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>CompDivGrid</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
        <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
		<script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
		<script  src="../scripts1/CompDivGridaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function getecd1() {
             var compnum = document.getElementById("txtnewcomp").value;
             //if (compnum != "") {
             var eReturn = window.showModalDialog("compdiv1dialog.aspx?compnum=" + compnum + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
             if (eReturn) {
                 //alert(eReturn)
                 var ret = eReturn.split("~");
                 var chk = ret[0];
                 if (chk != "can") {
                     var chk2 = ret[1];
                     if (chk2 != compnum) {
                         document.getElementById("txtnewcomp").value = chk2;
                     }
                     document.getElementById("lblecd1id").value = chk;
                     document.getElementById("form1").submit();
                 }
                 else {
                     return false;
                 }

             }
             else {
                 
                 return false;
             }
             //}
         }
     </script>
	</HEAD>
	<body class="tbg" onload="checkit();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 0px" width="720">
				<TBODY>
					<tr>
						<td colSpan="3">
							<table>
								<tr>
									<td class="bluelabel tbg" align="left" width="190"><asp:Label id="lang2048" runat="server">Add New Component</asp:Label></td>
									<td style="width: 110px" class="tbg"><asp:textbox id="txtnewcomp" runat="server" MaxLength="100" Width="120px"></asp:textbox></td>
									<td width="70" class="tbg"><asp:imagebutton id="btnAddEq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton></td>
									<td align="right" width="300"></td>
								</tr>
								<tr height="20">
									<td class="plainlabelblue"><asp:Label id="lang2049" runat="server">Selected Equipment:</asp:Label></td>
									<td class="plainlabelblue" id="tdeq" colSpan="3" runat="server"></td>
								</tr>
								<tr height="20">
									<td class="plainlabelblue"><asp:Label id="lang2050" runat="server">Selected Function:</asp:Label></td>
									<td class="plainlabelblue" id="tdfunc" colSpan="3" runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="4"><div id="spdiv" style="WIDTH: 740px; HEIGHT: 270px; OVERFLOW: auto"><asp:repeater id="rptrfunc" runat="server">
									<HeaderTemplate>
										<table cellspacing="0">
											<tr class="tbg" width="720" height="26">
												<td class="btmmenu plainlabel" align="center"><img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16"></td>
												<td class="btmmenu plainlabel" style="width: 120px"><asp:Label id="lang2051" runat="server">Component#</asp:Label></td>
												<td class="btmmenu plainlabel" width="300"><asp:Label id="lang2052" runat="server">Description</asp:Label></td>
												<td class="btmmenu plainlabel" width="300"><asp:Label id="lang2053" runat="server">Special Identifier</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg" height="20">
											<td class="plainlabel">
												<asp:CheckBox id="cb2" runat="server"></asp:CheckBox></td>
											<td class="plainlabel">&nbsp;
												<asp:LinkButton ID="Label4"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
												</asp:LinkButton></td>
											<td class="plainlabel">
												<asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>' Runat = server>
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lblcoiditem" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<AlternatingItemTemplate>
										<tr class="transrowblue" height="20">
											<td class="plainlabel">
												<asp:CheckBox id="cb3" runat="server"></asp:CheckBox></td>
											<td class="plainlabel transrowblue">&nbsp;
												<asp:LinkButton ID="Linkbutton1"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
												</asp:LinkButton></td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>' Runat = server>
												</asp:Label></td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lblcoidalt" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</AlternatingItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD></TR>
			<tr>
				<td colSpan="3">
				<table cellpadding="0">
				<tr>
				<td><img id="ibtnremove2" runat="server" src="../images/appbuttons/minibuttons/del.gif" onclick="verdel();"></td>
				<td><IMG id="imgcopy" onmouseover="return overlib('Lookup Component Records to Copy')" onclick="GetCompCopy();"
						onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif"
						style="width: 20px" runat="server"></td>
				</tr>
				</table>
				
					
				</td>
			</tr>
			</TBODY></TABLE><input id="lblrd" type="hidden" name="lblrd" runat="server"> <input id="lbleq" type="hidden" name="lbleq" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"> <input id="lbldchk" type="hidden" name="lbldchk" runat="server">
			<input id="lblsid" type="hidden" name="lblsid" runat="server"> <input id="lblcid" type="hidden" name="lblcid" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server"><input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input id="lblpar" type="hidden" name="lblpar" runat="server"> <input id="lblcoid" type="hidden" runat="server">
			<input id="lblcompchk" type="hidden" runat="server"> <input type="hidden" id="lblro" runat="server">
			<input type="hidden" id="lbllock" runat="server" NAME="lbllock"> <input type="hidden" id="lblcbonoff" runat="server" NAME="lblcbonoff">
			<input type="hidden" id="lbllockedby" runat="server" NAME="lbllockedby"> <input type="hidden" id="lbllog" runat="server">
		<input type="hidden" id="lblisecd" runat="server" />
        <input type="hidden" id="lblecd1id" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblwho" runat="server" />
<input type="hidden" id="lblchld" runat="server" />
</form>
	</body>
</HTML>
