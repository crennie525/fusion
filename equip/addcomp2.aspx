<%@ Page Language="vb" AutoEventWireup="false" Codebehind="addcomp2.aspx.vb" Inherits="lucy_r12.addcomp2" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>addcomp2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  type="text/javascript">
		    function getecd1() {
		        var compnum = document.getElementById("txtconame").value;
		        //if (compnum != "") {
		        var eReturn = window.showModalDialog("compdiv1dialog.aspx?compnum=" + compnum + "&date=" + Date(), "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
		        if (eReturn) {
		            //alert(eReturn)
		            var ret = eReturn.split("~");
		            var chk = ret[0];
		            if (chk != "can") {
		                var chk2 = ret[1];
		                if (chk2 != compnum) {
		                    document.getElementById("txtconame").value = chk2;
		                }
		                document.getElementById("lblecd1id").value = chk;
		                document.getElementById("lblnewecd1id").value = chk;
		                //document.getElementById("lblcompchk").value = "upfail";
		                //document.getElementById("form1").submit();
		            }
		            else {
		                return false;
		            }

		        }
		        else {

		            return false;
		        }
		        //}
		    }
		    function savdets() {
		        var pchk = document.getElementById("lblpcnt").value;
		        var nchk = document.getElementById("txtiorder").value;
		        var cchk = document.getElementById("lblcurrp").value;
		        if (parseInt(nchk) > parseInt(pchk)) {
		            document.getElementById("txtiorder").value = parseInt(cchk) + 1;
		            alert("New Order Value Greater Than Image Count")
		        }
		        else if (parseInt(nchk) == 0) {
		            document.getElementById("txtiorder").value = parseInt(cchk) + 1;
		            alert("New Order Value Must Be Greater Than 0")
		        }
		        else {
		            var img = document.getElementById("lblcurrimg").value;
		            if (img != "") {
		                var coid = document.getElementById("lblcoid").value;
		                if (coid != "") {
		                    document.getElementById("lblcompchk").value = "savedets";
		                    document.getElementById("form1").submit();
		                }
		                else {
		                    alert("No Component Selected")
		                }
		            }
		            else {
		                alert("No Image Selected")
		            }
		        }
		    }
		function addpic() {
		//complibuploaddialog
		//window.parent.setref();

		    var fuid = document.getElementById("lblfuid").value;
		    var eqid = document.getElementById("lbleqid").value;
		    var coid = document.getElementById("lblcoid").value;
		    var ro = document.getElementById("lblro").value;
		if (coid != "" && coid != "0") {
		    //was ../complib/complibuploaddialog
		    //var eReturn = window.showModalDialog("equploadimagedialog.aspx?typ=co&comid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
		    var eReturn = window.showModalDialog("equploadimagedialog.aspx?typ=co&eqid=" + eqid + "&funcid=" + fuid + "&comid=" + coid + "&ro=" + ro + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:700px; resizable=yes");
            if (eReturn=="ok") {
				document.getElementById("lblcompchk").value = "checkpic";
				document.getElementById("form1").submit();
			}
		}
        else {
        alert("No Component Selected")
        }
		}
		function delimg() {
		var comid = document.getElementById("lblcomid").value;
		if(comid!=""&&comid!="0") {
			id = document.getElementById("lblimgid").value
			if(id!="") {
			document.getElementById("lblcompchk").value = "delimg";
			document.getElementById("form1").submit();
			}
			else {
			alert("No Image Selected")
			}
		}
        else {
        alert("No Component Selected")
        }

        }
		function getpnext() {
		var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) + 1
			pcnt = parseInt(pcnt) - 1;
			if (currp<=pcnt) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				//alert(document.getElementById("lblcurrbimg").value)
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgco").src=nimg;
				getdets(currp + 1, pid);
			}	
		}
		function getplast() {
		var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(pcnt) - 1;
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgco").src=nimg;	
			getdets(currp + 1, pid);	
		}
		function getpprev() {
		var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			if (currp>=0) {
				var det = imgsarr[currp];
				var detarr = det.split(";")
				nimg = detarr[1];
				pid = detarr[0];
				document.getElementById("lblcurrimg").value= ovimgsarr[currp];
				document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
				document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
				document.getElementById("lblimgid").value= pid;
				document.getElementById("lblcurrp").value= currp;
				document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
				document.getElementById("imgco").src=nimg;
				getdets(currp + 1, pid);
			}
				
		}
		function getpfirst() {
		var currp = document.getElementById("lblcurrp").value;
			var pcnt = document.getElementById("lblpcnt").value;
			var imgs = document.getElementById("lblimgs").value;
			var ovimgs = document.getElementById("lblovimgs").value;
			var ovbimgs = document.getElementById("lblovbimgs").value;
			var ovtimgs = document.getElementById("lblovtimgs").value;
			var imgsarr = imgs.split("~");
			var ovimgsarr = ovimgs.split(",");
			var ovbimgsarr = ovbimgs.split(",");
			var ovtimgsarr = ovtimgs.split(",");
			var nimg;
			var pid;
			currp = 0; //parseInt(currp) - 1
			pcnt = parseInt(pcnt) - 1;
			var det = imgsarr[currp];
			var detarr = det.split(";")
			nimg = detarr[1];
			pid = detarr[0];
			document.getElementById("lblcurrimg").value= ovimgsarr[currp];
			document.getElementById("lblcurrbimg").value= ovbimgsarr[currp];
			document.getElementById("lblcurrtimg").value= ovtimgsarr[currp];
			document.getElementById("lblimgid").value= pid;
			document.getElementById("lblcurrp").value= currp;
			document.getElementById("lblpg").innerHTML= "Image " + (currp + 1) + " of " + (pcnt + 1);
			document.getElementById("imgco").src=nimg;	
			getdets(currp + 1, pid);	
        }
        function getdets(order, pic) {
        document.getElementById("lbloldorder").value = order;
        document.getElementById("lblimgid").value = pic;
        var iorder = document.getElementById("txtiorder");
        var ord = parseInt(order) - 1;
        //alert(ord)
        var iorders = document.getElementById("lbliorders").value;
        
        var iordersarr = iorders.split(",");
        var iordersstr = iordersarr[ord];
        //alert(iordersstr)
        if(iordersstr!="") {
        iorder.value = iordersstr;
        }
        else {
        iorder.value = "";
        }
        }
        function DisableButton(b) {
            document.getElementById("btntocomp").className = "details";
            document.getElementById("btnfromcomp").className = "details";
            document.getElementById("todis").className = "view";
            document.getElementById("fromdis").className = "view";
            var ecd2 = document.getElementById("lblcurrecd2").value;
            var chk = document.getElementById("lblisecd").value;
            if (chk == "1") {
                var eReturn = window.showModalDialog("compdivecd3dialog.aspx?ecd2=" + ecd2 + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:500px; resizable=yes");
                if (eReturn) {
                    document.getElementById("lblnewecd3").value = eReturn;
                    document.getElementById("form1").submit();
                }
            }
            else {
                document.getElementById("form1").submit();
            }
		}
		function FreezeScreen(msg) {
			scroll(0,0);
			var outerPane = document.getElementById('FreezePane');
			var innerPane = document.getElementById('InnerFreezePane');
			if (outerPane) outerPane.className = 'FreezePaneOn';
			if (innerPane) innerPane.innerHTML = msg;
		}
		function getstasks() {
		
		}
		function savecdets() {
		
		}
		function getport() {
		window.parent.setref();
		var co = document.getElementById("lblcoid").value;
		var fu = document.getElementById("lblfuid").value;
		var eq = document.getElementById("lbleqid").value;
		window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=" + co);
		}
		function getbig() {
		//window.parent.setref();
		var img = document.getElementById("lblcurrimg").value;
		if(img!="") {
		var currp = document.getElementById("lblcurrp").value;
		var bimgs = document.getElementById("lblbimgs").value;
		var bimgsarr = bimgs.split(",");
		var src = bimgsarr[currp];
		src = "../eqimages/" + src;
		window.open("../equip/BigPic.aspx?src=" + src)
		}
		}
		function getsgrid() {
		var comid = document.getElementById("lblcoid").value;
		//txtconame
		var comp = document.getElementById("txtconame").value;
		var fuid = document.getElementById("lblfuid").value;
		var tpm = document.getElementById("lbltpm").value;
		if(comid!=""&&comid!="0") {
		var eReturn = window.showModalDialog("../complib/complibtasks2dialog.aspx?tpm=" + tpm + "&typ=tasksno&coid=" + comid + "&fuid=" + fuid + "&comp=" + comp + "&date=" + Date(), "", "dialogHeight:480px; dialogWidth:850px;resizable=yes");
		if (eReturn) {
		//document.getElementById("geteq").src="../complib/complibtaskview2.aspx?tpm=" + tpm + "&typ=pm&comid=" + comid;
		document.getElementById("lblcompchk").value = "upfail";
		document.getElementById("form1").submit();
		}
		else {
		document.getElementById("geteq").src="../complib/complibtaskview2.aspx?tpm=" + tpm + "&typ=pm&comid=" + comid;
		document.getElementById("lblcompchk").value = "upfail";
		document.getElementById("form1").submit();
		}
		}
		}
		function getACDiv() {
		//window.parent.setref();
		//handleapp();
		var coid = document.getElementById("lblcoid").value;
		var ro = document.getElementById("lblro").value;
		var cid = "0";
		if(coid!=""&&coid!="0") {
		var eReturn = window.showModalDialog("../complib/compclassdialog.aspx?cid=" + cid + "&ro=" + ro, "", "dialogHeight:660px; dialogWidth:800px; resizable=yes");
		if (eReturn) {
			//document.getElementById("lblcompchk").value = "1";
			//document.getElementById("form1").submit();
		}
		}
		}
		function cget() {
		var coid = document.getElementById("lblcoid").value;
		var ro = document.getElementById("lblro").value;
		if(coid!=""&&coid!="0") {
		var eReturn = window.showModalDialog("../complib/cgetdialog.aspx?lib=no&comid=" + coid, "", "dialogHeight:650px; dialogWidth:780px; resizable=yes");
		if (eReturn) {
			document.getElementById("lblcompchk").value = "1";
			document.getElementById("form1").submit();
		}
		}
		}
		function checkwarn() {
		//needs to be determined
		//will approval be necessary for master list additions
		}
		function checktasks() {
		var chk = document.getElementById("lblcompchk").value;
		if(chk=="gettasks") {
		document.getElementById("lblcompchk").value="";
		var comid = document.getElementById("lblcoid").value;
		var tpm = document.getElementById("lbltpm").value;
		document.getElementById("geteq").src="../complib/complibtaskview2.aspx?tpm=" + tpm + "&typ=pm&comid=" + comid;
		}
}
function showcomp() {
//alert('0')
    var chk = document.getElementById("lblswitch").value;
    if (chk == "all") {
        document.getElementById("lblswitch").value = "comp";
        document.getElementById("lbfailmaster").className = "details";
        document.getElementById("lbfailcomp").className = "view";
        document.getElementById("aswitch").innerHTML = "Unassigned Failure Modes";
    }
    else {
        document.getElementById("lblswitch").value = "all";
        document.getElementById("lbfailmaster").className = "view";
        document.getElementById("lbfailcomp").className = "details";
        document.getElementById("aswitch").innerHTML = "Available Failure Modes";
    }
}
function showcompret() {
    var chk1 = document.getElementById("lblgetecd2").value;
    if (chk1 == "1") {
        var ecd1 = document.getElementById("lblecd1id").value;
        var ecd2txt = document.getElementById("lblecd2txt").value;
        //alert(ecd1)
        var eReturn = window.showModalDialog("compdivecd2dialog.aspx?ecd1=" + ecd1 + "&ecd2txt=" + ecd2txt + "&date=" + Date(), "", "dialogHeight:200px; dialogWidth:500px; resizable=yes");
        if (eReturn) {
            document.getElementById("lblgetecd2").value = "";
            document.getElementById("lblcurrecd2").value = eReturn;
        }
    }

    var chk = document.getElementById("lblswitch").value;
    //alert('1')
    if (chk == "all" || chk == "") {

        document.getElementById("lbfailmaster").className = "view";
        document.getElementById("lbfailcomp").className = "details";
        document.getElementById("aswitch").innerHTML = "Available Failure Modes";
    }
    else {

        document.getElementById("lbfailmaster").className = "details";
        document.getElementById("lbfailcomp").className = "view";
        document.getElementById("aswitch").innerHTML = "Unassigned Failure Modes";

    }
}
		</script>
	</HEAD>
	<body  onload="checktasks();showcompret();">
		<form id="form1" method="post" runat="server">
			<div style="WIDTH: 720px" id="FreezePane" class="FreezePaneOff" align="center">
				<div id="InnerFreezePane" class="InnerFreezePane"></div>
			</div>
			<div style="Z-INDEX: 1000; POSITION: absolute; VISIBILITY: hidden" id="overDiv"></div>
			<table style="POSITION: absolute; TOP: 10px; LEFT: 10px" id="eqdetdiv" cellSpacing="0"
				cellPadding="0" width="675">
				<tr>
					<td style="width: 110px"></td>
					<td style="width: 90px"></td>
					<td style="width: 50px"></td>
					<td width="160"></td>
					<td style="width: 100px"></td>
					<td style="width: 140px"></td>
					<td style="width: 120px"></td>
				</tr>
				<tr>
					<td colSpan="7">
						<table cellSpacing="0" cellPadding="1" width="675">
							<tr>
								<td id="tdnavtop" class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="653">New Component Details</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label">New Component</td>
					<td colSpan="3"><asp:textbox id="txtconame" runat="server" Width="180px" cssclass="plainlabel wd180" MaxLength="100"></asp:textbox></td>
                    <td><img alt="" src="../images/appbuttons/minibuttons/addnew.gif" id="imgaddecd" runat="server" /></td>
					<td height="40" rowSpan="9" colSpan="2" align="center">
						<table>
							<tr>
								<td><IMG id="imgco" onclick="getbig();" src="../images/appimages/compimg.gif" style="width: 216px"
										height="206" runat="server">
								</td>
							</tr>
							<tr height="20">
								<td align="center">
									<table>
										<tr>
											<td class="bluelabel" style="width: 80px">Order</td>
											<td style="width: 60px"><asp:textbox id="txtiorder" runat="server" style="width: 40px" CssClass="plainlabel wd40"></asp:textbox></td>
											<td style="width: 20px"><IMG class="details" onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
											<td style="width: 20px"><IMG onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)" onmouseout="return nd()"
													onclick="addpic();" src="../images/appbuttons/minibuttons/addmod.gif"></td>
											<td style="width: 20px"><IMG id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
													src="../images/appbuttons/minibuttons/del.gif" runat="server"></td>
											<td style="width: 20px"><IMG id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)" onmouseout="return nd()"
													onclick="savdets();" src="../images/appbuttons/minibuttons/saveDisk1.gif" runat="server">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
										cellSpacing="0" cellPadding="0">
										<tr>
											<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
													runat="server"></td>
											<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="134" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
											<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
													runat="server"></td>
											<td style="width: 20px"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
													runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label">Description</td>
					<td colSpan="4"><asp:textbox id="txtdesc" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label">SPL</td>
					<td colSpan="4"><asp:textbox id="txtspl" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label">Designation</td>
					<td colSpan="4"><asp:textbox id="txtdesig" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label">Mfg</td>
					<td colSpan="4"><asp:textbox id="txtmfg" runat="server" Width="250px" cssclass="plainlabel wd250" MaxLength="200"></asp:textbox></td>
				</tr>
				<tr id="tdpreadd" runat="server">
					<td colSpan="4" align="center" class="plainlabelred">Save New Component Details to 
						add Failure Modes and Tasks</td>
					<td align="right"><asp:imagebutton id="btnsave1" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:imagebutton>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td class="label" height="20">Component Class</td>
					<td colSpan="3" class="plainlabel" id="tdcclass" runat="server"></td>
					<td>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td><IMG id="btnaddasset" runat="server" class="details" onmouseover="return overlib('Add/Edit Asset Class', LEFT)"
										onmouseout="return nd()" onclick="getACDiv();" alt="" src="../images/appbuttons/minibuttons/addmod.gif"></td>
								<td><IMG id="btncget" runat="server" class="details" onmouseover="return overlib('Lookup Asset Class', LEFT)"
										onmouseout="return nd()" onclick="cget();" alt="" src="../images/appbuttons/minibuttons/magnifier.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="label" height="20">Sub Class</td>
					<td colSpan="3" class="plainlabel" id="tdsclass" runat="server"></td>
				</tr>
                   <tr>
                <td class="label" height="20">Occurrence Area</td>
                <td colspan="3"><asp:DropDownList ID="ddoca" runat="server" AutoPostBack="true">
                    </asp:DropDownList></td>
                    
                </tr>
				<tr>
					<td colSpan="5">
						<table cellSpacing="2" cellPadding="0">
							<TBODY>
								<tr vAlign="middle" height="20">
									<td width="10"></td>
									<td class="bluelabel" align="center" width="188"><asp:Label id="aswitch" runat="server">Available Failure Modes</asp:Label>
                                    </td>
                                    <td width="22"><img src="../images/appbuttons/minibuttons/checkrel.gif" onclick="showcomp();" id="imgchk" runat="server" /></td>
									<td width="22"></td>
									<td class="bluelabel" align="center" width="210"><asp:Label id="lang2042" runat="server">Component Failure Modes</asp:Label></td>
									<td width="10"></td>
								</tr>
								<tr>
									<td style="WIDTH: 31px" class="label" vAlign="top" width="31" align="center"><br>
										<br>
									</td>
									<td colspan="2" align="center"><asp:listbox id="lbfailmaster" runat="server" Width="170px" CssClass="fmboxo" Height="90px" SelectionMode="Multiple"></asp:listbox>
                                    <asp:listbox id="lbfailcomp" runat="server" Width="200px" SelectionMode="Multiple" Height="70px" CssClass="details"></asp:listbox></td>
									<td vAlign="middle" width="22" align="center"><IMG id="todis" class="details" src="../images/appbuttons/minibuttons/forwardgraybg.gif"
											style="width: 20px" height="20" runat="server"> <IMG id="fromdis" class="details" src="../images/appbuttons/minibuttons/backgraybg.gif"
											style="width: 20px" height="20" runat="server">
										<asp:imagebutton id="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif"></asp:imagebutton><br>
										<asp:imagebutton id="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif"></asp:imagebutton></td>
									<td align="center"><asp:listbox id="lbfailmodes" runat="server" Width="170px" CssClass="fmboxo" Height="90px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</tr>
								<tr>
									<td class="label details" colSpan="5">Add a New Failure Mode&nbsp;<asp:textbox id="txtnewfail" runat="server" Width="170px" cssclass="plainlabel wd170"></asp:textbox>
										&nbsp;<asp:imagebutton id="btnaddfail" runat="server" ImageUrl="../images/appbuttons/minibuttons/addmod.gif"></asp:imagebutton></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
                <tr>
					<td colSpan="5" class="plainlabelblue">To remove a failure mode that is assigned to an Occurrence Area the Occurrence Area needs to be selected.</td>
				</tr>
				<tr id="tdafteradd" runat="server">
					<td colSpan="4" align="center" class="plainlabelred">Click Save Button to Save 
						Changes to Current Component Details</td>
					<td align="right"><asp:imagebutton id="btnsave" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"></asp:imagebutton>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="7">
						<table cellSpacing="0" cellPadding="1" width="675">
							<tr>
								<td id="tdbot" class="thdrsinglft" width="22"><IMG border="0" src="../images/appbuttons/minibuttons/eqarch.gif"></td>
								<td class="thdrsingrt label" width="653">Component Tasks</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="7" align="right"><IMG id="Img2" onmouseover="return overlib('Add/Edit Task Steps for this Component', LEFT)"
							onmouseout="return nd()" onclick="getsgrid();" alt="" src="../images/appbuttons/minibuttons/addmod.gif" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="7"><iframe style="BORDER-BOTTOM-STYLE: none; BORDER-RIGHT-STYLE: none; BACKGROUND-COLOR: transparent; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none"
							id="geteq" height="100" src="../complib/complibtaskview2.aspx?jump=no" frameBorder="no" width="675"
							 scrolling="yes" runat="server"></iframe>
					</td>
				</tr>
			</table>
			<input id="lblcoid" type="hidden" runat="server" NAME="lblcoid"> <input id="lblsavetype" type="hidden" runat="server" NAME="lblsavetype">
			<input id="lblpcnt" type="hidden" name="lblpcnt" runat="server"> <input id="lblcurrp" type="hidden" name="lblcurrp" runat="server">
			<input id="lblimgs" type="hidden" name="lblimgs" runat="server"> <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
			<input id="lblovimgs" type="hidden" name="lblovimgs" runat="server"> <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
			<input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server"> <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
			<input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" name="lbloldorder" runat="server"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"> <input id="lblcompchk" type="hidden" name="lblcompchk" runat="server">
			<input id="lbllog" type="hidden" runat="server" NAME="lbllog"> <input id="lblro" type="hidden" runat="server" NAME="lblro">
			<input type="hidden" id="lblnew" runat="server" NAME="lblnew"> <input type="hidden" id="lblsid" runat="server" NAME="lblsid">
			<input type="hidden" id="lbluser" runat="server" NAME="lbluser"> <input type="hidden" id="lbloldcomp" runat="server" NAME="lbloldcomp">
			<input type="hidden" id="lbloldkey" runat="server" NAME="lbloldkey"> <input type="hidden" id="lblfuid" runat="server">
			<input type="hidden" id="lblfailchk" runat="server"> <input type="hidden" id="lbltpm" runat="server">
            <input type="hidden" id="lblfailmode" runat="server" />
            <input type="hidden" id="lblswitch" runat="server" />
            <input type="hidden" id="lblisecd" runat="server" />
            <input type="hidden" id="lblgetecd2" runat="server" />
            <input type="hidden" id="lblgetecd3" runat="server" />
            <input type="hidden" id="lblcurrecd2" runat="server" />
            <input type="hidden" id="lblnewecd3" runat="server" />
            <input type="hidden" id="lblecd1id" runat="server" />
            <input type="hidden" id="lblecd2txt" runat="server" />
            <input type="hidden" id="lblnewecd1id" runat="server" />
            <input type="hidden" id="lbleqid" runat="server" />
            <input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
