

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class EQCopyMini
    Inherits System.Web.UI.Page
    Protected WithEvents btnaddasset As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents lang2240 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2239 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2238 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2237 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2236 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2235 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2234 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2233 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2232 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2231 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2230 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2229 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2228 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2227 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2226 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2225 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2224 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2223 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2222 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2221 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2220 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2219 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2218 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2217 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2216 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2215 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2214 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2213 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2212 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2211 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2210 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2209 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2208 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2207 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2206 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2205 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2204 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2203 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2202 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtfpc As System.Web.UI.WebControls.TextBox
    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim cid, srch, sid, lid As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tblrevdetails As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcmail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdloc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoem As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtneweqnum As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtncopy As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tbleqrev As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locdet As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllistr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim eqcopy As New Utilities
    Dim did, clid, psid, login, ro As String
    Protected WithEvents lblcopyflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdbt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tblneweq As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdneweq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewsql As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcbydate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcbyphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcbyemail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcbymail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txteqdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqmod As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqoem As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqsn As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqecr As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ddlstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnecr As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Td8 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcureq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdac As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ibtnsaveneweq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbfunc As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbcomp As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbpm As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbtpm As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents rbro As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbai As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents cbecryn As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbrat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblnewid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddoem As System.Web.UI.WebControls.DropDownList
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents treq As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater
    Protected WithEvents tbleq As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not Me.IsPostBack Then
            If lbllog.Value <> "no" Then
                Try
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro
                    If ro = "1" Then
                        ibtncopy.Enabled = False
                        ibtncopy.ImageUrl = "../images/appbuttons/bgbuttons/copydis.gif"
                        'imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    Else
                        'ibtncopy.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/copyhov.gif'")
                        'ibtncopy.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/eqcopy.gif'")
                        ibtncopy.Attributes.Add("onclick", "getfreeze();")
                    End If
                    did = Request.QueryString("dept").ToString
                    lbldid.Value = did
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    If Len(did) <> 0 AndAlso did <> "" AndAlso did <> "0" Then
                        txtpg.Value = "1"
                        cid = "0" ' HttpContext.Current.Session("comp").ToString
                        lblcid.Value = cid
                        psid = "12" 'HttpContext.Current.Session("psite").ToString
                        lblpsid.Value = "12"
                        sid = Request.QueryString("sid").ToString
                        clid = Request.QueryString("cell").ToString
                        lblsid.Value = sid
                        lblclid.Value = clid
                        eqcopy.Open()
                        PopEq(PageNumber)
                        PopAC()
                        PopOEM()
                        eqcopy.Dispose()
                        tbleqrev.Attributes.Add("class", "details")
                        tblrevdetails.Attributes.Add("class", "details")
                        tblneweq.Attributes.Add("class", "details")
                    ElseIf lid <> "" Then
                        txtpg.Value = "1"
                        cid = "0" 'HttpContext.Current.Session("comp").ToString
                        lblcid.Value = cid
                        psid = "12" 'HttpContext.Current.Session("psite").ToString
                        lblpsid.Value = "12"
                        sid = Request.QueryString("sid").ToString
                        lblsid.Value = sid
                        eqcopy.Open()
                        PopEq(PageNumber)
                        PopAC()
                        PopOEM()
                        eqcopy.Dispose()
                        tbleqrev.Attributes.Add("class", "details")
                        tblrevdetails.Attributes.Add("class", "details")
                        tblneweq.Attributes.Add("class", "details")
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr881", "EQCopyMini.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "noeqid"
                    End If
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr882", "EQCopyMini.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End Try
            End If
            If Request.Form("lblret") = "next" Then
                eqcopy.Open()
                GetNext()
                eqcopy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                eqcopy.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopEq(PageNumber)
                eqcopy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                eqcopy.Open()
                GetPrev()
                eqcopy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                eqcopy.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopEq(PageNumber)
                eqcopy.Dispose()
                lblret.Value = ""
            End If
        End If
        'btnprev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'btnprev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'btnnext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'btnnext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")

        'ibtnsaveneweq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ibtnsaveneweq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
        'ibtneqcopy.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/copyhov.gif'")
        'ibtneqcopy.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/eqcopy.gif'")
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
    End Sub
    Private Sub PopOEM()
        'eqr.Open()
        sql = "select * from invcompanies where type = 'M' order by name"
        dr = eqcopy.GetRdrData(sql)
        ddoem.DataSource = dr
        ddoem.DataTextField = "name"
        ddoem.DataValueField = "name"
        ddoem.DataBind()
        dr.Close()
        ddoem.Items.Insert(0, "Select OEM")
    End Sub
    Private Sub PopAC()
        cid = "0" ' lblcid.value
        sql = "select * from pmAssetClass"
        dr = eqcopy.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        ddac.DataBind()
        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopEq(PageNumber)
        Catch ex As Exception
            eqcopy.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr883", "EQCopyMini.aspx.vb")

            eqcopy.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopEq(PageNumber)
        Catch ex As Exception
            eqcopy.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr884", "EQCopyMini.aspx.vb")

            eqcopy.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopEq(ByVal PageNumber As Integer)
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        'cid = "0"
        cid = "0" ' lblcid.value
        sid = lblsid.Value
        psid = lblpsid.Value
        Dim intPgCnt As Integer
        If Len(srch) = 0 Then
            srch = ""
            sql = "select count(*) from Equipment where siteid = '" & sid & "' and desig <> 2"
            intPgCnt = eqcopy.Scalar(sql)
        Else
            Dim csrch As String
            csrch = "%" & srch & "%"
            sql = "select count(*) from Equipment where siteid = '" & sid & "' and (eqnum like '" & csrch & "' or eqdesc like '" & csrch & "' or spl like '" & csrch & "') and desig <> 2"
            intPgCnt = eqcopy.Scalar(sql)
        End If
        'txtpg.Value = PageNumber
        PageNumber = txtpg.Value
        'intPgNav = eqcopy.PageCount(intPgCnt, PageSize)
        intPgNav = eqcopy.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        If Len(srch) = 0 Then
            srch = ""
        End If

        sql = "usp_getAllEqPg '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '%%', '" & sid & "'"
        dr = eqcopy.GetRdrData(sql)
        rptreq.DataSource = dr
        rptreq.DataBind()
        dr.Close()
        updatepos(intPgNav)
    End Sub
    Private Sub updatepos(ByVal intPgNav)
        PageNumber = txtpg.Value
        If intPgNav = 0 Then
            tdpg.InnerHtml = "Page 0 of 0"
        Else
            tdpg.InnerHtml = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber = 1 And intPgNav = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = False

        ElseIf PageNumber = 1 And intPgNav > 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = True

        ElseIf PageNumber = intPgNav Then
            btnnext.Enabled = False
            btnprev.Enabled = True

        Else
            btnnext.Enabled = True
            btnprev.Enabled = True
        End If
    End Sub
    Public Sub GetEq1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rb As RadioButton = New RadioButton
        Dim eqid, eqdesc, spl, loc, model, serial, oem As String
        Dim ecr, cby, mby, cdat, mdat, cph, cm, eph, em, bt, ass As String
        rb = sender
        tdcureq.InnerHtml = rb.ClientID
        For Each i As RepeaterItem In rptreq.Items
            rb = CType(i.FindControl("rbeq"), RadioButton)
            eqid = CType(i.FindControl("lbleqid"), Label).Text
            eqdesc = CType(i.FindControl("lbleqdesc"), Label).Text
            spl = CType(i.FindControl("lblspl"), Label).Text
            loc = CType(i.FindControl("lblloc"), Label).Text
            model = CType(i.FindControl("lblmod"), Label).Text
            oem = CType(i.FindControl("lbloem"), Label).Text
            serial = CType(i.FindControl("lblser"), Label).Text
            ecr = CType(i.FindControl("lblecr"), Label).Text
            cby = CType(i.FindControl("lblcby"), Label).Text
            mby = CType(i.FindControl("lblmby"), Label).Text
            cdat = CType(i.FindControl("lblcdate"), Label).Text
            mdat = CType(i.FindControl("lblmdate"), Label).Text
            cph = CType(i.FindControl("lblcph"), Label).Text
            cm = CType(i.FindControl("lblcm"), Label).Text
            eph = CType(i.FindControl("lbleph"), Label).Text
            em = CType(i.FindControl("lblem"), Label).Text
            bt = CType(i.FindControl("lblbt"), Label).Text
            ass = CType(i.FindControl("lblass"), Label).Text
            rb.Checked = False
            If tdcureq.InnerHtml = rb.ClientID Then
                tbleqrev.Attributes.Add("class", "view")
                tblrevdetails.Attributes.Add("class", "view")
                rb.Checked = True
                lbleq.Value = eqid
                tdcureq.InnerHtml = rb.Text.Trim
                tddesc.InnerHtml = eqdesc
                tdbt.InnerHtml = bt
                tdspl.InnerHtml = spl
                tdloc.InnerHtml = loc
                tdmod.InnerHtml = model
                tdoem.InnerHtml = oem
                tdecr.InnerHtml = ecr
                tdcby.InnerHtml = cby
                tdcdate.InnerHtml = cdat
                tdcphone.InnerHtml = cph
                tdcmail.InnerHtml = cm
                tdsn.InnerHtml = serial
                tdac.InnerHtml = ass
            End If
        Next
    End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        eqcopy.Open()
        PopEq(PageNumber)
        eqcopy.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprev.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        eqcopy.Open()
        PopEq(PageNumber)
        eqcopy.Dispose()
    End Sub

    Private Sub ibtncopy_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtncopy.Click
        Dim neweq As String = txtneweqnum.Text
        neweq = eqcopy.ModString3(neweq)
        'neweq = Replace(neweq, "'", Chr(180), , , vbTextCompare)
        'neweq = Replace(neweq, "--", "-", , , vbTextCompare)
        'neweq = Replace(neweq, ";", ":", , , vbTextCompare)
        eqcopy.Open()
        If Len(neweq) > 0 Then
            If Len(neweq) > 50 Then
                Dim strMessage1 As String = tmod.getmsg("cdstr885", "EQCopyMini.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
            cid = "0" 'lblcid.Value
            Dim eqcnt As Integer
            Dim mdb As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
            Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            '*** Multi Add ***

            Dim cmd0 As New SqlCommand("select count(*) from [" & srvr & "].[" & orig & "].[dbo].[equipment] where eqnum = @neweq")
            Dim param1 = New SqlParameter("@srvr", SqlDbType.VarChar)
            param1.Value = srvr
            cmd0.Parameters.Add(param1)
            Dim param2 = New SqlParameter("@mdb", SqlDbType.VarChar)
            param2.Value = mdb
            cmd0.Parameters.Add(param2)
            ''Dim param3 = New SqlParameter("@cid", SqlDbType.VarChar)
            'param3.Value = cid
            'cmd0.Parameters.Add(param3)
            Dim param4 = New SqlParameter("@neweq", SqlDbType.VarChar)
            param4.Value = neweq
            cmd0.Parameters.Add(param4)
            'eqcopy.Open()
            eqcnt = eqcopy.ScalarHack(cmd0)
            'eqcnt = eqcopy.Scalar(sql)
            Dim lid As String = lbllid.Value
            If eqcnt = 0 And lid <> "" And lid <> "0" Then
                sql = "select count(*) from pmlocations where location = '" & neweq & "'"
                eqcnt = eqcopy.Scalar(sql)
            End If
            If eqcnt = 0 Then
                txtneweqnum.Enabled = False
                'lbleqcopy.Value = "yes"
                'ibtneqcopy.Visible = False
                'ibtncancel.Visible = True
                'Dim flg As String = lbldflt.Value
                'If flg = "0" Then
                'tblchoosedept.Attributes.Add("class", "view")
                'tblrevdetails.Attributes.Add("class", "details")
                'Else
                'Try

                'Try
                DoCopy()
                'Catch ex As Exception
                'Dim strMessage2 As String = ex.Message
                'strMessage2 = Replace(strMessage2, "'", Chr(180), , , vbTextCompare) '"Problem Saving Record\nPlease review selections and try again."
                'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                'End Try

                'Catch ex As Exception
                'Dim strMessage2 As String =  tmod.getmsg("cdstr886" , "EQCopyMini.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                'End Try
                ''eqcopy.Dispose()
            Else
                'eqcopy.Dispose()
                Dim strMessage3 As String = tmod.getmsg("cdstr887", "EQCopyMini.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage3, "strKey1")
            End If
        Else
            'eqcopy.Dispose()
            Dim strMessage4 As String = tmod.getmsg("cdstr888", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage4, "strKey1")
        End If
        eqcopy.Dispose()
    End Sub
    Function DoCopy()
        Dim li As ListItem
        Dim listr As String
        Dim pm, tpm As String
        Dim tflg As Integer = 0
        If cbfunc.Checked = True Then
            listr = "f"
            If cbcomp.Checked = True Then
                listr += "c"
                If cbpm.Checked = True Then
                    listr += "t"
                    tflg = 1
                    pm = "Y"
                Else
                    pm = "N"
                End If
                If cbtpm.Checked = True Then
                    If tflg = 0 Then
                        listr += "t"
                    End If
                    tpm = "Y"
                Else
                    tpm = "N"
                End If
            End If
        End If
        Dim eqid As String = lbleq.Value
        Dim neweq As String = txtneweqnum.Text
        neweq = eqcopy.ModString3(neweq)
        'neweq = Replace(neweq, "'", Chr(180), , , vbTextCompare)
        ''neweq = Replace(neweq, " ", " ", , , vbTextCompare)
        'neweq = Replace(neweq, ";", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "/", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "\", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "&", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "#", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "$", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "%", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "!", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "~", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "^", " ", , , vbTextCompare)
        'neweq = Replace(neweq, "*", " ", , , vbTextCompare)
        'neweq = Replace(neweq, ".", " ", , , vbTextCompare)
        cid = "0" 'lblcid.Value
        Dim site, dept, cell As String
        site = lblsid.Value
        dept = lbldid.Value
        cell = lblclid.Value
        If cell = "" Then
            cell = "0"
        End If
        sid = lblsid.Value
        Dim osid As String = sid
        did = lbldid.Value
        clid = lblclid.Value
        Dim lid As String = lbllid.Value
        If cell = "none" Then
            cell = "0"
        End If
        Dim newid As Integer
        Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        Dim mdb As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")

        Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")

        Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim ecryn As String
        If cbecryn.Checked = True Then
            ecryn = "Y"
        Else
            ecryn = "N"
        End If
        Dim roa As String
        If rbro.Checked = True Then
            roa = "R"
        Else
            roa = "A"
        End If
        Dim rat As String
        If cbrat.Checked = True Then
            rat = "Y"
        Else
            rat = "N"
        End If
        Dim cmd0 As New SqlCommand("exec usp_copyEqMDB2 @cid, @site, @dept, @cell, @eqid, @neweq, @ustr, @mdb, @orig, @srvr, @ecryn, @loc")
        Dim param1 = New SqlParameter("@cid", SqlDbType.VarChar)
        param1.Value = cid
        cmd0.Parameters.Add(param1)
        Dim param2 = New SqlParameter("@site", SqlDbType.VarChar)
        param2.Value = site
        cmd0.Parameters.Add(param2)
        Dim param3 = New SqlParameter("@dept", SqlDbType.VarChar)
        param3.Value = dept
        cmd0.Parameters.Add(param3)
        Dim param4 = New SqlParameter("@cell", SqlDbType.VarChar)
        param4.Value = cell
        cmd0.Parameters.Add(param4)
        Dim param5 = New SqlParameter("@eqid", SqlDbType.VarChar)
        param5.Value = eqid
        cmd0.Parameters.Add(param5)
        Dim param6 = New SqlParameter("@neweq", SqlDbType.VarChar)
        param6.Value = neweq
        cmd0.Parameters.Add(param6)
        Dim param7 = New SqlParameter("@ustr", SqlDbType.VarChar)
        param7.Value = ustr
        cmd0.Parameters.Add(param7)
        Dim param8 = New SqlParameter("@mdb", SqlDbType.VarChar)
        param8.Value = mdb
        cmd0.Parameters.Add(param8)
        Dim param9 = New SqlParameter("@orig", SqlDbType.VarChar)
        param9.Value = orig
        cmd0.Parameters.Add(param9)
        Dim param10 = New SqlParameter("@srvr", SqlDbType.VarChar)
        param10.Value = srvr
        cmd0.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@ecryn", SqlDbType.VarChar)
        param11.Value = ecryn
        cmd0.Parameters.Add(param11)
        Dim param12 = New SqlParameter("@loc", SqlDbType.VarChar)
        param12.Value = lid
        cmd0.Parameters.Add(param12)
        newid = eqcopy.ScalarHack(cmd0)

        sql = "usp_copyEqPicsMDB2 '" & eqid & "', '" & newid & "', '" & mdb & "', '" & orig & "','" & srvr & "'"

        Dim oloc As String = ap
        Dim cloc As String = ap
        Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
        If mdb <> orig Then
            Try
                Dim ds As New DataSet
                ds = eqcopy.GetDSData(sql)
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")

                    nt = "a-eqImg" & newid & "i"
                    ntn = "atn-eqImg" & newid & "i"
                    ntm = "atm-eqImg" & newid & "i"

                    ont = "a-eqImg" & eqid & "i"
                    ontn = "atn-eqImg" & eqid & "i"
                    ontm = "atm-eqImg" & eqid & "i"

                    ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & cloc & t
                    td = Replace(td, "\", "/")

                    tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                    tns = Replace(tns, "\", "/")
                    tnd = Server.MapPath("\") & cloc & tn
                    tnd = Replace(tnd, "\", "/")

                    tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & cloc & tm
                    tmd = Replace(tmd, "\", "/")

                    '***NEED WWW CONFIG REF HERE***
                    'ts = Replace(ts, "wwwlai1", "wwwlai2")
                    'tns = Replace(ts, "wwwlai1", "wwwlai2")
                    'tms = Replace(ts, "wwwlai1", "wwwlai2")

                    System.IO.File.Copy(ts, td, True)
                    System.IO.File.Copy(tns, tnd, True)
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception

            End Try
        Else
            Try
                Dim ds As New DataSet
                ds = eqcopy.GetDSData(sql)
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")

                    nt = "a-eqImg" & newid & "i"
                    ntn = "atn-eqImg" & newid & "i"
                    ntm = "atm-eqImg" & newid & "i"

                    ont = "a-eqImg" & eqid & "i"
                    ontn = "atn-eqImg" & eqid & "i"
                    ontm = "atm-eqImg" & eqid & "i"


                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & ap & t
                    td = Replace(td, "\", "/")

                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                    tns = Replace(tns, "\", "/")
                    tnd = Server.MapPath("\") & ap & tn
                    tnd = Replace(tnd, "\", "/")

                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & ap & tm
                    tmd = Replace(tmd, "\", "/")

                    System.IO.File.Copy(ts, td, True)
                    System.IO.File.Copy(tns, tnd, True)
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception

            End Try
        End If

        'need total time copy here
        If mdb <> orig Then
            sql = "insert into [" & srvr & "].[" & mdb & "].[dbo].pmtottime (eqid, skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, maxdays, meter, meterfreq, meterid, meterunit) " _
            + "select  '" & newid & "', skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, maxdays, meter, meterfreq, meterid, meterunit " _
            + "from [" & srvr & "].[" & orig & "].[dbo].pmtottime where eqid = '" & eqid & "'"
        Else
            sql = "insert into pmtottime (eqid, skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, maxdays, meter, meterfreq, meterid, meterunit) " _
            + "select  '" & newid & "', skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, maxdays, meter, meterfreq, meterid, meterunit " _
            + "from pmtottime where eqid = '" & eqid & "'"
        End If

        Dim copy2a As New Utilities
        Try
            copy2a.Open()
            copy2a.Update(sql)
            copy2a.Dispose()
        Catch ex As Exception

        End Try

        sql = "exec usp_chkrtoneqadd '" & site & "'"
        Try
            copy2a.Open()
            copy2a.Update(sql)
            copy2a.Dispose()
        Catch ex As Exception

        End Try

        Dim flg As String
        lblnewid.Value = newid
        If listr = "fct" Then
            flg = "1"
            sql = "usp_copyAllMDB2 '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "', '" & ustr & "'"
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    ds = eqcopy.GetDSData(sql)
                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")

                        '***NEED WWW CONFIG REF HERE***
                        'ts = Replace(ts, "wwwlai1", "wwwlai2")
                        'tns = Replace(ts, "wwwlai1", "wwwlai2")
                        'tms = Replace(ts, "wwwlai1", "wwwlai2")

                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
                'insert pm task image file copy here for mdb *********************************************

            Else
                Try
                    Dim ds As New DataSet
                    ds = eqcopy.GetDSData(sql)
                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")

                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)

                    Next
                Catch ex As Exception

                End Try
                'insert pm task image file copy here for local
                Try
                    sql = "select pic_id, parfuncid, funcid, oldfuncid, pm_image, pm_image_med, pm_image_thumb from pmimages where funcid in " _
                    + "(select func_id from functions where eqid = '" & newid & "')"
                    Dim dsp As New DataSet
                    dsp = eqcopy.GetDSData(sql)
                    Dim fid1, pfid1 As String
                    oloc = ap
                    Dim i1 As Integer
                    Dim f1 As Integer = 0
                    Dim c1 As Integer = 0
                    Dim ot, otn, otm As String
                    Dim pict, pictn, pictm As String
                    Dim opict, opictn, opictm As String
                    Dim x1 As Integer = dsp.Tables(0).Rows.Count
                    Dim ofid1 As String
                    Dim newt, newtn, newtm

                    For i1 = 0 To (x1 - 1)
                        pid = dsp.Tables(0).Rows(i1)("pic_id").ToString
                        fid1 = dsp.Tables(0).Rows(i1)("funcid").ToString
                        ofid1 = dsp.Tables(0).Rows(i1)("oldfuncid").ToString
                        pfid1 = dsp.Tables(0).Rows(i1)("parfuncid").ToString
                        t = dsp.Tables(0).Rows(i1)("pm_image").ToString
                        tn = dsp.Tables(0).Rows(i1)("pm_image_thumb").ToString
                        tm = dsp.Tables(0).Rows(i1)("pm_image_med").ToString
                        Dim intFileNameLength As Integer
                        intFileNameLength = InStr(1, StrReverse(t), "/")
                        t = Mid(t, (Len(t) - intFileNameLength) + 2)
                        intFileNameLength = InStr(1, StrReverse(tn), "/")
                        tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                        intFileNameLength = InStr(1, StrReverse(tm), "/")
                        tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                        t = appstr & "/pmimages/" & t
                        tn = appstr & "/pmimages/" & tn
                        tm = appstr & "/pmimages/" & tm

                        t = Replace(t, "http://localhost/", "")
                        tn = Replace(tn, "http://localhost/", "")
                        tm = Replace(tm, "http://localhost/", "")

                        t = Replace(t, "http://", "")
                        tn = Replace(tn, "http://", "")
                        tm = Replace(tm, "http://", "")

                        ts = Replace(t, "g" & fid1 & "-", "g" & ofid1 & "-")
                        tns = Replace(tn, "g" & fid1 & "-", "g" & ofid1 & "-")
                        tms = Replace(tm, "g" & fid1 & "-", "g" & ofid1 & "-")

                        td = Replace(t, "g" & fid1 & "-", "g" & fid1 & "-")
                        tnd = Replace(tn, "g" & fid1 & "-", "g" & fid1 & "-")
                        tmd = Replace(tm, "g" & fid1 & "-", "g" & fid1 & "-")


                        If oloc <> "" Then
                            ts = Replace(ts, ap, oloc)
                            tns = Replace(tns, ap, oloc)
                            tms = Replace(tms, ap, oloc)

                            td = Replace(td, ap, oloc)
                            tnd = Replace(tnd, ap, oloc)
                            tmd = Replace(tmd, ap, oloc)
                        End If


                        ts = Server.MapPath("\") & ts
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & td
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & tns
                        tns = Replace(tns, "\", "/")
                        tnd = Server.MapPath("\") & tnd
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & tms
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & tmd
                        tmd = Replace(tmd, "\", "/")

                        'System.IO.File.Copy(source, dest)
                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)


                    Next

                Catch ex As Exception

                End Try

            End If
            'rbro, rbai

            '2MDB includes switch for Revised or As is
            sql = "usp_copyEqTasks2MDB '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & newid & "', '" & ustr & "', '" & mdb & "', '" & orig & "','" & pm & "','" & tpm & "', '" & ap & "','" & oloc & "','" & srvr & "','" & roa & "','" & rat & "'"
            eqcopy.Update(sql)
            'need to put pm task images here !!!Did this with function call above

            '***Added to speed up result time
            If sid <> osid Then
                sql = "usp_updateSkillsNew '" & sid & "','" & newid & "'"
                eqcopy.Update(sql)
                sql = "usp_updateSiteFM '" & cid & "', '" & sid & "'"
                eqcopy.Update(sql)
            End If
            sql = "iusp_updateCompFailId '" & newid & "'"
            eqcopy.Update(sql)

            '***

            If tpm = "Y" Then
                sql = "select pic_id, parfuncid, funcid, tpm_image, tpm_image_med, tpm_image_thumb from tpmimages where funcid in " _
                + "(select func_id from functions where eqid = '" & newid & "')"
                If mdb <> orig Then
                    Try
                        Dim ds As New DataSet
                        ds = eqcopy.GetDSData(sql)
                        Dim fid, pfid As String
                        oloc = ap
                        Dim i As Integer
                        Dim f As Integer = 0
                        Dim c As Integer = 0
                        Dim ot, otn, otm As String
                        Dim pict, pictn, pictm As String
                        Dim opict, opictn, opictm As String
                        Dim x As Integer = ds.Tables(0).Rows.Count
                        For i = 0 To (x - 1)
                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                            pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                            t = ds.Tables(0).Rows(i)("tpm_image").ToString
                            tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                            tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                            Dim intFileNameLength As Integer
                            intFileNameLength = InStr(1, StrReverse(t), "/")
                            t = Mid(t, (Len(t) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tn), "/")
                            tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tm), "/")
                            tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                            t = appstr & "/tpmimages/" & t
                            tn = appstr & "/tpmimages/" & tn
                            tm = appstr & "/tpmimages/" & tm

                            t = Replace(t, "http://localhost/", "")
                            tn = Replace(tn, "http://localhost/", "")
                            tm = Replace(tm, "http://localhost/", "")

                            t = Replace(t, "http://", "")
                            tn = Replace(tn, "http://", "")
                            tm = Replace(tm, "http://", "")

                            pict = "a-newsImg" + fid
                            pictn = "atn-newsImg" + fid
                            pictm = "atm-newsImg" + fid

                            opict = "a-newsImg" + pfid
                            opictn = "atn-newsImg" + pfid
                            opictm = "atm-newsImg" + pfid

                            ts = Replace(t, pict, opict)
                            tns = Replace(tn, pictn, opictn)
                            tms = Replace(tm, pictm, opictm)

                            If oloc <> "" Then
                                ts = Replace(t, ap, oloc)
                                tns = Replace(tn, ap, oloc)
                                tms = Replace(tm, ap, oloc)
                            End If

                            ts = Server.MapPath("\") & ts
                            ts = Replace(ts, "\", "/")
                            td = Server.MapPath("\") & t
                            td = Replace(td, "\", "/")


                            tns = Server.MapPath("\") & tns
                            tns = Replace(tns, "\", "/")
                            tnd = Server.MapPath("\") & tn
                            tnd = Replace(tnd, "\", "/")


                            tms = Server.MapPath("\") & tms
                            tms = Replace(tms, "\", "/")
                            tmd = Server.MapPath("\") & tm
                            tmd = Replace(tmd, "\", "/")

                            System.IO.File.Copy(ts, td, True)
                            System.IO.File.Copy(tns, tnd, True)
                            System.IO.File.Copy(tms, tmd, True)


                        Next
                    Catch ex As Exception

                    End Try
                Else
                    Try
                        Dim ds As New DataSet
                        ds = eqcopy.GetDSData(sql)
                        Dim fid, pfid As String
                        Dim i As Integer
                        Dim f As Integer = 0
                        Dim c As Integer = 0
                        Dim ot, otn, otm As String
                        Dim pict, pictn, pictm As String
                        Dim opict, opictn, opictm As String
                        Dim x As Integer = ds.Tables(0).Rows.Count
                        For i = 0 To (x - 1)
                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                            pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                            t = ds.Tables(0).Rows(i)("tpm_image").ToString
                            tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                            tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                            Dim intFileNameLength As Integer
                            intFileNameLength = InStr(1, StrReverse(t), "/")
                            t = Mid(t, (Len(t) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tn), "/")
                            tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tm), "/")
                            tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                            t = appstr & "/tpmimages/" & t
                            tn = appstr & "/tpmimages/" & tn
                            tm = appstr & "/tpmimages/" & tm

                            t = Replace(t, "http://localhost/", "")
                            tn = Replace(tn, "http://localhost/", "")
                            tm = Replace(tm, "http://localhost/", "")

                            t = Replace(t, "http://", "")
                            tn = Replace(tn, "http://", "")
                            tm = Replace(tm, "http://", "")

                            pict = "a-newsImg" + fid
                            pictn = "atn-newsImg" + fid
                            pictm = "atm-newsImg" + fid

                            opict = "a-newsImg" + pfid
                            opictn = "atn-newsImg" + pfid
                            opictm = "atm-newsImg" + pfid

                            ts = Replace(t, pict, opict)
                            tns = Replace(tn, pictn, opictn)
                            tms = Replace(tm, pictm, opictm)

                            ts = Server.MapPath("\") & ts
                            ts = Replace(ts, "\", "/")
                            td = Server.MapPath("\") & t
                            td = Replace(td, "\", "/")
                            System.IO.File.Copy(ts, td, True)

                            tns = Server.MapPath("\") & tns
                            tns = Replace(tns, "\", "/")
                            tnd = Server.MapPath("\") & tn
                            tnd = Replace(tnd, "\", "/")
                            System.IO.File.Copy(tns, tnd, True)

                            tms = Server.MapPath("\") & tms
                            tms = Replace(tms, "\", "/")
                            tmd = Server.MapPath("\") & tm
                            tmd = Replace(tmd, "\", "/")
                            System.IO.File.Copy(tms, tmd, True)
                        Next
                    Catch ex As Exception

                    End Try
                End If

            End If
        ElseIf listr = "fc" Then
            flg = "1"
            sql = "usp_copyAllMDB '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & ustr & "'"
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    ds = eqcopy.GetDSData(sql)
                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
            Else
                Try
                    Dim ds As New DataSet
                    ds = eqcopy.GetDSData(sql)
                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
            End If
        ElseIf listr = "f" Then
            flg = "0"
            'sql = "usp_copyAll '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "'"
            sql = "usp_copyAllMDB '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & ustr & "'"
            'eqcopy.Update(sql)
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    ds = eqcopy.GetDSData(sql)
                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
            Else
                Try
                    Dim ds As New DataSet
                    ds = eqcopy.GetDSData(sql)
                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
            End If
        Else
        End If

        sql = "usp_copyProcsMDB '" & eqid & "', '" & newid & "', '" & mdb & "', '" & orig & "','" & srvr & "'"

        If mdb <> orig Then
            Try
                Dim ds As New DataSet
                ds = eqcopy.GetDSData(sql)
                Dim f, fs, fd As String
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    f = ds.Tables(0).Rows(i)("filename").ToString
                    fs = Server.MapPath("\") & oloc & "/eqimages/" & f
                    fd = Server.MapPath("\") & cloc & "/eqimages/" & f
                    System.IO.File.Copy(fs, fd, True)
                Next
            Catch ex As Exception

            End Try
        Else
            Try
                Dim ds As New DataSet
                ds = eqcopy.GetDSData(sql)
                Dim f, fs, fd As String
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    f = ds.Tables(0).Rows(i)("filename").ToString
                    fs = Server.MapPath("\") & ap & "/eqimages/" & f
                    fd = Server.MapPath("\") & ap & "/eqimages/" & f
                    System.IO.File.Copy(fs, fd, True)
                Next
            Catch ex As Exception

            End Try
        End If


       

        Try
            PopNewEq(newid)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr889", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        rptreq.DataSource = Nothing
        rptreq.DataBind()
        txtpg.Value = "1"
        lblcopyflg.Value = flg
        txtneweqnum.Text = ""
        tbltop.Attributes.Add("class", "details")
        tbleqrev.Attributes.Add("class", "details")
        tblrevdetails.Attributes.Add("class", "details")
        tblneweq.Attributes.Add("class", "view")

    End Function
    Private Sub PopNewEq(ByVal newid As Integer)
        LoadStatus()
        sql = "usp_getNewEqRecord '" & newid & "'"
        dr = eqcopy.GetRdrData(sql)
        While dr.Read
            tdneweq.InnerHtml = dr.Item("eqnum").ToString
            txteqdesc.Text = dr.Item("eqdesc").ToString
            txteqspl.Text = dr.Item("spl").ToString
            txteqloc.Text = dr.Item("location").ToString
            txteqmod.Text = dr.Item("model").ToString
            txteqsn.Text = dr.Item("serial").ToString
            txtfpc.Text = dr.Item("fpc").ToString
            Try
                ddoem.SelectedValue = dr.Item("oem").ToString
            Catch ex As Exception

            End Try

            lblnewid.Value = dr.Item("eqid").ToString
            txteqecr.Text = dr.Item("ecr").ToString
            tdnewcby.InnerHtml = dr.Item("createdby").ToString
            tdnewcbydate.InnerHtml = dr.Item("createdate").ToString
            tdnewcbyphone.InnerHtml = dr.Item("cph").ToString
            tdnewcbymail.InnerHtml = dr.Item("cm").ToString
            Try
                ddlstat.SelectedValue = dr.Item("statid").ToString
            Catch ex As Exception
                Try
                    ddlstat.SelectedIndex = 0
                Catch ex1 As Exception

                End Try

            End Try

        End While
        dr.Close()

    End Sub
    Private Sub LoadStatus()
        cid = "0" ' lblcid.value
        sql = "select * from eqstatus"
        dr = eqcopy.GetRdrData(sql)
        ddlstat.DataSource = dr
        ddlstat.DataTextField = "status"
        ddlstat.DataValueField = "statid"
        ddlstat.DataBind()
        dr.Close()
        ddlstat.Items.Insert(0, "Select Status")
    End Sub
    Private Sub ibtnsaveneweq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsaveneweq.Click
        Dim eqd, eqs, eql, eqm, eqsn, eqo, eqi, eqe, estat As String
        eqd = txteqdesc.Text
        eqd = Replace(eqd, "'", Chr(180), , , vbTextCompare)
        eqd = Replace(eqd, "--", "-", , , vbTextCompare)
        eqd = Replace(eqd, ";", ":", , , vbTextCompare)
        If Len(eqd) = 0 Then eqd = ""
        If Len(eqd) > 100 Then
            Dim strMessage As String = tmod.getmsg("cdstr890", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqs = txteqspl.Text
        eqs = Replace(eqs, "'", Chr(180), , , vbTextCompare)
        eqs = Replace(eqs, "--", "-", , , vbTextCompare)
        eqs = Replace(eqs, ";", ":", , , vbTextCompare)
        If Len(eqs) = 0 Then eqs = ""
        If Len(eqs) > 250 Then
            Dim strMessage As String = tmod.getmsg("cdstr891", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eql = txteqloc.Text
        eql = Replace(eql, "'", Chr(180), , , vbTextCompare)
        eql = Replace(eql, "--", "-", , , vbTextCompare)
        eql = Replace(eql, ";", ":", , , vbTextCompare)
        If Len(eql) = 0 Then eql = ""
        If Len(eql) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr892", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqm = txteqmod.Text
        eqm = Replace(eqm, "'", Chr(180), , , vbTextCompare)
        eqm = Replace(eqm, "--", "-", , , vbTextCompare)
        eqm = Replace(eqm, ";", ":", , , vbTextCompare)
        If Len(eqm) = 0 Then eqm = ""
        If Len(eqm) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr893", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqsn = txteqsn.Text
        eqsn = Replace(eqsn, "'", Chr(180), , , vbTextCompare)
        eqsn = Replace(eqsn, "--", "-", , , vbTextCompare)
        eqsn = Replace(eqsn, ";", ":", , , vbTextCompare)
        If Len(eqsn) = 0 Then eqsn = ""
        If Len(eqsn) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr894", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        'eqo = txteqoem.Text
        eqo = ddoem.SelectedValue.ToString
        eqo = Replace(eqo, "'", Chr(180), , , vbTextCompare)
        eqo = Replace(eqo, "'", Chr(180), , , vbTextCompare)
        eqo = Replace(eqo, "--", "-", , , vbTextCompare)
        eqo = Replace(eqo, ";", ":", , , vbTextCompare)
        If Len(eqo) = 0 Then eqo = ""
        If Len(eqo) > 50 Then
            Dim strMessage As String = tmod.getmsg("cdstr895", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqi = lblnewid.Value
        eqe = txteqecr.Text
        Dim eqechk As Long
        Try
            eqechk = System.Convert.ToDecimal(eqe)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr896", "EQCopyMini.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If ddlstat.SelectedIndex <> 0 Then
            estat = ddlstat.SelectedValue.ToString
        Else
            estat = "0"
        End If
        Dim fpc As String = txtfpc.Text
        fpc = eqcopy.ModString3(fpc)
        If Len(fpc) > 50 Then
            Dim strMessage As String = "Column Number Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If

        eqcopy.Open()
        sql = "usp_saveNewEq '" & eqd & "', '" & eqs & "', '" & eql & "', '" & eqm & "', " _
        + "'" & eqsn & "', '" & eqo & "', '" & eqi & "', '" & eqe & "', '" & estat & "', '" & fpc & "', 0"
        'Try
        eqcopy.Update(sql)
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr897" , "EQCopyMini.aspx.vb")

        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End Try
        txtpg.Value = "1"
        PageNumber = "1"
        PopEq(PageNumber)
        eqcopy.Dispose()
        tbltop.Attributes.Add("class", "view")
        'tbleqrev.Attributes.Add("class", "view")
        'tblrevdetails.Attributes.Add("class", "view")
        tblneweq.Attributes.Add("class", "details")
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        tbleqrev.Attributes.Add("class", "details")
        tblrevdetails.Attributes.Add("class", "details")
        tblneweq.Attributes.Add("class", "details")

        txtpg.Value = "1"
        PageNumber = txtpg.Value
        eqcopy.Open()
        PopEq(PageNumber)
        eqcopy.Dispose()
    End Sub

    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                                e.Item.ItemType <> ListItemType.Footer Then
            Dim pic As HtmlImage = CType(e.Item.FindControl("imgpic"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim cnt As String = DataBinder.Eval(e.Item.DataItem, "epcnt").ToString
            If cnt <> "0" Then
                pic.Attributes.Add("onclick", "geteqport('" & id & "');")
                pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpic.gif")
            Else
                pic.Attributes.Add("onclick", "")
                pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpicdis.gif")
            End If
        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2202 As Label
                lang2202 = CType(e.Item.FindControl("lang2202"), Label)
                lang2202.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2202")
            Catch ex As Exception
            End Try
            Try
                Dim lang2203 As Label
                lang2203 = CType(e.Item.FindControl("lang2203"), Label)
                lang2203.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2203")
            Catch ex As Exception
            End Try
            Try
                Dim lang2204 As Label
                lang2204 = CType(e.Item.FindControl("lang2204"), Label)
                lang2204.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2204")
            Catch ex As Exception
            End Try
            Try
                Dim lang2205 As Label
                lang2205 = CType(e.Item.FindControl("lang2205"), Label)
                lang2205.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2205")
            Catch ex As Exception
            End Try
            Try
                Dim lang2206 As Label
                lang2206 = CType(e.Item.FindControl("lang2206"), Label)
                lang2206.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2206")
            Catch ex As Exception
            End Try
            Try
                Dim lang2207 As Label
                lang2207 = CType(e.Item.FindControl("lang2207"), Label)
                lang2207.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2207")
            Catch ex As Exception
            End Try
            Try
                Dim lang2208 As Label
                lang2208 = CType(e.Item.FindControl("lang2208"), Label)
                lang2208.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2208")
            Catch ex As Exception
            End Try
            Try
                Dim lang2209 As Label
                lang2209 = CType(e.Item.FindControl("lang2209"), Label)
                lang2209.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2209")
            Catch ex As Exception
            End Try
            Try
                Dim lang2210 As Label
                lang2210 = CType(e.Item.FindControl("lang2210"), Label)
                lang2210.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2210")
            Catch ex As Exception
            End Try
            Try
                Dim lang2211 As Label
                lang2211 = CType(e.Item.FindControl("lang2211"), Label)
                lang2211.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2211")
            Catch ex As Exception
            End Try
            Try
                Dim lang2212 As Label
                lang2212 = CType(e.Item.FindControl("lang2212"), Label)
                lang2212.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2212")
            Catch ex As Exception
            End Try
            Try
                Dim lang2213 As Label
                lang2213 = CType(e.Item.FindControl("lang2213"), Label)
                lang2213.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2213")
            Catch ex As Exception
            End Try
            Try
                Dim lang2214 As Label
                lang2214 = CType(e.Item.FindControl("lang2214"), Label)
                lang2214.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2214")
            Catch ex As Exception
            End Try
            Try
                Dim lang2215 As Label
                lang2215 = CType(e.Item.FindControl("lang2215"), Label)
                lang2215.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2215")
            Catch ex As Exception
            End Try
            Try
                Dim lang2216 As Label
                lang2216 = CType(e.Item.FindControl("lang2216"), Label)
                lang2216.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2216")
            Catch ex As Exception
            End Try
            Try
                Dim lang2217 As Label
                lang2217 = CType(e.Item.FindControl("lang2217"), Label)
                lang2217.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2217")
            Catch ex As Exception
            End Try
            Try
                Dim lang2218 As Label
                lang2218 = CType(e.Item.FindControl("lang2218"), Label)
                lang2218.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2218")
            Catch ex As Exception
            End Try
            Try
                Dim lang2219 As Label
                lang2219 = CType(e.Item.FindControl("lang2219"), Label)
                lang2219.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2219")
            Catch ex As Exception
            End Try
            Try
                Dim lang2220 As Label
                lang2220 = CType(e.Item.FindControl("lang2220"), Label)
                lang2220.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2220")
            Catch ex As Exception
            End Try
            Try
                Dim lang2221 As Label
                lang2221 = CType(e.Item.FindControl("lang2221"), Label)
                lang2221.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2221")
            Catch ex As Exception
            End Try
            Try
                Dim lang2222 As Label
                lang2222 = CType(e.Item.FindControl("lang2222"), Label)
                lang2222.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2222")
            Catch ex As Exception
            End Try
            Try
                Dim lang2223 As Label
                lang2223 = CType(e.Item.FindControl("lang2223"), Label)
                lang2223.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2223")
            Catch ex As Exception
            End Try
            Try
                Dim lang2224 As Label
                lang2224 = CType(e.Item.FindControl("lang2224"), Label)
                lang2224.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2224")
            Catch ex As Exception
            End Try
            Try
                Dim lang2225 As Label
                lang2225 = CType(e.Item.FindControl("lang2225"), Label)
                lang2225.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2225")
            Catch ex As Exception
            End Try
            Try
                Dim lang2226 As Label
                lang2226 = CType(e.Item.FindControl("lang2226"), Label)
                lang2226.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2226")
            Catch ex As Exception
            End Try
            Try
                Dim lang2227 As Label
                lang2227 = CType(e.Item.FindControl("lang2227"), Label)
                lang2227.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2227")
            Catch ex As Exception
            End Try
            Try
                Dim lang2228 As Label
                lang2228 = CType(e.Item.FindControl("lang2228"), Label)
                lang2228.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2228")
            Catch ex As Exception
            End Try
            Try
                Dim lang2229 As Label
                lang2229 = CType(e.Item.FindControl("lang2229"), Label)
                lang2229.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2229")
            Catch ex As Exception
            End Try
            Try
                Dim lang2230 As Label
                lang2230 = CType(e.Item.FindControl("lang2230"), Label)
                lang2230.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2230")
            Catch ex As Exception
            End Try
            Try
                Dim lang2231 As Label
                lang2231 = CType(e.Item.FindControl("lang2231"), Label)
                lang2231.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2231")
            Catch ex As Exception
            End Try
            Try
                Dim lang2232 As Label
                lang2232 = CType(e.Item.FindControl("lang2232"), Label)
                lang2232.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2232")
            Catch ex As Exception
            End Try
            Try
                Dim lang2233 As Label
                lang2233 = CType(e.Item.FindControl("lang2233"), Label)
                lang2233.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2233")
            Catch ex As Exception
            End Try
            Try
                Dim lang2234 As Label
                lang2234 = CType(e.Item.FindControl("lang2234"), Label)
                lang2234.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2234")
            Catch ex As Exception
            End Try
            Try
                Dim lang2235 As Label
                lang2235 = CType(e.Item.FindControl("lang2235"), Label)
                lang2235.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2235")
            Catch ex As Exception
            End Try
            Try
                Dim lang2236 As Label
                lang2236 = CType(e.Item.FindControl("lang2236"), Label)
                lang2236.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2236")
            Catch ex As Exception
            End Try
            Try
                Dim lang2237 As Label
                lang2237 = CType(e.Item.FindControl("lang2237"), Label)
                lang2237.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2237")
            Catch ex As Exception
            End Try
            Try
                Dim lang2238 As Label
                lang2238 = CType(e.Item.FindControl("lang2238"), Label)
                lang2238.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2238")
            Catch ex As Exception
            End Try
            Try
                Dim lang2239 As Label
                lang2239 = CType(e.Item.FindControl("lang2239"), Label)
                lang2239.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2239")
            Catch ex As Exception
            End Try
            Try
                Dim lang2240 As Label
                lang2240 = CType(e.Item.FindControl("lang2240"), Label)
                lang2240.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2240")
            Catch ex As Exception
            End Try

        End If

    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2202.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2202")
        Catch ex As Exception
        End Try
        Try
            lang2203.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2203")
        Catch ex As Exception
        End Try
        Try
            lang2204.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2204")
        Catch ex As Exception
        End Try
        Try
            lang2205.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2205")
        Catch ex As Exception
        End Try
        Try
            lang2206.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2206")
        Catch ex As Exception
        End Try
        Try
            lang2207.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2207")
        Catch ex As Exception
        End Try
        Try
            lang2208.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2208")
        Catch ex As Exception
        End Try
        Try
            lang2209.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2209")
        Catch ex As Exception
        End Try
        Try
            lang2210.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2210")
        Catch ex As Exception
        End Try
        Try
            lang2211.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2211")
        Catch ex As Exception
        End Try
        Try
            lang2212.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2212")
        Catch ex As Exception
        End Try
        Try
            lang2213.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2213")
        Catch ex As Exception
        End Try
        Try
            lang2214.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2214")
        Catch ex As Exception
        End Try
        Try
            lang2215.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2215")
        Catch ex As Exception
        End Try
        Try
            lang2216.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2216")
        Catch ex As Exception
        End Try
        Try
            lang2217.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2217")
        Catch ex As Exception
        End Try
        Try
            lang2218.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2218")
        Catch ex As Exception
        End Try
        Try
            lang2219.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2219")
        Catch ex As Exception
        End Try
        Try
            lang2220.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2220")
        Catch ex As Exception
        End Try
        Try
            lang2221.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2221")
        Catch ex As Exception
        End Try
        Try
            lang2222.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2222")
        Catch ex As Exception
        End Try
        Try
            lang2223.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2223")
        Catch ex As Exception
        End Try
        Try
            lang2224.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2224")
        Catch ex As Exception
        End Try
        Try
            lang2225.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2225")
        Catch ex As Exception
        End Try
        Try
            lang2226.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2226")
        Catch ex As Exception
        End Try
        Try
            lang2227.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2227")
        Catch ex As Exception
        End Try
        Try
            lang2228.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2228")
        Catch ex As Exception
        End Try
        Try
            lang2229.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2229")
        Catch ex As Exception
        End Try
        Try
            lang2230.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2230")
        Catch ex As Exception
        End Try
        Try
            lang2231.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2231")
        Catch ex As Exception
        End Try
        Try
            lang2232.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2232")
        Catch ex As Exception
        End Try
        Try
            lang2233.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2233")
        Catch ex As Exception
        End Try
        Try
            lang2234.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2234")
        Catch ex As Exception
        End Try
        Try
            lang2235.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2235")
        Catch ex As Exception
        End Try
        Try
            lang2236.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2236")
        Catch ex As Exception
        End Try
        Try
            lang2237.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2237")
        Catch ex As Exception
        End Try
        Try
            lang2238.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2238")
        Catch ex As Exception
        End Try
        Try
            lang2239.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2239")
        Catch ex As Exception
        End Try
        Try
            lang2240.Text = axlabs.GetASPXPage("EQCopyMini.aspx", "lang2240")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.Value
        Try
            If lang = "eng" Then
                ibtncopy.Attributes.Add("src", "../images2/eng/bgbuttons/copy.gif")
            ElseIf lang = "fre" Then
                ibtncopy.Attributes.Add("src", "../images2/fre/bgbuttons/copy.gif")
            ElseIf lang = "ger" Then
                ibtncopy.Attributes.Add("src", "../images2/ger/bgbuttons/copy.gif")
            ElseIf lang = "ita" Then
                ibtncopy.Attributes.Add("src", "../images2/ita/bgbuttons/copy.gif")
            ElseIf lang = "spa" Then
                ibtncopy.Attributes.Add("src", "../images2/spa/bgbuttons/copy.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddasset.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("EQCopyMini.aspx", "btnaddasset") & "')")
            btnaddasset.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
