<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ECMError.aspx.vb" Inherits="lucy_r12.ECMError" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>Error Finding Export Application</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table align="center">
				<tr>
					<td width="5%">&nbsp;</td>
					<td width="90%" class="label"><br><asp:Label id="lang2071" runat="server">No export application for the requested RCM Software was detected</asp:Label><br>
						<br><asp:Label id="lang2072" runat="server">LAI Reliability Systems provides custom export solutions for various RCM applications</asp:Label><br>
						<br><asp:Label id="lang2073" runat="server">If you have an external RCM application that you would like to interface with this application please contact LAI Reliability Systems for more information.</asp:Label><br>
						<br><asp:Label id="lang2074" runat="server">If you believe that you received this message in error please contact your PM System Administrator</asp:Label><br>
					</td>
					<td width="5%">&nbsp;</td>
				</tr>
			</table>
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
