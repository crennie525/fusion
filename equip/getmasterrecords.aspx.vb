﻿Imports System.Data.SqlClient
Imports System.Text
Public Class getmasterrecords
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim lu As New Utilities
    Dim list, sid, retid, retid1, retid2, retid3, retid4, retid5, retid6, eq, pm, jp, did, clid, locid, nc, eqid As String
    Dim filter As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            retid = Request.QueryString("eqid").ToString
            lblid.Value = retid
            lu.Open()
            GetRecords()
            lu.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                lu.Open()
                GetRecords()
                lu.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                lu.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                list = lbllist.Value
                GetRecords()

                lu.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                lu.Open()
                GetRecords()
                lu.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                lu.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                list = lbllist.Value
                GetRecords()
                lu.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "sav" Then
                lu.Open()
                eqid = lbleq.Value
                SaveRecord(eqid)
                lu.Dispose()
                lblret.Value = "return"
            End If
        End If
    End Sub
    Private Sub SaveRecord(ByVal eqid As String)
        retid = lblid.Value
        sql = "update equipment set desigmstr = '" & eqid & "' where eqid = '" & retid & "'"
        lu.Update(sql)
    End Sub
    Private Sub GetRecords()
        Dim filt, s, e, Filter, FilterCNT, srch, eqid, eqnum, bc, eqdesc As String
        Dim rowid As Integer
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim sb As StringBuilder = New StringBuilder
        Filter = ""
        FilterCNT = "%"
        If srch <> "" Then
            Filter += "%" & srch & "%"
            FilterCNT += "%" & srch & "%"
        End If

        sql = "select count(*) from equipment e where (eqnum like '" & FilterCNT & "' or eqdesc like '" & FilterCNT & "' or spl like '" & FilterCNT & "') and e.desig = 1"

        Dim intPgCnt, intPgNav As Integer
        intPgCnt = lu.Scalar(sql)
        If intPgCnt > 0 Then
            txtpg.Value = PageNumber
            intPgNav = lu.PageCountRev(intPgCnt, PageSize)
            txtpgcnt.Value = intPgNav
            If intPgNav = 0 Then
                lblpg.Text = "Page 0 of 0"
            Else
                lblpg.Text = "Page " & PageNumber & " of " & intPgNav
            End If
            sql = "usp_getAllMasterPg '" & PageNumber & "', '" & PageSize & "', '" & Filter & "', '" & sid & "' "
            sb.Append("<table width=""500"">")
            sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""500"">Equipment Number</td></tr>")
            sb.Append("<tr><td><div style=""OVERFLOW: auto; width: 500px; height: 340px;"">")
            sb.Append("<table width=""500"">")
            sb.Append("<tr><td width=""500""></td></tr>")
            dr = lu.GetRdrData(sql)
            While dr.Read
                If rowid = 0 Then
                    rowid = 1
                    bc = "white"
                Else
                    rowid = 0
                    bc = "#E7F1FD"
                End If
                eqid = dr.Item("eqid").ToString
                eqnum = dr.Item("eqnum").ToString
                eqdesc = dr.Item("eqdesc").ToString
                sb.Append("<tr bgcolor=""" & bc & """>")
                sb.Append("<td class=""plainlabel""><a href=""#"" class=""linklabel"" onclick=""pmret('" & eqid & "');"">")
                sb.Append(eqnum & "</a>&nbsp;&nbsp;" & eqdesc & "</td></tr>")


            End While
            dr.Close()
            sb.Append("</table></div></td></tr></table>")
            tdlist.InnerHtml = sb.ToString
        End If
    End Sub

    Protected Sub btnsrch1_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnsrch1.Click
        PageNumber = 1
        txtpg.Value = "1"
        lu.Open()
        GetRecords()
        lu.Dispose()
    End Sub
End Class