﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqchildselectdialog.aspx.vb" Inherits="lucy_r12.eqchildselectdialog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Equipment Child Select Dialog</title>
    <script  src="../scripts/sessrefdialog.js" type="text/javascript"></script>
    <script  type="text/javascript">
        function handleexit() {
            window.returnValue = "yes";
            window.close();
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <iframe id="ifeq" runat="server" width="800px" height="550px" frameborder="0"></iframe>
	<iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    <input type="hidden" id="lblsessrefresh" runat="server" />		
    <input type="hidden" id="lbllog" runat="server" />
    </form>
</body>
</html>
