<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibnewtpmtaskdets.aspx.vb" Inherits="lucy_r12.pmlibnewtpmtaskdets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibnewtpmtaskdets</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  src="../scripts1/pmlibnewtpmtaskdetsaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     
	</HEAD>
	<body  onload="setref();">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px">
				<TBODY>
					<tr id="trmsg" runat="server" height="30" class="details">
						<td colspan="7" id="tdmsg" runat="server" class="plainlabelblue"></td>
					</tr>
					<tr>
						<td colSpan="7">
							<div style="OVERFLOW: auto; WIDTH: 990px; HEIGHT: 260px"><asp:repeater id="rptrtaskrev" Runat="server">
									<HeaderTemplate>
										<table width="1280">
											<tr>
												<td align="center" style="width: 20px" class="btmmenu plainlabel"><img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16"></td>
												<td width="40" class="btmmenu plainlabel"><asp:Label id="lang2503" runat="server">Task#</asp:Label></td>
												<td width="70" class="btmmenu plainlabel"><asp:Label id="lang2504" runat="server">Sub Task#</asp:Label></td>
												<td width="200" class="btmmenu plainlabel"><asp:Label id="lang2505" runat="server">Component Addressed</asp:Label></td>
												<td width="30" class="btmmenu plainlabel">Qty</td>
												<td width="220" class="btmmenu plainlabel"><asp:Label id="lang2506" runat="server">Failure Modes Addressed</asp:Label></td>
												<td width="260" class="btmmenu plainlabel"><asp:Label id="lang2507" runat="server">Task Description</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2508" runat="server">Task Type</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2509" runat="server">Operator Qty</asp:Label></td>
												<td width="40" class="btmmenu plainlabel"><asp:Label id="lang2510" runat="server">Time</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2511" runat="server">Frequency</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2512" runat="server">Eq Status</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2513" runat="server">Down Time</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg">
											<td class="plainlabel">
												<asp:CheckBox id="cb4" runat="server"></asp:CheckBox></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' Runat = server ID="lbltasknum">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"subtask")%>' Runat = server ID="lblsubtasknum">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server ID="Label11">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"cqty")%>' Runat = server ID="Label12">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"fm1")%>' Runat = server ID="Label13">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' Runat = server ID="Label14">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasktype")%>' Runat = server ID="Label15">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"qty")%>' Runat = server ID="Label18">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"ttime")%>' Runat = server ID="Label19">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"freq")%>' Runat = server ID="Label20">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rd")%>' Runat = server ID="Label21">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rdt")%>' Runat = server ID="Label22">
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD></TR>
			<tr>
				<td colSpan="7"><asp:imagebutton id="remtask" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"></asp:imagebutton></td>
			</tr>
			</TBODY></TABLE><input id="lbldb" type="hidden" name="lbldb" runat="server"> <input id="lblfuid" type="hidden" name="lblfuid" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
