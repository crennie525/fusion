﻿Imports System.Data.SqlClient
Public Class ecadd
    Inherits System.Web.UI.Page
    Dim eca As New Utilities
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 20
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim ecd1, ecd2, ecd3, ecd1id, ecd2id, ecd3id As String
    Dim e1desc, e2desc, e3desc As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ecd1 = Request.QueryString("ecd1").ToString
            lblecd1.Value = ecd1
            tdec1.InnerHtml = ecd1
            e1desc = Request.QueryString("e1desc").ToString
            lble1desc.Value = e1desc
            tdedesc1.InnerHtml = e1desc

            eca.Open()
            getecd1(ecd1, e1desc)
            ecd1id = lblecd1id.Value
            getecd2(ecd1id)
            eca.Dispose()
            txtecd2.Attributes.Add("onkeyup", "cecd2();")
        Else
            If Request.Form("lblsubmit") = "new2" Then
                lblsubmit.Value = ""
                eca.Open()
                AddECD2()
                eca.Dispose()

            ElseIf Request.Form("lblsubmit") = "new3" Then
                lblsubmit.Value = ""
                eca.Open()
                AddECD3()
                eca.Dispose()

            End If
            If Request.Form("lblret") = "next" Then
                eca.Open()
                GetNext()
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                eca.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber

                GetEC(PageNumber)
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                eca.Open()
                GetPrev()
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                eca.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                GetEC(PageNumber)
                eca.Dispose()
                lblret.Value = ""
            End If
            
            ecd1 = lblecd1.Value
            tdec1.InnerHtml = ecd1
            e1desc = lble1desc.Value
            tdedesc1.InnerHtml = e1desc
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            GetEC(PageNumber)
        Catch ex As Exception
            eca.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetEC(PageNumber)
        Catch ex As Exception
            eca.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub AddECD3()
        Dim e3d As String
        ecd1id = lblecd1id.Value
        ecd2id = lblecd2id.Value
        If ecd2id <> "" Then
            ecd3 = txtecd3.Text
            e3d = txtedesc3.Text
            ecd1id = lblecd1id.Value
            Dim ecnt As Integer = 0
            sql = "select count(*) from ecd3 where ecd3 = '" & ecd3 & "'"
            ecnt = eca.Scalar(sql)
            If ecnt = 0 Then
                sql = "insert into ecd3 (ecd3, ecddesc, ecd1id, ecd2id) values('" & ecd3 & "','" & Trim(e3d) & "','" & ecd1id & "','" & ecd2id & "') select @@identity"
                ecd3id = eca.Scalar(sql)

                Dim ocnt As Integer = 0
                sql = "select count(*) from failuremodes where failuremode = '" & Trim(e3d) & "'"
                ocnt = eca.Scalar(sql)
                If ocnt = 0 Then
                    sql = "insert into failuremodes(failuremode) values ('" & Trim(e3d) & "')"
                    eca.Update(sql)
                End If
                imge2.Attributes.Add("class", "details")
                txtecd3.Text = ""
                txtedesc3.Text = ""
            Else
                Dim strMessage As String = "This Error Code 3 Value Already Exists in another Record"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
        ecd1id = lblecd1id.Value
        If ecd1id <> "" Then
            PageNumber = 1
            txtpg.Value = PageNumber

            GetEC(PageNumber)
        End If
    End Sub
    Private Sub AddECD2()
        Dim e2d As String
        ecd2id = lblecd2id.Value
        If ecd2id = "" Then
            ecd2 = txtecd2.Text
            e2d = txtedesc2.Text
            ecd1id = lblecd1id.Value
            Dim ecnt As Integer = 0
            sql = "select count(*) from ecd2 where ecd2 = '" & ecd2 & "'"
            ecnt = eca.Scalar(sql)
            If ecnt = 0 Then
                sql = "insert into ecd2 (ecd2, ecddesc, ecd1id) values('" & ecd2 & "','" & Trim(e2d) & "','" & ecd1id & "') select @@identity"
                ecd2id = eca.Scalar(sql)
                lblecd2id.Value = ecd2id
                Dim ocnt As Integer = 0
                sql = "select count(*) from ocareas where ocarea = '" & Trim(e2d) & "'"
                ocnt = eca.Scalar(sql)
                If ocnt = 0 Then
                    sql = "insert into ocareas(ocarea) values ('" & Trim(e2d) & "')"
                    eca.Update(sql)
                End If
                imge2.Attributes.Add("class", "details")
            Else
                Dim strMessage As String = "This Error Code 2 Value Already Exists in another Record"

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        End If
        ecd1id = lblecd1id.Value
        If ecd1id <> "" Then
            PageNumber = 1
            txtpg.Value = PageNumber

            GetEC(PageNumber)
        End If
    End Sub
    Private Sub getecd1(ByVal ecd1 As String, ByVal e1desc As String)
        Dim ecnt As Integer
        sql = "select count(*) from ecd1 where ecd1 = '" & ecd1 & "'"
        ecnt = eca.Scalar(sql)
        If ecnt = 0 Then
            ecd1 = eca.ModString2(ecd1)
            e1desc = eca.ModString2(e1desc)
            sql = "insert into ecd1 (ecd1, ecddesc) values('" & ecd1 & "','" & Trim(e1desc) & "') select @@identity"
            ecd1id = eca.Scalar(sql)
            lblecd1id.Value = ecd1id
        Else
            sql = "select ecd1id from ecd1 where ecd1 = '" & ecd1 & "'"
            ecd1id = eca.Scalar(sql)
            lblecd1id.Value = ecd1id

            PageNumber = 1
            txtpg.Value = PageNumber

            GetEC(PageNumber)
        End If

    End Sub
    Private Sub getecd2(ByVal ecd1id As String)
        Dim ecd2cnt As Integer
        sql = "select count(*) from ecd2 where ecd1id = '" & ecd1id & "'"
        ecd2cnt = eca.Scalar(sql)
        If ecd2cnt = 0 Then
            tr2.Attributes.Add("class", "details")
        Else
            sql = "select ecd2id, ecd2 from ecd2 where ecd1id = '" & ecd1id & "'"
            dr = eca.GetRdrData(sql)
            ddecd2.DataSource = dr
            ddecd2.DataTextField = "ecd2"
            ddecd2.DataValueField = "ecd2id"
            ddecd2.DataBind()
            dr.Close()
            ddecd2.Items.Insert(0, "Select")
        End If

    End Sub

    Private Sub ddecd2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddecd2.SelectedIndexChanged
        Dim e2d As String
        If ddecd2.SelectedIndex <> 0 And ddecd2.SelectedIndex <> -1 Then
            ecd2id = ddecd2.SelectedValue.ToString
            sql = "select ecddesc from ecd2 where ecd2id = '" & ecd2id & "'"
            eca.Open()
            e2d = eca.strScalar(sql)
            lble2desc.Value = e2d
            txtedesc2.Text = e2d
            lblecd2id.Value = ecd2id
            ecd2 = ddecd2.SelectedItem.ToString
            txtecd2.Text = ecd2
            imge2.Attributes.Add("class", "details")
           
        End If
        ecd1 = lblecd1.Value
        tdec1.InnerHtml = ecd1
        e1desc = lble1desc.Value
        tdedesc1.InnerHtml = e1desc
    End Sub
    Private Sub GetEC(ByVal PageNumber As Integer)
        sql = "select e1.ecd1id, e1.ecd1, e1.ecddesc as edesc1, " _
            + "e2.ecd2id, e2.ecd2,e2.ecddesc as edesc2, e3.ecd3, " _
            + "e3.ecd3id, e3.ecddesc as edesc3 " _
            + "from ecd1 e1 " _
            + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
            + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
            + "order by e1.ecd1, e2.ecd2, e3.ecd3"
        Dim intPgNav, intPgCnt As Integer
        Dim srch As String = "" 'txtsearch.Text
        ecd1id = lblecd1id.Value
        If srch = "" Then
            'sql = "select count(*) from ecd3 where ecd1id is not null and ecd2id is not null and ecd1id = '" & ecd1id & "'"
            sql = "select count(*) from ecd3 where ecd1id is not null and ecd1id = '" & ecd1id & "'"  'and ecd2id is not null
        Else
            sql = "select count(*) from ecd3 e3 " _
                + "left join ecd2 e2 on e2.ecd2id = e3.ecd2id " _
            + "left join ecd1 e1 on e1.ecd1id = e2.ecd1id " _
            + "where e1.ecd1 like '%" & srch & "%'"
            intPgCnt = eca.Scalar(sql)
            If intPgCnt = 0 Then
                sql = "select count(*) from ecd2 e2 " _
            + "left join ecd1 e1 on e1.ecd1id = e2.ecd1id " _
            + "where e1.ecd1 like '%" & srch & "%'"
                intPgCnt = eca.Scalar(sql)
                If intPgCnt = 0 Then
                    sql = "select count(*) from ecd1 e1 " _
            + "where e1.ecd1 like '%" & srch & "%'"
                End If
            End If
        End If

        intPgCnt = eca.Scalar(sql)
        If intPgCnt = 0 Then

            'Dim strMessage As String = "No Records Found"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub


        End If
        PageSize = "20"
        intPgNav = eca.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = ""

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        If srch <> "" Then
            Filter = "ecd1 like ''%" & srch & "%''"
        Else
            'Filter = "ecd1id is not null and ecd2id is not null and ecd1id = ''" & ecd1id & "''"
            Filter = "ecd1id is not null and ecd1id = ''" & ecd1id & "''" 'and ecd2id is not null
        End If

        sql = "usp_geteclist '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        dr = eca.GetRdrData(sql)
        GridView1.DataSource = dr
        GridView1.DataBind()
        dr.Close()
    End Sub

    Private Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        GridView1.EditIndex = -1
        eca.Open()
        PageNumber = txtpg.Value
        GetEC(PageNumber)
        eca.Dispose()
    End Sub


    Private Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        Dim ecd1id, ecd2id, ecd3id As String
        Dim index As Integer = Convert.ToInt32(e.RowIndex)
        Dim row As GridViewRow = GridView1.Rows(index)
        Try
            ecd1id = CType(row.FindControl("lblecd1idi"), Label).Text
            ecd2id = CType(row.FindControl("lblecd2idi"), Label).Text
            ecd3id = CType(row.FindControl("lblecd3idi"), Label).Text
        Catch ex As Exception
            ecd1id = CType(row.FindControl("lblecd1ide"), Label).Text
            ecd2id = CType(row.FindControl("lblecd2ide"), Label).Text
            ecd3id = CType(row.FindControl("lblecd3ide"), Label).Text
        End Try
        Dim ecnt As Integer = 0
        sql = "select count(*) from componentfailmodes where ecd2id = '" & ecd2id & "'"
        eca.Open()
        ecnt = eca.Scalar(sql)
        If ecnt = 0 Then
            sql = "delete from ecd2 where ecd2id = '" & ecd2id & "'; " _
                + "delete from ecd3 where ecd2id = '" & ecd2id & "'; "
            GridView1.EditIndex = -1
            eca.Open()
            eca.Update(sql)
            GridView1.EditIndex = -1
            PageNumber = txtpg.Value
            GetEC(PageNumber)
            eca.Dispose()
        Else
            Dim strMessage As String = "Code 2 is assigned to one or more Component Failure Modes and cannot be Deleted"""
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
    End Sub

    Private Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        PageNumber = txtpg.Value
        GridView1.EditIndex = e.NewEditIndex
        eca.Open()
        GetEC(PageNumber)
        eca.Dispose()
    End Sub

    Private Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        Dim index As Integer = Convert.ToInt32(e.RowIndex)
        Dim row As GridViewRow = GridView1.Rows(index)
        Dim ecd1id, ecd2id, ecd3id As String
        ecd1id = CType(row.FindControl("lblecd1ide"), Label).Text
        ecd2id = CType(row.FindControl("lblecd2ide"), Label).Text
        ecd3id = CType(row.FindControl("lblecd3ide"), Label).Text
        Dim ecd1, ecd2, ecd3 As String
        'ecd1 = CType(row.FindControl("txtecd1"), TextBox).Text
        ecd2 = CType(row.FindControl("txtecd2"), TextBox).Text
        ecd3 = CType(row.FindControl("txtecd3"), TextBox).Text
        ecd1 = Trim(ecd1)
        ecd2 = Trim(ecd2)
        ecd3 = Trim(ecd3)

        Dim oecd1, oecd2, oecd3 As String
        'oecd1 = CType(row.FindControl("lblecd1e"), Label).Text
        oecd2 = CType(row.FindControl("lblecd2e"), Label).Text
        oecd3 = CType(row.FindControl("lblecd3e"), Label).Text
        oecd1 = Trim(oecd1)
        oecd2 = Trim(oecd2)
        oecd3 = Trim(oecd3)
        'If Len(ecd1) > 5 Then
        'Dim strMessage As String = "Code 1 Limited to 5 Characters"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        If Len(ecd2) > 5 Then
            Dim strMessage As String = "Code 2 Limited to 5 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(ecd3) > 5 Then
            Dim strMessage As String = "Code 3 Limited to 5 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim edesc1, edesc2, edesc3 As String
        'edesc1 = CType(row.FindControl("txtedesc1"), TextBox).Text
        edesc2 = CType(row.FindControl("txtedesc2"), TextBox).Text
        edesc3 = CType(row.FindControl("txtedesc3"), TextBox).Text
        edesc1 = Trim(edesc1)
        edesc2 = Trim(edesc2)
        edesc3 = Trim(edesc3)
        'If Len(edesc1) > 50 Then
        'Dim strMessage As String = "Code 1 Description Limited to 50 Characters"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        If Len(edesc2) > 50 Then
            Dim strMessage As String = "Code 2 Description Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(edesc3) > 50 Then
            Dim strMessage As String = "Code 3 Description Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eca.Open()
        'Dim oldedesc3 As String = CType(row.FindControl("lbledesc3"), Label).Text
        Dim fcnt As Integer
        sql = "select count(*) from failuremodes where failuremode = '" & edesc3 & "'"
        fcnt = eca.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into failuremodes (failuremode) values ('" & edesc3 & "')"
            eca.Update(sql)
        End If
        Dim e1cnt, e2cnt, e3cnt As Integer
        sql = "select e1cnt = (select count(*) from ecd1 where ecd1 = '" & ecd1 & "' and ecd1id <> '" & ecd1id & "'), " _
            + "e2cnt = (select count(*) from ecd2 where ecd2 = '" & ecd2 & "' and ecd2id <> '" & ecd2id & "'), " _
            + "e3cnt = (select count(*) from ecd3 where ecd3 = '" & ecd3 & "' and ecd3id <> '" & ecd3id & "') "
        dr = eca.GetRdrData(sql)
        While dr.Read
            e1cnt = dr.Item("e1cnt").ToString
            e2cnt = dr.Item("e2cnt").ToString
            e3cnt = dr.Item("e3cnt").ToString
        End While
        dr.Close()
        'If e1cnt = 0 Then
        'sql = "update ecd1 set ecd1 = '" & ecd1 & "', ecddesc = '" & edesc1 & "' where ecd1id = @ecd1id; "
        'eca.Update(sql)
        'Else
        'Dim strMessage As String = "Revised Code 1 Already Exists in another record"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'eca.Dispose()
        'Exit Sub
        'End If
        If e2cnt = 0 And ecd2 <> oecd2 Then
            sql = "select count(*) from ecd2 where ecd2 = '" & ecd2 & "' and ecd1id = '" & ecd1id & "'"
            e2cnt = eca.Scalar(sql)
            If ecd2 <> oecd2 Then
                If e2cnt = 0 Then
                    If oecd2 <> "" Then
                        sql = "update ecd2 set ecd2 = '" & ecd2 & "', ecddesc = '" & edesc2 & "' where ecd1id = '" & ecd1id & "' and ecd2id = '" & ecd2id & "'; "
                        eca.Update(sql)
                    Else
                        sql = "insert into ecd2 (ecd2, ecddesc, ecd1id) values ('" & ecd2 & "','" & edesc2 & "','" & ecd1id & "') select @@identity"
                        ecd2id = eca.Scalar(sql)
                    End If
                Else
                    Dim strMessage As String = "Revised Code 2 Already Exists in another record"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    eca.Dispose()
                    Exit Sub
                End If
            End If
        End If
        If e3cnt = 0 And ecd3 <> oecd3 Then
            sql = "select count(*) from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1id & "' and ecd2id = '" & ecd2id & "'"
            e3cnt = eca.Scalar(sql)
            If ecd3 <> oecd3 Then
                If e3cnt = 0 Then
                    If oecd3 <> "" Then
                        sql = "update ecd3 set ecd3 = '" & ecd3 & "', ecddesc = '" & edesc3 & "' where ecd1id = '" & ecd1id & "' and ecd2id = '" & ecd2id & "' and ecd3id = '" & ecd3id & "'; "
                        eca.Update(sql)
                    Else
                        sql = "insert into ecd3 (ecd3, ecddesc, ecd1id, ecd2id) values ('" & ecd3 & "','" & edesc3 & "','" & ecd1id & "','" & ecd2id & "')"
                        eca.Update(sql)
                    End If
                Else
                    Dim strMessage As String = "Revised Code 3 Already Exists in another record"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    eca.Dispose()
                    Exit Sub
                End If
            End If
        End If
        GridView1.EditIndex = -1
        PageNumber = txtpg.Value
        GetEC(PageNumber)

        eca.Dispose()
    End Sub
End Class