

'********************************************************
'*
'********************************************************



Public Class eqmain2
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim Login, start, eqid, fuid, comid, did, sid, lid, clid, chk, ustr As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifinv As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim appc As New AppUtils
        Dim url As String = appc.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        End If
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("eqmain2.aspx?lvl=no&start=no")
        Catch ex As Exception

        End Try
        If Not IsPostBack Then
            
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                ustr = Request.QueryString("ustr").ToString
                lbluser.Value = ustr

                Dim lvl As String = Request.QueryString("lvl").ToString
                Try
                    eqid = Request.QueryString("eqid").ToString
                Catch ex As Exception
                    eqid = ""
                End Try
                Try
                    fuid = Request.QueryString("fuid").ToString
                Catch ex As Exception
                    Try
                        fuid = Request.QueryString("funid").ToString
                    Catch ex1 As Exception
                        fuid = ""
                    End Try
                End Try
                Try
                    comid = Request.QueryString("comid").ToString
                Catch ex As Exception
                    comid = ""
                End Try

                sid = Request.QueryString("sid").ToString
                lid = Request.QueryString("lid").ToString
                did = Request.QueryString("did").ToString
                clid = Request.QueryString("clid").ToString
                chk = Request.QueryString("chk").ToString

                ifinv.Attributes.Add("src", "eqmain3.aspx?start=" + start + "&lvl=" + lvl + "&eqid=" + eqid + "&fuid=" + fuid + "&comid=" + comid + "&sid=" + sid + "&lid=" + lid + "&did=" + did + "&clid=" + clid + "&chk=" + chk + "&ustr=" + ustr + "&date=" + Now)
            Else
                sid = Request.QueryString("sid").ToString
                ifinv.Attributes.Add("src", "eqmain3.aspx?start=" + start + "&sid=" + sid + "&ustr=" + ustr)
            End If
        End If
    End Sub

End Class
