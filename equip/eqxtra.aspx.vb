﻿Imports System.Data.SqlClient

Public Class eqxtra
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqx As New Utilities
    Dim eqid, eqnum As String
    Dim POSEQIP, ITEMGRP, STATUS, SUBPROCESS, RESPONSIBLE, FIXEDASSET, PRIORITY, CRITICALITYCLS, PLANNINGPOSITION, structureType As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '[POSEQIP],[ITEMGRP],[STATUS], [SUBPROCESS], [RESPONSIBLE], [FIXEDASSET], [PRIORITY], [CRITICALITYCLS],[PLANNINGPOSITION], [eqid], [structureType]
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            eqx.Open()
            geteq(eqid)
            eqx.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                eqx.Open()
                savex()
                eqx.Dispose()
            End If
        End If
        


    End Sub
    Private Sub savex()
        eqid = lbleqid.Value
        POSEQIP = txtPOSEQIP.Text
        ITEMGRP = txtITEMGRP.Text
        STATUS = txtSTATUS.Text
        SUBPROCESS = txtSUBPROCESS.Text
        RESPONSIBLE = txtRESPONSIBLE.Text
        FIXEDASSET = txtFIXEDASSET.Text
        PRIORITY = txtPRIORITY.Text
        CRITICALITYCLS = txtCRITICALITYCLS.Text
        PLANNINGPOSITION = txtPLANNINGPOSITION.Text
        structureType = txtstructureType.Text
        Dim xcnt As Integer = 0
        Dim cmd As New SqlCommand
        sql = "select count(*) from equipment_xtra where eqid = '" & eqid & "'"
        xcnt = eqx.Scalar(sql)
        If xcnt = 0 Then
            sql = "insert into equipment_xtra (POSEQIP, ITEMGRP, STATUS, SUBPROCESS, RESPONSIBLE, FIXEDASSET, PRIORITY, CRITICALITYCLS, PLANNINGPOSITION, structureType, eqid) " _
                + "values('" & POSEQIP & "','" & ITEMGRP & "','" & STATUS & "','" & SUBPROCESS & "','" & RESPONSIBLE & "','" & FIXEDASSET & "','" & PRIORITY & "','" & CRITICALITYCLS & "','" & PLANNINGPOSITION & "','" & structureType & "','" & eqid & "')"

            cmd.CommandText = "insert into equipment_xtra (POSEQIP, ITEMGRP, STATUS, SUBPROCESS, RESPONSIBLE, FIXEDASSET, PRIORITY, CRITICALITYCLS, PLANNINGPOSITION, structureType, eqid) " _
                + "values(@POSEQIP,@ITEMGRP,@STATUS,@SUBPROCESS,@RESPONSIBLE,@FIXEDASSET,@PRIORITY,@CRITICALITYCLS,@PLANNINGPOSITION,@structureType,@eqid)"

        Else
            sql = "update equipment_xtra set POSEQIP = '" & POSEQIP & "', ITEMGRP = '" & ITEMGRP & "', STATUS = '" & STATUS & "', " _
                + "SUBPROCESS = '" & SUBPROCESS & "', RESPONSIBLE = '" & RESPONSIBLE & "', FIXEDASSET = '" & FIXEDASSET & "', " _
                + "PRIORITY = '" & PRIORITY & "', CRITICALITYCLS = '" & CRITICALITYCLS & "', PLANNINGPOSITION + '" & PLANNINGPOSITION & "', " _
                + "structureType = '" & structureType & "' where eqid = '" & eqid & "'"

            cmd.CommandText = "update equipment_xtra set POSEQIP = @POSEQIP, ITEMGRP = @ITEMGRP, STATUS = @STATUS, " _
                + "SUBPROCESS = @SUBPROCESS, RESPONSIBLE = @RESPONSIBLE, FIXEDASSET = @FIXEDASSET, " _
                + "PRIORITY = @PRIORITY, CRITICALITYCLS = @CRITICALITYCLS, PLANNINGPOSITION = @PLANNINGPOSITION, " _
                + "structureType = @structureType where eqid = @eqid"

        End If
        Dim param01 = New SqlParameter("@POSEQIP", SqlDbType.VarChar)
        If POSEQIP = "" Then
            param01.Value = System.DBNull.Value
        Else
            param01.Value = POSEQIP
        End If
        cmd.Parameters.Add(param01)
        Dim param02 = New SqlParameter("@ITEMGRP", SqlDbType.VarChar)
        If ITEMGRP = "" Then
            param02.Value = System.DBNull.Value
        Else
            param02.Value = ITEMGRP
        End If
        cmd.Parameters.Add(param02)
        Dim param03 = New SqlParameter("@STATUS", SqlDbType.VarChar)
        If STATUS = "" Then
            param03.Value = System.DBNull.Value
        Else
            param03.Value = STATUS
        End If
        cmd.Parameters.Add(param03)
        Dim param04 = New SqlParameter("@SUBPROCESS", SqlDbType.VarChar)
        If SUBPROCESS = "" Then
            param04.Value = System.DBNull.Value
        Else
            param04.Value = SUBPROCESS
        End If
        cmd.Parameters.Add(param04)
        Dim param05 = New SqlParameter("@RESPONSIBLE", SqlDbType.VarChar)
        If RESPONSIBLE = "" Then
            param05.Value = System.DBNull.Value
        Else
            param05.Value = RESPONSIBLE
        End If
        cmd.Parameters.Add(param05)
        Dim param06 = New SqlParameter("@FIXEDASSET", SqlDbType.VarChar)
        If FIXEDASSET = "" Then
            param06.Value = System.DBNull.Value
        Else
            param06.Value = FIXEDASSET
        End If
        cmd.Parameters.Add(param06)
        Dim param07 = New SqlParameter("@PRIORITY", SqlDbType.VarChar)
        If PRIORITY = "" Then
            param07.Value = System.DBNull.Value
        Else
            param07.Value = PRIORITY
        End If
        cmd.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@CRITICALITYCLS", SqlDbType.VarChar)
        If CRITICALITYCLS = "" Then
            param08.Value = System.DBNull.Value
        Else
            param08.Value = CRITICALITYCLS
        End If
        cmd.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@PLANNINGPOSITION", SqlDbType.VarChar)
        If PLANNINGPOSITION = "" Then
            param09.Value = System.DBNull.Value
        Else
            param09.Value = PLANNINGPOSITION
        End If
        cmd.Parameters.Add(param09)
        Dim param010 = New SqlParameter("@structureType", SqlDbType.VarChar)
        If structureType = "" Then
            param010.Value = System.DBNull.Value
        Else
            param010.Value = structureType
        End If
        cmd.Parameters.Add(param010)
        Dim param011 = New SqlParameter("@eqid", SqlDbType.VarChar)
        If eqid = "" Then
            param011.Value = System.DBNull.Value
        Else
            param011.Value = eqid
        End If
        cmd.Parameters.Add(param011)
        'eqx.Update(sql)
        eqx.ScalarHack(cmd)
        geteq(eqid)
    End Sub
    Private Sub geteq(ByVal eqid As String)
        Dim xcnt As Integer = 0
        sql = "select count(*) from equipment_xtra where eqid = '" & eqid & "'"
        xcnt = eqx.Scalar(sql)
        If xcnt = 0 Then
            sql = "select eqnum from equipment where eqid = '" & eqid & "'"
            eqnum = eqx.strScalar(sql)
            tdeqnum.InnerHtml = eqnum
        Else
            sql = "select x.*, e.eqnum from equipment_xtra x left join equipment e on e.eqid = x.eqid where x.eqid = '" & eqid & "'"
            dr = eqx.GetRdrData(sql)
            While dr.Read
                POSEQIP = dr.Item("POSEQIP").ToString
                ITEMGRP = dr.Item("ITEMGRP").ToString
                STATUS = dr.Item("STATUS").ToString
                SUBPROCESS = dr.Item("SUBPROCESS").ToString
                RESPONSIBLE = dr.Item("RESPONSIBLE").ToString
                FIXEDASSET = dr.Item("FIXEDASSET").ToString
                PRIORITY = dr.Item("PRIORITY").ToString
                CRITICALITYCLS = dr.Item("CRITICALITYCLS").ToString
                PLANNINGPOSITION = dr.Item("PLANNINGPOSITION").ToString
                structureType = dr.Item("structureType").ToString
                eqnum = dr.Item("eqnum").ToString

            End While
            dr.Close()
            txtPOSEQIP.Text = POSEQIP
            txtITEMGRP.Text = ITEMGRP
            txtSTATUS.Text = STATUS
            txtSUBPROCESS.Text = SUBPROCESS
            txtRESPONSIBLE.Text = RESPONSIBLE
            txtFIXEDASSET.Text = FIXEDASSET
            txtPRIORITY.Text = PRIORITY
            txtCRITICALITYCLS.Text = CRITICALITYCLS
            txtPLANNINGPOSITION.Text = PLANNINGPOSITION
            txtstructureType.Text = structureType
            tdeqnum.InnerHtml = eqnum
        End If
       

    End Sub
End Class