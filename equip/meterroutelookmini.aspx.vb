﻿Imports System.Data.SqlClient
Public Class meterroutelookmini
    Inherits System.Web.UI.Page
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sid As String
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            txtpg.Value = "1"
            appset.Open()
            getroutes(PageNumber)
            appset.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber

                getroutes(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                getroutes(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getroutes(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getroutes(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub getroutes(ByVal PageNumber As Integer)
        sid = lblsid.Value
        Dim intPgNav, intPgCnt As Integer
        sql = "select count(*) from meterroutes where siteid = '" & sid & "'"
        intPgCnt = appset.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        PageSize = "50"
        intPgNav = appset.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "mroute"

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        Dim srch As String = txtsrch.Text
        srch = appset.ModString2(srch)
        If Len(srch) > 0 Then
            Filter = "s.mroute like '%" & srch & "%' and s.siteid = " & sid
        Else
            Filter = "s.siteid = " & sid
        End If

        'mrouteid, mroute, siteid, mdesc, createdby, createdate, modifiedby, modifieddate
        sql = "usp_getroutes_mtr '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        dr = appset.GetRdrData(sql)
        dglist.DataSource = dr
        dglist.DataBind()
    End Sub

    Private Sub dglist_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dglist.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            Dim mi As LinkButton = CType(e.Item.FindControl("lblmroutei"), LinkButton)
            Dim rid As String = DataBinder.Eval(e.Item.DataItem, "mrouteid").ToString
            mi.Attributes.Add("onclick", "getmeter('" & rid & "')")
            Dim imgp As HtmlImage = CType(e.Item.FindControl("imgprint"), HtmlImage)
            imgp.Attributes.Add("onclick", "getprint('" & rid & "')")
            Dim imgrec As HtmlImage = CType(e.Item.FindControl("imgrec"), HtmlImage)
            imgrec.Attributes.Add("onclick", "getrec('" & rid & "')")
        End If
    End Sub

    Protected Sub btnsrch1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsrch1.Click
        appset.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        getroutes(PageNumber)
        appset.Dispose()
        lblret.Value = ""
    End Sub
End Class