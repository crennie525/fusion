﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="complistdialog.aspx.vb" Inherits="lucy_r12.complistdialog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Component Look Up</title>

    <script  type="text/javascript">
        function GetWidth() {
            var x = 0;
            if (self.innerHeight) {
                x = self.innerWidth;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                x = document.documentElement.clientWidth;
            }
            else if (document.body) {
                x = document.body.clientWidth;
            }
            return x;
        }

        function GetHeight() {
            var y = 0;
            if (self.innerHeight) {
                y = self.innerHeight;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                y = document.documentElement.clientHeight;
            }
            else if (document.body) {
                y = document.body.clientHeight;
            }
            return y;
        }
        var flag = 5
        function upsize(who) {
            if (who != flag) {
                flag = who;
                document.getElementById("ifcomp").height = GetHeight();
                window.scrollTo(0, top);
                //alert(who)
            }
        }
        function getdco(coid, comp) {
            var ret = coid + "," + comp;
            window.returnValue = ret;
            window.close();
        }
    </script>
</head>
<body onload="upsize('1');">
    <form id="form1" runat="server">
    <script type="text/javascript">
        document.body.onresize = function () {
            upsize('2');
        }
        self.onresize = function () {
            upsize('3');
        }
        document.documentElement.onresize = function () {
            upsize('4');
        }
    </script>
    <div>
        <iframe id="ifcomp" runat="server" width="100%" height="100%" frameborder="0">
        </iframe>
        <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    </div>
    </form>
</body>
</html>
