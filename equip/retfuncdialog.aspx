﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="retfuncdialog.aspx.vb" Inherits="lucy_r12.retfuncdialog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script  type="text/javascript">
        function getdfu(fuid, func) {
            window.returnValue = fuid + "," + func;
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <iframe id="iffunc" runat="server" width="450" height="450" frameBorder="no"></iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    </div>
    </form>
</body>
</html>
