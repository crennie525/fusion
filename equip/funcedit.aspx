<%@ Page Language="vb" AutoEventWireup="false" Codebehind="funcedit.aspx.vb" Inherits="lucy_r12.funcedit" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>funcedit</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<LINK rel="stylesheet" type="text/css" href="../styles/reports.css">
		<script  src="../scripts1/funceditaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkret();">
		<form id="form1" method="post" runat="server">
			<table id="tblneweq" class="details" width="400" runat="server">
				<TBODY>
					<TR>
						<td style="width: 120px"></td>
						<td style="width: 100px"></td>
						<td style="width: 100px"></td>
						<td style="width: 80px"></td>
					</TR>
					<tr height="24">
						<td class="thdrsing label" colSpan="4"><asp:Label id="lang2386" runat="server">Function Record Details</asp:Label></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2387" runat="server">Function Name</asp:Label></td>
						<td colSpan="3"><asp:textbox id="txtfunc" runat="server" MaxLength="100" Width="210px" CssClass="plainlabel"></asp:textbox></td>
					</tr>
					<TR>
						<td class="label"><asp:Label id="lang2388" runat="server">Special Identifier</asp:Label></td>
						<td colSpan="3"><asp:textbox id="txtfuncspl" runat="server" MaxLength="100" Width="210px" CssClass="plainlabel"></asp:textbox></td>
					</TR>
					<tr>
						<td colSpan="4" align="right"><asp:imagebutton id="ibtnsaveneweq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
					</tr>
				</TBODY>
			</table>
			<input id="lblfuid" type="hidden" runat="server"> <input id="lblfunc" type="hidden" runat="server">
			<input id="lblspl" type="hidden" runat="server"><input type="hidden" id="lbleqid" runat="server">
			<input type="hidden" id="lblret" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
