﻿Imports System.Data.SqlClient
Public Class tpmrev
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim ed As New Utilities
    Dim eqid As String
    Dim tpmrevchar, tpmapprdate, tpmapprby As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid

            ed.Open()
            getrev(eqid)
            ed.Dispose()
        Else
            If Request.Form("saverev") = "go" Then
                eqid = lbleqid.Value
                ed.Open()
                savetpmrev(eqid)
                ed.Dispose()
            End If

        End If
    End Sub
    Private Sub getrev(ByVal eqid As String)
        sql = "select tpmrevchar, Convert(char(10),tpmapprdate,101) as tpmapprdate, tpmapprby from equipment where eqid = '" & eqid & "'"
        dr = ed.GetRdrData(sql)
        While dr.Read
            tpmrevchar = dr.Item("tpmrevchar").ToString
            tpmapprdate = dr.Item("tpmapprdate").ToString
            tpmapprby = dr.Item("tpmapprby").ToString
        End While
        dr.Close()
        txtrev.Text = tpmrevchar
        txtrevdate.Text = tpmapprdate
        txtappr.Text = tpmapprby
    End Sub
    Private Sub savetpmrev(ByVal eqid As String)
        tpmrevchar = txtrev.Text
        tpmapprdate = txtrevdate.Text
        tpmapprby = txtappr.Text
        sql = "update equipment set tpmrevchar = '" & tpmrevchar & "', tpmapprdate = '" & tpmapprdate & "', " _
            + "tpmapprby = '" & tpmapprby & "' where eqid = '" & eqid & "'"
        ed.Update(sql)
        saverev.Value = "done"
    End Sub
End Class