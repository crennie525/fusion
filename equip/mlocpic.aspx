﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="mlocpic.aspx.vb" Inherits="lucy_r12.mlocpic" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script  type="text/javascript">
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lup";
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                var ret = eReturn + "~" + "dept";
                window.returnValue = ret;
                window.close();
            }
        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lup&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~");
                var ret = eReturn + "~" + "loc";
                window.returnValue = ret;
                window.close();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td id="tddepts" class="bluelabel" runat="server">
                    Use Departments
                </td>
                <td>
                    <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                </td>
                <td id="tdlocs1" class="bluelabel" runat="server">
                    Use Locations
                </td>
                <td>
                    <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    </form>
</body>
</html>
