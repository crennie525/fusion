﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="compdivecd3.aspx.vb" Inherits="lucy_r12.compdivecd3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet"/>
    <script  type ="text/javascript">
        function saveecd3() {
            var chk = document.getElementById("txtnewecd3").value;
            if (chk != "") {
                var ret = chk
                if (chk.length > 5) {
                    alert("New Error Code 3 Value Limited to 5 Characters")
                }
                else {
                    document.getElementById("lblsubmit").value = "go";
                    document.getElementById("form1").submit();
                }  
            }
        }
        function checkret() {
            var chk = document.getElementById("lblret").value;
            if (chk == "go") {
                var ret = document.getElementById("txtnewecd3").value;
                window.parent.handlereturn(ret);
            }
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="redlabel">Enter New Error Code 3 Value</td>
    <td><asp:TextBox ID="txtnewecd3" runat="server"></asp:TextBox></td>
    <td><img src="../images/appbuttons/minibuttons/savedisk1.gif" alt="" onclick="saveecd3();" /></td>
    </tr>
    </table>
    </div>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblecd2id" runat="server" />
    <input type="hidden" id="lblecd2" runat="server" />
    
    </form>
</body>
</html>
