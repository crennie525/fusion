<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibnewfudets.aspx.vb" Inherits="lucy_r12.pmlibnewfudets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibnewfudets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  type="text/javascript" src="../scripts1/pmlibnewfudetsaspx_3.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     
	</HEAD>
	<body onload="GetsScroll();" >
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 0px" width="980">
				<TBODY>
					<tr>
						<td class="labelibl" colSpan="2"><asp:Label id="lang2486" runat="server">Select a Function to view Function Image, Component, and Task Details</asp:Label></td>
					</tr>
					<tr>
						<td vAlign="top" align="center" width="750">
							<div id="spdiv" style="WIDTH: 500px; HEIGHT: 260px; OVERFLOW: auto" onscroll="SetsDivPosition();"><asp:repeater id="rptrfuncnew" runat="server">
									<HeaderTemplate>
										<table>
											<tr>
												<td class="btmmenu plainlabel" align="center"><img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16"></td>
												<td class="btmmenu plainlabel" width="50px"><asp:Label id="lang2487" runat="server">Edit</asp:Label></td>
												<td class="btmmenu plainlabel" width="250px"><asp:Label id="lang2488" runat="server">Function</asp:Label></td>
												<td class="btmmenu plainlabel" width="250px"><asp:Label id="lang2489" runat="server">Special Identifier</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr  id="selrow" runat="server" bgcolor='<%# HighlightRowFR(DataBinder.Eval(Container.DataItem, "func_id"))%>'>
											<td class="plainlabel"><asp:CheckBox id="cb1" runat="server"></asp:CheckBox></td>
											<td><img src="../images/appbuttons/minibuttons/lilpentrans.gif" id="imgcoedit" runat="server"></td>
											<td class="plainlabel"><asp:LinkButton  CommandName="Select" ID="Linkbutton2" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>' Runat = server>
												</asp:LinkButton></td>
											<td class="plainlabel"><asp:Label ID="Label9" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
												</asp:Label></td>
											<td class="details"><asp:Label ID="lblfuncrevid" Text='<%# DataBinder.Eval(Container.DataItem,"func_id")%>' Runat = server>
												</asp:Label></td>
											<td class="details"><asp:Label ID="lblparfu1" Text='<%# DataBinder.Eval(Container.DataItem,"origparent")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD>
			<td vAlign="top" align="center" width="230">
				<table>
					<tr>
						<td vAlign="top" align="center" colSpan="2"><A onclick="getbig();" href="#"><IMG id="imgfu" height="206" src="../images/appimages/funcimg.gif" style="width: 216px" border="0"
									runat="server"></A>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tr>
									<td class="bluelabel" style="width: 80px" id="tst" runat="server"><asp:Label id="lang2490" runat="server">Order</asp:Label></td>
									<td style="width: 60px"><asp:textbox id="txtiorder" runat="server" style="width: 40px" CssClass="plainlabel"></asp:textbox></td>
									<td style="width: 20px"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
									<td style="width: 20px"><IMG onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)" onclick="addpic();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif"></td>
									<td style="width: 20px"><IMG id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/del.gif" runat="server"></td>
									<td style="width: 20px"><IMG id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)" onclick="savdets();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif" runat="server">
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirstf" onclick="getpfirstf();" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprevf" onclick="getpprevf();" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 140px;" vAlign="middle" align="center"><asp:label id="lblpgf" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inextf" onclick="getpnextf();" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px"><IMG id="ilastf" onclick="getplastf();" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			</TR>
			<tr>
				<td colSpan="3"><asp:imagebutton id="ibtnremove" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"></asp:imagebutton>
					<IMG onmouseover="return overlib('Add a New Function Record')" onclick="GetFuncDiv();"
						onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
						style="width: 20px"> <IMG onmouseover="return overlib('Lookup Function Records to Copy')" onclick="GetFuncCopy();"
						onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif" style="width: 20px">
					<IMG id="ggrid" onmouseover="return overlib('Edit in Grid View')" onclick="GetGrid();"
						onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/grid1.gif"
						style="width: 20px" runat="server"></td>
			</tr>
			</TBODY></TABLE><input id="lblfuid" type="hidden" runat="server" NAME="lblfuid">
			<input id="lbleqid" type="hidden" runat="server" NAME="lbleqid"><input id="lbldb" type="hidden" runat="server" NAME="lbldb">
			<input id="lblpcntf" type="hidden" name="lblpcntf" runat="server"> <input id="lblcurrpf" type="hidden" name="lblcurrpf" runat="server">
			<input id="lblimgsf" type="hidden" name="lblimgsf" runat="server"> <input id="lblimgidf" type="hidden" name="lblimgidf" runat="server">
			<input id="lblovimgsf" type="hidden" name="lblovimgsf" runat="server"> <input id="lblovbimgsf" type="hidden" name="lblovbimgsf" runat="server">
			<input id="lblcurrimgf" type="hidden" name="lblcurrimgf" runat="server"> <input id="lblcurrbimgf" type="hidden" name="lblcurrbimgf" runat="server">
			<input id="lblbimgsf" type="hidden" name="lblbimgsf" runat="server"><input id="lbliordersf" type="hidden" name="lbliordersf" runat="server">
			<input id="lbloldorderf" type="hidden" name="lbloldorderf" runat="server"> <input id="lblovtimgsf" type="hidden" name="lblovtimgsf" runat="server">
			<input id="lblcurrtimgf" type="hidden" name="lblcurrtimgf" runat="server"> <input id="spdivy" type="hidden" runat="server" NAME="spdivy">
			<input type="hidden" id="lblchk" runat="server" NAME="lblchk"> <input type="hidden" id="lblpchk" runat="server">
			<input id="lblcurrp" type="hidden" runat="server" NAME="lblcurrp"> <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
			<input id="lblimgid" type="hidden" name="lblimgid" runat="server"> <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
			<input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server"> <input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server">
			<input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server"> <input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"> <input type="hidden" id="lblcid" runat="server">
			<input type="hidden" id="lblsid" runat="server"> <input type="hidden" id="lbldid" runat="server">
			<input type="hidden" id="lblclid" runat="server"><input type="hidden" id="lbloloc" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
