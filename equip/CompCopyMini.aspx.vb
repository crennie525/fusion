Imports System.Data.SqlClient
Imports System.IO
Public Class CompCopyMini
    Inherits System.Web.UI.Page
    Dim cid, eqid, fuid, sid, login, did, clid, usr, ro, coid As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim mu As New mmenu_utils_a
    Dim intPgNav As Integer
    Dim coi As String
    Dim ds As DataSet
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tblnewcompdet As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdnewcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewecby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqecph As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqemm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqecd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncfcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncfcph As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncfmm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncfcd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tblnewcomphdr As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtnewspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdoldcomphdr As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ibtnreturn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblnewcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdolddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdolddesig As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig As System.Web.UI.WebControls.TextBox
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtnewcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnewdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbtsks As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtskcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim db, oloc, tpm, mopt As String
    Protected WithEvents lbloloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdmc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdac As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcurtab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewcompkey As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbow As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbcpyo1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents cbcpyo2 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents tdcurcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbloldcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtmfg As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdoldmfg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoldac As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoldsc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoldcomptasks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents geteq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbltpm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifrep As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents tblnewcompedit As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cbrat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbmopt As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents rbro As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbrs As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbai As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents cbdl As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltskcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cball As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblcoi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rptrcomprev As System.Web.UI.WebControls.Repeater
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents hpBackward As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtncopy As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdoldcompdet As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdoldcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecph As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfcph As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfcm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfcd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbfm As System.Web.UI.WebControls.CheckBox
    Protected WithEvents tdoldspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdopts As System.Web.UI.HtmlControls.HtmlTableCell
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        coi = mu.COMPI
        lblcoi.Value = coi
        If Not IsPostBack Then
            cbdl.Attributes.Add("onclick", "checkcb('dl');")
            cbow.Attributes.Add("onclick", "checkcb('ow');")
            cbcpyo1.Attributes.Add("onclick", "checkcb('cpyo1');")
            cbcpyo2.Attributes.Add("onclick", "checkcb('cpyo2');")
            cbfm.Attributes.Add("onclick", "checkcb('cfm');")
            cbtsks.Attributes.Add("onclick", "checkcb('ctsk');")
            If lbllog.Value <> "no" Then
                'Try
                Try
                    tpm = Request.QueryString("tpm").ToString
                    If tpm = "no" Then
                        tpm = "N"
                    End If
                    lbltpm.Value = tpm
                Catch ex As Exception
                    lbltpm.Value = "N"
                End Try
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                Try
                    coid = Request.QueryString("coid").ToString
                    lbloldcoid.Value = coid
                Catch ex As Exception
                    cbow.Enabled = False
                    cbdl.Enabled = False
                    'cbcpyo1.Enabled = False
                    'cbcpyo2.Enabled = False
                End Try
                If coid = "" Or coid = "0" Then
                    cbow.Enabled = False
                    cbdl.Enabled = False
                    'cbcpyo1.Enabled = False
                    'cbcpyo2.Enabled = False
                End If
                lblro.Value = ro
                Try
                    db = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString 'Request.QueryString("db").ToString
                    oloc = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString 'Request.QueryString("oloc").ToString
                    lbldb.Value = db
                    lbloloc.Value = oloc
                Catch ex As Exception
                    db = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString ' "0"
                    oloc = "0"
                    lbldb.Value = db
                    lbloloc.Value = oloc
                End Try
                If ro = "1" Then
                    ibtncopy.Enabled = False
                    ibtncopy.ImageUrl = "../images/appbuttons/bgbuttons/copydis.gif"
                    'imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                Else
                    ibtncopy.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/copyhov.gif'")
                    ibtncopy.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/copy.gif'")
                End If
                If Len(fuid) <> 0 AndAlso fuid <> "" AndAlso fuid <> "0" Then
                    txtpg.Value = "1"
                    eqid = Request.QueryString("eqid").ToString
                    lbleqid.Value = eqid
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    sid = Request.QueryString("sid").ToString
                    lblsid.Value = sid
                    did = Request.QueryString("did").ToString
                    lbldid.Value = did
                    clid = Request.QueryString("clid").ToString
                    lblclid.Value = clid
                    usr = Request.QueryString("usr").ToString
                    lblusername.Value = usr
                    tdoldcomptasks.Attributes.Add("class", "details")
                    tdoldcomphdr.Attributes.Add("class", "details")
                    tdoldcompdet.Attributes.Add("class", "details")
                    tblnewcomphdr.Attributes.Add("class", "details")
                    tblnewcompdet.Attributes.Add("class", "details")
                    lblcurtab.Value = "tdmc"
                    comp.Open()
                    PopComp()
                    PopCompRev(PageNumber)
                    comp.Dispose()
                Else
                    Dim strMessage As String = "Problem Retrieving Function ID\nPlease Reselect Equipment and try Again"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If
                'Catch ex As Exception
                'Dim strMessage As String = "Problem Retrieving Function ID\nPlease Reselect Equipment and try Again"
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'lbllog.Value = "noeqid"
                'End Try

            End If
        Else
            If Request.Form("lblret") = "next" Then
                comp.Open()
                GetNext()
                comp.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                comp.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopCompRev(PageNumber)
                comp.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                comp.Open()
                GetPrev()
                comp.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                comp.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopCompRev(PageNumber)
                comp.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "return" Then
                PageNumber = 1
                txtpg.Value = PageNumber
                tdoldcomptasks.Attributes.Add("class", "details")
                tdoldcomphdr.Attributes.Add("class", "details")
                tdoldcompdet.Attributes.Add("class", "details")
                tblnewcomphdr.Attributes.Add("class", "details")
                tblnewcompdet.Attributes.Add("class", "details")
                tblnewcompedit.Attributes.Add("class", "details")
                tbltop.Attributes.Add("class", "view")
                ifrep.Attributes.Add("src", "")
                comp.Open()
                PopComp()
                PopCompRev(PageNumber)
                comp.Dispose()
                If lblcurtab.Value <> "tdmc" Then
                    'cbcpyo1.Enabled = False
                    'cbcpyo2.Enabled = False
                    'cbmopt.Attributes.Add("disabled", "true")
                    'tdopts.Attributes.Add("class", "graylabel")
                End If

                lblret.Value = ""
            End If
            If lblcurtab.Value <> "tdmc" Then
                'cbcpyo1.Enabled = False
                'cbcpyo2.Enabled = False
                'cbmopt.Attributes.Add("disabled", "true")
                'tdopts.Attributes.Add("class", "graylabel")
            Else
                cbcpyo1.Enabled = True
                cbcpyo2.Enabled = True
                'cbmopt.Attributes.Add("disabled", "false")
                'tdopts.Attributes.Add("class", "bluelabel")
            End If
        End If
        btnnext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        btnnext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
        btnprev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        btnprev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        ibtnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        ibtnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
        hpBackward.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        hpBackward.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        ibtncopy.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
    End Sub
    Private Sub PopComp()
        Dim comid As String = lbloldcoid.Value
        Dim compnum, compdesc As String
        If comid <> "" Then
            sql = "select compnum, compdesc from Components where comid = '" & comid & "'"
            dr = comp.GetRdrData(sql)
            While dr.Read
                compnum = dr.Item("compnum").ToString
                compdesc = dr.Item("compdesc").ToString
            End While
            dr.Close()
            tdcurcomp.InnerHtml = compnum & " - " & compdesc
            lblcurcomp.Value = compnum
        End If

    End Sub
    Private Sub MarkDelCur()
        Dim comid, fuid As String
        cid = lblcid.Value
        comid = lbloldcoid.Value
        fuid = lblfuid.Value
        sid = lblsid.Value
        'sql = "update pmtasks set taskstatus = 'Delete' where comid = '" & comid & "' and funcid = '" & fuid & "'"
        sql = "usp_delrevisedcomp '" & comid & "','" & fuid & "','" & sid & "'"
        comp.Update(sql)
    End Sub
    Private Sub DelCur(Optional ByVal ocomid As Integer = 0)
        'comp.Open()
        Dim comid, fuid As String
        cid = lblcid.Value
        comid = lbloldcoid.Value
        If ocomid <> 0 Then
            comid = ocomid
        End If
        fuid = lblfuid.Value
        Dim pic, picn, picm As String
        sql = "select picurl, picurltn, picurltm from pmpictures where comid = '" & comid & "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            pic = dr.Item("picurl").ToString
            picn = dr.Item("picurltn").ToString
            picm = dr.Item("picurltm").ToString
            DelFuImg(pic, picn, picm)
        End While
        dr.Close()
        sql = "usp_delComp '" & comid & "'" ', '" & fuid & "'"
        Try
            comp.Update(sql)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub DelFuImg(ByVal pic As String, ByVal picn As String, ByVal picm As String)
        'fuid = lblfuid.Value

        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"

        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopCompRev(PageNumber)
        Catch ex As Exception
            comp.Dispose()
            Dim strMessage As String = "Problem Retrieving Page"
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopCompRev(PageNumber)
        Catch ex As Exception
            comp.Dispose()
            Dim strMessage As String = "Problem Retrieving Page"
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopCompRev(ByVal PageNumber As Integer)
        tdoldcompdet.Attributes.Add("class", "details")
        txtpg.Value = PageNumber
        eqid = lbleqid.Value
        cid = lblcid.Value
        sid = lblsid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim intPgCnt As Integer
        Dim tab As String = lblcurtab.Value
        Dim mdb As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim cts, ctss As String
        Dim lang As String = lblfslang.Value
        If tab = "tdmc" Then
            cts = " and createdby = '" & usr & "'"
            ctss = " where createdby = '" & usr & "'"
            tdmc.Attributes.Add("class", "thdrhov label")
            tdac.Attributes.Add("class", "thdr label")
            tdsc.Attributes.Add("class", "thdr label")
        ElseIf tab = "tdsc" Then
            If cbmopt.Checked = True Then
                cts = " and c.sid = '" & sid & "' and t.tasktype is not null and t.taskdesc is not null and t.skill is not null and (t.ttime is not null and t.ttime <> 0) and t.rd is not null and (c.lang = '" & lang & "' or c.lang is null)"
            Else
                cts = " and sid = '" & sid & "' and (lang = '" & lang & "' or lang is null)"
            End If

            ctss = " where sid = '" & sid & "' and (lang = '" & lang & "' or lang is null)"
            tdsc.Attributes.Add("class", "thdrhov label")
            tdmc.Attributes.Add("class", "thdr label")
            tdac.Attributes.Add("class", "thdr label")
        Else
            If cbmopt.Checked = True Then
                cts = " and c.sid is null and t.tasktype is not null and t.taskdesc is not null and t.skill is not null and (t.ttime is not null and t.ttime <> 0) and t.rd is not null and (c.lang = '" & lang & "' or c.lang is null)"
            Else
                cts = " and sid is null and (lang = '" & lang & "' or lang is null)"
            End If

            ctss = " where sid is null and (lang = '" & lang & "' or lang is null)"
            tdmc.Attributes.Add("class", "thdr label")
            tdac.Attributes.Add("class", "thdrhov label")
            tdsc.Attributes.Add("class", "thdr label")
        End If
        If tab <> "tdmc" Then
            cbfm.Checked = True
            'cbfm.Enabled = False
            cbtsks.Checked = True
            'cbtsks.Enabled = False
            cbrat.Disabled = True
            rbro.Disabled = True
            rbrs.Disabled = True
            rbai.Disabled = True
        Else
            cbfm.Checked = True
            'cbfm.Enabled = True
            cbtsks.Checked = True
            'cbtsks.Enabled = True
            cbrat.Disabled = False
            rbro.Disabled = False
            rbrs.Disabled = False
            rbai.Disabled = False
        End If
        If Len(srch) = 0 Then
            srch = ""
            If tab = "tdmc" Then
                If cbmopt.Checked = True Then
                    sql = "select count(*) from components c join functions f on f.func_id = c.func_id " _
                + "join equipment e on e.eqid = f.eqid join pmtasks t on t.comid = c.comid  where e.siteid = '" & sid & "' " _
                + "and t.tasktype is not null and t.taskdesc is not null " _
                + "and t.skill is not null and (t.ttime is not null and t.ttime <> 0) " _
                + "and t.rd is not null and t.taskstatus is not null"
                Else
                    sql = "select count(*) from components c join functions f on f.func_id = c.func_id " _
                    + "join equipment e on e.eqid = f.eqid   where e.siteid = '" & sid & "'"
                End If

            ElseIf tab = "tdsc" Then
                If cbmopt.Checked = True Then
                    sql = "select count(*) from complib c join comptasks t on t.comid = c.comid where c.sid = '" & sid & "' " _
               + "and t.tasktype is not null and t.taskdesc is not null " _
               + "and t.skill is not null and (t.ttime is not null and t.ttime <> 0) " _
               + "and t.rd is not null and (lang = '" & lang & "' or lang is null)"

                Else
                    sql = "select count(*) from complib where sid = '" & sid & "' and (c.lang = '" & lang & "' or c.lang is null)"
                End If


            ElseIf tab = "tdac" Then
                If cbmopt.Checked = True Then
                    sql = "select count(*) from complib c join comptasks t on t.comid = c.comid where c.sid is null " _
                        + "and t.tasktype is not null and t.taskdesc is not null " _
                + "and t.skill is not null and (t.ttime is not null and t.ttime <> 0) " _
                + "and t.rd is not null and (c.lang = '" & lang & "' or c.lang is null)"
                Else
                    sql = "select count(*) from complib where sid is null and (lang = '" & lang & "' or lang is null)"
                End If

            End If

            intPgCnt = comp.Scalar(sql)
        Else
            Dim csrch As String
            csrch = "%" & srch & "%"
            If tab = "tdmc" Then
                If cbmopt.Checked = True Then
                    sql = "select distinct count(*) from components c join functions f on f.func_id = c.func_id " _
                + "join equipment e on e.eqid = f.eqid join pmtasks t on t.comid = c.comid  where " _
                + "(c.compkey like '%" + csrch + "%' or c.compnum like '" + csrch + "%' or " _
                + "c.spl like '%" + csrch + "%' or c.compdesc like '%" + csrch + "%' or c.desig like '%" + csrch + "%') and e.siteid = '" & sid & "' " _
                + "and t.tasktype is not null and t.taskdesc is not null " _
                + "and t.skill is not null and (t.ttime is not null and t.ttime <> 0) " _
                + "and t.rd is not null and t.taskstatus is not null"
                Else
                    sql = "select distinct count(*) from components c join functions f on f.func_id = c.func_id " _
                    + "join equipment e on e.eqid = f.eqid where " _
                    + "(c.compkey like '%" + csrch + "%' or c.compnum like '" + csrch + "%' or " _
                    + "c.spl like '%" + csrch + "%' or c.compdesc like '%" + csrch + "%' or c.desig like '%" + csrch + "%') and e.siteid = '" & sid & "'"
                End If

                intPgCnt = comp.Scalar(sql)
            ElseIf tab = "tdsc" Then

                If cbmopt.Checked = True Then
                    sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] c left join [" & srvr & "].[" & mdb & "].[dbo].[comptasks] t on t.comid = c.comid " _
                + "where (c.compkey like '%" + csrch + "%' or c.compnum like '%" + csrch + "%' or " _
                + "c.spl like '%" + csrch + "%' or c.compdesc like '%" + csrch + "%' or c.desig like '%" + csrch + "%') " & cts & " and c.sid = '" & sid & "'"
                    Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] c left join [" & srvr & "].[" & mdb & "].[dbo].[comptasks] t on t.comid = c.comid " _
                + "where (c.compkey like '%' + @srch + '%' or c.compnum like '%' + @srch + '%' or " _
                + "c.spl like '%' + @srch + '%' or c.compdesc like '%' + @srch + '%' or c.desig like '%' + @srch + '%') " & cts & " and c.sid = '" & sid & "'")
                    Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
                    param7.Value = csrch
                    cmd1.Parameters.Add(param7)
                    intPgCnt = comp.ScalarHack(cmd1)

                Else
                    Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] " _
                + "where (compkey like '%' + @srch + '%' or compnum like '%' + @srch + '%' or " _
                + "spl like '%' + @srch + '%' or compdesc like '%' + @srch + '%' or desig like '%' + @srch + '%') " & cts & " or sid = '" & sid & "'")
                    Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
                    param7.Value = csrch
                    cmd1.Parameters.Add(param7)
                    intPgCnt = comp.ScalarHack(cmd1)
                End If


            ElseIf tab = "tdac" Then
                If cbmopt.Checked = True Then
                    Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] c left join [" & srvr & "].[" & mdb & "].[dbo].[comptasks] t on t.comid = c.comid " _
               + "where (c.compkey like '%' + @srch + '%' or c.compnum like '%' + @srch + '%' or " _
               + "c.spl like '%' + @srch + '%' or c.compdesc like '%' + @srch + '%' or c.desig like '%' + @srch + '%') " & cts)
                    Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
                    param7.Value = csrch
                    cmd1.Parameters.Add(param7)
                    intPgCnt = comp.ScalarHack(cmd1)
                Else
                    Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[complib] " _
               + "where (compkey like '%' + @srch + '%' or compnum like '%' + @srch + '%' or " _
               + "spl like '%' + @srch + '%' or compdesc like '%' + @srch + '%' or desig like '%' + @srch + '%') " & cts)
                    Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
                    param7.Value = csrch
                    cmd1.Parameters.Add(param7)
                    intPgCnt = comp.ScalarHack(cmd1)
                End If

            End If


        End If
        PageNumber = txtpg.Value
        'intPgCnt = func.Scalar(sql)
        intPgNav = comp.PageCount100(intPgCnt, PageSize)
        'intPgNav = comp.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        Dim ac As String = ""
        Dim acs As String = ""


        'Get Page
        If tab = "tdmc" Then
            If cbmopt.Checked = True Then
                sql = "usp_getAllCompPg_sp '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '" & sid & "'"
                dr = comp.GetRdrData(sql)
            Else
                sql = "usp_getAllCompPg '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '" & sid & "'"
                dr = comp.GetRdrData(sql)
            End If

        Else
            If cbmopt.Checked = True Then
                sql = "usp_getAllCompPgMdb_new_sp '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '" & usr & "', '" & ac & "','" & tab & "','" & sid & "','" & acs & "','" & lang & "'"
                Try
                    dr = comp.GetRdrData(sql)
                Catch ex As Exception
                    Try
                        Dim sql1 As String
                        sql1 = "insert into err_tmp (edate, sql) values (getdate(), '" & sql & "')"
                        comp.Update(sql1)
                        Dim strMessage As String = "Problem Copying Record - SQL Statement recorded for analysis by LAI"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Catch ex1 As Exception

                    End Try
                End Try

            Else
                sql = "usp_getAllCompPgMdb_new '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '" & usr & "', '" & ac & "','" & tab & "','" & sid & "','" & acs & "','" & lang & "'"
                dr = comp.GetRdrData(sql)
            End If

        End If

        'dr = comp.GetRdrData(sql)
        rptrcomprev.DataSource = dr
        rptrcomprev.DataBind()
        dr.Close()
        updatepos(intPgCnt)
    End Sub
    Private Sub updatepos(ByVal intPgCnt)
        PageNumber = txtpg.Value
        If intPgNav = 0 Then
            tdpg.InnerHtml = "Page 0 of 0"
        Else
            tdpg.InnerHtml = "Page " & PageNumber & " of " & intPgNav
        End If

        If PageNumber = 1 And intPgCnt = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = False

        ElseIf PageNumber = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = True

        ElseIf PageNumber = intPgNav Then
            btnnext.Enabled = False
            btnprev.Enabled = True

        Else
            btnnext.Enabled = True
            btnprev.Enabled = True
        End If
    End Sub
    Public Sub GetCo(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rb As RadioButton = New RadioButton
        Dim eqnum, eqdesc, spl, loc, model, serial, oem As String
        Dim ecr, cby, mby, cdat, mdat, cph, cm, eph, em As String
        Dim func, funcid, funcdesc, funcspl, comid, cdesc, cspl, cdesig, ctskcnt, ck
        Dim mfg, ac, sc As String
        Dim tab As String = lblcurtab.Value
        rb = sender
        tdoldcomp.InnerHtml = rb.ClientID
        For Each i As RepeaterItem In rptrcomprev.Items
            rb = CType(i.FindControl("rbfu"), RadioButton)
            comid = CType(i.FindControl("lblcomprevid"), Label).Text
            cdesc = CType(i.FindControl("lblcdesc"), Label).Text
            cspl = CType(i.FindControl("lblcspl"), Label).Text
            cdesig = CType(i.FindControl("lblcdesig"), Label).Text
            ctskcnt = CType(i.FindControl("lbltskcnt"), Label).Text

            mfg = CType(i.FindControl("lblmfg"), Label).Text
            ac = CType(i.FindControl("lblac"), Label).Text
            sc = CType(i.FindControl("lblsc"), Label).Text

            eqnum = CType(i.FindControl("lbleqnum"), Label).Text
            eqdesc = CType(i.FindControl("lbleqdesc"), Label).Text
            spl = CType(i.FindControl("lbleqspl"), Label).Text
            func = CType(i.FindControl("lblfunc"), Label).Text
            funcspl = CType(i.FindControl("lblfspl"), Label).Text
            funcdesc = CType(i.FindControl("lblfdesc"), Label).Text
            funcspl = CType(i.FindControl("lblfspl"), Label).Text
            'eq
            Dim ecby, ecph, ecm, ecd As String
            ecby = CType(i.FindControl("lblecby"), Label).Text
            ecph = CType(i.FindControl("lblecph"), Label).Text
            ecm = CType(i.FindControl("lblecm"), Label).Text
            ecd = CType(i.FindControl("lblcdate"), Label).Text
            'fu
            Dim fcby, fcph, fcm, fcd As String
            fcby = CType(i.FindControl("lblfcby"), Label).Text
            fcph = CType(i.FindControl("lblfcph"), Label).Text
            fcm = CType(i.FindControl("lblfcm"), Label).Text
            fcd = CType(i.FindControl("lblfcd"), Label).Text
            ck = CType(i.FindControl("lblck"), Label).Text
            rb.Checked = False
            coi = lblcoi.Value
            Try
                If coi <> "EMSC" Then
                    cball.Visible = False
                End If
            Catch ex As Exception

            End Try
            If tdoldcomp.InnerHtml = rb.ClientID Then
                lblcoid.Value = comid
                tdoldcomphdr.Attributes.Add("class", "view")
                tdoldcompdet.Attributes.Add("class", "details")
                tdoldcomptasks.Attributes.Add("class", "view")
                tpm = lbltpm.Value
                If cbmopt.Checked = True Then
                    mopt = "1"
                Else
                    mopt = "0"
                End If
                If tab = "tdmc" Then
                    geteq.Attributes.Add("src", "../complib/complibtaskview2.aspx?typ=pm&comid=" & comid & "&tpm=" & tpm & "&mopt=" & mopt)
                Else
                    geteq.Attributes.Add("src", "../complib/complibtaskview2.aspx?typ=comp&comid=" & comid & "&mopt=" & mopt)
                End If


                tblnewcomphdr.Attributes.Add("class", "details")
                tblnewcompdet.Attributes.Add("class", "details")
                rb.Checked = True
                tdoldcomp.InnerHtml = rb.Text.Trim
                tdolddesc.InnerHtml = cdesc
                tdoldspl.InnerHtml = cspl
                tdolddesig.InnerHtml = cdesig
                tdtskcnt.InnerHtml = ctskcnt
                lbltskcnt.Value = ctskcnt
                coi = lblcoi.Value
                Dim cochk As String = lbloldcoid.Value
                If ctskcnt = "0" Or ctskcnt = "" Then
                    cbow.Enabled = False
                    cbdl.Enabled = False
                    cbcpyo1.Enabled = False
                    cbcpyo2.Enabled = False
                Else
                    If cochk = "0" Or cochk = "" Then
                        cbow.Enabled = False
                        cbdl.Enabled = False
                        cbcpyo1.Enabled = False
                        cbcpyo2.Enabled = False
                    Else
                        cbow.Enabled = True
                        cbdl.Enabled = True
                        cbcpyo1.Enabled = True
                        cbcpyo2.Enabled = True
                        If coi = "EMSC" Then
                            cball.Visible = True
                            cball.Attributes.Add("onclick", "checkcb('al');")
                        Else
                            cball.Visible = False
                        End If
                    End If
                End If
                If coi <> "EMSC" Then
                    cball.Visible = False
                End If
                If lblcurtab.Value <> "tdmc" Then
                    'cbcpyo1.Enabled = False
                    'cbcpyo2.Enabled = False
                End If
                tdeqnum.InnerHtml = eqnum
                tdeqdesc.InnerHtml = eqdesc
                tdeqspl.InnerHtml = spl
                tdecby.InnerHtml = ecby
                tdecph.InnerHtml = ecph
                tdecm.InnerHtml = ecm
                tdecd.InnerHtml = ecd
                tdfunc.InnerHtml = func
                'tdfuncdesc.InnerHtml = funcdesc
                tdfspl.InnerHtml = funcspl
                tdfcby.InnerHtml = fcby
                tdfcph.InnerHtml = fcph
                tdfcm.InnerHtml = fcm
                tdfcd.InnerHtml = fcd
                lblnewcompkey.Value = ck
                tdoldmfg.InnerHtml = mfg
                tdoldac.InnerHtml = ac
                tdoldsc.InnerHtml = sc
            End If
        Next
    End Sub

    Private Sub ibtncopy_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtncopy.Click
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Dim strMessage As String = "Sorry, Your Session Expired While You Were Making Selections"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            lbllog.Value = "no"
            Exit Sub
        End Try
        Dim tab As String = lblcurtab.Value
        Dim xcpy As String
        If cbmopt.Checked = True Then
            xcpy = "1"
        Else
            xcpy = "0"
        End If

        Dim compflag, taskflag As Integer
        Dim newcomp As Integer
        If cbfm.Checked = True Then
            compflag = "1"
        Else
            compflag = "0"
        End If
        If cbtsks.Checked = True Then
            taskflag = "1"
        Else
            taskflag = "0"
        End If

        Dim rat As String
        If cbrat.Checked = True Then
            rat = "Y"
        Else
            rat = "N"
        End If

        Dim mdb As String = lbldb.Value
        db = lbldb.Value

        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString

        Dim roa As String
        If rbro.Checked = True Then
            roa = "R"
        ElseIf rbai.Checked = True Then
            roa = "A"
        ElseIf rbrs.Checked = True Then
            roa = "S"
        End If

        fuid = lblfuid.Value
        eqid = lbleqid.Value

        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        usr = lblusername.Value
        usr = Replace(usr, "'", Chr(180), , , vbTextCompare)

        tpm = lbltpm.Value

        Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim oloc As String = lbloloc.Value
        Dim cloc As String = ap

        Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
        coi = lblcoi.Value

        If tab <> "tdmc" Then
            comp.Open()
            CheckCopy()
            comp.Dispose()
        Else
            If cball.Checked = True And coi = "EMSC" Then
                Dim ac As String = lblcurcomp.Value
                Dim acd As String
                If Len(ac) > 0 Then
                    comp.Open()
                    Dim aci, afid As String
                    If Len(ac) > 0 Then
                        sql = "select comid, compdesc from components where compnum = '" & ac & "'"
                        ds = comp.GetDSData(sql)
                        Dim dt As New DataTable
                        dt = New DataTable
                        dt = ds.Tables(0)
                        Dim row As DataRow
                        For Each row In dt.Rows
                            aci = row("comid").ToString
                            acd = row("compdesc").ToString
                            afid = row("func_id").ToString

                            sql = "select eqid from functions where func_id = '" & afid & "'"
                            eqid = comp.Scalar(sql)

                            sql = "select dept_id, cellid from equipment where eqid = '" & eqid & "'"
                            dr = comp.GetRdrData(sql)
                            While dr.Read
                                did = dr.Item("dept_id").ToString
                                clid = dr.Item("cellid").ToString
                            End While
                            dr.Close()

                            If db = "0" Or db = "" Then
                                sql = "usp_copyCompSing1 '" & cid & "', '" & aci & "', '" & eqid & "', '" & fuid & "', " _
                                + "'" & compflag & "', '" & ac & "', '" & acd & "', '" & taskflag & "', '" & sid & "', " _
                                + "'" & did & "', '" & clid & "', '" & usr & "', '" & tpm & "','" & rat & "'"
                            Else
                                sql = "usp_copyCompSing1MDB '" & cid & "', '" & aci & "', '" & eqid & "', '" & fuid & "', " _
                                + "'" & compflag & "', '" & ac & "', '" & acd & "', '" & taskflag & "', '" & sid & "', " _
                                + "'" & did & "', '" & clid & "', '" & usr & "','" & mdb & "','" & orig & "','" & srvr & "','" & roa & "','" & rat & "','" & tpm & "','" & xcpy & "'"
                            End If
                            newcomp = comp.Scalar(sql)
                            lblnewcomid.Value = newcomp

                            DelCur(aci)
                            'pics here
                            If db = "0" Or db = "" Then
                                sql = "select pic_id, parfuncid, funcid, parcomid, comid, picurl, picurltn, picurltm from " _
                                + "pmpictures where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & newcomp & "' order by pic_id"
                            Else
                                sql = "select pic_id, parfuncid, funcid, parcomid, comid, picurl, picurltn, picurltm from " _
                                + "[" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & newcomp & "' order by pic_id"
                            End If

                            If db <> "0" Then
                                Try
                                    Dim ds As New DataSet
                                    ds = comp.GetDSData(sql)
                                    Dim fid, cmid, ofid, ocmid As String
                                    Dim i As Integer
                                    Dim f As Integer = 0
                                    Dim c As Integer = 0
                                    Dim x As Integer = ds.Tables(0).Rows.Count
                                    For i = 0 To (x - 1)
                                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                        t = ds.Tables(0).Rows(i)("picurl").ToString
                                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                        t = Replace(t, "..", "")
                                        tn = Replace(tn, "..", "")
                                        tm = Replace(tm, "..", "")
                                        If cmid = "" Then
                                            f = f + 1
                                            nt = "b-eqImg" & fid & "i"
                                            ntn = "btn-eqImg" & fid & "i"
                                            ntm = "btm-eqImg" & fid & "i"

                                            ont = "b-eqImg" & ofid & "i"
                                            ontn = "btn-eqImg" & ofid & "i"
                                            ontm = "btm-eqImg" & ofid & "i"
                                        Else
                                            c = c + 1
                                            nt = "c-eqImg" & cmid & "i"
                                            ntn = "ctn-eqImg" & cmid & "i"
                                            ntm = "ctm-eqImg" & cmid & "i"

                                            ont = "c-eqImg" & ocmid & "i"
                                            ontn = "ctn-eqImg" & ocmid & "i"
                                            ontm = "ctm-eqImg" & ocmid & "i"
                                        End If

                                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                                        ts = Replace(ts, "\", "/")
                                        td = Server.MapPath("\") & cloc & t
                                        td = Replace(td, "\", "/")
                                        Try
                                            System.IO.File.Copy(ts, td, True)

                                        Catch ex As Exception

                                        End Try

                                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                                        tn = Replace(tn, "\", "/")
                                        tnd = Server.MapPath("\") & cloc & tn
                                        tnd = Replace(tnd, "\", "/")
                                        Try
                                            System.IO.File.Copy(tns, tnd, True)

                                        Catch ex As Exception

                                        End Try

                                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                                        tms = Replace(tms, "\", "/")
                                        tmd = Server.MapPath("\") & cloc & tm
                                        tmd = Replace(tmd, "\", "/")
                                        Try
                                            System.IO.File.Copy(tms, tmd, True)
                                        Catch ex As Exception

                                        End Try

                                    Next
                                Catch ex As Exception

                                End Try
                            Else
                                Try
                                    Dim ds As New DataSet
                                    ds = comp.GetDSData(sql)
                                    Dim fid, cmid, ofid, ocmid As String
                                    Dim i As Integer
                                    Dim f As Integer = 0
                                    Dim c As Integer = 0
                                    Dim x As Integer = ds.Tables(0).Rows.Count
                                    For i = 0 To (x - 1)
                                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                        t = ds.Tables(0).Rows(i)("picurl").ToString
                                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                        t = Replace(t, "..", "")
                                        tn = Replace(tn, "..", "")
                                        tm = Replace(tm, "..", "")
                                        If cmid = "" Then
                                            f = f + 1
                                            nt = "b-eqImg" & fid & "i"
                                            ntn = "btn-eqImg" & fid & "i"
                                            ntm = "btm-eqImg" & fid & "i"

                                            ont = "b-eqImg" & ofid & "i"
                                            ontn = "btn-eqImg" & ofid & "i"
                                            ontm = "btm-eqImg" & ofid & "i"
                                        Else
                                            c = c + 1
                                            nt = "c-eqImg" & cmid & "i"
                                            ntn = "ctn-eqImg" & cmid & "i"
                                            ntm = "ctm-eqImg" & cmid & "i"

                                            ont = "c-eqImg" & ocmid & "i"
                                            ontn = "ctn-eqImg" & ocmid & "i"
                                            ontm = "ctm-eqImg" & ocmid & "i"
                                        End If

                                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                                        ts = Replace(ts, "\", "/")
                                        td = Server.MapPath("\") & ap & t
                                        td = Replace(td, "\", "/")
                                        Try
                                            System.IO.File.Copy(ts, td, True)
                                        Catch ex As Exception

                                        End Try


                                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                                        tn = Replace(tn, "\", "/")
                                        tnd = Server.MapPath("\") & ap & tn
                                        tnd = Replace(tnd, "\", "/")
                                        Try
                                            System.IO.File.Copy(tns, tnd, True)
                                        Catch ex As Exception

                                        End Try


                                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                                        tms = Replace(tms, "\", "/")
                                        tmd = Server.MapPath("\") & ap & tm
                                        tmd = Replace(tmd, "\", "/")
                                        Try
                                            System.IO.File.Copy(tms, tmd, True)
                                        Catch ex As Exception

                                        End Try

                                    Next
                                Catch ex As Exception

                                End Try


                            End If
                        Next
                        'lblret.Value = "getnew"
                        txtnewcomp.Text = ""
                        txtnewdesc.Text = ""
                        tdoldcomphdr.Attributes.Add("class", "details")
                        tdoldcompdet.Attributes.Add("class", "details")
                        tbltop.Attributes.Add("class", "details")
                        tdoldcomptasks.Attributes.Add("class", "details")
                        tblnewcompedit.Attributes.Add("class", "view")
                        ifrep.Attributes.Add("src", "../complib/complibedit.aspx?comid=" & newcomp & "&tpm=" & tpm)
                    End If
                    comp.Dispose()
                End If
            Else

                If Len(fuid) <> 0 AndAlso fuid <> "" AndAlso fuid <> "0" Then
                    cid = lblcid.Value


                    Dim nc As String = txtnewcomp.Text
                    nc = Replace(nc, "'", Chr(180), , , vbTextCompare)
                    nc = Replace(nc, "--", "-", , , vbTextCompare)
                    nc = Replace(nc, ";", ":", , , vbTextCompare)
                    If cbcpyo1.Checked = False And cbcpyo2.Checked = False Then
                        If Len(nc) > 100 Then
                            Dim strMessage As String = "Component is limited to 100 characters."
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            Exit Sub
                        End If
                    End If

                    If Len(nc) > 0 Or cbcpyo1.Checked = True Or cbcpyo2.Checked = True Then
                        Dim nd As String = txtnewdesc.Text
                        nd = Replace(nd, "'", Chr(180), , , vbTextCompare)
                        nd = Replace(nd, "--", "-", , , vbTextCompare)
                        nd = Replace(nd, ";", ":", , , vbTextCompare)
                        Dim oldcomp As String = lblcoid.Value





                        comp.Open()
                        Dim cocnt As Integer
                        Dim chk As String
                        Dim dchk As String
                        'Or cbcpyo1.Checked = True Or cbcpyo2.Checked = True
                        If cbow.Checked = True Then
                            chk = "1"
                        Else
                            chk = "0"
                        End If
                        If chk = "1" Then
                            DelCur()
                        End If

                        If cbcpyo1.Checked = False And cbcpyo2.Checked = False And cball.Checked = False Then
                            If db = "0" Or db = "" Then
                                sql = "select count(*) from components where func_id = '" & fuid & "' " _
                                                         + "and compnum = '" & nc & "' and compdesc = '" & nd & "'"
                            Else
                                sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[components] where func_id = '" & fuid & "' " _
                                                         + "and compnum = '" & nc & "' and compdesc = '" & nd & "'"
                            End If
                            cocnt = comp.Scalar(sql)
                        Else
                            cocnt = 0
                        End If




                        If cbdl.Checked = True Then
                            dchk = "1"
                        Else
                            dchk = "0"
                        End If

                        Dim cp1chk, cp2chk As String
                        If cbcpyo1.Checked = True Then
                            cp1chk = "1"
                        Else
                            cp1chk = "0"
                        End If
                        If cbcpyo2.Checked = True Then
                            cp2chk = "1"
                            cp1chk = "1"
                        Else
                            cp2chk = "0"
                        End If
                        If cocnt = 0 Then

                            If dchk = "1" Or cp2chk = "1" Then
                                MarkDelCur()
                            End If


                            Dim currcomp As String = lbloldcoid.Value
                            'rev only
                            '@cid int, @sid int, @did int, @clid int, @eqid int , @fuid int, @oldcomp int, @newcomp int, @user varchar(50), @rat varchar(1)
                            'as is
                            '@cid int, @sid int, @did int, @clid int, @eqid int , @fuid int, @oldcomp int, @newcomp int, @user varchar(50), @rat varchar(1)
                            'old comp is new comp to copy from
                            'new comp is current comp
                            If cp1chk = "1" Then
                                'need to check for missing failmodes
                                sql = "usp_compfailtrans '" & oldcomp & "', '" & currcomp & "'"
                                comp.Update(sql)
                                '[usp_copyTasks1tpmMDBsp] (@cid int, @sid int, @did int, @clid int, @eqid int , @fuid int, @user varchar(50), 
                                '@db varchar(50), @orig varchar(50),@app varchar(50), @oloc varchar(50), @srvr varchar(200)
                                If tpm = "Y" Then
                                    If cbmopt.Checked = True Then
                                        sql = "usp_copyCompTasks1tpmsp_x '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                                       + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & currcomp & "', '" & usr & "', " _
                                       + "'" & rat & "'"
                                    Else
                                        sql = "usp_copyCompTasks1tpmsp '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                                       + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & currcomp & "', '" & usr & "', " _
                                       + "'" & rat & "'"
                                    End If

                                Else
                                    If roa = "R" Then
                                        If cbmopt.Checked = True Then
                                            sql = "usp_copyCompTasks2sp_x '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                                        + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & currcomp & "', '" & usr & "', " _
                                        + "'" & rat & "'"
                                        Else
                                            sql = "usp_copyCompTasks2sp '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                                                                                + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & currcomp & "', '" & usr & "', " _
                                                                                + "'" & rat & "'"
                                        End If

                                    ElseIf roa = "S" Then
                                        If cbmopt.Checked = True Then
                                            sql = "usp_copyCompTasks2sp_xs '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                                        + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & currcomp & "', '" & usr & "', " _
                                        + "'" & rat & "'"
                                        Else
                                            sql = "usp_copyCompTasks2sp_s '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                                                                                + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & currcomp & "', '" & usr & "', " _
                                                                                + "'" & rat & "'"
                                        End If


                                    Else
                                        If cbmopt.Checked = True Then
                                            sql = "usp_copyCompTasks1sp_x '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                                       + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & currcomp & "', '" & usr & "', " _
                                       + "'" & rat & "'"
                                        Else
                                            sql = "usp_copyCompTasks1sp '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                                       + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & currcomp & "', '" & usr & "', " _
                                       + "'" & rat & "'"
                                        End If

                                    End If
                                End If
                                comp.Update(sql)
                                newcomp = currcomp
                                lblnewcomid.Value = newcomp
                            Else
                                If db = "0" Or db = "" Then
                                    sql = "usp_copyCompSing1 '" & cid & "', '" & oldcomp & "', '" & eqid & "', '" & fuid & "', " _
                                    + "'" & compflag & "', '" & nc & "', '" & nd & "', '" & taskflag & "', '" & sid & "', " _
                                    + "'" & did & "', '" & clid & "', '" & usr & "', '" & tpm & "','" & rat & "'"
                                Else
                                    sql = "usp_copyCompSing1MDB '" & cid & "', '" & oldcomp & "', '" & eqid & "', '" & fuid & "', " _
                                    + "'" & compflag & "', '" & nc & "', '" & nd & "', '" & taskflag & "', '" & sid & "', " _
                                    + "'" & did & "', '" & clid & "', '" & usr & "','" & mdb & "','" & orig & "','" & srvr & "','" & roa & "','" & rat & "','" & tpm & "','" & xcpy & "'"
                                End If
                                newcomp = comp.Scalar(sql)
                                lblnewcomid.Value = newcomp
                            End If


                            'sql = "usp_copyCompSing '" & cid & "', '" & oldcomp & "', '" & eqid & "', '" & fuid & "', '" & compflag & "', '" & nc & "', '" & nd & "'"
                            'Try
                            'newcomp = comp.Scalar(sql)
                            'lblnewcomid.Value = newcomp
                            'lblret.Value = "getnew"

                            '***Added to speed up result time
                            Dim fsi As Integer
                            Try
                                sql = "select count(*) from pmsitefm where siteid = '" & sid & "'"
                                fsi = comp.Scalar(sql)
                                If fsi > 0 Then
                                    sql = "usp_updateSiteFM '" & cid & "', '" & sid & "'"
                                    comp.Update(sql)
                                End If
                            Catch ex As Exception

                            End Try
                            sql = "iusp_updateCompFailId '" & eqid & "'"
                            comp.Update(sql)

                            '***

                            txtnewcomp.Text = ""
                            txtnewdesc.Text = ""
                            tdoldcomphdr.Attributes.Add("class", "details")
                            tdoldcompdet.Attributes.Add("class", "details")
                            tbltop.Attributes.Add("class", "details")
                            tdoldcomptasks.Attributes.Add("class", "details")
                            tblnewcompedit.Attributes.Add("class", "view")
                            ifrep.Attributes.Add("src", "../complib/complibedit.aspx?comid=" & newcomp & "&tpm=" & tpm)

                            'PopNewComp(newcomp)
                            'tdoldcomphdr.Attributes.Add("class", "details")
                            'tdoldcompdet.Attributes.Add("class", "details")
                            'tbltop.Attributes.Add("class", "details")
                            'tblnewcomphdr.Attributes.Add("class", "view")
                            'tblnewcompdet.Attributes.Add("class", "view")
                            'If cp1chk = "1" Then
                            'If db = "0" Or db = "" Then
                            'sql = "select pic_id, parfuncid, funcid, parcomid, comid, picurl, picurltn, picurltm from " _
                            '+ "pmpictures where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & oldcomp & "' order by pic_id"
                            'Else
                            'sql = "select pic_id, parfuncid, funcid, parcomid, comid, picurl, picurltn, picurltm from " _
                            '+ "[" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & oldcomp & "' order by pic_id"
                            'End If
                            'Else
                            If db = "0" Or db = "" Then
                                sql = "select pic_id, parfuncid, funcid, parcomid, comid, picurl, picurltn, picurltm from " _
                                + "pmpictures where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & newcomp & "' order by pic_id"
                            Else
                                sql = "select pic_id, parfuncid, funcid, parcomid, comid, picurl, picurltn, picurltm from " _
                                + "[" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & newcomp & "' order by pic_id"
                            End If
                            'End If


                            If db <> "0" Then
                                Try
                                    Dim ds As New DataSet
                                    ds = comp.GetDSData(sql)
                                    Dim fid, cmid, ofid, ocmid As String
                                    Dim i As Integer
                                    Dim f As Integer = 0
                                    Dim c As Integer = 0
                                    Dim x As Integer = ds.Tables(0).Rows.Count
                                    For i = 0 To (x - 1)
                                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                        t = ds.Tables(0).Rows(i)("picurl").ToString
                                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                        t = Replace(t, "..", "")
                                        tn = Replace(tn, "..", "")
                                        tm = Replace(tm, "..", "")
                                        If cmid = "" Then
                                            f = f + 1
                                            nt = "b-eqImg" & fid & "i"
                                            ntn = "btn-eqImg" & fid & "i"
                                            ntm = "btm-eqImg" & fid & "i"

                                            ont = "b-eqImg" & ofid & "i"
                                            ontn = "btn-eqImg" & ofid & "i"
                                            ontm = "btm-eqImg" & ofid & "i"
                                        Else
                                            c = c + 1
                                            nt = "c-eqImg" & cmid & "i"
                                            ntn = "ctn-eqImg" & cmid & "i"
                                            ntm = "ctm-eqImg" & cmid & "i"

                                            ont = "c-eqImg" & ocmid & "i"
                                            ontn = "ctn-eqImg" & ocmid & "i"
                                            ontm = "ctm-eqImg" & ocmid & "i"
                                        End If

                                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                                        ts = Replace(ts, "\", "/")
                                        td = Server.MapPath("\") & cloc & t
                                        td = Replace(td, "\", "/")
                                        Try
                                            System.IO.File.Copy(ts, td, True)

                                        Catch ex As Exception

                                        End Try

                                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                                        tn = Replace(tn, "\", "/")
                                        tnd = Server.MapPath("\") & cloc & tn
                                        tnd = Replace(tnd, "\", "/")
                                        Try
                                            System.IO.File.Copy(tns, tnd, True)

                                        Catch ex As Exception

                                        End Try

                                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                                        tms = Replace(tms, "\", "/")
                                        tmd = Server.MapPath("\") & cloc & tm
                                        tmd = Replace(tmd, "\", "/")
                                        Try
                                            System.IO.File.Copy(tms, tmd, True)
                                        Catch ex As Exception

                                        End Try

                                    Next
                                Catch ex As Exception

                                End Try
                            Else
                                Try
                                    Dim ds As New DataSet
                                    ds = comp.GetDSData(sql)
                                    Dim fid, cmid, ofid, ocmid As String
                                    Dim i As Integer
                                    Dim f As Integer = 0
                                    Dim c As Integer = 0
                                    Dim x As Integer = ds.Tables(0).Rows.Count
                                    For i = 0 To (x - 1)
                                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                        t = ds.Tables(0).Rows(i)("picurl").ToString
                                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                        t = Replace(t, "..", "")
                                        tn = Replace(tn, "..", "")
                                        tm = Replace(tm, "..", "")
                                        If cmid = "" Then
                                            f = f + 1
                                            nt = "b-eqImg" & fid & "i"
                                            ntn = "btn-eqImg" & fid & "i"
                                            ntm = "btm-eqImg" & fid & "i"

                                            ont = "b-eqImg" & ofid & "i"
                                            ontn = "btn-eqImg" & ofid & "i"
                                            ontm = "btm-eqImg" & ofid & "i"
                                        Else
                                            c = c + 1
                                            nt = "c-eqImg" & cmid & "i"
                                            ntn = "ctn-eqImg" & cmid & "i"
                                            ntm = "ctm-eqImg" & cmid & "i"

                                            ont = "c-eqImg" & ocmid & "i"
                                            ontn = "ctn-eqImg" & ocmid & "i"
                                            ontm = "ctm-eqImg" & ocmid & "i"
                                        End If

                                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                                        ts = Replace(ts, "\", "/")
                                        td = Server.MapPath("\") & ap & t
                                        td = Replace(td, "\", "/")
                                        Try
                                            System.IO.File.Copy(ts, td, True)
                                        Catch ex As Exception

                                        End Try


                                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                                        tn = Replace(tn, "\", "/")
                                        tnd = Server.MapPath("\") & ap & tn
                                        tnd = Replace(tnd, "\", "/")
                                        Try
                                            System.IO.File.Copy(tns, tnd, True)
                                        Catch ex As Exception

                                        End Try


                                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                                        tms = Replace(tms, "\", "/")
                                        tmd = Server.MapPath("\") & ap & tm
                                        tmd = Replace(tmd, "\", "/")
                                        Try
                                            System.IO.File.Copy(tms, tmd, True)
                                        Catch ex As Exception

                                        End Try

                                    Next
                                Catch ex As Exception

                                End Try


                            End If

                            'Catch ex As Exception
                            'Dim strMessage As String = "Problem Saving Record\nPlease review selections and try again."
                            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            'End Try
                        Else
                            Dim strMessage As String = "Cannot Enter Duplicate Component\n     for a Function"
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End If
                        comp.Dispose()
                    End If
                    Try
                        login = HttpContext.Current.Session("Logged_IN").ToString()
                    Catch ex As Exception
                        Dim strMessage As String = "Sorry, Your Session Expired While You Were Making Selections"
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "no"
                        Exit Sub
                    End Try
                    'Dim strMessage1 As String = "Problem Retrieving Function ID\nPlease Reselect Equipment and try Again"
                    'Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                    'lbllog.Value = "noeqid"
                End If

            End If

        End If



    End Sub
    Private Sub CheckCopy()
        Dim lang As String = lblfslang.Value
        cid = "0" 'lblcid.Value
        Dim eqcnt As Integer
        Dim mdb As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim typ As String = "dest"

        Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
        Dim piccnt As Integer
        Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")

        Dim fid, cmid, ofid, ocmid As String
        Dim oloc As String = lbloloc.Value
        Dim cloc As String = ap

        Dim neweq As String = txtnewcomp.Text 'lblcomp.Value
        neweq = comp.ModString1(neweq)

        Dim oldcomp, eqid, fuid, compflag, nc, nd, taskflag, sid, did, clid, usr, newcomp As String
        Dim newkey As String = lblnewcompkey.Value
        tpm = lbltpm.Value

        fuid = lblfuid.Value
        usr = lblusername.Value
        sid = lblsid.Value

        taskflag = "1"
        compflag = "1"
        coi = lblcoi.Value
        If cball.Checked = True And coi = "EMSC" Then
            Dim ac As String = lblcurcomp.Value
            Dim acd, afid As String
            If Len(ac) > 0 Then
                'comp.Open()
                Dim aci As String
                If Len(ac) > 0 Then
                    sql = "select comid, compdesc, func_id from components where compnum = '" & ac & "'"
                    ds = comp.GetDSData(sql)
                    Dim dt As New DataTable
                    dt = New DataTable
                    dt = ds.Tables(0)
                    Dim row As DataRow
                    For Each row In dt.Rows
                        aci = row("comid").ToString
                        acd = row("compdesc").ToString
                        afid = row("func_id").ToString

                        sql = "select eqid from functions where func_id = '" & afid & "'"
                        eqid = comp.Scalar(sql)

                        sql = "select dept_id, cellid from equipment where eqid = '" & eqid & "'"
                        dr = comp.GetRdrData(sql)
                        While dr.Read
                            did = dr.Item("dept_id").ToString
                            clid = dr.Item("cellid").ToString
                        End While
                        dr.Close()

                        sql = "usp_copyCompSing1MDBco '" & cid & "', '" & aci & "', '" & eqid & "', '" & fuid & "', " _
                        + "'" & compflag & "', '" & ac & "', '" & acd & "', '" & taskflag & "', '" & sid & "', " _
                        + "'" & did & "', '" & clid & "', '" & usr & "','" & mdb & "','" & orig & "','" & srvr & "','" & newkey & "','" & tpm & "'"

                        newcomp = comp.Scalar(sql)
                        lblnewcomid.Value = newcomp

                        DelCur(aci)
                        'pics here

                        sql = "select pic_id, parfuncid, funcid, parcomid, comid, picurl, picurltn, picurltm from " _
                        + "[" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & newcomp & "' order by pic_id"



                        Try
                            Dim ds As New DataSet
                            ds = comp.GetDSData(sql)
                            Dim i As Integer
                            Dim f As Integer = 0
                            Dim c As Integer = 0
                            Dim x As Integer = ds.Tables(0).Rows.Count
                            For i = 0 To (x - 1)
                                piccnt = ds.Tables(0).Rows(i)("piccnt").ToString
                                If piccnt = 0 Then
                                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                                    lblnewcomid.Value = cmid
                                Else
                                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                                    lblnewcomid.Value = cmid
                                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                    t = ds.Tables(0).Rows(i)("picurl").ToString
                                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                    t = Replace(t, "..", "")
                                    tn = Replace(tn, "..", "")
                                    tm = Replace(tm, "..", "")
                                    If cmid = "" Then
                                        f = f + 1
                                        nt = "b-eqImg" & fid & "i"
                                        ntn = "btn-eqImg" & fid & "i"
                                        ntm = "btm-eqImg" & fid & "i"

                                        ont = "b-eqImg" & ofid & "i"
                                        ontn = "btn-eqImg" & ofid & "i"
                                        ontm = "btm-eqImg" & ofid & "i"
                                    Else
                                        c = c + 1
                                        nt = "c-eqImg" & cmid & "i"
                                        ntn = "ctn-eqImg" & cmid & "i"
                                        ntm = "ctm-eqImg" & cmid & "i"

                                        ont = "c-eqImg" & ocmid & "i"
                                        ontn = "ctn-eqImg" & ocmid & "i"
                                        ontm = "ctm-eqImg" & ocmid & "i"
                                    End If

                                    ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                                    ts = Replace(ts, "\", "/")
                                    td = Server.MapPath("\") & cloc & t
                                    td = Replace(td, "\", "/")


                                    tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                                    tn = Replace(tn, "\", "/")
                                    tnd = Server.MapPath("\") & cloc & tn
                                    tnd = Replace(tnd, "\", "/")


                                    tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                                    tms = Replace(tms, "\", "/")
                                    tmd = Server.MapPath("\") & cloc & tm
                                    tmd = Replace(tmd, "\", "/")

                                    '***NEED WWW CONFIG REF HERE***
                                    'ts = Replace(ts, "wwwlai1", "wwwlai2")
                                    'tns = Replace(ts, "wwwlai1", "wwwlai2")
                                    'tms = Replace(ts, "wwwlai1", "wwwlai2")
                                    Try
                                        System.IO.File.Copy(ts, td, True)
                                        System.IO.File.Copy(tns, tnd, True)
                                        System.IO.File.Copy(tms, tmd, True)
                                    Catch ex As Exception

                                    End Try

                                End If

                            Next
                        Catch ex As Exception

                        End Try



                    Next
                    'lblret.Value = "getnew"
                    txtnewcomp.Text = ""
                    txtnewdesc.Text = ""
                    tdoldcomphdr.Attributes.Add("class", "details")
                    tdoldcompdet.Attributes.Add("class", "details")
                    tbltop.Attributes.Add("class", "details")
                    tdoldcomptasks.Attributes.Add("class", "details")
                    tblnewcompedit.Attributes.Add("class", "view")
                    ifrep.Attributes.Add("src", "../complib/complibedit.aspx?comid=" & newcomp & "&tpm=" & tpm)
                End If
                comp.Dispose()
            End If
        Else
            If Len(neweq) > 0 Or cbcpyo1.Checked = True Or cbcpyo2.Checked = True Then
                If cbcpyo1.Checked = False And cbcpyo2.Checked = False Then
                    If Len(neweq) > 100 Then
                        Dim strMessage1 As String = "Component Name limited to 100 characters."
                        Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                        Exit Sub
                    End If
                End If


                '*** Multi Add ***

                Dim cmd0 As New SqlCommand("select count(*) from [" & srvr & "].[" & orig & "].[dbo].[complib] where compnum = @neweqand (lang = '" & lang & "' or lang is null)")
                Dim param1 = New SqlParameter("@srvr", SqlDbType.VarChar)
                param1.Value = srvr
                cmd0.Parameters.Add(param1)
                Dim param2 = New SqlParameter("@mdb", SqlDbType.VarChar)
                param2.Value = mdb
                cmd0.Parameters.Add(param2)
                Dim param3 = New SqlParameter("@cid", SqlDbType.Int)
                param3.Value = cid
                cmd0.Parameters.Add(param3)
                Dim param4 = New SqlParameter("@neweq", SqlDbType.VarChar)
                param4.Value = neweq
                cmd0.Parameters.Add(param4)
                'copy.Open()
                'eqcnt = comp.ScalarHack(cmd0)
                '
                Dim dchk As String
                Dim chk As String
                'Or cbcpyo1.Checked = True Or cbcpyo2.Checked = True
                If cbow.Checked = True Then
                    chk = "1"
                Else
                    chk = "0"
                End If
                If chk = "1" Then
                    DelCur()
                End If
                If cbcpyo1.Checked = False And cbcpyo2.Checked = False Then
                    sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[components] where func_id = '" & fuid & "' " _
                     + " and compnum = '" & neweq & "'"
                    eqcnt = comp.Scalar(sql)
                Else
                    eqcnt = 0
                End If

                Dim cp1chk, cp2chk As String
                If cbcpyo1.Checked = True Then
                    cp1chk = "1"
                Else
                    cp1chk = "0"
                End If
                If cbcpyo2.Checked = True Then
                    cp2chk = "1"
                    cp1chk = "1"
                Else
                    cp2chk = "0"
                End If

                If eqcnt = 0 Then
                    'Try

                    If cbdl.Checked = True Then
                        dchk = "1"
                    Else
                        dchk = "0"
                    End If

                    If dchk = "1" Or cp2chk = "1" Then
                        MarkDelCur()
                    End If

                    CopyComp()
                    lblret.Value = "getnew"
                    'Catch ex As Exception
                    'lblcopyret.Value = ""
                    'Dim strMessage2 As String = ex.Message
                    'strMessage2 = "Problem Saving Record\nPlease review selections and try again." 'Replace(strMessage2, "'", Chr(180), , , vbTextCompare) '
                    'strMessage2 = Replace(strMessage2, """", Chr(180), , , vbTextCompare)
                    'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                    'End Try
                Else
                    Dim strMessage3 As String = "Component Name Must Be Unique within a Function!"
                    Utilities.CreateMessageAlert(Me, strMessage3, "strKey1")
                End If
            Else
                Dim strMessage4 As String = "No Component Name Found!"
                Utilities.CreateMessageAlert(Me, strMessage4, "strKey1")
            End If
        End If




    End Sub


    Private Sub CopyComp()
        Dim oldcomp, eqid, fuid, compflag, nc, nd, taskflag, sid, did, clid, usr, typ, newcomp As String
        Dim mdb As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString
        If cbcpyo1.Checked = False And cbcpyo2.Checked = False Then
            nc = txtnewcomp.Text
            nc = comp.ModString1(nc)
            If Len(nc) > 100 Then
                Dim strMessage1 As String = "Component Name limited to 100 characters."
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
            nd = txtnewdesc.Text
            nd = comp.ModString1(nd)
            If Len(nd) > 100 Then
                Dim strMessage1 As String = "Component Description limited to 100 characters."
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
        End If

        Dim cp1chk, cp2chk As String
        If cbcpyo1.Checked = True Then
            cp1chk = "1"
        Else
            cp1chk = "0"
        End If
        If cbcpyo2.Checked = True Then
            cp2chk = "1"
            cp1chk = "1"
        Else
            cp2chk = "0"
        End If

        taskflag = "1"
        compflag = "1"
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        fuid = lblfuid.Value
        typ = "dest"
        sid = lblsid.Value
        oldcomp = lblcoid.Value
        usr = lblusername.Value

        Dim newkey As String = lblnewcompkey.Value
        tpm = lbltpm.Value
        '[usp_copyCompTasks1co] (@cid int, @sid int, @did int, @clid int, @eqid int , @fuid int, @oldcomp int, @newcomp int, @user varchar(50))
        If cp1chk = "1" Then
            newcomp = lbloldcoid.Value
            lblnewcomid.Value = newcomp
            If tpm = "Y" Then
                sql = "usp_copyCompTasks1tpmco '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & newcomp & "', '" & usr & "'"
            Else
                sql = "usp_copyCompTasks1co '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', " _
                + "'" & eqid & "', '" & fuid & "', '" & oldcomp & "', '" & newcomp & "', '" & usr & "'"
            End If

        Else
            sql = "usp_copyCompSing1MDBco '" & cid & "', '" & oldcomp & "', '" & eqid & "', '" & fuid & "', " _
       + "'" & compflag & "', '" & nc & "', '" & nd & "', '" & taskflag & "', '" & sid & "', " _
       + "'" & did & "', '" & clid & "', '" & usr & "','" & mdb & "','" & orig & "','" & srvr & "','" & newkey & "','" & tpm & "'"
        End If


        Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")

        Dim fid, cmid, ofid, ocmid As String
        Dim oloc As String = lbloloc.Value
        Dim cloc As String = ap
        Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
        Dim piccnt As Integer
        If cp1chk <> "1" Then
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    ds = comp.GetDSData(sql)
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        piccnt = ds.Tables(0).Rows(i)("piccnt").ToString
                        If piccnt = 0 Then
                            cmid = ds.Tables(0).Rows(i)("comid").ToString
                            lblnewcomid.Value = cmid
                        Else
                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                            cmid = ds.Tables(0).Rows(i)("comid").ToString
                            lblnewcomid.Value = cmid
                            ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                            t = ds.Tables(0).Rows(i)("picurl").ToString
                            tn = ds.Tables(0).Rows(i)("picurltn").ToString
                            tm = ds.Tables(0).Rows(i)("picurltm").ToString
                            t = Replace(t, "..", "")
                            tn = Replace(tn, "..", "")
                            tm = Replace(tm, "..", "")
                            If cmid = "" Then
                                f = f + 1
                                nt = "b-eqImg" & fid & "i"
                                ntn = "btn-eqImg" & fid & "i"
                                ntm = "btm-eqImg" & fid & "i"

                                ont = "b-eqImg" & ofid & "i"
                                ontn = "btn-eqImg" & ofid & "i"
                                ontm = "btm-eqImg" & ofid & "i"
                            Else
                                c = c + 1
                                nt = "c-eqImg" & cmid & "i"
                                ntn = "ctn-eqImg" & cmid & "i"
                                ntm = "ctm-eqImg" & cmid & "i"

                                ont = "c-eqImg" & ocmid & "i"
                                ontn = "ctn-eqImg" & ocmid & "i"
                                ontm = "ctm-eqImg" & ocmid & "i"
                            End If

                            ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                            ts = Replace(ts, "\", "/")
                            td = Server.MapPath("\") & cloc & t
                            td = Replace(td, "\", "/")


                            tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                            tn = Replace(tn, "\", "/")
                            tnd = Server.MapPath("\") & cloc & tn
                            tnd = Replace(tnd, "\", "/")


                            tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                            tms = Replace(tms, "\", "/")
                            tmd = Server.MapPath("\") & cloc & tm
                            tmd = Replace(tmd, "\", "/")

                            '***NEED WWW CONFIG REF HERE***
                            'ts = Replace(ts, "wwwlai1", "wwwlai2")
                            'tns = Replace(ts, "wwwlai1", "wwwlai2")
                            'tms = Replace(ts, "wwwlai1", "wwwlai2")
                            Try
                                System.IO.File.Copy(ts, td, True)
                                System.IO.File.Copy(tns, tnd, True)
                                System.IO.File.Copy(tms, tmd, True)
                            Catch ex As Exception

                            End Try

                        End If

                    Next
                Catch ex As Exception

                End Try
                'insert pm task image file copy here for mdb *********************************************

            Else
                'Try
                Dim ds As New DataSet
                Dim tst As String = sql
                ds = comp.GetDSData(sql)
                Dim i As Integer
                Dim f As Integer = 0
                Dim c As Integer = 0
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    piccnt = ds.Tables(0).Rows(i)("piccnt").ToString
                    If piccnt = 0 Then
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        lblnewcomid.Value = cmid
                    Else
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        'fid = ds.Tables(0).Rows(i)("funcid").ToString
                        'ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        lblnewcomid.Value = cmid
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            If typ = "site" Then
                                c = c + 1
                                nt = "c-clImg" & cmid & "i"
                                ntn = "ctn-clImg" & cmid & "i"
                                ntm = "ctm-clImg" & cmid & "i"

                                ont = "c-clImg" & ocmid & "i"
                                ontn = "ctn-clImg" & ocmid & "i"
                                ontm = "ctm-clImg" & ocmid & "i"
                            Else
                                c = c + 1
                                nt = "c-coImg" & cmid & "i"
                                ntn = "ctn-coImg" & cmid & "i"
                                ntm = "ctm-coImg" & cmid & "i"

                                ont = "c-clImg" & ocmid & "i"
                                ontn = "ctn-clImg" & ocmid & "i"
                                ontm = "ctm-clImg" & ocmid & "i"

                            End If

                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")

                        Try
                            System.IO.File.Copy(ts, td, True)
                            System.IO.File.Copy(tns, tnd, True)
                            System.IO.File.Copy(tms, tmd, True)
                        Catch ex As Exception

                        End Try

                    End If
                Next
                'Catch ex As Exception

                'End Try


            End If
        Else
            comp.Update(sql)
        End If


        newcomp = lblnewcomid.Value
        'PopNewComp(newcomp)
        'tdoldcomphdr.Attributes.Add("class", "details")
        'tdoldcompdet.Attributes.Add("class", "details")
        'tbltop.Attributes.Add("class", "details")
        'tblnewcomphdr.Attributes.Add("class", "view")
        'tblnewcompdet.Attributes.Add("class", "view")
        tpm = lbltpm.Value
        txtnewcomp.Text = ""
        txtnewdesc.Text = ""
        'tdoldcomphdr.Attributes.Add("class", "details")
        'tdoldcompdet.Attributes.Add("class", "details")
        'tbltop.Attributes.Add("class", "details")
        'tdoldcomptasks.Attributes.Add("class", "details")
        'tblnewcompedit.Attributes.Add("class", "view")
        'ifrep.Attributes.Add("src", "../complib/complibedit.aspx?comid=" & newcomp & "&tpm=" & tpm)
    End Sub

    Private Sub PopNewComp(ByVal newcomp As String)
        'PopAC()
        Dim ac, acid As String
        cid = lblcid.Value
        sql = "usp_getCompDetails '" + cid + "', '" + newcomp + "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdnewcomp.InnerHtml = dr.Item("compnum").ToString
            txtnewspl.Text = dr.Item("spl").ToString
            txtdesc.Text = dr.Item("compdesc").ToString
            txtdesig.Text = dr.Item("desig").ToString
            tdneweq.InnerHtml = dr.Item("eqnum").ToString
            tdneweqdesc.InnerHtml = dr.Item("eqdesc").ToString
            tdneweqspl.InnerHtml = dr.Item("eqspl").ToString
            tdnewecby.InnerHtml = dr.Item("createdby").ToString
            tdneweqecph.InnerHtml = dr.Item("ecph").ToString
            tdneweqemm.InnerHtml = dr.Item("emm").ToString
            tdneweqecd.InnerHtml = dr.Item("createdate").ToString
            tdnewfunc.InnerHtml = dr.Item("func").ToString
            tdnewfuncdesc.InnerHtml = dr.Item("func_desc").ToString
            tdnewfuncspl.InnerHtml = dr.Item("fspl").ToString
            tdnewfuncfcby.InnerHtml = dr.Item("fcby").ToString
            tdnewfuncfcph.InnerHtml = dr.Item("fcph").ToString
            tdnewfuncfmm.InnerHtml = dr.Item("fcm").ToString
            tdnewfuncfcd.InnerHtml = dr.Item("fcd").ToString

            txtmfg.Text = dr.Item("mfg").ToString
            ac = dr.Item("assetclass").ToString
            acid = dr.Item("acid").ToString
        End While
        dr.Close()
        Try
            ddac.SelectedValue = acid
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ibtnreturn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnreturn.Click
        Dim spl As String = txtnewspl.Text
        spl = comp.ModString1(spl)

        If Len(spl) = 0 Then spl = ""
        If Len(spl) > 200 Then
            Dim strMessage As String = "Special Identifier is limited to 200 characters."
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim desc As String = txtdesc.Text
        desc = comp.ModString1(desc)
        If Len(desc) = 0 Then desc = ""
        If Len(desc) > 100 Then
            Dim strMessage As String = "Equipment Description is limited to 100 characters."
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim desig As String = txtdesig.Text
        desig = comp.ModString1(desig)
        If Len(desig) = 0 Then desig = ""
        If Len(desig) > 200 Then
            Dim strMessage As String = "Designation is limited to 200 characters."
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim ac, acid As String
        If ddac.SelectedIndex <> 0 Then
            ac = ddac.SelectedItem.ToString
            acid = ddac.SelectedValue.ToString
        End If
        Dim mfg As String = txtmfg.Text
        mfg = comp.ModString1(mfg)
        If Len(mfg) > 50 Then
            Dim strMessage As String = "Character limit for Mfg is 50"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim coid As String = lblnewcomid.Value
        sql = "update Components set spl = '" & spl & "', " _
        + "compdesc = '" & desc & "', " _
        + "desig = '" & desig & "', mfg = '" & mfg & "', acid = '" & acid & "', assetclass = '" & ac & "' where comid = '" & coid & "'"
        comp.Open()
        Try
            comp.Update(sql)
        Catch ex As Exception
            Dim strMessage As String = "Problem Saving Record\nPlease review selections and try again."
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        tbltop.Attributes.Add("class", "view")
        tdoldcomphdr.Attributes.Add("class", "details")
        tdoldcompdet.Attributes.Add("class", "details")
        tblnewcomphdr.Attributes.Add("class", "details")
        tblnewcompdet.Attributes.Add("class", "details")

        txtpg.Value = "1"
        PageNumber = txtpg.Value
        PopCompRev(PageNumber)
        comp.Dispose()
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        tbltop.Attributes.Add("class", "view")
        tdoldcomphdr.Attributes.Add("class", "details")
        tdoldcompdet.Attributes.Add("class", "details")
        tblnewcomphdr.Attributes.Add("class", "details")
        tblnewcompdet.Attributes.Add("class", "details")

        txtpg.Value = "1"
        comp.Open()
        PageNumber = txtpg.Value
        PopCompRev(PageNumber)
        comp.Dispose()
    End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        comp.Open()
        PopCompRev(PageNumber)
        comp.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprev.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        comp.Open()
        PopCompRev(PageNumber)
        comp.Dispose()
    End Sub

    Private Sub rptrcomprev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrcomprev.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                                 e.Item.ItemType <> ListItemType.Footer Then
            Dim pic As HtmlImage = CType(e.Item.FindControl("imgpic"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim fid As String = DataBinder.Eval(e.Item.DataItem, "func_id").ToString
            Dim cid As String = DataBinder.Eval(e.Item.DataItem, "comid").ToString
            Dim cnt As String = DataBinder.Eval(e.Item.DataItem, "cpcnt").ToString
            Dim tab As String = lblcurtab.Value

            If tab = "tdmc" Then
                If cnt <> "0" Then
                    pic.Attributes.Add("onclick", "getfuport('" & id & "','" & fid & "','" & cid & "');")
                    pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpic.gif")
                Else
                    pic.Attributes.Add("onclick", "")
                    pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpicdis.gif")
                End If
            Else
                If cnt <> "0" Then
                    pic.Attributes.Add("onclick", "getcoport('" & cid & "');")
                    pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpic.gif")
                Else
                    pic.Attributes.Add("onclick", "")
                    pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpicdis.gif")
                End If
            End If

        End If
    End Sub
End Class
