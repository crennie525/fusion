﻿Public Class meterlookdialog
    Inherits System.Web.UI.Page
    Dim eqid, typ As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try

        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            typ = Request.QueryString("typ").ToString
            iftot.Attributes.Add("src", "meterlook.aspx?eqid=" & eqid & "&typ=" & typ & "&date=" & Now)

        End If
    End Sub

End Class