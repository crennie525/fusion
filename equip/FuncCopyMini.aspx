<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FuncCopyMini.aspx.vb" Inherits="lucy_r12.FuncCopyMini" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>FuncCopyMini</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<LINK rel="stylesheet" type="text/css" href="../styles/reports.css">
		<script  src="../scripts/gridnav.js"></script>
		<script  src="../scripts1/FuncCopyMiniaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkfu();" >
		<form id="form1" method="post" runat="server">
			<table id="tbltop" width="730" runat="server">
				<TBODY>
					<tr height="24">
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang2304" runat="server">Function Copy Mini Dialog</asp:Label></td>
					</tr>
					<tr id="tbleqrec" runat="server">
						<td colSpan="3" align="center">
							<table width="760">
								<TBODY>
									<tr>
										<td style="width: 80px"></td>
										<td width="220"></td>
										<td width="460"></td>
									</tr>
									<tr>
										<td class="bluelabel"><asp:Label id="lang2305" runat="server">Search</asp:Label></td>
										<td><asp:textbox id="txtsrch" runat="server" Width="170px" CssClass="plainlabel"></asp:textbox><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
										<td align="right"><IMG id="ibtnret" onclick="handlereturn();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
												width="69" height="19" runat="server"></td>
									</tr>
									<tr>
										<td colSpan="3">
											<div style="WIDTH: 780px; HEIGHT: 300px; OVERFLOW: auto" onscroll="SetsDivPosition();"
												id="spdiv"><asp:repeater id="rptrfuncrev" runat="server">
													<HeaderTemplate>
														<table width="760">
															<tr>
																<td class="btmmenu plainlabel" width="230px"><asp:Label id="lang2306" runat="server">Function</asp:Label></td>
																<td class="btmmenu plainlabel" width="15px"><img src="../images/appbuttons/minibuttons/gridpic.gif"></td>
																<td class="btmmenu plainlabel" width="220px"><asp:Label id="lang2307" runat="server">Special Identifier</asp:Label></td>
																<td class="btmmenu plainlabel" width="180px"><asp:Label id="lang2308" runat="server">Equipment</asp:Label></td>
																<td class="btmmenu plainlabel" width="70px"><asp:Label id="lang2309" runat="server">Components</asp:Label></td>
																<td class="btmmenu plainlabel" width="70px"><asp:Label id="lang2310" runat="server">Total Tasks</asp:Label></td>
															</tr>
													</HeaderTemplate>
													<ItemTemplate>
														<tr class="tbg">
															<td class="plainlabel">
																<asp:RadioButton AutoPostBack="True"  CssClass="plainlabel" OnCheckedChanged="GetFu" id="rbfu" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>' runat="server" />
															</td>
															<td><img src="../images/appbuttons/minibuttons/gridpic.gif" id="imgpic" runat="server"></td>
															<td class="plainlabel"><asp:Label ID="lblfuncspl" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
																</asp:Label></td>
															<td class="plainlabel"><asp:Label ID="lbleqnum" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' Runat = server>
																</asp:Label></td>
															<td class="plainlabel"><asp:Label ID="lblcompcnt" Text='<%# DataBinder.Eval(Container.DataItem,"compcnt")%>' Runat = server>
																</asp:Label></td>
															<td class="plainlabel"><asp:Label ID="lbltaskcnt" Text='<%# DataBinder.Eval(Container.DataItem,"taskcnt")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfuncdesc" Text='<%# DataBinder.Eval(Container.DataItem,"func_desc")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfuncrevid" Text='<%# DataBinder.Eval(Container.DataItem,"func_id")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcby" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblmby" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcdate" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblmdate" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfueqid" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcph" Text='<%# DataBinder.Eval(Container.DataItem,"fcph")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcm" Text='<%# DataBinder.Eval(Container.DataItem,"fcm")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleph" Text='<%# DataBinder.Eval(Container.DataItem,"feph")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblem" Text='<%# DataBinder.Eval(Container.DataItem,"fem")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleqspl" Text='<%# DataBinder.Eval(Container.DataItem,"eqspl")%>' Runat = server>
																</asp:Label></td>
														</tr>
													</ItemTemplate>
													<FooterTemplate>
							</table>
							</FooterTemplate> </asp:repeater></DIV></td>
					</tr>
					<tr>
						<td colSpan="3" align="center">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="details">
						<td id="tdcurrpage" class="bluelabel" colSpan="2" runat="server"></td>
						<td align="right"><asp:imagebutton id="btnprev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnnext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton></td>
					</tr>
				</TBODY>
			</table>
			</TD></TR></TBODY></TABLE></TD></TR></TABLE>
			<table id="tblfuncrev" class="details" cellSpacing="0" width="760" runat="server">
				<tr>
					<td style="width: 100px"></td>
					<td width="160"></td>
					<td width="154"></td>
					<td style="width: 60px"></td>
				</tr>
				<tr height="24">
					<td class="thdrsing label" colSpan="4"><asp:Label id="lang2311" runat="server">Function Details</asp:Label></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang2312" runat="server">New Function</asp:Label></td>
					<td class="label"><asp:textbox id="txtnewfunc" runat="server" style="width: 150px" CssClass="plainlabel" MaxLength="100"></asp:textbox></td>
					<td class="label"><asp:checkboxlist id="cbloptions" runat="server" Width="155px" CssClass="label" RepeatLayout="Flow"
							Height="8px" RepeatDirection="Horizontal">
							<asp:ListItem Value="c" Selected="True">Components</asp:ListItem>
							<asp:ListItem Value="t" Selected="True">Tasks</asp:ListItem>
						</asp:checkboxlist><asp:checkbox id="cbtpm" runat="server" Checked="False" Text="TPM Tasks"></asp:checkbox></td>
					<td align="right"><asp:imagebutton id="ibtncopy" runat="server" ImageUrl="../images/appbuttons/bgbuttons/copy.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td class="label"><input id="cbrat" type="checkbox" CHECKED name="cbrat" runat="server"><asp:Label id="lang2313" runat="server">Copy Rationale</asp:Label><input id="rbro" value="rbro" type="radio" name="rbroa" runat="server"><asp:Label id="lang2314" runat="server">Revised Only</asp:Label><input id="rbai" CHECKED value="rbai" type="radio" name="rbroa" runat="server"><asp:Label id="lang2315" runat="server">As Is</asp:Label></td>
				</tr>
			</table>
			<table id="tblrevdetails" class="details" cellSpacing="0" width="760" runat="server">
				<TBODY>
					<TR>
						<td style="width: 120px"></td>
						<td width="260"></td>
						<td style="width: 120px"></td>
						<td width="260"></td>
					</TR>
					<tr>
						<td class="label"><asp:Label id="lang2316" runat="server">Function Name</asp:Label></td>
						<td id="tdfunc" class="plainlabel" colSpan="3" runat="server"></td>
					</tr>
					<TR>
						<td class="label"><asp:Label id="lang2317" runat="server">Special Identifier</asp:Label></td>
						<td id="tdspl" class="plainlabel" colSpan="3" runat="server"></td>
					</TR>
					<tr>
						<td class="label">&nbsp;</td>
						<td colSpan="3" align="right"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2318" runat="server">Component Count</asp:Label></td>
						<td id="tdcompcnt" class="plainlabel" colSpan="3" runat="server"></td>
					</tr>
					<TR>
						<td class="label"><asp:Label id="lang2319" runat="server">Task Count</asp:Label></td>
						<td id="tdtaskcnt" class="plainlabel" colSpan="3" runat="server"></td>
					</TR>
					<tr>
						<td class="label">&nbsp;</td>
						<td colSpan="3" align="right"></td>
					</tr>
					<tr>
						<td id="Td7" class="label" runat="server"><asp:Label id="lang2320" runat="server">Created By</asp:Label></td>
						<td id="tdcby" class="plainlabel" runat="server"></td>
						<td class="label"><asp:Label id="lang2321" runat="server">Create Date</asp:Label></td>
						<td id="tdcdate" class="plainlabel" runat="server"></td>
					</tr>
					<tr>
						<td id="Td9" class="label" runat="server"><asp:Label id="lang2322" runat="server">Phone</asp:Label></td>
						<td id="tdcphone" class="plainlabel" runat="server"></td>
						<td class="label"><asp:Label id="lang2323" runat="server">Email Address</asp:Label></td>
						<td id="tdcmail" class="plainlabel" runat="server"></td>
					</tr>
					<tr>
						<td colSpan="4">&nbsp;</td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2324" runat="server">Belongs To:</asp:Label></td>
						<td id="tdeqnum" class="labellt" colSpan="3" runat="server"></td>
					</tr>
					<tr>
						<td class="label">&nbsp;</td>
						<td id="tdeqdesc" class="labellt" colSpan="3" runat="server"></td>
					</tr>
					<tr>
						<td class="label">&nbsp;</td>
						<td id="tdeqspl" class="labellt" colSpan="3" runat="server"></td>
					</tr>
				</TBODY>
			</table>
			<table id="tblneweq" class="details" width="760" runat="server">
				<TBODY>
					<TR>
						<td style="width: 120px"></td>
						<td width="260"></td>
						<td style="width: 120px"></td>
						<td width="260"></td>
					</TR>
					<tr height="24">
						<td class="thdrsing label" colSpan="4"><asp:Label id="lang2325" runat="server">New Function Record Details</asp:Label></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2326" runat="server">Function Name</asp:Label></td>
						<td id="tdfuncnew" class="plainlabel" colSpan="3" runat="server"></td>
					</tr>
					<TR>
						<td class="label"><asp:Label id="lang2327" runat="server">Special Identifier</asp:Label></td>
						<td colSpan="3"><asp:textbox id="txtfuncspl" runat="server" Width="210px" CssClass="plainlabel" MaxLength="100"></asp:textbox></td>
					</TR>
					<tr>
						<td class="label">&nbsp;</td>
						<td colSpan="3" align="right"></td>
					</tr>
					<tr height="20">
						<td id="Td8" class="label" runat="server"><asp:Label id="lang2328" runat="server">Created By</asp:Label></td>
						<td id="tdcrby" class="plainlabel" runat="server"></td>
						<td class="label"><asp:Label id="lang2329" runat="server">Create Date</asp:Label></td>
						<td id="tdcrdate" class="plainlabel" runat="server"></td>
					</tr>
					<tr height="20">
						<td id="Td2" class="label" runat="server"><asp:Label id="lang2330" runat="server">Phone</asp:Label></td>
						<td id="Td3" class="plainlabel" runat="server"></td>
						<td class="label"><asp:Label id="lang2331" runat="server">Email Address</asp:Label></td>
						<td id="Td4" class="plainlabel" runat="server"></td>
					</tr>
					<tr height="20">
						<td id="Td5" class="label" runat="server"><asp:Label id="lang2332" runat="server">Modified By</asp:Label></td>
						<td id="tdmby" class="plainlabel" runat="server"></td>
						<td class="label"><asp:Label id="lang2333" runat="server">Modified Date</asp:Label></td>
						<td id="tdmdate" class="plainlabel" runat="server"></td>
					</tr>
					<tr height="20">
						<td id="Td6" class="label" runat="server"><asp:Label id="lang2334" runat="server">Phone</asp:Label></td>
						<td id="tdcrphone" class="plainlabel" runat="server">&nbsp;</td>
						<td class="label"><asp:Label id="lang2335" runat="server">Email Address</asp:Label></td>
						<td id="tdcremail" class="plainlabel" runat="server">&nbsp;</td>
					</tr>
					<tr>
						<td colSpan="4" align="right"><asp:imagebutton id="ibtnsaveneweq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
					</tr>
				</TBODY>
			</table>
			<input id="lblnewfuncid" type="hidden" runat="server"> <input id="lbleqid" type="hidden" runat="server">
			<input id="lblcid" type="hidden" runat="server"><input id="txtpg" type="hidden" runat="server">
			<input id="lblfromeq" type="hidden" runat="server"><input id="lbloldfuncid" type="hidden" runat="server">
			<input id="lblsid" type="hidden" runat="server"><input id="lbllog" type="hidden" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="Hidden1" type="hidden" name="Hidden1" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"> <input id="lblsubmit" type="hidden" runat="server">
			<input id="spdivy" type="hidden" runat="server"><input id="lblro" type="hidden" runat="server">
			<input id="lbldb" type="hidden" runat="server"><input id="lbloloc" type="hidden" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
