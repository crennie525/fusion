

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class eqcopy2
    Inherits System.Web.UI.Page
	Protected WithEvents ovid252 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2201 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2200 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2199 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2198 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2197 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2196 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2195 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2194 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2193 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2192 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2191 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2190 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2189 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2188 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2187 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2186 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2185 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2184 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2183 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2182 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2181 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2180 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2179 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2178 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2177 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2176 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2175 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2174 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2173 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2172 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2171 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim cid, srch, sid, did, clid As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Dim coid, ro As String
    Dim copy As New Utilities
    Dim dept, cell, psid As String
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtneweqnum As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtncopy As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ibtncancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tbleqrev As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents trcopy As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents treqdets As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents treq As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents ifeqdets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iffudets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfu As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents iftpmtaskdets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifpmtaskdets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcellchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tblbot As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ifneweq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifnewfu As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifcodets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifnewpmtaskdets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifnewtpmtaskdets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifnewcodets As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents cbfunc As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbcomp As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbpm As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbtpm As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lbloserv As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldrbi As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbuseappr As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents lblchkchngd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurreqsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcurrsort As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbcds As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblcopyret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbro As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbai As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblt1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblt3 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblce As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents cbecryn As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents cbrat As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents rbto As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblstrmsg As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim Login As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dddb As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater
    Protected WithEvents tbleq As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        lblt2.Value = Now
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)
        Dim app As New AppUtils
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
            Exit Sub
        End Try

        Dim url As String = app.Switch
        If url <> "ok" Then
            Response.Redirect(url)
        End If
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("EQCopy.aspx")
        Catch ex As Exception

        End Try
        If Not IsPostBack Then

            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            If ro = "1" Then
                'ibtncopy.ImageUrl = "../images/appbuttons/bgbuttons/copydis.gif"
                'ibtncopy.Enabled = False
                ibtncopy.Attributes.Add("src", "../images/appbuttons/bgbuttons/copydis.gif")
                ibtncopy.Attributes.Add("onclick", "")
            Else
                ibtncopy.Attributes.Add("onclick", "checkeq();")
            End If
            Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim db As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            Dim mdba As String = System.Configuration.ConfigurationManager.AppSettings("MDBA")
            '*** Multi Add ***
            copy.Open()
            If mdba = "yes" Then
                'PopDBS(ap)
                dddb.SelectedValue = db
                dddb.Enabled = True
            Else
                dddb.Enabled = False
            End If
            lbldb.Value = db
            cid = HttpContext.Current.Session("comp").ToString
            psid = HttpContext.Current.Session("psite").ToString
            sid = HttpContext.Current.Session("dfltps").ToString
            lblpsid.Value = "0"
            lblsid.Value = sid
            lblcid.Value = cid
            txtpg.Value = "1"
            lblcurreqsort.Value = "eqnum asc"
            tdcurrsort.InnerHtml = "Equipment# Ascending"
            PopDepts(sid)
            PopEq(PageNumber)
            copy.Dispose()
        Else
            If Request.Form("lblsubmit") = "docopy" Then
                lblsubmit.Value = ""
                copy.Open()
                CheckCopy()
                copy.Dispose()
            ElseIf Request.Form("lblsubmit") = "getchngd" Then
                lblsubmit.Value = ""
                copy.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopEq(PageNumber)
                copy.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                copy.Open()
                GetNext()
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                copy.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopEq(PageNumber)
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                copy.Open()
                GetPrev()
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                copy.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopEq(PageNumber)
                copy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "msrch" Then
                lblret.Value = ""
                getsrch()
            End If
        End If
        'ibtncopy.Attributes.Add("onclick", "checksave();")

    End Sub
    Public Sub SortEq(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sort As String = lblcurreqsort.Value
        If sort = "eqnum asc" Then
            sort = "eqnum desc"
            tdcurrsort.InnerHtml = "Equipment# Descending"
        ElseIf sort = "eqnum desc" Then
            sort = "eqnum asc"
            tdcurrsort.InnerHtml = "Equipment# Ascending"
        Else
            sort = "eqnum asc"
            tdcurrsort.InnerHtml = "Equipment# Ascending"
        End If
        lblcurreqsort.Value = sort
        copy.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopEq(PageNumber)
        copy.Dispose()
        lblret.Value = ""
    End Sub
    Private Sub cbcds_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbcds.CheckedChanged
        If cbcds.Checked = True Then
            sort = "createdate asc"
            tdcurrsort.InnerHtml = "Date Created Ascending"
            cbcds.Checked = True
        Else
            sort = "eqnum asc"
            tdcurrsort.InnerHtml = "Equipment# Ascending"
            cbcds.Checked = False
        End If

        lblcurreqsort.Value = sort
        copy.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopEq(PageNumber)
        copy.Dispose()
        lblret.Value = ""
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopEq(PageNumber)
        Catch ex As Exception
            copy.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr875" , "eqcopy2.aspx.vb")
 
            copy.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopEq(PageNumber)
        Catch ex As Exception
            copy.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr876" , "eqcopy2.aspx.vb")
 
            copy.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopDBS(ByVal ap As String)
        Dim aconn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("aConn"))
        Dim admin As String = HttpContext.Current.Session("pmadmin").ToString
        sql = "exec usp_pfizer_dbs '" & admin & "'"
        aconn.Open()
        Dim cmd As New SqlCommand
        cmd.CommandText = sql
        cmd.Connection = aconn
        cmd.CommandTimeout = 120
        dr = cmd.ExecuteReader()
        dddb.DataSource = dr
        dddb.DataTextField = "language"
        dddb.DataValueField = "dbname"
        dddb.DataBind()
        dr.Close()
        aconn.Close()
        aconn.Dispose()
        dddb.Items.Insert(0, "Select Database")
    End Sub
    Private Sub PopEq(ByVal PageNumber As Integer, Optional ByVal db As String = "")
        Dim aapr As String
        If cbuseappr.Checked = True Then
            aapr = "1"
            lblchkchngd.Value = "1"
        Else
            aapr = "0"
            lblchkchngd.Value = "0"
        End If
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        'cid = "0"
        cid = lblcid.Value
        psid = lblpsid.Value
        Dim intPgCnt As Integer
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        If Len(srch) = 0 Then
            srch = ""
            sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[Equipment] where siteid <> '" & psid & "' and apprstg1 = '" & aapr & "'"
            intPgCnt = copy.Scalar(sql)
        Else
            Dim csrch As String
            csrch = "%" & srch & "%"
            sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[Equipment] " _
            + "where (eqnum like '" & csrch & "' or eqdesc like '" & csrch & "' or " _
            + "spl like '" & csrch & "') and siteid <> '" & psid & "'"
            Dim cmd1 As New SqlCommand("select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[Equipment] " _
            + "where (eqnum like '%' + @srch + '%' or eqdesc like '%' + @srch + '%' or " _
            + "spl like '%' + @srch + '%') and siteid <> @psid and apprstg1 = @aapr")
            Dim param7 = New SqlParameter("@srch", SqlDbType.VarChar)
            param7.Value = srch
            cmd1.Parameters.Add(param7)
            Dim param8 = New SqlParameter("@srvr", SqlDbType.VarChar)
            param8.Value = PageNumber
            cmd1.Parameters.Add(param8)
            Dim param9 = New SqlParameter("@mdb", SqlDbType.VarChar)
            param9.Value = mdb
            cmd1.Parameters.Add(param9)
            Dim param10 = New SqlParameter("@psid", SqlDbType.Int)
            param10.Value = psid
            cmd1.Parameters.Add(param10)
            'Dim param11 = New SqlParameter("@cid", SqlDbType.Int)
            'param11.Value = cid
            'cmd1.Parameters.Add(param11)
            Dim param12 = New SqlParameter("@aapr", SqlDbType.Int)
            param12.Value = aapr
            cmd1.Parameters.Add(param12)
            intPgCnt = copy.ScalarHack(cmd1)
            'intPgCnt = copy.Scalar(sql)
        End If
        PageNumber = txtpg.Value
        intPgNav = copy.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        srch = Replace(srch, "/", " ", , , vbTextCompare)
        srch = Replace(srch, "\", " ", , , vbTextCompare)
        If Len(srch) = 0 Then
            srch = ""
        End If
        Dim sort As String = lblcurreqsort.Value
        If sort = "createdate asc" Then
            cbcds.Checked = True
        End If
        If lblcopyret.Value = "yes" Then
            lblcopyret.Value = "no"
            'document.getElementById("tbltop").className= "view";
            'document.getElementById("tblbot").className = "details";
            tbltop.Attributes.Add("class", "view")
            tblbot.Attributes.Add("class", "details")
            tdcurrsort.InnerHtml = "Date Created Descending"
            cbcds.Checked = True
            ifeqdets.Attributes.Add("src", "pmlibeqdets.aspx?start=no")
            iffudets.Attributes.Add("src", "pmlibfudets.aspx?start=no")
        End If
        cid = lblcid.Value
        '*** Multi Add ***
        'sql = "usp_getAllEqPgMDB_New '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '%%', '" & psid & "', '" & mdb & "'"
        Dim cmd0 As New SqlCommand("exec usp_getAllEqPgMDB_Sort @cid, @PageNumber, @PageSize, @srch, @psid, @mdb, @appr, @srvr, @sort")
        Dim param1 = New SqlParameter("@cid", SqlDbType.Int)
        param1.Value = cid
        cmd0.Parameters.Add(param1)
        Dim param2 = New SqlParameter("@PageNumber", SqlDbType.Int)
        param2.Value = PageNumber
        cmd0.Parameters.Add(param2)
        Dim param3 = New SqlParameter("@PageSize", SqlDbType.Int)
        param3.Value = PageSize
        cmd0.Parameters.Add(param3)
        Dim param4 = New SqlParameter("@srch", SqlDbType.VarChar)
        param4.Value = srch
        cmd0.Parameters.Add(param4)
        Dim param5 = New SqlParameter("@psid", SqlDbType.Int)
        param5.Value = psid
        cmd0.Parameters.Add(param5)
        Dim param6 = New SqlParameter("@mdb", SqlDbType.VarChar)
        param6.Value = mdb
        cmd0.Parameters.Add(param6)
        Dim param07 = New SqlParameter("@appr", SqlDbType.VarChar)
        param07.Value = aapr
        cmd0.Parameters.Add(param07)
        Dim param08 = New SqlParameter("@srvr", SqlDbType.VarChar)
        param08.Value = srvr
        cmd0.Parameters.Add(param08)
        Dim param09 = New SqlParameter("@sort", SqlDbType.VarChar)
        param09.Value = sort
        cmd0.Parameters.Add(param09)
        dr = copy.GetRdrDataHack(cmd0)

        rptreq.DataSource = dr
        rptreq.DataBind()
        dr.Close()

        If lblchkchngd.Value = "1" Then
            cbuseappr.Checked = True
        Else
            cbuseappr.Checked = False
        End If
    End Sub
    Public Sub GetEq1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mdb As String = lbldb.Value
        Dim rb As RadioButton = New RadioButton
        Dim eqid, geq, sido As String
        rb = sender
        Dim rbi As String = rb.ClientID
        For Each i As RepeaterItem In rptreq.Items
            If i.ItemType = ListItemType.Item Then
                rb = CType(i.FindControl("rbeq"), RadioButton)
                eqid = CType(i.FindControl("lbleqid"), Label).Text
                sido = CType(i.FindControl("lblsidi"), Label).Text

            ElseIf i.ItemType = ListItemType.AlternatingItem Then
                rb = CType(i.FindControl("rbeqalt"), RadioButton)
                eqid = CType(i.FindControl("lbleqidalt"), Label).Text
                sido = CType(i.FindControl("lblsida"), Label).Text

            End If
            If rbi = rb.ClientID Then
                geq = eqid
            Else
                rb.Checked = False
            End If
        Next
        lbleq.Value = geq
        lbloldsid.Value = sido
        ifeqdets.Attributes.Add("src", "pmlibeqdets.aspx?start=yes&db=" & mdb & "&eqid=" & geq)
        iffudets.Attributes.Add("src", "pmlibfudets.aspx?start=yes&db=" & mdb & "&eqid=" & geq)

    End Sub
    Private Sub PopDepts_NMBD(ByVal sid As String)
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        sql = "select Dept_ID, Dept_Line from [" & srvr & "].[" & mdb & "].[dbo].[dept] where siteid = '" & sid & "' order by Dept_Line asc"
        dr = copy.GetRdrData(sql)
        dddepts.DataSource = dr
        dddepts.DataTextField = "dept_line"
        dddepts.DataValueField = "dept_id"
        dddepts.DataBind()
        dr.Close()
        dddepts.Items.Insert(0, "Select Department")
    End Sub
    Private Sub PopDepts(ByVal site As String)
        Dim filt, dt, val As String
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        filt = " where siteid = '" & site & "'"
        dt = "Dept"
        val = "dept_id , dept_line "
        dr = copy.GetList(dt, val, filt)
        dddepts.DataSource = dr
        dddepts.DataTextField = "dept_line"
        dddepts.DataValueField = "dept_id"
        dddepts.DataBind()
        dr.Close()
        dddepts.Items.Insert(0, "Select Department")
    End Sub

    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        If dddepts.SelectedIndex <> 0 Then
            dept = dddepts.SelectedValue
            lbldid.Value = dept
            Dim mdb As String = lbldb.Value
            Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
            Dim cellchk As Integer
            copy.Open()
            sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[cells] where dept_id = '" & dept & "'"
            cellchk = copy.Scalar(sql)
            If cellchk > 0 Then
                sql = "select cellid, cell_name from [" & srvr & "].[" & mdb & "].[dbo].[cells] where dept_id = '" & dept & "' order by cell_name asc"
                dr = copy.GetRdrData(sql)
                ddcells.DataSource = dr
                ddcells.DataTextField = "cell_name"
                ddcells.DataValueField = "cellid"
                ddcells.DataBind()
                ddcells.Items.Insert(0, "Select Station/Cell")
                dr.Close()

            Else
                lblcellchk.Value = "ok"
            End If

            copy.Dispose()
        End If

    End Sub

    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged
        If ddcells.SelectedIndex <> 0 Then
            lblclid.Value = ddcells.SelectedValue
            lblcellchk.Value = "ok"
        End If
    End Sub
    Private Sub CheckCopy()
        Dim neweq As String = txtneweqnum.Text
        neweq = Replace(neweq, "'", Chr(180), , , vbTextCompare)
        neweq = Replace(neweq, "--", "-", , , vbTextCompare)
        neweq = Replace(neweq, ";", ":", , , vbTextCompare)
        If Len(neweq) > 0 Then
            If Len(neweq) > 50 Then
                Dim strMessage1 As String =  tmod.getmsg("cdstr877" , "eqcopy2.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
            cid = lblcid.Value
            Dim eqcnt As Integer
            Dim mdb As String = lbldb.Value
            Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
            Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            '*** Multi Add ***

            Dim cmd0 As New SqlCommand("select count(*) from [" & srvr & "].[" & orig & "].[dbo].[equipment] where eqnum = @neweq")
            Dim param1 = New SqlParameter("@srvr", SqlDbType.VarChar)
            param1.Value = srvr
            cmd0.Parameters.Add(param1)
            Dim param2 = New SqlParameter("@mdb", SqlDbType.VarChar)
            param2.Value = mdb
            cmd0.Parameters.Add(param2)
            'Dim param3 = New SqlParameter("@cid", SqlDbType.Int)
            'param3.Value = cid
            'cmd0.Parameters.Add(param3)
            Dim param4 = New SqlParameter("@neweq", SqlDbType.VarChar)
            param4.Value = neweq
            cmd0.Parameters.Add(param4)
            'copy.Open()
            eqcnt = copy.ScalarHack(cmd0)
            'eqcnt = copy.Scalar(sql)

            If eqcnt = 0 Then
                txtneweqnum.Enabled = False
                'lblcopy.Value = "yes"
                'ibtncopy.Visible = False
                'ibtncancel.Visible = True
                'Dim flg As String = lbldflt.Value
                'If flg = "0" Then
                'tblchoosedept.Attributes.Add("class", "view")
                'tblrevdetails.Attributes.Add("class", "details")
                'Else
                'Try

                'Try
                DoCopy()
                'Catch ex As Exception
                'Dim strMessage2 As String = ex.Message
                'strMessage2 = Replace(strMessage2, "'", "", , , vbTextCompare) '"Problem Saving Record\nPlease review selections and try again."
                'strMessage2 = Replace(strMessage2, """", "", , , vbTextCompare)
                'lblstrmsg.Value = strMessage2
                ''Utilities.CreateMessageAlert2(Me, strMessage2, "strKey1")
                'End Try

                'Catch ex As Exception
                'Dim strMessage2 As String =  tmod.getmsg("cdstr878" , "eqcopy2.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                'End Try
                ''copy.Dispose()
            Else
                'copy.Dispose()
                Dim strMessage3 As String = tmod.getmsg("cdstr879", "eqcopy2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage3, "strKey1")
            End If
        Else
            'copy.Dispose()
            Dim strMessage4 As String =  tmod.getmsg("cdstr880" , "eqcopy2.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage4, "strKey1")
        End If
    End Sub
    Private Sub DoCopy()
        Dim multiserver As String
        Try
            multiserver = System.Configuration.ConfigurationManager.AppSettings("multiserver")
        Catch ex As Exception
            multiserver = "no"
        End Try

        Dim li As ListItem
        Dim listr As String
        Dim pm, tpm As String
        Dim tflg As Integer = 0
        If cbfunc.Checked = True Then
            listr = "f"
            If cbcomp.Checked = True Then
                listr += "c"
                If cbpm.Checked = True Then
                    listr += "t"
                    tflg = 1
                    pm = "Y"
                Else
                    pm = "N"
                End If
                If cbtpm.Checked = True Then
                    If tflg = 0 Then
                        listr += "t"
                    End If
                    tpm = "Y"
                Else
                    tpm = "N"
                End If
            End If
        End If
        Dim eqid As String = lbleq.Value
        Dim neweq As String = txtneweqnum.Text
        neweq = Replace(neweq, "'", Chr(180), , , vbTextCompare)
        'neweq = Replace(neweq, " ", " ", , , vbTextCompare)
        neweq = Replace(neweq, ";", " ", , , vbTextCompare)
        neweq = Replace(neweq, "/", " ", , , vbTextCompare)
        neweq = Replace(neweq, "\", " ", , , vbTextCompare)
        neweq = Replace(neweq, "&", " ", , , vbTextCompare)
        neweq = Replace(neweq, "#", " ", , , vbTextCompare)
        neweq = Replace(neweq, "$", " ", , , vbTextCompare)
        neweq = Replace(neweq, "%", " ", , , vbTextCompare)
        neweq = Replace(neweq, "!", " ", , , vbTextCompare)
        neweq = Replace(neweq, "~", " ", , , vbTextCompare)
        neweq = Replace(neweq, "^", " ", , , vbTextCompare)
        neweq = Replace(neweq, "*", " ", , , vbTextCompare)
        'neweq = Replace(neweq, ".", " ", , , vbTextCompare)
        cid = lblcid.Value
        Dim site, dept, cell As String
        site = lblsid.Value
        dept = lbldid.Value
        cell = lblclid.Value
        If cell = "" Then
            cell = "0"
        End If
        sid = lblsid.Value
        Dim osid As String = lbloldsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        If cell = "none" Then
            cell = "0"
        End If
        Dim newid As Integer
        Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        Dim mdb As String = lbldb.Value

        Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim lid As String = "0"
        Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim ecryn As String
        If cbecryn.Checked = True Then
            ecryn = "Y"
        Else
            ecryn = "N"
        End If
        Dim roa As String
        If rbro.Checked = True Then
            roa = "R"
        ElseIf rbai.Checked = True Then
            roa = "A"
        End If
        Dim rat As String
        If cbrat.Checked = True Then
            rat = "Y"
        Else
            rat = "N"
        End If
        Dim cmd0 As New SqlCommand("exec usp_copyEqMDB2 @cid, @site, @dept, @cell, @eqid, @neweq, @ustr, @mdb, @orig, @srvr, @ecryn")
        Dim param1 = New SqlParameter("@cid", SqlDbType.Int)
        param1.Value = cid
        cmd0.Parameters.Add(param1)
        Dim param2 = New SqlParameter("@site", SqlDbType.Int)
        param2.Value = site
        cmd0.Parameters.Add(param2)
        Dim param3 = New SqlParameter("@dept", SqlDbType.Int)
        param3.Value = dept
        cmd0.Parameters.Add(param3)
        Dim param4 = New SqlParameter("@cell", SqlDbType.Int)
        param4.Value = cell
        cmd0.Parameters.Add(param4)
        Dim param5 = New SqlParameter("@eqid", SqlDbType.Int)
        param5.Value = eqid
        cmd0.Parameters.Add(param5)
        Dim param6 = New SqlParameter("@neweq", SqlDbType.VarChar)
        param6.Value = neweq
        cmd0.Parameters.Add(param6)
        Dim param7 = New SqlParameter("@ustr", SqlDbType.VarChar)
        param7.Value = ustr
        cmd0.Parameters.Add(param7)
        Dim param8 = New SqlParameter("@mdb", SqlDbType.VarChar)
        param8.Value = mdb
        cmd0.Parameters.Add(param8)
        Dim param9 = New SqlParameter("@orig", SqlDbType.VarChar)
        param9.Value = orig
        cmd0.Parameters.Add(param9)
        Dim param10 = New SqlParameter("@srvr", SqlDbType.VarChar)
        param10.Value = srvr
        cmd0.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@ecryn", SqlDbType.VarChar)
        param11.Value = ecryn
        cmd0.Parameters.Add(param11)
        newid = copy.ScalarHack(cmd0)

        sql = "usp_copyEqPicsMDB2 '" & eqid & "', '" & newid & "', '" & mdb & "', '" & orig & "','" & srvr & "'"

        Dim oloc As String = lbloloc.Value
        Dim cloc As String = ap
        Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
        If mdb <> orig Then
            Try
                Dim ds As New DataSet
                Try
                    ds = copy.GetDSData(sql)
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")

                    nt = "a-eqImg" & newid & "i"
                    ntn = "atn-eqImg" & newid & "i"
                    ntm = "atm-eqImg" & newid & "i"

                    ont = "a-eqImg" & eqid & "i"
                    ontn = "atn-eqImg" & eqid & "i"
                    ontm = "atm-eqImg" & eqid & "i"

                    ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & cloc & t
                    td = Replace(td, "\", "/")

                    tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                    tns = Replace(tns, "\", "/")
                    tnd = Server.MapPath("\") & cloc & tn
                    tnd = Replace(tnd, "\", "/")

                    tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & cloc & tm
                    tmd = Replace(tmd, "\", "/")

                    If multiserver = "yes" Then
                        ts = Replace(ts, "wwwlai1", "wwwlai2")
                        tns = Replace(ts, "wwwlai1", "wwwlai2")
                        tms = Replace(ts, "wwwlai1", "wwwlai2")
                    End If


                    System.IO.File.Copy(ts, td, True)
                    System.IO.File.Copy(tns, tnd, True)
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception

            End Try
        Else
            Try
                Dim ds As New DataSet
                Try
                    ds = copy.GetDSData(sql)
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try

                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")

                    nt = "a-eqImg" & newid & "i"
                    ntn = "atn-eqImg" & newid & "i"
                    ntm = "atm-eqImg" & newid & "i"

                    ont = "a-eqImg" & eqid & "i"
                    ontn = "atn-eqImg" & eqid & "i"
                    ontm = "atm-eqImg" & eqid & "i"


                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & ap & t
                    td = Replace(td, "\", "/")

                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                    tns = Replace(tns, "\", "/")
                    tnd = Server.MapPath("\") & ap & tn
                    tnd = Replace(tnd, "\", "/")

                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & ap & tm
                    tmd = Replace(tmd, "\", "/")

                    System.IO.File.Copy(ts, td, True)
                    System.IO.File.Copy(tns, tnd, True)
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception

            End Try
        End If

        Dim flg As String
        lblnewid.Value = newid
        If listr = "fct" Then
            flg = "1"
            sql = "usp_copyAllMDB2 '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & ustr & "'"
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    Try
                        ds = copy.GetDSData(sql)
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")

                        If multiserver = "yes" Then
                            ts = Replace(ts, "wwwlai1", "wwwlai2")
                            tns = Replace(ts, "wwwlai1", "wwwlai2")
                            tms = Replace(ts, "wwwlai1", "wwwlai2")
                        End If

                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
                'insert pm task image file copy here for mdb *********************************************

            Else
                Try
                    Dim ds As New DataSet
                    Try
                        ds = copy.GetDSData(sql)
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")

                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)

                    Next
                Catch ex As Exception

                End Try
                'insert pm task image file copy here for local
                Try
                    sql = "select pic_id, parfuncid, funcid, oldfuncid, pm_image, pm_image_med, pm_image_thumb from pmimages where funcid in " _
                    + "(select func_id from functions where eqid = '" & newid & "')"
                    Dim dsp As New DataSet
                    Try
                        dsp = copy.GetDSData(sql)
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                    End Try

                    Dim fid1, pfid1 As String
                    oloc = lbloloc.Value
                    Dim i1 As Integer
                    Dim f1 As Integer = 0
                    Dim c1 As Integer = 0
                    Dim ot, otn, otm As String
                    Dim pict, pictn, pictm As String
                    Dim opict, opictn, opictm As String
                    Dim x1 As Integer = dsp.Tables(0).Rows.Count
                    Dim ofid1 As String
                    Dim newt, newtn, newtm

                    For i1 = 0 To (x1 - 1)
                        pid = dsp.Tables(0).Rows(i1)("pic_id").ToString
                        fid1 = dsp.Tables(0).Rows(i1)("funcid").ToString
                        ofid1 = dsp.Tables(0).Rows(i1)("oldfuncid").ToString
                        pfid1 = dsp.Tables(0).Rows(i1)("parfuncid").ToString
                        t = dsp.Tables(0).Rows(i1)("pm_image").ToString
                        tn = dsp.Tables(0).Rows(i1)("pm_image_thumb").ToString
                        tm = dsp.Tables(0).Rows(i1)("pm_image_med").ToString
                        Dim intFileNameLength As Integer
                        intFileNameLength = InStr(1, StrReverse(t), "/")
                        t = Mid(t, (Len(t) - intFileNameLength) + 2)
                        intFileNameLength = InStr(1, StrReverse(tn), "/")
                        tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                        intFileNameLength = InStr(1, StrReverse(tm), "/")
                        tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                        t = appstr & "/tpmimages/" & t
                        tn = appstr & "/tpmimages/" & tn
                        tm = appstr & "/tpmimages/" & tm

                        t = Replace(t, "http://localhost/", "")
                        tn = Replace(tn, "http://localhost/", "")
                        tm = Replace(tm, "http://localhost/", "")

                        t = Replace(t, "http://", "")
                        tn = Replace(tn, "http://", "")
                        tm = Replace(tm, "http://", "")

                        ts = Replace(t, "g" & fid1 & "-", "g" & ofid1 & "-")
                        tns = Replace(tn, "g" & fid1 & "-", "g" & ofid1 & "-")
                        tms = Replace(tm, "g" & fid1 & "-", "g" & ofid1 & "-")

                        td = Replace(t, "g" & fid1 & "-", "g" & fid1 & "-")
                        tnd = Replace(tn, "g" & fid1 & "-", "g" & fid1 & "-")
                        tmd = Replace(tm, "g" & fid1 & "-", "g" & fid1 & "-")


                        If oloc <> "" Then
                            ts = Replace(ts, ap, oloc)
                            tns = Replace(tns, ap, oloc)
                            tms = Replace(tms, ap, oloc)

                            td = Replace(td, ap, oloc)
                            tnd = Replace(tnd, ap, oloc)
                            tmd = Replace(tmd, ap, oloc)
                        End If


                        ts = Server.MapPath("\") & ts
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & td
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & tns
                        tns = Replace(tns, "\", "/")
                        tnd = Server.MapPath("\") & tnd
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & tms
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & tmd
                        tmd = Replace(tmd, "\", "/")

                        'System.IO.File.Copy(source, dest)
                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)


                    Next

                Catch ex As Exception

                End Try

            End If
            'rbro, rbai

            '2MDB includes switch for Revised or As is
            sql = "usp_copyEqTasks2MDB '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & newid & "', '" & ustr & "', '" & mdb & "', '" & orig & "','" & pm & "','" & tpm & "', '" & ap & "','" & oloc & "','" & srvr & "','" & roa & "','" & rat & "'"
            copy.Update(sql)
            'need to put pm task images here !!!Did this with function call above

            '***Added to speed up result time
            If sid <> osid Then
                sql = "usp_updateSkillsNew '" & sid & "','" & newid & "'"
                copy.Update(sql)
                sql = "usp_updateSiteFM '" & cid & "', '" & sid & "'"
                copy.Update(sql)
            End If
            sql = "iusp_updateCompFailId '" & newid & "'"
            copy.Update(sql)

            '***

            If tpm = "Y" Then
                sql = "select parfuncid, funcid, tpm_image, tpm_image_med, tpm_image_thumb from tpmimages where funcid in " _
                + "(select func_id from functions where eqid = '" & newid & "')"
                If mdb <> orig Then
                    Try
                        Dim ds As New DataSet
                        Try
                            ds = copy.GetDSData(sql)
                        Catch ex As Exception
                            Dim strMessage2 As String = ex.Message
                            strMessage2 = copy.ModString1(strMessage2)
                            If lblstrmsg.Value = "" Then
                                lblstrmsg.Value = strMessage2
                            Else
                                lblstrmsg.Value += "\n\n" & strMessage2
                            End If
                        End Try

                        Dim fid, pfid As String
                        oloc = lbloloc.Value
                        Dim i As Integer
                        Dim f As Integer = 0
                        Dim c As Integer = 0
                        Dim ot, otn, otm As String
                        Dim pict, pictn, pictm As String
                        Dim opict, opictn, opictm As String
                        Dim x As Integer = ds.Tables(0).Rows.Count
                        For i = 0 To (x - 1)
                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                            pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                            t = ds.Tables(0).Rows(i)("tpm_image").ToString
                            tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                            tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                            Dim intFileNameLength As Integer
                            intFileNameLength = InStr(1, StrReverse(t), "/")
                            t = Mid(t, (Len(t) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tn), "/")
                            tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tm), "/")
                            tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                            t = appstr & "/tpmimages/" & t
                            tn = appstr & "/tpmimages/" & tn
                            tm = appstr & "/tpmimages/" & tm

                            t = Replace(t, "http://localhost/", "")
                            tn = Replace(tn, "http://localhost/", "")
                            tm = Replace(tm, "http://localhost/", "")

                            t = Replace(t, "http://", "")
                            tn = Replace(tn, "http://", "")
                            tm = Replace(tm, "http://", "")

                            pict = "a-newsImg" + fid
                            pictn = "atn-newsImg" + fid
                            pictm = "atm-newsImg" + fid

                            opict = "a-newsImg" + pfid
                            opictn = "atn-newsImg" + pfid
                            opictm = "atm-newsImg" + pfid

                            ts = Replace(t, pict, opict)
                            tns = Replace(tn, pictn, opictn)
                            tms = Replace(tm, pictm, opictm)

                            If oloc <> "" Then
                                ts = Replace(t, ap, oloc)
                                tns = Replace(tn, ap, oloc)
                                tms = Replace(tm, ap, oloc)
                            End If

                            ts = Server.MapPath("\") & ts
                            ts = Replace(ts, "\", "/")
                            td = Server.MapPath("\") & t
                            td = Replace(td, "\", "/")


                            tns = Server.MapPath("\") & tns
                            tns = Replace(tns, "\", "/")
                            tnd = Server.MapPath("\") & tn
                            tnd = Replace(tnd, "\", "/")


                            tms = Server.MapPath("\") & tms
                            tms = Replace(tms, "\", "/")
                            tmd = Server.MapPath("\") & tm
                            tmd = Replace(tmd, "\", "/")

                            System.IO.File.Copy(ts, td, True)
                            System.IO.File.Copy(tns, tnd, True)
                            System.IO.File.Copy(tms, tmd, True)


                        Next
                    Catch ex As Exception

                    End Try
                Else
                    Try
                        Dim ds As New DataSet
                        Try
                            ds = copy.GetDSData(sql)
                        Catch ex As Exception
                            Dim strMessage2 As String = ex.Message
                            strMessage2 = copy.ModString1(strMessage2)
                            If lblstrmsg.Value = "" Then
                                lblstrmsg.Value = strMessage2
                            Else
                                lblstrmsg.Value += "\n\n" & strMessage2
                            End If
                        End Try

                        Dim fid, pfid As String
                        Dim i As Integer
                        Dim f As Integer = 0
                        Dim c As Integer = 0
                        Dim ot, otn, otm As String
                        Dim pict, pictn, pictm As String
                        Dim opict, opictn, opictm As String
                        Dim x As Integer = ds.Tables(0).Rows.Count
                        For i = 0 To (x - 1)
                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                            pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                            t = ds.Tables(0).Rows(i)("tpm_image").ToString
                            tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                            tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                            Dim intFileNameLength As Integer
                            intFileNameLength = InStr(1, StrReverse(t), "/")
                            t = Mid(t, (Len(t) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tn), "/")
                            tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tm), "/")
                            tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                            t = appstr & "/tpmimages/" & t
                            tn = appstr & "/tpmimages/" & tn
                            tm = appstr & "/tpmimages/" & tm

                            t = Replace(t, "http://localhost/", "")
                            tn = Replace(tn, "http://localhost/", "")
                            tm = Replace(tm, "http://localhost/", "")

                            t = Replace(t, "http://", "")
                            tn = Replace(tn, "http://", "")
                            tm = Replace(tm, "http://", "")

                            pict = "a-newsImg" + fid
                            pictn = "atn-newsImg" + fid
                            pictm = "atm-newsImg" + fid

                            opict = "a-newsImg" + pfid
                            opictn = "atn-newsImg" + pfid
                            opictm = "atm-newsImg" + pfid

                            ts = Replace(t, pict, opict)
                            tns = Replace(tn, pictn, opictn)
                            tms = Replace(tm, pictm, opictm)

                            ts = Server.MapPath("\") & ts
                            ts = Replace(ts, "\", "/")
                            td = Server.MapPath("\") & t
                            td = Replace(td, "\", "/")
                            System.IO.File.Copy(ts, td, True)

                            tns = Server.MapPath("\") & tns
                            tns = Replace(tns, "\", "/")
                            tnd = Server.MapPath("\") & tn
                            tnd = Replace(tnd, "\", "/")
                            System.IO.File.Copy(tns, tnd, True)

                            tms = Server.MapPath("\") & tms
                            tms = Replace(tms, "\", "/")
                            tmd = Server.MapPath("\") & tm
                            tmd = Replace(tmd, "\", "/")
                            System.IO.File.Copy(tms, tmd, True)
                        Next
                    Catch ex As Exception

                    End Try
                End If

            End If
        ElseIf listr = "fc" Then
            flg = "1"
            sql = "usp_copyAllMDB '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & ustr & "'"
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    Try
                        ds = copy.GetDSData(sql)
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
            Else
                Try
                    Dim ds As New DataSet
                    Try
                        ds = copy.GetDSData(sql)
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
            End If
        ElseIf listr = "f" Then
            flg = "0"
            'sql = "usp_copyAll '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "'"
            sql = "usp_copyAllMDB '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & ustr & "'"
            'copy.Update(sql)
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    Try
                        ds = copy.GetDSData(sql)
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
            Else
                Try
                    Dim ds As New DataSet
                    Try
                        ds = copy.GetDSData(sql)
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception

                End Try
            End If
        Else
        End If

        sql = "usp_copyProcsMDB '" & eqid & "', '" & newid & "', '" & mdb & "', '" & orig & "','" & srvr & "'"

        If mdb <> orig Then
            Try
                Dim ds As New DataSet
                ds = copy.GetDSData(sql)
                Dim f, fs, fd As String
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    f = ds.Tables(0).Rows(i)("filename").ToString
                    fs = Server.MapPath("\") & oloc & "/eqimages/" & f
                    fd = Server.MapPath("\") & cloc & "/eqimages/" & f
                    System.IO.File.Copy(fs, fd, True)
                Next
            Catch ex As Exception

            End Try
        Else
            Try
                Dim ds As New DataSet
                ds = copy.GetDSData(sql)
                Dim f, fs, fd As String
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    f = ds.Tables(0).Rows(i)("filename").ToString
                    fs = Server.MapPath("\") & ap & "/eqimages/" & f
                    fd = Server.MapPath("\") & ap & "/eqimages/" & f
                    System.IO.File.Copy(fs, fd, True)
                Next
            Catch ex As Exception

            End Try
        End If
        
        

        tbltop.Attributes.Add("class", "details")
        tblbot.Attributes.Add("class", "view")
        Dim chk, dchk As String
        If clid = "" Or clid = "0" Then
            chk = "no"
        Else
            chk = "yes"
        End If
        txtneweqnum.Enabled = True
        txtneweqnum.Text = ""
        ifneweq.Attributes.Add("src", "pmlibneweqdets.aspx?start=yes&dchk=yes&db=" & mdb & "&eqid=" & newid & "&chk=" & chk)
        ifnewfu.Attributes.Add("src", "pmlibnewfudets.aspx?start=yes&db=" & mdb & "&eqid=" & newid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&oloc=" & oloc)

    End Sub

    Private Sub dddb_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddb.SelectedIndexChanged
        Dim db As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim adb As String = System.Configuration.ConfigurationManager.AppSettings("custAdminDB")
        Dim tbl As String = System.Configuration.ConfigurationManager.AppSettings("custAdminTBL")
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim oloc As String
        lbldb.Value = dddb.SelectedValue
        txtpg.Value = "1"
        PageNumber = txtpg.Value
        copy.Open()
        If lbldb.Value <> db Then
            'sql = "select appname, appstr from [" & srvr & "].[" & adb & "].[dbo].[" & tbl & "] where dbname = '" & lbldb.Value & "'"
            'dr = copy.GetRdrData(sql)
            'While dr.Read
            'oloc = copy.strScalar(sql)
            lbloloc.Value = "fusion_mod_35" 'dr.Item("appname").ToString
            lblappstr.Value = "" 'dr.Item("appstr").ToString
            'End While

        End If
        PopEq(PageNumber)
        copy.Dispose()
    End Sub
    Private Sub getsrch()
        copy.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopEq(PageNumber)
        copy.Dispose()
    End Sub

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub


	



    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2171 As Label
                lang2171 = CType(e.Item.FindControl("lang2171"), Label)
                lang2171.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2171")
            Catch ex As Exception
            End Try
            Try
                Dim lang2172 As Label
                lang2172 = CType(e.Item.FindControl("lang2172"), Label)
                lang2172.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2172")
            Catch ex As Exception
            End Try
            Try
                Dim lang2173 As Label
                lang2173 = CType(e.Item.FindControl("lang2173"), Label)
                lang2173.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2173")
            Catch ex As Exception
            End Try
            Try
                Dim lang2174 As Label
                lang2174 = CType(e.Item.FindControl("lang2174"), Label)
                lang2174.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2174")
            Catch ex As Exception
            End Try
            Try
                Dim lang2175 As Label
                lang2175 = CType(e.Item.FindControl("lang2175"), Label)
                lang2175.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2175")
            Catch ex As Exception
            End Try
            Try
                Dim lang2176 As Label
                lang2176 = CType(e.Item.FindControl("lang2176"), Label)
                lang2176.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2176")
            Catch ex As Exception
            End Try
            Try
                Dim lang2177 As Label
                lang2177 = CType(e.Item.FindControl("lang2177"), Label)
                lang2177.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2177")
            Catch ex As Exception
            End Try
            Try
                Dim lang2178 As Label
                lang2178 = CType(e.Item.FindControl("lang2178"), Label)
                lang2178.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2178")
            Catch ex As Exception
            End Try
            Try
                Dim lang2179 As Label
                lang2179 = CType(e.Item.FindControl("lang2179"), Label)
                lang2179.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2179")
            Catch ex As Exception
            End Try
            Try
                Dim lang2180 As Label
                lang2180 = CType(e.Item.FindControl("lang2180"), Label)
                lang2180.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2180")
            Catch ex As Exception
            End Try
            Try
                Dim lang2181 As Label
                lang2181 = CType(e.Item.FindControl("lang2181"), Label)
                lang2181.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2181")
            Catch ex As Exception
            End Try
            Try
                Dim lang2182 As Label
                lang2182 = CType(e.Item.FindControl("lang2182"), Label)
                lang2182.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2182")
            Catch ex As Exception
            End Try
            Try
                Dim lang2183 As Label
                lang2183 = CType(e.Item.FindControl("lang2183"), Label)
                lang2183.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2183")
            Catch ex As Exception
            End Try
            Try
                Dim lang2184 As Label
                lang2184 = CType(e.Item.FindControl("lang2184"), Label)
                lang2184.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2184")
            Catch ex As Exception
            End Try
            Try
                Dim lang2185 As Label
                lang2185 = CType(e.Item.FindControl("lang2185"), Label)
                lang2185.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2185")
            Catch ex As Exception
            End Try
            Try
                Dim lang2186 As Label
                lang2186 = CType(e.Item.FindControl("lang2186"), Label)
                lang2186.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2186")
            Catch ex As Exception
            End Try
            Try
                Dim lang2187 As Label
                lang2187 = CType(e.Item.FindControl("lang2187"), Label)
                lang2187.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2187")
            Catch ex As Exception
            End Try
            Try
                Dim lang2188 As Label
                lang2188 = CType(e.Item.FindControl("lang2188"), Label)
                lang2188.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2188")
            Catch ex As Exception
            End Try
            Try
                Dim lang2189 As Label
                lang2189 = CType(e.Item.FindControl("lang2189"), Label)
                lang2189.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2189")
            Catch ex As Exception
            End Try
            Try
                Dim lang2190 As Label
                lang2190 = CType(e.Item.FindControl("lang2190"), Label)
                lang2190.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2190")
            Catch ex As Exception
            End Try
            Try
                Dim lang2191 As Label
                lang2191 = CType(e.Item.FindControl("lang2191"), Label)
                lang2191.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2191")
            Catch ex As Exception
            End Try
            Try
                Dim lang2192 As Label
                lang2192 = CType(e.Item.FindControl("lang2192"), Label)
                lang2192.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2192")
            Catch ex As Exception
            End Try
            Try
                Dim lang2193 As Label
                lang2193 = CType(e.Item.FindControl("lang2193"), Label)
                lang2193.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2193")
            Catch ex As Exception
            End Try
            Try
                Dim lang2194 As Label
                lang2194 = CType(e.Item.FindControl("lang2194"), Label)
                lang2194.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2194")
            Catch ex As Exception
            End Try
            Try
                Dim lang2195 As Label
                lang2195 = CType(e.Item.FindControl("lang2195"), Label)
                lang2195.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2195")
            Catch ex As Exception
            End Try
            Try
                Dim lang2196 As Label
                lang2196 = CType(e.Item.FindControl("lang2196"), Label)
                lang2196.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2196")
            Catch ex As Exception
            End Try
            Try
                Dim lang2197 As Label
                lang2197 = CType(e.Item.FindControl("lang2197"), Label)
                lang2197.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2197")
            Catch ex As Exception
            End Try
            Try
                Dim lang2198 As Label
                lang2198 = CType(e.Item.FindControl("lang2198"), Label)
                lang2198.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2198")
            Catch ex As Exception
            End Try
            Try
                Dim lang2199 As Label
                lang2199 = CType(e.Item.FindControl("lang2199"), Label)
                lang2199.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2199")
            Catch ex As Exception
            End Try
            Try
                Dim lang2200 As Label
                lang2200 = CType(e.Item.FindControl("lang2200"), Label)
                lang2200.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2200")
            Catch ex As Exception
            End Try
            Try
                Dim lang2201 As Label
                lang2201 = CType(e.Item.FindControl("lang2201"), Label)
                lang2201.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2201")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2171.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2171")
        Catch ex As Exception
        End Try
        Try
            lang2172.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2172")
        Catch ex As Exception
        End Try
        Try
            lang2173.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2173")
        Catch ex As Exception
        End Try
        Try
            lang2174.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2174")
        Catch ex As Exception
        End Try
        Try
            lang2175.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2175")
        Catch ex As Exception
        End Try
        Try
            lang2176.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2176")
        Catch ex As Exception
        End Try
        Try
            lang2177.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2177")
        Catch ex As Exception
        End Try
        Try
            lang2178.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2178")
        Catch ex As Exception
        End Try
        Try
            lang2179.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2179")
        Catch ex As Exception
        End Try
        Try
            lang2180.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2180")
        Catch ex As Exception
        End Try
        Try
            lang2181.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2181")
        Catch ex As Exception
        End Try
        Try
            lang2182.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2182")
        Catch ex As Exception
        End Try
        Try
            lang2183.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2183")
        Catch ex As Exception
        End Try
        Try
            lang2184.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2184")
        Catch ex As Exception
        End Try
        Try
            lang2185.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2185")
        Catch ex As Exception
        End Try
        Try
            lang2186.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2186")
        Catch ex As Exception
        End Try
        Try
            lang2187.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2187")
        Catch ex As Exception
        End Try
        Try
            lang2188.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2188")
        Catch ex As Exception
        End Try
        Try
            lang2189.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2189")
        Catch ex As Exception
        End Try
        Try
            lang2190.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2190")
        Catch ex As Exception
        End Try
        Try
            lang2191.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2191")
        Catch ex As Exception
        End Try
        Try
            lang2192.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2192")
        Catch ex As Exception
        End Try
        Try
            lang2193.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2193")
        Catch ex As Exception
        End Try
        Try
            lang2194.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2194")
        Catch ex As Exception
        End Try
        Try
            lang2195.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2195")
        Catch ex As Exception
        End Try
        Try
            lang2196.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2196")
        Catch ex As Exception
        End Try
        Try
            lang2197.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2197")
        Catch ex As Exception
        End Try
        Try
            lang2198.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2198")
        Catch ex As Exception
        End Try
        Try
            lang2199.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2199")
        Catch ex As Exception
        End Try
        Try
            lang2200.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2200")
        Catch ex As Exception
        End Try
        Try
            lang2201.Text = axlabs.GetASPXPage("eqcopy2.aspx", "lang2201")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtncancel.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtncancel.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtncancel.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtncancel.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtncancel.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtncopy.Attributes.Add("src", "../images2/eng/bgbuttons/copy.gif")
            ElseIf lang = "fre" Then
                ibtncopy.Attributes.Add("src", "../images2/fre/bgbuttons/copy.gif")
            ElseIf lang = "ger" Then
                ibtncopy.Attributes.Add("src", "../images2/ger/bgbuttons/copy.gif")
            ElseIf lang = "ita" Then
                ibtncopy.Attributes.Add("src", "../images2/ita/bgbuttons/copy.gif")
            ElseIf lang = "spa" Then
                ibtncopy.Attributes.Add("src", "../images2/spa/bgbuttons/copy.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            ovid252.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("eqcopy2.aspx", "ovid252") & "')")
            ovid252.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
