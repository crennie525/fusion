﻿Imports System.Data.SqlClient
Public Class meterroutes
    Inherits System.Web.UI.Page
    Dim appset As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim eqid, meterid, meter, munit, enterby, sid, routeid, route, desc As String
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 12
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '  mstopid, mrouteid, mroute, eqid, meterid, routeorder, lastreading, lastdate, lastby, location, locref

        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString '"12" '
            lblsid.Value = sid
            enterby = Request.QueryString("uid").ToString '"PM Admin" '
            lblenterby.Value = enterby
            routeid = Request.QueryString("routeid").ToString '"1" '
            If routeid = "" Or routeid = "0" Then
                routeid = "0"
                tradd.Attributes.Add("class", "details")
            Else
                tradd.Attributes.Add("class", "view")
            End If
            lblrouteid.Value = routeid
            'route = "Meter Route 1" 'Request.QueryString("route").ToString
            'lblroute.Value = route
            'txtnewroute.Text = route
            txtpg.Value = "1"
            appset.Open()
            getdets()
            getroute(PageNumber)
            appset.Dispose()
        Else
            If Request.Form("lblret") = "addseq" Then
                lblret.Value = ""
                appset.Open()
                addseq()
                PageNumber = txtpg.Value
                getroute(PageNumber)
                appset.Dispose()
                tradd.Attributes.Add("class", "view")
            ElseIf Request.Form("lblret") = "addroute" Then
                lblret.Value = ""
                appset.Open()
                txtpg.Value = 1
                PageNumber = 1
                addroute()
                getroute(PageNumber)
                appset.Dispose()
                tradd.Attributes.Add("class", "view")
            ElseIf Request.Form("lblret") = "saveroute" Then
                lblret.Value = ""
                appset.Open()
                PageNumber = txtpg.Value
                saveroute()
                getroute(PageNumber)
                appset.Dispose()
                tradd.Attributes.Add("class", "view")
            ElseIf Request.Form("lblret") = "delroute" Then
                lblret.Value = ""
                appset.Open()
                txtpg.Value = 1
                PageNumber = 1
                delroute()
                getroute(PageNumber)
                appset.Dispose()
                tradd.Attributes.Add("class", "details")

            ElseIf Request.Form("lblret") = "next" Then
                appset.Open()
                GetNext()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                appset.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber

                getroute(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                appset.Open()
                GetPrev()
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                getroute(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "getroute" Then
                appset.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                getdets()
                getroute(PageNumber)
                appset.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            getroute(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "AppSetAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            getroute(PageNumber)
        Catch ex As Exception
            appset.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "AppSetAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub delroute()
        routeid = lblrouteid.Value
        sql = "delete from meterroutes where mrouteid = '" & routeid & "'; delete from meterstops where mrouteid = '" & routeid & "'"
        appset.Update(sql)
        lblrouteid.Value = ""
        lblroute.Value = ""
        lbldesc.Value = ""
        txtnewroute.Text = ""
        txtnewdesc.Text = ""
        clearall()
    End Sub
    Private Sub saveroute()
        routeid = lblrouteid.Value
        route = txtnewroute.Text
        route = appset.ModString2(route)
        If Len(route) > 50 Then
            Dim strMessage As String = "Route Name Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim oroute As String = lblroute.Value
        desc = txtnewdesc.Text
        desc = appset.ModString2(desc)
        If Len(desc) > 100 Then
            Dim strMessage As String = "Description Limited to 100 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim currdt As Date = appset.CNOW
        enterby = lblenterby.Value
        Dim mcnt As Integer = 0
        If oroute <> route Then
            sql = "select count(*) from meterroutes where mroute = '" & route & "'"
            mcnt = appset.Scalar(sql)
        End If
        If mcnt = 0 Then
            If desc <> "" Then
                sql = "update meterroutes set mroute = '" & route & "', mdesc = '" & desc & "', " _
                + "modifiedby = '" & enterby & "', modifieddate = '" & currdt & "' where mrouteid = '" & routeid & "'"
            Else
                sql = "update meterroutes set mroute = '" & route & "', " _
                + "modifiedby = '" & enterby & "', modifieddate = '" & currdt & "' where mrouteid = '" & routeid & "'"
            End If
            appset.Update(sql)
        Else
            Dim strMessage As String = "A Route with this Name already exists"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub addroute()
        route = txtnewroute.Text
        route = appset.ModString2(route)
        enterby = lblenterby.Value
        If Len(route) > 50 Then
            Dim strMessage As String = "Route Name Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        desc = txtnewdesc.Text
        desc = appset.ModString2(desc)
        If Len(desc) > 100 Then
            Dim strMessage As String = "Description Limited to 100 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        sid = lblsid.Value
        Dim currdt As Date = appset.CNOW
        Dim mcnt As Integer = 0
        sql = "select count(*) from meterroutes where mroute = '" & route & "'"
        mcnt = appset.Scalar(sql)
        If mcnt = 0 Then
            If desc <> "" Then
                sql = "insert into meterroutes (mroute, mdesc, siteid, createdby, createdate) " _
                    + "values ('" & route & "','" & desc & "','" & sid & "','" & enterby & "','" & currdt & "') select @@identity"
            Else
                sql = "insert into meterroutes (mroute, siteid, createdby, createdate) " _
                    + "values ('" & route & "','" & sid & "','" & enterby & "','" & currdt & "') select @@identity"
            End If

            routeid = appset.Scalar(sql)
            lblrouteid.Value = routeid
            lblroute.Value = route
            lbldesc.Value = desc
        Else
            Dim strMessage As String = "A Route with this Name already exists"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub addseq()
        routeid = lblrouteid.Value
        route = lblroute.Value
        sql = "declare @rtcnt int; "
        sql += "set @rtcnt = (select count(*) from pmroute_stops where rid = '" & routeid & "'); "
        sql += "set @rtcnt = @rtcnt + 1; "
        sql += "insert into meterstops (mrouteid, mroute, routeorder) values " _
            + "('" & routeid & "','" & route & "',@rtcnt) "
        appset.Update(sql)

        enterby = lblenterby.Value
        Dim currdt As Date = appset.CNOW
        routeid = lblrouteid.Value
        sql = "update meterroutes set modifiedby = '" & enterby & "', modifieddate = '" & currdt & "' where mrouteid = '" & routeid & "'"
        appset.Update(sql)

    End Sub

    Private Sub getdets()
        routeid = lblrouteid.Value
        sql = "select mroute, mdesc from meterroutes where mrouteid = '" & routeid & "'"
        dr = appset.GetRdrData(sql)
        While dr.Read
            route = dr.Item("mroute").ToString
            desc = dr.Item("mdesc").ToString
        End While
        dr.Close()
        lblroute.Value = route
        lbldesc.Value = desc
        txtnewroute.Text = route
        txtnewdesc.Text = desc
    End Sub
    Private Sub getroute(ByVal PageNumber As Integer)
        Dim intPgNav, intPgCnt As Integer
        routeid = lblrouteid.Value
        sql = "select count(*) from meterstops where mrouteid = '" & routeid & "'"
        intPgCnt = appset.Scalar(sql)
        If intPgCnt = 0 Then
            If routeid <> "0" Then
                'Dim strMessage As String = tmod.getmsg("cdstr588", "wolist.aspx.vb")
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'Exit Sub
            End If

        End If
        PageSize = "50"
        intPgNav = appset.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = "routeorder"

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If

        Filter = "mrouteid = ''" & routeid & "''"
        'sql = "select * from meterhist where meterid = '" & meterid & "'"
        sql = "usp_getroutestops_mtr '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        dr = appset.GetRdrData(sql)
        dglist.DataSource = dr
        dglist.DataBind()
    End Sub

    Private Sub dglist_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.CancelCommand
        dglist.EditItemIndex = -1
        appset.Open()
        PageNumber = txtpg.Value
        getroute(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dglist_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.DeleteCommand
        dglist.EditItemIndex = -1
        Dim rsid As String
        Try
            rsid = CType(e.Item.FindControl("lblmstopidi"), Label).Text
        Catch ex As Exception
            rsid = CType(e.Item.FindControl("lblmstopid"), Label).Text
        End Try
        sql = "usp_delroutestop_mtr '" & rsid & "'"
        appset.Open()
        appset.Update(sql)

        enterby = lblenterby.Value
        Dim currdt As Date = appset.CNOW
        routeid = lblrouteid.Value
        sql = "update meterroutes set modifiedby = '" & enterby & "', modifieddate = '" & currdt & "' where mrouteid = '" & routeid & "'"
        appset.Update(sql)

        dglist.EditItemIndex = -1
        PageNumber = txtpg.Value
        getroute(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dglist_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.EditCommand
        lbloldorder.Value = CType(e.Item.FindControl("lblseq"), Label).Text
        appset.Open()
        dglist.EditItemIndex = e.Item.ItemIndex
        PageNumber = txtpg.Value
        getroute(PageNumber)
        appset.Dispose()
    End Sub

    Private Sub dglist_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dglist.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                                 e.Item.ItemType <> ListItemType.Footer Then
            Dim delButton As ImageButton = CType(e.Item.FindControl("ibDel"), ImageButton)
            delButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete this Route Stop?')"
        End If
        If e.Item.ItemType = ListItemType.EditItem Then
            Dim eq As String = CType(e.Item.FindControl("lbleqs"), Label).ClientID
            Dim eqimg As HtmlImage = CType(e.Item.FindControl("lookupeq"), HtmlImage)

            Dim mm As String = CType(e.Item.FindControl("lblmtrid"), Label).ClientID
            Dim mu As String = CType(e.Item.FindControl("lbluni"), Label).ClientID
            Dim mimg As HtmlImage = CType(e.Item.FindControl("lookupmtr"), HtmlImage)
            Dim eqid As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString

            eqimg.Attributes("onclick") = "getloc('" & eq & "','" & mm & "','" & mu & "');"
            mimg.Attributes("onclick") = "getmeter('" & mm & "','" & mu & "','" & eqid & "');"


        End If
    End Sub
    Private Sub clearall()
        lbleq.Value = ""
        lbldid.Value = ""
        lbldept.Value = ""
        lblclid.Value = ""
        lblcell.Value = ""
        lbleqid.Value = ""
        lbllid.Value = ""
        lblloc.Value = ""
        lblmeterid.Value = ""
        lblmeter.Value = ""
        lblunit.Value = ""
        lblrettyp.Value = ""
    End Sub
    Private Sub dglist_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.UpdateCommand
        Dim eq, did, dept, clid, cell, eqid, lid, loc, meterid, meter, unit, rettyp As String
        Dim locstr As String
        eq = lbleq.Value
        did = lbldid.Value
        dept = lbldept.Value
        clid = lblclid.Value
        cell = lblcell.Value
        eqid = lbleqid.Value
        lid = lbllid.Value
        loc = lblloc.Value
        meterid = lblmeterid.Value
        meter = lblmeter.Value
        unit = lblunit.Value
        rettyp = lblrettyp.Value
        
        If eqid = "" Then
            eqid = CType(e.Item.FindControl("lbleqide"), Label).Text
        End If
        If meterid = "" Then
            meterid = CType(e.Item.FindControl("lblmeteride"), Label).Text
        End If
        ' eqid, meterid, routeorder, lastreading, lastdate, lastby, location, locref
        If rettyp = "dept" Then
            If cell = "" Then
                locstr = "Department: " & dept
            Else
                locstr = "Department: " & dept & "  Cell: " & cell
            End If
        Else
            If loc <> "" Then
                locstr = "Location: " & loc
            End If
        End If
        If locstr = "" Then
            locstr = CType(e.Item.FindControl("lbllocs"), Label).Text
        End If
        Dim seq, locref, stopid, oseq As String
        seq = CType(e.Item.FindControl("txtseq"), TextBox).Text
        oseq = lbloldorder.Value
        Dim seqi As Long
        Try
            seqi = System.Convert.ToInt32(seq)
        Catch ex As Exception
            Dim strMessage As String = "Sequence Must Be a Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        locref = CType(e.Item.FindControl("txtlocref"), TextBox).Text
        locref = appset.ModString2(locref)
        stopid = CType(e.Item.FindControl("lblmstopid"), Label).Text
        If locref = "" Then
            'locref = System.DBNull.Value
        End If
        If locref = "" Then
            sql = "update meterstops set eqid = '" & eqid & "', meterid = '" & meterid & "', " _
            + "location = '" & locstr & "' " _
            + " where mstopid = '" & stopid & "'"
        Else
            sql = "update meterstops set eqid = '" & eqid & "', meterid = '" & meterid & "', " _
            + "location = '" & locstr & "', locref = '" & locref & "' " _
            + " where mstopid = '" & stopid & "'"
        End If
        appset.Open()
        appset.Update(sql)

        enterby = lblenterby.Value
        Dim currdt As Date = appset.CNOW
        routeid = lblrouteid.Value
        sql = "update meterroutes set modifiedby = '" & enterby & "', modifieddate = '" & currdt & "' where mrouteid = '" & routeid & "'"
        appset.Update(sql)

        If oseq <> seq Then
            sql = "usp_reroutestops_mtr '" & stopid & "', '" & seq & "', '" & oseq & "'"
            appset.Update(sql)
        End If
        dglist.EditItemIndex = -1
        PageNumber = txtpg.Value
        getroute(PageNumber)
        appset.Dispose()
        clearall()
    End Sub
End Class