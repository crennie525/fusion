

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmlibnewpmtaskdets
    Inherits System.Web.UI.Page
	

	Protected WithEvents lang2502 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2501 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2500 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2499 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2498 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2497 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2496 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2495 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2494 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2493 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2492 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2491 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim eqid, db, start, fuid, Login As String
    Protected WithEvents rptrtaskrev As System.Web.UI.WebControls.Repeater
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents remtask As System.Web.UI.WebControls.ImageButton
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                fuid = Request.QueryString("fuid").ToString
                db = Request.QueryString("db").ToString
                lblfuid.Value = fuid
                lbldb.Value = db
                copy.Open()
                PopTaskRev(fuid)
                copy.Dispose()
            Else
                tdmsg.InnerHtml = "Waiting for data..."
                trmsg.Attributes.Add("class", "view")
            End If
        Else
            If Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                fuid = lblfuid.Value
                copy.Open()
                PopTaskRev(fuid)
                copy.Dispose()
            End If
        End If
    End Sub
    Private Sub PopTaskRev(ByVal funcid As String)
        Dim db As String = lbldb.Value
        Dim lang As String = lblfslang.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        db = mdbarr(2).ToString
        Dim tcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & db & "].[dbo].[pmtasks] where funcid = '" & funcid & "'"
        tcnt = copy.Scalar(sql)
        If tcnt = 0 Then
            tdmsg.InnerHtml = "No Records Found"
            trmsg.Attributes.Add("class", "view")
        Else
            trmsg.Attributes.Add("class", "details")
        End If
        sql = "usp_getTaskReview '" & funcid & "', '" & db & "','" & srvr & "', '" & lang & "'"
        dr = copy.GetRdrData(sql)
        rptrtaskrev.DataSource = dr
        rptrtaskrev.DataBind()
        dr.Close()
    End Sub


    Private Sub remtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles remtask.Click
        Dim cb As CheckBox
        Dim fi As String = lblfuid.Value
        Dim tsknum, subnum As String
        copy.Open()
        For Each i As RepeaterItem In rptrtaskrev.Items
            cb = CType(i.FindControl("cb4"), CheckBox)
            If cb.Checked Then
                tsknum = CType(i.FindControl("lbltasknum"), Label).Text
                subnum = CType(i.FindControl("lblsubtasknum"), Label).Text
                If subnum = "0" Then
                    Dim pmtskid As String = CType(i.FindControl("lbltaskid"), Label).Text
                    'sql = "sp_delPMTask '" & fuid & "', '" & tnum & "', '" & st & "'"
                    sql = "usp_delpmtask '" & fi & "', '" & pmtskid & "', '" & tsknum & "'"
                Else
                    sql = "usp_delPMSubTask '" & fi & "', '" & tsknum & "', '" & subnum & "'"
                End If
                copy.Update(sql)
            End If
        Next

        PopTaskRev(fi)
        copy.Dispose()
    End Sub
	



    Private Sub rptrtaskrev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtaskrev.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2491 As Label
                lang2491 = CType(e.Item.FindControl("lang2491"), Label)
                lang2491.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2491")
            Catch ex As Exception
            End Try
            Try
                Dim lang2492 As Label
                lang2492 = CType(e.Item.FindControl("lang2492"), Label)
                lang2492.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2492")
            Catch ex As Exception
            End Try
            Try
                Dim lang2493 As Label
                lang2493 = CType(e.Item.FindControl("lang2493"), Label)
                lang2493.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2493")
            Catch ex As Exception
            End Try
            Try
                Dim lang2494 As Label
                lang2494 = CType(e.Item.FindControl("lang2494"), Label)
                lang2494.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2494")
            Catch ex As Exception
            End Try
            Try
                Dim lang2495 As Label
                lang2495 = CType(e.Item.FindControl("lang2495"), Label)
                lang2495.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2495")
            Catch ex As Exception
            End Try
            Try
                Dim lang2496 As Label
                lang2496 = CType(e.Item.FindControl("lang2496"), Label)
                lang2496.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2496")
            Catch ex As Exception
            End Try
            Try
                Dim lang2497 As Label
                lang2497 = CType(e.Item.FindControl("lang2497"), Label)
                lang2497.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2497")
            Catch ex As Exception
            End Try
            Try
                Dim lang2498 As Label
                lang2498 = CType(e.Item.FindControl("lang2498"), Label)
                lang2498.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2498")
            Catch ex As Exception
            End Try
            Try
                Dim lang2499 As Label
                lang2499 = CType(e.Item.FindControl("lang2499"), Label)
                lang2499.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2499")
            Catch ex As Exception
            End Try
            Try
                Dim lang2500 As Label
                lang2500 = CType(e.Item.FindControl("lang2500"), Label)
                lang2500.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2500")
            Catch ex As Exception
            End Try
            Try
                Dim lang2501 As Label
                lang2501 = CType(e.Item.FindControl("lang2501"), Label)
                lang2501.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2501")
            Catch ex As Exception
            End Try
            Try
                Dim lang2502 As Label
                lang2502 = CType(e.Item.FindControl("lang2502"), Label)
                lang2502.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2502")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2491.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2491")
        Catch ex As Exception
        End Try
        Try
            lang2492.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2492")
        Catch ex As Exception
        End Try
        Try
            lang2493.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2493")
        Catch ex As Exception
        End Try
        Try
            lang2494.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2494")
        Catch ex As Exception
        End Try
        Try
            lang2495.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2495")
        Catch ex As Exception
        End Try
        Try
            lang2496.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2496")
        Catch ex As Exception
        End Try
        Try
            lang2497.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2497")
        Catch ex As Exception
        End Try
        Try
            lang2498.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2498")
        Catch ex As Exception
        End Try
        Try
            lang2499.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2499")
        Catch ex As Exception
        End Try
        Try
            lang2500.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2500")
        Catch ex As Exception
        End Try
        Try
            lang2501.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2501")
        Catch ex As Exception
        End Try
        Try
            lang2502.Text = axlabs.GetASPXPage("pmlibnewpmtaskdets.aspx", "lang2502")
        Catch ex As Exception
        End Try

    End Sub



End Class
