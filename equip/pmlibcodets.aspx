<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibcodets.aspx.vb" Inherits="lucy_r12.pmlibcodets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibcodets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  src="../scripts/imglibnav.js"></script>
		<script  src="../scripts1/pmlibcodetsaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     	</HEAD>
	<body  class="tbg" onload="setref();">
		<form id="form1" method="post" runat="server">
			<table width="980" style="POSITION: absolute; TOP: 0px; LEFT: 0px">
				<TBODY>
					<tr>
						<td class="labelibl"><asp:Label id="lang2436" runat="server">Select a Component to view Component Image and Failure Mode Details</asp:Label></td>
					</tr>
					<tr>
						<td vAlign="top"><asp:repeater id="rptrcomprev" runat="server">
								<HeaderTemplate>
									<table>
										<tr>
											<td class="btmmenu plainlabel" style="width: 150px"><asp:Label id="lang2437" runat="server">Component</asp:Label></td>
											<td class="btmmenu plainlabel" width="200px"><asp:Label id="lang2438" runat="server">Description</asp:Label></td>
											<td class="btmmenu plainlabel" width="200px"><asp:Label id="lang2439" runat="server">Special Identifier</asp:Label></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr id="coselrow" runat="server" bgcolor='<%# HighlightRowCP(DataBinder.Eval(Container.DataItem, "comid"))%>'>
										<td class="plainlabel">
											<asp:LinkButton CommandName="Select" ID="Linkbutton3" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
											</asp:LinkButton></td>
										<td class="plainlabel">
											<asp:Label ID="Label10" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel">
											<asp:Label ID="Label25" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblcomprevid" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblpurl3" Text='<%# DataBinder.Eval(Container.DataItem,"picurl")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblpcnt3" Text='<%# DataBinder.Eval(Container.DataItem,"piccnt")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblparco1" Text='<%# DataBinder.Eval(Container.DataItem,"origparent")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></TD>
			<td vAlign="top" align="center" width="200"><asp:repeater id="rptrfailrev" runat="server">
					<HeaderTemplate>
						<table>
							<tr>
								<td class="btmmenu plainlabel" width="200px"><asp:Label id="lang2440" runat="server">Failure Modes</asp:Label></td>
							</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr class="tbg">
							<td class="plainlabel">
								<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"failuremode")%>' Runat = server ID="Label8">
								</asp:Label></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:repeater></td>
			<td vAlign="top" align="center" width="230">
				<table>
					<tr>
						<td vAlign="top" align="center" colSpan="2" height="40"><IMG id="imgco" onclick="getbig();" height="206" src="../images/appimages/compimg.gif"
								style="width: 216px" runat="server"></td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirstc" onclick="getpfirstc();" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprevc" onclick="getpprevc();" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 140px;" vAlign="middle" align="center"><asp:label id="lblpgc" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inextc" onclick="getpnextc();" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ilastc" onclick="getplasc();" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
									<td style="width: 20px"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			</TR>
			<tr class="details" id="tdfailrev" runat="server">
				<td colSpan="3">&nbsp;</td>
			</tr>
			</TBODY></TABLE></TD></TR> <input type="hidden" id="lblcoid" runat="server"> <input type="hidden" id="lblfuid" runat="server">
			<input type="hidden" id="lbldb" runat="server"> <input id="lblpcntc" type="hidden" runat="server" NAME="lblpcntc">
			<input id="lblcurrpc" type="hidden" runat="server" NAME="lblcurrpc"> <input id="lblimgsc" type="hidden" name="lblimgsc" runat="server">
			<input id="lblimgidc" type="hidden" name="lblimgidc" runat="server"> <input id="lblovimgsc" type="hidden" name="lblovimgsc" runat="server">
			<input id="lblovbimgsc" type="hidden" name="lblovbimgsc" runat="server"> <input id="lblcurrimgc" type="hidden" name="lblcurrimgc" runat="server">
			<input id="lblcurrbimgc" type="hidden" name="lblcurrbimgc" runat="server"> <input id="lblbimgsc" type="hidden" name="lblbimgsc" runat="server"><input id="lbliordersc" type="hidden" name="lbliordersc" runat="server">
			<input id="lbloldorderc" type="hidden" runat="server" NAME="lbloldorderc"> <input id="lblovtimgsc" type="hidden" name="lblovtimgsc" runat="server">
			<input id="lblcurrtimgc" type="hidden" name="lblcurrtimgc" runat="server"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
