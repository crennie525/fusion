﻿Imports System.Data.SqlClient
Imports System.Text
Public Class meterlook
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Dim eqid, typ As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            typ = Request.QueryString("typ").ToString
            lbleqid.Value = eqid
            lbltyp.Value = typ
            tots.Open()
            GetTots(eqid, typ)
            tots.Dispose()
        End If
    End Sub
    Private Sub GetTots(ByVal eqid As String, ByVal typ As String)
        Dim skillid, skill, freqid, freq, rdid, rd, pm, pmstr, sqty As String
        Dim meterid, meter, munit, mfreq, maxdays As String
        If typ <> "orig" Then
            sql = "select distinct t.meterid, m.meter, m.unit, t.maxdays, t.freq, t.meterfreq, t.rdid, t.rd " _
                + "t.skillid, t.skill, t.qty from pmtasks t " _
                + "left join meters m on m.meterid = t.meterid " _
                + "where t.eqid = '" & eqid & "' and t.usemeter = 1"
        Else
            sql = "select distinct t.meterido as 'meterid', m.meter, m.unit, t.maxdayso as 'maxdays', " _
                + "t.origfreq as 'freq', t.meterfreq, t.origrdid as 'rdid', t.origrd as 'rd' " _
                + "t.origskillid as 'skillid', t.origskill as 'skill', t.origqty as 'qty' from pmtasks t " _
                + "left join meters m on m.meterid = t.meterid " _
                + "where t.eqid = '" & eqid & "' and t.usemeter = 1"
        End If
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")

        Dim noflg As Integer = 0

        dr = tots.GetRdrData(sql)
        While dr.Read
            noflg = 1
            skillid = dr.Item("skillid").ToString
            skill = dr.Item("skill").ToString
            freq = dr.Item("freq").ToString
            rdid = dr.Item("rdid").ToString
            rd = dr.Item("rd").ToString
            sqty = dr.Item("qty").ToString

            meterid = dr.Item("meterid").ToString
            meter = dr.Item("meter").ToString
            munit = dr.Item("unit").ToString
            mfreq = dr.Item("meterfreq").ToString
            maxdays = dr.Item("maxdays").ToString

            freqid = ""

            pm = skill & "(" & sqty & ") / " & freq & " days / " & rd & " - " & meter & " / " & munit & " / " & mfreq & " - Max Days = " & maxdays
            pmstr = skillid & "," & skill & "," & freqid & "," & freq & "," & rdid & "," & rd & "," & sqty & ",1," & meterid & "," _
                + meter & "," & munit & "," & mfreq & "," & maxdays
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getpm('" & pmstr & "');"">" & pm & "</a></td></tr>")

        End While
        dr.Close()
        If noflg = 0 Then
            sb.Append("<tr><td class=""redlabel"">No Records Found</td></tr>")
        End If
        sb.Append("</table>")
        totdiv.InnerHtml = sb.ToString

    End Sub
End Class