<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FuncCopyMinitpm.aspx.vb" Inherits="lucy_r12.FuncCopyMinitpm" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>FuncCopyMini</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<script  src="../scripts/gridnav.js"></script>
		<script  src="../scripts1/FuncCopyMinitpmaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="checkfu();" >
		<form id="form1" method="post" runat="server">
			<table id="tbltop" width="730" runat="server">
				<TBODY>
					<tr height="24">
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang2336" runat="server">Function Copy Mini Dialog</asp:Label></td>
					</tr>
					<tr id="tbleqrec" runat="server">
						<td align="center" colSpan="3">
							<table width="760">
								<TBODY>
									<tr>
										<td style="width: 80px"></td>
										<td width="220"></td>
										<td width="460"></td>
									</tr>
									<tr>
										<td class="bluelabel"><asp:Label id="lang2337" runat="server">Search</asp:Label></td>
										<td><asp:textbox id="txtsrch" runat="server" CssClass="plainlabel" Width="170px"></asp:textbox><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
										<td align="right"><IMG id="ibtnret" onclick="handlereturn();" height="19" alt="" src="../images/appbuttons/bgbuttons/return.gif"
												width="69" runat="server"></td>
									</tr>
									<tr>
										<td colSpan="3">
											<div style="OVERFLOW: auto; WIDTH: 780px; HEIGHT: 220px" id="spdiv" onscroll="SetsDivPosition();"><asp:repeater id="rptrfuncrev" runat="server">
													<HeaderTemplate>
														<table width="760">
															<tr>
																<td class="btmmenu plainlabel" width="230px"><asp:Label id="lang2338" runat="server">Function</asp:Label></td>
																<td class="btmmenu plainlabel" width="15px"><img src="../images/appbuttons/minibuttons/gridpic.gif"></td>
																<td class="btmmenu plainlabel" width="205px"><asp:Label id="lang2339" runat="server">Special Identifier</asp:Label></td>
																<td class="btmmenu plainlabel" width="180px"><asp:Label id="lang2340" runat="server">Equipment</asp:Label></td>
																<td class="btmmenu plainlabel" width="70px"><asp:Label id="lang2341" runat="server">Components</asp:Label></td>
																<td class="btmmenu plainlabel" width="70px"><asp:Label id="lang2342" runat="server">Total Tasks</asp:Label></td>
															</tr>
													</HeaderTemplate>
													<ItemTemplate>
														<tr class="tbg">
															<td class="plainlabel">
																<asp:RadioButton AutoPostBack="True"  CssClass="plainlabel" OnCheckedChanged="GetFu" id="rbfu" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>' runat="server" />
															</td>
															<td><img src="../images/appbuttons/minibuttons/gridpic.gif" id="imgpic" runat="server"></td>
															<td class="plainlabel"><asp:Label ID="lblfuncspl" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
																</asp:Label></td>
															<td class="plainlabel"><asp:Label ID="lbleqnum" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' Runat = server>
																</asp:Label></td>
															<td class="plainlabel"><asp:Label ID="lblcompcnt" Text='<%# DataBinder.Eval(Container.DataItem,"compcnt")%>' Runat = server>
																</asp:Label></td>
															<td class="plainlabel"><asp:Label ID="lbltaskcnt" Text='<%# DataBinder.Eval(Container.DataItem,"taskcnt")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfuncdesc" Text='<%# DataBinder.Eval(Container.DataItem,"func_desc")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfuncrevid" Text='<%# DataBinder.Eval(Container.DataItem,"func_id")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcby" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblmby" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcdate" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblmdate" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfueqid" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcph" Text='<%# DataBinder.Eval(Container.DataItem,"fcph")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcm" Text='<%# DataBinder.Eval(Container.DataItem,"fcm")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleph" Text='<%# DataBinder.Eval(Container.DataItem,"feph")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblem" Text='<%# DataBinder.Eval(Container.DataItem,"fem")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleqspl" Text='<%# DataBinder.Eval(Container.DataItem,"eqspl")%>' Runat = server>
																</asp:Label></td>
														</tr>
													</ItemTemplate>
													<FooterTemplate>
							</table>
							</FooterTemplate> </asp:repeater></DIV></td>
					</tr>
					<tr>
						<td align="center" colSpan="3">
							<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="details">
						<td class="bluelabel" id="tdcurrpage" colSpan="2" runat="server"></td>
						<td align="right"><asp:imagebutton id="btnprev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnnext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton></td>
					</tr>
				</TBODY>
			</table>
			</TD></TR></TBODY></TABLE></TD></TR></TABLE>
			<table class="details" id="tblfuncrev" cellSpacing="0" width="760" runat="server">
				<tr>
					<td style="width: 100px"></td>
					<td width="160"></td>
					<td width="154"></td>
					<td style="width: 60px"></td>
				</tr>
				<tr height="24">
					<td class="thdrsing label" colSpan="4"><asp:Label id="lang2343" runat="server">Function Details</asp:Label></td>
				</tr>
				<tr>
					<td class="bluelabel"><asp:Label id="lang2344" runat="server">New Function</asp:Label></td>
					<td class="label"><asp:textbox id="txtnewfunc" runat="server" CssClass="plainlabel" style="width: 150px" MaxLength="100"></asp:textbox></td>
					<td class="label" align="right"><asp:checkboxlist id="cbloptions" runat="server" CssClass="label" Width="155px" RepeatDirection="Horizontal"
							Height="8px" RepeatLayout="Flow">
							<asp:ListItem Value="c" Selected="True">Components</asp:ListItem>
							<asp:ListItem Value="t" Selected="True">Tasks</asp:ListItem>
						</asp:checkboxlist></td>
					<td align="right"><asp:imagebutton id="ibtncopy" runat="server" ImageUrl="../images/appbuttons/bgbuttons/copy.gif"></asp:imagebutton></td>
				</tr>
			</table>
			<table class="details" id="tblrevdetails" cellSpacing="0" width="760" runat="server">
				<TBODY>
					<TR>
						<td style="width: 120px"></td>
						<td width="260"></td>
						<td style="width: 120px"></td>
						<td width="260"></td>
					</TR>
					<tr>
						<td class="label"><asp:Label id="lang2345" runat="server">Function Name</asp:Label></td>
						<td class="plainlabel" id="tdfunc" colSpan="3" runat="server"></td>
					</tr>
					<TR>
						<td class="label"><asp:Label id="lang2346" runat="server">Special Identifier</asp:Label></td>
						<td class="plainlabel" id="tdspl" colSpan="3" runat="server"></td>
					</TR>
					<tr>
						<td class="label">&nbsp;</td>
						<td align="right" colSpan="3"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2347" runat="server">Componet Count</asp:Label></td>
						<td class="plainlabel" id="tdcompcnt" colSpan="3" runat="server"></td>
					</tr>
					<TR>
						<td class="label"><asp:Label id="lang2348" runat="server">Task Count</asp:Label></td>
						<td class="plainlabel" id="tdtaskcnt" colSpan="3" runat="server"></td>
					</TR>
					<tr>
						<td class="label">&nbsp;</td>
						<td align="right" colSpan="3"></td>
					</tr>
					<tr>
						<td class="label" id="Td7" runat="server"><asp:Label id="lang2349" runat="server">Created By</asp:Label></td>
						<td class="plainlabel" id="tdcby" runat="server"></td>
						<td class="label"><asp:Label id="lang2350" runat="server">Create Date</asp:Label></td>
						<td class="plainlabel" id="tdcdate" runat="server"></td>
					</tr>
					<tr>
						<td class="label" id="Td9" runat="server"><asp:Label id="lang2351" runat="server">Phone</asp:Label></td>
						<td class="plainlabel" id="tdcphone" runat="server"></td>
						<td class="label"><asp:Label id="lang2352" runat="server">Email Address</asp:Label></td>
						<td class="plainlabel" id="tdcmail" runat="server"></td>
					</tr>
					<tr>
						<td colSpan="4">&nbsp;</td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2353" runat="server">Belongs To:</asp:Label></td>
						<td class="labellt" id="tdeqnum" colSpan="3" runat="server"></td>
					</tr>
					<tr>
						<td class="label">&nbsp;</td>
						<td class="labellt" id="tdeqdesc" colSpan="3" runat="server"></td>
					</tr>
					<tr>
						<td class="label">&nbsp;</td>
						<td class="labellt" id="tdeqspl" colSpan="3" runat="server"></td>
					</tr>
				</TBODY>
			</table>
			<table class="details" id="tblneweq" width="760" runat="server">
				<TBODY>
					<TR>
						<td style="width: 120px"></td>
						<td width="260"></td>
						<td style="width: 120px"></td>
						<td width="260"></td>
					</TR>
					<tr height="24">
						<td class="thdrsing label" colSpan="4"><asp:Label id="lang2354" runat="server">New Function Record Details</asp:Label></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2355" runat="server">Function Name</asp:Label></td>
						<td class="plainlabel" id="tdfuncnew" colSpan="3" runat="server"></td>
					</tr>
					<TR>
						<td class="label"><asp:Label id="lang2356" runat="server">Special Identifier</asp:Label></td>
						<td colSpan="3"><asp:textbox id="txtfuncspl" runat="server" CssClass="plainlabel" Width="210px" MaxLength="100"></asp:textbox></td>
					</TR>
					<tr>
						<td class="label">&nbsp;</td>
						<td align="right" colSpan="3"></td>
					</tr>
					<tr height="20">
						<td class="label" id="Td8" runat="server"><asp:Label id="lang2357" runat="server">Created By</asp:Label></td>
						<td class="plainlabel" id="tdcrby" runat="server"></td>
						<td class="label"><asp:Label id="lang2358" runat="server">Create Date</asp:Label></td>
						<td class="plainlabel" id="tdcrdate" runat="server"></td>
					</tr>
					<tr height="20">
						<td class="label" id="Td2" runat="server"><asp:Label id="lang2359" runat="server">Phone</asp:Label></td>
						<td class="plainlabel" id="Td3" runat="server"></td>
						<td class="label"><asp:Label id="lang2360" runat="server">Email Address</asp:Label></td>
						<td class="plainlabel" id="Td4" runat="server"></td>
					</tr>
					<tr height="20">
						<td class="label" id="Td5" runat="server"><asp:Label id="lang2361" runat="server">Modified By</asp:Label></td>
						<td class="plainlabel" id="tdmby" runat="server"></td>
						<td class="label"><asp:Label id="lang2362" runat="server">Modified Date</asp:Label></td>
						<td class="plainlabel" id="tdmdate" runat="server"></td>
					</tr>
					<tr height="20">
						<td class="label" id="Td6" runat="server"><asp:Label id="lang2363" runat="server">Phone</asp:Label></td>
						<td class="plainlabel" id="tdcrphone" runat="server">&nbsp;</td>
						<td class="label"><asp:Label id="lang2364" runat="server">Email Address</asp:Label></td>
						<td class="plainlabel" id="tdcremail" runat="server">&nbsp;</td>
					</tr>
					<tr>
						<td align="right" colSpan="4"><asp:imagebutton id="ibtnsaveneweq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
					</tr>
				</TBODY>
			</table>
			<input id="lblnewfuncid" type="hidden" runat="server"> <input id="lbleqid" type="hidden" runat="server">
			<input id="lblcid" type="hidden" runat="server"><input id="txtpg" type="hidden" runat="server">
			<input id="lblfromeq" type="hidden" runat="server"><input id="lbloldfuncid" type="hidden" runat="server">
			<input id="lblsid" type="hidden" runat="server"><input id="lbllog" type="hidden" runat="server">
			<input id="lbldid" type="hidden" name="lbldid" runat="server"> <input id="lblclid" type="hidden" name="lblclid" runat="server">
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="Hidden1" type="hidden" name="Hidden1" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="spdivy" runat="server"><input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
