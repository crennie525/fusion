﻿Imports System.Data.SqlClient

Public Class usemeter
    Inherits System.Web.UI.Page
    Dim mf As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim eqid, eqnum, pmtskid, meterid As String
    Dim skill, skillid, skillqty, func, fuid, rd, rdid, rdt As String
    Dim meter, dayfreq, meterfreq, maxdays, munit, mfid As String
    Dim wkuse As Integer
    Dim typ, skillo, skillido, skillqtyo, rdo, rdido, meterido As String
    Dim mfido, meterfreqo, dayfreqo, maxdayso As String
    Dim tottime, downtime, who, pmtid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
                If who = "tot" Then
                    pmtid = Request.QueryString("pmtid").ToString
                    lblpmtid.Value = pmtid
                End If
            Catch ex As Exception

            End Try
            eqid = Request.QueryString("eqid").ToString
            eqnum = Request.QueryString("eqnum").ToString
            pmtskid = Request.QueryString("pmtskid").ToString
            fuid = Request.QueryString("fuid").ToString
            skillid = Request.QueryString("skillid").ToString
            skillqty = Request.QueryString("skillqty").ToString
            rdid = Request.QueryString("rdid").ToString

            lbleqid.Value = eqid
            lbleqnum.Value = eqnum
            lblpmtskid.Value = pmtskid
            lblfuid.Value = fuid
            lblskillid.Value = skillid
            lblskillqty.Value = skillqty
            lblrdid.Value = rdid

            func = Request.QueryString("func").ToString
            skill = Request.QueryString("skill").ToString
            rd = Request.QueryString("rd").ToString

            lblfunc.Value = func

            lblskill.Value = skill

            lblrd.Value = rd


            meterid = Request.QueryString("meterid").ToString
            If meterid = "" Then
                meterid = "0"
            End If
            lblmeterid.Value = meterid



            If typ = "dad" Or typ = "dadr" Then
                adel.Attributes.Add("class", "details")
                'downtime = Request.QueryString("downtime").ToString
                'tottime = Request.QueryString("tottime").ToString
                'If downtime = "" Then
                'downtime = "0"
                'End If
                'If tottime = "" Then
                'tottime = "0"
                'End If
                'lbldowntime.Value = downtime
                'lbltottime.Value = tottime
            End If

            If typ = "orig" Then
                skillido = Request.QueryString("skillido").ToString
                skillqtyo = Request.QueryString("skillqtyo").ToString
                rdido = Request.QueryString("rdido").ToString
                skillo = Request.QueryString("skillo").ToString
                rdo = Request.QueryString("rdo").ToString
                meterido = Request.QueryString("meterido").ToString
                If meterido = "" Then
                    meterido = "0"
                End If
                lblskillido.Value = skillido
                lblskillqtyo.Value = skillqtyo
                lblrdido.Value = rdido
                lblskillo.Value = skillo
                tdskill.InnerHtml = skillo
                tdqty.InnerHtml = skillqtyo
                lblrdo.Value = rdo
                tdrd.InnerHtml = rdo
                tdfunc.InnerHtml = func
                lblmeterido.Value = meterido
            Else
                tdrd.InnerHtml = rd
                tdfunc.InnerHtml = func
                tdskill.InnerHtml = skill
                tdqty.InnerHtml = skillqty
            End If

            mf.Open()

            If typ = "orig" Then
                getdetails(meterid, pmtskid, typ, meterido)
            Else
                getdetails(meterid, pmtskid, typ)
            End If
            If typ <> "dad" And typ <> "dadr" Then
                updatestuff()
            End If

            mf.Dispose()
        Else
            If Request.Form("lblsubmit") = "getmeter" Then
                meterid = lblmeterid.Value
                meterido = lblmeterido.Value
                pmtskid = lblpmtskid.Value
                typ = lbltyp.Value
                mf.Open()
                If typ = "orig" Then
                    getdetails(meterid, pmtskid, typ, meterido)
                Else
                    getdetails(meterid, pmtskid, typ)
                End If
                mf.Dispose()
                lblsubmit.Value = ""
            ElseIf Request.Form("lblsubmit") = "savemeter" Then
                mf.Open()
                savemeter()
                mf.Dispose()
            ElseIf Request.Form("lblsubmit") = "del" Then
                mf.Open()
                delmeter()
                mf.Dispose()
            End If
            func = lblfunc.Value
            tdfunc.InnerHtml = func
            skill = lblskill.Value
            tdskill.InnerHtml = skill
            rd = lblrd.Value
            tdrd.InnerHtml = rd
            skillqty = lblskillqty.Value
            tdqty.InnerHtml = skillqty

            typ = lbltyp.Value
            If typ = "orig" Then
                meter = lblmetero.Value
            Else
                meter = lblmeter.Value
            End If

            meterid = lblmeterid.Value

            tdmeter.InnerHtml = meter
            wkuse = lblwkuse.Value
            tdwkuse.InnerHtml = wkuse
            munit = lblunit.Value
            tdunits.InnerHtml = munit

        End If

    End Sub
    Private Sub updatestuff()
        skillid = lblskillid.Value
        skillqty = lblskillqty.Value
        rdid = lblrdid.Value
        skill = lblskill.Value
        rd = lblrd.Value
        pmtskid = lblpmtskid.Value
        mfid = lblmfid.Value
        sql = "update pmtasks set skillid = '" & skillid & "', skill = '" & skill & "', rdid = '" & rdid & "', " _
            + "rd = '" & rd & "', qty = '" & skillqty & "' where pmtskid = '" & pmtskid & "'"
        If mfid <> "" Then
            sql += "update meterfreq set skillid = '" & skillid & "', rdid = '" & rdid & "', skillqty = '" & skillqty & "' " _
           + "where mfid = '" & mfid & "'"
        End If
        mf.Update(sql)

        typ = lbltyp.Value
        If typ = "orig" Then
            skillido = lblskillido.Value
            skillqtyo = lblskillqtyo.Value
            rdido = lblrdido.Value
            skillo = lblskillo.Value
            rdo = lblrdo.Value
            mfido = lblmfido.Value
            sql = "update pmtasks set origskillid = '" & skillido & "', origskill = '" & skillo & "', origrdid = '" & rdido & "', " _
           + "origrd = '" & rdo & "', origqty = '" & skillqtyo & "' where pmtskid = '" & pmtskid & "'"
            If mfido <> "" Then
                sql += "update meterfreqo set skillid = '" & skillido & "', rdid = '" & rdido & "', skillqty = '" & skillqtyo & "' " _
           + "where mfid = '" & mfid & "'"
            End If
            mf.Update(sql)
        End If

        meterid = lblmeterid.Value
        meterido = lblmeterido.Value
        pmtskid = lblpmtskid.Value
        typ = lbltyp.Value
        If typ = "orig" Then
            getdetails(meterid, pmtskid, typ, meterido)
        Else
            getdetails(meterid, pmtskid, typ)
        End If

    End Sub
    Private Sub delmeter()
        mfid = lblmfid.Value
        mfido = lblmfido.Value
        meterid = lblmeterid.Value
        meterido = lblmeterido.Value
        typ = lbltyp.Value
        pmtskid = lblpmtskid.Value
        If typ <> "dad" Then
            If typ = "orig" Then
                sql = "delete from meterfreqo where mfid = '" & mfido & "'; " _
                    + "update pmtasks set usemetero = null, meterido = null, mfido = null, maxdayso = null, meterfreqo = null " _
                    + "where pmtskid = '" & pmtskid & "'"
                mf.Update(sql)
                lblmeterido.Value = ""
                lblmetero.Value = ""
                lblwkuse.Value = ""
                lblunit.Value = ""
                If meterid = meterido Then
                    sql = "delete from meterfreq where mfid = '" & mfid & "'; " _
                    + "update pmtasks set usemeter = null, meterid = null, mfid = null, maxdays = null, meterfreq = null " _
                    + "where pmtskid = '" & pmtskid & "'"
                    mf.Update(sql)
                    lblmeterid.Value = ""
                    lblmeter.Value = ""
                End If
                lbldel.Value = "yes"
            Else
                sql = "delete from meterfreq where mfid = '" & mfid & "'; " _
                    + "update pmtasks set usemeter = null, meterid = null, mfid = null, maxdays = null, meterfreq = null " _
                    + "where pmtskid = '" & pmtskid & "'"
                mf.Update(sql)
                lblmeterid.Value = ""
                lblmeter.Value = ""
                lblwkuse.Value = ""
                lblunit.Value = ""
                lbldel.Value = "yes"
            End If
        End If
    End Sub
    Private Sub savemeter()
        wkuse = lblwkuse.Value
        munit = lblunit.Value
        meterfreq = txtfreq.Text
        maxdays = txtmax.Text
        If maxdays = "" Then
            maxdays = "0"
        End If
        Dim mfchk As Long
        Try
            'mfchk = System.Convert.ToDecimal(meterfreq)
            mfchk = System.Convert.ToInt32(meterfreq)
        Catch ex As Exception
            'Dim strMessage As String = "Frequency Must Be a Numeric Value"
            Dim strMessage As String = "Frequency Must Be a Non-Decimal Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim mxchk As Long
        Try
            mxchk = System.Convert.ToInt32(maxdays)
        Catch ex As Exception
            Dim strMessage As String = "Max Days Must Be a Non-Decimal Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim wkusei As Integer
        '**** do not get why on this one - see emeter
        Try
            wkusei = System.Convert.ToInt32(wkuse)
        Catch ex As Exception
            Dim strMessage As String = "Problem Converting Weekly Use Value to Integer"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        mfid = lblmfid.Value
        mfido = lblmfido.Value
        meterid = lblmeterid.Value
        meterido = lblmeterido.Value
        typ = lbltyp.Value
        pmtskid = lblpmtskid.Value
        tottime = lbltottime.Value
        downtime = lbldowntime.Value
        meter = lblmeter.Value
        eqid = lbleqid.Value
        Dim dadcnt As Integer
        If munit <> "CYCLES" Then
            Dim dayuse As Integer 'Decimal
            Dim dayhold As String
            Try
                dayuse = (mfchk / (wkuse / 7)) 'changed from wkusei
            Catch ex As Exception
                dayuse = (mfchk / (wkusei / 7)) 'changed from wkusei
            End Try

            dayuse = System.Math.Round(dayuse)
            dayfreq = dayuse
            If maxdays = "0" Then
                maxdays = dayfreq
            End If
            If dayuse > mxchk Then
                dayhold = dayuse
                dayfreq = maxdays
                tdmsg.InnerHtml = "Warning! Your calculated Meter Frequency in Days (" & dayhold & " days) was greater than the Max Days value you entered. " _
                    + "<br />Fusion defaulted your Max Days value to your New Meter Frequency in Days value."
                txtmax.Text = dayhold
            End If
            If typ = "orig" Then
                sql = "update meterfreqo set meterfreq = '" & meterfreq & "', dayfreq = '" & dayfreq & "', maxdays = '" & maxdays & "' " _
               + "where mfid = '" & mfido & "'; "
                sql += "update pmtasks set origfreq = '" & dayfreq & "', meterfreqo = '" & meterfreq & "', maxdayso = '" & maxdays & "' where pmtskid = '" & pmtskid & "'"
                If meterid = meterido Then
                    sql += "update meterfreq set meterfreq = '" & meterfreq & "', dayfreq = '" & dayfreq & "', maxdays = '" & maxdays & "' " _
               + "where mfid = '" & mfid & "'; "
                    sql += "update pmtasks set freq = '" & dayfreq & "', meterfreq = '" & meterfreq & "', maxdays = '" & maxdays & " where pmtskid = '" & pmtskid & "'"
                End If
            ElseIf typ = "dad" Then
                tddayfreq.InnerHtml = dayfreq
                lbldayfreq.Value = dayfreq
                lblmeterid.Value = meterid
                Exit Sub
                sql = "select count(*) from pmtottime where eqid = '" & eqid & "' and skillid = '" & skillid & "', and freq = '" & dayfreq & "', " _
                    + "and rdid = '" & rdid & "', and meterid = '" & meterid & "' and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "'"
                dadcnt = mf.Scalar(sql)
                If dadcnt = 0 Then
                    sql = "insert into pmtottime (eqid, skillid, skill, freq, rdid, rd, tottime, skillqty, downtime, " _
                   + "ismeter, meterid, meter, meterunit, meterfreq, maxdays) " _
                   + "values ('" & eqid & "','" & skillid & "','" & skill & "','" & dayfreq & "','" & rdid & "','" & rd & "', " _
                   + "'" & tottime & "','" & downtime & "','1'," & meter & "','" & munit & "','" & meterid & "','" & meterfreq & "','" & maxdays & "')"
                Else
                    Dim strMessage As String = "This is Already an Established Meter Frequency for this Equipment Record"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If


            Else
                lbldayfreq.Value = dayfreq
                sql = "update meterfreq set meterfreq = '" & meterfreq & "', dayfreq = '" & dayfreq & "', maxdays = '" & maxdays & "' " _
               + "where mfid = '" & mfid & "'; "
                If typ <> "dadr" Then
                    sql += "update pmtasks set freq = '" & dayfreq & "', meterfreq = '" & meterfreq & "', maxdays = '" & maxdays & "' where pmtskid = '" & pmtskid & "'"
                End If

            End If

            mf.Update(sql)
            tddayfreq.InnerHtml = dayfreq
        Else
            If typ = "orig" Then
                sql = "update meterfreqo set meterfreq = '" & meterfreq & "', dayfreq = '" & maxdays & "', maxdays = '" & maxdays & "' " _
               + "where mfid = '" & mfido & "'; "
                sql += "update pmtasks set origfreq = '" & maxdays & "', meterfreqo = '" & meterfreq & "', maxdayso = '" & maxdays & " where pmtskid = '" & pmtskid & "'"
                If meterid = meterido Then
                    sql += "update meterfreq set meterfreq = '" & meterfreq & "', dayfreq = '" & maxdays & "', maxdays = '" & maxdays & "' " _
               + "where mfid = '" & mfid & "'; "
                    sql += "update pmtasks set freq = '" & maxdays & "', meterfreq = '" & meterfreq & "', maxdays = '" & maxdays & " where pmtskid = '" & pmtskid & "'"
                End If
            ElseIf typ = "dad" Then
                tddayfreq.InnerHtml = dayfreq
                lbldayfreq.Value = dayfreq
                Exit Sub
                sql = "select count(*) from pmtottime where eqid = '" & eqid & "' and skillid = '" & skillid & "' and freq = '" & dayfreq & "' " _
                    + "and rdid = '" & rdid & "' and meterid = '" & meterid & "' and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "'"
                dadcnt = mf.Scalar(sql)
                If dadcnt = 0 Then
                    sql = "insert into pmtottime (eqid, skillid, skill, freq, rdid, rd, tottime, skillqty, downtime, " _
                    + "ismeter, meterid, meter, meterunit, meterfreq, maxdays) " _
                   + "values ('" & eqid & "','" & skillid & "','" & skill & "','" & maxdays & "','" & rdid & "','" & rd & "', " _
                   + "'" & tottime & "','" & downtime & "','1'," & meter & "','" & munit & "','" & meterid & "','" & meterfreq & "','" & maxdays & "')"
                Else
                    Dim strMessage As String = "This is Already an Established Meter Frequency for this Equipment Record"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If

            Else
                sql = "update meterfreq set meterfreq = '" & meterfreq & "', dayfreq = '" & maxdays & "', maxdays = '" & maxdays & "' " _
               + "where mfid = '" & mfid & "'; "
                If typ <> "dadr" Then
                    sql += "update pmtasks set freq = '" & maxdays & "', meterfreq = '" & meterfreq & "', maxdays = '" & maxdays & "' where pmtskid = '" & pmtskid & "'"
                End If

            End If

            mf.Update(sql)
            tddayfreq.InnerHtml = maxdays
            lbldel.Value = ""
        End If
    End Sub
    Private Sub getdetails(ByVal meterid As String, ByVal pmtskid As String, ByVal typ As String, Optional ByVal meterido As String = "0")
        If typ <> "orig" Then
            If meterid <> "0" Then
                If meterid <> "" Then
                    sql = "select meter, unit, wkuse from meters where meterid = '" & meterid & "'"
                    dr = mf.GetRdrData(sql)
                    While dr.Read
                        meter = dr.Item("meter").ToString
                        wkuse = dr.Item("wkuse").ToString
                        munit = dr.Item("unit").ToString
                    End While
                    dr.Close()
                    lblmeter.Value = meter
                    tdmeter.InnerHtml = meter
                    lblwkuse.Value = wkuse
                    tdwkuse.InnerHtml = wkuse
                    lblunit.Value = munit
                    tdunits.InnerHtml = munit
                End If

            End If
        Else
            If meterido <> "0" Then
                If meterido <> "" Then
                    sql = "select meter, unit, wkuse from meters where meterid = '" & meterido & "'"
                    dr = mf.GetRdrData(sql)
                    While dr.Read
                        meter = dr.Item("meter").ToString
                        wkuse = dr.Item("wkuse").ToString
                        munit = dr.Item("unit").ToString
                    End While
                    dr.Close()
                    lblmetero.Value = meter
                    tdmeter.InnerHtml = meter
                    lblwkuse.Value = wkuse
                    tdwkuse.InnerHtml = wkuse
                    lblunit.Value = munit
                    tdunits.InnerHtml = munit
                End If

            End If
        End If
        who = lblwho.Value
        eqid = lbleqid.Value
        skillid = lblskillid.Value
        rdid = lblrdid.Value
        pmtid = lblpmtid.Value
        'sql = "select count(*) from pmtottime where eqid = '" & eqid & "' and skillid = '" & skillid & "' and freq = '" & dayfreq & "' " _
        '+ "and rdid = '" & rdid & "' and meterid = '" & meterid & "' and meterfreq = '" & meterfreq & "' and maxdays = '" & maxdays & "'"
        If who = "tot" Then
            sql = "select m.mfid, t.meterid, t.meterfreq, t.freq as dayfreq, t.maxdays from pmtottime t left join meterfreq m on m.meterid = t.meterid where t.pmtid = '" & pmtid & "'"
        Else
            sql = "select mfid, meterfreq, dayfreq, maxdays from meterfreq where pmtskid = '" & pmtskid & "'"
        End If

        dr = mf.GetRdrData(sql)
        While dr.Read
            mfid = dr.Item("mfid").ToString
            meterfreq = dr.Item("meterfreq").ToString
            dayfreq = dr.Item("dayfreq").ToString
            maxdays = dr.Item("maxdays").ToString
        End While
        dr.Close()
        lbldayfreq.Value = dayfreq
        If munit <> "CYCLES" Then
            tddayfreq.InnerHtml = dayfreq
        Else
            tddayfreq.InnerHtml = maxdays
        End If
        txtfreq.Text = meterfreq
        txtmax.Text = maxdays
        lblfreq.Value = meterfreq
        lblmax.Value = maxdays
        lblmfid.Value = mfid
        If typ = "orig" Then
            sql = "select mfid, meterfreq, dayfreq, maxdays from meterfreqo where pmtskid = '" & pmtskid & "'"
            dr = mf.GetRdrData(sql)
            While dr.Read
                mfido = dr.Item("mfid").ToString
                meterfreqo = dr.Item("meterfreq").ToString
                dayfreqo = dr.Item("dayfreq").ToString
                maxdayso = dr.Item("maxdays").ToString
            End While
            dr.Close()
            lbldayfreq.Value = dayfreqo
            If munit <> "CYCLES" Then
                tddayfreq.InnerHtml = dayfreqo
            Else
                tddayfreq.InnerHtml = maxdayso
            End If
            txtfreq.Text = meterfreqo
            txtmax.Text = maxdayso
            lblfreqo.Value = meterfreqo
            lblmaxo.Value = maxdayso
            lblmfido.Value = mfido
        End If
    End Sub
End Class