

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class funcedit
    Inherits System.Web.UI.Page
	Protected WithEvents lang2388 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2387 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2386 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim func As New Utilities
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim fuid, funcn, spl, eqid As String
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblspl As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtfuncspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsaveneweq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tblneweq As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtfunc As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            'funcn = Request.QueryString("func").ToString
            'lblfunc.Value = funcn
            'txtfunc.Text = funcn
            'spl = Request.QueryString("spl").ToString
            'lblspl.Value = spl
            'txtfuncspl.Text = spl
            func.Open()
            GetFunc(fuid)
            tblneweq.Attributes.Add("class", "view")
            func.Dispose()

        End If
    End Sub
    Private Sub GetFunc(ByVal fuid As String)
        sql = "select func, spl from functions where func_id = '" & fuid & "'"
        dr = func.GetRdrData(sql)
        While dr.Read
            funcn = dr.Item("func").ToString
            spl = dr.Item("spl").ToString
        End While
        dr.Close()
        lblfunc.Value = funcn
        txtfunc.Text = funcn
        lblspl.Value = spl
        txtfuncspl.Text = spl
    End Sub
    Private Sub ibtnsaveneweq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsaveneweq.Click
        Dim newfunc, newspl As String
        fuid = lblfuid.Value
        funcn = lblfunc.Value
        spl = lblspl.Value

        eqid = lbleqid.Value

        newfunc = txtfunc.Text

        newfunc = Replace(newfunc, "'", Chr(180), , , vbTextCompare)
        newfunc = Replace(newfunc, "--", "-", , , vbTextCompare)
        newfunc = Replace(newfunc, ";", ":", , , vbTextCompare)

        newspl = txtfuncspl.Text

        newspl = Replace(newspl, "'", Chr(180), , , vbTextCompare)
        newspl = Replace(newspl, "--", "-", , , vbTextCompare)
        newspl = Replace(newspl, ";", ":", , , vbTextCompare)
        func.Open()
        Dim funccnt As Integer
        If newfunc <> funcn Then
            sql = "select count(*) from functions where func = '" & newfunc & "' and eqid = '" & eqid & "'"
            funccnt = func.Scalar(sql)
        Else
            funccnt = 0
        End If
        Dim ustr As String
        Try
            ustr = HttpContext.Current.Session("username").ToString
        Catch ex As Exception
            ustr = ""
        End Try
        If funccnt = 0 Then
            sql = "update functions set func = '" & newfunc & "', " _
            + "spl = '" & newspl & "', " _
            + "modifiedby = '" & ustr & "', " _
            + "modifieddate = getDate() " _
            + "where func_id = '" & fuid & "'"

            func.Update(sql)

            lblret.Value = "go"
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr962" , "funcedit.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        func.Dispose()
    End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2386.Text = axlabs.GetASPXPage("funcedit.aspx","lang2386")
		Catch ex As Exception
		End Try
		Try
			lang2387.Text = axlabs.GetASPXPage("funcedit.aspx","lang2387")
		Catch ex As Exception
		End Try
		Try
			lang2388.Text = axlabs.GetASPXPage("funcedit.aspx","lang2388")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			ibtnsaveneweq.Attributes.Add("src" , "../images2/eng/bgbuttons/save.gif")
			ElseIf lang = "fre" Then
			ibtnsaveneweq.Attributes.Add("src" , "../images2/fre/bgbuttons/save.gif")
			ElseIf lang = "ger" Then
			ibtnsaveneweq.Attributes.Add("src" , "../images2/ger/bgbuttons/save.gif")
			ElseIf lang = "ita" Then
			ibtnsaveneweq.Attributes.Add("src" , "../images2/ita/bgbuttons/save.gif")
			ElseIf lang = "spa" Then
			ibtnsaveneweq.Attributes.Add("src" , "../images2/spa/bgbuttons/save.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class
