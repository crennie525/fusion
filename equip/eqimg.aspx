<%@ Page Language="vb" AutoEventWireup="false" Codebehind="eqimg.aspx.vb" Inherits="lucy_r12.eqimg" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>eqimg</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/eqimgaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function getbig() {
             var img = document.getElementById("lblcurrimg").value;
             if (img != "") {
                 var currp = document.getElementById("lblcurrp").value;
                 var bimgs = document.getElementById("lblbimgs").value;
                 var bimgsarr = bimgs.split(",");
                 var src = bimgsarr[currp];
                 //src = "../eqimages/" + src;
                 window.open("BigPic.aspx?src=" + src)
             }
         }
     </script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="240" style="LEFT: 0px; POSITION: absolute; TOP: 0px">
				<tr height="16">
					<td align="center" class="plainlabel" id="tdeq" runat="server"></td>
				</tr>
				<tr>
					<td align="center">
						<A onclick="getbig();" href="#"><IMG id="imgeq" height="216" src="../images/appimages/eqimg1.gif" style="width: 216px" border="0"
								runat="server"></A></td>
				</tr>
				<tr>
					<td align="center" colSpan="2">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 140px;" vAlign="middle" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ilast" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbleqid" runat="server"> <input id="lblpcnt" type="hidden" runat="server" NAME="lblpcnt">
			<input id="lblcurrp" type="hidden" runat="server" NAME="lblcurrp"> <input id="lblimgs" type="hidden" name="lblimgs" runat="server">
			<input id="lblimgid" type="hidden" name="lblimgid" runat="server"> <input id="lblovimgs" type="hidden" name="lblovimgs" runat="server">
			<input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server"> <input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server">
			<input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server"> <input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"><input type="hidden" id="lblfuid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
