﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="delec.aspx.vb" Inherits="lucy_r12.delec" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Delete Options</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript">
        function goback(who) {
            window.returnValue = who;
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="redlabel">
    <input type="radio" id="rbe1" name="rb" onclick="goback('1');" />Error Code 1 and All Associated Values<br />
    <input type="radio" id="rbe2" name="rb" onclick="goback('2');" />Error Code 2 and All Associated Values<br />
    <input type="radio" id="rbe3" name="rb" onclick="goback('3');" />Just Error Code 3<br />
    <input type="radio" id="rbc" name="rb" onclick="goback('4');" />Cancel Delete<br />
    </td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
