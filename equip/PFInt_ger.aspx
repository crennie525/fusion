<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PFInt_ger.aspx.vb" Inherits="lucy_r12.PFInt_ger" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>PFInt_ger</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		<script  type="text/javascript">
		<!--
		function updateri() {
		calc1();
		calc2();
		}
		function calc1() {
		var pf = document.getElementById("txtpfi").value;
		var lt = document.getElementById("txtlt").value;
			if (isNaN(parseInt(pf))||isNaN(parseInt(lt))) {
			alert("Nur Zahlen zul�ssig");
			}
			else {
			var ans1 = parseInt(pf) - parseInt(lt);
			document.getElementById("lbans1").innerHTML = ans1;
			document.getElementById("lblans1").value = ans1;
			}
		}
		function calc2() {
		var rs = document.getElementById("lbans1").innerHTML;
		var sf = document.getElementById("ddsf").value;
		if (parseInt(sf)!=0) {
		var ans1 = Math.round(parseInt(rs) / parseInt(sf));
		if(isNaN(ans1)) {
		alert("Interval is not a Number")
		}
		else {
		    document.getElementById("txtri").value = ans1;
		    document.getElementById("lblpfi").value = ans1;
		}
		}
		else {
		alert("W�hlen Sie einen Sicherheitsfaktor")
		}
		}
		function handleexit() {
		var log = document.getElementById("lbllog").value;
			if(log=="no"||log=="noeqid") setref();
			else window.setTimeout("setref();", 1205000);
		var ecrflag = document.getElementById("ecrflag").value;
		if (ecrflag=="1") updateri();
		var fin = document.getElementById("txtfin").value;
		if (fin=="1") {
		var pfi = document.getElementById("lblpfi").value;
		//alert(pfi)
		window.parent.handleexit(pfi);
		}
		}
		function setref() {
		//document.getElementById("form1").submit();
		document.getElementById("lblpfi").value="0";
		document.getElementById("txtfin").value="1";
		handleexit();
		}
		-->
		</script>
	</HEAD>
	<body onload="handleexit();" >
		<form id="form1" method="post" runat="server">
			&nbsp;
			<DIV style="BACKGROUND-IMAGE: url(../images2/ger/pfbg.gif); POSITION: absolute; WIDTH: 629px; HEIGHT: 510px; TOP: 0px; LEFT: 0px"
				class="pfbg" ><asp:textbox style="Z-INDEX: 100; POSITION: absolute; TOP: 320px; LEFT: 320px" id="txtpfi" runat="server"
					Width="60px" Height="26px"></asp:textbox><asp:label style="Z-INDEX: 112; POSITION: absolute; TOP: 216px; LEFT: 112px" id="lblecr" runat="server"
					CssClass="label"></asp:label><asp:dropdownlist style="Z-INDEX: 107; POSITION: absolute; TOP: 410px; LEFT: 320px" id="ddsf" runat="server">
					<asp:ListItem Value="0">W�hlen</asp:ListItem>
					<asp:ListItem Value="3">3</asp:ListItem>
					<asp:ListItem Value="2">2</asp:ListItem>
					<asp:ListItem Value="1">1</asp:ListItem>
				</asp:dropdownlist><IMG style="Z-INDEX: 106; POSITION: absolute; TOP: 410px; LEFT: 416px; width: 20px;" id="c2" onmouseover="return overlib('Errechnen Sie Abstand', WRAP)"
					onmouseout="return nd()" onclick="calc2();" alt="" src="../images/appbuttons/minibuttons/lilcalc.gif"
					height="20"><IMG style="Z-INDEX: 105; POSITION: absolute; TOP: 352px; LEFT: 416px; width: 20px;" id="c1" onmouseover="return overlib('Errechnen Sie', WRAP)"
					onmouseout="return nd()" onclick="calc1();" alt="" src="../images/appbuttons/minibuttons/lilcalc.gif"
					height="20">
				<asp:textbox style="Z-INDEX: 104; POSITION: absolute; TOP: 90px; LEFT: 318px" id="txtpfidup"
					runat="server" Width="60px" Height="26px" Enabled="False"></asp:textbox><asp:textbox style="Z-INDEX: 103; POSITION: absolute; TOP: 198px; LEFT: 536px" id="txtltdup"
					runat="server" Width="60px" Height="26px" Enabled="False"></asp:textbox><asp:textbox style="Z-INDEX: 102; POSITION: absolute; TOP: 452px; LEFT: 320px" id="txtri" runat="server"
					Width="60px" Height="26px" ReadOnly="True"></asp:textbox><asp:textbox style="Z-INDEX: 101; POSITION: absolute; TOP: 346px; LEFT: 320px" id="txtlt" runat="server"
					Width="60px" Height="26px"></asp:textbox><asp:imagebutton style="Z-INDEX: 108; POSITION: absolute; TOP: 450px; LEFT: 448px" id="ImageButton1"
					runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"></asp:imagebutton><asp:label style="Z-INDEX: 109; POSITION: absolute; TOP: 384px; LEFT: 320px" id="lbans1" runat="server"
					CssClass="label"></asp:label><asp:label style="Z-INDEX: 111; POSITION: absolute; TOP: 216px; LEFT: 16px" id="Label1" runat="server"
					CssClass="label">Aktuelle ECR:</asp:label></DIV>
			<input id="txttaskid" type="hidden" name="txttaskid" runat="server"><input id="txtfin" type="hidden" name="txtfin" runat="server">
			<input id="lblpfi" type="hidden" name="lblpfi" runat="server"><input id="ecrflag" type="hidden" name="ecrflag" runat="server">
			<input id="lblans1" type="hidden" name="lblans1" runat="server"><input id="lbllog" type="hidden" name="lbllog" runat="server">
			<input id="lblfslang" type="hidden" name="lblfslang" runat="server"><input type="hidden" id="lblro" runat="server" NAME="lblro">
		</form>
	</body>
</HTML>
