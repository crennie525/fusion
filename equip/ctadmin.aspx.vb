

'********************************************************
'*
'********************************************************



Public Class ctadmin
    Inherits System.Web.UI.Page
	Protected WithEvents lang2070 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2069 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2068 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2067 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2066 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2065 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2064 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2063 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2062 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2061 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtps As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdss As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspa As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdscs As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcpl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            Dim ss, spa, scs, cpl As DateTime
            Try
                ss = Request.QueryString("ss").ToString
                tdss.InnerHtml = ss
            Catch ex As Exception

            End Try
            Try
                spa = Request.QueryString("spa").ToString
                tdspa.InnerHtml = spa
            Catch ex As Exception

            End Try
            Try
                scs = Request.QueryString("scs").ToString
                tdscs.InnerHtml = scs
            Catch ex As Exception

            End Try
            Try

                cpl = Request.QueryString("cpl").ToString
                tdcpl.InnerHtml = cpl
            Catch ex As Exception

            End Try
            Dim ps As Integer
            Try
                ps = Request.QueryString("ps").ToString
                txtps.Text = ps
            Catch ex As Exception

            End Try
        End If
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2061.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2061")
        Catch ex As Exception
        End Try
        Try
            lang2062.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2062")
        Catch ex As Exception
        End Try
        Try
            lang2063.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2063")
        Catch ex As Exception
        End Try
        Try
            lang2064.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2064")
        Catch ex As Exception
        End Try
        Try
            lang2065.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2065")
        Catch ex As Exception
        End Try
        Try
            lang2066.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2066")
        Catch ex As Exception
        End Try
        Try
            lang2067.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2067")
        Catch ex As Exception
        End Try
        Try
            lang2068.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2068")
        Catch ex As Exception
        End Try
        Try
            lang2069.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2069")
        Catch ex As Exception
        End Try
        Try
            lang2070.Text = axlabs.GetASPXPage("ctadmin.aspx", "lang2070")
        Catch ex As Exception
        End Try

    End Sub

End Class
