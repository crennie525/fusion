<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FuncGrid.aspx.vb" Inherits="lucy_r12.FuncGrid" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>FuncGrid</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/sessrefdialog.js"></script>
		<script  type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script  src="../scripts1/FuncGridaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="scrolltop();checkit();checkit2();" onunload="stoptimer();" >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 4px" width="980"
				bgColor="#0066ff">
				<tr>
					<td style="width: 100px"></td>
					<td width="370"></td>
					<td style="width: 130px"></td>
					<td width="190"></td>
					<td width="200"></td>
				</tr>
				<tr height="22">
					<td class="label"><asp:Label id="lang2389" runat="server">Equipment#</asp:Label></td>
					<td class="whitelabel" id="tdeq" runat="server"></td>
					<td class="label"><asp:Label id="lang2390" runat="server">Description</asp:Label></td>
					<td class="whitelabel" id="tdeqd" colSpan="2" runat="server"></td>
				</tr>
				<tr>
					<td><asp:label id="Label9" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Current Sort</asp:label></td>
					<td><asp:label id="lblsort" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"
							ForeColor="White">Current Sort</asp:label></td>
					<td class="label"><asp:Label id="lang2391" runat="server">New Function#</asp:Label></td>
					<td><asp:textbox id="txtnewfunc" runat="server" MaxLength="100"></asp:textbox></td>
					<td align="right"><asp:imagebutton id="addfunc" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton><asp:imagebutton id="ibtnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td align="center" colSpan="5"><asp:label id="ErrorLabel" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small"
							ForeColor="White"></asp:label></td>
				</tr>
			</table>
			<table style="Z-INDEX: 101; POSITION: absolute; TOP: 72px; LEFT: 4px" width="880">
				<TBODY>
					<tr>
						<td><asp:datagrid id="dgfunc" runat="server" GridLines="None" CellSpacing="1" AllowSorting="True"
								AutoGenerateColumns="False">
								<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel"></ItemStyle>
								<HeaderStyle Height="26px" CssClass="btmmenu plainlabel"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit">
										<HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												ToolTip="Save Changes" CommandName="Update"></asp:imagebutton>
											<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												ToolTip="Cancel Changes" CommandName="Cancel"></asp:imagebutton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="routing desc" HeaderText="Sequence#">
										<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=lblroute runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id=txtrouting runat="server" style="width: 40px" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="func desc" HeaderText="Function#">
										<HeaderStyle Width="240px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id=txtfunum runat="server" Width="130px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False" HeaderText="Special Designation">
										<HeaderStyle Width="300px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=lbldesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func_desc") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id=txtdesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func_desc") %>' width="240">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label runat="server" id="lblfuid" Text='<%# DataBinder.Eval(Container, "DataItem.func_id") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox runat="server" id="txtfuid" width="290" Text='<%# DataBinder.Eval(Container, "DataItem.func_id") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<ItemTemplate>
											<asp:Label runat="server" id="lbleqlock" Text='<%# DataBinder.Eval(Container, "DataItem.eqloc") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox runat="server" id="Textbox1" width="290" Text='<%# DataBinder.Eval(Container, "DataItem.func_id") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Special Designation">
										<HeaderStyle Width="300px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label runat="server" id="lblspl" Text='<%# DataBinder.Eval(Container, "DataItem.spl") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox runat="server" id="txtspl" width="290" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.spl") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td><asp:textbox id="xcoord" runat="server" ForeColor="Black" BorderColor="White" BorderStyle="None"
								Width="32px"></asp:textbox><asp:textbox id="ycoord" runat="server" ForeColor="Black" BorderColor="White" BorderStyle="None"
								Width="32px"></asp:textbox><asp:textbox id="txtfu" runat="server" ForeColor="White" BorderColor="White" BorderStyle="None"
								Width="24px"></asp:textbox></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</TBODY>
			</table>
			<iframe id="ifsession" class="details" src="" frameBorder="no" runat="server" style="Z-INDEX: 0">
			</iframe><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input id="lblcid" type="hidden" runat="server"><input id="lbleqid" type="hidden" runat="server">
			<input id="lblcurrsort" type="hidden" runat="server"> <input id="lbluser" type="hidden" runat="server">
			<input id="spy" type="hidden" runat="server"><input id="lblrouting" type="hidden" runat="server">
			<input id="lblchk" type="hidden" name="lblchk" runat="server"><input id="lblsid" type="hidden" name="lblsid" runat="server"><input id="lbldid" type="hidden" name="lbldid" runat="server">
			<input id="lblclid" type="hidden" name="lblclid" runat="server"><input id="lbleqchk" type="hidden" runat="server">
			<input type="hidden" id="lbllid" runat="server">
		<input type="hidden" id="lblustr" runat="server" />
        <input type="hidden" id="lblwho" runat="server" />
<input type="hidden" id="lbldept" runat="server" />
<input type="hidden" id="lblcell" runat="server" />
<input type="hidden" id="lblloc" runat="server" />
<input type="hidden" id="lbleqidh" runat="server" />
<input type="hidden" id="lbleq" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblfuid" runat="server" />
</form>
	</body>
</HTML>
