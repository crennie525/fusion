﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ecadmin.aspx.vb" Inherits="lucy_r12.ecadmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/smartscroll4.js"></script>
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
    <script  type="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function getnew() {
            var id = document.getElementById("txtnewecd").value;
            var desc = document.getElementById("txtnewdesc").value;
            if (id != "" && desc != "") {
                var eReturn = window.showModalDialog("ecadddialog.aspx?ecd1=" + id + "&e1desc=" + desc, "", "dialogHeight:500px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    document.getElementById("txtsearch").value = id;
                    document.getElementById("lblret").value = "first"
                    document.getElementById("form1").submit();
                }
            }
            else {
                alert("Error Code and Description Required")
            }
        }
        function jump(id, desc) {
            document.getElementById("txtnewecd").value = id;
            document.getElementById("txtnewdesc").value = desc;
        }
        function getfm(who) {
            var eReturn = window.showModalDialog("faillook.aspx", "", "dialogHeight:350px; dialogWidth:200px; resizable=yes");
            if (eReturn) {
                document.getElementById(who).value = eReturn;

            }
        }
        function getoc(who) {
            var eReturn = window.showModalDialog("oclook.aspx", "", "dialogHeight:350px; dialogWidth:200px; resizable=yes");
            if (eReturn) {
                document.getElementById(who).value = eReturn;

            }
        }
        function refit(who) {
            if (who == "1") {
                document.getElementById("txtsearch").value = "";
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
            else {
                document.getElementById("txtnewecd").value = "";
                document.getElementById("txtnewdesc").value = "";
            }
        }
        function getwho() {
            var eReturn = window.showModalDialog("delec.aspx", "", "dialogHeight:300px; dialogWidth:400px; resizable=yes");
            if (eReturn) {
                var who = eReturn;
                if (who != "4") {
                    document.getElementById("lbldodel").value = "";
                    document.getElementById("lbldelwho").value = eReturn;
                }
                else {
                    document.getElementById("lbldodel").value = "no";
                }

            }
            else {
                document.getElementById("lbldodel").value = "no";
            }
        }
    </script>
</head>
<body onload="GetsScroll();">
    <form id="form1" runat="server">
    
    <div>
    <table>
    <tr>
    <td>
    <table>
    <tr>
    <td class="label" style="width: 130px">Search by Code 1</td>
    <td style="width: 150px"><asp:TextBox ID="txtsearch" runat="server"></asp:TextBox></td>
    <td style="width: 120px"><img id="img2" alt="" onclick="refit('1');" runat="server" src="../images/appbuttons/minibuttons/refreshit.gif" /><asp:imagebutton id="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
    
    </tr>
    <tr>
    <td class="bluelabel">New Error Code 1</td>
    <td><asp:TextBox ID="txtnewecd" runat="server"></asp:TextBox></td>
    <td class="bluelabel">New Description</td>
    <td><asp:TextBox ID="txtnewdesc" runat="server"></asp:TextBox></td>
    <td style="width: 60px"><img id="img1" alt="" onclick="refit('2');" runat="server" src="../images/appbuttons/minibuttons/refreshit.gif" /><img src="../images/appbuttons/minibuttons/addmod.gif" onclick="getnew();" /></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <div style="WIDTH: 850px; HEIGHT: 300px; OVERFLOW: auto" id="spdiv" runat="server" onscroll="SetsDivPosition();">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" Width="830">
                        <AlternatingRowStyle CssClass="ptransrowblue"></AlternatingRowStyle>
                        <RowStyle CssClass="ptransrow"></RowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Edit">
                                <HeaderStyle Height="20px" Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton7" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Code 1">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <a href="#" ID="lnkphase" runat="server" >
                                    <%# DataBinder.Eval(Container, "DataItem.ecd1") %>
                                    </a>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtecd1" runat="server" Width="80px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.ecd1") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <HeaderStyle Width="130px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbldate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.edesc1") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtedesc1" runat="server" width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.edesc1") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Code 2">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblby" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtecd2" runat="server" width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Description">
                                <HeaderStyle width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmroute" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.edesc2") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtedesc2" runat="server" width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.edesc2") %>'>
                                    </asp:TextBox>
                                    <img id="imgecd2" alt="" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Code 3">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblpm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtecd3" runat="server" width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <HeaderStyle width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbledesc3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.edesc3") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtedesc3" runat="server" width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.edesc3") %>'>
                                    </asp:TextBox>
                                    <img id="imgfm" alt="" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd1idi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd1id") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd1ide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd1id") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd2idi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2id") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd2ide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2id") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd3idi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3id") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd3ide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3id") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd1i" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd1") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd1e" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd1") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd2i" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd2e" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd3i" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd3e" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Height="20px" Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
											CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                               
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    </div>
    </td>
    </tr>
    <tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
    </table>
    </div>
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lbloldedesc3" runat="server" />
    <input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="xCoord" type="hidden"><input id="yCoord" type="hidden">
            <input type="hidden" id="lbldodel" runat="server" />
            <input type="hidden" id="lbldelwho" runat="server" />
    </form>
</body>
</html>
