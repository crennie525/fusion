﻿Imports System.Data.SqlClient
Imports System.Text

Public Class meterprint
    Inherits System.Web.UI.Page
    Dim routeid As String
    Dim sql As String
    Dim met As New Utilities
    Dim dr As SqlDataReader

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            routeid = Request.QueryString("routeid").ToString
            met.Open()
            getroute(routeid)
            met.Dispose()

        End If
    End Sub
    Private Sub getroute(ByVal routeid As String)
        Dim sb As New System.Text.StringBuilder

        sb.Append("<Table cellSpacing=""0"" cellPadding=""2"" width=""680"">")
        sb.Append("<tr><td width=""120""></td><td width=""100""></td><td width=""80""></td><td width=""80""></td><td width=""300""></td></tr>")
        sb.Append("<tr><td colspan=""5"" align=""center"" class=""label"">Meter Route Report</td></tr>")
        sb.Append("<tr><td colspan=""5"" align=""center"" class=""label"">&nbsp;</td></tr>")
        sb.Append("<tr><td colspan=""5"" align=""center"" class=""label"">&nbsp;</td></tr>")

        sql = "select r.mroute, r.mdesc, m.meter, m.unit, e.eqnum, e.eqdesc, s.routeorder, s.location, s.locref " _
            + "from meterstops s " _
            + "left join meterroutes r on r.mrouteid = s.mrouteid " _
            + "left join meters m on m.meterid = s.meterid " _
            + "left join equipment e on e.eqid = s.eqid " _
            + "where s.mrouteid = '" & routeid & "' " _
            + "order by s.routeorder"
        Dim route, desc, meter, eqnum, eqdesc, routeorder, location, locref, units As String
        Dim startflag As String = "0"
        dr = met.GetRdrData(sql)
        While dr.Read
            route = dr.Item("mroute").ToString
            desc = dr.Item("mdesc").ToString
            meter = dr.Item("meter").ToString
            units = dr.Item("unit").ToString
            eqnum = dr.Item("eqnum").ToString
            eqdesc = dr.Item("eqdesc").ToString
            routeorder = dr.Item("routeorder").ToString
            location = dr.Item("location").ToString
            locref = dr.Item("locref").ToString
            If locref = "" Then
                locref = "Not Provided"
            End If
            If startflag = "0" Then
                startflag = "1"
                sb.Append("<tr><td class=""label"">Meter Route</td><td colspan=""4"" class=""plainlabel"">" & route & "</td></tr>")
                sb.Append("<tr><td class=""label"">Description</td><td colspan=""4"" class=""plainlabel"">" & desc & "</td></tr>")
                sb.Append("<tr><td colspan=""5"" align=""center"" class=""label"">&nbsp;</td></tr>")
                sb.Append("<tr><td colspan=""5"" align=""center"" class=""label"">&nbsp;</td></tr>")
            End If
            sb.Append("<tr><td class=""label"">Route Stop</td><td colspan=""4"" class=""plainlabel"">" & routeorder & "</td></tr>")
            sb.Append("<tr><td class=""label"">Location</td><td colspan=""4"" class=""plainlabel"">" & location & "</td></tr>")
            If eqdesc <> "" Then
                sb.Append("<tr><td class=""label"">Equipment</td><td colspan=""4"" class=""plainlabel"">" & eqnum & " - " & eqdesc & "</td></tr>")
            Else
                sb.Append("<tr><td class=""label"">Equipment</td><td colspan=""4"" class=""plainlabel"">" & eqnum & "</td></tr>")
            End If
            sb.Append("<tr><td class=""label"">Reference</td><td colspan=""4"" class=""plainlabel"">" & locref & "</td></tr>")
            sb.Append("<tr><td colspan=""5"" align=""center"" class=""label"">&nbsp;</td></tr>")

            sb.Append("<tr><td class=""plainlabel"">" & meter & "</td>")
            'If desc = "" Then
            'sb.Append("<td class=""plainlabel"">&nbsp;</td>")
            'Else
            'sb.Append("<td class=""plainlabel"">" & desc & "</td>")
            'End If

            sb.Append("<td class=""plainlabel"">" & units & "</td>")
            sb.Append("<td class=""plainlabel"">Reading:</td>")
            sb.Append("<td style=""border-bottom: solid Black 1px;"">&nbsp;</td></tr>")

            sb.Append("<tr><td colspan=""5"" align=""center"" class=""label"">&nbsp;</td></tr>")
            sb.Append("<tr><td colspan=""5"" align=""center"" class=""label"">&nbsp;</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        repdiv.InnerHtml = sb.ToString
    End Sub
End Class