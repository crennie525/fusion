﻿Imports System.Data.SqlClient
Public Class eqrouting
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim rt As New Utilities
    Dim tmod As New transmod
    Dim sid, eqid As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            rt.Open()
            Geteq(sid)
            rt.Dispose()

        End If
    End Sub
    Private Sub Geteq(ByVal sid As String)

        sql = "select count(*) from equipment where (routing is not null and routing <> 0) and siteid = '" & sid & "'"
        Dim rcnt As Integer
        rcnt = rt.Scalar(sql)
        If rcnt = 0 Then
            sql = "exec usp_eqdfltrt '" & sid & "'"
            rt.Update(sql)
        Else
            sql = "exec usp_uprteeqseq_tmp '" & sid & "'"
            rt.Update(sql)
        End If
        sql = "select e.routing, e.eqid, e.eqnum, e.eqdesc, location = (select top 1 p.parent from pmlocheir p where p.location = e.eqnum) from equipment e where e.siteid = '" & sid & "' order by e.routing"
        dr = rt.GetRdrData(sql)
        dglist.DataSource = dr
        dglist.DataBind()
        dr.Close()
    End Sub

    Private Sub dglist_CancelCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.CancelCommand
        dglist.EditItemIndex = -1
        rt.Open()
        sid = lblsid.Value
        Geteq(sid)
        rt.Dispose()
    End Sub

    Private Sub dglist_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.EditCommand
        lblrow.Value = CType(e.Item.FindControl("lblseq"), Label).Text
        sid = lblsid.Value
        rt.Open()
        dglist.EditItemIndex = e.Item.ItemIndex
        Geteq(sid)
        rt.Dispose()
    End Sub

       Private Sub dglist_UpdateCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dglist.UpdateCommand
        Dim st, ost As String
        ost = lblrow.Value
        sid = lblsid.Value
        st = CType(e.Item.FindControl("txtseq"), TextBox).Text
        Dim stchk As Long
        Try

            stchk = System.Convert.ToInt64(st)
        Catch ex As Exception
            Dim strMessage As String = tmod.getmsg("cdstr238", "PMTaskDivFunc.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If stchk < 1 Then
            st = ost
            Dim strMessage As String = "Task Number Must Be Greater Than Zero!"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else

        End If
        rt.Open()
        dglist.EditItemIndex = -1
        sql = "usp_reeqrt '" & sid & "', '" & st & "', '" & ost & "'"
        rt.Update(sql)
        Geteq(sid)
        rt.Dispose()
    End Sub
End Class