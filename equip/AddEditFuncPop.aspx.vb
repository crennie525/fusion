

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class AddEditFuncPop
    Inherits System.Web.UI.Page
	Protected WithEvents lang1911 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1910 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1909 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid As String
    Dim eqid, db, oloc As String
    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents dgfunc As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtnewspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblnewfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunchold As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim func As New Utilities
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Dim login, ro As String
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtnewfunc As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibaddfunc As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Page.EnableViewState = True
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try

        If Not IsPostBack Then
            If lbllog.Value <> "no" Then
                'Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                Try
                    db = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString 'Request.QueryString("db").ToString
                    oloc = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString 'Request.QueryString("oloc").ToString
                    lbldb.Value = db
                    lbloloc.Value = oloc
                Catch ex As Exception
                    db = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString '"0"
                    oloc = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString '"0"
                    lbldb.Value = db
                    lbloloc.Value = oloc
                End Try
                If ro = "1" Then
                    ibaddfunc.Enabled = False
                    ibaddfunc.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                    'imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                Else
                    'ibaddfunc.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
                    'ibaddfunc.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
                End If
                If Len(eqid) <> 0 AndAlso eqid <> "" AndAlso eqid <> "0" Then
                    Dim user As String = HttpContext.Current.Session("username").ToString
                    lbluser.Value = user
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    func.Open()
                    PopFunc()
                    func.Dispose()
                Else
                    Dim strMessage As String = tmod.getmsg("cdstr780" , "AddEditFuncPop.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If
                'Catch ex As Exception
                'Dim strMessage As String =  tmod.getmsg("cdstr781" , "AddEditFuncPop.aspx.vb")
 
                'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                'lbllog.Value = "noeqid"
                'End Try
            End If
        End If

        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub PopFunc()
        cid = lblcid.Value
        eqid = lbleqid.Value

        sql = "select f.func_id, f.func, f.func_desc, f.spl, " _
        + "count(c.comid) as comps, " _
        + "count(t.tasknum) as tasks " _
        + "from functions f left join components c on " _
        + "(c.func_id = f.func_id) or c.comid = 0 left join pmtasks t on " _
        + "(t.funcid = f.func_id) or " _
        + " t.pmtskid = 0 " _
        + "where(f.eqid = '" & eqid & "') " _
        + "group by f.func_id, f.func, f.func_desc, f.spl"

        sql = "select f.routing, f.func_id, f.func, f.func_desc, f.spl, " _
        + "comps = ( " _
        + "select count(c.comid) from components c where c.func_id = f.func_id), " _
        + "tasks = ( " _
        + "select count(t.tasknum) from pmtasks t where t.funcid = f.func_id and t.subtask = 0) " _
        + "from functions f " _
        + "where(f.eqid = '" & eqid & "') " _
        + "group by f.func_id, f.func, f.func_desc, f.spl, f.routing order by f.routing"

        dr = func.GetRdrData(sql)
        dgfunc.DataSource = dr
        dgfunc.DataBind()
        dr.Close()
    End Sub
    Private Sub ibaddfunc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibaddfunc.Click

        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr782" , "AddEditFuncPop.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            lbllog.Value = "no"
            Exit Sub
        End Try
        func.Open()
        eqid = lbleqid.Value
        If Len(eqid) <> 0 AndAlso eqid <> "" AndAlso eqid <> "0" Then
            cid = lblcid.Value
            Dim user As String = lbluser.Value
            Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
            Dim funum, fudesc, fuspl As String
            funum = txtnewfunc.Text
            funum = Replace(funum, "'", Chr(180), , , vbTextCompare)
            funum = Replace(funum, "--", "-", , , vbTextCompare)
            funum = Replace(funum, ";", ":", , , vbTextCompare)
            fudesc = "" 'txtnewdesc.Text
            fudesc = Replace(fudesc, "'", Chr(180), , , vbTextCompare)
            fuspl = txtnewspl.Text
            fuspl = Replace(fuspl, "'", Chr(180), , , vbTextCompare)
            fuspl = Replace(fuspl, "--", "-", , , vbTextCompare)
            fuspl = Replace(fuspl, ";", ":", , , vbTextCompare)
            Dim fucnt As Integer
            db = lbldb.Value
            Dim mdb As String = lbldb.Value
            Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
            Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB").ToString
            If db = "0" Or db = "" Then
                sql = "select count(*) from functions where eqid = '" & eqid & "' " _
                + "and func = '" & funum & "'"
            Else
                sql = "select count(*) from [" & srvr & "].[" & mdb & "].[dbo].[functions] where eqid = '" & eqid & "' " _
                + "and func = '" & funum & "'"
            End If
            fucnt = func.Scalar(sql)
            Dim funid As Integer
            If fucnt = 0 Then
                If db = "0" Or db = "" Then
                    sql = "usp_addFunc '" & cid & "', '" & eqid & "', '" & funum & "', '" & fudesc & "', '" & fuspl & "', '" & ustr & "'"
                Else
                    sql = "usp_addFuncMDB '" & cid & "', '" & eqid & "', '" & funum & "', '" & fudesc & "', '" & fuspl & "', '" & ustr & "','" & mdb & "','" & orig & "','" & srvr & "'"
                End If

                funid = func.Scalar(sql)
                lblnewfuncid.Value = funid
                txtnewfunc.Text = ""
                'txtnewdesc.Text = ""
                txtnewspl.Text = ""
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr783" , "AddEditFuncPop.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            lblchk.Value = "1"
            PopFunc()
            func.Dispose()
        Else
            Try
                login = HttpContext.Current.Session("Logged_IN").ToString()
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr784" , "AddEditFuncPop.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "no"
                Exit Sub
            End Try
            Dim strMessage1 As String =  tmod.getmsg("cdstr785" , "AddEditFuncPop.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
            lbllog.Value = "noeqid"
        End If

    End Sub

    Private Sub dgfunc_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.EditCommand

        dgfunc.EditItemIndex = e.Item.ItemIndex
        'Dim hold As String = CType(e.Item.FindControl("lblfunc"), Label).Text
        'lblfunchold.Value = hold
        func.Open()
        PopFunc()
        func.Dispose()

    End Sub



    Private Sub dgfunc_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.CancelCommand
        dgfunc.EditItemIndex = -1
        func.Open()
        PopFunc()
        func.Dispose()
    End Sub

    Private Sub dgfunc_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.DeleteCommand
        func.Open()
        Dim fuid As String
        fuid = CType(e.Item.Cells(4).Controls(1), Label).Text
        sql = "usp_delFunc '" & fuid & "'"
        func.Update(sql)
        sql = "update AdminTasks " _
                   + "set func = (func - 1) where cid = '" & cid & "'"
        func.Update(sql)
        dgfunc.EditItemIndex = -1
        PopFunc()
        func.Dispose()
    End Sub

    Private Sub dgfunc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgfunc.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                 e.Item.ItemType <> ListItemType.Footer Then
            Dim deleteButton As ImageButton = CType(e.Item.FindControl("ibdel"), ImageButton)

            'We can now add the onclick event handler
            deleteButton.Attributes("onclick") = "javascript:return " & _
            "confirm('Are you sure you want to delete Function " & _
            DataBinder.Eval(e.Item.DataItem, "func") & "\nWith " & _
            DataBinder.Eval(e.Item.DataItem, "comps") & " Component(s) and " & _
            DataBinder.Eval(e.Item.DataItem, "tasks") & " Task(s)?')"

        End If
    End Sub

    Private Sub dgfunc_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.UpdateCommand
        func.Open()

        eqid = lbleqid.Value
        Dim cid As String = "0"
        Dim user As String = lbluser.Value
        Dim mdate As DateTime
        Dim rte As String = CType(e.Item.FindControl("txtrouting"), TextBox).Text
        Dim orte As String = CType(e.Item.FindControl("lbloldrte"), Label).Text
        Dim rtechk As Long
        Try
            rtechk = System.Convert.ToInt32(rte)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr786" , "AddEditFuncPop.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try

        Dim fun, des, fid As String
        fid = CType(e.Item.FindControl("txtfuid"), Label).Text
        fun = CType(e.Item.FindControl("txtfunum"), TextBox).Text
        fun = func.ModString1(fun)

        If Len(fun) = 0 Then
            Dim strMessage As String =  tmod.getmsg("cdstr787" , "AddEditFuncPop.aspx.vb")
 
            func.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        ElseIf Len(fun) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr788" , "AddEditFuncPop.aspx.vb")
 
            func.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        Else
            Dim fcnt As Integer
            sql = "select count(*) from functions " _
            + "where eqid = '" & eqid & "' and func = '" & fun & "'"
            fcnt = func.Scalar(sql)

            If fcnt > 1 And fun <> "New Function" Then
                Dim strMessage As String =  tmod.getmsg("cdstr789" , "AddEditFuncPop.aspx.vb")
 
                func.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            Else
                Dim spl As String = CType(e.Item.FindControl("txtspl"), TextBox).Text
                spl = func.ModString1(spl)
                If Len(spl) > 100 Then
                    Dim strMessage As String =  tmod.getmsg("cdstr790" , "AddEditFuncPop.aspx.vb")
 
                    func.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End If
                mdate = func.CNOW
                sql = "update functions set " _
                + "compid = '" & cid & "', " _
                + "eqid = '" & eqid & "', " _
                + "func = '" & fun & "', " _
                + "spl = '" & spl & "', " _
                + "modifiedby = '" & user & "', " _
                + "modifieddate = '" & mdate & "' " _
                + "where func_id = '" & fid & "'"
                Try
                    func.Update(sql)
                Catch ex As Exception
                    Try
                        Dim funcadd As New Utilities
                        funcadd.Open()
                        funcadd.Update(sql)
                        funcadd.Dispose()
                    Catch ex1 As Exception
                        'Dim strMessage As String = tmod.getmsg("cdstr791", "AddEditFuncPop.aspx.vb")
                        Dim strMessage As String = "Problem Saving Record\n" & ex1.Message
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End Try
                    
                End Try
                dgfunc.EditItemIndex = -1
                If rte <> orte And rte <> "0" Then
                    sql = "usp_rerouteFunctions '" & eqid & "', '" & rte & "', '" & orte & "'"
                    Try
                        func.Update(sql)
                    Catch ex As Exception
                        Try
                            Dim funcadd1 As New Utilities
                            funcadd1.Open()
                            funcadd1.Update(sql)
                            funcadd1.Dispose()
                        Catch ex1 As Exception
                            Dim strMessage As String = "Problem Saving Record\n" & ex1.Message
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        End Try
                    End Try

                ElseIf rte = "0" Then
                    Dim strMessage As String =  tmod.getmsg("cdstr792" , "AddEditFuncPop.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
            End If

            PopFunc()
        End If
        func.Dispose()
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgfunc.Columns(0).HeaderText = dlabs.GetDGPage("AddEditFuncPop.aspx", "dgfunc", "0")
        Catch ex As Exception
        End Try
        Try
            dgfunc.Columns(1).HeaderText = dlabs.GetDGPage("AddEditFuncPop.aspx", "dgfunc", "1")
        Catch ex As Exception
        End Try
        Try
            dgfunc.Columns(2).HeaderText = dlabs.GetDGPage("AddEditFuncPop.aspx", "dgfunc", "2")
        Catch ex As Exception
        End Try
        Try
            dgfunc.Columns(3).HeaderText = dlabs.GetDGPage("AddEditFuncPop.aspx", "dgfunc", "3")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang1909.Text = axlabs.GetASPXPage("AddEditFuncPop.aspx", "lang1909")
        Catch ex As Exception
        End Try
        Try
            lang1910.Text = axlabs.GetASPXPage("AddEditFuncPop.aspx", "lang1910")
        Catch ex As Exception
        End Try
        Try
            lang1911.Text = axlabs.GetASPXPage("AddEditFuncPop.aspx", "lang1911")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibaddfunc.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                ibaddfunc.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                ibaddfunc.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                ibaddfunc.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                ibaddfunc.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
