

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class EQCopyMinitpm
    Inherits System.Web.UI.Page
	Protected WithEvents btnaddasset As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2271 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2270 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2269 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2268 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2267 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2266 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2265 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2264 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2263 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2262 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2261 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2260 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2259 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2258 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2257 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2256 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2255 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2254 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2253 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2252 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2251 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2250 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2249 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2248 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2247 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2246 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2245 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2244 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2243 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2242 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2241 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim cid, srch, sid, lid As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tblrevdetails As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcmail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdloc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoem As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtneweqnum As System.Web.UI.WebControls.TextBox
    Protected WithEvents cbloptions As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents ibtncopy As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tbleqrev As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents locdet As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllistr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim eqcopy As New Utilities
    Dim did, clid, psid, login, ro As String
    Protected WithEvents lblcopyflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdbt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tblneweq As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdneweq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewsql As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcbydate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcbyphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcbyemail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewcbymail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txteqdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqmod As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqoem As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqsn As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqecr As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ddlstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnecr As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Td8 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcureq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdac As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtnret As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ibtnsaveneweq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnewid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents treq As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater
    Protected WithEvents tbleq As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not Me.IsPostBack Then
            If lbllog.Value <> "no" Then
                Try
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro
                    If ro = "1" Then
                        ibtncopy.Enabled = False
                        ibtncopy.ImageUrl = "../images/appbuttons/bgbuttons/copydis.gif"
                        'imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    Else
                        'ibtncopy.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/copyhov.gif'")
                        'ibtncopy.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/copy.gif'")
                    End If
                    did = Request.QueryString("dept").ToString
                    lbldid.Value = did
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    If Len(did) <> 0 AndAlso did <> "" AndAlso did <> "0" Then
                        txtpg.Value = "1"
                        cid = HttpContext.Current.Session("comp").ToString
                        lblcid.Value = cid
                        psid = HttpContext.Current.Session("psite").ToString
                        lblpsid.Value = "12"
                        sid = Request.QueryString("sid").ToString
                        clid = Request.QueryString("cell").ToString
                        lblsid.Value = sid
                        lblclid.Value = clid
                        eqcopy.Open()
                        PopEq(PageNumber)
                        PopAC()
                        eqcopy.Dispose()
                        tbleqrev.Attributes.Add("class", "details")
                        tblrevdetails.Attributes.Add("class", "details")
                        tblneweq.Attributes.Add("class", "details")
                        cbloptions.Attributes.Add("onclick", "checkbox();")
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr902" , "EQCopyMinitpm.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "noeqid"
                    End If
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr903" , "EQCopyMinitpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End Try
            End If
            If Request.Form("lblret") = "next" Then
                eqcopy.Open()
                GetNext()
                eqcopy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                eqcopy.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopEq(PageNumber)
                eqcopy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                eqcopy.Open()
                GetPrev()
                eqcopy.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                eqcopy.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopEq(PageNumber)
                eqcopy.Dispose()
                lblret.Value = ""
            End If
        End If
        'btnprev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'btnprev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'btnnext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'btnnext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")

        'ibtnsaveneweq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ibtnsaveneweq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
        'ibtncopy.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/copyhov.gif'")
        'ibtncopy.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/copy.gif'")
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'ibtnret.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ibtnret.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
    End Sub
    Private Sub PopAC()
        cid = lblcid.Value
        sql = "select * from pmAssetClass where compid = '" & cid & "'"
        dr = eqcopy.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        ddac.DataBind()
        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopEq(PageNumber)
        Catch ex As Exception
            eqcopy.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr904" , "EQCopyMinitpm.aspx.vb")
 
            eqcopy.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopEq(PageNumber)
        Catch ex As Exception
            eqcopy.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr905" , "EQCopyMinitpm.aspx.vb")
 
            eqcopy.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopEq(ByVal PageNumber As Integer)
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        'cid = "0"
        cid = lblcid.Value
        sid = lblsid.Value
        psid = lblpsid.Value
        Dim intPgCnt As Integer
        If Len(srch) = 0 Then
            srch = ""
            sql = "select count(*) from Equipment where compid = '" & cid & "' and siteid = '" & sid & "'"
            intPgCnt = eqcopy.Scalar(sql)
        Else
            Dim csrch As String
            csrch = "%" & srch & "%"
            sql = "select count(*) from Equipment where compid = '" & cid & "' and siteid = '" & sid & "' and (eqnum like '" & csrch & "' or eqdesc like '" & csrch & "' or spl like '" & csrch & "')"
            intPgCnt = eqcopy.Scalar(sql)
        End If
        'txtpg.Value = PageNumber
        PageNumber = txtpg.Value
        'intPgNav = eqcopy.PageCount(intPgCnt, PageSize)
        intPgNav = eqcopy.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        If Len(srch) = 0 Then
            srch = ""
        End If

        sql = "usp_getAllEqPg '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '%%', '" & sid & "'"
        dr = eqcopy.GetRdrData(sql)
        rptreq.DataSource = dr
        rptreq.DataBind()
        dr.Close()
        updatepos(intPgNav)
    End Sub
    Private Sub updatepos(ByVal intPgNav)
        PageNumber = txtpg.Value

        If intPgNav = 0 Then
            tdpg.InnerHtml = "Page 0 of 0"
        Else
            tdpg.InnerHtml = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber = 1 And intPgNav = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = False

        ElseIf PageNumber = 1 And intPgNav > 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = True

        ElseIf PageNumber = intPgNav Then
            btnnext.Enabled = False
            btnprev.Enabled = True

        Else
            btnnext.Enabled = True
            btnprev.Enabled = True
        End If
    End Sub
    Public Sub GetEq1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rb As RadioButton = New RadioButton
        Dim eqid, eqdesc, spl, loc, model, serial, oem As String
        Dim ecr, cby, mby, cdat, mdat, cph, cm, eph, em, bt As String
        rb = sender
        tdcureq.InnerHtml = rb.ClientID
        For Each i As RepeaterItem In rptreq.Items
            rb = CType(i.FindControl("rbeq"), RadioButton)
            eqid = CType(i.FindControl("lbleqid"), Label).Text
            eqdesc = CType(i.FindControl("lbleqdesc"), Label).Text
            spl = CType(i.FindControl("lblspl"), Label).Text
            loc = CType(i.FindControl("lblloc"), Label).Text
            model = CType(i.FindControl("lblmod"), Label).Text
            oem = CType(i.FindControl("lbloem"), Label).Text
            serial = CType(i.FindControl("lblser"), Label).Text
            ecr = CType(i.FindControl("lblecr"), Label).Text
            cby = CType(i.FindControl("lblcby"), Label).Text
            mby = CType(i.FindControl("lblmby"), Label).Text
            cdat = CType(i.FindControl("lblcdate"), Label).Text
            mdat = CType(i.FindControl("lblmdate"), Label).Text
            cph = CType(i.FindControl("lblcph"), Label).Text
            cm = CType(i.FindControl("lblcm"), Label).Text
            eph = CType(i.FindControl("lbleph"), Label).Text
            em = CType(i.FindControl("lblem"), Label).Text
            bt = CType(i.FindControl("lblbt"), Label).Text
            rb.Checked = False
            If tdcureq.InnerHtml = rb.ClientID Then
                tbleqrev.Attributes.Add("class", "view")
                tblrevdetails.Attributes.Add("class", "view")
                rb.Checked = True
                lbleq.Value = eqid
                tdcureq.InnerHtml = rb.Text.Trim
                tddesc.InnerHtml = eqdesc
                tdbt.InnerHtml = bt
                tdspl.InnerHtml = spl
                tdloc.InnerHtml = loc
                tdmod.InnerHtml = model
                tdoem.InnerHtml = oem
                tdecr.InnerHtml = ecr
                tdcby.InnerHtml = cby
                tdcdate.InnerHtml = cdat
                tdcphone.InnerHtml = cph
                tdcmail.InnerHtml = cm
                tdsn.InnerHtml = serial

            End If
        Next
    End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        eqcopy.Open()
        PopEq(PageNumber)
        eqcopy.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprev.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        eqcopy.Open()
        PopEq(PageNumber)
        eqcopy.Dispose()
    End Sub

    Private Sub ibtncopy_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtncopy.Click
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr906" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            lbllog.Value = "no"
            Exit Sub
        End Try

        Dim neweq As String = txtneweqnum.Text
        neweq = Replace(neweq, "'", Chr(180), , , vbTextCompare)
        neweq = Replace(neweq, "--", "-", , , vbTextCompare)
        neweq = Replace(neweq, ";", ":", , , vbTextCompare)
        If Len(neweq) > 0 Then
            If Len(neweq) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr907" , "EQCopyMinitpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            cid = lblcid.Value
            Dim eqcnt As Integer
            sql = "select count(*) from equipment where compid = '" & cid & "' and eqnum = '" & neweq & "'"
            eqcopy.Open()
            eqcnt = eqcopy.Scalar(sql)

            If eqcnt = 0 Then
                DoCopy()
                eqcopy.Dispose()
            Else
                eqcopy.Dispose()
                Dim strMessage As String =  tmod.getmsg("cdstr908" , "EQCopyMinitpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Else
            eqcopy.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr909" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Function DoCopy()
        Dim li As ListItem
        Dim listr As String
        For Each li In cbloptions.Items
            If li.Selected = True Then
                listr += li.Value.ToString
            End If
        Next
        lbllistr.Value = listr
        Dim eqid As String = lbleq.Value
        Dim neweq As String = txtneweqnum.Text
        cid = lblcid.Value
        Dim site, dept, cell As String
        lid = lbllid.Value
        site = lblsid.Value
        dept = lbldid.Value
        cell = lblclid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        Dim newid As Integer
        Dim user As String = HttpContext.Current.Session("username").ToString '"PM Administrator"
        Session("username") = user
        If cell = "none" Then
            cell = "0"
        End If
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
        Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        sql = "usp_copyEq '" & cid & "', '" & site & "', '" & dept & "', '" & cell & "', '" & eqid & "', '" & neweq & "', '" & ustr & "', '" & lid & "'"
        newid = eqcopy.Scalar(sql)
        sql = "usp_copyEqPics2 '" & eqid & "', '" & newid & "'"
        Try
            Dim ds As New DataSet
            ds = eqcopy.GetDSData(sql)
            Dim i As Integer
            Dim x As Integer = ds.Tables(0).Rows.Count
            For i = 0 To (x - 1)
                pid = ds.Tables(0).Rows(i)("pic_id").ToString
                t = ds.Tables(0).Rows(i)("picurl").ToString
                tn = ds.Tables(0).Rows(i)("picurltn").ToString
                tm = ds.Tables(0).Rows(i)("picurltm").ToString
                t = Replace(t, "..", "")
                tn = Replace(tn, "..", "")
                tm = Replace(tm, "..", "")

                nt = "a-eqImg" & newid & "i"
                ntn = "atn-eqImg" & newid & "i"
                ntm = "atm-eqImg" & newid & "i"

                ont = "a-eqImg" & eqid & "i"
                ontn = "atn-eqImg" & eqid & "i"
                ontm = "atm-eqImg" & eqid & "i"


                ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                ts = Replace(ts, "\", "/")
                td = Server.MapPath("\") & ap & t
                td = Replace(td, "\", "/")

                tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                tns = Replace(tns, "\", "/")
                tnd = Server.MapPath("\") & ap & tn
                tnd = Replace(tnd, "\", "/")

                tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                tms = Replace(tms, "\", "/")
                tmd = Server.MapPath("\") & ap & tm
                tmd = Replace(tmd, "\", "/")

                System.IO.File.Copy(ts, td, True)
                System.IO.File.Copy(tns, tnd, True)
                System.IO.File.Copy(tms, tmd, True)
            Next
        Catch ex As Exception

        End Try
        Dim flg As String
        lblnewid.Value = newid
        If listr = "fct" Then
            flg = "1"
            sql = "usp_copyAll '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "'"
            'eqcopy.Update(sql)
            Try
                Dim ds As New DataSet
                ds = eqcopy.GetDSData(sql)
                Dim fid, cmid, ofid, ocmid As String
                Dim i As Integer
                Dim f As Integer = 0
                Dim c As Integer = 0
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    fid = ds.Tables(0).Rows(i)("funcid").ToString
                    ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")
                    If cmid = "" Then
                        f = f + 1
                        nt = "b-eqImg" & fid & "i"
                        ntn = "btn-eqImg" & fid & "i"
                        ntm = "btm-eqImg" & fid & "i"

                        ont = "b-eqImg" & ofid & "i"
                        ontn = "btn-eqImg" & ofid & "i"
                        ontm = "btm-eqImg" & ofid & "i"
                    Else
                        c = c + 1
                        nt = "c-eqImg" & cmid & "i"
                        ntn = "ctn-eqImg" & cmid & "i"
                        ntm = "ctm-eqImg" & cmid & "i"

                        ont = "c-eqImg" & ocmid & "i"
                        ontn = "ctn-eqImg" & ocmid & "i"
                        ontm = "ctm-eqImg" & ocmid & "i"
                    End If

                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & ap & t
                    td = Replace(td, "\", "/")
                    System.IO.File.Copy(ts, td, True)

                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                    tn = Replace(tn, "\", "/")
                    tnd = Server.MapPath("\") & ap & tn
                    tnd = Replace(tnd, "\", "/")
                    System.IO.File.Copy(tns, tnd, True)

                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & ap & tm
                    tmd = Replace(tmd, "\", "/")
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception

            End Try
            sql = "usp_copyProcs '" & eqid & "', '" & newid & "'"
            eqcopy.Update(sql)
            sql = "usp_copyEqTasks1 '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & newid & "', '" & ustr & "','Y'"
            eqcopy.Update(sql)
            sql = "select pic_id, parfuncid, funcid, tpm_image, tpm_image_med, tpm_image_thumb from tpmimages where funcid in " _
                + "(select func_id from functions where eqid = '" & newid & "')"
            Try
                Dim ds As New DataSet
                ds = eqcopy.GetDSData(sql)
                Dim fid, pfid As String
                Dim i As Integer
                Dim f As Integer = 0
                Dim c As Integer = 0
                Dim ot, otn, otm As String
                Dim pict, pictn, pictm As String
                Dim opict, opictn, opictm As String
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    fid = ds.Tables(0).Rows(i)("funcid").ToString
                    pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                    t = ds.Tables(0).Rows(i)("tpm_image").ToString
                    tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                    tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                    Dim intFileNameLength As Integer
                    intFileNameLength = InStr(1, StrReverse(t), "/")
                    t = Mid(t, (Len(t) - intFileNameLength) + 2)
                    intFileNameLength = InStr(1, StrReverse(tn), "/")
                    tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                    intFileNameLength = InStr(1, StrReverse(tm), "/")
                    tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                    Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                    t = appstr & "/tpmimages/" & t
                    tn = appstr & "/tpmimages/" & tn
                    tm = appstr & "/tpmimages/" & tm
                    t = Replace(t, "http//", "")
                    tn = Replace(tn, "http//", "")
                    tm = Replace(tm, "http//", "")

                    pict = "a-newsImg" + fid
                    pictn = "atn-newsImg" + fid
                    pictm = "atm-newsImg" + fid

                    opict = "a-newsImg" + pfid
                    opictn = "atn-newsImg" + pfid
                    opictm = "atm-newsImg" + pfid

                    ts = Replace(t, pict, opict)
                    tns = Replace(tn, pictn, opictn)
                    tms = Replace(tm, pictm, opictm)

                    ts = Server.MapPath("\") & ts
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & t
                    td = Replace(td, "\", "/")
                    System.IO.File.Copy(ts, td, True)

                    tns = Server.MapPath("\") & tns
                    tns = Replace(tns, "\", "/")
                    tnd = Server.MapPath("\") & tn
                    tnd = Replace(tnd, "\", "/")
                    System.IO.File.Copy(tns, tnd, True)

                    tms = Server.MapPath("\") & tms
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & tm
                    tmd = Replace(tmd, "\", "/")
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception

            End Try
            'PopNewEq(newid)
        ElseIf listr = "fc" Then
            flg = "1"
            sql = "usp_copyAll '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "'"
            'eqcopy.Update(sql)
            Try
                Dim ds As New DataSet
                ds = eqcopy.GetDSData(sql)
                Dim fid, cmid, ofid, ocmid As String
                Dim i As Integer
                Dim f As Integer = 0
                Dim c As Integer = 0
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    fid = ds.Tables(0).Rows(i)("funcid").ToString
                    ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")
                    If cmid = "" Then
                        f = f + 1
                        nt = "b-eqImg" & fid & "i"
                        ntn = "btn-eqImg" & fid & "i"
                        ntm = "btm-eqImg" & fid & "i"

                        ont = "b-eqImg" & ofid & "i"
                        ontn = "btn-eqImg" & ofid & "i"
                        ontm = "btm-eqImg" & ofid & "i"
                    Else
                        c = c + 1
                        nt = "c-eqImg" & cmid & "i"
                        ntn = "ctn-eqImg" & cmid & "i"
                        ntm = "ctm-eqImg" & cmid & "i"

                        ont = "c-eqImg" & ocmid & "i"
                        ontn = "ctn-eqImg" & ocmid & "i"
                        ontm = "ctm-eqImg" & ocmid & "i"
                    End If

                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & ap & t
                    td = Replace(td, "\", "/")
                    System.IO.File.Copy(ts, td, True)

                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                    tn = Replace(tn, "\", "/")
                    tnd = Server.MapPath("\") & ap & tn
                    tnd = Replace(tnd, "\", "/")
                    System.IO.File.Copy(tns, tnd, True)

                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & ap & tm
                    tmd = Replace(tmd, "\", "/")
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception

            End Try
            'PopNewEq(newid)
        ElseIf listr = "f" Then
            flg = "0"
            sql = "usp_copyAll '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "'"
            'eqcopy.Update(sql)
            Try
                Dim ds As New DataSet
                ds = eqcopy.GetDSData(sql)
                Dim fid, cmid, ofid, ocmid As String
                Dim i As Integer
                Dim f As Integer = 0
                Dim c As Integer = 0
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    fid = ds.Tables(0).Rows(i)("funcid").ToString
                    ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                    cmid = ds.Tables(0).Rows(i)("comid").ToString
                    ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")
                    If cmid = "" Then
                        f = f + 1
                        nt = "b-eqImg" & fid & "i"
                        ntn = "btn-eqImg" & fid & "i"
                        ntm = "btm-eqImg" & fid & "i"

                        ont = "b-eqImg" & ofid & "i"
                        ontn = "btn-eqImg" & ofid & "i"
                        ontm = "btm-eqImg" & ofid & "i"
                    Else
                        c = c + 1
                        nt = "c-eqImg" & cmid & "i"
                        ntn = "ctn-eqImg" & cmid & "i"
                        ntm = "ctm-eqImg" & cmid & "i"

                        ont = "c-eqImg" & ocmid & "i"
                        ontn = "ctn-eqImg" & ocmid & "i"
                        ontm = "ctm-eqImg" & ocmid & "i"
                    End If

                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & ap & t
                    td = Replace(td, "\", "/")
                    System.IO.File.Copy(ts, td, True)

                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                    tn = Replace(tn, "\", "/")
                    tnd = Server.MapPath("\") & ap & tn
                    tnd = Replace(tnd, "\", "/")
                    System.IO.File.Copy(tns, tnd, True)

                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & ap & tm
                    tmd = Replace(tmd, "\", "/")
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception

            End Try
            'PopNewEq(newid)
        Else
            'PopNewEq(newid)
        End If
        Try
            PopNewEq(newid)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr910" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        rptreq.DataSource = Nothing
        rptreq.DataBind()
        txtpg.Value = "1"
        lblcopyflg.Value = flg
        txtneweqnum.Text = ""
        tbltop.Attributes.Add("class", "details")
        tbleqrev.Attributes.Add("class", "details")
        tblrevdetails.Attributes.Add("class", "details")
        tblneweq.Attributes.Add("class", "view")

    End Function
    Private Sub PopNewEq(ByVal newid As Integer)
        LoadStatus()
        sql = "usp_getNewEqRecord '" & newid & "'"
        dr = eqcopy.GetRdrData(sql)
        While dr.Read
            tdneweq.InnerHtml = dr.Item("eqnum").ToString
            txteqdesc.Text = dr.Item("eqdesc").ToString
            txteqspl.Text = dr.Item("spl").ToString
            txteqloc.Text = dr.Item("location").ToString
            txteqmod.Text = dr.Item("model").ToString
            txteqsn.Text = dr.Item("serial").ToString
            txteqoem.Text = dr.Item("oem").ToString
            lblnewid.Value = dr.Item("eqid").ToString
            txteqecr.Text = dr.Item("ecr").ToString
            tdnewcby.InnerHtml = dr.Item("createdby").ToString
            tdnewcbydate.InnerHtml = dr.Item("createdate").ToString
            tdnewcbyphone.InnerHtml = dr.Item("cph").ToString
            tdnewcbymail.InnerHtml = dr.Item("cm").ToString
            Try
                ddlstat.SelectedValue = dr.Item("statid").ToString
            Catch ex As Exception
                Try
                    ddlstat.SelectedIndex = 0
                Catch ex1 As Exception

                End Try

            End Try

        End While
        dr.Close()

    End Sub
    Private Sub LoadStatus()
        cid = lblcid.Value
        sql = "select * from eqstatus where compid = '" & cid & "'"
        dr = eqcopy.GetRdrData(sql)
        ddlstat.DataSource = dr
        ddlstat.DataTextField = "status"
        ddlstat.DataValueField = "statid"
        ddlstat.DataBind()
        dr.Close()
        ddlstat.Items.Insert(0, "Select Status")
    End Sub
    Private Sub ibtnsaveneweq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsaveneweq.Click
        Dim eqd, eqs, eql, eqm, eqsn, eqo, eqi, eqe, estat As String
        eqd = txteqdesc.Text
        eqd = Replace(eqd, "'", Chr(180), , , vbTextCompare)
        eqd = Replace(eqd, "--", "-", , , vbTextCompare)
        eqd = Replace(eqd, ";", ":", , , vbTextCompare)
        If Len(eqd) = 0 Then eqd = ""
        If Len(eqd) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr911" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqs = txteqspl.Text
        eqs = Replace(eqs, "'", Chr(180), , , vbTextCompare)
        eqs = Replace(eqs, "--", "-", , , vbTextCompare)
        eqs = Replace(eqs, ";", ":", , , vbTextCompare)
        If Len(eqs) = 0 Then eqs = ""
        If Len(eqs) > 250 Then
            Dim strMessage As String =  tmod.getmsg("cdstr912" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eql = txteqloc.Text
        eql = Replace(eql, "'", Chr(180), , , vbTextCompare)
        eql = Replace(eql, "--", "-", , , vbTextCompare)
        eql = Replace(eql, ";", ":", , , vbTextCompare)
        If Len(eql) = 0 Then eql = ""
        If Len(eql) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr913" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqm = txteqmod.Text
        eqm = Replace(eqm, "'", Chr(180), , , vbTextCompare)
        eqm = Replace(eqm, "--", "-", , , vbTextCompare)
        eqm = Replace(eqm, ";", ":", , , vbTextCompare)
        If Len(eqm) = 0 Then eqm = ""
        If Len(eqm) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr914" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqsn = txteqsn.Text
        eqsn = Replace(eqsn, "'", Chr(180), , , vbTextCompare)
        eqsn = Replace(eqsn, "--", "-", , , vbTextCompare)
        eqsn = Replace(eqsn, ";", ":", , , vbTextCompare)
        If Len(eqsn) = 0 Then eqsn = ""
        If Len(eqsn) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr915" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqo = txteqoem.Text
        eqo = Replace(eqo, "'", Chr(180), , , vbTextCompare)
        eqo = Replace(eqo, "'", Chr(180), , , vbTextCompare)
        eqo = Replace(eqo, "--", "-", , , vbTextCompare)
        eqo = Replace(eqo, ";", ":", , , vbTextCompare)
        If Len(eqo) = 0 Then eqo = ""
        If Len(eqo) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr916" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eqi = lblnewid.Value
        eqe = txteqecr.Text
        Dim eqechk As Long
        Try
            eqechk = System.Convert.ToInt32(eqe)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr917" , "EQCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        If ddlstat.SelectedIndex <> 0 Then
            estat = ddlstat.SelectedValue.ToString
        Else
            estat = "0"
        End If

        eqcopy.Open()
        sql = "usp_saveNewEq '" & eqd & "', '" & eqs & "', '" & eql & "', '" & eqm & "', " _
        + "'" & eqsn & "', '" & eqo & "', '" & eqi & "', '" & eqe & "', '" & estat & "', 0"
        'Try
        eqcopy.Update(sql)
        'Catch ex As Exception
        'Dim strMessage As String =  tmod.getmsg("cdstr918" , "EQCopyMinitpm.aspx.vb")
 
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'End Try
        txtpg.Value = "1"
        PageNumber = "1"
        PopEq(PageNumber)
        eqcopy.Dispose()
        tbltop.Attributes.Add("class", "view")
        'tbleqrev.Attributes.Add("class", "view")
        'tblrevdetails.Attributes.Add("class", "view")
        tblneweq.Attributes.Add("class", "details")
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        tbleqrev.Attributes.Add("class", "details")
        tblrevdetails.Attributes.Add("class", "details")
        tblneweq.Attributes.Add("class", "details")

        txtpg.Value = "1"
        PageNumber = txtpg.Value
        eqcopy.Open()
        PopEq(PageNumber)
        eqcopy.Dispose()
    End Sub

    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                               e.Item.ItemType <> ListItemType.Footer Then
            Dim pic As HtmlImage = CType(e.Item.FindControl("imgpic"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim cnt As String = DataBinder.Eval(e.Item.DataItem, "epcnt").ToString
            If cnt <> "0" Then
                pic.Attributes.Add("onclick", "geteqport('" & id & "');")
                pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpic.gif")
            Else
                pic.Attributes.Add("onclick", "")
                pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpicdis.gif")
            End If
        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang2241 As Label
			lang2241 = CType(e.Item.FindControl("lang2241"), Label)
			lang2241.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2241")
		Catch ex As Exception
		End Try
		Try
                Dim lang2242 As Label
			lang2242 = CType(e.Item.FindControl("lang2242"), Label)
			lang2242.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2242")
		Catch ex As Exception
		End Try
		Try
                Dim lang2243 As Label
			lang2243 = CType(e.Item.FindControl("lang2243"), Label)
			lang2243.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2243")
		Catch ex As Exception
		End Try
		Try
                Dim lang2244 As Label
			lang2244 = CType(e.Item.FindControl("lang2244"), Label)
			lang2244.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2244")
		Catch ex As Exception
		End Try
		Try
                Dim lang2245 As Label
			lang2245 = CType(e.Item.FindControl("lang2245"), Label)
			lang2245.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2245")
		Catch ex As Exception
		End Try
		Try
                Dim lang2246 As Label
			lang2246 = CType(e.Item.FindControl("lang2246"), Label)
			lang2246.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2246")
		Catch ex As Exception
		End Try
		Try
                Dim lang2247 As Label
			lang2247 = CType(e.Item.FindControl("lang2247"), Label)
			lang2247.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2247")
		Catch ex As Exception
		End Try
		Try
                Dim lang2248 As Label
			lang2248 = CType(e.Item.FindControl("lang2248"), Label)
			lang2248.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2248")
		Catch ex As Exception
		End Try
		Try
                Dim lang2249 As Label
			lang2249 = CType(e.Item.FindControl("lang2249"), Label)
			lang2249.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2249")
		Catch ex As Exception
		End Try
		Try
                Dim lang2250 As Label
			lang2250 = CType(e.Item.FindControl("lang2250"), Label)
			lang2250.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2250")
		Catch ex As Exception
		End Try
		Try
                Dim lang2251 As Label
			lang2251 = CType(e.Item.FindControl("lang2251"), Label)
			lang2251.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2251")
		Catch ex As Exception
		End Try
		Try
                Dim lang2252 As Label
			lang2252 = CType(e.Item.FindControl("lang2252"), Label)
			lang2252.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2252")
		Catch ex As Exception
		End Try
		Try
                Dim lang2253 As Label
			lang2253 = CType(e.Item.FindControl("lang2253"), Label)
			lang2253.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2253")
		Catch ex As Exception
		End Try
		Try
                Dim lang2254 As Label
			lang2254 = CType(e.Item.FindControl("lang2254"), Label)
			lang2254.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2254")
		Catch ex As Exception
		End Try
		Try
                Dim lang2255 As Label
			lang2255 = CType(e.Item.FindControl("lang2255"), Label)
			lang2255.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2255")
		Catch ex As Exception
		End Try
		Try
                Dim lang2256 As Label
			lang2256 = CType(e.Item.FindControl("lang2256"), Label)
			lang2256.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2256")
		Catch ex As Exception
		End Try
		Try
                Dim lang2257 As Label
			lang2257 = CType(e.Item.FindControl("lang2257"), Label)
			lang2257.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2257")
		Catch ex As Exception
		End Try
		Try
                Dim lang2258 As Label
			lang2258 = CType(e.Item.FindControl("lang2258"), Label)
			lang2258.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2258")
		Catch ex As Exception
		End Try
		Try
                Dim lang2259 As Label
			lang2259 = CType(e.Item.FindControl("lang2259"), Label)
			lang2259.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2259")
		Catch ex As Exception
		End Try
		Try
                Dim lang2260 As Label
			lang2260 = CType(e.Item.FindControl("lang2260"), Label)
			lang2260.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2260")
		Catch ex As Exception
		End Try
		Try
                Dim lang2261 As Label
			lang2261 = CType(e.Item.FindControl("lang2261"), Label)
			lang2261.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2261")
		Catch ex As Exception
		End Try
		Try
                Dim lang2262 As Label
			lang2262 = CType(e.Item.FindControl("lang2262"), Label)
			lang2262.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2262")
		Catch ex As Exception
		End Try
		Try
                Dim lang2263 As Label
			lang2263 = CType(e.Item.FindControl("lang2263"), Label)
			lang2263.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2263")
		Catch ex As Exception
		End Try
		Try
                Dim lang2264 As Label
			lang2264 = CType(e.Item.FindControl("lang2264"), Label)
			lang2264.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2264")
		Catch ex As Exception
		End Try
		Try
                Dim lang2265 As Label
			lang2265 = CType(e.Item.FindControl("lang2265"), Label)
			lang2265.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2265")
		Catch ex As Exception
		End Try
		Try
                Dim lang2266 As Label
			lang2266 = CType(e.Item.FindControl("lang2266"), Label)
			lang2266.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2266")
		Catch ex As Exception
		End Try
		Try
                Dim lang2267 As Label
			lang2267 = CType(e.Item.FindControl("lang2267"), Label)
			lang2267.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2267")
		Catch ex As Exception
		End Try
		Try
                Dim lang2268 As Label
			lang2268 = CType(e.Item.FindControl("lang2268"), Label)
			lang2268.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2268")
		Catch ex As Exception
		End Try
		Try
                Dim lang2269 As Label
			lang2269 = CType(e.Item.FindControl("lang2269"), Label)
			lang2269.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2269")
		Catch ex As Exception
		End Try
		Try
                Dim lang2270 As Label
			lang2270 = CType(e.Item.FindControl("lang2270"), Label)
			lang2270.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2270")
		Catch ex As Exception
		End Try
		Try
                Dim lang2271 As Label
			lang2271 = CType(e.Item.FindControl("lang2271"), Label)
			lang2271.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx","lang2271")
		Catch ex As Exception
		End Try

End If

 End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2241.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2241")
        Catch ex As Exception
        End Try
        Try
            lang2242.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2242")
        Catch ex As Exception
        End Try
        Try
            lang2243.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2243")
        Catch ex As Exception
        End Try
        Try
            lang2244.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2244")
        Catch ex As Exception
        End Try
        Try
            lang2245.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2245")
        Catch ex As Exception
        End Try
        Try
            lang2246.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2246")
        Catch ex As Exception
        End Try
        Try
            lang2247.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2247")
        Catch ex As Exception
        End Try
        Try
            lang2248.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2248")
        Catch ex As Exception
        End Try
        Try
            lang2249.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2249")
        Catch ex As Exception
        End Try
        Try
            lang2250.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2250")
        Catch ex As Exception
        End Try
        Try
            lang2251.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2251")
        Catch ex As Exception
        End Try
        Try
            lang2252.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2252")
        Catch ex As Exception
        End Try
        Try
            lang2253.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2253")
        Catch ex As Exception
        End Try
        Try
            lang2254.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2254")
        Catch ex As Exception
        End Try
        Try
            lang2255.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2255")
        Catch ex As Exception
        End Try
        Try
            lang2256.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2256")
        Catch ex As Exception
        End Try
        Try
            lang2257.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2257")
        Catch ex As Exception
        End Try
        Try
            lang2258.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2258")
        Catch ex As Exception
        End Try
        Try
            lang2259.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2259")
        Catch ex As Exception
        End Try
        Try
            lang2260.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2260")
        Catch ex As Exception
        End Try
        Try
            lang2261.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2261")
        Catch ex As Exception
        End Try
        Try
            lang2262.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2262")
        Catch ex As Exception
        End Try
        Try
            lang2263.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2263")
        Catch ex As Exception
        End Try
        Try
            lang2264.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2264")
        Catch ex As Exception
        End Try
        Try
            lang2265.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2265")
        Catch ex As Exception
        End Try
        Try
            lang2266.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2266")
        Catch ex As Exception
        End Try
        Try
            lang2267.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2267")
        Catch ex As Exception
        End Try
        Try
            lang2268.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2268")
        Catch ex As Exception
        End Try
        Try
            lang2269.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2269")
        Catch ex As Exception
        End Try
        Try
            lang2270.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2270")
        Catch ex As Exception
        End Try
        Try
            lang2271.Text = axlabs.GetASPXPage("EQCopyMinitpm.aspx", "lang2271")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtncopy.Attributes.Add("src", "../images2/eng/bgbuttons/copy.gif")
            ElseIf lang = "fre" Then
                ibtncopy.Attributes.Add("src", "../images2/fre/bgbuttons/copy.gif")
            ElseIf lang = "ger" Then
                ibtncopy.Attributes.Add("src", "../images2/ger/bgbuttons/copy.gif")
            ElseIf lang = "ita" Then
                ibtncopy.Attributes.Add("src", "../images2/ita/bgbuttons/copy.gif")
            ElseIf lang = "spa" Then
                ibtncopy.Attributes.Add("src", "../images2/spa/bgbuttons/copy.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnret.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnret.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnret.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnret.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnret.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                ibtnsaveneweq.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddasset.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("EQCopyMinitpm.aspx", "btnaddasset") & "')")
            btnaddasset.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
