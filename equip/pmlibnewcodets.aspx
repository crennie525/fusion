<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibnewcodets.aspx.vb" Inherits="lucy_r12.pmlibnewcodets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibnewcodets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  src="../scripts1/pmlibnewcodetsaspx_a.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     
	</HEAD>
	<body  onload="setref();">
		<form id="form1" method="post" runat="server">
			<table width="980" style="POSITION: absolute; TOP: 0px; LEFT: 0px">
				<TBODY>
					<tr>
						<td class="labelibl"><asp:Label id="lang2459" runat="server">Select a Component to view Component Image and Failure Mode Details</asp:Label></td>
					</tr>
					<tr>
						<td vAlign="top"><asp:repeater id="rptrcomprev" runat="server">
								<HeaderTemplate>
									<table>
										<tr>
											<td class="btmmenu plainlabel" align="center"><img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16"></td>
											<td class="btmmenu plainlabel" width="50px"><asp:Label id="lang2460" runat="server">Edit</asp:Label></td>
											<td class="btmmenu plainlabel" style="width: 150px"><asp:Label id="lang2461" runat="server">Component</asp:Label></td>
											<td class="btmmenu plainlabel" width="200px"><asp:Label id="lang2462" runat="server">Description</asp:Label></td>
											<td class="btmmenu plainlabel" width="200px"><asp:Label id="lang2463" runat="server">Special Identifier</asp:Label></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr id="coselrow" runat="server" bgcolor='<%# HighlightRowCP(DataBinder.Eval(Container.DataItem, "comid"))%>'>
										<td class="plainlabel">
											<asp:CheckBox id="cb2" runat="server"></asp:CheckBox></td>
										<td><img src="../images/appbuttons/minibuttons/lilpentrans.gif" id="imgcoedit" runat="server"></td>
										<td class="plainlabel">
											<asp:LinkButton CommandName="Select" ID="Linkbutton3" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server>
											</asp:LinkButton></td>
										<td class="plainlabel">
											<asp:Label ID="Label10" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>' Runat = server>
											</asp:Label></td>
										<td class="plainlabel">
											<asp:Label ID="Label25" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblcomprevid" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblpurl3" Text='<%# DataBinder.Eval(Container.DataItem,"picurl")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblpcnt3" Text='<%# DataBinder.Eval(Container.DataItem,"piccnt")%>' Runat = server>
											</asp:Label></td>
										<td class="details">
											<asp:Label ID="lblparco1" Text='<%# DataBinder.Eval(Container.DataItem,"origparent")%>' Runat = server>
											</asp:Label></td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></TD>
			<td vAlign="top" align="center" width="200"><asp:repeater id="rptrfailrev" runat="server">
					<HeaderTemplate>
						<table>
							<tr>
								<td class="btmmenu plainlabel" width="200px"><asp:Label id="lang2464" runat="server">Failure Modes</asp:Label></td>
							</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr class="tbg">
							<td class="plainlabel">
								<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"failuremode")%>' Runat = server ID="Label8">
								</asp:Label></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:repeater></td>
			<td vAlign="top" align="center" width="230">
				<table>
					<tr>
						<td vAlign="top" align="center" colSpan="2" height="40"><IMG id="imgco" onclick="getbig();" height="206" src="../images/appimages/compimg.gif"
								style="width: 216px" runat="server"></td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<table>
								<tr>
									<td class="bluelabel" style="width: 80px"><asp:Label id="lang2465" runat="server">Order</asp:Label></td>
									<td style="width: 60px"><asp:textbox id="txtiorder" runat="server" style="width: 40px" CssClass="plainlabel"></asp:textbox></td>
									<td style="width: 20px"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
									<td style="width: 20px"><IMG onmouseover="return overlib('Add\Edit Images for this block', ABOVE, LEFT)" onclick="addpic();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/addnew.gif"></td>
									<td style="width: 20px"><IMG id="imgdel" onmouseover="return overlib('Delete This Image', ABOVE, LEFT)" onclick="delimg();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/del.gif" runat="server"></td>
									<td style="width: 20px"><IMG id="imgsavdet" onmouseover="return overlib('Save Image Order', ABOVE, LEFT)" onclick="savdets();"
											onmouseout="return nd()" src="../images/appbuttons/minibuttons/saveDisk1.gif" runat="server">
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirstc" onclick="getpfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprevc" onclick="getpprev();" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 140px;" vAlign="middle" align="center"><asp:label id="lblpgc" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inextc" onclick="getpnext();" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ilastc" onclick="getplast();" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			</TR>
			<tr>
				<td><asp:imagebutton id="ibtnremcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"></asp:imagebutton><IMG id="btnaddcomp" onmouseover="return overlib('Add a New Component Record')" onclick="GetCompDiv();"
						onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif" style="width: 20px" runat="server"><IMG id="btncopycomp" onmouseover="return overlib('Lookup Component Records to Copy')"
						onclick="GetCompCopy();" onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif" style="width: 20px" runat="server">
					<IMG id="ggrid" onmouseover="return overlib('Edit in Grid View')" onclick="GetGrid();"
						onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/grid1.gif"
						style="width: 20px" runat="server">
				</td>
			</tr>
			<tr class="details" id="tdfailrev" runat="server">
				<td colSpan="3">&nbsp;</td>
			</tr>
			</TBODY></TABLE></TD></TR> <input type="hidden" id="lblcoid" runat="server" NAME="lblcoid">
			<input type="hidden" id="lblfuid" runat="server" NAME="lblfuid"> <input type="hidden" id="lbldb" runat="server" NAME="lbldb">
			<input id="lblpcntc" type="hidden" runat="server" NAME="lblpcntc"> <input id="lblcurrpc" type="hidden" runat="server" NAME="lblcurrpc">
			<input id="lblimgsc" type="hidden" name="lblimgsc" runat="server"> <input id="lblimgidc" type="hidden" name="lblimgidc" runat="server">
			<input id="lblovimgsc" type="hidden" name="lblovimgsc" runat="server"> <input id="lblovbimgsc" type="hidden" name="lblovbimgsc" runat="server">
			<input id="lblcurrimgc" type="hidden" name="lblcurrimgc" runat="server"> <input id="lblcurrbimgc" type="hidden" name="lblcurrbimgc" runat="server">
			<input id="lblbimgsc" type="hidden" name="lblbimgsc" runat="server"><input id="lbliordersc" type="hidden" name="lbliordersc" runat="server">
			<input id="lbloldorderc" type="hidden" runat="server" NAME="lbloldorderc"> <input id="lblovtimgsc" type="hidden" name="lblovtimgsc" runat="server">
			<input id="lblcurrtimgc" type="hidden" name="lblcurrtimgc" runat="server"> <input type="hidden" id="lbloloc" runat="server">
			<input id="lblpcnt" type="hidden" runat="server" NAME="lblpcnt"> <input id="lblcurrp" type="hidden" runat="server" NAME="lblcurrp">
			<input id="lblimgs" type="hidden" name="lblimgs" runat="server"> <input id="lblimgid" type="hidden" name="lblimgid" runat="server">
			<input id="lblovimgs" type="hidden" name="lblovimgs" runat="server"> <input id="lblovbimgs" type="hidden" name="lblovbimgs" runat="server">
			<input id="lblcurrimg" type="hidden" name="lblcurrimg" runat="server"> <input id="lblcurrbimg" type="hidden" name="lblcurrbimg" runat="server">
			<input id="lblbimgs" type="hidden" name="lblbimgs" runat="server"><input id="lbliorders" type="hidden" name="lbliorders" runat="server">
			<input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder"> <input id="lblovtimgs" type="hidden" name="lblovtimgs" runat="server">
			<input id="lblcurrtimg" type="hidden" name="lblcurrtimg" runat="server"><input type="hidden" id="lblcnt" runat="server" NAME="lblcnt">
			<input type="hidden" id="lblcompchk" runat="server"><input type="hidden" id="lblchk" runat="server">
			<input type="hidden" id="lbleqid" runat="server"> <input type="hidden" id="lblsid" runat="server">
			<input type="hidden" id="lbldid" runat="server"> <input type="hidden" id="lblclid" runat="server">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog"> <input type="hidden" id="lblupret" runat="server">
			<input type="hidden" id="lblro" runat="server"><input type="hidden" id="lblcoi" runat="server" />
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
