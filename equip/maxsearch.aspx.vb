

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class maxsearch
    Inherits System.Web.UI.Page
	

	Protected WithEvents lang2425 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2424 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2423 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2422 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2421 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2420 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2419 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2418 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2417 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2416 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2415 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2414 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2413 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2412 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2411 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2410 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2409 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqid As String
    Dim dr As SqlDataReader
    Dim sql As String
    Dim rep As New Utilities
    Dim fuid, cid, psite, filt, dt, val, eq, srchtyp As String
    Dim Login As String
    Dim tl, sid, did, clid, chk, clib As String
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Tables As String = ""
    Dim PK As String = ""
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnumsrch As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqdesc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblspl As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloem As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmodel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblserial As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblac As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dddepts As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddcells As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddeq As System.Web.UI.WebControls.DropDownList
    Protected WithEvents celldiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents eqdiv As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblgofunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblsrchtyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents trsave As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblgo As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsidret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllib As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifeq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents iffu As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifco As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents xcoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ycoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrep As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    
    Dim Sort As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txteqnum As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoem As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmodel As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtserial As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents trhead As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents trmain As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents diveq As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iffunc As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifcomp As System.Web.UI.HtmlControls.HtmlGenericControl

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        xCoord.Value = 0
        yCoord.Value = 0
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = "0"
            lblcid.Value = cid
            psite = HttpContext.Current.Session("dfltps").ToString()
            lblsid.Value = psite
            srchtyp = Request.QueryString("srchtyp").ToString
            lblsrchtyp.Value = srchtyp
            Try
                clib = Request.QueryString("lib").ToString
                lbllib.Value = clib
            Catch ex As Exception
                clib = "no"
                lbllib.Value = clib
            End Try
           

            tdeq.InnerHtml = "No Equipment Record Selected"
            tdeq.Attributes.Add("class", "redlabel")

            If srchtyp = "nofu" Then
                tdfu.InnerHtml = "Function Selection Not Required"
                tdfu.Attributes.Add("class", "redlabel")
                tdco.InnerHtml = "Component Selection Not Required"
                tdco.Attributes.Add("class", "redlabel")
            Else
                tdfu.InnerHtml = "No Function Record Selected"
                tdfu.Attributes.Add("class", "redlabel")
                If srchtyp = "noco" Then
                    tdco.InnerHtml = "Component Selection Not Required"
                    tdco.Attributes.Add("class", "redlabel")
                Else
                    tdco.InnerHtml = "No Component Record Selected"
                    tdco.Attributes.Add("class", "redlabel")
                End If
            End If

            txtpg.Value = "1"
            trhead.Attributes.Add("class", "view")
            trmain.Attributes.Add("class", "view")
            rep.Open()
            GetEqList()
            PopAC()
            PopDepts(psite)
            rep.Dispose()
        Else
            If Request.Form("lblsubmit") = "srcheq" Then
                lblsubmit.Value = ""
                txtpg.Value = "1"
                rep.Open()
                SrchEq()
                GetEqList()
                rep.Dispose()
            Else
                If Request.Form("lblret") = "next" Then
                    rep.Open()
                    GetNext()
                    rep.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "last" Then
                    rep.Open()
                    PageNumber = txtpgcnt.Value
                    txtpg.Value = PageNumber
                     GetEqList()
                    rep.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "prev" Then
                    rep.Open()
                    GetPrev()
                    rep.Dispose()
                    lblret.Value = ""
                ElseIf Request.Form("lblret") = "first" Then
                    rep.Open()
                    PageNumber = 1
                    txtpg.Value = PageNumber
                     GetEqList()
                    rep.Dispose()
                    lblret.Value = ""
                End If
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetEqList()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr975" , "maxsearch.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetEqList()
        Catch ex As Exception
            rep.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr976" , "maxsearch.aspx.vb")
 
            rep.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Function ModString(ByVal str As String)
        str = Replace(str, "'", Chr(180), , , vbTextCompare)
        str = Replace(str, "--", "-", , , vbTextCompare)
        str = Replace(str, ";", ":", , , vbTextCompare)
        Return str
    End Function
    Private Sub SrchEq()
        Dim Filter, eq, ed, sp, oe, mo, se, ac As String
        eq = txteqnum.Text
        If Len(eq) <> 0 Then
            eq = ModString(eq)
            Filter = "eqnum like ''%" & eq & "%'' "
            FilterCnt = "eqnum like '%" & eq & "%' "
            lbleqnumsrch.Value = eq
        End If
        ed = txteqdesc.Text
        If Len(ed) <> 0 Then
            ed = ModString(ed)
            If Len(Filter) = 0 Then
                Filter = "eqdesc like ''%" & ed & "%'' "
                FilterCnt = "eqdesc like '%" & ed & "%' "
            Else
                Filter += "and eqdesc like ''%" & ed & "%'' "
                FilterCnt += "and eqdesc like '%" & ed & "%' "
            End If
            lbleqdesc.Value = ed
        End If
        sp = txtspl.Text
        If Len(sp) <> 0 Then
            sp = ModString(sp)
            If Len(Filter) = 0 Then
                Filter = "spl like ''%" & sp & "%'' "
                FilterCnt = "spl like '%" & sp & "%' "
            Else
                Filter += "and spl like ''%" & sp & "%'' "
                FilterCnt += "and spl like '%" & sp & "%' "
            End If
            lblspl.Value = sp
        End If
        oe = txtoem.Text
        If Len(oe) <> 0 Then
            oe = ModString(oe)
            If Len(Filter) = 0 Then
                Filter = "oem like ''%" & oe & "%'' "
                FilterCnt = "oem like '%" & oe & "%' "
            Else
                Filter += "and oem like ''%" & oe & "%'' "
                FilterCnt += "and oem like '%" & oe & "%' "
            End If
            lbloem.Value = oe
        End If
        mo = txtmodel.Text
        If Len(mo) <> 0 Then
            mo = ModString(mo)
            If Len(Filter) = 0 Then
                Filter = "model like ''%" & mo & "%'' "
                FilterCnt = "model like '%" & mo & "%' "
            Else
                Filter += "and model like ''%" & mo & "%'' "
                FilterCnt += "and model like '%" & mo & "%' "
            End If
            lblmodel.Value = mo
        End If
        se = txtserial.Text
        If Len(se) <> 0 Then
            se = ModString(se)
            If Len(Filter) = 0 Then
                Filter = "serial like ''%" & se & "%'' "
                FilterCnt = "serial like '%" & se & "%' "
            Else
                Filter += "and serial like ''%" & se & "%'' "
                FilterCnt += "and serial like '%" & se & "%' "
            End If
            lblserial.Value = se
        End If
        If ddac.SelectedIndex <> 0 Then
            ac = ddac.SelectedValue.ToString
            If Len(Filter) = 0 Then
                Filter = "acid like ''%" & ac & "%'' "
                FilterCnt = "acid like '%" & ac & "%' "
            Else
                Filter += "and acid like ''%" & ac & "%'' "
                FilterCnt += "and acid like '%" & ac & "%' "
            End If
            lblac.Value = ac
        End If

    End Sub
    Private Sub GetEqList()
        PageNumber = txtpg.Value
        Dim sb As New StringBuilder
        Dim eqnum, eqdesc As String
        Dim Filter, eq, ed, sp, oe, mo, se, ac, did, clid, lid, sid As String
        eq = lbleqnumsrch.Value '
        If Len(eq) <> 0 Then
            eq = ModString(eq)
            Filter = "eqnum like ''%" & eq & "%'' "
            FilterCnt = "eqnum like '%" & eq & "%' "
            txteqnum.Text = eq
        End If
        ed = lbleqdesc.Value '
        If Len(ed) <> 0 Then
            ed = ModString(ed)
            If Len(Filter) = 0 Then
                Filter = "eqdesc like ''%" & ed & "%'' "
                FilterCnt = "eqdesc like '%" & ed & "%' "
            Else
                Filter += "and eqdesc like ''%" & ed & "%'' "
                FilterCnt += "and eqdesc like '%" & ed & "%' "
            End If
            txteqdesc.Text = ed
        End If
        sp = lblspl.Value '
        If Len(sp) <> 0 Then
            sp = ModString(sp)
            If Len(Filter) = 0 Then
                Filter = "spl like ''%" & sp & "%'' "
                FilterCnt = "spl like '%" & sp & "%' "
            Else
                Filter += "and spl like ''%" & sp & "%'' "
                FilterCnt += "and spl like '%" & sp & "%' "
            End If
            txtspl.Text = sp
        End If
        oe = lbloem.Value '
        If Len(oe) <> 0 Then
            oe = ModString(oe)
            If Len(Filter) = 0 Then
                Filter = "oem like ''%" & oe & "%'' "
                FilterCnt = "oem like '%" & oe & "%' "
            Else
                Filter += "and oem like ''%" & oe & "%'' "
                FilterCnt += "and oem like '%" & oe & "%' "
            End If
            txtoem.Text = oe
        End If
        mo = lblmodel.Value '
        If Len(mo) <> 0 Then
            mo = ModString(mo)
            If Len(Filter) = 0 Then
                Filter = "model like ''%" & mo & "%'' "
                FilterCnt = "model like '%" & mo & "%' "
            Else
                Filter += "and model like ''%" & mo & "%'' "
                FilterCnt += "and model like '%" & mo & "%' "
            End If
            txtmodel.Text = mo
        End If
        se = lblserial.Value '
        If Len(se) <> 0 Then
            se = ModString(se)
            If Len(Filter) = 0 Then
                Filter = "serial like ''%" & se & "%'' "
                FilterCnt = "serial like '%" & se & "%' "
            Else
                Filter += "and serial like ''%" & se & "%'' "
                FilterCnt += "and serial like '%" & se & "%' "
            End If
            txtserial.Text = se
        End If
        If ddac.SelectedIndex <> 0 And ddac.SelectedIndex <> -1 Then
            ac = lblac.Value '
            If Len(Filter) = 0 Then
                Filter = "acid like ''%" & ac & "%'' "
                FilterCnt = "acid like '%" & ac & "%' "
            Else
                Filter += "and acid like ''%" & ac & "%'' "
                FilterCnt += "and acid like '%" & ac & "%' "
            End If
            ddac.SelectedValue = ac
        End If


        If lbldid.Value <> "" Then
            ac = lbldid.Value '
            If Len(Filter) = 0 Then
                Filter = "dept_id like ''%" & ac & "%'' "
                FilterCnt = "dept_id like '%" & ac & "%' "
            Else
                Filter += "and dept_id like ''%" & ac & "%'' "
                FilterCnt += "and dept_id like '%" & ac & "%' "
            End If
            'ddac.SelectedValue = ac
        End If

        If lblclid.Value <> "" Then
            ac = lblclid.Value '
            If Len(Filter) = 0 Then
                Filter = "cellid like ''%" & ac & "%'' "
                FilterCnt = "cellid like '%" & ac & "%' "
            Else
                Filter += "and cellid like ''%" & ac & "%'' "
                FilterCnt += "and cellid like '%" & ac & "%' "
            End If
            'ddac.SelectedValue = ac
        End If
        psite = lblsid.Value
        clib = lbllib.Value
        If clib = "no" Then
            If Len(Filter) = 0 Then
                FilterCnt = "siteid = '" & psite & "'"
                Filter = "siteid = ''" & psite & "''"
            Else
                FilterCnt += "and siteid = '" & psite & "'"
                Filter += "and siteid = ''" & psite & "''"
            End If
        End If
        If Len(Filter) <> 0 Then
            sql = "select Count(*) from Equipment where " & FilterCnt
        Else
            sql = "select Count(*) from Equipment"
        End If

        Dim dc As Integer = rep.PageCount(sql, PageSize) 'rep.Scalar(sql)
        If dc = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & dc
        End If
        txtpgcnt.Value = dc
        ' sql = "select count(*) from pmTaskMeasDetMan m " _
        '+ "left join pm p on p.pmid = m.pmid where p.eqid = '" & eqid & "'"
        Tables = "equipment"
        PK = "eqid"
        PageSize = "200"
        Dim dept, cell As String
        Fields = "siteid, locid, dept_id, cellid, eqid, eqnum, eqdesc, mcnt = ''0'', " _
        + "dept = (select d.dept_line from dept d where d.dept_id = equipment.dept_id), " _
        + "cell = (select c.cell_name from cells c where c.cellid = equipment.cellid) "
        dr = rep.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        'sql = "select dept_id, cellid, eqid, eqnum, eqdesc from equipment where siteid = '" & psite & "'"
        sb.Append("<table>")
        'dr = rep.GetRdrData(sql)
        'If dr.Read Then
        Dim mcnt As String
        Dim cchk As String
        While dr.Read
            sid = dr.Item("siteid").ToString
            lid = dr.Item("locid").ToString
            dept = dr.Item("dept").ToString
            cell = dr.Item("cell").ToString
            did = dr.Item("dept_id").ToString
            mcnt = dr.Item("mcnt").ToString
            clid = dr.Item("cellid").ToString
            If clid <> "" Then
                cchk = "yes"
            Else
                cchk = "no"
            End If
            eqid = dr.Item("eqid").ToString
            eqnum = dr.Item("eqnum").ToString
            eqnum = Replace(eqnum, "/", "//", , , vbTextCompare)
            eqnum = Replace(eqnum, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqnum = ModString(eqnum)
            eqdesc = dr.Item("eqdesc").ToString
            eqdesc = Replace(eqdesc, "/", "//", , , vbTextCompare)
            eqdesc = Replace(eqdesc, """", Chr(180) & Chr(180), , , vbTextCompare)
            eqdesc = ModString(eqdesc)
            sb.Append("<tr><td>")
            sb.Append("<a class=""A1"" href=""#"" onclick=""getdeq('" & did & "','" & clid & "','" & eqid & "','" & eqnum & "','" & eqdesc & "','" & mcnt & "','" & dept & "','" & cell & "','" & cchk & "','" & lid & "','" & sid & "')"">")
            sb.Append(eqnum & " - " & eqdesc)
            sb.Append("</td></tr>")
        End While
        'Else
        'sb.Append("<tr><td class=""plainlabelred"">" & tmod.getlbl("cdlbl449" , "maxsearch.aspx.vb") & "</td></tr>")
        'End If
        dr.Close()
        sb.Append("</table>")
        diveq.InnerHtml = sb.ToString
    End Sub


    Private Sub PopAC()
        cid = lblcid.Value
        sql = "select * from pmAssetClass order by assetclass"
        dr = rep.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        ddac.DataBind()
        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Private Sub PopDepts(ByVal site As String)
        filt = " where siteid = '" & site & "'"
        sql = "select count(*) from Dept where siteid = '" & site & "'"
        Dim dcnt As Integer = rep.Scalar(sql)
        If dcnt > 0 Then
            dt = "Dept"
            val = "dept_id , dept_line "
            dr = rep.GetList(dt, val, filt)
            dddepts.DataSource = dr
            dddepts.DataTextField = "dept_line"
            dddepts.DataValueField = "dept_id"
            dddepts.DataBind()
            dr.Close()
            dddepts.Enabled = True
            ddcells.Enabled = False
            ddeq.Enabled = False
            dddepts.Items.Insert(0, "Select Department")
            ddcells.Items.Insert(0, "Select Station/Cell")
            ddeq.Items.Insert(0, "Select Equipment")
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr977" , "maxsearch.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            dddepts.Enabled = False
            ddcells.Enabled = False
            ddeq.Enabled = False
            dddepts.Items.Insert(0, "Select Department")
            ddcells.Items.Insert(0, "Select Station/Cell")
            ddeq.Items.Insert(0, "Select Equipment")
        End If

    End Sub

    Private Sub dddepts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dddepts.SelectedIndexChanged
        

        If dddepts.SelectedIndex <> 0 Then
            Dim did As String = dddepts.SelectedValue.ToString
            lbldid.Value = did
            Dim dept As String = dddepts.SelectedItem.ToString
            lbldept.Value = dept
            rep.Open()
            'celldiv.Style.Add("display", "none")
            'celldiv.Style.Add("visibility", "hidden")
            'eqdiv.Style.Add("display", "none")
            'eqdiv.Style.Add("visibility", "hidden")
            'fudiv.Style.Add("display", "none")
            'fudiv.Style.Add("visibility", "hidden")
            'lbldeptdesc.Text = ""
            Dim tl As Integer
            sql = "select count(*) from Cells where dept_id = '" & did & "'"
            tl = rep.Scalar(sql)
            If tl <> 0 Then
                PopCells(did)
            Else
                PopEq(did, "d")
            End If
            GetEqList()
            rep.Dispose()

            lblgofunc.Value = "no"
            'lblgofunc.Value = "yes"
        End If
    End Sub
    Private Sub PopCells(ByVal dept As String)
        filt = " where dept_id = " & dept
        dt = "Cells"
        val = "cellid , cell_name "
        dr = rep.GetList(dt, val, filt)
        ddcells.DataSource = dr
        ddcells.DataTextField = "cell_name"
        ddcells.DataValueField = "cellid"
        ddcells.DataBind()
        dr.Close()
        ddcells.Items.Insert(0, "Select Station/Cell")
        ddcells.Enabled = True
        ddeq.Enabled = False
        'celldiv.Style.Add("display", "block")
        'celldiv.Style.Add("visibility", "visible")
    End Sub
    Private Sub ddcells_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddcells.SelectedIndexChanged

        'eqdiv.Style.Add("display", "none")
        'eqdiv.Style.Add("visibility", "hidden")
        If ddcells.SelectedIndex <> 0 Then
            Dim clid As String = ddcells.SelectedValue.ToString
            lblclid.Value = clid
            Dim cell As String = ddcells.SelectedItem.ToString
            lblcell.Value = cell
            rep.Open()
            PopEq(clid, "c")
            GetEqList()
            rep.Dispose()
        End If
    End Sub
    Private Sub PopEq(ByVal var As String, ByVal typ As String)

        Dim eqcnt As Integer
        If typ = "d" Then
            sql = "select count(*) from equipment where dept_id = '" & var & "'"
            filt = " where dept_id = '" & var & "'"
        Else
            sql = "select count(*) from equipment where cellid = '" & var & "'"
            filt = " where cellid = '" & var & "'"
        End If
        eqcnt = rep.Scalar(sql)
        If eqcnt > 0 Then
            dt = "equipment"
            val = "eqid, eqnum "

            dr = rep.GetList(dt, val, filt)
            ddeq.DataSource = dr
            ddeq.DataTextField = "eqnum"
            ddeq.DataValueField = "eqid"
            ddeq.DataBind()
            dr.Close()
            ddeq.Items.Insert(0, "Select Equipment")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = True
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr978" , "maxsearch.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            ddeq.Items.Insert(0, "Select Equipment")
            'eqdiv.Style.Add("display", "block")
            'eqdiv.Style.Add("visibility", "visible")
            ddeq.Enabled = False
        End If

    End Sub

    Private Sub ddeq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddeq.SelectedIndexChanged
        

        If ddeq.SelectedIndex <> 0 Then
            rep.Open()
            'GetHead(eq)
            'CheckMeas(eq)
            eq = ddeq.SelectedValue.ToString
            lbleqid.Value = eq
            lbleqnumsrch.Value = ddeq.SelectedItem.ToString
            lbleqnum.Value = ddeq.SelectedItem.ToString
            GetEqList()
            lblgofunc.Value = "yes"
            rep.Dispose()
            'Dim strMessage As String =  tmod.getmsg("cdstr979" , "maxsearch.aspx.vb")
 
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Private Sub ClearFields()
        lbleqnumsrch.Value = ""
        txteqnum.Text = ""
        'lbleqnum.Value = ""
        txteqnum.Text = ""
        lbleqdesc.Value = ""
        txteqdesc.Text = ""
        lblspl.Value = ""
        txtspl.Text = ""
        lbloem.Value = ""
        txtoem.Text = ""
        lblmodel.Value = ""
        txtmodel.Text = ""
        lblserial.Value = ""
        txtserial.Text = ""
        lblac.Value = ""
        Try
            ddac.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2409.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2409")
        Catch ex As Exception
        End Try
        Try
            lang2410.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2410")
        Catch ex As Exception
        End Try
        Try
            lang2411.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2411")
        Catch ex As Exception
        End Try
        Try
            lang2412.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2412")
        Catch ex As Exception
        End Try
        Try
            lang2413.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2413")
        Catch ex As Exception
        End Try
        Try
            lang2414.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2414")
        Catch ex As Exception
        End Try
        Try
            lang2415.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2415")
        Catch ex As Exception
        End Try
        Try
            lang2416.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2416")
        Catch ex As Exception
        End Try
        Try
            lang2417.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2417")
        Catch ex As Exception
        End Try
        Try
            lang2418.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2418")
        Catch ex As Exception
        End Try
        Try
            lang2419.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2419")
        Catch ex As Exception
        End Try
        Try
            lang2420.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2420")
        Catch ex As Exception
        End Try
        Try
            lang2421.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2421")
        Catch ex As Exception
        End Try
        Try
            lang2422.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2422")
        Catch ex As Exception
        End Try
        Try
            lang2423.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2423")
        Catch ex As Exception
        End Try
        Try
            lang2424.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2424")
        Catch ex As Exception
        End Try
        Try
            lang2425.Text = axlabs.GetASPXPage("maxsearch.aspx", "lang2425")
        Catch ex As Exception
        End Try

    End Sub



End Class
