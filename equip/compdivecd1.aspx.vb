﻿Imports System.Data.SqlClient
Imports System.Text
Public Class compdivecd1
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim necd As New Utilities
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim ecd, ecd1id, compnum As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            compnum = Request.QueryString("compnum").ToString
            lblcompnum.Value = compnum
            txtnewcomp.Text = compnum
            txtsearch.Text = compnum
            If compnum <> "" Then
                txtnewcomp.Enabled = False
                tdnc.Attributes.Add("class", "graylabel")
            Else
                txtnewcomp.Enabled = True
                tdnc.Attributes.Add("class", "redlabel")
            End If
            necd.Open()
            PopEC1(1)
            necd.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                necd.Open()
                checkcnt()
                necd.Dispose()
            End If
            If Request.Form("lblret") = "next" Then
                necd.Open()
                GetNext()
                necd.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                necd.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber

                PopEC1(PageNumber)
                necd.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                necd.Open()
                GetPrev()
                necd.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                necd.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                PopEC1(PageNumber)
                necd.Dispose()
                lblret.Value = ""
            End If
        End If
        
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            PopEC1(PageNumber)
        Catch ex As Exception
            necd.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopEC1(PageNumber)
        Catch ex As Exception
            necd.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopEC1(ByVal PageNumber As Integer)
        Dim sb As New StringBuilder
        Dim fm, fmi, fmii As String
        sb.Append("<table>")
        Dim intPgNav, intPgCnt As Integer
        Dim srch As String = txtsearch.Text
        srch = necd.ModString1(srch)
        srch = Trim(srch)
        If srch = "" Then
            sql = "select count(*) from ecd1"
        Else
            sql = "select count(*) from ecd1 where ecd1 like '%" & srch & "%' or ecddesc like '%" & srch & "%'"
        End If

        intPgCnt = necd.Scalar(sql)
        If intPgCnt = 0 Then
            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub
        End If
        PageSize = "50"
        intPgNav = necd.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        'sql = "select * from ecd1 order by ecd1"
        If srch <> "" Then
            Filter = srch
        Else
            Filter = ""
        End If
        sql = "usp_getallecd1pg '" & PageNumber & "','" & PageSize & "','" & Filter & "'"
        dr = necd.GetRdrData(sql)
        While dr.Read
            sb.Append("<tr><td class=""plainlabel"">")
            fm = dr.Item("ecddesc").ToString
            fmi = dr.Item("ecd1").ToString
            fmii = dr.Item("ecd1id").ToString
            sb.Append("<a href=""#"" onclick=""getfm('" & fmi & "','" & fm & "','" & fmii & "')"">" & fmi & " - " & fm & "</a>")
            sb.Append("</td></tr>")
        End While
        dr.Close()
        sb.Append("</table>")
        fdiv.InnerHtml = sb.ToString
    End Sub
    Private Sub checkcnt()
        ecd = txtnewecd1.Text
        lblecd1.Value = ecd
        compnum = lblcompnum.Value
        Dim ecdcnt As Integer = 0
        sql = "select count(*) from ecd1 where ecd1 = '" & ecd & "'"
        ecdcnt = necd.Scalar(sql)
        If ecdcnt = 0 Then
            sql = "insert into ecd1 (ecd1, ecddesc) values ('" & ecd & "','" & compnum & "') select @@identity"
            ecd1id = necd.Scalar(sql)
            lblecd1id.Value = ecd1id
            lblret.Value = "go"
        Else
            Dim strMessage As String = "This Error Code Value Already Exists"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub

    Protected Sub ibtnsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        necd.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        PopEC1(PageNumber)
        necd.Dispose()
    End Sub
End Class