

'********************************************************
'*
'********************************************************



Imports System.Data.sqlclient
Public Class translist
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim trans As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents cbtranslist As System.Web.UI.WebControls.CheckBoxList
    Protected WithEvents cball As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnsubmit As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnsubmit1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnreturn As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        If Not IsPostBack Then
            lbleqnum.Value = Request.QueryString("eq").ToString
            trans.Open()
            cbtest()
            trans.Dispose()
        End If

    End Sub
    Private Sub cbtest()
        Dim list As String '= "1,0,1,0,1,0,1,0,1"
        Dim arr As String() '= list.Split(",")
        'Dim str As String() = {"eq", "eqdesc", "eqspl", "fu", "fuspl", "comp", "compdesc", "compspl", "tasks"}
        Dim eq As String = lbleqnum.Value
        sql = "usp_gettransvars '" & eq & "'"
        dr = trans.GetRdrData(sql)
        While dr.Read
            list = dr.Item("list").ToString
        End While
        If list = "all" Then
            cball.Checked = True
        Else
            Dim i As Integer
            Dim n As Integer
            Dim li As ListItem
            arr = list.Split(",")
            For i = 0 To arr.Length - 2
                If arr(i) = 1 Then
                    cbtranslist.Items(i).Selected = True
                Else
                    cbtranslist.Items(i).Selected = False
                End If
            Next
        End If
        
    End Sub
    Private Sub btnsubmit1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsubmit1.Click
        Dim eq As String = lbleqnum.Value
        Dim li As ListItem
        Dim list As String
        If cball.Checked = False Then
            For Each li In cbtranslist.Items
                If li.Selected = True Then
                    list += "1,"
                Else
                    list += "0,"
                End If
            Next
        Else
            list = "1,1,1,1,1,1,1,1,1,"
        End If
        sql = "update pmtransvars set list = '" & list & "' where eqnum = '" & eq & "'"
        trans.Open()
        trans.Update(sql)
        trans.Dispose()
        lblret.Value = "1"
    End Sub
	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			btnreturn.Attributes.Add("src" , "../images2/eng/bgbuttons/return.gif")
			ElseIf lang = "fre" Then
			btnreturn.Attributes.Add("src" , "../images2/fre/bgbuttons/return.gif")
			ElseIf lang = "ger" Then
			btnreturn.Attributes.Add("src" , "../images2/ger/bgbuttons/return.gif")
			ElseIf lang = "ita" Then
			btnreturn.Attributes.Add("src" , "../images2/ita/bgbuttons/return.gif")
			ElseIf lang = "spa" Then
			btnreturn.Attributes.Add("src" , "../images2/spa/bgbuttons/return.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			btnsubmit1.Attributes.Add("src" , "../images2/eng/bgbuttons/submit.gif")
			ElseIf lang = "fre" Then
			btnsubmit1.Attributes.Add("src" , "../images2/fre/bgbuttons/submit.gif")
			ElseIf lang = "ger" Then
			btnsubmit1.Attributes.Add("src" , "../images2/ger/bgbuttons/submit.gif")
			ElseIf lang = "ita" Then
			btnsubmit1.Attributes.Add("src" , "../images2/ita/bgbuttons/submit.gif")
			ElseIf lang = "spa" Then
			btnsubmit1.Attributes.Add("src" , "../images2/spa/bgbuttons/submit.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class
