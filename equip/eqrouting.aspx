﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqrouting.aspx.vb" Inherits="lucy_r12.eqrouting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Equipment Routing</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script language="JavaScript" type="text/javascript" src="../scripts/overlib2.js"></script>
  <script language="javascript" type="text/javascript" src="../scripts/smartscroll.js"></script>
</head>
<body onload="sstchur_SmartScroller_Scroll();">
    <form id="form1" runat="server">
    <table>
    <tr>
    <td class="label">Equipment Routing</td>
    </tr>
    <tr>
    <td>
    <asp:DataGrid ID="dglist" runat="server" BackColor="transparent" GridLines="None"
                        CellSpacing="1" AutoGenerateColumns="False">
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Seq#">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label Width="50px" CssClass="plainlabel" ID="lblseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox Width="40px" CssClass="plainlabelred" ID="txtseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Location">
                                <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabelred" ID="lbllocs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Equipment#">
                                <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabelred" ID="lbleqs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Equipment Description">
                                <HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqdesc") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabelred" ID="lblnc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqdesc") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbleqidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                    </asp:Label>
                                    </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbleqid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                    </asp:Label>
                                    </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
    </td>
    </tr>
    </table>
    <input id="lblrow" type="hidden" name="lblrow" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="yCoord" runat="server" />
    <input type="hidden" id="xCoord" runat="server" />
    </form>
</body>
</html>
