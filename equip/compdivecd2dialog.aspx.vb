﻿Public Class compdivecd2dialog
    Inherits System.Web.UI.Page
    Dim ecd1id, ecd2txt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ecd1id = Request.QueryString("ecd1").ToString
            ecd2txt = Request.QueryString("ecd2txt").ToString
            ifecd.Attributes.Add("src", "compdivecd2.aspx?ecd1=" + ecd1id + "&ecd2txt=" + ecd2txt + "&date=" + Now)
        End If
    End Sub

End Class