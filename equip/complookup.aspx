<%@ Page Language="vb" AutoEventWireup="false" Codebehind="complookup.aspx.vb" Inherits="lucy_r12.complookup" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>complookup</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/gridnav.js"></script>
		<script  src="../scripts1/complookupaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table id="tbltop" width="650" runat="server">
				<tr>
					<td class="thdrsingrt label" colSpan="3"><asp:Label id="lang2059" runat="server">Component Lookup Dialog</asp:Label></td>
				</tr>
				<tr id="tbleqrec" runat="server">
					<td class="bluelabel" style="width: 80px"><asp:Label id="lang2060" runat="server">Search</asp:Label></td>
					<td width="310"><asp:textbox id="txtsrch" runat="server" Width="250px"></asp:textbox><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
					<td width="210"></td>
				</tr>
				<tr>
					<td id="tdcomp" align="center" colSpan="3" runat="server"></td>
				</tr>
				<tr>
					<td align="center" colSpan="6">
						<table style="BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblret" type="hidden" name="lblret" runat="server"> <input id="txtpg" type="hidden" name="txtpg" runat="server">
			<input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
