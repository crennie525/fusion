

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmlibeqdets
    Inherits System.Web.UI.Page
	Protected WithEvents lang2455 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2454 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2453 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2452 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2451 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2450 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2449 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2448 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2447 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2446 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2445 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2444 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2443 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2442 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2441 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpcnte As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim eqid, db, start As String
    Protected WithEvents tdac As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents tblrevdetails As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdloc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdoem As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmod As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdsn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td8 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcmail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td7 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmphone As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmail As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                eqid = Request.QueryString("eqid").ToString
                db = Request.QueryString("db").ToString
                lbleqid.Value = eqid
                lbldb.Value = db
                copy.Open()
                GetEq()
                LoadEqPics()
                copy.Dispose()
            End If

        End If

    End Sub
    Private Sub GetEq()
        eqid = lbleqid.Value
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        mdb = mdbarr(2).ToString
        sql = "select e.*, " _
        + "cph = (select s.phonenum from  [" + srvr + "].[" + mdb + "].[dbo].[pmsysusers] s  where  s.username = e.createdby), " _
        + "cm = (select s.email from  [" + srvr + "].[" + mdb + "].[dbo].[pmsysusers] s  where  s.username = e.createdby), " _
        + "eph = (select s.phonenum from  [" + srvr + "].[" + mdb + "].[dbo].[pmsysusers] s  where  s.username = e.modifiedby), " _
        + "em = (select s.email from  [" + srvr + "].[" + mdb + "].[dbo].[pmsysusers] s  where  s.username = e.modifiedby) " _
        + "from  [" & srvr & "].[" & mdb & "].[dbo].[equipment] e where e.eqid = '" & eqid & "'"
        dr = copy.GetRdrData(sql)
        While dr.Read

            tdeq.InnerHtml = dr.Item("eqnum").ToString
            tddesc.InnerHtml = dr.Item("eqdesc").ToString
            tdspl.InnerHtml = dr.Item("spl").ToString
            tdloc.InnerHtml = dr.Item("location").ToString
            tdmod.InnerHtml = dr.Item("model").ToString
            tdoem.InnerHtml = dr.Item("oem").ToString
            tdecr.InnerHtml = dr.Item("ecr").ToString
            tdecr.InnerHtml = dr.Item("ecr").ToString
            tdac.InnerHtml = dr.Item("assetclass").ToString
            tdmby.InnerHtml = dr.Item("modifiedby").ToString
            tdcby.InnerHtml = dr.Item("createdby").ToString
            tdmdate.InnerHtml = dr.Item("modifieddate").ToString
            tdcdate.InnerHtml = dr.Item("createdate").ToString
            tdcphone.InnerHtml = dr.Item("cph").ToString
            tdcmail.InnerHtml = "<A href=""mailto:" & dr.Item("cm").ToString & """>" & dr.Item("cm").ToString & "</A>"
            tdmphone.InnerHtml = dr.Item("eph").ToString
            tdmail.InnerHtml = "<A href=""mailto:" & dr.Item("em").ToString & """>" & dr.Item("em").ToString & "</A>"
            tdsn.InnerHtml = dr.Item("serial").ToString

        End While
        dr.Close()
    End Sub

    Private Sub LoadEqPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgeq.Attributes.Add("src", "../images/appimages/eqimg1.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/eqimg1.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/eqimg1.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/eqimg1.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/eqimg1.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/eqimg1.gif")
        End If


        Dim img, imgs, picid, eqid As String
        Dim mdb As String = lbldb.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        mdb = mdbarr(2).ToString
        eqid = lbleqid.Value
        Dim pcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] where funcid is null and comid is null and eqid = '" & eqid & "'"
        'sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        pcnt = copy.Scalar(sql)
        lblpcnte.Value = pcnt

        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from [" & srvr & "].[" & mdb & "].[dbo].[pmpictures] p where p.funcid is null and p.comid is null and p.eqid = '" & eqid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = copy.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    imgeq.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    'txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2441.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2441")
        Catch ex As Exception
        End Try
        Try
            lang2442.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2442")
        Catch ex As Exception
        End Try
        Try
            lang2443.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2443")
        Catch ex As Exception
        End Try
        Try
            lang2444.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2444")
        Catch ex As Exception
        End Try
        Try
            lang2445.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2445")
        Catch ex As Exception
        End Try
        Try
            lang2446.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2446")
        Catch ex As Exception
        End Try
        Try
            lang2447.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2447")
        Catch ex As Exception
        End Try
        Try
            lang2448.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2448")
        Catch ex As Exception
        End Try
        Try
            lang2449.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2449")
        Catch ex As Exception
        End Try
        Try
            lang2450.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2450")
        Catch ex As Exception
        End Try
        Try
            lang2451.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2451")
        Catch ex As Exception
        End Try
        Try
            lang2452.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2452")
        Catch ex As Exception
        End Try
        Try
            lang2453.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2453")
        Catch ex As Exception
        End Try
        Try
            lang2454.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2454")
        Catch ex As Exception
        End Try
        Try
            lang2455.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lang2455")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("pmlibeqdets.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub

End Class
