﻿Imports System.Data.SqlClient
Public Class compdivecd2
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim necd As New Utilities
    Dim dr As SqlDataReader
    Dim ecd1id, ecd2id, newecd2, ecd2txt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ecd1id = Request.QueryString("ecd1").ToString
            lblecd1id.Value = ecd1id
            ecd2txt = Request.QueryString("ecd2txt").ToString
            lblecd2txt.Value = ecd2txt
            tr1.Attributes.Add("class", "details")
            tr2.Attributes.Add("class", "details")
            'necd.Open()
            'getecd2(ecd1id)
            'necd.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                lblsubmit.Value = ""
                necd.Open()
                upecd2()
                necd.Dispose()
            End If
        End If
        txtnewecd2.Attributes.Add("onkeyup", "fillnew(this.value);")
        ddecd2.Attributes.Add("onchange", "filldown(this.value);")
    End Sub
    Private Sub getecd2(ByVal ecd1id As String)
        Dim ecd2cnt As Integer
        sql = "select count(*) from ecd2 where ecd1id = '" & ecd1id & "'"
        ecd2cnt = necd.Scalar(sql)
        If ecd2cnt = 0 Then
            'Dim strMessage As String = "This Error Code Value Already Exists"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            tr1.Attributes.Add("class", "details")
            tr2.Attributes.Add("class", "details")
        Else
            sql = "select ecd2id, ecd2 from ecd2 where ecd1id = '" & ecd1id & "'"
            dr = necd.GetRdrData(sql)
            ddecd2.DataSource = dr
            ddecd2.DataTextField = "ecd2"
            ddecd2.DataValueField = "ecd2id"
            ddecd2.DataBind()
            dr.Close()
            ddecd2.Items.Insert(0, "Select")
        End If
       
    End Sub
    Private Sub upecd2()
        ecd1id = lblecd1id.Value
        newecd2 = txtnewecd2.Text
        newecd2 = necd.ModString2(newecd2)
        If Len(newecd2) > 5 Then
            Dim strMessage As String = "New Error Code 2 Value Limited to 5 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim ecd2cnt As Integer
        sql = "select count(*) from ecd2 where ecd1id = '" & ecd1id & "' and ecd2 = '" & newecd2 & "'"
        ecd2cnt = necd.Scalar(sql)
        If ecd2cnt = 0 Then
            ecd2txt = lblecd2txt.Value
            sql = "insert into ecd2 (ecd2, ecd1id, ecddesc) values ('" & newecd2 & "','" & ecd1id & "','" & ecd2txt & "') select @@identity"
            ecd2id = necd.Scalar(sql)
            lblecd2id.Value = ecd2id

        Else
            sql = "select top 1 ecd2id from ecd2 where ecd1id = '" & ecd1id & "' and ecd2 = '" & newecd2 & "'"
            ecd2id = necd.Scalar(sql)
            lblecd2id.Value = ecd2id
        End If
        lblret.Value = "go"
    End Sub
End Class