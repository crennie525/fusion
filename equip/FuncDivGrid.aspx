<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FuncDivGrid.aspx.vb" Inherits="lucy_r12.FuncDivGrid" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>FuncDivGrid</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		<script  src="../scripts1/FuncDivGridaspx.js"></script>
        <link rel="stylesheet" type="text/css" href="../jqplot/easyui.css" />
        <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
        <script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="../jqplot/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="../scripts/showModalDialog.js"></script>
	</HEAD>
	<body class="tbg"  onload="checkit();" onunload="handleeq();">
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px" width="720">
				<TBODY>
					<tr>
						<td colspan="3">
							<table>
								<tr height="20">
									<td class="label" style="width: 140px" align="left" class="tbg"><asp:Label id="lang2382" runat="server">Add New Function</asp:Label></td>
									<td style="width: 140px" class="tbg"><asp:textbox id="txtnewfunc" runat="server" Width="130px" MaxLength="100"></asp:textbox></td>
									<td width="440" class="tbg"><asp:imagebutton id="btnAddEq" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton></td>
								</tr>
								<tr height="20">
									<td class="plainlabelblue"><asp:Label id="lang2383" runat="server">Selected Equipment:</asp:Label></td>
									<td class="plainlabelblue" colspan="2" id="tdcurr" runat="server"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3"><div id="spdiv" style="OVERFLOW: auto; WIDTH: 740px; HEIGHT: 300px">
								<asp:repeater id="rptrfunc" runat="server">
									<HeaderTemplate>
										<table cellspacing="0">
											<tr class="tbg" width="720" height="26">
												<td class="btmmenu plainlabel" align="center"><img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16"></td>
												<td class="btmmenu plainlabel" width="210"><asp:Label id="lang2384" runat="server">Function#</asp:Label></td>
												<td class="btmmenu plainlabel" width="510"><asp:Label id="lang2385" runat="server">Special Identifier</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg" height="20">
											<td class="plainlabel"><asp:CheckBox id="cb1" runat="server"></asp:CheckBox></td>
											<td class="plainlabel">&nbsp;
												<asp:LinkButton ID="Label4"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>' Runat = server>
												</asp:LinkButton></td>
											<td class="details">
												<asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"func_desc")%>' Runat = server>
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lblfuiditem" Text='<%# DataBinder.Eval(Container.DataItem,"func_id")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<AlternatingItemTemplate>
										<tr class="transrowblue" height="20">
											<td class="plainlabel"><asp:CheckBox id="cb2" runat="server"></asp:CheckBox></td>
											<td class="plainlabel transrowblue">&nbsp;
												<asp:LinkButton ID="Linkbutton1"  CommandName="Select" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>' Runat = server>
												</asp:LinkButton></td>
											<td class="plainlabel transrowblue">
												<asp:Label ID="Label3" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
												</asp:Label></td>
											<td class="details">
												<asp:Label ID="lblfuidalt" Text='<%# DataBinder.Eval(Container.DataItem,"func_id")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</AlternatingItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD></TR>
			<tr>
				<td colSpan="3"><img id="ibtnremove2" runat="server" src="../images/appbuttons/minibuttons/del.gif" onclick="verdel();">
					<IMG class="details" onmouseover="return overlib('Add a New Function Record')" onclick="GetFuncDiv();"
						onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/addnewbg1.gif"
						style="width: 20px"> <IMG onmouseover="return overlib('Lookup Function Records to Copy')" onclick="GetFuncCopy();"
						onmouseout="return nd()" height="20" alt="" src="../images/appbuttons/minibuttons/copybg.gif" style="width: 20px"
						id="imgcopy" runat="server"></td>
			</tr>
			</TBODY></TABLE> <input type="hidden" id="lblrd" runat="server" NAME="lblrd"> <input type="hidden" id="lbleqid" runat="server" NAME="lbleq">
			<input type="hidden" id="lblchk" runat="server" NAME="lblchk"> <input type="hidden" id="lbldchk" runat="server" NAME="lbldchk">
			<input type="hidden" id="lblsid" runat="server" NAME="lblsid"> <input type="hidden" id="lblcid" runat="server" NAME="lblcid">
			<input type="hidden" id="lbldid" runat="server" NAME="lbldid"> <input type="hidden" id="lblclid" runat="server" NAME="lblclid"><input type="hidden" id="lbluser" runat="server">
			<input type="hidden" id="lblfuid" runat="server"><input type="hidden" id="lblpar" runat="server">
			<input type="hidden" id="lbladdchk" runat="server"><input type="hidden" id="lblro" runat="server">
			<input type="hidden" id="lblpchk" runat="server"> <input type="hidden" id="lbllock" runat="server" NAME="lbllock">
			<input type="hidden" id="lbllockedby" runat="server" NAME="lbllockedby"> <input type="hidden" id="lblcbonoff" runat="server">
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblwho" runat="server" />
<input type="hidden" id="Hidden1" runat="server" />
<input type="hidden" id="lbldept" runat="server" />
<input type="hidden" id="lblcell" runat="server" />
<input type="hidden" id="lblloc" runat="server" />
<input type="hidden" id="lbleqidh" runat="server" />
<input type="hidden" id="lbleq" runat="server" />
<input type="hidden" id="lbllog" runat="server">
<input type="hidden" id="lblchld" runat="server" />
</form>
		
	</body>
</HTML>
