<%@ Page Language="vb" AutoEventWireup="false" Codebehind="equploadimagedialog.aspx.vb" Inherits="lucy_r12.equploadimagedialog" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>Upload Image Dialog</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script  src="../scripts/sessrefdialog.js"></script>
		<script  src="../scripts1/equploadimagedialogaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onunload="handleexit();" onload="resetsess();pageScroll();">
		<form id="form1" method="post" runat="server">
			<iframe id="ifimg" runat="server" width="100%" height="100%" frameBorder="no"></iframe>
			<iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
