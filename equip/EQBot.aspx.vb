

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.IO
Public Class EQBot
    Inherits System.Web.UI.Page
	Protected WithEvents ovid251 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents ovid250 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents btnaddasset As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2166 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2165 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2164 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2163 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2162 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2161 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2160 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2159 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2158 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2157 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2156 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2155 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2154 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2153 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2152 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2151 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2150 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2149 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2148 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2147 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2146 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2145 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2144 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2143 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2142 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpmostat1 As System.Web.UI.HtmlControls.HtmlInputHidden

    'Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Dim eq, chk, dchk, cid, sid, did, clid, sql, filt, dt, df, val, msg, eqid, lid, typ, chrg, ro, appstr, uid, who, fpc, crit, cadm As String
    Dim dr As SqlDataReader
    Dim echk As Integer
    Dim newstr, strto, saveas As String
    Dim eqr As New Utilities
    Dim Login, usr, ghostoff, xstatus As String
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcadm As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents appchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpareqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbcrit As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents tdmby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmdate As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td3 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td4 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td5 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td6 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td7 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td8 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbladdchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtneweq As System.Web.UI.WebControls.TextBox

    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btnlock As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnunlock As System.Web.UI.HtmlControls.HtmlImage

    Protected WithEvents cbtrans As System.Web.UI.HtmlControls.HtmlInputCheckBox
    Protected WithEvents ifgl As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtrans As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tduse As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtcharge As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbdev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbopt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblappstr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblapp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btndel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbldel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rbtdev As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rbtopt As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblpcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblovbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbliorders As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtiorder As System.Web.UI.WebControls.TextBox
    Protected WithEvents imgdel As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgsavdet As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblovtimgs As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrtimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnodel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqlid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ddoem As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tdgtt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdgttrb As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdopts As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents imgxtra As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbleqnum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcharge As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblisdown As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tcttimelbl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtcttime As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnsavechildtot As System.Web.UI.HtmlControls.HtmlImage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnAddEq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnsavechildtot As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txteqname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtecr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtfpc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoem As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmod As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtser As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnsave As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents imgeq As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnecr As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btngtt As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecrflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpic As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblghostoff As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblxstatus As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblischild As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldesig As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgtpmrev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ddpmostat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddmc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblpmostat As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblmc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdmast As System.Web.UI.HtmlControls.HtmlTableCell
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim eoem As New mmenu_utils_a
        Dim isoem As String = eoem.OEM
        If isoem = "1" Then
            ddoem.Visible = False
            txtoem.Visible = True
        Else
            ddoem.Visible = True
            txtoem.Visible = False
        End If
        Dim coi As String = eoem.COMPI
        If coi = "NISS" Then
            imgxtra.Attributes.Add("class", "view")
        Else
            imgxtra.Attributes.Add("class", "details")
        End If
        If coi = "GSK" Then
            lblghostoff.Value = "yes"
        End If
        If coi = "NISS_SYM" Then
            'imgtpmrev.Attributes.Add("class", "view")
            imgtpmrev.Visible = True
        Else
            'imgtpmrev.Attributes.Add("class", "details")
            imgtpmrev.Visible = False
        End If
        GetFSOVLIBS()



        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        'Dim ftr As Footer
        'ftr = Page.FindControl("Footer1")
        'ftr.loc = Footer.eloc.eqdivpg
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
            cadm = HttpContext.Current.Session("cadm").ToString()
            lblcadm.Value = cadm
            usr = HttpContext.Current.Session("username").ToString
            lbluser.Value = usr
            uid = HttpContext.Current.Session("uid").ToString
            lbluid.Value = uid
        Catch ex As Exception
            lbllog.Value = "no"
            'Exit Sub
        End Try
        If Request.Form("txtecr") = "return" Then
            txtecr.Text = ""
            SaveEq()
            PopPage()
        End If
        If Request.Form("lblpchk") = "ac" Then
            lblpchk.Value = ""
            eqr.Open()
            SaveEq()
            PopPage()
            eqr.Dispose()
        ElseIf Request.Form("lblpchk") = "lock" Then
            lblpchk.Value = ""
            eqr.Open()
            eqid = lbleqid.Value
            usr = lbluser.Value
            LockRecord(usr, eqid)
            SaveEq()
            PopPage()
            eqr.Dispose()
        ElseIf Request.Form("lblpchk") = "unlock" Then
            lblpchk.Value = ""
            eqr.Open()
            eqid = lbleqid.Value
            usr = lbluser.Value
            UnLockRecord(usr, eqid)
            SaveEq()
            PopPage()
            eqr.Dispose()
        ElseIf Request.Form("lblpchk") = "delimg" Then
            lblpchk.Value = ""
            eqr.Open()
            DelImg()
            LoadPics()
            eqr.Dispose()
            'lblchksav.Value = "yes"
        ElseIf Request.Form("lblpchk") = "checkpic" Then
            lblpchk.Value = ""
            eqr.Open()
            LoadPics()
            eqr.Dispose()
        ElseIf Request.Form("lblpchk") = "savedets" Then
            lblpchk.Value = ""
            eqr.Open()
            SaveDets()
            LoadPics()
            eqr.Dispose()
        ElseIf Request.Form("lblpchk") = "saveeq" Then
            lblpchk.Value = ""
            eqr.Open()
            SaveEq()
            eqr.Dispose()
        ElseIf Request.Form("lblpchk") = "changepmo" Then
            lblpchk.Value = ""
            eqr.Open()
            savePMO()
            eqr.Dispose()
        ElseIf Request.Form("lblpchk") = "changedesig" Then
            lblpchk.Value = ""
            eqr.Open()
            savechild()
            eqr.Dispose()
        End If
        Dim start As String = Request.QueryString("start").ToString

        If Not IsPostBack Then
            If start = "yes" Then
                Try
                    who = Request.QueryString("who").ToString
                    lblwho.Value = who
                    If who = "tab" Or who = "tabret" Then
                        tdgtt.Attributes.Add("class", "details")
                        tdgttrb.Attributes.Add("class", "details")
                        btnecr.Attributes.Add("class", "details")
                        tdopts.Attributes.Add("width", "744")
                    ElseIf who = "noloc" Or who = "eqready" Or who = "locready" Then
                        tdgtt.Attributes.Add("class", "details")
                        tdgttrb.Attributes.Add("class", "details")
                        tdopts.Attributes.Add("width", "744")
                    Else
                        tdgtt.Attributes.Add("class", "view label")
                        tdgttrb.Attributes.Add("class", "view label")
                    End If
                Catch ex As Exception

                End Try
                rbopt.Checked = True
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                lblro.Value = ro
                If ro = "1" Then
                    btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                    btnAddEq.Enabled = False
                    'btnupload.Disabled = True
                    cbtrans.Disabled = True
                    btnsave.Attributes.Add("src", "../images/appbuttons/minibuttons/savedisk1dis.gif")
                    btnsave.Attributes.Add("onclick", "")
                    'imgproc.Attributes.Add("src", "../images/appbuttons/minibuttons/uploaddis.gif")
                    'imgproc.Attributes.Add("onclick", "")
                    btndel.ImageUrl = "../images/appbuttons/minibuttons/deldis.gif"
                    btndel.Enabled = False
                End If
                appstr = HttpContext.Current.Session("appstr").ToString() '"all" '
                CheckApp(appstr)
                chk = Request.QueryString("chk").ToString
                lblchk.Value = chk
                dchk = Request.QueryString("dchk").ToString
                lbldchk.Value = dchk
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid

                lid = Request.QueryString("lid").ToString
                lbllid.Value = lid
                typ = Request.QueryString("typ").ToString
                lbltyp.Value = typ
                If typ = "loc" Then
                    tduse.InnerHtml = "Using Locations"
                Else
                    lbltyp.Value = "reg"
                    tduse.InnerHtml = "Using Departments"
                End If
                If dchk = "yes" Then
                    If chk = "yes" Then
                        did = Request.QueryString("did").ToString
                        lbldept.Value = did
                        clid = Request.QueryString("clid").ToString
                        lblclid.Value = clid
                    ElseIf chk = "no" Then
                        did = Request.QueryString("did").ToString
                        lbldept.Value = did

                    End If
                Else
                    lbldchk.Value = "no"
                End If
                If did <> "" Then
                    If lid <> "" And lid <> "0" Then
                        typ = "dloc"
                    Else
                        typ = "reg"
                    End If
                    If clid = "" Or clid = "0" Or clid = "no" Then
                        lblpar.Value = "dept"
                    Else
                        lblpar.Value = "cell"
                    End If
                Else
                    If lid <> "" And lid <> "0" Then
                        typ = "loc"
                        lbldchk.Value = "no"
                        lblpar.Value = "loc"
                    End If
                End If
                eqr.Open()
                PopOEM()
                PopStatus()
                CheckStat(cid)
                PopPage()
                'Label2.Visible = True
                'Label4.Visible = True
                'po.Visible = True
                'pr.Visible = True
                'ne.Visible = True
                'PopEq(dchk, chk)
                'Label2.Visible = True
                'Label4.Visible = True
                'po.Visible = True
                'pr.Visible = True
                'ne.Visible = True
                eqr.Dispose()
                'Label2.Text = Request.QueryString.ToString
            End If

        End If
        'temp until master/child revs complete
        uid = lbluid.Value
        If uid = "jkilgore" Or uid = "pmadmin1" Or uid = "rbryant" Then
            ddmc.Enabled = True
            ddmc.Attributes.Add("onchange", "checkchild(this.value);")
            lblmc.Attributes.Add("class", "labeltmp")
        Else
            ddmc.Enabled = False
            lblmc.Attributes.Add("class", "labelgrtmp")
        End If
        
        'tdmast.InnerHtml = "N/A"
        '******************
        btnunlock.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov270", "EQBot.aspx.vb") & "')")
        btnunlock.Attributes.Add("onmouseout", "return nd()")
        btnlock.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov271", "EQBot.aspx.vb") & "')")
        btnlock.Attributes.Add("onmouseout", "return nd()")
        btnsave.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov272", "EQBot.aspx.vb") & "')")
        btnsave.Attributes.Add("onmouseout", "return nd()")
        btnsave.Attributes.Add("onclick", "FreezeScreen('Your Data is Being Processed...');")
        btngtt.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov273", "EQBot.aspx.vb") & "')")
        btngtt.Attributes.Add("onmouseout", "return nd()")
        'X.Web.UI.PersistentStatePage.RemoveSavedPageState("CompDiv.aspx")
        'X.Web.UI.PersistentStatePage.RemoveSavedPageState("FuncDiv.aspx")
        'btnAddEq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnAddEq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'btngtt.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yjtotaskssm.gif'")
        'btngtt.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/jtotaskssm.gif'")
        btndel.Attributes.Add("onmouseover", "return overlib('" & tmod.getov("cov274", "EQBot.aspx.vb") & "')")
        btndel.Attributes.Add("onmouseout", "return nd()")
    End Sub
    Private Sub PopOEM()
        'eqr.Open()
        sql = "select * from invcompanies where type = 'M' order by name"
        dr = eqr.GetRdrData(sql)
        ddoem.DataSource = dr
        ddoem.DataTextField = "name"
        ddoem.DataValueField = "name"
        ddoem.DataBind()
        dr.Close()
        ddoem.Items.Insert(0, "Select OEM")
    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr865" , "EQBot.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim old As String = oldiord 'lbloldorder.Value
            Dim neword As String = txtiorder.Text
            eqid = lbleqid.Value
            typ = "eq"
            sql = "usp_reordereqimg '" & typ & "','" & eqid & "','" & neword & "','" & old & "'"
            eqr.Update(sql)
        End If
    End Sub
    Private Sub DelImg()
        eqid = lbleqid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        typ = "eq"
        sql = "usp_deleqimg '" & typ & "','" & eqid & "','" & pid & "','" & old & "'"
        eqr.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
        lblcurrp.Value = "0"
    End Sub
    Private Sub CheckApp(ByVal appstr)
        Dim apparr() As String = appstr.Split(",")
        Dim app As Integer = 0
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "dev" Then
                    rbdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "opt" Then
                    rbopt.Disabled = False
                    rbopt.Checked = True
                    app += 1
                ElseIf apparr(i) = "tpd" Then
                    rbtdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "tpo" Then
                    rbtopt.Disabled = False
                    app += 1
                End If
            Next
            If app = 0 Then
                lblappstr.Value = "no"
            Else
                lblappstr.Value = "yes"
                If rbopt.Disabled = True And rbdev.Disabled = True Then
                    If rbtopt.Disabled = True Then
                        rbtdev.Checked = True
                        lblapp.Value = "rbdev"
                    Else
                        rbtopt.Checked = True
                        lblapp.Value = "rbopt"
                    End If
                Else
                    If rbopt.Disabled = True Then
                        rbdev.Checked = True
                        lblapp.Value = "pmdev"
                    Else
                        rbopt.Checked = True
                        lblapp.Value = "pmopt"
                    End If
                End If
            End If

        Else
            lblappstr.Value = "yes"
            lblapp.Value = "pmopt"
            rbdev.Disabled = False
            rbopt.Disabled = False
            rbopt.Checked = True
            rbtdev.Disabled = False
            rbtopt.Disabled = False
        End If
    End Sub
    Private Sub CheckAppOLD(ByVal appstr)
        Dim apparr() As String = appstr.Split(",")
        Dim app As Integer = 0
        'eq,dev,opt,inv
        If appstr <> "all" Then
            Dim i As Integer
            For i = 0 To apparr.Length - 1
                If apparr(i) = "dev" Then
                    rbdev.Disabled = False
                    app += 1
                ElseIf apparr(i) = "opt" Then
                    rbopt.Disabled = False
                    rbopt.Checked = True
                    app += 1
                End If
            Next
            If app = 0 Then
                lblappstr.Value = "no"
                lblapp.Value = "none"
            Else
                lblappstr.Value = "yes"
                If rbopt.Disabled = True Then
                    If rbdev.Disabled = True Then

                    End If
                    rbdev.Checked = True
                    lblapp.Value = "pmdev"
                Else
                    rbopt.Checked = True
                    lblapp.Value = "pmopt"
                End If
            End If

        Else
            lblappstr.Value = "yes"
            rbdev.Disabled = False
            rbopt.Disabled = False
            rbopt.Checked = True
            lblapp.Value = "pmopt"
        End If
    End Sub
    Private Sub CheckStat(ByVal cid As String)
        Dim cnt, madmn As Integer
        sql = "select ecrflg from pmsysvars"
        cnt = eqr.Scalar(sql)
        If cnt = 0 Then
            btnecr.Src = "../images/appbuttons/minibuttons/cant.gif"
            btnecr.Attributes.Add("title", "ECR levels and weights have not been defined")
            lblecrflg.Value = "noecr"
        End If
        usr = lbluser.Value
        If usr <> "pmadmin" Then
            uid = lbluid.Value
            sql = "select isnull(admin, 0) from pmsysusers where uid = '" & uid & "'"
            madmn = eqr.Scalar(sql)
        Else
            madmn = "1"
        End If
        If madmn <> "1" Then

        End If
        
    End Sub
    Private Sub PopStatus()
        'eqr.Open()
        sql = "select * from eqstatus"
        dr = eqr.GetRdrData(sql)
        ddlstat.DataSource = dr
        ddlstat.DataTextField = "status"
        ddlstat.DataValueField = "statid"
        ddlstat.DataBind()
        dr.Close()
        ddlstat.Items.Insert(0, "Select Status")
    End Sub
    Private Sub PopEq(ByVal dchk As String, ByVal chk As String)
        If dchk = "yes" Then
            clid = lblclid.Value
            If chk = "yes" And clid <> "no" Then
                msg = "Cell"
                clid = lblclid.Value
                filt = " where cellid = '" & clid & "'"
            ElseIf chk = "no" Then
                msg = "Department"
                did = lbldept.Value
                filt = " where dept_id = '" & did & "'"
            End If
        Else
            msg = "Site"
            sid = lblsid.Value
            filt = " where siteid = '" & sid & "'"
        End If
        Dim eqcnt As Integer
        sql = "select count(*) from equipment" & filt
        eqcnt = eqr.Scalar(sql)
        If eqcnt = 0 Then
            Dim strMessage As String = tmod.getmsg("cdstr866", "EQBot.aspx.vb") & " " & msg & " Yet"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        dt = "Equipment"
        val = "eqid , eqnum "
        dr = eqr.GetList(dt, val, filt)
        'ddeq.DataSource = dr
        'ddeq.DataTextField = "eqnum"
        'ddeq.DataValueField = "eqid"
        'ddeq.DataBind()
        dr.Close()
        'ddeq.Items.Insert(0, "Select Equipment")
        'eqr.Dispose()
    End Sub
    Public Function GetEqRecord(ByVal eq As String) As SqlDataReader
        Dim desig, mstr, childtot, pmostat As String
        sql = "select isnull(desig, 0) as 'desig', desigmstr, pmostat from equipment where eqid = '" & eq & "'"
        dr = eqr.GetRdrData(sql)
        While dr.Read
            desig = dr.Item("desig").ToString
            mstr = dr.Item("desigmstr").ToString
            pmostat = dr.Item("pmostat").ToString
        End While
        dr.Close()
        lblpmostat1.Value = pmostat
        lbldesig.Value = desig
        If desig = "1" Or desig = "" Or desig = "0" Or desig = "3" Then
            lblischild.Value = "no"
            lblchld.Value = "0"
            sql = "select e.*, " _
            + "cph = (select s.phonenum from [pmsysusers] s  where  s.username = e.createdby), " _
            + "cm = (select s.email from [pmsysusers] s  where  s.username = e.createdby), " _
            + "mph = (select s.phonenum from [pmsysusers] s  where  s.username = e.modifiedby), " _
            + "mm = (select s.email from [pmsysusers] s  where  s.username = e.modifiedby) " _
            + "from " _
            + "equipment e where e.eqid = '" & eq & "'"
            txtcttime.Enabled = False
            btnsavechildtot.Enabled = False
        ElseIf desig = "2" Then
            lblischild.Value = "yes"
            lblchld.Value = eq
            If (mstr = "" Or mstr = "0") Then
                sql = "select e.*, " _
            + "cph = (select s.phonenum from [pmsysusers] s  where  s.username = e.createdby), " _
            + "cm = (select s.email from [pmsysusers] s  where  s.username = e.createdby), " _
            + "mph = (select s.phonenum from [pmsysusers] s  where  s.username = e.modifiedby), " _
            + "mm = (select s.email from [pmsysusers] s  where  s.username = e.modifiedby), " _
            + "chld = (select eqnum from equipment where eqid = '" & eq & "') " _
            + "from " _
            + "equipment e where e.eqid = '" & eq & "'"
            Else
                sql = "select e.*, " _
            + "cph = (select s.phonenum from [pmsysusers] s  where  s.username = e.createdby), " _
            + "cm = (select s.email from [pmsysusers] s  where  s.username = e.createdby), " _
            + "mph = (select s.phonenum from [pmsysusers] s  where  s.username = e.modifiedby), " _
            + "mm = (select s.email from [pmsysusers] s  where  s.username = e.modifiedby), " _
            + "chld = (select eqnum from equipment where eqid = '" & eq & "') " _
            + "from " _
            + "equipment e where e.eqid = '" & mstr & "'"
            End If
            txtcttime.Enabled = True
            btnsavechildtot.Enabled = True
        End If
        
        dr = eqr.GetRdrData(sql)
        Return dr
    End Function
    Public Function chkcnt(ByVal eqid As String, ByVal eq As String) As Integer
        chk = lblchk.Value
        dchk = lbldchk.Value
        If dchk = "yes" Then
            clid = lblclid.Value
            If chk = "yes" And clid <> "no" Then
                clid = lblclid.Value
                filt = "where eqnum = '" & eq & "'" ' and cellid = '" & clid & "'"
            ElseIf chk = "no" Then
                did = lbldept.Value
                filt = "where eqnum = '" & eq & "'" ' and dept_id = '" & did & "'"
            End If
        Else
            sid = lblsid.Value
            filt = "where eqnum = '" & eq & "'" ' and siteid = '" & sid & "'"
        End If

        sql = "select count(*) from equipment " & filt
        echk = eqr.Scalar(sql)
        lid = lbllid.Value
        If lid = "0" Then lid = ""
        If echk = 0 And lid <> "" Then
            sql = "select count(*) from pmlocations where location = '" & eq & "'"
            echk = eqr.Scalar(sql)
        End If
        Return echk
    End Function
    Private Sub CheckDummy(ByVal field As String)
        sid = lblsid.Value
        cid = "0" ' lblcid.value
        Dim cnt As Integer
        If field = "cell" Then
            did = lbldept.Value
            sql = "Select count(*) from cells where " _
            + "siteid = '" & sid & "' and dept_id = '" & did & "' " _
            + "and cell_name = 'No Cells'"
            cnt = eqr.Scalar(sql)
            If cnt = 0 Then
                sql = "insert into cells (compid, siteid, dept_id, cell_name, cell_desc) values " _
                + "('" & cid & "', '" & sid & "', '" & did & "', 'No Cells', 'Not Using Cells') select @@identity as 'identity'"
                clid = eqr.Scalar(sql)
                lblclid.Value = clid
            End If
        ElseIf field = "dept" Then
            sql = "Select count(*) from dept where " _
            + "siteid = '" & sid & "' " _
            + "and dept_line = 'No Depts'"
            cnt = eqr.Scalar(sql)
            If cnt = 0 Then
                sql = "insert into dept (compid, siteid, dept_line, dept_desc) values " _
                + "('" & cid & "', '" & sid & "', 'No Depts, 'Not Using Departments') select @@identity as 'identity'"
                did = eqr.Scalar(sql)
                lbldept.Value = did
            End If
        End If
    End Sub
    Private Sub btnAddEq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddEq.Click
        eqr.Open()
        Dim eq As String = txtneweq.Text
        eq = eqr.ModString3(eq)

        If Len(eq) > 0 Then
            chk = lblchk.Value
            dchk = lbldchk.Value
            sid = lblsid.Value
            cid = "0" ' lblcid.value
            lid = lbllid.Value
            If lid = "0" Then lid = ""
            'If lid = "" Then lid = "0"
            typ = lbltyp.Value
            Dim user As String = lbluser.Value
            Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
            If typ = "reg" Or typ = "dloc" Then
                If dchk = "yes" Then
                    did = lbldept.Value
                    If chk = "yes" Then
                        clid = lblclid.Value
                    ElseIf chk = "no" Then
                        CheckDummy("cell")
                        clid = lblclid.Value
                        If clid = "no" Then
                            clid = "0"
                        End If
                    End If
                Else
                    CheckDummy("dept")
                    did = lbldept.Value
                End If
            End If

            filt = "where eqnum = '" & eq & "'"
            sql = "select count(*) from equipment " & filt
            echk = eqr.Scalar(sql)
            If echk = 0 And lid <> "" Then
                sql = "select count(*) from pmlocations where location = '" & eq & "'"
                echk = eqr.Scalar(sql)
            End If
            Dim eqid As Integer
            If echk = 0 Then
                sql = "addneweq '" & cid & "', '" & eq & "', '" & clid & "', '" & sid & "', '" & did & "', '" & lid & " ', '" & ustr & "', '" & ustr & "', '" & typ & "'"
                eqid = eqr.Scalar(sql)
                lbleqid.Value = eqid
                PopPage()

                '*** Multi Add ***
                cbtrans.Visible = False
                '*** Multi Add End ***

                Dim copy2a As New Utilities
                sql = "exec usp_chkrtoneqadd '" & sid & "'"
                Try
                    copy2a.Open()
                    copy2a.Update(sql)
                    copy2a.Dispose()
                Catch ex As Exception

                End Try

                txtneweq.Text = ""
                lbladdchk.Value = "yes"
                PopEq(dchk, chk)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr867" , "EQBot.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            eqr.Dispose()
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr868" , "EQBot.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Public Function GetEqDesc(ByVal eq As String) As SqlDataReader
        dt = "Equipment"
        df = "eqdesc"
        filt = " where eqid = '" & eq & "'"
        dr = eqr.GetDesc(dt, df, filt)
        Return dr
    End Function
    Private Sub PopPage()
        PopAC()
        'eqr.Open()
        'eq = ddeq.SelectedValue
        eq = lbleqid.Value '= eq
        'lblpar.Value = "eq"
        'GetEqDesc(eq)
        'If dr.Read Then
        'lbleqdesc.Text = dr.Item("eqdesc").ToString
        'End If
        'dr.Close()
        Dim stati As String
        dr = GetEqRecord(eq)
        Dim chld As String
        chld = lblischild.Value
        Dim ac As String
        Dim haspm As String
        Dim desig, desigmstr, childtot As String
        '*** Multi Add ***
        Dim mdb, trans, transdate, transstatus, transstatusdate, language, eqlid, pmostat, pmostat1 As String
        '*** Multi Add End ***

        If dr.Read Then
            'childtot = dr.Item("childtot").ToString
            txtcttime.Text = dr.Item("childtot").ToString
            desig = dr.Item("desig").ToString
            desigmstr = dr.Item("desigmstr").ToString
            crit = dr.Item("critical").ToString
            fpc = dr.Item("fpc").ToString
            txtfpc.Text = fpc
            xstatus = dr.Item("xstatus").ToString
            lblxstatus.Value = xstatus
            eqlid = dr.Item("locid").ToString
            lbleqlid.Value = eqlid
            If chld = "yes" Then
                txteqname.Text = dr.Item("chld").ToString
                lbleqnum.Value = dr.Item("chld").ToString
                tdmast.InnerHtml = dr.Item("eqnum").ToString
                lbleqid.Value = dr.Item("eqid").ToString
                'txtcttime.Text = dr.Item("childtot").ToString
                txtcttime.Enabled = True
            Else
                txteqname.Text = dr.Item("eqnum").ToString
                lbleqnum.Value = dr.Item("eqnum").ToString
                txtcttime.Enabled = False
            End If

            txteqdesc.Text = dr.Item("eqdesc").ToString
            txtspl.Text = dr.Item("spl").ToString
            txtloc.Text = dr.Item("location").ToString
            txtoem.Text = dr.Item("oem").ToString

            pmostat = dr.Item("pmostat").ToString
            If pmostat = "" Then
                pmostat = "0"
            End If
            If chld = "yes" Then
                pmostat1 = lblpmostat1.Value
                ddpmostat.SelectedValue = pmostat1
                ddpmostat.Attributes.Add("onchange", "getpmostat(this.value);")
            Else
                ddpmostat.SelectedValue = pmostat
            End If


            Try
                ddoem.SelectedValue = dr.Item("oem").ToString
            Catch ex As Exception

            End Try

                txtmod.Text = dr.Item("model").ToString
                txtser.Text = dr.Item("serial").ToString
                lblpic.Value = dr.Item("picurl").ToString
                'lblcnt.Text = dr.Item("piccnt").ToString
                txtecr.Text = dr.Item("ecr").ToString
                tdcby.InnerHtml = dr.Item("createdby").ToString
                tdcdate.InnerHtml = dr.Item("createdate").ToString
                tdmby.InnerHtml = dr.Item("modifiedby").ToString
                tdmdate.InnerHtml = dr.Item("modifieddate").ToString
                txtcharge.Text = dr.Item("chargenum").ToString
                lblcharge.Value = dr.Item("chargenum").ToString
                Try
                    ac = dr.Item("acid").ToString
                    ddac.SelectedValue = dr.Item("acid").ToString
                Catch ex As Exception
                    ddac.SelectedIndex = 0
                End Try

                lblpareqid.Value = dr.Item("origpareqid").ToString
                Try
                    stati = dr.Item("statid").ToString
                    ddlstat.SelectedValue = stati
                Catch ex As Exception

                End Try
                lbllock.Value = dr.Item("locked").ToString
                lbllockedby.Value = dr.Item("lockedby").ToString

                '*** Multi Add ***
                mdb = dr.Item("mdb").ToString
                lbldb.Value = dr.Item("mdbname").ToString
                trans = dr.Item("trans").ToString
                transdate = dr.Item("transdate").ToString
                transstatus = dr.Item("transstatus").ToString
                transstatusdate = dr.Item("transstatusdate").ToString
                language = dr.Item("mdblang").ToString
                haspm = dr.Item("haspm").ToString
                '*** Multi Add End ***

                Td3.InnerHtml = dr.Item("cph").ToString
                Td4.InnerHtml = dr.Item("cm").ToString
                Td6.InnerHtml = dr.Item("mph").ToString
                Td7.InnerHtml = dr.Item("mm").ToString
        End If
        dr.Close()
        Dim uid As String = lbluid.Value
        If chld = "yes" Then

            If uid = "jkilgore" Or uid = "pmadmin1" Or uid = "rbryant" Then
                ddpmostat.Enabled = True
                'ddmc.Attributes.Add("onchange", "checkchild(this.value);")
                lblpmostat.Attributes.Add("class", "labeltmp")
            Else
                ddpmostat.Enabled = False
                lblpmostat.Attributes.Add("class", "labelgrtmp")
            End If

            Dim cheqid As String = lblchld.Value
            sql = "select isnull(childtot, 0) from equipment where eqid = '" & cheqid & "'"
            childtot = eqr.strScalar(sql)
            txtcttime.Text = childtot
        End If
        desig = lbldesig.Value
        'Try
        'If chld = "yes" Then
        'ddmc.SelectedValue = "2"
        'Else
        ddmc.SelectedValue = desig
        'End If

        'Catch ex As Exception
        'ddmc.SelectedValue = "0"
        'End Try

        btndel.Attributes.Add("onclick", "javascript:return " & _
        "confirm('Are you sure you want to delete Equipment Record " & txteqname.Text & "\nWith all Functions, Components, and Tasks?'")
        If crit = "1" Then
            cbcrit.Checked = True
        Else
            cbcrit.Checked = False
        End If
        Dim pmcnt As Integer = 0
        If haspm = "1" Then
            sql = "select count(*) from pm where eqid = '" & eq & "'"
            pmcnt = eqr.Scalar(sql)
        End If
        If pmcnt = 0 Then
            'btndel.Attributes.Add("onclick", "alert('This Equipment Record Has PM Records and Cannot Be Deleted')")
            'lblnodel.Value = "nodel"
            btndel.Attributes.Add("onclick", "javascript:return " & _
            "confirm('Are you sure you want to delete Equipment Record " & _
            txteqname.Text & "\nWith " & _
            "Functions, Component(s) and " & _
            "Task(s)?')")
            lblnodel.Value = "ok"
        Else
            btndel.Attributes.Add("onclick", "alert('This Equipment Record Has PM Records and Cannot Be Deleted')")
            lblnodel.Value = "nodel"
        End If



        '*** Multi Add ***
        ghostoff = lblghostoff.Value
        Try
            If mdb = "1" And ghostoff <> "yes" Then
                If trans = "0" Then
                    cbtrans.Visible = True
                Else
                    cbtrans.Visible = False
                    tdtrans.InnerHtml = ""
                End If
            ElseIf ghostoff = "yes" Then
                xstatus = lblxstatus.Value
                cbtrans.Visible = True
                If xstatus = "yes" Then
                    cbtrans.Checked = True
                Else
                    cbtrans.Checked = False
                End If
                lang2143.Text = "Turn Off PMO Ghost Write"
                cbtrans.Attributes.Add("onclick", "checkghost();")
            Else
                cbtrans.Visible = False
                tdtrans.InnerHtml = ""
            End If
        Catch ex As Exception

        End Try

        '*** Multi Trans End ***

        Dim user As String = lbluser.Value

        Dim lockby As String = lbllockedby.Value
        Dim lock As String = lbllock.Value
        ro = lblro.Value
        cadm = lblcadm.Value
        If lock = "1" Or chld = "yes" Then
            If lockby = user And chld <> "yes" Then ' Or uid = "pmadmin"
                lbllock.Value = "0"
                btnunlock.Attributes.Add("class", "details")
                btnlock.Attributes.Add("class", "btnview")
            Else
                If chld = "yes" Then
                    lockby = "PMO Admin"

                End If
                Dim strMessage As String = tmod.getmsg("cdstr869", "EQBot.aspx.vb") & " " & lockby & "."
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                btnsave.Attributes.Add("class", "details")
                'btnupload.Disabled = True
                btnecr.Attributes.Add("class", "details")
                btnunlock.Attributes.Add("class", "details")
                btnlock.Attributes.Add("class", "btnview")
                btnlock.Attributes.Add("src", "../images/appbuttons/minibuttons/lockg.gif")
                btnlock.Attributes.Add("onclick", "")
                btndel.ImageUrl = "../images/appbuttons/minibuttons/deldis.gif"
                btndel.Enabled = False
            End If
        ElseIf lock = "0" OrElse Len(lock) = 0 Then
            btnsave.Attributes.Add("class", "btnview")
            btnecr.Attributes.Add("class", "btnview")
            btndel.ImageUrl = "../images/appbuttons/minibuttons/deldis.gif"
            btndel.Enabled = True

            btnunlock.Attributes.Add("class", "btnview")
            btnlock.Attributes.Add("class", "details")
            If ro = "1" Then
                btnunlock.Attributes.Add("src", "../images/appbuttons/minibuttons/unlockg.gif")
                btnunlock.Attributes.Add("onclick", "")
            End If

            'LockRecord(user, eq)
        End If
        'Dim pic As String
        'sql = "select top 1 picurltm from pmPictures where funcid is null and comid is null and eqid = '" & eq & "' " _
        '+ "order by pic_id"
        'pic = eqr.strScalar(sql)
        'lblpic.Value = pic
        'Dim piccnt As Integer
        'sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        'piccnt = eqr.Scalar(sql)
        'lblcnt.Text = piccnt

        'eqr.Dispose()
        'If Len(pic) <> 0 Then
        'imgeq.Src = pic
        'lblcur.Text = "1"
        'Else
        'lblcur.Text = "0"
        'lblcnt.Text = "0"
        'End If

        LoadPics()
        'lbleqid.Value = "1494"
    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        'imgeq.Attributes.Add("src", "../images/appimages/eqimg1.gif")
        Dim lang As String = lblfslang.Value
        If lang = "eng" Then
            imgeq.Attributes.Add("src", "../images2/eng/eqimg1.gif")
        ElseIf lang = "fre" Then
            imgeq.Attributes.Add("src", "../images2/fre/eqimg1.gif")
        ElseIf lang = "ger" Then
            imgeq.Attributes.Add("src", "../images2/ger/eqimg1.gif")
        ElseIf lang = "ita" Then
            imgeq.Attributes.Add("src", "../images2/ita/eqimg1.gif")
        ElseIf lang = "spa" Then
            imgeq.Attributes.Add("src", "../images2/spa/eqimg1.gif")
        End If

        Dim img, imgs, picid As String

        eqid = lbleqid.Value
        Dim pcnt As Integer
        sql = "select count(*) from pmpictures where funcid is null and comid is null and eqid = '" & eqid & "'"
        pcnt = eqr.Scalar(sql)
        lblpcnt.Value = pcnt

        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lbloldorder.Value = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from pmpictures p where p.funcid is null and p.comid is null and p.eqid = '" & eqid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = eqr.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    'If File.Exists(img) Then
                    imgeq.Attributes.Add("src", img)
                    'End If
                    'imgeq.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    txtiorder.Text = order
                End If
        If imgs = "" Then
            imgs = picid & ";" & img
        Else
            imgs += "~" & picid & ";" & img
        End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub
    Private Sub LockRecord(ByVal user As String, ByVal eqid As String)
        sql = "update Equipment set locked = '1', lockedby = '" & user & "' where eqid = '" & eqid & "'"
        eqr.Update(sql)
        lbllockchk.Value = "yes"
       
    End Sub
    Private Sub UnLockRecord(ByVal user As String, ByVal eqid As String)
        sql = "update Equipment set locked = '0', lockedby = '" & user & "' where eqid = '" & eqid & "'"
        eqr.Update(sql)
        lbllockchk.Value = "yes"
       
    End Sub
    Private Sub PopAC()
        cid = "0" ' lblcid.value
        sql = "select * from pmAssetClass"
        dr = eqr.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        Try
            ddac.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    
    Private Sub SaveChildTot()
        eqid = lblchld.Value 'lbleqid.Value
        Dim childtot As String
        childtot = txtcttime.Text
        Dim childtoti As Long
        Try

            childtoti = System.Convert.ToInt64(childtot)
        Catch ex As Exception
            Dim strMessage As String = "Child Total Must be a Non Decimal Numeric Value"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "update equipment set childtot = '" & childtot & "' where eqid = '" & eqid & "'"
        eqr.Update(sql)
    End Sub
    Private Sub SaveEq()
        Dim eqid, eq, eqd, loc, oem, model, ser, spl, ecr, ac, acid, lid, chrg, oeq, pmostat, childtot As String
        'childtot = txtcttime.Text
        'need int check here
        

        eq = txteqname.Text
        oeq = lbleqnum.Value
        If Len(eq) <> 0 Then
            eq = eqr.ModString3(eq)
            If cbcrit.Checked = True Then
                crit = "1"
            Else
                crit = "0"
            End If
            ecr = txtecr.Text
            Dim etst As Integer = Len(ecr)

            If Len(ecr) = 0 Then
                ecr = "0"
            End If
            Dim user As String = lbluser.Value
            Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
            'eqid = ddeq.SelectedValue
            eqid = lbleqid.Value
            If oeq <> eq Then
                echk = chkcnt(eqid, eq)
            Else
                echk = "0"
            End If

            eqd = txteqdesc.Text
            eqd = eqr.ModString1(eqd)
            chrg = lblcharge.Value
            chrg = eqr.ModString2(chrg)
            loc = txtloc.Text
            loc = eqr.ModString1(loc)
            oem = ddoem.SelectedValue.ToString
            If oem <> "0" And oem <> "Select OEM" Then
                oem = eqr.ModString1(oem)
            Else
                oem = txtoem.Text
                oem = eqr.ModString1(oem)
                'oem = ""
            End If
            model = txtmod.Text
            model = eqr.ModString1(model)
            ser = txtser.Text
            ser = eqr.ModString1(ser)
            spl = txtspl.Text
            spl = eqr.ModString1(spl)
            eqid = lbleqid.Value
            xstatus = lblxstatus.Value
            If ddac.SelectedIndex <> 0 Then
                ac = ddac.SelectedItem.ToString
                acid = ddac.SelectedValue.ToString
                ac = Replace(ac, "'", Chr(180), , , vbTextCompare)
            Else
                ac = "Select"
                acid = "0"
            End If
            pmostat = ddpmostat.SelectedValue.ToString
            '*** Multi Add ***
            Dim t As String
            ghostoff = lblghostoff.Value
            If cbtrans.Visible = True And ghostoff <> "yes" Then
                If cbtrans.Checked = True Then
                    t = "1"
                Else
                    t = "0"
                End If
            Else
                t = ""
            End If
            '*** Multi Add End ***
            Dim stati, stat As String
            If ddlstat.SelectedIndex <> 0 Then
                stati = ddlstat.SelectedValue.ToString
                stat = ddlstat.SelectedItem.ToString
                stat = Replace(stat, "'", Chr(180), , , vbTextCompare)
            Else
                stati = ""
                stat = ""
            End If
            Dim fpc As String = txtfpc.Text
            fpc = eqr.ModString3(fpc)
            If Len(fpc) > 50 Then
                Dim strMessage As String = "Column Number Limited to 50 Characters"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
           
            Dim cmd As New SqlCommand
            cmd.CommandText = "update equipment set " _
            + "eqnum = @eq, " _
            + "eqdesc = @eqd, " _
            + "location = @loc, " _
            + "oem = @oem, " _
            + "model = @model, " _
            + "serial = @ser, " _
            + "spl = @spl, " _
            + "ecr = @ecr, " _
            + "statid = @stati, " _
            + "status = @stat, " _
            + "acid = @acid, " _
            + "assetclass = @ac, " _
            + "chargenum = @chrg, " _
            + "trans = @t, " _
            + "modifiedby = @ustr, " _
            + "modifieddate = getDate(), " _
            + "xstatus = @xstatus, " _
            + "fpc = @fpc, " _
            + "pmostat = @pmostat, " _
            + "critical = @crit " _
            + "where eqid = @eqid " _
            + "update equipment  set eqstatindex = (" _
            + "select eqstatindex from eqstatus where " _
            + "statid = @stati) where eqid = @eqid"
            '            + "childtot = @childtot, " _
            Dim param01 = New SqlParameter("@eq", SqlDbType.VarChar)
            If eq = "" Then
                param01.Value = System.DBNull.Value
            Else
                param01.Value = eq
            End If
            cmd.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@eqd", SqlDbType.VarChar)
            If eqd = "" Then
                param02.Value = System.DBNull.Value
            Else
                param02.Value = eqd
            End If
            cmd.Parameters.Add(param02)
            Dim param03 = New SqlParameter("@loc", SqlDbType.VarChar)
            If loc = "" Then
                param03.Value = System.DBNull.Value
            Else
                param03.Value = loc
            End If
            cmd.Parameters.Add(param03)
            Dim param04 = New SqlParameter("@oem", SqlDbType.VarChar)
            If oem = "" Then
                param04.Value = System.DBNull.Value
            Else
                param04.Value = oem
            End If
            cmd.Parameters.Add(param04)
            Dim param05 = New SqlParameter("@ser", SqlDbType.VarChar)
            If ser = "" Then
                param05.Value = System.DBNull.Value
            Else
                param05.Value = ser
            End If
            cmd.Parameters.Add(param05)
            Dim param06 = New SqlParameter("@spl", SqlDbType.VarChar)
            If spl = "" Then
                param06.Value = System.DBNull.Value
            Else
                param06.Value = spl
            End If
            cmd.Parameters.Add(param06)
            Dim param07 = New SqlParameter("@ecr", SqlDbType.VarChar)
            If ecr = "" Then
                param07.Value = System.DBNull.Value
            Else
                param07.Value = ecr
            End If
            cmd.Parameters.Add(param07)
            Dim param08 = New SqlParameter("@stati", SqlDbType.VarChar)
            If stati = "" Then
                param08.Value = System.DBNull.Value
            Else
                param08.Value = stati
            End If
            cmd.Parameters.Add(param08)
            Dim param09 = New SqlParameter("@stat", SqlDbType.VarChar)
            If stat = "" Then
                param09.Value = System.DBNull.Value
            Else
                param09.Value = stat
            End If
            cmd.Parameters.Add(param09)
            Dim param010 = New SqlParameter("@acid", SqlDbType.VarChar)
            If acid = "" Then
                param010.Value = System.DBNull.Value
            Else
                param010.Value = acid
            End If
            cmd.Parameters.Add(param010)
            Dim param011 = New SqlParameter("@ac", SqlDbType.VarChar)
            If ac = "" Then
                param011.Value = System.DBNull.Value
            Else
                param011.Value = ac
            End If
            cmd.Parameters.Add(param011)
            Dim param012 = New SqlParameter("@chrg", SqlDbType.VarChar)
            If chrg = "" Then
                param012.Value = System.DBNull.Value
            Else
                param012.Value = chrg
            End If
            cmd.Parameters.Add(param012)
            Dim param013 = New SqlParameter("@t", SqlDbType.VarChar)
            If t = "" Then
                param013.Value = System.DBNull.Value
            Else
                param013.Value = t
            End If
            cmd.Parameters.Add(param013)
            Dim param014 = New SqlParameter("@ustr", SqlDbType.VarChar)
            If ustr = "" Then
                param014.Value = System.DBNull.Value
            Else
                param014.Value = ustr
            End If
            cmd.Parameters.Add(param014)
            Dim param015 = New SqlParameter("@eqid", SqlDbType.VarChar)
            If eqid = "" Then
                param015.Value = System.DBNull.Value
            Else
                param015.Value = eqid
            End If
            cmd.Parameters.Add(param015)
            Dim param016 = New SqlParameter("@xstatus", SqlDbType.VarChar)
            If xstatus = "" Then
                param016.Value = System.DBNull.Value
            Else
                param016.Value = xstatus
            End If
            cmd.Parameters.Add(param016)
            Dim param017 = New SqlParameter("@model", SqlDbType.VarChar)
            If model = "" Then
                param017.Value = System.DBNull.Value
            Else
                param017.Value = model
            End If
            cmd.Parameters.Add(param017)
            Dim param018 = New SqlParameter("@fpc", SqlDbType.VarChar)
            If fpc = "" Then
                param018.Value = System.DBNull.Value
            Else
                param018.Value = fpc
            End If
            cmd.Parameters.Add(param018)

            Dim param020 = New SqlParameter("@pmostat", SqlDbType.VarChar)
            If pmostat = "" Then
                param020.Value = System.DBNull.Value
            Else
                param020.Value = pmostat
            End If
            cmd.Parameters.Add(param020)

            Dim param019 = New SqlParameter("@crit", SqlDbType.VarChar)
            If crit = "" Then
                param019.Value = System.DBNull.Value
            Else
                param019.Value = crit
            End If
            cmd.Parameters.Add(param019)

            Dim param021 = New SqlParameter("@childtot", SqlDbType.VarChar)
            If childtot = "" Then
                param021.Value = System.DBNull.Value
            Else
                param021.Value = childtot
            End If
            cmd.Parameters.Add(param021)
            'Else
            ' sql = "update equipment set " _
            '+ "eqnum = '" & eq & "', " _
            '+ "eqdesc = '" & eqd & "', " _
            '+ "location = '" & loc & "', " _
            '+ "oem = '" & oem & "', " _
            '+ "model = '" & model & "', " _
            '+ "serial = '" & ser & "', " _
            '+ "spl = '" & spl & "', " _
            '+ "ecr = '" & ecr & "', " _
            '+ "acid = '" & acid & "', " _
            '+ "assetclass = '" & ac & "', " _
            ' + "chargenum = '" & chrg & "', " _
            '+ "trans = '" & t & "', " _
            '+ "modifiedby = '" & ustr & "', " _
            '+ "modifieddate = getDate() " _
            '+ "where eqid = '" & eqid & "'"
            'End If
            'Try
            If echk = "0" Then
                eqr.UpdateHack(cmd)
            Else
                Dim strMessage As String = tmod.getmsg("cdstr867", "EQBot.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            'Catch ex As Exception
            'Dim strMessage As String =  tmod.getmsg("cdstr870" , "EQBot.aspx.vb")

            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'End Try
            If ddac.SelectedIndex <> 0 Then
                sql = "update equipment  set acindex = (" _
                + "select acindex from pmAssetClass where " _
                + "acid = '" & acid & "') where eqid = '" & eqid & "'"
                eqr.Update(sql)
            End If
            If cbtrans.Checked = True And ghostoff <> "yes" Then
                ToTrans()
            End If
            lid = lbleqlid.Value
            sid = lblsid.Value
            If lid <> "" Then
                sql = "update pmlocations set location = '" & eq & "', description = '" & eqd & "' where locid = '" & lid & "'; " _
                    + "update pmlocheir set location = '" & eq & "' where location = '" & oeq & "' and siteid = '" & sid & "'"
                eqr.Update(sql)
            End If

            PopPage()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr871", "EQBot.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub
    Private Sub ToTrans()
        ghostoff = lblghostoff.Value
        If ghostoff <> "yes" Then
            Dim db As String = lbldb.Value
            Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            Dim cmd0 As New SqlCommand("exec usp_sendtrans @eq, @db, @orig")
            Dim param0 = New SqlParameter("@eq", SqlDbType.Int)
            param0.Value = lbleqid.Value
            cmd0.Parameters.Add(param0)
            Dim param01 = New SqlParameter("@db", SqlDbType.VarChar)
            param01.Value = db
            cmd0.Parameters.Add(param01)
            Dim param02 = New SqlParameter("@orig", SqlDbType.VarChar)
            param02.Value = orig
            cmd0.Parameters.Add(param02)
            eqr.UpdateHack(cmd0)
        End If
        
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        eqr.Open()
        SaveEq()
        Try
            eqr.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    
    Private Sub btnupload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Private Sub btngotofunc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        eq = lbleqid.Value
        cid = "0" ' lblcid.value
        sid = lblsid.Value
        did = lbldept.Value
        clid = lblclid.Value
        If Len(eq) > 0 Then
            'RedirectSavingPageState("FuncDivGrid.aspx?eqid=" & eq & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid)
            'Response.Redirect("FuncDiv.aspx?eqid=" & eq & "&cid=" & cid)
        End If

    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        eqr.Open()
        eq = lbleqid.Value
        sql = "select ecr from equipment where eqid = '" & eq & "'"
        dr = eqr.GetRdrData(sql)
        While dr.Read
            txtecr.Text = dr.Item("ecr").ToString
        End While
        dr.Close()
        eqr.Dispose()
    End Sub

    Private Sub btndel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btndel.Click
        Dim pmchk As String = lblnodel.Value
        Dim lock As String = lbllock.Value
        If pmchk <> "nodel" And lock <> "1" Then
            Dim pic, picn, picm As String
            eq = lbleqid.Value
            sql = "select picurl, picurltn, picurltm from pmpictures where eqid = '" & eq & "'"
            eqr.Open()
            dr = eqr.GetRdrData(sql)
            While dr.Read
                pic = dr.Item("picurl").ToString
                picn = dr.Item("picurltn").ToString
                picm = dr.Item("picurltm").ToString
                DelFuImg(pic, picn, picm)
            End While
            dr.Close()
            sql = "usp_deleq '" & eq & "'"
            eqr.Update(sql)
            DelImg()
            eqr.Dispose()
            lbldel.Value = "1"
        End If
        
    End Sub
    Private Sub DelFuImg(ByVal pic As String, ByVal picn As String, ByVal picm As String)
        'fuid = lblfuid.Value

        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"

        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2142.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2142")
        Catch ex As Exception
        End Try
        Try
            ghostoff = lblghostoff.Value
            If ghostoff <> "yes" Then
                lang2143.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2143")
            End If

        Catch ex As Exception
        End Try
        Try
            lang2144.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2144")
        Catch ex As Exception
        End Try
        Try
            lang2145.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2145")
        Catch ex As Exception
        End Try
        Try
            lang2146.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2146")
        Catch ex As Exception
        End Try
        Try
            lang2147.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2147")
        Catch ex As Exception
        End Try
        Try
            lang2148.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2148")
        Catch ex As Exception
        End Try
        Try
            lang2149.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2149")
        Catch ex As Exception
        End Try
        Try
            lang2150.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2150")
        Catch ex As Exception
        End Try
        Try
            lang2151.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2151")
        Catch ex As Exception
        End Try
        Try
            lang2152.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2152")
        Catch ex As Exception
        End Try
        Try
            lang2153.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2153")
        Catch ex As Exception
        End Try
        Try
            lang2154.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2154")
        Catch ex As Exception
        End Try
        Try
            lang2155.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2155")
        Catch ex As Exception
        End Try
        Try
            lang2156.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2156")
        Catch ex As Exception
        End Try
        Try
            lang2157.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2157")
        Catch ex As Exception
        End Try
        Try
            lang2158.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2158")
        Catch ex As Exception
        End Try
        Try
            lang2159.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2159")
        Catch ex As Exception
        End Try
        Try
            lang2160.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2160")
        Catch ex As Exception
        End Try
        Try
            lang2161.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2161")
        Catch ex As Exception
        End Try
        Try
            lang2162.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2162")
        Catch ex As Exception
        End Try
        Try
            lang2163.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2163")
        Catch ex As Exception
        End Try
        Try
            lang2164.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2164")
        Catch ex As Exception
        End Try
        Try
            lang2165.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2165")
        Catch ex As Exception
        End Try
        Try
            lang2166.Text = axlabs.GetASPXPage("EQBot.aspx", "lang2166")
        Catch ex As Exception
        End Try
        Try
            lblpg.Text = axlabs.GetASPXPage("EQBot.aspx", "lblpg")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnAddEq.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
            ElseIf lang = "fre" Then
                btnAddEq.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
            ElseIf lang = "ger" Then
                btnAddEq.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
            ElseIf lang = "ita" Then
                btnAddEq.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
            ElseIf lang = "spa" Then
                btnAddEq.Attributes.Add("src", "../images/appbuttons/minibuttons/addmod.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btngtt.Attributes.Add("src", "../images2/eng/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "fre" Then
                btngtt.Attributes.Add("src", "../images2/fre/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "ger" Then
                btngtt.Attributes.Add("src", "../images2/ger/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "ita" Then
                btngtt.Attributes.Add("src", "../images2/ita/bgbuttons/jtotaskssm.gif")
            ElseIf lang = "spa" Then
                btngtt.Attributes.Add("src", "../images2/spa/bgbuttons/jtotaskssm.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            btnaddasset.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("EQBot.aspx", "btnaddasset") & "')")
            btnaddasset.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgdel.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("EQBot.aspx", "imgdel") & "', ABOVE, LEFT)")
            imgdel.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            imgsavdet.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("EQBot.aspx", "imgsavdet") & "', ABOVE, LEFT)")
            imgsavdet.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid250.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("EQBot.aspx", "ovid250") & "', ABOVE, LEFT)")
            ovid250.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid251.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("EQBot.aspx", "ovid251") & "')")
            ovid251.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

    Protected Sub btnsavechildtot_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnsavechildtot.Click
        eqr.Open()
        SaveChildTot()
        'PopPage()
        Try
            eqr.Dispose()
            Dim strMessage As String = "Child Total Time Saved"

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub savePMO()
        Dim mcv As String = ddpmostat.SelectedValue
        eqid = lblchld.Value
        'eqr.Open()
        
        sql = "update equipment set pmostat = '" & mcv & "' where eqid = '" & eqid & "'"
        eqr.Update(sql)
        
        'PopPage()
        'eqr.Dispose()
    End Sub
    Private Sub savechild()
        Dim lockby As String
        Dim mcv As String = ddmc.SelectedValue
        Dim ch As String = lblischild.Value
        If ch = "yes" Then
            eqid = lblchld.Value
        Else
            eqid = lbleqid.Value
        End If
        'eqr.Open()
        If mcv = "1" Or mcv = "0" Or mcv = "3" Then
            sql = "update equipment set desig = NULL, desigmstr = NULL, locked = 0, lockedby = NULL where eqid = '" & eqid & "'"
            eqr.Update(sql)
        End If
        sql = "update equipment set desig = '" & mcv & "' where eqid = '" & eqid & "'"
        eqr.Update(sql)
        If mcv = "2" Then
            lblischild.Value = "yes"
            ddmc.Attributes.Add("onchange", "checkchild(this.value);")
            lblmc.Attributes.Add("class", "labeltmp")
            lbleqid.Value = eqid
            
        ElseIf mcv = "1" Or mcv = "0" Or mcv = "3" Then
            lblischild.Value = ""
            lblmc.Attributes.Add("class", "labeltmp")
            lbleqid.Value = eqid
            tdmast.InnerHtml = ""

        End If
        PopPage()
        'eqr.Dispose()
    End Sub
    
End Class

