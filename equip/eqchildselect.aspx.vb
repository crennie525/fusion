﻿Imports System.Data.SqlClient
Public Class eqchildselect
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim win As New Utilities
    Dim eqid, sid, acid As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            sid = Request.QueryString("sid").ToString
            lbleqid.Value = eqid
            lblsid.Value = sid
            win.Open()
            getac()
            loadchildren(sid, "0")
            loadchildlist(eqid, sid)
            win.Dispose()

        Else

        End If
    End Sub
    Private Sub ddac_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddac.SelectedIndexChanged
        acid = ddac.SelectedValue
        lblacid.Value = acid
        'lblelist.Value = ""
        sid = lblsid.Value
        win.Open()
        '(sid, acid)
        loadchildren(sid, acid)
        loadchildlist(eqid, sid)
        win.Dispose()
    End Sub
    Private Sub getac()
        'cid = "0" ' lblcid.value
        sql = "select * from pmAssetClass"
        dr = win.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        Try
            ddac.DataBind()
        Catch ex As Exception

        End Try

        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Private Sub loadchildren(ByVal sid As String, ByVal acid As String)
        If acid = "0" Then
            sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where desig = 2 and siteid = '" & sid & "' and desigmstr is NULL"
        Else
            sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where desig = 2 and siteid = '" & sid & "' and desigmstr is NULL and acid = '" & acid & "'"
        End If

        dr = win.GetRdrData(sql)
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "eqnum"
        lbfailmaster.DataValueField = "eqid"
        lbfailmaster.DataBind()
        dr.Close()
    End Sub
    Private Sub loadchildlist(ByVal eqid As String, ByVal sid As String)
       sql = "select eqid, eqnum + ' - ' + eqdesc as 'eqnum' from equipment where desig = 2 and siteid = '" & sid & "' and desigmstr = '" & eqid & "'"
        dr = win.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        lbfailmodes.DataValueField = "eqid"
        lbfailmodes.DataTextField = "eqnum"
        lbfailmodes.DataBind()
        dr.Close()

    End Sub


    Protected Sub btntocomp_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        ToComp()
    End Sub
    Private Sub ToComp()
        eqid = lbleqid.Value
        sid = lblsid.Value

        Dim Item As ListItem
        Dim f, fi As String
        win.Open()
        'sql = "select eqid, eqnum from equipment where desigmstr = '" & eqid & "' and siteid = '" & sid & "'"
        For Each Item In lbfailmaster.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                sql = "update equipment set locked = 1, desig = 2, desigmstr = '" & eqid & "', pmostat = 3 where eqid = '" & fi & "'"

                win.Update(sql)

            End If

        Next
        loadchildren(eqid, sid)
        loadchildlist(eqid, sid)

        win.Dispose()
    End Sub

    Protected Sub btnfromcomp_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        eqid = lbleqid.Value
        sid = lblsid.Value

        Dim Item As ListItem
        Dim f, fi As String
        win.Open()
        'sql = "select eqid, eqnum from equipment where desigmstr = '" & eqid & "' and siteid = '" & sid & "'"
        For Each Item In lbfailmodes.Items
            If Item.Selected Then
                f = Item.Text.ToString
                fi = Item.Value.ToString
                sql = "update equipment set desigmstr = NULL, pmostat = 0 where eqid = '" & fi & "'"

                win.Update(sql)

            End If

        Next
        loadchildren(eqid, sid)
        loadchildlist(eqid, sid)

        win.Dispose()
    End Sub
End Class