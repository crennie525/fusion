﻿Imports System.Data.SqlClient
Imports System.Text
Public Class meterlookup
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim dr As SqlDataReader
    Dim tots As New Utilities
    Dim eqid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            tots.Open()
            getmeters()
            tots.Dispose()
        End If
    End Sub
    Private Sub getmeters()
        eqid = lbleqid.Value
        Dim meterid, meter, munit, mfreq, maxdays As String
        sql = "select * from meters where eqid = '" & eqid & "'"
        dr = tots.GetRdrData(sql)
        Dim sb As New System.Text.StringBuilder
        sb.Append("<table>")
        Dim noflg As Integer = 0
        While dr.Read
            noflg = 1
            meterid = dr.Item("meterid").ToString
            meter = dr.Item("meter").ToString
            munit = dr.Item("unit").ToString
            'mfreq = dr.Item("meterfreq").ToString
            'maxdays = dr.Item("maxdays").ToString
            sb.Append("<tr><td class=""linklabel""><a href=""#"" onclick=""getmeter('" & meterid & "','" & munit & "','" & meter & "');"">" & meter & " - " & munit & "</a></td></tr>")
        End While
        dr.Close()
        If noflg = 0 Then
            sb.Append("<tr><td class=""redlabel"">No Records Found</td></tr>")
        End If
        sb.Append("</table>")
        totdiv.InnerHtml = sb.ToString
    End Sub
End Class