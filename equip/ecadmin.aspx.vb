﻿Imports System.Data.SqlClient
Public Class ecadmin
    Inherits System.Web.UI.Page
    Dim eca As New Utilities
    Dim tmod As New transmod
    Dim sql As String
    Dim dr As SqlDataReader
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 50
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCNT As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eca.Open()
            GetEC(1)
            eca.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                eca.Open()
                GetNext()
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                eca.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber

                GetEC(PageNumber)
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                eca.Open()
                GetPrev()
                eca.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                eca.Open()
                PageNumber = 1
                txtpg.Value = PageNumber

                GetEC(PageNumber)
                eca.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber

            GetEC(PageNumber)
        Catch ex As Exception
            eca.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr7", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetEC(PageNumber)
        Catch ex As Exception
            eca.Dispose()
            Dim strMessage As String = tmod.getmsg("cdstr8", "ecaAssetClass.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetEC(ByVal PageNumber As Integer)
        sql = "select e1.ecd1id, e1.ecd1, e1.ecddesc as edesc1, " _
            + "e2.ecd2id, e2.ecd2,e2.ecddesc as edesc2, e3.ecd3, " _
            + "e3.ecd3id, e3.ecddesc as edesc3 " _
            + "from ecd1 e1 " _
            + "left join ecd2 e2 on e2.ecd1id = e1.ecd1id " _
            + "left join ecd3 e3 on e3.ecd2id = e2.ecd2id " _
            + "order by e1.ecd1, e2.ecd2, e3.ecd3"
        Dim intPgNav, intPgCnt As Integer
        Dim srch As String = txtsearch.Text
        If srch = "" Then
            sql = "select count(*) from ecd3 where ecd1id is not null and ecd2id is not null"
        Else
            sql = "select count(*) from ecd3 e3 " _
                + "left join ecd2 e2 on e2.ecd2id = e3.ecd2id " _
            + "left join ecd1 e1 on e1.ecd1id = e2.ecd1id " _
            + "where e1.ecd1 like '%" & srch & "%'"
            intPgCnt = eca.Scalar(sql)
            If intPgCnt = 0 Then
                sql = "select count(*) from ecd2 e2 " _
            + "left join ecd1 e1 on e1.ecd1id = e2.ecd1id " _
            + "where e1.ecd1 like '%" & srch & "%'"
                intPgCnt = eca.Scalar(sql)
                If intPgCnt = 0 Then
                    sql = "select count(*) from ecd1 e1 " _
            + "where e1.ecd1 like '%" & srch & "%'"
                End If
            End If
        End If

        intPgCnt = eca.Scalar(sql)
        If intPgCnt = 0 Then

            Dim strMessage As String = "No Records Found"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'Exit Sub


        End If
        PageSize = "50"
        intPgNav = eca.PageCountRev(intPgCnt, PageSize)
        txtpg.Value = PageNumber
        txtpgcnt.Value = intPgNav
        Sort = ""

        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        If PageNumber > intPgNav Then
            PageNumber = intPgNav
        End If
        If srch <> "" Then
            Filter = "ecd1 like ''%" & srch & "%''"
        Else
            Filter = "ecd1id is not null and ecd2id is not null"
        End If

        sql = "usp_geteclist '" & PageNumber & "','" & PageSize & "','" & Filter & "','" & Sort & "'"
        dr = eca.GetRdrData(sql)
        GridView1.DataSource = dr
        GridView1.DataBind()
        dr.Close()
    End Sub

    Protected Sub ibtnsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        eca.Open()
        PageNumber = 1
        txtpg.Value = PageNumber
        GetEC(PageNumber)
        eca.Dispose()
    End Sub

    Private Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        GridView1.EditIndex = -1
        eca.Open()
        PageNumber = txtpg.Value
        GetEC(PageNumber)
        eca.Dispose()
    End Sub

    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim idtst As String
        If e.Row.RowType = DataControlRowType.DataRow Then
            'ImageButton1
            Dim idel As ImageButton = CType(e.Row.FindControl("ImageButton1"), ImageButton)
            idel.Attributes("onclick") = "getwho();"

            Try
                Dim id As String = DataBinder.Eval(e.Row.DataItem, "ecd1", "")
                Dim desc As String = DataBinder.Eval(e.Row.DataItem, "edesc1", "")
                Dim jump As HtmlAnchor = CType(e.Row.FindControl("lnkphase"), HtmlAnchor)
                jump.Attributes("onclick") = "jump('" & id & "', '" & desc & "');"
            Catch ex As Exception
                'No way to tell if in Edit Mode?
            End Try
            Try
                Dim fmi As HtmlImage = CType(e.Row.FindControl("imgfm"), HtmlImage)
                Dim fms As String = CType(e.Row.FindControl("txtedesc3"), TextBox).ClientID
                fmi.Attributes("onclick") = "getfm('" & fms & "');"
                Dim oci As HtmlImage = CType(e.Row.FindControl("imgecd2"), HtmlImage)
                Dim ocs As String = CType(e.Row.FindControl("txtedesc2"), TextBox).ClientID
                oci.Attributes("onclick") = "getoc('" & ocs & "');"
            Catch ex As Exception
                'No way to tell if in Edit Mode?
            End Try
        End If

        'Dim index As Integer = Convert.ToInt32(e.Row.RowIndex)
        'Dim row As GridViewRow = GridView1.Rows(index)

    End Sub

    Private Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        Dim ecd1id, ecd2id, ecd3id, dodel As String
        dodel = lbldodel.Value
        If dodel = "" Then
            Dim index As Integer = Convert.ToInt32(e.RowIndex)
            Dim row As GridViewRow = GridView1.Rows(index)
            Try
                ecd1id = CType(row.FindControl("lblecd1idi"), Label).Text
                ecd2id = CType(row.FindControl("lblecd2idi"), Label).Text
                ecd3id = CType(row.FindControl("lblecd3idi"), Label).Text
            Catch ex As Exception
                ecd1id = CType(row.FindControl("lblecd1ide"), Label).Text
                ecd2id = CType(row.FindControl("lblecd2ide"), Label).Text
                ecd3id = CType(row.FindControl("lblecd3ide"), Label).Text
            End Try
            Dim ecnt As Integer = 0
            Dim ewho As String = lbldelwho.Value
            If ewho = "1" Then
                sql = "select count(*) from components where ecd1id = '" & ecd1id & "'"
            ElseIf ewho = "2" Then
                sql = "select count(*) from componentfailmodes where ecd2id = '" & ecd2id & "'"
            ElseIf ewho = "3" Then
                sql = "select count(*) from componentfailmodes where ecd3id = '" & ecd3id & "'"
            End If

            eca.Open()
            ecnt = eca.Scalar(sql)

            If ecnt = 0 Then
                If ewho = "1" Then
                    sql = "delete from ecd1 where ecd1id = '" & ecd1id & "'; " _
                   + "delete from ecd2 where ecd1id = '" & ecd1id & "'; " _
                   + "delete from ecd3 where ecd1id = '" & ecd1id & "'; "
                ElseIf ewho = "2" Then
                    sql = "delete from ecd2 where ecd2id = '" & ecd2id & "'; " _
                   + "delete from ecd3 where ecd2id = '" & ecd2id & "'; "
                ElseIf ewho = "3" Then
                    sql = "delete from ecd3 where ecd3id = '" & ecd3id & "'; "
                Else
                    Exit Sub
                End If
               
                GridView1.EditIndex = -1
                'eca.Open()
                eca.Update(sql)
                GridView1.EditIndex = -1
                PageNumber = txtpg.Value
                GetEC(PageNumber)
                eca.Dispose()
            Else
                Dim strMessage As String = "Designated Error Code to Delete is assigned to one or more Components and cannot be Deleted"""
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                eca.Dispose()
                Exit Sub
            End If
        Else
            lbldodel.Value = ""
            Exit Sub
        End If
        
    End Sub

    Private Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
       
        PageNumber = txtpg.Value
        GridView1.EditIndex = e.NewEditIndex
        eca.Open()
        GetEC(PageNumber)
        eca.Dispose()
    End Sub

    Private Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        Dim index As Integer = Convert.ToInt32(e.RowIndex)
        Dim row As GridViewRow = GridView1.Rows(index)
        Dim ecd1id, ecd2id, ecd3id As String
        ecd1id = CType(row.FindControl("lblecd1ide"), Label).Text
        ecd2id = CType(row.FindControl("lblecd2ide"), Label).Text
        ecd3id = CType(row.FindControl("lblecd3ide"), Label).Text
        Dim ecd1, ecd2, ecd3 As String
        ecd1 = CType(row.FindControl("txtecd1"), TextBox).Text
        ecd2 = CType(row.FindControl("txtecd2"), TextBox).Text
        ecd3 = CType(row.FindControl("txtecd3"), TextBox).Text
        ecd1 = Trim(ecd1)
        ecd2 = Trim(ecd2)
        ecd3 = Trim(ecd3)

        Dim oecd1, oecd2, oecd3 As String
        oecd1 = CType(row.FindControl("lblecd1e"), Label).Text
        oecd2 = CType(row.FindControl("lblecd2e"), Label).Text
        oecd3 = CType(row.FindControl("lblecd3e"), Label).Text
        oecd1 = Trim(oecd1)
        oecd2 = Trim(oecd2)
        oecd3 = Trim(oecd3)

        If Len(ecd1) > 5 Then
            Dim strMessage As String = "Code 1 Limited to 5 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(ecd2) > 5 Then
            Dim strMessage As String = "Code 2 Limited to 5 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(ecd3) > 5 Then
            Dim strMessage As String = "Code 3 Limited to 5 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim edesc1, edesc2, edesc3 As String
        edesc1 = CType(row.FindControl("txtedesc1"), TextBox).Text
        edesc2 = CType(row.FindControl("txtedesc2"), TextBox).Text
        edesc3 = CType(row.FindControl("txtedesc3"), TextBox).Text
        edesc1 = Trim(edesc1)
        edesc2 = Trim(edesc2)
        edesc3 = Trim(edesc3)
        If Len(edesc1) > 50 Then
            Dim strMessage As String = "Code 1 Description Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(edesc2) > 50 Then
            Dim strMessage As String = "Code 2 Description Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(edesc3) > 50 Then
            Dim strMessage As String = "Code 3 Description Limited to 50 Characters"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        eca.Open()
        'Dim oldedesc3 As String = CType(row.FindControl("lbledesc3"), Label).Text
        Dim fcnt As Integer
        sql = "select count(*) from failuremodes where failuremode = '" & edesc3 & "'"
        fcnt = eca.Scalar(sql)
        If fcnt = 0 Then
            sql = "insert into failuremodes (failuremode) values ('" & edesc3 & "')"
            eca.Update(sql)
        End If
        Dim e1cnt, e2cnt, e3cnt As Integer
        sql = "select e1cnt = (select count(*) from ecd1 where ecd1 = '" & ecd1 & "' and ecd1id <> '" & ecd1id & "'), " _
            + "e2cnt = (select count(*) from ecd2 where ecd2 = '" & ecd2 & "' and ecd2id <> '" & ecd2id & "'), " _
            + "e3cnt = (select count(*) from ecd3 where ecd3 = '" & ecd3 & "' and ecd3id <> '" & ecd3id & "') "
        dr = eca.GetRdrData(sql)
        While dr.Read
            e1cnt = dr.Item("e1cnt").ToString
            e2cnt = dr.Item("e2cnt").ToString
            e3cnt = dr.Item("e3cnt").ToString
        End While
        dr.Close()
        If e1cnt = 0 And ecd1 <> oecd1 Then
            sql = "update ecd1 set ecd1 = '" & ecd1 & "', ecddesc = '" & edesc1 & "' where ecd1id = '" & ecd1id & "'; "
            eca.Update(sql)
        Else
            If ecd1 <> oecd1 Then
                Dim strMessage As String = "Revised Code 1 Already Exists in another record"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                eca.Dispose()
                Exit Sub
            End If
           
        End If
        If e2cnt = 0 And ecd2 <> oecd2 Then
            sql = "select count(*) from ecd2 where ecd2 = '" & ecd2 & "' and ecd1id = '" & ecd1id & "'"
            e2cnt = eca.Scalar(sql)
            If ecd2 <> oecd2 Then
                If e2cnt = 0 Then
                    If oecd2 <> "" Then
                        sql = "update ecd2 set ecd2 = '" & ecd2 & "', ecddesc = '" & edesc2 & "' where ecd1id = '" & ecd1id & "' and ecd2id = '" & ecd2id & "'; "
                        eca.Update(sql)
                    Else
                        sql = "insert into ecd2 (ecd2, ecddesc, ecd1id) values ('" & ecd2 & "','" & edesc2 & "','" & ecd1id & "') select @@identity"
                        ecd2id = eca.Scalar(sql)
                    End If
                Else
                    Dim strMessage As String = "Revised Code 2 Already Exists in another record"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    eca.Dispose()
                    Exit Sub
                End If
            End If
        End If
        If e3cnt = 0 And ecd3 <> oecd3 Then
            sql = "select count(*) from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1id & "' and ecd2id = '" & ecd2id & "'"
            e3cnt = eca.Scalar(sql)
            If ecd3 <> oecd3 Then
                If e3cnt = 0 Then
                    If oecd3 <> "" Then
                        sql = "update ecd3 set ecd3 = '" & ecd3 & "', ecddesc = '" & edesc3 & "' where ecd1id = '" & ecd1id & "' and ecd2id = '" & ecd2id & "' and ecd3id = '" & ecd3id & "'; "
                        eca.Update(sql)
                    Else
                        sql = "insert into ecd3 (ecd3, ecddesc, ecd1id, ecd2id) values ('" & ecd3 & "','" & edesc3 & "','" & ecd1id & "','" & ecd2id & "')"
                        eca.Update(sql)
                    End If
                Else
                    Dim strMessage As String = "Revised Code 3 Already Exists in another record"
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    eca.Dispose()
                    Exit Sub
                End If
            End If
        End If
        GridView1.EditIndex = -1
        PageNumber = txtpg.Value
        GetEC(PageNumber)

        eca.Dispose()


    End Sub
End Class