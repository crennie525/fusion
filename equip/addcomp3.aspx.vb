﻿Imports System.Data.SqlClient
Imports System.IO
Public Class addcomp3
    Inherits System.Web.UI.Page
    Dim comp As New Utilities
    Dim dr As SqlDataReader
    Dim mu As New mmenu_utils_a
    Dim eq, fu, sql, dt, val, filt, cid, co, cod, coid, sid, did, clid, ro, appstr, typ, Login, ck, mfg, usr, tpm, isecd As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        Try
            sid = HttpContext.Current.Session("dfltps").ToString
        Catch ex As Exception
            sid = Request.QueryString("sid").ToString
        End Try


        lblsid.Value = sid
        If Not IsPostBack Then
            isecd = mu.ECD
            lblisecd.Value = isecd
            If isecd = "1" Then
                lbfailcomp.SelectionMode = ListSelectionMode.Single
                lbfailmaster.SelectionMode = ListSelectionMode.Single
                imgaddecd.Attributes.Add("class", "view")
                imgaddecd.Attributes.Add("onclick", "getecd1();")
                'btnsave1.Attributes.Add("onclick", "getecd1();")
            Else
                imgaddecd.Attributes.Add("class", "details")
            End If
            Try
                tpm = Request.QueryString("tpm").ToString
                If tpm = "no" Then
                    tpm = "N"
                End If
                lbltpm.Value = tpm
            Catch ex As Exception
                tpm = "N"
                lbltpm.Value = "N"
            End Try
            Try
                coid = Request.QueryString("comid").ToString
            Catch ex As Exception
                coid = "0"
            End Try
            fu = Request.QueryString("fuid").ToString
            lblfuid.Value = fu
            Try
                eq = Request.QueryString("eqid").ToString
                lbleqid.Value = eq
            Catch ex As Exception

            End Try
            Try
                usr = HttpContext.Current.Session("username").ToString()
            Catch ex As Exception
                usr = Request.QueryString("ustr").ToString
            End Try

            lbluser.Value = usr
            lblcoid.Value = coid
            If coid <> "0" Then
                comp.Open()
                cid = "0"
                'PopAC()
                lblfailmode.Value = "norm"
                lblswitch.Value = "all"
                imgchk.Attributes.Add("class", "details")
                popoca()
                PopComp(coid)
                PopFailList(cid, coid)
                PopFail(cid, coid)
                PopFailComp(cid, coid)
                comp.Dispose()
                'geteq.Attributes.Add("src", "../complib/complibtaskview2.aspx?typ=comp&comid=" & coid & "&tpm=" & tpm)
                tdpreadd.Attributes.Add("class", "details")
                'rbmso.Disabled = True
                'rbml.Disabled = True
                tdafteradd.Attributes.Add("class", "view")
                'btnsave.ImageUrl = "../images/appbuttons/minibuttons/savedisk1.gif"
                lblsavetype.Value = "save"
                btncget.Attributes.Add("class", "view")
                btnaddasset.Attributes.Add("class", "view")
                btntocomp.Attributes.Add("class", "view")
                btnfromcomp.Attributes.Add("class", "view")
                todis.Attributes.Add("class", "details")
                fromdis.Attributes.Add("class", "details")
                'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
                'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")
            Else
                tdpreadd.Attributes.Add("class", "view")
                'rbmso.Disabled = False
                'rbml.Disabled = False
                tdafteradd.Attributes.Add("class", "details")
                'btnsave.ImageUrl = "../images/appbuttons/minibuttons/savedisk1.gif"
                lblsavetype.Value = "add"
                cid = "0"
                comp.Open()
                'PopAC()

                'document.getElementById("btntocomp").className="details";
                'document.getElementById("btnfromcomp").className="details";
                'document.getElementById("todis").className="view";
                'document.getElementById("fromdis").className="view";
                btntocomp.Attributes.Add("class", "details")
                btnfromcomp.Attributes.Add("class", "details")
                todis.Attributes.Add("class", "view")
                fromdis.Attributes.Add("class", "view")

                comp.Dispose()
            End If
        Else
            If Request.Form("lblcompchk") = "1" Then
                lblcompchk.Value = ""
                co = lblcoid.Value
                comp.Open()
                PopComp(co)
                comp.Dispose()
            ElseIf Request.Form("lblcompchk") = "delimg" Then
                lblcompchk.Value = ""
                comp.Open()
                DelImg()
                LoadPics()
                comp.Dispose()
                'lblchksav.Value = "yes"
            ElseIf Request.Form("lblcompchk") = "checkpic" Then
                lblcompchk.Value = ""
                comp.Open()
                LoadPics()
                comp.Dispose()
            ElseIf Request.Form("lblcompchk") = "savedets" Then
                lblcompchk.Value = ""
                comp.Open()
                SaveDets()
                LoadPics()
                comp.Dispose()
            ElseIf Request.Form("lblcompchk") = "upfail" Then
                lblcompchk.Value = ""
                comp.Open()
                cid = "0"
                coid = lblcoid.Value
                PopFailList(cid, coid)
                PopFail(cid, coid)
                PopFailComp(cid, coid)
                comp.Dispose()
                tpm = lbltpm.Value
                'geteq.Attributes.Add("src", "../complib/complibtaskview2.aspx?typ=comp&comid=" & coid & "&tpm=" & tpm)
                lblcompchk.Value = "gettasks"
            End If
        End If
    End Sub
    Private Sub popoca()
        sql = "select * from ocareas"
        dr = comp.GetRdrData(sql)
        ddoca.DataSource = dr
        ddoca.DataTextField = "ocarea"
        ddoca.DataValueField = "oaid"
        ddoca.DataBind()
        dr.Close()
        ddoca.Items.Insert(0, "ALL")
    End Sub
    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        comp.Open()
        'Dim typ As String = lblsavetype.Value
        'If typ = "save" Then
        SaveComp()
        'Else
        'AddComp()
        'End If

        comp.Dispose()
    End Sub
    Private Sub AddComp()
        Dim ecd1id As String = lblnewecd1id.Value
        isecd = lblisecd.Value
        sid = lblsid.Value
        co = txtconame.Text
        co = comp.ModString1(co)
        'If isecd = "1" And Len(co) > 5 Then
        'Dim strMessage As String = "New Component Name (Error Code 1) Limited to 5 Characters"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        If Len(co) > 50 Then
            Dim strMessage As String = "Character limit for Common Name is 50"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        If Len(co) > 0 Then
            Dim desc As String = txtdesc.Text
            desc = comp.ModString1(desc)
            If Len(desc) > 50 Then
                Dim strMessage As String = "Character limit for Description is 50"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            Dim spl As String = txtspl.Text
            spl = comp.ModString1(spl)
            If Len(spl) > 50 Then
                Dim strMessage As String = "Character limit for Special Identifier is 50"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            Dim desig As String = txtdesig.Text
            desig = comp.ModString1(desig)
            If Len(desig) > 50 Then
                Dim strMessage As String = "Character limit for Designation is 50"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            Dim cocnt As Integer

            Dim mfg As String = txtmfg.Text
            mfg = comp.ModString1(mfg)
            If Len(mfg) > 50 Then
                Dim strMessage As String = "Character limit for Mfg is 50"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            fu = lblfuid.Value
            sql = "select count(*) from components where compnum = '" & co & "' and func_id = '" & fu & "'"

            sid = lblsid.Value
            Dim ac, acid As String
            cid = "0"
            usr = lbluser.Value
            cocnt = comp.Scalar(sql)
            'If cocnt = 0 Then
            'sql = "usp_addComponent '" & cid & "','" & fu & "', '" & co & "', '" & desc & "', '" & spl & "', '" & desig & "','" & mfg & "'"
            sql = "usp_addComponent " & cid & ", " & fu & ", '" & co & "', '" & desc & "', '" & spl & "', '" & desig & "','" & mfg & "','" & ecd1id & "','" & sid & "'"
            coid = comp.Scalar(sql)
            lblcoid.Value = coid
            popoca()
            PopComp(coid)
            'PopAC()
            PopComp(coid)
            PopFailList(cid, coid)
            PopFail(cid, coid)
            PopFailComp(cid, coid)
            lblsavetype.Value = "save"
            'tdpreadd0.Attributes.Add("class", "details")
            tdpreadd.Attributes.Add("class", "details")
            tdafteradd.Attributes.Add("class", "view")
            btntocomp.Attributes.Add("class", "view")
            btnfromcomp.Attributes.Add("class", "view")
            todis.Attributes.Add("class", "details")
            fromdis.Attributes.Add("class", "details")
            btncget.Attributes.Add("class", "view")
            btnaddasset.Attributes.Add("class", "view")
            'btntocomp.Attributes.Add("onclick", "DisableButton(this);")
            'btnfromcomp.Attributes.Add("onclick", "DisableButton(this);")
            'Else'
            'Dim strMessage As String = "Cannot Enter Duplicate Component Key"
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            'comp.Dispose()
            'End If
        Else
            Dim strMessage As String = "Please Enter a Component Name"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            comp.Dispose()
        End If

    End Sub
    Private Sub PopComp(ByVal coid As String)
        sql = "select * from components where comid = '" & coid & "'"
        dr = comp.GetRdrData(sql)
        Dim ecd1id As String
        While dr.Read

            ecd1id = dr.Item("ecd1id").ToString
            lblecd1id.Value = ecd1id
            lbloldkey.Value = dr.Item("compnum").ToString
            txtconame.Text = dr.Item("compnum").ToString
            lbloldcomp.Value = dr.Item("compnum").ToString
            txtdesc.Text = dr.Item("compdesc").ToString
            txtdesig.Text = dr.Item("desig").ToString
            txtspl.Text = dr.Item("spl").ToString
            txtmfg.Text = dr.Item("mfg").ToString
            'Try
            'ddac.SelectedValue = dr.Item("acid").ToString
            'Catch ex As Exception
            tdcclass.InnerHtml = dr.Item("assetclass").ToString
            tdsclass.InnerHtml = dr.Item("subclass").ToString
            'End Try
        End While
        dr.Close()
        cid = "0"
        PopFailList(cid, coid)
        PopFail(cid, coid)
        PopFailComp(cid, coid)
        LoadPics()
    End Sub
    Private Sub SaveComp()
        isecd = lblisecd.Value
        coid = lblcoid.Value
        co = txtconame.Text
        co = comp.ModString1(co)
        Dim desc As String = txtdesc.Text
        desc = comp.ModString1(desc)
        Dim spl As String = txtspl.Text
        spl = comp.ModString1(spl)
        Dim desig As String = txtdesig.Text
        desig = comp.ModString1(desig)
        Dim cqty As String = "1"
        Dim cocnt As Integer
        Dim ac, acid As String
        'If isecd = "1" And Len(co) > 5 Then
        'Dim strMessage As String = "Component Name (Error Code 1) Limited to 5 Characters"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'Exit Sub
        'End If
        Dim mfg As String = txtmfg.Text
        mfg = comp.ModString1(mfg)
        If Len(mfg) > 50 Then
            Dim strMessage As String = "Character limit for Mfg is 50"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim oldkey As String = lbloldkey.Value
        Dim typ As String = lblsavetype.Value
        fu = lblfuid.Value
        If co <> oldkey Or typ = "add" Then
            sql = "select count(*) from components where compnum = '" & co & "' and func_id = '" & fu & "'"
            cocnt = comp.Scalar(sql)
        Else
            If typ = "add" Then
                cocnt = 0
            Else
                cocnt = 1
            End If

        End If



        'If (cocnt = 0 And typ = "add") Or (cocnt = 1 And typ = "save") Then

        sql = "usp_updateComponent '" & coid & "', '" & ck & "', '" & co & "', '" & cqty & "', '" & desc & "', '" & spl & "', '" & desig & "', '" & ac & "','" & acid & "','" & mfg & "'"
        Try
            comp.Update(sql)
            lblsavetype.Value = "save"
            'PopAC()
            PopComp(coid)
            PopFailList(cid, coid)
            PopFail(cid, coid)
            'PopFailComp(cid, coid)
            tdpreadd.Attributes.Add("class", "details")
            btncget.Attributes.Add("class", "view")

            tdafteradd.Attributes.Add("class", "view")
        Catch ex As Exception
            Dim strMessage As String = "Problem Saving Record\nPlease review selections and try again."
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            comp.Dispose()
        End Try
        'Else
        'Dim strMessage As String = "Cannot Enter Duplicate Component Key"
        'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        'comp.Dispose()
        'End If


    End Sub

    Private Sub btntocomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btntocomp.Click
        cid = "0" 'lblcid.Value
        coid = lblcoid.Value
        Dim Item As ListItem
        Dim f, fi As String
        Dim fail As New Utilities
        fail.Open()
        For Each Item In Me.lbfailmaster.Items
            If Item.Selected Then
                f = Item.ToString
                fi = Item.Value.ToString
                GetItems(fi, f)
            End If
        Next
        PopFailList(cid, coid)
        PopFail(cid, coid)
        fail.Dispose()
    End Sub

    Private Sub btnfromcomp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnfromcomp.Click
        cid = "0" 'lblcid.Value
        coid = lblcoid.Value
        Dim Item As ListItem
        Dim f, fi As String
        Dim fail As New Utilities
        fail.Open()
        For Each Item In Me.lbfailmodes.Items
            If Item.Selected Then
                f = Item.ToString
                fi = Item.Value.ToString
                RemItems(fi, f)
            End If
        Next
        PopFailList(cid, coid)
        PopFail(cid, coid)
        fail.Dispose()
    End Sub
    Private Sub RemItems(ByVal failid As String, ByVal failstr As String)
        isecd = lblisecd.Value
        coid = lblcoid.Value
        Dim sw As String = lblswitch.Value
        Dim fail As New Utilities
        fail.Open()
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            oaid = ""
        End If
        Dim ecd3id, ecd3cnt As Integer
        If isecd = "1" Then
            sql = "delete from ecd3 where ecd3id = (select ecd3id from componentfailmodes where compfailid = '" & failid & "')"
            fail.Update(sql)
        End If
        If sw = "comp" Then
            sql = "update ComponentFailModes set oaid = NULL, ecd1id = NULL, ecd2id = NULL, ecd3id = NULL where compfailid = '" & failid & "';"
            sql += "usp_deltaskoaid '" & failid & "','" & oaid & "'"
        Else

            sql = "sp_delComponentFailMode '" & coid & "', '" & failid & "','" & oaid & "'"


        End If

        fail.Update(sql)
        fail.Dispose()
    End Sub
    Private Sub GetItems(ByVal failid As String, ByVal failstr As String)
        isecd = lblisecd.Value
        cid = "0"
        coid = lblcoid.Value
        sid = lblsid.Value
        Dim sw As String = lblswitch.Value
        Dim fail As New Utilities
        fail.Open()
        Dim fcnt As Integer
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            sql = "select count(*) from ComponentFailModes where " _
        + "comid = '" & coid & "' and failid = '" & failid & "' and oaid is null"
        Else
            sql = "select count(*) from ComponentFailModes where " _
        + "comid = '" & coid & "' and failid = '" & failid & "' and oaid = '" & oaid & "'"
        End If
        fcnt = fail.Scalar(sql)
        Dim ecd As String = lblcurrecd2.Value
        'lblcurrecd2.Value = ""
        Dim ecd1, ecd2, ecd3 As String
        If ecd <> "" Then
            Dim ecdarr() As String = ecd.Split("-")
            ecd2 = ecdarr(0).ToString
            ecd1 = ecdarr(1).ToString
        End If
        ecd3 = lblnewecd3.Value
        lblnewecd3.Value = ""
        Dim ecd3id, ecd3cnt As Integer
        If isecd = "1" Then
            If ecd3 <> "" Then
                sql = "select count(*) from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1 & "' and ecd2id = '" & ecd2 & "'"
                ecd3cnt = fail.Scalar(sql)
                If ecd3cnt = 0 Then
                    sql = "insert into ecd3 (ecd3, ecddesc, ecd1id, ecd2id) values ('" & ecd3 & "','" & oca & "','" & ecd1 & "','" & ecd2 & "') select @@identity"
                    ecd3id = fail.Scalar(sql)
                Else
                    sql = "select ecd3id from ecd3 where ecd3 = '" & ecd3 & "' and ecd1id = '" & ecd1 & "' and ecd2id = '" & ecd2 & "'"
                    ecd3id = fail.Scalar(sql)
                End If
            Else
                Dim strMessage As String = "No Error Code 3 Value Created"
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
        End If


        If fcnt = 0 Then
            If oca = "ALL" Then
                oaid = ""
            End If
            If sw = "comp" And oca <> "ALL" Then
                If ecd <> "" Then
                    sql = "update ComponentFailModes set oaid = '" & oaid & "', ecd1id = '" & ecd1 & "', ecd2id = '" & ecd2 & "', ecd3id = '" & ecd3id & "' where compfailid = '" & failid & "'"
                Else
                    sql = "update ComponentFailModes set oaid = '" & oaid & "' where compfailid = '" & failid & "'"
                End If

            Else
                sql = "sp_addFailureMode '" & cid & "', '" & coid & "', '" & failid & "', '" & failstr & "','" & oaid & "','" & ecd1 & "','" & ecd2 & "','" & ecd3id & "','" & sid & "'"
            End If

            fail.Update(sql)
        End If
        fail.Dispose()

    End Sub
    Private Sub PopFailComp(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        faillist.Open()
        sql = "select compfailid, failuremode from ComponentFailModes where " _
          + "comid = '" & coid & "' and oaid is null order by failuremode"
        dr = faillist.GetRdrData(sql)
        lbfailcomp.DataSource = dr
        lbfailcomp.DataValueField = "compfailid"
        lbfailcomp.DataTextField = "failuremode"
        lbfailcomp.DataBind()
        dr.Close()
        Dim fcnt As Integer = 0
        Dim sw As String = lblswitch.Value
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "All" Or ddoca.SelectedIndex = 0 Or ddoca.SelectedIndex = -1 Then
            lblfailmode.Value = "norm"
            imgchk.Attributes.Add("class", "details")
            lbfailcomp.Attributes.Add("class", "details")
            lbfailmaster.Attributes.Add("class", "view")
            aswitch.Text = "Available Failure Modes"
        Else

            sql = "select count(*) from ComponentFailModes where " _
          + "comid = '" & coid & "' and oaid is null"
            fcnt = faillist.Scalar(sql)
            If fcnt > 0 Then
                lblfailmode.Value = "rev"
                imgchk.Attributes.Add("class", "view")
                If sw = "comp" Then
                    lbfailcomp.Attributes.Add("class", "view")
                    lbfailmaster.Attributes.Add("class", "details")
                    aswitch.Text = "Unassigned Failure Modes"
                Else
                    lbfailcomp.Attributes.Add("class", "details")
                    lbfailmaster.Attributes.Add("class", "view")
                    aswitch.Text = "Available Failure Modes"
                End If
            Else
                lblfailmode.Value = "norm"
                imgchk.Attributes.Add("class", "details")
                If sw = "comp" Then
                    lbfailcomp.Attributes.Add("class", "details")
                    lbfailmaster.Attributes.Add("class", "view")
                    aswitch.Text = "Available Failure Modes"
                    lblswitch.Value = "all"

                End If
            End If
        End If
        faillist.Dispose()
    End Sub
    Private Sub PopFailList(ByVal cid As String, ByVal coid As String)
        Dim faillist As New Utilities
        faillist.Open()
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        Dim ocas As String
        If oca = "ALL" Then
            sql = "select distinct compfailid, failuremode from ComponentFailModes where " _
              + "comid = '" & coid & "'"
            sql = "usp_getcfall '" & coid & "'"
        Else
            sql = "select distinct compfailid, failuremode from ComponentFailModes where " _
              + "comid = '" & coid & "' and oaid = '" & oaid & "' order by failuremode"
        End If

        dr = faillist.GetRdrData(sql)
        lbfailmodes.DataSource = dr
        If oca = "ALL" Then
            lbfailmodes.DataValueField = "compfailid"
            lbfailmodes.DataTextField = "failuremode"
        Else
            lbfailmodes.DataValueField = "compfailid"
            lbfailmodes.DataTextField = "failuremode"
        End If

        lbfailmodes.DataBind()
        dr.Close()

        faillist.Dispose()

    End Sub
    Private Sub PopFail(ByVal cid As String, ByVal comid As String)
        Dim fail As New Utilities
        fail.Open()
        Dim chk As String = lblfailchk.Value
        Dim scnt As Integer
        sid = lblsid.Value
        If chk = "" Then
            sql = "select count(*) from pmSiteFM where siteid = '" & sid & "'"
            scnt = fail.Scalar(sql)
            If scnt = 0 Then
                lblfailchk.Value = "open"
                chk = "open"
            Else
                lblfailchk.Value = "site"
                chk = "site"
            End If
        End If
        Dim dt, val, filt As String
        If chk = "open" Then
            dt = "FailureModes"
        ElseIf chk = "site" Then
            dt = "pmSiteFM"
        Else
            Exit Sub
        End If

        val = "failid, failuremode"
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If oca = "ALL" Then
            If chk = "site" Then
                filt = " where siteid = '" & sid & "' and failid not in (" _
        + "select failid from componentfailmodes where comid = '" & comid & "' and oaid is null)"
            Else
                filt = " where failid not in (" _
        + "select failid from componentfailmodes where comid = '" & comid & "' and oaid is null)"
            End If
            
        Else
            If chk = "site" Then
                filt = " where siteid = '" & sid & "' and failid not in (" _
           + "select failid from componentfailmodes where comid = '" & comid & "' and oaid = '" & oaid & "')"
            Else
                filt = " where failid not in (" _
           + "select failid from componentfailmodes where comid = '" & comid & "' and oaid = '" & oaid & "')"
            End If
           
        End If

        dr = fail.GetList(dt, val, filt)
        lbfailmaster.DataSource = dr
        lbfailmaster.DataTextField = "failuremode"
        lbfailmaster.DataValueField = "failid"
        lbfailmaster.DataBind()
        dr.Close()
        fail.Dispose()
    End Sub
    Private Sub LoadPics()
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/tpmimages/"
        Dim nsimage As String = System.Configuration.ConfigurationManager.AppSettings("nsimageurl")
        'lblnsimage.Value = nsimage
        'lblstrfrom.Value = strfrom
        imgco.Attributes.Add("src", "../images/appimages/compimg.gif")

        Dim img, imgs, picid As String

        coid = lblcoid.Value
        Dim pcnt As Integer
        sql = "select count(*) from pmpictures where comid is not null and comid = '" & coid & "'"
        pcnt = comp.Scalar(sql)
        lblpcnt.Value = pcnt

        Dim currp As String = lblcurrp.Value
        Dim rcnt As Integer = 0
        Dim iflag As Integer = 0
        Dim oldiord As Integer
        If pcnt > 0 Then
            If currp <> "" Then
                oldiord = System.Convert.ToInt32(currp) + 1
                lblpg.Text = "Image " & oldiord & " of " & pcnt
            Else
                oldiord = 1
                lblpg.Text = "Image 1 of " & pcnt
                lblcurrp.Value = "0"
            End If

            sql = "select p.pic_id, p.picurl, p.picurltn, p.picurltm, p.picorder " _
            + "from pmpictures p where p.comid is not null and p.comid = '" & coid & "'" _
            + "order by p.picorder"
            Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts, iorders As String
            Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink, bimg, bimgs, timg As String
            Dim tlink, ttext, iloc1, pcss As String
            Dim ovimg, ovbimg, ovimgs, ovbimgs, ovtimg, ovtimgs
            dr = comp.GetRdrData(sql)
            While dr.Read
                iflag += 1
                img = dr.Item("picurltm").ToString
                Dim imgarr() As String = img.Split("/")
                ovimg = imgarr(imgarr.Length - 1)
                bimg = dr.Item("picurl").ToString
                Dim bimgarr() As String = bimg.Split("/")
                ovbimg = bimgarr(bimgarr.Length - 1)

                timg = dr.Item("picurltn").ToString
                Dim timgarr() As String = timg.Split("/")
                ovtimg = timgarr(timgarr.Length - 1)

                picid = dr.Item("pic_id").ToString
                order = dr.Item("picorder").ToString
                If iorders = "" Then
                    iorders = order
                Else
                    iorders += "," & order
                End If
                If bimgs = "" Then
                    bimgs = bimg
                Else
                    bimgs += "," & bimg
                End If
                If ovbimgs = "" Then
                    ovbimgs = ovbimg
                Else
                    ovbimgs += "," & ovbimg
                End If

                If ovtimgs = "" Then
                    ovtimgs = ovtimg
                Else
                    ovtimgs += "," & ovtimg
                End If

                If ovimgs = "" Then
                    ovimgs = ovimg
                Else
                    ovimgs += "," & ovimg
                End If
                If iflag = oldiord Then 'was 0
                    'iflag = 1

                    lblcurrimg.Value = ovimg
                    lblcurrbimg.Value = ovbimg
                    lblcurrtimg.Value = ovtimg
                    imgco.Attributes.Add("src", img)
                    'imgeq.Attributes.Add("onclick", "getbig();")
                    lblimgid.Value = picid

                    txtiorder.Text = order
                End If
                If imgs = "" Then
                    imgs = picid & ";" & img
                Else
                    imgs += "~" & picid & ";" & img
                End If
            End While
            dr.Close()
            lblimgs.Value = imgs
            lblbimgs.Value = bimgs
            lblovimgs.Value = ovimgs
            lblovbimgs.Value = ovbimgs
            lblovtimgs.Value = ovtimgs
            lbliorders.Value = iorders
        End If
    End Sub
    Private Sub DelImg()
        coid = lblcoid.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        typ = "co"
        sql = "usp_deleqimg '" & typ & "','" & coid & "','" & pid & "','" & old & "'"
        comp.Update(sql)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblcurrbimg.Value
        picn = lblcurrtimg.Value
        picm = lblcurrimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'ile.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
        lblcurrp.Value = "0"

    End Sub
    Private Sub SaveDets()
        Dim iord As String = txtiorder.Text
        Try
            Dim piord As Integer = System.Convert.ToInt32(iord)
        Catch ex As Exception
            Dim strMessage As String = "Image Order Must Be a Numeric Value"
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        Dim oldiord As Integer = lblcurrp.Value + 1
        If oldiord <> iord Then
            Dim old As String = lbloldorder.Value
            Dim neword As String = oldiord 'txtiorder.Text
            coid = lblcoid.Value
            typ = "co"
            sql = "usp_reordereqimg '" & typ & "','" & coid & "','" & neword & "','" & old & "'"
            comp.Update(sql)
        End If
    End Sub

    Private Sub btnsave1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave1.Click
        comp.Open()
        Dim typ As String = lblsavetype.Value
        If typ = "save" Then
            SaveComp()
        Else
            AddComp()
        End If

        comp.Dispose()
    End Sub

    Private Sub ddoca_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddoca.SelectedIndexChanged
        Dim coid As String = lblcoid.Value
        Dim cid As String = "0"
        comp.Open()
        isecd = lblisecd.Value
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        If isecd = "1" And oca <> "ALL" Then
            checkecd()
        End If
        PopFailList(cid, coid)
        PopFailComp(cid, coid)
        comp.Dispose()
    End Sub
    Private Sub checkecd()
        isecd = lblisecd.Value
        Dim ecd2, ecd2id, ecd1id As String
        Dim oaid As String = ddoca.SelectedValue.ToString
        Dim oca As String = ""
        If oaid <> "" Then
            oca = ddoca.SelectedItem.ToString
        End If
        Dim coid As String = lblcoid.Value
        ecd1id = lblecd1id.Value
        If isecd = "1" Then
            sql = "select count(*) from ecd2 where ecd1id = '" & ecd1id & "' and ecddesc = '" & oca & "'"
        Else
            sql = "select count(*) from componentfailmodes where oaid = '" & oaid & "' and comid = '" & coid & "'"
        End If

        Dim oaidcnt As Integer = comp.Scalar(sql)
        If oaidcnt = 0 Then
            lblcurrecd2.Value = ""
            lblnewecd3.Value = ""
            lblgetecd2.Value = "1"
            lblecd2txt.Value = oca
            'lblecd1id.Value = ""
        Else
            If isecd = "1" Then
                sql = "select ecd1id, ecd2id from ecd2 where ecd1id = '" & ecd1id & "' and ecddesc = '" & oca & "'"
            Else
                sql = "select e.ecd2, e.ecd1id, c.ecd2id from componentfailmodes c left join ecd2 e on e.ecd2id = c.ecd2id where c.oaid = '" & oaid & "' and c.comid = '" & coid & "'"
            End If

            dr = comp.GetRdrData(sql)
            While dr.Read
                ecd2id = dr.Item("ecd2id").ToString
                ecd1id = dr.Item("ecd1id").ToString
            End While
            dr.Close()
            lblcurrecd2.Value = ecd2id & "-" & ecd1id
            lblnewecd3.Value = ""
            lblgetecd2.Value = "0"
            lblecd1id.Value = ecd1id
        End If
    End Sub
End Class