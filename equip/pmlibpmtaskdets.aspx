<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibpmtaskdets.aspx.vb" Inherits="lucy_r12.pmlibpmtaskdets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibpmtaskdets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 0px; POSITION: absolute; TOP: 0px">
				<TBODY>
					<tr id="trmsg" runat="server" height="30" class="details">
						<td colspan="7" id="tdmsg" runat="server" class="plainlabelblue"></td>
					</tr>
					<tr>
						<td colSpan="7"><div style="OVERFLOW: auto; WIDTH: 990px; HEIGHT: 260px">
								<asp:repeater id="rptrtaskrev" Runat="server">
									<HeaderTemplate>
										<table width="1400">
											<tr>
												<td width="40" class="btmmenu plainlabel"><asp:Label id="lang2514" runat="server">Task#</asp:Label></td>
												<td width="70" class="btmmenu plainlabel"><asp:Label id="lang2515" runat="server">Sub Task#</asp:Label></td>
												<td width="200" class="btmmenu plainlabel"><asp:Label id="lang2516" runat="server">Component Addressed</asp:Label></td>
												<td width="30" class="btmmenu plainlabel">Qty</td>
												<td width="220" class="btmmenu plainlabel"><asp:Label id="lang2517" runat="server">Failure Modes Addressed</asp:Label></td>
												<td width="260" class="btmmenu plainlabel"><asp:Label id="lang2518" runat="server">Task Description</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2519" runat="server">Task Type</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2520" runat="server">Pdm</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2521" runat="server">Skill</asp:Label></td>
												<td width="30" class="btmmenu plainlabel">Qty</td>
												<td width="40" class="btmmenu plainlabel"><asp:Label id="lang2522" runat="server">Time</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2523" runat="server">Frequency</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2524" runat="server">Eq Status</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2525" runat="server">Down Time</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg">
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' Runat = server ID="Label5">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"subtask")%>' Runat = server ID="Label6">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server ID="Label11">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"cqty")%>' Runat = server ID="Label12">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"fm1")%>' Runat = server ID="Label13">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' Runat = server ID="Label14">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasktype")%>' Runat = server ID="Label15">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"pretech")%>' Runat = server ID="Label16">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"skill")%>' Runat = server ID="Label17">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"qty")%>' Runat = server ID="Label18">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"ttime")%>' Runat = server ID="Label19">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"freq")%>' Runat = server ID="Label20">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rd")%>' Runat = server ID="Label21">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rdt")%>' Runat = server ID="Label22">
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD></TR></TBODY></TABLE> <input type="hidden" id="lbldb" runat="server">
			<input type="hidden" id="lblfuid" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
