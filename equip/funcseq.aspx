<%@ Page Language="vb" AutoEventWireup="false" Codebehind="funcseq.aspx.vb" Inherits="lucy_r12.funcseq" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>funcseq</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script  type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script  src="../scripts1/funcseqaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td><asp:datagrid id="dgfunc" runat="server" AutoGenerateColumns="False" AllowSorting="True" CellSpacing="1"
							GridLines="None">
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="plainlabel"></ItemStyle>
							<HeaderStyle Height="26px" CssClass="btmmenu plainlabel"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											ToolTip="Save Changes" CommandName="Update"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											ToolTip="Cancel Changes" CommandName="Cancel"></asp:imagebutton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Sequence#">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblroute runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtrouting runat="server"  CssClass="plainlabel" style="width: 40px" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Function#">
									<HeaderStyle Width="240px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id=txtfunum runat="server"  CssClass="plainlabel" Width="230px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="True" HeaderText="Special Designation">
									<HeaderStyle Width="300px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lbldesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.spl") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" id="txtspl"  CssClass="plainlabel" width="290" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.spl") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label runat="server" id="lblfuid" Text='<%# DataBinder.Eval(Container, "DataItem.func_id") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label runat="server" id="txtfuid" width="290" Text='<%# DataBinder.Eval(Container, "DataItem.func_id") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Sequence#" Visible="False">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lbloldrte" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lbleqid" type="hidden" runat="server"> <input id="lbluser" type="hidden" runat="server">
			<input type="hidden" id="lbllog" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
