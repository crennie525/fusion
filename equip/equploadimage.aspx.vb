

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Imports System.IO

Public Class equploadimage
    Inherits System.Web.UI.Page
    Protected WithEvents lang2299 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2298 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2297 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, funcid, eqid, ro, comid, typ As String
    Dim news As New Utilities
    Protected WithEvents lbltyp As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblbimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblnimg As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents MyFile As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents btnupload As System.Web.UI.HtmlControls.HtmlInputButton
    Protected WithEvents tblpics As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents picdiv As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblfuncid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltasknum As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfunc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblimgid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpicorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblneworder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmaxorder As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblititles As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblref As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbloldtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblmaxtask As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbledit As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            funcid = Request.QueryString("funcid").ToString
            lblfuncid.Value = funcid
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            comid = Request.QueryString("comid").ToString
            lblcomid.Value = comid
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            If ro = "1" Then
                btnupload.Disabled = True
                lbledit.Value = "no"
            End If
            news.Open()
            GetPics()
            news.Dispose()
        Else
            If Request.Form("lblsubmit") = "savord" Then
                lblsubmit.Value = ""
                news.Open()
                SaveOrd()
                GetPics()
                news.Dispose()
            ElseIf Request.Form("lblsubmit") = "delimg" Then
                lblsubmit.Value = ""
                news.Open()
                DelImg()
                GetPics()
                news.Dispose()
            End If
        End If
    End Sub
    Private Sub DelImg()
        typ = lbltyp.Value
        Dim pid As String = lblimgid.Value
        Dim old As String = lbloldorder.Value
        If typ = "fu" Then
            funcid = lblfuncid.Value
            sql = "usp_deleqimg '" & typ & "','" & funcid & "','" & pid & "','" & old & "'"
        ElseIf typ = "eq" Then
            eqid = lbleqid.Value
            sql = "usp_deleqimg '" & typ & "','" & eqid & "','" & pid & "','" & old & "'"
        ElseIf typ = "co" Then
            comid = lblcomid.Value
            sql = "usp_deleqimg '" & typ & "','" & comid & "','" & pid & "','" & old & "'"
        End If

        news.Update(sql)

        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim pic, picn, picm As String
        pic = lblbimg.Value
        picn = lblnimg.Value
        picm = lblimg.Value
        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub SaveOrd()
        typ = lbltyp.Value
        Dim old As String = lbloldorder.Value
        Dim neword As String = lblneworder.Value
        If typ = "fu" Then
            funcid = lblfuncid.Value
            sql = "usp_reordereqimg '" & typ & "','" & funcid & "','" & neword & "','" & old & "'"
        ElseIf typ = "eq" Then
            eqid = lbleqid.Value
            sql = "usp_reordereqimg '" & typ & "','" & eqid & "','" & neword & "','" & old & "'"
        ElseIf typ = "co" Then
            comid = lblcomid.Value
            sql = "usp_reordereqimg '" & typ & "','" & comid & "','" & neword & "','" & old & "'"
        End If
        news.Update(sql)

    End Sub
    Private Sub GetPics()
        typ = lbltyp.Value
        ro = lblro.Value
        Dim pic, order, picorder, iheight, iwidth, ititle, iloc, pcol, pdec, pstyle, ilink As String
        Dim x As Integer
        Dim sb As New StringBuilder
        sb.Append("<table>")
        Dim maxord As Integer
        If typ = "fu" Then
            eqid = lbleqid.Value
            funcid = lblfuncid.Value
            sql = "select max(picorder) as maxord from pmpictures where funcid = '" & funcid & "' and comid is null"
        ElseIf typ = "eq" Then
            eqid = lbleqid.Value
            sql = "select max(picorder) as maxord from pmpictures where eqid = '" & eqid & "' and funcid is null and comid is null"
        ElseIf typ = "co" Then
            eqid = lbleqid.Value
            funcid = lblfuncid.Value
            comid = lblcomid.Value
            sql = "select max(picorder) as maxord from pmpictures where comid = '" & comid & "'"
        End If

        Try
            maxord = news.Scalar(sql)
        Catch ex As Exception
            maxord = 0
        End Try

        lblmaxorder.Value = maxord
        Dim iheights, iwidths, ititles, ilocs, ilocs1, pcols, pdecs, pstyles, ilinks, tlinks, ttexts As String
        Dim tlink, ttext, iloc1, pcss As String
        If typ = "fu" Then
            eqid = lbleqid.Value
            funcid = lblfuncid.Value
            sql = "select * from pmpictures where funcid = '" & funcid & "' and comid is null"
        ElseIf typ = "eq" Then
            eqid = lbleqid.Value
            sql = "select * from pmpictures where eqid = '" & eqid & "' and funcid is null and comid is null"
        ElseIf typ = "co" Then
            eqid = lbleqid.Value
            funcid = lblfuncid.Value
            comid = lblcomid.Value
            sql = "select * from pmpictures where comid = '" & comid & "'"
        End If
        dr = news.GetRdrData(sql)
        sb.Append("<tr>")
        x = 1
        'id=""" + pic + """
        Dim img, nimg, bimg As String
        While dr.Read
            If x = 1 Then
                sb.Append("<tr>")
            End If
            pic = dr.Item("pic_id").ToString
            order = dr.Item("picorder").ToString
            img = dr.Item("picurltm").ToString
            bimg = dr.Item("picurl").ToString
            nimg = dr.Item("picurltn").ToString

            sb.Append("<td><table><tr><td colspan=""2"">")
            If ro = "1" Then
                sb.Append("<a href=""#"" ondblclick=""getbig('" + bimg + "');""" _
           + "onclick=""getdets('" + order + "','" + pic + "','" + order + "');"">" _
           + "<img border=""0"" src=""" + nimg + """>" _
           + "</a></td><tr><td class=""bluelabel"">" & tmod.getlbl("cdlbl436", "equploadimage.aspx.vb") & "</td><td>" _
           + "<input size=""2"" value=""" + order + """ id=""" + pic + """>" _
           + "</td></tr><tr><td colspan=""2"" align=""right"">" _
           + "<img src=""../images/appbuttons/minibuttons/deldis.gif"" " _
           + " onmouseover=""return overlib('" & tmod.getov("cov275", "equploadimage.aspx.vb") & "', ABOVE, LEFT)"" onmouseout='return nd()'>" _
           + "<img src=""../images/appbuttons/minibuttons/saveDisk1dis.gif"" " _
           + "onmouseover=""return overlib('" & tmod.getov("cov276", "equploadimage.aspx.vb") & "', ABOVE, LEFT)"" onmouseout='return nd()'></td></tr></table></td>")
            Else
                sb.Append("<a href=""#"" ondblclick=""getbig('" + bimg + "');""" _
                          + "onclick=""getdets('" + order + "','" + pic + "','" + order + "');"">" _
                          + "<img border=""0"" src=""" + nimg + """>" _
                          + "</a></td><tr><td class=""bluelabel"">" & tmod.getlbl("cdlbl439", "equploadimage.aspx.vb") & "</td><td>" _
                          + "<input size=""2"" value=""" + order + """ id=""" + pic + """>" _
                          + "</td></tr><tr><td colspan=""2"" align=""right"">" _
                          + "<img src=""../images/appbuttons/minibuttons/del.gif"" onclick=""delimg('" + pic + "','" + order + "','" + img + "','" + bimg + "','" + nimg + "');"" " _
                          + " onmouseover=""return overlib('" & tmod.getov("cov277", "equploadimage.aspx.vb") & "', ABOVE, LEFT)"" onmouseout='return nd()'>" _
                          + "<img src=""../images/appbuttons/minibuttons/saveDisk1.gif"" onclick=""saveord('" + pic + "','" + order + "');"" " _
                          + "onmouseover=""return overlib('" & tmod.getov("cov278", "equploadimage.aspx.vb") & "', ABOVE, LEFT)"" onmouseout='return nd()'></td></tr></table></td>")
            End If

            If picorder = "" Then
                picorder = pic & "-" & order
            Else
                picorder += "~" & pic & "-" & order
            End If
            x = x + 1
            If x = 5 Then
                sb.Append("</tr>")
                x = 1
            End If
        End While
        dr.Close()
        lblpicorder.Value = picorder
        'iheights, iwidths, ititles, ilocs, pcols, pdecs, pstyles
        lblititles.Value = ititles
        Dim Remainder As Integer
        Remainder = x Mod 4
        If Remainder > 0 Then
            sb.Append("</tr>")
        End If
        sb.Append("</table>")
        'tblpics.InnerHtml = sb.ToString
        picdiv.InnerHtml = sb.ToString

    End Sub
    Private Sub AddPic()
        If Not (MyFile.PostedFile Is Nothing) Then
            'Check to make sure we actually have a file to upload
            Dim strLongFilePath As String = MyFile.PostedFile.FileName
            Dim intFileNameLength As Integer = InStr(1, StrReverse(strLongFilePath), "\")
            Dim strFileName As String = Mid(strLongFilePath, (Len(strLongFilePath) - intFileNameLength) + 2)
            typ = lbltyp.Value
            Dim icnt, irand As Integer
            news.Open()
            If typ = "fu" Then
                eqid = lbleqid.Value
                funcid = lblfuncid.Value
                sql = "select count(*) from pmpictures where funcid = '" & funcid & "' and comid is null"
            ElseIf typ = "eq" Then
                eqid = lbleqid.Value
                sql = "select count(*) from pmpictures where eqid = '" & eqid & "' and funcid is null and comid is null"
            ElseIf typ = "co" Then
                eqid = lbleqid.Value
                funcid = lblfuncid.Value
                comid = lblcomid.Value
                sql = "select count(*) from pmpictures where comid = '" & comid & "'"
            End If
            sql = "usp_getpiccnt '" & eqid & "','" & funcid & "','" & comid & "','" & typ & "'"
            'icnt = news.Scalar(sql)
            dr = news.GetRdrData(sql)
            While dr.Read
                icnt = dr.Item("icnt").ToString
                irand = dr.Item("irand").ToString
            End While
            dr.Close()
            icnt += 1
            'irand = CInt(Int((99 * Rnd()) + 1))

            Dim newstr As String '= "a-newsImg" & funcid & "-" & tasknum & "-" & icnt & ".jpg"
            Dim thumbstr As String '= "atn-newsImg" & funcid & "-" & tasknum & "-" & icnt & ".jpg"
            Dim medstr As String '= "atm-newsImg" & funcid & "-" & tasknum & "-" & icnt & ".jpg"
            If typ = "eq" Then
                newstr = "a-eqImg" & eqid & "i" & irand & ".jpg"
                thumbstr = "atn-eqImg" & eqid & "i" & irand & ".jpg"
                medstr = "atm-eqImg" & eqid & "i" & irand & ".jpg"
            ElseIf typ = "fu" Then
                newstr = "b-fuImg" & funcid & "i" & irand & ".jpg"
                thumbstr = "btn-fuImg" & funcid & "i" & irand & ".jpg"
                medstr = "btm-fuImg" & funcid & "i" & irand & ".jpg"
            ElseIf typ = "co" Then
                newstr = "c-coImg" & comid & "i" & irand & ".jpg"
                thumbstr = "ctn-coImg" & comid & "i" & irand & ".jpg"
                medstr = "ctm-coImg" & comid & "i" & irand & ".jpg"
            End If

            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
            Dim strto As String = appstr + "/eqimages/"
            Select Case MyFile.PostedFile.ContentType
                Case "image/pjpeg", "image/jpeg"  'Make sure we are getting a valid JPG image
                    Try
                        If File.Exists(Server.MapPath("\") & strto & newstr) Then
                            File.Delete(Server.MapPath("\") & strto & newstr)
                        End If
                    Catch ex As Exception
                        strto = "/eqimages/"
                        If File.Exists(Server.MapPath("\") & strto & newstr) Then
                            File.Delete(Server.MapPath("\") & strto & newstr)
                        End If
                    End Try
                    Try
                        If File.Exists(Server.MapPath("\") & strto & thumbstr) Then
                            File.Delete(Server.MapPath("\") & strto & thumbstr)
                        End If
                    Catch ex As Exception
                        strto = "/eqimages/"
                        If File.Exists(Server.MapPath("\") & strto & thumbstr) Then
                            File.Delete(Server.MapPath("\") & strto & thumbstr)
                        End If
                    End Try
                    Try
                        If File.Exists(Server.MapPath("\") & strto & medstr) Then
                            File.Delete(Server.MapPath("\") & strto & medstr)
                        End If
                    Catch ex As Exception
                        strto = "/eqimages/"
                        If File.Exists(Server.MapPath("\") & strto & medstr) Then
                            File.Delete(Server.MapPath("\") & strto & medstr)
                        End If
                    End Try
                    Dim chkfile As String
                    Dim teststring As String
                    Dim tstro As String
                    Try
                        tstro = "eqimages"
                        chkfile = Server.MapPath("\") & tstro
                        If Directory.Exists(chkfile) Then
                            teststring = "ok"
                        Else
                            tstro = appstr + "/eqimages/"
                            chkfile = Server.MapPath("\") & tstro
                            If Directory.Exists(chkfile) Then
                                teststring = "ok"
                            End If
                        End If
                        If teststring <> "ok" Then
                            Dim strMessage As String = "Your eqimages folder does not exist. Please contact your system administrator"
                            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                            news.Dispose()
                            Exit Sub
                        End If

                    Catch ex1 As Exception
                        Dim strMessage As String = tmod.getmsg("cdstr926", "equploadimage.aspx.vb")
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        news.Dispose()
                        Exit Sub
                    End Try
                    Try
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & newstr)
                    Catch ex As Exception
                        'temp for Canton
                        ''Dim strMessage As String = Server.MapPath("\") & strto & newstr
                        ''Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        ''news.Dispose()
                        ''Exit Sub
                        
                    End Try
                    Try
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & thumbstr)
                    Catch ex As Exception
                        'temp for Canton
                        ''Dim strMessage As String = Server.MapPath("\") & strto & newstr
                        ''Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        ''news.Dispose()
                        ''Exit Sub
                        strto = "/eqimages/"
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & thumbstr)
                    End Try
                    Try
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & medstr)
                    Catch ex As Exception
                        strto = "/eqimages/"
                        MyFile.PostedFile.SaveAs(Server.MapPath("\") & strto & medstr)
                    End Try
                    Dim fsimg As System.Drawing.Image
                    'Response.ContentType = "image/jpeg"
                    Try
                        fsimg = System.Drawing.Image.FromFile(Server.MapPath("\") & strto & medstr)
                    Catch ex As Exception
                        strto = "/eqimages/"
                        fsimg = System.Drawing.Image.FromFile(Server.MapPath("\") & strto & medstr)
                    End Try
                    Dim biw, bih As Integer
                    biw = fsimg.Width
                    bih = fsimg.Height
                    If biw > 500 Then
                        'biw = 500
                    End If
                    If bih > 382 Then
                        'bih = 382
                    End If
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    fsimg.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone)
                    Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
                    dummyCallBack = New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)
                    Dim tnImg As System.Drawing.Image
                    Dim iw, ih As Integer
                    iw = 100
                    ih = 100
                    tnImg = fsimg.GetThumbnailImage(iw, ih, dummyCallBack, IntPtr.Zero)
                    tnImg.Save(Server.MapPath("\") & strto & thumbstr)

                    Dim tmImg As System.Drawing.Image
                    Dim iwm, ihm As Integer
                    iwm = 220 'System.Convert.ToInt32(wd)
                    ihm = 220 'System.Convert.ToInt32(ht)
                    tmImg = fsimg.GetThumbnailImage(iwm, ihm, dummyCallBack, IntPtr.Zero)
                    tmImg.Save(Server.MapPath("\") & strto & medstr)

                    Dim tbImg As System.Drawing.Image
                    Dim iwb, ihb, iwb1, ihb1 As Integer
                    iwb = fsimg.Width
                    ihb = fsimg.Height
                    iwb1 = System.Convert.ToInt32(iwb)
                    ihb1 = System.Convert.ToInt32(ihb)
                    If iwb1 > 500 Then

                        Dim iper As Decimal
                        iper = ihb1 / iwb1
                        iwb1 = 500
                        ihb1 = iwb1 * iper
                        ihb1 = Math.Round(ihb1, 0)

                    End If


                    'tbImg = fsimg.GetThumbnailImage(biw, bih, dummyCallBack, IntPtr.Zero)
                    tbImg = fsimg.GetThumbnailImage(iwb1, ihb1, dummyCallBack, IntPtr.Zero)
                    tbImg.Save(Server.MapPath("\") & strto & newstr)


                    'tnImg.Dispose()
                    'tmImg.Dispose()
                    tbImg.Dispose()
                    fsimg.Dispose()

                    Dim nurl As String = System.Configuration.ConfigurationManager.AppSettings("tpmurl")
                    Dim savstr As String = "../eqimages/" & newstr 'nurl & "/eqimages/" & newstr
                    Dim savtnstr As String = "../eqimages/" & thumbstr ' nurl & "/eqimages/" & thumbstr
                    Dim savtmstr As String = "../eqimages/" & medstr 'nurl & "/eqimages/" & medstr
                    Dim cmd As New SqlCommand
                    cmd.CommandText = "exec usp_addneweqimg @eqid, @funcid, @comid, @ni, @nit, @nim, " _
                    + "@order"
                    Dim param001 = New SqlParameter("@eqid", SqlDbType.Int)
                    If eqid = "" Then
                        param001.Value = System.DBNull.Value
                    Else
                        param001.Value = eqid
                    End If
                    cmd.Parameters.Add(param001)
                    Dim param01 = New SqlParameter("@funcid", SqlDbType.Int)
                    If funcid = "" Or funcid = "0" Then
                        param01.Value = System.DBNull.Value
                    Else
                        param01.Value = funcid
                    End If
                    cmd.Parameters.Add(param01)
                    Dim param1 = New SqlParameter("@comid", SqlDbType.Int)
                    If comid = "" Or comid = "0" Then
                        param1.Value = System.DBNull.Value
                    Else
                        param1.Value = comid
                    End If
                    cmd.Parameters.Add(param1)
                    Dim param2 = New SqlParameter("@ni", SqlDbType.VarChar)
                    param2.Value = savstr
                    cmd.Parameters.Add(param2)
                    Dim param3 = New SqlParameter("@nit", SqlDbType.VarChar)
                    param3.Value = savtnstr
                    cmd.Parameters.Add(param3)
                    Dim param4 = New SqlParameter("@nim", SqlDbType.VarChar)
                    param4.Value = savtmstr
                    cmd.Parameters.Add(param4)
                    Dim param5 = New SqlParameter("@order", SqlDbType.Int)
                    param5.Value = icnt
                    cmd.Parameters.Add(param5)

                    news.UpdateHack(cmd)
                    lblref.Value = "yes"
                Case Else
                    Dim strMessage As String = tmod.getmsg("cdstr926", "equploadimage.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End Select
            GetPics()
            news.Dispose()
        Else
            Dim strMessage As String = tmod.getmsg("cdstr927", "equploadimage.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function

    Private Sub btnupload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.ServerClick
        AddPic()
    End Sub










    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2297.Text = axlabs.GetASPXPage("equploadimage.aspx", "lang2297")
        Catch ex As Exception
        End Try
        Try
            lang2298.Text = axlabs.GetASPXPage("equploadimage.aspx", "lang2298")
        Catch ex As Exception
        End Try
        Try
            lang2299.Text = axlabs.GetASPXPage("equploadimage.aspx", "lang2299")
        Catch ex As Exception
        End Try

    End Sub

End Class
