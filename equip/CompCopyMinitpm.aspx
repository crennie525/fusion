<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CompCopyMinitpm.aspx.vb" Inherits="lucy_r12.CompCopyMinitpm" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>CompCopyMini</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<LINK href="../styles/reports.css" type="text/css" rel="stylesheet">
		<script  src="../scripts/gridnav.js"></script>
		<script  src="../scripts1/CompCopyMinitpmaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg"  onload="checkpg();">
		<form id="form1" method="post" runat="server">
			<table id="tbltop" width="650" runat="server">
				<TBODY>
					<tr height="24">
						<td colSpan="3" class="thdrsing label"><asp:Label id="lang1985" runat="server">Component Copy Mini Dialog</asp:Label></td>
					</tr>
					<tr id="tbleqrec" runat="server">
						<td class="bluelabel" style="width: 80px"><asp:Label id="lang1986" runat="server">Search</asp:Label></td>
						<td width="310"><asp:textbox id="txtsrch" runat="server" Width="250px"></asp:textbox><asp:imagebutton id="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
						<td width="210" align="right"><IMG id="hpBackward" runat="server" onclick="handlereturn();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
								width="69" height="19"></td>
					</tr>
					<tr>
						<td align="center" colSpan="3">
							<table>
								<TBODY>
									<tr>
										<td align="center" colSpan="2"><div style="WIDTH: 760px; HEIGHT: 180px; OVERFLOW: auto" id="spdiv" onscroll="SetsDivPosition();"><asp:repeater id="rptrcomprev" runat="server">
													<HeaderTemplate>
														<table>
															<tr>
																<td class="btmmenu plainlabel" width="210px"><asp:Label id="lang1987" runat="server">Component</asp:Label></td>
																<td class="btmmenu plainlabel" width="15px"><img src="../images/appbuttons/minibuttons/gridpic.gif"></td>
																<td class="btmmenu plainlabel" width="255px"><asp:Label id="lang1988" runat="server">Description</asp:Label></td>
																<td class="btmmenu plainlabel" width="70px"><asp:Label id="lang1989" runat="server">Total Tasks</asp:Label></td>
																<td class="btmmenu plainlabel" width="170px"><asp:Label id="lang1990" runat="server">Parent Function</asp:Label></td>
															</tr>
													</HeaderTemplate>
													<ItemTemplate>
														<tr class="tbg">
															<td class="plainlabel">
																<asp:RadioButton AutoPostBack="True" OnCheckedChanged="GetCo" id="rbfu" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' runat="server" /></td>
															<td><img src="../images/appbuttons/minibuttons/gridpic.gif" id="imgpic" runat="server"></td>
															<td class="plainlabel">
																<asp:Label ID="lblcdesc" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>' Runat = server>
																</asp:Label></td>
															<td class="plainlabel"><asp:Label ID="lbltskcnt" Text='<%# DataBinder.Eval(Container.DataItem,"tskcnt")%>' Runat = server>
																</asp:Label></td>
															<td class="plainlabel"><asp:Label ID="lblfunc" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>' Runat = server>
																</asp:Label></td>
															<td class="details">
																<asp:Label ID="lblcomprevid" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcspl" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcdesig" Text='<%# DataBinder.Eval(Container.DataItem,"desig")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfdesc" Text='<%# DataBinder.Eval(Container.DataItem,"func_desc")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfspl" Text='<%# DataBinder.Eval(Container.DataItem,"fspl")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblecby" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblmby" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblcdate" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblmdate" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleqnum" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lbleqspl" Text='<%# DataBinder.Eval(Container.DataItem,"eqspl")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfcd" Text='<%# DataBinder.Eval(Container.DataItem,"fcd")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfcby" Text='<%# DataBinder.Eval(Container.DataItem,"fcby")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfcph" Text='<%# DataBinder.Eval(Container.DataItem,"fcph")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfcm" Text='<%# DataBinder.Eval(Container.DataItem,"fcm")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfmph" Text='<%# DataBinder.Eval(Container.DataItem,"fmph")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblfmm" Text='<%# DataBinder.Eval(Container.DataItem,"fmm")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblecph" Text='<%# DataBinder.Eval(Container.DataItem,"ecph")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblecm" Text='<%# DataBinder.Eval(Container.DataItem,"ecm")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblemph" Text='<%# DataBinder.Eval(Container.DataItem,"emph")%>' Runat = server>
																</asp:Label></td>
															<td class="details"><asp:Label ID="lblemm" Text='<%# DataBinder.Eval(Container.DataItem,"emm")%>' Runat = server>
																</asp:Label></td>
														</tr>
													</ItemTemplate>
													<FooterTemplate>
							</table>
							</FooterTemplate> </asp:repeater></DIV></td>
					</tr>
				</TBODY>
			</table>
			</TD></TR>
			<tr>
				<td align="center" colSpan="3">
					<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
						cellSpacing="0" cellPadding="0">
						<tr>
							<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
									runat="server"></td>
							<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
									runat="server"></td>
							<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="220"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
							<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
									runat="server"></td>
							<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
									runat="server"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="details">
				<td class="bluelabel" id="tdpg" colSpan="2" runat="server"></td>
				<td><asp:imagebutton id="btnprev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnnext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif"></asp:imagebutton></td>
			</tr>
			</TBODY></TABLE></TD></TR></TBODY></TABLE>
			<table class="details" id="tdoldcomphdr" width="780" runat="server">
				<TBODY>
					<tr>
						<td colSpan="5">
							<div>
								<hr color="blue" SIZE="1">
							</div>
						</td>
					</tr>
					<tr>
						<td style="width: 130px"></td>
						<td style="width: 130px"></td>
						<td style="width: 100px"></td>
						<td width="230"></td>
						<td width="40"></td>
					</tr>
					<tr>
						<td class="bluelabel"><asp:Label id="lang1991" runat="server">You Selected:</asp:Label></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang1992" runat="server">Component#</asp:Label></td>
						<td class="plainlabel" id="tdoldcomp" colSpan="43" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang1993" runat="server">Description</asp:Label></td>
						<td class="plainlabel" id="tdolddesc" colSpan="4" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang1994" runat="server">Special Indentifier</asp:Label></td>
						<td class="plainlabel" id="tdoldspl" colSpan="4" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang1995" runat="server">Designation</asp:Label></td>
						<td class="plainlabel" id="tdolddesig" colSpan="3" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang1996" runat="server"># of Tasks</asp:Label></td>
						<td class="plainlabel" id="tdtskcnt" colSpan="3" runat="server"></td>
					</tr>
					<tr>
						<td class="bluelabel"><asp:Label id="lang1997" runat="server">New Component#</asp:Label></td>
						<td>
							<asp:TextBox id="txtnewcomp" runat="server" style="width: 150px" MaxLength="100"></asp:TextBox></td>
						<td class="bluelabel"><asp:Label id="lang1998" runat="server">Description</asp:Label></td>
						<td colspan="2">
							<asp:TextBox id="txtnewdesc" runat="server" Width="260px" MaxLength="100"></asp:TextBox></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td class="label"><asp:checkbox id="cbfm" runat="server" Width="104px" Text="Failure Modes"></asp:checkbox>
							<asp:checkbox id="cbtsks" runat="server" Width="54px" Text="Tasks"></asp:checkbox></td>
						<td align="right"><asp:imagebutton id="ibtncopy" runat="server" ImageUrl="../images/appbuttons/bgbuttons/copy.gif"></asp:imagebutton></td>
					</tr>
					<tr>
						<td colSpan="5">
							<div>
								<hr color="blue" SIZE="1">
							</div>
						</td>
					</tr>
				</TBODY>
			</table>
			<table id="tdoldcompdet" width="780" runat="server">
				<tr>
					<td style="width: 120px"></td>
					<td style="width: 270px"></td>
					<td style="width: 120px"></td>
					<td style="width: 270px"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1999" runat="server">Equipment#</asp:Label></td>
					<td class="plainlabel" id="tdeqnum" colspan="3" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2000" runat="server">Description</asp:Label></td>
					<td class="plainlabel" id="tdeqdesc" colspan="3" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2001" runat="server">Special Identifier</asp:Label></td>
					<td class="plainlabel" id="tdeqspl" colspan="3" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2002" runat="server">Created By</asp:Label></td>
					<td class="plainlabel" id="tdecby" runat="server"></td>
					<td class="label"><asp:Label id="lang2003" runat="server">Create Date</asp:Label></td>
					<td class="plainlabel" id="tdecd" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2004" runat="server">Phone</asp:Label></td>
					<td class="plainlabel" id="tdecph" runat="server"></td>
					<td class="label"><asp:Label id="lang2005" runat="server">Email</asp:Label></td>
					<td class="plainlabel" id="tdecm" runat="server"></td>
				</tr>
				<tr>
					<td colSpan="4">
						<div>
							<hr color="blue" SIZE="1">
						</div>
					</td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2006" runat="server">Function#</asp:Label></td>
					<td class="plainlabel" id="tdfunc" colspan="3" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2007" runat="server">Special Identifier</asp:Label></td>
					<td class="plainlabel" id="tdfspl" colspan="3" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2008" runat="server">Created By</asp:Label></td>
					<td class="plainlabel" id="tdfcby" runat="server"></td>
					<td class="label"><asp:Label id="lang2009" runat="server">Create Date</asp:Label></td>
					<td class="plainlabel" id="tdfcd" runat="server"></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang2010" runat="server">Phone</asp:Label></td>
					<td class="plainlabel" id="tdfcph" runat="server"></td>
					<td class="label"><asp:Label id="lang2011" runat="server">Email</asp:Label></td>
					<td class="plainlabel" id="tdfcm" runat="server"></td>
				</tr>
			</table>
			<table class="details" id="tblnewcomphdr" runat="server" width="660">
				<TR height="24">
					<td colSpan="3" class="thdrsingrt label"><asp:Label id="lang2012" runat="server">New Component Details</asp:Label></td>
				</TR>
				<tr>
					<td colSpan="2">&nbsp;</td>
					<td align="right"><asp:imagebutton id="ibtnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton></td>
				</tr>
			</table>
			<table class="details" id="tblnewcompdet" width="660" runat="server">
				<TBODY>
					<tr>
						<td class="label" style="width: 120px"><asp:Label id="lang2013" runat="server">Component#</asp:Label></td>
						<td class="plainlabel" id="tdnewcomp" width="540" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2014" runat="server">Description</asp:Label></td>
						<td class="plainlabel" id="Td1" width="330" runat="server"><asp:textbox id="txtdesc" runat="server" Width="415px" Rows="1" MaxLength="100"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2015" runat="server">Special Identifier</asp:Label></td>
						<td class="plainlabel" id="tdnewspl" width="330" runat="server"><asp:textbox id="txtnewspl" runat="server" Width="415px" Rows="1" MaxLength="200"></asp:textbox></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2016" runat="server">Designation</asp:Label></td>
						<td class="plainlabel" id="Td2" width="330" runat="server"><asp:textbox id="txtdesig" runat="server" Width="415px" Rows="1" MaxLength="200"></asp:textbox></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2017" runat="server">Equipment#</asp:Label></td>
						<td class="plainlabel" id="tdneweq" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2018" runat="server">Description</asp:Label></td>
						<td class="plainlabel" id="tdneweqdesc" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2019" runat="server">Special Identifier</asp:Label></td>
						<td class="plainlabel" id="tdneweqspl" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2020" runat="server">Created By</asp:Label></td>
						<td class="plainlabel" id="tdnewecby" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2021" runat="server">Phone</asp:Label></td>
						<td class="plainlabel" id="tdneweqecph" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2022" runat="server">Email</asp:Label></td>
						<td class="plainlabel" id="tdneweqemm" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2023" runat="server">Create Date</asp:Label></td>
						<td class="plainlabel" id="tdneweqecd" runat="server"></td>
					</tr>
					<tr>
						<td colSpan="3">
							<div>
								<hr color="blue" SIZE="1">
							</div>
						</td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2024" runat="server">Function#</asp:Label></td>
						<td class="plainlabel" id="tdnewfunc" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2025" runat="server">Description</asp:Label></td>
						<td class="plainlabel" id="tdnewfuncdesc" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2026" runat="server">Special Identifier</asp:Label></td>
						<td class="plainlabel" id="tdnewfuncspl" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2027" runat="server">Created By</asp:Label></td>
						<td class="plainlabel" id="tdnewfuncfcby" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2028" runat="server">Phone</asp:Label></td>
						<td class="plainlabel" id="tdnewfuncfcph" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2029" runat="server">Email</asp:Label></td>
						<td class="plainlabel" id="tdnewfuncfmm" runat="server"></td>
					</tr>
					<tr>
						<td class="label"><asp:Label id="lang2030" runat="server">Create Date</asp:Label></td>
						<td class="plainlabel" id="tdnewfuncfcd" runat="server"></td>
					</tr>
				</TBODY>
			</table>
			<input type="hidden" id="lblcid" runat="server"> <input type="hidden" id="lbleqid" runat="server">
			<input type="hidden" id="lblfuid" runat="server"><input type="hidden" id="txtpg" runat="server">
			<input type="hidden" id="lblcoid" runat="server"><input type="hidden" id="lblnewcomid" runat="server">
			<input type="hidden" id="lblsid" runat="Server"><input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lbldid" runat="server"><input type="hidden" id="lblclid" runat="server">
			<input type="hidden" id="lblusername" runat="server"> <input id="lblret" type="hidden" name="lblret" runat="server">
			<input id="Hidden1" type="hidden" name="Hidden1" runat="server"> <input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit"><input type="hidden" id="spdivy" runat="server" NAME="spdivy">
			<input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
