<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CompCopyMini.aspx.vb"
    Inherits="lucy_r12.CompCopyMini" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>CompCopyMini</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <link rel="stylesheet" type="text/css" href="../styles/reports.css">
    <script  src="../scripts/gridnav.js"></script>
    <script  type="text/javascript">
		<!--
        function upsearch() {
            //alert("test")
            document.getElementById("lblret").value = "first";
            document.getElementById("form1").submit();

        }
        function checkcb(who) {
            try {
                var al = document.getElementById("cball");
            }
            catch (err) {

            }
            var ow = document.getElementById("cbow");
            var dl = document.getElementById("cbdl");
            var cp1 = document.getElementById("cbcpyo1");
            var cp2 = document.getElementById("cbcpyo2");
            var fm = document.getElementById("cbfm");
            var tsk = document.getElementById("cbtsks");
            if (who == "ow") {
                try {
                    al.checked = false;
                }
                catch (err) {

                }
                dl.checked = false;
                cp1.checked = false;
                cp2.checked = false;
            }
            else if (who == "dl") {
                try {
                    al.checked = false;
                }
                catch (err) {

                }
                ow.checked = false;
                cp1.checked = false;
                cp2.checked = false;
            }
            else if (who == "cpyo1") {
                try {
                    al.checked = false;
                }
                catch (err) {

                }
                ow.checked = false;
                dl.checked = false;
                cp2.checked = false;
                fm.checked = false;
                tsk.checked = false;
                document.getElementById("tdnewcompenter").className = "graylabel";
                document.getElementById("tdnewcompenterdesc").className = "graylabel";
                document.getElementById("txtnewcomp").disabled = true;
                document.getElementById("txtnewdesc").disabled = true;
            }
            else if (who == "cpyo2") {
                try {
                    al.checked = false;
                }
                catch (err) {

                }
                ow.checked = false;
                dl.checked = false;
                cp1.checked = false;
                fm.checked = false;
                tsk.checked = false;
                document.getElementById("tdnewcompenter").className = "graylabel";
                document.getElementById("tdnewcompenterdesc").className = "graylabel";
                document.getElementById("txtnewcomp").disabled = true;
                document.getElementById("txtnewdesc").disabled = true;
            }
            else if (who == "cfm") {
                cp1.checked = false;
                cp2.checked = false;
                tsk.checked = false;
                if (fm.checked = false) {
                    tsk.checked = false;
                }
                else {
                    fm.checked = true;
                }
            }
            else if (who == "ctsk") {
                cp1.checked = false;
                cp2.checked = false;

            }

            if (who != "cpyo1" && who != "cpyo2") {
            //alert(who)
                document.getElementById("tdnewcompenter").className = "bluelabel";
                document.getElementById("tdnewcompenterdesc").className = "bluelabel";
                document.getElementById("txtnewcomp").disabled = false;
                document.getElementById("txtnewdesc").disabled = false;
                try {
                    if (who == "al") {
                    //alert("al")
                        ow.checked = false;
                        dl.checked = false;
                        cp1.checked = false;
                        cp2.checked = false;
                        fm.checked = true;
                        tsk.checked = true;
                        document.getElementById("tdnewcompenter").className = "graylabel";
                        document.getElementById("tdnewcompenterdesc").className = "graylabel";
                        document.getElementById("txtnewcomp").disabled = true;
                        document.getElementById("txtnewdesc").disabled = true;
                    }
                }
                catch (err) {

                }
            }
        }
        function getfuport(eq, fu, co) {
            window.open("../equip/pmportfolio.aspx?eqid=" + eq + "&fuid=" + fu + "&comid=" + co + "&date=" + Date());
        }
        function handlereturn() {
            var id = document.getElementById("lblnewcomid").value;
            if (id != "") {
                window.parent.handlereturn(id);
            }
            else {
                window.parent.handlereturn("no");
            }
        }
        function setref() {
            //document.getElementById("form1").submit();
            //window.parent.handlereturn("log");
        }
        function checkfu() {
            var id = document.getElementById("lblnewcomid").value;
            if (id != "") {
                window.parent.handlefu(id);
            }
        }
        function checkpg() {
            var who = document.getElementById("lblcurtab").value;
            var cb = document.getElementById("cbmopt");
            /*
            if (who == "tdmc") {
            cb.disabled = false;
            }
            else {
            cb.disabled = true;
            }
            */
            var log = document.getElementById("lbllog").value;

            if (log == "no") setref();
            else if (log == "noeqid") window.parent.handlereturn("no");
            else window.setTimeout("setref();", 300000);

            GetsScroll();
            var chk = document.getElementById("lblret").value;
            if (chk == "getnew") {
                var comid = document.getElementById("lblnewcomid").value;
                //alert()
                getedit2(comid);
            }
        }
        function GetsScroll() {
            var strY = document.getElementById("spdivy").value;
            if (strY.indexOf("!~") != 0) {
                var intS = strY.indexOf("!~");
                var intE = strY.indexOf("~!");
                var strPos = strY.substring(intS + 2, intE);
                document.getElementById("spdiv").scrollTop = strPos;
            }
        }
        function SetsDivPosition() {
            var intY = document.getElementById("spdiv").scrollTop;
            document.getElementById("spdivy").value = "yPos=!~" + intY + "~!";
        }
        function gettab(who) {
            document.getElementById("lblret").value = "return";
            document.getElementById("lblcurtab").value = who;
            document.getElementById("form1").submit();
        }
        function getedit2(comid) {
            //alert("1")
            var tpm = document.getElementById("lbltpm").value;
            try {
                var eReturn = window.showModalDialog("../complib/complibadddialog.aspx?tpm=" + tpm + "&typ=pm&comid=" + comid + "&date=" + Date(), "", "dialogHeight:700px; dialogWidth:800px; resizable=yes");
                if (eReturn) {
                    //document.getElementById("lblret").value = "checkcomp";
                    //document.getElementById("form1").submit()
                }
            }
            catch (err) {

            }
        }
        function getold() {
            /*
            document.getElementById("ifrep").src="";
            document.getElementById("tblnewcompedit").className="details";
            document.getElementById("tdoldcomphdr").className="view";
            document.getElementById("tdoldcompdet").className="view";
            document.getElementById("tbltop").className="view";
            document.getElementById("tdoldcomptasks").className="view";
            */
            document.getElementById("lblret").value = "return";
            document.getElementById("form1").submit();
        }
        function FreezeScreen(msg) {
            try {
                scroll(0, 0);
                var outerPane = document.getElementById('FreezePane');
                var innerPane = document.getElementById('InnerFreezePane');
                if (outerPane) outerPane.className = 'FreezePaneOn';
                if (innerPane) innerPane.innerHTML = msg;
            }
            catch (err) {

            }
        }
		-->
    </script>
</head>
<body class="tbg" onload="checkpg();">
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <table id="tbltop" width="760" runat="server">
        <tbody>
            <tr height="24">
                <td class="thdrsing label" colspan="4">
                    Component Copy Mini Dialog
                </td>
            </tr>
            <tr id="tbleqrec" runat="server">
                <td class="bluelabel" style="width: 80px">
                    Search
                </td>
                <td width="280">
                    <asp:TextBox ID="txtsrch" runat="server" Width="250px"></asp:TextBox><asp:ImageButton
                        ID="ibtnsearch" runat="server" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif">
                    </asp:ImageButton>
                </td>
                <td width="300" class="bluelabel" id="tdopts" runat="server">
                    <input type="checkbox" id="cbmopt" runat="server" onclick="upsearch();" checked />Components
                    with Complete Tasks Only
                </td>
                <td style="width: 100px" align="right">
                    <img id="hpBackward" onclick="handlereturn();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
                        width="69" height="19" runat="server">
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table>
                        <tr height="22">
                            <td id="tdmc" class="thdrhov label" onclick="gettab('tdmc');" style="width: 150px" runat="server">
                                <a class="label" href="#">Components In Progress</a>
                            </td>
                            <td id="tdsc" class="thdr label" onclick="gettab('tdsc');" style="width: 150px" runat="server">
                                <a class="label" href="#">Plant Site Components</a>
                            </td>
                            <td id="tdac" class="thdr label" onclick="gettab('tdac');" style="width: 150px" runat="server">
                                <a class="label" href="#">Master List</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <table>
                        <tbody>
                            <tr>
                                <td colspan="2" align="center">
                                    <div style="width: 760px; height: 180px; overflow: auto" onscroll="SetsDivPosition();"
                                        id="spdiv">
                                        <asp:Repeater ID="rptrcomprev" runat="server">
                                            <HeaderTemplate>
                                                <table>
                                                    <tr>
                                                        <td class="btmmenu plainlabel" width="190px">
                                                            Component
                                                        </td>
                                                        <td class="btmmenu plainlabel" width="190px">
                                                            Search Key
                                                        </td>
                                                        <td class="btmmenu plainlabel" width="15px">
                                                            <img src="../images/appbuttons/minibuttons/gridpic.gif">
                                                        </td>
                                                        <td class="btmmenu plainlabel" width="315px">
                                                            Description
                                                        </td>
                                                        <td class="btmmenu plainlabel" width="270px">
                                                            Parent Function
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class="tbg">
                                                    <td class="plainlabel">
                                                        <asp:RadioButton AutoPostBack="True" OnCheckedChanged="GetCo" ID="rbfu" Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>'
                                                            runat="server" />
                                                    </td>
                                                    <td class="plainlabel">
                                                        <asp:Label ID="lblck" Text='<%# DataBinder.Eval(Container.DataItem,"compkey")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td>
                                                        <img src="../images/appbuttons/minibuttons/gridpic.gif" id="imgpic" runat="server">
                                                    </td>
                                                    <td class="plainlabel">
                                                        <asp:Label ID="lblcdesc" Text='<%# DataBinder.Eval(Container.DataItem,"compdesc")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lbltskcnt" Text='<%# DataBinder.Eval(Container.DataItem,"tskcnt")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="plainlabel">
                                                        <asp:Label ID="lblfunc" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblcomprevid" Text='<%# DataBinder.Eval(Container.DataItem,"comid")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblcspl" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblcdesig" Text='<%# DataBinder.Eval(Container.DataItem,"desig")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblfdesc" Text='<%# DataBinder.Eval(Container.DataItem,"func_desc")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblfspl" Text='<%# DataBinder.Eval(Container.DataItem,"fspl")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblecby" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblmby" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblcdate" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblmdate" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lbleqnum" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lbleqspl" Text='<%# DataBinder.Eval(Container.DataItem,"eqspl")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblfcd" Text='<%# DataBinder.Eval(Container.DataItem,"fcd")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblfcby" Text='<%# DataBinder.Eval(Container.DataItem,"fcby")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblfcph" Text='<%# DataBinder.Eval(Container.DataItem,"fcph")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblfcm" Text='<%# DataBinder.Eval(Container.DataItem,"fcm")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblfmph" Text='<%# DataBinder.Eval(Container.DataItem,"fmph")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblfmm" Text='<%# DataBinder.Eval(Container.DataItem,"fmm")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblecph" Text='<%# DataBinder.Eval(Container.DataItem,"ecph")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblecm" Text='<%# DataBinder.Eval(Container.DataItem,"ecm")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblemph" Text='<%# DataBinder.Eval(Container.DataItem,"emph")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblemm" Text='<%# DataBinder.Eval(Container.DataItem,"emm")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblmfg" Text='<%# DataBinder.Eval(Container.DataItem,"mfg")%>' runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblac" Text='<%# DataBinder.Eval(Container.DataItem,"assetclass")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="details">
                                                        <asp:Label ID="lblsc" Text='<%# DataBinder.Eval(Container.DataItem,"subclass")%>'
                                                            runat="server">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                        border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: blue 1px solid" style="width: 20px">
                                <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" style="width: 20px">
                                <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                    runat="server">
                            </td>
                            <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                            </td>
                            <td style="border-right: blue 1px solid" style="width: 20px">
                                <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                    runat="server">
                            </td>
                            <td style="width: 20px">
                                <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="details">
                <td id="tdpg" class="bluelabel" colspan="2" runat="server">
                </td>
                <td>
                    <asp:ImageButton ID="btnprev" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bprev.gif">
                    </asp:ImageButton>&nbsp;<asp:ImageButton ID="btnnext" runat="server" ImageUrl="../images/appbuttons/bgbuttons/bnext.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </tbody>
    </table>
    </TD></TR></TBODY></TABLE>
    <table id="tdoldcomphdr" class="details" width="760" runat="server">
        <tbody>
            <tr height="24">
                <td class="thdrsing label" colspan="5">
                    New Component Details
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <table border="0">
                        <tr>
                            <td class="redlabel" height="20">
                                Current Component
                            </td>
                            <td id="tdcurcomp" class="plainlabelred" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel" style="width: 140px" id="tdnewcompenter">
                                New Component#
                            </td>
                            <td width="240">
                                <asp:TextBox ID="txtnewcomp" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
                            </td>
                            <td class="bluelabel" style="width: 80px" id="tdnewcompenterdesc">
                                Description
                            </td>
                            <td width="240">
                                <asp:TextBox ID="txtnewdesc" runat="server" Width="220px" MaxLength="100"></asp:TextBox>
                            </td>
                            <td style="width: 60px">
                            </td>
                        </tr>
                        <tr>
                            <td class="redlabel" colspan="2">
                                <asp:CheckBox ID="cball" runat="server" Text="Replace All Similar Components?"></asp:CheckBox><br />
                                <asp:CheckBox ID="cbow" runat="server" Text="Replace Current Component?"></asp:CheckBox><br />
                                <asp:CheckBox ID="cbdl" runat="server" Text="Mark Current Component As Delete?">
                                </asp:CheckBox><br />
                                <asp:CheckBox ID="cbcpyo1" runat="server" Text="Just Add Tasks to Current Component">
                                </asp:CheckBox><br />
                                <asp:CheckBox ID="cbcpyo2" runat="server" Text="Add Tasks to Current Component and Mark Current Tasks as Delete">
                                </asp:CheckBox>
                            </td>
                            <td class="label" colspan="2" valign="top">
                                <asp:CheckBox ID="cbfm" runat="server" Checked Width="104px" Text="Failure Modes">
                                </asp:CheckBox>
                                <asp:CheckBox ID="cbtsks" Checked runat="server" Width="54px" Text="Tasks"></asp:CheckBox>
                                <input type="checkbox" checked id="cbrat" runat="server" name="cbrat">Rationale<br />
                                <input id="rbro" type="radio" name="rbroa" runat="server" value="rbro">Revised Only&nbsp;&nbsp;
                                <input id="rbrs" type="radio" name="rbroa" runat="server" value="rbrs">Revised Side
                                Only&nbsp;&nbsp;
                                <input id="rbai" checked type="radio" name="rbroa" runat="server" value="rbai">As
                                Is
                            </td>
                            <td align="right" valign="top">
                                <asp:ImageButton ID="ibtncopy" runat="server" ImageUrl="../images/appbuttons/bgbuttons/copy.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="24">
                <td class="thdrsing label" colspan="5">
                    Component Details
                </td>
            </tr>
            <tr>
                <td style="width: 130px">
                </td>
                <td style="width: 130px">
                </td>
                <td style="width: 100px">
                </td>
                <td width="230">
                </td>
                <td width="40">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Component#
                </td>
                <td id="tdoldcomp" class="plainlabel" colspan="4" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Description
                </td>
                <td id="tdolddesc" class="plainlabel" colspan="4" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Special Indentifier
                </td>
                <td id="tdoldspl" class="plainlabel" colspan="4" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Designation
                </td>
                <td id="tdolddesig" class="plainlabel" colspan="3" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Mfg
                </td>
                <td id="tdoldmfg" class="plainlabel" colspan="3" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Component Class
                </td>
                <td id="tdoldac" class="plainlabel" colspan="3" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Sub Class
                </td>
                <td id="tdoldsc" class="plainlabel" colspan="3" runat="server">
                </td>
            </tr>
            <tr class="details">
                <td class="label">
                    # of Tasks
                </td>
                <td id="tdtskcnt" class="plainlabel" colspan="3" runat="server">
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <div>
                        <hr color="blue" size="1">
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <table id="tdoldcompdet" width="760" runat="server">
        <tr>
            <td style="width: 120px">
            </td>
            <td style="width: 270px">
            </td>
            <td style="width: 120px">
            </td>
            <td style="width: 270px">
            </td>
        </tr>
        <tr>
            <td class="label">
                Equipment#
            </td>
            <td id="tdeqnum" class="plainlabel" colspan="3" runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Description
            </td>
            <td id="tdeqdesc" class="plainlabel" colspan="3" runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Special Identifier
            </td>
            <td id="tdeqspl" class="plainlabel" colspan="3" runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Created By
            </td>
            <td id="tdecby" class="plainlabel" runat="server">
            </td>
            <td class="label">
                Create Date
            </td>
            <td id="tdecd" class="plainlabel" runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Phone
            </td>
            <td id="tdecph" class="plainlabel" runat="server">
            </td>
            <td class="label">
                Email
            </td>
            <td id="tdecm" class="plainlabel" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div>
                    <hr color="blue" size="1">
                </div>
            </td>
        </tr>
        <tr>
            <td class="label">
                Function#
            </td>
            <td id="tdfunc" class="plainlabel" colspan="3" runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Special Identifier
            </td>
            <td id="tdfspl" class="plainlabel" colspan="3" runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Created By
            </td>
            <td id="tdfcby" class="plainlabel" runat="server">
            </td>
            <td class="label">
                Create Date
            </td>
            <td id="tdfcd" class="plainlabel" runat="server">
            </td>
        </tr>
        <tr>
            <td class="label">
                Phone
            </td>
            <td id="tdfcph" class="plainlabel" runat="server">
            </td>
            <td class="label">
                Email
            </td>
            <td id="tdfcm" class="plainlabel" runat="server">
            </td>
        </tr>
    </table>
    <table id="tdoldcomptasks" width="760" runat="server">
        <tr height="24">
            <td class="thdrsing label" colspan="5">
                Component Tasks
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <iframe style="border-bottom-style: none; border-right-style: none; background-color: transparent;
                    border-top-style: none; border-left-style: none" id="geteq" height="100" src="../complib/complibtaskview2.aspx?typ=pm"
                    frameborder="no" width="780"  scrolling="yes" runat="server">
                </iframe>
            </td>
        </tr>
    </table>
    <table id="tblnewcompedit" class="details" width="740" runat="server">
        <tr>
            <td align="right">
                <img src="../images/appbuttons/bgbuttons/return.gif" onclick="getold();">
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="ifrep" runat="server" src="" width="740" height="630" frameborder="no">
                </iframe>
            </td>
        </tr>
    </table>
    <table id="tblnewcomphdr" class="details" width="760" runat="server">
        <tr height="24">
            <td class="thdrsingrt label" colspan="3">
                New Component Details
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
            <td align="right">
                <asp:ImageButton ID="ibtnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif">
                </asp:ImageButton>
            </td>
        </tr>
    </table>
    <table id="tblnewcompdet" class="details" width="760" runat="server">
        <tbody>
            <tr>
                <td class="label" style="width: 120px">
                    Component#
                </td>
                <td id="tdnewcomp" class="plainlabel" width="540" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Description
                </td>
                <td id="Td1" class="plainlabel" width="330" runat="server">
                    <asp:TextBox ID="txtdesc" runat="server" Width="415px" MaxLength="100" Rows="1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Special Identifier
                </td>
                <td id="tdnewspl" class="plainlabel" width="330" runat="server">
                    <asp:TextBox ID="txtnewspl" runat="server" Width="415px" MaxLength="200" Rows="1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Designation
                </td>
                <td id="Td2" class="plainlabel" width="330" runat="server">
                    <asp:TextBox ID="txtdesig" runat="server" Width="415px" MaxLength="200" Rows="1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Mfg
                </td>
                <td>
                    <asp:TextBox ID="txtmfg" runat="server" Width="250px" MaxLength="200" CssClass="plainlabel wd250"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Component Class
                </td>
                <td>
                    <asp:DropDownList ID="ddac" runat="server" Width="248px" CssClass="plainlabel">
                        <asp:ListItem Value="Select Asset Class">Select Component Class</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="label">
                    Equipment#
                </td>
                <td id="tdneweq" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Description
                </td>
                <td id="tdneweqdesc" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Special Identifier
                </td>
                <td id="tdneweqspl" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Created By
                </td>
                <td id="tdnewecby" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Phone
                </td>
                <td id="tdneweqecph" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Email
                </td>
                <td id="tdneweqemm" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Create Date
                </td>
                <td id="tdneweqecd" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div>
                        <hr color="blue" size="1">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Function#
                </td>
                <td id="tdnewfunc" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Description
                </td>
                <td id="tdnewfuncdesc" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Special Identifier
                </td>
                <td id="tdnewfuncspl" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Created By
                </td>
                <td id="tdnewfuncfcby" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Phone
                </td>
                <td id="tdnewfuncfcph" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Email
                </td>
                <td id="tdnewfuncfmm" class="plainlabel" runat="server">
                </td>
            </tr>
            <tr>
                <td class="label">
                    Create Date
                </td>
                <td id="tdnewfuncfcd" class="plainlabel" runat="server">
                </td>
            </tr>
        </tbody>
    </table>
    <input id="lblcid" type="hidden" runat="server">
    <input id="lbleqid" type="hidden" runat="server">
    <input id="lblfuid" type="hidden" runat="server"><input id="txtpg" type="hidden"
        runat="server">
    <input id="lblcoid" type="hidden" runat="server"><input id="lblnewcomid" type="hidden"
        runat="server">
    <input id="lblsid" type="hidden" runat="Server"><input id="lbllog" type="hidden"
        name="lbllog" runat="server">
    <input id="lbldid" type="hidden" runat="server"><input id="lblclid" type="hidden"
        runat="server">
    <input id="lblusername" type="hidden" runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server">
    <input id="lblcurtab" type="hidden" name="Hidden1" runat="server">
    <input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="spdivy"
        type="hidden" name="spdivy" runat="server">
    <input id="lblro" type="hidden" runat="server">
    <input id="lbloloc" type="hidden" runat="server">
    <input id="lbldb" type="hidden" runat="server">
    <input id="lblnewcompkey" type="hidden" runat="server">
    <input id="lbloldcoid" type="hidden" runat="server">
    <input type="hidden" id="lbltpm" runat="server">
    <input type="hidden" id="lbltskcnt" runat="server" />
    <input type="hidden" id="lblcoi" runat="server" />
    <input type="hidden" id="lblcurcomp" runat="server" />
    <input type="hidden" id="lblfslang" runat="server" />
    </form>
</body>
</html>
