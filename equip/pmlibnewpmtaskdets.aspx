<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibnewpmtaskdets.aspx.vb" Inherits="lucy_r12.pmlibnewpmtaskdets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibnewpmtaskdets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  src="../scripts1/pmlibnewpmtaskdetsaspx_2.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     
	</HEAD>
	<body  onload="setref();">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 0px">
				<TBODY>
					<tr id="trmsg" runat="server" height="30" class="details">
						<td colspan="7" id="tdmsg" runat="server" class="plainlabelblue"></td>
					</tr>
					<tr>
						<td colSpan="7"><div style="WIDTH: 990px; HEIGHT: 260px; OVERFLOW: auto">
								<asp:repeater id="rptrtaskrev" Runat="server">
									<HeaderTemplate>
										<table width="1400">
											<tr>
												<td align="center" style="width: 20px" class="btmmenu plainlabel"><img src="../images/appbuttons/minibuttons/del.gif" width="16" height="16"></td>
												<td width="40" class="btmmenu plainlabel"><asp:Label id="lang2491" runat="server">Task#</asp:Label></td>
												<td width="70" class="btmmenu plainlabel"><asp:Label id="lang2492" runat="server">Sub Task#</asp:Label></td>
												<td width="200" class="btmmenu plainlabel"><asp:Label id="lang2493" runat="server">Component Addressed</asp:Label></td>
												<td width="30" class="btmmenu plainlabel">Qty</td>
												<td width="220" class="btmmenu plainlabel"><asp:Label id="lang2494" runat="server">Failure Modes Addressed</asp:Label></td>
												<td width="260" class="btmmenu plainlabel"><asp:Label id="lang2495" runat="server">Task Description</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2496" runat="server">Task Type</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2497" runat="server">Pdm</asp:Label></td>
												<td style="width: 90px" class="btmmenu plainlabel"><asp:Label id="lang2498" runat="server">Skill</asp:Label></td>
												<td width="30" class="btmmenu plainlabel">Qty</td>
												<td width="40" class="btmmenu plainlabel"><asp:Label id="lang2499" runat="server">Time</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2500" runat="server">Frequency</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2501" runat="server">Eq Status</asp:Label></td>
												<td style="width: 80px" class="btmmenu plainlabel"><asp:Label id="lang2502" runat="server">Down Time</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr class="tbg">
											<td class="plainlabel">
												<asp:CheckBox id="cb4" runat="server"></asp:CheckBox></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasknum")%>' Runat = server ID="lbltasknum">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"subtask")%>' Runat = server ID="lblsubtasknum">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"compnum")%>' Runat = server ID="Label11">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"cqty")%>' Runat = server ID="Label12">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"fm1")%>' Runat = server ID="Label13">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"taskdesc")%>' Runat = server ID="Label14">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"tasktype")%>' Runat = server ID="Label15">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"pretech")%>' Runat = server ID="Label16">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"skill")%>' Runat = server ID="Label17">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"qty")%>' Runat = server ID="Label18">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"ttime")%>' Runat = server ID="Label19">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"freq")%>' Runat = server ID="Label20">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rd")%>' Runat = server ID="Label21">
												</asp:Label></td>
											<td class="plainlabel">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"rdt")%>' Runat = server ID="Label22">
												</asp:Label></td>
											<td class="details">
												<asp:Label Text='<%# DataBinder.Eval(Container.DataItem,"pmtskid")%>' Runat = server ID="lbltaskid">
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD></TR>
			<tr>
				<td colSpan="7"><asp:imagebutton id="remtask" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"></asp:imagebutton>
					<img src="../images/appbuttons/minibuttons/lilpentrans.gif" onclick="gettasks();" onmouseover="return overlib('Add/Edit Tasks for Selected Function', LEFT)"
						onmouseout="return nd()"></td>
			</tr>
			</TBODY></TABLE> <input type="hidden" id="lbldb" runat="server" NAME="lbldb"> <input type="hidden" id="lblfuid" runat="server" NAME="lblfuid">
			<input type="hidden" id="lbllog" runat="server" NAME="lbllog"> <input type="hidden" id="lblsubmit" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
