<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddEditFuncPop.aspx.vb" Inherits="lucy_r12.AddEditFuncPop" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>AddEditFuncPop</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/AddEditFuncPopaspx_1.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onload="showFocus();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td class="thdrsing label" colspan="2"><asp:Label id="lang1909" runat="server">Add/Edit Function Mini Dialog</asp:Label></td>
				</tr>
				<tr>
					<td class="label" style="width: 150px"><asp:Label id="lang1910" runat="server">New Function Name/Code</asp:Label></td>
					<td width="450"><asp:textbox id="txtnewfunc" runat="server" style="width: 150px" CssClass="plainlabel" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td class="label"><asp:Label id="lang1911" runat="server">Special Identifier</asp:Label></td>
					<td><asp:textbox id="txtnewspl" runat="server" Width="210px" Rows="3" CssClass="plainlabel" MaxLength="100"></asp:textbox></td>
				</tr>
				<tr>
					<td></td>
					<td align="right"><asp:imagebutton id="ibaddfunc" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif"></asp:imagebutton>&nbsp;<IMG onclick="handlereturn();" alt="" src="../images/appbuttons/bgbuttons/return.gif"
							id="ibtnret" runat="server" width="69" height="19"></td>
				</tr>
				<tr>
					<td colSpan="2"><asp:datagrid id="dgfunc" runat="server" AutoGenerateColumns="False" AllowSorting="True" CellSpacing="1"
							GridLines="None">
							<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
							<ItemStyle CssClass="plainlabel"></ItemStyle>
							<HeaderStyle Height="26px" CssClass="btmmenu plainlabel"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Edit">
									<HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
											ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
											ToolTip="Save Changes" CommandName="Update"></asp:imagebutton>
										<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
											ToolTip="Cancel Changes" CommandName="Cancel"></asp:imagebutton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Sequence#">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblroute runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=txtrouting runat="server"   CssClass="plainlabel" style="width: 40px" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Function#">
									<HeaderStyle Width="240px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:textbox id=txtfunum runat="server"  CssClass="plainlabel" Width="230px" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.func") %>'>
										</asp:textbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="True" HeaderText="Special Designation">
									<HeaderStyle Width="300px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lbldesc runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.spl") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" id="txtspl"  CssClass="plainlabel" width="290" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.spl") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<ItemTemplate>
										<asp:Label runat="server" id="lblfuid" Text='<%# DataBinder.Eval(Container, "DataItem.func_id") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label runat="server" id="txtfuid" width="290" Text='<%# DataBinder.Eval(Container, "DataItem.func_id") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Sequence#" Visible="False">
									<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lbloldrte" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routing") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remove">
									<HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
									<ItemTemplate>
										&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:ImageButton id="ibdel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif" CommandName="Delete"></asp:ImageButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lbluser" type="hidden" runat="server"><input id="lblchk" type="hidden" runat="server">
			<input type="hidden" id="lblnewfuncid" runat="server"><input type="hidden" id="lblfunchold" runat="server">
			<input type="hidden" id="lbllog" runat="server"><input type="hidden" id="lblro" runat="server">
			<input type="hidden" id="lbldb" runat="server"> <input type="hidden" id="lbloloc" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
