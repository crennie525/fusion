<%@ Page Language="vb" AutoEventWireup="false" Codebehind="levels.aspx.vb" Inherits="lucy_r12.levels" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>levels</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/levelsaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="chkref();">
		<form id="form1" method="post" runat="server">
			<table style="Z-INDEX: 101; POSITION: absolute; TOP: 0px; LEFT: 0px" width="400">
				<TBODY>
					<tr>
						<td colSpan="4"><asp:label id="lblcat" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Small"
								ForeColor="Blue"></asp:label></td>
					</tr>
					<tr>
						<td style="width: 90px"></td>
						<td width="30"></td>
						<td style="width: 80px"></td>
						<td width="190"></td>
					</tr>
					<tr>
						<td colSpan="4">
							<hr style="BORDER-BOTTOM: #0000ff 2px solid; BORDER-LEFT: #0000ff 2px solid; BORDER-TOP: #0000ff 2px solid; BORDER-RIGHT: #0000ff 2px solid">
						</td>
					</tr>
					<tr>
						<td><asp:label id="Label2" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 5</asp:label><input id="rb5" onclick="document.getElementById('lbllev').value='5';" type="radio" value="rb5"
								name="rbw" runat="server"></td>
						<td colSpan="3"><asp:textbox id="t5" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td><asp:label id="Label3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 4</asp:label><input id="rb4" onclick="document.getElementById('lbllev').value='4';" type="radio" value="rb4"
								name="rbw" runat="server"></td>
						<td colSpan="3"><asp:textbox id="t4" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td><asp:label id="Label4" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 3</asp:label><input id="rb3" onclick="document.getElementById('lbllev').value='3';" type="radio" value="rb3"
								name="rbw" runat="server"></td>
						<td colSpan="3"><asp:textbox id="t3" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td><asp:label id="Label5" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 2</asp:label><input id="rb2" onclick="document.getElementById('lbllev').value='2';" type="radio" value="rb2"
								name="rbw" runat="server"></td>
						<td colSpan="3"><asp:textbox id="t2" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td><asp:label id="Label6" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Level 1</asp:label><input id="rb1" onclick="document.getElementById('lbllev').value='1';" type="radio" value="rb1"
								name="rbw" runat="server"></td>
						<td colSpan="3"><asp:textbox id="t1" runat="server" Font-Names="Arial" Font-Size="X-Small" TextMode="MultiLine"
								Width="300px" ReadOnly="True" CssClass="plainlabel" MaxLength="250"></asp:textbox></td>
					</tr>
					<tr>
						<td class="plainlabelblue" colSpan="4">
							<P><asp:Label id="lang2407" runat="server">Choose a level for this category</asp:Label></P>
						</td>
					</tr>
					<tr>
						<td colSpan="4">&nbsp;</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:label id="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Current ECR:</asp:label></td>
						<td colSpan="2"><asp:label id="tc" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Red"></asp:label></td>
					</tr>
					<tr>
						<td align="right" colSpan="4"><asp:imagebutton id="btnsave" runat="server" ImageUrl="../images/appbuttons/bgbuttons/save.gif"></asp:imagebutton>&nbsp;<IMG id="Image1" onclick="checkit();" src="../images/appbuttons/bgbuttons/return.gif"
								runat="server" width="69" height="19"></td>
					</tr>
					<tr>
						<td class="plainlabelblue" id="tdrcm" colSpan="4" runat="server"></td>
					</tr>
					<tr class="details" id="trdd" runat="server">
						<td class="label" colSpan="2"><asp:Label id="lang2408" runat="server">RCM Software</asp:Label></td>
						<td colSpan="2">
							<asp:DropDownList id="ddecm" runat="server">
								<asp:ListItem Value="0">Select</asp:ListItem>
								<asp:ListItem Value="1">Reliasoft RCM++</asp:ListItem>
								<asp:ListItem Value="2">Relex Reliability</asp:ListItem>
								<asp:ListItem Value="3">Isograph RCMCost</asp:ListItem>
							</asp:DropDownList></td>
					</tr>
				</TBODY>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lblecrid" type="hidden" name="lblecrid" runat="server"><input id="lbllev" type="hidden" name="lbllev" runat="server">
			<input id="lbleqid" type="hidden" name="lbleqid" runat="server"><input id="lblchk" type="hidden" name="lblchk" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
