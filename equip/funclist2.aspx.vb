

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Public Class funclist2
    Inherits System.Web.UI.Page
	Protected WithEvents lang2392 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql, eqid, Login, srchtyp, fuid As String
    Dim dr As SqlDataReader
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents divco As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents xCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents yCoord As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim rep As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents divfu As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsrchtyp As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        xCoord.Value = 0
        yCoord.Value = 0
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            'lbllog.Value = "no"
            'Exit Sub
        End Try
        If Not IsPostBack Then
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                srchtyp = Request.QueryString("srchtyp").ToString
                lblsrchtyp.Value = srchtyp
                rep.Open()
                GetFunctions()
                rep.Dispose()
            Catch ex As Exception

            End Try
        Else
            If Request.Form("lblsubmit") = "getco" Then
                lblsubmit.Value = ""
                rep.Open()
                GetComponents()
                rep.Dispose()
            End If
        End If
    End Sub
    Private Sub GetFunctions()
        eqid = lbleqid.Value
        Dim fi, fu As String
        Dim sb As New StringBuilder
        srchtyp = lblsrchtyp.Value

        sql = "select func_id, func from functions where eqid = '" & eqid & "' order by routing"
        dr = rep.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            fi = dr.Item("func_id").ToString
            fu = dr.Item("func").ToString
            sb.Append("<tr><td>")
            If srchtyp = "nofu" Then
                'sb.Append(fu)
                sb.Append("<a class=""A1"" href=""#"" onclick=""getdfu('" & fi & "','" & fu & "','down')"">")
                sb.Append(fu)
            Else
                sb.Append("<a class=""A1"" href=""#"" onclick=""getdfu('" & fi & "','" & fu & "','down')"">")
                sb.Append(fu)
                sb.Append("</td></tr>")
            End If
        End While
        dr.Close()
        sb.Append("</table>")
        divfu.InnerHtml = sb.ToString

    End Sub
    Private Sub GetComponents()

        fuid = lblfuid.Value
        Dim ci, co As String
        Dim sb As New StringBuilder
        srchtyp = lblsrchtyp.Value

        sql = "select comid, compnum from components where func_id = '" & fuid & "' order by crouting"
        dr = rep.GetRdrData(sql)
        sb.Append("<table>")
        While dr.Read
            ci = dr.Item("comid").ToString
            co = dr.Item("compnum").ToString
            sb.Append("<tr><td class=""plainlabel"">")
            If srchtyp = "noco" Or srchtyp = "nofu" Then
                sb.Append(co)
            Else
                sb.Append("<a class=""A1"" href=""#"" onclick=""getdco('" & ci & "','" & co & "')"">")
                sb.Append(co & "</a>")
            End If

            sb.Append("</td></tr>")

        End While
        dr.Close()
        sb.Append("</table>")
        divco.InnerHtml = sb.ToString

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2392.Text = axlabs.GetASPXPage("funclist2.aspx", "lang2392")
        Catch ex As Exception
        End Try

    End Sub

End Class
