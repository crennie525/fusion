﻿<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RPNSelection.aspx.vb" Inherits="lucy_r12.RPNSelection" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>RPN Selection</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<script type="text/javascript" src="../jqplot/jquery-1.11.2.min.js"></script>
		<link rel="stylesheet" type="text/css" href="../dashboard/dashboard.css" />
	</head>
	<body>
		<form id="form1" method="post" runat="server">
			<table style="width:100%;">
                <tr>
                    <td id="rpnQuestions" style="width:30%;vertical-align:top;">
                        <div id="OccuranceRate" class="dashboardLinkSelected">
                            <a onclick="QuestionSelected('OccuranceRate')">Occurance Rate</a>
                        </div>
                        <div id="DetectionProbability" class="dashboardLink">
                            <a onclick="QuestionSelected('DetectionProbability')">Detection Probability</a>
                        </div>
						<div id="QualityImpact" class="dashboardLink">
                            <a onclick="QuestionSelected('QualityImpact')">Quality Impact</a>
                        </div>
						<div id="DowntimeImpact" class="dashboardLink">
                            <a onclick="QuestionSelected('DowntimeImpact')">Downtime Impact</a>
                        </div>
						<div id="CostImpact" class="dashboardLink">
                            <a onclick="QuestionSelected('CostImpact')">Cost Impact</a>
                        </div>
						<div id="SafetyImpact" class="dashboardLink">
                            <a onclick="QuestionSelected('SafetyImpact')">Safety Impact</a>
                        </div>
                    </td>
                    <td id="rpnWeights" style="width:70%;vertical-align:top;">
						<div style="margin-top: 10px;">
							<input type="radio" name="level" value="5" style="margin:0;" />
							<b>Level 5</b> - <span id="sLevel5">Frequent - Likely to occur at least monthly</span>
						</div>
						<div>&nbsp;</div>
						<div>
							<input type="radio" name="level" value="4" style="margin:0;" />
							<b>Level 4</b> - <span id="sLevel4">Resonably Probable - Likely to occur at least once per year</span>
						</div>
						<div>&nbsp;</div>
						<div>
							<input type="radio" name="level" value="3" style="margin:0;" />
							<b>Level 3</b> - <span id="sLevel3">Probable - Likely to occur in a 1-10 year period</span>
						</div>
						<div>&nbsp;</div>
						<div>
							<input type="radio" name="level" value="2" style="margin:0;" />
							<b>Level 2</b> - <span id="sLevel2">Occasional - Likely to occur in a 10-25 year period</span>
						</div>
						<div>&nbsp;</div>
						<div>
							<input type="radio" name="level" value="1" style="margin:0;" />
							<b>Level 1</b> - <span id="sLevel1">Remote - Likely to occur in ops lifetime</span>
						</div>
						<div>&nbsp;</div>
						<div id="dLevel0">
							<input type="radio" name="level" value="0" style="margin:0;" />
							<b>Level 0</b> - Extremely Improbable - not likely to occur
						</div>
						<div>&nbsp;</div>
						<div>
							<b>RPN:</b>&nbsp;<span id="sRPN" style="font-weight:bold;color:red;"></span>
						</div>
						<div>&nbsp;</div>
						<div>
							<asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save All Levels"/>
						</div>
					</td>
                </tr>
            </table>
			<asp:HiddenField id="hSelectedQuestion" runat="server" Value="#hOccuranceRate" />
			<asp:HiddenField id="hOccuranceRate" runat="server" />
			<asp:HiddenField id="hDetectionProbability" runat="server" />
			<asp:HiddenField id="hQualityImpact" runat="server" />
			<asp:HiddenField id="hDowntimeImpact" runat="server" />
			<asp:HiddenField id="hCostImpact" runat="server" />
			<asp:HiddenField id="hSafetyImpact" runat="server" />
			<asp:HiddenField id="hRPN" runat="server" />
		</form>
		<script type="text/javascript">

		$(document).ready(function () {
			var value = $("#hOccuranceRate").val();
			if (value != "") {
				$("input[name='level'][value=" + value + "]").prop('checked', true);
			}

			SetRPN();

			$("input[name='level']").click(function () {
				$($("#hSelectedQuestion").val()).val($("input[name='level']:checked").val());
				SetRPN();
			});
		});

		function SetRPN() {
			var rpn = GetRPN();
			$("#hRPN").val(rpn);
			$("#sRPN").text(rpn);
		}

		function GetRPN() {
			var occuranceRate = parseInt($("#hOccuranceRate").val());
			var detectionProbability = parseInt($("#hDetectionProbability").val());
			var qualityImpact =  parseInt($("#hQualityImpact").val());
			var downtimeImpact = parseInt($("#hDowntimeImpact").val());
			var costImpact = parseInt($("#hCostImpact").val());
			var safetyImpact = parseInt($("#hSafetyImpact").val());
			 
			var rpn = "";
			if (occuranceRate >= 0 && detectionProbability > 0 && qualityImpact > 0 && downtimeImpact > 0 
				&& costImpact > 0 && safetyImpact > 0) {
				rpn = occuranceRate * detectionProbability *
					(qualityImpact + downtimeImpact + costImpact + safetyImpact);
			}

			return rpn;
		}

		function QuestionSelected(question) {

			$($("#hSelectedQuestion").val()).val($("input[name='level']:checked").val());
			SetRPN();

			$("input[name='level']").prop('checked', false);

			switch (question) {
				case 'OccuranceRate':
					$("#hSelectedQuestion").val("#hOccuranceRate");

					var value = $("#hOccuranceRate").val();
					if (value != "") {
						$("input[name='level'][value=" + value + "]").prop('checked', true);
					}

					$("#sLevel5").text("Frequent - Likely to occur at least monthly");
					$("#sLevel4").text("Resonably Probable - Likely to occur at least once per year");
					$("#sLevel3").text("Probable - Likely to occur in a 1-10 year period");
					$("#sLevel2").text("Occasional - Likely to occur in a 10-25 year period");
					$("#sLevel1").text("Remote - Likely to occur in ops lifetime");
					$("#dLevel0").show();
					break;
				case 'DetectionProbability':
					$("#hSelectedQuestion").val("#hDetectionProbability");

					var value = $("#hDetectionProbability").val();
					if (value != "") {
						$("input[name='level'][value=" + value + "]").prop('checked', true);
					}

					$("#sLevel5").text("Hard failure is the only way to detect (0% prob)");
					$("#sLevel4").text("Defect unlikely to be noticed before failure (<50% prob)");
					$("#sLevel3").text("Defect will possibly be noticed before failure (>50% prob)");
					$("#sLevel2").text("Defect will likely be noticed before failure (>80% prob)");
					$("#sLevel1").text("Defect will certainly be noticed or detection method in place before failure (100% prob)");
					$("#dLevel0").hide();
					break;
				case 'QualityImpact':
					$("#hSelectedQuestion").val("#hQualityImpact");

					var value = $("#hQualityImpact").val();
					if (value != "") {
						$("input[name='level'][value=" + value + "]").prop('checked', true);
					}

					$("#sLevel5").text("Catastrophic, one shift or more of scrap parts or high risk of reaching customer");
					$("#sLevel4").text("Severe, many scrap parts at a slight risk of reaching customer");
					$("#sLevel3").text("Moderate, a few scrap parts that are detectable");
					$("#sLevel2").text("Slight, at least one scrap part before problem is detected or parts must be reworked");
					$("#sLevel1").text("Negligible, no scrap parts");
					$("#dLevel0").hide();
					break;
				case 'DowntimeImpact':
					$("#hSelectedQuestion").val("#hDowntimeImpact");

					var value = $("#hDowntimeImpact").val();
					if (value != "") {
						$("input[name='level'][value=" + value + "]").prop('checked', true);
					}

					$("#sLevel5").text("Catastrophic, downtime >24 hours");
					$("#sLevel4").text("Severe, downtime between 12 and 24 hours");
					$("#sLevel3").text("Moderate, downtime from 2 to 12 hours");
					$("#sLevel2").text("Slight, downtime less than 2 hours");
					$("#sLevel1").text("Negligible, no reasonable risk of downtime");
					$("#dLevel0").hide();
					break;
				case 'CostImpact':
					$("#hSelectedQuestion").val("#hCostImpact");

					var value = $("#hCostImpact").val();
					if (value != "") {
						$("input[name='level'][value=" + value + "]").prop('checked', true);
					}

					$("#sLevel5").text("Catastrophic, cost >$10k");
					$("#sLevel4").text("Severe, cost between $5k and $10k");
					$("#sLevel3").text("Moderate, cost between $1k and $5k");
					$("#sLevel2").text("Slight, cost <$1k");
					$("#sLevel1").text("Negligible, minor cost");
					$("#dLevel0").hide();
					break;
				case 'SafetyImpact':
					$("#hSelectedQuestion").val("#hSafetyImpact");

					var value = $("#hSafetyImpact").val();
					if (value != "") {
						$("input[name='level'][value=" + value + "]").prop('checked', true);
					}

					$("#sLevel5").text("Catastrophic, possible loss of life");
					$("#sLevel4").text("Severe, possible severe injury");
					$("#sLevel3").text("Moderate, possible minor injury");
					$("#sLevel2").text("Slight, minor injury is a risk");
					$("#sLevel1").text("Negligible, no reasonable risk of injury");
					$("#dLevel0").hide();
					break;
			}

			$(".dashboardLinkSelected").attr("class", "dashboardLink");
			$("#" + question).attr("class", "dashboardLinkSelected");
		}
    </script>
	</body>
</html>
