

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient

Public Class ECMExport
    Inherits System.Web.UI.Page
	Protected WithEvents lang2075 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim ecm As New Utilities
    Dim cid, ecms, eqid, sql As String
    Protected WithEvents lblecmindex As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecmredirect As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            cid = HttpContext.Current.Session("comp").ToString
            ecms = Request.QueryString("ecm").ToString
            eqid = Request.QueryString("eqid").ToString
            sql = "select count(*) from pmECMExport where compid = '" & cid & "' and " _
            + "ecmindexid = '" & ecms & "'"
            Dim ecmcnt As Integer
            ecm.Open()
            ecmcnt = ecm.Scalar(sql)
            Dim ecmi, act, re As String
            If ecmcnt <> 0 Then
                sql = "select ecmindexid, activated, redirect from pmECMExport"
                dr = ecm.GetRdrData(sql)
                While dr.Read
                    ecmi = dr.Item("ecmindexid").ToString
                    lblecmindex.Value = ecmi
                    act = dr.Item("activated").ToString
                    re = dr.Item("redirect").ToString & "?eqid=" & eqid
                    lblecmredirect.Value = re
                    If act = "1" Then
                        Response.Redirect(re + "?ecmi=" + ecmi)
                    End If
                End While
            Else
                lblchk.Value = "noecm"
            End If
        End If
    End Sub

	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang2075.Text = axlabs.GetASPXPage("ECMExport.aspx","lang2075")
		Catch ex As Exception
		End Try

	End Sub

End Class
