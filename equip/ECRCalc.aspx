<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ECRCalc.aspx.vb" Inherits="lucy_r12.ECRCalc" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>ECRCalc</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/ecradm.js"></script>
		<script  src="../scripts1/ECRCalcaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="startrow();" >
		<form id="form1" method="post" runat="server">
			<table style="LEFT: 8px; POSITION: absolute; TOP: 0px" cellSpacing="0" cellPadding="2"
				width="800">
				<tr>
					<td class="thdrsinglft" align="left" style="width: 26px"><IMG src="../images/appbuttons/minibuttons/calchd.gif" border="0"></td>
					<td class="thdrsingrt label" width="784"><asp:Label id="lang2109" runat="server">PM Equipment Criticality Calculation</asp:Label></td>
				</tr>
				<tr>
					<td colSpan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td style="width: 100px"><asp:label id="Label7" runat="server" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
										Font-Bold="True">Equipment#</asp:label></td>
								<td style="width: 150px"><asp:label id="lbleqnum" runat="server" ForeColor="Black" Font-Size="X-Small" Font-Names="Arial"
										Font-Bold="True"></asp:label></td>
								<td style="width: 100px"><asp:label id="Label8" runat="server" ForeColor="Blue" Font-Size="X-Small" Font-Names="Arial"
										Font-Bold="True">Description:</asp:label></td>
								<td width="360"><asp:label id="lbleqdesc" runat="server" ForeColor="Black" Font-Size="X-Small" Font-Names="Arial"
										Font-Bold="True"></asp:label></td>
							</tr>
							<tr>
								<td colspan="4">
									<hr style="BORDER-RIGHT: #0000ff 2px solid; BORDER-TOP: #0000ff 2px solid; BORDER-LEFT: #0000ff 2px solid; BORDER-BOTTOM: #0000ff 2px solid">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td vAlign="top">
									<table width="380">
										<tr>
											<td class="hdr1" colSpan="2"><asp:Label id="lang2110" runat="server">Criteria Impacted by the Equipment</asp:Label></td>
										</tr>
										<tr>
											<td><IMG id="row1" onclick="controlrow();" height="20" alt="" src="../images/appbuttons/optdown.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2111" runat="server">Environmental, Health &amp; Safety</asp:Label></td>
										</tr>
										<tr id="r1">
											<td></td>
											<td class="hdr3"><A class="ecrlinksel" id="11A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('11A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11A')" href="javascript:void(0)"><asp:Label id="lang2112" runat="server">Health and Safety Impact</asp:Label></A></td>
										</tr>
										<tr id="r1a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="11B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('11B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11B')" href="javascript:void(0)"><asp:Label id="lang2113" runat="server">Environmental Impact</asp:Label></A></td>
										</tr>
										<tr id="r1b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="11C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('11C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('11C')" href="javascript:void(0)"><asp:Label id="lang2114" runat="server">Quality Impact on the Customer</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row2" onclick="controlrow2();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2115" runat="server">Business Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r2">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('12A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12A')" href="javascript:void(0)"><asp:Label id="lang2116" runat="server">Compliance Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r2a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('12B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12B')" href="javascript:void(0)"><asp:Label id="lang2117" runat="server">Facility/Utility Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r2b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="12C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('12C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('12C')" href="javascript:void(0)"><asp:Label id="lang2118" runat="server">Personnel Impact - Number of People Impacted</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row3" onclick="controlrow3();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2119" runat="server">Production Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r3">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('13A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13A')" href="javascript:void(0)"><asp:Label id="lang2120" runat="server">Product/Value Stream Impact</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('13B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13B')" href="javascript:void(0)"><asp:Label id="lang2121" runat="server">Constraint/Bottleneck � Demand vs. Capacity</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('13C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13C')" href="javascript:void(0)"><asp:Label id="lang2122" runat="server">Redundant Resource Availability</asp:Label></A></td>
										</tr>
										<tr class="details" id="r3c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="13D" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('13D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('13D')" href="javascript:void(0)"><asp:Label id="lang2123" runat="server">Controlled Environment Impact for Production</asp:Label></A></td>
										</tr>
										<tr>
											<td class="hdr1" colSpan="2"><asp:Label id="lang2124" runat="server">Criteria that Impacts the Equipment</asp:Label></td>
										</tr>
										<tr>
											<td><IMG id="row4" onclick="controlrow4();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2125" runat="server">Equipment Condition</asp:Label></td>
										</tr>
										<tr class="details" id="r4">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('21A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21A')" href="javascript:void(0)"><asp:Label id="lang2126" runat="server">Current condition</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('21B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21B')" href="javascript:void(0)"><asp:Label id="lang2127" runat="server">Age</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('21C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21C')" href="javascript:void(0)"><asp:Label id="lang2128" runat="server">Robustness</asp:Label></A></td>
										</tr>
										<tr class="details" id="r4c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="21D" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('21D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('21D')" href="javascript:void(0)"><asp:Label id="lang2129" runat="server">Equipment Quality</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row5" onclick="controlrow5();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2130" runat="server">Operational Context</asp:Label></td>
										</tr>
										<tr class="details" id="r5">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('22A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22A')" href="javascript:void(0)"><asp:Label id="lang2131" runat="server">Demand vs. capability</asp:Label></A></td>
										</tr>
										<tr class="details" id="r5a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('22B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22B')" href="javascript:void(0)"><asp:Label id="lang2132" runat="server">Operating Environment</asp:Label></A></td>
										</tr>
										<tr class="details" id="r5b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="22C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('22C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('22C')" href="javascript:void(0)"><asp:Label id="lang2133" runat="server">Equipment Availability for Maintenance Access vs. Planned Work Required</asp:Label></A>
											</td>
										</tr>
										<tr>
											<td><IMG id="row6" onclick="controlrow6();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2134" runat="server">Equipment Maintainability</asp:Label></td>
										</tr>
										<tr class="details" id="r6">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('23A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23A')" href="javascript:void(0)"><asp:Label id="lang2135" runat="server">Ease of Maintenance</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('23B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23B')" href="javascript:void(0)"><asp:Label id="lang2136" runat="server">Availability of Parts</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6b">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23C" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('23C');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23C')" href="javascript:void(0)"><asp:Label id="lang2137" runat="server">Availability of Vendor Resources</asp:Label></A></td>
										</tr>
										<tr class="details" id="r6c">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="23D" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('23D');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('23D')" href="javascript:void(0)"><asp:Label id="lang2138" runat="server">Availability of Maintenance Skills to Service</asp:Label></A></td>
										</tr>
										<tr>
											<td><IMG id="row7" onclick="controlrow7();" height="20" alt="" src="../images/appbuttons/optup.gif"
													style="width: 20px"></td>
											<td class="hdr2"><asp:Label id="lang2139" runat="server">Equipment Financial Impact</asp:Label></td>
										</tr>
										<tr class="details" id="r7">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="24A" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('24A');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('24A')" href="javascript:void(0)"><asp:Label id="lang2140" runat="server">Replacement Cost</asp:Label></A></td>
										</tr>
										<tr class="details" id="r7a">
											<td></td>
											<td class="hdr3"><A class="ecrlink" id="24B" onmouseover="this.className = 'ecrlinkover'" onclick="CalcTab('24B');"
													onmouseout="this.className = 'ecrlinkout';checkcurr('24B')" href="javascript:void(0)"><asp:Label id="lang2141" runat="server">Replaceability</asp:Label></A></td>
										</tr>
									</table>
								</td>
								<td vAlign="top">
									<table height="400" width="420">
										<TBODY>
											<tr>
												<td vAlign="top" align="left"><iframe id="ifwt" src="levels.aspx" frameBorder="no" width="400" scrolling="no" height="400"
														runat="server"></iframe>
												</td>
											</tr>
										</TBODY>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="lblcid" type="hidden" name="lblcid" runat="server"><input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblcatid" type="hidden" runat="server" NAME="lblcatid"> <input type="hidden" id="lblro" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
