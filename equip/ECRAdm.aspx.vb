

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class ECRAdm
    Inherits System.Web.UI.Page
	Protected WithEvents lang2108 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2107 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2106 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2105 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2104 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2103 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2102 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2101 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2100 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2099 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2098 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2097 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2096 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2095 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2094 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2093 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2092 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2091 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2090 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2089 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2088 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2087 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2086 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2085 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2084 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2083 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2082 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2081 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2080 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2079 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2078 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2077 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2076 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, ro As String
    Protected WithEvents lblcatid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents btncalcall As System.Web.UI.HtmlControls.HtmlInputButton
    Dim Login As String
    Dim scr As String
    Dim ecradm As New Utilities
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim sql As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifwt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sitst As String = Request.QueryString.ToString
        siutils.GetAscii(Me, sitst)

        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
        End Try
        If Not IsPostBack Then

            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            cid = HttpContext.Current.Session("comp").ToString
            lblcid.Value = cid
            ifwt.Attributes.Add("src", "weights.aspx?cid=" & cid & "&id=11A" & "&lbl=Health and Safety Impact&ro=" & ro)
            lblcatid.Value = "11A"
        Else
            If Request.Form("lblsubmit") = "docalc" Then
                lblsubmit.Value = ""
                CalcAll()

            End If
        End If
        Try
            Dim df, ps As String
            df = Request.QueryString("psid").ToString
            ps = Request.QueryString("psite").ToString
            Session("dfltps") = df
            Session("psite") = ps
            Response.Redirect("ECRAdm.aspx")
        Catch ex As Exception

        End Try
    End Sub
    Private Sub CalcAll()
        Dim cid As String = "0"
        sql = "declare @t decimal(10,2), @tc decimal(10,2), @eqid int, @cid int " _
        + "set @cid = 0 " _
        + "update ecrcatdata set weight = (select top 1 m.weight from ecrcatmaster m where m.ecrcatid = ecrcatdata.ecrcatid) " _
        + "declare eq_cursor cursor for " _
        + "select eqid from equipment where siteid = 12 " _
        + "open eq_cursor " _
        + "fetch next from eq_cursor into @eqid " _
        + "while @@fetch_status = 0 " _
        + "begin " _
        + "set @tc = (" _
        + "select sum(ecrlevel * (weight * weight * weight)) / sum(5 * (weight * weight * weight)) * 10 " _
        + "from ecrcatdata where eqid = @eqid " _
        + "and (ecrcatid = '11A' or ecrcatid = '11B')) " _
        + "set @t = (" _
        + "select sum(ecrlevel * square(weight)) / sum(5 * square(weight)) * 10 " _
        + "from ecrcatdata where eqid = @eqid " _
        + "and (ecrcatid <> '11A' and ecrcatid <> '11B')) " _
        + "set @t = (@t + @tc) / 2 " _
        + "update equipment set " _
        + "ecr = @t where eqid = @eqid " _
        + "fetch next from eq_cursor into @eqid " _
        + "end " _
        + "close eq_cursor " _
        + "deallocate eq_cursor"
        Try
            ecradm.Open()
            ecradm.Update(sql)
            ecradm.Dispose()
        Catch ex As Exception
            Dim strMessage2 As String = ex.Message
            strMessage2 = Replace(strMessage2, "'", Chr(180), , , vbTextCompare)
            strMessage2 = Replace(strMessage2, """", Chr(180), , , vbTextCompare)
            Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
        End Try
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2076.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2076")
        Catch ex As Exception
        End Try
        Try
            lang2077.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2077")
        Catch ex As Exception
        End Try
        Try
            lang2078.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2078")
        Catch ex As Exception
        End Try
        Try
            lang2079.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2079")
        Catch ex As Exception
        End Try
        Try
            lang2080.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2080")
        Catch ex As Exception
        End Try
        Try
            lang2081.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2081")
        Catch ex As Exception
        End Try
        Try
            lang2082.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2082")
        Catch ex As Exception
        End Try
        Try
            lang2083.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2083")
        Catch ex As Exception
        End Try
        Try
            lang2084.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2084")
        Catch ex As Exception
        End Try
        Try
            lang2085.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2085")
        Catch ex As Exception
        End Try
        Try
            lang2086.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2086")
        Catch ex As Exception
        End Try
        Try
            lang2087.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2087")
        Catch ex As Exception
        End Try
        Try
            lang2088.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2088")
        Catch ex As Exception
        End Try
        Try
            lang2089.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2089")
        Catch ex As Exception
        End Try
        Try
            lang2090.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2090")
        Catch ex As Exception
        End Try
        Try
            lang2091.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2091")
        Catch ex As Exception
        End Try
        Try
            lang2092.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2092")
        Catch ex As Exception
        End Try
        Try
            lang2093.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2093")
        Catch ex As Exception
        End Try
        Try
            lang2094.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2094")
        Catch ex As Exception
        End Try
        Try
            lang2095.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2095")
        Catch ex As Exception
        End Try
        Try
            lang2096.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2096")
        Catch ex As Exception
        End Try
        Try
            lang2097.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2097")
        Catch ex As Exception
        End Try
        Try
            lang2098.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2098")
        Catch ex As Exception
        End Try
        Try
            lang2099.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2099")
        Catch ex As Exception
        End Try
        Try
            lang2100.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2100")
        Catch ex As Exception
        End Try
        Try
            lang2101.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2101")
        Catch ex As Exception
        End Try
        Try
            lang2102.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2102")
        Catch ex As Exception
        End Try
        Try
            lang2103.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2103")
        Catch ex As Exception
        End Try
        Try
            lang2104.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2104")
        Catch ex As Exception
        End Try
        Try
            lang2105.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2105")
        Catch ex As Exception
        End Try
        Try
            lang2106.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2106")
        Catch ex As Exception
        End Try
        Try
            lang2107.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2107")
        Catch ex As Exception
        End Try
        Try
            lang2108.Text = axlabs.GetASPXPage("ECRAdm.aspx", "lang2108")
        Catch ex As Exception
        End Try

    End Sub

End Class
