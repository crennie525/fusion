﻿Imports System.Data.SqlClient
Imports System.Text
Public Class oclook
    Inherits System.Web.UI.Page
    Dim oc As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim sb As New StringBuilder
            Dim ocs, oci As String
            sb.Append("<table>")
            sql = "select * from ocareas order by ocarea"
            oc.Open()
            dr = oc.GetRdrData(sql)
            While dr.Read
                sb.Append("<tr><td class=""plainlabel"">")
                ocs = dr.Item("ocarea").ToString
                oci = dr.Item("oaid").ToString
                sb.Append("<a href=""#"" onclick=""getoc('" & oci & "','" & ocs & "')"">" & ocs & "</a>")
                sb.Append("</td></tr>")
            End While
            dr.Close()
            oc.Dispose()
            sb.Append("</table>")
            odiv.InnerHtml = sb.ToString
        End If
    End Sub

End Class