

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class eqhier
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim h As New Utilities
    Dim dr As SqlDataReader

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Dim sb As StringBuilder = New StringBuilder
        Dim dept As String = "24"
        Dim cell As String = "11"
        Dim eqnum As String = "eqcopytest"
        Dim eqid As String = "129"
        sb.Append("<table cellspacing=""0"" border=""0""><tr>")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""15"">")
        sb.Append("<td width=""155""></tr>" & vbCrLf)
       
        sql = "select distinct e.eqid, e.eqnum, f.func_id, f.func, c.comid, c.compnum, " _
        + "cnt = (select count(c.compnum) from components c where c.func_id = f.func_id) " _
        + "from equipment e join functions f on " _
        + "f.eqid = e.eqid join components c on c.func_id = f.func_id " _
        + "where(e.dept_id = '" & dept & "' and e.cellid = '" & cell & "')"
        '+ "where(e.eqid = '" & eqid & "')"

        h.Open()
        dr = h.GetRdrData(sql)
        Dim fid As String = "0"
        Dim cid As String = "0"
        Dim eid As String = "0"
        Dim cidhold As Integer = 0
        Dim cnt As Integer = 0
        While dr.Read
            If dr.Item("eqid") <> eid Then
                If eid <> 0 Then
                    sb.Append("</table></td></tr>")
                End If
                eid = dr.Item("eqid").ToString
                eqnum = dr.Item("eqnum").ToString
                sb.Append("<tr><td><img id='i" + eid + "' ")
                sb.Append("onclick=""fclose('t" + eid + "', 'i" + eid + "');""")
                sb.Append(" src=""../images/appbuttons/bgbuttons/minus.gif""></td>")
                sb.Append("<td colspan=""2"" ><a href=""#"" onclick=""gotoeq('" & eid & "')"" class=""blklink"">" & eqnum & "</a></td></tr>" & vbCrLf)
                sb.Append("<tr><td></td><td colspan=""2""><table cellspacing=""0"" id='t" + eid + "' border=""0"">")
                sb.Append("<tr><td width==""15""></td><td width==""155""></td></tr>")
            End If


            If dr.Item("func_id").ToString <> fid Then
                fid = dr.Item("func_id").ToString
                cidhold = dr.Item("cnt").ToString
                sb.Append("<tr>" & vbCrLf & "<td colspan=""2""><table cellspacing=""0"" border=""0"">" & vbCrLf)
                sb.Append("<tr><td><img id='i" + fid + "' onclick=""fclose('t" + fid + "', 'i" + fid + "');"" src=""../images/appbuttons/bgbuttons/plus.gif""></td>" & vbCrLf)
                sb.Append("<td class=""label""><a href=""#"" onclick=""gotofu('" & fid & "')"" class=""blink"">" & dr.Item("func").ToString & "</a></td></tr></table></td></tr>" & vbCrLf)
                If dr.Item("comid").ToString <> cid Then
                    cid = dr.Item("comid").ToString
                    If cnt = 0 Then
                        cnt = cnt + 1
                        sb.Append("<tr><td width=""15""></td>" & vbCrLf & "<td width=""155""><table border=""0"" class=""details"" cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                        sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                    End If
                End If
            ElseIf dr.Item("comid").ToString <> cid Then
                cid = dr.Item("comid").ToString
                If cnt = 0 Then
                    cnt = cnt + 1
                    sb.Append("<tr><td></td><td></td>" & vbCrLf & "<td><table cellspacing=""0"" id=""t" + fid + """>" & vbCrLf)
                    sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                Else
                    cnt = cnt + 1
                    sb.Append("<tr><td class=""plainlabel""><a href=""#"" onclick=""gotoco('" & cid & "')"" class=""labellink"">" & dr.Item("compnum").ToString & "</a></td></tr>" & vbCrLf)
                End If
                If cnt = cidhold Then
                    cnt = 0
                    sb.Append("</table></td></tr>")
                End If
            End If

        End While
        dr.Close()
        h.Dispose()
        sb.Append("</td></tr></table></td></tr></table>")
        Response.Write(sb.ToString)
    End Sub

End Class
