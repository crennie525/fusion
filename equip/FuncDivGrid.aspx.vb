

'********************************************************
'*
'********************************************************




Imports System.Data.SqlClient
Imports System.IO

Public Class FuncDivGrid
    Inherits System.Web.UI.Page
	Protected WithEvents ovid257 As System.Web.UI.HtmlControls.HtmlImage

	Protected WithEvents lang2385 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2384 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2383 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2382 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tmod As New transmod
    Dim cid, eq, sid, did, clid, ro, lock, lockby, Login, who, chld As String
    Dim sql As String
    Dim funcg As New Utilities
    Dim dr As SqlDataReader
    Dim ds As DataSet
    Protected WithEvents txtnewfunc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchld As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblpar As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbladdchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcurr As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rptrfunc As System.Web.UI.WebControls.Repeater
    Protected WithEvents lblpchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgcopy As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllock As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllockedby As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnremove2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblcbonoff As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnAddEq As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblrd As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSOVLIBS()



	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
        End Try
        If Not IsPostBack Then
            Try
                chld = Request.QueryString("chld").ToString
                lblchld.Value = chld
                who = Request.QueryString("who").ToString
                lblwho.Value = who
            Catch ex As Exception

            End Try
            Try
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If ro = "1" Then
                    btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                    btnAddEq.Enabled = False
                    ibtnremove2.Attributes.Add("class", "details")
                    imgcopy.Attributes.Add("class", "details")
                End If
                lblro.Value = ro
                cid = Request.QueryString("cid").ToString
                lblcid.Value = cid
                sid = Request.QueryString("sid").ToString
                lblsid.Value = sid
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                clid = Request.QueryString("clid").ToString
                lblcid.Value = cid
                eq = Request.QueryString("eqid").ToString
                lbleqid.Value = eq
                Dim user As String = HttpContext.Current.Session("username").ToString
                lbluser.Value = user
                funcg.Open()
                lock = CheckLock(eq)
                lockby = lbllockedby.Value
                If lock = "1" Then
                    If lockby = user Then
                        lbllock.Value = "0"
                    Else
                        btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                        btnAddEq.Enabled = False
                        ibtnremove2.Attributes.Add("class", "details")
                        imgcopy.Attributes.Add("class", "details")
                    End If
                ElseIf lock = "0" OrElse Len(lock) = 0 Then
                    btnAddEq.ImageUrl = "../images/appbuttons/bgbuttons/badd.gif"
                    btnAddEq.Enabled = True
                    ibtnremove2.Attributes.Add("class", "view")
                    imgcopy.Attributes.Add("class", "view")
                End If
                GetCurrent(eq)
                PopFunc(eq, cid)
                funcg.Dispose()
            Catch ex As Exception

            End Try
        Else
            If Request.Form("lblpchk") = "func" Then
                lblpchk.Value = ""
                eq = lbleqid.Value
                cid = lblcid.Value
                funcg.Open()
                PopFunc(eq, cid)
                funcg.Dispose()
            ElseIf Request.Form("lblpchk") = "del" Then
                lblpchk.Value = ""
                eq = lbleqid.Value
                cid = lblcid.Value
                funcg.Open()
                DelFunc()
                funcg.Dispose()
            End If
        End If
        'btnAddEq.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'btnAddEq.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
    End Sub
    Private Function CheckLock(ByVal eqid As String) As String
        chld = lblchld.Value
        Dim lock As String
        If chld = "" Or chld = "0" Then
            sql = "select locked, lockedby from equipment where eqid = '" & eqid & "'"
            Try
                dr = funcg.GetRdrData(sql)
                While dr.Read
                    lock = dr.Item("locked").ToString
                    lbllock.Value = dr.Item("locked").ToString
                    lbllockedby.Value = dr.Item("lockedby").ToString
                End While
                dr.Close()
            Catch ex As Exception

            End Try
        Else
            lock = "1"
            lbllock.Value = "1"
            lbllockedby.Value = "PMO Admin"
        End If
        
        Return lock
    End Function
    Private Sub GetCurrent(ByVal eq As String)
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eq & "'"
        Dim eqs, eqd As String
        dr = funcg.GetRdrData(sql)
        While dr.Read
            eqs = dr.Item("eqnum").ToString
            eqd = dr.Item("eqdesc").ToString
        End While
        dr.Close()
        If eqd <> "" Then
            tdcurr.InnerHtml = eqs & " - " & eqd
        Else
            tdcurr.InnerHtml = eqs
        End If

    End Sub
    Private Sub PopFunc(ByVal eq As String, ByVal cid As String)
        Dim cnt As Integer
        If cid = "na" Then
            cnt = 0
            eq = 0
        Else
            sql = "select count(*) from functions where eqid = '" & eq & "'" 'deptid = '" & dept & "'"
            cnt = funcg.Scalar(sql)
        End If

        sql = "select * from functions where eqid = '" & eq & "' order by routing"
        ds = funcg.GetDSData(sql)
        Dim dt As New DataTable
        dt = ds.Tables(0)
        rptrfunc.DataSource = dt
        Dim i, eint As Integer
        eint = 13
        For i = cnt To eint
            dt.Rows.InsertAt(dt.NewRow(), i)
        Next
        rptrfunc.DataBind()

    End Sub
    Private Sub rptrfunc_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrfunc.ItemCommand
        If e.CommandName = "Select" Then
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eq = lbleqid.Value
            Dim fuid As String
            Try
                fuid = CType(e.Item.FindControl("lblfuiditem"), Label).Text
            Catch ex As Exception
                fuid = CType(e.Item.FindControl("lblfuidalt"), Label).Text
            End Try
            lblfuid.Value = fuid
            who = lblwho.Value
            chld = lblchld.Value
            Response.Redirect("FuncDiv.aspx?who=" + who & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eq & "&fuid=" & fuid & "&chld=" & chld)
        End If
    End Sub
    Private Sub rptrfunc_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        If e.CommandName = "Select" Then
            cid = lblcid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            eq = lbleqid.Value
            Dim fuid As String
            Try
                fuid = CType(e.Item.FindControl("lblfuiditem"), Label).Text
            Catch ex As Exception
                fuid = CType(e.Item.FindControl("lblfuidalt"), Label).Text
            End Try
            lblfuid.Value = fuid
            Response.Redirect("FuncDiv.aspx?cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eq & "&fuid=" & fuid)
        End If
    End Sub

    Private Sub btnreturntoeq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("EQBot.aspx", True)
    End Sub

    Private Sub btnAddEq_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddEq.Click
        eq = lbleqid.Value
        cid = lblcid.Value
        Dim fu As String = txtnewfunc.Text
        fu = funcg.ModString1(fu)
        If Len(fu) > 0 Then
            Dim user As String = lbluser.Value
            Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
            Dim fudesc As String = "TBD"
            Dim fuspl As String = "TBD"
            Dim fucnt As Integer
            funcg.Open()
            sql = "select count(*) from functions where eqid = '" & eq & "' " _
            + "and func = '" & fu & "'"
            fucnt = funcg.Scalar(sql)
            If fucnt = 0 Then
                sql = "usp_addFunc '" & cid & "', '" & eq & "', '" & fu & "', '" & fudesc & "', '" & fuspl & "', '" & ustr & "'"
                Dim fuid As Integer = funcg.Scalar(sql)
                lblfuid.Value = fuid
                lbladdchk.Value = "yes"
                lblpar.Value = "func"
                txtnewfunc.Text = ""
                PopFunc(eq, cid)
            Else
                Dim strMessage As String =  tmod.getmsg("cdstr960" , "FuncDivGrid.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            funcg.Dispose()
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr961" , "FuncDivGrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If

    End Sub

    Private Sub rptrfunc_ItemDataBound1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrfunc.ItemDataBound
        Dim cb As CheckBox
        Dim ci As String
        ro = lblro.Value
        If e.Item.ItemType <> ListItemType.Header And _
                                e.Item.ItemType <> ListItemType.Footer Then
            Try
                cb = CType(e.Item.FindControl("cb1"), CheckBox)
                ci = CType(e.Item.FindControl("lblfuiditem"), Label).Text
            Catch ex As Exception
                cb = CType(e.Item.FindControl("cb2"), CheckBox)
                ci = CType(e.Item.FindControl("lblfuidalt"), Label).Text
            End Try

            If ci = "" Or ro = "1" Then
                cb.Enabled = False
            Else
                cb.Attributes.Add("onclick", "checkcb(this.checked);")
            End If

        End If
    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang2382 As Label
			lang2382 = CType(e.Item.FindControl("lang2382"), Label)
			lang2382.Text = axlabs.GetASPXPage("FuncDivGrid.aspx","lang2382")
		Catch ex As Exception
		End Try
		Try
                Dim lang2383 As Label
			lang2383 = CType(e.Item.FindControl("lang2383"), Label)
			lang2383.Text = axlabs.GetASPXPage("FuncDivGrid.aspx","lang2383")
		Catch ex As Exception
		End Try
		Try
                Dim lang2384 As Label
			lang2384 = CType(e.Item.FindControl("lang2384"), Label)
			lang2384.Text = axlabs.GetASPXPage("FuncDivGrid.aspx","lang2384")
		Catch ex As Exception
		End Try
		Try
                Dim lang2385 As Label
			lang2385 = CType(e.Item.FindControl("lang2385"), Label)
			lang2385.Text = axlabs.GetASPXPage("FuncDivGrid.aspx","lang2385")
		Catch ex As Exception
		End Try

End If

 End Sub
    Private Sub DelFunc()
        Dim cb As CheckBox
        Dim fi As String
        'funcg.Open()
        For Each i As RepeaterItem In rptrfunc.Items
            If i.ItemType = ListItemType.Item Or i.ItemType = ListItemType.AlternatingItem Then

                If i.ItemType = ListItemType.Item Then
                    cb = CType(i.FindControl("cb1"), CheckBox)
                    fi = CType(i.FindControl("lblfuiditem"), Label).Text
                ElseIf i.ItemType = ListItemType.AlternatingItem Then
                    cb = CType(i.FindControl("cb2"), CheckBox)
                    fi = CType(i.FindControl("lblfuidalt"), Label).Text
                End If

                If cb.Checked Then

                    sql = "usp_delFunc '" & fi & "'"
                    funcg.Update(sql)
                    lblfuid.Value = fi
                    Dim pic, picn, picm As String
                    sql = "select picurl, picurltn, picurltm from pmpictures where funcid = '" & fi & "'"
                    dr = funcg.GetRdrData(sql)
                    While dr.Read
                        pic = dr.Item("picurl").ToString
                        picn = dr.Item("picurltn").ToString
                        picm = dr.Item("picurltm").ToString
                        DelFuImg(pic, picn, picm)
                    End While
                    dr.Close()
                End If
            End If

        Next
        lblchk.Value = "rem"
        Dim eq As String = lbleqid.Value
        cid = lblcid.Value
        PopFunc(eq, cid)
        'funcg.Dispose()
    End Sub
    Private Sub ibtnremove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
       
    End Sub
    Private Sub DelFuImg(ByVal pic As String, ByVal picn As String, ByVal picm As String)
        'fuid = lblfuid.Value

        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/eqimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/eqimages/"

        Dim picarr() As String = pic.Split("/")
        Dim picnarr() As String = picn.Split("/")
        Dim picmarr() As String = picm.Split("/")
        Dim dpic, dpicn, dpicm As String
        dpic = picarr(picarr.Length - 1)
        dpicn = picnarr(picnarr.Length - 1)
        dpicm = picmarr(picmarr.Length - 1)
        Dim fpic, fpicn, fpicm As String
        fpic = strfrom + dpic
        fpicn = strfrom + dpicn
        fpicm = strfrom + dpicm
        'Try
        'If File.Exists(fpic) Then
        'File.Delete1(fpic)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicm) Then
        'File.Delete1(fpicm)
        'End If
        'Catch ex As Exception

        'End Try
        'Try
        'If File.Exists(fpicn) Then
        'File.Delete1(fpicn)
        'End If
        'Catch ex As Exception

        'End Try
    End Sub


	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2382.Text = axlabs.GetASPXPage("FuncDivGrid.aspx", "lang2382")
        Catch ex As Exception
        End Try
        Try
            lang2383.Text = axlabs.GetASPXPage("FuncDivGrid.aspx", "lang2383")
        Catch ex As Exception
        End Try
        Try
            lang2384.Text = axlabs.GetASPXPage("FuncDivGrid.aspx", "lang2384")
        Catch ex As Exception
        End Try
        Try
            lang2385.Text = axlabs.GetASPXPage("FuncDivGrid.aspx", "lang2385")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnAddEq.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                btnAddEq.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                btnAddEq.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                btnAddEq.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                btnAddEq.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub GetFSOVLIBS()
        Dim axovlib As New aspxovlib
        Try
            imgcopy.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("FuncDivGrid.aspx", "imgcopy") & "')")
            imgcopy.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try
        Try
            ovid257.Attributes.Add("onmouseover", "return overlib('" & axovlib.GetASPXOVLIB("FuncDivGrid.aspx", "ovid257") & "')")
            ovid257.Attributes.Add("onmouseout", "return nd()")
        Catch ex As Exception
        End Try

    End Sub

End Class
