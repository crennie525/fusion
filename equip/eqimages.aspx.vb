

'********************************************************
'*
'********************************************************



Public Class eqimages
    Inherits System.Web.UI.Page
    Protected WithEvents lang2283 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2282 As System.Web.UI.WebControls.Label

    Protected WithEvents lang2281 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqid, fuid, coid, typ As String
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbltab As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblin As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ifimg As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        GetFSLangs()

        Try
            lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
        Catch ex As Exception
            Dim dlang As New mmenu_utils_a
            lblfslang.Value = dlang.AppDfltLang
        End Try
        'Put user code to initialize the page here
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fuid = Request.QueryString("fuid").ToString
            lblfuid.Value = fuid
            coid = Request.QueryString("coid").ToString
            lblcoid.Value = coid
            typ = Request.QueryString("typ").ToString
            lblin.Value = typ
        End If
    End Sub











    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2281.Text = axlabs.GetASPXPage("eqimages.aspx", "lang2281")
        Catch ex As Exception
        End Try
        Try
            lang2282.Text = axlabs.GetASPXPage("eqimages.aspx", "lang2282")
        Catch ex As Exception
        End Try
        Try
            lang2283.Text = axlabs.GetASPXPage("eqimages.aspx", "lang2283")
        Catch ex As Exception
        End Try

    End Sub

End Class
