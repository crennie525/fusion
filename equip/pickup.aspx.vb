﻿Imports System.Data.SqlClient
Imports System.IO
Public Class pickup
    Inherits System.Web.UI.Page
    Dim sql As String
    Dim picup As New Utilities
    Dim Tables As String = "functions"
    Dim PK As String = "func_id"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "func_id"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim Sort As String = ""
    Dim ocnt, piccnt, picset, fucnt, fuset, fuid As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            picup.Open()
            sql = "select count(*) from pmimages"
            ocnt = picup.Scalar(sql)
            sql = "select count(*) from functions"
            fuset = picup.PageCount(sql, 100)
            picup.Dispose()
            For i = 1 To fuset
                PageNumber = i
                getfu(PageNumber)
            Next
            sql = "select count(*) from pmimages"
            Dim picup4 As New Utilities
            picup4.Open()
            piccnt = picup4.Scalar(sql)
            picup4.Dispose()
            Response.Write("Original Count = " & ocnt & " and Current Count = " & piccnt)
        End If

    End Sub
    Private Sub getfu(ByVal PageNumber As Integer)
        Dim ds As New DataSet
        Filter = "func_id in (select distinct funcid from pmimages)"
        sql = "Paging_Cursor '" & Tables & "', '" & PK & "', '" & Sort & "', '" & PageNumber & "', '" & PageSize & "', '" & Fields & "', '" & Filter & "', '" & Group & "'"
        Dim picup1 As New Utilities
        picup1.Open()
        ds = picup1.GetDSPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        picup1.Dispose()
        Dim i As Integer
        Dim x As Integer = ds.Tables(0).Rows.Count
        For i = 0 To (x - 1)
            fuid = ds.Tables(0).Rows(i)("func_id").ToString
            uppics(fuid)
        Next
    End Sub
    Private Sub uppics(ByVal fuid As String)
        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim strfrom As String = Server.MapPath("\") + appstr + "/pmimages/"
        Dim strto As String = Server.MapPath("\") + appstr + "/pmimages/"

        Dim ds As New DataSet
        Dim pic, picn, picm, picid As String
        sql = "select pic_id, pm_image, pm_image_med, pm_image_thumb from pmimages where funcid = '" & fuid & "'"
        Dim picup2 As New Utilities
        ds = picup2.GetDSData(sql)
        picup2.Dispose()
        Dim i As Integer
        Dim x As Integer = ds.Tables(0).Rows.Count
        For i = 0 To (x - 1)
            picid = ds.Tables(0).Rows(i)("pic_id").ToString
            pic = ds.Tables(0).Rows(i)("pm_image").ToString
            picn = ds.Tables(0).Rows(i)("pm_image_thumb").ToString
            picm = ds.Tables(0).Rows(i)("pm_image_med").ToString
            Dim picarr() As String = pic.Split("/")
            Dim picnarr() As String = picn.Split("/")
            Dim picmarr() As String = picm.Split("/")
            Dim dpic, dpicn, dpicm As String
            dpic = picarr(picarr.Length - 1)
            dpicn = picnarr(picnarr.Length - 1)
            dpicm = picmarr(picmarr.Length - 1)
            Dim fpic, fpicn, fpicm As String
            fpic = strfrom + dpic
            fpicn = strfrom + dpicn
            fpicm = strfrom + dpicm
            Dim delflag As Integer = 0
            Try
                If File.Exists(fpic) Then
                    delflag += 1
                End If
            Catch ex As Exception

            End Try
            Try
                If File.Exists(fpicm) Then
                    delflag += 1
                End If
            Catch ex As Exception

            End Try
            Try
                If File.Exists(fpicn) Then
                    delflag += 1
                End If
            Catch ex As Exception

            End Try
            If delflag = 0 Then
                delpics(picid)
            End If
        Next

        
    End Sub
    Private Sub delpics(ByVal picid As String)
        Dim picup3 As New Utilities
        picup3.Open()
        sql = "delete from pmimages where pic_id = '" & picid & "'"
        picup3.Update(sql)
        picup3.Dispose()

    End Sub
End Class