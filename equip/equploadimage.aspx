<%@ Page Language="vb" AutoEventWireup="false" Codebehind="equploadimage.aspx.vb" Inherits="lucy_r12.equploadimage" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>equploadimage</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/tpmimgtasknav.js"></script>
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<!--<script  src="../scripts1/equploadimageaspx.js"></script>-->
		<script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	<script  type="text/javascript">
	    function getbig(src) {
	        window.open("bigpic.aspx?src=" + src)
	    }
	    function GetsScroll() {
	        var strY = document.getElementById("spdivy").value;
	        if (strY.indexOf("!~") != 0) {
	            var intS = strY.indexOf("!~");
	            var intE = strY.indexOf("~!");
	            var strPos = strY.substring(intS + 2, intE);
	            document.getElementById("picdiv").scrollTop = strPos;
	        }
	        checkref();
	    }
	    function SetsDivPosition() {
	        var intY = document.getElementById("picdiv").scrollTop;
	        document.getElementById("spdivy").value = "yPos=!~" + intY + "~!";
	    }
	    function saveorder() {
	        var ids = document.getElementById("lblpicorder").value;
	        var idsarr = ids.split("~");
	        var newpo = "";
	        var eflag = 0;
	        for (var i = 0; i <= idsarr.length - 1; i++) {
	            var pstr = idsarr[i];
	            var pstrarr = pstr.split("-");
	            var pid = pstrarr[0];
	            var ord = document.getElementById(pid).value;
	            if (isNaN(ord) != false) {
	                eflag = 1;
	            }
	            if (newpo == "") {
	                newpo = pid + "-" + ord;
	            }
	            else {
	                newpo += "~" + pid + "-" + ord;
	            }
	        }
	        if (eflag == 1) {
	            alert("Order must be a numeric value")
	        }
	        else {
	            document.getElementById("lblpicorder").value = newpo;
	            document.getElementById("lblsubmit").value = "saveorder";
	            document.getElementById("form1").submit();
	        }
	    }
	    function delimg(id, order, img, bimg, nimg) {
	        document.getElementById("lblimgid").value = id;
	        document.getElementById("lbloldorder").value = order;

	        document.getElementById("lblimg").value = img;
	        document.getElementById("lblbimg").value = bimg;
	        document.getElementById("lblnimg").value = nimg;

	        document.getElementById("lblsubmit").value = "delimg";
	        document.getElementById("form1").submit();
	    }
	    function saveord(id, order) {
	        var neworder = document.getElementById(id).value;
	        var maxorder = document.getElementById("lblmaxorder").value;
	        if (order != neworder) {
	            if (parseInt(neworder) > parseInt(maxorder)) {
	                alert("Maximum Order Value is " + maxorder);
	            }
	            else if (neworder == "0") {
	                alert("Minimum Order Value is 1");
	            }
	            else {
	                document.getElementById("lblneworder").value = neworder;
	                document.getElementById("lbloldorder").value = order;
	                document.getElementById("lblsubmit").value = "savord";
	                document.getElementById("form1").submit();
	            }
	        }
	    }
	    function savdets() {
	        document.getElementById("lblsubmit").value = "savedets";
	        document.getElementById("form1").submit();
	    }
	    function checkref() {
	        var chk = document.getElementById("lblref").value;
	        if (chk == "yes") {
	            document.getElementById("lblref").value = "";
	            refreshit();
	        }
	    }
	    function refreshit() {
	        //document.getElementById("tdcurr").innerHTML="Enter New Image Details";
	        //document.getElementById("imgsavdet").className="details";
	        //var title = document.getElementById("txtpictitle");
	        //var task = document.getElementById("ddtasks");
	        var tasknum = document.getElementById("lbltasknum").value;
	        //var pcol = document.getElementById("ddpcol");
	        //var pstyle = document.getElementById("ddpstyle");
	        //var pdec = document.getElementById("ddpdec");
	        //var ihgt = document.getElementById("txtihgt");
	        //var iwdth = document.getElementById("txtiwdth");
	        //title.value = "";
	        //task.value = tasknum;
	        //pcol.value = "Select";
	        //pstyle.valu = "Select";
	        //pdec.value = "Select";
	        //ihgt.value = "200";
	        //iwdth.value = "200";
	    }
	    function getdets(order, pic) {
	        document.getElementById("lbloldorder").value = order;
	        document.getElementById("lblimgid").value = pic;
	        //document.getElementById("tdcurr").innerHTML="Edit Image #" + order + " Details";
	        //document.getElementById("imgsavdet").className="view";
	        //var title = document.getElementById("txtpictitle");
	        var ord = parseInt(order) - 1;
	        var titles = document.getElementById("lblititles").value;
	        var titlearr = titles.split(",");
	        var titlestr = titlearr[ord];
	        //title.value = titlestr;

	    }
	    //
    </script>

</HEAD>
	<body  onload="GetsScroll();">
		<form id="form1" method="post" runat="server">
			<table width="520">
				<TBODY>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang2297" runat="server">Image Upload Dialog</asp:Label></td>
					</tr>
					<tr>
						<td class="bluelabel" style="width: 150px"><asp:Label id="lang2298" runat="server">Choose Image to Upload</asp:Label></td>
						<td class="plainlabel" width="240"><INPUT id="MyFile" style="WIDTH: 230px" type="file" size="5" name="MyFile" RunAt="Server"></td>
						<td style="width: 130px"><input class="plainlabel" id="btnupload" type="button" value="Upload" name="btnupload"
								runat="server" ></td>
					</tr>
					<tr>
						<td class="thdrsing label" colSpan="3"><asp:Label id="lang2299" runat="server">Current Images</asp:Label></td>
					</tr>
					<tr>
						<td id="tblpics" align="center" colSpan="3" runat="server">
							<div id="picdiv" style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; WIDTH: 520px; HEIGHT: 260px; OVERFLOW: auto; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								onscroll="SetsDivPosition();" runat="server"></div>
						</td>
					</tr>
				</TBODY>
			</table>
			<input id="lblfuncid" type="hidden" runat="server" NAME="lblblockid"> <input type="hidden" id="lbltasknum" runat="server" NAME="lbltasknum">
			<input type="hidden" id="lblfunc" runat="server" NAME="lblfunc"><input id="lblimgid" type="hidden" runat="server" NAME="lblimgid">
			<input id="lblro" type="hidden" runat="server" NAME="lblro"><input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="lblpicorder" type="hidden" runat="server" NAME="lblpicorder"> <input id="lblsubmit" type="hidden" runat="server" NAME="lblsubmit">
			<input id="lblneworder" type="hidden" runat="server" NAME="lblneworder"> <input id="lbloldorder" type="hidden" runat="server" NAME="lbloldorder">
			<input id="lblmaxorder" type="hidden" runat="server" NAME="lblmaxorder"> <input id="lblititles" type="hidden" runat="server" NAME="lblititles">
			<input id="lblref" type="hidden" runat="server" NAME="lblref"><input type="hidden" id="lbloldtask" runat="server" NAME="lbloldtask">
			<input type="hidden" id="lblmaxtask" runat="server" NAME="lblmaxtask"><input type="hidden" id="lbledit" runat="server" NAME="lbledit">
			<input type="hidden" id="lbltyp" runat="server"> <input type="hidden" id="lbleqid" runat="server">
			<input type="hidden" id="lblcomid" runat="server"> <input type="hidden" id="lblimg" runat="server">
			<input type="hidden" id="lblbimg" runat="server"> <input type="hidden" id="lblnimg" runat="server">
			<input type="hidden" id="lblfslang" runat="server" />
		</form>
	</body>
</HTML>
