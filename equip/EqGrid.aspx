<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EqGrid.aspx.vb" Inherits="lucy_r12.EqGrid" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>Equipment Grid View - Filtered</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts/sessrefdialog.js"></script>
		<script  type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script  src="../scripts1/EqGridaspx.js"></script>
        
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body onload="scrolltop();checkit2();" onunload="stoptimer();" >
		<form id="form1" method="post" runat="server">
			<table id="scrollmenu" style="Z-INDEX: 103; POSITION: absolute; TOP: 8px; LEFT: 4px" width="900"
				bgColor="#0066ff">
				<tr>
					<td><asp:label id="Label54" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Filter</asp:label></td>
					<td><asp:dropdownlist id="lstfilter" runat="server" AutoPostBack="True">
							<asp:ListItem Value="0">Select</asp:ListItem>
							<asp:ListItem Value="1">OEM</asp:ListItem>
							<asp:ListItem Value="2">Model#/Type</asp:ListItem>
							<asp:ListItem Value="3">Equipment Status</asp:ListItem>
							<asp:ListItem Value="4">Criticality Rating</asp:ListItem>
						</asp:dropdownlist></td>
					<td><asp:label id="Label55" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Filter Definition</asp:label></td>
					<td><asp:dropdownlist id="lstdef" runat="server" AutoPostBack="True"></asp:dropdownlist></td>
					<td><asp:imagebutton id="btnexport" runat="server" Visible="False" ImageUrl="../images/appbuttons/bgbuttons/export.gif"></asp:imagebutton></td>
					<td><asp:imagebutton id="addtask" runat="server" ImageUrl="../images/appbuttons/bgbuttons/badd.gif" CssClass="details"></asp:imagebutton></td>
					<td><asp:imagebutton id="btnreturn" runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td><asp:label id="Label9" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True">Current Sort</asp:label></td>
					<td colSpan="2"><asp:label id="lblsort" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
							ForeColor="White">Current Sort</asp:label></td>
					<td align="center" colSpan="4"><asp:label id="ErrorLabel" runat="server" Font-Size="X-Small" Font-Names="Arial" Font-Bold="True"
							ForeColor="White"></asp:label></td>
				</tr>
				<tr>
					<td style="width: 100px"></td>
					<td style="width: 120px"></td>
					<td style="width: 120px"></td>
					<td width="250"></td>
					<td style="width: 90px"></td>
					<td style="width: 90px"></td>
					<td style="width: 90px"></td>
				</tr>
			</table>
			<table style="Z-INDEX: 102; POSITION: absolute; TOP: 64px; LEFT: 4px" width="2000">
				<TBODY>
					<tr>
						<td><asp:datalist id="dlhd" runat="server">
								<HeaderTemplate>
									<table width="900">
								</HeaderTemplate>
								<ItemTemplate>
									<TR height="20">
										<TD>
											<asp:label id="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Company</asp:label></TD>
										<TD>
											<asp:label id="lblcomp" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="X-Small" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.compname") %>'>
											</asp:label>
										</TD>
										<TD class="lbl">
											<asp:label id="Label13" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Plant Site</asp:label></TD>
										<TD colSpan="3">
											<asp:label id="lblplant" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="X-Small" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.sitename") %>'>
											</asp:label></TD>
									</TR>
									<TR height="20">
										<TD class="lbl">
											<asp:label id="Label14" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Department</asp:label></TD>
										<TD>
											<asp:label id="lbldept" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="X-Small" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.dept_line") %>'>
											</asp:label></TD>
										<TD class="lbl">
											<asp:label id="Label15" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="X-Small">Station/Cell</asp:label></TD>
										<TD colSpan="3">
											<asp:label id="lblcell" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="X-Small" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.cell_name") %>'>
											</asp:label></TD>
									</TR>
									<TR>
										<TD style="width: 100px"></TD>
										<TD width="200"></TD>
										<TD style="width: 100px"></TD>
										<TD width="200"></TD>
										<TD style="width: 100px"></TD>
										<TD width="200"></TD>
									</TR>
								</ItemTemplate>
							</asp:datalist></td>
					</tr>
					<tr>
						<td><asp:datagrid id="dgeq" runat="server" AutoGenerateColumns="False" GridLines="None" AllowSorting="True"
								CellSpacing="1">
								<AlternatingItemStyle CssClass="plainlabel" BackColor="#E7F1FD"></AlternatingItemStyle>
								<ItemStyle CssClass="plainlabel"></ItemStyle>
								<HeaderStyle CssClass="btmmenu plainlabel"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Edit">
										<HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:ImageButton id="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
												ToolTip="Edit Record" CommandName="Edit"></asp:ImageButton>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:imagebutton id="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
												ToolTip="Save Changes" CommandName="Update"></asp:imagebutton>
											<asp:imagebutton id="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
												ToolTip="Cancel Changes" CommandName="Cancel"></asp:imagebutton>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="eqnum desc" HeaderText="Equipment#">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=lblta runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txteqnum" runat="server" MaxLength="50" Width="110px" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="8px"></HeaderStyle>
										<HeaderTemplate>
											<asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
												ToolTip="Hide Description" CommandName="hd"></asp:ImageButton><BR>
											<asp:ImageButton id="ImageButton2" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
												ToolTip="Show Description" CommandName="sd"></asp:ImageButton>
										</HeaderTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqdesc") %>' ID="Label3" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox width="240" runat="server" id="txtdesc" MaxLength="100" Text='<%# DataBinder.Eval(Container, "DataItem.eqdesc") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="8px"></HeaderStyle>
										<HeaderTemplate>
											<asp:ImageButton id="ImageButton3" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
												ToolTip="Hide Location" CommandName="hl"></asp:ImageButton><BR>
											<asp:ImageButton id="ImageButton4" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
												ToolTip="Show Location" CommandName="sl"></asp:ImageButton>
										</HeaderTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Location">
										<HeaderStyle Width="250px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>' ID="Label2" >
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox width="240" runat="server" id="txtloco" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="8px"></HeaderStyle>
										<HeaderTemplate>
											<asp:ImageButton id="Imagebutton5" runat="server" CommandName="ho" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
												ToolTip="Hide OEM"></asp:ImageButton><BR>
											<asp:ImageButton id="Imagebutton6" runat="server" CommandName="so" ImageUrl="../images/appbuttons/minibuttons/show.gif"
												ToolTip="Show OEM"></asp:ImageButton>
										</HeaderTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="oem desc" HeaderText="OEM">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.oem") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txtoem" runat="server" Width="50px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.oem") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="8px"></HeaderStyle>
										<HeaderTemplate>
											<asp:ImageButton id="Imagebutton7" runat="server" CommandName="hm" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
												ToolTip="Hide Model"></asp:ImageButton><BR>
											<asp:ImageButton id="Imagebutton8" runat="server" CommandName="sm" ImageUrl="../images/appbuttons/minibuttons/show.gif"
												ToolTip="Show Model"></asp:ImageButton>
										</HeaderTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="model desc" HeaderText="Model">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.model") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txtmodel" runat="server" Width="110px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.model") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderTemplate>
											<asp:ImageButton id="Imagebutton9" runat="server" CommandName="hs" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
												ToolTip="Hide Serial#"></asp:ImageButton><BR>
											<asp:ImageButton id="Imagebutton10" runat="server" CommandName="ss" ImageUrl="../images/appbuttons/minibuttons/show.gif"
												ToolTip="Show Serial#"></asp:ImageButton>
										</HeaderTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="serial desc" HeaderText="Serial#">
										<HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.serial") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txtserial" runat="server" Width="110px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.serial") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="8px"></HeaderStyle>
										<HeaderTemplate>
											<asp:ImageButton id="Imagebutton13" runat="server" CommandName="hes" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
												ToolTip="Hide Equipment Status"></asp:ImageButton><BR>
											<asp:ImageButton id="Imagebutton14" runat="server" CommandName="ses" ImageUrl="../images/appbuttons/minibuttons/show.gif"
												ToolTip="Show Equipment Status"></asp:ImageButton>
										</HeaderTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="status desc" HeaderText="Equipment Status">
										<HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:label id=Label75 runat="server" Font-Size="10pt" Font-Names="Arial" ForeColor="Black" Text='<%# DataBinder.Eval(Container, "DataItem.status") %>'>
											</asp:label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:dropdownlist id="ddstat" runat="server" DataSource="<%# PopulateStatus %>" DataTextField="status" DataValueField="statid" SelectedIndex='<%# GetSelIndex(Container.DataItem("eqstatindex")) %>'>
											</asp:dropdownlist>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="8px"></HeaderStyle>
										<HeaderTemplate>
											<asp:ImageButton id="Imagebutton11" runat="server" ImageUrl="../images/appbuttons/minibuttons/hide.gif"
												ToolTip="Hide ECR" CommandName="hec"></asp:ImageButton><BR>
											<asp:ImageButton id="Imagebutton12" runat="server" ImageUrl="../images/appbuttons/minibuttons/show.gif"
												ToolTip="Show ECR" CommandName="sec"></asp:ImageButton>
										</HeaderTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="ecr desc" HeaderText="ECR">
										<HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecr") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txtecr" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.ecr") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<HeaderStyle Width="50px"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="lbleq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="txteqid" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<HeaderStyle Width="50px"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="lbleqlock" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.locked") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="Textbox1" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<HeaderStyle Width="50px"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id="lbllockby" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.lockedby") %>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:textbox id="Textbox2" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
											</asp:textbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td><asp:label id="lbleqid" runat="server" ForeColor="White"></asp:label><asp:textbox id="xcoord" runat="server" ForeColor="Black" Width="32px" BorderStyle="None" BorderColor="White"></asp:textbox><asp:textbox id="ycoord" runat="server" ForeColor="Black" Width="32px" BorderStyle="None" BorderColor="White"></asp:textbox><asp:textbox id="txteq" runat="server" ForeColor="White" Width="24px" BorderStyle="None" BorderColor="White"></asp:textbox></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</TBODY>
			</table>
			<iframe id="ifsession" class="details" src="" frameBorder="no" runat="server" style="Z-INDEX: 0">
			</iframe><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
			<input style="POSITION: absolute; TOP: 900px"> <input id="lblcid" type="hidden" runat="server"><input id="lblfilt" type="hidden" runat="server">
			<input id="lblsid" type="hidden" runat="server"><input id="lbldid" type="hidden" runat="server">
			<input id="lblclid" type="hidden" runat="server"><input id="lblchk" type="hidden" runat="server">
			<input id="lblfiltfilt" type="hidden" runat="server"><input id="lblcurrsort" type="hidden" runat="server">
			<input type="hidden" id="lbllid" runat="server">
		<input type="hidden" id="lblustr" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
<input type="hidden" id="lblwho" runat="server" />
<input type="hidden" id="lbldept" runat="server" />
<input type="hidden" id="lblcell" runat="server" />
<input type="hidden" id="lblloc" runat="server" />
<input type="hidden" id="lbleqidh" runat="server" />
<input type="hidden" id="lbleq" runat="server" />
</form>
	</body>
</HTML>
