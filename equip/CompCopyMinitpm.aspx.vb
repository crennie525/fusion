

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class CompCopyMinitpm
    Inherits System.Web.UI.Page
	Protected WithEvents lang2030 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2029 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2028 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2027 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2026 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2025 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2024 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2023 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2022 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2021 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2020 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2019 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2018 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2017 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2016 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2015 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2014 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2013 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2012 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2011 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2010 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2009 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2008 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2007 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2006 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2005 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2004 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2003 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2002 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2001 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2000 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1999 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1998 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1997 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1996 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1995 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1994 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1993 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1992 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1991 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1990 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1989 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1988 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1987 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1986 As System.Web.UI.WebControls.Label

	Protected WithEvents lang1985 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, eqid, fuid, sid, login, did, clid, usr, ro As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 100
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim sql As String
    Dim dr As SqlDataReader
    Dim comp As New Utilities
    Dim intPgNav As Integer
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tblnewcompdet As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdnewcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewecby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqecph As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqemm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdneweqecd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncfcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncfcph As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncfmm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdnewfuncfcd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tblnewcomphdr As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtnewspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents tdoldcomphdr As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents ibtnreturn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblnewcomid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdolddesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdolddesig As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtdesig As System.Web.UI.WebControls.TextBox
    Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Td2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtnewcomp As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtnewdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents cbtsks As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblusername As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdtskcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spdivy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim coid As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rptrcomprev As System.Web.UI.WebControls.Repeater
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents hpBackward As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents ibtncopy As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdoldcompdet As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdoldcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqnum As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqdesc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecph As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdecd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfcby As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfcph As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfcm As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfcd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents cbfm As System.Web.UI.WebControls.CheckBox
    Protected WithEvents tdoldspl As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            If lbllog.Value <> "no" Then
                Try
                    fuid = Request.QueryString("fuid").ToString
                    lblfuid.Value = fuid
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro
                    If ro = "1" Then
                        ibtncopy.Enabled = False
                        ibtncopy.ImageUrl = "../images/appbuttons/bgbuttons/copydis.gif"
                        'imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    Else
                        'ibtncopy.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/copyhov.gif'")
                        'ibtncopy.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/copy.gif'")
                    End If
                    If Len(fuid) <> 0 AndAlso fuid <> "" AndAlso fuid <> "0" Then
                        txtpg.Value = "1"
                        eqid = Request.QueryString("eqid").ToString
                        lbleqid.Value = eqid
                        cid = Request.QueryString("cid").ToString
                        lblcid.Value = cid
                        sid = Request.QueryString("sid").ToString
                        lblsid.Value = sid
                        did = Request.QueryString("did").ToString
                        lbldid.Value = did
                        clid = Request.QueryString("clid").ToString
                        lblclid.Value = clid
                        usr = Request.QueryString("usr").ToString
                        lblusername.Value = usr
                        tdoldcomphdr.Attributes.Add("class", "details")
                        tdoldcompdet.Attributes.Add("class", "details")
                        tblnewcomphdr.Attributes.Add("class", "details")
                        tblnewcompdet.Attributes.Add("class", "details")
                        comp.Open()
                        PopCompRev(PageNumber)
                        comp.Dispose()
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr820" , "CompCopyMinitpm.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "noeqid"
                    End If
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr821" , "CompCopyMinitpm.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End Try

            End If
        Else
            If Request.Form("lblret") = "next" Then
                comp.Open()
                GetNext()
                comp.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                comp.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                PopCompRev(PageNumber)
                comp.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                comp.Open()
                GetPrev()
                comp.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                comp.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                PopCompRev(PageNumber)
                comp.Dispose()
                lblret.Value = ""
            End If
        End If
        'btnnext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'btnnext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
        'btnprev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'btnprev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'ibtnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'ibtnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
        'hpBackward.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'hpBackward.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            PopCompRev(PageNumber)
        Catch ex As Exception
            comp.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr822" , "CompCopyMinitpm.aspx.vb")
 
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            PopCompRev(PageNumber)
        Catch ex As Exception
            comp.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr823" , "CompCopyMinitpm.aspx.vb")
 
            comp.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub PopCompRev(ByVal PageNumber As Integer)
        txtpg.Value = PageNumber
        eqid = lbleqid.Value
        cid = lblcid.Value
        sid = lblsid.Value
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        Dim intPgCnt As Integer
        If Len(srch) = 0 Then
            srch = ""
            sql = "select count(*) from components c join functions f on f.func_id = c.func_id " _
            + "join equipment e on e.eqid = f.eqid   where c.compid = '" & cid & "' and e.siteid = '" & sid & "'"
            intPgCnt = comp.Scalar(sql)
        Else
            Dim csrch As String
            csrch = "%" & srch & "%"
            sql = "select count(*) from components c join functions f on f.func_id = c.func_id " _
            + "join equipment e on e.eqid = f.eqid    where c.compid = '" & cid & "' and (c.compnum like '" & csrch & "' or c.spl like '" & csrch & "') and e.siteid = '" & sid & "'"
            intPgCnt = comp.Scalar(sql)
        End If
        PageNumber = txtpg.Value
        'intPgCnt = func.Scalar(sql)
        'intPgNav = func.PageCount(intPgCnt, PageSize)
        intPgNav = comp.PageCount(sql, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav

        'Get Page
        sql = "usp_getAllCompPgtpm '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '" & sid & "'"
        dr = comp.GetRdrData(sql)
        rptrcomprev.DataSource = dr
        rptrcomprev.DataBind()
        dr.Close()
        updatepos(intPgCnt)
    End Sub
    Private Sub updatepos(ByVal intPgCnt)
        PageNumber = txtpg.Value
        If intPgNav = 0 Then
            tdpg.InnerHtml = "Page 0 of 0"
        Else
            tdpg.InnerHtml = "Page " & PageNumber & " of " & intPgNav
        End If

        If PageNumber = 1 And intPgCnt = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = False

        ElseIf PageNumber = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = True

        ElseIf PageNumber = intPgNav Then
            btnnext.Enabled = False
            btnprev.Enabled = True

        Else
            btnnext.Enabled = True
            btnprev.Enabled = True
        End If
    End Sub
    Public Sub GetCo(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rb As RadioButton = New RadioButton
        Dim eqnum, eqdesc, spl, loc, model, serial, oem As String
        Dim ecr, cby, mby, cdat, mdat, cph, cm, eph, em As String
        Dim func, funcid, funcdesc, funcspl, comid, cdesc, cspl, cdesig, ctskcnt
        rb = sender
        tdoldcomp.InnerHtml = rb.ClientID
        For Each i As RepeaterItem In rptrcomprev.Items
            rb = CType(i.FindControl("rbfu"), RadioButton)
            comid = CType(i.FindControl("lblcomprevid"), Label).Text
            cdesc = CType(i.FindControl("lblcdesc"), Label).Text
            cspl = CType(i.FindControl("lblcspl"), Label).Text
            cdesig = CType(i.FindControl("lblcdesig"), Label).Text
            ctskcnt = CType(i.FindControl("lbltskcnt"), Label).Text

            eqnum = CType(i.FindControl("lbleqnum"), Label).Text
            eqdesc = CType(i.FindControl("lbleqdesc"), Label).Text
            spl = CType(i.FindControl("lbleqspl"), Label).Text
            func = CType(i.FindControl("lblfunc"), Label).Text
            funcspl = CType(i.FindControl("lblfspl"), Label).Text
            funcdesc = CType(i.FindControl("lblfdesc"), Label).Text
            funcspl = CType(i.FindControl("lblfspl"), Label).Text
            'eq
            Dim ecby, ecph, ecm, ecd As String
            ecby = CType(i.FindControl("lblecby"), Label).Text
            ecph = CType(i.FindControl("lblecph"), Label).Text
            ecm = CType(i.FindControl("lblecm"), Label).Text
            ecd = CType(i.FindControl("lblcdate"), Label).Text
            'fu
            Dim fcby, fcph, fcm, fcd As String
            fcby = CType(i.FindControl("lblfcby"), Label).Text
            fcph = CType(i.FindControl("lblfcph"), Label).Text
            fcm = CType(i.FindControl("lblfcm"), Label).Text
            fcd = CType(i.FindControl("lblfcd"), Label).Text
            rb.Checked = False
            If tdoldcomp.InnerHtml = rb.ClientID Then
                lblcoid.Value = comid
                tdoldcomphdr.Attributes.Add("class", "view")
                tdoldcompdet.Attributes.Add("class", "view")
                tblnewcomphdr.Attributes.Add("class", "details")
                tblnewcompdet.Attributes.Add("class", "details")
                rb.Checked = True
                tdoldcomp.InnerHtml = rb.Text.Trim
                tdolddesc.InnerHtml = cdesc
                tdoldspl.InnerHtml = cspl
                tdolddesig.InnerHtml = cdesig
                tdtskcnt.InnerHtml = ctskcnt
                tdeqnum.InnerHtml = eqnum
                tdeqdesc.InnerHtml = eqdesc
                tdeqspl.InnerHtml = spl
                tdecby.InnerHtml = ecby
                tdecph.InnerHtml = ecph
                tdecm.InnerHtml = ecm
                tdecd.InnerHtml = ecd
                tdfunc.InnerHtml = func
                'tdfuncdesc.InnerHtml = funcdesc
                tdfspl.InnerHtml = funcspl
                tdfcby.InnerHtml = fcby
                tdfcph.InnerHtml = fcph
                tdfcm.InnerHtml = fcm
                tdfcd.InnerHtml = fcd

            End If
        Next
    End Sub

    Private Sub ibtncopy_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtncopy.Click
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr824" , "CompCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            lbllog.Value = "no"
            Exit Sub
        End Try
        fuid = lblfuid.Value
        If Len(fuid) <> 0 AndAlso fuid <> "" AndAlso fuid <> "0" Then
            cid = lblcid.Value
            eqid = lbleqid.Value
            sid = lblsid.Value
            did = lbldid.Value
            clid = lblclid.Value
            usr = lblusername.Value
            usr = Replace(usr, "'", Chr(180), , , vbTextCompare)
            Dim nc As String = txtnewcomp.Text
            nc = Replace(nc, "'", Chr(180), , , vbTextCompare)
            nc = Replace(nc, "--", "-", , , vbTextCompare)
            nc = Replace(nc, ";", ":", , , vbTextCompare)
            If Len(nc) > 50 Then
                Dim strMessage As String =  tmod.getmsg("cdstr825" , "CompCopyMinitpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                Exit Sub
            End If
            If Len(nc) > 0 Then
                Dim nd As String = txtnewdesc.Text
                nd = Replace(nd, "'", Chr(180), , , vbTextCompare)
                nd = Replace(nd, "--", "-", , , vbTextCompare)
                nd = Replace(nd, ";", ":", , , vbTextCompare)
                Dim oldcomp As String = lblcoid.Value
                Dim compflag, taskflag As Integer
                Dim newcomp As Integer
                If cbfm.Checked = True Then
                    compflag = "1"
                Else
                    compflag = "0"
                End If
                If cbtsks.Checked = True Then
                    taskflag = "1"
                Else
                    taskflag = "0"
                End If
                Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
                Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                comp.Open()
                Dim cocnt As Integer
                sql = "select count(*) from components where func_id = '" & fuid & "' " _
                         + "and compid = '" & cid & "' and compnum = '" & nc & "' and compdesc = '" & nd & "'"
                cocnt = 0 'comp.Scalar(sql)
                If cocnt = 0 Then
                    sql = "usp_copyCompSing1 '" & cid & "', '" & oldcomp & "', '" & eqid & "', '" & fuid & "', " _
                    + "'" & compflag & "', '" & nc & "', '" & nd & "', '" & taskflag & "', '" & sid & "', " _
                    + "'" & did & "', '" & clid & "', '" & usr & "','Y'"
                    'sql = "usp_copyCompSing '" & cid & "', '" & oldcomp & "', '" & eqid & "', '" & fuid & "', '" & compflag & "', '" & nc & "', '" & nd & "'"
                    Try
                        newcomp = comp.Scalar(sql)
                        lblnewcomid.Value = newcomp
                        PopNewComp(newcomp)
                        tdoldcomphdr.Attributes.Add("class", "details")
                        tdoldcompdet.Attributes.Add("class", "details")
                        tbltop.Attributes.Add("class", "details")
                        tblnewcomphdr.Attributes.Add("class", "view")
                        tblnewcompdet.Attributes.Add("class", "view")
                        sql = "select pic_id, parfuncid, funcid, parcomid, comid, picurl, picurltn, picurltm from " _
                        + "pmpictures where eqid = '" & eqid & "' and funcid = '" & fuid & "' and comid = '" & newcomp & "' order by pic_id"
                        Try
                            Dim ds As New DataSet
                            ds = comp.GetDSData(sql)
                            Dim fid, cmid, ofid, ocmid As String
                            Dim i As Integer
                            Dim f As Integer = 0
                            Dim c As Integer = 0
                            Dim x As Integer = ds.Tables(0).Rows.Count
                            For i = 0 To (x - 1)
                                pid = ds.Tables(0).Rows(i)("pic_id").ToString
                                fid = ds.Tables(0).Rows(i)("funcid").ToString
                                ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                                cmid = ds.Tables(0).Rows(i)("comid").ToString
                                ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                                t = ds.Tables(0).Rows(i)("picurl").ToString
                                tn = ds.Tables(0).Rows(i)("picurltn").ToString
                                tm = ds.Tables(0).Rows(i)("picurltm").ToString
                                t = Replace(t, "..", "")
                                tn = Replace(tn, "..", "")
                                tm = Replace(tm, "..", "")
                                If cmid = "" Then
                                    f = f + 1
                                    nt = "b-eqImg" & fid & "i"
                                    ntn = "btn-eqImg" & fid & "i"
                                    ntm = "btm-eqImg" & fid & "i"

                                    ont = "b-eqImg" & ofid & "i"
                                    ontn = "btn-eqImg" & ofid & "i"
                                    ontm = "btm-eqImg" & ofid & "i"
                                Else
                                    c = c + 1
                                    nt = "c-eqImg" & cmid & "i"
                                    ntn = "ctn-eqImg" & cmid & "i"
                                    ntm = "ctm-eqImg" & cmid & "i"

                                    ont = "c-eqImg" & ocmid & "i"
                                    ontn = "ctn-eqImg" & ocmid & "i"
                                    ontm = "ctm-eqImg" & ocmid & "i"
                                End If

                                ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                                ts = Replace(ts, "\", "/")
                                td = Server.MapPath("\") & ap & t
                                td = Replace(td, "\", "/")
                                System.IO.File.Copy(ts, td, True)

                                tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                                tn = Replace(tn, "\", "/")
                                tnd = Server.MapPath("\") & ap & tn
                                tnd = Replace(tnd, "\", "/")
                                System.IO.File.Copy(tns, tnd, True)

                                tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                                tms = Replace(tms, "\", "/")
                                tmd = Server.MapPath("\") & ap & tm
                                tmd = Replace(tmd, "\", "/")
                                System.IO.File.Copy(tms, tmd, True)
                            Next
                        Catch ex As Exception

                        End Try
                    Catch ex As Exception
                        Dim strMessage As String =  tmod.getmsg("cdstr826" , "CompCopyMinitpm.aspx.vb")
 
                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End Try
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr827" , "CompCopyMinitpm.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End If
                comp.Dispose()
            End If
            Try
                login = HttpContext.Current.Session("Logged_IN").ToString()
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr828" , "CompCopyMinitpm.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "no"
                Exit Sub
            End Try
            'Dim strMessage1 As String =  tmod.getmsg("cdstr829" , "CompCopyMinitpm.aspx.vb")
 
            'Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
            'lbllog.Value = "noeqid"
        End If


    End Sub

    Private Sub PopNewComp(ByVal newcomp As String)
        cid = lblcid.Value
        sql = "usp_getCompDetails '" + cid + "', '" + newcomp + "'"
        dr = comp.GetRdrData(sql)
        While dr.Read
            tdnewcomp.InnerHtml = dr.Item("compnum").ToString
            txtnewspl.Text = dr.Item("spl").ToString
            txtdesc.Text = dr.Item("compdesc").ToString
            txtdesig.Text = dr.Item("desig").ToString
            tdneweq.InnerHtml = dr.Item("eqnum").ToString
            tdneweqdesc.InnerHtml = dr.Item("eqdesc").ToString
            tdneweqspl.InnerHtml = dr.Item("eqspl").ToString
            tdnewecby.InnerHtml = dr.Item("createdby").ToString
            tdneweqecph.InnerHtml = dr.Item("ecph").ToString
            tdneweqemm.InnerHtml = dr.Item("emm").ToString
            tdneweqecd.InnerHtml = dr.Item("createdate").ToString
            tdnewfunc.InnerHtml = dr.Item("func").ToString
            tdnewfuncdesc.InnerHtml = dr.Item("func_desc").ToString
            tdnewfuncspl.InnerHtml = dr.Item("fspl").ToString
            tdnewfuncfcby.InnerHtml = dr.Item("fcby").ToString
            tdnewfuncfcph.InnerHtml = dr.Item("fcph").ToString
            tdnewfuncfmm.InnerHtml = dr.Item("fcm").ToString
            tdnewfuncfcd.InnerHtml = dr.Item("fcd").ToString
        End While
        dr.Close()
    End Sub

    Private Sub ibtnreturn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnreturn.Click
        Dim spl As String = txtnewspl.Text
        spl = Replace(spl, "'", Chr(180), , , vbTextCompare)
        spl = Replace(spl, "--", "-", , , vbTextCompare)
        spl = Replace(spl, ";", ":", , , vbTextCompare)
        If Len(spl) = 0 Then spl = ""
        If Len(spl) > 200 Then
            Dim strMessage As String =  tmod.getmsg("cdstr830" , "CompCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim desc As String = txtdesc.Text
        desc = Replace(desc, "'", Chr(180), , , vbTextCompare)
        desc = Replace(desc, "--", "-", , , vbTextCompare)
        desc = Replace(desc, ";", ":", , , vbTextCompare)
        If Len(desc) = 0 Then desc = ""
        If Len(desc) > 100 Then
            Dim strMessage As String =  tmod.getmsg("cdstr831" , "CompCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim desig As String = txtdesig.Text
        desig = Replace(desig, "'", Chr(180), , , vbTextCompare)
        desig = Replace(desig, "--", "-", , , vbTextCompare)
        desig = Replace(desig, ";", ":", , , vbTextCompare)
        If Len(desig) = 0 Then desig = ""
        If Len(desig) > 200 Then
            Dim strMessage As String =  tmod.getmsg("cdstr832" , "CompCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        Dim coid As String = lblnewcomid.Value
        sql = "update Components set spl = '" & spl & "', " _
        + "compdesc = '" & desc & "', " _
        + "desig = '" & desig & "' where comid = '" & coid & "'"
        comp.Open()
        Try
            comp.Update(sql)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr833" , "CompCopyMinitpm.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        tbltop.Attributes.Add("class", "view")
        tdoldcomphdr.Attributes.Add("class", "details")
        tdoldcompdet.Attributes.Add("class", "details")
        tblnewcomphdr.Attributes.Add("class", "details")
        tblnewcompdet.Attributes.Add("class", "details")

        txtpg.Value = "1"
        PageNumber = txtpg.Value
        PopCompRev(PageNumber)
        comp.Dispose()
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        tbltop.Attributes.Add("class", "view")
        tdoldcomphdr.Attributes.Add("class", "details")
        tdoldcompdet.Attributes.Add("class", "details")
        tblnewcomphdr.Attributes.Add("class", "details")
        tblnewcompdet.Attributes.Add("class", "details")

        txtpg.Value = "1"
        comp.Open()
        PageNumber = txtpg.Value
        PopCompRev(PageNumber)
        comp.Dispose()
    End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        comp.Open()
        PopCompRev(PageNumber)
        comp.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprev.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        comp.Open()
        PopCompRev(PageNumber)
        comp.Dispose()
    End Sub

    Private Sub rptrcomprev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrcomprev.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And _
                                e.Item.ItemType <> ListItemType.Footer Then
            Dim pic As HtmlImage = CType(e.Item.FindControl("imgpic"), HtmlImage)
            Dim id As String = DataBinder.Eval(e.Item.DataItem, "eqid").ToString
            Dim fid As String = DataBinder.Eval(e.Item.DataItem, "func_id").ToString
            Dim cid As String = DataBinder.Eval(e.Item.DataItem, "comid").ToString
            Dim cnt As String = DataBinder.Eval(e.Item.DataItem, "cpcnt").ToString
            If cnt <> "0" Then
                pic.Attributes.Add("onclick", "getfuport('" & id & "','" & fid & "','" & cid & "');")
                pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpic.gif")
            Else
                pic.Attributes.Add("onclick", "")
                pic.Attributes.Add("src", "../images/appbuttons/minibuttons/gridpicdis.gif")
            End If
        End If

    
If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
		Try
                Dim lang1985 As Label
			lang1985 = CType(e.Item.FindControl("lang1985"), Label)
			lang1985.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1985")
		Catch ex As Exception
		End Try
		Try
                Dim lang1986 As Label
			lang1986 = CType(e.Item.FindControl("lang1986"), Label)
			lang1986.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1986")
		Catch ex As Exception
		End Try
		Try
                Dim lang1987 As Label
			lang1987 = CType(e.Item.FindControl("lang1987"), Label)
			lang1987.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1987")
		Catch ex As Exception
		End Try
		Try
                Dim lang1988 As Label
			lang1988 = CType(e.Item.FindControl("lang1988"), Label)
			lang1988.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1988")
		Catch ex As Exception
		End Try
		Try
                Dim lang1989 As Label
			lang1989 = CType(e.Item.FindControl("lang1989"), Label)
			lang1989.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1989")
		Catch ex As Exception
		End Try
		Try
                Dim lang1990 As Label
			lang1990 = CType(e.Item.FindControl("lang1990"), Label)
			lang1990.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1990")
		Catch ex As Exception
		End Try
		Try
                Dim lang1991 As Label
			lang1991 = CType(e.Item.FindControl("lang1991"), Label)
			lang1991.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1991")
		Catch ex As Exception
		End Try
		Try
                Dim lang1992 As Label
			lang1992 = CType(e.Item.FindControl("lang1992"), Label)
			lang1992.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1992")
		Catch ex As Exception
		End Try
		Try
                Dim lang1993 As Label
			lang1993 = CType(e.Item.FindControl("lang1993"), Label)
			lang1993.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1993")
		Catch ex As Exception
		End Try
		Try
                Dim lang1994 As Label
			lang1994 = CType(e.Item.FindControl("lang1994"), Label)
			lang1994.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1994")
		Catch ex As Exception
		End Try
		Try
                Dim lang1995 As Label
			lang1995 = CType(e.Item.FindControl("lang1995"), Label)
			lang1995.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1995")
		Catch ex As Exception
		End Try
		Try
                Dim lang1996 As Label
			lang1996 = CType(e.Item.FindControl("lang1996"), Label)
			lang1996.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1996")
		Catch ex As Exception
		End Try
		Try
                Dim lang1997 As Label
			lang1997 = CType(e.Item.FindControl("lang1997"), Label)
			lang1997.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1997")
		Catch ex As Exception
		End Try
		Try
                Dim lang1998 As Label
			lang1998 = CType(e.Item.FindControl("lang1998"), Label)
			lang1998.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1998")
		Catch ex As Exception
		End Try
		Try
                Dim lang1999 As Label
			lang1999 = CType(e.Item.FindControl("lang1999"), Label)
			lang1999.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1999")
		Catch ex As Exception
		End Try
		Try
                Dim lang2000 As Label
			lang2000 = CType(e.Item.FindControl("lang2000"), Label)
			lang2000.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2000")
		Catch ex As Exception
		End Try
		Try
                Dim lang2001 As Label
			lang2001 = CType(e.Item.FindControl("lang2001"), Label)
			lang2001.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2001")
		Catch ex As Exception
		End Try
		Try
                Dim lang2002 As Label
			lang2002 = CType(e.Item.FindControl("lang2002"), Label)
			lang2002.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2002")
		Catch ex As Exception
		End Try
		Try
                Dim lang2003 As Label
			lang2003 = CType(e.Item.FindControl("lang2003"), Label)
			lang2003.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2003")
		Catch ex As Exception
		End Try
		Try
                Dim lang2004 As Label
			lang2004 = CType(e.Item.FindControl("lang2004"), Label)
			lang2004.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2004")
		Catch ex As Exception
		End Try
		Try
                Dim lang2005 As Label
			lang2005 = CType(e.Item.FindControl("lang2005"), Label)
			lang2005.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2005")
		Catch ex As Exception
		End Try
		Try
                Dim lang2006 As Label
			lang2006 = CType(e.Item.FindControl("lang2006"), Label)
			lang2006.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2006")
		Catch ex As Exception
		End Try
		Try
                Dim lang2007 As Label
			lang2007 = CType(e.Item.FindControl("lang2007"), Label)
			lang2007.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2007")
		Catch ex As Exception
		End Try
		Try
                Dim lang2008 As Label
			lang2008 = CType(e.Item.FindControl("lang2008"), Label)
			lang2008.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2008")
		Catch ex As Exception
		End Try
		Try
                Dim lang2009 As Label
			lang2009 = CType(e.Item.FindControl("lang2009"), Label)
			lang2009.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2009")
		Catch ex As Exception
		End Try
		Try
                Dim lang2010 As Label
			lang2010 = CType(e.Item.FindControl("lang2010"), Label)
			lang2010.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2010")
		Catch ex As Exception
		End Try
		Try
                Dim lang2011 As Label
			lang2011 = CType(e.Item.FindControl("lang2011"), Label)
			lang2011.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2011")
		Catch ex As Exception
		End Try
		Try
                Dim lang2012 As Label
			lang2012 = CType(e.Item.FindControl("lang2012"), Label)
			lang2012.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2012")
		Catch ex As Exception
		End Try
		Try
                Dim lang2013 As Label
			lang2013 = CType(e.Item.FindControl("lang2013"), Label)
			lang2013.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2013")
		Catch ex As Exception
		End Try
		Try
                Dim lang2014 As Label
			lang2014 = CType(e.Item.FindControl("lang2014"), Label)
			lang2014.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2014")
		Catch ex As Exception
		End Try
		Try
                Dim lang2015 As Label
			lang2015 = CType(e.Item.FindControl("lang2015"), Label)
			lang2015.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2015")
		Catch ex As Exception
		End Try
		Try
                Dim lang2016 As Label
			lang2016 = CType(e.Item.FindControl("lang2016"), Label)
			lang2016.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2016")
		Catch ex As Exception
		End Try
		Try
                Dim lang2017 As Label
			lang2017 = CType(e.Item.FindControl("lang2017"), Label)
			lang2017.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2017")
		Catch ex As Exception
		End Try
		Try
                Dim lang2018 As Label
			lang2018 = CType(e.Item.FindControl("lang2018"), Label)
			lang2018.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2018")
		Catch ex As Exception
		End Try
		Try
                Dim lang2019 As Label
			lang2019 = CType(e.Item.FindControl("lang2019"), Label)
			lang2019.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2019")
		Catch ex As Exception
		End Try
		Try
                Dim lang2020 As Label
			lang2020 = CType(e.Item.FindControl("lang2020"), Label)
			lang2020.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2020")
		Catch ex As Exception
		End Try
		Try
                Dim lang2021 As Label
			lang2021 = CType(e.Item.FindControl("lang2021"), Label)
			lang2021.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2021")
		Catch ex As Exception
		End Try
		Try
                Dim lang2022 As Label
			lang2022 = CType(e.Item.FindControl("lang2022"), Label)
			lang2022.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2022")
		Catch ex As Exception
		End Try
		Try
                Dim lang2023 As Label
			lang2023 = CType(e.Item.FindControl("lang2023"), Label)
			lang2023.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2023")
		Catch ex As Exception
		End Try
		Try
                Dim lang2024 As Label
			lang2024 = CType(e.Item.FindControl("lang2024"), Label)
			lang2024.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2024")
		Catch ex As Exception
		End Try
		Try
                Dim lang2025 As Label
			lang2025 = CType(e.Item.FindControl("lang2025"), Label)
			lang2025.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2025")
		Catch ex As Exception
		End Try
		Try
                Dim lang2026 As Label
			lang2026 = CType(e.Item.FindControl("lang2026"), Label)
			lang2026.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2026")
		Catch ex As Exception
		End Try
		Try
                Dim lang2027 As Label
			lang2027 = CType(e.Item.FindControl("lang2027"), Label)
			lang2027.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2027")
		Catch ex As Exception
		End Try
		Try
                Dim lang2028 As Label
			lang2028 = CType(e.Item.FindControl("lang2028"), Label)
			lang2028.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2028")
		Catch ex As Exception
		End Try
		Try
                Dim lang2029 As Label
			lang2029 = CType(e.Item.FindControl("lang2029"), Label)
			lang2029.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2029")
		Catch ex As Exception
		End Try
		Try
                Dim lang2030 As Label
			lang2030 = CType(e.Item.FindControl("lang2030"), Label)
			lang2030.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2030")
		Catch ex As Exception
		End Try

End If

 End Sub
	

	

	

	

	

	Private Sub GetFSLangs()
		Dim axlabs as New aspxlabs
		Try
			lang1985.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1985")
		Catch ex As Exception
		End Try
		Try
			lang1986.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1986")
		Catch ex As Exception
		End Try
		Try
			lang1987.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1987")
		Catch ex As Exception
		End Try
		Try
			lang1988.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1988")
		Catch ex As Exception
		End Try
		Try
			lang1989.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1989")
		Catch ex As Exception
		End Try
		Try
			lang1990.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1990")
		Catch ex As Exception
		End Try
		Try
			lang1991.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1991")
		Catch ex As Exception
		End Try
		Try
			lang1992.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1992")
		Catch ex As Exception
		End Try
		Try
			lang1993.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1993")
		Catch ex As Exception
		End Try
		Try
			lang1994.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1994")
		Catch ex As Exception
		End Try
		Try
			lang1995.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1995")
		Catch ex As Exception
		End Try
		Try
			lang1996.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1996")
		Catch ex As Exception
		End Try
		Try
			lang1997.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1997")
		Catch ex As Exception
		End Try
		Try
			lang1998.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1998")
		Catch ex As Exception
		End Try
		Try
			lang1999.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang1999")
		Catch ex As Exception
		End Try
		Try
			lang2000.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2000")
		Catch ex As Exception
		End Try
		Try
			lang2001.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2001")
		Catch ex As Exception
		End Try
		Try
			lang2002.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2002")
		Catch ex As Exception
		End Try
		Try
			lang2003.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2003")
		Catch ex As Exception
		End Try
		Try
			lang2004.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2004")
		Catch ex As Exception
		End Try
		Try
			lang2005.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2005")
		Catch ex As Exception
		End Try
		Try
			lang2006.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2006")
		Catch ex As Exception
		End Try
		Try
			lang2007.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2007")
		Catch ex As Exception
		End Try
		Try
			lang2008.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2008")
		Catch ex As Exception
		End Try
		Try
			lang2009.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2009")
		Catch ex As Exception
		End Try
		Try
			lang2010.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2010")
		Catch ex As Exception
		End Try
		Try
			lang2011.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2011")
		Catch ex As Exception
		End Try
		Try
			lang2012.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2012")
		Catch ex As Exception
		End Try
		Try
			lang2013.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2013")
		Catch ex As Exception
		End Try
		Try
			lang2014.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2014")
		Catch ex As Exception
		End Try
		Try
			lang2015.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2015")
		Catch ex As Exception
		End Try
		Try
			lang2016.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2016")
		Catch ex As Exception
		End Try
		Try
			lang2017.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2017")
		Catch ex As Exception
		End Try
		Try
			lang2018.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2018")
		Catch ex As Exception
		End Try
		Try
			lang2019.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2019")
		Catch ex As Exception
		End Try
		Try
			lang2020.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2020")
		Catch ex As Exception
		End Try
		Try
			lang2021.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2021")
		Catch ex As Exception
		End Try
		Try
			lang2022.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2022")
		Catch ex As Exception
		End Try
		Try
			lang2023.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2023")
		Catch ex As Exception
		End Try
		Try
			lang2024.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2024")
		Catch ex As Exception
		End Try
		Try
			lang2025.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2025")
		Catch ex As Exception
		End Try
		Try
			lang2026.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2026")
		Catch ex As Exception
		End Try
		Try
			lang2027.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2027")
		Catch ex As Exception
		End Try
		Try
			lang2028.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2028")
		Catch ex As Exception
		End Try
		Try
			lang2029.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2029")
		Catch ex As Exception
		End Try
		Try
			lang2030.Text = axlabs.GetASPXPage("CompCopyMinitpm.aspx","lang2030")
		Catch ex As Exception
		End Try

	End Sub

	

	

	Private Sub GetBGBLangs()
		Dim lang as String = lblfslang.value
		Try
			If lang = "eng" Then
			hpBackward.Attributes.Add("src" , "../images2/eng/bgbuttons/return.gif")
			ElseIf lang = "fre" Then
			hpBackward.Attributes.Add("src" , "../images2/fre/bgbuttons/return.gif")
			ElseIf lang = "ger" Then
			hpBackward.Attributes.Add("src" , "../images2/ger/bgbuttons/return.gif")
			ElseIf lang = "ita" Then
			hpBackward.Attributes.Add("src" , "../images2/ita/bgbuttons/return.gif")
			ElseIf lang = "spa" Then
			hpBackward.Attributes.Add("src" , "../images2/spa/bgbuttons/return.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			ibtncopy.Attributes.Add("src" , "../images2/eng/bgbuttons/copy.gif")
			ElseIf lang = "fre" Then
			ibtncopy.Attributes.Add("src" , "../images2/fre/bgbuttons/copy.gif")
			ElseIf lang = "ger" Then
			ibtncopy.Attributes.Add("src" , "../images2/ger/bgbuttons/copy.gif")
			ElseIf lang = "ita" Then
			ibtncopy.Attributes.Add("src" , "../images2/ita/bgbuttons/copy.gif")
			ElseIf lang = "spa" Then
			ibtncopy.Attributes.Add("src" , "../images2/spa/bgbuttons/copy.gif")
			End If
		Catch ex As Exception
		End Try
		Try
			If lang = "eng" Then
			ibtnreturn.Attributes.Add("src" , "../images2/eng/bgbuttons/save.gif")
			ElseIf lang = "fre" Then
			ibtnreturn.Attributes.Add("src" , "../images2/fre/bgbuttons/save.gif")
			ElseIf lang = "ger" Then
			ibtnreturn.Attributes.Add("src" , "../images2/ger/bgbuttons/save.gif")
			ElseIf lang = "ita" Then
			ibtnreturn.Attributes.Add("src" , "../images2/ita/bgbuttons/save.gif")
			ElseIf lang = "spa" Then
			ibtnreturn.Attributes.Add("src" , "../images2/spa/bgbuttons/save.gif")
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class
