<%@ Page Language="vb" AutoEventWireup="false" Codebehind="funclist.aspx.vb" Inherits="lucy_r12.funclist" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>funclist</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/funclistaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<div id="divfu" style="BORDER-BOTTOM: black 1px solid; POSITION: absolute; BORDER-LEFT: black 1px solid; WIDTH: 350px; HEIGHT: 350px; OVERFLOW: auto; BORDER-TOP: black 1px solid; TOP: 6px; BORDER-RIGHT: black 1px solid; LEFT: 6px"
				runat="server"></div>
			<input type="hidden" id="lbleqid" runat="server" NAME="lbleqid"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblsrchtyp" runat="server" NAME="lblsrchtyp">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
