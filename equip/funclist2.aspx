<%@ Page Language="vb" AutoEventWireup="false" Codebehind="funclist2.aspx.vb" Inherits="lucy_r12.funclist2" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>funclist2</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/smartscroll.js"></script>
		<script  src="../scripts1/funclist2aspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkit();">
		<form id="form1" method="post" runat="server">
			<table cellpadding="1" cellspacing="1" style="POSITION: absolute;  TOP: 0px;  LEFT: 0px"
				id="scrollmenu">
				<tr>
					<td><div id="divfu" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 200px; HEIGHT: 118px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel"><asp:Label id="lang2392" runat="server">Function Components</asp:Label></td>
				</tr>
				<tr>
					<td><div id="divco" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 200px; HEIGHT: 118px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
							runat="server"></div>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lbleqid" runat="server" NAME="lbleqid"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
			<input type="hidden" id="lblsrchtyp" runat="server" NAME="lblsrchtyp"> <input type="hidden" id="lblfuid" runat="server" NAME="lblfuid">
			<input type="hidden" id="lblsubmit" runat="server" NAME="lblsubmit"> <input type="hidden" id="xCoord" runat="server" NAME="Hidden1">
			<input type="hidden" id="yCoord" runat="server" NAME="Hidden2">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
