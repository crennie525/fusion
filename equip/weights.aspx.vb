

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class weights
    Inherits System.Web.UI.Page
	Protected WithEvents lang2553 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, ecid, lbl, sql, ro As String
    Dim dr As SqlDataReader
    Protected WithEvents lblwt2 As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim weights As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblcat As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents lblwt As System.Web.UI.WebControls.Label
    Protected WithEvents lblcid As System.Web.UI.WebControls.Label
    Protected WithEvents lblecrid As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents t5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents t4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents t3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents t2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents t1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnsave As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rb1 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb2 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb3 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb4 As System.Web.UI.HtmlControls.HtmlInputRadioButton
    Protected WithEvents rb5 As System.Web.UI.HtmlControls.HtmlInputRadioButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()

        If Not IsPostBack Then
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                btnsave.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                btnsave.Enabled = False
            End If
            cid = Request.QueryString("cid").ToString
            ecid = Request.QueryString("id").ToString
            lbl = Request.QueryString("lbl").ToString
            lblcat.Text = lbl
            weights.Open()
            GetWT(cid, ecid)

        End If
        'btnsave.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
        'btnsave.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")

    End Sub
    Private Sub GetWT(ByVal cid As String, ByVal ecid As String)
        Try
            weights.Open()
        Catch ex As Exception

        End Try
        sql = "select * from ecrcatmaster where " _
            + "ecrcatid = '" & ecid & "'"

        dr = weights.GetRdrData(sql)
        If dr.Read Then
            lblecrid.Text = dr.Item("ecrcatid").ToString
            lblwt2.Value = dr.Item("weight").ToString
            t5.Text = dr.Item("level5").ToString
            t4.Text = dr.Item("level4").ToString
            t3.Text = dr.Item("level3").ToString
            t2.Text = dr.Item("level2").ToString
            t1.Text = dr.Item("level1").ToString
        End If
        dr.Close()

        Dim wt As String = lblwt2.Value
       
        If wt = "1" Then
            rb1.Checked = True
            rb2.Checked = False
            rb3.Checked = False
            rb4.Checked = False
            rb5.Checked = False
        ElseIf wt = "2" Then
            rb2.Checked = True
            rb1.Checked = False
            rb3.Checked = False
            rb4.Checked = False
            rb5.Checked = False
        ElseIf wt = "3" Then
            rb3.Checked = True
            rb2.Checked = False
            rb1.Checked = False
            rb4.Checked = False
            rb5.Checked = False
        ElseIf wt = "4" Then   
            rb4.Checked = True
            rb2.Checked = False
            rb3.Checked = False
            rb1.Checked = False
            rb5.Checked = False
        ElseIf wt = "5" Then
            rb5.Checked = True
            rb2.Checked = False
            rb3.Checked = False
            rb4.Checked = False
            rb1.Checked = False
            'Dim strMessage As String = wt
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        weights.Dispose()
    End Sub


    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsave.Click
        Dim wt, l1, l2, l3, l4, l5 As String
        wt = lblwt2.Value
        l1 = t1.Text
        l2 = t2.Text
        l3 = t3.Text
        l4 = t4.Text
        l5 = t5.Text
        ecid = lblecrid.Text
        cid = lblcid.Text
        Dim weights1 As New Utilities
        weights1.Open()
        sql = "update ecrcatmaster set " _
        + "weight = '" & wt & "', " _
        + "level1 = '" & l1 & "', " _
        + "level2 = '" & l2 & "', " _
        + "level3 = '" & l3 & "', " _
        + "level4 = '" & l4 & "', " _
        + "level5 = '" & l5 & "' " _
        + "where ecrcatid = '" & ecid & "'"
        weights1.Update(sql)
        sql = "update ecrcatdata set weight = (select top 1 m.weight from ecrcatmaster m where m.ecrcatid = '" & ecid & "') where ecrcatid = '" & ecid & "'"
        weights1.Update(sql)
        'Update wtstat & catstat
        sql = "update ecrcatmaster " _
        + "set catstat = 'all defined' " _
        + "where compid ='" & cid & "' and " _
        + "(len(level1) > 0 and len(level2) > 0 and " _
        + "len(level3) > 0 and len(level4) > 0 and len(level5) > 0)"
        weights1.Update(sql)
        sql = "update ecrcatmaster " _
        + "set wtstat = 'assigned' " _
        + "where compid ='" & cid & "' and " _
        + "weight <> 0"
        weights1.Update(sql)

        Dim asncnt As Integer
        sql = "select count(*) from ecrcatmaster where catstat = 'incomplete' or wtstat = 'unassigned'"
        asncnt = weights1.Scalar(sql)
        If asncnt = 0 Then
            sql = "update pmsysvars set ecrflg = '1'"
            weights1.Update(sql)
        End If
        weights1.Dispose()
        GetWT(cid, ecid)
    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label3.Text = axlabs.GetASPXPage("weights.aspx", "Label3")
        Catch ex As Exception
        End Try
        Try
            Label4.Text = axlabs.GetASPXPage("weights.aspx", "Label4")
        Catch ex As Exception
        End Try
        Try
            Label5.Text = axlabs.GetASPXPage("weights.aspx", "Label5")
        Catch ex As Exception
        End Try
        Try
            Label6.Text = axlabs.GetASPXPage("weights.aspx", "Label6")
        Catch ex As Exception
        End Try
        Try
            Label7.Text = axlabs.GetASPXPage("weights.aspx", "Label7")
        Catch ex As Exception
        End Try
        Try
            Label8.Text = axlabs.GetASPXPage("weights.aspx", "Label8")
        Catch ex As Exception
        End Try
        Try
            lang2553.Text = axlabs.GetASPXPage("weights.aspx", "lang2553")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                btnsave.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                btnsave.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                btnsave.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                btnsave.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                btnsave.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
