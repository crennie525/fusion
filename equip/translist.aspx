<%@ Page Language="vb" AutoEventWireup="false" Codebehind="translist.aspx.vb" Inherits="lucy_r12.translist" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>translist</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  src="../scripts1/translistaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="checkret();">
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td>
						<asp:CheckBoxList id="cbtranslist" runat="server" Width="208px" CssClass="label">
							<asp:ListItem Value="eq">Equipment</asp:ListItem>
							<asp:ListItem Value="eqdesc">- Equipment Description</asp:ListItem>
							<asp:ListItem Value="eqspl">- Equipment SPL</asp:ListItem>
							<asp:ListItem Value="fu">Functions</asp:ListItem>
							<asp:ListItem Value="fuspl">- Function SPL</asp:ListItem>
							<asp:ListItem Value="comp">Components</asp:ListItem>
							<asp:ListItem Value="compdesc">- Component Description</asp:ListItem>
							<asp:ListItem Value="compdesg">- Component Designation</asp:ListItem>
							<asp:ListItem Value="compspl">- Component SPL</asp:ListItem>
						</asp:CheckBoxList>
					</td>
				</tr>
				<tr>
					<td>
						<asp:CheckBox id="cball" runat="server" CssClass="bluelabel" Text="Translate All"></asp:CheckBox></td>
				</tr>
				<tr>
					<td align="right">
						<img id="btnreturn" runat="server" src="../images/appbuttons/bgbuttons/return.gif" onclick="ret();">&nbsp;
						<asp:ImageButton id="btnsubmit1" runat="server" ImageUrl="../images/appbuttons/bgbuttons/submit.gif"></asp:ImageButton></td>
				</tr>
			</table>
			<input type="hidden" id="lbleqnum" runat="server"><input type="hidden" id="lblret" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
