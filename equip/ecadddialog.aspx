﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ecadddialog.aspx.vb" Inherits="lucy_r12.ecadddialog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error Code Entry</title>
    <script  type ="text/javascript">
        function handlereturn(ret) {
            window.returnValue = ret;
            window.close();
        }
        function GetWidth() {
            var x = 0;
            if (self.innerHeight) {
                x = self.innerWidth;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                x = document.documentElement.clientWidth;
            }
            else if (document.body) {
                x = document.body.clientWidth;
            }
            return x;
        }

        function GetHeight() {
            var y = 0;
            if (self.innerHeight) {
                y = self.innerHeight;
            }
            else if (document.documentElement && document.documentElement.clientHeight) {
                y = document.documentElement.clientHeight;
            }
            else if (document.body) {
                y = document.body.clientHeight;
            }
            return y;
        }
        var flag = 5
        function upsize(who) {
            if (who != flag) {
                flag = who;
                document.getElementById("ifecd").height = GetHeight();
                window.scrollTo(0, top);
                //alert(who)
            }
        }
    </script>
</head>
<body onload="upsize('1');">
<script type="text/javascript">
    document.body.onresize = function () {
        upsize('2');
    }
    self.onresize = function () {
        upsize('3');
    }
    document.documentElement.onresize = function () {
        upsize('4');
    }
    </script>
    <form id="form1" runat="server">
    <div>
    <iframe id="ifecd" runat="server" width="100%" height="100%" frameborder="0"></iframe>
    <iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script>
    </div>
    </form>
</body>
</html>
