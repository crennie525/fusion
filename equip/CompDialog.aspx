<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CompDialog.aspx.vb" Inherits="lucy_r12.CompDialog" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>Add/Edit Components</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<script  src="../scripts/sessrefdialog.js"></script>
		<script  src="../scripts1/CompDialogaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body class="tbg" onunload="handleexit();" onload="handleentry();" >
		<form id="form1" method="post" runat="server">
			<table>
				<tr>
					<td id="tdmsg"></td>
				</tr>
			</table>
			<iframe id="ifaddcomp" height="150%" src="" frameBorder="no" width="100%"></iframe>
			<iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script><input id="lblcid" type="hidden" name="lblcid" runat="server"> <input id="lbleqid" type="hidden" name="lbleqid" runat="server">
			<input id="lblfuid" type="hidden" name="lblfuid" runat="server"> <input id="delflag" type="hidden" runat="server">
			<input id="lbllog" type="hidden" runat="server"><input id="lblro" type="hidden" runat="server">
			<input id="lbltpm" type="hidden" runat="server"> <input type="hidden" id="lblsessrefresh" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
