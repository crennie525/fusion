

'********************************************************
'*
'********************************************************




Imports System.Data.SqlClient

Public Class EqGrid
    Inherits System.Web.UI.Page
	Protected WithEvents Label15 As System.Web.UI.WebControls.Label

	Protected WithEvents Label14 As System.Web.UI.WebControls.Label

	Protected WithEvents Label13 As System.Web.UI.WebControls.Label

	Protected WithEvents Label1 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden


    Dim chk, cid, sid, did, clid, eqid As String
    Dim se, Filter, AddVal, SubVal, FiltFilt, login, sql, ro, lid, ustr, who, dept, cell, loc, eq As String
    Dim ds, dshd, dslev As DataSet
    Dim dr As SqlDataReader
    Dim eqg As New Utilities
    Protected WithEvents txteq As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfiltfilt As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents dgeq As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblustr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqidh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label54 As System.Web.UI.WebControls.Label
    Protected WithEvents lstfilter As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Label55 As System.Web.UI.WebControls.Label
    Protected WithEvents lstdef As System.Web.UI.WebControls.DropDownList
    Protected WithEvents addtask As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnexport As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnreturn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsort As System.Web.UI.WebControls.Label
    Protected WithEvents ErrorLabel As System.Web.UI.WebControls.Label
    Protected WithEvents dlhd As System.Web.UI.WebControls.DataList
    Protected WithEvents lbleqid As System.Web.UI.WebControls.Label
    Protected WithEvents xcoord As System.Web.UI.WebControls.TextBox
    Protected WithEvents ycoord As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            urlname = urlname & "?logout=yes"
            Response.Redirect(urlname)
            Exit Sub
        End Try

        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
                dept = Request.QueryString("dept").ToString
                lbldept.Value = dept
                cell = Request.QueryString("cell").ToString
                lblcell.Value = cell
                loc = Request.QueryString("loc").ToString
                lblloc.Value = loc
                eqid = Request.QueryString("eqid").ToString
                lbleqidh.Value = eqid
                eq = Request.QueryString("eq").ToString
                lbleq.Value = eq
            Catch ex As Exception

            End Try
            ustr = Request.QueryString("ustr").ToString
            lblustr.Value = ustr
            Try
                Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
                Dim sessrefi As Integer = sessref * 1000 * 60
                lblsessrefresh.Value = sessrefi
            Catch ex As Exception
                lblsessrefresh.Value = "300000"
            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgeq.Columns(0).Visible = False
            End If
            cid = Request.QueryString("cid").ToString
            lblcid.Value = cid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            lid = Request.QueryString("lid").ToString
            lbllid.Value = lid
            chk = Request.QueryString("chk").ToString
            lblchk.Value = chk
            'ErrorLabel.Text = chk
            eqg.Open()
            If chk = "yes" Then
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                clid = Request.QueryString("clid").ToString
                lblclid.Value = clid
                If lid = "" Then
                    Filter = "siteid = '" & sid & "' and " _
                                   + "dept_id = '" & did & "' and cellid = '" & clid & "'"
                Else
                    Filter = "siteid = '" & sid & "' and " _
                                   + "dept_id = '" & did & "' and cellid = '" & clid & "' and locid = '" & lid & "'"
                End If

            ElseIf chk = "no" Then
                did = Request.QueryString("did").ToString
                lbldid.Value = did
                If lid = "" Then
                    Filter = "siteid = '" & sid & "' and " _
                                   + "dept_id = '" & did & "'"
                Else
                    Filter = "siteid = '" & sid & "' and " _
                                   + "dept_id = '" & did & "' and locid = '" & lid & "'"
                End If

            ElseIf chk = "nn" Then
                Dim lcnt As Integer
                Dim loc2 As String
                sql = "select location from pmlocations where locid = '" & lid & "'"
                loc2 = eqg.strScalar(sql)
                If loc2 <> "" Then
                    sql = "select count(*) from pmlocations where locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                       + "and type = 'EQUIPMENT'"
                    lcnt = eqg.Scalar(sql)
                    If lcnt > 0 Then
                        Filter = "locid in (select locid from pmlocations where location in (select location from pmlocheir where parent = '" & loc2 & "' and siteid = '" & sid & "')) " _
                        + "and siteid = '" & sid & "' and dept_id is null"
                    Else
                        Filter = "locid = '" & lid & "' and siteid = '" & sid & "' and dept_id is null"

                    End If
                End If

            End If
            lblfilt.Value = Filter
            lblcurrsort.Value = "eqnum asc"
            FiltFilt = ""
            lblsort.Text = "Equipment# Ascending"
            BindGrid("eqnum asc", Filter, FiltFilt)
            BindHead(chk, cid, sid, did, clid, Filter)
            eqg.Dispose()
        End If
        'addtask.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'addtask.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'btnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'btnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub BindHead(ByVal chk As String, ByVal cid As String, ByVal sid As String, _
    ByVal did As String, ByVal clid As String, ByVal filt As String)

        'Try

        If chk = "yes" Then
            sql = "select " _
                + "c.compname, " _
                + "d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                + "cl.cell_name, cl.cell_desc " _
                + "from company c, dept d, sites s, cells cl " _
                + "where cl.Dept_ID = d.Dept_ID and " _
                + "d.siteid = s.siteid  and cl.cellid = '" & clid & "'"
        ElseIf chk = "yn" Then
            sql = "select " _
                + "c.compname, " _
                + "d.dept_line, d.dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                + "'No Cells' as cell_name, 'No Cells Used' as cell_desc " _
                + "from company c, dept d, sites s " _
                + "where " _
                + "d.siteid = s.siteid and d.Dept_ID = '" & did & "'"
        ElseIf chk = "nn" Then
            sql = "select " _
                + "c.compname, " _
                + "'No Depts' as dept_line, 'No Depts/Lines Used' as dept_desc, s.siteid, s.sitename, s.sitedesc, " _
                + "'No Cells' as cell_name, 'No Cells Used' as cell_desc " _
                + "from company c, sites s " _
                + "where s.siteid = '" & sid & "'"

        End If
        dr = eqg.GetRdrData(sql)
        dlhd.DataSource = dr
        dlhd.DataBind()
        dr.Close()
        eqg.Dispose()
        'Catch ex As Exception
        'Label8.Text = sql
        'End Try

    End Sub
    Private Sub BindGrid(ByVal sortExp As String, ByVal Filter As String, ByVal FiltFilt As String)

        Try
            
            sql = "select * from equipment where " & Filter & FiltFilt & " order by " & sortExp
            ds = eqg.GetDSData(sql)
            Dim dv As DataView
            dv = ds.Tables(0).DefaultView
            Try
                dgeq.DataSource = dv
                dgeq.DataBind()
                dgeq.Columns(16).Visible = False
            Catch ex As Exception

            End Try

        Catch ex As Exception

        End Try
    End Sub
    Public Function PopulateStatus() As DataSet
        'eqg.Open()
        cid = lblcid.Value
        sql = "select statid, status from eqstatus where eqstatindex = '0' order by eqstatindex"
        ds = eqg.GetDSData(sql)
        'eqg.Dispose()
        Return ds
    End Function
    Public Function GetSelIndex(ByVal CatID As String) As Integer
        Dim iL As Integer
        'If Not IsDBNull(CatID) OrElse CatID <> "" Then
        iL = CatID
        'Else
        'CatID = 0
        'End If
        Return iL
    End Function

    Private Sub dgeq_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeq.ItemCommand
        Dim vc As String = e.CommandName
        Select Case vc
            Case "hd"
                dgeq.Columns(3).Visible = False
            Case "sd"
                dgeq.Columns(3).Visible = True
            Case "hl"
                dgeq.Columns(5).Visible = False
            Case "sl"
                dgeq.Columns(5).Visible = True
            Case "ho"
                dgeq.Columns(7).Visible = False
            Case "so"
                dgeq.Columns(7).Visible = True
            Case "hm"
                dgeq.Columns(9).Visible = False
            Case "sm"
                dgeq.Columns(9).Visible = True
            Case "hs"
                dgeq.Columns(11).Visible = False
            Case "ss"
                dgeq.Columns(11).Visible = True
            Case "hes"
                dgeq.Columns(13).Visible = False
            Case "ses"
                dgeq.Columns(13).Visible = True
            Case "hec"
                dgeq.Columns(15).Visible = False
            Case "sec"
                dgeq.Columns(15).Visible = True

        End Select
    End Sub

    Private Sub dgeq_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgeq.SortCommand
        Filter = lblfilt.Value
        FiltFilt = lblfiltfilt.Value
        BindGrid(e.SortExpression, Filter, FiltFilt)
        Dim se As String = e.SortExpression
        Session("CurrSort") = se
        Select Case se
            Case "eqnum desc"
                dgeq.Columns(1).SortExpression = "eqnum asc"
                lblsort.Text = "Equipment# Descending"
            Case "eqnum asc"
                dgeq.Columns(1).SortExpression = "eqnum desc"
                lblsort.Text = "Equipment# Ascending"
            Case "oem desc"
                dgeq.Columns(7).SortExpression = "oem asc"
                lblsort.Text = "OEM Descending"
            Case "oem asc"
                dgeq.Columns(7).SortExpression = "oem desc"
                lblsort.Text = "OEM Ascending"
            Case "model desc"
                dgeq.Columns(9).SortExpression = "model asc"
                lblsort.Text = "Model Descending"
            Case "model asc"
                dgeq.Columns(9).SortExpression = "model desc"
                lblsort.Text = "Model Ascending"
            Case "serial desc"
                dgeq.Columns(11).SortExpression = "serial asc"
                lblsort.Text = "Serial Descending"
            Case "serial asc"
                dgeq.Columns(11).SortExpression = "serial desc"
                lblsort.Text = "Serial Ascending"
            Case "status desc"
                dgeq.Columns(13).SortExpression = "status asc"
                lblsort.Text = "Status Descending"
            Case "status asc"
                dgeq.Columns(13).SortExpression = "status desc"
                lblsort.Text = "Status Ascending"
            Case "ecr desc"
                dgeq.Columns(15).SortExpression = "ecr asc"
                lblsort.Text = "ECR Descending"
            Case "ecr asc"
                dgeq.Columns(15).SortExpression = "ecr desc"
                lblsort.Text = "ECR Ascending"

        End Select
    End Sub

    Private Sub dgeq_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeq.EditCommand
        Dim eqlock, lockby As String
        Dim user As String = HttpContext.Current.Session("username").ToString
        eqlock = CType(e.Item.FindControl("lbleqlock"), Label).Text
        lockby = CType(e.Item.FindControl("lbllockby"), Label).Text
        If eqlock = "1" Then
            If user = lockby Then
                eqlock = "0"
            End If
        End If
        If eqlock <> "1" Then
            dgeq.Columns(16).Visible = True
            Dim eid As String
            eid = CType(e.Item.FindControl("lbleq"), Label).Text
            txteq.Text = eid
            dgeq.Columns(16).Visible = False
            dgeq.EditItemIndex = e.Item.ItemIndex
            se = lblcurrsort.Value
            Filter = lblfilt.Value
            FiltFilt = lblfiltfilt.Value
            BindGrid(se, Filter, FiltFilt)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr919" , "EqGrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        
    End Sub

    Private Sub dgeq_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeq.UpdateCommand
        eqg.Open()
        Dim stat, statid As String
        statid = CType(e.Item.FindControl("ddstat"), DropDownList).SelectedValue
        If Len(statid) = 0 Then
            statid = "0"
        End If
        Try
            stat = CType(e.Item.FindControl("ddstat"), DropDownList).SelectedItem.ToString
        Catch ex As Exception

        End Try
        Dim eqn, des, loc, oem, mode, ser, ecr, eqi As String
        eqn = CType(e.Item.FindControl("txteqnum"), TextBox).Text
        eqn = eqg.ModString3(eqn)
        clid = lblclid.Value
        'clid = HttpContext.Current.Session("clid").ToString()
        'did = HttpContext.Current.Session("did").ToString()
        did = lbldid.Value
        lid = lbllid.Value
        If Len(eqn) = 0 Then
            ErrorLabel.Text = "Equipment Number Cannot Be NULL"
        Else
            Dim echk As Integer
            If clid = "0" Then
                sql = "select count(*) from equipment " _
                + "where eqnum = '" & eqn & "' and Dept_ID = '" & did & "'"
                echk = eqg.Scalar(sql)
            Else
                sql = "select count(*) from equipment " _
                + "where eqnum = '" & eqn & "' and cellid = '" & clid & "'"
                echk = eqg.Scalar(sql)
            End If
            If echk = 0 And lid <> "" And lid <> "0" Then
                sql = "select count(*) from pmlocations where location = '" & eqn & "'"
                echk = eqg.Scalar(sql)
            End If
            If echk > 1 And clid = "0" And (lid = "" Or lid = "0") Then
                ErrorLabel.Text = "Equipment Number must be unique within a Department"
            ElseIf echk > 1 And clid <> "0" And (lid = "" Or lid = "0") Then
                ErrorLabel.Text = "Equipment Number must be unique within a Station/Cell"
            ElseIf echk > 1 And lid <> "" And lid <> "0" Then
                ErrorLabel.Text = "Location Based Equipment Number must be unique in Locations Table"
            Else
                des = CType(e.Item.FindControl("txtdesc"), TextBox).Text
                If Len(des) = 0 Then
                    des = Nothing
                End If
                des = eqg.ModString1(des)
                loc = CType(e.Item.FindControl("txtloco"), TextBox).Text
                loc = eqg.ModString1(loc)
                oem = CType(e.Item.FindControl("txtoem"), TextBox).Text
                oem = eqg.ModString1(oem)
                mode = CType(e.Item.FindControl("txtmodel"), TextBox).Text
                mode = eqg.ModString1(mode)
                ser = CType(e.Item.FindControl("txtserial"), TextBox).Text
                ser = eqg.ModString1(ser)
                ecr = CType(e.Item.FindControl("txtecr"), TextBox).Text
                Dim ecrchk As Long
                Try
                    System.Convert.ToDecimal(ecr)
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr920", "EqGrid.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    Exit Sub
                End Try
                If Len(ecr) = 0 Then
                    ecr = "0"
                End If
                eqi = txteq.Text 'CType(e.Item.FindControl("txteqid"), TextBox).Text
                'Filter = HttpContext.Current.Session("Filt").ToString()
                Filter = lblfilt.Value
                sql = "update equipment set " _
                + "eqnum = '" & eqn & "', " _
                + "eqdesc = '" & des & "', " _
                + "location = '" & loc & "', " _
                + "oem = '" & oem & "', " _
                + "model = '" & mode & "', " _
                + "serial = '" & ser & "', " _
                + "ecr = '" & ecr & "', " _
                + "statid = '" & statid & "', " _
                + "status = '" & stat & "' " _
                + "where " & Filter & " and eqid = '" & eqi & "' " _
                + "update equipment  set eqstatindex = (" _
                + "select eqstatindex from eqstatus where " _
                + "statid = '" & statid & "') where " & Filter & " and eqid = '" & eqi & "'"
                Try
                    eqg.Update(sql)
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr921", "EqGrid.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                End Try
                'eqg.Dispose()
                dgeq.EditItemIndex = -1
                se = lblcurrsort.Value
                Filter = lblfilt.Value
                FiltFilt = lblfiltfilt.Value
                BindGrid(se, Filter, FiltFilt)
            End If
        End If
    End Sub
    Private Sub dgeq_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgeq.CancelCommand
        dgeq.EditItemIndex = -1
        se = lblcurrsort.Value
        Filter = lblfilt.Value
        FiltFilt = lblfiltfilt.Value
        BindGrid(se, Filter, FiltFilt)
    End Sub

    Private Sub addtask_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addtask.Click
        eqg.Open()
        cid = "0"
        sid = HttpContext.Current.Session("sid").ToString()
        did = HttpContext.Current.Session("did").ToString()
        clid = HttpContext.Current.Session("clid").ToString()
        If clid = "0" Then
            sql = "insert into equipment (compid, siteid, eqnum, Dept_ID) values " _
           + "('" & cid & "', '" & sid & "', 'New', '" & did & "')"
            eqg.Update(sql)
        Else
            sql = "insert into equipment (compid, siteid, eqnum, cellid, dept_id) values " _
            + "('" & cid & "', '" & sid & "', 'New', '" & clid & "', '" & did & "')"
            eqg.Update(sql)
        End If
        sql = "update admintasks set eq = (select count(*) from equipment)"
        eqg.Update(sql)

        eqg.Dispose()
        se = lblcurrsort.Value
        Filter = lblfilt.Value
        FiltFilt = lblfiltfilt.Value
        BindGrid(se, Filter, FiltFilt)
    End Sub

    Private Sub lstfilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstfilter.SelectedIndexChanged
        eqg.Open()
        Dim filtid As String
        filtid = lstfilter.SelectedValue
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        Filter = lblfilt.Value
        Select Case filtid
            Case 1 'OEM
                sql = "select distinct oem " _
                + "from equipment where " & Filter & " and oem is not null"
                dr = eqg.GetRdrData(sql)
                lstdef.DataSource = dr
                lstdef.DataTextField = "oem"
                lstdef.DataValueField = "oem"
                lstdef.DataBind()
                dr.Close()
                lstdef.Items.Insert(0, New ListItem("Select OEM"))
                lstdef.Items(0).Value = 0
            Case 2 'model
                sql = "select distinct model " _
                + "from equipment where " & Filter & " and model is not null and len(model) <> 0"
                dr = eqg.GetRdrData(sql)
                lstdef.DataSource = dr
                lstdef.DataTextField = "model"
                lstdef.DataValueField = "model"
                lstdef.DataBind()
                dr.Close()
                lstdef.Items.Insert(0, New ListItem("Select Model"))
                lstdef.Items(0).Value = 0
            Case 3 'status
                sql = "select distinct statid, status " _
                + "from equipment where " & Filter & " and statid <> '0'"
                dr = eqg.GetRdrData(sql)
                lstdef.DataSource = dr
                lstdef.DataTextField = "status"
                lstdef.DataValueField = "statid"
                lstdef.DataBind()
                dr.Close()
                lstdef.Items.Insert(0, New ListItem("Select Status"))
                lstdef.Items(0).Value = 0
            Case 4 ' ecr
                lstdef.Items.Insert(0, New ListItem("Select Rating"))
                lstdef.Items(0).Value = 0
                lstdef.Items.Insert(1, New ListItem("1"))
                lstdef.Items(0).Value = 1
                lstdef.Items.Insert(2, New ListItem("2"))
                lstdef.Items(0).Value = 2
                lstdef.Items.Insert(3, New ListItem("3"))
                lstdef.Items(0).Value = 3
                lstdef.Items.Insert(4, New ListItem("4"))
                lstdef.Items(0).Value = 4
                lstdef.Items.Insert(5, New ListItem("5"))
                lstdef.Items(0).Value = 5
                lstdef.Items.Insert(6, New ListItem("6"))
                lstdef.Items(0).Value = 6
                lstdef.Items.Insert(7, New ListItem("7"))
                lstdef.Items(0).Value = 7
                lstdef.Items.Insert(8, New ListItem("8"))
                lstdef.Items(0).Value = 8
                lstdef.Items.Insert(9, New ListItem("9"))
                lstdef.Items(0).Value = 9
                lstdef.Items.Insert(10, New ListItem("10"))
                lstdef.Items(0).Value = 10
            Case Else
        End Select
        eqg.Dispose()
    End Sub

    Private Sub lstdef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstdef.SelectedIndexChanged
        Dim filtid, defid, defidmax As String
        filtid = lstfilter.SelectedValue
        Dim rdrfu As SqlDataReader
        Filter = lblfilt.Value
        se = lblcurrsort.Value
        Select Case filtid
            Case 1 'oem
                defid = lstdef.SelectedValue.ToString
                FiltFilt = " and oem = '" & defid & "'"
                lblfiltfilt.Value = FiltFilt
            Case 2 'model
                defid = lstdef.SelectedValue.ToString
                FiltFilt = " and model = '" & defid & "'"
                lblfiltfilt.Value = FiltFilt
            Case 3 ' status
                defid = lstdef.SelectedValue.ToString
                FiltFilt = " and statid = '" & defid & "'"
                lblfiltfilt.Value = FiltFilt
            Case 4 'ecr
                defid = lstdef.SelectedValue.ToString
                defidmax = defid + 0.99
                FiltFilt = " and ecr between '" & defid & "' and '" & defidmax & "'"
                lblfiltfilt.Value = FiltFilt
            Case Else
        End Select
        BindGrid(se, Filter, FiltFilt)
    End Sub

    Private Sub btnexport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnexport.Click


    End Sub

    Private Sub btnreturn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnreturn.Click
        chk = lblchk.Value
        cid = lblcid.Value
        sid = lblsid.Value
        lid = lbllid.Value
        ustr = lblustr.Value
        who = lblwho.Value
        dept = lbldept.Value
        cell = lblcell.Value
        loc = lblloc.Value
        eqid = lbleqidh.Value
        eq = lbleq.Value
        If chk = "yes" Then
            did = lbldid.Value
            clid = lblclid.Value
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=no&start=yes&ret=yes&chk=yes&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&usrname=" + ustr + "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&eqid=" & eqid & "&eq=" & eq & "&fuid=&coid=")
            Else
                Response.Redirect("eqmain2.aspx?lvl=no&start=yes&ret=yes&chk=yes&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&ustr=" + ustr)
            End If

        ElseIf chk = "yn" Then
            did = lbldid.Value
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=no&start=yes&ret=yes&chk=yn&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&usrname=" + ustr + "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&eqid=" & eqid & "&eq=" & eq & "&fuid=&coid=")
            Else
                Response.Redirect("eqmain2.aspx?lvl=no&start=yes&ret=yes&chk=yn&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & "&ustr=" + ustr)
            End If

        ElseIf chk = "nn" Then
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=no&start=yes&ret=yes&chk=nn&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&usrname=" + ustr + "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&eqid=" & eqid & "&eq=" & eq & "&fuid=&coid=")
            Else
                Response.Redirect("eqmain2.aspx?lvl=no&start=yes&ret=yes&chk=nn&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=&clid=" & "&ustr=" + ustr)
            End If

        End If

    End Sub

    
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgeq.Columns(0).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "0")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(1).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "1")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(2).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "2")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(3).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "3")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(4).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "4")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(5).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "5")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(8).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "8")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(9).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "9")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(10).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "10")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(11).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "11")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(12).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "12")
        Catch ex As Exception
        End Try
        Try
            dgeq.Columns(13).HeaderText = dlabs.GetDGPage("EqGrid.aspx", "dgeq", "13")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label1.Text = axlabs.GetASPXPage("EqGrid.aspx", "Label1")
        Catch ex As Exception
        End Try
        Try
            Label13.Text = axlabs.GetASPXPage("EqGrid.aspx", "Label13")
        Catch ex As Exception
        End Try
        Try
            Label14.Text = axlabs.GetASPXPage("EqGrid.aspx", "Label14")
        Catch ex As Exception
        End Try
        Try
            Label15.Text = axlabs.GetASPXPage("EqGrid.aspx", "Label15")
        Catch ex As Exception
        End Try
        Try
            Label54.Text = axlabs.GetASPXPage("EqGrid.aspx", "Label54")
        Catch ex As Exception
        End Try
        Try
            Label55.Text = axlabs.GetASPXPage("EqGrid.aspx", "Label55")
        Catch ex As Exception
        End Try
        Try
            Label9.Text = axlabs.GetASPXPage("EqGrid.aspx", "Label9")
        Catch ex As Exception
        End Try
        Try
            lblsort.Text = axlabs.GetASPXPage("EqGrid.aspx", "lblsort")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addtask.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                addtask.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                addtask.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                addtask.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                addtask.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnexport.Attributes.Add("src", "../images2/eng/bgbuttons/export.gif")
            ElseIf lang = "fre" Then
                btnexport.Attributes.Add("src", "../images2/fre/bgbuttons/export.gif")
            ElseIf lang = "ger" Then
                btnexport.Attributes.Add("src", "../images2/ger/bgbuttons/export.gif")
            ElseIf lang = "ita" Then
                btnexport.Attributes.Add("src", "../images2/ita/bgbuttons/export.gif")
            ElseIf lang = "spa" Then
                btnexport.Attributes.Add("src", "../images2/spa/bgbuttons/export.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                btnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                btnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                btnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                btnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                btnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
