﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="meterlookup.aspx.vb" Inherits="lucy_r12.meterlookup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Equipment Meters</title>
<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet"/>
		<script  type="text/javascript">
		    function getmeter(meterid, munit, meter) {
		        var ret = meterid + "~" + munit + "~" + meter;
		        window.returnValue = ret;
		        window.close();
		    }
		</script>
</head>
<body>
	<form id="form1" runat="server">
	<div>
	<table>
				<tr>
					<td>
						<div id="totdiv" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; OVERFLOW: auto; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; HEIGHT: 200px; WIDTH: 250px;"
							runat="server"></div>
					</td>
				</tr>
			</table>
			
	</div>
	<input id="lbleqid" type="hidden" runat="server"/> <input id="lbltyp" type="hidden" runat="server"/>
	</form>
</body>
</html>
