

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class pmlibtpmtaskdets
    Inherits System.Web.UI.Page
	Protected WithEvents lang2536 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2535 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2534 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2533 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2532 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2531 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2530 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2529 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2528 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2527 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2526 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim eqid, db, start, fuid As String
    Protected WithEvents trmsg As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents tdmsg As System.Web.UI.HtmlControls.HtmlTableCell
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rptrtaskrev As System.Web.UI.WebControls.Repeater
    Protected WithEvents lbldb As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                fuid = Request.QueryString("fuid").ToString
                db = Request.QueryString("db").ToString
                lblfuid.Value = fuid
                lbldb.Value = db
                copy.Open()
                PopTaskRev(fuid)
                copy.Dispose()
            Else
                tdmsg.InnerHtml = "Waiting for data..."
                trmsg.Attributes.Add("class", "view")
            End If

        End If
    End Sub
    Private Sub PopTaskRev(ByVal funcid As String)
        Dim db As String = lbldb.Value
        Dim lang As String = lblfslang.Value
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        db = lbldb.Value
        Dim mdbarr() As String = db.Split(",")
        Dim tst As Integer = mdbarr.Length
        srvr = mdbarr(1).ToString
        db = mdbarr(2).ToString
        Dim tcnt As Integer
        sql = "select count(*) from  [" & srvr & "].[" & db & "].[dbo].[pmtaskstpm] where funcid = '" & funcid & "'"
        tcnt = copy.Scalar(sql)
        If tcnt = 0 Then
            tdmsg.InnerHtml = "No Records Found"
            trmsg.Attributes.Add("class", "view")
        Else
            trmsg.Attributes.Add("class", "details")
        End If
        sql = "usp_getTaskReviewtpm '" & funcid & "', '" & db & "','" & srvr & "','" & lang & "'"
        dr = copy.GetRdrData(sql)
        rptrtaskrev.DataSource = dr
        rptrtaskrev.DataBind()
        dr.Close()
    End Sub
	



    Private Sub rptrtaskrev_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrtaskrev.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2526 As Label
                lang2526 = CType(e.Item.FindControl("lang2526"), Label)
                lang2526.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2526")
            Catch ex As Exception
            End Try
            Try
                Dim lang2527 As Label
                lang2527 = CType(e.Item.FindControl("lang2527"), Label)
                lang2527.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2527")
            Catch ex As Exception
            End Try
            Try
                Dim lang2528 As Label
                lang2528 = CType(e.Item.FindControl("lang2528"), Label)
                lang2528.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2528")
            Catch ex As Exception
            End Try
            Try
                Dim lang2529 As Label
                lang2529 = CType(e.Item.FindControl("lang2529"), Label)
                lang2529.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2529")
            Catch ex As Exception
            End Try
            Try
                Dim lang2530 As Label
                lang2530 = CType(e.Item.FindControl("lang2530"), Label)
                lang2530.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2530")
            Catch ex As Exception
            End Try
            Try
                Dim lang2531 As Label
                lang2531 = CType(e.Item.FindControl("lang2531"), Label)
                lang2531.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2531")
            Catch ex As Exception
            End Try
            Try
                Dim lang2532 As Label
                lang2532 = CType(e.Item.FindControl("lang2532"), Label)
                lang2532.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2532")
            Catch ex As Exception
            End Try
            Try
                Dim lang2533 As Label
                lang2533 = CType(e.Item.FindControl("lang2533"), Label)
                lang2533.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2533")
            Catch ex As Exception
            End Try
            Try
                Dim lang2534 As Label
                lang2534 = CType(e.Item.FindControl("lang2534"), Label)
                lang2534.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2534")
            Catch ex As Exception
            End Try
            Try
                Dim lang2535 As Label
                lang2535 = CType(e.Item.FindControl("lang2535"), Label)
                lang2535.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2535")
            Catch ex As Exception
            End Try
            Try
                Dim lang2536 As Label
                lang2536 = CType(e.Item.FindControl("lang2536"), Label)
                lang2536.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2536")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2526.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2526")
        Catch ex As Exception
        End Try
        Try
            lang2527.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2527")
        Catch ex As Exception
        End Try
        Try
            lang2528.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2528")
        Catch ex As Exception
        End Try
        Try
            lang2529.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2529")
        Catch ex As Exception
        End Try
        Try
            lang2530.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2530")
        Catch ex As Exception
        End Try
        Try
            lang2531.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2531")
        Catch ex As Exception
        End Try
        Try
            lang2532.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2532")
        Catch ex As Exception
        End Try
        Try
            lang2533.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2533")
        Catch ex As Exception
        End Try
        Try
            lang2534.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2534")
        Catch ex As Exception
        End Try
        Try
            lang2535.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2535")
        Catch ex As Exception
        End Try
        Try
            lang2536.Text = axlabs.GetASPXPage("pmlibtpmtaskdets.aspx", "lang2536")
        Catch ex As Exception
        End Try

    End Sub

End Class
