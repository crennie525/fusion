﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="emeterlook.aspx.vb" Inherits="lucy_r12.emeterlook" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script  type="text/javascript">
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lup";
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~");
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("lblsubmit").value = "getmeter";
                document.getElementById("form1").submit();
            }
        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lup&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~");
                //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" + eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp + "~" + lev;
                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];
                document.getElementById("lblsubmit").value = "getmeter";
                document.getElementById("form1").submit();
                
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
         <tr>
                <td class="thdrsing label" colspan="4">
                    Add/Edit Meters
                </td>
            </tr>
        <tr>
                <td>
                    <table>
                        <tr>
                            <td id="tddepts" class="bluelabel" runat="server">
                                Use Departments
                            </td>
                            <td>
                                <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td id="tdlocs1" class="bluelabel" runat="server">
                                Use Locations
                            </td>
                            <td>
                                <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="bluelabel" style="width: 100px">
                                Equipment#
                            </td>
                            <td id="tdeq" class="plainlabel" runat="server" width="160">
                            </td>
                            <td>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dgmeters" runat="server" CellPadding="0" GridLines="None" AllowPaging="True"
                        AllowCustomPaging="True" AutoGenerateColumns="False" ShowFooter="True" CellSpacing="1"
                        BackColor="transparent">
                        <FooterStyle BackColor="transparent" CssClass="plainlabel"></FooterStyle>
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="Imagebutton7" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                        CommandName="Add"></asp:ImageButton>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblmeterid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'
                                        NAME="Label21">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmeteride" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'
                                        NAME="Label21">
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Meter ID">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="lblmeter" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewmeter" runat="server" Width="154px" MaxLength="50">
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:TextBox ID="txtmeter" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="shift desc" HeaderText="Units">
                                <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.unit") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewunit" runat="server" Width="60px" MaxLength="50" >
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                   <asp:TextBox ID="txtunit" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.unit") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Weekly Use">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="lbluse" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wkuse") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewuse" runat="server" Width="80px" MaxLength="50">
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:TextBox ID="txtuse" runat="server" Width="80px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wkuse") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Remove">
                                <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="Imagebutton16" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblunitret" runat="server" />
    <input type="hidden" id="lbluret" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblold" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    </form>
</body>
</html>
