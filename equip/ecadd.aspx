﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ecadd.aspx.vb" Inherits="lucy_r12.ecadd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts/smartscroll4.js"></script>
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
    <script  type="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function getfm(who) {
            var eReturn = window.showModalDialog("faillook.aspx", "", "dialogHeight:350px; dialogWidth:200px; resizable=yes");
            if (eReturn) {
                document.getElementById(who).value = eReturn;

            }
        }
        function getoc(who) {
            var eReturn = window.showModalDialog("oclook.aspx", "", "dialogHeight:350px; dialogWidth:200px; resizable=yes");
            if (eReturn) {
                document.getElementById(who).value = eReturn;

            }
        }
        function getnew2() {
            var e2 = document.getElementById("txtecd2").value;
            var e2d = document.getElementById("txtedesc2").value;
            if (e2.length > 5) {
                alert("Error Code 2 Limited To 5 Characters")
            }
            else if (e2d.length > 50) {
                alert("Error Code 2 Description Limited To 50 Characters")
            }
            else {
                if (e2 != "" && e2d != "") {
                    document.getElementById("lblsubmit").value = "new2";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Error Code 2 and Description Required")
                }
            }
        }
        function getnew3() {
            var e3 = document.getElementById("txtecd3").value;
            var e3d = document.getElementById("txtedesc3").value;
            if (e3.length > 5) {
                alert("Error Code 3 Limited To 5 Characters")
            }
            else if (e3d.length > 50) {
                alert("Error Code 3 Description Limited To 50 Characters")
            }
            else {
                if (e3 != "" && e3d != "") {
                    document.getElementById("lblsubmit").value = "new3";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Error Code 3 and Description Required")
                }
            }
        }
        function refit(who) {
            if (who == "2") {
                document.getElementById("txtecd2").value = "";
                document.getElementById("txtedesc2").value = "";
                document.getElementById("lblecd2id").value = "";
                document.getElementById("lble2desc").value = "";
                document.getElementById("lblecd2").value = "";
                document.getElementById("ddecd2").value = "Select";
                document.getElementById("imge2").className = "view";
            }
            else if (who == "3") {
                document.getElementById("txtecd3").value = "";
                document.getElementById("txtedesc3").value = "";
            }
        }
        function cecd2() {
            document.getElementById("lblecd2id").value = "";
            document.getElementById("lble2desc").value = "";
            document.getElementById("ddecd2").value = "Select";
            document.getElementById("imge2").className = "view";
        }
        function gohome() {
            window.parent.handlereturn("ok");

        }
    </script>
</head>
<body onload="GetsScroll();">
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="bluelabel" style="width: 140px" height="22">Error Code 1 Value</td>
    <td class="plainlabelred" id="tdec1" runat="server" style="width: 120px"></td>
    <td class="bluelabel" style="width: 100px" height="22">Description</td>
    <td class="plainlabelred" id="tdedesc1" runat="server" style="width: 120px"></td>
    </tr>
    <tr> 
    <td class="bluelabel">Error Code 2 Value</td>
    <td><asp:TextBox ID="txtecd2" runat="server" cssclass="plainlabelred"></asp:TextBox></td>
    <td class="bluelabel">Description</td>
    <td><asp:TextBox ID="txtedesc2" runat="server" cssclass="plainlabelred"></asp:TextBox></td>
    <td><img id="img1" alt="" onclick="refit('2');" runat="server" src="../images/appbuttons/minibuttons/refreshit.gif" /></td>
    <td><img id="imgecd2" alt="" onclick="getoc('txtedesc2');" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" /></td>
    <td><img src="../images/appbuttons/minibuttons/addmod.gif" alt="" onclick="getnew2();" id="imge2" runat="server" /></td>
    </tr>
    
    <tr id="tr2" runat="server">
    <td class="bluelabel">or Select Existing</td>
    <td><asp:DropDownList ID="ddecd2"  cssclass="plainlabelred" runat="server" 
            AutoPostBack="True">
        </asp:DropDownList></td>
    </tr>
    <tr id="tr3" runat="server"> 
    <td class="bluelabel">Error Code 3 Value</td>
    <td><asp:TextBox ID="txtecd3" runat="server" cssclass="plainlabelred"></asp:TextBox></td>
    <td class="bluelabel">Description</td>
    <td><asp:TextBox ID="txtedesc3" runat="server" cssclass="plainlabelred"></asp:TextBox></td>
    <td><img id="img2" alt="" onclick="refit('3');" runat="server" src="../images/appbuttons/minibuttons/refreshit.gif" /></td>
    <td><img id="imgfm" alt="" runat="server" onclick="getfm('txtedesc3');" src="../images/appbuttons/minibuttons/magnifier.gif" /></td>
    <td><img src="../images/appbuttons/minibuttons/addmod.gif" alt="" onclick="getnew3();" /></td>
    </tr>
    <tr>
    <td colspan="8" align="right" class="plainlabel" height="22"><a href="" onclick="gohome();">Return</a></td>
    </tr>
    <tr>
    <td colspan="8">
    <div style="WIDTH: 600px; HEIGHT: 200px; OVERFLOW: auto" id="spdiv" runat="server" onscroll="SetsDivPosition();">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" Width="530">
                        <AlternatingRowStyle CssClass="ptransrowblue"></AlternatingRowStyle>
                        <RowStyle CssClass="ptransrow"></RowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Edit">
                                <HeaderStyle Height="20px" Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton7" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                           
                            <asp:TemplateField HeaderText="Code 2">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblby" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtecd2" runat="server" width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Description">
                                <HeaderStyle width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmroute" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.edesc2") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtedesc2" runat="server" width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.edesc2") %>'>
                                    </asp:TextBox>
                                    <img id="imgecd2" alt="" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Code 3">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblpm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtecd3" runat="server" width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                <HeaderStyle width="150px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbledesc3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.edesc3") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtedesc3" runat="server" width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.edesc3") %>'>
                                    </asp:TextBox>
                                    <img id="imgfm" alt="" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd1idi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd1id") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd1ide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd1id") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd2idi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2id") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd2ide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2id") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd3idi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3id") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd3ide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3id") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd2i" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd2e" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd2") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblecd3i" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblecd3e" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ecd3") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remove" ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Height="20px" Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
											CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                               
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    </div>
    </td>
    </tr>
    <tr>
					<td align="center" colspan="8">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
    </table>
    </div>
    <input type="hidden" id="lblecd1" runat="server" />
    <input type="hidden" id="lblecd1id" runat="server" />
    <input type="hidden" id="lblecd2" runat="server" />
    <input type="hidden" id="lblecd2id" runat="server" />
    <input type="hidden" id="lble1desc" runat="server" />
    <input type="hidden" id="lble2desc" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lblret" runat="server" />
    <input id="spdivy" type="hidden" name="spdivy" runat="server">
			<input id="xCoord" type="hidden"><input id="yCoord" type="hidden">
    </form>
</body>
</html>
