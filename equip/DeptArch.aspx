<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DeptArch.aspx.vb" Inherits="lucy_r12.DeptArch" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>DeptArch</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  src="../scripts1/DeptArchaspx3.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function getdets(eqid, sid, did, clid, chk, lid, eqnum) {
             //alert(eqnum)
             eqnum = eqnum.replace(/#/, "%23");
             //alert(eqnum)
             var eReturn = window.showModalDialog("../equip/fulist2dialog.aspx?eqid=" + eqid + "&eqnum=" + eqnum + "&date=" + Date(), "", "dialogHeight:400px; dialogWidth:450px; resizable=yes");
             if (eReturn) {
                 var ret = eReturn;
                 var retarr = ret.split(",");
                 var fid = retarr[0];
                 var cid = retarr[1];
                 if (fid == "log") {
                     window.parent.setref();
                 }
                 else {
                     if (fid != "no") {
                         if (cid != "") {
                             //gotoco(eqid, fid, cid, sid, did, clid, chk, lid)
                             gotoco(eqid, fid, cid, sid, did, clid, chk, lid)
                         }
                         else {
                             //gotofu(eqid, fid, sid, did, clid, chk, lid)
                             gotofu(eqid, fid, sid, did, clid, chk, lid)
                         }
                     }
                 }
             }
         }
         function getnext() {

             var cnt = document.getElementById("txtpgcnt").value;
             var pg = document.getElementById("txtpg").value;
             pg = parseInt(pg);
             cnt = parseInt(cnt)
             if (pg < cnt) {
                 var d = new Date();
                 var curr_date = d.getDate();
                 var curr_month = d.getMonth();
                 curr_month++;
                 var curr_year = d.getFullYear();
                 var hr = d.getHours();
                 var mn = d.getMinutes();
                 var ss = d.getSeconds();
                 //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                 document.getElementById("lblret").value = "next"
                 document.getElementById("form1").submit();
             }
         }
         function getlast() {

             var cnt = document.getElementById("txtpgcnt").value;
             var pg = document.getElementById("txtpg").value;
             pg = parseInt(pg);
             cnt = parseInt(cnt)
             if (pg < cnt) {
                 var d = new Date();
                 var curr_date = d.getDate();
                 var curr_month = d.getMonth();
                 curr_month++;
                 var curr_year = d.getFullYear();
                 var hr = d.getHours();
                 var mn = d.getMinutes();
                 var ss = d.getSeconds();
                 //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                 document.getElementById("lblret").value = "last"
                 document.getElementById("form1").submit();
             }
         }
         function getprev() {

             var cnt = document.getElementById("txtpgcnt").value;
             var pg = document.getElementById("txtpg").value;
             pg = parseInt(pg);
             cnt = parseInt(cnt)
             if (pg > 1) {
                 var d = new Date();
                 var curr_date = d.getDate();
                 var curr_month = d.getMonth();
                 curr_month++;
                 var curr_year = d.getFullYear();
                 var hr = d.getHours();
                 var mn = d.getMinutes();
                 var ss = d.getSeconds();
                 //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                 document.getElementById("lblret").value = "prev"
                 document.getElementById("form1").submit();
             }
         }
         function getfirst() {

             var cnt = document.getElementById("txtpgcnt").value;
             var pg = document.getElementById("txtpg").value;
             pg = parseInt(pg);
             cnt = parseInt(cnt)
             if (pg > 1) {
                 var d = new Date();
                 var curr_date = d.getDate();
                 var curr_month = d.getMonth();
                 curr_month++;
                 var curr_year = d.getFullYear();
                 var hr = d.getHours();
                 var mn = d.getMinutes();
                 var ss = d.getSeconds();
                 //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                 document.getElementById("lblret").value = "first"
                 document.getElementById("form1").submit();
             }
         }
         function chksrch() {
             var srch = document.getElementById("txtsrch").value;
             if (srch != "") {
                 document.getElementById("lblret").value = "srch"
                 var d = new Date();
                 var curr_date = d.getDate();
                 var curr_month = d.getMonth();
                 curr_month++;
                 var curr_year = d.getFullYear();
                 var hr = d.getHours();
                 var mn = d.getMinutes();
                 var ss = d.getSeconds();
                 //document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
                 document.getElementById("form1").submit();
             }
         }
         //function gotoeq(eid) {
         //    window.parent.geteq(eid);
         //}
         //function gotofu(eid, fid) {
         //    window.parent.getfu(eid, fid);
         //}
         //function gotoco(cid, fid, eid) {
         //    window.parent.getco(eid, fid, cid);
         //} class="details"
     </script>
	</HEAD>
	<body  class="tbg">
		<form id="form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" style="LEFT: 0px; POSITION: absolute; TOP: 0px">
            <tr class="details">
                <td class="label"><asp:Label id="lang3181" runat="server">Search</asp:Label><asp:TextBox id="txtsrch" runat="server" CssClass="plainlabel" Width="170px"></asp:TextBox><img src="../images/appbuttons/minibuttons/srchsm1.gif" onclick="chksrch();"></td>
                </tr>
            <tr class="details">
					<td>
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst2.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev2.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 140px;" vAlign="middle" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext2.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast2.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
                
				<tr>
					<td id="tdarch" runat="server"></td>
				</tr>
			</table>
			<input type="hidden" id="lbldid" runat="server"> <input type="hidden" id="lblclid" runat="server">
			<input type="hidden" id="lblchk" runat="server"><input type="hidden" id="lblloc" runat="server">
			<input type="hidden" id="lblsid" runat="server">
            <input type="hidden" id="lbluser" runat="server" />
		<input id="txtpg" type="hidden" name="Hidden1" runat="server"><input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server">
			<input type="hidden" id="lblret" runat="server" NAME="lblret">
            <input type="hidden" id="lbltyp" runat="server" />
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
