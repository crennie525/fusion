﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqmasterselect.aspx.vb"
    Inherits="lucy_r12.eqmasterselect" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Master Equipment Selection</title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  type="text/javascript" src="../scripts1/EqSelectaspx.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script  type="text/javascript">
        function excel() {
            //document.getElementById("lblret").value = "excel"
            //document.getElementById("form1").submit();
            var eqstr;
            eqstr = document.getElementById("lblelist").value;
            window.open("../reports/eqmasterout.aspx?eqstr=" + eqstr + "&date=" + Date);
        }
        function DisableButton(b) {
            document.getElementById("btntocomp").className = "details";
            document.getElementById("btnfromcomp").className = "details";
            document.getElementById("todis").className = "view";
            document.getElementById("fromdis").className = "view";
            document.getElementById("form1").submit();
        }
        function handleexit() {
            //window.parent.handleexit();
        }
        function chkrb(who) {
            document.getElementById("lblret").value = "switch"
            document.getElementById("form1").submit();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="572">
    </table>
    <table width="572">
        <tr>
            <td class="thdrsingrt label" colspan="3">
                <asp:Label ID="lang3326" runat="server">Select Child Equipment</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr style="border-right: #0000ff 1px solid; border-top: #0000ff 1px solid; border-left: #0000ff 1px solid;
                    border-bottom: #0000ff 1px solid">
            </td>
        </tr>
        <tr>
        <td colspan="3">
        <table>
        <tr>
        <td class="label">Asset Class</td>
        <td class="plainlabel"><asp:DropDownList ID="ddac" runat="server" 
                AutoPostBack="True">
            </asp:DropDownList></td>
            <td class="plainlabel">Child Records&nbsp;&nbsp;<input type="radio" name="rbwho" id="rbcho" checked runat="server" onclick="chkrb('ch');" />
            &nbsp;&nbsp;Master Records&nbsp;&nbsp;<input type="radio" name="rbwho" id="rbmst" runat="server" onclick="chkrb('ms');" /></td>
        </tr>
        </table>
        </td>
         </tr>  
        <tr>
            <td class="label" align="center">
                <asp:Label ID="lang3327" runat="server">Available Equipment</asp:Label>
            </td>
            <td>
            </td>
            <td class="label" align="center">
                <asp:Label ID="lang3328" runat="server">Selected Equipment</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" width="280">
                <asp:ListBox ID="lbfailmaster" runat="server" Width="370px" SelectionMode="Multiple"
                    Height="300px"></asp:ListBox>
            </td>
            <td valign="middle" align="center" width="22">
                <img src="../images/appbuttons/minibuttons/forwardgraybg.gif" class="details" id="todis"
                    style="width: 20px" height="20">
                <img src="../images/appbuttons/minibuttons/backgraybg.gif" class="details" id="fromdis"
                    style="width: 20px" height="20">
                <asp:ImageButton ID="btntocomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/forwardgbg.gif">
                </asp:ImageButton>
                <asp:ImageButton ID="btnfromcomp" runat="server" ImageUrl="../images/appbuttons/minibuttons/backgbg.gif">
                </asp:ImageButton><img id="btnaddtosite" onmouseover="return overlib('Choose Failure Modes for this Plant Site ')"
                    onclick="getss();" onmouseout="return nd()" src="../images/appbuttons/minibuttons/plusminus.gif"
                    style="width: 20px" height="20" class="details">
            </td>
            <td align="center" width="280">
                <asp:ListBox ID="lbfailmodes" runat="server" Width="370px" SelectionMode="Multiple"
                    Height="300px"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">
                <img onclick="excel();" alt="" src="../images/appbuttons/bgbuttons/submit.gif"
                    id="ibtnret" runat="server" width="69" height="19">
            </td>
        </tr>
        <tr>
            <td class="bluelabel" colspan="3">
                <asp:Label ID="lang3329" runat="server">List Box Options</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="label" colspan="3">
                <asp:RadioButtonList ID="cbopts" runat="server" Width="400px" AutoPostBack="True"
                    CssClass="labellt">
                    <asp:ListItem Value="0" Selected="True">Multi-Select (use arrows to move single or multiple selections)</asp:ListItem>
                    <asp:ListItem Value="1">Click-Once (move selected item by clicking that item)</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="note" colspan="3">
                <asp:Label ID="lang3330" runat="server">* Please note that the Multi-Select Mode must be used to remove an item from the Available Equipment list if only one item is present.</asp:Label>
            </td>
        </tr>
    </table>
    <div>
        <asp:DataGrid ID="dgout" runat="server" ShowFooter="True">
        </asp:DataGrid>
    </div>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblelist" runat="server" />
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblacid" runat="server" />
    </form>
</body>
</html>
