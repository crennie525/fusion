﻿Imports System.Data.SqlClient
Public Class eqcopy3test
    Inherits System.Web.UI.Page
    Dim dr As SqlDataReader
    Dim sql As String
    Dim copy As New Utilities
    Dim tmod As New transmod
    Dim cid, sid, did, clid, neweq As String
    Dim num, cnt As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lbleq.Value = "1186"
            lbllid.Value = ""
            lblsid.Value = "12"
            lbloldsid.Value = "12"
            lbldid.Value = "16"
            lblclid.Value = "53"
        Else

        End If
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sta, stp, tim As String
        sta = Now
        Dim tot As Integer
        Dim eq, pm, tpm, pmf, tpmf, rand As Integer
        num = txtnum.Text
        sql = "select count(*) from equipment where eqnum like 'xxxf%'"
        copy.Open()
        cnt = copy.strScalar(sql)
        'If cnt = 0 Then
        'cnt = 1
        'End If
        Dim who As Integer = 0
        While who < num

            cnt += 1
            neweq = "xxxf" & cnt
            If who > 0 Then
                sql = "declare @maxRandomValue int, @minRandomValue int, @rand int, @eqid int, @cnt int; set @cnt = 0 " _
                    + "set @minRandomValue = (select min(eqid) from equipment) " _
                    + "set @maxRandomValue = (select max(eqid) from equipment) " _
                    + "set @rand = (Select Cast(((@maxRandomValue + 1) - @minRandomValue) * Rand() + @minRandomValue As int) As 'randomNumber') " _
                    + "while @cnt = 0 " _
                    + "begin " _
                    + "set @cnt = (select count(*) from equipment where eqid = @rand) " _
                    + "if @cnt = 0 " _
                    + "begin " _
                    + "set @rand = (Select Cast(((@maxRandomValue + 1) - @minRandomValue) * Rand() + @minRandomValue As int) As 'randomNumber') " _
                    + "end " _
                    + "end " _
                    + "select @rand"
                rand = copy.strScalar(sql)
                lbleq.Value = rand
            End If

            CheckCopy(neweq)
            who += 1

        End While
        sql = "select count(*) from equipment where eqnum like 'xxx%'"
        tot = copy.strScalar(sql)
        sql = "select count(*) from equipment"
        eq = copy.strScalar(sql)
        sql = "select count(*) from pmtasks"
        pm = copy.strScalar(sql)
        sql = "select count(*) from pmtaskstpm"
        tpm = copy.strScalar(sql)
        sql = "select count(*) from pmtaskfailmodes"
        pmf = copy.strScalar(sql)
        sql = "select count(*) from pmtaskfailmodestpm"
        tpmf = copy.strScalar(sql)
        stp = Now
        sql = "select datediff(mi, Cast('" & sta & "' as datetime), Cast('" & stp & "' as datetime))"
        tim = copy.strScalar(sql)
        copy.Dispose()
        tdmsg.InnerHtml = "Current Count: " & cnt
        tdmsg1.InnerHtml = "Total Count: " & tot
        td1.InnerHtml = "Equipment Count: " & eq
        td2.InnerHtml = "PM Task Count: " & pm
        td3.InnerHtml = "TPM Task Count: " & tpm
        td4.InnerHtml = "PM Fail Count: " & pmf
        td5.InnerHtml = "TPM Fail Count: " & tpmf
        tdmsg2.InnerHtml = sta
        tdmsg3.InnerHtml = stp
        td6.InnerHtml = "Time: " & tim
        Dim strMessage1 As String = "Done!"
        Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
    End Sub
    Private Sub CheckCopy(ByVal neweq As String)

        neweq = Replace(neweq, "'", Chr(180), , , vbTextCompare)
        neweq = Replace(neweq, "--", "-", , , vbTextCompare)
        neweq = Replace(neweq, ";", ":", , , vbTextCompare)
        If Len(neweq) > 0 Then
            If Len(neweq) > 50 Then
                Dim strMessage1 As String = tmod.getmsg("cdstr877", "eqcopy2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage1, "strKey1")
                Exit Sub
            End If
            cid = "0" ' lblcid.value
            Dim eqcnt As Integer = 0
            Dim mdb As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
            Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
            '*** Multi Add ***

            Dim cmd0 As New SqlCommand("select count(*) from [" & srvr & "].[" & orig & "].[dbo].[equipment] where eqnum = @neweq")
            Dim param1 = New SqlParameter("@srvr", SqlDbType.VarChar)
            param1.Value = srvr
            cmd0.Parameters.Add(param1)
            Dim param2 = New SqlParameter("@mdb", SqlDbType.VarChar)
            param2.Value = mdb
            cmd0.Parameters.Add(param2)
            'Dim param3 = New SqlParameter("@cid", SqlDbType.Int)
            'param3.Value = cid
            'cmd0.Parameters.Add(param3)
            Dim param4 = New SqlParameter("@neweq", SqlDbType.VarChar)
            param4.Value = neweq
            cmd0.Parameters.Add(param4)
            'copy.Open()
            Dim copy1 As New Utilities
            Try
                copy1.Open()
                eqcnt = copy1.ScalarHack(cmd0)
                'copy1.Dispose()
            Catch ex As Exception
                Dim strMessage2 As String = ex.Message
                strMessage2 = copy.ModString1(strMessage2)
                strMessage2 = "Ref-643 - " + strMessage2
                If lblstrmsg.Value = "" Then
                    lblstrmsg.Value = strMessage2
                Else
                    lblstrmsg.Value += "\n\n" & strMessage2
                End If
                strMessage2 += "\n\n Copy Cancelled"
                Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                Try
                    copy1.Dispose()
                Catch ex1 As Exception

                End Try
                Exit Sub
            End Try

            'eqcnt = copy.Scalar(sql)
            Dim lid As String = lbllid.Value
            If eqcnt = 0 And lid <> "" And lid <> "0" Then
                sql = "select count(*) from pmlocations where location = '" & neweq & "'"
                Try
                    eqcnt = copy1.Scalar(sql)
                    copy1.Dispose()
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-669 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                    strMessage2 += "\n\n Copy Cancelled"
                    Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                    Try
                        copy1.Dispose()
                    Catch ex1 As Exception

                    End Try
                    Exit Sub
                End Try

            Else
                Try
                    copy1.Dispose()
                Catch ex1 As Exception

                End Try
            End If
            If eqcnt = 0 Then
                'txtneweqnum.Enabled = False
                'lblcopy.Value = "yes"
                'ibtncopy.Visible = False
                'ibtncancel.Visible = True
                'Dim flg As String = lbldflt.Value
                'If flg = "0" Then
                'tblchoosedept.Attributes.Add("class", "view")
                'tblrevdetails.Attributes.Add("class", "details")
                'Else
                'Try

                'Try
                DoCopy(neweq)
                'Catch ex As Exception
                'Dim strMessage2 As String = ex.Message
                'strMessage2 = Replace(strMessage2, "'", "", , , vbTextCompare) '"Problem Saving Record\nPlease review selections and try again."
                'strMessage2 = Replace(strMessage2, """", "", , , vbTextCompare)
                'lblstrmsg.Value = strMessage2
                ''Utilities.CreateMessageAlert2(Me, strMessage2, "strKey1")
                'End Try

                'Catch ex As Exception
                'Dim strMessage2 As String =  tmod.getmsg("cdstr878" , "eqcopy2.aspx.vb")

                'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                'End Try
                ''copy.Dispose()
            Else
                'copy.Dispose()
                Dim strMessage3 As String = tmod.getmsg("cdstr879", "eqcopy2.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage3, "strKey1")
            End If
        Else
            'copy.Dispose()
            Dim strMessage4 As String = tmod.getmsg("cdstr880", "eqcopy2.aspx.vb")

            Utilities.CreateMessageAlert(Me, strMessage4, "strKey1")
        End If
    End Sub
    Private Sub DoCopy(ByVal neweq As String)
        Dim multiserver As String
        Try
            multiserver = System.Configuration.ConfigurationManager.AppSettings("multiserver")
        Catch ex As Exception
            multiserver = "no"
        End Try

        Dim li As ListItem
        Dim listr As String
        Dim pm, tpm As String
        Dim tflg As Integer = 0
        If cbfunc.Checked = True Then
            listr = "f"
            If cbcomp.Checked = True Then
                listr += "c"
                If cbpm.Checked = True Then
                    listr += "t"
                    tflg = 1
                    pm = "Y"
                Else
                    pm = "N"
                End If
                If cbtpm.Checked = True Then
                    If tflg = 0 Then
                        listr += "t"
                    End If
                    tpm = "Y"
                Else
                    tpm = "N"
                End If
            End If
        End If
        Dim eqid As String = lbleq.Value
        'Dim neweq As String = txtneweqnum.Text
        neweq = Replace(neweq, "'", Chr(180), , , vbTextCompare)
        'neweq = Replace(neweq, " ", " ", , , vbTextCompare)
        neweq = Replace(neweq, ";", " ", , , vbTextCompare)
        neweq = Replace(neweq, "/", " ", , , vbTextCompare)
        neweq = Replace(neweq, "\", " ", , , vbTextCompare)
        neweq = Replace(neweq, "&", " ", , , vbTextCompare)
        neweq = Replace(neweq, "#", " ", , , vbTextCompare)
        neweq = Replace(neweq, "$", " ", , , vbTextCompare)
        neweq = Replace(neweq, "%", " ", , , vbTextCompare)
        neweq = Replace(neweq, "!", " ", , , vbTextCompare)
        neweq = Replace(neweq, "~", " ", , , vbTextCompare)
        neweq = Replace(neweq, "^", " ", , , vbTextCompare)
        neweq = Replace(neweq, "*", " ", , , vbTextCompare)
        'neweq = Replace(neweq, ".", " ", , , vbTextCompare)
        cid = "0" ' lblcid.value
        Dim site, dept, cell As String
        site = lblsid.Value
        dept = lbldid.Value
        cell = lblclid.Value
        If cell = "" Then
            cell = "0"
        End If
        sid = lblsid.Value
        Dim osid As String = lbloldsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        If cell = "none" Then
            cell = "0"
        End If
        Dim newid As Integer
        Dim user As String = "PM Administrator"
        Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
        Dim mdb As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")

        Dim ap As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
        Dim srvr As String = System.Configuration.ConfigurationManager.AppSettings("source")
        Dim lid As String = lbllid.Value
        Dim orig As String = System.Configuration.ConfigurationManager.AppSettings("custAppDB")
        Dim ecryn As String
        If cbecryn.Checked = True Then
            ecryn = "Y"
        Else
            ecryn = "N"
        End If
        Dim roa As String
        If rbro.Checked = True Then
            roa = "R"
        ElseIf rbai.Checked = True Then
            roa = "A"
        End If
        Dim rat As String
        If cbrat.Checked = True Then
            rat = "Y"
        Else
            rat = "N"
        End If
        sql = "exec usp_copyEqMDB2 '" + cid + "','" + site + "','" + dept + "','" + cell + "','" + eqid + "','" + neweq + "','" + ustr + "','" + mdb + "','" + orig + "','" + srvr + "','" + ecryn + "','" + lid + "'"
        Dim mdbtst As String = sql

        Dim cmd0 As New SqlCommand("exec usp_copyEqMDB2 @cid, @site, @dept, @cell, @eqid, @neweq, @ustr, @mdb, @orig, @srvr, @ecryn, @loc")
        Dim param1 = New SqlParameter("@cid", SqlDbType.Int)
        param1.Value = cid
        cmd0.Parameters.Add(param1)
        Dim param2 = New SqlParameter("@site", SqlDbType.Int)
        param2.Value = site
        cmd0.Parameters.Add(param2)
        Dim param3 = New SqlParameter("@dept", SqlDbType.VarChar)
        param3.Value = dept
        cmd0.Parameters.Add(param3)
        Dim param4 = New SqlParameter("@cell", SqlDbType.VarChar)
        param4.Value = cell
        cmd0.Parameters.Add(param4)
        Dim param5 = New SqlParameter("@eqid", SqlDbType.Int)
        param5.Value = eqid
        cmd0.Parameters.Add(param5)
        Dim param6 = New SqlParameter("@neweq", SqlDbType.VarChar)
        param6.Value = neweq
        cmd0.Parameters.Add(param6)
        Dim param7 = New SqlParameter("@ustr", SqlDbType.VarChar)
        param7.Value = ustr
        cmd0.Parameters.Add(param7)
        Dim param8 = New SqlParameter("@mdb", SqlDbType.VarChar)
        param8.Value = mdb
        cmd0.Parameters.Add(param8)
        Dim param9 = New SqlParameter("@orig", SqlDbType.VarChar)
        param9.Value = orig
        cmd0.Parameters.Add(param9)
        Dim param10 = New SqlParameter("@srvr", SqlDbType.VarChar)
        param10.Value = srvr
        cmd0.Parameters.Add(param10)
        Dim param11 = New SqlParameter("@ecryn", SqlDbType.VarChar)
        param11.Value = ecryn
        cmd0.Parameters.Add(param11)
        Dim param12 = New SqlParameter("@loc", SqlDbType.VarChar)
        param12.Value = lid
        cmd0.Parameters.Add(param12)
        Dim copy2 As New Utilities
        Try
            copy2.Open()
            newid = copy2.ScalarHack(cmd0)
            lblnewid.Value = newid
            copy2.Dispose()
        Catch ex As Exception
            Dim strMessage2 As String = ex.Message
            strMessage2 = copy.ModString1(strMessage2)
            strMessage2 = "Ref-873 - " + strMessage2
            If lblstrmsg.Value = "" Then
                lblstrmsg.Value = strMessage2
            Else
                lblstrmsg.Value += "\n\n" & strMessage2
            End If
            strMessage2 += "\n\n Copy Cancelled"
            Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
            Try
                copy2.Dispose()
            Catch ex1 As Exception

            End Try
            lblflag.Value = "1"
            Exit Sub
        End Try

        'need total time copy here
        If mdb <> orig Then
            sql = "insert into [" & srvr & "].[" & mdb & "].[dbo].pmtottime (eqid, skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, maxdays, meter, meterfreq, meterid, meterunit) " _
            + "select  '" & newid & "', skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, maxdays, meter, meterfreq, meterid, meterunit " _
            + "from [" & srvr & "].[" & orig & "].[dbo].pmtottime where eqid = '" & eqid & "'"
        Else
            sql = "insert into pmtottime (eqid, skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, maxdays, meter, meterfreq, meterid, meterunit) " _
            + "select  '" & newid & "', skillid, skill, skillqty, freqid, freq, rdid, rd, tottime, downtime, ismeter, maxdays, meter, meterfreq, meterid, meterunit " _
            + "from pmtottime where eqid = '" & eqid & "'"
        End If

        Dim copy2a As New Utilities
        Try
            copy2a.Open()
            copy2a.Update(sql)
            copy2a.Dispose()
        Catch ex As Exception

        End Try


        Dim copy3 As New Utilities
        sql = "usp_copyEqPicsMDB2 '" & eqid & "', '" & newid & "', '" & mdb & "', '" & orig & "','" & srvr & "'"

        Dim oloc As String = System.Configuration.ConfigurationManager.AppSettings("custAppName") 'lbloloc.Value
        Dim cloc As String = ap
        Dim pid, t, tn, tm, ts, td, tns, tnd, tms, tmd, nt, ntn, ntm, ont, ontn, ontm As String
        If mdb <> orig Then
            Try
                Dim ds As New DataSet
                Try
                    copy3.Open()
                    ds = copy3.GetDSData(sql)
                    copy3.Dispose()
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                    strMessage2 = "Ref-911 - " + strMessage2
                    'no exit here
                    'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                End Try
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")

                    nt = "a-eqImg" & newid & "i"
                    ntn = "atn-eqImg" & newid & "i"
                    ntm = "atm-eqImg" & newid & "i"

                    ont = "a-eqImg" & eqid & "i"
                    ontn = "atn-eqImg" & eqid & "i"
                    ontm = "atm-eqImg" & eqid & "i"

                    'from
                    ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    'to
                    td = Server.MapPath("\") & cloc & t
                    td = Replace(td, "\", "/")

                    tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                    tns = Replace(tns, "\", "/")
                    tnd = Server.MapPath("\") & cloc & tn
                    tnd = Replace(tnd, "\", "/")

                    tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & cloc & tm
                    tmd = Replace(tmd, "\", "/")

                    If multiserver = "yes" Then

                        ts = Replace(ts, "wwwlai1", "wwwlai2")
                        tns = Replace(ts, "wwwlai1", "wwwlai2")
                        tms = Replace(ts, "wwwlai1", "wwwlai2")
                    End If


                    System.IO.File.Copy(ts, td, True)
                    System.IO.File.Copy(tns, tnd, True)
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception
                Dim strMessage2 As String = ex.Message
                strMessage2 = copy.ModString1(strMessage2)
                strMessage2 = "Ref-964 - " + strMessage2
                If lblstrmsg.Value = "" Then
                    lblstrmsg.Value = strMessage2
                Else
                    lblstrmsg.Value += "\n\n" & strMessage2
                End If
            End Try
        Else
            Try
                Dim ds As New DataSet
                Try
                    copy3.Open()
                    ds = copy3.GetDSData(sql)
                    copy3.Dispose()
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-981 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                    'no exit here
                    'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                End Try

                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    pid = ds.Tables(0).Rows(i)("pic_id").ToString
                    t = ds.Tables(0).Rows(i)("picurl").ToString
                    tn = ds.Tables(0).Rows(i)("picurltn").ToString
                    tm = ds.Tables(0).Rows(i)("picurltm").ToString
                    t = Replace(t, "..", "")
                    tn = Replace(tn, "..", "")
                    tm = Replace(tm, "..", "")

                    nt = "a-eqImg" & newid & "i"
                    ntn = "atn-eqImg" & newid & "i"
                    ntm = "atm-eqImg" & newid & "i"

                    ont = "a-eqImg" & eqid & "i"
                    ontn = "atn-eqImg" & eqid & "i"
                    ontm = "atm-eqImg" & eqid & "i"


                    ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                    ts = Replace(ts, "\", "/")
                    td = Server.MapPath("\") & ap & t
                    td = Replace(td, "\", "/")

                    tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                    tns = Replace(tns, "\", "/")
                    tnd = Server.MapPath("\") & ap & tn
                    tnd = Replace(tnd, "\", "/")

                    tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                    tms = Replace(tms, "\", "/")
                    tmd = Server.MapPath("\") & ap & tm
                    tmd = Replace(tmd, "\", "/")

                    System.IO.File.Copy(ts, td, True)
                    System.IO.File.Copy(tns, tnd, True)
                    System.IO.File.Copy(tms, tmd, True)
                Next
            Catch ex As Exception
                Dim strMessage2 As String = ex.Message
                strMessage2 = copy.ModString1(strMessage2)
                strMessage2 = "Ref-1033 - " + strMessage2
                If lblstrmsg.Value = "" Then
                    lblstrmsg.Value = strMessage2
                Else
                    lblstrmsg.Value += "\n\n" & strMessage2
                End If
            End Try
        End If

        Dim flg As String

        If listr = "fct" Then
            flg = "1"
            Dim copy4 As New Utilities
            sql = "usp_copyAllMDB2 '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & ustr & "'"
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    Try
                        copy4.Open()
                        ds = copy4.GetDSData(sql)
                        copy4.Dispose()
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        strMessage2 = "Ref-1058 - " + strMessage2
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                        strMessage2 += "\n\n Copy Cancelled"
                        lblflag.Value = "1"
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                        Exit Sub
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")

                        If multiserver = "yes" Then
                            ts = Replace(ts, "wwwlai1", "wwwlai2")
                            tns = Replace(ts, "wwwlai1", "wwwlai2")
                            tms = Replace(ts, "wwwlai1", "wwwlai2")
                        End If

                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1137 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try
                'insert pm task image file copy here for mdb *********************************************

            Else
                Try
                    Dim ds As New DataSet
                    Try
                        copy4.Open()
                        ds = copy4.GetDSData(sql)
                        copy4.Dispose()
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        strMessage2 = "Ref-1156 - " + strMessage2
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                        strMessage2 += "\n\n Copy Cancelled"
                        lblflag.Value = "1"
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                        Exit Sub
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")

                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)

                    Next
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1230 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try
                'insert pm task image file copy here for local
                Try
                    Dim copy5 As New Utilities
                    sql = "select pic_id, parfuncid, funcid, oldfuncid, pm_image, pm_image_med, pm_image_thumb from pmimages where funcid in " _
                    + "(select func_id from functions where eqid = '" & newid & "')"
                    Dim dsp As New DataSet
                    Try
                        copy5.Open()
                        dsp = copy5.GetDSData(sql)
                        copy5.Dispose()
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        strMessage2 = "Ref-1250 - " + strMessage2
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                        'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                    End Try

                    Dim fid1, pfid1 As String
                    oloc = System.Configuration.ConfigurationManager.AppSettings("custAppName") 'lbloloc.Value
                    Dim i1 As Integer
                    Dim f1 As Integer = 0
                    Dim c1 As Integer = 0
                    Dim ot, otn, otm As String
                    Dim pict, pictn, pictm As String
                    Dim opict, opictn, opictm As String
                    Dim x1 As Integer = dsp.Tables(0).Rows.Count
                    Dim ofid1 As String
                    Dim newt, newtn, newtm

                    For i1 = 0 To (x1 - 1)
                        pid = dsp.Tables(0).Rows(i1)("pic_id").ToString
                        fid1 = dsp.Tables(0).Rows(i1)("funcid").ToString
                        ofid1 = dsp.Tables(0).Rows(i1)("oldfuncid").ToString
                        pfid1 = dsp.Tables(0).Rows(i1)("parfuncid").ToString
                        t = dsp.Tables(0).Rows(i1)("pm_image").ToString
                        tn = dsp.Tables(0).Rows(i1)("pm_image_thumb").ToString
                        tm = dsp.Tables(0).Rows(i1)("pm_image_med").ToString
                        Dim intFileNameLength As Integer
                        intFileNameLength = InStr(1, StrReverse(t), "/")
                        t = Mid(t, (Len(t) - intFileNameLength) + 2)
                        intFileNameLength = InStr(1, StrReverse(tn), "/")
                        tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                        intFileNameLength = InStr(1, StrReverse(tm), "/")
                        tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                        Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                        t = appstr & "/pmimages/" & t
                        tn = appstr & "/pmimages/" & tn
                        tm = appstr & "/pmimages/" & tm

                        t = Replace(t, "http://localhost/", "")
                        tn = Replace(tn, "http://localhost/", "")
                        tm = Replace(tm, "http://localhost/", "")

                        t = Replace(t, "http://", "")
                        tn = Replace(tn, "http://", "")
                        tm = Replace(tm, "http://", "")

                        ts = Replace(t, "g" & fid1 & "-", "g" & ofid1 & "-")
                        tns = Replace(tn, "g" & fid1 & "-", "g" & ofid1 & "-")
                        tms = Replace(tm, "g" & fid1 & "-", "g" & ofid1 & "-")

                        td = Replace(t, "g" & fid1 & "-", "g" & fid1 & "-")
                        tnd = Replace(tn, "g" & fid1 & "-", "g" & fid1 & "-")
                        tmd = Replace(tm, "g" & fid1 & "-", "g" & fid1 & "-")


                        If oloc <> "" Then
                            ts = Replace(ts, ap, oloc)
                            tns = Replace(tns, ap, oloc)
                            tms = Replace(tms, ap, oloc)

                            td = Replace(td, ap, oloc)
                            tnd = Replace(tnd, ap, oloc)
                            tmd = Replace(tmd, ap, oloc)
                        End If


                        ts = Server.MapPath("\") & ts
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & td
                        td = Replace(td, "\", "/")


                        tns = Server.MapPath("\") & tns
                        tns = Replace(tns, "\", "/")
                        tnd = Server.MapPath("\") & tnd
                        tnd = Replace(tnd, "\", "/")


                        tms = Server.MapPath("\") & tms
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & tmd
                        tmd = Replace(tmd, "\", "/")

                        'System.IO.File.Copy(source, dest)
                        System.IO.File.Copy(ts, td, True)
                        System.IO.File.Copy(tns, tnd, True)
                        System.IO.File.Copy(tms, tmd, True)


                    Next

                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1336 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try

            End If
            'rbro, rbai

            '2MDB includes switch for Revised or As is
            Dim copy6 As New Utilities
            sql = "usp_copyEqTasks2MDB '" & cid & "', '" & sid & "', '" & did & "', '" & clid & "', '" & newid & "', '" & ustr & "', '" & mdb & "', '" & orig & "','" & pm & "','" & tpm & "', '" & ap & "','" & oloc & "','" & srvr & "','" & roa & "','" & rat & "'"
            Try
                copy6.Open()
                copy6.Update(sql)
                copy6.Dispose()
            Catch ex As Exception
                Dim strMessage2 As String = ex.Message
                strMessage2 = copy.ModString1(strMessage2)
                strMessage2 = "Ref-1357 - " + strMessage2
                If lblstrmsg.Value = "" Then
                    lblstrmsg.Value = strMessage2
                Else
                    lblstrmsg.Value += "\n\n" & strMessage2
                End If
                lblflag.Value = "1"
                strMessage2 += "\n\n Copy Cancelled"
                Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                Exit Sub
            End Try

            'need to put pm task images here !!!Did this with function call above

            '***Added to speed up result time
            If sid <> osid Then
                Dim copy7 As New Utilities
                sql = "usp_updateSkillsNew '" & sid & "','" & newid & "'"
                Try
                    copy7.Open()
                    copy7.Update(sql)
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1380 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                    'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                End Try

                Dim fsi As Integer
                Try
                    sql = "select count(*) from pmsitefm where siteid = '" & sid & "'"
                    fsi = copy7.Scalar(sql)
                    If fsi > 0 Then
                        sql = "usp_updateSiteFM '" & cid & "', '" & sid & "'"
                        copy7.Update(sql)
                    End If
                    copy7.Dispose()
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1401 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                    'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                End Try
            End If
            Dim copy8 As New Utilities
            sql = "iusp_updateCompFailId '" & newid & "'"
            Try
                copy8.Open()
                copy8.Update(sql)
                copy8.Dispose()
            Catch ex As Exception
                Dim strMessage2 As String = ex.Message
                strMessage2 = copy.ModString1(strMessage2)
                strMessage2 = "Ref-1419 - " + strMessage2
                If lblstrmsg.Value = "" Then
                    lblstrmsg.Value = strMessage2
                Else
                    lblstrmsg.Value += "\n\n" & strMessage2
                End If
                'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
            End Try


            '***

            If tpm = "Y" Then
                Dim copy9 As New Utilities
                sql = "select parfuncid, funcid, tpm_image, tpm_image_med, tpm_image_thumb from tpmimages where funcid in " _
                + "(select func_id from functions where eqid = '" & newid & "')"
                If mdb <> orig Then
                    Try
                        Dim ds As New DataSet
                        Try
                            copy9.Open()
                            ds = copy9.GetDSData(sql)
                            copy9.Dispose()
                        Catch ex As Exception
                            Dim strMessage2 As String = ex.Message
                            strMessage2 = copy.ModString1(strMessage2)
                            strMessage2 = "Ref-1445 - " + strMessage2
                            If lblstrmsg.Value = "" Then
                                lblstrmsg.Value = strMessage2
                            Else
                                lblstrmsg.Value += "\n\n" & strMessage2
                            End If
                            'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                        End Try

                        Dim fid, pfid As String
                        oloc = System.Configuration.ConfigurationManager.AppSettings("custAppName") 'lbloloc.Value
                        Dim i As Integer
                        Dim f As Integer = 0
                        Dim c As Integer = 0
                        Dim ot, otn, otm As String
                        Dim pict, pictn, pictm As String
                        Dim opict, opictn, opictm As String
                        Dim x As Integer = ds.Tables(0).Rows.Count
                        For i = 0 To (x - 1)
                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                            pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                            t = ds.Tables(0).Rows(i)("tpm_image").ToString
                            tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                            tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                            Dim intFileNameLength As Integer
                            intFileNameLength = InStr(1, StrReverse(t), "/")
                            t = Mid(t, (Len(t) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tn), "/")
                            tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tm), "/")
                            tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                            t = appstr & "/tpmimages/" & t
                            tn = appstr & "/tpmimages/" & tn
                            tm = appstr & "/tpmimages/" & tm

                            t = Replace(t, "http://localhost/", "")
                            tn = Replace(tn, "http://localhost/", "")
                            tm = Replace(tm, "http://localhost/", "")

                            t = Replace(t, "http://", "")
                            tn = Replace(tn, "http://", "")
                            tm = Replace(tm, "http://", "")

                            pict = "a-newsImg" + fid
                            pictn = "atn-newsImg" + fid
                            pictm = "atm-newsImg" + fid

                            opict = "a-newsImg" + pfid
                            opictn = "atn-newsImg" + pfid
                            opictm = "atm-newsImg" + pfid

                            ts = Replace(t, pict, opict)
                            tns = Replace(tn, pictn, opictn)
                            tms = Replace(tm, pictm, opictm)

                            If oloc <> "" Then
                                ts = Replace(t, ap, oloc)
                                tns = Replace(tn, ap, oloc)
                                tms = Replace(tm, ap, oloc)
                            End If

                            ts = Server.MapPath("\") & ts
                            ts = Replace(ts, "\", "/")
                            td = Server.MapPath("\") & t
                            td = Replace(td, "\", "/")


                            tns = Server.MapPath("\") & tns
                            tns = Replace(tns, "\", "/")
                            tnd = Server.MapPath("\") & tn
                            tnd = Replace(tnd, "\", "/")


                            tms = Server.MapPath("\") & tms
                            tms = Replace(tms, "\", "/")
                            tmd = Server.MapPath("\") & tm
                            tmd = Replace(tmd, "\", "/")

                            System.IO.File.Copy(ts, td, True)
                            System.IO.File.Copy(tns, tnd, True)
                            System.IO.File.Copy(tms, tmd, True)


                        Next
                    Catch ex As Exception

                    End Try
                Else
                    Try
                        Dim ds As New DataSet
                        Try
                            copy9.Open()
                            ds = copy9.GetDSData(sql)
                            copy9.Dispose()
                        Catch ex As Exception
                            Dim strMessage2 As String = ex.Message
                            strMessage2 = copy.ModString1(strMessage2)
                            strMessage2 = "Ref-1533 - " + strMessage2
                            If lblstrmsg.Value = "" Then
                                lblstrmsg.Value = strMessage2
                            Else
                                lblstrmsg.Value += "\n\n" & strMessage2
                            End If
                            'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                        End Try

                        Dim fid, pfid As String
                        Dim i As Integer
                        Dim f As Integer = 0
                        Dim c As Integer = 0
                        Dim ot, otn, otm As String
                        Dim pict, pictn, pictm As String
                        Dim opict, opictn, opictm As String
                        Dim x As Integer = ds.Tables(0).Rows.Count
                        For i = 0 To (x - 1)
                            pid = ds.Tables(0).Rows(i)("pic_id").ToString
                            fid = ds.Tables(0).Rows(i)("funcid").ToString
                            pfid = ds.Tables(0).Rows(i)("parfuncid").ToString
                            t = ds.Tables(0).Rows(i)("tpm_image").ToString
                            tn = ds.Tables(0).Rows(i)("tpm_image_thumb").ToString
                            tm = ds.Tables(0).Rows(i)("tpm_image_med").ToString
                            Dim intFileNameLength As Integer
                            intFileNameLength = InStr(1, StrReverse(t), "/")
                            t = Mid(t, (Len(t) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tn), "/")
                            tn = Mid(tn, (Len(tn) - intFileNameLength) + 2)
                            intFileNameLength = InStr(1, StrReverse(tm), "/")
                            tm = Mid(tm, (Len(tm) - intFileNameLength) + 2)
                            Dim appstr As String = System.Configuration.ConfigurationManager.AppSettings("custAppName")
                            t = appstr & "/tpmimages/" & t
                            tn = appstr & "/tpmimages/" & tn
                            tm = appstr & "/tpmimages/" & tm

                            t = Replace(t, "http://localhost/", "")
                            tn = Replace(tn, "http://localhost/", "")
                            tm = Replace(tm, "http://localhost/", "")

                            t = Replace(t, "http://", "")
                            tn = Replace(tn, "http://", "")
                            tm = Replace(tm, "http://", "")

                            pict = "a-newsImg" + fid
                            pictn = "atn-newsImg" + fid
                            pictm = "atm-newsImg" + fid

                            opict = "a-newsImg" + pfid
                            opictn = "atn-newsImg" + pfid
                            opictm = "atm-newsImg" + pfid

                            ts = Replace(t, pict, opict)
                            tns = Replace(tn, pictn, opictn)
                            tms = Replace(tm, pictm, opictm)

                            ts = Server.MapPath("\") & ts
                            ts = Replace(ts, "\", "/")
                            td = Server.MapPath("\") & t
                            td = Replace(td, "\", "/")
                            System.IO.File.Copy(ts, td, True)

                            tns = Server.MapPath("\") & tns
                            tns = Replace(tns, "\", "/")
                            tnd = Server.MapPath("\") & tn
                            tnd = Replace(tnd, "\", "/")
                            System.IO.File.Copy(tns, tnd, True)

                            tms = Server.MapPath("\") & tms
                            tms = Replace(tms, "\", "/")
                            tmd = Server.MapPath("\") & tm
                            tmd = Replace(tmd, "\", "/")
                            System.IO.File.Copy(tms, tmd, True)
                        Next
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        strMessage2 = "Ref-1599 - " + strMessage2
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                    End Try
                End If

            End If
        ElseIf listr = "fc" Then
            Dim copy10 As New Utilities
            flg = "1"
            sql = "usp_copyAllMDB '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & ustr & "'"
            Dim tst111 As String = sql
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    Try
                        copy10.Open()
                        ds = copy10.GetDSData(sql)
                        copy10.Dispose()
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        strMessage2 = "Ref-1625 - " + strMessage2
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                        lblflag.Value = "1"
                        strMessage2 += "\n\n Copy Cancelled"
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                        Exit Sub
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1649 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try
            Else
                Try
                    Dim ds As New DataSet
                    Try
                        copy10.Open()
                        ds = copy10.GetDSData(sql)
                        copy10.Dispose()
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        strMessage2 = "Ref-1712 - " + strMessage2
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                        lblflag.Value = "1"
                        strMessage2 += "\n\n Copy Cancelled"
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                        Exit Sub
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1782 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try
            End If
        ElseIf listr = "f" Then
            Dim copy11 As New Utilities
            flg = "0"
            'sql = "usp_copyAll '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "'"
            sql = "usp_copyAllMDB '" & cid & "', '" & eqid & "', '" & newid & "', '" & flg & "', '" & mdb & "', '" & orig & "','" & srvr & "','" & ustr & "'"
            'copy.Update(sql)
            If mdb <> orig Then
                Try
                    Dim ds As New DataSet
                    Try
                        copy11.Open()
                        ds = copy11.GetDSData(sql)
                        copy11.Dispose()
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        strMessage2 = "Ref-1806 - " + strMessage2
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                        lblflag.Value = "1"
                        strMessage2 += "\n\n Copy Cancelled"
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                        Exit Sub
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & oloc & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & cloc & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & oloc & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & cloc & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & oloc & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & cloc & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1876 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try
            Else
                Try
                    Dim ds As New DataSet
                    Try
                        copy11.Open()
                        ds = copy11.GetDSData(sql)
                        copy11.Dispose()
                    Catch ex As Exception
                        Dim strMessage2 As String = ex.Message
                        strMessage2 = copy.ModString1(strMessage2)
                        strMessage2 = "Ref-1893 - " + strMessage2
                        If lblstrmsg.Value = "" Then
                            lblstrmsg.Value = strMessage2
                        Else
                            lblstrmsg.Value += "\n\n" & strMessage2
                        End If
                        lblflag.Value = "1"
                        strMessage2 += "\n\n Copy Cancelled"
                        Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
                        Exit Sub
                    End Try

                    Dim fid, cmid, ofid, ocmid As String
                    Dim i As Integer
                    Dim f As Integer = 0
                    Dim c As Integer = 0
                    Dim x As Integer = ds.Tables(0).Rows.Count
                    For i = 0 To (x - 1)
                        pid = ds.Tables(0).Rows(i)("pic_id").ToString
                        fid = ds.Tables(0).Rows(i)("funcid").ToString
                        ofid = ds.Tables(0).Rows(i)("parfuncid").ToString
                        cmid = ds.Tables(0).Rows(i)("comid").ToString
                        ocmid = ds.Tables(0).Rows(i)("parcomid").ToString
                        t = ds.Tables(0).Rows(i)("picurl").ToString
                        tn = ds.Tables(0).Rows(i)("picurltn").ToString
                        tm = ds.Tables(0).Rows(i)("picurltm").ToString
                        t = Replace(t, "..", "")
                        tn = Replace(tn, "..", "")
                        tm = Replace(tm, "..", "")
                        If cmid = "" Then
                            f = f + 1
                            nt = "b-eqImg" & fid & "i"
                            ntn = "btn-eqImg" & fid & "i"
                            ntm = "btm-eqImg" & fid & "i"

                            ont = "b-eqImg" & ofid & "i"
                            ontn = "btn-eqImg" & ofid & "i"
                            ontm = "btm-eqImg" & ofid & "i"
                        Else
                            c = c + 1
                            nt = "c-eqImg" & cmid & "i"
                            ntn = "ctn-eqImg" & cmid & "i"
                            ntm = "ctm-eqImg" & cmid & "i"

                            ont = "c-eqImg" & ocmid & "i"
                            ontn = "ctn-eqImg" & ocmid & "i"
                            ontm = "ctm-eqImg" & ocmid & "i"
                        End If

                        ts = Server.MapPath("\") & ap & Replace(t, nt, ont)
                        ts = Replace(ts, "\", "/")
                        td = Server.MapPath("\") & ap & t
                        td = Replace(td, "\", "/")
                        System.IO.File.Copy(ts, td, True)

                        tns = Server.MapPath("\") & ap & Replace(tn, ntn, ontn)
                        tn = Replace(tn, "\", "/")
                        tnd = Server.MapPath("\") & ap & tn
                        tnd = Replace(tnd, "\", "/")
                        System.IO.File.Copy(tns, tnd, True)

                        tms = Server.MapPath("\") & ap & Replace(tm, ntm, ontm)
                        tms = Replace(tms, "\", "/")
                        tmd = Server.MapPath("\") & ap & tm
                        tmd = Replace(tmd, "\", "/")
                        System.IO.File.Copy(tms, tmd, True)
                    Next
                Catch ex As Exception
                    Dim strMessage2 As String = ex.Message
                    strMessage2 = copy.ModString1(strMessage2)
                    strMessage2 = "Ref-1963 - " + strMessage2
                    If lblstrmsg.Value = "" Then
                        lblstrmsg.Value = strMessage2
                    Else
                        lblstrmsg.Value += "\n\n" & strMessage2
                    End If
                End Try
            End If
        Else
        End If

        Dim copy12 As New Utilities
        sql = "usp_copyProcsMDB '" & eqid & "', '" & newid & "', '" & mdb & "', '" & orig & "','" & srvr & "'"

        If mdb <> orig Then
            Try
                Dim ds As New DataSet
                copy12.Open()
                ds = copy12.GetDSData(sql)
                copy12.Dispose()
                Dim f, fs, fd As String
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    f = ds.Tables(0).Rows(i)("filename").ToString
                    fs = Server.MapPath("\") & oloc & "/eqimages/" & f
                    fd = Server.MapPath("\") & cloc & "/eqimages/" & f
                    System.IO.File.Copy(fs, fd, True)
                Next
            Catch ex As Exception
                Dim strMessage2 As String = ex.Message
                strMessage2 = copy.ModString1(strMessage2)
                strMessage2 = "Ref-1995 - " + strMessage2
                If lblstrmsg.Value = "" Then
                    lblstrmsg.Value = strMessage2
                Else
                    lblstrmsg.Value += "\n\n" & strMessage2
                End If
                'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
            End Try
        Else
            Try
                Dim ds As New DataSet
                copy12.Open()
                ds = copy12.GetDSData(sql)
                copy12.Dispose()
                Dim f, fs, fd As String
                Dim i As Integer
                Dim x As Integer = ds.Tables(0).Rows.Count
                For i = 0 To (x - 1)
                    f = ds.Tables(0).Rows(i)("filename").ToString
                    fs = Server.MapPath("\") & ap & "/eqimages/" & f
                    fd = Server.MapPath("\") & ap & "/eqimages/" & f
                    System.IO.File.Copy(fs, fd, True)
                Next
            Catch ex As Exception
                Dim strMessage2 As String = ex.Message
                strMessage2 = copy.ModString1(strMessage2)
                strMessage2 = "Ref-2021 - " + strMessage2
                If lblstrmsg.Value = "" Then
                    lblstrmsg.Value = strMessage2
                Else
                    lblstrmsg.Value += "\n\n" & strMessage2
                End If
                'Utilities.CreateMessageAlert(Me, strMessage2, "strKey1")
            End Try
        End If



        'tbltop.Attributes.Add("class", "details")
        'tblbot.Attributes.Add("class", "view")
        Dim chk, dchk As String
        If clid = "" Or clid = "0" Then
            chk = "no"
        Else
            chk = "yes"
        End If
        'txtneweqnum.Enabled = True
        'txtneweqnum.Text = ""
        'ifneweq.Attributes.Add("src", "pmlibneweqdets.aspx?start=yes&dchk=yes&db=" & mdb & "&eqid=" & newid & "&chk=" & chk)
        'ifnewfu.Attributes.Add("src", "pmlibnewfudets.aspx?start=yes&db=" & mdb & "&eqid=" & newid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&oloc=" & oloc)

    End Sub

    
End Class