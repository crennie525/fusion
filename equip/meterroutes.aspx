﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="meterroutes.aspx.vb" Inherits="lucy_r12.meterroutes" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script  type="text/javascript">
        function getprint() {
            var chk = document.getElementById("lblrouteid").value;
            if (chk != "") {
                window.parent.getprint(chk);
            }
        }
        function delroute() {
        var chk = document.getElementById("lblrouteid").value;
            if (chk != "") {
                var decision = confirm("Are You Sure You Want To Delete This Route?")
                if (decision == true) {
                    document.getElementById("lblret").value = "delroute";
                    document.getElementById("form1").submit();
                }
                else {
                    alert("Action Cancelled")
                }
            }
        }

        function addseq() {
            var chk = document.getElementById("lblrouteid").value;
            if (chk != "") {
                document.getElementById("lblret").value = "addseq";
                document.getElementById("form1").submit();
            }
        }
        function addroute() {

            var chk = document.getElementById("txtnewroute").value;
            var dchk = document.getElementById("lblroute").value;
            if (chk != "" && dchk != chk) {
                document.getElementById("lblret").value = "addroute";
                document.getElementById("form1").submit();
            }
        }
        function savetop() {
            var chk = document.getElementById("txtnewroute").value;
            var dchk = document.getElementById("lblroute").value;
            var chk2 = document.getElementById("txtnewdesc").value;
            var dchk2 = document.getElementById("lbldesc").value;
            if (chk != "" && (dchk != chk || dck2 != chk2)) {
                document.getElementById("lblroute").value = chk;
                document.getElementById("lblret").value = "saveroute";
                document.getElementById("form1").submit();
            }
        }
        function getmeter(mm, mu, eqid) {
            if (eqid == "") {
                eqid = document.getElementById("lbleqid").value;
            }
            if (eqid != "") {
                var eReturn = window.showModalDialog("meterlookup.aspx?typ=lup&eqid=" + eqid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
                if (eReturn) {
                    //clearall();
                    var ret = eReturn.split("~");
                    //meterid + "~" + munit + "~" + meter;
                    document.getElementById("lblmeterid").value = ret[0];
                    document.getElementById("lblmeter").value = ret[1];
                    document.getElementById("lblunit").value = ret[2];
                    document.getElementById(mu).innerHTML = ret[1];
                    document.getElementById(mm).innerHTML = ret[2];
                   
                }
            }
        }
        function getrt(who) {
            var sid = document.getElementById("lblsid").value;
            var eReturn = window.showModalDialog("meterroutelookminidialog.aspx?sid=" + sid, "", "dialogHeight:500px; dialogWidth:500px; resizable=yes");
            if (eReturn) {
                document.getElementById("lblret").value = "getroute";
                document.getElementById("form1").submit();
            }
        }
        function clearall(eq, mm, mu) {
            document.getElementById(eq).innerHTML = "";
            document.getElementById("lbleq").value = "";
            document.getElementById("lbldid").value = "";
            document.getElementById("lbldept").value = "";
            document.getElementById("lblclid").value = "";
            document.getElementById("lblcell").value = "";
            document.getElementById("lbleqid").value = "";
            document.getElementById("lbllid").value = "";
            document.getElementById("lblloc").value = "";
            document.getElementById("lblmeterid").value = "";
            document.getElementById("lblmeter").value = "";
            document.getElementById("lblunit").value = "";
            document.getElementById(mu).innerHTML = "";
            document.getElementById(mm).innerHTML = "";
            document.getElementById("lblrettyp").value = "";
        }
        function getloc(eq, mm, mu) {
            var sid = document.getElementById("lblsid").value;
        var eReturn = window.showModalDialog("mlocpic.aspx?sid=" + sid, "", "dialogHeight:200px; dialogWidth:350px; resizable=yes");
        if (eReturn) {
            //ret = did + "~" + dept + "~" + cid + "~" + cell + "~" + eqid + "~" + eq + "~" + fuid + "~" + fu + "~" + 
            //comid + "~" + comp + "~" + ncid + "~" + nc + "~" + lid + "~" + loc; + "~" + dept
            clearall(eq, mm, mu);
            var ret = eReturn.split("~");
            var chk = ret[14];
            document.getElementById("lblrettyp").value = chk;
            //alert(chk)
            if (chk == "dept") {
                document.getElementById(eq).innerHTML = ret[5];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("lbldid").value = ret[0];
                document.getElementById("lbldept").value = ret[1];
                document.getElementById("lblclid").value = ret[2];
                document.getElementById("lblcell").value = ret[3];
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbllid").value = ret[12];
                document.getElementById("lblloc").value = ret[13];
            }
        }
        else {
        //alert("no return")
        }
    }
    function getnext() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg < cnt) {
            document.getElementById("lblret").value = "next"
            document.getElementById("form1").submit();
        }
    }
    function getlast() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg < cnt) {
            document.getElementById("lblret").value = "last"
            document.getElementById("form1").submit();
        }
    }
    function getprev() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg > 1) {
            document.getElementById("lblret").value = "prev"
            document.getElementById("form1").submit();
        }
    }
    function getfirst() {

        var cnt = document.getElementById("txtpgcnt").value;
        var pg = document.getElementById("txtpg").value;
        pg = parseInt(pg);
        cnt = parseInt(cnt)
        if (pg > 1) {
            document.getElementById("lblret").value = "first"
            document.getElementById("form1").submit();
        }
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        .<table>
            <tr>
                <td class="thdrsing label">
                    Add/Edit Meter Routes
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="label">
                                Route Title
                            </td>
                            <td>
                                <asp:TextBox ID="txtnewroute" runat="server"></asp:TextBox>
                            </td>
                            <td class="label">
                                Description
                            </td>
                            <td>
                                <asp:TextBox ID="txtnewdesc" runat="server" Width="160"></asp:TextBox>
                            </td>
                            <td>
                                <img id="imgadd" onmouseover="return overlib('Create a Route with the Title and Description You Have Entered')"
                                    onmouseout="return nd()" onclick="addroute();" src="../images/appbuttons/minibuttons/addnew.gif"
                                    runat="server">
                            </td>
                            <td>
                                <img onmouseover="return overlib('View Meter Route List')" onmouseout="return nd()"
                                    onclick="getrt('rt');" src="../images/appbuttons/minibuttons/globem.gif">
                            </td>
                            <td>
                                <img onmouseover="return overlib('Print Route List')" onmouseout="return nd()"
                                    onclick="getprint();" src="../images/appbuttons/minibuttons/printx.gif">
                            </td>
                            <td>
                            <img onmouseover="return overlib('Delete this Route')" onmouseout="return nd()" src="../images/appbuttons/minibuttons/del.gif" alt = "" onclick="delroute();" id="imgdelrt" runat="server" />
                            </td>
                            <td>
                                <img id="imgsav" onmouseover="return overlib('Save Changes to Route Title and Description')" onmouseout="return nd()"
                                    onclick="savetop();" src="../images/appbuttons/minibuttons/savedisk1.gif" runat="server">
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td class="thdrsing label">
                    Route Stops
                </td>
            </tr>
            <tr id="tradd" runat="server">
            <td class="plainlabel"><a href="#" onclick="addseq();">Add a Sequence</a></td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dglist" runat="server" BackColor="transparent" GridLines="None"
                        CellSpacing="1" AutoGenerateColumns="False">
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Width="60px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton19" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit Record"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton20" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update" ToolTip="Save Changes"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton21" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel" ToolTip="Cancel Changes"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Seq#">
                                <HeaderStyle Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label Width="50px" CssClass="plainlabel" ID="lblseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routeorder") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox style="width: 40px" CssClass="plainlabelred" ID="txtseq" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.routeorder") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Equipment#">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbleqnumi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label width="140px" CssClass="plainlabelred" ID="lbleqs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqnum") %>'>
                                    </asp:Label>
                                    <img id="lookupeq" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                           
                            <asp:TemplateColumn HeaderText="Meter ID">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lblmeteri" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label  Width="140px" CssClass="plainlabelred" ID="lblmtrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:Label>
                                    <img id="lookupmtr" runat="server" src="../images/appbuttons/minibuttons/magnifier.gif">
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            
                             <asp:TemplateColumn HeaderText="Meter Units">
                                <HeaderStyle Width="80px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label  Width="60px" CssClass="plainlabel" ID="lbluniti" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.unit") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabelred" ID="lbluni" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.unit") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Location Reference">
                                <HeaderStyle Width="200px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbllocrefi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.locref") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:textbox CssClass="plainlabelred" ID="txtlocref" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.locref") %>'>
                                    </asp:textbox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Plant Site Location">
                                <HeaderStyle Width="200px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabelred" ID="lbllocs" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.location") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderImageUrl="../images/appbuttons/minibuttons/del.gif">
                                <HeaderStyle HorizontalAlign="Center" Width="50px" CssClass="btmmenu plainlabel">
                                </HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibDel" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbleqidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmeteridi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmrouteidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mrouteid") %>'>
                                    </asp:Label>
                                   <asp:Label CssClass="plainlabel" ID="lblmstopidi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mstopid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label CssClass="plainlabel" ID="lbleqide" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.eqid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmeteride" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmrouteid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mrouteid") %>'>
                                    </asp:Label>
                                    <asp:Label CssClass="plainlabel" ID="lblmstopid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mstopid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
        </table>
    </div>
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblrouteid" runat="server" />
    <input type="hidden" id="lbloldorder" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblenterby" runat="server" /><input type="hidden" id="lblroute" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblmeter" runat="server" />
    <input type="hidden" id="lblunit" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lbldid" runat="server" />
    <input type="hidden" id="lbldept" runat="server" />
    <input type="hidden" id="lblclid" runat="server" />
    <input type="hidden" id="lblcell" runat="server" />
    <input type="hidden" id="lbllid" runat="server" />
    <input type="hidden" id="lblloc" runat="server" />
    <input type="hidden" id="lblrettyp" runat="server" />
    <input type="hidden" id="lbldesc" runat="server" />
    </form>
</body>
</html>
