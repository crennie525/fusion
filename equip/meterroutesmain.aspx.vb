﻿Public Class meterroutesmain
    Inherits System.Web.UI.Page
    Dim sid, uid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            uid = Request.QueryString("usrname").ToString
            lbluid.Value = uid
            ifmeter.Attributes.Add("src", "meterroutelook.aspx?sid=" + sid)

        End If
    End Sub

End Class