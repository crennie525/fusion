

'********************************************************
'*
'********************************************************



Public Class FuncCopyMiniDialog
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, sid, did, clid, eqid, Login, ro, db, oloc As String
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents iffu As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        Try
            Login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        Try
            Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
            Dim sessrefi As Integer = sessref * 1000 * 60
            lblsessrefresh.Value = sessrefi
        Catch ex As Exception
            lblsessrefresh.Value = "300000"
        End Try
        If Not IsPostBack Then
            Try
                eqid = Request.QueryString("eqid").ToString
                lbleqid.Value = eqid
                Try
                    ro = HttpContext.Current.Session("ro").ToString
                Catch ex As Exception
                    ro = "0"
                End Try
                If Len(eqid) <> 0 AndAlso eqid <> "" AndAlso eqid <> "0" Then
                    cid = Request.QueryString("cid").ToString
                    lblcid.Value = cid
                    sid = HttpContext.Current.Session("dfltps").ToString
                    did = Request.QueryString("did").ToString
                    clid = Request.QueryString("clid").ToString
                    Try
                        db = Request.QueryString("db").ToString
                        oloc = Request.QueryString("oloc").ToString
                    Catch ex As Exception
                        db = "0"
                        oloc = "0"
                    End Try
                    iffu.Attributes.Add("src", "FuncCopyMini.aspx?cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&ro=" & ro & "&date=" & Now & "&db=" & db & "&oloc=" & oloc)
                Else
                    Dim strMessage As String =  tmod.getmsg("cdstr939" , "FuncCopyMiniDialog.aspx.vb")
 
                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "noeqid"
                End If
            Catch ex As Exception
                Dim strMessage As String =  tmod.getmsg("cdstr940" , "FuncCopyMiniDialog.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                lbllog.Value = "noeqid"
            End Try

        End If
    End Sub

End Class
