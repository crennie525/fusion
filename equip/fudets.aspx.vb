

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class fudets
    Inherits System.Web.UI.Page
	Protected WithEvents lang2303 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2302 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2301 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2300 As System.Web.UI.WebControls.Label


    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim tmod As New transmod
    Dim dr As SqlDataReader
    Dim sql As String
    Dim fuid, start As String
    Dim srch As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tdfunc As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdspl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdcompcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdtaskcnt As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            start = Request.QueryString("start").ToString
            If start = "yes" Then
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
                srch.Open()
                GetDetails(fuid)
                srch.Dispose()
            End If


        End If
    End Sub
    Private Sub GetDetails(ByVal fuid As String)
        sql = "select *, ccnt = (select count(*) from components where func_id = '" & fuid & "'), " _
        + "tcnt = (select count(*) from pmtasks where funcid = '" & fuid & "') " _
        + "from functions where func_id = '" & fuid & "'"

        dr = srch.GetRdrData(sql)
        While dr.Read
            tdfunc.InnerHtml = dr.Item("func").ToString
            tdspl.InnerHtml = dr.Item("spl").ToString

            tdcompcnt.InnerHtml = dr.Item("ccnt").ToString
            tdtaskcnt.InnerHtml = dr.Item("tcnt").ToString
           
        End While
        dr.Close()

    End Sub
	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2300.Text = axlabs.GetASPXPage("fudets.aspx", "lang2300")
        Catch ex As Exception
        End Try
        Try
            lang2301.Text = axlabs.GetASPXPage("fudets.aspx", "lang2301")
        Catch ex As Exception
        End Try
        Try
            lang2302.Text = axlabs.GetASPXPage("fudets.aspx", "lang2302")
        Catch ex As Exception
        End Try
        Try
            lang2303.Text = axlabs.GetASPXPage("fudets.aspx", "lang2303")
        Catch ex As Exception
        End Try

    End Sub

End Class
