﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tpmrev.aspx.vb" Inherits="lucy_r12.tpmrev" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TPM Revision Entry</title>
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <script  type="text/javascript">
        function getcal() {
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=''", "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                document.getElementById("txtrevdate").value = eReturn;
            }
        }
        function savetpmrev() {

            document.getElementById("saverev").value = "go";
            //alert()
            document.getElementById("form1").submit();
        }
        function handlereturn() {
            var ret = document.getElementById("saverev").value;
            if (ret == "done") {
                window.parent.handlereturn("done");
            }
        }
    </script>
</head>
<body onload="handlereturn();">
    <form id="form1" runat="server">
    <table>
        <tr>
            <td class="bluelabel">
                Revision #
            </td>
            <td class="plainlabel">
                <asp:TextBox ID="txtrev" runat="server" style="width: 180px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="bluelabel">
                Revision Date
            </td>
            <td class="plainlabel">
                <asp:TextBox ID="txtrevdate" runat="server" style="width: 180px"></asp:TextBox>
            </td>
            <td>
                <img onclick="getcal();" alt="" src="../images/appbuttons/minibuttons/btn_calendar.jpg"
                    width="19" height="19" />
            </td>
        </tr>
        <tr>
            <td class="bluelabel">
                Approved By
            </td>
            <td class="plainlabel">
                <asp:TextBox ID="txtappr" runat="server" style="width: 180px"></asp:TextBox>
            </td>
        </tr>
        <tr>
        <td colspan="3" align="right">
        <img onclick="savetpmrev();" src="../images/appbuttons/minibuttons/savedisk1.gif"
                                alt="" title="Save TPM Revision Information"
                                 />
        </td>
        </tr>
    </table>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="saverev" runat="server" />
    </form>
</body>
</html>
