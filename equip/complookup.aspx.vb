

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.text
Public Class complookup
    Inherits System.Web.UI.Page
	Protected WithEvents lang2060 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2059 As System.Web.UI.WebControls.Label

	Protected WithEvents lang136 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim sql As String
    Dim dr As SqlDataReader
    Dim rl As New Utilities
    Dim Tables As String = "components"
    Dim PK As String = "comid"
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 200
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim FilterCnt As String = ""
    Dim Group As String = ""
    Dim Sort As String
    Dim intPgCnt, intPgNav As Integer
    Protected WithEvents lblret As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdcomp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtpgcnt As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents tbltop As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tbleqrec As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblpg As System.Web.UI.WebControls.Label
    Protected WithEvents ifirst As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents iprev As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents inext As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ilast As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetDGLangs()

	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            txtpg.Value = "1"
            rl.Open()
            GetComps()
            rl.Dispose()
        Else
            If Request.Form("lblret") = "next" Then
                rl.Open()
                GetNext()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "last" Then
                rl.Open()
                PageNumber = txtpgcnt.Value
                txtpg.Value = PageNumber
                GetComps()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "prev" Then
                rl.Open()
                GetPrev()
                rl.Dispose()
                lblret.Value = ""
            ElseIf Request.Form("lblret") = "first" Then
                rl.Open()
                PageNumber = 1
                txtpg.Value = PageNumber
                GetComps()
                rl.Dispose()
                lblret.Value = ""
            End If
        End If
    End Sub
    Private Sub GetNext()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg + 1
            txtpg.Value = PageNumber
            GetComps()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr854" , "complookup.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetPrev()
        Try
            Dim pg As Integer = txtpg.Value
            PageNumber = pg - 1
            txtpg.Value = PageNumber
            GetComps()
        Catch ex As Exception
            rl.Dispose()
            Dim strMessage As String =  tmod.getmsg("cdstr855" , "complookup.aspx.vb")
 
            rl.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
    End Sub
    Private Sub GetComps()

        Dim srch As String = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        If Len(srch) > 0 Then
            srch = "%" & srch & "%"
            Filter = "compnum like ''" & srch & "'' or compdesc like ''" & srch & "'' or spl like ''" & srch & "'' or desig like ''" & srch & "'' and comid <> 0"
            FilterCnt = "compnum like '" & srch & "' or compdesc like '" & srch & "' or spl like '" & srch & "' or desig like '" & srch & "' and comid <> 0"
        Else
            Filter = "comid <> 0"
            FilterCnt = "comid <> 0"
        End If
        Dim sb As StringBuilder = New StringBuilder

        sb.Append("<table width=""700"">")
        sb.Append("<tr><td colspan=""6""><div style=""OVERFLOW: auto; width: 700px; height: 360px;"">")
        sb.Append("<table width=""680"">")
        sb.Append("<tr><td class=""thdrsingg plainlabel"" width=""170"">" & tmod.getlbl("cdlbl430" , "complookup.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""170"">" & tmod.getlbl("cdlbl431" , "complookup.aspx.vb") & "</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""170"">SPL</td>")
        sb.Append("<td class=""thdrsingg plainlabel"" width=""170"">" & tmod.getlbl("cdlbl433" , "complookup.aspx.vb") & "</td></tr>")
        Dim rowid As Integer = 0
        If FilterCnt <> "" Then
            sql = "select count(*) from components where " & FilterCnt
        Else
            sql = "select count(*) from components"
        End If
        PageNumber = txtpg.Value
        intPgCnt = rl.Scalar(sql)
        intPgNav = rl.PageCount(intPgCnt, PageSize)
        If intPgNav = 0 Then
            lblpg.Text = "Page 0 of 0"
        Else
            lblpg.Text = "Page " & PageNumber & " of " & intPgNav
        End If
        txtpgcnt.Value = intPgNav
        dr = rl.GetPage(Tables, PK, Sort, PageNumber, PageSize, Fields, Filter, Group)
        Dim bc, ci, co, cd, cs, cg As String
        While dr.Read
            If rowid = 0 Then
                rowid = 1
                bc = "white"
            Else
                rowid = 0
                bc = "#E7F1FD"
            End If
            ci = dr.Item("comid").ToString
            co = dr.Item("compnum").ToString
            cd = dr.Item("compdesc").ToString
            cs = dr.Item("spl").ToString
            cg = dr.Item("desig").ToString
            co = Replace(co, "//", "/")
            co = Replace(co, "'", Chr(180), , , vbTextCompare)
            co = Replace(co, """", Chr(180) & Chr(180), , , vbTextCompare)
            cd = Replace(cd, "//", "/")
            cs = Replace(cs, "//", "/")
            cg = Replace(cg, "//", "/")
            sb.Append("<tr bgcolor=""" & bc & """><td class=""linklabel""><a href=""#"" onclick=""rtret('" & ci & "', '" & co & "');"">")
            sb.Append(co & "</a></td>")
            sb.Append("<td class=""plainlabel"">" & cd & "</td>")
            sb.Append("<td class=""plainlabel"">" & cs & "</td>")
            sb.Append("<td class=""plainlabel"">" & cg & "</td></tr>")
        End While
        dr.Close()
        dr.Close()
        sb.Append("</table></div></td></tr></table>")
        tdcomp.InnerHtml = sb.ToString
    End Sub

    Private Sub ibtnsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        txtpg.Value = "1"
        rl.Open()
        GetComps()
        rl.Dispose()
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            'dgdepts.Columns(0).HeaderText = dlabs.GetDGPage("complookup.aspx","dgdepts","0")
        Catch ex As Exception
        End Try
        Try
            'dgdepts.Columns(2).HeaderText = dlabs.GetDGPage("complookup.aspx","dgdepts","2")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang136.Text = axlabs.GetASPXPage("complookup.aspx", "lang136")
        Catch ex As Exception
        End Try
        Try
            lang2059.Text = axlabs.GetASPXPage("complookup.aspx", "lang2059")
        Catch ex As Exception
        End Try
        Try
            lang2060.Text = axlabs.GetASPXPage("complookup.aspx", "lang2060")
        Catch ex As Exception
        End Try

    End Sub

End Class
