<%@ Page Language="vb" AutoEventWireup="false" Codebehind="funceditdialog.aspx.vb" Inherits="lucy_r12.funceditdialog" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>funceditdialog</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<script  src="../scripts/sessrefdialog.js"></script>
		<script  src="../scripts1/funceditdialogaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
	</HEAD>
	<body  onload="resetsess();pageScroll();">
		<form id="form1" method="post" runat="server">
			<iframe id="ifco" runat="server" width="100%" height="150%" frameBorder="no"></iframe>
			<iframe id="ifsession" class="details" src="" frameborder="0" runat="server" style="z-index: 0;"></iframe>
     <script type="text/javascript">
         document.getElementById("ifsession").src = "../session.aspx?who=mm";
    </script><input type="hidden" id="lblsessrefresh" runat="server" NAME="lblsessrefresh">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
