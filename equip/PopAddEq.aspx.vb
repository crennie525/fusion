

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class PopAddEq
    Inherits System.Web.UI.Page
	Protected WithEvents lang2552 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2551 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2550 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2549 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2548 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2547 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2546 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2545 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2544 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2543 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2542 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2541 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2540 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2539 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2538 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2537 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim eqr As New Utilities
    Dim dr As SqlDataReader
    Dim sql As String
    Dim cid As String
    Dim sid, dept, cell, eqid, filt, lid, ro As String
    Dim Tables As String = ""
    Dim PK As String = ""
    Dim PageNumber As Integer = 1
    Dim PageSize As Integer = 5
    Dim Fields As String = "*"
    Dim Filter As String = ""
    Dim Group As String = ""
    Dim sort As String
    Dim decPgNav As Decimal
    Dim intPgNav As Integer
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblecrflg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents txtneweq As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsave As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rptreq As System.Web.UI.WebControls.Repeater
    Protected WithEvents txtsrch As System.Web.UI.WebControls.TextBox
    Protected WithEvents ibtnsearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnprev As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnnext As System.Web.UI.WebControls.ImageButton
    Protected WithEvents tdpg As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtpg As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbluid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim usrname, uid, login As String
    Protected WithEvents ddac As System.Web.UI.WebControls.DropDownList
    Protected WithEvents irtn As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents tdedit As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents imgadd As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbllog As System.Web.UI.HtmlControls.HtmlInputHidden
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txteqname As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtecr As System.Web.UI.WebControls.TextBox
    Protected WithEvents txteqdesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtspl As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtloc As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtoem As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtmod As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtser As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlstat As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnecr As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        


	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
        End Try
        GetBGBLangs()
        'Put user code to initialize the page here
        Try
            login = HttpContext.Current.Session("Logged_IN").ToString()
        Catch ex As Exception
            lbllog.Value = "no"
            Exit Sub
        End Try
        If Not IsPostBack Then
            If lbllog.Value <> "no" Then
                usrname = HttpContext.Current.Session("username").ToString()
                lbluser.Value = usrname
                uid = HttpContext.Current.Session("uid").ToString()
                lbluid.Value = uid

                Try
                    Try
                        ro = HttpContext.Current.Session("ro").ToString
                    Catch ex As Exception
                        ro = "0"
                    End Try
                    lblro.Value = ro
                    If ro = "1" Then
                        ibtnsave.Enabled = False
                        ibtnsave.ImageUrl = "../images/appbuttons/bgbuttons/savedis.gif"
                        imgadd.Attributes.Add("src", "../images/appbuttons/minibuttons/addnewdis.gif")
                    Else
                        'ibtnsave.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/savehov.gif'")
                        'ibtnsave.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/save.gif'")
                    End If
                    dept = Request.QueryString("dept").ToString
                    lbldept.Value = dept
                    lid = Request.QueryString("lid").ToString
                    lbllid.Value = lid
                    If Len(dept) <> 0 AndAlso dept <> "" AndAlso dept <> "0" Then
                        cell = Request.QueryString("cell").ToString
                        sid = Request.QueryString("sid").ToString
                        eqid = Request.QueryString("eqid").ToString
                        cid = "0" 'HttpContext.Current.Session("comp").ToString
                        lblcid.Value = cid
                        lblcell.Value = cell
                        lblsid.Value = sid
                        lbleqid.Value = eqid
                        eqr.Open()
                        PopStatus()
                        PopEqPg(PageNumber)
                        CheckStat(cid)
                        If eqid <> "" Or Len(eqid) <> 0 Then
                            PopEq(eqid)
                            tdedit.InnerHtml = "Edit Selected Equipment Record"
                        Else
                            tdedit.InnerHtml = "Edit Selected Equipment Record"
                            txteqname.Text = "No Record Selected"
                        End If
                        eqr.Dispose()
                    ElseIf lid <> "" And Len(lid) <> 0 Then
                        sid = Request.QueryString("sid").ToString
                        lblsid.Value = sid
                        eqid = Request.QueryString("eqid").ToString
                        lbleqid.Value = eqid
                        cid = "0" 'HttpContext.Current.Session("comp").ToString
                        lblcid.Value = cid
                        eqr.Open()
                        PopStatus()
                        PopEqPg(PageNumber)
                        CheckStat(cid)
                        If eqid <> "" Or Len(eqid) <> 0 Then
                            PopEq(eqid)
                            tdedit.InnerHtml = "Edit Selected Equipment Record"
                        Else
                            tdedit.InnerHtml = "Edit Selected Equipment Record"
                            txteqname.Text = "No Record Selected"
                        End If
                        eqr.Dispose()
                    Else
                        Dim strMessage As String = tmod.getmsg("cdstr1000" , "PopAddEq.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                        lbllog.Value = "nodeptid"
                    End If
                Catch ex As Exception
                    Dim strMessage As String = tmod.getmsg("cdstr1001" , "PopAddEq.aspx.vb")

                    Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    lbllog.Value = "nodeptid"
                End Try
            End If
        End If
        If lbllog.Value <> "no" Then
            If Request.Form("lblchk") = "ok" Then
                AddEq()
            End If
            If Request.Form("lblchk") = "add" Then
                AddEq()
            End If
        End If

        'irtn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'irtn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
        'btnprev.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yprev.gif'")
        'btnprev.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bprev.gif'")
        'btnnext.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/ynext.gif'")
        'btnnext.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/bnext.gif'")
    End Sub
    Private Sub PopEqPg(ByVal PageNumber As Integer)
        Dim srch As String
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        'cid = "0"
        cid = lblcid.Value
        sid = lblsid.Value
        Dim intPgCnt As Integer
        'Dim practice As String = HttpContext.Current.Session("practice").ToString
        If Len(srch) = 0 Then
            srch = ""
            sql = "select count(*) from Equipment"
            intPgCnt = eqr.Scalar(sql)
        Else
            Dim csrch As String
            csrch = "%" & srch & "%"
            sql = "select count(*) from Equipment where (eqnum like '" & csrch & "' or eqdesc like '" & csrch & "' or spl like '" & csrch & "')"
            intPgCnt = eqr.Scalar(sql)
        End If
        txtpg.Value = PageNumber

        intPgNav = eqr.PageCount(intPgCnt, PageSize)
        srch = txtsrch.Text
        srch = Replace(srch, "'", Chr(180), , , vbTextCompare)
        srch = Replace(srch, "--", "-", , , vbTextCompare)
        srch = Replace(srch, ";", ":", , , vbTextCompare)
        If Len(srch) = 0 Then
            srch = ""
        End If

        sql = "usp_getAllEqPg '" & cid & "', '" & PageNumber & "', '" & PageSize & "', '" & srch & "', '%%', '" & sid & "'"
        dr = eqr.GetRdrData(sql)
        rptreq.DataSource = dr
        rptreq.DataBind()
        dr.Close()
        updatepos(intPgNav)
        PopAC()
    End Sub
    Private Sub PopAC()
        cid = lblcid.Value
        sql = "select * from pmAssetClass"
        dr = eqr.GetRdrData(sql)
        ddac.DataSource = dr
        ddac.DataTextField = "assetclass"
        ddac.DataValueField = "acid"
        ddac.DataBind()
        dr.Close()
        ddac.Items.Insert(0, "Select Asset Class")
    End Sub
    Private Sub updatepos(ByVal intPgNav)
        PageNumber = txtpg.Value
        tdpg.InnerHtml = "Page " & PageNumber & " of " & intPgNav
        If PageNumber = 1 And intPgNav = 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = False

        ElseIf PageNumber = 1 And intPgNav > 1 Then
            btnprev.Enabled = False
            btnnext.Enabled = True

        ElseIf PageNumber = intPgNav Then
            btnnext.Enabled = False
            btnprev.Enabled = True

        Else
            btnnext.Enabled = True
            btnprev.Enabled = True
        End If
    End Sub
    Private Sub CheckStat(ByVal cid As String)
        Dim cnt As Integer
        sql = "select ecrflg from pmsysvars"
        cnt = eqr.Scalar(sql)
        If cnt = 0 Then
            btnecr.Src = "../images/appbuttons/minibuttons/cant.gif"
            btnecr.Attributes.Add("title", "ECR levels and weights have not been defined")
            lblecrflg.Value = "noecr"
        End If
    End Sub
    Private Sub PopStatus()

        sql = "select * from eqstatus"
        dr = eqr.GetRdrData(sql)
        ddlstat.DataSource = dr
        ddlstat.DataTextField = "status"
        ddlstat.DataValueField = "statid"
        ddlstat.DataBind()
        dr.Close()
        ddlstat.Items.Insert(0, "Select Status")
    End Sub
    Private Sub PopEq(ByVal eqid As String)
        sql = "select * from equipment where eqid = '" & eqid & "'"
        dr = eqr.GetRdrData(sql)
        While dr.Read
            txteqname.Text = dr.Item("eqnum").ToString
            txteqdesc.Text = dr.Item("eqdesc").ToString
            txtspl.Text = dr.Item("spl").ToString
            txtloc.Text = dr.Item("location").ToString
            txtoem.Text = dr.Item("oem").ToString
            txtmod.Text = dr.Item("model").ToString
            txtser.Text = dr.Item("serial").ToString
            Try
                ddlstat.SelectedValue = dr.Item("statid").ToString
            Catch ex As Exception

            End Try

            txtecr.Text = dr.Item("ecr").ToString
        End While

    End Sub
    Private Sub AddEq()
        Dim desc, spl, loc, oem, mode, ser, stat As String
        eqr.Open()
        Dim eq As String = txtneweq.Text
        eq = eqr.ModString3(eq)

        If Len(eq) > 50 Then
            Dim strMessage As String =  tmod.getmsg("cdstr1002" , "PopAddEq.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End If
        lid = lbllid.Value
        If lid = "0" Then lid = ""
        lbleqid.Value = eq
        dept = lbldept.Value
        cell = lblcell.Value
        sid = lblsid.Value
        cid = lblcid.Value
        usrname = lbluser.Value
        Dim typ As String
        If dept <> "" Then
            If lid <> "" And lid <> "0" Then
                typ = "dloc"
            Else
                typ = "reg"
            End If
        Else
            If lid <> "" And lid <> "0" Then
                typ = "loc"
            End If
        End If
        If cell = "none" Then
            cell = "0" 'filt = "where eqnum = '" & eq & "' and dept_id = '" & dept & "'"
        Else
            'was filt below
        End If
        If lid = "" Then
            filt = "where eqnum = '" & eq & "' and dept_id = '" & dept & "' and cellid = '" & cell & "'"
        Else
            filt = "where eqnum = '" & eq & "' and dept_id = '" & dept & "' and cellid = '" & cell & "' and locid = '" & lid & "'"
        End If

        sql = "select count(*) from equipment " & filt
        Dim echk As Integer = eqr.Scalar(sql)
        If echk = 0 And lid <> "" Then
            sql = "select count(*) from pmlocations where location = '" & eq & "'"
            echk = eqr.Scalar(sql)
        End If
        Dim ei As Integer
        If echk = 0 Then
            Dim ustr As String = Replace(usrname, "'", Chr(180), , , vbTextCompare)
            'If cell = "none" Then
            'sql = "insert into equipment (cellid, compid, eqnum, siteid, dept_id, createdby, createdate) values " _
            '+ "('0', '" & cid & "', '" & eq & "', '" & sid & "', '" & dept & "', '" & ustr & "', getDate())" _
            '+ "select @@identity as 'identity'"
            'Else
            'sql = "insert into equipment (compid, eqnum, cellid, siteid, dept_id, createdby, createdate) values " _
            '+ "('" & cid & "', '" & eq & "', '" & cell & "', '" & sid & "', '" & dept & "', '" & ustr & "', getDate())" _
            '+ "select @@identity as 'identity'"
            'removed if for cell, added change for lock
            If lid = "" Then
                sql = "insert into equipment (compid, eqnum, cellid, siteid, dept_id, createdby, createdate, locked, lockedby) values " _
                                 + "('" & cid & "', '" & eq & "', '" & cell & "', '" & sid & "', '" & dept & "', '" & ustr & "', getDate(), 1, '" & ustr & "')" _
                                 + " select @@identity as 'identity'"
            Else
                sql = "insert into equipment (compid, eqnum, cellid, siteid, locid, dept_id, createdby, createdate, locked, lockedby) values " _
                 + "('" & cid & "', '" & eq & "', '" & cell & "', '" & sid & "', '" & lid & "', '" & dept & " ', '" & ustr & "', getDate(), 1, '" & ustr & "')" _
                 + " select @@identity as 'identity'"
            End If

            sql = "addneweq '" & cid & "', '" & eq & "', '" & cell & "', '" & sid & "', '" & dept & "', '" & lid & " ', '" & ustr & "', '" & ustr & "', '" & typ & "'"

            'End If
            ei = eqr.Scalar(sql)
            lbleqid.Value = ei

            tdedit.InnerHtml = "Edit New Equipment Record"
            txteqname.Text = eq
            txteqdesc.Text = ""
            txtspl.Text = ""
            txtloc.Text = ""
            txtoem.Text = ""
            txtmod.Text = ""
            txtser.Text = ""
            txtecr.Text = ""
            Try
                ddlstat.SelectedValue = "0"
            Catch ex As Exception

            End Try

            lblchk.Value = ""
            txtneweq.Text = ""
            PageNumber = txtpg.Value
            PopEqPg(PageNumber)
        Else
            Dim strMessage As String =  tmod.getmsg("cdstr1003" , "PopAddEq.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
        eqr.Dispose()
    End Sub
    Private Sub ibtnsave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsave.Click
        Dim desc, spl, loc, oem, mode, ser, stat, statstr, ecr, ac, acid As String
        eqid = lbleqid.Value
        desc = txteqdesc.Text
        desc = eqr.ModString1(desc)

        spl = txtspl.Text
        spl = eqr.ModString1(spl)

        loc = txtloc.Text
        loc = eqr.ModString1(loc)

        oem = txtoem.Text
        oem = eqr.ModString1(oem)

        mode = txtmod.Text
        mode = eqr.ModString1(mode)

        ser = txtser.Text
        ser = eqr.ModString1(ser)

        If ddlstat.SelectedIndex <> 0 Then
            stat = ddlstat.SelectedValue
            statstr = ddlstat.SelectedItem.ToString
        Else
            stat = "0"
            statstr = "Select"
        End If
        If ddac.SelectedIndex <> 0 Then
            ac = ddac.SelectedItem.ToString
            acid = ddac.SelectedValue.ToString
        Else
            ac = "Select"
            acid = "0"
        End If
        ecr = txtecr.Text
        If Len(ecr) = 0 Then
            ecr = "0"
        End If
        Dim eqechk As Long
        Try
            eqechk = System.Convert.ToDecimal(ecr)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1004" , "PopAddEq.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        sql = "usp_updateEquipment '" & eqid & "', '" & desc & "', '" & spl & "', " _
        + "'" & loc & "', '" & oem & "', '" & mode & "', '" & ser & "', '" & stat & "', " _
        + "'" & statstr & "', '" & ecr & "', '" & acid & "', '" & ac & "' "
        Dim eqname As String = txteqname.Text
        eqr.Open()
        Try
            If Len(eqname) > 0 And eqname <> "" Then

                eqr.Update(sql)

            Else
                Dim strMessage As String =  tmod.getmsg("cdstr1005" , "PopAddEq.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr1006" , "PopAddEq.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End Try
        If ddlstat.SelectedIndex <> 0 Then
            sql = "update equipment  set eqstatindex = (" _
            + "select eqstatindex from eqstatus where " _
            + "statid = '" & stat & "') where eqid = '" & eqid & "'"
            eqr.Update(sql)
        End If
        If ddac.SelectedIndex <> 0 Then
            sql = "update equipment  set acindex = (" _
            + "select acindex from pmAssetClass where " _
            + "acid = '" & acid & "') where eqid = '" & eqid & "'"
            eqr.Update(sql)
        End If
        eqr.Dispose()


    End Sub

    Private Sub btnnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnnext.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg + 1
        txtpg.Value = PageNumber
        eqr.Open()
        PopEqPg(PageNumber)
        eqr.Dispose()
    End Sub

    Private Sub btnprev_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnprev.Click
        Dim pg As Integer = txtpg.Value
        PageNumber = pg - 1
        txtpg.Value = PageNumber
        eqr.Open()
        PopEqPg(PageNumber)
        eqr.Dispose()
    End Sub
	



    Private Sub rptreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptreq.ItemDataBound


        If e.Item.ItemType = ListItemType.Header Then
            Dim axlabs As New aspxlabs
            Try
                Dim lang2537 As Label
                lang2537 = CType(e.Item.FindControl("lang2537"), Label)
                lang2537.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2537")
            Catch ex As Exception
            End Try
            Try
                Dim lang2538 As Label
                lang2538 = CType(e.Item.FindControl("lang2538"), Label)
                lang2538.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2538")
            Catch ex As Exception
            End Try
            Try
                Dim lang2539 As Label
                lang2539 = CType(e.Item.FindControl("lang2539"), Label)
                lang2539.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2539")
            Catch ex As Exception
            End Try
            Try
                Dim lang2540 As Label
                lang2540 = CType(e.Item.FindControl("lang2540"), Label)
                lang2540.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2540")
            Catch ex As Exception
            End Try
            Try
                Dim lang2541 As Label
                lang2541 = CType(e.Item.FindControl("lang2541"), Label)
                lang2541.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2541")
            Catch ex As Exception
            End Try
            Try
                Dim lang2542 As Label
                lang2542 = CType(e.Item.FindControl("lang2542"), Label)
                lang2542.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2542")
            Catch ex As Exception
            End Try
            Try
                Dim lang2543 As Label
                lang2543 = CType(e.Item.FindControl("lang2543"), Label)
                lang2543.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2543")
            Catch ex As Exception
            End Try
            Try
                Dim lang2544 As Label
                lang2544 = CType(e.Item.FindControl("lang2544"), Label)
                lang2544.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2544")
            Catch ex As Exception
            End Try
            Try
                Dim lang2545 As Label
                lang2545 = CType(e.Item.FindControl("lang2545"), Label)
                lang2545.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2545")
            Catch ex As Exception
            End Try
            Try
                Dim lang2546 As Label
                lang2546 = CType(e.Item.FindControl("lang2546"), Label)
                lang2546.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2546")
            Catch ex As Exception
            End Try
            Try
                Dim lang2547 As Label
                lang2547 = CType(e.Item.FindControl("lang2547"), Label)
                lang2547.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2547")
            Catch ex As Exception
            End Try
            Try
                Dim lang2548 As Label
                lang2548 = CType(e.Item.FindControl("lang2548"), Label)
                lang2548.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2548")
            Catch ex As Exception
            End Try
            Try
                Dim lang2549 As Label
                lang2549 = CType(e.Item.FindControl("lang2549"), Label)
                lang2549.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2549")
            Catch ex As Exception
            End Try
            Try
                Dim lang2550 As Label
                lang2550 = CType(e.Item.FindControl("lang2550"), Label)
                lang2550.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2550")
            Catch ex As Exception
            End Try
            Try
                Dim lang2551 As Label
                lang2551 = CType(e.Item.FindControl("lang2551"), Label)
                lang2551.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2551")
            Catch ex As Exception
            End Try
            Try
                Dim lang2552 As Label
                lang2552 = CType(e.Item.FindControl("lang2552"), Label)
                lang2552.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2552")
            Catch ex As Exception
            End Try

        End If

    End Sub






    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            lang2537.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2537")
        Catch ex As Exception
        End Try
        Try
            lang2538.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2538")
        Catch ex As Exception
        End Try
        Try
            lang2539.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2539")
        Catch ex As Exception
        End Try
        Try
            lang2540.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2540")
        Catch ex As Exception
        End Try
        Try
            lang2541.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2541")
        Catch ex As Exception
        End Try
        Try
            lang2542.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2542")
        Catch ex As Exception
        End Try
        Try
            lang2543.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2543")
        Catch ex As Exception
        End Try
        Try
            lang2544.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2544")
        Catch ex As Exception
        End Try
        Try
            lang2545.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2545")
        Catch ex As Exception
        End Try
        Try
            lang2546.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2546")
        Catch ex As Exception
        End Try
        Try
            lang2547.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2547")
        Catch ex As Exception
        End Try
        Try
            lang2548.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2548")
        Catch ex As Exception
        End Try
        Try
            lang2549.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2549")
        Catch ex As Exception
        End Try
        Try
            lang2550.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2550")
        Catch ex As Exception
        End Try
        Try
            lang2551.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2551")
        Catch ex As Exception
        End Try
        Try
            lang2552.Text = axlabs.GetASPXPage("PopAddEq.aspx", "lang2552")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                ibtnsave.Attributes.Add("src", "../images2/eng/bgbuttons/save.gif")
            ElseIf lang = "fre" Then
                ibtnsave.Attributes.Add("src", "../images2/fre/bgbuttons/save.gif")
            ElseIf lang = "ger" Then
                ibtnsave.Attributes.Add("src", "../images2/ger/bgbuttons/save.gif")
            ElseIf lang = "ita" Then
                ibtnsave.Attributes.Add("src", "../images2/ita/bgbuttons/save.gif")
            ElseIf lang = "spa" Then
                ibtnsave.Attributes.Add("src", "../images2/spa/bgbuttons/save.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                irtn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                irtn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                irtn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                irtn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                irtn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

    Protected Sub ibtnsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnsearch.Click
        eqr.Open()
        txtpg.Value = "1"
        PageNumber = 1
        PopEqPg(PageNumber)
        eqr.Dispose()
    End Sub
End Class
