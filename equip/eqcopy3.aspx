<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="eqcopy3.aspx.vb" Inherits="lucy_r12.eqcopy3" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>eqcopy3_aspx</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
    <link rel="stylesheet" type="text/css" href="../styles/reports.css" />
    <script  type="text/javascript" src="../scripts/smartscroll4.js"></script>
    <script  type="text/javascript" src="../scripts/overlib2.js"></script>
    <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
    <script  type="text/javascript">
	
    function checktime1() {

		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		curr_month++;
		var curr_year = d.getFullYear();
		var hr = d.getHours();
		var mn = d.getMinutes();
		var ss = d.getSeconds();
		document.getElementById("lbltl").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
		//document.getElementById("td1").innerHTML = document.getElementById("lblt1").value;
		//document.getElementById("td2").innerHTML = document.getElementById("lblt2").value;
		//document.getElementById("td3").innerHTML = document.getElementById("lblt3").value;
		dateDiff();
		
		var strmsg = document.getElementById("lblstrmsg").value;
		if(strmsg!="") {
		document.getElementById("lblstrmsg").value="";
		alert("Problem Copying Record.\n\n" + strmsg)
		}
	}
    function dateDiff() {
		date1 = new Date();
		date2 = new Date();
		diff  = new Date();

		if(document.getElementById("lblsubmit").value=="didcopy") {
		//document.getElementById("lblsubmit").value="";
		date1temp = new Date(document.getElementById("lblcs").value);
		date1.setTime(date1temp.getTime());
		}
		else {
			if(document.getElementById("lblt1").value=="") {
				date1temp = new Date(document.getElementById("lblt2").value);
				date1.setTime(date1temp.getTime());
			}
			else {
				date1temp = new Date(document.getElementById("lblt1").value);
				date1.setTime(date1temp.getTime());
			}
		}
date2temp = new Date(document.getElementById("lbltl").value);
date2.setTime(date2temp.getTime());



// sets difference date to difference of first date and second date

diff.setTime(Math.abs(date1.getTime() - date2.getTime()));

timediff = diff.getTime();

weeks = Math.floor(timediff / (1000 * 60 * 60 * 24 * 7));
timediff -= weeks * (1000 * 60 * 60 * 24 * 7);

days = Math.floor(timediff / (1000 * 60 * 60 * 24));
timediff -= days * (1000 * 60 * 60 * 24);

hours = Math.floor(timediff / (1000 * 60 * 60));
timediff -= hours * (1000 * 60 * 60);

mins = Math.floor(timediff / (1000 * 60));
timediff -= mins * (1000 * 60);

secs = Math.floor(timediff / 1000);
timediff -= secs * 1000;
//weeks + " weeks, " + days + " days, " + hours + " hours, " + 
if (document.getElementById("lblsubmit").value == "didcopy") {
    document.getElementById("lblsubmit").value = "";
    document.getElementById("tdct").innerHTML = mins + " minutes, and " + secs + " seconds";
}
else {
    document.getElementById("td0").innerHTML = mins + " minutes, and " + secs + " seconds";
}

}
function clearall() {
    document.getElementById("lbllid").value = "";
    document.getElementById("lblloc").value = "";
    document.getElementById("tdloc2").innerHTML = "";

    document.getElementById("trlocs").className = "details";
    document.getElementById("lbldid").value = "";
    document.getElementById("lbldept").value = "";
    document.getElementById("tddept").innerHTML = "";
    document.getElementById("lblclid").value = "";
    document.getElementById("lblcell").value = "";
    document.getElementById("tdcell").innerHTML = "";
    document.getElementById("trdepts").className = "details";
}

function getminsrch() {
    var sid = document.getElementById("lblsid").value;
    var wo = "";
    var typ;
    if (wo == "") {
        typ = "lul";
    }
    else {
        typ = "wo";
    }
    var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
    if (eReturn) {
        clearall();
        var ret = eReturn.split("~");
        document.getElementById("lbldid").value = ret[0];
        document.getElementById("lbldept").value = ret[1];
        document.getElementById("tddept").innerHTML = ret[1];
        document.getElementById("lblclid").value = ret[2];
        document.getElementById("lblcell").value = ret[3];
        document.getElementById("tdcell").innerHTML = ret[3];
        document.getElementById("trdepts").className = "view";
        //document.getElementById("lblrettyp").value="depts";
        var did = ret[0];
        var eqid = ret[4];
        var clid = ret[2];
        document.getElementById("lblcellchk").value = "ok";
        if (clid == "") {
            document.getElementById("lblclid").value = "0";
        }
        var lid = ret[12];
        var typ;
        if (lid == "") {
            typ = "reg";
        }
        else {
            typ = "dloc";
        }


    }
}

function getlocs1() {
    var sid = document.getElementById("lblsid").value;
    var lid = document.getElementById("lbllid").value;
    var typ = "lul"
    //alert(lid)
    if (lid != "") {
        typ = "retloc"
    }
    var eqid = ""; //document.getElementById("lbleqid").value;
    var fuid = ""; //document.getElementById("lblfuid").value;
    var coid = ""; //document.getElementById("lblcoid").value;
    //alert(fuid)
    var wo = "";
    var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=" + typ + "&sid=" + sid + "&wo=" + wo + "&rlid=" + lid + "&eqid=" + eqid + "&fuid=" + fuid + "&coid=" + coid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
    if (eReturn) {
        clearall();
        var ret = eReturn.split("~");
        //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" + eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp + "~" + lev;
        document.getElementById("lbllid").value = ret[0];
        document.getElementById("lblloc").value = ret[1];
        document.getElementById("tdloc2").innerHTML = ret[2];

        document.getElementById("trlocs").className = "view";
        //document.getElementById("lblrettyp").value="locs";
        var did = "";

        var clid = "";

        var lid = ret[0];
        var typ;
        typ = "loc";


    }
}

function chkcurr() {
	var curr;
	try {
	curr = "<%=Session("chkit")%>";
	document.getElementById("curr").firstChild.nodeValue = curr;
	var stimer = window.setTimeout("chkcurr();", 1000);
	}
	catch(err) {
	document.getElementById("curr").firstChild.nodeValue = "err"
	}
	}
		function updateClock ( )
{
  var currentTime = new Date ( );

  var currentHours = currentTime.getHours ( );
  var currentMinutes = currentTime.getMinutes ( );
  var currentSeconds = currentTime.getSeconds ( );

  // Pad the minutes and seconds with leading zeros, if required
  currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  // Choose either "AM" or "PM" as appropriate
  var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

  // Convert the hours component to 12-hour format if needed
  currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  // Convert an hours component of "0" to "12"
  currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  // Compose the string for display
  var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

  // Update the time display
  document.getElementById("clock").firstChild.nodeValue = currentTimeString;
}

function getnext() {
		
var cnt = document.getElementById("txtpgcnt").value;
var pg = document.getElementById("txtpg").value;
pg = parseInt(pg);
cnt = parseInt(cnt)
if(pg<cnt) {
var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		curr_month++;
		var curr_year = d.getFullYear();
		var hr = d.getHours();
		var mn = d.getMinutes();
		var ss = d.getSeconds();
		document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
document.getElementById("lblret").value = "next"
document.getElementById("form1").submit();
}
}
function getlast() {
	
var cnt = document.getElementById("txtpgcnt").value;
var pg = document.getElementById("txtpg").value;	
pg = parseInt(pg);
cnt = parseInt(cnt)
if(pg<cnt) {
var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		curr_month++;
		var curr_year = d.getFullYear();
		var hr = d.getHours();
		var mn = d.getMinutes();
		var ss = d.getSeconds();
		document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
document.getElementById("lblret").value = "last"
document.getElementById("form1").submit();
}
}
function getprev() {
		
var cnt = document.getElementById("txtpgcnt").value;
var pg = document.getElementById("txtpg").value;
pg = parseInt(pg);
cnt = parseInt(cnt)
if(pg>1) {
var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		curr_month++;
		var curr_year = d.getFullYear();
		var hr = d.getHours();
		var mn = d.getMinutes();
		var ss = d.getSeconds();
		document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
document.getElementById("lblret").value = "prev"
document.getElementById("form1").submit();
}
}
function getfirst() {
		
var cnt = document.getElementById("txtpgcnt").value;
var pg = document.getElementById("txtpg").value;
pg = parseInt(pg);
cnt = parseInt(cnt)
if(pg>1) {
var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		curr_month++;
		var curr_year = d.getFullYear();
		var hr = d.getHours();
		var mn = d.getMinutes();
		var ss = d.getSeconds();
		document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
document.getElementById("lblret").value = "first"
document.getElementById("form1").submit();
}
}

		function handlecomp(fuid) {
		document.getElementById("lblfu").value = fuid;
		//document.getElementById("lblsubmit").value = "getcomprev";
		//document.getElementById("form1").submit();
		var db = document.getElementById("lbldb").value;
		document.getElementById("ifcodets").src ="pmlibcodets.aspx?start=yes&fuid=" + fuid + "&db=" + db;
		document.getElementById("ifpmtaskdets").src ="pmlibpmtaskdets.aspx?start=yes&fuid=" + fuid + "&db=" + db;
		document.getElementById("iftpmtaskdets").src ="pmlibtpmtaskdets.aspx?start=yes&fuid=" + fuid + "&db=" + db;
		}
		
		function handlecompnew(fuid) {
		document.getElementById("lblfu").value = fuid;
		//document.getElementById("lblsubmit").value = "getcomprev";
		//document.getElementById("form1").submit();
		var db = document.getElementById("lbldb").value;
		var oloc = document.getElementById("lbloloc").value;
		document.getElementById("ifnewcodets").src ="pmlibnewcodets.aspx?start=yes&fuid=" + fuid + "&db=" + db + "&oloc=" + oloc;
		document.getElementById("ifnewpmtaskdets").src ="pmlibnewpmtaskdets.aspx?start=yes&fuid=" + fuid + "&db=" + db;
		document.getElementById("ifnewtpmtaskdets").src ="pmlibnewtpmtaskdets.aspx?start=yes&fuid=" + fuid + "&db=" + db;
		
		}
		
		function handlecompnewrem() {
		var db = document.getElementById("lbldb").value;
		var oloc = document.getElementById("lbloloc").value;
		document.getElementById("ifnewcodets").src ="pmlibnewcodets.aspx?start=no";
		document.getElementById("ifpmtaskdets").src ="pmlibnewpmtaskdets.aspx?start=no";
		document.getElementById("iftpmtaskdets").src ="pmlibnewtpmtaskdets.aspx?start=no";
		}
		
		function checkeq() {
        
		var eq = document.getElementById("lbleq").value;
		var eqn = document.getElementById("txtneweqnum").value;
		var did = document.getElementById("lbldid").value;
        var lid = document.getElementById("lbllid").value;
		var chk = document.getElementById("lblcellchk").value;
        //alert(eq)
		if(eq!="") {
		if(did==""&&lid=="") {
		alert("Department or Location Required")
		}
		else if(did!=""&&chk!="ok") {
		alert("Cell Required")
		}
		else if(eqn=="") {
		alert("New Equipment Name Required")
		}
		else {
		var msg = "";
		var msg = "";
		for (var i = 0; i < 50; i++) {
		msg += 'Your Data Is Being Processed...'
		msg += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		msg += 'Your Data Is Being Processed...'
		msg += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		msg += 'Your Data Is Being Processed...'
		msg += '<br><br><br>'
		}
		FreezeScreen(msg);
		document.getElementById("lblsubmit").value = "docopy";
		document.getElementById("form1").submit();
		}
		}				
		}

		function checktaskopts(id) {
		var chk = document.getElementById(id);
		if(chk.checked==true) {
		document.getElementById("cbfunc").checked = true;
		document.getElementById("cbcomp").checked = true;
		}
		}
		function checkopts(id) {
		var chk = document.getElementById(id);
		if(chk.checked==false) {
		if(id=="cbfunc") {
		document.getElementById("cbcomp").checked = false;
		}
		document.getElementById("cbpm").checked = false;
		document.getElementById("cbtpm").checked = false;
		}
		}
		function handleret() {
		document.getElementById("tbltop").className= "view";
		document.getElementById("tblbot").className = "details";
		//document.getElementById("cbcds").checked = true;
		document.getElementById("lblcopyret").value = "yes";
		document.getElementById("lblcurreqsort").value = "createdate desc";
		document.getElementById("lblsubmit").value = "getchngd";
		document.getElementById("form1").submit();
		}
		function chkchngd() {
		document.getElementById("lblsubmit").value = "getchngd";
		document.getElementById("form1").submit();
		}
		function setref() {
		window.parent.setref();
		}
		function setrefdialog() {
		window.parent.setrefdialog();
		}

        function checksave() {
		
		//var stest = "<%=Session("chkit")%>";
		//alert(stest)
		/*
		var msg = "";
		var msg = "";
		for (var i = 0; i < 50; i++) {
		msg += 'Your Data Is Being Processed...'
		msg += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		msg += 'Your Data Is Being Processed...'
		msg += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		msg += 'Your Data Is Being Processed...'
		msg += '<br><br><br>'
		}
		FreezeScreen(msg);
		*/
		}

        function dateDiff() {
		date1 = new Date();
		date2 = new Date();
		diff  = new Date();

		if(document.getElementById("lblsubmit").value=="didcopy") {
		//document.getElementById("lblsubmit").value="";
		date1temp = new Date(document.getElementById("lblcs").value);
		date1.setTime(date1temp.getTime());
		}
		else {
			if(document.getElementById("lblt1").value=="") {
				date1temp = new Date(document.getElementById("lblt2").value);
				date1.setTime(date1temp.getTime());
			}
			else {
				date1temp = new Date(document.getElementById("lblt1").value);
				date1.setTime(date1temp.getTime());
			}
		}
		
		date2temp = new Date(document.getElementById("lbltl").value);
		date2.setTime(date2temp.getTime());



		// sets difference date to difference of first date and second date

		diff.setTime(Math.abs(date1.getTime() - date2.getTime()));

		timediff = diff.getTime();

		weeks = Math.floor(timediff / (1000 * 60 * 60 * 24 * 7));
		timediff -= weeks * (1000 * 60 * 60 * 24 * 7);

		days = Math.floor(timediff / (1000 * 60 * 60 * 24)); 
		timediff -= days * (1000 * 60 * 60 * 24);

		hours = Math.floor(timediff / (1000 * 60 * 60)); 
		timediff -= hours * (1000 * 60 * 60);

		mins = Math.floor(timediff / (1000 * 60)); 
		timediff -= mins * (1000 * 60);

		secs = Math.floor(timediff / 1000); 
		timediff -= secs * 1000;
		//weeks + " weeks, " + days + " days, " + hours + " hours, " + 
		if(document.getElementById("lblsubmit").value=="didcopy") {
		document.getElementById("lblsubmit").value="";
		document.getElementById("tdct").innerHTML = mins + " minutes, and " + secs + " seconds";
		}
		else {
		document.getElementById("td0").innerHTML = mins + " minutes, and " + secs + " seconds";
		}

		}

        function getsaadmin() {
		var ss = document.getElementById("lblt1").value;
		var spa = document.getElementById("lblt2").value;
		var scs = document.getElementById("lblt3").value;
		var cpl = document.getElementById("lbltl").value;
		//alert("saadmin.aspx?ss=" + ss + "&spa=" + spa + "&scs=" + scs + "&cpl=" + cpl)
		var eReturn = window.showModalDialog("../mainmenu/saadmin.aspx?ss=" + ss + "&spa=" + spa + "&scs=" + scs + "&cpl=" + cpl, "", "dialogHeight:225px; dialogWidth:320px; resizable=yes");
				if (eReturn) {
				//fld = "txt" + fld;	
				//document.getElementById(fld).value = eReturn;
				//document.getElementById("form1").submit();
				}
		}
		function getctadmin() {
		var ss = document.getElementById("lblcs").value;
		var spa = document.getElementById("lblt2").value;
		var scs = document.getElementById("lblce").value;
		var cpl = document.getElementById("lbltl").value;
		//alert("saadmin.aspx?ss=" + ss + "&spa=" + spa + "&scs=" + scs + "&cpl=" + cpl)
		var eReturn = window.showModalDialog("ctadmin.aspx?ss=" + ss + "&spa=" + spa + "&scs=" + scs + "&cpl=" + cpl, "", "dialogHeight:225px; dialogWidth:320px; resizable=yes");
				if (eReturn) {
				//fld = "txt" + fld;	
				//document.getElementById(fld).value = eReturn;
				//document.getElementById("form1").submit();
				}
		}
		function mainsearch() {
		var srch = document.getElementById("txtsrch").value;
		if(srch!="") {
		document.getElementById("lblret").value = "msrch"
		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		curr_month++;
		var curr_year = d.getFullYear();
		var hr = d.getHours();
		var mn = d.getMinutes();
		var ss = d.getSeconds();
		document.getElementById("lblt1").value = curr_month + "-" + curr_date + "-" + curr_year + " " + hr + ":" + mn + ":" + ss;
		document.getElementById("form1").submit();
		}
		}
		function checkdddb() {
        document.getElementById("lblret").value = "getdddb"
document.getElementById("form1").submit();
        }
		function FreezeScreen(msg) {
			scroll(0,0);
			var outerPane = document.getElementById('FreezePane');
			var innerPane = document.getElementById('InnerFreezePane');
			if (outerPane) outerPane.className = 'FreezePaneOn3';
			if (innerPane) innerPane.innerHTML = msg;
		}

        function getsrch() {
		var eReturn = window.showModalDialog("../equip/maxsearchdialog.aspx?lib=yes&srchtyp=nofu" + "&date=" + Date(), "", "dialogHeight:720px; dialogWidth:890px; resizable=yes");
			if (eReturn) {
				var ret = eReturn;
				var retarr = ret.split(",");
				document.getElementById("lbleq").value = retarr[2];
				document.getElementById("lbloldsid").value = retarr[11];
				var geq = retarr[2];
				var mdb = document.getElementById("lbldb").value;
				//ifeqdets.Attributes.Add("src", "pmlibeqdets.aspx?start=yes&db=" & mdb & "&eqid=" & geq)
				//iffudets.Attributes.Add("src", "pmlibfudets.aspx?start=yes&db=" & mdb & "&eqid=" & geq)
				document.getElementById("ifeqdets").src="pmlibeqdets.aspx?start=yes&db=" + mdb + "&eqid=" + geq;
				document.getElementById("iffudets").src="pmlibfudets.aspx?start=yes&db=" + mdb + "&eqid=" + geq;
				
			}
			else {
				
			}
		}


        function getchildren() {
        var eqid = document.getElementById("lbleq").value;
        var sid = document.getElementById("lblsid").value;
        var desig = document.getElementById("lblismaster").value;
        //alert(eqid)
        if (desig == "1") {
            if (eqid != "") {
                var eReturn = window.showModalDialog("../equip/eqchildselectdialog.aspx?sid=" + sid + "&eqid=" + eqid + "&date=" + Date(), "", "dialogHeight:720px; dialogWidth:890px; resizable=yes");
			    if (eReturn) {
				    var ret = eReturn;
                    alert("Child Record Selection Completed")
                }
            }
        }
        else {
            alert("Requires a Master Record")
            //alert("No Designated Child Records Found for this site")
        }
        }
    </script>
</head>
<body class="tbg" onload="checktime1();">
    <form id="form1" method="post" runat="server">
    <div id="FreezePane" class="FreezePaneOff" align="center">
        <div id="InnerFreezePane" class="InnerFreezePane">
        </div>
    </div>
    <div style="z-index: 1000; position: absolute; visibility: hidden" id="overDiv">
    </div>
    <div style="position: absolute; top: 8px; left: 4px" id="scrollmenu">
    </div>
    <table style="z-index: 1; position: absolute; top: 4px; left: 4px" cellspacing="0"
        cellpadding="0" width="1000">
        <tr>
            <td colspan="3" align="right">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <span id="curr">&nbsp;</span> <span id="clock">&nbsp;</span>
                        </td>
                        <td id="td0" class="plainlabel" width="165">
                        </td>
                        <td style="width: 20px">
                            <img onclick="getsaadmin();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="z-index: 1; position: absolute; top: 24px; left: 4px" id="tbltop" cellspacing="0"
        cellpadding="1" width="1000" runat="server">
        <tr>
            <td colspan="3">
                <table cellspacing="0" cellpadding="1" width="1000">
                    <tr>
                        <td id="tdnavtop" class="thdrsinglft" width="22">
                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                        </td>
                        <td class="thdrsingrt label" width="978">
                            <asp:Label ID="lang2171" runat="server">Equipment Copy</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td class="bluelabel" width="255">
                            <table>
                                <tr>
                                    <td class="bluelabel">
                                        <asp:Label ID="lang2172" runat="server">Select Language Database</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="dddb" runat="server" AutoPostBack="False">
                                            <asp:ListItem Value="devdb2">English</asp:ListItem>
                                            <asp:ListItem Value="laurentide,laurentide">Old Dev DB</asp:ListItem>
                                            <asp:ListItem Value="pfizerpm3_ita">Italian</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="bluelabel" width="55">
                            <asp:Label ID="lang2173" runat="server">Search</asp:Label>
                        </td>
                        <td width="230">
                            <asp:TextBox ID="txtsrch" runat="server" Width="190px"></asp:TextBox><img id="Img1"
                                onclick="mainsearch();" src="../images/appbuttons/minibuttons/srchsm.gif" runat="server"><img
                                    onmouseover="return overlib('Use Max Search (Bypasses Grid Selection)', LEFT)"
                                    onmouseout="return nd()" onclick="getsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                        </td>
                        <td class="plainlabelred" width="290">
                            <input id="cbuseappr" onclick="chkchngd();" type="checkbox" name="cbuseappr" runat="server"><asp:Label
                                ID="lang2174" runat="server">Show Optimized PM Records Only</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td class="bluelabel" style="width: 80px">
                            <asp:Label ID="lang2175" runat="server">Current Sort</asp:Label>
                        </td>
                        <td id="tdcurrsort" class="plainlabelred" style="width: 150px" runat="server">
                        </td>
                        <td class="bluelabel">
                            <asp:CheckBox ID="cbcds" AutoPostBack="True" runat="server"></asp:CheckBox><asp:Label
                                ID="lang2176" runat="server">Order By Create Date Ascending</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table id="tbleq" cellspacing="0" cellpadding="0" width="1000" runat="server">
                    <tbody>
                        <tr id="tbleqrec" runat="server">
                            <td class="bluelabel" style="width: 80px">
                            </td>
                            <td width="540">
                            </td>
                            <td width="280">
                            </td>
                        </tr>
                        <tr id="treq" runat="server">
                            <td colspan="3">
                                <div style="width: 1000px; height: 180px; overflow: auto" onscroll="SetsDivPosition();"
                                    id="spdiv">
                                    <asp:Repeater ID="rptreq" runat="server">
                                        <HeaderTemplate>
                                            <table cellspacing="1">
                                                <tr>
                                                    <td class="btmmenu plainlabel" width="300px">
                                                        <asp:LinkButton OnClick="SortEq" ID="lbsorteq" runat="server">
                                                            <asp:Label ID="lang2177" runat="server">Equipment#</asp:Label>
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td class="btmmenu plainlabel" width="330px">
                                                        <asp:Label ID="lang2178" runat="server">Description</asp:Label>
                                                    </td>
                                                    <td class="btmmenu plainlabel" width="350px">
                                                        <asp:Label ID="lang2179" runat="server">Location</asp:Label>
                                                    </td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="tbg">
                                                <td class="plainlabel">
                                                    <asp:RadioButton AutoPostBack="True" OnCheckedChanged="GetEq1" ID="rbeq" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                                        runat="server" />
                                                </td>
                                                <td class="plainlabel">
                                                    <asp:Label ID="lbleqdesc" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="plainlabel">
                                                    <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"sitename") + ", " + DataBinder.Eval(Container.DataItem,"dept_line") + ", " + DataBinder.Eval(Container.DataItem,"cell_name")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbleqid" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblloc" Text='<%# DataBinder.Eval(Container.DataItem,"location")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblmod" Text='<%# DataBinder.Eval(Container.DataItem,"model")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbloem" Text='<%# DataBinder.Eval(Container.DataItem,"oem")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblser" Text='<%# DataBinder.Eval(Container.DataItem,"serial")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblspl" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblcby" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblmby" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblcdate" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblmdate" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblecr" Text='<%# DataBinder.Eval(Container.DataItem,"ecr")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblcph" Text='<%# DataBinder.Eval(Container.DataItem,"cph")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblcm" Text='<%# DataBinder.Eval(Container.DataItem,"cm")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbleph" Text='<%# DataBinder.Eval(Container.DataItem,"eph")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblem" Text='<%# DataBinder.Eval(Container.DataItem,"em")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblpurl" Text='<%# DataBinder.Eval(Container.DataItem,"picurl")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblpcnt" Text='<%# DataBinder.Eval(Container.DataItem,"piccnt")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbleqpar" Text='<%# DataBinder.Eval(Container.DataItem,"origpareqid")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblsidi" Text='<%# DataBinder.Eval(Container.DataItem,"siteid")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbledesigi" Text='<%# DataBinder.Eval(Container.DataItem,"desig")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr>
                                                <td class="plainlabel transrowblue">
                                                    <asp:RadioButton AutoPostBack="True" OnCheckedChanged="GetEq1" ID="rbeqalt" Text='<%# DataBinder.Eval(Container.DataItem,"eqnum")%>'
                                                        runat="server" />
                                                </td>
                                                <td class="plainlabel transrowblue">
                                                    <asp:Label ID="lbleqdescalt" Text='<%# DataBinder.Eval(Container.DataItem,"eqdesc")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="plainlabel transrowblue">
                                                    <asp:Label ID="Label1alt" Text='<%# DataBinder.Eval(Container.DataItem,"sitename") + ", " + DataBinder.Eval(Container.DataItem,"dept_line") + ", " + DataBinder.Eval(Container.DataItem,"cell_name")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbleqidalt" Text='<%# DataBinder.Eval(Container.DataItem,"eqid")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbllocalt" Text='<%# DataBinder.Eval(Container.DataItem,"location")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblmodalt" Text='<%# DataBinder.Eval(Container.DataItem,"model")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbloemalt" Text='<%# DataBinder.Eval(Container.DataItem,"oem")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblseralt" Text='<%# DataBinder.Eval(Container.DataItem,"serial")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblsplalt" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblcbyalt" Text='<%# DataBinder.Eval(Container.DataItem,"createdby")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblmbyalt" Text='<%# DataBinder.Eval(Container.DataItem,"modifiedby")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblcdatealt" Text='<%# DataBinder.Eval(Container.DataItem,"createdate")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblmdatealt" Text='<%# DataBinder.Eval(Container.DataItem,"modifieddate")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblecralt" Text='<%# DataBinder.Eval(Container.DataItem,"ecr")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblcphalt" Text='<%# DataBinder.Eval(Container.DataItem,"cph")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblcmalt" Text='<%# DataBinder.Eval(Container.DataItem,"cm")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblephalt" Text='<%# DataBinder.Eval(Container.DataItem,"eph")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblemalt" Text='<%# DataBinder.Eval(Container.DataItem,"em")%>' runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblpurlalt" Text='<%# DataBinder.Eval(Container.DataItem,"picurl")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblpcntalt" Text='<%# DataBinder.Eval(Container.DataItem,"piccnt")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbleqparalt" Text='<%# DataBinder.Eval(Container.DataItem,"origpareqid")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lblsida" Text='<%# DataBinder.Eval(Container.DataItem,"siteid")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                                <td class="details">
                                                    <asp:Label ID="lbledesiga" Text='<%# DataBinder.Eval(Container.DataItem,"desig")%>'
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <table style="border-bottom: blue 1px solid; border-left: blue 1px solid; border-top: blue 1px solid;
                                    border-right: blue 1px solid" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="border-right: blue 1px solid; width: 20px;">
                                            <img id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid; width: 20px;">
                                            <img id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
                                                runat="server">
                                        </td>
                                        <td style="border-right: blue 1px solid" valign="middle" width="220" align="center">
                                            <asp:Label ID="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:Label>
                                        </td>
                                        <td style="border-right: blue 1px solid; width: 20px;">
                                            <img id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
                                                runat="server">
                                        </td>
                                        <td style="width: 20px">
                                            <img id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
                                                runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellspacing="0" cellpadding="1" width="1000">
                                    <tr>
                                        <td class="thdrsinglft" width="22">
                                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                                        </td>
                                        <td class="thdrsingrt label" width="978">
                                            <asp:Label ID="lang2180" runat="server">Copy Details</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trcopy" runat="server">
                            <td colspan="3">
                                <table id="tbleqrev" cellspacing="0" cellpadding="0" width="1000" runat="server">
                                    <tr>
                                        <td width="160">
                                        </td>
                                        <td width="190">
                                        </td>
                                        <td width="160">
                                        </td>
                                        <td width="190">
                                        </td>
                                        <td width="230">
                                        </td>
                                        <td width="70">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table>
                                                <tr>
                                                    <td id="tddepts" class="bluelabel" runat="server">
                                                        Use Departments
                                                    </td>
                                                    <td>
                                                        <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                    </td>
                                                    <td id="tdlocs1" class="bluelabel" runat="server">
                                                        Use Locations
                                                    </td>
                                                    <td>
                                                        <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                                                    </td>
                                                    <td class="label" align="center">
                                                        <input id="cbecryn" checked type="checkbox" name="cbecryn" runat="server"><asp:Label
                                                            ID="lang2183" runat="server">Copy ECR</asp:Label><input id="cbrat" checked type="checkbox"
                                                                name="cbrat" runat="server"><asp:Label ID="lang2184" runat="server">Copy Rationale</asp:Label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="cbmast" type="checkbox"
                                                            name="cbmast" runat="server">
                                                        Copy as New Master&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="a1" onclick="getchildren();"
                                                            href="#" runat="server">Assign to Child Records Only</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trdepts" class="details" runat="server">
                                        <td colspan="6">
                                            <table>
                                                <tr>
                                                    <td class="label" style="width: 110px">
                                                        Department
                                                    </td>
                                                    <td id="tddept" class="plainlabel" width="170" runat="server">
                                                    </td>
                                                    <td class="label" style="width: 110px">
                                                        Station\Cell
                                                    </td>
                                                    <td id="tdcell" class="plainlabel" runat="server" width="170">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trlocs" class="details" runat="server">
                                        <td colspan="6">
                                            <table>
                                                <tr>
                                                    <td class="label">
                                                        Location
                                                    </td>
                                                    <td id="tdloc2" class="plainlabel" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bluelabel">
                                            <asp:Label ID="lang2185" runat="server">New Equipment#</asp:Label>
                                        </td>
                                        <td class="plainlabel">
                                            <asp:TextBox ID="txtneweqnum" runat="server" MaxLength="50" Width="180px"></asp:TextBox>
                                        </td>
                                        <td class="label" colspan="2">
                                            <input id="cbfunc" onclick="checkopts('cbfunc');" checked type="checkbox" name="cbfunc"
                                                runat="server"><asp:Label ID="lang2186" runat="server">Functions</asp:Label><input
                                                    id="cbcomp" onclick="checkopts('cbcomp');" checked type="checkbox" name="cbcomp"
                                                    runat="server"><asp:Label ID="lang2187" runat="server">Components</asp:Label><input
                                                        id="cbpm" onclick="checktaskopts('cbpm');" checked type="checkbox" name="cbpm"
                                                        runat="server"><asp:Label ID="lang2188" runat="server">PM Tasks</asp:Label><input
                                                            id="cbtpm" onclick="checktaskopts('cbtpm');" checked type="checkbox" name="cbtpm"
                                                            runat="server"><asp:Label ID="lang2189" runat="server">TPM Tasks</asp:Label>
                                        </td>
                                        <td class="label" align="center">
                                            <input id="rbro" value="rbro" type="radio" name="rbroa" runat="server"><asp:Label
                                                ID="lang2190" runat="server">Revised Only</asp:Label><input id="rbai" value="rbai"
                                                    checked type="radio" name="rbroa" runat="server"><asp:Label ID="lang2191" runat="server">As Is</asp:Label>
                                        </td>
                                        <td align="right">
                                            <img id="ibtncopy" src="../images/appbuttons/bgbuttons/copy.gif" runat="server" onclick="return ibtncopy_onclick()"><asp:ImageButton
                                                ID="ibtncancel" runat="server" ImageUrl="../images/appbuttons/bgbuttons/return.gif"
                                                Visible="False"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellspacing="0" cellpadding="1" width="1000">
                                    <tr>
                                        <td class="thdrsinglft" width="22">
                                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                                        </td>
                                        <td class="thdrsingrt label" width="978">
                                            <asp:Label ID="lang2192" runat="server">Equipment Details</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <iframe style="background-color: transparent; width: 1000px; height: 280px" id="ifeqdets"
                                    src="pmlibeqdets.aspx?start=no" frameborder="no"  runat="server">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellspacing="0" cellpadding="1" width="1000">
                                    <tr>
                                        <td class="thdrsinglft" width="22">
                                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                                        </td>
                                        <td class="thdrsingrt label" width="978">
                                            <asp:Label ID="lang2193" runat="server">Function Details</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <iframe style="background-color: transparent; width: 1000px; height: 300px" id="iffudets"
                                    src="pmlibfudets.aspx?start=no" frameborder="no"  runat="server">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellspacing="0" cellpadding="1" width="1000">
                                    <tr>
                                        <td class="thdrsinglft" width="22">
                                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                                        </td>
                                        <td class="thdrsingrt label" width="978">
                                            <asp:Label ID="lang2194" runat="server">Component Details</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <iframe style="background-color: transparent; width: 1000px; height: 280px" id="ifcodets"
                                    src="pmlibcodets.aspx?start=no" frameborder="no"  runat="server">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellspacing="0" cellpadding="1" width="1000">
                                    <tr>
                                        <td class="thdrsinglft" width="22">
                                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                                        </td>
                                        <td class="thdrsingrt label" width="978">
                                            <asp:Label ID="lang2195" runat="server">PM Task Details</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <iframe style="background-color: transparent; width: 1000px; height: 280px" id="ifpmtaskdets"
                                    src="pmlibpmtaskdets.aspx?start=no" frameborder="no"  runat="server">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellspacing="0" cellpadding="1" width="1000">
                                    <tr>
                                        <td class="thdrsinglft" width="22">
                                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                                        </td>
                                        <td class="thdrsingrt label" width="978">
                                            <asp:Label ID="lang2196" runat="server">TPM Task Details</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <iframe style="width: 1000px; height: 280px" id="iftpmtaskdets" src="pmlibtpmtaskdets.aspx?start=no"
                                    frameborder="no" runat="server"></iframe>
                            </td>
                        </tr>
                </table>
            </td>
        </tr>
        </TBODY>
    </table>
    <table style="z-index: 1; position: absolute; top: 24px; left: 4px" id="tblbot" class="details"
        cellspacing="0" cellpadding="1" width="1000" runat="server">
        <tr>
            <td colspan="3">
                <table cellspacing="0" cellpadding="1" width="1000">
                    <tr>
                        <td class="thdrsinglft" width="22">
                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                        </td>
                        <td class="thdrsingrt label" width="978">
                            <asp:Label ID="lang2197" runat="server">New Equipment Details</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" id="tderr" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="3" id="tderr2" runat="server">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <iframe style="background-color: transparent; width: 1000px; height: 330px" id="ifneweq"
                    src="pmlibneweqdets.aspx?start=no" frameborder="no"  runat="server">
                </iframe>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table cellspacing="0" cellpadding="1" width="1000">
                    <tr>
                        <td class="thdrsinglft" width="22">
                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                        </td>
                        <td class="thdrsingrt label" width="978">
                            <asp:Label ID="lang2198" runat="server">New Function Details</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <iframe style="width: 1000px; height: 350px" id="ifnewfu" src="pmlibnewfudets.aspx?start=no"
                    frameborder="no" runat="server"></iframe>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table cellspacing="0" cellpadding="1" width="1000">
                    <tr>
                        <td class="thdrsinglft" width="22">
                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                        </td>
                        <td class="thdrsingrt label" width="978">
                            <asp:Label ID="lang2199" runat="server">New Component Details</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <iframe style="width: 1000px; height: 350px" id="ifnewcodets" src="pmlibnewcodets.aspx?start=no"
                    frameborder="no" runat="server"></iframe>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table cellspacing="0" cellpadding="1" width="1000">
                    <tr>
                        <td class="thdrsinglft" width="22">
                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                        </td>
                        <td class="thdrsingrt label" width="978">
                            <asp:Label ID="lang2200" runat="server">New PM Task Details</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <iframe style="width: 1000px; height: 330px" id="ifnewpmtaskdets" src="pmlibnewpmtaskdets.aspx?start=no"
                    frameborder="no" runat="server"></iframe>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table cellspacing="0" cellpadding="1" width="1000">
                    <tr>
                        <td class="thdrsinglft" width="22">
                            <img border="0" src="../images/appbuttons/minibuttons/eqarch.gif">
                        </td>
                        <td class="thdrsingrt label" width="978">
                            <asp:Label ID="lang2201" runat="server">New TPM Task Details</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <iframe style="width: 1000px; height: 330px" id="ifnewtpmtaskdets" src="pmlibnewtpmtaskdets.aspx?start=no"
                    frameborder="no" runat="server"></iframe>
            </td>
        </tr>
    </table>
    <input id="lblro" type="hidden" name="lblro" runat="server">
    <input id="lbldb" type="hidden" name="lbldb" runat="server">
    <input id="lblpsid" type="hidden" name="lblpsid" runat="server">
    <input id="lblsid" type="hidden" name="lblsid" runat="server">
    <input id="lblcid" type="hidden" name="lblcid" runat="server">
    <input id="txtpg" type="hidden" name="txtpg" runat="server">
    <input id="txtpgcnt" type="hidden" name="txtpgcnt" runat="server"><input id="spdivy"
        type="hidden" name="spdivy" runat="server">
    <input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden"
        runat="server" />
    <input id="lbldid" type="hidden" name="lbldid" runat="server">
    <input id="lbleq" type="hidden" name="lbleq" runat="server"><input id="lblfu" type="hidden"
        name="lblfu" runat="server">
    <input id="lblsubmit" type="hidden" name="lblsubmit" runat="server"><input id="lblcellchk"
        type="hidden" name="lblcellchk" runat="server">
    <input id="lblclid" type="hidden" name="lblclid" runat="server"><input id="lbloloc"
        type="hidden" name="lbloloc" runat="server">
    <input id="lblnewid" type="hidden" name="lblnewid" runat="server">
    <input id="lblappstr" type="hidden" name="lblappstr" runat="server">
    <input id="lbloserv" type="hidden" name="lbloserv" runat="server"><input id="lbloldrbi"
        type="hidden" name="lbloldrbi" runat="server">
    <input id="lblret" type="hidden" name="lblret" runat="server">
    <input id="lblchkchngd" type="hidden" name="lblchkchngd" runat="server">
    <input id="lblt1" type="hidden" name="lblt1" runat="server">
    <input id="lblt2" type="hidden" name="lblt2" runat="server">
    <input id="lblt3" type="hidden" name="lblt3" runat="server">
    <input id="lbltl" type="hidden" name="lbltl" runat="server">
    <input id="lblcs" type="hidden" name="lblcs" runat="server">
    <input id="lblce" type="hidden" name="lblce" runat="server">
    <input id="lblsort" type="hidden" name="lblsort" runat="server"><input id="lblcurreqsort"
        type="hidden" name="lblcurreqsort" runat="server">
    <input id="lblcopyret" type="hidden" name="lblcopyret" runat="server">
    <input id="lbloldsid" type="hidden" name="lbloldsid" runat="server">
    <input id="lblstrmsg" type="hidden" name="lblstrmsg" runat="server">
    <input id="lblfslang" type="hidden" name="lblfslang" runat="server">
    <input id="lbllid" type="hidden" runat="server">
    <input id="lblloc" type="hidden" runat="server">
    <input id="lbldept" type="hidden" runat="server">
    <input type="hidden" id="lblcell" runat="server">
    <input type="hidden" id="lblflag" runat="server" />
    <input type="hidden" id="lblmdb" runat="server" />
    <input type="hidden" id="lblcpyeqid" runat="server" />
    <input type="hidden" id="lblhaschildrecs" runat="server" />
    <input type="hidden" id="lblismaster" runat="server" />
    </form>
</body>
</html>
