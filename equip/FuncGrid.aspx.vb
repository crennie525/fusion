

'********************************************************
'*
'********************************************************




Imports System.Data.SqlClient

Public Class FuncGrid
    Inherits System.Web.UI.Page
	Protected WithEvents lang2391 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2390 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2389 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, eqid, se, Login, sql, chk, ro, lid, ustr, dept, loc, cell, eq, who, fuid As String
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents lblsort As System.Web.UI.WebControls.Label
    Protected WithEvents ErrorLabel As System.Web.UI.WebControls.Label
    Protected WithEvents txtfu As System.Web.UI.WebControls.TextBox
    Protected WithEvents addfunc As System.Web.UI.WebControls.ImageButton
    Dim ds As DataSet
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcurrsort As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim dr As SqlDataReader
    Protected WithEvents lbluser As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents spy As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblrouting As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdeq As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqd As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents txtnewfunc As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblclid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ibtnreturn As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lbleqchk As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbllid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents ifsession As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblsessrefresh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblustr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblwho As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbldept As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcell As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblloc As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqidh As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleq As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim func As New Utilities
    Dim func2 As New Utilities
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgfunc As System.Web.UI.WebControls.DataGrid
    Protected WithEvents xcoord As System.Web.UI.WebControls.TextBox
    Protected WithEvents ycoord As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Property PostBackSpy() As Integer
        Get
            Return Convert.ToInt32(spy.Value)
        End Get
        Set(ByVal Value As Integer)
            spy.Value = Value.ToString()
        End Set
    End Property
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load



        

        If Not IsPostBack Then
            Try
                who = Request.QueryString("who").ToString
                lblwho.Value = who
                dept = Request.QueryString("dept").ToString
                lbldept.Value = dept
                cell = Request.QueryString("cell").ToString
                lblcell.Value = cell
                loc = Request.QueryString("loc").ToString
                lblloc.Value = loc
                eqid = Request.QueryString("eqid").ToString
                lbleqidh.Value = eqid
                eq = Request.QueryString("eq").ToString
                lbleq.Value = eq
                fuid = Request.QueryString("fuid").ToString
                lblfuid.Value = fuid
            Catch ex As Exception

            End Try
            ustr = Request.QueryString("ustr").ToString
            lblustr.Value = ustr
            GetDGLangs()

            GetFSLangs()

            Try
                lblfslang.Value = HttpContext.Current.Session("curlang").ToString()
            Catch ex As Exception
                Dim dlang As New mmenu_utils_a
                lblfslang.Value = dlang.AppDfltLang
            End Try
            GetBGBLangs()
            'Put user code to initialize the page here
            Dim urlname As String = System.Configuration.ConfigurationManager.AppSettings("custAppUrl")
            Try
                Login = HttpContext.Current.Session("Logged_IN").ToString()
            Catch ex As Exception
                urlname = urlname & "?logout=yes"
                Response.Redirect(urlname)
            End Try

            Try
                Dim sessref As String = System.Configuration.ConfigurationManager.AppSettings("sessRefreshDialog")
                Dim sessrefi As Integer = sessref * 1000 * 60
                lblsessrefresh.Value = sessrefi
            Catch ex As Exception
                lblsessrefresh.Value = "300000"
            End Try
            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            If ro = "1" Then
                dgfunc.Columns(0).Visible = False
                addfunc.ImageUrl = "../images/appbuttons/bgbuttons/badddis.gif"
                addfunc.Enabled = False
            End If
            Dim sid, did, clid As String
            chk = Request.QueryString("chk").ToString
            lblchk.Value = chk
            cid = Request.QueryString("cid").ToString
            lblcid.Value = cid
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            sid = Request.QueryString("sid").ToString
            lblsid.Value = sid
            did = Request.QueryString("did").ToString
            lbldid.Value = did
            clid = Request.QueryString("clid").ToString
            lblclid.Value = clid
            lid = Request.QueryString("lid").ToString
            lbllid.Value = lid
            Dim user As String = HttpContext.Current.Session("username").ToString
            lbluser.Value = user
            lblsort.Text = "Routing Ascending"
            lblcurrsort.Value = "routing asc"
            Try
                func.Open()
            Catch ex As Exception
                func2.Open()
            End Try


            GetHead(eqid)
            BindGrid("routing asc")
            Try
                func.Dispose()
            Catch ex As Exception
                func2.Dispose()
            End Try


        End If
        'addfunc.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/yadd.gif'")
        'addfunc.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/badd.gif'")
        'ibtnreturn.Attributes.Add("onmouseover", "this.src='../images/appbuttons/bgbuttons/returnhov.gif'")
        'ibtnreturn.Attributes.Add("onmouseout", "this.src='../images/appbuttons/bgbuttons/return.gif'")
    End Sub
    Private Sub GetHead(ByVal eqid As String)
        Dim lockedby As String
        sql = "select eqnum, eqdesc, locked, lockedby from equipment where eqid = '" & eqid & "'"
        Try
            dr = func.GetRdrData(sql)
        Catch ex As Exception
            dr = func2.GetRdrData(sql)
        End Try


        While dr.Read
            tdeq.InnerHtml = dr.Item("eqnum").ToString
            tdeqd.InnerHtml = dr.Item("eqdesc").ToString
            lbleqchk.Value = dr.Item("locked").ToString
            lockedby = dr.Item("lockedby").ToString
        End While
        dr.Close()
        If lbleqchk.Value = "1" Then
            If lbluser.Value <> lockedby Then
                addfunc.Attributes.Add("class", "details")
                Dim strMessage As String = tmod.getmsg("cdstr963", "FuncGrid.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Else
                lbleqchk.Value = "0"
            End If
        End If
    End Sub
    Private Sub BindGrid(ByVal sortExp As String)
        eqid = lbleqid.Value
        sql = "select f.*, e.locked as 'eqloc' from functions f " _
        + "left join equipment e on f.eqid = e.eqid " _
        + "where f.eqid = '" & eqid & "' order by " & sortExp
        Try
            dr = func.GetRdrData(sql)
        Catch ex As Exception
            dr = func2.GetRdrData(sql)
        End Try

        dgfunc.DataSource = dr
        dgfunc.DataBind()
        dr.Close()
    End Sub

    Private Sub dgfunc_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.EditCommand
        Dim eqchk As String
        eqchk = lbleqchk.Value 'CType(e.Item.FindControl("lbleqlock"), Label).Text
        If eqchk <> "1" Then
            dgfunc.Columns(4).Visible = True
            Dim fid As String
            fid = CType(e.Item.FindControl("lblfuid"), Label).Text
            txtfu.Text = fid
            dgfunc.Columns(4).Visible = False
            lblrouting.Value = CType(e.Item.FindControl("lblroute"), Label).Text
            se = lblcurrsort.Value
            dgfunc.EditItemIndex = e.Item.ItemIndex
            func.Open()
            BindGrid(se)
            func.Dispose()
        End If

    End Sub

    Private Sub dgfunc_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.UpdateCommand
        Try
            func.Open()
        Catch ex As Exception
            func2.Open()
        End Try

        cid = lblcid.Value
        eqid = lbleqid.Value
        Dim rte As String = CType(e.Item.FindControl("txtrouting"), TextBox).Text
        Dim orte As String = lblrouting.Value
        Dim rtechk As Long
        Try
            rtechk = System.Convert.ToInt32(rte)
        Catch ex As Exception
            Dim strMessage As String =  tmod.getmsg("cdstr964" , "FuncGrid.aspx.vb")
 
            Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            Exit Sub
        End Try
        'If Len(orte) = 0 Then
        'orte = rte
        'End If
        Dim fun, des, fid As String
        fun = CType(e.Item.FindControl("txtfunum"), TextBox).Text
        fun = func.ModString1(fun)

        If Len(fun) = 0 Then
            ErrorLabel.Text = "Name/Code Cannot Be NULL"
        Else
            Dim fcnt As Integer
            sql = "select count(*) from functions " _
            + "where eqid = '" & eqid & "' and func = '" & fun & "'"
            fcnt = func.Scalar(sql)
            If fcnt > 1 And fun <> "New Function" Then
                ErrorLabel.Text = "Name/Code not unique for Equipment"
            Else
                des = CType(e.Item.FindControl("txtdesc"), TextBox).Text
                des = func.ModString1(des)
                Dim spl As String = CType(e.Item.FindControl("txtspl"), TextBox).Text
                spl = func.ModString1(spl)
                fid = txtfu.Text 'CType(e.Item.FindControl("txtfuid"), TextBox).Text
                sql = "update functions set " _
                + "compid = '" & cid & "', " _
                + "eqid = '" & eqid & "', " _
                + "func_desc = '" & des & "', " _
                 + "func = '" & fun & "', " _
                + "spl = '" & spl & "' " _
                + "where func_id = '" & fid & "'"
                Try
                    func.Update(sql)
                Catch ex As Exception
                    Try
                        func2.Update(sql)
                    Catch ex1 As Exception
                        Dim strMessage As String = tmod.getmsg("cdstr965", "FuncGrid.aspx.vb")

                        Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
                    End Try
                   
                End Try
                dgfunc.EditItemIndex = -1
            End If
            If rte <> orte And rte <> "0" Then
                sql = "usp_rerouteFunctions '" & eqid & "', '" & rte & "', '" & orte & "'"
                Try
                    func.Update(sql)
                Catch ex As Exception
                    func2.Update(sql)
                End Try

            ElseIf rte = "0" Then
                Dim strMessage As String =  tmod.getmsg("cdstr966" , "FuncGrid.aspx.vb")
 
                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            se = lblcurrsort.Value
            BindGrid(se)
        End If
        Try
            func.Dispose()
        Catch ex As Exception
            func2.Dispose()
        End Try

    End Sub

    Private Sub dgfunc_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgfunc.CancelCommand
        dgfunc.EditItemIndex = -1
        se = lblcurrsort.Value
        Try
            func.Open()
        Catch ex As Exception
            func2.Open()
        End Try

        BindGrid(se)
        Try
            func.Dispose()
        Catch ex As Exception
            func2.Dispose()
        End Try
    End Sub

    Private Sub dgfunc_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgfunc.SortCommand
        Try
            func.Open()
        Catch ex As Exception
            func2.Open()
        End Try

        BindGrid(e.SortExpression)
        Try
            func.Dispose()
        Catch ex As Exception
            func2.Dispose()
        End Try

        Dim se As String = e.SortExpression
        lblcurrsort.Value = se
        Select Case se
            Case "func desc"
                dgfunc.Columns(2).SortExpression = "func asc"
                lblsort.Text = "Function# Descending"
            Case "func asc"
                dgfunc.Columns(2).SortExpression = "func desc"
                lblsort.Text = "Function# Ascending"
            Case "routing desc"
                dgfunc.Columns(1).SortExpression = "routing asc"
                lblsort.Text = "Routing Descending"
            Case "routing asc"
                dgfunc.Columns(1).SortExpression = "routing desc"
                lblsort.Text = "Routing Ascending"
        End Select
    End Sub

    Private Sub addfunc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles addfunc.Click

        Dim fu As String = txtnewfunc.Text
        fu = func.ModString1(fu)
        If Len(fu) > 0 Then
            Try
                func.Open()
            Catch ex As Exception
                func2.Open()
            End Try

            cid = lblcid.Value
            eqid = lbleqid.Value
            Dim user As String = lbluser.Value
            Dim ustr As String = Replace(user, "'", Chr(180), , , vbTextCompare)
            Dim fucnt As String
            sql = "select count(*) from functions where func = '" & fu & "' and eqid = '" & eqid & "'"
            Try
                fucnt = func.Scalar(sql)
            Catch ex As Exception
                fucnt = func2.Scalar(sql)
            End Try

            If fucnt = 0 Then
                Dim fudesc As String = "TBD"
                Dim fuspl As String = "TBD"
                sql = "usp_addFunc '" & cid & "', '" & eqid & "', '" & fu & "', '" & fudesc & "', '" & fuspl & "', '" & ustr & "'"
                Try
                    func.Update(sql)
                Catch ex As Exception
                    func2.Update(sql)
                End Try

                se = lblcurrsort.Value
                BindGrid(se)
                txtnewfunc.Text = ""
            Else
                Dim strMessage As String = tmod.getmsg("cdstr967", "FuncGrid.aspx.vb")

                Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
            End If
            Try
                func.Dispose()
            Catch ex As Exception
                func2.Dispose()
            End Try

        End If

    End Sub

    Private Sub ibtnreturn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnreturn.Click
        Dim sid, did, clid, eqid, lid As String
        chk = lblchk.Value
        cid = lblcid.Value
        sid = lblsid.Value
        did = lbldid.Value
        clid = lblclid.Value
        eqid = lbleqid.Value
        lid = lbllid.Value
        ustr = lblustr.Value
        who = lblwho.Value
        dept = lbldept.Value
        cell = lblcell.Value
        loc = lblloc.Value
        eqid = lbleqidh.Value
        eq = lbleq.Value
        fuid = lblfuid.Value
        If chk = "yes" Then
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=eq&start=yes&ret=yes&chk=yes&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&usrname=" + ustr + "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&eq=" & eq & "&fuid=" & fuid & "&coid=")
            Else
                Response.Redirect("eqmain2.aspx?lvl=eq&start=yes&ret=yes&chk=yes&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&ustr=" + ustr)
            End If

        ElseIf chk = "no" Then
            did = lbldid.Value
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=eq&start=yes&ret=yes&chk=no&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&usrname=" + ustr + "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&eq=" & eq & "&fuid=" & fuid & "&coid=")
            Else
                Response.Redirect("eqmain2.aspx?lvl=eq&start=yes&ret=yes&chk=no&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&ustr=" + ustr)
            End If

        ElseIf chk = "nn" Then
            If who = "tab" Then
                Response.Redirect("../appspmo123tab/pmo123tabmain.aspx?who=tabret&lvl=eq&start=yes&ret=yes&chk=nn&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=" & did & "&clid=" & clid & "&eqid=" & eqid & "&usrname=" + ustr + "&dept=" & dept & "&cell=" & cell & "&loc=" & loc & "&eq=" & eq & "&fuid=" & fuid & "&coid=")
            Else
                Response.Redirect("eqmain2.aspx?lvl=eq&start=yes&ret=yes&chk=nn&lid=" & lid & "&cid=" & cid & "&sid=" & sid & "&did=&clid=&eqid=" & eqid & "&ustr=" + ustr)
            End If

        Else
            'Dim strMessage As String = chk
            'Utilities.CreateMessageAlert(Me, strMessage, "strKey1")
        End If
    End Sub
	



    Private Sub GetDGLangs()
        Dim dlabs As New dglabs
        Try
            dgfunc.Columns(0).HeaderText = dlabs.GetDGPage("FuncGrid.aspx", "dgfunc", "0")
        Catch ex As Exception
        End Try
        Try
            dgfunc.Columns(1).HeaderText = dlabs.GetDGPage("FuncGrid.aspx", "dgfunc", "1")
        Catch ex As Exception
        End Try
        Try
            dgfunc.Columns(2).HeaderText = dlabs.GetDGPage("FuncGrid.aspx", "dgfunc", "2")
        Catch ex As Exception
        End Try

    End Sub







    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label9.Text = axlabs.GetASPXPage("FuncGrid.aspx", "Label9")
        Catch ex As Exception
        End Try
        Try
            lang2389.Text = axlabs.GetASPXPage("FuncGrid.aspx", "lang2389")
        Catch ex As Exception
        End Try
        Try
            lang2390.Text = axlabs.GetASPXPage("FuncGrid.aspx", "lang2390")
        Catch ex As Exception
        End Try
        Try
            lang2391.Text = axlabs.GetASPXPage("FuncGrid.aspx", "lang2391")
        Catch ex As Exception
        End Try
        Try
            lblsort.Text = axlabs.GetASPXPage("FuncGrid.aspx", "lblsort")
        Catch ex As Exception
        End Try

    End Sub





    Private Sub GetBGBLangs()
        Dim lang As String = lblfslang.value
        Try
            If lang = "eng" Then
                addfunc.Attributes.Add("src", "../images2/eng/bgbuttons/badd.gif")
            ElseIf lang = "fre" Then
                addfunc.Attributes.Add("src", "../images2/fre/bgbuttons/badd.gif")
            ElseIf lang = "ger" Then
                addfunc.Attributes.Add("src", "../images2/ger/bgbuttons/badd.gif")
            ElseIf lang = "ita" Then
                addfunc.Attributes.Add("src", "../images2/ita/bgbuttons/badd.gif")
            ElseIf lang = "spa" Then
                addfunc.Attributes.Add("src", "../images2/spa/bgbuttons/badd.gif")
            End If
        Catch ex As Exception
        End Try
        Try
            If lang = "eng" Then
                ibtnreturn.Attributes.Add("src", "../images2/eng/bgbuttons/return.gif")
            ElseIf lang = "fre" Then
                ibtnreturn.Attributes.Add("src", "../images2/fre/bgbuttons/return.gif")
            ElseIf lang = "ger" Then
                ibtnreturn.Attributes.Add("src", "../images2/ger/bgbuttons/return.gif")
            ElseIf lang = "ita" Then
                ibtnreturn.Attributes.Add("src", "../images2/ita/bgbuttons/return.gif")
            ElseIf lang = "spa" Then
                ibtnreturn.Attributes.Add("src", "../images2/spa/bgbuttons/return.gif")
            End If
        Catch ex As Exception
        End Try

    End Sub

End Class
