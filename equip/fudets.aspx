<%@ Page Language="vb" AutoEventWireup="false" Codebehind="fudets.aspx.vb" Inherits="lucy_r12.fudets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>fudets</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body >
		<form id="form1" method="post" runat="server">
			<table width="600">
				<tr>
					<td class="label" style="width: 120px"><asp:Label id="lang2300" runat="server">Function Name</asp:Label></td>
					<td class="plainlabel" id="tdfunc" runat="server" width="480"></td>
				</tr>
				<TR>
					<td class="label"><asp:Label id="lang2301" runat="server">Special Identifier</asp:Label></td>
					<td class="plainlabel" id="tdspl" runat="server"></td>
				</TR>
				<tr>
					<td class="label"><asp:Label id="lang2302" runat="server">Component Count</asp:Label></td>
					<td class="plainlabel" id="tdcompcnt" runat="server"></td>
				</tr>
				<TR>
					<td class="label"><asp:Label id="lang2303" runat="server">Task Count</asp:Label></td>
					<td class="plainlabel" id="tdtaskcnt" runat="server"></td>
				</TR>
			</table>
			<input type="hidden" id="lblfuid" runat="server" NAME="lbleqid">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
