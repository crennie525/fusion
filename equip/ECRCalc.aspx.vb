

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Public Class ECRCalc
    Inherits System.Web.UI.Page
	Protected WithEvents lang2141 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2140 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2139 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2138 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2137 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2136 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2135 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2134 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2133 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2132 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2131 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2130 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2129 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2128 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2127 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2126 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2125 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2124 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2123 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2122 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2121 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2120 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2119 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2118 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2117 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2116 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2115 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2114 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2113 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2112 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2111 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2110 As System.Web.UI.WebControls.Label

	Protected WithEvents lang2109 As System.Web.UI.WebControls.Label

    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim cid, eqid, sql, ro As String
    Dim dr As SqlDataReader
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents lbleqnum As System.Web.UI.WebControls.Label
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents lbleqdesc As System.Web.UI.WebControls.Label
    Protected WithEvents lblcatid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblro As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim calc As New Utilities

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents f11A As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f11B As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f11C As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f12A As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f12B As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f12C As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f13A As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f13B As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f13C As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f13D As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f21A As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f21B As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f21C As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f21D As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f22A As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f22B As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f22C As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f23A As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f23B As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f23C As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f23D As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f24A As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents f24B As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents ifwt As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblcid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
	GetFSLangs()

Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then

            Try
                ro = HttpContext.Current.Session("ro").ToString
            Catch ex As Exception
                ro = "0"
            End Try
            lblro.Value = ro
            cid = "0" 'Request.QueryString("cid").ToString
            lblcid.Value = cid
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            ifwt.Attributes.Add("src", "levels.aspx?cid=" & cid & "&id=11A" & "&lbl=Health and Safety Impact&eqid=" & eqid & "&ro=" & ro)
            calc.Open()
            GetEq(cid, eqid)
            calc.Close()
            lblcatid.Value = "11A"
        End If
    End Sub
    Private Sub GetEq(ByVal cid As String, ByVal eqid As String)
        sql = "select eqnum, eqdesc from equipment where eqid = '" & eqid & "'"
        dr = calc.GetRdrData(sql)
        If dr.Read Then
            lbleqnum.Text = dr.Item("eqnum").ToString
            lbleqdesc.Text = dr.Item("eqdesc").ToString
        End If
        dr.Close()
    End Sub

	









    Private Sub GetFSLangs()
        Dim axlabs As New aspxlabs
        Try
            Label7.Text = axlabs.GetASPXPage("ECRCalc.aspx", "Label7")
        Catch ex As Exception
        End Try
        Try
            Label8.Text = axlabs.GetASPXPage("ECRCalc.aspx", "Label8")
        Catch ex As Exception
        End Try
        Try
            lang2109.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2109")
        Catch ex As Exception
        End Try
        Try
            lang2110.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2110")
        Catch ex As Exception
        End Try
        Try
            lang2111.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2111")
        Catch ex As Exception
        End Try
        Try
            lang2112.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2112")
        Catch ex As Exception
        End Try
        Try
            lang2113.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2113")
        Catch ex As Exception
        End Try
        Try
            lang2114.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2114")
        Catch ex As Exception
        End Try
        Try
            lang2115.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2115")
        Catch ex As Exception
        End Try
        Try
            lang2116.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2116")
        Catch ex As Exception
        End Try
        Try
            lang2117.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2117")
        Catch ex As Exception
        End Try
        Try
            lang2118.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2118")
        Catch ex As Exception
        End Try
        Try
            lang2119.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2119")
        Catch ex As Exception
        End Try
        Try
            lang2120.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2120")
        Catch ex As Exception
        End Try
        Try
            lang2121.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2121")
        Catch ex As Exception
        End Try
        Try
            lang2122.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2122")
        Catch ex As Exception
        End Try
        Try
            lang2123.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2123")
        Catch ex As Exception
        End Try
        Try
            lang2124.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2124")
        Catch ex As Exception
        End Try
        Try
            lang2125.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2125")
        Catch ex As Exception
        End Try
        Try
            lang2126.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2126")
        Catch ex As Exception
        End Try
        Try
            lang2127.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2127")
        Catch ex As Exception
        End Try
        Try
            lang2128.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2128")
        Catch ex As Exception
        End Try
        Try
            lang2129.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2129")
        Catch ex As Exception
        End Try
        Try
            lang2130.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2130")
        Catch ex As Exception
        End Try
        Try
            lang2131.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2131")
        Catch ex As Exception
        End Try
        Try
            lang2132.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2132")
        Catch ex As Exception
        End Try
        Try
            lang2133.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2133")
        Catch ex As Exception
        End Try
        Try
            lang2134.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2134")
        Catch ex As Exception
        End Try
        Try
            lang2135.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2135")
        Catch ex As Exception
        End Try
        Try
            lang2136.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2136")
        Catch ex As Exception
        End Try
        Try
            lang2137.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2137")
        Catch ex As Exception
        End Try
        Try
            lang2138.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2138")
        Catch ex As Exception
        End Try
        Try
            lang2139.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2139")
        Catch ex As Exception
        End Try
        Try
            lang2140.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2140")
        Catch ex As Exception
        End Try
        Try
            lang2141.Text = axlabs.GetASPXPage("ECRCalc.aspx", "lang2141")
        Catch ex As Exception
        End Try

    End Sub

End Class
