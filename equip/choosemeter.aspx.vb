﻿Imports System.Data.SqlClient
Imports System.Text

Public Class choosemeter
    Inherits System.Web.UI.Page
    Dim cm As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim meterid, meter, eqid, eqnum, retmeterid, retmeter, retunit, retwkuse, pmtskid, mfid As String
    Dim mtr, mtrid, uni, wku As String
    Dim skillid, skillqty, fuid, rdid As String
    Dim typ, skillo, skillido, skillqtyo, rdo, rdido, meterido, mfido, metero As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            eqid = Request.QueryString("eqid").ToString
            eqnum = Request.QueryString("eqnum").ToString
            meterid = Request.QueryString("meterid").ToString
            meter = Request.QueryString("meter").ToString
            pmtskid = Request.QueryString("pmtskid").ToString
            mfid = Request.QueryString("mfid").ToString
            If meterid = "" Then
                meterid = "0"
            End If
            lbleqid.Value = eqid
            lbleqnum.Value = eqnum
            tdeq.InnerHtml = eqnum
            lblmeterid.Value = meterid
            lblmeter.Value = meter
            lblpmtskid.Value = pmtskid
            lblmfid.Value = mfid

            fuid = Request.QueryString("fuid").ToString
            skillid = Request.QueryString("skillid").ToString
            skillqty = Request.QueryString("skillqty").ToString
            rdid = Request.QueryString("rdid").ToString
            lblfuid.Value = fuid
            lblskillid.Value = skillid
            lblskillqty.Value = skillqty
            lblrdid.Value = rdid

            typ = Request.QueryString("typ").ToString
            lbltyp.Value = typ
            If typ = "orig" Then
                skillido = Request.QueryString("skillido").ToString
                skillqtyo = Request.QueryString("skillqtyo").ToString
                rdido = Request.QueryString("rdido").ToString
                'skillo = Request.QueryString("skillo").ToString
                'rdo = Request.QueryString("rdo").ToString
                meterido = Request.QueryString("meterido").ToString
                If meterido = "" Then
                    meterido = "0"
                End If
                metero = Request.QueryString("metero").ToString
                mfido = Request.QueryString("mfido").ToString
                lblskillido.Value = skillido
                lblskillqtyo.Value = skillqtyo
                lblrdido.Value = rdido
                'lblskillo.Value = skillo
                'lblrdo.Value = rdo
                lblmfido.Value = mfido
                lblmetero.Value = metero
                lblmeterido.Value = meterido
            End If

            cm.Open()
            GetMeters(eqid)
            cm.Dispose()
        Else
            If Request.Form("lblsubmit") = "go" Then
                cm.Open()
                GoMeters()
                cm.Dispose()
                lblsubmit.Value = "return"
            ElseIf Request.Form("lblsubmit") = "ref" Then
                eqid = lbleqid.Value
                cm.Open()
                GetMeters(eqid)
                cm.Dispose()
                lblsubmit.Value = ""
            End If
            eqnum = lbleqnum.Value
            tdeq.InnerHtml = eqnum
        End If
    End Sub
    Private Sub GoMeters()
        'mtr, mtrid, uni, wku
        mtrid = lblretmeterid.Value
        mtr = lblretmeter.Value
        uni = lblretunit.Value
        wku = lblretwkuse.Value
        meterid = lblmeterid.Value
        pmtskid = lblpmtskid.Value

        mfid = lblmfid.Value

        eqid = lbleqid.Value
        fuid = lblfuid.Value
        skillid = lblskillid.Value
        skillqty = lblskillqty.Value
        rdid = lblrdid.Value

        skillido = lblskillido.Value
        skillqtyo = lblskillqtyo.Value
        rdido = lblrdido.Value
        meterido = lblmeterido.Value
        mfido = lblmfido.Value

        typ = lbltyp.Value
        If typ <> "orig" Then
            If meterid <> "0" Then
                sql = "update meterfreq set meterid = '" & mtrid & "', meterfreq = null, dayfreq = null, maxdays = null where mfid = '" & mfid & "'; "
                sql += "update pmtasks set meterid = '" & mtrid & "', usemeter = '1' where pmtskid = '" & pmtskid & "'"
                cm.Update(sql)
            ElseIf typ = "dad" Then
                'don't update for now - should force frequency selection before entry into pmtottime
                Exit Sub
            Else
                sql = "insert into meterfreq (meterid, pmtskid, eqid, funcid, skillid, skillqty, rdid) values " _
                    + "('" & mtrid & "','" & pmtskid & "','" & eqid & "','" & fuid & "','" & skillid & "','" & skillqty & "','" & rdid & "') " _
                    + "select @@identity"
                mfid = cm.strScalar(sql)
                sql = "update pmtasks set meterid = '" & mtrid & "', mfid = '" & mfid & "', usemeter = '1' where pmtskid = '" & pmtskid & "'"
                cm.Update(sql)
            End If
        ElseIf typ = "dad" Then
            'don't update for now - should force frequency selection before entry into pmtottime
            Exit Sub
        Else
            If meterid <> "0" And meterid <> meterido Then
                If meterido <> "0" Then
                    sql = "update meterfreqo set meterid = '" & mtrid & "', meterfreq = null, dayfreq = null, maxdays = null  where mfid = '" & mfido & "'; "
                    sql += "update pmtasks set meterido = '" & mtrid & "', usemetero = '1' where pmtskid = '" & pmtskid & "'"
                    cm.Update(sql)
                Else
                    sql = "insert into meterfreqo (meterid, pmtskid, eqid, funcid, skillid, skillqty, rdid) values " _
                        + "('" & mtrid & "','" & pmtskid & "','" & eqid & "','" & fuid & "','" & skillido & "','" & skillqtyo & "','" & rdido & "') " _
                    + "select @@identity"
                    mfido = cm.strScalar(sql)
                    sql = "update pmtasks set meterido = '" & mtrid & "', mfido = '" & mfido & "', usemetero = '1' where pmtskid = '" & pmtskid & "'"
                    cm.Update(sql)
                End If
            Else
                If meterid <> "0" And meterid = meterido Then
                    sql = "update meterfreq set meterid = '" & mtrid & "', meterfreq = null, dayfreq = null, maxdays = null  where mfid = '" & mfid & "'; "
                    sql += "update pmtasks set meterid = '" & mtrid & "', usemeter = '1' where pmtskid = '" & pmtskid & "'"
                    cm.Update(sql)
                Else
                    sql = "insert into meterfreq (meterid, pmtskid, eqid, funcid, skillid, skillqty, rdid) values " _
                        + "('" & mtrid & "','" & pmtskid & "','" & eqid & "','" & fuid & "','" & skillid & "','" & skillqty & "','" & rdid & "') " _
                    + "select @@identity"
                    mfid = cm.strScalar(sql)
                    sql = "update pmtasks set meterid = '" & mtrid & "', mfid = '" & mfid & "', usemeter = '1' where pmtskid = '" & pmtskid & "'"
                    cm.Update(sql)
                End If

                If meterido <> "0" Then
                    sql = "update meterfreqo set meterid = '" & mtrid & "' where mfid = '" & mfido & "'; "
                    sql += "update pmtasks set meterido = '" & mtrid & "', usemetero = '1' where pmtskid = '" & pmtskid & "'"
                    cm.Update(sql)
                Else
                    sql = "insert into meterfreqo (meterid, pmtskid, eqid, funcid, skillid, skillqty, rdid) values " _
                        + "('" & mtrid & "','" & pmtskid & "','" & eqid & "','" & fuid & "','" & skillido & "','" & skillqtyo & "','" & rdido & "') " _
                    + "select @@identity"
                    mfido = cm.strScalar(sql)
                    sql += "update pmtasks set meterido = '" & mtrid & "', mfido = '" & mfido & "', usemetero = '1' where pmtskid = '" & pmtskid & "'"
                    cm.Update(sql)
                End If
            End If
        End If

    End Sub
    Private Sub GetMeters(ByVal eqid As String)
        Dim sb As New StringBuilder
        sb.Append("<div style=""OVERFLOW: auto; WIDTH: 330px;  HEIGHT: 300px; BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"">")
        sb.Append("<table>")
        sb.Append("<tr>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""120"">Meter Id</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""100"">Units</td>")
        sb.Append("<td class=""btmmenu plainlabel"" width=""90"">Weekly Use</td>")
        sb.Append("</tr>")
        sql = "select * from meters where eqid = '" & eqid & "'"
        dr = cm.GetRdrData(sql)
        While dr.Read
            mtrid = dr.Item("meterid").ToString
            mtr = dr.Item("meter").ToString
            uni = dr.Item("unit").ToString
            wku = dr.Item("wkuse").ToString

            sb.Append("<tr>")
            sb.Append("<td class=""plainlabel"" width=""120""><a href=""#"" onclick=""getmeter('" & mtrid & "','" & mtr & "','" & uni & "','" & wku & "');"">" & mtr & "</a></td>")
            sb.Append("<td class=""plainlabel"" width=""100"">" & uni & "</td>")
            sb.Append("<td class=""plainlabel"" width=""90"">" & wku & "</td>")
            sb.Append("</tr>")
        End While
        dr.Close()
        sb.Append("</table></div>")
        tdmeters.InnerHtml = sb.ToString
    End Sub
End Class