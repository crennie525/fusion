﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="meterhist.aspx.vb" Inherits="lucy_r12.meterhist" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
    <script  type="text/javascript">
        function getminsrch() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var typ = "lup";
            var eReturn = window.showModalDialog("../apps/appgetdialog.aspx?typ=" + typ + "&site=" + sid + "&wo=" + wo, "", "dialogHeight:600px; dialogWidth:800px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~");
                document.getElementById("lbleqid").value = ret[4];
                document.getElementById("lbleq").value = ret[5];
                document.getElementById("tdeq").innerHTML = ret[5];
                document.getElementById("imgmtr").className = "view";
            }
        }
        function getlocs1() {
            var sid = document.getElementById("lblsid").value;
            var wo = "";
            var eReturn = window.showModalDialog("../locs/locget3dialog.aspx?typ=lup&sid=" + sid + "&wo=" + wo, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
            if (eReturn) {
                var ret = eReturn.split("~");
                //ret = lidi + "~" + lid + "~" + loc + "~" + eq + "~" + eqnum + "~" + fu + "~" + func + "~" + co + "~" + comp + "~" + lev;
                document.getElementById("lbleq").value = ret[4];
                document.getElementById("tdeq").innerHTML = ret[4];
                document.getElementById("lbleqid").value = ret[3];
                document.getElementById("imgmtr").className = "view";
            }
        }
        function getmeter() {
            var eqid = document.getElementById("lbleqid").value;
            if (eqid != "") {
                var eReturn = window.showModalDialog("meterlookup.aspx?typ=lup&eqid=" + eqid, "", "dialogHeight:620px; dialogWidth:900px; resizable=yes");
                if (eReturn) {
                    var ret = eReturn.split("~");
                    //meterid + "~" + munit + "~" + meter;
                    document.getElementById("lblmeterid").value = ret[0];
                    document.getElementById("lblmeter").value = ret[1];
                    document.getElementById("lblunit").value = ret[2];
                    document.getElementById("lblsubmit").value = "getmeter";
                    document.getElementById("form1").submit();
                }
            }
        }
        function getcal(fld) {
            var eReturn = window.showModalDialog("../controls/caldialog.aspx?who=" + fld, "", "dialogHeight:325px; dialogWidth:325px; resizable=yes");
            if (eReturn) {
                document.getElementById(fld).innerHTML = eReturn;
                document.getElementById("lblnewdate").value = eReturn;
            }
        }
        function addread() {
            var nread = document.getElementById("txtnewreading").value;
            var rdate = document.getElementById("lblnewdate").value;
            if (newdate == "") {
                alert("Reading Date Required")
            }
            else if (nread == "") {
                alert("New Reading Required")
            }
            else if(isNaN(nread)) {
                alert("New Reading Must Be a Numeric Value")
            }
            else {
                document.getElementById("lblsubmit").value = "addread";
                document.getElementById("form1").submit();
            }
        }
        function getnext() {
		
		var cnt = document.getElementById("txtpgcnt").value;
		var pg = document.getElementById("txtpg").value;
		pg = parseInt(pg);
		cnt = parseInt(cnt)
		if(pg<cnt) {
		document.getElementById("lblret").value = "next"
		document.getElementById("form1").submit();
		}
		}
		function getlast() {
		
		var cnt = document.getElementById("txtpgcnt").value;
		var pg = document.getElementById("txtpg").value;
		pg = parseInt(pg);
		cnt = parseInt(cnt)
		if(pg<cnt) {
		document.getElementById("lblret").value = "last"
		document.getElementById("form1").submit();
		}
		}
		function getprev() {
		
		var cnt = document.getElementById("txtpgcnt").value;
		var pg = document.getElementById("txtpg").value;
		pg = parseInt(pg);
		cnt = parseInt(cnt)
		if(pg>1) {
		document.getElementById("lblret").value = "prev"
		document.getElementById("form1").submit();
		}
		}
		function getfirst() {
		
		var cnt = document.getElementById("txtpgcnt").value;
		var pg = document.getElementById("txtpg").value;
		pg = parseInt(pg);
		cnt = parseInt(cnt)
		if(pg>1) {
		document.getElementById("lblret").value = "first"
		document.getElementById("form1").submit();
		}
		}
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
         <tr>
                <td class="thdrsing label" colspan="4">
                    Meter Readings
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td id="tddepts" class="bluelabel" runat="server">
                                Use Departments
                            </td>
                            <td>
                                <img onclick="getminsrch();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                            <td id="tdlocs1" class="bluelabel" runat="server">
                                Use Locations
                            </td>
                            <td>
                                <img onclick="getlocs1();" src="../images/appbuttons/minibuttons/magnifier.gif">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="bluelabel" style="width: 100px">
                                Equipment#
                            </td>
                            <td id="tdeq" class="plainlabel" runat="server" width="160">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                Meter
                            </td>
                            <td id="tdmeter" runat="server" class="plainlabel">
                            </td>
                            <td>
                                <img alt="" class="details" id="imgmtr" runat="server" onclick="getmeter();" src="../images/appbuttons/minibuttons/magnifier.gif" />
                            </td>
                        </tr>
                        <tr>
                            <td class="bluelabel">
                                Units
                            </td>
                            <td id="tdunits" runat="server" class="plainlabel">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr id="trnew" runat="server" class="details">
                            <td class="label">
                                Meter Reading
                            </td>
                            <td>
                                <asp:TextBox ID="txtnewreading" runat="server" style="width: 100px"></asp:TextBox>
                            </td>
                            <td class="label">
                                Reading Date
                            </td>
                            <td id="tddate" runat="server" class="plainlabel" style="width: 100px">
                            </td>
                            <td>
                                <img onclick="getcal('tddate');" alt="" src="../images/appbuttons/minibuttons/btncal2.gif"
                                    width="19" height="19">
                            </td>
                            <td>
                                <img id="imgreading" onmouseover="return overlib('Add New Reading', ABOVE, LEFT)"
                                    onmouseout="return nd()" onclick="addread();" src="../images/appbuttons/minibuttons/addmod.gif"
                                    runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                        <AlternatingRowStyle CssClass="ptransrowblue"></AlternatingRowStyle>
                        <RowStyle CssClass="ptransrow"></RowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Edit">
                                <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton7" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Meter Reading">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblread" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterreading") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtreading" runat="server" Width="80px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.meterreading") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reading Date">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbldate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.readingdate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblreaddate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.readingdate") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Entered By">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblby" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.enterby") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblbye" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.enterby") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Route">
                                <HeaderStyle Width="160px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmroute" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mroute") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmroute" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mroute") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PM">
                                <HeaderStyle Width="400px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblpm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblpme" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.pm") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  Visible="false">
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblmhist" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mhistid") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmhiste" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mhistid") %>'>
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
					<td align="center">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
        </table>
    </div>
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lbleq" runat="server" />
    <input type="hidden" id="lblmeter" runat="server" />
    <input type="hidden" id="lblmeterid" runat="server" />
    <input type="hidden" id="lblunit" runat="server" />
    <input type="hidden" id="lblenterby" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblsid" runat="server" />
    <input type="hidden" id="lblnewdate" runat="server" />
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblmode" runat="server" />
    </form>
</body>
</html>
