﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="emeter.aspx.vb" Inherits="lucy_r12.emeter" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
    <script  src="../scripts/overlib1.js" type="text/javascript"></script>
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
        <tr>
        <td class="plainlabelblue">
        For Meters that are not Continuous (e.g. Hours, Miles, etc.) Please Use the Measurements Module.
        <br />
        <br />
        Weekly Use should be the average number of units your meter advances in One Week.
        <br />
        </td>
        </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dgmeters" runat="server" CellPadding="0" GridLines="None" AllowPaging="True"
                        AllowCustomPaging="True" AutoGenerateColumns="False" ShowFooter="True" CellSpacing="1"
                        BackColor="transparent">
                        <FooterStyle BackColor="transparent" CssClass="plainlabel"></FooterStyle>
                        <AlternatingItemStyle CssClass="ptransrowblue"></AlternatingItemStyle>
                        <ItemStyle CssClass="ptransrow"></ItemStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Height="20px" Width="50px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="Imagebutton7" runat="server" ImageUrl="../images/appbuttons/minibuttons/lilpentrans.gif"
                                        CommandName="Edit" ToolTip="Edit This Failure Mode"></asp:ImageButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    &nbsp;
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/appbuttons/minibuttons/addwhite.gif"
                                        CommandName="Add"></asp:ImageButton>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton ID="Imagebutton8" runat="server" ImageUrl="../images/appbuttons/minibuttons/savedisk1.gif"
                                        CommandName="Update"></asp:ImageButton>
                                    <asp:ImageButton ID="Imagebutton9" runat="server" ImageUrl="../images/appbuttons/minibuttons/candisk1.gif"
                                        CommandName="Cancel"></asp:ImageButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblmeterid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'
                                        NAME="Label21">
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblmeteride" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meterid") %>'
                                        NAME="Label21">
                                    </asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Meter ID">
                                <HeaderStyle Width="120px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="lblmeter" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewmeter" runat="server" Width="154px" MaxLength="50">
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:TextBox ID="txtmeter" runat="server" Width="154px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.meter") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="shift desc" HeaderText="Units">
                                <HeaderStyle Width="100px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.unit") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewunit" runat="server" Width="60px" MaxLength="50" >
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                   <asp:TextBox ID="txtunit" runat="server" Width="60px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.unit") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Weekly Use">
                                <HeaderStyle Width="90px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;
                                    <asp:Label ID="lbluse" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.wkuse") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtnewuse" runat="server" Width="80px" MaxLength="50">
                                    </asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    &nbsp;
                                    <asp:TextBox ID="txtuse" runat="server" Width="80px" MaxLength="50" Text='<%# DataBinder.Eval(Container, "DataItem.wkuse") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Remove">
                                <HeaderStyle Width="64px" CssClass="btmmenu plainlabel"></HeaderStyle>
                                <ItemTemplate>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="Imagebutton16" runat="server" ImageUrl="../images/appbuttons/minibuttons/del.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="lblunitret" runat="server" />
    <input type="hidden" id="lbluret" runat="server" />
    <input type="hidden" id="lbleqid" runat="server" />
    <input type="hidden" id="lblold" runat="server" />
    </form>
</body>
</html>
