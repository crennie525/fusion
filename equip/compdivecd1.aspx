﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="compdivecd1.aspx.vb" Inherits="lucy_r12.compdivecd1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet"/>
    <script  type ="text/javascript">
        function getnext() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "next"
                document.getElementById("form1").submit();
            }
        }
        function getlast() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg < cnt) {
                document.getElementById("lblret").value = "last"
                document.getElementById("form1").submit();
            }
        }
        function getprev() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "prev"
                document.getElementById("form1").submit();
            }
        }
        function getfirst() {

            var cnt = document.getElementById("txtpgcnt").value;
            var pg = document.getElementById("txtpg").value;
            pg = parseInt(pg);
            cnt = parseInt(cnt)
            if (pg > 1) {
                document.getElementById("lblret").value = "first"
                document.getElementById("form1").submit();
            }
        }
        function saveecd1() {
            var compnum = document.getElementById("lblcompnum").value;
            var chk = document.getElementById("txtnewecd1").value;
            var chk1 = document.getElementById("lblecd1id").value;
            var chng = document.getElementById("lblchange").value;
            if (compnum == "") {
                alert("No New Component Name Selected")
            }
            else {
                if (chk1 == "") {
                    if (chk != "") {
                        var ret = chk
                        if (chk.length > 5) {
                            alert("New Error Code 1 Value Limited to 5 Characters")
                        }
                        else {
                            document.getElementById("lblsubmit").value = "go";
                            document.getElementById("form1").submit();
                        }
                    }
                }
                else {
                    var ret = chk1 + "~" + compnum;
                    window.parent.handlereturn(ret);
                }

            }
            
        }
        function checkret() {
            var chk = document.getElementById("lblret").value;
            var compnum = document.getElementById("lblcompnum").value;
            var chk1 = document.getElementById("lblecd1id").value;
            if (chk == "go") {
                var ret = chk1 + "~" + compnum;
                //alert(ret)
                window.parent.handlereturn(ret);
            }
        }
        function canit() {
            window.parent.handlereturn("can");
        }
       
        function getfm(fmi, fm, fmii) {
            var compnum = document.getElementById("lblcompnum").value;
            if (compnum == "" || compnum == fm) {
                document.getElementById("txtnewcomp").value = fm;
                document.getElementById("lblcompnum").value = fm;
                document.getElementById("lblecd1id").value = fmii;
                document.getElementById("lblecd1").value = fmi;
                document.getElementById("txtnewecd1").value = fmi;
            }
            else {
                var decision = confirm("Replace " + compnum + " with " + fm + "?")
                if (decision == true) {
                    document.getElementById("txtnewcomp").value = fm;
                    document.getElementById("lblnewcompnum").value = fm;
                    document.getElementById("lblcompnum").value = fm;
                    document.getElementById("lblecd1id").value = fmii;
                    document.getElementById("lblecd1").value = fmi;
                    document.getElementById("txtnewecd1").value = fmi;
                    document.getElementById("lblchange").value = "yes";
                }
                else {
                    alert("Action Cancelled")
                }  
            }
        }
        function refit() {
            document.getElementById("txtsearch").value = ""
            document.getElementById("lblret").value = "first"
            document.getElementById("form1").submit();
        }
    </script>
</head>
<body onload="checkret();">
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td class="redlabel" id="tdnc" runat="server">Enter New Component #</td>
    <td><asp:TextBox ID="txtnewcomp" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
    <td class="redlabel">Enter New Error Code 1 Value</td>
    <td><asp:TextBox ID="txtnewecd1" runat="server"></asp:TextBox></td>
    <td><img src="../images/appbuttons/minibuttons/savedisk1.gif" alt="" onclick="saveecd1();" /></td>
    <td><img id="img1" alt="" onclick="canit();" runat="server" src="../images/appbuttons/minibuttons/candisk1.gif" /></td>
    </tr>
    <tr>
    <td class="redlabel">or Select Existing</td>
    <td><asp:TextBox ID="txtsearch" runat="server"></asp:TextBox></td>
    <td><asp:imagebutton id="ibtnsearch" runat="server" CssClass="imgbutton" ImageUrl="../images/appbuttons/minibuttons/srchsm.gif"></asp:imagebutton></td>
    <td><img id="img2" alt="" onclick="refit();" runat="server" src="../images/appbuttons/minibuttons/refreshit.gif" /></td>
    </tr>
    <tr>
    <td colspan="4" >
    <div style="WIDTH: 370px; HEIGHT: 250px; OVERFLOW: auto" ID="fdiv" runat="server" class="btmbrdr">
    </div>
    </td>
    </tr>
    <tr>
					<td align="center" colspan="4">
						<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
							cellSpacing="0" cellPadding="0" width="300">
							<tr>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
										runat="server"></td>
								<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" width="220" align="center"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
								<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
										runat="server"></td>
								<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
										runat="server"></td>
							</tr>
						</table>
					</td>
				</tr>
    </table>
    <input id="txtpg" type="hidden"  runat="server"/> <input id="txtpgcnt" type="hidden"  runat="server"/>
    
    <input type="hidden" id="lblret" runat="server" />
    <input type="hidden" id="lblsubmit" runat="server" />
    <input type="hidden" id="lblecd1id" runat="server" />
    <input type="hidden" id="lblecd1" runat="server" />
    <input type="hidden" id="lblcompnum" runat="server" />
    <input type="hidden" id="lblnewcompnum" runat="server" />
    <input type="hidden" id="lblchange" runat="server" />
    </div>
    </form>
</body>
</html>
