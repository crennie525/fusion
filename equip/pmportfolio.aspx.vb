

'********************************************************
'*
'********************************************************



Imports System.Data.SqlClient
Imports System.Text
Imports System.Drawing.Imaging
Public Class pmportfolio
    Inherits System.Web.UI.Page
    Dim tmod As New transmod
    Protected WithEvents lblfslang As System.Web.UI.HtmlControls.HtmlInputHidden

    Dim port As New Utilities
    Dim sql As String
    Dim dr As SqlDataReader
    Dim typ, eqid, fuid, comid, ofuid, ocomid As String
    Dim sb As New StringBuilder
    Protected WithEvents lblfuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblsubmit As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblcur As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lbleqid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents tdpics As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdeqfu As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdfuco As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents lblcoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblofuid As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblocoid As System.Web.UI.HtmlControls.HtmlInputHidden
    Dim x As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
Try
lblfslang.value = HttpContext.Current.Session("curlang").ToString()
Catch ex As Exception
            Dim dlang As New mmenu_utils_a
lblfslang.value = dlang.AppDfltLang
End Try
'Put user code to initialize the page here
        If Not IsPostBack Then
            'typ = Request.QueryString("typ").ToString
            eqid = Request.QueryString("eqid").ToString
            lbleqid.Value = eqid
            fuid = Request.QueryString("fuid").ToString
            lblcur.Value = fuid
            lblofuid.Value = fuid
            comid = Request.QueryString("comid").ToString
            lblocoid.Value = comid
            port.Open()
            LoadEqPics(eqid)
            If fuid <> "0" Then
                LoadFUPics(eqid, fuid)
            Else
                sb.Append("<tr><td colspan=""4"" >")
                sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl450" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl451" , "pmportfolio.aspx.vb") & "</td></tr>")
                sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            End If
            If comid <> "0" Then
                LoadCOPics(fuid, comid)
            Else
                If fuid <> "0" Then
                    LoadCOFUPics(eqid, fuid)
                    LoadComponents(fuid)
                Else
                    sb.Append("<tr><td colspan=""4"" >")
                    sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                    sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                    sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl452" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                    'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Component Images</td></tr>")
                    sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                    sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl453" , "pmportfolio.aspx.vb") & "</td></tr>")
                    sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
                End If

            End If
            LoadFunctions(eqid)
            port.Dispose()
            sb.Append("<tr><td width=""100""></td>")
            sb.Append("<td width=""100""></td>")
            sb.Append("<td width=""100""></td>")
            sb.Append("<td width=""100""></td></tr>")
            sb.Append("</table>")
            tdpics.InnerHtml = sb.ToString
        Else
            If Request.Form("lblsubmit") = "getfunc" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                comid = "0" 'lblcoid.Value
                lblcur.Value = fuid
                port.Open()
                LoadEqPics(eqid)
                If fuid <> "0" Then
                    LoadFUPics(eqid, fuid)
                Else
                    sb.Append("<tr><td colspan=""4"" >")
                    sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                    sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                    sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl454" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                    sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                    sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl455" , "pmportfolio.aspx.vb") & "</td></tr>")
                End If
                If comid <> "0" Then
                    LoadCOPics(fuid, comid)
                Else
                    If fuid <> "0" Then
                        LoadCOFUPics(eqid, fuid)
                        LoadComponents(fuid)
                    Else
                        sb.Append("<tr><td colspan=""4"" >")
                        sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                        sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                        sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl456" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                        'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Component Images</td></tr>")
                        sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                        sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl457" , "pmportfolio.aspx.vb") & "</td></tr>")
                    End If

                End If
                LoadFunctions(eqid)
                port.Dispose()
                sb.Append("<tr><td width=""100""></td>")
                sb.Append("<td width=""100""></td>")
                sb.Append("<td width=""100""></td>")
                sb.Append("<td width=""100""></td></tr>")
                sb.Append("</table>")
                tdpics.InnerHtml = sb.ToString
            ElseIf Request.Form("lblsubmit") = "getcomp" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                fuid = lblfuid.Value
                comid = lblcoid.Value
                lblcur.Value = fuid
                port.Open()
                LoadEqPics(eqid)
                If fuid <> "0" Then
                    LoadFUPics(eqid, fuid)
                Else
                    sb.Append("<tr><td colspan=""4"" >")
                    sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                    sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                    sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl458" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                    sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                    sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl459" , "pmportfolio.aspx.vb") & "</td></tr>")
                End If
                If comid <> "0" Then
                    LoadCOPics(fuid, comid)
                Else
                    If fuid <> "0" Then
                        LoadCOFUPics(eqid, fuid)
                        LoadComponents(fuid)
                    Else
                        sb.Append("<tr><td colspan=""4"" >")
                        sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                        sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                        sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl460" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                        'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Component Images</td></tr>")
                        sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                        sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl461" , "pmportfolio.aspx.vb") & "</td></tr>")
                    End If

                End If
                LoadFunctions(eqid)
                port.Dispose()
                sb.Append("<tr><td width=""100""></td>")
                sb.Append("<td width=""100""></td>")
                sb.Append("<td width=""100""></td>")
                sb.Append("<td width=""100""></td></tr>")
                sb.Append("</table>")
                tdpics.InnerHtml = sb.ToString

            ElseIf Request.Form("lblsubmit") = "getofu" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                fuid = lblofuid.Value
                comid = "0" 'lblcoid.Value
                lblcur.Value = fuid
                port.Open()
                LoadEqPics(eqid)
                If fuid <> "0" Then
                    LoadFUPics(eqid, fuid)
                Else
                    sb.Append("<tr><td colspan=""4"" >")
                    sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                    sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                    sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl462" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                    sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                    sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl463" , "pmportfolio.aspx.vb") & "</td></tr>")
                End If
                If comid <> "0" Then
                    LoadCOPics(fuid, comid)
                Else
                    If fuid <> "0" Then
                        LoadCOFUPics(eqid, fuid)
                        LoadComponents(fuid)
                    Else
                        sb.Append("<tr><td colspan=""4"" >")
                        sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                        sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                        sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl464" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                        'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Component Images</td></tr>")
                        sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                        sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl465" , "pmportfolio.aspx.vb") & "</td></tr>")
                    End If

                End If
                LoadFunctions(eqid)
                port.Dispose()
                sb.Append("<tr><td width=""100""></td>")
                sb.Append("<td width=""100""></td>")
                sb.Append("<td width=""100""></td>")
                sb.Append("<td width=""100""></td></tr>")
                sb.Append("</table>")
                tdpics.InnerHtml = sb.ToString
            ElseIf Request.Form("lblsubmit") = "getoco" Then
                lblsubmit.Value = ""
                eqid = lbleqid.Value
                fuid = lblofuid.Value
                comid = lblocoid.Value
                lblcur.Value = fuid
                port.Open()
                LoadEqPics(eqid)
                If fuid <> "0" Then
                    LoadFUPics(eqid, fuid)
                Else
                    sb.Append("<tr><td colspan=""4"" >")
                    sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                    sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                    sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl466" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                    sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                    sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl467" , "pmportfolio.aspx.vb") & "</td></tr>")
                End If
                If comid <> "0" Then
                    LoadCOPics(fuid, comid)
                Else
                    If fuid <> "0" Then
                        LoadCOFUPics(eqid, fuid)
                        LoadComponents(fuid)
                    Else
                        sb.Append("<tr><td colspan=""4"" >")
                        sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                        sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                        sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl468" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                        'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Component Images</td></tr>")
                        sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                        sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl469" , "pmportfolio.aspx.vb") & "</td></tr>")
                    End If

                End If
                LoadFunctions(eqid)
                port.Dispose()
                sb.Append("<tr><td width=""100""></td>")
                sb.Append("<td width=""100""></td>")
                sb.Append("<td width=""100""></td>")
                sb.Append("<td width=""100""></td></tr>")
                sb.Append("</table>")
                tdpics.InnerHtml = sb.ToString
            End If
        End If
    End Sub
    Private Sub LoadCOFUPics(ByVal eqid As String, ByVal fuid As String)

        'sb.Append("<table>")
        sb.Append("<tr><td colspan=""4"" >")
        sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
        sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
        sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl470" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
        'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Component Images</td></tr>")
        'sb.Append("<tr><td colspan=""4""><hr color=""blue"" size=""1px""></td></tr>")
        sql = "select pic_id, picurltn from pmpictures where funcid = '" & fuid & "' and comid is not null"
        dr = port.GetRdrData(sql)
        'sb.Append("<tr>")
        x = 1
        Dim start As Integer = 0
        While dr.Read
            If start = 0 Then
                start = 1
            End If
            If x = 1 Then
                sb.Append("<tr>")
            End If
            sb.Append("<td><a href=""#"" onclick=""getbig('" + dr.Item("pic_id").ToString + "');"">" _
            + "<img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltn").ToString + """>" _
            + "</a></td>")
            x = x + 1
            If x = 5 Then
                sb.Append("</tr>")
                x = 1
            End If
        End While
        Dim Remainder As Integer
        Remainder = x Mod 4
        If Remainder > 0 Then
            sb.Append("</tr>")
        End If
        dr.Close()
        If start = 0 Then
            sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl471" , "pmportfolio.aspx.vb") & "</td></tr>")
            sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
        End If
        'sb.Append("</table>")
        'Response.Write(sb.ToString)
        'tdpics.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadFUPics(ByVal eqid As String, ByVal fuid As String)
        'Dim sb As New StringBuilder
        'sb.Append("<table>")

        sql = "select p.pic_id, p.picurltn, f.func from pmpictures p left join functions f on f.func_id = p.funcid " _
        + "where p.eqid = '" & eqid & "' and " _
        + "p.funcid = '" & fuid & "' and comid is null"

        dr = port.GetRdrData(sql)
        'sb.Append("<tr>")
        x = 1
        Dim start As Integer = 0
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<tr><td colspan=""4"" >")
                sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl472" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Function Images</td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Current Function Record:</font> " & dr.Item("func").ToString & "</td></tr>")

                sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            End If
            If x = 1 Then
                sb.Append("<tr>")
            End If
            sb.Append("<td><a href=""#"" onclick=""getbig('" + dr.Item("pic_id").ToString + "');"">" _
            + "<img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltn").ToString + """>" _
            + "</a></td>")
            x = x + 1
            If x = 5 Then
                sb.Append("</tr>")
                x = 1
            End If
        End While
        Dim Remainder As Integer
        Remainder = x Mod 4
        If Remainder > 0 Then
            sb.Append("</tr>")
        End If
        dr.Close()
        If start = 0 Then
            sql = "select distinct func from functions where func_id = '" & fuid & "'"
            dr = port.GetRdrData(sql)
            While dr.Read
                sb.Append("<tr><td colspan=""4"" >")
                sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl473" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Function Images</td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Current Function Record:</font> " & dr.Item("func").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl474" , "pmportfolio.aspx.vb") & "</td></tr>")
            End While
            dr.Close()
            sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
        End If
        'sb.Append("</table>")
        'Response.Write(sb.ToString)
        'tdpics.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadCOPics(ByVal fuid As String, ByVal comid As String)
        'Dim sb As New StringBuilder
        'sb.Append("<table>")

        sql = "select p.pic_id, p.picurltn, c.compnum from pmpictures p left join components c on c.comid = p.comid " _
        + "where p.funcid = '" & fuid & "' and p.comid = '" & comid & "'"
        dr = port.GetRdrData(sql)
        'sb.Append("<tr>")
        x = 1
        Dim start As Integer = 0
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<tr><td colspan=""4"" >")
                sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl475" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Component Images</td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Current Component Record:</font> " & dr.Item("compnum").ToString & "</td></tr>")

                sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            End If
            If x = 1 Then
                sb.Append("<tr>")

            End If
            sb.Append("<td><a href=""#"" onclick=""getbig('" + dr.Item("pic_id").ToString + "');"">" _
            + "<img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltn").ToString + """>" _
            + "</a></td>")
            x = x + 1
            If x = 5 Then
                sb.Append("</tr>")
                x = 1
            End If
        End While
        Dim Remainder As Integer
        Remainder = x Mod 4
        If Remainder > 0 Then
            sb.Append("</tr>")
        End If
        dr.Close()

        If start = 0 Then
            sql = "select distinct compnum from components where comid = '" & comid & "'"
            dr = port.GetRdrData(sql)
            While dr.Read
                sb.Append("<tr><td colspan=""4"" >")
                sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl476" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Component Images</td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Current Component Record:</font> " & dr.Item("compnum").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl477" , "pmportfolio.aspx.vb") & "</td></tr>")
            End While
            dr.Close()
            sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
        End If
        'sb.Append("</table>")
        'Response.Write(sb.ToString)
        'tdpics.InnerHtml = sb.ToString
    End Sub
    Private Sub LoadComponents(ByVal fuid As String)
        Dim sbc As New StringBuilder
        sbc.Append("<table cellspacing=""2"" cellpadding=""2"">")
        sbc.Append("<tr><td colspan=""4"" >")
        sbc.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
        sbc.Append("<img src=""../images/appbuttons/minibuttons/eqarch.gif""></td>")
        sbc.Append("<td class=""thdrsingrt label"">Function Component Records&nbsp;&nbsp;</td></tr></table></td></tr>")
        'sbc.Append("<tr><td colspan=""4"" class=""thdrsing label""><IMG src=""../images/appbuttons/minibuttons/eqarch.gif"">&nbsp;Function Component Records</td></tr>")
        'sbf.Append("<tr><td colspan=""4""><hr color=""blue"" size=""1px""></td></tr>")
        sql = "select c.comid, c.compnum, f.func from components c left join functions f on f.func_id = c.func_id " _
        + "where c.func_id = '" & fuid & "'"
        dr = port.GetRdrData(sql)
        'sbf.Append("<tr>")
        x = 1
        ocomid = lblocoid.Value
        Dim start As Integer = 0
        While dr.Read
            If start = 0 Then
                start = 1
                sbc.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Selected Function Record:</font></td></tr>")

                sbc.Append("<tr><td colspan=""4"" class=""label"">" & dr.Item("func").ToString & "</td></tr>")
                If ocomid <> "0" Then
                    sbc.Append("<tr><td colspan=""4"" align=""center""><img src=""../images/appbuttons/minibuttons/refreshit.gif"" onclick=""refit('co');""></td></tr>")
                End If

                sbc.Append("<tr><td colspan=""4""><hr color=""blue"" size=""1px""></td></tr>")
                'sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            End If
            'If x = 1 Then
            'sbf.Append("<tr>")
            'End If
            sbc.Append("<tr><td colspan=""4""><a href=""#"" class=""A1"" onclick=""getcomp('" + dr.Item("comid").ToString + "');"">" _
            + "" & dr.Item("compnum").ToString & "" _
            + "</a></td>")
            sbc.Append("</tr>")
        End While
        dr.Close()
        If start = 0 Then
            sql = "select distinct func from functions where func_id = '" & fuid & "'"
            dr = port.GetRdrData(sql)
            While dr.Read
                sbc.Append("<tr><td colspan=""4"" >")
                sbc.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                sbc.Append("<img src=""../images/appbuttons/minibuttons/eqarch.gif""></td>")
                sbc.Append("<td class=""thdrsingrt label"">Function Component Records&nbsp;&nbsp;</td></tr></table></td></tr>")
                'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><IMG src=""../images/appbuttons/minibuttons/eqarch.gif"">&nbsp;Function Component Records</td></tr>")
                sbc.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Selected Function Record:</font></td></tr>")
                sbc.Append("<tr><td colspan=""4"" class=""label"">" & dr.Item("func").ToString & "</td></tr>")
                'sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                sbc.Append("<tr><td colspan=""4""><hr color=""blue"" size=""1px""></td></tr>")
                sbc.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl478" , "pmportfolio.aspx.vb") & "</td></tr>")
            End While
            dr.Close()
            'sb.Append("<tr><td colspan=""4"">&nbsp;</td></tr>")
            'sbc.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl479" , "pmportfolio.aspx.vb") & "</td></tr>")
        End If
        sbc.Append("</table>")
        'Response.Write(sb.ToString)
        tdfuco.InnerHtml = sbc.ToString
    End Sub
    Private Sub LoadFunctions(ByVal eqid As String)
        Dim sbf As New StringBuilder
        sbf.Append("<table cellspacing=""2"" cellpadding=""2"">")
        sbf.Append("<tr><td colspan=""4"" >")
        sbf.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
        sbf.Append("<img src=""../images/appbuttons/minibuttons/eqarch.gif""></td>")
        sbf.Append("<td class=""thdrsingrt label"">Equipment Function Records&nbsp;&nbsp;</td></tr></table></td></tr>")
        'sbf.Append("<tr><td colspan=""4"" class=""thdrsing label""><IMG src=""../images/appbuttons/minibuttons/eqarch.gif"">&nbsp;Equipment Function Records</td></tr>")
        sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
        sql = "select f.func_id, f.func, e.eqnum from functions f left join equipment e on e.eqid = f.eqid where f.eqid = '" & eqid & "'"
        dr = port.GetRdrData(sql)
        'sbf.Append("<tr>")
        x = 1
        ofuid = lblofuid.Value
        Dim start As Integer = 0
        While dr.Read
            If start = 0 Then
                start = 1
                sbf.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Selected Equipment Record:</font></td></tr>")

                sbf.Append("<tr><td colspan=""4"" class=""label"">" & dr.Item("eqnum").ToString & "</td></tr>")
                If ofuid <> "0" Then
                    sbf.Append("<tr><td colspan=""4"" align=""center""><img src=""../images/appbuttons/minibuttons/refreshit.gif"" onclick=""refit('fu');""></td></tr>")
                End If

                sbf.Append("<tr><td colspan=""4""><hr color=""blue"" size=""1px""></td></tr>")
            End If
            'If x = 1 Then
            'sbf.Append("<tr>")
            'End If
            sbf.Append("<tr><td colspan=""4""><a href=""#"" class=""A1"" onclick=""getfunc('" + dr.Item("func_id").ToString + "');"">" _
            + "" & dr.Item("func").ToString & "" _
            + "</a></td>")
            sbf.Append("</tr>")
        End While
        dr.Close()
        If start = 0 Then
            sbf.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl480" , "pmportfolio.aspx.vb") & "</td></tr>")
        End If
        sbf.Append("</table>")
        'Response.Write(sb.ToString)
        tdeqfu.InnerHtml = sbf.ToString
    End Sub
    Private Sub LoadEqPics(ByVal eqid As String)
        'Dim sb As New StringBuilder
        sb.Append("<table cellspacing=""2"" cellpadding=""2"">")



        sql = "select p.pic_id, p.picurltn, e.eqnum from pmpictures p left join equipment e on e.eqid = p.eqid " _
        + "where p.eqid = '" & eqid & "' and p.funcid is null and p.comid is null"
        dr = port.GetRdrData(sql)
        'sb.Append("<tr>")
        x = 1
        Dim start As Integer = 0
        While dr.Read
            If start = 0 Then
                start = 1
                sb.Append("<tr><td colspan=""4"" >")
                sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl481" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Equipment Images</td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Current Equipment Record:</font> " & dr.Item("eqnum").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
            End If
            If x = 1 Then
                sb.Append("<tr>")
            End If
            sb.Append("<td><a href=""#"" onclick=""getbig('" + dr.Item("pic_id").ToString + "');"">" _
            + "<img id=""" + dr.Item("pic_id").ToString + """ border=""0"" src=""" + dr.Item("picurltn").ToString + """>" _
            + "</a></td>")
            x = x + 1
            If x = 5 Then
                sb.Append("</tr>")
                x = 1
            End If
        End While
        Dim Remainder As Integer
        Remainder = x Mod 4
        If Remainder > 0 Then
            sb.Append("</tr>")
        End If
        dr.Close()
        If start = 0 Then
            sql = "select distinct eqnum from equipment where eqid = '" & eqid & "'"
            dr = port.GetRdrData(sql)
            While dr.Read
                sb.Append("<tr><td colspan=""4"" >")
                sb.Append("<table cellspacing=""0"" cellpadding=""2""><tr><td width=""24"" class=""thdrsinglft label"">")
                sb.Append("<img src=""../images/appbuttons/minibuttons/gridpichd.gif""></td>")
                sb.Append("<td class=""thdrsingrt label"" width=""378"">" & tmod.getlbl("cdlbl482" , "pmportfolio.aspx.vb") & "</td></tr></table></td></tr>")
                'sb.Append("<tr><td colspan=""4"" class=""thdrsing label""><img src=""../images/appbuttons/minibuttons/gridpichd.gif"">&nbsp;Equipment Images</td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""label""><font class=""bluelabel"">Current Equipment Record:</font> " & dr.Item("eqnum").ToString & "</td></tr>")
                sb.Append("<tr><td colspan=""4""><img src=""../images/appbuttons/minibuttons/4PX.gif""></td></tr>")
                sb.Append("<tr><td colspan=""4"" class=""plainlabel"">" & tmod.getlbl("cdlbl483" , "pmportfolio.aspx.vb") & "</td></tr>")
            End While
            dr.Close()
        End If
        'sb.Append("</table>")
        'Response.Write(sb.ToString)
        'tdpics.InnerHtml = sb.ToString
    End Sub
End Class
