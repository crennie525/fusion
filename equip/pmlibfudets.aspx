<%@ Page Language="vb" AutoEventWireup="false" Codebehind="pmlibfudets.aspx.vb" Inherits="lucy_r12.pmlibfudets" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>pmlibfudets</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<link href="../styles/pmcssa1.css" type="text/css" rel="stylesheet" />
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  type="text/javascript" src="../scripts/imglibnav.js"></script>
		<script  type="text/javascript" src="../scripts1/pmlibfudetsaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     
	</HEAD>
	<body onload="GetsScroll();"  class="tbg">
		<form id="form1" method="post" runat="server">
			<table style="POSITION: absolute; TOP: 0px; LEFT: 0px" width="980">
				<TBODY>
					<tr>
						<td class="labelibl" colSpan="2"><asp:Label id="lang2456" runat="server">Select a Function to view Function Image, Component, and Task Details</asp:Label></td>
					</tr>
					<tr>
						<td vAlign="top" align="center" width="750">
							<div id="spdiv" style="WIDTH: 500px; HEIGHT: 260px; OVERFLOW: auto" onscroll="SetsDivPosition();"><asp:repeater id="rptrfuncrev" runat="server">
									<HeaderTemplate>
										<table>
											<tr>
												<td class="btmmenu plainlabel" width="250px"><asp:Label id="lang2457" runat="server">Function</asp:Label></td>
												<td class="btmmenu plainlabel" width="250px"><asp:Label id="lang2458" runat="server">Special Identifier</asp:Label></td>
											</tr>
									</HeaderTemplate>
									<ItemTemplate>
										<tr  id="selrow" runat="server" bgcolor='<%# HighlightRowFR(DataBinder.Eval(Container.DataItem, "func_id"))%>'>
											<td class="plainlabel"><asp:LinkButton  CommandName="Select" ID="Linkbutton2" Text='<%# DataBinder.Eval(Container.DataItem,"func")%>' Runat = server>
												</asp:LinkButton></td>
											<td class="plainlabel"><asp:Label ID="Label9" Text='<%# DataBinder.Eval(Container.DataItem,"spl")%>' Runat = server>
												</asp:Label></td>
											<td class="details"><asp:Label ID="lblfuncrevid" Text='<%# DataBinder.Eval(Container.DataItem,"func_id")%>' Runat = server>
												</asp:Label></td>
											<td class="details"><asp:Label ID="lblparfu1" Text='<%# DataBinder.Eval(Container.DataItem,"origparent")%>' Runat = server>
												</asp:Label></td>
										</tr>
									</ItemTemplate>
									<FooterTemplate>
			</table>
			</FooterTemplate> </asp:repeater></DIV></TD>
			<td vAlign="top" align="center" width="230">
				<table>
					<tr>
						<td vAlign="top" align="center" colSpan="2"><A onclick="getbig();" href="#"><IMG id="imgfu" height="206" src="../images/appimages/funcimg.gif" style="width: 216px" border="0"
									runat="server"></A>
						</td>
					</tr>
					<tr>
						<td align="center" colSpan="2">
							<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
								cellSpacing="0" cellPadding="0">
								<tr>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirstf" onclick="getpfirstf();" src="../images/appbuttons/minibuttons/lfirst.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprevf" onclick="getpprevf();" src="../images/appbuttons/minibuttons/lprev.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 140px;" vAlign="middle" align="center"><asp:label id="lblpgf" runat="server" CssClass="bluelabel">Image 0 of 0</asp:label></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inextf" onclick="getpnextf();" src="../images/appbuttons/minibuttons/lnext.gif"
											runat="server"></td>
									<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ilastf" onclick="getplastf();" src="../images/appbuttons/minibuttons/llast.gif"
											runat="server"></td>
									<td style="width: 20px"><IMG onclick="getport();" src="../images/appbuttons/minibuttons/picgrid.gif"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			</TR></TBODY></TABLE><input id="lblfuid" type="hidden" runat="server"> <input id="lbleqid" type="hidden" runat="server"><input id="lbldb" type="hidden" runat="server">
			<input id="lblpcntf" type="hidden" name="lblpcntf" runat="server"> <input id="lblcurrpf" type="hidden" name="lblcurrpf" runat="server">
			<input id="lblimgsf" type="hidden" name="lblimgsf" runat="server"> <input id="lblimgidf" type="hidden" name="lblimgidf" runat="server">
			<input id="lblovimgsf" type="hidden" name="lblovimgsf" runat="server"> <input id="lblovbimgsf" type="hidden" name="lblovbimgsf" runat="server">
			<input id="lblcurrimgf" type="hidden" name="lblcurrimgf" runat="server"> <input id="lblcurrbimgf" type="hidden" name="lblcurrbimgf" runat="server">
			<input id="lblbimgsf" type="hidden" name="lblbimgsf" runat="server"><input id="lbliordersf" type="hidden" name="lbliordersf" runat="server">
			<input id="lbloldorderf" type="hidden" name="lbloldorderf" runat="server"> <input id="lblovtimgsf" type="hidden" name="lblovtimgsf" runat="server">
			<input id="lblcurrtimgf" type="hidden" name="lblcurrtimgf" runat="server"> <input id="spdivy" type="hidden" runat="server">
			<input type="hidden" id="lblchk" runat="server"> <input type="hidden" id="lbllog" runat="server" NAME="lbllog">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
