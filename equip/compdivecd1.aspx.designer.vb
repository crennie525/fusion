﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class compdivecd1

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''tdnc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdnc As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''txtnewcomp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtnewcomp As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtnewecd1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtnewecd1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''img1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents img1 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''txtsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtsearch As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ibtnsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ibtnsearch As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''img2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents img2 As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''fdiv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fdiv As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ifirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifirst As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''iprev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iprev As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''lblpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblpg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''inext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents inext As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''ilast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ilast As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''txtpg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpg As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtpgcnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpgcnt As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblret control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblret As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblsubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblsubmit As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblecd1id control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblecd1id As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblecd1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblecd1 As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblcompnum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcompnum As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblnewcompnum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblnewcompnum As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblchange control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblchange As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
