<%@ Page Language="vb" AutoEventWireup="false" Codebehind="maxsearch.aspx.vb" Inherits="lucy_r12.maxsearch" %>
<!DOCTYPE html>
<html lang="en">
	<HEAD>
		<title>maxsearch</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<link rel="stylesheet" type="text/css" href="../styles/pmcssa1.css" />
		<!--<script  type="text/javascript" src="../scripts/smartscroll.js"></script>-->
		<script  type="text/javascript" src="../scripts/overlib2.js"></script>
		
		<script  type="text/javascript" src="../scripts1/maxsearchaspx.js"></script>
     <script  type="text/javascript" src="../scripts2/jsfslangs.js"></script>
     <script  type="text/javascript">
         function checkgo() {
             var dept = document.getElementById("lbldept").value;
             //alert(dept)
             dept = dept.replace(",", "")
             var cell = document.getElementById("lblcell").value;
             //alert(cell)
             cell = cell.replace(",", "")
             var did = document.getElementById("lbldid").value;
             //alert(did)
             var clid = document.getElementById("lblclid").value;
             //alert(clid)
             var eqid = document.getElementById("lbleqid").value;
             //alert(eqid)
             var eqnum = document.getElementById("lbleqnum").value;
             //alert(eqnum)
             eqnum = eqnum.replace(",", "")
             var fuid = document.getElementById("lblfuid").value;
             //alert(fuid)
             var func = document.getElementById("lblfunc").value;
             //alert(func)
             func = func.replace(",", "")
             var comp = document.getElementById("lblcomp").value;
             //alert(comp)
             comp = comp.replace("," ,"")
             var chk = document.getElementById("lblchk").value;
             //alert(chk)
             var lid = document.getElementById("lbllid").value;
             //alert(lid)
             var sid = document.getElementById("lblsidret").value;
             //alert(sid)
             var cid = document.getElementById("lblcoid").value;
             //alert(cid)
             if (cid == "0") {
                 cid = "0";
             }
             var ret = did + "," + clid + "," + eqid + "," + eqnum + "," + fuid + "," + func + "," + comp + "," + dept + "," + cell + "," + chk + "," + lid + "," + sid + "," + cid;
             //alert(ret)
             window.parent.handlereturn(ret);

         }
     </script>
	</HEAD>
	<body  onload="checkfunc();">
		<form id="form1" method="post" runat="server">
			<table cellspacing="0" width="804" id="scrollmenu">
				<tr id="trhead" runat="server">
					<td>
						<table cellspacing="0" cellpadding="0">
							<tr>
								<td class="thdrsinglft label" style="width: 26px"><img src="../images/appbuttons/minibuttons/maghdr.gif"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2409" runat="server">Search Records</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trmain" runat="server">
					<td colspan="2">
						<table>
							<tr>
								<td vAlign="top">
									<table width="300" cellpadding="2" cellspacing="0">
										<tr>
											<td class="btmmenu plainlabel" colSpan="2"><asp:Label id="lang2410" runat="server">Choose From Drill Down</asp:Label></td>
										</tr>
										<tr>
											<td class="label"><asp:Label id="lang2411" runat="server">Department</asp:Label></td>
											<td><asp:dropdownlist id="dddepts" runat="server" AutoPostBack="True" CssClass="plainlabel" Width="190"></asp:dropdownlist></td>
										</tr>
										<tr id="celldiv" runat="server">
											<td class="label"><asp:Label id="lang2412" runat="server">Station/Cell</asp:Label></td>
											<td><asp:dropdownlist id="ddcells" runat="server" AutoPostBack="True" CssClass="plainlabel" Width="190"></asp:dropdownlist></td>
										</tr>
										<tr id="eqdiv" runat="server">
											<td class="label"><asp:Label id="lang2413" runat="server">Equipment#</asp:Label></td>
											<td><asp:dropdownlist id="ddeq" runat="server" AutoPostBack="True" CssClass="plainlabel" Width="190"></asp:dropdownlist></td>
										</tr>
										<tr>
											<td class="btmmenu plainlabel" colSpan="2"><asp:Label id="lang2414" runat="server">Search Equipment</asp:Label></td>
										</tr>
										<tr>
											<td class="label" style="width: 80px"><asp:Label id="lang2415" runat="server">Equipment#</asp:Label></td>
											<td width="200"><asp:textbox id="txteqnum" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="label"><asp:Label id="lang2416" runat="server">Description</asp:Label></td>
											<td><asp:textbox id="txteqdesc" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="label">SPL</td>
											<td><asp:textbox id="txtspl" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="label">OEM</td>
											<td><asp:textbox id="txtoem" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="label"><asp:Label id="lang2417" runat="server">Model</asp:Label></td>
											<td><asp:textbox id="txtmodel" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="label"><asp:Label id="lang2418" runat="server">Serial#</asp:Label></td>
											<td><asp:textbox id="txtserial" runat="server" CssClass="plainlabel" Width="160px"></asp:textbox></td>
										</tr>
										<tr>
											<td class="label"><asp:Label id="lang2419" runat="server">Asset Class</asp:Label></td>
											<td><asp:dropdownlist id="ddac" runat="server" CssClass="plainlabel" Width="190"></asp:dropdownlist></td>
										</tr>
										<tr>
											<td align="right" colSpan="2"><IMG onclick="srcheq();" src="../images/appbuttons/minibuttons/srchsm.gif">
												<IMG onclick="goback();" src="../images/appbuttons/minibuttons/refreshit.gif">
											</td>
										</tr>
									</table>
								</td>
								<td vAlign="top">
									<table style="width: 270px" cellpadding="2" cellspacing="0">
										<tr>
											<td class="btmmenu plainlabel"><asp:Label id="lang2420" runat="server">Equipment Records</asp:Label></td>
										</tr>
										<tr>
											<td>
												<div id="diveq" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 270px; HEIGHT: 234px; OVERFLOW: auto; BORDER-TOP: black 1px solid; BORDER-RIGHT: black 1px solid"
													runat="server"></div>
											</td>
										</tr>
										<tr>
											<td align="center">
												<table style="BORDER-BOTTOM: blue 1px solid; BORDER-LEFT: blue 1px solid; BORDER-TOP: blue 1px solid; BORDER-RIGHT: blue 1px solid"
													cellSpacing="0" cellPadding="0">
													<tr>
														<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="ifirst" onclick="getfirst();" src="../images/appbuttons/minibuttons/lfirst.gif"
																runat="server"></td>
														<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="iprev" onclick="getprev();" src="../images/appbuttons/minibuttons/lprev.gif"
																runat="server"></td>
														<td style="BORDER-RIGHT: blue 1px solid" vAlign="middle" align="center" width="190"><asp:label id="lblpg" runat="server" CssClass="bluelabellt">Page 1 of 1</asp:label></td>
														<td style="BORDER-RIGHT: blue 1px solid; width: 20px;"><IMG id="inext" onclick="getnext();" src="../images/appbuttons/minibuttons/lnext.gif"
																runat="server"></td>
														<td style="width: 20px"><IMG id="ilast" onclick="getlast();" src="../images/appbuttons/minibuttons/llast.gif"
																runat="server"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td vAlign="top">
									<table style="width: 204px" cellpadding="2" cellspacing="0">
										<tr>
											<td class="btmmenu plainlabel"><asp:Label id="lang2421" runat="server">Equipment Functions</asp:Label></td>
										</tr>
										<tr>
											<td><iframe id="iffunc" style="BACKGROUND-COLOR: transparent" src="funclist2.aspx" frameBorder="no"
													width="202" scrolling="no" height="262"  runat="server"></iframe>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="Tr1" runat="server">
					<td>
						<table cellspacing="0" cellpadding="0">
							<tr>
								<td class="thdrsinglft label" style="width: 26px"><img src="../images/appbuttons/minibuttons/maghdr.gif"></td>
								<td class="thdrsingrt label" align="left" width="764"><asp:Label id="lang2422" runat="server">Search Results</asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="trsave" runat="server" class="details">
					<td align="right"><img src="../images/appbuttons/minibuttons/savedisk1.gif" onclick="checkgo();" onmouseover="return overlib('Save Selection and Return', LEFT)"
							onmouseout="return nd()"></td>
				</tr>
				<tr>
					<td id="tdeq" runat="server" height="26"></td>
				</tr>
				<tr>
					<td id="tdfu" runat="server" height="26"></td>
				</tr>
				<tr>
					<td id="tdco" runat="server" height="26"></td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel"><asp:Label id="lang2423" runat="server">Equipment Details</asp:Label></td>
				</tr>
				<tr>
					<td>
						<iframe id="ifeq" style="BACKGROUND-COLOR: transparent" src="eqdets.aspx?start=no" frameBorder="no"
							width="600" scrolling="no" height="150"  runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel"><asp:Label id="lang2424" runat="server">Function Details</asp:Label></td>
				</tr>
				<tr>
					<td>
						<iframe id="iffu" style="BACKGROUND-COLOR: transparent" src="fudets.aspx?start=no" frameBorder="no"
							width="600" scrolling="no" height="90"  runat="server"></iframe>
					</td>
				</tr>
				<tr>
					<td class="btmmenu plainlabel"><asp:Label id="lang2425" runat="server">Component Details</asp:Label></td>
				</tr>
				<tr>
					<td>
						<iframe id="ifco" style="BACKGROUND-COLOR: transparent" src="codets.aspx?start=no" frameBorder="no"
							width="800" scrolling="no" height="550"  runat="server"></iframe>
					</td>
				</tr>
			</table>
			<input type="hidden" id="lblsid" runat="server"> <input type="hidden" id="lblcid" runat="server">
			<input type="hidden" id="txtpg" runat="server"> <input type="hidden" id="lbleqnumsrch" runat="server">
			<input type="hidden" id="lbleqdesc" runat="server"> <input type="hidden" id="lblspl" runat="server">
			<input type="hidden" id="lbloem" runat="server"> <input type="hidden" id="lblmodel" runat="server">
			<input type="hidden" id="lblserial" runat="server"> <input type="hidden" id="lblac" runat="server">
			<input type="hidden" id="txtpgcnt" runat="server"> <input type="hidden" id="lbldid" runat="server">
			<input type="hidden" id="lblclid" runat="server"> <input type="hidden" id="lbleqid" runat="server">
			<input type="hidden" id="lblfuid" runat="server"> <input type="hidden" id="lblcoid" runat="server">
			<input type="hidden" id="lblgofunc" runat="server"> <input type="hidden" id="lblsubmit" runat="server">
			<input type="hidden" id="lblsrchtyp" runat="server"> <input type="hidden" id="lbleqnum" runat="server">
			<input type="hidden" id="lblfunc" runat="server"> <input type="hidden" id="lblcomp" runat="server">
			<input type="hidden" id="lblgo" runat="server"> <input type="hidden" id="lbldept" runat="server">
			<input type="hidden" id="lblcell" runat="server"> <input type="hidden" id="lblchk" runat="server">
			<input type="hidden" id="lbllid" runat="server"><input type="hidden" id="lblsidret" runat="server">
			<input type="hidden" id="lbllib" runat="server"> <input id="xCoord" type="hidden" runat="server" /><input id="yCoord" type="hidden" runat="server" />  <input type="hidden" id="lblrep" runat="server">
			<input type="hidden" id="lblret" runat="server">
		
<input type="hidden" id="lblfslang" runat="server" />
</form>
	</body>
</HTML>
